﻿-- Table: mst_consumption_tax

-- DROP TABLE mst_consumption_tax;

CREATE TABLE mst_consumption_tax
(
  id serial NOT NULL, -- 消費税管理主キー
  tax_rate numeric(3,2) NOT NULL, -- 消費税率
  application_start_date timestamp without time zone NOT NULL, -- 適用開始日
  CONSTRAINT mst_consumption_tax_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mst_consumption_tax
  OWNER TO postgres;
COMMENT ON TABLE mst_consumption_tax
  IS '消費税管理';
COMMENT ON COLUMN mst_consumption_tax.id IS '消費税管理主キー';
COMMENT ON COLUMN mst_consumption_tax.tax_rate IS '消費税率';
COMMENT ON COLUMN mst_consumption_tax.application_start_date IS '適用開始日';
