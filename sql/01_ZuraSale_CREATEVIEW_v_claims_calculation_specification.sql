-- View: v_claims_calculation_specification

-- DROP VIEW v_claims_calculation_specification;

CREATE OR REPLACE VIEW v_claims_calculation_specification AS 
 SELECT present_purchase.facility_id,
    present_purchase.purchase_date,
    date_trunc('month'::text, present_purchase.purchase_date) + '2 mons'::interval + '-1 days'::interval AS billing_date,
    sum(present_purchase.not_extension_price) + sum(present_purchase.not_extension_tax) + sum(
        CASE
            WHEN present_purchase.commission > 0::numeric THEN present_purchase.commission
            ELSE 0::numeric
        END) + sum(
        CASE
            WHEN present_purchase.commission_tax > 0::numeric THEN present_purchase.commission_tax
            ELSE 0::numeric
        END) + sum(
        CASE
            WHEN anymonthlater.price > 0::numeric THEN anymonthlater.price
            ELSE 0::numeric
        END) + sum(
        CASE
            WHEN anymonthlater.tax > 0::numeric THEN anymonthlater.tax
            ELSE 0::numeric
        END) AS payment_total,
    sum(present_purchase.not_extension_tax) + sum(
        CASE
            WHEN present_purchase.commission_tax > 0::numeric THEN present_purchase.commission_tax
            ELSE 0::numeric
        END) + sum(
        CASE
            WHEN anymonthlater.tax > 0::numeric THEN anymonthlater.tax
            ELSE 0::numeric
        END) AS payment_tax,
    sum(present_purchase.not_extension_price) + sum(
        CASE
            WHEN present_purchase.commission > 0::numeric THEN present_purchase.commission
            ELSE 0::numeric
        END) + sum(
        CASE
            WHEN anymonthlater.price > 0::numeric THEN anymonthlater.price
            ELSE 0::numeric
        END) AS payment_price,
    present_purchase.total_price AS present_month_price,
    present_purchase.total_tax AS present_month_tax,
    present_purchase.not_extension_price AS present_month_not_extension_price,
    present_purchase.not_extension_tax AS present_month_not_extension_tax,
    present_purchase.extension_price AS present_month_extension_price,
    present_purchase.extension_tax AS present_month_extension_tax,
    present_purchase.commission AS present_month_commission,
    present_purchase.commission_tax AS present_month_commission_tax,
    sum(
        CASE
            WHEN anymonthlater.price > 0::numeric THEN anymonthlater.price
            ELSE 0::numeric
        END) AS all_month_before_price,
    sum(
        CASE
            WHEN anymonthlater.tax > 0::numeric THEN anymonthlater.tax
            ELSE 0::numeric
        END) AS all_month_before_tax,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 1 THEN anymonthlater.price
            ELSE 0::numeric
        END) AS _1month_before_price,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 1 THEN anymonthlater.tax
            ELSE 0::numeric
        END) AS _1month_before_tax,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 2 THEN anymonthlater.price
            ELSE 0::numeric
        END) AS _2month_before_price,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 2 THEN anymonthlater.tax
            ELSE 0::numeric
        END) AS _2month_before_tax,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 3 THEN anymonthlater.price
            ELSE 0::numeric
        END) AS _3month_before_price,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 3 THEN anymonthlater.tax
            ELSE 0::numeric
        END) AS _3month_before_tax,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 4 THEN anymonthlater.price
            ELSE 0::numeric
        END) AS _4month_before_price,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 4 THEN anymonthlater.tax
            ELSE 0::numeric
        END) AS _4month_before_tax,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 5 THEN anymonthlater.price
            ELSE 0::numeric
        END) AS _5month_before_price,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 5 THEN anymonthlater.tax
            ELSE 0::numeric
        END) AS _5month_before_tax,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 6 THEN anymonthlater.price
            ELSE 0::numeric
        END) AS _6month_before_price,
    sum(
        CASE
            WHEN anymonthlater.zura_number_of_month = 6 THEN anymonthlater.tax
            ELSE 0::numeric
        END) AS _6month_before_tax
   FROM ( SELECT facility.id AS facility_id,
            facility.zura_maximum_amount,
            date_trunc('month'::text, claims.claim_date) AS purchase_date,
            trunc(sum(claims.claim_price), 0) AS total_price,
            trunc(sum(claims.claim_price) * tax.tax_rate, 0) AS total_tax,
            trunc(sum(
                CASE
                    WHEN claims.zura_number_of_month = 0 THEN claims.claim_price
                    ELSE 0::numeric
                END), 0) AS not_extension_price,
            trunc(sum(
                CASE
                    WHEN claims.zura_number_of_month = 0 THEN claims.claim_price * tax.tax_rate
                    ELSE 0::numeric
                END), 0) AS not_extension_tax,
            trunc(sum(
                CASE
                    WHEN claims.zura_number_of_month > 0 THEN claims.claim_price
                    ELSE 0::numeric
                END), 0) AS extension_price,
            trunc(sum(
                CASE
                    WHEN claims.zura_number_of_month > 0 THEN claims.claim_price * tax.tax_rate
                    ELSE 0::numeric
                END), 0) AS extension_tax,
            trunc(sum(
                CASE
                    WHEN claims.zura_number_of_month > 0 THEN claims.claim_price * facility.zura_commission_rate * claims.zura_number_of_month::numeric
                    ELSE 0::numeric
                END), 0) AS commission,
            trunc(sum(
                CASE
                    WHEN claims.zura_number_of_month > 0 THEN claims.claim_price * facility.zura_commission_rate * claims.zura_number_of_month::numeric * tax.tax_rate
                    ELSE 0::numeric
                END), 0) AS commission_tax
           FROM trn_claims claims
      JOIN mst_departments department ON claims.department_id_to = department.id
   JOIN mst_facilities facility ON department.mst_facility_id = facility.id
   JOIN ( SELECT main.tax_rate
         FROM mst_consumption_tax main
    JOIN trn_claims sub ON main.application_start_date < sub.claim_date
   ORDER BY main.application_start_date DESC
  LIMIT 1) tax ON true
  WHERE claims.is_deleted = false AND claims.is_stock_or_sale = '2'::bpchar AND facility.facility_type = 2
  GROUP BY facility.id, date_trunc('month'::text, claims.claim_date), facility.zura_maximum_amount, tax.tax_rate
  ORDER BY facility.id, date_trunc('month'::text, claims.claim_date)) present_purchase
   LEFT JOIN ( SELECT facility.id AS facility_id,
            claims.zura_number_of_month,
            date_trunc('month'::text, claims.claim_date) AS purchase_date,
            trunc(sum(claims.claim_price), 0) AS price,
            trunc(sum(claims.claim_price) * tax.tax_rate, 0) AS tax
           FROM trn_claims claims
      JOIN mst_departments department ON claims.department_id_to = department.id
   JOIN mst_facilities facility ON department.mst_facility_id = facility.id
   JOIN ( SELECT main.tax_rate
         FROM mst_consumption_tax main
    JOIN trn_claims sub ON main.application_start_date < sub.claim_date
   ORDER BY main.application_start_date DESC
  LIMIT 1) tax ON true
  WHERE claims.zura_number_of_month > 0 AND claims.is_deleted = false AND claims.is_stock_or_sale = '2'::bpchar AND facility.facility_type = 2
  GROUP BY facility.id, date_trunc('month'::text, claims.claim_date), claims.zura_number_of_month, tax.tax_rate
  ORDER BY facility.id, date_trunc('month'::text, claims.claim_date), claims.zura_number_of_month) anymonthlater ON (anymonthlater.purchase_date = (present_purchase.purchase_date + '-1 mons'::interval) AND anymonthlater.zura_number_of_month = 1 OR anymonthlater.purchase_date = (present_purchase.purchase_date + '-2 mons'::interval) AND anymonthlater.zura_number_of_month = 2 OR anymonthlater.purchase_date = (present_purchase.purchase_date + '-3 mons'::interval) AND anymonthlater.zura_number_of_month = 3 OR anymonthlater.purchase_date = (present_purchase.purchase_date + '-4 mons'::interval) AND anymonthlater.zura_number_of_month = 4 OR anymonthlater.purchase_date = (present_purchase.purchase_date + '-5 mons'::interval) AND anymonthlater.zura_number_of_month = 5 OR anymonthlater.purchase_date = (present_purchase.purchase_date + '-6 mons'::interval) AND anymonthlater.zura_number_of_month = 6) AND anymonthlater.facility_id = present_purchase.facility_id
  GROUP BY present_purchase.facility_id, present_purchase.zura_maximum_amount, present_purchase.purchase_date, present_purchase.total_price, present_purchase.total_tax, present_purchase.not_extension_price, present_purchase.not_extension_tax, present_purchase.extension_price, present_purchase.extension_tax, present_purchase.commission, present_purchase.commission_tax
  ORDER BY present_purchase.facility_id, present_purchase.purchase_date;

ALTER TABLE v_claims_calculation_specification
  OWNER TO postgres;
