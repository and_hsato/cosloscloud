﻿ALTER TABLE trn_claims DROP COLUMN zura_claim_date;
ALTER TABLE trn_claims ADD COLUMN zura_number_of_month smallint DEFAULT 0;

COMMENT ON COLUMN trn_claims.zura_number_of_months IS '延伸月数';