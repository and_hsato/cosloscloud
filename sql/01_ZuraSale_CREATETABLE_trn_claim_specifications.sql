﻿-- Table: trn_claim_specifications

-- DROP TABLE trn_claim_specifications;

CREATE TABLE trn_claim_specifications
(
  id serial NOT NULL, -- 請求明細主キー
  mst_facility_id integer NOT NULL, -- 請求先施設ID
  closing_date timestamp without time zone NOT NULL, -- 締め日
  deposit_plans_date timestamp without time zone NOT NULL, -- 入金予定日
  last_month_carry_over numeric(12,2), -- 先月繰越金額　税抜き
  last_month_carry_over_in_tax numeric(12,2), -- 先月繰越金額　税込
  carry_over numeric(12,2), -- 繰越金額　税抜き
  carry_over_in_tax numeric(12,2), -- 繰越金額　税込
  purchase_price numeric(12,2), -- 当月購入金額　税抜き
  purchase_in_tax numeric(12,2), -- 当月購入金額　税込
  commission numeric(12,2), -- 延伸手数料　税抜き
  commission_in_tax numeric(12,2), -- 延伸手数料　税込
  extension_price numeric(12,2), -- 過去延伸購入金額 税抜き
  extension_in_tax numeric(12,2), -- 過去延伸購入金額　税込
  total_tax numeric(12,2), -- 消費税合計
  payment_track_record numeric(12,2), -- 入金実績　税抜き
  payment_track_record_in_tax numeric(12,2), -- 入金実績　税込
  is_deleted boolean NOT NULL DEFAULT false, -- 削除フラグ
  created timestamp without time zone, -- 作成日時
  modified timestamp without time zone, -- 更新日時
  creater integer, -- 作成者
  modifier integer, -- 更新者
  CONSTRAINT trn_claim_specifications_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE trn_claim_specifications
  OWNER TO postgres;
COMMENT ON TABLE trn_claim_specifications
  IS '請求明細';
COMMENT ON COLUMN trn_claim_specifications.id IS '請求明細主キー';
COMMENT ON COLUMN trn_claim_specifications.mst_facility_id IS '請求先施設ID';
COMMENT ON COLUMN trn_claim_specifications.closing_date IS '締め日';
COMMENT ON COLUMN trn_claim_specifications.deposit_plans_date IS '入金予定日';
COMMENT ON COLUMN trn_claim_specifications.last_month_carry_over IS '先月繰越金額　税抜き';
COMMENT ON COLUMN trn_claim_specifications.last_month_carry_over_in_tax IS '先月繰越金額　税込';
COMMENT ON COLUMN trn_claim_specifications.carry_over IS '繰越金額　税抜き';
COMMENT ON COLUMN trn_claim_specifications.carry_over_in_tax IS '繰越金額　税込';
COMMENT ON COLUMN trn_claim_specifications.purchase_price IS '当月購入金額　税抜き';
COMMENT ON COLUMN trn_claim_specifications.purchase_in_tax IS '当月購入金額　税込';
COMMENT ON COLUMN trn_claim_specifications.commission IS '延伸手数料　税抜き';
COMMENT ON COLUMN trn_claim_specifications.commission_in_tax IS '延伸手数料　税込';
COMMENT ON COLUMN trn_claim_specifications.extension_price IS '過去延伸購入金額 税抜き';
COMMENT ON COLUMN trn_claim_specifications.extension_in_tax IS '過去延伸購入金額　税込';
COMMENT ON COLUMN trn_claim_specifications.total_tax IS '消費税合計';
COMMENT ON COLUMN trn_claim_specifications.payment_track_record IS '入金実績　税抜き';
COMMENT ON COLUMN trn_claim_specifications.payment_track_record_in_tax IS '入金実績　税込';
COMMENT ON COLUMN trn_claim_specifications.is_deleted IS '削除フラグ';
COMMENT ON COLUMN trn_claim_specifications.created IS '作成日時';
COMMENT ON COLUMN trn_claim_specifications.modified IS '更新日時';
COMMENT ON COLUMN trn_claim_specifications.creater IS '作成者';
COMMENT ON COLUMN trn_claim_specifications.modifier IS '更新者';

