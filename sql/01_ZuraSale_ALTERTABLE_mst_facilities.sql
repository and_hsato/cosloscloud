﻿ALTER TABLE mst_facilities ADD COLUMN zura_maximum_amount numeric(12,2) DEFAULT 0.00;
ALTER TABLE mst_facilities ADD COLUMN zura_commission_rate numeric(4,3) DEFAULT 0.000;

COMMENT ON COLUMN mst_facilities.zura_maximum_amount IS '延伸上限金額 税抜き';
COMMENT ON COLUMN mst_facilities.zura_commission_rate IS '延伸手数料率';