﻿<?php
/**
 * code_defines.php
 * コード定数 (区分・状態)定義ファイル
 * @version 1.0.0
 * @since  2010.04.08
 */
/**************************************************************/
//バージョン番号
Configure::write('System.version' ,'1.1504.061');

//ユーザパスワードの暗号化
Configure::write('Password.Security' , 0 ); //暗号化する：1 暗号化しない:0

// 管理者メールアドレス
Configure::write('admin.inquiry.emails', 'sato@andinfo.co.jp');


//秘密の質問
Configure::write('secretQuestions', array(
        '利用者名は？', // 未設定の場合は、これをデフォルトとする。
        '母親の旧姓は？',
        '最初に飼ったペットの名前は？',
        '卒業した小学校の名前は？',
));

// 売上設定自動作成フラグ
Configure::write('SalesCreate.flag',1);  //無効 :0 有効:1

// 分割テーブル対応フラグ
Configure::write('SplitTable.flag',1);  //無効 :0 有効:1

// Menuタイプ
Configure::write('Menu.type',1);  //mbMemu形式 :0 BoxMenu形式:1

// ロールタイプ
Configure::write('role.type.system',  1);
Configure::write('role.type.edi',     2);
Configure::write('role.type.lite',    3);
Configure::write('role.type.catalog', 4);

// エディション
Configure::write('edition.non', 1); // 設定なし
Configure::write('edition.std', 2); // スタンダード
Configure::write('edition.prm', 3); // プレミア
Configure::write('edition.plt', 4); // プラチナ

// 請求延伸手数料係数
Configure::write('claim.delay.coefficient', 0.03);

// 制限がないことを意味する数値
Configure::write('numeric.unrestraint', -1);

// カタログURL
Configure::write('catalog.url' ,'http://157.7.212.53:7896/');

//サブコード
Configure::write('facility_subcode' ,'サブコード');  // サブコード
Configure::write('facility_subcode1','サブコード1'); // サブコード1
Configure::write('facility_subcode2','サブコード2'); // サブコード2
Configure::write('facility_subcode3','サブコード3'); // サブコード3

//採番管理の採番区分
Configure::write('NumberType.Update'             ,'1');   //グランドマスタ更新
Configure::write('NumberType.Numbering_hospital' ,'2');   //部署シール番号
Configure::write('NumberType.Numbering_center'   ,'3');   //センターシール番号
Configure::write('NumberType.WorkNo_Consume'     ,'4');   //消費
Configure::write('NumberType.WorkNo_ExPromise'   ,'5');   //臨時要求作成
Configure::write('NumberType.WorkNo_AddPromise'  ,'6');   //引当要求作成
Configure::write('NumberType.WorkNo_Promise'     ,'7');   //引当登録
Configure::write('NumberType.WorkNo_StickerIssue','8');   //シール発行
Configure::write('NumberType.WorkNo_Shipping'    ,'9');   //出荷
Configure::write('NumberType.WorkNo_Receipt'     ,'10');  //受領
Configure::write('NumberType.WorkNo_Order'       ,'11');  //発注
Configure::write('NumberType.WorkNo_Receiving'   ,'12');  //入荷
Configure::write('NumberType.WorkNo_Stock'       ,'13');  //入庫
Configure::write('NumberType.WorkNo_SetStock'    ,'14');  //セット組作業
Configure::write('NumberType.WorkNo_Repay'       ,'15');  //返却
Configure::write('NumberType.WorkNo_Return'      ,'16');  //返品
Configure::write('NumberType.WorkNo_Move'        ,'17');  //移動
Configure::write('NumberType.WorkNo_Inventory'   ,'18');  //棚卸
Configure::write('NumberType.WorkNo_Expiration'  ,'19');  //期限切れ
Configure::write('NumberType.WorkNo_D2customer'  ,'20');  //直納品登録
Configure::write('NumberType.WorkNo_Close'       ,'21');  //締め
Configure::write('NumberType.WorkNo_Retroact'    ,'22');  //遡及
Configure::write('NumberType.WorkNo_SalesRequest','23');  //売上依頼表

Configure::write('NumberType.WorkNo_EdiReciving' ,'24');  //業者受注
Configure::write('NumberType.WorkNo_EdiShipping' ,'25');  //業者出荷
Configure::write('NumberType.WorkNo_Proposal'    ,'26');  //見積依頼
Configure::write('NumberType.WorkNo_RfProposal'  ,'27');  //見積

Configure::write('NumberType.WorkNo_Application' ,'28');  //マスタ申請
Configure::write('NumberType.WorkNo_EdiD2Promise','33');  //業者直納品

Configure::write('ConvNumberType.01', '4');  //消費
Configure::write('ConvNumberType.02', '5');  //臨時要求作成
Configure::write('ConvNumberType.03', '6');  //引当要求作成
Configure::write('ConvNumberType.04', '7');  //引当登録
Configure::write('ConvNumberType.05', '8');  //シール発行
Configure::write('ConvNumberType.06', '9');  //出荷
Configure::write('ConvNumberType.07','10');  //受領
Configure::write('ConvNumberType.08','11');  //発注
Configure::write('ConvNumberType.09','12');  //入荷
Configure::write('ConvNumberType.10','13');  //入庫
Configure::write('ConvNumberType.11','14');  //セット組作業
Configure::write('ConvNumberType.12','15');  //返却
Configure::write('ConvNumberType.13','16');  //返品
Configure::write('ConvNumberType.14','17');  //移動
Configure::write('ConvNumberType.15','18');  //棚卸
Configure::write('ConvNumberType.16','19');  //期限切れ
Configure::write('ConvNumberType.18','20');  //直納品登録
Configure::write('ConvNumberType.20','21');  //締め
Configure::write('ConvNumberType.24','22');  //遡及
Configure::write('ConvNumberType.27','23');  //売上依頼表

Configure::write('ConvNumberType.28','24');  //業者受注
Configure::write('ConvNumberType.29','25');  //業者出荷
Configure::write('ConvNumberType.30','26');  //見積依頼
Configure::write('ConvNumberType.31','27');  //見積
Configure::write('ConvNumberType.32','28');  //マスタ申請

/**************************************************************/
// ConstGroupType (定数区分)
Configure::write('ConstGroupType.biogenousTypes',     '0001'); // 生物由来区分
Configure::write('ConstGroupType.classSeparations',   '0002'); // クラス分類マスタ
Configure::write('ConstGroupType.insuranceClaimType', '0003'); // 保険請求区分
Configure::write('ConstGroupType.taxes',              '0004'); // 課税区分
Configure::write('ConstGroupType.keep',               '0005'); // 保管区分
Configure::write('ConstGroupType.costSticker',        '0006'); // 医事シール区分
Configure::write('ConstGroupType.lotubd',             '0007'); // LOT/UBD区分
Configure::write('ConstGroupType.printerSetting',     '0008'); // 個別プリンタ設定

/**************************************************************/
//　types (金額丸め区分)
//仕入先、得意先に対して設定する。金額の小数点の扱いを定義。
Configure::write('Round', array('1'=>'四捨五入','2'=>'切り上げ','3'=>'切り捨て','4'=>'何もしない'));
Configure::write('RoundType.Round' ,'1');
Configure::write('RoundType.Ceil'  ,'2');
Configure::write('RoundType.Floor' ,'3');
Configure::write('RoundType.Normal','4'); //なにもしない
/**************************************************************/
//　gross types (明細まとめ区分)
//仕入先、得意先に対して設定する
//金額丸めをどの単位で適用するかを定義（シール固定）
Configure::write('Gross', array('1'=>'シール単位','2'=>'ロット単位','3'=>'商品単位'));
Configure::write('GrossType.Seal','1');//view表示用
Configure::write('GrossType.Item','2');
Configure::write('GrossType.Slip','3');
/**************************************************************/
// tax types (課税区分)
Configure::write('FacilityItems.taxes', array('1'=>'課税','0'=>'非課税'));
// tax types (課税区分：印刷用)
Configure::write('PrintFacilityItems.taxes', array('1'=>'課','0'=>'非'));
/**************************************************************/
// biogenous types　(生物由来区分)
Configure::write('FacilityItems.biogenous_types', array('0'=>'通常', '1'=>'生物', '2'=>'特生'));
/**************************************************************/
// item types
Configure::write('FacilityItems.item_types', array('1'=>'医療品材料'));
/**************************************************************/
// 施設採用品区分
Configure::write('ItemType.masterItem'    ,'0'); //マスタ連携品
Configure::write('ItemType.originalItem'  ,'0'); //独自採用品
Configure::write('ItemType.lowlevelItem'  ,'0'); //低レベル品
Configure::write('ItemType.setItem'       ,'0'); //セット品
/**************************************************************/
// 施設採用品低レベル管理
Configure::write('FacilityItems.low_level', array('0'=>'通常品','1'=>'低レベル品'));
/**************************************************************/
// 施設採用品MS法人対応
Configure::write('FacilityItems.ms_corporate', array('0'=>'利用しない','1'=>'利用する'));
/**************************************************************/
// items　types 採用品区分
//施設採用品に設定する
//商品情報の種別を設定
Configure::write('Items.item_types', array('1'=>'採用品','2'=>'セット品','3'=>'医事コード設定'));
Configure::write('Items.item_types.normalitem' , 1);
Configure::write('Items.item_types.setitem'    , 2);
Configure::write('Items.item_types.medicalcode', 3);

/**************************************************************/
// SpareKeyClass (予備キークラス)
Configure::write('SpareKeyClasses', array('1'=>'必須','2'=>'数値','3'=>'必須＋数値'));
Configure::write('SpareKeyClass.require'           , 1); // 必須
Configure::write('SpareKeyClass.numeric'           , 2); // 数値
Configure::write('SpareKeyClass.requre_and_numeric', 3); // 必須＋数値
Configure::write('SpareKeyClass.output',
    array(
        '1'=>' sparekey r',         // 必須
        '2'=>' sparekey numeric',   // 数値
        '3'=>' sparekey r numeric'  // 必須＋数値
    )
);


/**************************************************************/
// user_types (施設区分)
Configure::write('UserType.hospital' , '1'); //// 病院
Configure::write('UserType.edi'      , '2'); //// 業者

/**************************************************************/
// facility_types (施設区分)
Configure::write('FacilityType.center'     , '1'); //// センター
Configure::write('FacilityType.hospital'   , '2'); //// 病院
Configure::write('FacilityType.supplier'   , '3'); //// 仕入先
Configure::write('FacilityType.donor'      , '4'); //// 施主
Configure::write('FacilityType.outside'    , '5'); //// 管理外

Configure::write('FacilityType.typelist'  , array('1'=>'センター','2'=>'病院','3'=>'仕入先' ,4=>'施主' ,5=>'管理外')); //施設区分

/**************************************************************/
// 分析出力(区切り文字）
Configure::write('AnalyzeDelimiterType.tab'    , '0'); //タブ
Configure::write('AnalyzeDelimiterType.comma'  , '1'); //カンマ
Configure::write('AnalyzeDelimiterType.typelist'  , array('0'=>'タブ区切り','1'=>'カンマ区切り')); //区切り文字

// 分析出力(囲み文字）
Configure::write('AnalyzeQuoteType.none'         , '0'); //なし
Configure::write('AnalyzeQuoteType.doubleQuote'  , '1'); //ダブルクォート
Configure::write('AnalyzeQuoteType.singleQuote'  , '2'); //シングルクォート
Configure::write('AnalyzeQuoteType.typelist'  , array('0'=>'なし','1'=>'ダブルクォート','2'=>'シングルクォート')); //囲み文字

/**************************************************************/
// ユーザパスワードの更新月
Configure::write('UserPassword.updateMonth'  , array(3 , 9));
/**************************************************************/
// ユーザパスワードの有効期限切れ警告日数
Configure::write('UserPassword.AlertDay'  , 15);
/**************************************************************/
/*
 * dealer types (販売元区分)
 */
Configure::write('DealerTypes.donor'      , '1'); //// 連携データ
Configure::write('DealerTypes.cooperation', '2'); //// 独自管理

Configure::write('DealerType',array('1'=>'連携データ','2'=>'独自管理'));
/**************************************************************/
// department types (部署区分)
Configure::write('DepartmentType.warehouse' , '1'); // センター倉庫
Configure::write('DepartmentType.hospital'  , '2'); // 病院部署
Configure::write('DepartmentType.supplier'  , '3'); // 仕入先倉庫
Configure::write('DepartmentType.opestock'  , '4'); // オペ在庫用
/**************************************************************/
/**
 * closing types (締め区分)
 */
Configure::write('CloseType.supply', '1'); //仕入締め
Configure::write('CloseType.sales' , '2'); //売上締め
Configure::write('CloseType.stock' , '3'); //在庫締め

/**
 * close status (締め状態)
 */
Configure::write('CloseStatus.nonclose' , '0'); //未締め
Configure::write('CloseStatus.preclose' , '1'); //仮締め
Configure::write('CloseStatus.closed'   , '2'); //本締め

/**************************************************************/
/***
 * closing privileges types(締め後のテーブル操作権限種別)
 */
Configure::write('ClosePrivileges.create', '1');  //作成権限
Configure::write('ClosePrivileges.update', '2');  //更新権限(論理削除含む）

/**************************************************************/
// 仕入締め区分、売上締め区分、在庫締め区分
// 締めのレベルを設定
Configure::write('StockingCloseType.none', '0'); //仕入締め区分
Configure::write('SalesCloseType.none'   , '0'); //売上締め区分
Configure::write('FacilityCloseType.none', '0'); //在庫締め区分

/**************************************************************/
// order status (発注状態)
Configure::write('OrderStatus.notYetOrder', '1'); // 初期
Configure::write('OrderStatus.ordered'    , '2'); // 発注済
Configure::write('OrderStatus.partLoaded' , '3'); // 一部入庫済
Configure::write('OrderStatus.loaded'     , '4'); // 入庫済
/**************************************************************/
// order types (発注区分)
Configure::write('OrderTypes.nomal'         , '1');
Configure::write('OrderTypes.semi_nomal'    , '5');
Configure::write('OrderTypes.temporary'     , '2');
Configure::write('OrderTypes.temporary_calc', '6');
// 数量指定で自動計算の場合の発注区分
Configure::write('OrderTypes.temporary_autocalc', '7');
// 数量計算で自動計算対象外の場合の発注区分
Configure::write('OrderTypes.temporary_calc_notautocalc', '8');

Configure::write('OrderTypes.replenish'     , '3');
Configure::write('OrderTypes.direct'        , '4');

Configure::write('OrderTypes.nostock'       , '9');
Configure::write('OrderTypes.ExNostock'     , '10');
Configure::write('OrderTypes.opestock'      , '11');

Configure::write('OrderTypes.orderTypeName', array(
    '1' => '通常発注',
    '2' => '臨時発注(数量指定:対象外)',
    '7' => '臨時発注(数量指定:対象)',
    '8' => '臨時発注(数量計算:対象外)',
    '6' => '臨時発注(数量計算:対象)',
    '3' => '補充依頼',
    '9' => '非在庫品発注',
    '10'=> '非在庫品発注(臨時)',
    '11'=> 'オペ倉庫品発注',
    '4' => '直納品発注'
    ));

Configure::write('OrderTypes.orderTypeShortName', array(
    '1'  => '通常発注',
    '2'  => '臨時発注',
    '7'  => '臨時発注',
    '8'  => '臨時発注',
    '6'  => '臨時発注',
    '3'  => '補充依頼',
    '9'  => '非在庫品',
    '10' => '非在庫品',
    '11' => 'オペ発注',
    '4'  => '直納品'
    ));

/**************************************************************/
/*
 * shipping status 出荷区分
 */
Configure::write('ShippingTypes', array('1'=>'定数品','2'=>'準定数品','3'=>'預託品','4'=>'臨時品','5'=>'直納品','6'=>'臨時出荷','8'=>'非在庫品','9'=>'臨時消費','10'=>'業者直納品'));
Configure::write('ShippingType.fixed'        ,'1');  //定数品
Configure::write('ShippingType.semifixed'    ,'2');  //準定数品
Configure::write('ShippingType.deposit'      ,'3');  //預託品
Configure::write('ShippingType.extraordinary','4');  //臨時品
Configure::write('ShippingType.direct'       ,'5');  //直納品
Configure::write('ShippingType.exshipping'   ,'6');  //臨時出荷
Configure::write('ShippingType.move'         ,'7');  //移動
Configure::write('ShippingType.nostock'      ,'8');  //非在庫品
Configure::write('ShippingType.exconsume'    ,'9');  //臨時消費
Configure::write('ShippingType.edidirect'    ,'10'); //業者直納品


/*
 * shipping status 出荷状態
 */
Configure::write('ShippingStatus.ordered'   , '1');  // 初期
Configure::write('ShippingStatus.partLoaded', '2');  // 一部受領
Configure::write('ShippingStatus.loaded'    , '3');  // 受領済
/**************************************************************/
/*
 * receiving status 入荷状態
 */
Configure::write('ReceivingStatus.arrival'    , '1');  // 入荷済
Configure::write('ReceivingStatus.partStocked', '2');  // 一部入庫済
Configure::write('ReceivingStatus.stock'      , '3');  // 入庫済（受領時の入荷済も下記を使用予定）
/**************************************************************/
/*
 * edi receiving status 受注状態
 */
Configure::write('EdiReceivingStatus.loaded'      , '1');  // 受注済み
Configure::write('EdiReceivingStatus.partShipping', '2');  // 一部出荷
Configure::write('EdiReceivingStatus.shipping'    , '3');  // 出荷済
Configure::write('EdiReceivingStatus.statusName', array('1'=>'受注済み','2'=>'一部出荷','3'=>'出荷済'));
/**************************************************************/
/*
 * receiving Types 入荷区分
 */
Configure::write('ReceivingType.arrival'         , '1');  // 入荷
Configure::write('ReceivingType.return'          , '2');  // 返品
Configure::write('ReceivingType.repay'           , '3');  // 返却
Configure::write('ReceivingType.returnReceiving' , '4');  // 入荷返品
Configure::write('ReceivingType.returnStrage'    , '5');  // 入庫返品
Configure::write('ReceivingType.returnDeposit'   , '6');  // 預託品返品
Configure::write('ReceivingType.recombination'   , '7');  // 組換
Configure::write('ReceivingType.receipt'         , '8');  // 受領による入荷
Configure::write('ReceivingType.move'            , '9');  // 移動
Configure::write('ReceivingType.move2ope'       , '10');  // オペ倉庫移動
Configure::write('ReceivingType.typeName', array('1'=>'入荷','2'=>'返品','3'=>'返却','4'=>'入荷返品','5'=>'入庫返品','6'=>'預託品返品','7'=>'組換','8'=>'受領による入荷','9'=>'移動','10'=>'オペ倉庫移動'));

/**************************************************************/
/*
 * storage status 入庫状態
 */
Configure::write('StorageStatus.arrival', '1');  // 入庫済（受領時の入庫済も下記を使用予定）
Configure::write('StorageStatus.stock'  , '2');  // 入庫済の次の段階（部署シール発行前など？）
/**************************************************************/
/**
 * 入庫区分
 */
Configure::write('StorageType.normal', '1'); // 通常入庫
Configure::write('StorageType.direct', '2'); // 直納品
/**************************************************************/

/*
 * inventory types (棚卸区分)
 */
Configure::write('InventoryTypes',array(1=>'棚別' , 2=>'商品別'));
Configure::write('InventoryType.rack', '1');    // 棚別
Configure::write('InventoryType.item', '2');    // 商品別
/**************************************************************/
/*
 * consume types (消費区分)
 */
Configure::write('UseType.use'       , '1'); // 消費
Configure::write('UseType.temporary' , '2'); // 臨時
Configure::write('UseType.direct'    , '3'); // 直納
Configure::write('UseType.return'    , '4'); // 返却(戻し)
Configure::write('UseType.wastage'   , '5'); // 減耗
Configure::write('UseType.adjustment', '6'); // 調整
Configure::write('UseType.moveuse'   , '7'); // 移動消費
Configure::write('UseType.home'      , '8'); // 在宅
Configure::write('UseType.ope'       , '9'); // オペセット使用

// 消費区分
Configure::write('UseTypes',array('1'=>'消費', '2'=>'臨時', '3'=>'直納', '4'=>'返却(戻し)', '5'=>'減耗', '6'=>'調整', '7'=>'移動消費', '8'=>'在宅','9'=>'オペセット'));
/**************************************************************/
/*
 * ope status (手術状態)
 */

Configure::write('OpeStatus.init'     , '1'); // 初期
Configure::write('OpeStatus.shipping' , '2'); // 出荷
Configure::write('OpeStatus.return'   , '6'); // 返却中
Configure::write('OpeStatus.complete' , '3'); // 完了
Configure::write('OpeStatus.delete'   , '4'); // 削除
Configure::write('OpeStatus.abort'    , '5'); // 中止

Configure::write('OpeStatus.list' , array('1'=>'初期' , '2'=>'出荷', '6'=>'返却中','3'=>'完了', '4'=>'削除', '5'=>'中止'));
/**************************************************************/
// 請求(売上)区分
// 請求に設定。金額計上の種類を定義。
Configure::write('SalesType.use'    , '1');  // 消費売上
Configure::write('SalesType.receipt', '2');  // 受領売上
/**************************************************************/
//取引設定の取引区分に設定
//売上計上の種類を定義
Configure::write('Classes', array('1'=>'定数品','2'=>'準定数品','3'=>'預託品','4'=>'臨時品', '5'=>'直納品', '6'=>'臨時出荷','11'=>'臨時消費','7'=>'低レベル品' , '8'=>'セット品','9'=>'非在庫品','10'=>'臨時非在庫'));
Configure::write('ClassesType.Constant'    , '1'); //定数品
Configure::write('ClassesType.SemiConstant', '2'); //準定数品
Configure::write('ClassesType.Deposit'     , '3'); //預託品
Configure::write('ClassesType.Temporary'   , '4'); //臨時品
Configure::write('ClassesType.PayDirectly' , '5'); //直納品
Configure::write('ClassesType.ExShipping'  , '6'); //臨時出荷
Configure::write('ClassesType.ExConsume'   , '11');//臨時消費
Configure::write('ClassesType.LowLevel'    , '7'); //低レベル品
Configure::write('ClassesType.SetItem'     , '8'); //セット品
Configure::write('ClassesType.NoStock'     , '9'); //非在庫品
Configure::write('ClassesType.ExNoStock'   , '10');//臨時非在庫品

/**************************************************************/
//受領予定一覧画面用管理区分定数
Configure::write('ReceiptClasses', array('1'=>'定数品','2'=>'準定数品','3'=>'預託品','4'=>'臨時品'));

/**************************************************************/
//移動区分
Configure::write('MoveTypes',array('1'=>'施設内移動','2'=>'施設外移動','3'=>'センター間移動','4'=>'管理外移出','5'=>'管理外移入','10'=>'オペ倉庫移動'));
Configure::write('MoveType.move2inner'         , '1');  // 施設内移動
Configure::write('MoveType.move2outer'         , '2');  // 施設外移動
Configure::write('MoveType.move2other'         , '3');  // センター間移動
Configure::write('MoveType.move2outside_export', '4');  // 管理外移出
Configure::write('MoveType.move2outside_import', '5');  // 施設外移入
Configure::write('MoveType.repay.complete'     , '6');  // 返却：施設外移動済み
Configure::write('MoveType.recombination'      , '7');  // 組替入庫
Configure::write('MoveType.direct'             , '8');  // 直納
Configure::write('MoveType.inner'              , '9');  // 施設内移動
Configure::write('MoveType.move2ope'           ,'10');  // オペ倉庫移動

//移動状態(新)
Configure::write('MoveStatus.init'           ,  '0'); // 初期状態
Configure::write('MoveStatus.reserved'       ,  '1'); // 移動予約中
Configure::write('MoveStatus.moved_partially', '10'); // 一部移動
Configure::write('MoveStatus.complete'       ,'100'); // 移動済み
Configure::write('MoveStatus.repay_complete' ,'200'); // 返却：移動済み

// moves types 移動区分
Configure::write('MoveType.replenishment', '0');  // 補充
Configure::write('MoveType.temporary'    , '1');  // 臨時

/**************************************************************/
// 引当処理時に使用
// 引当要求時　定数区分
Configure::write('TeisuType.List', array('1'=>'定数品','2'=>'準定数品','3'=>'預託品','5'=>'非在庫品'));
Configure::write('TeisuType.Constant'    ,'1'); //定数品
Configure::write('TeisuType.SemiConstant','2'); //準定数品
Configure::write('TeisuType.Deposit'     ,'3'); //預託品
Configure::write('TeisuType.Temporary'   ,'4'); //臨時品
Configure::write('TeisuType.NonStock'    ,'5'); //非在庫品

// 引当確定時  定数区分
Configure::write('FixedType.List',array('1' => '定数','2'=>'休日定数','3'=>'予備定数'));
Configure::write('FixedType,Constant','1'); //定数
Configure::write('FixedType.Holiday' ,'2'); //休日定数
Configure::write('FixedType.Spare'   ,'3'); //予備定数

//引当区分
Configure::write('PromiseType.list', array('1'=>'定数','2'=>'臨時'));
Configure::write('PromiseType.Constant' ,'1'); //定数
Configure::write('PromiseType.Temporary','2'); //臨時

//引当状態
Configure::write('PromiseStatus.list', array('1'=>'初期','2'=>'一部処理済','3'=>'引当済','4'=>'シール発行済','5'=>'取消済'));
Configure::write('PromiseStatus.Init'         ,'1'); //初期
Configure::write('PromiseStatus.PartMatching' ,'2'); //一部処理済
Configure::write('PromiseStatus.Matching'     ,'3'); //引当済
Configure::write('PromiseStatus.SealIssue'    ,'4'); //シール発行済
Configure::write('PromiseStatus.Delete'       ,'5'); //取消済

/**************************************************************/

/**************************************************************/
//定数切替
Configure::write('SwitchType.list', array('1'=>'定数切替登録','2'=>'消費登録','3'=>'消費取消'));
Configure::write('SwitchType.register', 1);
Configure::write('SwitchType.consume' , 2);
Configure::write('SwitchType.cancel'  , 3);
/**************************************************************/


/**
 * シール移動履歴＠レコード区分
 */
Configure::write('RecordType.list', array('1'=>'発注','2'=>'入荷','3'=>'入庫','4'=>'消費','5'=>'直納品','6'=>'出荷','7'=>'受領','8'=>'移動','9'=>'期限切れ処理','10'=>'返却','11'=>'返品','12'=>'臨時出荷','13'=>'オペ倉庫移動'));
Configure::write('RecordType.order'     ,  1);  //発注
Configure::write('RecordType.receiving' ,  2);  //入荷
Configure::write('RecordType.storage'   ,  3);  //入庫
Configure::write('RecordType.consume'   ,  4);  //消費
Configure::write('RecordType.direct'    ,  5);  //直納品
Configure::write('RecordType.shipping'  ,  6);  //出荷
Configure::write('RecordType.receipts'  ,  7);  //受領
Configure::write('RecordType.move'      ,  8);  //移動
Configure::write('RecordType.ajast'     ,  9);  //期限切れ処理
Configure::write('RecordType.repay'     , 10);  //返却
Configure::write('RecordType.return'    , 11);  //返品
Configure::write('RecordType.ExShipping', 12);  //臨時出荷
Configure::write('RecordType.move2ope'  , 13);  //オペ倉庫移動

/**************************************************************/

Configure::write('Concierge.status', array('0'=>'新規','1'=>'対応中','2'=>'返答済','3'=>'完了'));

/*
 * コード種別
 */
Configure::write('Code.moveType'           , 'MoveType');             // 移動区分
Configure::write('Code.orderStatus'        , 'OrderStatus');          // 発注状態
Configure::write('Code.inventoryType'      , 'InventoryType');        // 棚卸区分
Configure::write('Code.useType'            , 'UseType');              // 消費区分
Configure::write('Code.centerType'         , 'CenterType');           // 施設区分
Configure::write('Code.divisionType'       , 'DivisionType');         // 部署区分
Configure::write('Code.salesType'          , 'SalesType');            // 売上区分
Configure::write('Code.centerAttributeType', 'CenterAttributeType');  // 施設属性区分
/**************************************************************/
/*
 * return Types 返品区分
 */
Configure::write('ReturnType.receiving', '1');  // 入荷返品
Configure::write('ReturnType.storage'  , '2');  // 入庫返品
Configure::write('Return', array('1'=>'定数品','2'=>'準定数品'));
/**************************************************************/
/*
 * return Types 返却区分
 */
Configure::write('RepayType.normal'    , '1'); // 通常
Configure::write('RepayType.replenish' , '2'); // 預託品
Configure::write('Repay', array('1'=>'通常','2'=>'預託品'));
/**************************************************************/
/*
 * return Types プリンター制御
 */
Configure::write('PrinterType.normal'                , '1');  // 通常
Configure::write('PrinterType.center'                , '2');  // センター
Configure::write('PrinterType.hospital.fixed'        , '3');  // 病院(定数)
Configure::write('PrinterType.hospital.semifixed'    , '4');  // 病院(準定数)
Configure::write('PrinterType.hospital.deposit'      , '5');  // 病院(預託)
Configure::write('PrinterType.hospital.extraordinary', '6');  // 病院(臨時)
Configure::write('PrinterType.hospital.nostock'      , '7');  // 病院(非在庫)
Configure::write('PrinterType.hospital.medicine'     , '8');  // 病院(薬品)
Configure::write('PrinterType.cost'                  , '9');  // コスト
Configure::write('PrinterType.spare1'                , '10'); // 個別設定1
Configure::write('PrinterType.spare2'                , '11'); // 個別設定2
Configure::write('PrinterType.spare3'                , '12'); // 個別設定3
/**************************************************************/
// expiration types (プリンタ選択)
Configure::write('PrinterSetting', array('0'=>'通常','1'=>'個別設定1','2'=>'個別設定2','3'=>'個別設定3'));

/**************************************************************/
// expiration types (期限切れ区分)
Configure::write('ExpirationType.captiveConsumptionReduction', '1');  //自家消費(在庫減)
Configure::write('ExpirationType.miscellaneousLossReduction',  '2');  //雑損(在庫減)
Configure::write('ExpirationType.captiveConsumptionIncrease',  '3');  //自家消費(在庫増)
Configure::write('ExpirationType.miscellaneousLossIncrease',   '4');  //雑損(在庫増)
Configure::write('ExpirationType1','自家消費(在庫減)');
Configure::write('ExpirationType2','雑損(在庫減)');
Configure::write('ExpirationType3','自家消費(在庫増)');
Configure::write('ExpirationType4','雑損(在庫増)');
Configure::write('Expiration', array('1'=>'自家消費(在庫減)','2'=>'雑損(在庫減)','3'=>'自家消費(在庫増)','4'=>'雑損(在庫増)'));
Configure::write('Managements',array('1'=>'定数品','2'=>'準定数品','3'=>'預託品'));
/**************************************************************/
// expiration types (返品状態)
Configure::write('ReturnStatus.send','1');
/**************************************************************/
// retroact types (遡及区分)
Configure::write('Retroact.change_price'           ,'1');
Configure::write('Retroact.stock_retroact'         ,'2');
Configure::write('Retroact.ms_stock_retroact'      ,'3');
Configure::write('Retroact.sales_retroact'         ,'4');
Configure::write('Retroact.stock_retroact_minus'   ,'5');
Configure::write('Retroact.stock_retroact_plus'    ,'6');
Configure::write('Retroact.ms_stock_retroact_minus','7');
Configure::write('Retroact.ms_stock_retroact_plus' ,'8');
Configure::write('Retroact.sales_retroact_minus'   ,'9');
Configure::write('Retroact.sales_retroact_plus'    ,'10');
Configure::write('Retroacts',array(
    '1' => '価格変更',
    '2' => '仕入遡及',
    '3' => 'MS法人仕入遡及',
    '4' => '売上遡及',
    ));
Configure::write('RetroactTypeDisplay',array(
    '1'  =>'価格変更',
    '2'  =>'仕入遡及',
    '3'  =>'MS法人遡及',
    '4'  =>'売上遡及',
    '5'  =>'仕入遡及赤',
    '6'  =>'仕入遡及黒',
    '7'  =>'MS法人仕入遡及赤',
    '8'  =>'MS法人仕入遡及黒',
    '9'  =>'売上遡及赤',
    '10' =>'売上遡及黒'
    ));

/*******************************************************************/
// claim type(請求区分)
Configure::write('ClaimType.stock'                   ,'1');  //仕入れ
Configure::write('ClaimType.sale'                    ,'2');  //売り上げ
Configure::write('ClaimType.repay'                   ,'3');  //返却
Configure::write('ClaimType.return'                  ,'4');  //返品
Configure::write('ClaimType.stock_retroact_minus'    ,'5');  //仕入遡及赤
Configure::write('ClaimType.stock_retroact_plus'     ,'6');  //仕入遡及黒
Configure::write('ClaimType.ms_stock_retroact_minus' ,'7');  //MS仕入遡及赤
Configure::write('ClaimType.ms_stock_retroact_plus'  ,'8');  //MS仕入遡及黒
Configure::write('ClaimType.sale_retroact_minus'     ,'9');  //売上遡及赤
Configure::write('ClaimType.sale_retroact_plus'      ,'10'); //売上遡及黒
Configure::write('ClaimTypes',array(
    '1'  => '仕入',
    '2'  => '売上',
    '3'  => '返却',
    '4'  => '返品',
    '5'  => '仕入遡及赤',
    '6'  => '仕入遡及黒',
    '7'  => 'MS仕入遡及赤',
    '8'  => 'MS仕入遡及黒',
    '9'  => '売上遡及赤',
    '10' => '売上遡及黒',
    ));

/*******************************************************************/
// サブコードの区切り文字
Configure::write('subcodeSplitter','-');//ハイフン
/*******************************************************************/
//仕入請求・売上請求判定区分
Configure::write('Claim.stock'   ,'1');  //仕入
Configure::write('Claim.sales'   ,'2');  //売上
Configure::write('Claim.ms_stock','3');  //MS仕入
/*******************************************************************/
//EDIステータス
Configure::write('EdiStatusList', array('0'=>'未処理', '1'=>'ピッキング中', '2'=>'一部発送済み', '3'=>'発送済み' , '4'=>'メーカー取り寄せ' ));
/**************************************************************/
//マスタ申請ステータス
Configure::write('ApplicationStatusList', array('1'=>'申請中','2'=>'申請受付','3'=>'メーカー問合中','4'=>'登録完了','5'=>'却下'));
Configure::write('ApplicationStatus.Pending'      , 1);
Configure::write('ApplicationStatus.Receptionist' , 2);
Configure::write('ApplicationStatus.Inquiry'      , 3);
Configure::write('ApplicationStatus.Complete'     , 4);
Configure::write('ApplicationStatus.Rejection'    , 5);
/**************************************************************/


/**
 * 各種データで使用するwork_no生成用
 */
Configure::write('workNoModels',
  array(
    '01'=>'TrnConsumeHeader',      // 消費
    '02'=>'TrnPromiseHeader',      // 臨時要求作成
    '03'=>'TrnPromiseHeader',      // 引当要求作成
    '04'=>'TrnFixedPromiseHeader', // 引当登録
    '05'=>'TrnStickerIssue',       // シール発行
    '06'=>'TrnShippingHeader',     // 出荷
    '07'=>'TrnReceipts',           // 受領
    '08'=>'TrnOrderHeader',        // 発注
    '09'=>'TrnReceivingHeader',    // 入荷
    '10'=>'TrnStorageHeader',      // 入庫
    '11'=>'TrnSetStockHeader',     // セット組作業
    '12'=>'TrnRepayHeader',        // 返却
    '13'=>'TrnReturnHeader',       // 返品
    '14'=>'TrnMoveHeader',         // 移動
    '15'=>'TrnInventoryHeader',    // 棚卸
    '16'=>'TrnAdjustHeaders',      // 期限切れ処理
    '17'=>'',                      // 本社データ連携
    '18'=>'TrnOrderHeader',        // 直納品登録
    '19'=>'',                      // シール管理
    '20'=>'TrnCloseHeader',        // 締め
    '21'=>'',                      // 在庫照会
    '22'=>'',                      // 横断検索
    '23'=>'',                      // 分析データ出力
    '24'=>'TrnRetroactHeader',     // 遡及
    '25'=>'',                      // ログイン
    '26'=>'',                      // マスタ管理系
    '27'=>'TrnSalesRequestHeader', // 売上依頼作成
  )
);

/*******************************************************************/
// Pdf帳票処理
Configure::write('PdfReportType.order'    ,'1');  //発注書
Configure::write('PdfReportType.sticker'    ,'2');  //シール


/**************************************************************/
/**
 * 各種帳票で使用するlayoutname
 */
Configure::write('layoutnameModels',
  array(
    '01'=>'01_センターシール.rpx',             // センターシール
    '02'=>'02_部署シール.rpx',                 // 部署シール
    '03'=>'03_コストシール.rpx',               // コストシール
    '04'=>'04_消費履歴.rpx',                   // 消費履歴
    '05'=>'05_要求作成履歴.rpx',               // 要求作成履歴
    '06'=>'06_欠品リスト.rpx',                 // 欠品リスト
    '07'=>'07_未引当一覧.rpx',                 // 未引当一覧
    '08'=>'08_出荷伝票.rpx',                   // 出荷伝票
    '09'=>'09_納品書.rpx',                     // 納品書
    '10'=>'10_発注書.rpx',                     // 発注書
    '11'=>'11_補充依頼票.rpx',                 // 補充依頼表
    '12'=>'12_売上依頼票.rpx',                 // 売上依頼票
    '13'=>'13_返品明細.rpx',                   // 返品明細
    '14'=>'14_棚卸表.rpx',                     // 棚卸表
    '15'=>'15_棚卸差異リスト.rpx',             // 棚卸差異リスト
    '16'=>'16_在庫表.rpx',                     // 在庫票
    '17'=>'17_期限切れ一覧.rpx',               // 期限切れ一覧
    '18'=>'18_不動在庫一覧.rpx',               // 不動在庫一覧
    '19'=>'19_金額差異一覧.rpx',               // 金額差異一覧
    '20'=>'20_仕入設定一覧.rpx',               // 仕入設定一覧
    '21'=>'21_売上設定一覧.rpx',               // 売上設定一覧
    '22'=>'22_部署別定数一覧.rpx',             // 部署別定数一覧
    '23'=>'23_請求集計票.rpx',                 // 請求集計票
    '24'=>'24_請求明細.rpx',                   // 請求明細
    '25'=>'25_仕入明細.rpx',                   // 仕入明細
    '30'=>'30_セット品明細表.rpx',             // セット品明細表
    '31'=>'31_棚卸バーコード.rpx',             // 棚番バーコード表
    '32'=>'32_EDI出荷伝票.rpx',                // 業者 出荷伝票
    '33'=>'33_請求書伝票.rpx',                 // 請求書伝票
    '34'=>'34_使用材料保険点数購入価比較リスト.rpx',       // 請求書伝票使用材料保険点数購入価比較リスト
    '35'=>'35_手術セットリスト.rpx',           // 手術セットリスト
    '36'=>'36_手術セットピッキングリスト.rpx', // 手術セットピッキングリスト
    '37'=>'37_払出先別消費実績集計表.rpx',     // 払出先別消費実績集計表
    '38'=>'38_倉庫在庫金額集計表.rpx',         // 倉庫在庫金額集計表
    '39'=>'39_倉庫在庫金額一覧表.rpx',         // 倉庫在庫金額一覧表
    '40'=>'40_EDI直納出荷伝票.rpx',            // 業者 直納出荷伝票
    '41'=>'41_材料別消費実績推移表.rpx',       // 材料別消費実績推移表
    '42'=>'42_払出先別消費実績推移表.rpx',     // 払出先別消費実績推移表
  )
);

/*権限用定数*/
Configure::write('Role',array(
    '1' => '消費登録',
    '2' => '消費履歴',
    '3' => '臨時引当要求登録',
    '4' => '臨時引当要求履歴',
    '5' => '引当要求作成',
    '6' => '要求作成履歴',
    '7' => '引当確定',
    '8' => '引当確定履歴',
    '9' => '部署シール発行',
    '10' => 'シール発行履歴',
    '11' => '出荷照合',
    '12' => '臨時出荷',
    '13' => '出荷履歴',
    '14' => '受領登録',
    '15' => '受領履歴',
    '16' => '発注・補充要求作成',
    '17' => '任意発注',
    '18' => '発注・補充確定',
    '19' => '発注履歴',
    '20' => '売上依頼票印刷',
    '21' => '売上依頼票印刷履歴',
    '22' => '入荷登録',
    '23' => '入荷履歴',
    '24' => '入庫登録',
    '25' => '入庫履歴',
    '26' => '組み換え登録',
    '27' => '組み替え履歴',
    '28' => 'セット作成',
    '29' => 'セット補充指示',
    '30' => 'セット作成履歴',
    '31' => '返却登録',
    '32' => '返却履歴',
    '33' => '入荷済み返品登録',
    '34' => '入庫済み返品登録',
    '35' => '預託品返品登録',
    '36' => '返品確認',
    '37' => '返品履歴',
    '38' => '施設内移動',
    '39' => '施設外移動',
    '40' => 'センター間移動',
    '41' => '移動受領登録',
    '42' => '管理外移出',
    '43' => '管理外移入',
    '44' => '移動履歴',
    '45' => '棚別予定登録',
    '46' => '商品別予定登録',
    '47' => '棚卸実施登録',
    '48' => '棚卸差異一覧',
    '49' => '自家消費・雑損',
    '50' => '在庫調整履歴',
    '51' => '連携データ作成',
    '52' => '直納品受領登録',
    '53' => '直納品履歴',
    '54' => 'シール検索',
    '55' => 'シール移動履歴',
    '56' => '仕入締め',
    '57' => '売上締め',
    '58' => '在庫締め',
    '59' => '締め履歴',
    '60' => '在庫票印刷',
    '61' => '仕入検索',
    '62' => '売上検索',
    '63' => '在庫検索',
    '64' => '期限切れ警告一覧',
    '65' => '不動在庫一覧',
    '66' => '金額差異一覧',
    '67' => '横断検索',
    '68' => '回転率一覧',
    '69' => '仕入遡及',
    '70' => '売上遡及',
    '71' => '遡及履歴',
    '72' => '仕入価格変更',
    '73' => '売上価格変更',
    '74' => '施設採用登録',
    '75' => '施設採用メンテ',
    '76' => '採用品ＣＳＶ登録',
    '77' => 'ＧＭメンテ',
    '78' => '医事コードメンテ',
    '79' => 'セットマスタ',
    '80' => '販売元マスタ',
    '81' => 'センター',
    '82' => '施主・病院・仕入先',
    '83' => '部署',
    '84' => '仕入設定',
    '85' => '売上設定',
    '86' => '商品別病院定数',
    '87' => '部署別病院定数',
    '88' => '倉庫定数',
    '89' => '定数切替設定',
    '90' => 'ロール',
    '91' => 'ユーザー',
    '92' => '単位名',
    '93' => '棚番号',
    '94' => '作業区分',
    '95' => 'お知らせ登録',
    '96' => 'ＧＭ登録申請',
    '97' => 'ＧＭ登録状況照会',
    '98' => '分析データ出力',
    '99' => '分析データ出力メンテ',
));


/**************************************************************/
/**
 * 件数表示用に使用
 */

/**************************************************************/
Configure::write('displaycounts_combobox',
  array(
    '100' => '100件表示',
    '500' => '500件表示',
    '1000' => '1000件表示'
    )
);

/**************************************************************/
// 分割対象テーブル列挙
/**************************************************************/
Configure::write('split_tables', array(
    'mst_facility_items',
    'mst_fixed_counts',
    'mst_item_units',
    'mst_sales_configs',
    'mst_transaction_configs',
    'trn_adjust_headers',
    'trn_appraised_values',
    'trn_claims',
    'trn_close_headers',
    'trn_close_records',
    'trn_consume_headers',
    'trn_consumes',
    'trn_fixed_promise_headers',
    'trn_fixed_promises',
    'trn_inventories',
    'trn_inventory_headers',
    'trn_move_headers',
    'trn_order_headers',
    'trn_orders',
    'trn_promise_headers',
    'trn_promises',
    'trn_receiving_headers',
    'trn_receivings',
    'trn_repay_headers',
    'trn_retroact_headers',
    'trn_retroact_records',
    'trn_return_headers',
    'trn_sales_request_headers',
    'trn_set_stock_headers',
    'trn_set_stocks',
    'trn_shipping_headers',
    'trn_shippings',
    'trn_sticker_issue_headers',
    'trn_sticker_issues',
    'trn_sticker_records',
    'trn_stickers',
    'trn_stocks',
    'trn_storage_headers',
    'trn_storages',
    'trn_switch_histories',
    'trn_switch_history_headers'
    ));

/**************************************************************/

class RoleManager{
    static public function getControlCode($role, $roleFunction){
        if(false === isset($role) || false === isset($roleFunction)){
            return '';
        }
        $target = self::getRoleMapping($role);
        if(is_array($roleFunction)){
            $allows = $target[$role][$roleFunction[0]];
        }else{
            $allows = $target[$role][$roleFunction];
        }

        $script = array();
        $script[] = '<script type="text/javascript">';
        $script[] = '$(document).ready(function(){';

        if(false === $allows['creatable']){
            //更新権限がないひと。
            $script[] = "$('input[type=button][class*=\"[p2]\"],input[type=submit][class*=\"[p2]\"]').css('display','none');";
        }

        $script[] = '});';
        $script[] = '</script>';

        return join("\n",$script);
    }

    static public function getRoleMapping($roleId){
        $M = ClassRegistry::init('MstUser');
        $sql  = ' select a.view_no, a.readable,a.updatable,a.creatable';
        $sql .= ' from mst_privileges a';
        $sql .=     ' inner join mst_roles b on a.mst_role_id = b.id';
        $sql .= ' where b.id = '.$roleId;
        $sql .= ' order by a.view_no ASC';

        $_SearchResult = $M->query($sql);
        $result = array();
        foreach($_SearchResult as $key => $val){
            $view_no = $val[0]['view_no'];
            $result[$roleId][$view_no] = $val[0];
        }
        return $result;
    }
}

function h_out($str , $align=null){
    $html = "";
    if(is_null($align)){
        $html = '<label title="' . $str. '">' . $str . '</label>';
    }else{
        $html = '<label title="' . $str. '"><p class="' . $align . '">' . $str . '</p></label>';
    }
    return $html;
}

// EOF
?>
