<?php
/**
 * MstAccountlistsController
 * @version 1.0.0
 * @since 2010/07/22
 */

class MstAccountlistsController extends AppController {
    var $name = 'MstAccountlists';

    /**
     *
     * @var array $uses
     */
    var $uses = array('MstAccountlist');

    /**
     * @var array $components
     */
    var $components = array('CsvWriteUtils');
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;

    /**
     * @var MstClasses
     */
    //var $MstAccountlist;

    function beforeFilter() {
        parent::beforeFilter();
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }
    
    
    function index(){
        $this->redirect('accounte_list');
    }

    /**
     * 集計科目一覧
     */
    function accounte_list() {
        $this->setRoleFunction(96);
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        $Accounte_List = array();
        App::import('Sanitize');

        //初回時でない場合のみデータを検索する
        if(isset($this->request->data['MstAccounte']['is_search'])){
            $where  ='';
            //ユーザ入力値による検索条件の作成--------------------------------------------
            //コード(LIKE検索)
            if($this->request->data['MstAccounte']['search_code'] != ""){
                $where .= " and a.code LIKE '%".Sanitize::escape($this->request->data['MstAccounte']['search_code'])."%'";
            }
            //名称(LIKE検索)
            if($this->request->data['MstAccounte']['search_name'] != ""){
                $where .= " and a.name LIKE '%".Sanitize::escape($this->request->data['MstAccounte']['search_name'])."%'";
            }
            //検索条件の作成終了---------------------------------------------------------

            $order = ' order by ' ;
            if($this->getSortOrder() === null){
                $order .= 'a.id';
            }else{
                $tmp = $this->getSortOrder();
                $order .= $tmp[0];
            }

            $sql  = 'select ';
            $sql .= '      a.id              as "MstAccounte__id"';
            $sql .= '    , a.code            as "MstAccounte__code"';
            $sql .= '    , a.name            as "MstAccounte__name"';
            $sql .= '  from ';
            $sql .= '    mst_accountlists as a  ';
            $sql .= '  where 1=1 ';
            $sql .= $where;
            $sql .= '  and  a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .=$order;
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstAccountlist'));
            $sql .= ' limit ' . $this->_getLimitCount();

            //SQL実行
            $Accounte_List = $this->MstAccountlist->query($sql);
        }
        $this->set('Accounte_List',$Accounte_List);
    }


    /**
     * 新規
     */
    function add() {
        $this->setRoleFunction(96);
        //プルダウン作成用データ取得
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }

    /**
     * 編集
     */
    function mod() {
        $this->setRoleFunction(96);
        $result = $this->getData($this->request->data['MstAccounte']['id']);
        $this->request->data = $result[0];
    }

    /**
     * 結果
     */
    function result() {
        $accounte_data = array();
        $now = date('Y/m/d H:i:s.u');

        //トランザクション開始
        $this->MstAccountlist->begin();
        //行ロック（更新時のみ）
        if(isset($this->request->data['MstAccounte']['id'])){
            $this->MstAccountlist->query('select * from mst_accountlists as a where a.id = ' .$this->request->data['MstAccounte']['id']. ' for update ');
        }

        //保存データの整形
        if(isset($this->request->data['MstAccounte']['id'])){
            //更新の場合
            $accounte_data['MstAccountlist']['id']      = $this->request->data['MstAccounte']['id'];
        }else{
            //新規の場合
            $accounte_data['MstAccountlist']['created'] = $now;
        }

        $accounte_data['MstAccountlist']['code']            = $this->request->data['MstAccounte']['code'];
        $accounte_data['MstAccountlist']['name']            = $this->request->data['MstAccounte']['name'];
        $accounte_data['MstAccountlist']['mst_facility_id'] = $this->Session->read('Auth.facility_id_selected');
        $accounte_data['MstAccountlist']['modified']        = $now;

        //SQL実行
        if(!$this->MstAccountlist->save($accounte_data)){
            //ロールバック
            $this->MstAccountlist->rollback();
            //エラーメッセージ
            $this->Session->setFlash('集計科目情報の登録に失敗しました。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('accounte_list');
        }
        $this->MstAccountlist->commit();
    }

    /*
     * データ取得
     */
    private function getData($id){
        $sql  = '  select ';
        $sql .= '      a.id                   as "MstAccounte__id" ';
        $sql .= '    , a.name                 as "MstAccounte__name" ';
        $sql .= '    , a.code                 as "MstAccounte__code" ';
        $sql .= '  from ';
        $sql .= '    mst_accountlists as a  ';
        $sql .= '  where ';
        $sql .= '    a.id = ' . $id;

        return $this->MstAccountlist->query($sql);
    }

    public function export_csv(){
        App::import('Sanitize');
        $where  ='';
        //ユーザ入力値による検索条件の作成--------------------------------------------
        //コード(LIKE検索)
        if($this->request->data['MstAccounte']['search_code'] != ""){
            $where .= " and a.code LIKE '%".Sanitize::escape($this->request->data['MstAccounte']['search_code'])."%'";
        }
        //名称(LIKE検索)
        if($this->request->data['MstAccounte']['search_name'] != ""){
            $where .= " and a.name LIKE '%".Sanitize::escape($this->request->data['MstAccounte']['search_name'])."%'";
        }
        //検索条件の作成終了---------------------------------------------------------

        $order = ' order by ' ;
        if($this->getSortOrder() === null){
            $order .= 'a.id';
        }else{
            $tmp = $this->getSortOrder();
            $order .= $tmp[0];
        }

        $sql  = 'select ';
        $sql .= '      a.code            as "集計科目コード"';
        $sql .= '    , a.name            as "集計科目名称"';
        $sql .= '  from ';
        $sql .= '    mst_accountlists as a  ';
        $sql .= '  where 1=1 ';
        $sql .= $where;
        $sql .= '  and  a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .=$order;
        
        $this->db_export_csv($sql , "集計科目一覧", 'accounte_list');
    }
}
