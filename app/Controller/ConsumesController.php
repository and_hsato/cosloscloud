<?php
/**
 * ConsumesController
 * 消費登録・消費履歴機能
 * @version 1.0.0
 * @since 2010/05/18
 */
class ConsumesController extends AppController {

    /**
     * Controller's name.
     * @var $name
     */
    var $name = 'consumes';

    /**
     * Model Objects using.
     * @var array $uses
     */
    var $uses = array('TrnConsumeHeader',
                      'TrnConsume',
                      'TrnSticker',
                      'MstDepartment',
                      'MstFacility',
                      'MstUser',
                      'MstClass',
                      'MstItemUnit',
                      'MstUserBelonging',
                      'MstDealer',
                      'MstFixedCount',
                      'TrnClaim',
                      'TrnStickerRecord',
                      'TrnShipping',
                      'TrnReceiving',
                      'TrnMoveHeader',
                      'MstSalesConfig',
                      'TrnStock',
                      'MstFacilityRelation',
                      'TrnSwitchHistory',
                      'TrnSwitchHistoryHeader'
                      );

    /**
     * Helpers using.
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'common');

    /**
     * Components using.
     * @var array $components
     */
    var $components = array('RequestHandler','ConsumeAlt','CsvWriteUtils','CsvReadUtils', 'TemporaryItem');

    /**
     *
     * @var array errors
     */
    var $errors;

    var $successes;

    /**
     *
     */
    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        /*
        //CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }
        */
    }

    /**
     *　index
     * @todo 廃止の方向で
     */
    function index (){
        $this->add();
    }

    /**
     *　消費登録画面
     * @return void
     */
    function add() {
        $this->setRoleFunction(1);
        $this->set('department_list' , $this->getHospitalDepartmentList($this->Session->read('Auth.facility_id_selected')));
    }

    /**
     * シール読込
     */
    function read_stickers () {
        $this->request->data['TrnConsumeHeader']['departmentId'] = $this->getHospitalDepartmentId($this->request->data['TrnConsumeHeader']['departmentCode'],$this->Session->read('Auth.facility_id_selected'));
    }

    /**
     * 消費登録確認
     * 消費日、操作施設・部署選択後、消費対象データを視認する。
     * @access public
     * @return void
     */
    function add_confirm () {
        //各配列初期化その１
        $_codes = $_recitails = array();
        //バーコード読込時
        if (isset($this->request->data['codez']) && $this->request->data['codez'] != '') {
            $_codes = explode(',',$this->request->data['codez']); 
        } else {
            $this->Session->setFlash('消費登録するシールがありません','growl',array('type'=>'error'));
            $this->redirect('add');
            return;
        }
        
        //入力されたシール番号をSQL用に文字列変換
        $temp_code = "";
        for($i=0; $i<count($_codes); $i++) {
            $temp_code .= "'".$_codes[$i]."',";
        }
        $temp_code = substr($temp_code, 0, -1);
        
        $where = '';
        // 通常消費の場合
        $where .= '     and (a.hospital_sticker_no in ('. $temp_code .')  ';
        $where .= '     or a.facility_sticker_no in ('. $temp_code .') )';
        $res = $this->getItemData($where);

        $stickers = array();
        //シールの状態を追加する
        for($i=0; $i<count($res); $i++){
            $res[$i]['TrnSticker']['consuming_status'][] = 0;
            //消費済み
            if( $res[$i]['TrnSticker']['is_deleted'] == false && $res[$i]['TrnSticker']['quantity'] == 0){
                $res[$i]['TrnSticker']['consuming_status'][] = 1;
            }

            //有効期限警告期間
            if($this->ConsumeAlt->isExpiredWarningTerm($this->request->data['TrnConsumeHeader']['work_date'], $res[$i]['TrnSticker']['validated_date'], $res[$i]['MstFacilityItem']['expired_date'])){
                $res[$i]['TrnSticker']['consuming_status'][] = 2;
            }

            //有効期限切れ
            if($this->ConsumeAlt->isOverExpiredDate($this->request->data['TrnConsumeHeader']['work_date'],$res[$i]['TrnSticker']['validated_date'])){
                $res[$i]['TrnSticker']['consuming_status'][] = 3;
            }

            //別施設にある商品
//            if ($this->request->data['TrnConsumeHeader']['facilityId'] != $res[$i]['TrnSticker']['facility_id']){
//                $res[$i]['TrnSticker']['consuming_status'][] = 4;
//            }
            //CSV 施設チェック
//            if ($csv_flg === true){
//                //CSV読み込みの施設・部署チェックエラーの場合
//                if(isset($_useless_stickers[$res[$i]['TrnSticker']['hospital_sticker_no']]) ){
//                    $res[$i]['TrnSticker']['consuming_status'][] = 4;
//                }
//            }

            //無効なシール
            if ($res[$i]['TrnSticker']['is_deleted'] === true ){
                $res[$i]['TrnSticker']['consuming_status'][] = 5;
            }
            //予約済み
            if($res[$i]['TrnSticker']['has_reserved'] === true){
                $res[$i]['TrnSticker']['consuming_status'][] = 6;
            }
            //売上設定チェック
            if(is_null($res[$i]['TrnSticker']['sales_price']) ){
                $res[$i]['TrnSticker']['consuming_status'][] = 9;
            }

            //移動消費
            if( $res[$i]['TrnSticker']['mst_department_id'] != $this->request->data['TrnConsumeHeader']['departmentId']){
                $res[$i]['TrnSticker']['consuming_status'][] = 10;
            }
            //切替商品
            if(!is_null($res[$i]['MstFixedCount']['substitute_unit_id'])){
                $res[$i]['TrnSticker']['consuming_status'][] = 11;
            }
            array_push($stickers ,$res[$i]['TrnSticker']['id']); //次のためにシールIDを保持しておく
        }
        //入力されたシール番号順に並べ替え＆不正シール番号の追加＆CSVからの備考を追加
        $stickers = array();
        $item_unit = array();
            for($i=0; $i<count($_codes); $i++){
                for($j=0; $j<count($res); $j++){
                    if($_codes[$i] === $res[$j]['TrnSticker']['facility_sticker_no'] ||
                       $_codes[$i] === $res[$j]['TrnSticker']['hospital_sticker_no']){
                        //シール番号重複
                        if( in_array($res[$j]['TrnSticker']['id'] , $stickers , TRUE)){
                            //保持しているシールID一覧に存在する場合は重複
                            $res[$j]['TrnSticker']['consuming_status'][] = 8;
                        }
                        array_push($stickers , $res[$j]['TrnSticker']['id']);//使ったIDを保持しておいて重複チェックに利用する
                        
                        if(!empty($_recitails[$_codes[$i]])){
                            $res[$j]['TrnSticker']['recitail'] = $_recitails[$_codes[$i]];
                        }else{
                            $res[$j]['TrnSticker']['recitail'] = "";
                        }
                        $item_unit[$i] = $res[$j];
                        break;
                    }else{
                        $temp_array = array();
                        $temp_array['TrnSticker']['hospital_sticker_no'] = $_codes[$i];
                        $temp_array['TrnSticker']['consuming_status'][] = 5;
                        $item_unit[$i] = $temp_array;
                    }
                }
            }
            
            //有効なシールが一枚もないと上記のループに入らないので。。
            if(count($res) === 0){
                foreach($_codes as $c ){
                    $temp_array = array();
                    $temp_array['TrnSticker']['hospital_sticker_no'] =$c;
                    $temp_array['TrnSticker']['consuming_status'][] = 5;
                    $item_unit[] = $temp_array;
                }
            }


        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnConsumes.token' , $token);
        $this->request->data['TrnConsumes']['token'] = $token;

        $this->Session->write('TrnConsumes.targets', $item_unit);

        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 1));
        $this->set('codez', $_codes);
        $this->set('item_units', $item_unit);

    }

    /**
     * 消費登録結果
     */
    function add_result() {
        $this->request->data['now'] = $now = date('Y/m/d H:i:s.u');
        $this->set('classes' , $this->getClassesList( $this->Session->read('Auth.facility_id_selected') , 1 ) );
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if(isset($this->request->data['TrnConsumes']['token']) &&
           $this->request->data['TrnConsumes']['token'] === $this->Session->read('TrnConsumes.token')) {
            $this->Session->delete('TrnConsumes.token');
            $_stickerId = $this->request->data['TrnConsume']['id'];
            //トランザクション開始
            $this->TrnConsume->begin();

            $sql = "select * from trn_stickers as a where a.id in (" . join(",",$_stickerId) . ")  for update  "; //シールIDで行ロック
            $this->TrnConsume->query($sql);

            //確認画面を開いた後更新されていればロールバック
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $_stickerId). ')';
            $sql .= "     and a.modified > '" . $this->request->data['TrnConsumes']['time'] ."'";

            $ret = $this->TrnSticker->query($sql , false);
            if($ret[0][0]['count']> 0 ){
                $this->TrnConsume->rollback();
                $this->Session->setFlash('シール情報が更新されています', 'growl', array('type'=>'error') );
                $this->redirect('add');
            }

            //商品情報を取得
            $where = ' and a.id in ( ' . join(",",$_stickerId) . ')' ;
            $ret2 = $this->getItemData($where);
            $ret = array();
            // 並べ替え
            foreach($this->request->data['TrnConsume']['id'] as $sid){
                foreach($ret2 as $r){
                    if($sid == $r['TrnSticker']['id']){
                        $ret[] = $r;
                        break;
                    }
                }
            }
            // 並べ替え
            
            // 作業番号を取得
            $work_no = $this->setWorkNo4Header($this->request->data['TrnConsumeHeader']['work_date'],'01');

            // 消費ヘッダ作成
            $TrnConsumeHeader = array(
                'TrnConsumeHeader'=> array(
                        'work_no'                => $work_no,
                        'work_date'              => $this->request->data['TrnConsumeHeader']['work_date'],
                        'use_type'               => Configure::read('UseType.use'),
                        'mst_department_id'      => $this->request->data['TrnConsumeHeader']['departmentId'],
                        'detail_count'           => count($ret),
                        'is_deleted'             => false,
                        'creater'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                => $now,
                        'modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'modified'               => $now
                    )
                );
            
            $this->TrnConsumeHeader->create();
            // SQL実行
            if (!$this->TrnConsumeHeader->save($TrnConsumeHeader)) {
                $this->TrnConsume->rollback();
                $this->Session->setFlash('消費xヘxッxダx登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('add');
            }
            $trn_consume_header_id = $this->TrnConsumeHeader->getLastInsertID();
            $work_seq = 1;
            //シール枚数分ループ
            foreach($ret as $r ){
                $useType = Configure::read('UseType.use');
                // 移動消費の場合区分がかわる
                if ($this->request->data['TrnConsumeHeader']['departmentId'] != $r['TrnSticker']['mst_department_id']){
                    $useType = Configure::read('UseType.moveuse');
                }

                // 消費明細作成
                $TrnConsume = array(
                    'TrnConsume'=> array(
                        'work_no'               => $work_no ,
                        'work_seq'              => $work_seq,
                        'work_date'             => $this->request->data['TrnConsumeHeader']['work_date'],
 //                       'work_type'             => $this->request->data['TrnConsume']['work_type'][$r['TrnSticker']['id']],
                        'recital'               => $this->request->data['TrnConsume']['recital'][$r['TrnSticker']['id']],
                        'use_type'              => $useType,
                        'mst_item_unit_id'      => $r['TrnSticker']['mst_item_unit_id'],
                        'mst_department_id'     => $this->request->data['TrnConsumeHeader']['departmentId'],
                        'quantity'              => $r['TrnSticker']['quantity'],
                        'trn_sticker_id'        => $r['TrnSticker']['id'],
                        'is_deleted'            => false,
                        'creater'               => $this->Session->read('Auth.MstUser.id'),
                        'created'               => $now,
                        'modifier'              => $this->Session->read('Auth.MstUser.id'),
                        'modified'              => $now,
                        'trn_consume_header_id' => $trn_consume_header_id ,
                        'facility_sticker_no'   => $r['TrnSticker']['facility_sticker_no'],
                        'hospital_sticker_no'   => $r['TrnSticker']['hospital_sticker_no'],
                        'trade_type'            => $r['TrnSticker']['trade_type']
                        )
                    );
                
                $this->TrnConsume->create();
                // SQL実行
                if (!$this->TrnConsume->save($TrnConsume)) {
                    $this->TrnConsume->rollback();
                    $this->Session->setFlash('消費明細登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }
                $trn_consume_id = $this->TrnConsume->getLastInsertID();
                // 在庫数▽、部署在庫から減算
                $this->TrnStock->create();
                $res = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.stock_count'     => 'TrnStock.stock_count - ' . $r['TrnSticker']['quantity'],
                        'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'        => "'".$now."'",
                        ),
                    array(
                        'TrnStock.id' => $this->getStockRecode($r['TrnSticker']['mst_item_unit_id'],
                                                               $r['TrnSticker']['mst_department_id']
                                                               )
                        ),
                    -1);
                
                if(!$res){
                    $this->TrnConsume->rollback();
                    $this->Session->setFlash('在庫更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }

                // 移動消費の場合、移動分の明細を作る
                if ($this->request->data['TrnConsumeHeader']['departmentId'] != $r['TrnSticker']['mst_department_id']){
                    // 移動消費用の処理
                    $this->TrnStickerRecord->create();
                    $TrnStickerRecord = array(
                        'TrnStickerRecord' => array(
                            'move_date'           => $this->request->data['TrnConsumeHeader']['work_date'],
                            'move_seq'            => $this->getNextRecord($r['TrnSticker']['id']),
                            'record_type'         => Configure::read('RecordType.move'),
                            'record_id'           => $trn_consume_id,
                            'trn_sticker_id'      => $r['TrnSticker']['id'],
                            'mst_department_id'   => $r['TrnSticker']['mst_department_id'],
                            'facility_sticker_no' => $r['TrnSticker']['facility_sticker_no'],
                            'hospital_sticker_no' => $r['TrnSticker']['hospital_sticker_no'],
                            'is_deleted'          => false,
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now,
                            )
                        );
                    if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                        $this->TrnConsume->rollback();
                        $this->Session->setFlash('シール移動履歴（移動）登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }

                    //移動先に在庫データがなければつくる
                    $this->getStockRecode($r['TrnSticker']['mst_item_unit_id'] ,
                                          $this->request->data['TrnConsumeHeader']['departmentId']);

                }
                
                // シール移動履歴作成
                $this->TrnStickerRecord->create();
                $TrnStickerRecord = array(
                    'TrnStickerRecord' => array(
                        'move_date'           => $this->request->data['TrnConsumeHeader']['work_date'],
                        'move_seq'            => $this->getNextRecord($r['TrnSticker']['id']),
                        'record_type'         => Configure::read('RecordType.consume'),
                        'record_id'           => $trn_consume_id,
                        'trn_sticker_id'      => $r['TrnSticker']['id'],
                        'mst_department_id'   => $this->request->data['TrnConsumeHeader']['departmentId'],
                        'facility_sticker_no' => $r['TrnSticker']['facility_sticker_no'],
                        'hospital_sticker_no' => $r['TrnSticker']['hospital_sticker_no'],
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        )
                    );
                if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                    $this->TrnConsume->rollback();
                    $this->Session->setFlash('シール移動履歴（消費）登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }

                // 定数品、預託品、非在庫品、即時品売上が「消費時」かつ即時品
                // もしくは臨時出荷または臨時非在庫品、業者直納品の場合、売上を作成
                $sale_claim_id = $r['TrnSticker']['sale_claim_id'];
                if($r['TrnSticker']['sales_flg'] == false && $r['TrnSticker']['class_name'] != '病院資産'){ // 売り上げ済みフラグがOFFの場合処理をする。
                    if($r['MstFacilityItem']['buy_flg'] == false ) { //薬品の場合売り上げは作成済みなのでスルー
                        if($r['TrnSticker']['trade_type'] == Configure::read('ClassesType.Constant') || 
                           $r['TrnSticker']['trade_type'] == Configure::read('ClassesType.Deposit') || 
                           $r['TrnSticker']['trade_type'] == Configure::read('ClassesType.NoStock') || 
                           ($r['TrnSticker']['trade_type'] == Configure::read('ClassesType.PayDirectly') &&
                            $r['TrnShipping']['shipping_type'] == Configure::read('ShippingType.edidirect')) ||
                           $this->TemporaryItem->isClaim($this->name, $r['TrnSticker']['trade_type'], $this->Session->read('Auth.Config.TemporaryClaimType'))
                           ){
                            $claim_price = $this->getFacilityUnitPrice( $r['Hospital']['round'], $r['TrnSticker']['sales_price'] , $r['TrnSticker']['quantity']);
                            // 売り上げ作成処理
                            $TrnClaim = array(
                                'TrnClaim'=> array(
                                    'department_id_from'    => $r['Center']['mst_department_id'], //センター部署ID
                                    'department_id_to'      => $this->request->data['TrnConsumeHeader']['departmentId'],
                                    'mst_item_unit_id'      => $r['TrnSticker']['mst_item_unit_id'],
                                    'claim_date'            => $this->request->data['TrnConsumeHeader']['work_date'],
                                    'claim_price'           => $claim_price,
                                    'trn_consume_id'        => $trn_consume_id,
                                    'trn_receiving_id'      => $r['TrnSticker']['trn_receiving_id'],
                                    'trn_shipping_id'       => $r['TrnSticker']['trn_shipping_id'],
                                    'is_stock_or_sale'      => '2',
                                    'count'                 => $r['TrnSticker']['quantity'],
                                    'unit_price'            => $r['TrnSticker']['sales_price'],
                                    'round'                 => $r['Hospital']['round'],
                                    'gross'                 => $r['Hospital']['gross'],
                                    'creater'               => $this->Session->read('Auth.MstUser.id'),
                                    'created'               => $now,
                                    'modifier'              => $this->Session->read('Auth.MstUser.id'),
                                    'modified'              => $now,
                                    'facility_sticker_no'   => $r['TrnSticker']['facility_sticker_no'],
                                    'hospital_sticker_no'   => $r['TrnSticker']['hospital_sticker_no'],
                                    'trade_type'            => $r['TrnSticker']['trade_type'],
                                    'claim_type'            => Configure::read('ClaimType.sale')
                                    )
                                );
                            $this->TrnClaim->create();
                            // SQL実行
                            if (!$this->TrnClaim->save($TrnClaim)) {
                                $this->TrnConsume->rollback();
                                $this->Session->setFlash('売上情報登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                $this->redirect('add');
                            }
                            $sale_claim_id = $this->TrnClaim->getLastInsertID();
                        }
                    }
                }
                // 預託品の場合、仕入情報を作成
                $buy_claim_id = $r['TrnSticker']['buy_claim_id'];
                if( $r['TrnSticker']['trade_type'] == Configure::read('ClassesType.Deposit') ) {
                    $claim_price = $this->getFacilityUnitPrice( $r['Supplier']['round'], $r['TrnSticker']['transaction_price'] , $r['TrnSticker']['quantity']);
                    // 仕入作成処理
                    $TrnClaim = array(
                        'TrnClaim'=> array(
                            'department_id_from'    => $r['Supplier']['mst_department_id'], //仕入先部署ID
                            'department_id_to'      => $r['Center']['mst_department_id'],   //センター部署ID
                            'mst_item_unit_id'      => $r['TrnSticker']['mst_item_unit_id'],
                            'claim_date'            => $this->request->data['TrnConsumeHeader']['work_date'],
                            'claim_price'           => $claim_price,
                            'trn_consume_id'        => $trn_consume_id,
                            'trn_receiving_id'      => $r['TrnSticker']['trn_receiving_id'],
                            'trn_shipping_id'       => $r['TrnSticker']['trn_shipping_id'],
                            'is_stock_or_sale'      => Configure::read('Claim.stock'),
                            'count'                 => $r['TrnSticker']['quantity'],
                            'unit_price'            => $r['TrnSticker']['transaction_price'],
                            'round'                 => $r['Supplier']['round'],
                            'gross'                 => $r['Supplier']['gross'],
                            'creater'               => $this->Session->read('Auth.MstUser.id'),
                            'created'               => $now,
                            'modifier'              => $this->Session->read('Auth.MstUser.id'),
                            'modified'              => $now,
                            'facility_sticker_no'   => $r['TrnSticker']['facility_sticker_no'],
                            'hospital_sticker_no'   => $r['TrnSticker']['hospital_sticker_no'],
                            'trade_type'            => $r['TrnSticker']['trade_type'],
                            'claim_type'            => Configure::read('ClaimType.stock')
                            )
                        );
                    $this->TrnClaim->create();
                    // SQL実行
                    if (!$this->TrnClaim->save($TrnClaim)) {
                        $this->TrnConsume->rollback();
                        $this->Session->setFlash('仕入情報登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }
                    $buy_claim_id = $this->TrnClaim->getLastInsertID();
                    // MS仕入登録
                    $this->createMsClaimRecode($buy_claim_id);
                }
                
                // シール更新 数量▽ , 移動消費の場合、部署情報を選択部署に変更、売上明細IDを更新、消費明細IDを更新
                $this->TrnSticker->create();
                $res = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.quantity'          => 'TrnSticker.quantity - ' . $r['TrnSticker']['quantity'],
                        'TrnSticker.mst_department_id' => $this->request->data['TrnConsumeHeader']['departmentId'],
                        'TrnSticker.trn_consume_id'    => $trn_consume_id,
                        'TrnSticker.sale_claim_id'     => $sale_claim_id ,
                        'TrnSticker.buy_claim_id'      => $buy_claim_id ,
                        'TrnSticker.modifier'          => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'          => "'".$now."'",
                        ),
                    array(
                        'TrnSticker.id' => $r['TrnSticker']['id']
                        ),
                    -1);
                
                if(!$res){
                    $this->TrnConsume->rollback();
                    $this->Session->setFlash('在庫更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }
                
                $work_seq++;
            }

            /* コミット直前に更新チェック */
            //すべてエラーがなければコミット
            $this->TrnConsume->commit();
            
            $this->set('recital', $this->request->data['TrnConsume']['recital']);
            $this->set('result' , $ret);

            // アプテージ用連携データ用にTrnConsumeHeaderのIDをセットする
            $this->request->data['TrnConsumeHeader']['ForAptage'] = $trn_consume_header_id;
            $this->Session->write('TrnConsumeHeader', $this->request->data['TrnConsumeHeader']);

            // 2度押しようにセッションに詰め込む
            $this->Session->write('Consume.recital', $this->request->data['TrnConsume']['recital']);
            $this->Session->write('Consume.result' , $ret);
        }else{
            // トランザクショントークン不正
            // リロード or 2度押し
            // セッションから必要情報取得
            $this->request->data['TrnConsumeHeader'] = $this->Session->read('TrnConsumeHeader');
            $this->set('recital', $this->Session->read('Consume.recital'));
            $this->set('result' , $this->Session->read('Consume.result'));
        }

        $this->render('add_result_view');
    }

    /**
     * 消費履歴
     * @access public
     */
    function edit_search ($_mode = 0) {
        $this->setRoleFunction(2); //消費履歴
        App::import('Sanitize');

        $_params = $_consumes = $_chs = $_params4report = $_chs_disp = array();

        //検索ボタンが押されたら
        if (isset($this->request->data['search']['is_search'])) {
            //画面検索用
            $sql_param = "";

            // 削除済み
            if (isset($this->request->data['TrnConsume']['is_deleted']) && $this->request->data['TrnConsume']['is_deleted'] == '0') {
                $sql_param .= " AND TrnConsume.is_deleted = FALSE ";
            }
            
            if (isset($this->request->data['TrnConsume']['is_output']) && $this->request->data['TrnConsume']['is_output'] == '1') {
                $sql_param .= " AND TrnConsume.is_output = false ";
            }

            // 作業番号
            if (isset($this->request->data['TrnConsume']['work_no']) && $this->request->data['TrnConsume']['work_no'] != '') {
                $sql_param .= " AND TrnConsume.work_no LIKE ". "'%".Sanitize::escape($this->request->data['TrnConsume']['work_no'])."%'";
            }

            // 消費日From
            if (isset($this->request->data['TrnConsume']['work_date_from']) && $this->request->data['TrnConsume']['work_date_from'] != ''){
                $sql_param .= " AND TrnConsume.work_date >= '". Sanitize::escape($this->request->data['TrnConsume']['work_date_from'])."'";
            }

            // 消費日To
            if (isset($this->request->data['TrnConsume']['work_date_to']) && $this->request->data['TrnConsume']['work_date_to'] != ''){
                $sql_param .= " AND TrnConsume.work_date <= '". Sanitize::escape($this->request->data['TrnConsume']['work_date_to'])."'";
            }

            // 作業区分
            if (isset($this->request->data['TrnConsume']['work_type']) && $this->request->data['TrnConsume']['work_type'] != ''){
                $sql_param .= " AND TrnConsume.work_type = '". Sanitize::escape($this->request->data['TrnConsume']['work_type'])."'";
            }

            // 施設
            if (isset($this->request->data['TrnConsume']['facilityCode']) && $this->request->data['TrnConsume']['facilityCode'] != '') {
                $sql_param .= " AND MstFacility.facility_code = '". $this->request->data['TrnConsume']['facilityCode'] . "'";
            }

            // 部署
            if (isset($this->request->data['TrnConsume']['departmentCode']) && $this->request->data['TrnConsume']['departmentCode'] != '') {
                $sql_param .= " AND MstDepartment.department_code = '". $this->request->data['TrnConsume']['departmentCode'] ."'";
            }

            // 施主
            if (isset($this->request->data['TrnConsume']['ownerCode']) && $this->request->data['TrnConsume']['ownerCode'] != '') {
                $sql_param .= " AND MstOwner.facility_code = '". $this->request->data['TrnConsume']['ownerCode'] . "'";
            }

            // 商品ID
            if (isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] != '') {
                $sql_param .= " AND MstFacilityItem.internal_code = '".  Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."'";
            }

            // 製品番号
            if (isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] != '') {
                $sql_param .= " AND MstFacilityItem.item_code LIKE '".  "%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
            }

            // 商品名
            if (isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] != '') {
                $sql_param .= " AND MstFacilityItem.item_name LIKE '".  "%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
            }

            // 規格
            if (isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] != '') {
                $sql_param .= " AND MstFacilityItem.standard LIKE '".  "%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
            }

            // 販売元
            if (isset($this->request->data['MstDealer']['dealer_name']) && $this->request->data['MstDealer']['dealer_name'] != '') {
                $sql_param .= " AND MstDealer.dealer_name LIKE '".  "%".Sanitize::escape($this->request->data['MstDealer']['dealer_name'])."%'";
            }

            // 管理区分
            if (isset($this->request->data['TrnConsume']['trade_type']) && $this->request->data['TrnConsume']['trade_type'] != '') {
                $sql_param .= " AND coalesce(TrnConsume.trade_type , TrnSticker.trade_type)= ".  Sanitize::escape($this->request->data['TrnConsume']['trade_type']);
            }
            // 使用区分
            if (isset($this->request->data['TrnConsume']['use_type']) && $this->request->data['TrnConsume']['use_type'] != '') {
                $sql_param .= " AND TrnConsume.use_type = ".  Sanitize::escape($this->request->data['TrnConsume']['use_type']);
            }

            // ロット番号
            if (isset($this->request->data['TrnSticker']['lot_no']) && $this->request->data['TrnSticker']['lot_no'] != '') {
                $sql_param .= " AND TrnSticker.lot_no   LIKE ".  "'%".Sanitize::escape($this->request->data['TrnSticker']['lot_no'])."%'";
            }

            // シール番号
            if (isset($this->request->data['TrnSticker']['facility_sticker_no']) && $this->request->data['TrnSticker']['facility_sticker_no'] != '') {
                $_params['conditions']['OR'] = array('TrnSticker.facility_sticker_no ILIKE'=>$this->request->data['TrnSticker']['facility_sticker_no'],
                                                     'TrnSticker.hospital_sticker_no ILIKE'=>$this->request->data['TrnSticker']['facility_sticker_no']);

                $sql_param .= " AND (TrnSticker.facility_sticker_no ILIKE '".  Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."'";
                $sql_param .= " OR TrnSticker.hospital_sticker_no ILIKE '".  Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."'";
                $sql_param .= " OR TrnConsume.hospital_sticker_no ILIKE '".  Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."'";
                $sql_param .= " OR TrnConsume.facility_sticker_no ILIKE '".  Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."')";
            }
            $sort = "  TrnConsumeHeader.work_no DESC ";

            $sql = " SELECT ";
            $sql .='     TrnConsumeHeader.id            as "TrnConsumeHeader__id"';
            $sql .='    ,TrnConsumeHeader.work_no       as "TrnConsumeHeader__work_no"';
            $sql .="    ,to_char(TrnConsumeHeader.work_date , 'YYYY/mm/dd')";
            $sql .='                                    as "TrnConsumeHeader__work_date"';
            $sql .="    ,to_char(TrnConsumeHeader.created , 'YYYY/mm/dd hh24:mi:ss')";
            $sql .='                                    as "TrnConsumeHeader__created"';
            $sql .='    ,TrnConsumeHeader.recital       as "TrnConsumeHeader__recital"';
            $sql .='    ,TrnConsumeHeader.detail_count  as "TrnConsumeHeader__detail_count"';
            $sql .='    ,MstFacility.facility_name      as "TrnConsumeHeader__facility_name"';
            $sql .='    ,MstDepartment.department_name  as "TrnConsumeHeader__department_name"';
            $sql .='    ,MstUser.user_name              as "TrnConsumeHeader__user_name"';
            $sql .='    ,count(*)                       as "TrnConsumeHeader__count"';
            $sql .='    ,TrnConsumeHeader.is_outputted  as "TrnConsumeHeader__is_outputted"';
            $sql .=" FROM trn_consumes AS TrnConsume ";
            $sql .=" INNER JOIN mst_item_units AS  MstItemUnit ";
            $sql .="     ON MstItemUnit.id = TrnConsume.mst_item_unit_id ";
            $sql .=" INNER JOIN mst_facility_items AS MstFacilityItem ";
            $sql .="     ON  MstItemUnit.mst_facility_item_id = MstFacilityItem.id ";
            $sql .=" LEFT JOIN mst_dealers AS MstDealer ";
            $sql .="     ON MstDealer.id = MstFacilityItem.mst_dealer_id";
            $sql .=" LEFT JOIN trn_stickers AS TrnSticker ";
            $sql .="     ON TrnConsume.trn_sticker_id = TrnSticker.id ";
            $sql .=" LEFT JOIN trn_consume_headers AS TrnConsumeHeader ";
            $sql .="     ON TrnConsume.trn_consume_header_id = TrnConsumeHeader.id ";
            $sql .=" LEFT JOIN mst_departments AS MstDepartment";
            $sql .="     ON TrnConsume.mst_department_id = MstDepartment.id";
            $sql .=" LEFT JOIN mst_users AS MstUser";
            $sql .="     ON MstUser.id = TrnConsumeHeader.creater";
            $sql .=" LEFT JOIN mst_facilities AS MstFacility";
            $sql .="     ON MstFacility.id = MstDepartment.mst_facility_id";
            $sql .=" LEFT JOIN mst_facilities as MstOwner";
            $sql .= "    ON MstOwner.id = MstFacilityItem.mst_owner_id";
            $sql .=" INNER JOIN mst_facility_relations AS MstFacilityRelations";
            $sql .="     ON MstFacilityRelations.mst_facility_id = " .$this->request->data['TrnConsume']['mst_facility_id'];
            $sql .="     AND MstFacilityRelations.partner_facility_id = MstFacility.id";
            $sql.= ' inner join mst_user_belongings as mstuserbelonging ';
            $sql.= '     on mstuserbelonging.mst_user_id = ' . $this->request->data['TrnConsume']['mst_user_id'];
            $sql.= '     and mstuserbelonging.mst_facility_id = mstfacility.id  ';
            $sql.= '     and mstuserbelonging.is_deleted = false ';
            $sql .=" WHERE ";
            $sql .="     TrnConsume.use_type IN(".Configure::read('UseType.use').",".
                                                  Configure::read('UseType.temporary').",".
                                                  Configure::read('UseType.direct').",".
                                                  Configure::read('UseType.moveuse').",".
                                                  Configure::read('UseType.home').",".
                                                  Configure::read('UseType.ope').
                                                  ")";
            //検索
            if(!$this->isNNs($sql_param)){
                $sql .= $sql_param;
            }
            $sql .=" GROUP BY ";
            $sql .="     TrnConsumeHeader.id";
            $sql .="    ,TrnConsumeHeader.work_no";
            $sql .="    ,TrnConsumeHeader.work_date";
            $sql .="    ,TrnConsumeHeader.creater";
            $sql .="    ,TrnConsumeHeader.created";
            $sql .="    ,TrnConsumeHeader.recital";
            $sql .="    ,TrnConsumeHeader.detail_count";
            $sql .="    ,MstDepartment.department_name";
            $sql .="    ,MstUser.user_name";
            $sql .="    ,MstFacility.facility_name ";
            $sql .=" ORDER BY ";
            $sql .=$sort;

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnConsume'));

            $sql .= " LIMIT ".$this->_getLimitCount();
            //履歴情報取得
            $_chs_disp = $this->TrnConsume->query($sql);
        } else {
            // 初期データ
            $this->request->data['TrnConsume']['work_date_from']   = date("Y/m/d",mktime(0, 0, 0,date("m"), date("d")-7,date("Y")));
            $this->request->data['TrnConsume']['work_date_to']     = date("Y/m/d");
            $this->request->data['TrnConsume']['mst_facility_id']  = $this->Session->read('Auth.facility_id_selected');
            $this->request->data['TrnConsume']['mst_user_id']      = $this->Session->read('Auth.MstUser.id');
        }

        $this->set('facility_id_selected', $this->Session->read('Auth.facility_id_selected'));

        $this->set('department_list' , $this->getHospitalDepartmentList($this->Session->read('Auth.facility_id_selected')));

        //画面表示
        $this->set('trade_type', Configure::read('Classes'));
        $this->set('use_type', Configure::read('UseTypes'));
        $this->set('consumes',$_chs_disp);
    }

    /**
     * 消費履歴編集
     */
    function edit () {
        list($result , $dummy) = $this->__getConsume($this->request->data);
        $this->set('result',$result);

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnConsumes.token' , $token);
        $this->request->data['TrnConsumes']['token'] = $token;

        $this->render('edit');
    }

    /**
     * 消費履歴編集完了
     */
    function edit_result () {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['TrnConsumes']['token'] === $this->Session->read('TrnConsumes.token')) {
            $this->Session->delete('TrnConsumes.token');
            $this->request->data['now'] = $now = date('Y/m/d H:i:s');
            //更新チェック
            if(!$this->updateCheck($this->request->data['TrnConsume']['id'] , $this->request->data['TrnConsume']['checktime'])){
                $this->Session->setFlash('シール情報が更新されています', 'growl', array('type'=>'error'));
                $this->redirect('edit_search');
            }
            
            //トランザクション開始
            $this->TrnConsume->begin();
            //行ロック
            $this->TrnConsume->query('select * from trn_consumes as a where a.id in ( ' . join(',' , $this->request->data['TrnConsume']['id']). ' ) for update ');
            
            // 必要情報を取得
            $sql  = ' select ';
            $sql .= '       a.id ';
            $sql .= '     , a.mst_item_unit_id ';
            $sql .= '     , a.quantity ';
            $sql .= '     , b.id                                               as sticker_id ';
            $sql .= '     , coalesce(d.mst_department_id, a.mst_department_id) as department_id  ';
            $sql .= '   from ';
            $sql .= '     trn_consumes as a  ';
            $sql .= '     left join trn_stickers as b  ';
            $sql .= '       on b.id = a.trn_sticker_id  ';
            $sql .= '     left join trn_sticker_records as d  ';
            $sql .= '       on d.record_type = ' . Configure::read('RecordType.move');
            $sql .= '       and d.record_id = a.id  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $this->request->data['TrnConsume']['id']). ') ';
            
            $result = $this->TrnConsume->query($sql);
            //対象レコードの情報を取得
            foreach ($result as $r) {
                // 消費明細
                $this->TrnConsume->create();
                $ret = $this->TrnConsume->updateAll(
                    array(
                        'TrnConsume.is_deleted' => "'true'",
                        'TrnConsume.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'TrnConsume.modified'   => "'" . $now . "'"
                        ),
                    array(
                        'TrnConsume.id' => $r[0]['id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnConsume->rollback();
                    $this->Session->setFlash('消費明細取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('edit_search');
                }
                
                // 在庫数
                $this->TrnStock->create();
                $ret = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.stock_count' => "TrnStock.stock_count + " . $r[0]['quantity'],
                        'TrnStock.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'   => "'" . $now . "'"
                        ),
                    array(
                        'TrnStock.id' => $this->getStockRecode($r[0]['mst_item_unit_id'],
                                                               $r[0]['department_id']),
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnConsume->rollback();
                    $this->Session->setFlash('在庫数更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('edit_search');
                }
                // 請求削除
                $this->TrnClaim->create();
                $ret = $this->TrnClaim->updateAll(
                    array(
                        'TrnClaim.is_deleted' => "'true'",
                        'TrnClaim.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'TrnClaim.modified'   => "'" . $now . "'"
                        ),
                    array(
                        'TrnClaim.trn_consume_id' => $r[0]['id']
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnConsume->rollback();
                    $this->Session->setFlash('請求情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('edit_search');
                }

                // シール移動履歴削除
                $this->TrnStickerRecord->create();
                $ret = $this->TrnStickerRecord->updateAll(
                    array(
                        'TrnStickerRecord.is_deleted' => "'true'",
                        'TrnStickerRecord.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'TrnStickerRecord.modified'   => "'" . $now . "'"
                        ),
                    array(
                        'TrnStickerRecord.record_id'      => $r[0]['id'],
                        'TrnStickerRecord.trn_sticker_id' => $r[0]['sticker_id'],
                        'TrnStickerRecord.record_type'    => array(Configure::read('RecordType.consume'),
                                                                   Configure::read('RecordType.move'))
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnConsume->rollback();
                    $this->Session->setFlash('シール移動履歴更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('edit_search');
                }
                
                // シール情報更新
                $this->TrnSticker->create();
                $ret = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.trn_consume_id'    => null ,
                        'TrnSticker.mst_department_id' => $r[0]['department_id'],
                        'TrnSticker.quantity'          => "TrnSticker.quantity + " . $r[0]['quantity'],
                        'TrnSticker.modifier'          => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'          => "'" . $now . "'"
                        ),
                    array(
                        'TrnSticker.id' => $r[0]['sticker_id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnConsume->rollback();
                    $this->Session->setFlash('在庫数更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('edit_search');
                }

                
                // 定数切替取消
                if(!$this->switchCancel($r[0]['id'])){
                    $this->TrnConsume->rollback();
                    $this->Session->setFlash('定数切替取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('edit_search');
                }
            }
            // 消費ヘッダ
            $sql  = ' update trn_consume_headers x  ';
            $sql .= '   set ';
            $sql .= '     is_deleted = y.is_deleted  ';
            $sql .= '   from ';
            $sql .= '     (  ';
            $sql .= '       select ';
            $sql .= '             a.trn_consume_header_id ';
            $sql .= '           , (  ';
            $sql .= '             case  ';
            $sql .= '               when sum(case when a.is_deleted = true then 1 else 0 end) = b.detail_count  ';
            $sql .= '               then true  ';
            $sql .= '               else false  ';
            $sql .= '               end ';
            $sql .= '           )                             as is_deleted  ';
            $sql .= '         from ';
            $sql .= '           trn_consumes as a  ';
            $sql .= '           left join trn_consume_headers as b  ';
            $sql .= '             on b.id = a.trn_consume_header_id  ';
            $sql .= '         where ';
            $sql .= '           a.id in (' . join(',' , $this->request->data['TrnConsume']['id']). ')';
            $sql .= '         group by ';
            $sql .= '           a.trn_consume_header_id ';
            $sql .= '           , b.detail_count ';
            $sql .= '     ) y  ';
            $sql .= '   where ';
            $sql .= '     x.id = y.trn_consume_header_id ';
            
            $this->TrnConsume->query($sql);
            
            //取消し成功
            //コミット直前に更新チェック
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_consumes as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $this->request->data['TrnConsume']['id']). ')';
            $sql .= "     and a.modified > '" . $this->request->data['TrnConsume']['checktime'] ."'";
            $sql .= "     and a.modified <> '" . $this->request->data['now'] ."'";
            
            $ret = $this->TrnConsume->query($sql);
            
            if(($ret[0][0]['count']> 0 )? true : false){
                $this->TrnConsume->rollback();
                $this->Session->setFlash('ほかのユーザによって更新されています。最初からやり直してください', 'growl', array('type'=>'error') );
                $this->redirect('edit_search');
                return;
            }
            
            $this->TrnConsume->commit();
            
            $this->Session->setFlash('消費取り消しが完了しました', 'growl', array('type'=>'star'));
            
            $this->request->data['TrnConsume']['is_deleted'] = true;
            list($result , $dummy)= $this->__getConsume($this->request->data);
            $this->set('result',$result);
            $this->Session->write('TrnConsumes.result',$result);
        }else{
            //2度押し本対策
            $this->set('result',$this->Session->read('TrnConsumes.result'));

        }
    }

    // 更新チェック
    function updateCheck($id , $time){
        $sql ='';
        $sql .=' select ';
        $sql .='       count(a.id)  ';
        $sql .='   from ';
        $sql .='     trn_consumes as a  ';
        $sql .='     left join trn_stickers as b  ';
        $sql .='       on a.trn_sticker_id = b.id  ';
        $sql .='   where ';
        $sql .="     a.id in (" .join(',',$id) ." )  ";
        $sql .='     and (  ';
        $sql .="       a.modified >= '" . date('Y/m/d H:i:s' , strtotime($time)) ."'  ";
        $sql .="       or b.modified >= '" . date('Y/m/d H:i:s' , strtotime($time)) ."' ";
        $sql .='     )  ';

        $ret = $this->TrnConsume->query($sql);

        return (($ret[0][0]['count'] == 0 )?true:false);
    }

    /**
     * 消費履歴CSV出力
     */
    function export_csv() {
        $this->request->data['TrnConsumeHeader']['id'] = null;

        $sql = $this->_getConsumeCSV($this->request->data);
        $this->db_export_csv($sql , "消費履歴", 'edit_search');
    }

    private function _getConsumeCSV($data){
        $this->request->data = $data;
        //施設が選択されていた場合、部署を取得
        if(isset($this->request->data['TrnConsume']['facilityCode']) && ($this->request->data['TrnConsume']['facilityCode'] !== "" )) {
            $departments = $this->getDepartmentsList($this->request->data['TrnConsume']['facilityCode'] , false);
        }else{
            $departments = array();
        }

        // 入力された検索条件で絞込み ココから
        $where = ' and a.use_type in (' . join(',',array(Configure::read('UseType.use'),          //消費
                                                         Configure::read('UseType.temporary'),    //臨時出荷
                                                         Configure::read('UseType.moveuse'),      //移動消費
                                                         Configure::read('UseType.ope'),          //オペセット使用
                                                         Configure::read('UseType.home'),         //在宅
                                                         Configure::read('UseType.direct'))       //直納
                                               ) . ')';


        $where .= " and k.id in ( ( select x.partner_facility_id as id  from  mst_facility_relations as x  where x.mst_facility_id = ".$this->request->data['TrnConsume']['mst_facility_id']." union all select id  from mst_facilities as z where z.id = ".$this->request->data['TrnConsume']['mst_facility_id']." )) ";

        //消費ヘッダー
        if (isset($this->request->data['TrnConsumeHeader']['id'])) {
            $where .= ' and b.id in ( ' . join(',',$this->request->data['TrnConsumeHeader']['id'] ) .')';
            $where2 = ' a3.id in ( ' . join(',',$this->request->data['TrnConsumeHeader']['id'] ) .')';
        }else{
            $where2 = ' 0 = 1 ';
        }
        //消費明細
        if (isset($this->request->data['TrnConsume']['id'])) {
            $where .= ' and a.id in ( ' . join(',',$this->request->data['TrnConsume']['id'] ) .')';
        }

        if (isset($this->request->data['TrnSticker']['facility_sticker_no']) && $this->request->data['TrnSticker']['facility_sticker_no'] != '') {
            $where .= " and ( a.facility_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."'";
            $where .=    " or a.hospital_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."'";
            $where .=    " or c.facility_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."'";
            $where .=    " or c.hospital_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."' )";
        }
        if (isset($this->request->data['TrnConsume']['work_no']) && $this->request->data['TrnConsume']['work_no'] != '') {
            $where .= " and b.work_no like '%" . $this->request->data['TrnConsume']['work_no'] ."%'";
        }
        if (isset($this->request->data['TrnConsume']['work_date_from']) && $this->request->data['TrnConsume']['work_date_from'] != ''){
            $where .= " and b.work_date >= '".  $this->request->data['TrnConsume']['work_date_from'] ."'";
        }
        if (isset($this->request->data['TrnConsume']['work_date_to']) && $this->request->data['TrnConsume']['work_date_to'] != ''){
            $where .= " and b.work_date <= '" . $this->request->data['TrnConsume']['work_date_to'] . "'";
        }

        if (isset($this->request->data['TrnConsume']['facilityCode']) && $this->request->data['TrnConsume']['facilityCode'] != '') {
            $where .= " and k.facility_code = '" . $this->request->data['TrnConsume']['facilityCode'] ."'";
        }
        if (isset($this->request->data['TrnConsume']['departmentCode']) && $this->request->data['TrnConsume']['departmentCode'] != '') {
            $where .= " and j.department_code = '" . $this->request->data['TrnConsume']['departmentCode'] ."'";
        }
        if (isset($this->request->data['TrnSticker']['lot_no']) && $this->request->data['TrnSticker']['lot_no'] != '') {
            $where .= " and c.lot_no like '%" . $this->request->data['TrnSticker']['lot_no'] ."%'";
        }
        if (isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] != '') {
            $where .= " and e.internal_code = '" . $this->request->data['MstFacilityItem']['internal_code'] ."'";
        }
        if (isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] != '') {
            $where .= " and e.item_code like '%" . $this->request->data['MstFacilityItem']['item_code'] ."%'";
        }
        if (isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] != '') {
            $where .= " and e.item_name like '%" . $this->request->data['MstFacilityItem']['item_name'] ."%'";
        }
        if (isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] != '') {
            $where .= " and e.standard like '%" . $this->request->data['MstFacilityItem']['standard'] ."%'";
        }
        if (isset($this->request->data['MstDealer']['dealer_name']) && $this->request->data['MstDealer']['dealer_name'] != '') {
            $where .= " and i.dealer_name like '%" . $this->request->data['MstDealer']['dealer_name'] ."%'";
        }
        if (isset($this->request->data['TrnConsume']['is_deleted']) && $this->request->data['TrnConsume']['is_deleted'] == '0') {
            $where .= " and a.is_deleted = false ";
        }
        if (isset($this->request->data['TrnConsume']['work_type']) && $this->request->data['TrnConsume']['work_type'] != ''){
            $where .= " and a.work_type = '" . $this->request->data['TrnConsume']['work_type'] ."'";
        }
        if (isset($this->request->data['TrnConsume']['ownerCode']) && $this->request->data['TrnConsume']['ownerCode'] != '') {
            $where .= " and xxx.facility_code = '" . $this->request->data['TrnConsume']['ownerCode'] ."'";
        }
        if (isset($this->request->data['TrnConsume']['trade_type']) && $this->request->data['TrnConsume']['trade_type'] != '') {
            $where .= " and coalesce(a.trade_type , c.trade_type) = '" . $this->request->data['TrnConsume']['trade_type'] ."'";
        }
        if (isset($this->request->data['TrnConsume']['use_type']) && $this->request->data['TrnConsume']['use_type'] != '') {
            $where .= " and a.use_type = '" . $this->request->data['TrnConsume']['use_type'] ."'";
        }
        // 入力された検索条件で絞込み ココまで
        $sql  =' select ';
        $sql .='       b.work_no                                              as 消費番号';
        $sql .="     , to_char(b.work_date , 'YYYY/mm/dd')                    as 消費日";
        $sql .='     , k.facility_name                                        as 施設名';
        $sql .='     , j.department_name                                      as 部署名';
        $sql .='     , e.internal_code                                        as "商品ID"';
        $sql .='     , ( ';
        $sql .='       case  ';
        foreach( Configure::read('Classes') as $k => $v ){
            $sql .="         when '".$k."' = coalesce(a.trade_type , c.trade_type ) then '".$v."' ";
        }
        $sql .='         end ';
        $sql .='      )                                                       as 管理区分';

        $sql .='     , e.item_name                                            as 商品名';
        $sql .='     , e.standard                                             as 規格';
        $sql .='     , e.item_code                                            as 製品番号';
        $sql .='     , i.dealer_name                                          as 販売元名';
        $sql .='     , e1.name                                                as 勘定科目';
        $sql .='     , e.insurance_claim_code                                 as レセプト電算コード';
        $sql .='     , e.refund_price                                         as 償還価格';
        $sql .='     , d.per_unit                                             as 入数';
        $sql .='     , d2.unit_name                                           as 基本単位';
        $sql .='     , d1.unit_name                                           as 包装単位';
        $sql .='     , a.quantity                                             as 数量';
        
        /*
        $sql .='     , (  ';
        $sql .='       case  ';
        $sql .='         when d.per_unit = 1  ';
        $sql .='         then d1.unit_name  ';
        $sql .="         else d1.unit_name || '(' || d.per_unit || d2.unit_name || ')'  ";
        $sql .='         end ';
        $sql .='     )                                                        as 包装単位名';
        */
        
        $sql .='     , sum(coalesce(c.sales_price,h.unit_price,h3.unit_price,h4.sales_price)) as 売上単価';
        $sql .='     , c.lot_no                                               as ロット番号';
        $sql .="     , to_char(c.validated_date , 'YYYY/mm/dd') ";
        $sql .='                                                              as 有効期限';
        $sql .='     , ( case a.use_type ';
        foreach(Configure::read('UseTypes') as $k => $v){
            $sql .="         when '" . $k . "' then '" . $v . "'";
        }
        $sql .='       end )                                                  as 使用区分';
        $sql .='     , g.name                                                 as 作業区分';
        $sql .='     , coalesce(a.hospital_sticker_no, c.hospital_sticker_no) as 部署シール番号';
        $sql .='     , coalesce(a.facility_sticker_no, c.facility_sticker_no) as センターシール番号';
        $sql .='     , f.user_name                                            as 更新者名';
        $sql .='     , a.recital                                              as 備考';
        $sql .='     , b.recital                                              as ヘッダ備考';
        /* オペセットの場合 ココから */
        if($this->Session->read('Auth.Config.OpeSetUseType') == '1'){
            $sql .='     , z2.work_no                                             as 手術コード';
            $sql .='     , z4.ope_method_set_code                                 as 術式コード';
            $sql .='     , z4.ope_method_name                                     as 術式名';
            $sql .='     , z3.ope_item_set_code                                   as 材料セットコード';
            $sql .='     , z6.ope_item_set_name                                   as 材料セット名';
        }
        /* オペセットの場合 ココまで */
        $sql .='   from ';
        $sql .='     trn_consumes as a  ';
        $sql .='     left join trn_consume_headers as b  ';
        $sql .='       on a.trn_consume_header_id = b.id  ';
        $sql .='     left join trn_stickers as c  ';
        $sql .='       on a.trn_sticker_id = c.id  ';
        $sql .='     left join mst_item_units as d  ';
        $sql .='       on coalesce(c.mst_item_unit_id,a.mst_item_unit_id) = d.id  ';
        $sql .='     left join mst_unit_names as d1  ';
        $sql .='       on d1.id = d.mst_unit_name_id  ';
        $sql .='     left join mst_unit_names as d2  ';
        $sql .='       on d2.id = d.per_unit_name_id  ';
        $sql .='     left join mst_facility_items as e  ';
        $sql .='       on e.id = d.mst_facility_item_id  ';
        $sql .='     left join mst_accountlists as e1 ';
        $sql .='        on e.mst_accountlist_id = e1.id ';
        $sql .='     left join mst_facilities as xxx  ';
        $sql .='       on xxx.id = e.mst_owner_id  ';
        $sql .='     left join mst_users as f  ';
        $sql .='       on f.id = a.modifier  ';
        $sql .='     left join mst_classes as g  ';
        $sql .='       on g.id = a.work_type  ';
        $sql .='     left join trn_claims as h  ';
        $sql .='       on h.trn_consume_id = a.id  ';
        $sql .="       and h.is_stock_or_sale = '2' ";
        $sql .='     left join trn_claims as h3  ';
        $sql .='       on h3.id = c.sale_claim_id  ';
        $sql .="       and h3.is_stock_or_sale = '2' ";
        $sql .='     left join trn_receivings as h2  ';
        $sql .='       on h2.id = h.trn_receiving_id  ';
        $sql .='     left join mst_dealers as i  ';
        $sql .='       on i.id = e.mst_dealer_id  ';
        $sql .='     left join mst_departments as j  ';
        $sql .='       on j.id = a.mst_department_id  ';
        $sql .='     left join mst_facilities as k  ';
        $sql .='       on k.id = j.mst_facility_id  ';
        $sql .='     left join mst_sales_configs as h4 ';
        $sql .='       on h4.mst_item_unit_id = d.id ';
        $sql .='      and h4.partner_facility_id = k.id ';
        $sql .='      and h4.start_date <= a.work_date '; 
        $sql .='      and h4.end_date > a.work_date ';
        $sql .='      and h4.is_deleted = false ';
        $sql .='     left join ';
        $sql .='(select ';
        $sql .='      a3.id ';
        $sql .='  from ';
        $sql .='    trn_consume_headers as a3  ';
        $sql .='    left join trn_consumes as b3  ';
        $sql .='      on b3.trn_consume_header_id = a3.id  ';
        $sql .='    left join trn_claims as c3  ';
        $sql .='      on c3.trn_consume_id = b3.id  ';
        $sql .='    inner join trn_receivings as d3  ';
        $sql .='      on d3.id = c3.trn_receiving_id  ';
        $sql .='      and d3.receiving_type = ' . Configure::read('ReceivingType.repay');
        $sql .='  where ';
        $sql .=$where2;
        $sql .='  group by ';
        $sql .='    a3.id  ';
        $sql .='  having ';
        $sql .='    count(a3.id) > 0 ';
        $sql .=') as x ';
        $sql .=' on x.id = b.id ';

        $sql .='     cross join ';

        $sql .='( select ';
        $sql .='      c.updatable ';
        $sql .='  from ';
        $sql .='    mst_users as a  ';
        $sql .='    left join mst_roles as b ';
        $sql .='      on b.id = a.mst_role_id ';
        $sql .='    left join mst_privileges as c ';
        $sql .='      on c.mst_role_id = b.id ';
        $sql .='      and c.view_no = 2';
        $sql .='  where ';
        $sql .='    a.id = ' . $this->request->data['TrnConsume']['mst_user_id'];
        $sql .=') as y ';

        $sql.= '    inner join mst_user_belongings as z ';
        $sql.= '      on z.mst_user_id = ' . $this->request->data['TrnConsume']['mst_user_id'];
        $sql.= '      and z.mst_facility_id = k.id  ';
        $sql.= '      and z.is_deleted = false  ';
        
        /* オペセットの場合 ココから */
        if($this->Session->read('Auth.Config.OpeSetUseType') == '1'){
            $sql .='    left join trn_ope_shippings as z1  ';
            $sql .='      on z1.trn_sticker_id = c.id  ';
            $sql .='    left join trn_ope_headers as z2  ';
            $sql .='      on z2.id = z1.trn_ope_header_id  ';
            $sql .='    left join trn_ope_items as z3  ';
            $sql .='      on z3.id = z1.trn_ope_item_id  ';
            $sql .='    left join mst_ope_method_names as z4  ';
            $sql .='      on z4.id = z2.mst_ope_method_id  ';
            $sql .='    left join trn_ope_returns as z5  ';
            $sql .='      on z5.trn_ope_item_id = z1.trn_ope_item_id  ';
            $sql .='     and z5.trn_sticker_id = z1.trn_sticker_id ';
            $sql .='    left join mst_ope_item_set_headers as z6 ';
            $sql .='      on z6.ope_item_set_code = z3.ope_item_set_code ';
        }
        /* オペセットの場合 ココまで */
        
        $sql .=' where 1 = 1 ';
        if($this->Session->read('Auth.Config.OpeSetUseType') == '1'){
            $sql .=' and z5.id is null ';
        }
        $sql .=' and ( h2.receiving_type <> ' . Configure::read('ReceivingType.repay') . 'or h2.receiving_type is null ) ';
        $sql .=$where;
        $sql .='   group by ';
        $sql .='    a.id ';
        $sql .='    , a.trade_type ';
        $sql .='    , a.facility_sticker_no ';
        $sql .='    , a.hospital_sticker_no ';
        $sql .='    , c.facility_sticker_no ';
        $sql .='    , c.hospital_sticker_no ';
        $sql .='    , b.work_no ';
        $sql .='    , b.work_date ';
        $sql .='    , e.internal_code ';
        $sql .='    , e.item_name ';
        $sql .='    , e.item_code ';
        $sql .='    , e.standard ';
        $sql .='    , e.tax_type ';
        $sql .='    , e1.name ';
        $sql .='    , i.dealer_name ';
        $sql .='    , k.facility_name ';
        $sql .='    , k.facility_formal_name ';
        $sql .='    , k.gross ';
        $sql .='    , k.round ';
        $sql .='    , j.id ';
        $sql .='    , j.department_name ';
        $sql .='    , j.department_formal_name ';
        $sql .='    , j.department_code ';
        $sql .='    , a.use_type ';
        $sql .='    , a.recital ';
        $sql .='    , b.recital ';
        $sql .='    , c.lot_no ';
        $sql .='    , c.validated_date ';
        $sql .='    , d.per_unit ';
        $sql .='    , d1.unit_name ';
        $sql .='    , d2.unit_name ';
        $sql .='    , f.user_name ';
        $sql .='    , g.name ';
        $sql .='    , c.trade_type ';
        $sql .='    , a.stocking_close_type ';
        $sql .='    , a.sales_close_type ';
        $sql .='    , a.facility_close_type ';
        $sql .='    , a.is_deleted ';
        $sql .='    , a.quantity ';
        $sql .='    , x.id ';
        $sql .='    , y.updatable';
        $sql .='    , e.insurance_claim_code';
        $sql .='    , e.refund_price';
        /* オペセットの場合 ココから */
        if($this->Session->read('Auth.Config.OpeSetUseType') == '1'){
            $sql .= ' ,z2.work_no ';
            $sql .= ' ,z4.ope_method_set_code ';
            $sql .= ' ,z4.ope_method_name ';
            $sql .= ' ,z3.ope_item_set_code ';   
            $sql .= ' ,z6.ope_item_set_name ';
        }
        /* オペセットの場合 ココまで */
        $sql .=' order by b.work_no desc ';

        return $sql;
    }

    private function __getConsume($data , $sort = null){
        $this->request->data = $data;
        $optimized = array();
        //作業区分
        $classes = $this->getClassesList( $this->request->data['TrnConsume']['mst_facility_id'] , 1);
        //管理区分
        $types= Configure::read('Classes');
        //使用区分
        $use_type= Configure::read('UseTypes');

        //施設が選択されていた場合、部署を取得
        if(isset($this->request->data['TrnConsume']['facilityCode']) && ($this->request->data['TrnConsume']['facilityCode'] !== "" )) {
            $departments = $this->getDepartmentsList($this->request->data['TrnConsume']['facilityCode'] , false);
        }else{
            $departments = array();
        }

        // 入力された検索条件で絞込み ココから
        $where = ' and a.use_type in (' . join(',',array(Configure::read('UseType.use'),          //消費
                                                         Configure::read('UseType.temporary'),    //臨時出荷
                                                         Configure::read('UseType.moveuse'),      //移動消費
                                                         Configure::read('UseType.direct'),       //直納
                                                         Configure::read('UseType.home'),         //在宅
                                                         Configure::read('UseType.ope'))          //オペセット使用
                                               ) . ')';


        $where .= " and k.id in ( ( select x.partner_facility_id as id  from  mst_facility_relations as x  where x.mst_facility_id = ".$this->request->data['TrnConsume']['mst_facility_id']." union all select id  from mst_facilities as z where z.id = ".$this->request->data['TrnConsume']['mst_facility_id']." )) ";

        //消費ヘッダー
        if (isset($this->request->data['TrnConsumeHeader']['id'])) {
            $where .= ' and b.id in ( ' . join(',',$this->request->data['TrnConsumeHeader']['id'] ) .')';
            $where2 = ' a3.id in ( ' . join(',',$this->request->data['TrnConsumeHeader']['id'] ) .')';
        }else{
            $where2 = ' 0 = 1 ';
        }

        //消費明細
        if (isset($this->request->data['TrnConsume']['id'])) {
            $where .= ' and a.id in ( ' . join(',',$this->request->data['TrnConsume']['id'] ) .')';
        }

        if (isset($this->request->data['TrnSticker']['facility_sticker_no']) && $this->request->data['TrnSticker']['facility_sticker_no'] != '') {
            $where .= " and ( a.facility_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."'";
            $where .=    " or a.hospital_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."'";
            $where .=    " or c.facility_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."'";
            $where .=    " or c.hospital_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."' )";
            $optimized[] = 'シール番号 = '. $this->request->data['TrnSticker']['facility_sticker_no'];
        }
        if (isset($this->request->data['TrnConsume']['work_no']) && $this->request->data['TrnConsume']['work_no'] != '') {
            $where .= " and b.work_no like '%" . $this->request->data['TrnConsume']['work_no'] ."%'";
            $optimized[] = '消費番号 = '.  $this->request->data['TrnConsume']['work_no'];
        }
        if (isset($this->request->data['TrnConsume']['work_date_from']) && $this->request->data['TrnConsume']['work_date_from'] != ''){
            $optimized[] = '消費日(はじめ)  = '. $this->request->data['TrnConsume']['work_date_from'];
            $where .= " and b.work_date >= '".  $this->request->data['TrnConsume']['work_date_from'] ."'";
        }
        if (isset($this->request->data['TrnConsume']['work_date_to']) && $this->request->data['TrnConsume']['work_date_to'] != ''){
            $where .= " and b.work_date <= '" . $this->request->data['TrnConsume']['work_date_to'] . "'";
            $optimized[] = '消費日(終わり)  = '. $this->request->data['TrnConsume']['work_date_to'];
        }

        if (isset($this->request->data['TrnConsume']['facilityCode']) && $this->request->data['TrnConsume']['facilityCode'] != '') {
            $where .= " and k.facility_code = '" . $this->request->data['TrnConsume']['facilityCode'] ."'";
            $optimized[] = '消費施設  = '. $this->request->data['TrnConsume']['facilityName'];
        }
        if (isset($this->request->data['TrnConsume']['departmentCode']) && $this->request->data['TrnConsume']['departmentCode'] != '') {
            $where .= " and j.department_code = '" . $this->request->data['TrnConsume']['departmentCode'] ."'";
            $optimized[] = '消費部署  = '. $departments[$this->request->data['TrnConsume']['departmentCode']];
        }
        if (isset($this->request->data['TrnSticker']['lot_no']) && $this->request->data['TrnSticker']['lot_no'] != '') {
            $where .= " and c.lot_no like '%" . $this->request->data['TrnSticker']['lot_no'] ."%'";
            $optimized[] = 'ロット番号  = '. $this->request->data['TrnSticker']['lot_no'];
        }
        if (isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] != '') {
            $where .= " and e.internal_code = '" . $this->request->data['MstFacilityItem']['internal_code'] ."'";
            $optimized[] = '商品ID  = '. $this->request->data['MstFacilityItem']['internal_code'];
        }
        if (isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] != '') {
            $where .= " and e.item_code like '%" . $this->request->data['MstFacilityItem']['item_code'] ."%'";
            $optimized[] = '製品番号  = '. $this->request->data['MstFacilityItem']['item_code'];
        }
        if (isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] != '') {
            $where .= " and e.item_name like '%" . $this->request->data['MstFacilityItem']['item_name'] ."%'";
            $optimized[] = '商品名  = '. $this->request->data['MstFacilityItem']['item_name'];
        }
        if (isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] != '') {
            $where .= " and e.standard like '%" . $this->request->data['MstFacilityItem']['standard'] ."%'";
            $optimized[] = '商品規格  = '. $this->request->data['MstFacilityItem']['standard'];
        }
        if (isset($this->request->data['MstDealer']['dealer_name']) && $this->request->data['MstDealer']['dealer_name'] != '') {
            $where .= " and i.dealer_name like '%" . $this->request->data['MstDealer']['dealer_name'] ."%'";
            $optimized[] = '販売元  = '. $this->request->data['MstDealer']['dealer_name'];
        }
        if (isset($this->request->data['TrnConsume']['is_deleted']) && $this->request->data['TrnConsume']['is_deleted'] == '0') {
            $where .= " and a.is_deleted = false ";
        }else{
            $optimized[] = '取消も表示 ';
        }
        if (isset($this->request->data['TrnConsume']['work_type']) && $this->request->data['TrnConsume']['work_type'] != ''){
            $where .= " and a.work_type = '" . $this->request->data['TrnConsume']['work_type'] ."'";
            $optimized[] = '作業区分  = '. $classes[$this->request->data['TrnConsume']['work_type']];
        }
        if (isset($this->request->data['TrnConsume']['ownerCode']) && $this->request->data['TrnConsume']['ownerCode'] != '') {
            $where .= " and xxx.facility_code = '" . $this->request->data['TrnConsume']['ownerCode'] ."'";
            $optimized[] = '施主  = '. $this->request->data['TrnConsume']['ownerName'] ;
        }
        if (isset($this->request->data['TrnConsume']['trade_type']) && $this->request->data['TrnConsume']['trade_type'] != '') {
            $where .= " and coalesce(a.trade_type , c.trade_type) = " . $this->request->data['TrnConsume']['trade_type'];
            $optimized[] = '管理区分  = '. $types[$this->request->data['TrnConsume']['trade_type']];
        }
        if (isset($this->request->data['TrnConsume']['use_type']) && $this->request->data['TrnConsume']['use_type'] != '') {
            $where .= " and a.use_type = " . $this->request->data['TrnConsume']['use_type'];
            $optimized[] = '使用区分  = '. $use_type[$this->request->data['TrnConsume']['use_type']];
        }
        // 入力された検索条件で絞込み ココまで
        $sql ='';
        $sql .=' select ';
        $sql .='       a.id                                                   as "TrnConsume__id"';
        $sql .='     , a.quantity                                             as "TrnConsume__quantity"';
        $sql .='     , coalesce(a.facility_sticker_no, c.facility_sticker_no) as "TrnConsume__facility_sticker_no"';
        $sql .='     , coalesce(a.hospital_sticker_no, c.hospital_sticker_no) as "TrnConsume__hospital_sticker_no"';
        $sql .='     , coalesce(a.trade_type , c.trade_type )                 as "TrnConsume__trade_type"';
        $sql .='     , b.work_no                                              as "TrnConsume__work_no"';
        $sql .="     , to_char(b.work_date , 'YYYY/mm/dd') ";
        $sql .='                                                              as "TrnConsume__work_date"';
        $sql .='     , e.internal_code                                        as "TrnConsume__internal_code"';
        $sql .='     , e.item_name                                            as "TrnConsume__item_name"';
        $sql .='     , e.item_code                                            as "TrnConsume__item_code"';
        $sql .='     , e.standard                                             as "TrnConsume__standard"';
        $sql .='     , e.tax_type                                             as "TrnConsume__tax_type"';
        $sql .='     , i.dealer_name                                          as "TrnConsume__dealer_name"';
        $sql .='     , j.department_name                                      as "TrnConsume__department_name" ';
        $sql .='     , a.use_type                                             as "TrnConsume__use_type"';
        $sql .='     , a.recital                                              as "TrnConsume__recital"';
        $sql .='     , c.lot_no                                               as "TrnConsume__lot_no"';
        $sql .="     , to_char(c.validated_date , 'YYYY/mm/dd') ";
        $sql .='                                                              as "TrnConsume__validated_date"';
        $sql .='     , sum(coalesce(c.sales_price,h.unit_price,h3.unit_price,h4.sales_price)) ';
        $sql .='                                                              as "TrnConsume__sales_price"';
        $sql .='     , (  ';
        $sql .='       case  ';
        $sql .='         when d.per_unit = 1  ';
        $sql .='         then d1.unit_name  ';
        $sql .="         else d1.unit_name || '(' || d.per_unit || d2.unit_name || ')'  ";
        $sql .='         end ';
        $sql .='     )                                                        as "TrnConsume__unit_name"';
        $sql .='     , f.user_name                                            as "TrnConsume__user_name"';
        $sql .='     , g.name                                                 as "TrnConsume__work_class_name"';
        
        $sql .='     , ( case ';
        foreach( Configure::read('UseTypes') as $k => $v){
            $sql .="          when a.use_type = '" . $k . "'  then '" . $v . "' ";
        }
        $sql .='          end )                                               as "TrnConsume__use_type_name"';
        $sql .='     , ( case  ';
        $sql .='         when a.stocking_close_type = 1 and y.updatable = false then 1 ';
        $sql .='         when a.stocking_close_type = 2 then 1 ';
        $sql .='         when a.sales_close_type = 1 and y.updatable = false then 1 ';
        $sql .='         when a.sales_close_type = 2 then 1 ';
        $sql .='         when a.facility_close_type = 1 and y.updatable = false then 1 ';
        $sql .='         when a.facility_close_type = 2 then 1 ';
        $sql .='         when a.use_type = ' . Configure::read('UseType.temporary'). ' then 1 ';
        $sql .='         when a.use_type = ' . Configure::read('UseType.direct'). ' then 1 ';
        $sql .='         when a.use_type = ' . Configure::read('UseType.ope'). ' then 1 ';
        $sql .='         when a.use_type = ' . Configure::read('UseType.home'). ' then 1 ';
        $sql .='         when a.is_deleted = true then 1 ';
        $sql .='         when x.id is not null then 1 ';
        $sql .='         else 0 ';
        $sql .='         end )                                                  as "TrnConsume__is_cancel"';
        $sql .='     , k.facility_name                                          as "TrnConsume__facility_name"';
        $sql .='     , k.facility_formal_name                                   as "TrnConsume__facility_formal_name"';
        $sql .='     , k.gross                                                  as "TrnConsume__gross"';
        $sql .='     , k.round                                                  as "TrnConsume__round"';
        $sql .='     , j.id                                                     as "TrnConsume__mst_department_id"';
        $sql .='     , j.department_name                                        as "TrnConsume__department_name"';
        $sql .='     , j.department_formal_name                                 as "TrnConsume__department_formal_name"';
        $sql .='     , j.department_code                                        as "TrnConsume__department_code"';
        $sql .='     , ( case  coalesce(a.trade_type , c.trade_type ) ';
        $sql .='         when 4 then 2 ';
        $sql .='         when 6 then 2 ';
        $sql .='         when 10 then 2 ';
        $sql .='         else 1 ';
        $sql .='          end  )                                                as "TrnConsume__trade_type2"';
        $sql .='   from ';
        $sql .='     trn_consumes as a  ';
        $sql .='     left join trn_consume_headers as b  ';
        $sql .='       on a.trn_consume_header_id = b.id  ';
        $sql .='     left join trn_stickers as c  ';
        $sql .='       on a.trn_sticker_id = c.id  ';
        $sql .='     left join mst_item_units as d  ';
        $sql .='       on a.mst_item_unit_id = d.id  ';
        $sql .='     left join mst_unit_names as d1  ';
        $sql .='       on d1.id = d.mst_unit_name_id  ';
        $sql .='     left join mst_unit_names as d2  ';
        $sql .='       on d2.id = d.per_unit_name_id  ';
        $sql .='     left join mst_facility_items as e  ';
        $sql .='       on e.id = d.mst_facility_item_id  ';
        $sql .='     left join mst_facilities as xxx  ';
        $sql .='       on xxx.id = e.mst_owner_id  ';
        $sql .='     left join mst_users as f  ';
        $sql .='       on f.id = a.modifier  ';
        $sql .='     left join mst_classes as g  ';
        $sql .='       on g.id = a.work_type  ';
        $sql .='     left join trn_claims as h  ';
        $sql .='       on h.trn_consume_id = a.id  ';
        $sql .="       and h.is_stock_or_sale = '2' ";
        $sql .='     left join trn_claims as h3  ';
        $sql .='       on h3.id = c.sale_claim_id ';
        $sql .="       and h3.is_stock_or_sale = '2' ";
        
        $sql .='     left join trn_receivings as h2  ';
        $sql .='       on h2.id = h.trn_receiving_id  ';
        $sql .='     left join mst_dealers as i  ';
        $sql .='       on i.id = e.mst_dealer_id  ';
        $sql .='     left join mst_departments as j  ';
        $sql .='       on j.id = a.mst_department_id  ';
        $sql .='     left join mst_facilities as k  ';
        $sql .='       on k.id = j.mst_facility_id  ';
        
        $sql .='     left join mst_sales_configs as h4 ';
        $sql .='       on h4.mst_item_unit_id = d.id ';
        $sql .='      and h4.partner_facility_id = k.id ';
        $sql .='      and h4.start_date <= a.work_date '; 
        $sql .='      and h4.end_date > a.work_date ';
        //$sql .='      and h4.is_deleted = false ';

        $sql .='     left join ';
        $sql .='(select ';
        $sql .='      b3.id ';
        $sql .='  from ';
        $sql .='    trn_consume_headers as a3  ';
        $sql .='    left join trn_consumes as b3  ';
        $sql .='      on b3.trn_consume_header_id = a3.id  ';
        $sql .='    left join trn_claims as c3  ';
        $sql .='      on c3.trn_consume_id = b3.id  ';
        $sql .='    inner join trn_receivings as d3  ';
        $sql .='      on d3.id = c3.trn_receiving_id  ';
        $sql .='      and d3.receiving_type = ' . Configure::read('ReceivingType.repay');
        $sql .='      and d3.is_deleted = false  ';
        $sql .='  where ';
        $sql .=$where2;
        $sql .='  group by ';
        $sql .='    b3.id  ';
        $sql .='  having ';
        $sql .='    count(b3.id) > 0 ';
        $sql .=') as x ';
        $sql .=' on x.id = a.id ';

        $sql .='     cross join ';

        $sql .='( select ';
        $sql .='      c.updatable ';
        $sql .='  from ';
        $sql .='    mst_users as a  ';
        $sql .='    left join mst_roles as b ';
        $sql .='      on b.id = a.mst_role_id ';
        $sql .='    left join mst_privileges as c ';
        $sql .='      on c.mst_role_id = b.id ';
        $sql .='      and c.view_no = 2';
        $sql .='  where ';
        $sql .='    a.id = ' . $this->request->data['TrnConsume']['mst_user_id'];
        $sql .=') as y ';

        $sql.= '    inner join mst_user_belongings as z ';
        $sql.= '      on z.mst_user_id = ' . $this->request->data['TrnConsume']['mst_user_id'];
        $sql.= '      and z.mst_facility_id = k.id  ';
        $sql.= '      and z.is_deleted = false  ';

        $sql .=' where 1 = 1 ';
        $sql .=' and ( h2.receiving_type <> ' . Configure::read('ReceivingType.repay') . ' or h2.receiving_type is null ) ';
        $sql .=$where;
        $sql .='   group by ';
        $sql .='    a.id ';
        $sql .='    , a.trade_type ';
        $sql .='    , a.use_type ';
        $sql .='    , a.facility_sticker_no ';
        $sql .='    , a.hospital_sticker_no ';
        $sql .='    , c.facility_sticker_no ';
        $sql .='    , c.hospital_sticker_no ';
        $sql .='    , b.work_no ';
        $sql .='    , b.work_date ';
        $sql .='    , e.internal_code ';
        $sql .='    , e.item_name ';
        $sql .='    , e.item_code ';
        $sql .='    , e.standard ';
        $sql .='    , e.tax_type ';
        $sql .='    , i.dealer_name ';
        $sql .='    , k.facility_name ';
        $sql .='    , k.facility_formal_name ';
        $sql .='    , k.gross ';
        $sql .='    , k.round ';
        $sql .='    , j.id ';
        $sql .='    , j.department_name ';
        $sql .='    , j.department_formal_name ';
        $sql .='    , j.department_code ';
        $sql .='    , a.use_type ';
        $sql .='    , a.recital ';
        $sql .='    , c.lot_no ';
        $sql .='    , c.validated_date ';
        $sql .='    , d.per_unit ';
        $sql .='    , d1.unit_name ';
        $sql .='    , d2.unit_name ';
        $sql .='    , f.user_name ';
        $sql .='    , g.name ';
        $sql .='    , c.trade_type ';
        $sql .='    , a.stocking_close_type ';
        $sql .='    , a.sales_close_type ';
        $sql .='    , a.facility_close_type ';
        $sql .='    , a.is_deleted ';
        $sql .='    , a.quantity ';
        $sql .='    , x.id ';
        $sql .='    , y.updatable ';

        if(is_null($sort)){
            
            $sql .=' order by ';
            $sql .="       k.facility_name ,  j.department_name , ";
            $sql .='      ( case coalesce(a.trade_type , c.trade_type ) ';
            $sql .='         when 4 then 2 ';
            $sql .='         when 6 then 2 ';
            $sql .='         when 10 then 2 ';
            $sql .='         else 1 ';
            $sql .='          end  )';
            $sql .='      , a.work_seq ';
        } else {
            $sql .= $sort;
        }
        $ret = $this->TrnConsume->query($sql);

        return array($ret , $optimized);
    }

    /*
     * 定数切替品処理
     */
    private function switchItemUnit($department_id , $item_unit_id){
        //部署IDと、包装単位IDから定数設定を取得
        $sql  = ' select ';
        $sql .= '       a.id                                    as "MstFixedCount__id" ';
        $sql .= '     , a.fixed_count                           as "MstFixedCount__fixed_count" ';
        $sql .= '     , a.substitute_unit_id                    as "MstFixedCount__substitute_unit_id" ';
        $sql .= '     , b.id                                    as "MstFixedCountTo__id" ';
        $sql .= '     , b.fixed_count                           as "MstFixedCountTo__fixed_count" ';
        $sql .= '     , d.stock_count + ( case when c.center_stock_flg = true then y.stock_count  else 0 end ) as "MstFixedCount__stock_count" ';

        $sql .='      , (  ';
        $sql .='       case  ';
        $sql .='         when c.center_stock_flg = true  ';
        $sql .='         then (a.fixed_count - d.stock_count ) ';
        $sql .='         else 1 ';
        $sql .='       end ';
        $sql .='       )                                        as "MstFixedCount__diff_count" ';

        $sql .='      , (  ';
        $sql .='        case  ';
        $sql .='          when c.center_stock_flg = true  ';
        $sql .='          then (  ';
        $sql .='            case  ';
        $sql .='              when ( y.stock_count - x.insufficiency_count ) < 0 '; //倉庫在庫に余裕が無い
        $sql .='               and ( a.fixed_count - d.stock_count ) >  0 '; // 病院在庫が、定数より少ない。
        $sql .='              then true  ';
        $sql .='              else false  ';
        $sql .='              end ';
        $sql .='          )  ';
        $sql .='          else (  ';
        $sql .='            case  ';
        $sql .='              when a.fixed_count > d.stock_count  '; // 部署の定数 より 在庫が少なかったら、処理ON
        $sql .='              then true  ';
        $sql .='              else false  ';
        $sql .='              end ';
        $sql .='          )  ';
        $sql .='          end ';
        $sql .='      )                                         as "MstFixedCount__check"  ';

        $sql .= '   from ';
        $sql .= '     mst_fixed_counts as a  ';
        $sql .= '     inner join mst_fixed_counts as b  ';
        $sql .= '       on a.substitute_unit_id = b.id  ';
        $sql .= '     inner join trn_switch_history_headers as c  ';
        $sql .= '       on c.fixed_count_id_from = a.id  ';
        $sql .= '       and c.end_date is null  ';
        $sql .= '       and c.is_deleted = false  ';
        $sql .= "       and c.start_date <= '" . $this->request->data['TrnConsumeHeader']['work_date'] . "'";
        $sql .= '     left join ';
        if($this->Session->read('Auth.Config.StockType') == 0 ){
            $sql .= '           ( select ';
            $sql .= '             coalesce(sum(a2.quantity),0) as stock_count , ';
            $sql .= '             a1.reserve_count , ';
            $sql .= '             a1.promise_count , ';
            $sql .= '             a1.mst_department_id , ';
            $sql .= '             a1.mst_item_unit_id ';
            $sql .= '              from trn_stocks as a1 ';
            $sql .= '             left join trn_stickers as a2 ';
            $sql .= '             on a1.mst_item_unit_id = a2.mst_item_unit_id  ';
            $sql .= '            and a1.mst_department_id = a2.mst_department_id  ';
            $sql .= '            and a2.is_deleted = false ';
            $sql .= '            and a2.quantity > 0 ';
            $sql .= "            and ( a2.facility_sticker_no != '' or a2.hospital_sticker_no != '' ) ";
            $sql .= '            and a2.trade_type not in ( ' . Configure::read('ClassesType.Temporary') . ' , ' . Configure::read('ClassesType.ExShipping') . ' , ' . Configure::read('ClassesType.ExNoStock') . ' )';
            $sql .= '            group by a1.reserve_count , a1.promise_count ,a1.mst_department_id , a1.mst_item_unit_id ';
            $sql .= '           ) as d ';
        } else {
            $sql .= '           trn_stocks as d';
        }
        $sql .= '       on d.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '      and d.mst_department_id = a.mst_department_id  ';
        $sql .= '     left join (  ';
        $sql .= '       select ';
        $sql .= '             a.mst_item_unit_id ';
        $sql .= '           , sum(  ';
        $sql .= '               case  ';
        $sql .= '               when (a.fixed_count - b.stock_count - b.requested_count) < 0  ';
        $sql .= '               then 0  ';
        $sql .= '               else (a.fixed_count - b.stock_count - b.requested_count)  ';
        $sql .= '               end ';
        $sql .= '             )  as insufficiency_count  ';
        $sql .= '         from ';
        $sql .= '           mst_fixed_counts as a  ';
        $sql .= '           left join ';
        if($this->Session->read('Auth.Config.StockType') == 0 ){
            $sql .= '            ( select ';
            $sql .= '             coalesce(sum(a2.quantity),0) as stock_count , ';
            $sql .= '             a1.reserve_count , ';
            $sql .= '             a1.promise_count , ';
            $sql .= '             a1.requested_count ,';
            $sql .= '             a1.mst_department_id , ';
            $sql .= '             a1.mst_item_unit_id ';
            $sql .= '              from trn_stocks as a1 ';
            $sql .= '             left join trn_stickers as a2 ';
            $sql .= '             on a1.mst_item_unit_id = a2.mst_item_unit_id  ';
            $sql .= '            and a1.mst_department_id = a2.mst_department_id  ';
            $sql .= '            and a2.is_deleted = false ';
            $sql .= '            and a2.quantity > 0 ';
            $sql .= "            and ( a2.facility_sticker_no != '' or a2.hospital_sticker_no != '' ) ";
            $sql .= '            and a2.trade_type not in ( ' . Configure::read('ClassesType.Temporary') . ' , ' . Configure::read('ClassesType.ExShipping') . ' , ' . Configure::read('ClassesType.ExNoStock') . ' )';
            $sql .= '            group by a1.reserve_count , a1.promise_count ,a1.requested_count ,a1.mst_department_id , a1.mst_item_unit_id ';
            $sql .= '           ) as b ';
        } else {
            $sql .= '           trn_stocks as b';
        }
        $sql .= '             on b.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '            and b.mst_department_id = a.mst_department_id  ';
        $sql .= '           left join mst_departments as c  ';
        $sql .= '             on c.id = a.mst_department_id  ';
        $sql .= '         where ';
        $sql .= '           c.department_type = ' . Configure::read('DepartmentType.hospital');
        $sql .= '           and a.mst_item_unit_id = ' . $item_unit_id;
        $sql .= '           and a.is_deleted = false ';
        $sql .= "           and a.start_date <= '" . $this->request->data['TrnConsumeHeader']['work_date'] . "'";
        $sql .= '         group by ';
        $sql .= '           a.mst_item_unit_id ';
        $sql .= '     ) as x  '; // 病院部署の不足合計
        $sql .= '       on x.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '     left join (  ';
        $sql .= '       select ';
        $sql .= '        (  ';
        $sql .= '         case  ';
        $sql .= '           when ( a.stock_count - a.reserve_count - a.promise_count ) < 0  ';
        $sql .= '           then 0  ';
        $sql .= '           else ( a.stock_count - a.reserve_count - a.promise_count )  ';
        $sql .= '         end ';
        $sql .= '        )    as stock_count ';
        $sql .= '           , a.mst_item_unit_id  ';
        $sql .= '         from ';
        if($this->Session->read('Auth.Config.StockType') == 0 ){
            // 臨時在庫を判定する
            $sql .= '           ( select ';
            $sql .= '             coalesce(sum(a2.quantity),0) as stock_count , ';
            $sql .= '             a1.reserve_count , ';
            $sql .= '             a1.promise_count , ';
            $sql .= '             a1.mst_department_id , ';
            $sql .= '             a1.mst_item_unit_id ';
            $sql .= '              from trn_stocks as a1 ';
            $sql .= '             left join trn_stickers as a2 ';
            $sql .= '             on a1.mst_item_unit_id = a2.mst_item_unit_id  ';
            $sql .= '            and a1.mst_department_id = a2.mst_department_id  ';
            $sql .= '            and a2.is_deleted = false ';
            $sql .= '            and a2.quantity > 0 ';
            $sql .= "            and ( a2.facility_sticker_no != '' or a2.hospital_sticker_no != '' ) ";
            $sql .= '            and a2.trade_type not in ( ' . Configure::read('ClassesType.Temporary') . ' , ' . Configure::read('ClassesType.ExShipping') . ' , ' . Configure::read('ClassesType.ExNoStock') . ' )';
            $sql .= '            group by a1.reserve_count , a1.promise_count ,a1.mst_department_id , a1.mst_item_unit_id ';
            $sql .= '           ) as a ';
        }else{
            // 臨時在庫を判定しない
            $sql .= '           trn_stocks as a  ';
        }
        $sql .= '           left join mst_departments as b  ';
        $sql .= '             on b.id = a.mst_department_id  ';
        $sql .= '         where ';
        $sql .= '           b.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '           and a.mst_item_unit_id = ' . $item_unit_id;
        $sql .= '     ) as y  ';  //センターの引当可能数
        $sql .= '       on y.mst_item_unit_id = x.mst_item_unit_id  ';
        $sql .= '   where ';
        $sql .= '     a.mst_department_id = ' . $department_id;
        $sql .= '     and a.mst_item_unit_id = ' . $item_unit_id;
        $sql .= '     and a.is_deleted = false ';

        $tmp = $this->MstFixedCount->query($sql);
        $fixedCount = (isset($tmp[0])?$tmp[0]:null);

        if( !is_null($fixedCount) && //定数設定が取得できなければスキップ
            $fixedCount['MstFixedCount']['check']  //チェックフラグがOFFの場合スキップ
            ){

            //差分が切替元の定数を超えていた場合、数量を調整
            if($fixedCount['MstFixedCount']['diff_count'] > $fixedCount['MstFixedCount']['fixed_count']){
                $fixedCount['MstFixedCount']['diff_count'] = $fixedCount['MstFixedCount']['fixed_count'];
            }

            //定数切替元更新
            if (!$this->MstFixedCount->updateAll(array('MstFixedCount.fixed_count'         => 'MstFixedCount.fixed_count - ' . $fixedCount['MstFixedCount']['diff_count'],
                                                       'MstFixedCount.holiday_fixed_count' => 'MstFixedCount.holiday_fixed_count - ' . $fixedCount['MstFixedCount']['diff_count'],
                                                       'MstFixedCount.spare_fixed_count'   => 'MstFixedCount.spare_fixed_count - ' . $fixedCount['MstFixedCount']['diff_count'],
                                                       'MstFixedCount.modifier'            => $this->Session->read('Auth.MstUser.id'),
                                                       'MstFixedCount.modified'            => "'" . $this->request->data['now'] . "'",
                                                       //在庫数量が0になる場合(=>使い切ったので)、代替包装単位をNULLにする。
                                                       'MstFixedCount.substitute_unit_id' => (($fixedCount['MstFixedCount']['stock_count']==0)?null:$fixedCount['MstFixedCount']['substitute_unit_id'])
                                                       )
                                                 , array(
                                                     'MstFixedCount.id' => $fixedCount['MstFixedCount']['id'],
                                                     )
                                                 ,
                                                 -1
                                                 )
                ) {
                return false;
            }
            //定数切替先更新
            if (!$this->MstFixedCount->updateAll(array('MstFixedCount.fixed_count'         => 'MstFixedCount.fixed_count + ' . $fixedCount['MstFixedCount']['diff_count'],
                                                       'MstFixedCount.holiday_fixed_count' => 'MstFixedCount.holiday_fixed_count + ' . $fixedCount['MstFixedCount']['diff_count'],
                                                       'MstFixedCount.spare_fixed_count'   => 'MstFixedCount.spare_fixed_count + ' . $fixedCount['MstFixedCount']['diff_count'],
                                                       'MstFixedCount.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                       'MstFixedCount.modified'    => "'" . $this->request->data['now'] . "'"
                                                       )
                                                 , array(
                                                     'MstFixedCount.id' => $fixedCount['MstFixedCountTo']['id'],
                                                     )
                                                 ,
                                                 -1
                                                 )
                ) {
                return false;
            }
            //切替先、切替元で定数切替履歴ヘッダを取得
            $switchHeader = $this->TrnSwitchHistoryHeader->find('first',
                                                                array(
                                                                    'conditions'=> array(
                                                                        'TrnSwitchHistoryHeader.fixed_count_id_from' => $fixedCount['MstFixedCount']['id'],
                                                                        'TrnSwitchHistoryHeader.fixed_count_id_to'   => $fixedCount['MstFixedCountTo']['id'],
                                                                        'TrnSwitchHistoryHeader.is_deleted'          => false
                                                                        ),
                                                                    'fields' => array('TrnSwitchHistoryHeader.id'
                                                                        ),
                                                                    'recursive' => -1,
                                                                    )
                                                                );
            //ヘッダ情報が取得できない場合エラーで返す
            if(!$switchHeader){
                return false;
            }

            //定数切替明細追加
            $switch_data = array();
            $switch_data['TrnSwitchHistory']['trn_switch_history_header_id'] = $switchHeader['TrnSwitchHistoryHeader']['id'];
            $switch_data['TrnSwitchHistory']['trn_consume_id']               = $this->TrnConsume->getLastInsertId();
            $switch_data['TrnSwitchHistory']['fixed_count_from']             = $fixedCount['MstFixedCount']['fixed_count']-$fixedCount['MstFixedCount']['diff_count'];
            $switch_data['TrnSwitchHistory']['fixed_count_to']               = $fixedCount['MstFixedCountTo']['fixed_count']+$fixedCount['MstFixedCount']['diff_count'];
            $switch_data['TrnSwitchHistory']['type']                         = Configure::read('SwitchType.consume');
            $switch_data['TrnSwitchHistory']['is_deleted']                   = false;
            $switch_data['TrnSwitchHistory']['creater']                      = $this->Session->read('Auth.MstUser.id');
            $switch_data['TrnSwitchHistory']['created']                      = $this->request->data['now'];
            $switch_data['TrnSwitchHistory']['modifier']                     = $this->Session->read('Auth.MstUser.id');
            $switch_data['TrnSwitchHistory']['modified']                     = $this->request->data['now'];

            $this->TrnSwitchHistory->create();
            if(!$this->TrnSwitchHistory->save($switch_data)){
                return false;
            }

            //切替元が0になったら、ヘッダに完了を入れる
            if($fixedCount['MstFixedCount']['fixed_count'] - $fixedCount['MstFixedCount']['diff_count'] == 0) {
                if (!$this->TrnSwitchHistoryHeader->updateAll( array('TrnSwitchHistoryHeader.end_date'  => "'" . $this->request->data['TrnConsumeHeader']['work_date'] . "'",
                                                                     'TrnSwitchHistoryHeader.modifier'  => $this->Session->read('Auth.MstUser.id'),
                                                                     'TrnSwitchHistoryHeader.modified'  => "'" . $this->request->data['now'] . "'"
                                                                     )
                                                               , array(
                                                                   'TrnSwitchHistoryHeader.id' => $switchHeader['TrnSwitchHistoryHeader']['id']
                                                                   )
                                                               ,
                                                               -1
                                                               )
                    ) {
                    return false;
                }
                //切替元商品の定数の適用をはずす
                if (!$this->MstFixedCount->updateAll( array('MstFixedCount.is_deleted'  => "'true'",
                                                                     )
                                                               , array(
                                                                   'MstFixedCount.id' => $fixedCount['MstFixedCount']['id']
                                                                   )
                                                               ,
                                                               -1
                                                               )
                    ) {
                    return false;
                }
            }
        }
        return true;
    }

    function getItemData($where) {
        //商品情報取得SQL
        $sql  = ' select ';
        $sql .= '       a.id                  as "TrnSticker__id"';
        $sql .= '     , a.facility_sticker_no as "TrnSticker__facility_sticker_no"';
        $sql .= '     , a.hospital_sticker_no as "TrnSticker__hospital_sticker_no"';
        $sql .= '     , a.quantity            as "TrnSticker__quantity"';
        $sql .= "     , to_char(a.validated_date,'YYYY/mm/dd') ";
        $sql .= '                             as "TrnSticker__validated_date"';
        $sql .= '     , a.lot_no              as "TrnSticker__lot_no"';
        $sql .= '     , a.is_deleted          as "TrnSticker__is_deleted"';
        $sql .= '     , a.has_reserved        as "TrnSticker__has_reserved"';
        $sql .= '     , a.trade_type          as "TrnSticker__trade_type"';
        $sql .= '     , a.modified            as "TrnSticker__modified"';
        $sql .= '     , a.mst_department_id   as "TrnSticker__mst_department_id"';
        $sql .= '     , a.mst_item_unit_id    as "TrnSticker__mst_item_unit_id"';
        $sql .= '     , a.trn_storage_id      as "TrnSticker__trn_storage_id"';
        $sql .= '     , a.trn_receiving_id    as "TrnSticker__trn_receiving_id"';
        $sql .= '     , a.trn_shipping_id     as "TrnSticker__trn_shipping_id"';
        $sql .= '     , a.sale_claim_id       as "TrnSticker__sale_claim_id"';
        $sql .= '     , a.buy_claim_id        as "TrnSticker__buy_claim_id"';
        $sql .= '     , a.trn_move_header_id  as "TrnSticker__trn_move_header_id"';
        $sql .= '     , a.sales_flg           as "TrnSticker__sales_flg"';
        $sql .= '     , a.type_name           as "TrnSticker__type_name"';
        $sql .= '     , a.class_name          as "TrnSticker__class_name"';
        $sql .= '     , j.facility_code       as "TrnSticker__facility_code"';
        $sql .= '     , j.id                  as "TrnSticker__facility_id"';
        $sql .= "     , to_char(q.work_date,'YYYY/mm/dd') ";
        $sql .= '                             as "TrnConsume__work_date"';
        $sql .= '     , f.dealer_name         as "MstDealer__dealer_name"';
        $sql .= '     , ( case when b.per_unit = 1 then c.unit_name ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'";
        $sql .= '        end )                as "TrnSticker__unit_name"';
        $sql .= '     , e.internal_code       as "MstFacilityItem__internal_code"';
        $sql .= '     , e.item_name           as "MstFacilityItem__item_name"';
        $sql .= '     , e.item_code           as "MstFacilityItem__item_code"';
        $sql .= '     , e.standard            as "MstFacilityItem__standard"';
        $sql .= '     , e.buy_flg             as "MstFacilityItem__buy_flg"';
        $sql .= '     , coalesce(e.expired_date , j.days_before_expiration ) ';
        $sql .= '                             as "MstFacilityItem__expired_date"';
        $sql .= '     , h.substitute_unit_id  as "MstFixedCount__substitute_unit_id"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when e.item_type = ' . Configure::read('Items.item_types.setitem');
        $sql .= '         then (  ';
        $sql .= '           select ';
        $sql .= '                 sum(e2.sales_price)  ';
        $sql .= '             from ';
        $sql .= '               trn_stickers as a2  ';
        $sql .= '               left join trn_set_stock_headers as b2  ';
        $sql .= '                 on b2.id = a2.trn_set_stock_id  ';
        $sql .= '               left join trn_set_stocks as c2  ';
        $sql .= '                 on c2.trn_set_stock_header_id = b2.id ';
        $sql .= '               left join mst_item_units as d2 ';
        $sql .= '                 on d2.id = c2.mst_item_unit_id ';
        $sql .= '               inner join mst_sales_configs as e2 ';
        $sql .= '                 on 1 = 1 ';
        $sql .= '                 and d2.mst_facility_item_id = e2.mst_facility_item_id ';
        $sql .= '                 and e2.mst_item_unit_id = d2.id  ';
        $sql .= "                 and e2.start_date <= '" . $this->request->data['TrnConsumeHeader']['work_date']."'";
        $sql .= "                 and e2.end_date >= '" . $this->request->data['TrnConsumeHeader']['work_date']."'";
//        $sql .= '                 and e2.partner_facility_id = '. $this->request->data['TrnConsumeHeader']['facilityId'];
        $sql .= '                 and e2.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '                 and e2.is_deleted = false  ';
        $sql .= '             where ';
        $sql .= '               a2.id = a.id  ';
        $sql .= '             group by ';
        $sql .= '               a2.id ';
        $sql .= '               , b2.detail_count  ';
        $sql .= '             having ';
        $sql .= '               count(a2.id) = b2.detail_count ';
        $sql .= '         )  ';
        $sql .= '         else coalesce(a.sales_price,g.sales_price)  ';
        $sql .= '         end ';
        $sql .= '     )                       as "TrnSticker__sales_price"  ';
        $sql .= '     , p.transaction_price   as "TrnSticker__transaction_price"';
        $sql .= '     , j.gross               as "Hospital__gross" ';
        $sql .= '     , j.round               as "Hospital__round" ';
        $sql .= '     , l.id                  as "Center__mst_department_id"';
        $sql .= '     , k.gross               as "Center__gross"';
        $sql .= '     , k.round               as "Center__round"';
        $sql .= '     , coalesce(m.department_id_to,m2.id) ';
        $sql .= '                             as "Supplier__mst_department_id"';
        $sql .= '     , o.gross               as "Supplier__gross"';
        $sql .= '     , o.round               as "Supplier__round"';
        $sql .= '     , r.shipping_type       as "TrnShipping__shipping_type"';
        $sql .= '   from ';
        // シール
        $sql .= '     trn_stickers as a  ';
        // 包装単位
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on a.mst_item_unit_id = b.id  ';
        // 包装単位名
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on c.id = b.mst_unit_name_id  ';
        // 入数包装単位名
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on b.per_unit_name_id = d.id  ';
        // 施設採用品
        $sql .= '     left join mst_facility_items as e  ';
        $sql .= '       on e.id = b.mst_facility_item_id  ';
        // 販売元
        $sql .= '     left join mst_dealers as f  ';
        $sql .= '       on f.id = e.mst_dealer_id  ';
        // 売上設定
        $sql .= '     left join mst_sales_configs as g  ';
        $sql .= '       on g.mst_item_unit_id = b.id  ';
        $sql .= "       and g.start_date <= '" . $this->request->data['TrnConsumeHeader']['work_date'] ."'";
        $sql .= "       and g.end_date >= '" . $this->request->data['TrnConsumeHeader']['work_date'] ."'";
        $sql .= '       and g.mst_facility_id = '. $this->Session->read('Auth.facility_id_selected');
//        $sql .= '       and g.partner_facility_id = '. $this->request->data['TrnConsumeHeader']['facilityId'];
        $sql .= '       and g.is_deleted = false  ';
        //定数設定//切替商品判定用
        $sql .= '     left join mst_fixed_counts as h';
        $sql .= '       on h.mst_item_unit_id = b.id ';
        $sql .= '       and  h.mst_department_id = a.mst_department_id';
        //病院部署
        $sql .= '     left join mst_departments as i';
        $sql .= '       on a.mst_department_id = i.id ';
        //病院施設
        $sql .= '     left join mst_facilities as j';
        $sql .= '       on i.mst_facility_id = j.id ';
        //センター施設
        $sql .= '     left join mst_facilities as k';
        $sql .= '       on e.mst_facility_id = k.id ';
        //センター部署
        $sql .= '     left join mst_departments as l';
        $sql .= '       on k.id = l.mst_facility_id ';
        $sql .= '      and l.department_type = ' . Configure::read('DepartmentType.warehouse');
        // 発注
        $sql .= '     left join trn_orders as m';
        $sql .= '       on m.id = a.trn_order_id ';
        // 代表仕入
        $sql .= '     left join mst_transaction_mains as m1';
        $sql .= '       on m1.mst_facility_item_id = e.id ';
        // 代表仕入部署
        $sql .= '     left join mst_departments as m2';
        $sql .= '       on m2.mst_facility_id = m1.mst_facility_id ';
        
        // 仕入先部署
        $sql .= '     left join mst_departments as n';
        $sql .= '       on n.id = coalesce(m.department_id_to,m2.id) ';
        // 仕入先施設
        $sql .= '     left join mst_facilities as o';
        $sql .= '       on n.mst_facility_id = o.id ';
        // 仕入設定
        $sql .= '     left join mst_transaction_configs as p ' ;
        $sql .= '       on p.mst_item_unit_id = a.mst_item_unit_id ';
        $sql .= '      and p.partner_facility_id = o.id ';
        $sql .= '      and p.mst_facility_id = k.id ';
        $sql .= "      and p.start_date <= '" . $this->request->data['TrnConsumeHeader']['work_date'] ."'";
        $sql .= "      and p.end_date >= '" . $this->request->data['TrnConsumeHeader']['work_date'] ."'";
        $sql .= '      and p.is_deleted = false ';
        $sql .= '     left join trn_consumes as q ';
        $sql .= '       on q.id = a.trn_consume_id ';
        $sql .= '      and q.is_deleted = false ';
        // 出荷
        $sql .= '     left join trn_shippings as r ';
        $sql .= '       on r.id = a.trn_shipping_id ';
        $sql .= '   where 1 = 1';
        $sql .= '     and e.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= $where ;

        return $this->TrnSticker->query($sql);
    }
    
    /*
     * カート残情報取得
     */
    private function getCartConsumeData($where){
        // 対象部署で、読み込まれなかったシールの一覧を返す。
        $sql  = ' select ';
        $sql .= '       a.id                      as "TrnSticker__id" ';
        $sql .= '     , a.is_deleted              as "TrnSticker__is_deleted" ';
        $sql .= '     , a.quantity                as "TrnSticker__quantity" ';
        $sql .= '     , a.validated_date          as "TrnSticker__validated_date" ';
        $sql .= '     , b.mst_facility_id         as "TrnSticker__facility_id" ';
        $sql .= '     , a.has_reserved            as "TrnSticker__has_reserved" ';
        $sql .= '     , c.sales_price             as "TrnSticker__sales_price" ';
        $sql .= '     , a.trade_type              as "TrnSticker__trade_type" ';
        $sql .= '     , a.mst_department_id       as "TrnSticker__mst_department_id"  ';
        $sql .= '     , a.hospital_sticker_no     as "TrnSticker__hospital_sticker_no"  ';
        $sql .= '     , a.facility_sticker_no     as "TrnSticker__facility_sticker_no"  ';
        $sql .= '     , a.lot_no                  as "TrnSticker__lot_no"';
        $sql .= "     , to_char(a.validated_date ,'YYYY/mm/dd')";
        $sql .= '                                 as "TrnSticker__validated_date"';
        $sql .= '     , a.modified                as "TrnSticker__modified"';
        $sql .= '     , \'\'                      as "TrnSticker__recitail"';
        $sql .= '     , e.expired_date            as "MstFacilityItem__expired_date"';
        $sql .= '     , e.internal_code           as "MstFacilityItem__internal_code"';
        $sql .= '     , e.item_name               as "MstFacilityItem__item_name"';
        $sql .= '     , e.item_code               as "MstFacilityItem__item_code"';
        $sql .= '     , e.standard                as "MstFacilityItem__standard"';
        $sql .= '     , ( case ';
        $sql .= '         when d.per_unit = 1 then d1.unit_name ';
        $sql .= "         else d1.unit_name || '(' || d.per_unit || d1.unit_name || ')'";
        $sql .= '       end )                     as "TrnSticker__unit_name"';
        $sql .= '     , g.dealer_name             as "MstDealer__dealer_name"';
        $sql .= '     , f.substitute_unit_id      as "MstFixedCount__substitute_unit_id"';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_departments as b  ';
        $sql .= '       on b.id = a.mst_department_id  ';
        $sql .= '     left join mst_sales_configs as c  ';
        $sql .= '       on c.partner_facility_id = b.mst_facility_id  ';
        $sql .= '       and c.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '       and c.is_deleted = false  ';
        $sql .= '     left join mst_item_units as d ';
        $sql .= '       on d.id = a.mst_item_unit_id ';
        $sql .= '     left join mst_unit_names as d1 ';
        $sql .= '       on d1.id = d.mst_unit_name_id ';
        $sql .= '     left join mst_unit_names as d2 ';
        $sql .= '       on d2.id = d.per_unit_name_id ';
        $sql .= '     left join mst_facility_items as e ';
        $sql .= '       on e.id = d.mst_facility_item_id ';
        $sql .= '     left join mst_fixed_counts as f ';
        $sql .= '       on f.mst_item_unit_id = a.mst_item_unit_id ';
        $sql .= '       and f.mst_department_id = a.mst_department_id ';
        $sql .= '     left join mst_dealers as g ';
        $sql .= '       on g.id = e.mst_dealer_id ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.quantity > 0  ';
        $sql .= $where;
        return $this->TrnSticker->query($sql);
    }
    
    /*
     * 定数切替商品消費取消し
     */
    private function switchCancel($id){
        //消費IDから定数切替履歴を検索してデータを取得
        $sql  = ' select ';
        $sql .= '       a.*  ';
        $sql .= '     , b.fixed_count_id_from  ';
        $sql .= '     , b.fixed_count_id_to  ';
        $sql .= '   from ';
        $sql .= '     trn_switch_histories as a  ';
        $sql .= '     inner join (  ';
        $sql .= '       select ';
        $sql .= '             a.id ';
        $sql .= '           , a.trn_switch_history_header_id  ';
        $sql .= '         from ';
        $sql .= '           trn_switch_histories as a  ';
        $sql .= '         where ';
        $sql .= '           a.trn_consume_id = ' . $id;
        $sql .= '           and a.type = ' . Configure::read('SwitchType.consume');
        $sql .= '     ) as x  ';
        $sql .= '       on x.trn_switch_history_header_id = a.trn_switch_history_header_id  ';
        $sql .= '     left join trn_switch_history_headers as b';
        $sql .= '       on b.id = a.trn_switch_history_header_id ';
        $sql .= '   order by ';
        $sql .= '     a.id desc ';
        
        $ret = $this->TrnSwitchHistory->query($sql);
        
        if(count($ret) > 0){
            
            $cancel_count = 0;
            $sw = null;
            foreach($ret as $r){
                if($r[0]['type']==3){
                    //取消しだったら
                    $cancel_count++;
                }elseif($r[0]['type']==2){
                    //消費だったら
                    if($cancel_count==0){
                        //取消しカウントが0だったら
                        $sw = $r;
                        break;
                    }
                    $cancel_count--;
                }
            }
            
            //取消しレコードを定数切替履歴を追加
            $switch_data = array();
            $switch_data['TrnSwitchHistory']['trn_switch_history_header_id'] = $sw[0]['trn_switch_history_header_id'];
            $switch_data['TrnSwitchHistory']['trn_consume_id']               = $id;
            $switch_data['TrnSwitchHistory']['fixed_count_from']             = $sw[0]['fixed_count_from']+1; //1つ前の履歴の状態の数量を入れる
            $switch_data['TrnSwitchHistory']['fixed_count_to']               = $sw[0]['fixed_count_to']-1;   //1つ前の履歴の状態の数量を入れる
            $switch_data['TrnSwitchHistory']['type']                         = Configure::read('SwitchType.cancel');
            $switch_data['TrnSwitchHistory']['is_deleted']                   = false;
            $switch_data['TrnSwitchHistory']['creater']                      = $this->Session->read('Auth.MstUser.id');
            $switch_data['TrnSwitchHistory']['created']                      = $this->request->data['now'];
            $switch_data['TrnSwitchHistory']['modifier']                     = $this->Session->read('Auth.MstUser.id');
            $switch_data['TrnSwitchHistory']['modified']                     = $this->request->data['now'];
            
            $this->TrnSwitchHistory->create();
            if(!$this->TrnSwitchHistory->save($switch_data)){
                return false;
            }
            //定数切替履歴ヘッダの終了日を削除
            if (!$this->TrnSwitchHistoryHeader->updateAll(array('TrnSwitchHistoryHeader.end_date'  => null,
                                                                'TrnSwitchHistoryHeader.modifier'  => $this->Session->read('Auth.MstUser.id'),
                                                                'TrnSwitchHistoryHeader.modified'  => "'" . $this->request->data['now'] . "'"
                                                                )
                                                          ,
                                                          array(
                                                              'TrnSwitchHistoryHeader.id' => $sw[0]['trn_switch_history_header_id']
                                                              )
                                                          ,
                                                          -1
                                                          )
                ) {
                return false;
            }
            
            //定数切替元更新
            if (!$this->MstFixedCount->updateAll(array('MstFixedCount.fixed_count'         => 'MstFixedCount.fixed_count + 1',
                                                       'MstFixedCount.holiday_fixed_count' => 'MstFixedCount.holiday_fixed_count + 1',
                                                       'MstFixedCount.spare_fixed_count'   => 'MstFixedCount.spare_fixed_count + 1',
                                                       'MstFixedCount.is_deleted'          => "'false'",
                                                       'MstFixedCount.modifier'            => $this->Session->read('Auth.MstUser.id'),
                                                       'MstFixedCount.modified'            => "'" . $this->request->data['now'] . "'",
                                                       'MstFixedCount.substitute_unit_id'  => $sw[0]['fixed_count_id_to']
                                                       )
                                                 ,
                                                 array(
                                                     'MstFixedCount.id' => $ret[0][0]['fixed_count_id_from'],
                                                     )
                                                 ,
                                                 -1
                                                 )
                ) {
                return false;
            }
            //定数切替先更新
            if (!$this->MstFixedCount->updateAll(array('MstFixedCount.fixed_count'         => 'MstFixedCount.fixed_count - 1 ',
                                                       'MstFixedCount.holiday_fixed_count' => 'MstFixedCount.holiday_fixed_count -1 ',
                                                       'MstFixedCount.spare_fixed_count'   => 'MstFixedCount.spare_fixed_count -1 ',
                                                       'MstFixedCount.is_deleted'          => "'false'",
                                                       'MstFixedCount.modifier'            => $this->Session->read('Auth.MstUser.id'),
                                                       'MstFixedCount.modified'            => "'" . $this->request->data['now'] . "'"
                                                       )
                                                 ,
                                                 array(
                                                     'MstFixedCount.id' => $sw[0]['fixed_count_id_to'],
                                                     )
                                                 ,
                                                 -1
                                                 )
                ) {
                return false;
            }
        }
        return true;
    }

    /**
     * 消費履歴出力(アプテージ用)
     */
    function export_aptage($call=null) {
        $this->request->data['TrnConsumeHeader']['id'] = null;
        $redirect = 'edit_search';

        //施設が選択されていた場合、部署を取得
        if(isset($this->request->data['TrnConsume']['facilityCode']) && ($this->request->data['TrnConsume']['facilityCode'] !== "" )) {
            $departments = $this->getDepartmentsList($this->request->data['TrnConsume']['facilityCode'] , false);
        }else{
            $departments = array();
        }
        // 入力された検索条件で絞込み ココから
        $where = " and k.id in ( ( select x.partner_facility_id as id  from  mst_facility_relations as x  where x.mst_facility_id = ".$this->request->data['TrnConsume']['mst_facility_id']." union all select id  from mst_facilities as z where z.id = ".$this->request->data['TrnConsume']['mst_facility_id']." )) ";

        $where = '' ;
        if (isset($this->request->data['TrnSticker']['facility_sticker_no']) && $this->request->data['TrnSticker']['facility_sticker_no'] != '') {
            $where .= " and ( a.facility_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."'";
            $where .=    " or a.hospital_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."'";
            $where .=    " or c.facility_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."'";
            $where .=    " or c.hospital_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] ."' )";
        }
        if (isset($this->request->data['TrnConsume']['work_no']) && $this->request->data['TrnConsume']['work_no'] != '') {
            $where .= " and b.work_no like '%" . $this->request->data['TrnConsume']['work_no'] ."%'";
        }
        if (isset($this->request->data['TrnConsume']['work_date_from']) && $this->request->data['TrnConsume']['work_date_from'] != ''){
            $where .= " and b.work_date >= '".  $this->request->data['TrnConsume']['work_date_from'] ."'";
        }
        if (isset($this->request->data['TrnConsume']['work_date_to']) && $this->request->data['TrnConsume']['work_date_to'] != ''){
            $where .= " and b.work_date <= '" . $this->request->data['TrnConsume']['work_date_to'] . "'";
        }

        if (isset($this->request->data['TrnConsume']['facilityCode']) && $this->request->data['TrnConsume']['facilityCode'] != '') {
            $where .= " and k.facility_code = '" . $this->request->data['TrnConsume']['facilityCode'] ."'";
        }
        if (isset($this->request->data['TrnConsume']['departmentCode']) && $this->request->data['TrnConsume']['departmentCode'] != '') {
            $where .= " and j.department_code = '" . $this->request->data['TrnConsume']['departmentCode'] ."'";
        }
        if (isset($this->request->data['TrnSticker']['lot_no']) && $this->request->data['TrnSticker']['lot_no'] != '') {
            $where .= " and c.lot_no like '%" . $this->request->data['TrnSticker']['lot_no'] ."%'";
        }
        if (isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] != '') {
            $where .= " and e.internal_code = '" . $this->request->data['MstFacilityItem']['internal_code'] ."'";
        }
        if (isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] != '') {
            $where .= " and e.item_code like '%" . $this->request->data['MstFacilityItem']['item_code'] ."%'";
        }
        if (isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] != '') {
            $where .= " and e.item_name like '%" . $this->request->data['MstFacilityItem']['item_name'] ."%'";
        }
        if (isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] != '') {
            $where .= " and e.standard like '%" . $this->request->data['MstFacilityItem']['standard'] ."%'";
        }
        if (isset($this->request->data['MstDealer']['dealer_name']) && $this->request->data['MstDealer']['dealer_name'] != '') {
            $where .= " and i.dealer_name like '%" . $this->request->data['MstDealer']['dealer_name'] ."%'";
        }
        if (isset($this->request->data['TrnConsume']['is_deleted']) && $this->request->data['TrnConsume']['is_deleted'] == '0') {
            $where .= " and a.is_deleted = false ";
        }
        if (isset($this->request->data['TrnConsume']['is_output']) && $this->request->data['TrnConsume']['is_output'] == '1') {
            $where .= " and a.is_output = false ";
        }
        if (isset($this->request->data['TrnConsume']['work_type']) && $this->request->data['TrnConsume']['work_type'] != ''){
            $where .= " and a.work_type = '" . $this->request->data['TrnConsume']['work_type'] ."'";
        }
        if (isset($this->request->data['TrnConsume']['ownerCode']) && $this->request->data['TrnConsume']['ownerCode'] != '') {
            $where .= " and xxx.facility_code = '" . $this->request->data['TrnConsume']['ownerCode'] ."'";
        }
        if (isset($this->request->data['TrnConsume']['trade_type']) && $this->request->data['TrnConsume']['trade_type'] != '') {
            $where .= " and coalesce(a.trade_type , c.trade_type) = '" . $this->request->data['TrnConsume']['trade_type'] ."'";
        }
        if (isset($this->request->data['TrnConsume']['use_type']) && $this->request->data['TrnConsume']['use_type'] != '') {
            $where .= " and a.use_type = '" . $this->request->data['TrnConsume']['use_type'] ."'";
        }
        
        // 入力された検索条件で絞込み ココまで
        $sql  =' select ';
        $sql .="       e.spare_key5                       as customer_code";
        $sql .="     , e.spare_key6                       as sales_code";
        $sql .="     , e.spare_key2                       as aptage_code";
        $sql .="     , sum(a.quantity * d.per_unit)       as sales_quantity";
        $sql .='     , d2.internal_code                   as unit_code';
        $sql .="     , to_char(a.work_date , 'YYYYmmdd')  as work_date";

        $base  ='   from ';
        $base .='     trn_consumes as a  ';
        $base .='     left join trn_consume_headers as b  ';
        $base .='       on a.trn_consume_header_id = b.id  ';
        $base .='     left join trn_stickers as c  ';
        $base .='       on a.trn_sticker_id = c.id  ';
        $base .='     left join trn_shippings as c1 ';
        $base .='       on c1.id = c.trn_shipping_id';
        $base .='     left join mst_item_units as d  ';
        $base .='       on c.mst_item_unit_id = d.id  ';
        $base .='     left join mst_unit_names as d1  ';
        $base .='       on d1.id = d.mst_unit_name_id  ';
        $base .='     left join mst_unit_names as d2  ';
        $base .='       on d2.id = d.per_unit_name_id  ';
        $base .='     left join mst_facility_items as e  ';
        $base .='       on e.id = d.mst_facility_item_id  ';
        $base .='     left join mst_facilities as xxx  ';
        $base .='       on xxx.id = e.mst_owner_id  ';
        $base .='     left join mst_users as f  ';
        $base .='       on f.id = a.modifier  ';
        $base .='     left join mst_classes as g  ';
        $base .='       on g.id = a.work_type  ';
        $base .='     left join trn_claims as h  ';
        $base .='       on h.trn_consume_id = a.id  ';
        $base .="       and h.is_stock_or_sale = '2' ";
        $base .='     left join trn_receivings as h2  ';
        $base .='       on h2.id = h.trn_receiving_id  ';
        $base .='     left join mst_dealers as i  ';
        $base .='       on i.id = e.mst_dealer_id  ';
        $base .='     left join mst_departments as j  ';
        $base .='       on j.id = a.mst_department_id  ';
        $base .='     left join mst_facilities as k  ';
        $base .='       on k.id = j.mst_facility_id  ';
        
        $base .=' where 1 = 1 ';
        $base .=' and c.trade_type != ' . Configure::read('ClassesType.Deposit');
        $base .=" and e.spare_key2 is not null and e.spare_key2 <> ''";
        $base .=' and ( h2.receiving_type <> ' . Configure::read('ReceivingType.repay') . 'or h2.receiving_type is null ) ';

        //低レベル品、直納品、在宅直納品は除く
        $base .=' and ( c1.shipping_type <> ' . Configure::read('ShippingType.direct') . ' or c1.shipping_type is null ) ' ;
        $base .=' and e.is_lowlevel = false ';

        // 病院資産、売上済みは除く(県総すぺしゃる）
        $base .= " and ( c.class_name != '病院資産' or c.class_name is null )";
        $base .= " and c.sales_flg != true ";
        
        //薬品は、売り上げに切り替え済みなので除外
        $base .=' and e.buy_flg = false';

        $base .=$where;
        $sql .=$base;
        // 集計する
        $sql .=' group by  ';
        $sql .='       e.spare_key5 ';
        $sql .='     , e.spare_key6 ';
        $sql .='     , e.spare_key2 ';
        $sql .='     , d2.internal_code ';
        $sql .='     , a.work_date';
        
        $sql .=' order by a.work_date desc ';
        
        $output_sql = $sql;
        
        $db = new DATABASE_CONFIG();
        $dsn = sprintf("dbname=%s port=%s user=%s password=%s options='--client_encoding=%s'"
                       , $db->default['database']
                       , $db->default['port']
                       , $db->default['login']
                       , $db->default['password']
                       , $db->default['encoding']);
        if ($db->default['host']) {
            $dsn .= sprintf(" host=%s", $db->default['host']);
        }

        //大量データ取得の場合Cakeのメモリ超過でエラーになるので、
        //Cakeのクエリメソッドは使用しない
        $con = pg_connect($dsn);
        if (!$con){
            $this->Session->setFlash('DBに接続できませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
            $this->redirect($redirect);
        }
        set_time_limit(0);
        $sql = $this->split_table($sql);

        $result = pg_query($con, $sql);
        if (!$result){
            $this->Session->setFlash('DBクエリエラー。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
            pg_close($con);
            $this->redirect($redirect);
        }

        $rec_cnt = pg_num_rows($result);
        if ($rec_cnt == 0){
            $this->Session->setFlash('対象データがありませんでした。', 'growl',  array('type'=>'important'));
            $this->redirect($redirect);
        }

        $fields = pg_num_fields($result);
        $column = array();
        for($i=0; $i<$fields; $i++){
            $column[pg_field_name($result, $i)] = $i;
        }

        //ファイル名設定
        $filename = '売上_'. date('YmdHis') .  '.txt';

        //消込データのID
        $outputted_ids = array();

        //出力データひな形
        $output_base = array(
            '処理区分'     => '1',                  //1固定
            '得意先CD'     => '',
            '需要先CD'     => '',
            '売上先CD'     => '',
            '商品CD'       => '',
            'JANCD'        => '              ',    //半角スペース14桁固定
            '売上数量'     => '',
            '単位CD'       => '',
            'ロット'       => '               ', //半角スペース15桁固定
            'シリアル'     => '               ', //半角スペース15桁固定
            '有効期限'     => '        ',          //半角スペース8桁固定
            '納品書日付'   => '',
            '売上単価'     => '0000000',           //0埋7桁固定
            '原価単価'     => '0000000',           //0埋7桁固定
            '赤納品書日付' => '        ',          //半角スペース8桁固定
            '赤売上単価'   => '0000000',
            '赤原価単価'   => '0000000',
            '元売上番号'   => '          '         //半角スペース10桁固定
        );
        
        $filename = mb_convert_encoding($filename,"SJIS","UTF-8");
        
        //HTTPヘッダ出力
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");

        //データ出力
        while($rows = pg_fetch_row($result)){
            $output = $output_base;
            $values = array_values($rows);

            //可変データ
            $output['得意先CD']   = $values[$column['customer_code']];
            $output['需要先CD']   = $values[$column['customer_code']];
            $output['売上先CD']   = $values[$column['sales_code']];
            $output['商品CD']     = $values[$column['aptage_code']];
            $output['売上数量']   = sprintf('%07d', $values[$column['sales_quantity']]);
            $output['単位CD']     = sprintf('%02d  ', $values[$column['unit_code']]);
            $output['納品書日付'] = $values[$column['work_date']];

            print implode('',$output)."\r\n";
            unset($output);
        }
        
        $this->TrnConsume->begin();
        
        /* 出力済みフラグを立てる */
        $sql  =' update trn_consumes set is_output = true ';
        $sql .='                        , modifier = ' . $this->Session->read('Auth.MstUser.id') ; 
        $sql .='                        , modified = now() '; 
        $sql .=' where id in ';
        $sql .=' ( select ';
        $sql .="       a.id";
        $sql .= $base;
        $sql .=' ) ';

        $this->TrnConsume->query($sql);
        
        $this->TrnConsume->commit();
        
        pg_close($con);
        exit(0);
    }


    
    public function remain_check(){
        // 施設コードから施設IDを取得
        $this->request->data['TrnConsumeHeader']['facilityId'] = $this->getFacilityId($this->request->data['TrnConsumeHeader']['facilityCode'] ,
                                                                             Configure::read('FacilityType.hospital'));
        // 部署コードから部署IDを取得
        if($this->request->data['TrnConsumeHeader']['departmentCode']!=''){
            $this->request->data['TrnConsumeHeader']['departmentId'] = $this->getDepartmentId($this->request->data['TrnConsumeHeader']['facilityId'],
                                                                                     Configure::read('DepartmentType.hospital'),
                                                                                     $this->request->data['TrnConsumeHeader']['departmentCode']
                                                                                     );
        }
    }

    public function remain_confirm(){
        
        $_codes = array();
        //CSV読み込み
        if(isset($_FILES['data']) && !empty($_FILES['data'])) {
            $facilityCode = $this->request->data['TrnConsumeHeader']['facilityCode'] ;
            $departmentCode = $this->request->data['TrnConsumeHeader']['departmentCode'] ;
            $consumeDate = $this->request->data['TrnConsumeHeader']['work_date'] ;
            
            // アップロードされたファイル名を設定
            $this->CsvReadUtils->setFileName( $this->request->data['upload_csv']['tmp_name'] );

            // CsvReadUtils初期設定
            $this->CsvReadUtils->setLineMaxLength( 2000 );

            // CSVオープン
            $this->CsvReadUtils->open();

            // ハッシュキー設定
            $this->CsvReadUtils->hashKeyClear();

            $this->CsvReadUtils->hashKeyAdd( "input_day" );
            $this->CsvReadUtils->hashKeyAdd( "input_time" );
            $this->CsvReadUtils->hashKeyAdd( "input_sec" );
            $this->CsvReadUtils->hashKeyAdd( "machine_id" );
            $this->CsvReadUtils->hashKeyAdd( "department_code" );
            $this->CsvReadUtils->hashKeyAdd( "sticker_no" );

            $i = 0;

            while ( ! $this->CsvReadUtils->eof() ) {
                // CSV一行読込
                $line_data = $this->CsvReadUtils->readCsvLine();
                // EOFだけの行を読み込んでしまうため
                if ( $line_data == "" ) {
                    continue;
                }

                //シール番号チェック
                if(!isset($line_data['sticker_no']) || $line_data['sticker_no'] == ''){
                    continue;
                }
                //部署チェック
                if($departmentCode == $line_data['department_code']){
                    $_codes[] = $line_data['sticker_no'];
                }
            }
            if(empty($_codes)){
                $this->Session->setFlash('ファイルが読み込めませんでした。CSVファイルを確認してください。','growl',array('type'=>'error'));
                $this->render('remain_check'); //リダイレクトするhidden値などが消えてしまうので。。
                return;
            }
        }

        // 読み込んだシール
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , a.facility_sticker_no ';
        $sql .= '     , a.hospital_sticker_no ';
        $sql .= '     , a.lot_no ';
        $sql .= "     , to_char(a.validated_date, 'YYYY/mm/dd') as validated_date ";
        $sql .= '     , d.department_name ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , c.item_name ';
        $sql .= '     , c.standard ';
        $sql .= '     , c.item_code ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then b1.unit_name  ';
        $sql .= "         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .= '         end ';
        $sql .= '     )                                         as unit_name ';
        $sql .= '     , c1.dealer_name ';
        $sql .= '     , e.sales_price ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.quantity = 0  ';
        $sql .= "         then '使用済みのシールです' ";
        $sql .= '         when a.is_deleted = true  ';
        $sql .= "         then '使用済みのシールです' ";
        $sql .= "         when d2.facility_code != '" . $facilityCode . "'  ";
        $sql .= "         then '未払出のシールです' ";
        $sql .= "         when d.department_code != '" . $departmentCode . "' ";
        $sql .= "         then d.department_name ";
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                                         as message ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.quantity = 0  ';
        $sql .= '         then 0 ';
        $sql .= '         when a.is_deleted = true  ';
        $sql .= '         then 0  ';
        $sql .= "         when d2.facility_code != '" . $facilityCode . "'  ";
        $sql .= '         then 0  ';
        $sql .= "         when d.department_code != '" . $departmentCode . "'  ";
        $sql .= '         then 1  ';
        $sql .= '         else 1  ';
        $sql .= '         end ';
        $sql .= '     )                                         as check ';
        $sql .= '  ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as c1  ';
        $sql .= '       on c1.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_departments as d  ';
        $sql .= '       on d.id = a.mst_department_id  ';
        $sql .= '     left join mst_facilities as d2';
        $sql .= '       on d2.id = d.mst_facility_id ';
        $sql .= '     left join mst_sales_configs as e  ';
        $sql .= '       on e.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '       and e.partner_facility_id = d.mst_facility_id  ';
        $sql .= "       and e.start_date <= '" . $consumeDate . "'  ";
        $sql .= "       and e.end_date > '" . $consumeDate . "'  ";
        $sql .= '       and e.is_deleted = false  ';
        $sql .= '   where ';
        $sql .= "     a.facility_sticker_no in ( '" . join("','",$_codes) . "' )  ";
        $sql .= "     or a.hospital_sticker_no in ( '" . join("','",$_codes) . "' )  ";
        
        // 部署にあって読み込んでないシール
        $sql .= ' union all  ';
        
        $sql .= ' select ';
        $sql .= '       a.id ';
        $sql .= '     , a.facility_sticker_no ';
        $sql .= '     , a.hospital_sticker_no ';
        $sql .= '     , a.lot_no ';
        $sql .= "     , to_char(a.validated_date, 'YYYY/mm/dd') as validated_date ";
        $sql .= '     , d.department_name ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , c.item_name ';
        $sql .= '     , c.standard ';
        $sql .= '     , c.item_code ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then b1.unit_name  ';
        $sql .= "         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                         as unit_name ';
        $sql .= '     , c1.dealer_name ';
        $sql .= '     , e.sales_price ';
        $sql .= "     , '' as message ";
        $sql .= '     , 2                                        as check  ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as c1  ';
        $sql .= '       on c1.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_departments as d  ';
        $sql .= '       on d.id = a.mst_department_id  ';
        $sql .= '     left join mst_sales_configs as e  ';
        $sql .= '       on e.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '       and e.partner_facility_id = d.mst_facility_id  ';
        $sql .= "       and e.start_date <= '" . $consumeDate .  "' ";
        $sql .= "       and e.end_date > '" . $consumeDate . "' ";
        $sql .= '       and e.is_deleted = false  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.quantity > 0  ';
        $sql .= "     and d.department_code = '" . $departmentCode . "' ";
        $sql .= "     and ( a.facility_sticker_no not in ( '" . join("','",$_codes) . "' )  ";
        $sql .= "     and a.hospital_sticker_no not in ( '" . join("','",$_codes) . "' )  ";
        $sql .= '     ) ';
        
        $result = $this->TrnConsume->query($sql);
        $this->set('result' , $result);
        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 1));
        
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnConsumes.token' , $token);
        $this->request->data['TrnConsumes']['token'] = $token;
        
    }
    
    public function remain_check_csv(){
        $facilityId = $this->request->data['TrnConsumeHeader']['facilityId'] ;
        $departmentId = $this->request->data['TrnConsumeHeader']['departmentId'] ;
        $consumeDate = $this->request->data['TrnConsumeHeader']['work_date'] ;
        
        $sql  = ' select ';
        $sql .= "       '[' || a.facility_sticker_no || ']'as センターシール番号";
        $sql .= "     , '[' ||a.hospital_sticker_no || ']' as 部署シール番号";
        $sql .= '     , a.lot_no as ロット番号';
        $sql .= "     , to_char(a.validated_date, 'YYYY/mm/dd') as 有効期限 ";
        $sql .= '     , d.department_name as 部署';
        $sql .= '     , c.internal_code as 商品ID';
        $sql .= '     , c.item_name as 商品名';
        $sql .= '     , c.standard as 規格';
        $sql .= '     , c.item_code as 製品番号';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then b1.unit_name  ';
        $sql .= "         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                         as 包装単位 ';
        $sql .= '     , c1.dealer_name                          as 販売元';
        $sql .= '     , e.sales_price                           as 売上単価';
        $sql .= "     , ( case ";
        $sql .= "          when a.is_deleted = true then '使用済みシールです' ";
        $sql .= "          when a.quantity = 0 then '使用済みシールです' ";
        $sql .= "          when d2.id != " . $facilityId . " then '未払出シールです' ";
        $sql .= "          when a.id in (" . join(',',$this->request->data['TrnConsume']['id']) . ") then '消費'";
        $sql .= "          when d.id != " . $departmentId . " then '別部署' ";
        $sql .= "          else '' ";
        $sql .= '       end ) as 状態 ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as c1  ';
        $sql .= '       on c1.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_departments as d  ';
        $sql .= '       on d.id = a.mst_department_id  ';
        $sql .= '     left join mst_facilities as d2';
        $sql .= '       on d2.id = d.mst_facility_id ';
        $sql .= '     left join mst_sales_configs as e  ';
        $sql .= '       on e.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '       and e.partner_facility_id = d.mst_facility_id  ';
        $sql .= "       and e.start_date <= '" . $consumeDate .  "' ";
        $sql .= "       and e.end_date > '" . $consumeDate . "' ";
        $sql .= '       and e.is_deleted = false  ';
        $sql .= '   where ';
        $sql .= '       a.id in (' . join(',',$this->request->data['TrnConsume']['report_id']) . ' )';

        $this->db_export_csv($sql , "残数チェック", 'add');
    }
    
}// EOF
