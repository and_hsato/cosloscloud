<?php
/**
 * ReceiptsHistoryController
 *　受領履歴
 * @version 1.0.0
 * @since 2010/08/23
 */
class ReceiptsHistoryController extends AppController {

    /**
     * @var $name
     */
    var $name = 'ReceiptsHistory';

    /**
     * @var array $uses
     */
    var $uses = array('MstUser',
                      'MstFacilityRelation',
                      'MstFacility',
                      'MstFacilityItem',
                      'MstDepartment',
                      'MstClass',
                      'TrnShippingHeader',
                      'TrnShipping',
                      'MstItemUnit',
                      'MstDealer',
                      'TrnSticker',
                      'TrnPromiseHeader',               //引当ヘッダ
                      'TrnPromise',                     //引当明細
                      'MstTransactionConfig',           //仕入設定
                      'MstSalesConfig',                 //売上設定
                      'TrnReceivingHeader',             //入荷ヘッダ
                      'TrnReceiving',                   //入荷明細
                      'TrnStorageHeader',               //入庫ヘッダ
                      'TrnStorage',                     //入庫明細
                      'TrnStock',                       //在庫テーブル
                      'TrnClaim',                       //請求テーブル（売上データ）
                      'MstUnitName',                    //単位名
                      'TrnConsumeHeader',               //消費ヘッダ
                      'TrnConsume',                     //消費明細
                      'TrnStickerRecord',               //シール移動履歴
                      'TrnOrder',                       //発注
                      'TrnOrderHeader',                 //発注ヘッダ
                      );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','CsvWriteUtils','Stickers');

    //ログインなしでの印刷を許可
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    /**
     *受領履歴（検索）
     */
    function index (){
        $this->setRoleFunction(15); //受領履歴

        App::import('Sanitize');

        //仕入先データセット
        $this->set('supplier_list' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                            array(Configure::read('FacilityType.supplier'))
                                                            ));
        //部署プルダウン用データ取得
        $this->set('department_list' , $this->getHospitalDepartmentList($this->Session->read('Auth.facility_id_selected')));
        
        $result = array();
        $where = array();

        //検索ボタン押下
        if(isset($this->request->data['TrnReceiving']['is_search'])){
            //where句作成
            $where = "";
            //ユーザ入力値による検索条件の追加****************************************
            //受領日----------------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_start_date'])) && ($this->request->data['MstReceipts']['search_start_date'] != "")){
                $where .= ' and a.work_date >= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_start_date'])."' ";
            }
            if((isset($this->request->data['MstReceipts']['search_end_date'])) && ($this->request->data['MstReceipts']['search_end_date'] != "")){
                $where .= ' and a.work_date <= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_end_date'])."' ";
            }

            //受領番号(完全一致)-------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_receipts_no'])) && ($this->request->data['MstReceipts']['search_receipts_no'] != "")){
                $where .= ' and a.work_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_receipts_no'])."' ";
            }

            //取消は表示しない-------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_display_deleted'])) && ($this->request->data['MstReceipts']['search_display_deleted'] == "1")){
                $where .= ' and (a.is_deleted = FALSE  or a.is_deleted = TRUE) ';
                $where .= ' and (b.is_deleted = FALSE  or b.is_deleted = TRUE) ';
            } else {
                $where .= ' and a.is_deleted = FALSE ';
                $where .= ' and b.is_deleted = FALSE ';
            }

            //作業区分(完全一致)-------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_classId'])) && ($this->request->data['MstReceipts']['search_classId'] != "")){
                $where .= ' and b.work_type = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_classId'])."' ";
            }

            //商品名(LIKE検索)-------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_item_name'])) && ($this->request->data['MstReceipts']['search_item_name'] != "")){
                $where .= ' and g.item_name ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_item_name'])."%' ";
            }

            //商品ID(完全一致)------------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_internal_code'])) && ($this->request->data['MstReceipts']['search_internal_code'] != "")){
                $where .= ' and g.internal_code ILIKE ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_internal_code'])."' ";
            }

            //規格(LIKE検索)------------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_standard'])) && ($this->request->data['MstReceipts']['search_standard'] != "")){
                $where .= ' and g.standard ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_standard'])."%' ";
            }

            //製品番号(LIKE検索)------------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_item_code'])) && ($this->request->data['MstReceipts']['search_item_code'] != "")){
                $where .= ' and g.item_code ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_item_code'])."%' ";
            }

            //販売元(LIKE検索)------------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_dealer_name'])) && ($this->request->data['MstReceipts']['search_dealer_name'] != "")){
                $where .= ' and h.dealer_name ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_dealer_name'])."%' ";
            }

            //シール番号(完全一致)---------------------------------------------------
            //部署シールとセンターシールのOR検索
            if((isset($this->request->data['MstReceipts']['search_sticker_no'])) && ($this->request->data['MstReceipts']['search_sticker_no'] != "")){
                $where .= ' and (e.facility_sticker_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_sticker_no'])."' ";
                $where .= ' or b.facility_sticker_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_sticker_no'])."'";
                $where .= ' or e.hospital_sticker_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_sticker_no'])."'";
                $where .= ' or b.hospital_sticker_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_sticker_no'])."') ";
            }

            //ロット番号(完全一致)---------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_lot_no'])) && ($this->request->data['MstReceipts']['search_lot_no'] != "")){
                $where .= ' and e.lot_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_lot_no'])."' ";
            }

            //管理区分(完全一致)---------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_type'])) && ($this->request->data['MstReceipts']['search_type'] != "")){
                $where .= ' and i.shipping_type = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_type'])."' ";
            }

            //仕入先(完全一致)---------------------------------------------------
            if((isset($this->request->data['MstReceipts']['supplierCode'])) && ($this->request->data['MstReceipts']['supplierCode'] != "")){
                $where .= " and e3.facility_code = '" . $this->request->data['MstReceipts']['supplierCode'] ."'";
            }
            
            //部署(完全一致)---------------------------------------------------
            if((isset($this->request->data['MstReceipts']['departmentCode'])) && ($this->request->data['MstReceipts']['departmentCode'] != "")){
                $where .= " and c2.department_code = '" . $this->request->data['MstReceipts']['departmentCode'] ."'";
            }
            //データ取得
            $result = $this->getReceivingHistory($where , $this->_getLimitCount());
        } else {
            //初期データ
            $this->request->data['MstReceipts']['search_start_date'] = date('Y/m/d', strtotime('-7 day'));
            $this->request->data['MstReceipts']['search_end_date']   = date('Y/m/d');
        }

        $this->set('result', $result);
    }

    /**
     *受領履歴編集
     */
    function conf (){
        App::import('Sanitize');

        $this->Session->write('Receipt.readTime',date('Y-m-d H:i:s'));
        $ShippingList = array();//データ取得用配列

        $where = $this->createSearch_Where(); //前画面の検索条件を引き継いでwhere句作成

        $ShippingList = $this->_getReceivingHistoryDetail($where);

        //--------------------------------------------------------------------------
        $this->set('ShippingList', $ShippingList);
    }

    /**
     * 登録処理
     */
    function result () {
        //************************************************************************
        $this->request->data['now'] = date('Y/m/d H:i:s');

        //締めチェック
        $targetDateAry = array();

        $params = array();
        $params['conditions'] = array('TrnReceiving.id'=>$this->request->data['TrnReceiving']['id']);
        $params['recursive']  = -1;
        $present_c = $this->TrnReceiving->find('first',$params);
        $targetDateAry[] = $present_c['TrnReceiving']['work_date'];

        //仮締更新権限チェック
        //[選択した明細を全チェックする]
        $type = Configure::read('ClosePrivileges.update');
        if(false === $this->canUpdateAfterCloseByDateAry($type ,$targetDateAry, $this->Session->read('Auth.facility_id_selected'))){
            $this->redirect('index');
            return;
        }

        $sticker_info = $this->TrnReceiving->find('all',array('fields'=>array('id','trn_sticker_id'),'recursive' => -1,'conditions'=>array('id'=>$this->request->data['TrnReceiving']['id'])));
        //更新時間チェック
        foreach($sticker_info as $sticker){
            if(!$this->check_StickerModified($sticker['TrnReceiving']['trn_sticker_id'],$this->Session->read('Receipt.readTime'))){
                $this->Session->setFlash('処理中のデータに更新がかけられました。最初からやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('index');
            }
        }

        //トランザクションを開始
        $this->TrnReceiving->begin();
        //行ロック
        $this->TrnReceiving->query('select * from trn_receivings as a where a.id in (' . join(',' , $this->request->data['TrnReceiving']['id']) . ') for update ');

        //受領取消に必要な処理各種を実行
        if($this->ReceiptsHistory($this->request->data)){

            //全受領取消処理が終了したのでコミット
            $this->TrnReceiving->commit();
            
            //完了画面作成処理へ
            $this->result_view($this->request->data['TrnReceiving']['id']);
            //途中でエラーがあったため、ロールバック
        } else {
            $this->TrnReceiving->rollback();
            $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
            $this->redirect('index');
        }
    }

    //****************************************************************************
    //完了画面ビュー作成処理
    function result_view($id){

        App::import('Sanitize');
        //SQL用に整形
        $sql  =' select ';
        $sql .="     to_char(TrnReceiving.work_date,'YYYY/mm/dd') ";
        $sql .='                                          as "TrnReceiving__work_date"';
        $sql .='    ,TrnReceiving.work_no                 as "TrnReceiving__work_no"';
        $sql .='    ,TrnReceiving.recital                 as "TrnReceiving__recital"';

        $sql .='    ,MstClass.name                        as "TrnReceiving__class_name"';
        $sql .='    ,MstDepartment.department_name        as "TrnReceiving__department_name"';
        $sql .='    ,MstFacility.facility_name            as "TrnReceiving__facility_name"';
        $sql .='    , ( case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name ';
        $sql .="        else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')'";
        $sql .='        end )                             as "TrnReceiving__unit_name"';
        $sql .='    ,MstFacilityItem.internal_code        as "MstFacilityItem__internal_code"';
        $sql .='    ,MstFacilityItem.item_name            as "MstFacilityItem__item_name"';
        $sql .='    ,MstFacilityItem.standard             as "MstFacilityItem__standard"';
        $sql .='    ,MstFacilityItem.item_code            as "MstFacilityItem__item_code"';
        $sql .='    ,MstUser.user_name                    as "MstUser__user_name"';
        $sql .='    ,MstDealer.dealer_name                as "MstDealer__dealer_name"';
        $sql .='    ,TrnSticker.hospital_sticker_no       as "TrnSticker__hospital_sticker_no"';
        $sql .='    ,TrnSticker.facility_sticker_no       as "TrnSticker__facility_sticker_no"';
        $sql .='    , ( case TrnShipping.shipping_type  ';
        foreach(Configure::read('ShippingTypes') as $k=>$v){
            $sql .= " when $k then '$v' ";
        }
        $sql .=        ' end )                            as TrnReceiving__type_name';
        $sql .='    ,TrnSticker.lot_no                    as "TrnSticker__lot_no"';
        $sql .="    ,to_char(TrnSticker.validated_date,'YYYY/mm/dd')";
        $sql .='                                          as "TrnSticker__validated_date"';
        $sql .='    ,TrnShipping.work_no                  as "TrnShipping__work_no"';
        $sql .=' from ';
        $sql .='     trn_receivings as TrnReceiving ';
        $sql .='     inner join mst_item_units as MstItemUnit on TrnReceiving.mst_item_unit_id = MstItemUnit.id ';
        $sql .='     inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ';
        $sql .='     inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id ';
        $sql .='     inner join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id ';
        $sql .='     inner join mst_users as MstUser on TrnReceiving.modifier = MstUser.id ';
        $sql .='     left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id ';
        $sql .='     inner join trn_stickers as TrnSticker on TrnReceiving.trn_sticker_id = TrnSticker.id ';
        $sql .='     inner join trn_shippings as TrnShipping on TrnReceiving.trn_shipping_id = TrnShipping.id ';
        $sql .='     left  join mst_departments as MstDepartment on MstDepartment.id = TrnReceiving.department_id_to';
        $sql .='     left  join mst_facilities as MstFacility on MstFacility.id = MstDepartment.mst_facility_id ';
        $sql .='     left  join mst_classes as MstClass on MstClass.id = TrnReceiving.work_type ';

        $sql .=      ' where TrnReceiving.id in  ('.join(',',$id) .') ';

        $ReceivingList = $this->TrnReceiving->query($sql);
        //--------------------------------------------------------------------------
        $this->set('ReceivingList', $ReceivingList);
        $this->render('result');
    }
    //****************************************************************************
    /**
     * ReceiptsHistory
     * 受領取消処理
     */
    function ReceiptsHistory($data){

        // 明細IDから必要情報を取得
        $sql  = ' select ';
        $sql .= '       a.id                      as "TrnReceiving__id" ';
        $sql .= '     , a.trn_receiving_header_id as "TrnReceiving__trn_receiving_header_id" ';
        $sql .= '     , a.work_date               as "TrnReceiving__work_date" ';
        $sql .= '     , a.quantity                as "TrnReceiving__quantity" ';
        $sql .= '     , a.mst_item_unit_id        as "TrnReceiving__mst_item_unit_id" ';
        $sql .= '     , a.department_id_from      as "TrnReceiving__department_id_from" ';
        $sql .= '     , a.department_id_to        as "TrnReceiving__department_id_to" ';
        $sql .= '     , b.id                      as "TrnSticker__id" ';
        $sql .= '     , b.trade_type              as "TrnSticker__trade_type" ';
        $sql .= '     , c.id                      as "TrnPromise__id" ';
        $sql .= '     , c.receipt_count           as "TrnPromise__receipt_count" ';
        $sql .= '     , c.unreceipt_count         as "TrnPromise__unreceipt_count" ';
        $sql .= '     , d.id                      as "TrnShipping__id" ';
        $sql .= '     , d.work_date               as "TrnShipping__work_date" ';
        $sql .= '     , d.shipping_type           as "TrnShipping__shipping_type"  ';
        $sql .= '     , coalesce(e.id,e4.id)      as "TrnSale__id"';
        $sql .= '     , e2.id                     as "TrnStoking__id"';
        $sql .= '     , e3.id                     as "TrnMsStoking__id"';
        $sql .= '     , f.id                      as "TrnConsume__id"';
        $sql .= '     , f.mst_item_unit_id        as "TrnConsume__mst_item_unit_id"';
        $sql .= '     , f.mst_department_id       as "TrnConsume__mst_department_id"';
        $sql .= '     , g.id                      as "TrnOrder__id"';
        $sql .= '     , i.buy_flg                 as "MstFacilityItem__buy_flg"';
        $sql .= '   from ';
        $sql .= '     trn_receivings as a  ';
        $sql .= '     left join trn_stickers as b  ';
        $sql .= '       on b.id = a.trn_sticker_id  ';
        $sql .= '     left join trn_promises as c  ';
        $sql .= '       on c.id = b.trn_promise_id  ';
        $sql .= '     left join trn_shippings as d  ';
        $sql .= '       on d.id = a.trn_shipping_id  ';
        $sql .= '     left join trn_claims as e ';
        $sql .= '       on e.trn_receiving_id = b.receipt_id ';
        $sql .= '       and e.is_deleted = false ';
        $sql .= "       and e.is_stock_or_sale = '2'";
        $sql .= '     left join trn_claims as e2 '; // 仕入：非在庫品のみ
        $sql .= '       on e2.id = b.buy_claim_id ';
        $sql .= '       and e2.is_deleted = false ';
        $sql .= "       and e2.is_stock_or_sale = '1'";
        $sql .= '     left join trn_claims as e3 '; // 仕入：非在庫品のみ
        $sql .= '       on e3.trn_receiving_id = b.trn_receiving_id ';
        $sql .= '       and e3.is_deleted = false ';
        $sql .= "       and e3.is_stock_or_sale = '3'";
        $sql .= '       and e3.hospital_sticker_no = b.hospital_sticker_no ';
        $sql .= '     left join trn_claims as e4 ';
        $sql .= '       on e4.is_deleted = false ';
        $sql .= "      and e4.is_stock_or_sale = '2' ";
        $sql .= '      and e4.hospital_sticker_no = b.hospital_sticker_no ';
        $sql .= '     left join trn_consumes as f ';
        $sql .= '       on f.id = b.trn_consume_id ';
        $sql .= '     left join trn_orders as g ';
        $sql .= '       on g.id = b.trn_order_id';
        $sql .= '     left join mst_item_units as h ';
        $sql .= '       on h.id = a.mst_item_unit_id';
        $sql .= '     left join mst_facility_items as i ';
        $sql .= '       on i.id = h.mst_facility_item_id';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',',$data['TrnReceiving']['id']) . ') ';
        $ret = $this->TrnReceiving->query($sql);
        foreach($ret as $r){
            //預託品、非在庫品以外： 要求レコード:受領数▽、未受領数△
            if($r['TrnShipping']['shipping_type'] != Configure::read('ShippingType.deposit')
               && $r['TrnShipping']['shipping_type'] != Configure::read('ShippingType.nostock')
               && $r['TrnShipping']['shipping_type'] != Configure::read('ShippingType.edidirect')
               ){
                $this->TrnPromise->create();
                $ret = $this->TrnPromise->updateAll(
                    array(
                        'TrnPromise.receipt_count'   => 'receipt_count - ' . $r['TrnReceiving']['quantity'] ,
                        'TrnPromise.unreceipt_count' => 'unreceipt_count + ' . $r['TrnReceiving']['quantity'] ,
                        'TrnPromise.modifier'        => $this->Session->read('Auth.MstUser.id'),
                        'TrnPromise.modified'        => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnPromise.id' => $r['TrnPromise']['id']
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }
            }else if($r['TrnShipping']['shipping_type'] == Configure::read('ShippingType.deposit')
               || $r['TrnShipping']['shipping_type'] == Configure::read('ShippingType.nostock')
               ){
                //預託品、非在庫品：発注明細の残数△
                $this->TrnOrder->create();
                $ret = $this->TrnOrder->updateAll(
                    array(
                        'TrnOrder.remain_count' => 'remain_count + ' . $r['TrnReceiving']['quantity'] ,
                        'TrnOrder.modifier'     => $this->Session->read('Auth.MstUser.id'),
                        'TrnOrder.modified'     => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnOrder.id' => $r['TrnOrder']['id']
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }
            }
            // 売上があがっている場合、削除（臨時品、準定数品）
            if(!empty($r['TrnSale']['id']) ){
                $this->TrnClaim->create();
                $ret = $this->TrnClaim->updateAll(
                    array(
                        'TrnClaim.is_deleted' => "'".TRUE."'" ,
                        'TrnClaim.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'TrnClaim.modified'   => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnClaim.id' => $r['TrnSale']['id']
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }
                // シールの売上参照IDを削除
                $this->TrnSticker->create();
                $ret = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.sale_claim_id' => NULL ,//売上請求参照キー
                        'TrnSticker.modifier'      => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'      => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnSticker.id' => $r['TrnSticker']['id']
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }
            }

            // 非在庫品、業者直納品の場合、仕入を削除
            if(( $r['TrnShipping']['shipping_type'] == Configure::read('ShippingType.nostock') ||
                 $r['TrnShipping']['shipping_type'] == Configure::read('ShippingType.edidirect') )
               && (!empty($r['TrnStoking']['id']))
               ){
                $this->TrnClaim->create();
                $ret = $this->TrnClaim->updateAll(
                    array(
                        'TrnClaim.is_deleted' => "'".TRUE."'" ,
                        'TrnClaim.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'TrnClaim.modified'   => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnClaim.id' => $r['TrnStoking']['id']
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }
                $this->TrnClaim->create();
                $ret = $this->TrnClaim->updateAll(
                    array(
                        'TrnClaim.is_deleted' => "'".TRUE."'" ,
                        'TrnClaim.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'TrnClaim.modified'   => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnClaim.id' => $r['TrnMsStoking']['id']
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }
                
                // シールの仕入参照IDを削除
                $this->TrnSticker->create();
                $ret = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.buy_claim_id'  => NULL ,//仕入請求参照キー
                        'TrnSticker.modifier'      => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'      => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnSticker.id' => $r['TrnSticker']['id']
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }
            }

            
            //臨時品の場合、消費を削除する
            if($r['TrnSticker']['trade_type'] == Configure::read('ClassesType.Temporary') ||
               $r['TrnShipping']['shipping_type'] == Configure::read('ShippingType.edidirect') ){
                //シールの数量を増やす
                $this->TrnSticker->create();
                $ret = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.quantity' =>'quantity + ' . $r['TrnReceiving']['quantity'] ,
                        'TrnSticker.modifier' => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified' => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnSticker.id' => $r['TrnSticker']['id']
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }

                //消費レコードを削除
                $this->TrnConsume->create();
                $ret = $this->TrnConsume->updateAll(
                    array(
                        'TrnConsume.is_deleted' => "'".TRUE."'" ,
                        'TrnConsume.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'TrnConsume.modified'   => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnConsume.id' => $r['TrnConsume']['id']
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }
                //病院在庫を増やす
                $this->TrnStock->create();
                $ret = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.stock_count' => 'stock_count + ' . $r['TrnReceiving']['quantity'],
                        'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'    => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnStock.mst_item_unit_id'  => $r['TrnConsume']['mst_item_unit_id'] ,
                        'TrnStock.mst_department_id' => $r['TrnConsume']['mst_department_id'] ,
                        'TrnStock.is_deleted'        => 'false'
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }
            }

            //入庫レコードを削除
            $this->TrnStorage->create();
            $ret = $this->TrnStorage->updateAll(
                array(
                    'TrnStorage.is_deleted' => "'".TRUE."'" ,
                    'TrnStorage.modifier'   => $this->Session->read('Auth.MstUser.id'),
                    'TrnStorage.modified'   => "'" . $data['now'] . "'"
                    ),
                array(
                    'TrnStorage.trn_receiving_id' => $r['TrnReceiving']['id']
                    ),
                -1
                );
            if(!$ret){
                return false;
            }

            //病院在庫：在庫を減らす
            $this->TrnStock->create();
            $ret = $this->TrnStock->updateAll(
                array(
                    'TrnStock.stock_count' => 'stock_count - ' . $r['TrnReceiving']['quantity'],
                    'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                    'TrnStock.modified'    => "'" . $data['now'] . "'"
                    ),
                array(
                    'TrnStock.mst_item_unit_id'  => $r['TrnReceiving']['mst_item_unit_id'] ,
                    'TrnStock.mst_department_id' => $r['TrnReceiving']['department_id_to'] ,
                    'TrnStock.is_deleted'        => 'false'
                    ),
                -1
                );
            if(!$ret){
                return false;
            }
            //病院在庫：未入荷数を増やす（臨時品、業者直納品以外）
            if($r['TrnSticker']['trade_type'] != Configure::read('ClassesType.Temporary') &&
               $r['TrnSticker']['trade_type'] != Configure::read('ClassesType.ExNoStock') &&
               ($r['TrnSticker']['trade_type'] != Configure::read('ClassesType.PayDirectly') &&
                $r['TrnShipping']['shipping_type'] != Configure::read('ShippingType.edidirect'))
            ){
                $this->TrnStock->create();
                $ret = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.requested_count' => 'requested_count + ' . $r['TrnReceiving']['quantity'],
                        'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'        => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnStock.mst_item_unit_id'  => $r['TrnReceiving']['mst_item_unit_id'] ,
                        'TrnStock.mst_department_id' => $r['TrnReceiving']['department_id_to'] ,
                        'TrnStock.is_deleted'        => 'false'
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }

            }
            // 預託品、非在庫品、業者直納品以外の場合
            if($r['TrnShipping']['shipping_type'] != Configure::read('ShippingType.deposit') &&
               $r['TrnShipping']['shipping_type'] != Configure::read('ShippingType.nostock') &&
               $r['TrnShipping']['shipping_type'] != Configure::read('ShippingType.edidirect')
            ){
                //センター在庫：在庫数を増やす , 予約数を増やす
                $this->TrnStock->create();
                $ret = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.stock_count'   => 'stock_count + ' . $r['TrnReceiving']['quantity'],
                        'TrnStock.reserve_count' => 'reserve_count + ' . $r['TrnReceiving']['quantity'],
                        'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'      => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnStock.mst_item_unit_id'  => $r['TrnReceiving']['mst_item_unit_id'] ,
                        'TrnStock.mst_department_id' => $r['TrnReceiving']['department_id_from'] ,
                        'TrnStock.is_deleted'        => 'false'
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }
            }

            //出荷レコードを更新
            $this->TrnShipping->create();
            $ret = $this->TrnShipping->updateAll(
                array(
                    'TrnShipping.shipping_status'  => "'".Configure::read('ShippingStatus.ordered')."'",
                    'TrnShipping.modifier'      => $this->Session->read('Auth.MstUser.id'),
                    'TrnShipping.modified'      => "'" . $data['now'] . "'"
                    ),
                array(
                    'TrnShipping.id'  => $r['TrnShipping']['id'] ,
                    ),
                -1
                );
            if(!$ret){
                return false;
            }

            //シール移動履歴の削除
            $this->TrnStickerRecord->create();
            $ret = $this->TrnStickerRecord->updateAll(
                array(
                    'TrnStickerRecord.is_deleted' => "'".TRUE."'" ,
                    'TrnStickerRecord.modifier'      => $this->Session->read('Auth.MstUser.id'),
                    'TrnStickerRecord.modified'      => "'" . $data['now'] . "'"
                    ),
                array(
                    'TrnStickerRecord.record_id'      => $r['TrnReceiving']['id'] ,
                    'TrnStickerRecord.trn_sticker_id' => $r['TrnSticker']['id'],  //シール参照キー
                    'TrnStickerRecord.move_date'      => $r['TrnReceiving']['work_date'], //移動日時(この場合入荷明細のwork_date)
                    'TrnStickerRecord.record_type'    => Configure::read('RecordType.receipts'), //シール移動履歴＠レコード区分「受領」
                    ),
                -1
                );
            if(!$ret){
                return false;
            }
                
            if($r['TrnShipping']['shipping_type'] != Configure::read('ShippingType.deposit') &&
               $r['TrnShipping']['shipping_type'] != Configure::read('ShippingType.nostock') &&
               $r['TrnShipping']['shipping_type'] != Configure::read('ShippingType.edidirect')
            ){
                //シールをセンターに移動 , 受領参照キー、消費参照キーを削除 、最終移動日を出荷日に変更 , 予約フラグをON
                $this->TrnSticker->create();
                $ret = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.mst_department_id' => $r['TrnReceiving']['department_id_from'],
                        'TrnSticker.trn_consume_id'    => NULL,//消費参照キー
                        'TrnSticker.receipt_id'        => NULL,//受領参照キー
                        'TrnSticker.last_move_date'    => "'".$r['TrnShipping']['work_date']."'",//最終移動日
                        'TrnSticker.has_reserved'      => 'TRUE',
                        'TrnSticker.modifier'          => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'          => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnSticker.id'      => $r['TrnSticker']['id'] ,
                        ),
                    -1
                    );
            }else{
                //シールをセンターに移動 , 受領参照キー、消費参照キーを削除 、最終移動日を出荷日に変更 , 削除フラグをON
                $this->TrnSticker->create();
                $ret = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.mst_department_id' => $r['TrnReceiving']['department_id_from'],
                        'TrnSticker.trn_consume_id'    => NULL,//消費参照キー
                        'TrnSticker.receipt_id'        => NULL,//受領参照キー
                        'TrnSticker.last_move_date'    => "'".$r['TrnShipping']['work_date']."'",//最終移動日
                        'TrnSticker.is_deleted'        => 'TRUE',
                        'TrnSticker.modifier'          => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'          => "'" . $data['now'] . "'"
                        ),
                    array(
                        'TrnSticker.id'      => $r['TrnSticker']['id'] ,
                        ),
                    -1
                    );
            }
            if(!$ret){
                return false;
            }

            //受領明細を削除
            $this->TrnReceiving->create();
            $ret = $this->TrnReceiving->updateAll(
                array(
                    'TrnReceiving.is_deleted' => "'".TRUE."'" ,
                    'TrnReceiving.recital'    => "'".$data['TrnReceiving']['recital'][$r['TrnReceiving']['id']]."'",
                    'TrnReceiving.modifier'   => $this->Session->read('Auth.MstUser.id'),
                    'TrnReceiving.modified'   => "'" . $data['now'] . "'"
                    ),
                array(
                    'TrnReceiving.id'      => $r['TrnReceiving']['id'] ,
                    ),
                -1
                );
            if(!$ret){
                return false;
            }
        }
        
        //全ての処理が正常に終了
        return true;
    }

    //****************************************************************************

    function check_StickerModified($sticker_id,$readtime){

        $Sticker_modified = $this->TrnSticker->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnSticker.id = ' => $sticker_id)));
        //受領確認画面に入った後に更新が行われていた場合はエラーとする
        if(strtotime($Sticker_modified['TrnSticker']['modified']) > strtotime($readtime)){
            return false;
        }
        return true;
    }

    //受領履歴検索　where句作成用
    function createSearch_Where(){
        $where = "";
        //ユーザ入力値による検索条件の追加****************************************
        //受領日----------------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_start_date'])) && ($this->request->data['MstReceipts']['search_start_date'] != "")){
            $where .= ' and TrnReceivingHeader.work_date >= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_start_date'])."' ";
        }
        if((isset($this->request->data['MstReceipts']['search_end_date'])) && ($this->request->data['MstReceipts']['search_end_date'] != "")){
            $where .= ' and TrnReceivingHeader.work_date <= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_end_date'])."' ";
        }

        //受領番号(完全一致)-------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_receipts_no'])) && ($this->request->data['MstReceipts']['search_receipts_no'] != "")){
            $where .= ' and TrnReceivingHeader.work_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_receipts_no'])."' ";
        }

        //取消は表示しない-------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_display_deleted'])) && ($this->request->data['MstReceipts']['search_display_deleted'] == "1")){
        } else {
            $where .= ' and TrnReceivingHeader.is_deleted = FALSE ';
            $where .= ' and TrnReceiving.is_deleted = FALSE ';
        }

        //作業区分(完全一致)-------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_classId'])) && ($this->request->data['MstReceipts']['search_classId'] != "")){
            $where .= ' and TrnReceiving.work_type = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_classId'])."' ";
        }

        //商品名(LIKE検索)-------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_item_name'])) && ($this->request->data['MstReceipts']['search_item_name'] != "")){
            $where .= ' and MstFacilityItem.item_name ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_item_name'])."%' ";
        }

        //商品ID(完全一致)------------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_internal_code'])) && ($this->request->data['MstReceipts']['search_internal_code'] != "")){
            $where .= ' and MstFacilityItem.internal_code ILIKE ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_internal_code'])."' ";
        }

        //規格(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_standard'])) && ($this->request->data['MstReceipts']['search_standard'] != "")){
            $where .= ' and MstFacilityItem.standard ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_standard'])."%' ";
        }

        //製品番号(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_item_code'])) && ($this->request->data['MstReceipts']['search_item_code'] != "")){
            $where .= ' and MstFacilityItem.item_code ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_item_code'])."%' ";
        }

        //販売元(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_dealer_name'])) && ($this->request->data['MstReceipts']['search_dealer_name'] != "")){
            $where .= ' and MstDealer.dealer_name ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_dealer_name'])."%' ";
        }

        //シール番号(完全一致)---------------------------------------------------
        //部署シールとセンターシールのOR検索
        if((isset($this->request->data['MstReceipts']['search_sticker_no'])) && ($this->request->data['MstReceipts']['search_sticker_no'] != "")){
            $where .= ' and (TrnSticker.facility_sticker_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_sticker_no'])."' ";
            $where .= ' or TrnReceiving.facility_sticker_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_sticker_no'])."'";
            $where .= ' or TrnSticker.hospital_sticker_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_sticker_no'])."'";
            $where .= ' or TrnReceiving.hospital_sticker_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_sticker_no'])."') ";
        }

        //ロット番号(完全一致)---------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_lot_no'])) && ($this->request->data['MstReceipts']['search_lot_no'] != "")){
            $where .= ' and TrnSticker.lot_no ILIKE ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_lot_no'])."' ";
        }

        //管理区分(完全一致)---------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_type'])) && ($this->request->data['MstReceipts']['search_type'] != "")){
            $where .= ' and TrnShipping.shipping_type = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_type'])."' ";
        }

        //施設(完全一致)---------------------------------------------------
        if((isset($this->request->data['MstReceipts']['facilityCode'])) && ($this->request->data['MstReceipts']['facilityCode'] != "")){
            $where .= " and MstFacility.facility_code = '".Sanitize::escape($this->request->data['MstReceipts']['facilityCode'])."' ";
        }
        //部署(完全一致)---------------------------------------------------
        if((isset($this->request->data['MstReceipts']['departmentCode'])) && ($this->request->data['MstReceipts']['departmentCode'] != "")){
            $where .= " and MstDepartment.department_code = '".Sanitize::escape($this->request->data['MstReceipts']['departmentCode'])."' ";
        }

        return $where;
    }


    function getReceivingHistory($where , $limit=null){
        $sql  = ' select ';
        $sql .= '       a.id                                       as "TrnReceivingHeader__id"';
        $sql .= '     , a.work_no                                  as "TrnReceivingHeader__work_no"';
        $sql .= '     , to_char(a.work_date, \'YYYY/mm/dd\')       as "TrnReceivingHeader__work_date"';
        $sql .= '     , a.is_deleted                               as "TrnReceivingHeader__is_deleted"';
        $sql .= '     , a.department_id_to                         as "TrnReceivingHeader__department_id_to"';
        $sql .= '     , a.recital                                  as "TrnReceivingHeader__recital"';
        $sql .= '     , to_char(a.created, \'YYYY/mm/dd HH24:MI\') as "TrnReceivingHeader__created"';
        $sql .= '     , a.detail_count                             as "TrnReceivingHeader__detail_count"';
        $sql .= '     , c3.facility_name                           as "TrnReceivingHeader__facility_name"';
        $sql .= '     , c2.department_name                         as "TrnReceivingHeader__department_name" ';
        $sql .= '     , e3.facility_name                           as "TrnReceivingHeader__supplier_name"';
        $sql .= '     , count(*)                                   as "TrnReceivingHeader__total_count"';
        $sql .= '     , d.user_name                                as "MstUser__user_name"';
        $sql .= '   from ';
        $sql .= '     trn_receiving_headers as a  ';
        $sql .= '     left join trn_receivings as b  ';
        $sql .= '       on b.trn_receiving_header_id = a.id  ';
        $sql .= '     left join mst_departments as c1  ';
        $sql .= '       on c1.id = a.department_id_from  ';
        $sql .= '     left join mst_facilities as c4';
        $sql .= '       on c4.id = c1.mst_facility_id ';
        $sql .= '     left join mst_departments as c2  ';
        $sql .= '       on c2.id = a.department_id_to  ';
        $sql .= '     left join mst_facilities as c3  ';
        $sql .= '       on c3.id = c2.mst_facility_id  ';
        $sql .= '     left join mst_users as d  ';
        $sql .= '       on d.id = a.creater  ';
        $sql .= '     left join trn_stickers as e  ';
        $sql .= '       on e.id = b.trn_sticker_id  ';
        $sql .= '     left join trn_orders as e1 ';
        $sql .= '       on e1.id = e.trn_order_id';
        $sql .= '     left join mst_departments as e2';
        $sql .= '       on e2.id = e1.department_id_to ';
        $sql .= '     left join mst_facilities as e3 ';
        $sql .= '       on e3.id = e2.mst_facility_id ';
        $sql .= '     left join mst_item_units as f  ';
        $sql .= '       on f.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as g  ';
        $sql .= '       on g.id = f.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as h  ';
        $sql .= '       on h.id = g.mst_dealer_id  ';
        $sql .= '     inner join mst_user_belongings as x  ';
        $sql .= '       on x.mst_facility_id = c3.id  ';
        $sql .= '       and x.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '       and x.is_deleted = false  ';
        $sql .= '     left join trn_shippings as i ';
        $sql .= '       on b.trn_shipping_id = i.id ';
        $sql .= '   where ';
        $sql .= '     a.receiving_type = ' . Configure::read('ReceivingType.receipt');
        $sql .= '     and a.receiving_status >= ' . Configure::read('ReceivingStatus.stock');
        $sql .= '     and c1.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= $where;
        $sql .= '   group by ';
        $sql .= '     a.id ';
        $sql .= '     , a.work_no ';
        $sql .= '     , a.work_date ';
        $sql .= '     , a.is_deleted ';
        $sql .= '     , a.department_id_to ';
        $sql .= '     , a.recital ';
        $sql .= '     , a.created ';
        $sql .= '     , a.detail_count ';
        $sql .= '     , c3.facility_name ';
        $sql .= '     , c2.department_name ';
        $sql .= '     , e3.facility_name';
        $sql .= '     , d.user_name ';

        $sql .= ' order by a.id desc ';

        //全件取得
        $this->set('max' , $this->getMaxCount($sql , 'TrnReceiving'));
        if(!is_null($limit)){
            $sql .= ' limit '.  $limit;
        }
        return $this->TrnReceiving->query($sql);
    }

    /**
     *受領履歴明細データ取得
     */
    private function _getReceivingHistoryDetail($where){
        $ShippingList = array();//データ取得用配列

        $sql  = ' select TrnReceiving.id                                 as "TrnReceiving__id"';
        $sql .=        ',to_char(TrnReceiving.work_date ,\'YYYY/mm/dd\') as "TrnReceiving__work_date"';
        $sql .=        ',TrnReceiving.work_no                            as "TrnReceiving__work_no"';
        $sql .=        ',TrnReceiving.work_type                          as "TrnReceiving__work_type"';
        $sql .=        ',TrnReceiving.department_id_from                 as "TrnReceiving__department_id_from"';
        $sql .=        ',TrnReceiving.department_id_to                   as "TrnReceiving__department_id_to"';
        $sql .=        ',TrnReceiving.mst_item_unit_id                   as "TrnReceiving__mst_item_unit_id"';
        $sql .=        ',TrnReceiving.stocking_close_type                as "TrnReceiving__stocking_close_type"';
        $sql .=        ',TrnReceiving.sales_close_type                   as "TrnReceiving__sales_close_type"';
        $sql .=        ',TrnReceiving.receiving_type                     as "TrnReceiving__receiving_type"';
        $sql .=        ',TrnReceiving.facility_close_type                as "TrnReceiving__facility_close_type"';
        $sql .=        ',TrnReceiving.trn_receiving_header_id            as "TrnReceiving__trn_receiving_header_id"';
        $sql .=        ',TrnReceiving.is_deleted                         as "TrnReceiving__is_deleted"';
        $sql .=        ',TrnReceiving.recital                            as "TrnReceiving__recital"';
        $sql .=        ',MstFacilityItem.id                              as "MstFacilityItem__id"';
        $sql .=        ',MstFacilityItem.internal_code                   as "MstFacilityItem__internal_code"';
        $sql .=        ',MstFacilityItem.item_name                       as "MstFacilityItem__item_name"';
        $sql .=        ',MstFacilityItem.standard                        as "MstFacilityItem__standard"';
        $sql .=        ',MstFacilityItem.item_code                       as "MstFacilityItem__item_code"';
        $sql .=        ',MstUser.user_name                               as "MstUser__user_name"';
        $sql .=        ',MstItemUnit.id                                  as "MstItemUnit__id"';
        $sql .=        ',( case ';
        $sql .=        '   when MstItemUnit.per_unit = 1 then MstUnitName.unit_name';
        $sql .=        "   else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')' ";
        $sql .=        ' end )                                           as "MstItemUnit__unit_name"';

        $sql .=        ',MstUser.user_name                               as "MstUser__user_name"';
        $sql .=        ',MstDealer.dealer_name                           as "MstDealer__dealer_name"';
        $sql .=        ',TrnSticker.id                                   as "TrnSticker__id"';
        $sql .=        ',TrnSticker.quantity                             as "TrnSticker__quantity"';
        $sql .=        ',TrnSticker.has_reserved                         as "TrnSticker__has_reserved"';
        $sql .=        ',TrnSticker.lot_no                               as "TrnSticker__lot_no"';
        $sql .=        ",to_char(TrnSticker.validated_date,'YYYY/mm/dd') as \"TrnSticker__validated_date\"";
        $sql .=        ",(CASE ";
        $sql .=        "   WHEN TrnReceiving.hospital_sticker_no = ' '";
        $sql .=        "   THEN TrnSticker.hospital_sticker_no";
        $sql .=        "   ELSE TrnReceiving.hospital_sticker_no";
        $sql .=        ' END )                                           as "TrnSticker__hospital_sticker_no"';
        $sql .=        ",(CASE ";
        $sql .=        "   WHEN TrnReceiving.facility_sticker_no IS NULL";
        $sql .=        "   THEN TrnSticker.facility_sticker_no";
        $sql .=        "   ELSE TrnReceiving.facility_sticker_no";
        $sql .=        ' END )                                           as "TrnSticker__facility_sticker_no"';
        $sql .=        ', TrnSticker.trade_type                          as "TrnSticker__trade_type"';
        $sql .=        ', ( case TrnShipping.shipping_type  ';
        foreach(Configure::read('ShippingTypes') as $k=>$v){
            $sql .= " when $k then '$v' ";
        }
        $sql .=        ' end )                                           as "TrnReceiving__type_name"';
        $sql .=        ',TrnSticker.trn_order_id                         as "TrnSticker__trn_order_id"';
        $sql .=        ',TrnSticker.mst_department_id                    as "TrnSticker__mst_department_id"';
        $sql .=        ',TrnShipping.id                                  as "TrnShipping__id"';
        $sql .=        ',TrnShipping.work_date                           as "TrnShipping__work_date"';
        $sql .=        ',TrnShipping.work_no                             as "TrnShipping__work_no"';
        $sql .=        ',TrnShipping.shipping_type                       as "TrnShipping__shipping_type"';
        $sql .=        ',MstDepartment.department_name                   as "TrnReceiving__department_name"';
        $sql .=        ',MstFacility.facility_name                       as "TrnReceiving__facility_name"';
        $sql .=        ',MstClass.id                                     as "TrnReceiving__class_id"';
        $sql .=        ',MstClass.name                                   as "TrnReceiving__class_name"';
        $sql .= '      , (case ';
        $sql .= "          when TrnReceiving.is_deleted = true then '取消済み'";
        $sql .= "          when TrnReceiving.stocking_close_type = 2 then '締め済み' ";
        $sql .= "          when TrnReceiving.sales_close_type = 2 then '締め済み' ";
        $sql .= "          when TrnReceiving.facility_close_type = 2 then '締め済み' ";
        $sql .= "          when TrnReceiving.stocking_close_type = 1 and x.updatable = false then '締め済み'";
        $sql .= "          when TrnReceiving.sales_close_type = 1 and x.updatable = false then '締め済み'";
        $sql .= "          when TrnReceiving.facility_close_type = 1 and x.updatable = false then '締め済み'";
        $sql .= "          when TrnSticker.trade_type = " . Configure::read('ClassesType.ExShipping') . " then '臨時出荷品'";
        $sql .= "          when TrnShipping.shipping_type = " . Configure::read('ShippingType.direct') . " then '直納品'";
        $sql .= "          when TrnSticker.quantity = 0 and TrnShipping.shipping_type != " . Configure::read('ShippingType.edidirect') . " then '消費済み'";
        $sql .= "          when TrnSticker.has_reserved = true then '予約済み'";
        $sql .= "          when TrnReceiving.department_id_to != TrnSticker.mst_department_id then '移動済み'";
        if($this->Session->read('Auth.Config.Shipping') == '1'){
            $sql .= "          when TrnShipping.shipping_type <> " . Configure::read('ShippingType.nostock') ;
            $sql .= "           and TrnShipping.shipping_type <> " . Configure::read('ShippingType.deposit') ;
            $sql .= "           and TrnShipping.shipping_type <> " . Configure::read('ShippingType.edidirect') ;
            $sql .= "           then '受領省略済み'";
        }
        $sql .= '        end )                                           as "TrnReceiving__status"';
        $sql .= ' from ';
        $sql .=        ' trn_receivings as TrnReceiving ';
        $sql .=        ' inner join trn_receiving_headers as TrnReceivingHeader on TrnReceiving.trn_receiving_header_id = TrnReceivingHeader.id';
        $sql .=        ' inner join mst_item_units as MstItemUnit on TrnReceiving.mst_item_unit_id = MstItemUnit.id ';
        $sql .=        ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ';
        $sql .=        ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id ';
        $sql .=        ' inner join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id ';
        $sql .=        ' inner join mst_users as MstUser on TrnReceiving.modifier = MstUser.id ';
        $sql .=        ' left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id ';
        $sql .=        ' inner join trn_stickers as TrnSticker on TrnReceiving.trn_sticker_id = TrnSticker.id ';
        $sql .=        ' inner join trn_shippings as TrnShipping on TrnReceiving.trn_shipping_id = TrnShipping.id ';
        $sql .=        ' inner join mst_departments as MstDepartment on TrnReceivingHeader.department_id_to = MstDepartment.id ';
        $sql .=        ' inner join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id ';
        $sql .=        ' left join mst_classes as MstClass on MstClass.id = TrnReceiving.work_type ';
        $sql .=        ' cross join (  ';
        $sql .=        '     select ';
        $sql .=        '         c.updatable  ';
        $sql .=        '     from ';
        $sql .=        '         mst_users as a  ';
        $sql .=        '         left join mst_roles as b  ';
        $sql .=        '             on a.mst_role_id = b.id  ';
        $sql .=        '         left join mst_privileges as c  ';
        $sql .=        '             on c.mst_role_id = b.id  ';
        $sql .=        '     where ';
        $sql .=        '         a.id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .=        '         and c.view_no = 15'; //受領履歴
        $sql .=        '     ) as x  ';

        $sql .= ' where  1=1 ';

        if (!empty($this->request->data['MstReceipts']['id'])) {
            $sql .=        ' and TrnReceiving.trn_receiving_header_id in  ('.join(',',$this->request->data['MstReceipts']['id']).') ';
        }
        $sql .=        ' and TrnReceiving.trn_repay_header_id is NULL ';
        $sql .= $where;

        $ShippingList = $this->TrnReceiving->query($sql);

        //入荷明細ごとに施設名・部署名の取得
        //ステータスチェック
        for($j=0;$j<count($ShippingList);$j++){
            //返却済みチェック
            $repay_status = "";
            $repay_check = $this->TrnReceiving->find('count',
                                                     array(
                                                         'recursive' => -1,
                                                         'conditions' => array(
                                                             'trn_sticker_id = ' => $ShippingList[$j]['TrnSticker']['id'],
                                                             'receiving_type = ' => Configure::read('ReceivingType.repay'),
                                                             'is_deleted = false '
                                                             )
                                                         )
                                                     );
            if($repay_check > 0){
                $repay_status = "err";
                //返却が複数件見つかった場合、再度チェックを行う
                $sql  = ' select ';
                $sql .= "      sum(case when receiving_type = '". Configure::read('ReceivingType.repay') ."' then 1 else 0 end)  as repay_cnt ";
                $sql .= "     ,sum(case when receiving_type = '". Configure::read('ReceivingType.receipt') ."' then 1 else 0 end) as recipt_cnt ";
                $sql .= ' from ';
                $sql .= '     trn_receivings ';
                $sql .= ' where ';
                $sql .= '     trn_sticker_id = ' . $ShippingList[$j]['TrnSticker']['id'];
                $sql .= '     and is_deleted = false ';
                $sql .= "     and receiving_type in ('". Configure::read('ReceivingType.repay') ."','".Configure::read('ReceivingType.receipt')."')";

                $cntList = $this->TrnReceiving->query($sql);

                //受領件数=返却件数の場合、返却済であるため取消不可とする。受領件数＞返却件数の場合に取消可能。
                if($cntList[0][0]['recipt_cnt'] == $cntList[0][0]['repay_cnt']){
                    $repay_status = "err";
                }
            }
        }
        
        //--------------------------------------------------------------------------
        return $ShippingList;
    }

    /**
     * 受領履歴CSV出力
     */
    function export_csv() {
        $this->request->data['TrnShipping']['trn_shipping_header_id']=null;

        $where = $this->createSearch_Where(); //前画面の検索条件を引き継いでwhere句作成

        $sql = $this->_getReceivingCSV($where);
        $this->db_export_csv($sql , "受領履歴", '/receipts_history/index');
    }

    private function _getReceivingCSV($where){
        $sql  = ' select ';
        $sql .=        ' TrnReceiving.work_no                            as 受領番号';
        $sql .=        ",to_char(TrnReceiving.work_date ,'YYYY/mm/dd')   as 受領日";
        $sql .=        ",MstFacility.facility_name||'／'||MstDepartment.department_name ";
        $sql .=        '                                                 as 受領部署名';
        $sql .=        ',MstFacilityItem.internal_code                   as "商品ID"';
        $sql .=        ', ( case TrnShipping.shipping_type  ';
        foreach(Configure::read('ShippingTypes') as $k=>$v){
            $sql .= " when $k then '$v' ";
        }
        $sql .=        ' end )                                           as 種別';
        $sql .=        ',MstFacilityItem.item_name                       as 商品名';
        $sql .=        ',MstFacilityItem.standard                        as 規格';
        $sql .=        ',MstFacilityItem.item_code                       as 製品番号';
        $sql .=        ',MstDealer.dealer_name                           as 販売元名';
        $sql .=        ',( case ';
        $sql .=        '   when MstItemUnit.per_unit = 1 then MstUnitName.unit_name';
        $sql .=        "   else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')' ";
        $sql .=        ' end )                                           as 包装単位名';
        $sql .=        ',TrnSticker.lot_no                               as ロット番号';
        $sql .=        ",(CASE ";
        $sql .=        "   WHEN TrnReceiving.hospital_sticker_no = ' '";
        $sql .=        "   THEN TrnSticker.hospital_sticker_no";
        $sql .=        "   ELSE TrnReceiving.hospital_sticker_no";
        $sql .=        " END )                                           as 部署シール";
        $sql .=        ",(CASE ";
        $sql .=        "   WHEN TrnReceiving.facility_sticker_no IS NULL";
        $sql .=        "   THEN TrnSticker.facility_sticker_no";
        $sql .=        "   ELSE TrnReceiving.facility_sticker_no";
        $sql .=        " END )                                           as センターシール";
        $sql .=        ',TrnShipping.work_no                             as 出荷番号';
        
        $sql .=        ',OrderFacility.facility_name                     as 仕入先';
        $sql .=        ',TrnOrder.stocking_price                         as 仕入金額   ';
        
        $sql .=        ',MstClass.name                                   as 作業区分名';
        $sql .=        ',MstClass.name                                   as 状態';
        $sql .=        ',TrnReceiving.recital                            as 備考';
        $sql .=        ',MstUser.user_name                               as 更新者名';
        $sql .= ' from ';
        $sql .=        ' trn_receivings as TrnReceiving ';
        $sql .=        ' inner join trn_receiving_headers as TrnReceivingHeader on TrnReceiving.trn_receiving_header_id = TrnReceivingHeader.id';
        $sql .=        ' inner join mst_item_units as MstItemUnit on TrnReceiving.mst_item_unit_id = MstItemUnit.id ';
        $sql .=        ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ';
        $sql .=        ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id ';
        $sql .=        ' inner join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id ';
        $sql .=        ' inner join mst_users as MstUser on TrnReceiving.modifier = MstUser.id ';
        $sql .=        ' left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id ';
        $sql .=        ' inner join trn_stickers as TrnSticker on TrnReceiving.trn_sticker_id = TrnSticker.id ';
        $sql .=        ' left join trn_orders as TrnOrder on TrnOrder.id = TrnSticker.trn_order_id';
        $sql .=        ' left join mst_departments as OrderDepartment on OrderDepartment.id = TrnOrder.department_id_to';
        $sql .=        ' left join mst_facilities as OrderFacility on OrderFacility.id = OrderDepartment.mst_facility_id';
        $sql .=        ' inner join trn_shippings as TrnShipping on TrnReceiving.trn_shipping_id = TrnShipping.id ';
        $sql .=        ' inner join mst_departments as MstDepartment on TrnReceivingHeader.department_id_to = MstDepartment.id ';
        $sql .=        ' inner join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id ';
        $sql .=        ' left join mst_classes as MstClass on MstClass.id = TrnReceiving.work_type ';
        $sql .= ' where  ';
        $sql .=        ' TrnReceiving.trn_repay_header_id is NULL ';
        $sql .=        ' and  1=1 '.$where;
        $sql .= ' order by TrnReceiving.id ';

        return $sql;
    }
    
    function seal($type=1){
        if($type == 1){
            // 部署シールの場合
            $sticker_no = array();
            foreach($this->request->data['TrnReceiving']['id'] as $id){
                $sticker_no[] = $this->request->data['TrnReceiving']['hospital_sticker_no'][$id];
            }
            if($sticker_no == array()){
                $data = array();
            }else{
                $order = 'MstDepartment.department_name asc, MstShelfNameTo.code asc';
                $data = $this->Stickers->getHospitalStickers($sticker_no, $order,2);
            }
            $this->set('sortkey' , '病院情報,センター棚番号,商品ID');
            $this->set('Sticker' , $data);
            $this->layout = 'xml/default';
            $this->render('/stickerissues/hospital_sticker');

        }else if($type == 2){
            // コストシールの場合
            $sticker_id = array();
            foreach($this->request->data['TrnReceiving']['id'] as $id){
                $sticker_id[] = $this->request->data['TrnReceiving']['sticker_id'][$id];
            }
            
            if($sticker_id == array()){
                $records = array();
            }else{
                $records = $this->Stickers->getCostStickers($sticker_id);
                // 空なら初期化
                if(!$records){ $records = array();}
            }
            $this->layout = 'xml/default';
            $this->set('records', $records);
            $this->render('/stickerissues/cost_sticker');
        }
    }
    
    /**
     * 印刷周り
     *
     */
    function report($type=null){
        switch($type){
          case 1: // 納品リスト
            $this->report_delivery();
            break;
          case 2: // 未納リスト
            $this->report_stockout();
            break;
          default:
            break;
        }
    }
    
    /**
     * 納品リスト印字
     *
     */
    function report_delivery(){
        $now = date('Y/m/d H:i');
        $sql  = ' select ';
        $sql .= '       g.facility_name ';
        $sql .= '     , f.department_name ';
        $sql .= "     , '〒' ||  coalesce(g.zip,'') || '  ' ||  coalesce(g.address,'') || '\nTel:' ||  coalesce(g.tel,'') || '／Fax:' ||  coalesce(g.fax,'') as facility_address ";
        $sql .= '     , a.work_no ';
        $sql .= "     , to_char(a.work_date, 'YYYY年MM月DD日')  as work_date ";
        $sql .= '     , h.facility_name                         as owner_name ';
        $sql .= "     , '〒' ||  coalesce(h.zip,'') || '  ' ||  coalesce(h.address,'') || '\nTel:' ||  coalesce(h.tel,'') || '／Fax:' ||  coalesce(h.fax,'') as owner_address ";
        $sql .= '     , c.trade_type ';
        $sql .= '     , e.internal_code ';
        $sql .= '     , e.item_name ';
        $sql .= '     , e.item_code ';
        $sql .= '     , e.standard ';
        $sql .= '     , e1.dealer_name ';
        $sql .= '     , c.hospital_sticker_no ';
        $sql .= '     , c.lot_no ';
        $sql .= "     , to_char(c.validated_date, 'YYYY/MM/DD') as validated_date ";
        $sql .= '     , c.quantity ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when d.per_unit = 1  ';
        $sql .= '         then d1.unit_name  ';
        $sql .= "         else d1.unit_name || '(' || d.per_unit || d2.unit_name || ')' ";
        $sql .= '         end ';
        $sql .= '     )                                         as unit_name ';
        $sql .= "     , '発注日:' || to_char(c1.work_date,'YYYY/MM/DD') || coalesce(b.recital,'') as recital ";
        $sql .= "     , to_char(a.work_date,'YYYYMMDD') || '_' || f.id || '_' || h.id             as key";
        $sql .= '   from ';
        $sql .= '     trn_receiving_headers as a  ';
        $sql .= '     left join trn_receivings as b  ';
        $sql .= '       on b.trn_receiving_header_id = a.id  ';
        $sql .= '     left join trn_stickers as c  ';
        $sql .= '       on c.id = b.trn_sticker_id  ';
        $sql .= '     left join trn_orders as c1';
        $sql .= '       on c1.id = c.trn_order_id ';
        $sql .= '     left join mst_item_units as d  ';
        $sql .= '       on d.id = c.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as d1  ';
        $sql .= '       on d1.id = d.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as d2  ';
        $sql .= '       on d2.id = d.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as e  ';
        $sql .= '       on e.id = d.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as e1  ';
        $sql .= '       on e1.id = e.mst_dealer_id  ';
        $sql .= '     left join mst_departments as f  ';
        $sql .= '       on f.id = a.department_id_to  ';
        $sql .= '     left join mst_facilities as g  ';
        $sql .= '       on g.id = f.mst_facility_id  ';
        $sql .= '     left join mst_facilities as h  ';
        $sql .= '       on h.id = e.mst_owner_id  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',',$this->request->data['MstReceipts']['id']) . ')  ';
        $sql .= '     and b.is_deleted = false  ';
        $sql .= '   order by ';
        $sql .= '     a.work_date ';
        $sql .= '     , f.id ';
        $sql .= '     , h.id ';
        $sql .= '     , c1.work_date';
        $sql .= '     , e.item_name ';
        $sql .= '     , e.standard ';
        $sql .= '     , e.item_code ';
        $sql .= '     , b.id ';
        
        $ret = $this->TrnReceiving->query($sql);
        
        
        $columns = array("納品先名",
                         "出荷番号",
                         "出荷先名",
                         "施設名",
                         "出荷日",
                         "出荷先住所",
                         "施設住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "部署シール",
                         "ロット番号",
                         "明細備考",
                         "数量",
                         "包装単位",
                         "管理区分",
                         "商品ID",
                         "印刷年月日",
                         "改ページ番号"
                         );
        
        $classes = Configure::read('Classes');

        $before = "0";
        foreach($ret as $r){
            // 明細番号
            $now = $r[0]['key'];
            if($before !== $now){
                $i = 1;
                $before = $now;
            }
            $lot_no = $r[0]['lot_no'];
            if(!empty($r[0]['validated_date'])){
                $lot_no = $lot_no . "\n" . $r[0]['validated_date'];
            }
            // 出力データ
            $data[] = array(
                $r[0]['facility_name'] . '様',          //納品先名
                $r[0]['work_no'],                       //出荷番号
                '納品先 : ' . $r[0]['department_name'], //出荷先名
                $r[0]['owner_name'],                    //施設名
                $r[0]['work_date'],                     //出荷日
                $r[0]['facility_address'],              //出荷先住所
                $r[0]['owner_address'],                 //施設住所
                $i,                                     //行番号
                $r[0]['item_name'],                     //商品名
                $r[0]['standard'],                      //規格
                $r[0]['item_code'],                     //製品番号
                $r[0]['dealer_name'],                   //販売元
                $r[0]['hospital_sticker_no'],           //部署シール
                $lot_no,                                //ロット番号
                $r[0]['recital'],                       //明細備考
                $r[0]['quantity'],                      //数量
                $r[0]['unit_name'],                     //包装単位
                $classes[$r[0]['trade_type']],          //管理区分
                $r[0]['internal_code'],                 //商品ID
                $now,                                   //印刷日時
                $r[0]['key']                            //改ページ番号
                );
            $i++;
        }
        
        if(empty($data)){
            $this->Session->setFlash('対象データがありませんでした。', 'growl', array('type'=>'important') );
            $this->redirect('index');
        }
        
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'納品リスト');
        $this->xml_output($data,join("\t",$columns),$layout_name['08'],$fix_value);
    }
    
    /**
     * 未納リスト印字
     *
     */
    function report_stockout(){
        $sql  = ' select ';
        $sql .= '       *  ';
        $sql .= '   from ';
        $sql .= '     (  ';
        /* 未納: 預託、非在庫発注 */
        $sql .= '       select ';
        $sql .= '             f.facility_code ';
        $sql .= '           , f.facility_name ';
        $sql .= '           , e.department_code ';
        $sql .= '           , e.department_name ';
        $sql .= "           , to_char(a.work_date,'YYYY/mm/dd') as work_date ";
        $sql .= '           , c.internal_code ';
        $sql .= '           , c.item_name ';
        $sql .= '           , c.item_code ';
        $sql .= '           , c.standard ';
        $sql .= '           , (  ';
        $sql .= '             case  ';
        $sql .= '               when b.per_unit = 1 ';
        $sql .= '               then b1.unit_name ';
        $sql .= "               else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .= '               end ';
        $sql .= '           )                            as unit_name ';
        $sql .= '           , d.dealer_name ';
        $sql .= '           , a.remain_count as quantity ';
        $sql .= '           , (  ';
        $sql .= '             case  ';
        $sql .= '               when c.is_lowlevel = true  ';
        $sql .= "               then '一般消耗品' ";
        $sql .= '               when a.order_type = ' . Configure::read('OrderTypes.ExNostock');
        $sql .= "               then '臨時' ";
        $sql .= "               else '非在庫 : ' || i.fixed_count ";
        $sql .= '               end ';
        $sql .= '           )                            as comment ';
        $sql .= '           , m.name                     as shelf_name ';
        $sql .= '           , 0                          as list_type ';
        $sql .= '           , ( case when c.is_lowlevel = true ';
        $sql .= '                    then ' . Configure::read('PromiseType.Temporary');
        $sql .= '                    when a.order_type = '  . Configure::read('OrderTypes.ExNostock');
        $sql .= '                    then ' . Configure::read('PromiseType.Temporary');
        $sql .= '                    else ' . Configure::read('PromiseType.Constant');
        $sql .= '               end ) as promise_type';
        $sql .= '           , ( case when c.is_lowlevel = true then 0 else 1 end ) as lowlevel';
        $sql .= '         from ';
        $sql .= '           trn_orders as a ';
        $sql .= '           left join mst_item_units as b ';
        $sql .= '             on b.id = a.mst_item_unit_id ';
        $sql .= '           left join mst_unit_names as b1 ';
        $sql .= '             on b1.id = b.mst_unit_name_id ';
        $sql .= '           left join mst_unit_names as b2 ';
        $sql .= '             on b2.id = b.per_unit_name_id ';
        $sql .= '           left join mst_facility_items as c ';
        $sql .= '             on c.id = b.mst_facility_item_id ';
        $sql .= '           left join mst_dealers as d ';
        $sql .= '             on d.id = c.mst_dealer_id ';
        $sql .= '           left join mst_departments as e ';
        $sql .= '             on e.id = a.department_id_from ';
        $sql .= '           left join mst_facilities as f ';
        $sql .= '             on f.id = e.mst_facility_id ';
        $sql .= '           left join mst_fixed_counts as i ';
        $sql .= '             on i.mst_item_unit_id = b.id ';
        $sql .= '            and i.mst_department_id = e.id ';
        $sql .= '            and i.is_deleted = false ';

        $sql .= '           left join mst_facilities as j ';
        $sql .= '             on j.id = c.mst_facility_id ';
        $sql .= '           left join mst_departments as k ';
        $sql .= '             on k.mst_facility_id = j.id';
        $sql .= '            and k.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '           left join mst_fixed_counts as l ';
        $sql .= '             on l.mst_department_id = k.id';
        $sql .= '            and l.mst_item_unit_id = b.id';
        $sql .= '            and b.is_deleted = false ';
        $sql .= '           left join mst_shelf_names as m';
        $sql .= '             on m.id = l.mst_shelf_name_id ';
        
        $sql .= '         where ';
        $sql .= '           a.remain_count > 0  ';
        $sql .= '           and a.is_deleted = false ';
        $sql .= '           and c.mst_facility_id = ' . $this->request->data['MstFacility']['id'];
        $sql .= '           and a.order_type in (' . Configure::read('OrderTypes.replenish') . ', ' . Configure::read('OrderTypes.nostock') . ', ' . Configure::read('OrderTypes.ExNostock') . ') ';
        $sql .= '     ) as xxx  ';
        $sql .= '   where 1 = 1 ';

        //発行施設
        if(!empty($this->request->data['MstReceipts']['facilityCode'])){
            $sql .= " and facility_code = '". $this->request->data['MstReceipts']['facilityCode'] ."'";
            //発行部署
            if(!empty($this->request->data['MstReceipts']['departmentCode'])){
                $sql .= " and department_code = '" . $this->request->data['MstReceipts']['departmentCode']."'";
            }
        }
        //商品ID
        if(!empty($this->request->data['MstReceipts']['search_internal_code'])){
            $sql .= " and internal_code = '" . Sanitize::escape($this->request->data['MstReceipts']['search_internal_code']) . "'";
        }
        //製品番号
        if(!empty($this->request->data['MstReceipts']['search_item_code'])){
            $sql .= " and item_code LIKE '%".Sanitize::escape($this->request->data['MstReceipts']['search_item_code'])."%'";
        }
        //規格
        if(!empty($this->request->data['MstReceipts']['search_standard'])){
            $sql .= " and standard LIKE '%".Sanitize::escape($this->request->data['MstReceipts']['search_standard'])."%'";
        }
        //商品名
        if(!empty($this->request->data['MstReceipts']['search_item_name'])){
            $sql .= " and item_name LIKE '%" . Sanitize::escape($this->request->data['MstReceipts']['search_item_name']) . "%'";
        }
        //販売元
        if(!empty($this->request->data['MstReceipts']['search_dealer_name'])){
            $sql .= " and dealer_name LIKE '%" . Sanitize::escape($this->request->data['MstReceipts']['search_dealer_name']) . "%'";
        }
        $sql .= '   order by ';
        $sql .= '     facility_code ';
        $sql .= '     , department_code ';
        $sql .= '     , work_date ';
        $sql .= '     , item_name ';
        $sql .= '     , standard ';
        $sql .= '     , item_code ';
        $sql .= '     , dealer_name ';

        $ret = $this->TrnPromise->query($sql);
        $columns = array("補充依頼番号",
                         "仕入先名",
                         "補充先名",
                         "施主名",
                         "補充依頼日",
                         "納品先住所",
                         "施主住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "明細備考",
                         "包装単位",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $tmpHeader = "";
        $i = 1;
        $tmp_page_no = '';
        foreach($ret as $r){
            $page_no = $r[0]['facility_name'].$r[0]['department_name'].'_'.$r[0]['lowlevel'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }

            if($r[0]['quantity'] == 0){
                $r[0]['quantity'] = '';
            }
            $r[0]['comment'] = '要求日 : ' . $r[0]['work_date'] . ' : ' . $r[0]['comment'];
            if($r[0]['shelf_name'] != ''){
                $r[0]['comment'] = $r[0]['comment'] . ' : 棚番 ' .$r[0]['shelf_name'];
            }
            
            $data[] = array(  ''                                                      //補充依頼番号
                             ,''                                                      //仕入先名
                             ,$r[0]['facility_name'] . ' ' . $r[0]['department_name'] //補充先名
                             ,''                                                      //施主名
                             ,''                                                      //補充依頼日
                             ,''                                                      //納品先住所
                             ,''                                                      //施主住所
                             ,$i                                                      //行番号
                             ,$r[0]['item_name']                                      //商品名
                             ,$r[0]['standard']                                       //規格
                             ,$r[0]['item_code']                                      //製品番号
                             ,$r[0]['dealer_name']                                    //販売元
                             ,$r[0]['internal_code']                                  //商品ID
                             ,$r[0]['comment']                                        //明細備考
                             ,$r[0]['unit_name']                                      //包装単位
                             ,$r[0]['quantity']                                       //数量
                             ,''                                                      //ヘッダ備考
                             ,''                                                      //印刷年月日
                             ,''                                                      //丸め区分
                             ,''                                                      //削除フラグ
                             ,''                                                      //変更フラグ
                             ,$page_no                                                //改ページ番号
                             );
            $i++;
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'未納リスト');
        $this->xml_output($data,join("\t",$columns),$layout_name['11'],$fix_value);
    }

    //入荷履歴Excel出力
    function export_excel(){
        $this->request->data['TrnShipping']['trn_shipping_header_id']=null;
        $where = $this->createSearch_Where(); //前画面の検索条件を引き継いでwhere句作成
        $sql = $this->_getReceivingCSV($where);

        $ret = $this->TrnReceiving->query($sql);

        //テンプレートファイルフルパス
        $template = realpath(TMP);
        $template .= DS . 'excel' . DS;
        $template_path = $template . "template12.xls";
        
        // Excelオブジェクト作成
        $PHPExcel = $this->createExcelObj($template_path);

        //表紙への入力
        //シートの設定
        $PHPExcel->setActiveSheetIndex(0);  //一番左のシート
        $sheet = $PHPExcel->getActiveSheet();
        // Excel書込処理
        foreach($ret as $num => $r){
            $cell =  (string)($num+3);
            
            $sheet->setCellValue("A" . $cell, $num + 1);              // No
            $sheet->setCellValueExplicit("B" . $cell, $r[0]['受領番号'] , PHPExcel_Cell_DataType::TYPE_STRING);  // 入荷番号
            $sheet->setCellValue("C" . $cell, $r[0]['受領日']);       // 入荷日
            $sheet->setCellValue("D" . $cell, $r[0]['受領部署名']);   // 部署名
            $sheet->setCellValue("E" . $cell, $r[0]['商品ID']);       // 商品ID
            $sheet->setCellValue("F" . $cell, $r[0]['商品名']);       // 商品名
            $sheet->setCellValue("G" . $cell, $r[0]['規格']);         // 規格
            $sheet->setCellValue("H" . $cell, $r[0]['製品番号']);     // 製品番号
            $sheet->setCellValue("I" . $cell, $r[0]['販売元名']);     // 販売元名
            $sheet->setCellValue("J" . $cell, $r[0]['包装単位名']);   // 包装単位
            $sheet->setCellValue("K" . $cell, $r[0]['仕入先']);       // 仕入先
            $sheet->setCellValue("L" . $cell, $r[0]['仕入金額']);     // 仕入金額
            $sheet->setCellValue("M" . $cell, $r[0]['状態']);         // 状態
            $sheet->setCellValue("N" . $cell, $r[0]['備考']);         // 備考
            $sheet->setCellValue("O" . $cell, $r[0]['更新者名']);     // 更新者名
        }
        
        //保存ファイル名
        $filename = "入荷履歴_"  . date('YmdHis')  . ".xls";

        $filename = mb_convert_encoding($filename, 'sjis', 'utf-8');
        
        // 保存ファイルパス
        $upload = realpath( TMP );
        $upload .= DS . '..' . DS . 'webroot' . DS . 'rfp' . DS;
        $path = $upload . $filename;
        
        $objWriter = new PHPExcel_Writer_Excel5( $PHPExcel );   //2003形式で保存
        $objWriter->save( $path );
        // 保存したExcelをダウンロード
        $result = file_get_contents(WWW_ROOT . DS . 'rfp' . DS . $filename);
        
        Configure::write('debug',0);
        header("Content-disposition: attachment; filename=" . $filename);
        header("Content-type: application/octet-stream; name=" . $filename);
        print($result);
        unlink(WWW_ROOT . DS . 'rfp' . DS . $filename);
        exit;
    }

    
    
}
