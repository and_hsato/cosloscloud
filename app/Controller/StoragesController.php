<?php
/**
 * StoragesController
 * 入庫登録
 * @since 2010/09/06
 */
class StoragesController extends AppController {
    var $name = 'Storages';
    var $uses = array(
        'MstFacility',
        'MstClass',
        'MstTransactionConfig',
        'MstDepartment',
        'MstFixedCount',
        'MstFacilityItem',
        'TrnReceivingHeader',
        'TrnReceiving',
        'TrnSticker',
        'TrnStickerRecord',
        'TrnStock',
        'TrnStorage',
        'TrnStorageHeader',
        'TrnStickerIssue',
        'MstUnitName',
        'MstDealer',
    );

    var $components = array('RequestHandler', 'Barcode', 'Storage','Stickers');

    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'seal';
    }


    public function index(){
        $this->setRoleFunction(23); //入庫登録
        $this->read_barcode();
    }


    private function getReceivingHeaders($cond,$limit){
        $o = $this->getSortOrder();
        if(isset($o)){
            if(preg_match('/^\./',$o[0]) > 0){
                $order = substr($o[0],1);
            }else{
                $order = $o[0];
            }
        }else{
            $order = "TrnReceiving.work_date desc";
        }

        $sql =
            ' select ' .
                ' TrnReceiving.work_no' .
                ',TrnReceiving.work_date' .
                ',TrnReceivingHeader.recital' .
                ',TrnReceiving.department_id_from' .
                ',TrnReceiving.creater' .
                ',TrnReceiving.created' .
                ',TrnReceiving.trn_receiving_header_id' .
                ',MstFacility.facility_name' .
                ',MstUser.user_name'.
                ',count(*) as detail_count' .
            ' from ' .
                ' trn_receivings as TrnReceiving '.
                ' left join trn_receiving_headers as TrnReceivingHeader on TrnReceiving.trn_receiving_header_id = TrnReceivingHeader.id ' .
                ' left join trn_orders as TrnOrder on TrnReceiving.trn_order_id = TrnOrder.id'.
                ' inner join mst_item_units as MstItemUnit on MstItemUnit.id = TrnReceiving.mst_item_unit_id' .
                ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id '.
                ' left join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id' .
                ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id'.
                ' inner join mst_unit_names as MstParUnitName on MstItemUnit.per_unit_name_id = MstParUnitName.id'.
                ' inner join mst_departments as MstDepartment on TrnReceivingHeader.department_id_from = MstDepartment.id ' .
                ' inner join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id' .
                ' inner join mst_users as MstUser on TrnReceiving.creater = MstUser.id ' .
            ' where ' .
                $cond .
                ' and TrnReceiving.receiving_type = ' . Configure::read('ReceivingType.arrival') .
            ' group by ' .
                ' TrnReceiving.work_no' .
                ',TrnReceiving.work_date' .
                ',TrnReceivingHeader.recital' .
                ',TrnReceiving.department_id_from' .
                ',TrnReceiving.creater' .
                ',TrnReceiving.created'.
                ',TrnReceiving.trn_receiving_header_id' .
                ',MstFacility.facility_name' .
                ',MstUser.user_name'.
            ' order by ' .
                $order .
            ' limit ' . $limit;
        return $this->TrnReceivingHeader->query($sql);
    }


    public function read_barcode(){
        if(isset($this->request->data['IsRead']) && $this->request->data['IsRead'] == '1'){
            
            //トークン検証
            if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
                $isAuto = $this->Session->read('isAuto');
                /*
                if ($isAuto == false){
                    $this->Session->setFlash('同じデータが同時に２度読込まれました。', 'growl', array('type'=>'error'));
                    $this->set('isError' , true);
                    $this->redirect('read_barcode');
                }else {
                  */
                    $lastHeaderId = $this->Session->read('lastHeaderId');
                    $this->Session->delete('lastHeaderId');
                    if (isset($lastHeaderId)){
                        $facilityId = $this->Session->read('facilityId');
                        $this->Session->delete('facilityId');
                        $facility_name = $this->Session->read('facility_name');
                        $this->Session->delete('facility_name');
                        $this->request->data['isPrintSticker'] = false;
                        $this->set('lastHeaderId', $lastHeaderId);
                        $this->set('facilityId', $facilityId);
                        $this->set('facility_name', $facility_name);
                    }

                    //2度押し対策用にトランザクショントークンを作る
                    $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                    $this->render('/storages/read_barcode');
                // }
                $this->Session->delete('lastHeaderId');
                $this->Session->delete('facilityId');
                $this->Session->delete('facility_name');
                return;
            }
        }

        $fid = $this->Session->read('Auth.facility_id_selected');
        $selects = $this->Session->read('Auth.facilities');
        $this->set('facility_name', $selects[$fid]);
        
        $this->Session->write('facility_name', $selects[$fid]);

        //在庫締め
        if(false === $this->canUpdateAfterClose(date('Y/m/d', time()), $this->Session->read('Auth.facility_id_selected'))){
            $this->Session->setFlash('すでに在庫締めが行われている為、入庫できません', 'growl', array('type'=>'error'));
            
            //2度押し対策用にトランザクショントークンを作る
            $this->request->data[$this->name]['token'] = $this->createToken($this->name);
            $this->render('/storages/read_barcode');
            return;
        }

        if(isset($this->request->data['IsRead']) && $this->request->data['IsRead'] == '1'){
            $this->request->data['IsRead'] = 0;
            $_barcode = $this->Barcode->readEan128($this->request->data['barcode']);
            if(false === $_barcode){
                $this->Session->setFlash('バーコードが正しくありません', 'growl', array('type'=>'error'));
                $this->set('isError' , true);
                
                //2度押し対策用にトランザクショントークンを作る
                $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                $this->render('/storages/read_barcode');
                return;
            }else if($this->isNNs($_barcode['MstFacilityItem'])){
                $this->Session->setFlash('該当商品がありませんでした', 'growl', array('type'=>'error'));
                $this->set('isError' , true);
                
                //2度押し対策用にトランザクショントークンを作る
                $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                $this->render('/storages/read_barcode');
                return;
            }
            $this->Session->write('Storages.barcode', $_barcode);

            $rv = array();
            $rv['department_id_to'] = $this->Storage->getDepartmentId($this->Session->read('Auth.facility_id_selected'), Configure::read('DepartmentType.warehouse'));
            $rv['remain_count > '] = 0;

            //入荷情報取得
            $_params = $this->getSearchParamater(array(), $rv, array('jan_code LIKE ' => "{$_barcode['pure_jan_code']}%"));
            $_receivings = $this->getReceivings($_params);
            if(count($_receivings) === 0){
                $this->Session->setFlash('入荷情報に該当する商品がありませんでした', 'growl', array('type'=>'error'));
                $this->set('isError' , true);
                
                //2度押し対策用にトランザクショントークンを作る
                $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                $this->render('/storages/read_barcode');
                return;
            }
            $this->Session->write('Storages.receivings', $_receivings);
            $this->set('Receivings', AppController::outputFilter($_receivings));
            //在庫情報取得
            $_stocks = array();
            foreach($_receivings as $_row){
                $_stocks[$_row['id']] = $this->Storage->getStocks($_row['mst_facility_item_id'], $_row['per_unit']);
            }

            $this->Session->write('Storages.stocks', $_stocks);
            //自動モードかどうか
            $_isAuto = ($this->request->data['isAutoStorage'] == '1' );

            $expired_date = $_receivings[0]['expired_date'];

            //有効期限警告だった場合、選択入庫に遷移する
            if($_barcode['validated_date'] <> '' && $this->getFormatedValidateDate($_barcode['validated_date']) < date('Y/m/d', strtotime('+'.$expired_date.' day', time())) ){
                $_isAuto = false ;
                $this->Session->setFlash('有効期限切れ間近の商品です。\n確認して入庫して下さい。' , 'growl' , array('type'=>'error'));
                $this->set('isError' , true);
            }

            //有効期限が現在日付よりも古い場合選択入庫に遷移する。
            if($_barcode['validated_date'] <> '' && $this->getFormatedValidateDate($_barcode['validated_date']) < date('Y/m/d') ){
                $_isAuto = false ;
                $this->Session->setFlash('有効期限切れ商品です。\n確認して入庫して下さい。' , 'growl' , array('type'=>'error'));
                $this->set('isError' , true);
            }

            /*
            // JANから拾った入荷にひもづく仕入先が複数ある場合選択入庫に変える
            foreach($_receivings as $_row){
                if($_row['sup_count'] > 1){
                    $_isAuto = false ;
                    $this->Session->setFlash('仕入先が複数存在します。\n選択して入庫して下さい。', 'growl' , array('type'=>'error'));
                    $this->set('isError' , true);
                }
            }
            */

            if(true === $_isAuto){
                $_autoTarget = $this->canAutoMode($_receivings, $_stocks);
                if(false === $_autoTarget){
                    $_isAuto = false;
                }
                //包装単位適用外だった場合 $_isAuto = false;
                if($_isAuto !== false){
                    foreach($_stocks as $_stock){
                        foreach($_stock as $value){
                            if($value['is_right_term_pack'] == FALSE){
                                $_isAuto = false;
                                $this->Session->setFlash('適用外の包装単位が含まれます。\n確認して入庫して下さい。', 'growl' , array('type'=>'error'));
                                $this->set('isError' , true);
                            }
                        }
                    }
                }
            }
            $lot_ubd_alert = $_receivings[0]['lot_ubd_alert'];
            //LOT-UBD設定値を取得（自動・選択モード両方）
            $isLotUbdChk = $this->isTypeLotAndValidatedDate($lot_ubd_alert, $lotchk, $datechk);

            //自動入庫モードの場合で、LOT/UBD警告設定ある場合、チェック実施
            //不正なら選択モードへ遷移させる
            if(true === $_isAuto){
                //LOT
                if (isset($lotchk) && $lotchk == true){
                    if (!isset($_barcode['lot_no']) || $_barcode['lot_no'] == ''){
                        $_isAuto = false;
                        $this->Session->setFlash('ロット番号がありません。\n確認して入庫して下さい。', 'growl' , array('type'=>'error'));
                        $this->set('isError' , true);
                    }
                }
                //UBD
                if ($_isAuto == true && isset($datechk) && $datechk == true){
                    if (!$this->chkValidatedDate($_barcode['validated_date'],$expired_date)){
                        $_isAuto = false;
                        $this->Session->setFlash('有効期限がありません。\n確認して入庫して下さい。', 'growl' , array('type'=>'error'));
                        $this->set('isError' , true);
                    }
                }
            }
            
            $this->Session->write('isAuto', $_isAuto);
            if(true === $_isAuto){
                //自動モード

                $cnt = 0;
                $i = 0;
                $receiving_count = $_receivings[0]['per_unit'];
                $stock_work = array();
                $stock_workId = 0;
                $stock_quantity = 0;

                //引当数がマイナスのものから処理
                while (count($_autoTarget['minus']) > $cnt ){
                    $target = $_autoTarget['minus'][0];
                    $stock_workId = $target['data']['TrnStock']['id'];

                    $stock_workId = $target['data']['TrnStock']['id'];

                    $stock_ret = $this->Storage->getStockById($stock_workId,$_stocks[$_receivings[0]['id']]);

                    $stock_quantity = 1;
                    if ($receiving_count - $target['per_unit'] >= 0) {
                        //入庫残数が足りている
                        if (isset($stock_work[$stock_workId])) {
                            $stock_work[$stock_workId]['input_quantity'] = $stock_work[$stock_workId]['input_quantity'] + $stock_quantity;

                        }else {
                            $stock_work[$stock_workId] = $stock_ret;
                            $stock_work[$stock_workId]['input_quantity'] = $stock_quantity;
                            $stock_work[$stock_workId]['per_unit'] = $target['per_unit'];
                        }

                        $receiving_count = $receiving_count - $target['per_unit'];

                        //在庫数を計算 在庫数 - 予約数 - 被要求数
                        $promisable = ($stock_ret['stock_count'] +  $stock_work[$stock_workId]['input_quantity']) -  $stock_ret['reserve_count'] -  $stock_ret['promise_count'];

                        if ($promisable != 0 && !is_null($target['data']['MstFixedCount']['fixed_count']) && $target['data']['MstFixedCount']['fixed_count'] > 0) {
                            $average = abs($promisable / (is_null($target['data']['MstFixedCount']['fixed_count'])?0:$target['data']['MstFixedCount']['fixed_count']));
                        } else {
                            //引当可能数がマイナスの場合、引当可能数が0になったらおしまい。
                            if ($promisable == 0) {
                                $average = PHP_INT_MAX;
                            } else {
                                $average = PHP_INT_MAX - 1;
                            }
                        }

                        $_autoTarget['minus'][0]['sort'] = $average;

                    } else {
                        //入庫対象 > 入庫残数の場合、その包装単位・入数への入庫を終了する。
                        $_autoTarget['minus'][0]['sort'] = PHP_INT_MAX;
                    }

                    $stock_flg = false;
                    foreach($_autoTarget['minus'] as $key=>$value){
                        $sort[$key]        = $value['sort'];
                        $standard[$key]    = $value['standard'];
                        $per_unit[$key]    = $value['per_unit'];
                        $fixed_count[$key] = $value['fixed_count'];

                        //全ての包装単位が終了しているかチェック
                        if ($value['sort'] < PHP_INT_MAX) {
                            $stock_flg = true;
                        }
                    }

                    //全包装単位の割り当てが終わった場合は終了。
                    if (!$stock_flg) {
                        break;
                    }

                    array_multisort($sort ,SORT_ASC,$fixed_count,SORT_DESC,$standard,SORT_DESC,$per_unit,SORT_DESC,$_autoTarget['minus']);

                }

                $stock_flg=false;
                $cnt = 0;
                //引当数プラスの処理
                while ($receiving_count > 0){
                    $target = $_autoTarget['plus'][0];
                    $stock_workId = $target['data']['TrnStock']['id'];

                    $stock_ret = $this->Storage->getStockById($stock_workId,$_stocks[$_receivings[0]['id']]);

                    $stock_quantity = 1;
                    if ($receiving_count - $target['per_unit'] >= 0) {
                        //入庫残数が足りている
                        if (isset($stock_work[$stock_workId])) {
                            $stock_work[$stock_workId]['input_quantity'] = $stock_work[$stock_workId]['input_quantity'] + $stock_quantity;

                        }else {
                            $stock_work[$stock_workId] = $stock_ret;
                            $stock_work[$stock_workId]['input_quantity'] = $stock_quantity;
                            $stock_work[$stock_workId]['per_unit'] = $target['per_unit'];
                        }

                        $receiving_count = $receiving_count - $target['per_unit'];

                        //在庫数を計算 在庫数 - 予約数 - 被要求数
                        $promisable = ($stock_ret['stock_count'] +  $stock_work[$stock_workId]['input_quantity']) -  $stock_ret['reserve_count'] -  $stock_ret['promise_count'];

                        if ($promisable != 0 && !is_null($target['data']['MstFixedCount']['fixed_count']) && $target['data']['MstFixedCount']['fixed_count'] > 0) {
                            $average = abs($promisable / (is_null($target['data']['MstFixedCount']['fixed_count'])?0:$target['data']['MstFixedCount']['fixed_count']));
                        } else {
                            $average = PHP_INT_MAX - 1;
                        }

                        $_autoTarget['plus'][0]['sort'] = $average;

                    } else {
                        //入庫対象 > 入庫残数の場合、その包装単位・入数への入庫を終了する。
                        $_autoTarget['plus'][0]['sort'] = PHP_INT_MAX;
                    }

                    $stock_flg = false;
                    foreach($_autoTarget['plus'] as $key=>$value){
                        $sort[$key]        = $value['sort'];
                        $standard[$key]    = $value['standard'];
                        $per_unit[$key]    = $value['per_unit'];
                        $fixed_count[$key] = $value['fixed_count'];

                        //全ての包装単位が終了しているかチェック
                        if ($value['sort'] < PHP_INT_MAX) {
                            $stock_flg = true;
                        }
                    }

                    //全包装単位の割り当てが終わった場合は終了。
                    if (!$stock_flg) {
                        break;
                    }

                    array_multisort($sort ,SORT_ASC,$fixed_count,SORT_DESC,$standard,SORT_DESC,$per_unit,SORT_DESC,$_autoTarget['plus']);

                }

                //上記で取得した内容を詰め直し
                foreach($stock_work as $row){
                    $stock[$i] = $row;
                    ++$i;
                }

                unset($sort,$standard,$per_unit,$fixed_count);

                //あまりがなければ処理続行
                if($receiving_count == 0){
                    foreach($stock as $key=>$value){
                        $stock_sort[$key] = $value['per_unit'];
                    }

                    array_multisort($stock_sort ,SORT_DESC,$stock);

                    //入庫数量
                    $_receivings[0]['storage_quantity'] = 1;
                    $lastHeaderId = $this->Storage->createStorages($_receivings[0], $stock, array('lot_no' => $_barcode['lot_no'], 'validated_date' => $_barcode['validated_date'], 'recital' => null , 'time'=>$this->request->data['TrnStorage']['time']));
                    $this->Session->setFlash('入庫が完了しました', 'growl', array('type'=>'star'));
                    $this->request->data['isPrintSticker'] = true;
                    $this->set('lastHeaderId', $lastHeaderId);
                    $this->set('facilityId', $this->Session->read('Auth.facility_id_selected'));
                    $this->request->data['barcode'] = '';
                    
                    $this->Session->write('lastHeaderId', $lastHeaderId);
                    $this->Session->write('facilityId', $this->Session->read('Auth.facility_id_selected'));
                    //2度押し対策用にトランザクショントークンを作る
                    $this->request->data[$this->name]['token'] = $this->createToken($this->name);

                    $this->render('/storages/read_barcode');
                }else{
                    //あまりがあったら選択入庫に変更
                    $_isAuto = false;
                }
            }

            if(false === $_isAuto){
                //選択モード
                $this->set('BarcodeInfo', $_barcode);
                $this->request->data['isAutoStorage'] = '0';
                $this->request->data['TrnSticker']['lot_no'] = $_barcode['lot_no'];
                $this->request->data['TrnSticker']['validated_date'] = $this->getFormatedValidateDate($_barcode['validated_date']);
                //画面でのLOT/UBDチェック
                if ($isLotUbdChk == true){
                    $this->set('lotchk', $lotchk);
                    $this->set('datechk', $datechk);
                    $current = date("Y/m/d");
                    $this->set('expired_date', $expired_date);
                    $this->set('current_date', $current);
                }
                
                //2度押し対策用にトランザクショントークンを作る
                $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                $this->render('/storages/divide_item');
            }


        }else{
            if(isset($this->request->data['TrnReceivingHeader']['Rows']) &&
               false === $this->isNNs($this->request->data['TrnReceivingHeader']['Rows'])){
                $this->deleteSortInfo();
                $_headerConditions = array();
                foreach($this->request->data['TrnReceivingHeader']['Rows'] as $_key => $_val){
                    if($_val == '1'){
                        $_headerConditions[] = $_key;
                    }
                }
                $this->Session->write('TrnStorage.headerConditions', array('id' => $_headerConditions));
            }
            $this->request->data['isAutoStorage'] = '1';
            
            //2度押し対策用にトランザクショントークンを作る
            $this->request->data[$this->name]['token'] = $this->createToken($this->name);
            $this->render('/storages/read_barcode');
        }
    }

    //有効期限チェック
    public function chkValidatedDate($target, $term){
        //期限切れ警告日数がなければ[チェックしない]
        if (!isset($term)){
            return true;
        }
        //有効期限がなければエラー
        if (!isset($target)){
            return false;
        }
        $validateddate = $this->getFormatedValidateDate($target);

        $today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
        //本日日付(入庫日) + 有効期限日数
        $second_added = $today + (86400 * $term);
        $date_added =  date('Y/m/d',$second_added);
        //[本日日付(入庫日) + 有効期限日数] >= シールの有効期限の場合
        if (strtotime($date_added) >= strtotime($validateddate)){
            return false;
        }else{
            return true;
        }
    }

    public function commit(){
        
        //トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            $lastHeaderId = $this->Session->read('lastHeaderId');
            if (isset($lastHeaderId)){
                $facilityId = $this->Session->read('facilityId');
                $facility_name = $this->Session->read('facility_name');
                $this->request->data['isPrintSticker'] = false;

                $this->set('lastHeaderId', $lastHeaderId);
                $this->set('facilityId', $facilityId);
                $this->set('facility_name', $facility_name);
            }
            $this->Session->delete('lastHeaderId');
            $this->Session->delete('facilityId');
            $this->Session->delete('facility_name');
            //2度押し対策用にトランザクショントークンを作る
            $this->request->data[$this->name]['token'] = $this->createToken($this->name);
            $this->render('/storages/read_barcode');
            return;
        }

        $_barcode = $this->Session->read('Storages.barcode');
        $_receiving = $_stocks = array();

        //更新日時チェック & 更新対象取得
        $rv = array();
        $rv['department_id_to'] = $this->Storage->getDepartmentId($this->Session->read('Auth.facility_id_selected'), Configure::read('DepartmentType.warehouse'));
        $rv['remain_count > '] = 0;
        $_preR = $this->Session->read('Storages.receivings');
        $_nowR = $this->getReceivings($this->getSearchParamater(array(), $rv, array('jan_code LIKE ' => "{$_barcode['pure_jan_code']}%")));
        if(true === $this->hasModefiedRow($_preR, $_nowR)){
            $this->Session->setFlash('入荷情報が更新されています。再度実行してください', 'growl', array('type'=>'error'));
            //2度押し対策用にトランザクショントークンを作る
            $this->request->data[$this->name]['token'] = $this->createToken($this->name);
            $this->render('/storages/read_barcode');
            return;
        }
        foreach($_nowR as $_row){
            if($_row['id'] == $this->request->data['TrnReceiving']['selected']){
                $_receiving = $_row;
                break;
            }
        }

        $_preS = $this->Session->read('Storages.stocks');
        $_nowS = $this->Storage->getStocks($_receiving['mst_facility_item_id'], $_receiving['per_unit']);
        if(true === $this->hasModefiedRow($_preS[$_receiving['id']], $_nowS)){
            $this->Session->setFlash('在庫情報が更新されています。再度実行してください', 'growl', array('type'=>'error'));
            //2度押し対策用にトランザクショントークンを作る
            $this->request->data[$this->name]['token'] = $this->createToken($this->name);
            $this->render('/storages/read_barcode');
            return;
        }

        foreach($this->request->data['TrnStorage']['Rows'] as $_key => $_val){
            if($_val['isSelected'] == '1'){
                $_r = $this->Storage->getStockById($_key, $_nowS);
                $_r['input_quantity'] = $_val['quantity'];
                $_stocks[] = $_r;
            }
        }

        //入庫数量
        $_receiving['storage_quantity'] = 1;

        foreach($_stocks as $key=>$value){
            $stock_sort[$key] = $value['per_unit'];
        }

        array_multisort($stock_sort ,SORT_DESC,$_stocks);

        //入庫関係データ作成
        $lastHeaderId = $this->Storage->createStorages($_receiving, $_stocks, array('lot_no'=>$this->request->data['TrnSticker']['lot_no'],'validated_date'=>$this->request->data['TrnSticker']['validated_date'],'recital'=>$this->request->data['TrnSticker']['recital'] , 'time'=>$this->request->data['TrnStorage']['time']));
        $this->request->data['isPrintSticker'] = true;
        $this->set('lastHeaderId', $lastHeaderId);
        $this->set('facilityId', $this->Session->read('Auth.facility_id_selected'));
        $this->Session->setFlash('入庫が完了しました', 'growl', array('type'=>'star'));
        $fid = $this->Session->read('Auth.facility_id_selected');
        $selects = $this->Session->read('Auth.facilities');
        $this->set('facility_name', $selects[$fid]);

        
        $this->Session->write('lastHeaderId', $lastHeaderId);
        $this->Session->write('facilityId', $this->Session->read('Auth.facility_id_selected'));
        $this->Session->write('facility_name', $selects[$fid]);
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);

        $this->render('/storages/read_barcode');
    }

    /**
     * シール発行関数
     * @param string $dummy ダミー用
     * @access public
     * @return void
     */
    public function seal($dummy = null){
        $sql  = ' select ';
        $sql .= '       c.id  ';
        $sql .= '     , c.facility_sticker_no  ';
        $sql .= '   from ';
        $sql .= '     trn_storage_headers as a  ';
        $sql .= '     left join trn_storages as b  ';
        $sql .= '       on b.trn_storage_header_id = a.id  ';
        $sql .= '     left join trn_stickers as c  ';
        $sql .= '       on c.id = b.trn_sticker_id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->request->data['lastHeaderId'];

        $ret = $this->TrnStorageHeader->query($sql);
        foreach($ret as $r ){
            $_fno[] = $r[0]['facility_sticker_no'];
            $ids[] = $r[0]['id'];
        }

        $fid = $this->request->data['MstFacility']['id'];
        $fname = $this->getFacilityName($fid);
        $stickers = $this->Stickers->getFacilityStickers($_fno, $fname, $fid , 2);

        // コストシール情報取得
        $records = $this->Stickers->getCostStickers($ids,5);
        if(!empty($records)){
            $this->set('costStickers', $records);
        }
        $this->layout = 'xml/default';
        $this->set('stickers', $stickers);
        $this->render('/storages/seal');
    }

    /**
     * 明細が更新されているか
     * @param array $pre
     * @param array $now
     * @return boolean
     */
    protected function hasModefiedRow($pre, $now){
        if(count($pre) !== count($now)){
            return true;
        }
        for($i = 0; $i < count($pre); $i++){
            if($pre[$i]['modified'] != $now[$i]['modified']){
                return true;
            }
        }
        return false;
    }


    /**
     * 自動入庫可能かどうか
     * @access private
     * @param array $receivings
     * @return mixed
     */
    private function canAutoMode($receivings, $stocks){
        $today = date('Y/m/d');
        //仕入先・包装単位・仕入単価が異なる明細があるか
        $_res = array();
        foreach($receivings as $_row){
            $_res[$_row['department_id_from'] . '|' . $_row['mst_item_unit_id'] . '|' . $_row['stocking_price']][] = $_row;
        }
        if(count($_res) > 1){
            return false;
        }

        //自動入庫許可フラグ
        $target = $receivings[0];
        if(false === (bool)$target['is_auto_storage']){
            return false;
        }

        //在庫設定があるか
        $res = $this->TrnStock->find('all', array(
            'conditions' => array(
                'MstItemUnit.mst_facility_item_id'   => $target['mst_facility_item_id'],
                'MstItemUnit.is_deleted'             => false,
                'TrnStock.mst_department_id'         => $this->Storage->getDepartmentId($this->Session->read('Auth.facility_id_selected'), Configure::read('DepartmentType.warehouse')),
                'TrnStock.is_deleted'                => false,
                'MstItemUnit.per_unit <= '           => $receivings[0]['per_unit'],
            ),
            'fields' => array(
                'TrnStock.*',
                'MstFixedCount.*',
                'MstItemUnit.*',
            ),
            'joins' => array(
                array(
                    'type' => 'left',
                    'alias' => 'MstFixedCount',
                    'table' => 'mst_fixed_counts',
                    'conditions' => array('MstFixedCount.trn_stock_id = TrnStock.id',
                                          'MstFixedCount.is_deleted = false'),
                ),
                array(
                    'type' => 'inner',
                    'alias' => 'MstItemUnit',
                    'table' => 'mst_item_units',
                    'conditions' => 'TrnStock.mst_item_unit_id = MstItemUnit.id',
                )
            ),
            'recursive' => -1,
        ));

        if(0 === count($res)){
            //定数の設定がない
            foreach($stocks[$target['id']] as $_row){
                if(true === (bool)$_row['is_standard']){
                    return $_row;
                }
            }
            return false;
        }

        $stockTarget = array();
        //在庫テーブルチェック@在庫不足
        foreach($res as $_row){
//            if(true === $this->hasStockLess($_row)){
                $stockTarget[] = $_row;
//            }
        }

        if(0 === count($stockTarget)){
            //在庫品の定数不足がない
            foreach($stocks[$target['id']] as $_row){
                if(true === (bool)$_row['is_standard']){
                    return $_row;
                }
            }
            return false;
        }else{
            return $this->selectStorageRecord($stockTarget);
        }
    }

    /**
     * 在庫不足があるかどうか
     * @param array $fixed
     * @access private
     * @return boolean
     */
    private function hasStockLess($stock){
        return (((is_null($stock['MstFixedCount']['fixed_count'])?0:$stock['MstFixedCount']['fixed_count'])
                 - ($stock['TrnStock']['stock_count'] - $stock['TrnStock']['reserve_count']
                    - $stock['TrnStock']['promise_count'])) > 0);
    }

    private function selectStorageRecord($stocks){
        $target = array();
        $stock= array();
        $standard= array();
        $per_unit= array();
        $sort = array();
        $fixed_count = array();

        $minus_target = array();
        $minus_stock= array();
        $minus_standard= array();
        $minus_per_unit= array();
        $minus_sort = array();
        $minus_fixed_count = array();

        $ret = array();

        foreach($stocks as $row){
            //在庫数を計算 在庫数 - 予約数 - 被要求数
            $promisable = $row['TrnStock']['stock_count'] - $row['TrnStock']['reserve_count'] - $row['TrnStock']['promise_count'];

            //引当可能数がマイナスの場合、最優先で入庫させる。
            if ($promisable < 0) {
                //引当可能数に対する不足数
                $stockcount = abs($promisable) * $row['MstItemUnit']['per_unit'];
                //引当可能数÷定数で割合を算出。少ないものから優先に入庫
                if (is_null($row['MstFixedCount']['fixed_count']) || $row['MstFixedCount']['fixed_count'] == 0) {
                    $average = 1;
                } else {
                    $average = abs($promisable / (is_null($row['MstFixedCount']['fixed_count'])?0:$row['MstFixedCount']['fixed_count']));
                }
                // 不足数、代表フラグ、入数の並びの配列にデータを入れる
                $minus_target[] = array(
                    'stock'       => $stockcount ,
                    'standard'    => ($row['MstItemUnit']['is_standard'] == true ? '1':'0'),
                    'per_unit'    => $row['MstItemUnit']['per_unit'] ,
                    'data'        => $row,
                    'sort'        => $average,
                    'fixed_count' => is_null($row['MstFixedCount']['fixed_count'])?0:$row['MstFixedCount']['fixed_count']
                );

                $promisable = $row['TrnStock']['stock_count'] - $row['TrnStock']['reserve_count'] - $row['TrnStock']['promise_count'] + abs($promisable);
                //定数に対して不足している数量を計算
                $stockcount = ((is_null($row['MstFixedCount']['fixed_count'])?0:$row['MstFixedCount']['fixed_count']) - $promisable )*$row['MstItemUnit']['per_unit'] ;
                //定数より在庫が多かった場合は不足０とする
                if($stockcount < 0){
                    $stockcount = 0;
                }

                //引当可能数÷定数で割合を算出。少ないものから優先に入庫
                if (is_null($row['MstFixedCount']['fixed_count']) || $row['MstFixedCount']['fixed_count'] == 0) {
                    $average = PHP_INT_MAX - 1;
                } else {
                    $average = $promisable / (is_null($row['MstFixedCount']['fixed_count'])?0:$row['MstFixedCount']['fixed_count']);
                }

                // 不足数、代表フラグ、入数の並びの配列にデータを入れる
                $target[] = array(
                    'stock'       => $stockcount ,
                    'standard'    => ($row['MstItemUnit']['is_standard'] == true ? '1':'0'),
                    'per_unit'    => $row['MstItemUnit']['per_unit'] ,
                    'data'        => $row,
                    'sort'        => $average,
                    'fixed_count' => is_null($row['MstFixedCount']['fixed_count'])?0:$row['MstFixedCount']['fixed_count']
                );

            } else {
                //定数に対して不足している数量を計算
                $stockcount = ((is_null($row['MstFixedCount']['fixed_count'])?0:$row['MstFixedCount']['fixed_count']) - $promisable )*$row['MstItemUnit']['per_unit'] ;
                //定数より在庫が多かった場合は不足０とする
                if($stockcount < 0){
                    $stockcount = 0;
                }


                //引当可能数÷定数で割合を算出。少ないものから優先に入庫
                if (is_null($row['MstFixedCount']['fixed_count']) || $row['MstFixedCount']['fixed_count'] == 0) {
                    $average = PHP_INT_MAX - 1;
                } else {
                    $average = $promisable / (is_null($row['MstFixedCount']['fixed_count'])?0:$row['MstFixedCount']['fixed_count']);
                }

                // 不足数、代表フラグ、入数の並びの配列にデータを入れる
                $target[] = array(
                    'stock'       => $stockcount ,
                    'standard'    => ($row['MstItemUnit']['is_standard'] == true ? '1':'0'),
                    'per_unit'    => $row['MstItemUnit']['per_unit'] ,
                    'data'        => $row,
                    'sort'        => $average,
                    'fixed_count' => is_null($row['MstFixedCount']['fixed_count'])?0:$row['MstFixedCount']['fixed_count']
                );

            }

        }

        foreach($target as  $row){
            $stock[]       = $row['stock'];
            $standard[]    = $row['standard'];
            $per_unit[]    = $row['per_unit'];
            $sort[]        = $row['sort'];
            $fixed_count[] = $row['fixed_count'];
        }
        array_multisort($sort,SORT_ASC, $fixed_count,SORT_DESC,$standard,SORT_DESC,$per_unit,SORT_DESC,$target);

        foreach($minus_target as  $row){
            $minus_stock[]       = $row['stock'];
            $minus_standard[]    = $row['standard'];
            $minus_per_unit[]    = $row['per_unit'];
            $minus_sort[]        = $row['sort'];
            $minus_fixed_count[] = $row['fixed_count'];
        }
        array_multisort($minus_sort,SORT_ASC, $minus_fixed_count,SORT_DESC,$minus_standard,SORT_DESC,$minus_per_unit,SORT_DESC,$minus_target);

        $ret['plus'] = $target;
        $ret['minus'] = $minus_target;

        return $ret;
        //return $target;
    }


    /**
     * AJAX Method
     * 最大作成可能数を取得
     *
     * @param string $itemId
     * @access public
     * @return JSON
     */
    public function get_stocks_table($itemId){
        Configure::write('debug', 0);
        if(false === $this->RequestHandler->isAjax()){
            return;
        }
        $this->layout = 'ajax';
        $receivings = $this->Session->read('Storages.receivings');
        $_s = $this->Session->read('Storages.stocks');

        $target = array();
        foreach($receivings as $_row){
            if($_row['id'] == $itemId){
                $target = $_row;
                break;
            }
        }

        $total = (integer)$target['per_unit'];
        $stocks = $_s[$target['id']];
        for($i=0;$i<count($stocks);$i++){
            $stocks[$i]['maxCreatableCount'] = $total / (integer)$stocks[$i]['per_unit'];
        }
        $this->set('Stocks', $stocks);
        $this->render('/storages/stocks');
    }


    /**
     * 入荷情報検索
     * @param array $_params
     * @access private
     * @return array
     */
    private function getReceivings($_params){
        $sql =
            ' select ' .
                ' TrnReceiving.*' .
                ',MstItemUnit.mst_facility_item_id'.
                ',MstItemUnit.mst_facility_id'.
                ',MstItemUnit.per_unit'.
                ',MstItemUnit.mst_unit_name_id'.
                ',MstItemUnit.is_standard'.
                ',MstItemUnit.per_unit_name_id'.
                ',MstFacilityItem.mst_facility_id'.
                ',MstFacilityItem.internal_code'.
                ',MstFacilityItem.item_name'.
                ',MstFacilityItem.standard'.
                ',MstFacilityItem.item_code'.
                ',MstFacilityItem.jan_code'.
                ',MstFacilityItem.mst_unit_name_id'.
                ',MstFacilityItem.unit_price'.
                ',MstFacilityItem.price'.
                ',MstFacilityItem.refund_price'.
                ',MstFacilityItem.insurance_claim_type'.
                ',MstFacilityItem.insurance_claim_code'.
                ',MstFacilityItem.item_category1'.
                ',MstFacilityItem.item_category2'.
                ',MstFacilityItem.item_category3'.
                ',MstFacilityItem.biogenous_type'.
                ',MstFacilityItem.jmdn_code'.
                ',MstFacilityItem.class_separation'.
                ',MstFacilityItem.tax_type'.
                ',MstFacilityItem.converted_num'.
                ',MstFacilityItem.item_type'.
                ',MstFacilityItem.mst_owner_id'.
                ',MstFacilityItem.medical_code'.
                ',MstFacilityItem.mst_dealer_id'.
                ',MstFacilityItem.is_lowlevel'.
                ',MstFacilityItem.is_auto_storage'.
                ',MstFacilityItem.lot_ubd_alert'.
                ',MstFacilityItem.spare_key7 as type_name'.
                ',MstFacilityItem.spare_key8 as class_name'.
                ',x.sup_count'.
                ',COALESCE(MstFacilityItem.expired_date , MstFacility2.days_before_expiration) as expired_date'.
                ',MstDealer.dealer_name'.
                ",(case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstParUnitName.unit_name || ')' end  ) as packing_name ".
                ',MstUnitName.unit_name as MstUnitName__unit_name'.
                ',MstParUnitName.unit_name as MstParUnitName__unit_name'.
                ',MstFacility.facility_name'.
                ',TrnClaim.id as trn_claim_id'.
                ',TrnOrder.order_type as TrnOrder__order_type'.
                ',TrnOrder.work_date as TrnOrder__work_date'.
                ',MstUser.user_name'.
                ',MstClasses.name as work_type_name'.
            ' from ' .
                ' trn_receivings as TrnReceiving '.
                ' left join trn_orders as TrnOrder on TrnReceiving.trn_order_id = TrnOrder.id'.
                ' inner join mst_item_units as MstItemUnit on MstItemUnit.id = TrnReceiving.mst_item_unit_id' .
                ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id '.
                ' left join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id' .
                ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id'.
                ' inner join mst_unit_names as MstParUnitName on MstItemUnit.per_unit_name_id = MstParUnitName.id'.
                ' inner join mst_departments as MstDepartment on TrnReceiving.department_id_from = MstDepartment.id ' .
                ' inner join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id' .
                ' inner join mst_facilities as MstFacility2 on MstFacilityItem.mst_facility_id = MstFacility2.id' .
                ' inner join mst_users as MstUser on TrnReceiving.modifier = MstUser.id ' .
                " left join trn_claims as TrnClaim on TrnReceiving.id = TrnClaim.trn_receiving_id and TrnClaim.is_stock_or_sale = '1'".
                ' left join ( select count(e.id) as sup_count, substr(c.jan_code, 1, 12) as jan from trn_receivings as a left join mst_item_units as b on b.id = a.mst_item_unit_id left join mst_facility_items as c on c.id = b.mst_facility_item_id left join trn_orders as d on d.id = a.trn_order_id left join mst_departments as e on e.id = d.department_id_to left join mst_departments as f on f.id = d.department_id_from where a.is_deleted = false and a.remain_count > 0 and a.receiving_type = ' . Configure::read('ReceivingType.arrival') . ' and c.item_type = ' . Configure::read('Items.item_types.normalitem') . ' and f.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected').' group by substr(c.jan_code, 1, 12) ) as x ' .
                  ' on x.jan = substr(MstFacilityItem.jan_code , 1 , 12) '.
                ' left join mst_classes as MstClasses on MstClasses.id = TrnReceiving.work_type '.
            ' where ' .
                $_params .
                ' and TrnReceiving.receiving_type = ' . Configure::read('ReceivingType.arrival') .
                ' and TrnReceiving.remain_count > 0' .
                ' and TrnReceiving.is_deleted = false '.
            ' order by ' .
                ' TrnReceiving.id asc';

        $_res = $this->TrnReceiving->query($sql);
        $_orderType = Configure::read('OrderTypes.orderTypeName');
        $_orderType[''] = '';
        $_result = array();
        foreach($_res as $_row){
            $_r = array();
            array_walk_recursive($_row, array('StoragesController','BuildSearchResult'), &$_r);
            //$_r['packing_name']        = $this->Storage->getPackingName($_row['mstunitname']['unit_name'],$_row[0]['per_unit'],$_row['mstparunitname']['unit_name']);
            $_r['order_type_name']     = $_orderType[$_row['trnorder']['order_type']];
            $_r['order_work_date']     = $_row['trnorder']['work_date'];
            $_r['receiving_work_date'] = $_row[0]['work_date'];
            $_result[] = $_r;
            $this->TrnReceiving->query("select * from trn_receivings where id = " . $_row[0]['id'] . " for update ");
        }
        return $_result;
    }

    /**
     * 検索条件を取得
     * @param array $_rh
     * @param array $_rs
     * @param array $_fi
     * @param array $_md
     * @param array $_to
     * @param array $_mf
     * @return array
     */
    private function getSearchParamater($_rh = array(), $_rs = array(), $_fi = array(), $_md = array(), $_to = array(), $_mf = array()){
        $where = ' 1 = 1 ';

        //2010/09/29@センター間移動の入庫ができないといけない為、ヘッダの参照はしない！
        foreach($_rh as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnReceiving.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnReceiving.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND TrnReceiving.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_rs as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnReceiving.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnReceiving.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND TrnReceiving.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_fi as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND MstFacilityItem.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND MstFacilityItem.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND MstFacilityItem.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_md as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND MstDealer.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND MstDealer.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND MstDealer.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_to as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnOrder.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnOrder.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND TrnOrder.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_mf as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND MstFacility.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND MstFacility.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND MstFacility.{$_key} '{$_val}' ";
                }
            }
        }

        return $where;
    }

    /**
     * Use this with php function: array_walk_recursive
     * @param mixed $item
     * @param string $key
     * @param array $res
     * @access public
     * @static
     */
    public static function BuildSearchResult($item, $key, $res){
        //HACK:  最上位の配列値を優先するようにしてます・・
        if(false === isset($res[$key])){
            $res[$key] = $item;
        }
    }

    //入庫ヘッダに紐付くシールで医事シールを印字
    private function getCostSeal($id = null ){
        $result = array();
        $sql = '';
        if(!is_null($id)){
            $sql .=' select ';
            $sql .='      e.internal_code';
            $sql .='    , f.dealer_name';
            $sql .='    , c.lot_no';
            $sql .='    , c.validated_date';
            $sql .='    , e.item_name';
            $sql .='    , e.item_code';
            $sql .='    , d.per_unit';
            $sql .='    , e.refund_price';
            $sql .='    , e.standard';
            $sql .='    , e.medical_code';
            $sql .='    , g.insurance_claim_name_s';
            $sql .='    , h.const_nm as insurance_claim_department_name';
            $sql .='    , (  ';
            $sql .='      case e.biogenous_type  ';

            $_biogenous_types = Configure::read('FacilityItems.biogenous_types');
            foreach($_biogenous_types as $v=>$n){
                $sql .=' when \'' .$v.'\' then \'' . $n . '\' ';
            }

            $sql .='        else \'\'  ';
            $sql .='        end ';
            $sql .='    )  as biogenous_type_name ';
            $sql .='  from ';
            $sql .='    trn_storage_headers as a ';
            $sql .='    left join trn_storages as b ';
            $sql .='      on a.id = b.trn_storage_header_id ';
            $sql .='      and a.is_deleted = false ';
            $sql .='    left join trn_stickers as c ';
            $sql .='      on c.id = b.trn_sticker_id ';
            $sql .='      and c.is_deleted = false ';
            $sql .='    left join mst_item_units as d ';
            $sql .='      on d.id = c.mst_item_unit_id ';
            $sql .='      and d.is_deleted = false ';
            $sql .='    left join mst_facility_items as e ';
            $sql .='      on e.id = d.mst_facility_item_id ';
            $sql .='      and e.item_type = ' . Configure::read('Items.item_types.normalitem');
            $sql .='      and e.is_deleted = false ';
            $sql .='    left join mst_dealers as f ';
            $sql .='      on f.id = e.mst_dealer_id ';
            $sql .='    left join mst_insurance_claims as g ';
            $sql .='      on g.insurance_claim_code = e.insurance_claim_code ';
            $sql .='    left join mst_consts as h ';
            $sql .='      on h.const_cd = e.insurance_claim_type::varchar ';
            $sql .="      and h.const_group_cd ='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
            $sql .=' where  a.id = ' . $id ;
            $sql .=' and e.enable_medicalcode = true ';
            $sql .=' and e.sealissue_medicalcode = true ';
            $result = $this->TrnStorage->query($sql);
        }
        return $result;
    }
}
?>
