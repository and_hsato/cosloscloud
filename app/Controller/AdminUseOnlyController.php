<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class AdminUseOnlyController extends AppController {
    var $name = 'AdminUseOnly';
    /**
     * @var array $uses
     */
    var $uses = array(
        'MstUser'
        );

    public function index(){
        $res = $this->getPgTables();
        $this->set('tables', $res);
        $this->set('tblName', '');
        $this->render('/admin/index');
    }

    public function show(){
        $res = $this->getPgTables();
        $this->set('tables', $res);
        $tbl = $this->request->data['tblName'];
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        $res = $this->getAllRecords($tbl, $this->_getLimitCount());
        $this->set('records', $res);
        $this->set('tblName', $tbl);
        $this->render('/admin/index');
    }

    public function exportAsTsv(){
        $tbl = $this->request->data['tblName'];
        $this->deleteSortInfo();
        $res = $this->getAllRecords($tbl, -1, true);
        header("Content-type: application/octet-stream");
        header("Content-disposition: attachment;filename={$tbl}.csv");
        print join("\t", array_keys($res[0]));
        print "\n";
        foreach($res as $_row){
            print join("\t", array_values($_row));
            print "\n";
        }
        exit();
    }

    public function exportAsXML(){
        $tbl = $this->request->data['tblName'];
        $this->deleteSortInfo();
        $res = $this->getAllRecords($tbl, -1);
        header("Content-type: application/octet-stream");
        header("Content-disposition: attachment;filename={$tbl}.xml");
        print '<?xml version="1.0" encoding="UTF-8"?>';
        print "\n";
        print "<exportdata>\n";
        print "<datatable>\n";
        print "<columns>" . join("\t",$this->outputFilter(array_keys($res[0]))) . "</columns>\n";
        print "<rows>\n";
        foreach($res as $_row){
            print "<row>" . join("\t",$this->outputFilter(array_values($_row))) . "</row>\n";
        }
        print "</rows>\n";
        print "</datatable>\n";
        print "</exportdata>\n";
        exit;
    }

    public function executeSQL(){
        $res = $this->getPgTables();
        $this->set('tables', $res);
        $tbl = $this->request->data['tblName'];
        $sql = $this->request->data['SQL'];

        $this->MstUser->begin();
        $res = $this->MstUser->query($sql , false , false );
        if(false === $res){
            $this->MstUser->rollback();
            throw new Exception('SQL発行エラー:' . $sql);
        }else{
            $this->MstUser->commit();
        }
        $result = array();
        $this->set('sqlResult', "{$sql}");
        foreach($res as $_row){
            $r = array();
            $k = array_keys($_row[0]);
            foreach($k as $key){
                    $r[$key] = $_row[0][$key];
            }
            $result[] = $r;
        }
        $this->set('records', $result);
        $this->set('tblName', $tbl);
        $this->render('/admin/index');
    }

    private function getPgTables(){
        if($this->Session->check('Admin.db')){
            $db = $this->Session->read('Admin.db');
        }else{
            $db = new DATABASE_CONFIG();
            $this->Session->write('Admin.db' , $db);
        }
        
        $tblown =  $db->default['login'];
        $sql = "select tablename from pg_tables where tableowner = '".$tblown."' and schemaname = 'public' order by tablename ";
        $res = $this->MstUser->query($sql);
        $tables = array();
        foreach($res as $_row){
            $tables[] = $_row[0]['tablename'];
        }
        return $tables;
    }


    private function getAllRecords($tblName, $limit, $addQuote = false){
        $o = $this->getSortOrder();
        if(isset($o)){
            $order = substr($o[0], 1);
        }else{
            $order = ' 1 asc ';
        }

        $sql =
            " select * from {$tblName} " .
            ' where ' .
                ' 1 = 1 '.
            ' order by ' .
                $order .
            ($limit > 0 ? " limit $limit" : '');
        $res = $this->MstUser->query($sql , false , false );

        $result = array();
        foreach($res as $_row){
            $r = array();
            $k = array_keys($_row[0]);
            foreach($k as $key){
                if(true === $addQuote){
                    $r['"' . $key . '"'] = '"' . $_row[0][$key] . '"';
                }else{
                    $r[$key] = $_row[0][$key];
                }
            }
            $result[] = $r;
        }
        return $result;
    }


}
?>
