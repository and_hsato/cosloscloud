<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of analyze_controller
 */
class AnalyzeController extends AppController {
    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'export';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    var $name = 'Analyze';
    var $uses = array(
        'MstUser',
        'MstAnalyze',
        'MstFacility',
        'MstDepartment'
    );

    var $helpers = array('Form', 'Html', 'Time');

    var $components = array('RequestHandler','CsvWriteUtils');

    public function index(){
        $joins=array();
        $joins[]=array('type'=>'INNER'
                      ,'table'=>'mst_facility_relations'
                      ,'alias'=>'MstFacilityRelation'
                      ,'conditions'=>array('MstFacilityRelation.partner_facility_id = MstFacility.id'
                                          ,'MstFacilityRelation.is_deleted = false'
                                          ,'MstFacilityRelation.mst_facility_id' => $this->Session->read('Auth.facility_id_selected')));

        //分析データ
        $sql  = ' select ';
        $sql .= '       a.id                       as "MstAnalyze__id"';
        $sql .= '     , coalesce(b.title, a.title) as "MstAnalyze__title" ';
        $sql .= '   from ';
        $sql .= '     mst_analyzes as a  ';
        $sql .= '     left join mst_analyze_orders as b  ';
        $sql .= '       on b.mst_analyze_id = a.id  ';
        $sql .= '       and b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '    inner join mst_analyze_roles as c ';
        $sql .= '      on c.mst_analyze_id = a.id ';
        $sql .= '      and c.mst_role_id = ' . $this->Session->read('Auth.MstUser.mst_role_id');
        $sql .= '      and c.is_deleted = false ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and (  ';
        $sql .= '       a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '       or a.mst_facility_id is null ';
        $sql .= '     )  ';
        $sql .= '     and (b.is_hidden is null or b.is_hidden = false)  ';
        $sql .= '   order by ';
        $sql .= '     b.order_num ';
        $sql .= '     , a.id ';

        $res = Set::Combine($this->MstAnalyze->query($sql),'{n}.MstAnalyze.id','{n}.MstAnalyze.title');

        //施主
        $donor = Set::Combine($this->MstFacility->find('all',array(
            'recursive' => -1,
            'conditions' => array(
                'MstFacility.is_deleted' => false,
                'MstFacility.facility_type' => Configure::read('FacilityType.donor'),
            ),
            'joins' =>$joins,
            'order'=>'MstFacility.facility_code'
        )),'{n}.MstFacility.id','{n}.MstFacility.facility_name');

        //センター、病院はユーザに関連づいているものを表示
        $joins[]=array('type'=>'INNER'
                      ,'table'=>'mst_user_belongings'
                      ,'alias'=>'MstUserBelonging'
                      ,'conditions'=>array('MstUserBelonging.mst_facility_id = MstFacilityRelation.partner_facility_id'
                                          ,'MstUserBelonging.is_deleted = false'
                                          ,'MstUserBelonging.mst_user_id' => $this->Session->read('Auth.MstUser.id')));


        //施設
        $selected_facility = $this->MstFacility->find('all',array(
            'recursive' => -1,
            'conditions' => array(
                'MstFacility.is_deleted' => false,
                'MstFacility.id' => $this->Session->read('Auth.facility_id_selected'),
            ),
            'order'=>'MstFacility.facility_type desc , MstFacility.facility_code'
        ));
        $facility_list = $this->MstFacility->find('all',array(
            'recursive' => -1,
            'conditions' => array(
                'MstFacility.is_deleted' => false,
                'MstFacility.facility_type' => array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')),
            ),
            'joins' =>$joins,
            'order' =>'MstFacility.facility_type desc , MstFacility.facility_code',
        ));
        $facility_list = array_merge($selected_facility,$facility_list);
        $facilities = Set::Combine($facility_list,'{n}.MstFacility.id','{n}.MstFacility.facility_name');

        $this->set('items', $res);
        $this->set('donors', $donor);
        $this->set('facilities', $facilities);
        $this->render('/analyze/index');
    }

    //大量データの場合、ブラウザ側でタイムアウトになるので
    //メソッド名を変えて、ブラウザで受け取らせない様に変更
    public function output_csv(){
        $sql  = ' select ';
        $sql .= '       a.sql_data                 as "MstAnalyze__sql_data"';
        $sql .= '     , a.split_table_flg          as "MstAnalyze__split_table_flg"';
        $sql .= '     , coalesce(b.title, a.title) as "MstAnalyze__title" ';
        $sql .= '     , a.delimiter_type           as "MstAnalyze__delimiter_type"';
        $sql .= '     , a.quote_type               as "MstAnalyze__quote_type"';
        $sql .= '     , a.title_selectable         as "MstAnalyze__title_selectable"';
        $sql .= '   from ';
        $sql .= '     mst_analyzes as a  ';
        $sql .= '     left join mst_analyze_orders as b  ';
        $sql .= '       on b.mst_analyze_id = a.id  ';
        $sql .= '       and b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and (  ';
        $sql .= '       a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '       or a.mst_facility_id is null ';
        $sql .= '     )  ';
        $sql .= '     and (b.is_hidden is null or b.is_hidden = false)  ';
        $sql .= '     and a.id = ' . $this->request->data['MstAnalyze']['id'];

        $tmp = $this->MstAnalyze->query($sql);
        $res = $tmp[0];
        $sql = $res['MstAnalyze']['sql_data'];
        
        $delimiter_type = $res['MstAnalyze']['delimiter_type'];
        $quote_type     = $res['MstAnalyze']['quote_type'];
        $title          = $res['MstAnalyze']['title_selectable'];
        
        //検索条件置換
        $params = $this->request->data['Analyze'];
        //操作施設
        if(isset($params['f']) && $params['f'] != ''){
            $sql = preg_replace('@(/\*操作[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*操作\*/)@i', ' ', $sql);
            $sql = preg_replace('/@操作id/i', "'{$params['f']}'", $sql);
        }

        //操作ユーザ
        if(isset($params['u']) && $params['u'] != ''){
            $sql = preg_replace('@(/\*ユーザ[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*ユーザ\*/)@i', ' ', $sql);
            $sql = preg_replace('/@ユーザid/i', "'{$params['u']}'", $sql);
        }

        //施主
        if(isset($params['donor_id']) && $params['donor_id'] != ''){
            $sql = preg_replace('@(/\*施主[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*施主\*/)@i', ' ', $sql);
            $sql = preg_replace('/@施主id/i', "'{$params['donor_id']}'", $sql);
        }else{
            //ストアドで条件に値がない場合
            $sql = preg_replace('/@施主id/i', "null", $sql);
        }
        //施設
        if(isset($params['facility_id']) && $params['facility_id'] != ''){
            $sql = preg_replace('@(/\*施設[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*施設\*/)@i', ' ', $sql);
            $sql = preg_replace('/@施設id/i', "'{$params['facility_id']}'", $sql);
        }else{
            //ストアドで条件に値がない場合
            $sql = preg_replace('/@施設id/i', "null", $sql);
        }
        //部署
        if(isset($params['department_id']) && $params['department_id'] != ''){
            $sql = preg_replace('@(/\*部署[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*部署\*/)@i', ' ', $sql);
            $sql = preg_replace('/@部署id/i', "'{$params['department_id']}'", $sql);
        }else{
            //ストアドで条件に値がない場合
            $sql = preg_replace('/@部署id/i', "null", $sql);
        }
        //日付From
        if(isset($params['day_from']) && $params['day_from'] != ''){
            $sql = preg_replace('@(/\*日付from[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*日付from\*/)@i', ' ', $sql);
            $sql = preg_replace('/@日付from/i', "'{$params['day_from']}'", $sql);
        }else{
            //ストアドで条件に値がない場合
            $sql = preg_replace('/@日付from/i', "null", $sql);
        }
        //日付To
        if(isset($params['day_to']) && $params['day_to'] != ''){
            $sql = preg_replace('@(/\*日付to[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*日付to\*/)@i', ' ', $sql);
            $sql = preg_replace('/@日付to/i', "'{$params['day_to']}'", $sql);
        }else{
            //ストアドで条件に値がない場合
            $sql = preg_replace('/@日付to/i', "null", $sql);
        }
        //日付数値
        if(isset($params['day_num']) && $params['day_num'] != ''){
            $sql = preg_replace('@(/\*日数[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*日数\*/)@i', ' ', $sql);
            $sql = preg_replace('/@日数day/i', "'{$params['day_num']}'", $sql);
        }else{
            //ストアドで条件に値がない場合
            $sql = preg_replace('/@日数day/i', "null", $sql);
        }

        // 区切り文字
        switch($delimiter_type){
          case Configure::read('AnalyzeDelimiterType.tab'): //タブ区切り
            $this->CsvWriteUtils->setDelimiter("\t");
            break;
          case Configure::read('AnalyzeDelimiterType.comma'): //カンマ区切り
            $this->CsvWriteUtils->setDelimiter(",");
            break;
        }
        
        // 囲み文字
        switch($quote_type){
          case Configure::read('AnalyzeQuoteType.none'): //囲みなし
            $this->CsvWriteUtils->setQuote("");
            break;
          case Configure::read('AnalyzeQuoteType.doubleQuote'): //ダブル
            $this->CsvWriteUtils->setQuote("\"");
            break;
          case Configure::read('AnalyzeQuoteType.singleQuote'): //シングル
            $this->CsvWriteUtils->setQuote("'");
            break;
        }
        

        $this->db_export_csv($sql , $res['MstAnalyze']['title'], 'index', $res['MstAnalyze']['split_table_flg'] , $title);
    }

    public function getAnalyzeMethod($id){
        Configure::write('debug', 0);
        if(false === $this->RequestHandler->isAjax()){
            return;
        }
        $this->layout = 'ajax';
        $res = $this->MstAnalyze->find('first', array(
            'fields' => array(
                'donor_selectable',
                'facility_selectable',
                'department_selectable',
                'dates_selectable',
                'daynumber_selectable'
                ),
            'recursive' => -1,
            'conditions' => array(
                'id' => $id,
            ),
            'order'=>'MstAnalyze.id'
        ));
        $this->set('value', json_encode($res));
        $this->render('/storages/ajax');
    }

    public function getDepartments($id){
        Configure::write('debug', 0);
        if(false === $this->RequestHandler->isAjax()){
            return;
        }
        $this->layout = 'ajax';
        $res = $this->MstDepartment->find('all', array(
            'fields' => array(
                'id',
                'department_name',
            ),
            'recursive' => -1,
            'conditions' => array(
                'mst_facility_id' => $id,
            ),
            'order'=>'MstDepartment.department_code'
        ));
        $this->set('value', json_encode($res));
        $this->render('/storages/ajax');
    }

}
?>
