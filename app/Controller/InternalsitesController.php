<?php
/**
 * InternalController
 *
 * @author 
 * @version 1.0.0
 * @since 
 */
class InternalsitesController extends AppController {
    
    /**
     * @var $name
     */
    var $name = 'Internalsites';
    
    /**
     * @var array $uses
     */
    var $uses = array();
    
    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time');
    
    /**
     * @var array $components
     */
    var $components = array('RequestHandler');
    
    function beforeFilter() {
        //ログインを必要としない処理の設定
        $this->Auth->allow('index','download');
    }
    
    /**
     *
     */
    function index (){
        $this->render('/internalsites/index');
    }
    
    function download($filetype = null){
        $this->autoRender = false;
        switch ($filetype){
          case 1:
            $filename = "setup.exe";
            break;
          case 2:
            $filename = "AQUA-V_Install_manual.pdf";
            $filename = mb_convert_encoding($filename, "SJIS-win","UTF-8");
            break;
          case 3:
            $filename = "DataTransfer_Instraller.zip";
            break;
          case 4:
            $filename = "DataTransfer_Install_Manual.pdf";
            $filename = mb_convert_encoding($filename, "SJIS-win","UTF-8");
            break;
          case 5:
            $filename = "BT-1000_1500UsersManual.pdf";
            $filename = mb_convert_encoding($filename, "SJIS-win","UTF-8");
            break;
          case 6:
            $filename = "PDA_Program_Update_Manual.pdf";
            $filename = mb_convert_encoding($filename, "SJIS-win","UTF-8");
            break;
          case 7:
            $filename = "AQUA-V_PDA_UserManual.pdf";
            $filename = mb_convert_encoding($filename, "SJIS-win","UTF-8");
            break;
          case 8:
            $filename = "K_apl.sb3";
            break;
          case 9:
            $filename = "AQUA-V_manual.pdf";
            $filename = mb_convert_encoding($filename, "SJIS-win","UTF-8");
            break;
          case 10:
            $filename = "BTNaviKapl.xho";
            break;
        } 
        
        $result = file_get_contents(WWW_ROOT . $filename);
        
        Configure::write('debug',0);
        header("Content-disposition: attachment; filename=" . $filename);
        header("Content-type: application/octet-stream; name=" . $filename);
        print($result);
        return;
    } 
}