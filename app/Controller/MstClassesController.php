<?php
/**
 * MstClassesController
 *
 * @version 1.0.0
 * @since 2010/07/22
 */

class MstClassesController extends AppController {
    var $name = 'MstClasses';
    
    /**
     *
     * @var array $uses
     */
    var $uses = array('MstClass','MstMenu','MstFacility','MstFacilityRelation');

    /**
     * @var array $components
     */
    var $components = array('CsvWriteUtils');
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;
    
    /**
     * @var MstClasses
     */
    var $MstClasses;

    function beforeFilter() {
        parent::beforeFilter();
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }
    
    /**
     * 作業区分一覧
     */
    function classes_list() {
        $this->setRoleFunction(94); //作業区分
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        $Claasses_List = array();
        App::import('Sanitize');
        //プルダウン作成用データ取得
        $this->set('Menu_List',$this->getMenu());
        $this->set('Facility_List',$this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                          array(Configure::read('FacilityType.center')),
                                                          true,
                                                          array('id','facility_name')));
        
        //初回時でない場合のみデータを検索する
        if(isset($this->request->data['MstClass']['is_search'])){
            $where  ='   where 1 = 1 ';
            //作業区分情報の取得
            //ユーザ入力値による検索条件の作成--------------------------------------------
            //コード(LIKE検索)
            if((isset($this->request->data['MstClass']['search_code'])) and ($this->request->data['MstClass']['search_code'] != "")){
                $where .= " and c.code LIKE '%".Sanitize::escape($this->request->data['MstClass']['search_code'])."%'";
            }
            //名称(LIKE検索)
            if((isset($this->request->data['MstClass']['search_name'])) and ($this->request->data['MstClass']['search_name'] != "")){
                $where .= " and c.name LIKE '%".Sanitize::escape($this->request->data['MstClass']['search_name'])."%'";
            }
            //備考(LIKE検索)
            if((isset($this->request->data['MstClass']['search_comment'])) and ($this->request->data['MstClass']['search_comment'] != "")){
                $where .= " and c.comment LIKE '%".Sanitize::escape($this->request->data['MstClass']['search_comment'])."%'";
            }
            
            //表示箇所(完全一致)
            if((isset($this->request->data['MstClass']['search_mst_menu_id'])) and ($this->request->data['MstClass']['search_mst_menu_id'] != "")){
                $where .= " and c.mst_menu_id = " .Sanitize::escape($this->request->data['MstClass']['search_mst_menu_id']) ;
            }
            //施設(完全一致)
            if((isset($this->request->data['MstClass']['search_mst_facility_id'])) and ($this->request->data['MstClass']['search_mst_facility_id'] != "")){
                $where .= ' and c.mst_facility_id = ' . Sanitize::escape($this->request->data['MstClass']['search_mst_facility_id']);
            }
            //検索条件の作成終了---------------------------------------------------------
            
            
            $order = ' order by ' ;
            if($this->getSortOrder() === null){
                $order .= 'b.facility_code, d.id, c.code';
            }else{
                $tmp = $this->getSortOrder();
                $order .= $tmp[0];
            }

            $sql  = 'select ';
            $sql .= '      c.id              as "MstClass__id"';
            $sql .= '    , b.facility_name   as "MstClass__facility_name"';
            $sql .= '    , d.category1       as "MstClass__category1"';
            $sql .= '    , c.code            as "MstClass__code"';
            $sql .= '    , c.name            as "MstClass__name"';
            $sql .= '    , c.comment         as "MstClass__comment" ';
            $sql .= '  from ';
            $sql .= '    mst_facility_relations as a  ';
            $sql .= '    left join mst_facilities as b  ';
            $sql .= '      on b.id = a.mst_facility_id  ';
            $sql .= '      or b.id = a.partner_facility_id  ';
            $sql .= '    left join mst_classes as c  ';
            $sql .= '      on c.mst_facility_id = b.id  ';
            $sql .= '    left join mst_menus as d  ';
            $sql .= '      on d.id = c.mst_menu_id  ';
            $sql .= $where;
            $sql .= '  and  c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '  group by ';
            $sql .= '    c.id ';
            $sql .= '    , b.facility_name ';
            $sql .= '    , b.facility_code ';
            $sql .= '    , d.id ';
            $sql .= '    , d.category1 ';
            $sql .= '    , c.code ';
            $sql .= '    , c.name ';
            $sql .= '    , c.comment  ';
            $sql .=$order;
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstClass'));
            $sql .= ' limit ' . $this->_getLimitCount();

            //SQL実行
            $Claasses_List = $this->MstClass->query($sql);
        }
        $this->set('Claasses_List',$Claasses_List);
    }


    /**
     * 新規
     */
    function add() {
        $this->setRoleFunction(94); //作業区分
        //プルダウン作成用データ取得
        $this->set('Menu_List',$this->getMenu());
        $this->set('Facility_List',$this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                          array(Configure::read('FacilityType.center')),
                                                          true,
                                                          array('id','facility_name')));
        
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }
    
    /**
     * 編集
     */
    function mod() {
        $this->setRoleFunction(94); //作業区分
        $result = $this->getData($this->request->data['MstClass']['id']);
        $this->request->data = $result[0];
    }
    

    /**
     * 結果
     */
    function result() {
        //トークン検証
        if(!isset($this->request->data['MstClass']['id'])){
            if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
                $this->request->data = $this->Session->read('result');
                $this->render('result');
                return;
            }
        }
        
        //トランザクション開始
        $this->MstClass->begin();
        //行ロック(更新のときのみ）
        if(isset($this->request->data['MstClass']['id'])){
            $this->MstClass->query(' select * from mst_classes as a where a.id = ' .$this->request->data['MstClass']['id']. ' for update ');
        }
        //********************************************************************
        //新規登録
        //********************************************************************
        if(!isset($this->request->data['MstClass']['id'])){
            $class_data = array(
                //作業区分
                'MstClass' => array(
                    'code'            => $this->request->data['MstClass']['code'],
                    'name'            => $this->request->data['MstClass']['name'],
                    'comment'         => $this->request->data['MstClass']['comment'],
                    'mst_menu_id'     => $this->request->data['MstClass']['mst_menu_id'],
                    'mst_facility_id' => $this->request->data['MstClass']['mst_facility_id']
                    )
                );
            
            
            $this->MstClass->create();
            //SQL実行
            if (!$this->MstClass->save($class_data)) {
                $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                //ロールバック
                $this->MstClass->rollback();
                $this->redirect('classes_list');
                
            }
            $this->request->data['MstClass']['id'] = $this->MstClass->getLastInsertId();
            
        } else {
            //更新
            
            //削除処理
            if((isset($this->request->data['MstClass']['delete'])) and ($this->request->data['MstClass']['delete'] == "1")){
                if (!$this->MstClass->delete($this->request->data['MstClass']['id'])) {
                    $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                    //ロールバック
                    $this->MstClass->rollback();
                    $this->redirect('classes_list');
                }
                $this->Session->write('Del' , 'Del');
            } else {
                
                $condtion = array( 'MstClass.id ='      => $this->request->data['MstClass']['id'] );
                $updatefield = array( 'code'            => "'".$this->request->data['MstClass']['code']."'",
                                      'name'            => "'".$this->request->data['MstClass']['name']."'",
                                      'comment'         => "'".$this->request->data['MstClass']['comment']."'",
                                      'mst_menu_id'     => "'".$this->request->data['MstClass']['mst_menu_id']."'",
                                      'mst_facility_id' => "'".$this->request->data['MstClass']['mst_facility_id']."'",
                                      );
                
                
                //*******************************
                //UPDATE処理
                //*******************************
                
                //MstClassオブジェクトをcreate
                $this->MstClass->create();
                
                //SQL実行
                if (!$this->MstClass->updateAll( $updatefield, $condtion )) {
                    $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                    //ロールバック
                    $this->MstClass->rollback();
                    $this->redirect('classes_list');
                }
            }
        }
        //コミット
        $this->MstClass->commit();
        
        $result = $this->getData($this->request->data['MstClass']['id']);
        $this->request->data =  $result[0];
        $this->Session->write('result', $result[0]);
    }
    
    /**
     * 機能データ取得
     */
    function getMenu() {
        //機能名の取得
        $params = array (
            'fields'     => array('MstMenu.id',
                                  'MstMenu.category1'),
            'order'      => array('MstMenu.id'),
            'conditions' => array('MstMenu.is_deleted' => FALSE),
            'recursive' => -1
            );
        
        return Set::Combine(
            $this->MstMenu->find('all',$params),
            "{n}.MstMenu.id",
            "{n}.MstMenu.category1"
            );
    }

    /*
     * データ取得
     */
    private function getData($id){
        $sql  = '  select ';
        $sql .= '      a.id                   as "MstClass__id" ';
        $sql .= '    , c.facility_name        as "MstClass__facility_name" ';
        $sql .= '    , a.mst_facility_id      as "MstClass__mst_facility_id" ';
        $sql .= '    , a.mst_menu_id          as "MstClass__mst_menu_id" ';
        $sql .= '    , b.category1            as "MstClass__catgory1" ';
        $sql .= '    , a.name                 as "MstClass__name" ';
        $sql .= '    , a.code                 as "MstClass__code" ';
        $sql .= '    , a.comment              as "MstClass__comment"  ';
        $sql .= '  from ';
        $sql .= '    mst_classes as a  ';
        $sql .= '    left join mst_menus as b  ';
        $sql .= '      on b.id = a.mst_menu_id  ';
        $sql .= '    left join mst_facilities as c  ';
        $sql .= '      on c.id = a.mst_facility_id  ';
        $sql .= '  where ';
        $sql .= '    a.id = ' . $id;
        
        return $this->MstClass->query($sql);
    }
    
    public function export_csv(){
        App::import('Sanitize');
        $where  ='   where 1 = 1 ';
        //作業区分情報の取得
        //ユーザ入力値による検索条件の作成--------------------------------------------
        //コード(LIKE検索)
        if((isset($this->request->data['MstClass']['search_code'])) and ($this->request->data['MstClass']['search_code'] != "")){
            $where .= " and c.code LIKE '%".Sanitize::escape($this->request->data['MstClass']['search_code'])."%'";
        }
            //名称(LIKE検索)
        if((isset($this->request->data['MstClass']['search_name'])) and ($this->request->data['MstClass']['search_name'] != "")){
            $where .= " and c.name LIKE '%".Sanitize::escape($this->request->data['MstClass']['search_name'])."%'";
        }
        //備考(LIKE検索)
        if((isset($this->request->data['MstClass']['search_comment'])) and ($this->request->data['MstClass']['search_comment'] != "")){
            $where .= " and c.comment LIKE '%".Sanitize::escape($this->request->data['MstClass']['search_comment'])."%'";
        }
        
        //表示箇所(完全一致)
        if((isset($this->request->data['MstClass']['search_mst_menu_id'])) and ($this->request->data['MstClass']['search_mst_menu_id'] != "")){
            $where .= " and c.mst_menu_id = " .Sanitize::escape($this->request->data['MstClass']['search_mst_menu_id']) ;
        }
        //施設(完全一致)
        if((isset($this->request->data['MstClass']['search_mst_facility_id'])) and ($this->request->data['MstClass']['search_mst_facility_id'] != "")){
            $where .= ' and c.mst_facility_id = ' . Sanitize::escape($this->request->data['MstClass']['search_mst_facility_id']);
        }
        //検索条件の作成終了---------------------------------------------------------
        $sql  = 'select ';
        $sql .= '      b.facility_name   as 施設';
        $sql .= '    , d.category1       as 表示箇所';
        $sql .= '    , c.code            as コード';
        $sql .= '    , c.name            as 名称';
        $sql .= '    , c.comment         as 備考';
        $sql .= '  from ';
        $sql .= '    mst_facility_relations as a  ';
        $sql .= '    left join mst_facilities as b  ';
        $sql .= '      on b.id = a.mst_facility_id  ';
        $sql .= '      or b.id = a.partner_facility_id  ';
        $sql .= '    left join mst_classes as c  ';
        $sql .= '      on c.mst_facility_id = b.id  ';
        $sql .= '    left join mst_menus as d  ';
        $sql .= '      on d.id = c.mst_menu_id  ';
        $sql .= $where;
        $sql .= '  and  c.mst_facility_id = ' . $this->request->data['MstClass']['mst_facility_id'];
        $sql .= '  group by ';
        $sql .= '    c.id ';
        $sql .= '    , b.facility_name ';
        $sql .= '    , b.facility_code ';
        $sql .= '    , d.id ';
        $sql .= '    , d.category1 ';
        $sql .= '    , c.code ';
        $sql .= '    , c.name ';
        $sql .= '    , c.comment  ';
        $sql .= ' order by b.facility_code, d.id, c.code';

        $this->db_export_csv($sql , "作業区分一覧", 'classes_list');
    }
}
