<?php
/**
 * D2CustomersController
 * 直納品登録
 * @version 1.0.0
 * @since 2010/05/18
 */
class D2CustomersController extends AppController {
    /**
     * @var $name
     */
    var $name = 'D2Customers';
    /**
     * @var array $uses
     */
    var $uses = array(
        'MstUser',              //ユーザ
        'MstFacilityRelation',  //施設関係
        'MstFacility',          //施設
        'MstFacilityItem',      //施設採用品
        'MstDepartment',        //部署
        'MstClass',             //作業区分
        'TrnShippingHeader',    //出荷ヘッダ
        'TrnShipping',          //出荷明細
        'MstItemUnit',          //包装単位
        'MstDealer',            //販売元
        'TrnSticker',           //シール
        'TrnPromiseHeader',     //引当ヘッダ
        'TrnPromise',           //引当明細
        'MstTransactionConfig', //仕入設定
        'MstSalesConfig',       //売上設定
        'TrnReceivingHeader',   //入荷ヘッダ
        'TrnReceiving',         //入荷明細
        'TrnStorageHeader',     //入庫ヘッダ
        'TrnStorage',           //入庫明細
        'TrnStock',             //在庫テーブル
        'TrnClaim',             //請求テーブル（売上データ）
        'MstUnitName',          //単位名
        'TrnConsumeHeader',     //消費ヘッダ
        'TrnConsume',           //消費明細
        'TrnCloseHeader',       //締め
        'TrnStickerRecord',     //シール移動履歴
        'TrnOrderHeader',       //発注ヘッダ
        'TrnOrder',             //発注明細
        'MstPrivilege',         //権限
        'TrnStickerIssue',
    );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;
    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');
    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Stickers','Barcode');

    public function beforeFilter(){
      parent::beforeFilter();
      $this->Auth->allowedActions[] = 'report';
      $this->Auth->allowedActions[] = 'seal';
    }

    /**
     * 直納品条件入力画面
     */
    function index() {
        $this->setRoleFunction(48); //直納品受領登録
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected') ,'18')); //作業区分プルダウン用データ取得

        //得意先
        $this->set('hospital_list',
                   $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                          array(Configure::read('FacilityType.hospital')),
                                          true
                                          )
                   );
        //仕入先
        $this->set('supplier_list',
                   $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                          array(Configure::read('FacilityType.supplier')),
                                          true,
                                          array('facility_code' , 'facility_name'),
                                          'facility_name, facility_code'
                                          )
                   );
        
        //$this->request->data['d2customers']['work_date'] = date('Y/m/d');
    }

    /**
     * 施設採用品選択
     * @param
     * @return
     */
    function add() {
        App::import('Sanitize');

        $SearchResult = array();
        $CartSearchResult = array();

        //初回時は検索を行わない
        if (isset($this->request->data['add_search']['is_search'])) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $limit = '' ;
            if(isset($this->request->data['limit'])){
                $limit .= 'limit ' . $this->request->data['limit'];
            }
            $order =' order by a.internal_code ,b.per_unit ';
            $where = '';
            $where .=' and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
            $where .=' and a.item_type = ' . Configure::read('Items.item_types.normalitem'); // 通常品のみ
            $where .=' and a.is_deleted = false '; //施設採用品で削除されているものは除外
            $where .=' and b.is_deleted = false '; //包装単位で適用が外れているものは除外
            $where .=' and a.is_lowlevel = false '; // 低レベル品は除外

            $where .=" and a.start_date <= '" . $this->request->data['d2customers']['work_date'] . "'"; //施設採用品開始日
            $where .=" and ( a.end_date >= '" . $this->request->data['d2customers']['work_date'] . "'"; //施設採用品終了日
            $where .=' or a.end_date is null ) ';

            $where .=" and b.start_date <= '" . $this->request->data['d2customers']['work_date'] . "'"; //包装単位開始日
            $where .=" and ( b.end_date >= '" . $this->request->data['d2customers']['work_date'] . "'"; //包装単位終了日
            $where .=' or b.end_date is null ) ';

            $where2 = $where;

            if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and b.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 .= " and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 .= " and 0 = 1 ";
            }

            //商品ID
            if(isset($this->request->data['MstFacilityItems']['internal_code']) && $this->request->data['MstFacilityItems']['internal_code'] !== ''){
                $where .=" and a.internal_code ilike '%" .$this->request->data['MstFacilityItems']['internal_code']."%'";
            }
            //規格
            if(isset($this->request->data['MstFacilityItems']['standard']) && $this->request->data['MstFacilityItems']['standard'] !== '' ){
                $where .=" and a.standard ilike '%" .$this->request->data['MstFacilityItems']['standard']."%'";
            }
            //商品名
            if(isset($this->request->data['MstFacilityItems']['item_name']) && $this->request->data['MstFacilityItems']['item_name'] !== ''){
                $where .=" and a.item_name ilike '%" .$this->request->data['MstFacilityItems']['item_name']."%'";
            }
            //製品番号
            if(isset($this->request->data['MstFacilityItems']['item_code']) && $this->request->data['MstFacilityItems']['item_code'] !== ''){
                $where .=" and a.item_code ilike '%" .$this->request->data['MstFacilityItems']['item_code']."%'";
            }
            //販売元
            if(isset($this->request->data['MstFacilityItems']['dealer_name']) && $this->request->data['MstFacilityItems']['dealer_name'] !== ''){
                $where .=" and c.dealer_name ilike '%" .$this->request->data['MstFacilityItems']['dealer_name']."%'";
            }
            //JAN(前方一致)
            if(isset($this->request->data['MstFacilityItems']['jan_code']) && $this->request->data['MstFacilityItems']['jan_code'] !== ''){
                $where .=" and a.jan_code ilike '" .$this->request->data['MstFacilityItems']['jan_code']."%'";
            }
            //APコード
            if(isset($this->request->data['MstFacilityItems']['spare_key2']) && $this->request->data['MstFacilityItems']['spare_key2'] !== ''){
                $where .=" and a.spare_key2 ilike '%" .$this->request->data['MstFacilityItems']['spare_key2']."%'";
            }
            
            //登録可能のみ表示
            if(isset($this->request->data['MstFacilityItems']['canChecked']) && $this->request->data['MstFacilityItems']['canChecked']==1){
                $where .=' and f.id is not null ';
                $where .=' and g.id is not null ';
            }
            
            // 検索（画面上）用一覧取得
            $SearchResult = $this->MstFacilityItem->query($this->searchFacilityItems($where , $order , $limit));

            // カート（画面下）用一覧取得
            $CartSearchResultTmp = $this->MstFacilityItem->query($this->searchFacilityItems($where2 , $order ));

            if(!empty($this->request->data['cart']['tmpid'])){
                //カートに追加した順に並べなおす。
                foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
                    foreach($CartSearchResultTmp as $tmp){
                        if($tmp['MstFacilityItems']['id'] == $id){
                            $CartSearchResult[] = $tmp;
                            break;
                        }
                    }
                }
            }
            $this->request->data = $data;
            $this->set('SearchResult' , $SearchResult);
            $this->set('CartSearchResult' , $CartSearchResult);
            $this->set('data' , $this->request->data);

            $this->render('add');
            //初回遷移時のみ締めチェック

        } else {
            //直納品条件入力の入力データをセッションに保持
            $this->Session->write('input_D2customer', $this->request->data['d2customers']);

            //得意先コードから施設IDを取得
            $tmp = $this->MstFacility->find('first', array('fields' => 'id', 'conditions' => array('MstFacility.facility_code' => $this->request->data['d2customers']['hospitalCode'] ,
                                                                                                   'MstFacility.facility_type'=> array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital'))
                                                                                                   ),
                                                           'recursive' => -1));
            $this->request->data['d2customers']['hospitalId'] = $tmp['MstFacility']['id'];
            unset($tmp);

            //得意先部署コードから部署IDを取得
            $tmp = $this->MstDepartment->find('first',
                    array(
                          'fields' => 'id',
                          'conditions' => array('MstDepartment.department_code' => $this->request->data['d2customers']['departmentCode'],
                                                'MstDepartment.mst_facility_id' => $this->request->data['d2customers']['hospitalId']),
                          'recursive' => -1
                         )
                    );
            $this->request->data['d2customers']['departmentId'] = $tmp['MstDepartment']['id'];
            unset($tmp);

            //仕入先コードから施設IDを取得
            $tmp = $this->MstFacility->find('first', array('fields' => 'id',
                                                           'conditions' => array('MstFacility.facility_code' => $this->request->data['d2customers']['supplierCode'] ,
                                                                                 'MstFacility.facility_type'=> array(Configure::read('FacilityType.supplier'))),

                                                           'joins' => array(
                                                               array('type'=>'inner'
                                                                     ,'table'=>'mst_facility_relations'
                                                                     ,'alias'=>'MstFacilityRelation'
                                                                     ,'conditions'=> array('MstFacilityRelation.partner_facility_id = MstFacility.id'
                                                                                           ,'MstFacilityRelation.mst_facility_id'    => $this->Session->read('Auth.facility_id_selected')
                                                                                           ,'MstFacilityRelation.is_deleted = false'
                                                                                           )
                                                                             )
                                                               ),
                                                           'recursive' => -1));
            $this->request->data['d2customers']['supplierId'] = $tmp['MstFacility']['id'];
            unset($tmp);

            //締めチェック
            if(false === $this->canUpdateAfterClose($this->request->data['d2customers']['work_date'], $this->request->data['d2customers']['supplierId'])){
                $this->Session->setFlash('仕入締めが行われている為更新できません', 'growl', array('type'=>'error'));
                $this->request->data = array();
                $this->index();
                return;
            }

            if(false === $this->canUpdateAfterClose($this->request->data['d2customers']['work_date'], $this->request->data['d2customers']['hospitalId'])){
                $this->Session->setFlash('売上締めが行われている為更新できません', 'growl', array('type'=>'error'));
                $this->request->data = array();
                $this->index();
                return;
            }

            if(false === $this->canUpdateAfterClose($this->request->data['d2customers']['work_date'], $this->Session->read('Auth.facility_id_selected'))){
                $this->Session->setFlash('在庫締めが行われている為更新できません', 'growl', array('type'=>'error'));
                $this->request->data = array();
                $this->index();
                return;
            }

            $this->set('data' , $this->request->data);
            $this->render('add');
        }
    }

    /**
     * 直納品登録シール読み込み
     */
    function read_sticker(){
        //直納品条件入力の入力データをセッションに保持
        $this->Session->write('input_D2customer', $this->request->data['d2customers']);
        
        //得意先コードから施設IDを取得
        $tmp = $this->MstFacility->find('first', array('fields' => 'id', 'conditions' => array('MstFacility.facility_code' => $this->request->data['d2customers']['hospitalCode'] ,
                                                                                               'MstFacility.facility_type'=> array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital'))
                                                                                               ),
                                                       'recursive' => -1));
        $this->request->data['d2customers']['hospitalId'] = $tmp['MstFacility']['id'];
        unset($tmp);
        
        //得意先部署コードから部署IDを取得
        $tmp = $this->MstDepartment->find('first',
                                          array(
                                              'fields' => 'id',
                                              'conditions' => array('MstDepartment.department_code' => $this->request->data['d2customers']['departmentCode'],
                                                                    'MstDepartment.mst_facility_id' => $this->request->data['d2customers']['hospitalId']),
                                              'recursive' => -1
                                              )
                                          );
        $this->request->data['d2customers']['departmentId'] = $tmp['MstDepartment']['id'];
        unset($tmp);
        
        //仕入先コードから施設IDを取得
        $tmp = $this->MstFacility->find('first', array('fields' => 'id',
                                                       'conditions' => array('MstFacility.facility_code' => $this->request->data['d2customers']['supplierCode'] ,
                                                                             'MstFacility.facility_type'=> array(Configure::read('FacilityType.supplier'))),
                                                       
                                                       'joins' => array(
                                                           array('type'=>'inner'
                                                                 ,'table'=>'mst_facility_relations'
                                                                 ,'alias'=>'MstFacilityRelation'
                                                                 ,'conditions'=> array('MstFacilityRelation.partner_facility_id = MstFacility.id'
                                                                                       ,'MstFacilityRelation.mst_facility_id'    => $this->Session->read('Auth.facility_id_selected')
                                                                                       ,'MstFacilityRelation.is_deleted = false'
                                                                                       )
                                                                 )
                                                           ),
                                                       'recursive' => -1));
        $this->request->data['d2customers']['supplierId'] = $tmp['MstFacility']['id'];
        unset($tmp);
        
        //締めチェック
        if(false === $this->canUpdateAfterClose($this->request->data['d2customers']['work_date'], $this->request->data['d2customers']['supplierId'])){
            $this->Session->setFlash('仕入締めが行われている為更新できません', 'growl', array('type'=>'error'));
            $this->request->data = array();
            $this->index();
            return;
        }
        
        if(false === $this->canUpdateAfterClose($this->request->data['d2customers']['work_date'], $this->request->data['d2customers']['hospitalId'])){
            $this->Session->setFlash('売上締めが行われている為更新できません', 'growl', array('type'=>'error'));
            $this->request->data = array();
            $this->index();
            return;
        }
        
        if(false === $this->canUpdateAfterClose($this->request->data['d2customers']['work_date'], $this->Session->read('Auth.facility_id_selected'))){
            $this->Session->setFlash('在庫締めが行われている為更新できません', 'growl', array('type'=>'error'));
            $this->request->data = array();
            $this->index();
            return;
        }
        
        $this->set('data' , $this->request->data);
        $this->render('read_sticker');
    }
    
    /**
     * 直納品登録シール読み込み確認画面
     */
    function read_confirm(){
        $ean = array();
        $result = array();
        foreach(explode(',',$this->request->data['codez']) as $barcode){
            $ean[] = $this->Barcode->readEAN128($barcode);
        }

        foreach($ean as $e){
            $ret = array();
            $data = isset($e['MstFacilityItem'][0])?$e['MstFacilityItem'][0]:array();
            if(isset($data['MstFacilityItem']['id'])){
                $ret['MstFacilityItem']['internal_code']               = $data['MstFacilityItem']['internal_code'];
                $ret['MstFacilityItem']['item_name']                   = $data['MstFacilityItem']['item_name'];
                $ret['MstFacilityItem']['item_code']                   = $data['MstFacilityItem']['item_code'];
                $ret['MstFacilityItem']['standard']                    = $data['MstFacilityItem']['standard'];
                $ret['MstFacilityItem']['class_separation']            = $data['MstFacilityItem']['class_separation'];
                $ret['MstFacilityItem']['is_lowlevel']                 = $data['MstFacilityItem']['is_lowlevel'];
                $ret['MstFacilityItem']['spare_key8']                  = $data['MstFacilityItem']['spare_key8']; // 県総すぺしゃる
                $ret['MstFacilityItem']['spare_key7']                  = $data['MstFacilityItem']['spare_key7']; // 県総すぺしゃる
                $ret['MstFacilityItem']['lot_no']                      = $e['lot_no'];
                $ret['MstFacilityItem']['validated_date']              = $this->getFormatedValidateDate($e['validated_date']);
                $itemUnit = $this->getItemUnit($data['MstFacilityItem']['id'],
                                               $this->request->data['d2customers']['supplierId'],
                                               $this->request->data['d2customers']['hospitalId'],
                                               $this->request->data['d2customers']['work_date']
                                               );
                if(!empty($itemUnit)){
                    $ret['MstFacilityItem']['item_unit'] = Set::Combine(
                        $itemUnit,
                        "{n}.MstItemUnit.id",
                        "{n}.MstItemUnit.unit_name"
                        );
                    $ret['MstFacilityItem']['sales_price']       = $itemUnit[0]['MstItemUnit']['sales_price'];
                    $ret['MstFacilityItem']['transaction_price'] = $itemUnit[0]['MstItemUnit']['transaction_price'];
                    $ret['MstFacilityItem']['check']             = true;
                }else{
                    $ret['MstFacilityItem']['item_unit']         = array();
                    $ret['MstFacilityItem']['sales_price']       = '';
                    $ret['MstFacilityItem']['transaction_price'] = '';
                    $ret['MstFacilityItem']['check']             = false;
                }
                $ret['MstFacilityItem']['dealer_name']    = $data['MstDealer']['dealer_name'];
            }else{
                $ret['MstFacilityItem']['internal_code']     = '該当商品がありません';
                $ret['MstFacilityItem']['item_name']         = '';
                $ret['MstFacilityItem']['item_code']         = '';
                $ret['MstFacilityItem']['standard']          = '';
                $ret['MstFacilityItem']['class_separation']  = '';
                $ret['MstFacilityItem']['is_lowlevel']       = '';
                $ret['MstFacilityItem']['lot_no']            = '';
                $ret['MstFacilityItem']['validated_date']    = '';
                $ret['MstFacilityItem']['item_unit']         = array();
                $ret['MstFacilityItem']['dealer_name']       = '';
                $ret['MstFacilityItem']['sales_price']       = '';
                $ret['MstFacilityItem']['transaction_price'] = '';
                $ret['MstFacilityItem']['check']             = false;
            }
            $result[] = $ret;
        }
        $this->set('result' , $result);
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'18'));

        /* トランザクショントークン */
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('d2customers.token' , $token);
        $this->request->data['d2customers']['token'] = $token;
        
    }
    
    /**
     * 直納品登録確認画面
     */
    function add_confirm() {
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'18'));

        $sql  = ' SELECT ';
        $sql .= '       MstItemUnit.id                       AS "MstItemUnit__id" ';
        $sql .= '     , MstItemUnit.per_unit                 AS "MstItemUnit__per_unit" ';
        $sql .= '     , MstUnitName.unit_name                AS "MstUnitName__unit_name" ';
        $sql .= '     , MstParUnitName.unit_name             AS "MstParUnitName__unit_name" ';
        $sql .= '     , MstFacilityItem.id                   AS "MstFacilityItem__id" ';
        $sql .= '     , MstFacilityItem.internal_code        AS "MstFacilityItem__internal_code" ';
        $sql .= '     , MstFacilityItem.item_name            AS "MstFacilityItem__item_name" ';
        $sql .= '     , MstFacilityItem.standard             AS "MstFacilityItem__standard" ';
        $sql .= '     , MstFacilityItem.item_code            AS "MstFacilityItem__item_code" ';
        $sql .= '     , MstFacilityItem.jan_code             AS "MstFacilityItem__jan_code" ';
        $sql .= '     , MstFacilityItem.class_separation     AS "MstFacilityItem__class_separation" ';
        $sql .= '     , MstFacilityItem.is_lowlevel          AS "MstFacilityItem__is_lowlevel" ';
        $sql .= '     , MstFacilityItem.spare_key8           as "MstFacilityItem__spare_key8"'; // 県総すぺしゃる
        $sql .= '     , MstFacilityItem.spare_key7           as "MstFacilityItem__spare_key7"'; // 県総すぺしゃる
        $sql .= '   FROM ';
        $sql .= '     mst_item_units AS MstItemUnit  ';
        $sql .= '     LEFT JOIN mst_unit_names AS MstUnitName  ';
        $sql .= '       ON MstItemUnit.mst_unit_name_id = MstUnitName.id ';
        $sql .= '        ';
        $sql .= '     LEFT JOIN mst_unit_names AS MstParUnitName  ';
        $sql .= '       ON MstItemUnit.per_unit_name_id = MstParUnitName.id ';
        $sql .= '        ';
        $sql .= '     LEFT JOIN mst_facility_items AS MstFacilityItem  ';
        $sql .= '       ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id ';
        $sql .= '        ';
        $sql .= '   WHERE ';
        $sql .= '     MstItemUnit.id in (' . $this->request->data['cart']['tmpid'] . ')' ;

        $FacilityItemListTmp = $this->MstItemUnit->query($sql);

        //カートに追加した順に並べなおす。
        foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
            foreach($FacilityItemListTmp as $tmp){
                if($tmp['MstItemUnit']['id'] == $id){
                    $FacilityItemList[] = $tmp;
                    break;
                }
            }
        }

        //参考仕入単価・参考売上単価の取得
        for ($i = 0; $i < count($FacilityItemList); $i++) {
            $_params = array();
            $_params['mst_item_unit_id']     = $FacilityItemList[$i]['MstItemUnit']['id'];
            $_params['mst_facility_item_id'] = $FacilityItemList[$i]['MstFacilityItem']['id'];
            $_params['start_date <= ']       = $this->request->data['d2customers']['work_date'];
            $_params['end_date > ']          = $this->request->data['d2customers']['work_date'];
            $_params['is_deleted']           = false;

            $_params['partner_facility_id']  = $this->request->data['d2customers']['supplierId'];
            //仕入設定取得
            $transaction_price = $this->MstTransactionConfig->find('first', array('fields' => 'transaction_price', 'conditions' => $_params, 'recursive' => -1));

            $_params['partner_facility_id']  = $this->request->data['d2customers']['hospitalId'];
            //売上設定取得
            $sales_price = $this->MstSalesConfig->find('first', array('fields' => 'sales_price', 'conditions' => $_params, 'recursive' => -1));

            if(false === $transaction_price){
                $FacilityItemList[$i]['MstItemUnit']['transaction_price'] = false;
            }else{
                $FacilityItemList[$i]['MstItemUnit']['transaction_price'] = $transaction_price['MstTransactionConfig']['transaction_price'];
            }
            if(false === $sales_price){
                $FacilityItemList[$i]['MstItemUnit']['sales_price'] = false;
            }else{
                $FacilityItemList[$i]['MstItemUnit']['sales_price'] = $sales_price['MstSalesConfig']['sales_price'];
            }

            if(false === $transaction_price || false === $sales_price){
                $FacilityItemList[$i]['showCheckbox'] = false;
            }else{
                $FacilityItemList[$i]['showCheckbox'] = true;
            }
        }

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('d2customers.token' , $token);
        $this->request->data['d2customers']['token'] = $token;

        $this->set('FacilityItemList', $FacilityItemList);
        $this->render('add_confirm');
    }

    /**
     * 直納品登録
     */
    function add_result() {
        if($this->request->data['d2customers']['token'] === $this->Session->read('d2customers.token')) {
            $this->Session->delete('d2customers.token');
            $this->request->data['now'] = date('Y/m/d H:i:s');
            //対象データ
            $res = $this->getCommonData();

            //トランザクションの開始
            $this->TrnOrderHeader->begin();

            //直納品登録処理。
            try {
                //在庫データ確認(無い場合は作成)
                $this->CheckStock($res);

                //発注
                $res = $this->createOrder($res);

                //入荷
                $res = $this->createReceiving($res);

                //入庫
                $res = $this->createStorage($res);

                //出荷
                $res = $this->createShipping($res);

                //受領
                $res = $this->createReceipt($res);
                
                //シール更新
                $this->updateStickerInfo($res);
                
            } catch (Exception $ex) {
                $this->TrnOrderHeader->rollback();
                throw new Exception($ex->getMessage() . "\n" . print_r($res,true));
            }

            $this->TrnOrderHeader->commit();

            $this->set('header', $this->request->data['d2customers']);
            $this->set('result', $res);

            // リロード時読み出し用にセッションにも格納しておく
            $this->Session->write('d2customers.header', $this->request->data['d2customers']);
            $this->Session->write('d2customers.result', $res);

            $this->render('add_result');
        }else{
            // トランザクショントークン不正
            // リロード or ２度押し
            $this->set('header' , $this->Session->read('d2customers.header'));
            $this->set('result' , $this->Session->read('d2customers.result'));
        }
    }

    /**
     * Override Method
     * @return array
     */
    public function  validateErrors() {
        $objects = func_get_args();
        if (empty($objects)) {
            return false;
        }

        $errors = array();
        foreach ($objects as $object) {
            $this->{$object->alias}->set($object->data);
            $errors = array_merge($errors, $this->{$object->alias}->invalidFields());
        }

        return $this->validationErrors = (!empty($errors) ? $errors : array());
    }


    /**
     * 共通データを作成＆取得
     * @access private
     * @return array
     */
    private function getCommonData() {
        $data = array();

        $data['consume_date'] = $this->request->data['d2customers']['work_date'];
        $data['recital_date'] = $this->request->data['d2customers']['recital_date'];
        $data['recital1']     = $this->request->data['d2customers']['recital1'];
        $data['recital2']     = $this->request->data['d2customers']['recital2'];

        //センター丸め区分
        $data['center_round_type'] = $this->getRoundType($this->Session->read('Auth.facility_id_selected'));
        //病院丸め区分
        $data['customer_round_type'] = $this->getRoundType($this->request->data['d2customers']['hospitalId']);

        //要求元部署（仕入先）の部署IDを取得
        $supplier_department_id = $this->MstDepartment->find('first', array('fields' => 'id', 'conditions' => array('MstDepartment.mst_facility_id' => $this->request->data['d2customers']['supplierId']), 'recursive' => -1));
        $data['supplier_department_id'] = $supplier_department_id['MstDepartment']['id'];
        //施設IDも取得
        $supplier_facility_id = $this->MstDepartment->find('first', array('fields' => 'mst_facility_id', 'conditions' => array('MstDepartment.id' => $data['supplier_department_id']), 'recursive' => -1));
        $data['supplier_facility_id'] = $supplier_facility_id['MstDepartment']['mst_facility_id'];
        $data['supplier_round_type'] = $this->getRoundType($data['supplier_facility_id']);

        //要求先であり要求元でもある部署（センター）の部署IDを取得
        $department_id_to = $this->MstDepartment->find('first', array('fields' => 'id', 'conditions' => array('MstDepartment.mst_facility_id' => $this->Session->read('Auth.facility_id_selected')), 'recursive' => -1));
        $data['center_department_id'] = $department_id_to['MstDepartment']['id'];
        //施設IDも取得
        $department_id_to = $this->MstDepartment->find('first', array('fields' => 'mst_facility_id', 'conditions' => array('MstDepartment.id' => $data['center_department_id']), 'recursive' => -1));
        $data['center_facility_id'] = $department_id_to['MstDepartment']['mst_facility_id'];

        //要求先部署（病院）の施設ID・部署IDを取得
        $data['customer_department_id'] = $this->request->data['d2customers']['departmentId'];
        $data['customer_facility_id']   = $this->request->data['d2customers']['hospitalId'];
        $data['customer_gross_type']    = $this->getGrossType($this->request->data['d2customers']['hospitalId']);

        //明細数の取得
        $data['detail_count'] = count($this->request->data['d2customers']['d2customersNo']); //施設採用品の数

        $classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),18);
        $classes[''] = '';

        $all_data = array();
        foreach ($this->request->data['d2customers']['d2customersNo'] as $fitem_id) {
            $r = $this->request->data['d2customers'][$fitem_id];
            $r['work_type_name'] = $classes[$r['classId']];
            if(empty($r['packing_name'])){
                $r['packing_name'] = $this->getPackingName($r['item_unit_id']);
            }
            $data['all_data'][] = $r;
        }
        return $data;
    }

    /**
     * 在庫データ作成
     * @param array $info
     * @return void
     */
    private function CheckStock($info) {
        foreach ($info['all_data'] as $data) {
            //センター在庫テーブルの更新対象レコードを探す-------------------------------
            $params = array(
                'recursive' => -1,
                'conditions' => array('TrnStock.is_deleted' => FALSE,
                    'TrnStock.mst_item_unit_id'  => $data['item_unit_id'], //包装単位
                    'TrnStock.mst_department_id' => $info['center_department_id'], //センター部署
                ),
            );
            $Stock = $this->TrnStock->find('count', $params);

            if(0 === $Stock) {
                //センター在庫レコードが無い場合はレコードを作成する
                $stock_data = array(
                    'id'                => false,
                    'mst_item_unit_id'  => $data['item_unit_id'],
                    'mst_department_id' => $info['center_department_id'],
                    'stock_count'       => '0',
                    'reserve_count'     => '0',
                    'promise_count'     => '0',
                    'requested_count'   => '0',
                    'receipted_count'   => '0',
                    'is_deleted'        => false,
                    'creater'           => $this->Session->read('Auth.MstUser.id'),
                    'created'           => $this->request->data['now'],
                    'modifier'          => $this->Session->read('Auth.MstUser.id'),
                    'modified'          => $this->request->data['now'],
                );

                //TrnStockオブジェクトをcreate
                $this->TrnStock->create(false);
                $this->TrnStock->save($stock_data);
                //SQL実行
                if (count($this->validateErrors($this->TrnStock)) > 0) {
                    throw new Exception('センター在庫データ作成失敗' . print_r($this->validateErrors($this->TrnStock),true));
                }
            }

            unset($params, $Stock, $stock_data);
            //病院在庫テーブルの更新対象レコードを探す-------------------------------
            $params = array(
                'recursive' => -1,
                'conditions' => array('TrnStock.is_deleted' => FALSE,
                    'TrnStock.mst_item_unit_id'  => $data['item_unit_id'], //包装単位
                    'TrnStock.mst_department_id' => $info['customer_department_id'], //病院部署
                ),
            );

            $Stock = $this->TrnStock->find('count', $params);
            if(0 === $Stock) {
                //病院在庫レコードが無い場合はレコードを作成する
                $stock_data = array(
                    'id'                => false,
                    'mst_item_unit_id'  => $data['item_unit_id'],
                    'mst_department_id' => $info['customer_department_id'],
                    'stock_count'       => '0',
                    'reserve_count'     => '0',
                    'promise_count'     => '0',
                    'requested_count'   => '0',
                    'receipted_count'   => '0',
                    'is_deleted'        => false,
                    'creater'           => $this->Session->read('Auth.MstUser.id'),
                    'created'           => $this->request->data['now'],
                    'modifier'          => $this->Session->read('Auth.MstUser.id'),
                    'modified'          => $this->request->data['now'],
                );
                $this->TrnStock->create(false);
                $this->TrnStock->save($stock_data);
                if (count($this->validateErrors($this->TrnStock)) > 0) {
                    throw new Exception('病院在庫データ作成失敗' . print_r($this->validateErrors($this->TrnStock),true));
                }
            }
        }
    }

    /**
     * 発注データ作成
     * @param array $info
     * @return array
     */
    private function createOrder($info) {
        $work_no = $this->setWorkNo4Header($info['consume_date'], '18');
        $info['direct_no'] = $work_no;
        $order_header_data = array(
            'work_no'            => $work_no,
            'work_date'          => $info['consume_date'],
            'recital'            => $info['recital1'],                     //備考
            'order_type'         => Configure::read('OrderTypes.direct'),  //発注区分：直納発注
            'order_status'       => Configure::read('OrderStatus.loaded'), //発注状態：入庫済
            'department_id_from' => $info['center_department_id'],         //要求先部署=センター
            'department_id_to'   => $info['supplier_department_id'],       //要求元部署=仕入先
            'detail_count'       => $info['detail_count'],                 //明細数
            'creater'            => $this->Session->read('Auth.MstUser.id'),
            'created'            => $this->request->data['now'],
            'modifier'           => $this->Session->read('Auth.MstUser.id'),
            'modified'           => $this->request->data['now'],
            'is_deleted'         => false,
        );

        $this->TrnOrderHeader->create(false);
        $this->TrnOrderHeader->save($order_header_data);

        if (count($this->validateErrors($this->TrnOrderHeader)) > 0) {
            throw new Exception('発注ヘッダ作成失敗' . print_r($this->validateErrors($this->TrnOrderHeader),true));
        }
        $last_order_header_id = $this->TrnOrderHeader->getLastInsertID();
        $info['trn_order_header_id'] = $last_order_header_id;

        //発注明細
        $work_seq = 0;
        for($i = 0; $i < count($info['all_data']); $i++){
            $item_data = $info['all_data'][$i];
            $work_seq++;
            $order_data = array(
                'work_no'             => $work_no,                              //作業番号
                'work_seq'            => $work_seq,                             //作業連番
                'work_date'           => $info['consume_date'],                 //作業日時
                'work_type'           => $item_data['classId'],                 //作業区分
                'recital'             => $item_data['recital'],                 //備考
                'order_type'          => Configure::read('OrderTypes.direct'),  //発注区分：直納品
                'order_status'        => Configure::read('OrderStatus.loaded'), //発注状態：入庫済
                'mst_item_unit_id'    => $item_data['item_unit_id'],            //包装単位参照キー
                'department_id_from'  => $info['center_department_id'],         //要求先部署参照キー（センター）
                'department_id_to'    => $info['supplier_department_id'],       //要求元部署参照キー（仕入先）
                'before_quantity'     => $item_data['quantity'],                //変更前数量
                'quantity'            => $item_data['quantity'],                //数量
                'remain_count'        => 0,                                     //残数
                'stocking_price'      => $item_data['transaction_price'],       //仕入れ価格
                'trn_order_header_id' => $last_order_header_id,                 //発注ヘッダ外部キー
                'fixed_num_used'      => 0,                                     //使用した定数（通常・休日・予備）
                'creater'             => $this->Session->read('Auth.MstUser.id'),
                'created'             => $this->request->data['now'],
                'modifier'            => $this->Session->read('Auth.MstUser.id'),
                'modified'            => $this->request->data['now'],
                'is_deleted'          => false,
            );
            $this->TrnOrder->create(false);

            $this->TrnOrder->save($order_data);
            if(count($this->validateErrors($this->TrnOrder)) > 0) {
                throw new Exception('発注明細作成失敗' . print_r($this->validateErrors($this->TrnOrder),true));
            }
            $last_order_id = $this->TrnOrder->getLastInsertID();
            $info['all_data'][$i]['trn_order_id'] = $last_order_id;
        }
        return $info;
    }

    /**
     * 入荷データ作成
     * @param array $info
     * @access private
     * @return array
     */
    private function createReceiving($info) {
        //入荷ヘッダ
        $work_no = $info['direct_no'];
        $receiving_header_data = array(
            'work_no'            => $work_no,
            'work_date'          => $info['consume_date'],
            'recital'            => $info['recital1'],
            'receiving_type'     => Configure::read('ReceivingType.arrival'), //入荷区分：入荷
            'receiving_status'   => Configure::read('ReceivingStatus.stock'), //入荷状態：入庫済
            'department_id_from' => $info['supplier_department_id'],          //要求元部署参照キー（仕入先）
            'department_id_to'   => $info['center_department_id'],            //要求先部署参照キー（センター）
            'detail_count'       => $info['detail_count'],                    //明細数
            'creater'            => $this->Session->read('Auth.MstUser.id'),
            'created'            => $this->request->data['now'],
            'modifier'           => $this->Session->read('Auth.MstUser.id'),
            'modified'           => $this->request->data['now'],
        );

        //TrnReceivingHeaderオブジェクトをcreate
        $this->TrnReceivingHeader->create(false);
        $this->TrnReceivingHeader->save($receiving_header_data);
        if (count($this->validateErrors($this->TrnReceivingHeader)) > 0) {
            throw new Exception('入荷ヘッダ作成エラー' . print_r($this->validateErrors($this->TrnReceivingHeader), true));
        }
        $last_receiving_header_id = $this->TrnReceivingHeader->getLastInsertID();
        $info['trn_receiving_header_id'] = $last_receiving_header_id;

        $work_seq = 0;
        for($i = 0; $i < count($info['all_data']); $i++){
            $work_seq++;
            $item_data = $info['all_data'][$i];
            $receiving_data = array(
                'id'                      => false,
                'work_no'                 => $work_no,
                'work_seq'                => $work_seq,
                'work_date'               => $info['consume_date'],
                'work_type'               => $item_data['classId'],
                'recital'                 => $item_data['recital'],
                'receiving_type'          => Configure::read('ReceivingType.arrival'), //入荷区分：入荷
                'receiving_status'        => Configure::read('ReceivingStatus.stock'), //入荷状態：入庫済
                'mst_item_unit_id'        => $item_data['item_unit_id'],               //包装単位参照キー
                'department_id_from'      => $info['supplier_department_id'],          //要求元部署参照キー（仕入先）
                'department_id_to'        => $info['center_department_id'],            //要求先部署参照キー（センター）
                'quantity'                => $item_data['quantity'],                   //数量
                'remain_count'            => 0,
                'stocking_price'          => $item_data['transaction_price'],          //仕入れ価格
                'sales_price'             => $item_data['sales_price'],                //売上価格
                'trn_sticker_id'          => '',                                       //シール参照キー
                'trn_order_id'            => $item_data['trn_order_id'],               //発注参照キー
                'trn_receiving_header_id' => $last_receiving_header_id,                //入荷ヘッダ外部キー
                'creater'                 => $this->Session->read('Auth.MstUser.id'),
                'created'                 => $this->request->data['now'],
                'modifier'                => $this->Session->read('Auth.MstUser.id'),
                'modified'                => $this->request->data['now'],
            );

            //入荷明細
            $this->TrnReceiving->create(false);
            $this->TrnReceiving->save($receiving_data);
            if (count($this->validateErrors($this->TrnReceiving)) > 0) {
                throw new Exception('入荷明細作成エラー' . print_r($this->validateErrors($this->TrnReceiving), true));
            }
            $last_receiving_id = $this->TrnReceiving->getLastInsertID(); //入荷ヘッダID
            $info['all_data'][$i]['trn_receiving_id'] = $last_receiving_id;

            //請求
            $this->TrnClaim->create(false);
            $this->TrnClaim->save(array(
                'id' => false,
                'department_id_from'  => $info['supplier_department_id'], //要求元部署参照キー（仕入先）
                'department_id_to'    => $info['center_department_id'],   //要求先部署参照キー（センター）
                'mst_item_unit_id'    => $item_data['item_unit_id'],      //包装単位参照キー
                'claim_date'          => $info['consume_date'],
                'claim_price'         => $this->getFacilityUnitPrice($info['supplier_round_type'],  $item_data['transaction_price'] , $item_data['quantity']),
                'trn_receiving_id'    => $last_receiving_id,
                'stocking_close_type' => Configure::read('StockingCloseType.none'),
                'sales_close_type'    => Configure::read('SalesCloseType.none'),
                'facility_close_type' => Configure::read('FacilityCloseType.none'),
                'is_deleted'          => false,
                'is_stock_or_sale'    => '1',
                'creater'             => $this->Session->read('Auth.MstUser.id'),
                'created'             => $this->request->data['now'],
                'modifier'            => $this->Session->read('Auth.MstUser.id'),
                'modified'            => $this->request->data['now'],
                'count'               => $item_data['quantity'],
                'unit_price'          => $item_data['transaction_price'], //仕入価格
                'round'               => $info['supplier_round_type'],
                'gross'               => Configure::read('GrossType.Seal'),
                'is_not_retroactive'  => (isset($item_data['sokyu']) && $item_data['sokyu'] == '1') ? true : false,
                'claim_type'          => Configure::read('ClaimType.stock')
            ));
            if (count($this->validateErrors($this->TrnClaim)) > 0) {
                throw new Exception('入荷＠請求データ作成エラー' . print_r($this->validateErrors($this->TrnClaim), true));
            }
            $lastClaimId = $this->TrnClaim->getLastInsertID();
            
            $this->createMsClaimRecode($lastClaimId);
            
            $info['all_data'][$i]['trn_claim_id'] = $lastClaimId;
        }
        return $info;
    }


    /**
     * 入庫データ作成
     * @param array $info
     * @return array
     */
    private function createStorage($info) {
        $sticker = array();
        $workNo = $info['direct_no'];
        $facilityCode = $this->getFacilityCode($this->Session->read('Auth.facility_id_selected'));
        $hospitalFacilityCode = $this->getFacilityCode($info['customer_facility_id']);
        $detailCount = 0;
         for($i = 0; $i < count($info['all_data']); $i++){
            $item_info = $info['all_data'][$i];
            if($item_info['is_lowlevel'] == '1'){
                continue;
            }
            //シール発行
            for($j = 0; $j < (integer)$item_info['quantity']; $j++){
                $rec = $item_info;
                $rec['quantity'] = 1; //override
                $detailCount++;
                $stickerNo = $this->getCenterStickerNo($info['consume_date'], $facilityCode);
                $hospitalStickerNo = $this->getHospitalStickerNo($info['consume_date'],$hospitalFacilityCode);
                $this->TrnSticker->create(false);
                $this->TrnSticker->save(array(
                    'facility_sticker_no' => $stickerNo,
                    'hospital_sticker_no' => $hospitalStickerNo,
                    'mst_item_unit_id'    => $item_info['item_unit_id'],
                    'mst_department_id'   => $info['center_department_id'],
                    'trade_type'          => Configure::read('ClassesType.PayDirectly'),
                    'quantity'            => 1,
                    'lot_no'              => trim($item_info['lot_no']),
                    'validated_date'      => $this->getFormatedValidateDate($item_info['validated_date']),
                    'original_price'      => $item_info['reference_transaction_price'],
                    'transaction_price'   => $item_info['transaction_price'],
//                    'sales_price'         => $item_info['sales_price'], 
                    'trn_order_id'        => $item_info['trn_order_id'],
                    'trn_receiving_id'    => $item_info['trn_receiving_id'],
                    'buy_claim_id'        => $item_info['trn_claim_id'],
                    'is_deleted'          => false,
                    'has_reserved'        => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $this->request->data['now'],
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $this->request->data['now'],
                    'class_name'          => $item_info['class_name'],
                    'type_name'           => $item_info['type_name'],
                ));
                if(count($this->validateErrors($this->TrnSticker)) > 0){
                    throw new Exception('入庫＠センターシール作成失敗' . print_r($this->validateErrors($this->TrnSticker),true));
                }
                $lastStickerId = $this->TrnSticker->getLastInsertID();
                $rec['trn_sticker_id'] = $lastStickerId;
                $rec['facility_sticker_no'] = $stickerNo;
                $rec['hospital_sticker_no'] = $hospitalStickerNo;

                //入庫明細
                $this->TrnStorage->create(false);
                $this->TrnStorage->save(array(
                    'id'                  => false,
                    'work_no'             => $workNo,
                    'work_seq'            => ($i+1)*($j+1),
                    'work_date'           => $info['consume_date'],
                    'work_type'           => $item_info['classId'], //作業区分
                    'recital'             => $item_info['recital'],
                    'storage_type'        => Configure::read('StorageType.direct'),
                    'storage_status'      => Configure::read('StorageStatus.arrival'),
                    'mst_item_unit_id'    => $item_info['item_unit_id'],
                    'mst_department_id'   => $info['center_department_id'],
                    'quantity'            => 1,
                    'trn_sticker_id'      => $lastStickerId,
                    'trn_receiving_id'    => $item_info['trn_receiving_id'],
                    'is_deleted'          => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $this->request->data['now'],
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $this->request->data['now'],
                    'facility_sticker_no' => $stickerNo,
                    'hospital_sticker_no' => $hospitalStickerNo,
                ));
                if(count($this->validateErrors($this->TrnStorage)) > 0){
                    throw new Exception('入庫＠入庫明細作成失敗' . print_r($this->validateErrors($this->TrnStorage),true));
                }
                $lastStorageId = $this->TrnStorage->getLastInsertID();
                $rec['trn_storage_id'] = $lastStorageId;

                //シール更新
                $this->TrnSticker->updateAll(array(
                    'TrnSticker.trn_storage_id' => $lastStorageId,
                ),array(
                    'TrnSticker.id' => $lastStickerId,
                ),-1);
                if(count($this->validateErrors($this->TrnSticker)) > 0){
                    throw new Exception('入庫＠センターシール更新失敗' . print_r($this->validateErrors($this->TrnSticker),true));
                }

                //シール発行履歴
                $this->TrnStickerRecord->create(false);
                $this->TrnStickerRecord->save(array(
                    'id'                  => false,
                    'move_date'           => $info['consume_date'],
                    'move_seq'            => $this->getNextRecord($lastStickerId),
                    'record_type'         => Configure::read('RecordType.direct'),
                    'record_id'           => $lastStorageId,
                    'trn_sticker_id'      => $lastStickerId,
                    'mst_department_id'   => $info['center_department_id'],
                    'facility_sticker_no' => $stickerNo,
                    'hospital_sticker_no' => $hospitalStickerNo,
                    'is_deleted'          => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $this->request->data['now'],
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $this->request->data['now'],
                ));
                if(count($this->validateErrors($this->TrnStickerRecord)) > 0){
                    throw new Exception('入庫＠センターシール移動履歴作成失敗' . print_r($this->validateErrors($this->TrnStickerRecord),true));
                }
                $sticker[] = $rec;
            }
        }

        if($detailCount > 0){
            //入庫ヘッダ作成
            $this->TrnStorageHeader->create(false);
            $this->TrnStorageHeader->save(array(
                'id'                      => false,
                'work_no'                 => $workNo,
                'work_date'               => $info['consume_date'],
                'recital'                 => $info['recital1'],
                'storage_type'            => Configure::read('StorageType.direct'),
                'storage_status'          => Configure::read('StorageStatus.arrival'),
                'mst_department_id'       => $info['center_department_id'],
                'detail_count'            => $detailCount,
                'is_deleted'              => false,
                'creater'                 => $this->Session->read('Auth.MstUser.id'),
                'created'                 => $this->request->data['now'],
                'modifier'                => $this->Session->read('Auth.MstUser.id'),
                'modified'                => $this->request->data['now'],
                'trn_receiving_header_id' => $info['trn_receiving_header_id'],
            ));
            if(count($this->validateErrors($this->TrnStorageHeader)) > 0){
                throw new Exception('入庫ヘッダ作成失敗' . print_r($this->validateErrors($this->TrnStorageHeader),true));
            }
            $lastHeaderId = $this->TrnStorageHeader->getLastInsertID();
            $info['trn_storage_header_id'] = $lastHeaderId;

            //入庫明細更新
            $this->TrnStorage->updateAll(array(
                'TrnStorage.trn_storage_header_id' => $lastHeaderId,
            ),array(
                'TrnStorage.work_no' => $workNo,
            ),-1);
            if(count($this->validateErrors($this->TrnStorage)) > 0){
                throw new Exception('入庫明細にヘッダIDの更新失敗' . print_r($this->validateErrors($this->TrnStorage),true));
            }
        }
        $info['all_data']['sticker'] = $sticker;
        return $info;
    }


    /**
     * 出荷データ作成
     * @param array $info
     * @access private
     * @return array
     */
    private function createShipping($info){
        $workNo = $info['direct_no'];
        //出荷ヘッダ
        $this->TrnShippingHeader->create(false);
        $this->TrnShippingHeader->save(array(
            'id'                 => false,
            'work_no'            => $workNo,
            'work_date'          => $info['consume_date'],
            'recital'            => $info['recital1'],
            'shipping_type'      => Configure::read('ShippingType.direct'),
            'shipping_status'    => Configure::read('ShippingStatus.loaded'), //受領済
            'department_id_from' => $info['center_department_id'],
            'department_id_to'   => $info['customer_department_id'],
            'detail_count'       => count($info['all_data']['sticker']),
            'is_deleted'         => false,
            'creater'            => $this->Session->read('Auth.MstUser.id'),
            'created'            => $this->request->data['now'],
            'modifier'           => $this->Session->read('Auth.MstUser.id'),
            'modified'           => $this->request->data['now'],
        ));
        if(count($this->validateErrors($this->TrnShippingHeader)) > 0){
            throw new Exception('出荷ヘッダ作成失敗' . print_r($this->validateErrors($this->TrnShippingHeader),true));
        }

        $lastHeaderId = $this->TrnShippingHeader->getLastInsertID();
        $info['trn_shipping_header_id'] = $lastHeaderId;

        $seq = 0;
        for($i = 0; $i < count($info['all_data']['sticker']); $i++){
            $seq++;
            $item_info = $info['all_data']['sticker'][$i];
            $this->TrnShipping->create(false);
            $this->TrnShipping->save(array(
                'id'                     => false,
                'work_no'                => $workNo,
                'work_seq'               => $seq,
                'work_date'              => $info['consume_date'],
                'work_type'              => $item_info['classId'], //作業区分
                'recital'                => $item_info['recital'],
                'shipping_type'          => Configure::read('ShippingType.direct'),
                'shipping_status'        => Configure::read('ShippingStatus.loaded'), //受領済
                'mst_item_unit_id'       => $item_info['item_unit_id'],
                'department_id_from'     => $info['center_department_id'],
                'department_id_to'       => $info['customer_department_id'],
                'quantity'               => $item_info['quantity'],
                'trn_sticker_id'         => $item_info['trn_sticker_id'],
                'is_deleted'             => false,
                'creater'                => $this->Session->read('Auth.MstUser.id'),
                'created'                => $this->request->data['now'],
                'modifier'               => $this->Session->read('Auth.MstUser.id'),
                'modified'               => $this->request->data['now'],
                'trn_shipping_header_id' => $lastHeaderId,
                'facility_sticker_no'    => $item_info['facility_sticker_no'],
                'hospital_sticker_no'    => $item_info['hospital_sticker_no'],
            ));
            if(count($this->validateErrors($this->TrnShipping)) > 0){
                throw new Exception('出荷明細作成失敗' . print_r($this->validateErrors($this->TrnShipping),true));
            }
            $info['all_data']['sticker'][$i]['trn_shipping_id'] = $this->TrnShipping->getLastInsertID();
        }
        return $info;
    }

    /**
     * 受領データ作成
     * @param array $info
     * @access private
     * @return array
     */
    private function createReceipt($info){
        $receivingWorkNo = $info['direct_no'];
        $storageWorkNo = $info['direct_no'];
        $consumeWorkNo = $info['direct_no'];

        //入荷ヘッダ作成
        $this->TrnReceivingHeader->create(false);
        $this->TrnReceivingHeader->save(array(
            'id'                     => false,
            'work_no'                => $receivingWorkNo,
            'work_date'              => $info['consume_date'],
            'recital'                => $info['recital1'],
            'receiving_type'         => Configure::read('ReceivingType.receipt'),
            'receiving_status'       => Configure::read('ReceivingStatus.stock'),
            'department_id_from'     => $info['center_department_id'],
            'department_id_to'       => $info['customer_department_id'],
            'detail_count'           => count($info['all_data']['sticker']),
            'creater'                => $this->Session->read('Auth.MstUser.id'),
            'created'                => $this->request->data['now'],
            'trn_shipping_header_id' => $info['trn_shipping_header_id'],//出荷ヘッダ参照キー
            'modifier'               => $this->Session->read('Auth.MstUser.id'),
            'modified'               => $this->request->data['now'],
        ));
        if(count($this->validateErrors($this->TrnReceivingHeader)) > 0){
            throw new Exception('受領＠入荷ヘッダ作成失敗' . print_r($this->validateErrors($this->TrnReceivingHeader),true));
        }
        $lastReceivingHeaderId = $this->TrnReceivingHeader->getLastInsertID();
        $info['receipt_trn_receiving_header_id'] = $lastReceivingHeaderId;

        //入庫ヘッダ作成
        $this->TrnStorageHeader->create(false);
        $this->TrnStorageHeader->save(array(
            'id'                      => false,
            'work_no'                 => $storageWorkNo,
            'work_date'               => $info['consume_date'],
            'recital'                 => $info['recital1'],
            'storage_type'            => Configure::read('ShippingType.direct'),       //入庫区分
            'storage_status'          => Configure::read('StorageStatus.stock'), //入庫状態
            'mst_department_id'       => $info['customer_department_id'],        //部署参照キー
            'detail_count'            => count($info['all_data']['sticker']),
            'creater'                 => $this->Session->read('Auth.MstUser.id'),
            'created'                 => $this->request->data['now'],
            'trn_receiving_header_id' => $lastReceivingHeaderId,                 //入荷ヘッダ参照キー
            'modifier'                => $this->Session->read('Auth.MstUser.id'),
            'modified'                => $this->request->data['now'],
        ));
        if(count($this->validateErrors($this->TrnStorageHeader)) > 0){
            throw new Exception('受領＠入庫ヘッダ作成失敗' . print_r($this->validateErrors($this->TrnStorageHeader),true));
        }
        $lastStorageHeaderId = $this->TrnStorageHeader->getLastInsertID();
        $info['receipt_trn_storage_header_id'] = $lastStorageHeaderId;

        if($this->Session->read('Auth.Config.D2Consumes') == '0'){
            //消費ヘッダ
            $this->TrnConsumeHeader->create(false);
            $this->TrnConsumeHeader->save(array(
                'id'                => false,
                'work_no'           => $consumeWorkNo,
                'work_date'         => $info['consume_date'],
                'recital'           => $info['recital1'],
                'use_type'          => Configure::read('UseType.direct'), //消費区分：直納品
                'mst_department_id' => $info['customer_department_id'],   //部署参照キー
                'recital_date'      => $info['recital_date'],
                'spare_recital'     => $info['recital2'],
                'detail_count'      => count($info['all_data']['sticker']),
                'creater'           => $this->Session->read('Auth.MstUser.id'),
                'created'           => $this->request->data['now'],
                'modifier'          => $this->Session->read('Auth.MstUser.id'),
                'modified'          => $this->request->data['now'],
                'is_deleted'        => false,
                ));
            if(count($this->validateErrors($this->TrnConsumeHeader)) > 0){
                throw new Exception('受領＠消費ヘッダ作成失敗' . print_r($this->validateErrors($this->TrnConsumeHeader),true));
            }
            $lastConsumeHeaderId = $this->TrnConsumeHeader->getLastInsertID();
            $info['receipt_trn_consume_header_id'] = $lastConsumeHeaderId;
        }
        //明細作成
        for($i = 0; $i < count($info['all_data']['sticker']); $i++){
            $item_info = $info['all_data']['sticker'][$i];

            //入荷明細作成
            $this->TrnReceiving->create(false);
            $this->TrnReceiving->save(array(
                'id'                      => false,
                'work_no'                 => $receivingWorkNo,
                'work_seq'                => $i + 1,
                'work_date'               => $info['consume_date'],
                'work_type'               => $item_info['classId'],                    //最初の画面で選択した作業区分,
                'recital'                 => $item_info['recital'],
                'receiving_type'          => Configure::read('ReceivingType.receipt'), //入荷区分:受領による入荷
                'receiving_status'        => Configure::read('ShippingStatus.loaded'), //入荷状態（受領済み）
                'mst_item_unit_id'        => $item_info['item_unit_id'],               //包装単位参照キー
                'department_id_from'      => $info['center_department_id'],
                'department_id_to'        => $info['customer_department_id'],
                'quantity'                => $item_info['quantity'],                   //数量
                'stocking_price'          => $item_info['transaction_price'],          //仕入れ価格
                'sales_price'             => $item_info['sales_price'],                //売上価格
                'trn_sticker_id'          => $item_info['trn_sticker_id'],             //シール参照キー
                'trn_shipping_id'         => $item_info['trn_shipping_id'],            //出荷参照キー
                'trn_order_id'            => $item_info['trn_order_id'],               //発注参照キー
                'trn_receiving_header_id' => $lastReceivingHeaderId,                   //入荷ヘッダ外部キー
                'creater'                 => $this->Session->read('Auth.MstUser.id'),
                'created'                 => $this->request->data['now'],
                'modifier'                => $this->Session->read('Auth.MstUser.id'),
                'modified'                => $this->request->data['now'],
                'facility_sticker_no'     => $item_info['facility_sticker_no'],
                'hospital_sticker_no'     => $item_info['hospital_sticker_no'],
            ));
            if(count($this->validateErrors($this->TrnReceiving)) > 0){
                throw new Exception('受領＠入荷明細作成失敗' . print_r($this->validateErrors($this->TrnReceiving),true));
            }
            $lastReceivingId = $this->TrnReceiving->getLastInsertID();
            $info['all_data']['sticker'][$i]['receipt']['trn_receiving_id'] = $lastReceivingId;

            //入庫明細
            $this->TrnStorage->create(false);
            $this->TrnStorage->save(array(
                'id'                    => false,
                'work_no'               => $storageWorkNo,
                'work_seq'              => $i + 1,
                'work_date'             => $info['consume_date'],
                'work_type'             => $item_info['classId'],
                'recital'               => $item_info['recital'],
                'storage_type'          => Configure::read('ShippingType.direct'),          //入庫区分
                'storage_status'        => Configure::read('ReceivingStatus.stock'),  //入庫状態
                'mst_item_unit_id'      => $item_info['item_unit_id'],                //包装単位参照キー
                'mst_department_id'     => $info['customer_department_id'],           //部署参照キー
                'quantity'              => $item_info['quantity'],                    //数量
                'trn_sticker_id'        => $item_info['trn_sticker_id'],              //シール参照キー
                'trn_receiving_id'      => $lastReceivingId,                          //入荷参照キー
                'trn_storage_header_id' => $lastStorageHeaderId,                      //入庫ヘッダ外部キー
                'creater'               => $this->Session->read('Auth.MstUser.id'),
                'created'               => $this->request->data['now'],
                'modifier'              => $this->Session->read('Auth.MstUser.id'),
                'modified'              => $this->request->data['now'],
                'facility_sticker_no'   => $item_info['facility_sticker_no'],
                'hospital_sticker_no'   => $item_info['hospital_sticker_no'],
            ));
            if(count($this->validateErrors($this->TrnStorage)) > 0){
                throw new Exception('受領＠入庫明細作成失敗' . print_r($this->validateErrors($this->TrnStorage),true));
            }
            $lastStorageId = $this->TrnStorage->getLastInsertID();
            $info['all_data']['sticker'][$i]['receipt']['trn_storage_id'] = $lastStorageId;

            if($this->Session->read('Auth.Config.D2Consumes') == '0'){
            //消費明細
                $this->TrnConsume->create(false);
                $this->TrnConsume->save(array(
                    'id'                    => false,
                    'work_no'               => $consumeWorkNo,
                    'work_seq'              => $i + 1,
                    'work_date'             => $info['consume_date'],
                    'work_type'             => $item_info['classId'],
                    'recital'               => $item_info['recital'],
                    'use_type'              => Configure::read('UseType.direct'), //消費区分：直納品
                    'mst_item_unit_id'      => $item_info['item_unit_id'],        //包装単位参照キー
                    'mst_department_id'     => $info['customer_department_id'],   //部署参照キー
                    'quantity'              => $item_info['quantity'],            //数量
                    'trn_sticker_id'        => $item_info['trn_sticker_id'],      //シール参照キー
                    'trn_storage_id'        => $lastStorageId,                    //入庫参照キー
                    'trn_consume_header_id' => $lastConsumeHeaderId,              //消費ヘッダ外部キー
                    'is_retroactable'       => false,
                    'creater'               => $this->Session->read('Auth.MstUser.id'),
                    'created'               => $this->request->data['now'],
                    'modifier'              => $this->Session->read('Auth.MstUser.id'),
                    'modified'              => $this->request->data['now'],
                    'is_deleted'            => false,
                    'facility_sticker_no'   => $item_info['facility_sticker_no'],
                    'hospital_sticker_no'   => $item_info['hospital_sticker_no'],
                    ));
                if(count($this->validateErrors($this->TrnConsume)) > 0){
                    throw new Exception('受領＠消費明細作成失敗' . print_r($this->validateErrors($this->TrnConsume),true));
                }
                $lastConsumeId = $this->TrnConsume->getLastInsertID();
                $info['all_data']['sticker'][$i]['receipt']['trn_consume_id'] = $lastConsumeId;

                //請求データ作成
                $this->TrnClaim->create(false);
                $this->TrnClaim->save(array(
                    'id'                  => false,
                    'department_id_from'  => $info['center_department_id'],   //要求元部署参照キー（センター）
                    'department_id_to'    => $info['customer_department_id'], //要求先部署参照キー（病院）
                    'mst_item_unit_id'    => $item_info['item_unit_id'],      //包装単位参照キー
                    'claim_date'          => $info['consume_date'],
                    'claim_price'         => $this->getHospitalClaimPrice($info['customer_gross_type'], $info['customer_round_type'], $item_info['sales_price'], $item_info['quantity']),
                    'trn_shipping_id'     => $item_info['trn_shipping_id'],
                    'trn_receiving_id'    => $lastReceivingId,
                    'trn_consume_id'      => $lastConsumeId,
                    'stocking_close_type' => Configure::read('StockingCloseType.none'),
                    'sales_close_type'    => Configure::read('SalesCloseType.none'),
                    'facility_close_type' => Configure::read('FacilityCloseType.none'),
                    'is_deleted'          => false,
                    'is_stock_or_sale'    => '2',
                    'trade_type'          => Configure::read('ClassesType.PayDirectly'),
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $this->request->data['now'],
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $this->request->data['now'],
                    'count'               => $item_info['quantity'],
                    'unit_price'          => $item_info['sales_price'], //仕入価格
                    'round'               => $info['customer_round_type'],
                    'gross'               => $info['customer_gross_type'],
                    'is_not_retroactive'  => (isset($item_info['sokyu']) && $item_info['sokyu'] == '1') ? true : false,
                    'facility_sticker_no' => $item_info['facility_sticker_no'],
                    'hospital_sticker_no' => $item_info['hospital_sticker_no'],
                    'claim_type'          => Configure::read('ClaimType.sale')
                    ));
                if(count($this->validateErrors($this->TrnClaim)) > 0){
                    throw new Exception('受領＠売上データ作成失敗' . print_r($this->validateErrors($this->TrnClaim),true));
                }
                $info['all_data']['sticker'][$i]['receipt']['trn_claim_id'] = $this->TrnClaim->getLastInsertID();

                //シール発行履歴
                $this->TrnStickerRecord->create(false);
                $this->TrnStickerRecord->save(array(
                    'id'                  => false,
                    'move_date'           => $info['consume_date'],
                    'move_seq'            => $this->getNextRecord($item_info['trn_sticker_id']),
                    'record_type'         => Configure::read('RecordType.consume'),
                    'record_id'           => $info['all_data']['sticker'][$i]['receipt']['trn_consume_id'],
                    'trn_sticker_id'      => $item_info['trn_sticker_id'],
                    'mst_department_id'   => $info['customer_department_id'],
                    'facility_sticker_no' => $item_info['facility_sticker_no'],
                    'hospital_sticker_no' => $item_info['hospital_sticker_no'],
                    'is_deleted'          => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $this->request->data['now'],
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $this->request->data['now'],
                    ));
                if(count($this->validateErrors($this->TrnStickerRecord)) > 0){
                    throw new Exception('消費＠シール移動履歴作成失敗' . print_r($this->validateErrors($this->TrnStickerRecord),true));
                }
            }
        }
        return $info;
    }

    /**
     * シール情報の更新
     * @param array $info
     * @access private
     * @retirn void
     */
    private function updateStickerInfo($info){
        foreach($info['all_data']['sticker'] as $item_info){
            if($this->Session->read('Auth.Config.D2Consumes') == '0' ){
                // 消費ありの場合
                $this->TrnSticker->updateAll(array(
                    'TrnSticker.mst_department_id' => $info['customer_department_id'],
                    'TrnSticker.trn_consume_id'    => $item_info['receipt']['trn_consume_id'],
                    'TrnSticker.trn_shipping_id'   => $item_info['trn_shipping_id'],
                    'TrnSticker.sale_claim_id'     => $item_info['receipt']['trn_claim_id'],
                    'TrnSticker.receipt_id'        => $item_info['receipt']['trn_receiving_id'],
                    'TrnSticker.quantity'          => 0,
                    'TrnSticker.modifier'          => $this->Session->read('Auth.MstUser.id'),
                    'TrnSticker.modified'          => "'" .$this->request->data['now'] . "'",
                    ),array(
                        'TrnSticker.id' => $item_info['trn_sticker_id'],
                        ),-1);
            }else{
                // 消費なしの場合
                $this->TrnSticker->updateAll(array(
                    'TrnSticker.mst_department_id' => $info['customer_department_id'],
                    'TrnSticker.trn_shipping_id'   => $item_info['trn_shipping_id'],
                    'TrnSticker.receipt_id'        => $item_info['receipt']['trn_receiving_id'],
                    'TrnSticker.modifier'          => $this->Session->read('Auth.MstUser.id'),
                    'TrnSticker.modified'          => "'" .$this->request->data['now'] . "'",
                    ),array(
                        'TrnSticker.id' => $item_info['trn_sticker_id'],
                        ),-1);
            }
            if(count($this->validateErrors($this->TrnSticker)) > 0){
                throw new Exception('消費＠シール情報更新失敗' . print_r($this->validateErrors($this->TrnSticker),true));
            }
            
            // 消費なしの場合、病院在庫を増やす
            if($this->Session->read('Auth.Config.D2Consumes') == '1' ){
                $this->updateTrnStock($item_info['trn_sticker_id']);
            }
        }
    }


    /**
     * 病院在庫増加
     */
    public function updateTrnStock($sticker_id){
        // シールIDから、在庫IDを取得する。
        $sql = " select b.id , a.mst_item_unit_id , a.mst_department_id from trn_stickers as a left join trn_stocks as b on a.mst_item_unit_id = b.mst_item_unit_id and a.mst_department_id = b.mst_department_id where a.id = " . $sticker_id;
        $ret = $this->TrnSticker->query($sql);
        
        // 在庫数を増加する。
        if(is_null($ret[0][0]['id'])){
            // 在庫レコードがない場合
            $TrnStock = array(
                'TrnStock'=> array(
                    'mst_item_unit_id'  => $ret[0][0]['mst_item_unit_id'],
                    'mst_department_id' => $ret[0][0]['mst_department_id'],
                    'stock_count'       => 1,
                    'reserve_count'     => 0,
                    'promise_count'     => 0,
                    'requested_count'   => 0,
                    'receipted_count'   => 0,
                    'is_deleted'        => false,
                    'creater'           => $this->Session->read('Auth.MstUser.id'),
                    'created'           => $now,
                    'modifier'          => $this->Session->read('Auth.MstUser.id'),
                    'modified'          => $now
                    )
                );

            $this->TrnStock->create();
            // SQL実行
            if (!$this->TrnStock->save($TrnEdiReceivingHeader ,  array('validates' => true,'atomic' => false))) {
                throw new Exception('消費＠在庫情報作成失敗' . print_r($this->validateErrors($this->TrnStock),true));
            }          
            
        }else{
            // 在庫レコードがある場合
            $this->TrnStock->updateAll(array(
                'TrnStock.stock_count' => 'TrnStock.stock_count + 1 ',
                'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                'TrnStock.modified'    => "'" .$this->request->data['now'] . "'",
                ),array(
                    'TrnStock.id' => $ret[0][0]['id'],
                ),-1);
            if(count($this->validateErrors($this->TrnStock)) > 0){
                throw new Exception('消費＠在庫情報更新失敗' . print_r($this->validateErrors($this->TrnStock),true));
            }
        }
    }
    
    /**
     * 病院の請求価格を取得
     * @param string $gross
     * @param string $round
     * @param float $unitPrice
     * @param integer $quantity
     * @return float
     */
    private function getHospitalClaimPrice($gross, $round, $unitPrice, $quantity){
        if($gross == Configure::read('GrossType.Seal')){
            //シール単位の場合は単価を丸め
            return $this->getFacilityUnitPrice($round, $unitPrice) * $quantity;
        }else{
            //それ以外は金額を丸め
            return $this->getRoundValue($unitPrice * $quantity, $round, 2);
        }
    }



   /**
     * 少数桁考慮した各種丸め
     * @param float $value
     * @param string $round
     * @param int $digit 対象桁
     * @access public
     * @return float
     */
    private function getRoundValue($value, $round, $digit){
        switch($round){
            case Configure::read('RoundType.Round'):
                return round($value, 3);
                break;
            case Configure::read('RoundType.Ceil'):
                return ceil($value*pow(10,$digit))/pow(10, $digit);
                break;
            case Configure::read('RoundType.Floor'):
            case Configure::read('RoundType.Normal'):
            default:
                return floor($value*pow(10,$digit))/pow(10, $digit);
                break;
        }
    }

    public function report(){
        $this->request->data['now'] = date('Y/m/d H:i:s');

        if(is_array($this->request->data['TrnShippingHeader']['id'])){
            $cond = explode(',', trim($this->request->data['TrnShippingHeader']['id'][0]));
        }else{
            $cond = explode(',', trim($this->request->data['TrnShippingHeader']['id']));
        }

        $res = $this->getReportData($cond);
        $columns = join("\t", array(
            '納品先名',
            '納品番号',
            '納品先部署名',
            '施主名',
            '施設名',
            '納品日',
            '納品先住所',
            '施主住所',
            '施設住所',
            '行番号',
            '商品名',
            '規格',
            '製品番号',
            '販売元',
            '部署シール',
            'ロット番号',
            '明細備考',
            '数量',
            '包装単位',
            '管理区分',
            '商品ID',
            '単価',
            '金額',
            '印刷年月日',
            'ヘッダ備考',
            '丸め区分',
            '削除フラグ',
            '改ページ番号',
        ));
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'納品書','sortkey' =>'納品番号,改ページ番号');

        $this->layout = 'xml/default';
        $this->set('columns',$columns);
        $this->set('posted',$res);
        $this->set('layout_name',$layout_name['09']);
        $this->set('fix_value',$fix_value); //連想配列のキーにname属性を指定
        $this->render('/shippings/xml_output');

        //印刷日時更新
        $this->TrnShippingHeader->begin();
        $upd = $this->TrnShippingHeader->updateAll(array(
            'TrnShippingHeader.printed_date2' => "'" . $this->request->data['now'] . "'",
            'TrnShippingHeader.modifier'      => $this->request->data['User']['user_id'],
            'TrnShippingHeader.modified'      => "'" . $this->request->data['now'] . "'",
        ), array(
            'TrnShippingHeader.id' => $cond,
        ), -1);

        if(false === $upd){
            $this->TrnShippingHeader->rollback();
        }else{
            $this->TrnShippingHeader->commit();
        }
    }

    private function getReportData($cond){
        $sql =
            'select ' .
                ' MstFacilityItem.internal_code' .
                ',MstFacilityItem.item_name' .
                ',MstFacilityItem.standard' .
                ',MstFacilityItem.item_code' .
                ',MstFacilityItem.mst_owner_id' .
                ',MstDealer.dealer_name' .
                ',COALESCE(TrnShipping.hospital_sticker_no,TrnSticker.hospital_sticker_no) AS hospital_sticker_no' .
                ',TrnSticker.lot_no' .
                ',TrnShipping.recital' .
                ',TrnShipping.quantity' .
                ",(case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')' end) as packing_name" .
                ',TrnShipping.shipping_type' .
                ',SalesClaim.claim_price as sales_price' .
                ',SalesClaim.count as quantity'.
                ',SalesClaim.unit_price'.
                ',SalesClaim.round'.
                ',TrnShippingHeader.printed_date2' .
                ',TrnShippingHeader.recital as header_recital' .
                ',TrnShippingHeader.work_date'.
                ',TrnShipping.is_deleted' .
                ',TrnShipping.work_no' .
                ',DepartmentFrom.mst_facility_id'.
                ',TrnShipping.department_id_to'.
                ',FacilityTo.facility_formal_name'.
                ',FacilityTo.zip'.
                ',FacilityTo.address'.
                ',FacilityTo.tel'.
                ',FacilityTo.fax'.
                ',FacilityTo.gross'.
                ',FacilityTo.round'.
                ',FacilityTo.facility_formal_name'.
                ',FacilityTo.facility_formal_name'.
                ',DepartmentTo.department_name'.
            ' from ' .
                ' trn_shippings as TrnShipping' .
                ' inner join trn_shipping_headers as TrnShippingHeader on TrnShipping.trn_shipping_header_id = TrnSHippingHeader.id' .
                ' left join mst_item_units as MstItemUnit on TrnShipping.mst_item_unit_id = MstItemUnit.id' .
                ' left join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id' .
                ' left join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id' .
                ' left join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id' .
                ' left join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id' .
                ' left join trn_stickers as TrnSticker on TrnShipping.trn_sticker_id = TrnSticker.id' .
                ' left join trn_claims as SalesClaim on TrnSticker.sale_claim_id = SalesClaim.id' .
                ' left join mst_departments as DepartmentFrom on TrnShipping.department_id_from = DepartmentFrom.id'.
                ' left join mst_departments as DepartmentTo on DepartmentTo.id = TrnShipping.department_id_to'.
                ' left join mst_facilities as FacilityTo on FacilityTo.id = DepartmentTo.mst_facility_id '.
            ' where ' .
                ' 1 = 1';
        if(is_array($cond)){
            $sql .= ' and TrnShippingHeader.id in (' . join(',', $cond) . ')';
        }else{
            $sql .= ' and TrnShippingHeader.id = ' . $cond;
        }
        $res = $this->TrnShipping->query($sql);

        $deps = array();
        $owns = array();
        $fas = array();
        $seq = array();
        $results = array();
        $classes = Configure::read('Classes');
        $classes[''] = '';
        $before_page_no = "";
        foreach($res as $r){
            $row = $r[0];

            // 納品先施設名
            $row['invoice_to_facility_name'] = $r[0]['facility_formal_name'];
            // 納品先部署名
            $row['invoice_to_department_name'] = $r[0]['department_name'];
            // 納品先住所
            $row['invoice_to_address'] = "〒".$r[0]['zip']."　".$r[0]['address']
                                 ."\nTel:".$r[0]['tel']."／Fax:".$r[0]['fax'];
            // まとめ区分
            $row['gross'] = $r[0]['gross'];
            // 丸め区分
            $row['round'] = $r[0]['round'];

            //施主取得
            if($row['mst_owner_id'] != ''){
                if(false === isset($owns[$row['mst_owner_id']])){
                    $o = $this->MstFacility->find('first',array('recursive' => -1,'conditions'=>array('MstFacility.id'=>$row['mst_owner_id'])));
                    $owns[$row['mst_owner_id']] = $o;
                }
                $row['ownerAddress'] = "〒".$owns[$row['mst_owner_id']]['MstFacility']['zip']."　".$owns[$row['mst_owner_id']]['MstFacility']['address']
                                         ."\nTel:".$owns[$row['mst_owner_id']]['MstFacility']['tel']."／Fax:".$owns[$row['mst_owner_id']]['MstFacility']['fax'];
                $row['owner_name'] = $owns[$row['mst_owner_id']]['MstFacility']['facility_formal_name']
                                         ? $owns[$row['mst_owner_id']]['MstFacility']['facility_formal_name']
                                         : $owns[$row['mst_owner_id']]['MstFacility']['facility_name'];
            }else{
                $row['ownerAddress'] = '';
                $row['owner_name'] = '';
            }

            //センター情報取得
            if(false === isset($fas[$row['mst_facility_id']])){
                $f = $this->MstFacility->find('first', array('recursive' => -1,'conditions'=>array('MstFacility.id'=>(int)$row['mst_facility_id'])));
                $fas[$row['mst_facility_id']] = $f;
            }
            $row['center_address'] = "〒".$fas[$row['mst_facility_id']]['MstFacility']['zip']."　".$fas[$row['mst_facility_id']]['MstFacility']['address']
                         ."\nTel:".$fas[$row['mst_facility_id']]['MstFacility']['tel']."／Fax:".$fas[$row['mst_facility_id']]['MstFacility']['fax'];
            $row['center_name'] = $fas[$row['mst_facility_id']]['MstFacility']['facility_formal_name'];

            //行番号
            if(($before_page_no !== $row['work_no'].$row['mst_owner_id'])){
              $seq[$row['work_no']] = 1;
            } else {
              $seq[$row['work_no']]++;
            }

            $results[] = array(
              $row['invoice_to_facility_name'] . ' 様',//納品先名
                $row['work_no'],//納品番号
              '納品先名：' . $row['invoice_to_department_name'],//納品先部署名
              $row['owner_name'],//施主名
              $row['center_name'],//施設名
              date('Y年m月d日', strtotime($row['work_date'])),//納品日
              $row['invoice_to_address'],//納品先住所
              $row['ownerAddress'],//施主住所
              $row['center_address'],//施設住所
              $seq[$row['work_no']],//行番号
              $row['item_name'],//商品名
              $row['standard'],//規格
              $row['item_code'],//製品番号
              $row['dealer_name'],//販売元
              $row['hospital_sticker_no'],//部署シール
              $row['lot_no'],//ロット番号
              $row['recital'],//明細備考
              $row['quantity'],//数量
              $row['packing_name'],//包装単位
              $classes[$row['shipping_type']],//管理区分
              $row['internal_code'],//商品ID
              number_format($row['unit_price'], 2),//単価
              number_format($row['sales_price'], 2),//金額
              $row['printed_date2'],//印刷日時　
              $row['header_recital'],// ヘッダ備考
              $row['round'],// 丸め区分
              $row['is_deleted'],//削除フラグ
              $row['work_no'].$row['mst_owner_id'] // 改ページ番号
            );
            $before_page_no = $row['work_no'].$row['mst_owner_id'];//改ページ確認用
        }
        return $results;
    }

    private function searchFacilityItems($where = null , $order = null , $limit = null ){
        $sql  =' select ';
        $sql .='       b.id            as "MstFacilityItems__id"';
        $sql .='     , a.internal_code as "MstFacilityItems__internal_code"';
        $sql .='     , a.item_name     as "MstFacilityItems__item_name"';
        $sql .='     , a.standard      as "MstFacilityItems__standard"';
        $sql .='     , a.item_code     as "MstFacilityItems__item_code"';
        $sql .='     , c.dealer_name   as "MstFacilityItems__dealer_name"';
        $sql .='     , ( case when b.per_unit = 1 then d.unit_name ';
        $sql .="         else d.unit_name || '(' || b.per_unit || e.unit_name || ')' ";
        $sql .='         end )         as "MstFacilityItems__unit_name"';
        $sql .='     , ( case ';
        $sql .='         when f.id is null then false ';
        $sql .='         when g.id is null then false ';
        $sql .='         else true ';
        $sql .='         end )         as "MstFacilityItems__check" ';
        $sql .='   from ';
        $sql .='     mst_facility_items as a  ';
        $sql .='     left join mst_item_units as b  ';
        $sql .='       on a.id = b.mst_facility_item_id  ';
        $sql .='     left join mst_dealers as c  ';
        $sql .='       on c.id = a.mst_dealer_id  ';
        $sql .='     left join mst_unit_names as d  ';
        $sql .='       on d.id = b.mst_unit_name_id  ';
        $sql .='     left join mst_unit_names as e  ';
        $sql .='       on e.id = b.per_unit_name_id  ';
        $sql .='    left join mst_sales_configs as f  ';
        $sql .='      on f.mst_item_unit_id = b.id  ';
        $sql .='      and f.is_deleted = false  ';
        $sql .='      and f.partner_facility_id = ' . $this->request->data['d2customers']['hospitalId'];
        $sql .="      and f.start_date <= '" . $this->request->data['d2customers']['work_date'] . "'"; //売上設定開始日
        $sql .="      and ( f.end_date >= '" . $this->request->data['d2customers']['work_date'] . "'"; //売上設定終了日
        $sql .='      or f.end_date is null ) ';

        $sql .='    left join mst_transaction_configs as g  ';
        $sql .='      on g.mst_item_unit_id = b.id  ';
        $sql .='      and g.is_deleted = false  ';
        $sql .='      and g.partner_facility_id = ' . $this->request->data['d2customers']['supplierId'];
        $sql .="      and g.start_date <= '" . $this->request->data['d2customers']['work_date'] . "'"; //仕入設定開始日
        $sql .="      and ( g.end_date >= '" . $this->request->data['d2customers']['work_date'] . "'"; //仕入設定終了日
        $sql .='      or g.end_date is null ) ';

        if(!is_null($where)){
            $sql .=' where 1 = 1 ';
            $sql .=' ' . $where;
        }
        if(!is_null($order)){
            $sql .=' ' . $order;
        }
        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));
            $sql .=' ' .$limit;
        }
        return $sql;
    }

    public function seal($type){
        App::import('Sanitize');
        $_hno = $_sid = array();
        $sql  = ' select ';
        $sql .= '       c.id  ';
        $sql .= '     , c.hospital_sticker_no  ';
        $sql .= '   from ';
        $sql .= '     trn_shipping_headers as a  ';
        $sql .= '     left join trn_shippings as b  ';
        $sql .= '       on b.trn_shipping_header_id = a.id  ';
        $sql .= '     left join trn_stickers as c  ';
        $sql .= '       on c.id = b.trn_sticker_id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->request->data['TrnShippingHeader']['id'];
        $ret = $this->TrnShipping->query($sql);
        foreach($ret as $r ){
            $_hno[] = $r[0]['hospital_sticker_no'];
            $_sid[] = $r[0]['id'];
        }

        if($type==1){
            //部署シール
            $order = 'MstDepartment.department_name asc, MstShelfNameTo.code asc';
            $data = $this->Stickers->getHospitalStickers($_hno, $order,2);
            $this->set('sortkey' , '病院情報,センター棚番号,商品ID');
            $this->set('Sticker' , $data);
            $this->layout = 'xml/default';
            $this->render('/stickerissues/hospital_sticker');

        }elseif($type==2){
            // コストシールデータ取得
            $records = $this->Stickers->getCostStickers($_sid);
            // 空なら初期化
            if(!$records){ $records = array();}

            $this->layout = 'xml/default';
            $this->set('records', $records);
            $this->render('/stickerissues/cost_sticker');
        }

    }
    
    function getItemUnit($facility_item_id , $supplier_id, $hospital_id,$work_date){
        $sql  = ' select ';
        $sql .= '     a.id                as "MstItemUnit__id"';
        $sql .= '   , ( case  ';
        $sql .= '       when a.per_unit = 1  ';
        $sql .= '       then a1.unit_name  ';
        $sql .= "       else a1.unit_name || '(' || a.per_unit || a2.unit_name || ')'  ";
        $sql .= '       end )             as "MstItemUnit__unit_name" ';
        $sql .= '   , b.sales_price       as "MstItemUnit__sales_price"';
        $sql .= '   , c.transaction_price as "MstItemUnit__transaction_price" ';
        $sql .= ' from ';
        $sql .= '   mst_item_units as a  ';
        $sql .= '   left join mst_unit_names as a1  ';
        $sql .= '     on a1.id = a.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as a2  ';
        $sql .= '     on a2.id = a.per_unit_name_id  ';
        $sql .= '   inner join mst_sales_configs as b  ';
        $sql .= '     on b.mst_item_unit_id = a.id  ';
        $sql .= '     and b.partner_facility_id = ' . $hospital_id;
        $sql .= '     and b.is_deleted = false  ';
        $sql .= "     and b.start_date <= '" . $work_date . "'";
        $sql .= "     and b.end_date > '" . $work_date . "'";
        $sql .= '   inner join mst_transaction_configs as c  ';
        $sql .= '     on c.mst_item_unit_id = a.id  ';
        $sql .= '     and c.partner_facility_id = ' . $supplier_id;
        $sql .= '     and c.is_deleted = false  ';
        $sql .= "     and c.start_date <= '" . $work_date . "'";
        $sql .= "     and c.end_date > '" . $work_date . "'";
        $sql .= ' where ';
        $sql .= '   a.mst_facility_item_id = ' . $facility_item_id;
        $sql .= ' order by ';
        $sql .= '   a.per_unit desc ';
        $ret = $this->MstFacilityItem->query($sql);
        return $ret;
    }
    
    function getPackingName($item_unit_id){
        $sql  = ' select ';
        $sql .= '     ( case  ';
        $sql .= '       when a.per_unit = 1  ';
        $sql .= '       then b.unit_name  ';
        $sql .= "       else b.unit_name || '(' || a.per_unit || c.unit_name || ')'  ";
        $sql .= '       end )                        as packing_name  ';
        $sql .= ' from ';
        $sql .= '   mst_item_units as a  ';
        $sql .= '   left join mst_unit_names as b  ';
        $sql .= '     on b.id = a.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as c  ';
        $sql .= '     on c.id = a.per_unit_name_id  ';
        $sql .= ' where ';
        $sql .= '   a.id = ' . $item_unit_id;
        
        $ret = $this->MstItemUnit->query($sql);
        return $ret[0][0]['packing_name'];
    }
}

