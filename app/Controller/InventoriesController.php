<?php
/**
 * InventoriesController
 *　棚卸
 * @version 1.0.0
 * @since 2010/05/18
 */
class InventoriesController extends AppController {

  /**
   * @var $name
   */
    var $name = 'Inventories';

    /**
     * @var array $uses
     */
    var $uses = array('TrnInventory'
                      ,'TrnInventoryHeader'
                      ,'MstClass'
                      ,'MstDepartment'
                      ,'MstFacility'
                      ,'MstFacilityItem'
                      ,'MstUser'
                      ,'TrnSticker'
                      ,'MstShelfName'
                      );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker' );

    /**
     * @var array $components
     */
    var $components = array('xml','Common','RequestHandler','utils' , 'Barcode','CsvWriteUtils','CsvReadUtils');

    var $inventory_limit = 20;
    
    /**
     *
     */
    function index (){
        $this->redirect('/');
    }


    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
    }

    /**
     * 棚別棚卸初期画面
     * @access public
     */
    function each_shelf_add() {
        $this->setRoleFunction(42); //棚別予定登録
        $department_list = array();
        $SearchResult = array();
        //検索ボタンが押されたら。
        if(isset( $this->request->data['inventory']['isSearch'])) {

            $where = "";
            //絞り込み条件
            //施設
            if($this->request->data['inventory']['facilityCode'] != ""){
                $where .=" and b.facility_code = '" . $this->request->data['inventory']['facilityCode']. "'";
            }
            //部署
            if($this->request->data['inventory']['departmentCode'] != ""){
                $where .=" and c.department_code = '" . $this->request->data['inventory']['departmentCode']. "'";
            }
            $SearchResult = $this->getShelfList($where , $where , $this->request->data['limit']);
            // 施設が選択済みの場合、紐づく部署リストを取得する
            if($this->request->data['inventory']['facilityCode'] != "" ){
                $department_list = $this->getDepartmentsList($this->request->data['inventory']['facilityCode']);
            }
            
        }

        $this->set('SearchResult', $this->outputFilter($SearchResult));
        //施設
        $this->set('facility_list' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                            array(Configure::read('FacilityType.center')
                                                                  ,Configure::read('FacilityType.hospital')
                                                                )
                                                            )
                   );
        //部署
        $this->set('department_list' , $department_list);
    }

    /**
     * 棚別棚卸確認画面
     * @access public
     */
    function each_shelf_add_confirm() {
        //管理区分
        $this->setClass();

        //検索結果格納配列
        $SearchResult = array();
        //棚卸検索実行
        $SearchResult = $this->getShelfList2($this->request->data);

        $this->set('SearchResult', $this->outputFilter($SearchResult));
        $this->render('/inventories/each_shelf_add_confirm');
    }

    /**
     * 棚別棚卸完了画面
     * @access public
     */
    function each_shelf_add_result() {

        //検索結果格納配列
        $SearchResult = array();
        //棚卸検索実行
        $SearchResult = $this->getShelfList2($this->request->data);
        
        if(!$this->shelf_regist($SearchResult)){
            //登録失敗
            $this->Session->setFlash('登録が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
        }
        
        $this->set('SearchResult', $this->outputFilter($SearchResult));
    }

    /**
     * 商品別棚卸初期画面
     * @access public
     */
    function each_commodity_add() {
        $this->setRoleFunction(43); //商品別予定登録

        //管理区分
        $this->setClass();

        //検索結果格納変数
        $SearchResult = array();
        $CartSearchResult = array();
        //検索実行フラグ
        $SearchFlg = true;

        if( isset($this->request->data['search']['is_search']) ) {
            //検索処理
            $where = "";
            //商品ID(前方一致)
            if( !empty($this->request->data['search']['item_id']) ){
                $where .= " and a.internal_code ILIKE '" . $this->request->data['search']['item_id']."%'";
            }
            //製品番号(前方一致)
            if( !empty($this->request->data['search']['item_code']) ){
                $where .= " and a.item_code ILIKE '" . $this->request->data['search']['item_code']."'%";
            }
            //商品名(部分一致)
            if( !empty($this->request->data['search']['item_name']) ){
                $where .= " and a.item_name ILIKE '%" . $this->request->data['search']['item_name'] . "%'";
            }
            //規格(部分一致)
            if( !empty($this->request->data['search']['standard']) ){
                $where .= " and a.standard ILIKE '%" . $this->request->data['search']['standard'] . "%'";
            }
            //販売元(部分一致)
            if( !empty($this->request->data['search']['dealer_name']) ){
                $where .= " and b.dealer_name ILIKE '%" . $this->request->data['search']['dealer_name'] . "%'";
            }
            //JANコード(前方一致)
            if( !empty($this->request->data['search']['jan_code']) ){
                $where .= " and a.jan_code ILIKE '" . $this->request->data['search']['jan_code']."%'";
            }
            if(!empty($this->request->data['search']['tempId'])){
                $where .= " and c.id not in ( " . $this->request->data['search']['tempId'] . ")";
            }

            //検索実行
            $SearchResult = $this->getItemList($where , $this->request->data['Inventory']['limit']);
        }else{
            $SearchFlg = false;
        }

        //カートで持ち越している商品の検索実行
        if(!empty($this->request->data['search']['tempId'])){
            $where = " and c.id in ( " . $this->request->data['search']['tempId'] . ")";
            $CartSearchResult = $this->getItemList($where);
        }
        $this->set('SearchFlg',$SearchFlg);
        $this->set('CartSearchResult', $this->outputFilter($CartSearchResult));
        $this->set('SearchResult', $this->outputFilter($SearchResult));


        $this->render('/inventories/each_commodity_add');
    }

    /**
     * 商品別棚卸確認画面
     * @access public
     */
    function each_commodity_add_confirm() {

        //管理区分
        $this->setClass();

        $this->set('facility_list' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                            array(Configure::read('FacilityType.center')
                                                                  ,Configure::read('FacilityType.hospital')
                                                                )
                                                            )
                   );

        $SearchResult = array();

        if(isset($this->request->data['inventory']['is_search'])){
            /* 棚検索 */
            $where = "";
            if(isset($this->request->data['inventory']['facilityCode']) && $this->request->data['inventory']['facilityCode']!=""){
                $where = " and b.facility_code = '" . $this->request->data['inventory']['facilityCode'] . "'";
            }
            $SearchResult = $this->getShelfList($where , $where);
            
        }

        $this->set('SearchResult' , $this->outputFilter($SearchResult));
        
        
        $this->render('/inventories/each_commodity_add_confirm');
    }

    /**
     * 商品別棚卸完了画面
     * @access public
     */
    function each_commodity_add_result() {
        //検索結果格納配列
        $SearchResult = array();
        //棚卸検索実行
        $SearchResult = $this->getShelfList2($this->request->data);
        
        //棚卸登録
        $result = $this->commodity_regist($SearchResult);
        if($result===false){
            //登録失敗
            $this->Session->setFlash('登録が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
        }
        $this->set('SearchResult' , $result);
        $this->render('/inventories/each_commodity_add_result');
    }

    /**
     * 棚卸実施登録初期画面
     * @access public
     */
    function execution_add() {
        App::import('Sanitize');
        $this->setRoleFunction(44); //棚卸実施登録
        // 帳票用に埋め込み 
        $this->request->data['TrnInventory']['mst_facility_id'] = $this->Session->read('Auth.facility_id_selected');

        //検索結果格納変数
        $SearchResult = array();

        if( isset($this->request->data['search']['is_search']) ) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            /* 画面から入力された検索条件 */
            $where = '';
            //棚卸番号(前方一致)
            if( $this->request->data["search"]["work_no"] != '' ){
                $where .= " and a.work_no ILIKE '" . $this->request->data["search"]["work_no"]."%'";
            }
            //棚卸実施名(前方一致)
            if( $this->request->data["search"]["title"] != '' ){
                $where .= " and a.title ILIKE '" . $this->request->data["search"]["title"]."%'";
            }
            //作成日FROMのみ
            if( $this->request->data["search"]["work_date_from"] != ''  ){
                $where .= " and to_char(a.created, 'YYYY/MM/DD')   >= '"  . $this->request->data["search"]["work_date_from"] . "'";
            }
            //作成日TOのみ
            if( $this->request->data["search"]["work_date_to"] != ''  ){
                $where .= " and to_char(a.created, 'YYYY/MM/DD')  <= '" . $this->request->data["search"]["work_date_to"] . "'";
            }
            //未実施のみ
            if( !empty($this->request->data["search"]["check"])){
                $where .= ' and a.work_date is null ';
            }
            $SearchResult = $this->getInventoryList($where , $this->request->data['Inventory']['limit']);

            $this->request->data = $data;
        }

        $this->set('SearchResult', $this->outputFilter($SearchResult));
        $this->render('/inventories/execution_add');
    }

    /**
     * 棚卸実施データのクリア
     * @access public
     */
    function execution_clear() {
        $now = date('Y/m/d H:i:s.u');
        $this->set('InventoryType',Configure::read('InventoryTypes'));
        $params = array();

        //Begin
        $this->TrnInventoryHeader->begin();

        //行ロック
        $this->TrnInventoryHeader->query('select * from trn_inventory_headers as a where a.id = ' . $this->request->data['TrnInventoryHeader']['id'] . ' for update ' ) ;

        //更新チェック
        $ret = $this->TrnInventoryHeader->query("select count(*) from trn_inventory_headers as a where a.id = " . $this->request->data['TrnInventoryHeader']['id'] . " and a.modified > '" .$this->request->data['TrnInventory']['time'] . "'");
        if($ret[0][0]['count'] > 0){
            $this->TrnInventoryHeader->rollback();
            $this->Session->setFlash('棚卸データのクリアに失敗しました。', 'growl', array('type'=>'error') );
            $this->redirect('execution_add');
        }

        //1.ヘッダを更新(work_date="" difference_count=0)
        $params = array(
            'TrnInventoryHeader.work_date'        => null,
            'TrnInventoryHeader.difference_count' => 0,
            'TrnInventoryHeader.modifier'         => $this->Session->read('Auth.MstUser.id'),
            'TrnInventoryHeader.modified'         => "'".$now."'",
            );
        $where = array('TrnInventoryHeader.id' => $this->request->data['TrnInventoryHeader']['id']);

        //SQL実行
        if (!$this->TrnInventoryHeader->updateAll($params,$where)) {
            //RollBack
            $this->TrnInventoryHeader->rollback();
            $this->Session->setFlash('棚卸データのクリアに失敗しました。', 'growl', array('type'=>'error') );
            $this->redirect('execution_add');
        }

        //2.明細を更新(real_quantity = 0 )
        $params = array(
            'TrnInventory.real_quantity' => 0,
            'TrnInventory.modifier' => $this->Session->read('Auth.MstUser.id'),
            'TrnInventory.modified' => "'".$now."'",
            );
        $where = array('TrnInventory.trn_inventory_header_id' => $this->request->data['TrnInventoryHeader']['id']);
        //SQL実行
        if (!$this->TrnInventory->updateAll($params,$where)) {
            //RollBack
            $this->TrnInventoryHeader->rollback();
            $this->Session->setFlash('棚卸データのクリアに失敗しました。', 'growl', array('type'=>'error') );
            $this->redirect('execution_add');
        }

        //3.明細を更新(is_initial = false  を全て is_deleted=trueに)
        $params = array(
            'TrnInventory.is_deleted' => "true",
            'TrnInventory.modifier' => $this->Session->read('Auth.MstUser.id'),
            'TrnInventory.modified' => "'".$now."'",
            );
        $where = array('TrnInventory.trn_inventory_header_id' => $this->request->data['TrnInventoryHeader']['id']
                       ,'TrnInventory.is_initial' => 'FALSE');
        //SQL実行
        if (!$this->TrnInventory->updateAll($params,$where)) {
            //RollBack
            $this->TrnInventoryHeader->rollback();
            $this->Session->setFlash('棚卸データのクリアに失敗しました。', 'growl', array('type'=>'error') );
            $this->redirect('execution_add');
        }

        //コミット前に更新チェック
        $ret = $this->TrnInventoryHeader->query("select count(*) from trn_inventory_headers as a where a.id = " . $this->request->data['TrnInventoryHeader']['id'] . " and a.modified <> '" . $now . "' and a.modified > '" .$this->request->data['TrnInventory']['time'] . "'");
        if($ret[0][0]['count'] > 0){
            $this->TrnInventoryHeader->rollback();
            $this->Session->setFlash('棚卸データのクリアに失敗しました。', 'growl', array('type'=>'error') );
            $this->redirect('execution_add');
        }

        
        $data = $this->request->data;
        $this->request->data = Sanitize::clean($this->request->data);
        
        /* 画面から入力された検索条件 */
        $where = '';
        //棚卸番号(前方一致)
        if( $this->request->data['search']['work_no'] != '' ){
            $where .= " and a.work_no ILIKE '" . $this->request->data['search']['work_no']."%'";
        }
        //棚卸実施名(前方一致)
        if( $this->request->data['search']['title'] != '' ){
            $where .= " and a.title ILIKE '" . $this->request->data['search']['title']."%'";
        }
        //作成日FROMのみ
        if( $this->request->data['search']['work_date_from'] != ''  ){
            $where .= " and to_char(a.created, 'YYYY/MM/DD')   >= '"  . $this->request->data['search']['work_date_from'] . "'";
        }
        //作成日TOのみ
        if( $this->request->data['search']['work_date_to'] != ''  ){
            $where .= " and to_char(a.created, 'YYYY/MM/DD')  <= '" . $this->request->data['search']['work_date_to'] . "'";
        }
        //未実施のみ
        if( !empty($this->request->data['search']['check'])){
            $where .= ' and a.work_date is null ';
        }
        $SearchResult = $this->getInventoryList($where , $this->request->data['Inventory']['limit']);
        
        $this->request->data = $data;
        //コミット
        $this->TrnInventoryHeader->commit();
        $this->set('clearSuccess','true');
        $this->set('SearchResult',$SearchResult);
        $this->render('/inventories/execution_add');
    }
    
    /**
     * 棚卸実施データの削除
     * @access public
     */
    function execution_delete() {
        $now = date('Y/m/d H:i:s.u');
        $this->set('InventoryType',Configure::read('InventoryTypes'));
        $params = array();

        //Begin
        $this->TrnInventoryHeader->begin();

        //行ロック
        $this->TrnInventoryHeader->query('select * from trn_inventory_headers as a where a.id = ' . $this->request->data['TrnInventoryHeader']['id'] . ' for update ' ) ;

        //更新チェック
        $ret = $this->TrnInventoryHeader->query("select count(*) from trn_inventory_headers as a where a.id = " . $this->request->data['TrnInventoryHeader']['id'] . " and a.modified > '" .$this->request->data['TrnInventory']['time'] . "'");
        if($ret[0][0]['count'] > 0){
            $this->TrnInventoryHeader->rollback();
            $this->Session->setFlash('棚卸データのクリアに失敗しました。', 'growl', array('type'=>'error') );
            $this->redirect('execution_add');
        }

        //ヘッダを論理削除(is_deleted=true)
        $params = array(
            'TrnInventoryHeader.is_deleted' => "true",
            'TrnInventoryHeader.modifier' => $this->Session->read('Auth.MstUser.id'),
            'TrnInventoryHeader.modified' => "'".$now."'",
            );
        //SQL実行
        $where = array('TrnInventoryHeader.id' => $this->request->data['TrnInventoryHeader']['id']);

        if (!$this->TrnInventoryHeader->updateAll($params,$where)) {
            //RollBack
            $this->TrnInventoryHeader->rollback();
            $this->Session->setFlash('棚卸データのクリアに失敗しました。', 'growl', array('type'=>'error') );
            $this->redirect('execution_add');
        }

        //コミット前に更新チェック
        $ret = $this->TrnInventoryHeader->query("select count(*) from trn_inventory_headers as a where a.id = " . $this->request->data['TrnInventoryHeader']['id'] . " and a.modified <> '" . $now . "' and a.modified > '" .$this->request->data['TrnInventory']['time'] . "'");
        if($ret[0][0]['count'] > 0){
            $this->TrnInventoryHeader->rollback();
            $this->Session->setFlash('棚卸データのクリアに失敗しました。', 'growl', array('type'=>'error') );
            $this->redirect('execution_add');
        }

        $data = $this->request->data;
        $this->request->data = Sanitize::clean($this->request->data);
        
        /* 画面から入力された検索条件 */
        $where = '';
        //棚卸番号(前方一致)
        if( $this->request->data['search']['work_no'] != '' ){
            $where .= " and a.work_no ILIKE '" . $this->request->data['search']['work_no']."%'";
        }
        //棚卸実施名(前方一致)
        if( $this->request->data['search']['title'] != '' ){
            $where .= " and a.title ILIKE '" . $this->request->data['search']['title']."%'";
        }
        //作成日FROMのみ
        if( $this->request->data['search']['work_date_from'] != ''  ){
            $where .= " and to_char(a.created, 'YYYY/MM/DD')   >= '"  . $this->request->data['search']['work_date_from'] . "'";
        }
        //作成日TOのみ
        if( $this->request->data['search']['work_date_to'] != ''  ){
            $where .= " and to_char(a.created, 'YYYY/MM/DD')  <= '" . $this->request->data['search']['work_date_to'] . "'";
        }
        //未実施のみ
        if( !empty($this->request->data['search']['check'])){
            $where .= ' and a.work_date is null ';
        }
        $SearchResult = $this->getInventoryList($where , $this->request->data['Inventory']['limit']);
        
        $this->request->data = $data;
        //コミット
        $this->TrnInventoryHeader->commit();
        $this->set('deleteSuccess','true');
        $this->set('SearchResult', $SearchResult);
        $this->render('/inventories/execution_add');
    }

    /**
     * 棚卸実施登録明細確認画面
     * @access public
     */
    function execution_detail_confirm($mode='') {
        $Stickers = array();
        $SearchResult = array();
        
        if($this->Session->check('Inventory.SearchResult')){
            $SearchResult = $this->Session->read('Inventory.SearchResult');
        }
        if($this->Session->check('Inventory.Stickers')){
            $Stickers = $this->Session->read('Inventory.Stickers');
        }
        
        if($mode==""){
            //モード指定がない場合。初回を想定
            $ret = $this->getInventoryHeader($this->request->data['TrnInventoryHeader']['id']);
            $this->request->data['TrnInventoryHeader'] = $ret[0]['TrnInventoryHeader'];
        }
        
        switch($mode){
          case 'inventory_search':
            //棚卸データ絞込み
            $sql  = ' select ';
            $sql .= '     b.id                  as "Inventory__id"';
            $sql .= '   , e.internal_code       as "Inventory__internal_code"';
            $sql .= '   , e.item_name           as "Inventory__item_name"';
            $sql .= '   , e.item_code           as "Inventory__item_code"';
            $sql .= '   , b.quantity            as "Inventory__quantity"';
            $sql .= '   , c.facility_sticker_no as "Inventory__facility_sticker_no"';
            $sql .= '   , c.lot_no              as "Inventory__lot_no"';
            $sql .= '   , g.fixed_count         as "Inventory__fixed_count"';
            $sql .= '   , i.facility_name       as "Inventory__facility_name"';
            $sql .= '   , h.department_name     as "Inventory__department_name"';
            $sql .= "   , (case when c.trn_adjust_id is not null then '調整済み' end) ";
            $sql .= '                           as "Inventory__status"';
            $sql .= '   , e.standard            as "Inventory__standard"';
            $sql .= '   , f.dealer_name         as "Inventory__dealer_name"';
            $sql .= '   , b.real_quantity       as "Inventory__real_quantity"';
            $sql .= '   , c.hospital_sticker_no as "Inventory__hospital_sticker_no" ';
            $sql .= "   , to_char(c.validated_date, 'YYYY/mm/dd') ";
            $sql .= '                           as "Inventory__validated_date" ';
            $sql .= "   , date_part('day', current_timestamp - c.last_move_date) ";
            $sql .= '                           as "Inventory__immovable_date" ';
            $sql .= '   , b.recital             as "Inventory__recital" ';
            $sql .= ' from ';
            $sql .= '   trn_inventory_headers as a ';
            $sql .= '   left join trn_inventories as b ';
            $sql .= '     on b.trn_inventory_header_id = a.id ';
            $sql .= '    and b.is_deleted = false ';
            $sql .= '   left join trn_stickers as c ';
            $sql .= '     on c.id = b.trn_sticker_id ';
            $sql .= '   left join mst_item_units as d ';
            $sql .= '     on d.id = c.mst_item_unit_id ';
            $sql .= '   left join mst_facility_items as e ';
            $sql .= '     on e.id = d.mst_facility_item_id ';
            $sql .= '   left join mst_dealers as f ';
            $sql .= '     on f.id = e.mst_dealer_id ';
            $sql .= '   left join mst_fixed_counts as g ';
            $sql .= '     on g.mst_item_unit_id = c.mst_item_unit_id ';
            $sql .= '     and g.mst_department_id = c.mst_department_id ';
            $sql .= '   left join mst_departments as h ';
            $sql .= '     on h.id = c.mst_department_id ';
            $sql .= '   left join mst_facilities as i ';
            $sql .= '     on i.id = h.mst_facility_id ';
            $sql .= ' where ';
            $sql .= '   a.id = ' . $this->request->data['TrnInventoryHeader']['id'];
            /* 絞込条件 */
            //商品ID
            if(isset($this->request->data['Inventory']['internal_code']) && $this->request->data['Inventory']['internal_code']!=''){
                $sql .= " and e.internal_code = '" . $this->request->data['Inventory']['internal_code'] . "'";
            }
            //商品名
            if(isset($this->request->data['Inventory']['item_name']) && $this->request->data['Inventory']['item_name']!=''){
                $sql .= " and e.item_name like '%" . $this->request->data['Inventory']['item_name'] . "%'";
            }
            //製品番号
            if(isset($this->request->data['Inventory']['item_code']) && $this->request->data['Inventory']['item_code']!=''){
                $sql .= " and e.item_code like '" . $this->request->data['Inventory']['item_code'] . "%'";
            }
            //規格
            if(isset($this->request->data['Inventory']['standard']) && $this->request->data['Inventory']['standard']!=''){
                $sql .= " and e.standard like '%" . $this->request->data['Inventory']['standard'] . "%'";
            }
            //販売元
            if(isset($this->request->data['Inventory']['dealer_name']) && $this->request->data['Inventory']['dealer_name']!=''){
                $sql .= " and f.dealer_name like '%" . $this->request->data['Inventory']['dealer_name'] . "%'";
            }
            //シール番号
            if(isset($this->request->data['Inventory']['sticker_no']) && $this->request->data['Inventory']['sticker_no']!=''){
                $sql .= " and ( c.facility_sticker_no = '" . $this->request->data['Inventory']['sticker_no'] . "'";
                $sql .= " or c.hospital_sticker_no = '" . $this->request->data['Inventory']['sticker_no'] . "' ) ";
            }
            //ロット番号
            if(isset($this->request->data['Inventory']['lot_no']) && $this->request->data['Inventory']['lot_no']!=''){
                $sql .= " and c.lot_no like '%" . $this->request->data['Inventory']['lot_no'] . "%'";
            }
            //差分有りのみ表示
            if(isset($this->request->data['Inventory']['flg'])){
                $sql .= " and b.quantity <> b.real_quantity";
            }
            
            $sql .= ' order by ';
            $sql .= '   b.id ';
            $maxCount =  $this->getMaxCount($sql , 'TrnInventory');
            $pageMax = ceil($maxCount / $this->inventory_limit);
            $sql .= ' limit ' . $this->inventory_limit;
            if(isset($this->request->data['inventory']['pgno'])){
                $sql .= ' offset ' . ( $this->request->data['inventory']['pgno'] -1 )* $this->inventory_limit;
            }
            $SearchResult = $this->TrnInventoryHeader->query($sql);
            
            for( $i=1 ; $i <= $pageMax ; $i++){
                $page_list[$i] = $i . "ページ目" ;
            }
            $this->set('page_list' , $page_list);
            
            break;
          case 'regist':
            //更新チェック
            if($this->updateCheck(
                $this->request->data['TrnInventoryHeader']['id'] ,
                $this->request->data["work_time"]
                )
               ){
                if(!$this->inventoryUpdate()){
                    $this->Session->setFlash('登録が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                }
            }else{
                $this->Session->setFlash('他のユーザによって更新されています。', 'growl', array('type'=>'error') );
            }

            break;

          case 'search':
            //シール検索フラグを立てる
            $this->request->data['search']['is_search'] = 1;

            break;
          case 'add':

            //シール検索はしない。
            $this->request->data['search']['is_search'] = 0;

            $ret = $this->searchStickers( array("TrnSticker.id" => $this->request->data['TrnSticker']['sticker_id']) );

            break;
        }

        /* シール検索フラグが有効だったら検索処理をする */
        if($this->request->data['search']['is_search'] == 1) {

            $where  = " and a.quantity > 0 ";
            $where .= " and c.mst_facility_id = " . $this->Session->read('Auth.facility_id_selected');
            
            /* 商品ID */
            if($this->request->data['search']['internal_code'] != ""){
                $where .= " and c.internal_code ILIKE '" . $this->request->data['search']['internal_code'] . "%'";
            }

            /* 製品番号 */
            if($this->request->data['search']['item_code'] != ""){
                $where .= " and c.item_code ILIKE '" . $this->request->data['search']['item_code'] . "%'";
            }

            /* ロット番号 */
            if($this->request->data['search']['lot_no'] != ""){
                $where .= " and a.lot_no ILIKE '" . $this->request->data['search']['lot_no'] . "%'";
            }

            /* 商品名 */
            if($this->request->data['search']['item_name'] != ""){
                $where .= " and a.item_name ILIKE '" . $this->request->data['search']['item_name'] . "%'";
            }

            /* 販売元 */
            if($this->request->data['search']['dealer_name'] != ""){
                $where .= " and d.dealer_name ILIKE '" . $this->request->data['search']['dealer_name'] . "%'";
            }

            /* 規格 */
            if($this->request->data['search']['standard'] != ""){
                $where .= " and c.standard ILIKE '" . $this->request->data['search']['standard'] . "%'";
            }

            /* シール番号 */
            if($this->request->data['search']['sticker_no'] != ""){
                $where .= " and ( a.facility_sticker_no ILIKE '" . $this->request->data['search']['sticker_no'] . "%'";
                $where .= " or  a.hospital_sticker_no ILIKE '" . $this->request->data['search']['sticker_no'] . "%')";
            }
            
            //すでに棚卸に含まれるシールは除外
            $where .=  " and a.id not in (select trn_stickers.id from trn_stickers left join trn_inventories on trn_inventories.trn_sticker_id = trn_stickers.id left join trn_inventory_headers on trn_inventory_headers.id = trn_inventories.trn_inventory_header_id  where trn_inventory_headers.id = ".$this->request->data['TrnInventoryHeader']['id'];
            
            $sql  = ' select ';
            $sql .= '     a.id                  as "TrnSticker__id" ';
            $sql .= '   , a.facility_sticker_no as "TrnSticker__facility_sticker_no" ';
            $sql .= '   , a.hospital_sticker_no as "TrnSticker__hospital_sticker_no" ';
            $sql .= '   , a.lot_no              as "TrnSticker__lot_no" ';
            $sql .= "   , to_char(a.validated_date, 'YYYY/mm/dd')";
            $sql .= '                           as "TrnSticker__validated_date" ';
            $sql .= '   , c.internal_code       as "TrnSticker__internal_code" ';
            $sql .= '   , c.item_name           as "TrnSticker__item_name" ';
            $sql .= '   , c.standard            as "TrnSticker__standard" ';
            $sql .= '   , c.item_code           as "TrnSticker__item_code" ';
            $sql .= '   , d.dealer_name         as "TrnSticker__dealer_name" ';
            $sql .= '   , f.facility_name       as "TrnSticker__facility_name"';
            $sql .= '   , e.department_name     as "TrnSticker__department_name"';
            $sql .= '   , (  ';
            $sql .= '     case  ';
            $sql .= '       when b.per_unit = 1  ';
            $sql .= '       then b1.unit_name  ';
            $sql .= "       else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
            $sql .= '       end ';
            $sql .= '   )                       as "TrnSticker__unit_name" ';
            $sql .= "   , (case when a.has_reserved = true then '予約中' end) ";
            $sql .= '                           as "TrnSticker__status"  ';
            $sql .= ' from ';
            $sql .= '   trn_stickers as a  ';
            $sql .= '   left join mst_item_units as b  ';
            $sql .= '     on b.id = a.mst_item_unit_id  ';
            $sql .= '   left join mst_unit_names as b1  ';
            $sql .= '     on b1.id = b.mst_unit_name_id  ';
            $sql .= '   left join mst_unit_names as b2  ';
            $sql .= '     on b2.id = b.per_unit_name_id  ';
            $sql .= '   left join mst_facility_items as c  ';
            $sql .= '     on c.id = b.mst_facility_item_id  ';
            $sql .= '   left join mst_dealers as d  ';
            $sql .= '     on d.id = c.mst_dealer_id  ';
            $sql .= '   left join mst_departments as e  ';
            $sql .= '     on e.id = a.mst_department_id  ';
            $sql .= '   left join mst_facilities as f  ';
            $sql .= '     on f.id = e.mst_facility_id  ';
            $sql .= ' where ';
            $sql .= "   a.lot_no != ' ' ";
            $sql .= '   and a.is_deleted = false  ';
            $sql .= '   and a.quantity > 0 ';
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnSticker'));
            $sql .= ' limit ' . $this->request->data['Inventory']['limit'];
            $Stickers = $this->TrnSticker->query($sql);
        }

        $this->set('SearchResult' , $this->outputFilter($SearchResult));
        $this->set('Stickers'     , $this->outputFilter($Stickers));
        
        $this->Session->write('Inventory.SearchResult' , $SearchResult);
        $this->Session->write('Inventory.Stickers'     , $Stickers);

        $this->render('/inventories/execution_detail_confirm');
    }

    /**
     * 棚卸実施登録確認画面
     * @access public
     */
    function execution_add_confirm() {
        $ret = $this->getInventoryHeader($this->request->data['TrnInventoryHeader']['id']);
        $this->request->data['TrnInventoryHeader'] = $ret[0]['TrnInventoryHeader'];
        
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnInventoryHeader.token' , $token);
        $this->request->data['TrnInventoryHeader']['token'] = $token;

        $this->render('/inventories/execution_add_confirm');
    }

    /**
     * 棚卸実施登録完了画面
     * @access public
     */
    function execution_add_result() {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['TrnInventoryHeader']['token'] === $this->Session->read('TrnInventoryHeader.token')) {
            $this->Session->delete('TrnInventoryHeader.token');
            $this->request->data['TrnInventory']['mst_facility_id'] = $this->Session->read('Auth.facility_id_selected');

            $this->Session->write('TrnInventoryHeader.data' , $this->request->data);
            $this->set("data",$this->request->data);
            
            $now = date('Y/m/d H:i:s.u');

            //CSV登録
                //Begin
                $this->TrnInventoryHeader->begin();

                //行ロック
                $this->TrnInventoryHeader->query( ' select * from trn_inventories as a where a.trn_inventory_header_id = ' . $this->request->data['TrnInventoryHeader']['id'] . ' for update ');
                //更新チェック
                $ret = $this->TrnInventoryHeader->query( ' select count(*) from trn_inventories as a where a.trn_inventory_header_id = ' . $this->request->data['TrnInventoryHeader']['id'] . " and a.modified > '" . $this->request->data['TrnInventoryHeader']['time'] . "'");
                if($ret[0][0]['count'] > 0){
                    $this->Session->setFlash('ほかユーザによって更新されています。最初からやり直してください。', 'growl', array('type'=>'error') );
                    //ロールバック
                    $this->TrnInventoryHeader->rollback();
                    $this->render('/inventories/execution_add_confirm');
                    exit;
                }else{
                    //ファイルアップ
                    $_upfile = $this->request->data['Inventory']['csv_file']['tmp_name'];
                    $_fileName = '../tmp/'.date('YmdHi').'_invent.csv';
                    if (is_uploaded_file($_upfile)){
                        move_uploaded_file($_upfile, mb_convert_encoding($_fileName, 'UTF-8', "SJIS-win"));
                        chmod($_fileName, 0644);
                    }else{
                        //ファイル無しエラー画面に遷移
                        $this->Session->setFlash('ファイルが読み込めませんでした。CSVファイルを確認してください。', 'growl', array('type'=>'error') );
                        $this->render('/inventories/execution_add_confirm');
                        return;
                    }
                    //アップロードしたCSVデータ文字列として読み込む
                    $_tsvdata = mb_convert_encoding(file_get_contents ($_fileName), 'UTF-8', "SJIS-win");
                    // 配列変換
                    $csv_array = $this->utils->input_tsv($_tsvdata);
                    $mapped_csv = $this->_csvMapping($csv_array);

                    unlink($_fileName);

                    //データチェック
                    $work_no  = $this->request->data['TrnInventoryHeader']['work_no']; //棚卸番号

                    //更新用データ
                    $updateInventory = array();
                    $updateInventoryHeader = array();

                    //新規追加用データ
                    $insertInventory = array();

                    //チェック用
                    $depStickers = array(); //シール重複チェック用
                    $read_count    = 0; //読み込み件数カウント
                    $err_count     = 0; //エラー件数カウント
                    
                    $errdata = array();
                    foreach($mapped_csv as $value) {
                        //Hで始まる行はとりあえず無視
                        if($value['header_type'] == 'D') {
                            $read_count++ ;
                            $_barcode = $this->Barcode->readEan128($value['sticker_id']);

                            //川鉄シールの場合
                            if($_barcode != false ){
                                $ret = $this->kawatetu($_barcode);
                                if($ret){
                                    if(isset($kawatetu[$ret['id']])){
                                        $kawatetu[$ret['id']]['quantity']++;
                                    }else{
                                        $kawatetu[$ret['id']]['quantity']          = 1;
                                        $kawatetu[$ret['id']]['id']                = $ret['id'];
                                        $kawatetu[$ret['id']]['mst_facility_id']   = $ret['mst_facility_id'];
                                        $kawatetu[$ret['id']]['mst_department_id'] = $ret['mst_department_id'];
                                        $kawatetu[$ret['id']]['mst_shelf_name_id'] = $ret['mst_shelf_name_id'];
                                    }
                                }else{
                                    //シール情報に該当無
                                    //エラーをカウントアップ
                                    $err_count++;
                                    
                                    $errdata[] = array(
                                        'row_no' => $read_count,
                                        'sticker_value' => $value['sticker_id']
                                        );
                                 }
                            }else{
                                //通常のシールの場合
                                $sql  = ' select ';
                                $sql .= '       a.id                       as "TrnSticker__id" ';
                                $sql .= '     , a.mst_department_id        as "TrnSticker__mst_department_id" ';
                                $sql .= '     , a.facility_sticker_no      as "TrnSticker__facility_sticker_no" ';
                                $sql .= '     , a.hospital_sticker_no      as "TrnSticker__hospital_sticker_no" ';
                                $sql .= '     , c.mst_facility_id          as "TrnSticker__mst_facility_id" ';
                                $sql .= '     , d.id                       as "TrnSticker__trn_inventory_id" ';
                                $sql .= '     , f.id                       as "TrnSticker__mst_shelf_name_id"  ';
                                $sql .= '   from ';
                                $sql .= '     trn_stickers as a  ';
                                $sql .= '     left join mst_item_units as b  ';
                                $sql .= '       on b.id = a.mst_item_unit_id  ';
                                $sql .= '     left join mst_facility_items as c  ';
                                $sql .= '       on c.id = b.mst_facility_item_id  ';
                                $sql .= '     left join trn_inventories as d  ';
                                $sql .= '       on d.trn_sticker_id = a.id  ';
                                $sql .= '       and d.trn_inventory_header_id = ' . $this->request->data['TrnInventoryHeader']['id'];
                                $sql .= '     left join mst_fixed_counts as e  ';
                                $sql .= '       on e.mst_item_unit_id = b.id  ';
                                $sql .= '       and e.mst_department_id = a.mst_department_id  ';
                                $sql .= '     left join mst_shelf_names as f  ';
                                $sql .= '       on f.id = e.mst_shelf_name_id  ';
                                $sql .= '   where ';
                                $sql .= "     a.lot_no <> ' '  ";
                                $sql .= '     and a.is_deleted = false  ';
                                $sql .= '     and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
                                $sql .= '     and (  ';
                                $sql .= "       a.facility_sticker_no = '" . $value['sticker_id'] . "' " ;
                                $sql .= "       or a.hospital_sticker_no = '" . $value['sticker_id'] . "' ";
                                $sql .= '     )  ';
                                $Stickers = $this->TrnSticker->query($sql);

                                if( !empty($Stickers)) {
                                    $Stickers = $Stickers[0];
                                    //シール重複チェック
                                    if(in_array($Stickers['TrnSticker']['id'] , $depStickers , TRUE)){
                                        $err_count++;
                                        
                                        $errdata[] = array(
                                            'row_no' => $read_count,
                                            'sticker_value' => $value['sticker_id']
                                            );
                                    }else{
                                        if(!is_null($Stickers['TrnSticker']['trn_inventory_id'] )){
                                            //棚卸.作業番号にシールIDが関連ついている（更新とする）
                                            $updateInventory[] = array(
                                                'TrnInventory' => array(
                                                    'id'            => $Stickers['TrnSticker']['trn_inventory_id'],
                                                    'real_quantity' => 1 ,
                                                    'is_deleted'    => false,
                                                    'modifier'      => $this->Session->read('Auth.MstUser.id'),
                                                    'modified'      => $now
                                                    )
                                                );


                                        }else{
                                            //作業番号に関連ついていないが、シールID自体は存在する。（追加扱いとする）
                                            //棚卸.作業番号にシールIDが関連ついている（更新とする）
                                            $insertInventory[] = array(
                                                'TrnInventory' => array(
                                                    'work_no'                 => $work_no,
                                                    'real_quantity'           => 1,
                                                    'quantity'                => 0,
                                                    'trn_inventory_header_id' => $this->request->data['TrnInventoryHeader']['id'],
                                                    'mst_shelf_name_id'       => $Stickers['TrnSticker']['mst_shelf_name_id'],
                                                    'mst_facility_id'         => $Stickers['TrnSticker']['mst_facility_id'],
                                                    'mst_department_id'       => $Stickers['TrnSticker']['mst_department_id'],
                                                    'trn_sticker_id'          => $Stickers['TrnSticker']['id'],
                                                    'is_exists'               => true,
                                                    'is_deleted'              => false,
                                                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                                                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                                                    'modified'                => $now,
                                                    'created'                 => $now,
                                                    'facility_sticker_no'     => $Stickers['TrnSticker']['facility_sticker_no'],
                                                    'hospital_sticker_no'     => $Stickers['TrnSticker']['hospital_sticker_no']
                                                    )
                                                );
                                        }
                                    }
                                    array_push($depStickers , $Stickers['TrnSticker']['id'] );
                                }else{
                                    //入力されたシールID自体存在しない。（エラーとして完了画面で商品名の箇所にエラーメッセージを表示、DBには登録しない。）
                                    $err_count++;
                                    $errdata[] = array(
                                        'row_no' => $read_count,
                                        'sticker_value' => $value['sticker_id']
                                        );
                                }
                            }
                        }
                    }

                    if(!empty($kawatetu)){
                        foreach($kawatetu as $k){
                            $updateInventory[] = array(
                            'TrnInventory' => array(
                                'id'                => $k['id'],
                                'real_quantity'     => $k['quantity'],
                                'is_deleted'        => false,
                                'mst_department_id' => $k['mst_department_id'],
                                'mst_facility_id'   => $k['mst_facility_id'],
                                'mst_shelf_name_id' => $k['mst_shelf_name_id'],
                                'modifier'          => $this->Session->read('Auth.MstUser.id'),
                                'modified'          => $now,

                                )
                                );
                        }
                    }
                    $updateInventoryHeader = array(
                        'TrnInventoryHeader' => array(
                            'id'         => $this->request->data['TrnInventoryHeader']['id'],
                            'work_date'  => $this->request->data['selected']['date'],
                            'is_deleted' => false,
                            'modifier'   => $this->Session->read('Auth.MstUser.id'),
                            'modified'   => $now
                            )
                        );

                    // ここで UPDATE & INSER 実施
                    if(!$this->_csvRegist( $insertInventory , $updateInventoryHeader , $updateInventory )){
                        //登録失敗
                        $this->Session->setFlash('登録が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                        //ロールバック
                        $this->TrnInventoryHeader->rollback();
                    }
                    //コミット直前に更新チェック
                    $ret = $this->TrnInventoryHeader->query( ' select count(*) from trn_inventories as a where a.trn_inventory_header_id = ' . $this->request->data['TrnInventoryHeader']['id'] . " and a.modified > '" . $this->request->data['TrnInventoryHeader']['time'] . "' and a.modified <> '" .$now. "'" );
                    if($ret[0][0]['count'] > 0){
                        $this->Session->setFlash('ほかユーザによって更新されています。最初からやり直してください。', 'growl', array('type'=>'error') );
                        //ロールバック
                        $this->TrnInventoryHeader->rollback();

                    }else{
                        //コミット
                        $this->TrnInventoryHeader->commit();
                    }
                }
                $this->Session->write('TrnInventoryHeader.read_count' , $read_count);
                $this->Session->write('TrnInventoryHeader.err_count' , $err_count);
                $this->set('read_count' , $read_count);
                $this->set('err_count' , $err_count);
                $this->set('errdata', $errdata);


        }else{
            //2度押しの場合
            $this->request->data  =  $this->Session->read('TrnInventoryHeader.data');
            $this->set('data' , $this->request->data);
            $this->set('read_count' , $this->Session->read('TrnInventoryHeader.read_count'));
            $this->set('err_count' , $this->Session->read('TrnInventoryHeader.err_count'));
        }

        $this->render('/inventories/execution_add_result');
    }

    /**
     * 棚卸差異一覧初期画面
     * @access public
     */
    function difference_list () {
        App::import('Sanitize');
        $this->setRoleFunction(45); //棚卸差異一覧
        $SearchResult = array();
        $this->set('InventoryType',Configure::read('InventoryTypes'));

        //検索ボタン押下後の処理
        if( isset($this->request->data['search']['is_search']) ) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $where = '';
            //棚卸番号(前方一致)
            if( $this->request->data['search']['work_no'] != '' ){
                $where .= " and a.work_no ilike '" .$this->request->data['search']['work_no']. "%'";
            }
            //棚卸実施名(前方一致)
            if( $this->request->data['search']['title'] != '' ){
                $where .= " and a.title ilike '" .$this->request->data['search']['title']. "%'";
            }
            //作成日FROM
            if( $this->request->data['search']['work_date_from'] != ''  ){
                $where .= " and a.work_date >= '" .$this->request->data['search']['work_date_from']. "%'";
            }
            //作成日TO
            if( $this->request->data['search']['work_date_to'] != '' ){
                $where .= " and a.work_date <= '" .$this->request->data['search']['work_date_to']. "%'";
            }
            $sql  = ' select ';
            $sql .= '       a.id               as "TrnInventoryHeader__id" ';
            $sql .= '     , a.work_no          as "TrnInventoryHeader__work_no" ';
            $sql .= "     , to_char(a.created,'YYYY/mm/dd') ";
            $sql .= '                          as "TrnInventoryHeader__created" ';
            $sql .= '     , a.inventory_type   as "TrnInventoryHeader__inventory_type" ';
            $sql .= '     , a.title            as "TrnInventoryHeader__title" ';
            $sql .= "     , coalesce(to_char(a.work_date, 'YYYY/mm/dd') , '未実施')  ";
            $sql .= '                          as "TrnInventoryHeader__work_date" ';
            $sql .= '     , a.difference_count as "TrnInventoryHeader__difference_count"  ';
            $sql .= '   from ';
            $sql .= '     trn_inventory_headers as a  ';
            $sql .= '   where ';
            $sql .= '     a.is_deleted = false ';
            $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= $where;
            $sql .= '   order by ';
            $sql .= '     a.work_no asc  ';

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnInventoryHeader'));

            $sql .= '   limit ' . $this->request->data['limit'];

            //検索実行
            $SearchResult = $this->TrnInventoryHeader->query($sql);
            $this->request->data = $data;
        }

        $this->set('SearchResult',$this->outputFilter($SearchResult));

        $this->render('/inventories/difference_list');
    }

    /**
     * 棚卸差異一覧明細画面
     * @access public
     */
    function difference_detail () {

        $sql  = ' select ';
        $sql .= '     c.id                  as "Inventory__id" ';
        $sql .= '   , a.work_no             as "Inventory__work_no" ';
        $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') ";
        $sql .= '                           as "Inventory__work_date" ';
        $sql .= '   , e.internal_code       as "Inventory__internal_code" ';
        $sql .= '   , e.item_name           as "Inventory__item_name" ';
        $sql .= '   , e.item_code           as "Inventory__item_code" ';
        $sql .= '   , e.standard            as "Inventory__standard" ';
        $sql .= '   , f.dealer_name         as "Inventory__dealer_name" ';
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when d.per_unit = 1  ';
        $sql .= '       then d1.unit_name  ';
        $sql .= "       else d1.unit_name || '(' || d.per_unit || d2.unit_name || ')' ";
        $sql .= '       end ';
        $sql .= '   )                       as "Inventory__unit_name" ';
        $sql .= '   , c.facility_sticker_no as "Inventory__facility_sticker_no" ';
        $sql .= '   , c.hospital_sticker_no as "Inventory__hospital_sticker_no" ';
        $sql .= "   , h.facility_name || ' / ' ||   g.department_name ";
        $sql .= '                           as "Inventory__real_place" ';
        $sql .= "   , j.facility_name || ' / ' || i.department_name ";
        $sql .= '                           as "Inventory__logical_place" ';
        $sql .= "   , to_char(c.last_move_date, 'YYYY/mm/dd') ";
        $sql .= '                           as "Inventory__last_move_date" ';
        $sql .= '   , coalesce(b.real_quantity, 0) - coalesce(b.quantity, 0) ';
        $sql .= '                           as "Inventory__diff_quantity"';
       
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when c.trn_adjust_id is not null  ';
        $sql .= "       then '調整済み'  ";
        $sql .= '       end ';
        $sql .= '   )                       as "Inventory__status"  ';
        $sql .= ' from ';
        $sql .= '   trn_inventory_headers as a  ';
        $sql .= '   left join trn_inventories as b  ';
        $sql .= '     on b.trn_inventory_header_id = a.id  ';
        $sql .= '   left join trn_stickers as c  ';
        $sql .= '     on c.id = b.trn_sticker_id  ';
        $sql .= '   left join mst_item_units as d  ';
        $sql .= '     on d.id = c.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as d1  ';
        $sql .= '     on d1.id = d.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as d2  ';
        $sql .= '     on d2.id = d.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as e  ';
        $sql .= '     on e.id = d.mst_facility_item_id  ';
        $sql .= '   left join mst_dealers as f  ';
        $sql .= '     on f.id = e.mst_dealer_id  ';
        $sql .= '   left join mst_departments as g  ';
        $sql .= '     on g.id = c.mst_department_id  ';
        $sql .= '   left join mst_facilities as h  ';
        $sql .= '     on h.id = g.mst_facility_id  ';
        $sql .= '   left join mst_departments as i  ';
        $sql .= '     on i.id = b.mst_department_id  ';
        $sql .= '   left join mst_facilities as j  ';
        $sql .= '     on j.id = i.mst_facility_id  ';
        $sql .= ' where ';
        $sql .= '   a.id in ( ' . join(',',$this->request->data['TrnInventoryHeader']['id']) . ')';
        $sql .= '   and b.quantity <> b.real_quantity  ';
        $sql .= '   and c.id is not null  ';
        $sql .= '   and a.is_deleted = false  ';
        $sql .= '   and b.is_deleted = false ';
        $sql .= '   and b.real_quantity is not null ';
        
        $SearchResult = $this->TrnInventoryHeader->query($sql);
        
        //結果をViewで参照できるように
        $this->set('SearchResult',$this->outputFilter($SearchResult));
        //施設リスト
        $this->set('facilities', $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                        array(Configure::read('FacilityType.hospital'),
                                                              Configure::read('FacilityType.center'))
                                                        )
                   );
        //作業区分
        $this->set('classes', $this->getClassesList());

        $this->request->data['setting']['date'] = date('Y/m/d');
        
        //Viewを表示
        $this->render('/inventories/difference_detail');
    }

    /**
     * 棚別棚卸登録処理メイン
     * @access protected
     */

    public function shelf_regist($result) {

        //Begin
        $this->TrnInventory->begin();
        $now = date('Y/m/d H:i:s');

        //作業番号を取得
        $wk_no = $this->setWorkNo4Header( date('Ymd') , 15);
        $this->request->data['inventory']['work_no'] = $wk_no;

        //棚卸情報 Insert
        $TrnInventoryHeader = array(
            //棚卸情報
            'TrnInventoryHeader' => array(
                'work_no'         => $wk_no,
                'work_date'       => NULL,
                'recital'         => $this->request->data['inventory']['recital'],    //備考（入力情報）
                'title'           => $this->request->data['inventory']['title'],      //棚卸名称（入力情報）
                'inventory_type'  => Configure::read('InventoryType.rack'),  //棚卸区分 1：棚別 2：商品別
                'is_deleted'      => FALSE,
                'creater'         => $this->Session->read('Auth.MstUser.id'),
                'created'         => $now,
                'modifier'        => $this->Session->read('Auth.MstUser.id'),
                'modified'        => $now,
                'mst_facility_id' => $this->Session->read('Auth.facility_id_selected')
                )
            );

        //棚卸ヘッダオブジェクトをcreate
        $this->TrnInventoryHeader->create();

        //SQL実行
        if (!$this->TrnInventoryHeader->save($TrnInventoryHeader)) {
            //RollBack
            $this->TrnInventory->rollback();
            return false;
        }

        //棚卸情報の最後に挿入したIDを取得
        $header_id = $this->TrnInventoryHeader->getLastInsertID();
        
        $sql  = ' select ';
        $sql .= '     a.id                  as "inventory__id"';
        $sql .= '   , a.quantity            as "inventory__quantity"';
        $sql .= '   , b.mst_facilities_id   as "inventory__mst_facility_id"';
        $sql .= '   , b.mst_department_id   as "inventory__mst_department_id"';
        $sql .= '   , a.facility_sticker_no as "inventory__facility_sticker_no"';
        $sql .= '   , a.hospital_sticker_no as "inventory__hospital_sticker_no" ';
        $sql .= ' from ';
        $sql .= '   trn_stickers as a  ';
        $sql .= '   left join mst_fixed_counts as b ';
        $sql .= '     on a.mst_item_unit_id = b.mst_item_unit_id  ';
        $sql .= '     and a.mst_department_id = b.mst_department_id  ';
        $sql .= ' where ';
        $sql .= "   a.lot_no != ' '  ";
        $sql .= '   and a.is_deleted = false  ';
        $sql .= '   and a.quantity > 0 ';
        
        foreach($result as $r) {
            if($r['inventory']['id'] > 0){
                $where = ' and b.mst_shelf_name_id = ' . $r['inventory']['id'];
                $stickers = $this->TrnSticker->query($sql . $where);
                unset($Inventory_data);
                if(count($stickers) > 0) {
                    foreach($stickers as $s ) {
                        //棚卸 Insert
                        $Inventory_data[] = array(
                            'TrnInventory' => array(
                                'work_no'                  => $wk_no,
                                'work_type'                => $this->request->data['inventory']['work_type'],//作業区分ID （入力情報）
                                'trn_sticker_id'           => $s['inventory']['id'],
                                'quantity'                 => $s['inventory']['quantity'],
                                'real_quantity'            => 0,
                                'is_exists'                => TRUE,
                                'is_initial'               => TRUE,                                 //初期データフラグ(クリア時の判定用）
                                'trn_inventory_header_id'  => $header_id,
                                'mst_shelf_name_id'        => $r['inventory']['id'],
                                'is_deleted'               => FALSE,
                                'creater'                  => $this->Session->read('Auth.MstUser.id'),
                                'created'                  => $now,
                                'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                                'modified'                 => $now,
                                'mst_facility_id'          => $s['inventory']['mst_facility_id'],
                                'mst_department_id'        => $s['inventory']['mst_department_id'],
                                'facility_sticker_no'      => $s['inventory']['facility_sticker_no'],
                                'hospital_sticker_no'      => $s['inventory']['hospital_sticker_no']
                                )
                            );
                    }

                    //棚卸オブジェクトをcreate
                    $this->TrnInventory->create();

                    //SQL実行
                    if (!$this->TrnInventory->saveAll($Inventory_data , array('validates' => true,'atomic' => false))) {
                        //RollBack
                        $this->TrnInventory->rollback();

                        return false;
                    }
                }
            }else{
                unset($Inventory_data);
                $where  = ' and b.mst_shelf_name_id is null  ';
                $where .= ' and a.mst_department_id = ' . $r['inventory']['id'] * -1 ;
                
                $stickers = $this->TrnSticker->query($sql . $where);

                if(count($stickers) > 0) {
                    foreach($stickers as $s ) {
                        //棚卸 Insert
                        $Inventory_data[] = array(
                            'TrnInventory' => array(
                                'work_no'                  => $wk_no,
                                'work_type'                => $this->request->data['inventory']['work_type'],//作業区分ID （入力情報）
                                'trn_sticker_id'           => $s['inventory']['id'],
                                'quantity'                 => $s['inventory']['quantity'],
                                'real_quantity'            => 0,
                                'is_exists'                => TRUE,
                                'is_initial'               => TRUE,                                 //初期データフラグ(クリア時の判定用）
                                'trn_inventory_header_id'  => $header_id,
                                'is_deleted'               => FALSE,
                                'creater'                  => $this->Session->read('Auth.MstUser.id'),
                                'created'                  => $now,
                                'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                                'modified'                 => $now,
                                'mst_facility_id'          => $s['inventory']['mst_facility_id'],
                                'mst_department_id'        => $s['inventory']['mst_department_id'],
                                "facility_sticker_no"      => $s['inventory']['facility_sticker_no'],
                                "hospital_sticker_no"      => $s['inventory']['hospital_sticker_no'],
                                )
                            );
                    }

                    //棚卸オブジェクトをcreate
                    $this->TrnInventory->create();

                    //SQL実行
                    if (!$this->TrnInventory->saveAll($Inventory_data , array('validates' => true,'atomic' => false))) {
                        //RollBack
                        $this->TrnInventory->rollback();
                        return false;
                    }
                }
            }
        }

        $this->TrnInventory->commit();

        return true;
    }


    /**
     * 商品別棚卸登録処理メイン
     * @access protected
     */

    protected function commodity_regist($result) {
        //Begin
        $this->TrnInventory->begin();
        $now = date('Y/m/d H:i:s');

        // 結果表示データ取得用
        $sticker_list = array();
        
        //作業番号を取得
        $wk_no = $this->setWorkNo4Header( date('Ymd') , 15);
        $this->request->data['inventory']['work_no'] = $wk_no;

        //棚卸情報 Insert
        $TrnInventoryHeader = array(
            //棚卸情報
            'TrnInventoryHeader' => array(
                'work_no'         => $wk_no,
                'work_date'       => NULL,
                'recital'         => $this->request->data['inventory']['recital'],    //備考（入力情報）
                'title'           => $this->request->data['inventory']['title'],      //棚卸名称（入力情報）
                'inventory_type'  => Configure::read('InventoryType.item'),  //棚卸区分 2：棚別 2：商品別
                'is_deleted'      => FALSE,
                'creater'         => $this->Session->read('Auth.MstUser.id'),
                'created'         => $now,
                'modifier'        => $this->Session->read('Auth.MstUser.id'),
                'modified'        => $now,
                'mst_facility_id' => $this->Session->read('Auth.facility_id_selected')
                )
            );

        //棚卸ヘッダオブジェクトをcreate
        $this->TrnInventoryHeader->create();

        //SQL実行
        if (!$this->TrnInventoryHeader->save($TrnInventoryHeader)) {
            //RollBack
            $this->TrnInventory->rollback();
            return false;
        }

        //棚卸情報の最後に挿入したIDを取得
        $header_id = $this->TrnInventoryHeader->getLastInsertID();
        
        $sql  = ' select ';
        $sql .= '     a.id                  as "inventory__id"';
        $sql .= '   , a.quantity            as "inventory__quantity"';
        $sql .= '   , b.mst_facilities_id   as "inventory__mst_facility_id"';
        $sql .= '   , b.mst_department_id   as "inventory__mst_department_id"';
        $sql .= '   , a.facility_sticker_no as "inventory__facility_sticker_no"';
        $sql .= '   , a.hospital_sticker_no as "inventory__hospital_sticker_no" ';
        $sql .= ' from ';
        $sql .= '   trn_stickers as a  ';
        $sql .= '   left join mst_fixed_counts as b  ';
        $sql .= '     on a.mst_item_unit_id = b.mst_item_unit_id  ';
        $sql .= '     and a.mst_department_id = b.mst_department_id  ';
        $sql .= ' where ';
        $sql .= "   a.lot_no != ' '  ";
        $sql .= '   and a.is_deleted = false  ';
        $sql .= '   and a.quantity > 0 ';
        
        foreach($result as $r) {
            if($r['inventory']['id'] > 0){
                $where  = ' and b.mst_shelf_name_id = ' . $r['inventory']['id'];
                $where .= ' and a.mst_item_unit_id in ( ' . $this->request->data['search']['tempId'] . ')';
                $stickers = $this->TrnSticker->query($sql . $where);

                if(count($stickers) > 0) {
                    foreach($stickers as $s ) {
                        //棚卸 Insert
                        $Inventory_data[] = array(
                            'TrnInventory' => array(
                                'work_no'                  => $wk_no,
                                'work_type'                => $this->request->data['inventory']['work_type'],//作業区分ID （入力情報）
                                'trn_sticker_id'           => $s['inventory']['id'],
                                'quantity'                 => $s['inventory']['quantity'],
                                'real_quantity'            => 0,
                                'is_exists'                => TRUE,
                                'is_initial'               => TRUE,                                 //初期データフラグ(クリア時の判定用）
                                'trn_inventory_header_id'  => $header_id,
                                'mst_shelf_name_id'        => $r['inventory']['id'],
                                'is_deleted'               => FALSE,
                                'creater'                  => $this->Session->read('Auth.MstUser.id'),
                                'created'                  => $now,
                                'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                                'modified'                 => $now,
                                'mst_facility_id'          => $s['inventory']['mst_facility_id'],
                                'mst_department_id'        => $s['inventory']['mst_department_id'],
                                'facility_sticker_no'      => $s['inventory']['facility_sticker_no'],
                                'hospital_sticker_no'      => $s['inventory']['hospital_sticker_no']
                                )
                            );
                        array_push($sticker_list, $s['inventory']['id']);
                    }

                    //棚卸オブジェクトをcreate
                    $this->TrnInventory->create();

                    //SQL実行
                    if (!$this->TrnInventory->saveAll($Inventory_data , array('validates' => true,'atomic' => false))) {
                        //RollBack
                        $this->TrnInventory->rollback();

                        return false;
                    }
                }
            }else{
                
                $where  = ' and b.mst_shelf_name_id is null  ';
                $where .= ' and a.mst_department_id = ' . $r['inventory']['id'] * -1 ;
                $where .= ' and a.mst_item_unit_id in ( ' . $this->request->data['search']['tempId'] . ')';
                
                $stickers = $this->TrnSticker->query($sql . $where);

                if(count($stickers) > 0) {
                    foreach($stickers as $s ) {
                        //棚卸 Insert
                        $Inventory_data[] = array(
                            'TrnInventory' => array(
                                'work_no'                  => $wk_no,
                                'work_type'                => $this->request->data['inventory']['work_type'],//作業区分ID （入力情報）
                                'trn_sticker_id'           => $s['inventory']['id'],
                                'quantity'                 => $s['inventory']['quantity'],
                                'real_quantity'            => 0,
                                'is_exists'                => TRUE,
                                'is_initial'               => TRUE,                                 //初期データフラグ(クリア時の判定用）
                                'trn_inventory_header_id'  => $header_id,
                                'is_deleted'               => FALSE,
                                'creater'                  => $this->Session->read('Auth.MstUser.id'),
                                'created'                  => $now,
                                'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                                'modified'                 => $now,
                                'mst_facility_id'          => $s['inventory']['mst_facility_id'],
                                'mst_department_id'        => $s['inventory']['mst_department_id'],
                                "facility_sticker_no"      => $s['inventory']['facility_sticker_no'],
                                "hospital_sticker_no"      => $s['inventory']['hospital_sticker_no'],
                                )
                            );
                        array_push($sticker_list, $s['inventory']['id']);
                    }

                    //棚卸オブジェクトをcreate
                    $this->TrnInventory->create();

                    //SQL実行
                    if (!$this->TrnInventory->saveAll($Inventory_data , array('validates' => true,'atomic' => false))) {
                        //RollBack
                        $this->TrnInventory->rollback();
                        return false;
                    }
                }
            }
        }

        //結果表示データ取得
        $sql  = ' select ';
        $sql .= '     e.facility_name   as "inventory__facility_name"';
        $sql .= '   , d.department_name as "inventory__department_name"';
        $sql .= '   , c.name            as "inventory__name"';
        $sql .= '   , g.internal_code   as "inventory__internal_code"';
        $sql .= '   , g.item_name       as "inventory__item_name"';
        $sql .= '   , g.item_code       as "inventory__item_code"';
        $sql .= '   , g.standard        as "inventory__standard"';
        $sql .= '   , h.dealer_name     as "inventory__dealer_name"';
        $sql .= '   , ( case  ';
        $sql .= '     when f.per_unit = 1  ';
        $sql .= '     then f1.unit_name  ';
        $sql .= "     else f1.unit_name || '(' || f.per_unit || f2.unit_name || ')'  ";
        $sql .= '     end )             as "inventory__unit_name"';
        $sql .= '   , count(*)          as "inventory__count" ';
        $sql .= ' from ';
        $sql .= '   trn_stickers as a  ';
        $sql .= '   left join mst_fixed_counts as b  ';
        $sql .= '     on b.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '     and b.mst_department_id = a.mst_department_id  ';
        $sql .= '   left join mst_shelf_names as c  ';
        $sql .= '     on c.id = b.mst_shelf_name_id  ';
        $sql .= '   left join mst_departments as d  ';
        $sql .= '     on d.id = a.mst_department_id  ';
        $sql .= '   left join mst_facilities as e  ';
        $sql .= '     on e.id = d.mst_facility_id  ';
        $sql .= '   left join mst_item_units as f  ';
        $sql .= '     on f.id = a.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as f1  ';
        $sql .= '     on f1.id = f.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as f2  ';
        $sql .= '     on f2.id = f.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as g  ';
        $sql .= '     on g.id = f.mst_facility_item_id  ';
        $sql .= '   left join mst_dealers as h  ';
        $sql .= '     on h.id = g.mst_dealer_id  ';
        $sql .= ' where a.id in ( ' . join(',',$sticker_list) . ')';
        $sql .= ' group by ';
        $sql .= '   e.facility_name ';
        $sql .= '   , e.facility_code ';
        $sql .= '   , d.department_name ';
        $sql .= '   , d.department_code ';
        $sql .= '   , c.name ';
        $sql .= '   , g.internal_code ';
        $sql .= '   , g.item_name ';
        $sql .= '   , g.item_code ';
        $sql .= '   , g.standard ';
        $sql .= '   , h.dealer_name ';
        $sql .= '   , f.per_unit ';
        $sql .= '   , f1.unit_name ';
        $sql .= '   , f2.unit_name ';
        $sql .= ' order by e.facility_code , d.department_code , g.internal_code , f.per_unit ';
        if(count($sticker_list)>0){
            $result = $this->TrnSticker->query($sql);
        }else{
            $result = array();
        }
        $this->TrnInventory->commit();

        return $result;

    }

    /**
     *
     * 選択商品検索処理
     *
     */
    function search_item_inventories($condition){
        $SearchResult = array();
        $this->MstFacilityItem->recursive = -1;

        $options['fields'] = array(   'MstFacilityItem.id'
                                      , 'MstFacilityItem.internal_code'
                                      , 'MstFacilityItem.item_name'
                                      , 'MstFacilityItem.standard'
                                      , 'MstFacilityItem.item_code'
                                      , 'IUA.unit_name'
                                      , 'DLR.dealer_name'
                                      );

        /* JOINテーブル */
        //単位名
        $options['joins'][] = array('type'=>'left'
                                    ,'table'=>'mst_unit_names'
                                    ,'alias'=>'IUA'
                                    ,'conditions'=>array('MstFacilityItem.mst_unit_name_id = IUA.id')
                                    );

        //販売元
        $options['joins'][] = array('type'=>'left'
                                    ,'table'=>'mst_dealers'
                                    ,'alias'=>'DLR'
                                    ,'conditions'=>array('MstFacilityItem.mst_dealer_id = DLR.id')
                                    );


        /* WHERE句 */
        $options["conditions"] = array("MstFacilityItem.id " => $condition);

        /* ORDER BY句 */
        $options['order'] = array('MstFacilityItem.internal_code');

        //検索実行
        $this->MstFacilityItem->recursive = -1;
        $SearchResult = $this->MstFacilityItem->find('all', $options);

        return $SearchResult;
    }



    function setClass(){
        $classes = Set::Combine($this->MstClass->find('all', array(
            'order' => 'MstClass.code',
            'conditions' => array('MstClass.mst_menu_id' => array_keys(Configure::read('workNoModels'),"TrnInventoryHeader")
                                  ,"MstClass.mst_facility_id" => $this->Session->read('Auth.facility_id_selected')
                                  ), //棚卸作業区分
            )),'{n}.MstClass.id','{n}.MstClass.name');

        $this->set('classes', $classes);
    }

    /* シール + 棚卸のデータもろもろJOINして返す。*/
    function searchInventory( $where = null , $limit = null , $join = null , $order = array('mfi.internal_code') , $mode='all'){
        $SearchResult = array();

        //別名指定
        $this->TrnSticker->virtualFields = array(
            'logical_place'       => 'mfa2.facility_name || \' / \' || mde2.department_name',
            'diff_quantity'       => 'coalesce(tin.real_quantity , 0) - coalesce(tin.quantity , 0)',
            'real_place'          => 'mfa.facility_name || \' / \' || mde.department_name',
            'inventory_id'        => 'tin.id',
            'old_facility_name'   => 'mfa2.facility_name',
            'old_facility_code'   => 'mfa2.facility_code',
            'old_department_name' => 'mde2.department_name',
            'old_department_code' => 'mde2.department_code',
            'sticker_id'          => 'TrnSticker.id',
            'old_shelf_name'      => 'msn2.name',
            'shelf_name'          => 'msn.name',
            'shelf_name_id'       => 'msn.id',
            'status'              => 'CASE
                                          WHEN tco.id is not NULL THEN \'調整済み\'
                                      END ',
            'price' => '(tav.price * ( coalesce(tin.real_quantity , 0) - coalesce(tin.quantity , 0) ) )'
            );
        //カラム
        $options['fields'] = array(   'TrnSticker.sticker_id'
                                      ,'TrnSticker.inventory_id'
                                      ,'tih.id'
                                      ,'tih.title'
                                      ,'TrnSticker.quantity'
                                      ,'tin.work_no'
                                      ,'tin.real_quantity'
                                      ,'tin.quantity'
                                      ,'mfi.internal_code'
                                      ,'mfi.item_name'
                                      ,'mfi.item_code'
                                      ,'mun.unit_name'
                                      ,'TrnSticker.facility_sticker_no'
                                      ,'TrnSticker.logical_place'
                                      ,'TrnSticker.validated_date'
                                      ,'tin.modified'
                                      ,'tih.work_date'
                                      ,'mfi.standard'
                                      ,'mdr.dealer_name'
                                      ,'TrnSticker.diff_quantity'
                                      ,'TrnSticker.hospital_sticker_no'
                                      ,'TrnSticker.real_place'
                                      ,'tin.recital'
                                      ,'mfi.mst_facility_id'
                                      ,'TrnSticker.mst_department_id'
                                      ,'TrnSticker.shelf_name_id'
                                      ,'mfa.facility_name'
                                      ,'mfa.facility_code'
                                      ,'mde.department_name'
                                      ,'mde.department_code'
                                      ,'TrnSticker.old_facility_name'
                                      ,'TrnSticker.old_facility_code'
                                      ,'TrnSticker.old_department_name'
                                      ,'TrnSticker.old_department_code'
                                      ,'date_part(\'day\' , current_timestamp - coalesce("TrnSticker"."last_move_date","tre"."work_date","TrnSticker"."created"))'
                                      ,'mfc.fixed_count'
                                      ,'TrnSticker.lot_no'
                                      ,'to_char("TrnSticker"."last_move_date",\'YYYY/mm/dd\') as "TrnSticker__last_move_date"'
                                      ,'TrnSticker.shelf_name'
                                      ,'TrnSticker.old_shelf_name'
                                      ,'TrnSticker.status'
                                      ,'TrnSticker.price'
                                      ,"tin.facility_sticker_no"
                                      ,"tin.hospital_sticker_no"
                                      );
        //結合
        //棚卸
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'trn_inventories'
                                    ,'alias'=>'tin'
                                    ,'conditions'=>array('TrnSticker.id = tin.trn_sticker_id')
                                    );

        //部署(現在)
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_departments'
                                    ,'alias'=>'mde'
                                    ,'conditions'=>array('mde.id = TrnSticker.mst_department_id')
                                    );
        //施設(現在)
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_facilities'
                                    ,'alias'=>'mfa'
                                    ,'conditions'=>array('mfa.id  = mde.mst_facility_id')
                                    );

        //包装単位
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_item_units'
                                    ,'alias'=>'miu'
                                    ,'conditions'=>array('miu.id = TrnSticker.mst_item_unit_id')
                                    );


        //単位名
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_unit_names'
                                    ,'alias'=>'mun'
                                    ,'conditions'=>array('mun.id = miu.mst_unit_name_id')
                                    );

        //施設採用品
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_facility_items'
                                    ,'alias'=>'mfi'
                                    ,'conditions'=>array('mfi.id = miu.mst_facility_item_id')
                                    );

        //棚卸情報
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'trn_inventory_headers'
                                    ,'alias'=>'tih'
                                    ,'conditions'=>array('tih.id = tin.trn_inventory_header_id')
                                    );

        //出荷
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'trn_shippings'
                                    ,'alias'=>'tsp'
                                    ,'conditions'=>array('tsp.id = TrnSticker.trn_shipping_id')
                                    );

        //販売元
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_dealers'
                                    ,'alias'=>'mdr'
                                    ,'conditions'=>array('mdr.id = mfi.mst_dealer_id')
                                    );

        //部署(棚卸実施時)
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_departments'
                                    ,'alias'=>'mde2'
                                    ,'conditions'=>array('mde2.id = tin.mst_department_id')
                                    );
        //施設(棚卸実施時)
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_facilities'
                                    ,'alias'=>'mfa2'
                                    ,'conditions'=>array('mfa2.id  = tin.mst_facility_id')
                                    );


        //棚名称(棚卸実施時)
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_shelf_names'
                                    ,'alias'=>'msn2'
                                    ,'conditions'=>array('msn2.id  = tin.mst_shelf_name_id' )
                                    );



        //在庫
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'trn_stocks'
                                    ,'alias'=>'tst'
                                    ,'conditions'=>array('tst.mst_item_unit_id = miu.id',
                                                         'tst.mst_department_id = mde.id')
                                    );


        //定数設定
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_fixed_counts'
                                    ,'alias'=>'mfc'
                                    ,'conditions'=>array('mfc.mst_item_unit_id = TrnSticker.mst_item_unit_id',
                                                         'mfc.mst_department_id = TrnSticker.mst_department_id')
                                    );

        //棚名称(現在)
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_shelf_names'
                                    ,'alias'=>'msn'
                                    ,'conditions'=>array('msn.id  = mfc.mst_shelf_name_id')
                                    );
        //調整
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'(select x.price, x.mst_facility_item_id from trn_appraised_values as x inner join (select a.mst_facility_item_id, max(a.trn_close_header_id) as trn_close_header_id from trn_appraised_values as a where a.is_deleted = false group by a.mst_facility_item_id) as y on x.mst_facility_item_id = y.mst_facility_item_id and x.trn_close_header_id = y.trn_close_header_id where x.is_deleted = false)'
                                    ,'alias'=>'tav'
                                    ,'conditions'=>array(
                                        'tav.mst_facility_item_id = mfi.id'
                                        )
                                    );

        //消費
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'trn_consumes'
                                    ,'alias'=>'tco'
                                    ,'conditions'=>array('tco.id = TrnSticker.trn_adjust_id')
                                    );

        //入荷
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'trn_receivings'
                                    ,'alias'=>'tre'
                                    ,'conditions'=>array('tre.id = TrnSticker.trn_receiving_id')
                                    );


        //追加JOIN
        if($join != null){
            $options["joins"][] = $join;
        }


        //Where句
        $options["conditions"][] = array("TrnSticker.is_deleted =" => false );

        //追加Where
        if($where != null){
            $options["conditions"][] = $where;
        }

        //Limit
        if($limit != null){
            $options['limit'] = $limit;
        }

        /* ORDER BY句 */
        $options['order'] = $order;

        //検索実行
        $this->TrnSticker->recursive = -1;
        $SearchResult = $this->TrnSticker->find($mode, $options);

        //後始末
        $this->TrnSticker->virtualFields = null ;
        return $SearchResult;
    }


    /* 棚卸情報のデータを返す。*/
    function searchInventoryHeader( $where = null , $limit = null , $join = null , $mode= 'all'){
        $SearchResult = array();

        //カラム
        $options['fields'] = array('TrnInventoryHeader.id'
                                   , 'TrnInventoryHeader.work_no'
                                   , 'TrnInventoryHeader.created'
                                   , 'TrnInventoryHeader.title'
                                   , 'TrnInventoryHeader.inventory_type'
                                   , 'TrnInventoryHeader.work_date'
                                   , 'TrnInventoryHeader.difference_count'
                                   );

        //追加JOIN
        if($join != null){
            $options["joins"][] = $join;
        }

        //追加Where
        if($where != null){
            $options["conditions"][] = $where;
        }

        //Limit
        if($limit != null){
            $options['limit'] = $limit;
        }

        /* ORDER BY句 */
        $options['order'] = array('TrnInventoryHeader.id');

        //検索実行
        $this->TrnInventoryHeader->recursive = -1;
        $SearchResult = $this->TrnInventoryHeader->find($mode, $options);

        return $SearchResult;
    }



    /* シールを検索して返す。*/
    function searchStickers( $where = null , $limit = null , $join = null , $mode = 'all'){
        $SearchResult = array();

        //別名指定
        $this->TrnSticker->virtualFields = array(
            'status'              => 'CASE
                                          WHEN tsp.move_status = 1   THEN \'移動予定\'
                                          WHEN tsp.move_status = 10  THEN \'移動予定\'
                                      ELSE
                                          CASE
                                              WHEN TrnSticker.trn_return_receiving_id IS NOT NULL THEN
                                              CASE
                                                  WHEN TrnSticker.trn_return_header_id IS NULL THEN \'返品予定\'
                                              ELSE \'\'
                                              END
                                           ELSE
                                               CASE
                                                   WHEN TrnSticker.has_reserved = true THEN \'予約中\'
                                               ELSE \'\'
                                               END
                                           END
                                      END ');



        //カラム
        $options['fields'] = array(   'TrnSticker.id'
                                      ,'TrnSticker.quantity'
                                      , 'mfi.id'
                                      , 'mfi.internal_code'
                                      , 'mfi.item_name'
                                      , 'mfi.item_code'
                                      , 'mun.unit_name'
                                      , 'TrnSticker.facility_sticker_no'
                                      , 'TrnSticker.validated_date'
                                      , 'tsg.storage_status'
                                      , 'mfi.standard'
                                      , 'mdr.dealer_name'
                                      , 'TrnSticker.hospital_sticker_no'
                                      , 'mfi.mst_facility_id'
                                      , 'TrnSticker.mst_department_id'
                                      , 'mfa.facility_name'
                                      , 'mfa.facility_code'
                                      , 'mde.department_name'
                                      , 'mde.department_code'
                                      , 'mfc.fixed_count'
                                      , 'msn.id'
                                      , 'msn.name'
                                      , 'TrnSticker.lot_no'
                                      , 'TrnSticker.last_move_date'
                                      , 'date_part(\'day\' , current_timestamp - last_move_date)'
                                      , 'TrnSticker.status'
                                      , 'TrnSticker.hospital_sticker_no'
                                      );

        //結合
        //部署(現在)
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_departments'
                                    ,'alias'=>'mde'
                                    ,'conditions'=>array('mde.id = TrnSticker.mst_department_id')
                                    );

        //施設(現在)
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_facilities'
                                    ,'alias'=>'mfa'
                                    ,'conditions'=>array('mfa.id  = mde.mst_facility_id')
                                    );

        //包装単位
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_item_units'
                                    ,'alias'=>'miu'
                                    ,'conditions'=>array('miu.id = TrnSticker.mst_item_unit_id')
                                    );


        //単位名
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_unit_names'
                                    ,'alias'=>'mun'
                                    ,'conditions'=>array('mun.id = miu.mst_unit_name_id')
                                    );

        //施設採用品
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_facility_items'
                                    ,'alias'=>'mfi'
                                    ,'conditions'=>array('mfi.id = miu.mst_facility_item_id')
                                    );

        //入庫
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'trn_storages'
                                    ,'alias'=>'tsg'
                                    ,'conditions'=>array('tsg.id = TrnSticker.trn_storage_id')
                                    );

        //出荷
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'trn_shippings'
                                    ,'alias'=>'tsp'
                                    ,'conditions'=>array('tsp.id = TrnSticker.trn_shipping_id')
                                    );

        //販売元
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_dealers'
                                    ,'alias'=>'mdr'
                                    ,'conditions'=>array('mdr.id = mfi.mst_dealer_id')
                                    );
        //在庫
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'trn_stocks'
                                    ,'alias'=>'tst'
                                    ,'conditions'=>array('tst.mst_item_unit_id = miu.id',
                                                         'tst.mst_department_id = mde.id')
                                    );

        //定数設定
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_fixed_counts'
                                    ,'alias'=>'mfc'
                                    ,'conditions'=>array('mfc.trn_stock_id = tst.id')
                                    );

        //棚名称(現在)
        $options['joins'][] = array('type'=>'LEFT'
                                    ,'table'=>'mst_shelf_names'
                                    ,'alias'=>'msn'
                                    ,'conditions'=>array('msn.id  = mfc.mst_shelf_name_id')
                                    );


        //追加JOIN
        if($join != null){
            $options["joins"][] = $join;
        }


        //Where句
        $options["conditions"][] = array("TrnSticker.is_deleted =" => false );
        $options["conditions"][] = array("TrnSticker.lot_no !=" => ' ' );

        //追加Where
        if($where != null){
            $options["conditions"][] = $where;
        }

        //Limit
        if($limit != null){
            $options['limit'] = $limit;
        }

        /* ORDER BY句 */
        $options['order'] = array('mfi.internal_code');

        //検索実行
        $this->TrnSticker->recursive = -1;
        $SearchResult = $this->TrnSticker->find($mode, $options);

        return $SearchResult;
    }


    function inventoryUpdate(){
        $now = date('Y/m/d H:i:s');
        //Begin
        $this->TrnInventory->begin();

        $i=0;
        if(isset($this->request->data['id']) && isset($this->request->data['chkInventory'])){
            foreach($this->request->data['id'] as $id ) {
                //棚卸 Update
                if(isset($this->request->data['chkInventory'][$i])){
                    if($this->request->data['quantity'][$i] != "" && $this->request->data['chkInventory'][$i] != "" ){
                        $Inventory_data[] = array(
                            'TrnInventory' => array(
                                'id'            => $id,
                                'real_quantity' => $this->request->data['quantity'][$i],
                                'recital'       => $this->request->data['recital'][$i],
                                'is_deleted'    => false
                                )
                            );
                    }
                }
                $i++;
            }
        }else{
            foreach($this->request->data['quantity'] as $quantity ) {
                //棚卸 Insert

                //カラム
                $options['fields'] = array(   'TrnInventory.id'
                                              ,'TrnInventory.work_type'
                                              ,'TrnInventory.work_no'
                                              );

                //Where句
                $options["conditions"][] = array("TrnInventory.is_deleted =" => false,
                                                 "TrnInventory.trn_inventory_header_id =" => $this->request->data['TrnInventoryHeader']['id']
                                                 );

                //検索実行
                if(!empty($TrnInventory)){
                    $work_type = $TrnInventory['TrnInventory']['work_type'];
                    $work_no = $TrnInventory['TrnInventory']['work_no'];
                }else{
                    $work_type = '';
                    $work_no = $this->request->data['work_no'];
                }

                if(isset($this->request->data['chkInventory'][$i])) {
                    if($this->request->data['quantity'][$i] != "" && $this->request->data['chkInventory'][$i] != "" ){

                        // シール情報を検索
                        $sticker = $this->searchStickers(array(' TrnSticker.id = '  => $this->request->data['TrnSticker']['sticker_id'][$i]) , null , null , 'first');

                        $Inventory_data[] = array(
                            'TrnInventory' => array(
                                'real_quantity'            => $this->request->data['quantity'][$i],
                                'work_no'                  => $work_no,
                                'recital'                  => $this->request->data['recital'][$i],
                                'is_deleted'               => false,
                                'work_type'                => $work_type,                        //作業区分ID （入力情報）
                                'trn_sticker_id'           => $sticker['TrnSticker']['id'],
                                'quantity'                 => 0,                                 //予定にないので、予定数量は0固定
                                'is_exists'                => TRUE,
                                'trn_inventory_header_id'  => $this->request->data['TrnInventoryHeader']['id'],
                                'mst_shelf_name_id'        => $sticker['msn']['id'],
                                'creater'                  => $this->Session->read('Auth.MstUser.id'),
                                'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                                'created'                  => $now,
                                'modified'                 => $now,
                                'mst_department_id'        => $sticker['TrnSticker']['mst_department_id'],
                                'mst_facility_id'          => $sticker['mfi']['mst_facility_id'],
                                "facility_sticker_no"      => $sticker["TrnSticker"]["facility_sticker_no"],
                                "hospital_sticker_no"      => $sticker["TrnSticker"]["hospital_sticker_no"],
                                )
                            );
                    }
                }
                $i++;
            }
        }

        if(isset($Inventory_data)){

            //棚卸オブジェクトをcreate
            $this->TrnInventory->create();

            //SQL実行
            if (!$this->TrnInventory->saveAll($Inventory_data , array('validates' => true,'atomic' => false))) {
                $this->TrnInventory->rollback();
                return false;
            }

            unset($options);

            $options['fields'] = array('count(TrnInventory.trn_inventory_header_id)');

            //Where句
            $options["conditions"][] = array('TrnInventory.quantity <> TrnInventory.real_quantity',
                                             'NOT'=> array('TrnInventory.real_quantity'=>NULL),
                                             'TrnInventory.is_deleted' => false ,
                                             'TrnInventory.trn_inventory_header_id =' => $this->request->data['TrnInventoryHeader']['id']
                                             );
            $options["group"][] = 'TrnInventory.trn_inventory_header_id';

            $this->TrnInventory->recursive = -1;
            $ret = $this->TrnInventory->find('all' , $options);
            $count = isset($ret[0][0]['count'])?$ret[0][0]['count']:0;
            //棚卸情報 Insert
            $InventoryHeader_data = array(
                //棚卸情報
                'TrnInventoryHeader' => array(
                    'id'               => $this->request->data['TrnInventoryHeader']['id'],
                    'difference_count' => $count,
                    'work_date'        => $now,
                    'is_deleted'       => FALSE,
                    'modifier'         => $this->Session->read('Auth.MstUser.id') ,
                    'modified'         => $now
                    )
                );

            //棚卸ヘッダオブジェクトをcreate
            $this->TrnInventoryHeader->create();

            //SQL実行
            if (!$this->TrnInventoryHeader->save($InventoryHeader_data)) {
                //RollBack
                $this->TrnInventory->rollback();
                return false;
            }
        }

        $this->TrnInventory->commit();
        return true;
    }

    private function _csvMapping($csv_array){

        $tmp = array();
        $data = array();

        foreach($csv_array as $title => $row ){
            $tmp = explode ( "," , $row[0] );
            if(count($tmp) > 3){
                $data[] = array("header_type" => $tmp[0] , "quantity" => $tmp[1] , "sticker_id" => trim($tmp[2]));
            }
        }
        return $data;
    }

    private function _csvRegist($insertData , $updateHeader , $updateData){
        if(count($insertData) >0){
            //棚卸オブジェクトをcreate
            $this->TrnInventory->create();
            //SQL実行
            if (!$this->TrnInventory->saveAll($insertData , array('validates' => true,'atomic' => false))) {
                return false;
            }
        }

        if(count($updateData)>0){
            //棚卸オブジェクトをcreate
            $this->TrnInventory->create();

            //SQL実行
            if (!$this->TrnInventory->saveAll($updateData , array('validates' => true,'atomic' => false))) {
                return false;
            }
        }


        $options['fields'] = array('count(TrnInventory.trn_inventory_header_id)');

        //Where句
        $options["conditions"][] = array('TrnInventory.quantity <> TrnInventory.real_quantity',
                                         'NOT'=> array('TrnInventory.real_quantity'=>NULL),
                                         'TrnInventory.is_deleted' => false ,
                                         'TrnInventory.trn_inventory_header_id =' => $this->request->data['TrnInventoryHeader']['id']
                                         );
        $options["group"][] = 'TrnInventory.trn_inventory_header_id';

        $this->TrnInventory->recursive = -1;
        $ret = $this->TrnInventory->find('all' , $options);
        $count = isset($ret[0][0]['count'])?$ret[0][0]['count']:0;

        $updateHeader['TrnInventoryHeader']['difference_count'] = $count; // 差異カウント
        //棚卸ヘッダオブジェクトをcreate
        $this->TrnInventoryHeader->create();

        //SQL実行
        if (!$this->TrnInventoryHeader->saveAll($updateHeader , array('validates' => true,'atomic' => false))) {
            return false;
        }
        return true;

    }


    /**
     * @param
     * @return
     */
    function inventory_report() {
        //棚卸一覧

        $where[] = array("tih.is_deleted" => false);

        //棚卸情報のIDがあった場合
        if( isset($this->request->data['TrnInventoryHeader']['id']) ){
            $where[] = array("tih.id" => $this->request->data['TrnInventoryHeader']['id']);
        }

        //棚卸のIDがあった場合
        if( isset($this->request->data['TrnInventory']['id']) ){
            $where[] = array("tin.id" => $this->request->data["TrnInventory"]["id"]);
        }
        $data = $this->searchInventory( $where );
        $output = array();
        if(count($data) > 0 ){
            //帳票データ
            $output = array();
            $i = 1;
            foreach ($data as $row ) {

                $validated_date =  ($row['TrnSticker']['validated_date']=="")?"":date('Y/m/d', strtotime($row['TrnSticker']['validated_date']));

                $output[] = array(
                    $row['tih']['title'],                                                   //範囲
                    $i,                                                                     //行番号
                    $row['mfi']['item_name'],                                               //商品名
                    $row['mfi']['standard'],                                                //規格
                    $row['mfi']['item_code'],                                               //製品番号
                    $row['mdr']['dealer_name'],                                             //販売元
                    $row['TrnSticker']['old_facility_name']."[".$row['TrnSticker']['old_facility_code']."]",      //棚卸時の施設
                    $row['TrnSticker']['facility_sticker_no'],                              //センターシール
                    $row['tin']['real_quantity'],                                           //数量
                    $row['mun']['unit_name'],                                               //包装単位
                    $row['TrnSticker']['lot_no'],                                           //ロット番号
                    $validated_date,                                                        //有効期限
                    $row['TrnSticker']['old_shelf_name'],                                   //棚卸時の棚番
                    $row['mfi']['internal_code'],                                           //商品ID
                    $row['mfc']['fixed_count'],                                             //定数
                    $row[0]['date_part'],                                                   //不動日数
                    $row['TrnSticker']['last_move_date'],                                   //最終移動日
                    $row['TrnSticker']['hospital_sticker_no'],                              //部署シール
                    $row['TrnSticker']['old_department_name']."[".$row['TrnSticker']['old_department_code']."]",  //棚卸時の部署
                    date("Y/m/d")                                                           //印刷年月日
                    );
                $i++;
            }
        }
        $columns = array("範囲",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "施設",
                         "センターシール",
                         "数量",
                         "包装単位",
                         "ロット番号",
                         "有効期限",
                         "棚番",
                         "商品ID",
                         "定数",
                         "不動日数",
                         "最終移動日",
                         "部署シール",
                         "部署",
                         "印刷年月日"
                         );
        $layout_name = Configure::read('layoutnameModels');
        $layout_file = "/inventories/xml_output/";
        $search_facter = "XXX";
        $fix_value = array('タイトル'=>'棚卸表','検索条件'=>$search_facter);
        $this->xml_output($output,join("\t",$columns),$layout_name[14],$fix_value,$layout_file);
    }



    /**
     * @param
     * @return
     */
    function inventory_diff_report() {
        //棚卸差異一覧

        $where[] = array("tih.is_deleted" => false ,
                         "tih.difference_count > " => 0 ,
                         "NOT" => array("tih.work_date" => NULL)
                         );

        //棚卸情報のIDがあった場合
        if( isset($this->request->data['TrnInventoryHeader']['id']) ){
            if(is_array($this->request->data['TrnInventoryHeader']['id'])){
                foreach($this->request->data['TrnInventoryHeader']['id'] as $i){
                    $TrnInventoryHeaderid[] = $i;
                }
            }else{
                $TrnInventoryHeaderid = $this->request->data['TrnInventoryHeader']['id'];
            }
            $where[] = array("tih.id" => $TrnInventoryHeaderid);
        }

        //棚卸のIDがあった場合
        if( isset($this->request->data['TrnInventory']['id']) ){
            $where[] = array("tin.id" => $this->request->data["TrnInventory"]["id"]);
        }

        $where[] = array("TrnSticker.diff_quantity != " => 0 ,
                         "NOT" => array("tin.real_quantity " => NULL)
                         );

        $data = $this->searchInventory( $where );

        $output = array();
        if(count($data) > 0 ){
            //帳票データ
            $output = array();
            $i = 1;
            foreach ($data as $row ) {
                $department_name = ($row['TrnSticker']['old_department_name'] == $row['mde']['department_name'])?"": $row['mde']['department_name']."[".$row['mde']['department_code']."]";
                $facility_name   = ($row['TrnSticker']['old_facility_name'] == $row['mfa']['facility_name'])?"":$row['mfa']['facility_name']."[".$row['mfa']['facility_code']."]";
                $shelf_name      = ($row['TrnSticker']['shelf_name'] == $row['TrnSticker']['old_shelf_name'])?"":$row['TrnSticker']['shelf_name'];

                $validated_date  = isset($row['TrnSticker']['validated_date'])? date('Y/m/d', strtotime($row['TrnSticker']['validated_date'])):"";

                $output[] = array(
                    $i,                                                                                          //行番号
                    $row['mfi']['item_name'],                                                                    //商品名
                    $row['mfi']['standard'],                                                                     //規格
                    $row['mfi']['item_code'],                                                                    //製品番号
                    $row['mdr']['dealer_name'],                                                                  //販売元
                    $row['TrnSticker']['facility_sticker_no'],                                                   //センターシール
                    $row['TrnSticker']['lot_no'],                                                                //ロット番号
                    $row['tin']['recital'],                                                                      //備考
                    $row['TrnSticker']['diff_quantity'],                                                         //数量
                    $row['mun']['unit_name'],                                                                    //包装単位
                    $row['TrnSticker']['status'],                                                                //状態
                    $row['mfi']['internal_code'],                                                                //商品ID1
                    $shelf_name,                                                                                 //最新棚番号
                    $row['TrnSticker']['price'],                                                                 //調整金額
                    $row['TrnSticker']['hospital_sticker_no'],                                                   //部署シール
                    $validated_date,                                                                             //有効期限
                    date('Y/m/d', strtotime($row['tih']['work_date'])),                                          //実施日
                    date('Y/m/d', strtotime($row['tin']['modified'])),                                           //最終更新日
                    $row['TrnSticker']['old_shelf_name'],                                                        //棚番号
                    $row['TrnSticker']['old_facility_name']."[".$row['TrnSticker']['old_facility_code']."]",     //実施施設
                    $facility_name,                                                                              //最新施設
                    $row['TrnSticker']['old_department_name']."[".$row['TrnSticker']['old_department_code']."]", //実施部署
                    $department_name,                                                                            //最新部署
                    date('Y/m/d')                                                                                //印刷年月日
                    );
                $i++;

            }
        }
        $columns = array("行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "センターシール",
                         "ロット番号",
                         "備考",
                         "数量",
                         "包装単位",
                         "状態",
                         "商品ID",
                         "最新棚番号",
                         "調整金額",
                         "部署シール",
                         "有効期限",
                         "実施日",
                         "最終更新日",
                         "棚番号",
                         "実施施設",
                         "最新施設",
                         "実施部署",
                         "最新部署",
                         "印刷年月日"
                         );
        $layout_name = Configure::read('layoutnameModels');
        $layout_file = "/inventories/xml_output/";
        $search_facter = "XXX";
        $fix_value = array('タイトル'=>'棚卸差異リスト','検索条件'=>$search_facter);
        $this->xml_output($output,join("\t",$columns),$layout_name[15],$fix_value,$layout_file);
    }


    /**
     * xml出力テスト用
     * @param
     * @todo 各コントローラの共通関数化
     */
    function xml_output($data,$columns,$layout_name,$fix_value,$layout_file){
        $this->layout = 'xml/default';
        $this->set('columns',$columns);
        $this->set('posted',$data);
        $this->set('layout_name',$layout_name);
        $this->set('fix_value',$fix_value); //連想配列のキーにname属性を指定
        $this->render($layout_file);
    }

    /* 帳票出力 */
    function report($mode){
        switch($mode){
          case 'diff':
            //棚卸差異一覧帳票
            $this->inventory_diff_report();
            break;
          case 'report':
            //棚卸一覧帳票
            $this->inventory_report();
            break;
        }
    }


    /**
     * 作業区分リスト取得
     */
    function getClassesList(){
        $list = Set::Combine(
            $this->MstClass->find('all',
                                  array('order' => 'MstClass.code',
                                        'conditions'=>array('MstClass.mst_menu_id' => '15'
                                                            ,"MstClass.mst_facility_id"=>$this->Session->read('Auth.facility_id_selected'))
                                        )),'{n}.MstClass.id','{n}.MstClass.name');
        return $list;
    }

    function updateCheck($id , $time){
        return $this->searchInventoryHeader(array("TrnInventoryHeader.id = " => $id ,
                                                  "TrnInventoryHeader.modified <= " => $time)
                                            , null , null , 'first');
    }

    /*
     *  棚卸CSV出力
     */
    public function export_csv() {
        App::import("sanitize");

        $sql  = ' select ';
        $sql .= '       e.internal_code                    as 商品id ';
        $sql .= '     , e.item_name                        as 商品名 ';
        $sql .= '     , e.item_code                        as 製品番号 ';
        $sql .= '     , e.standard                         as 規格 ';
        $sql .= '     , f.dealer_name                      as 販売元 ';
        $sql .= '     , c.facility_sticker_no              as センターシール ';
        $sql .= '     , c.hospital_sticker_no              as 部署シール ';
        $sql .= '     , c.lot_no                           as ロット番号 ';
        $sql .= "     , to_char(c.validated_date,'YYYY/mm/dd') as 有効期限 ";
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when d.per_unit = 1  ';
        $sql .= '         then g.unit_name  ';
        $sql .= "         else g.unit_name || '(' || d.per_unit || h.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                    as 包装単位 ';
        $sql .= "     , to_char(a.created, 'YYYY/mm/dd hh24:mi:ss') as 作成日時 ";
        $sql .= '     , i.facility_name                    as 棚卸施設 ';
        $sql .= '     , j.department_name                  as 棚卸部署 ';
        $sql .= '     , k.name                             as 棚番号 ';
        $sql .= '     , b.quantity                         as 論理数 ';
        $sql .= "     , to_char(a.work_date, 'YYYY/mm/dd') as 実施日 ";
        $sql .= '     , m.facility_name                    as 最新施設 ';
        $sql .= '     , l.department_name                  as 最新部署 ';
        $sql .= '     , o.name                             as 最新棚番号 ';
        $sql .= '     , b.real_quantity                    as 実施数 ';
        $sql .= '     , (b.real_quantity - b.quantity)     as 差分 ';
        $sql .= "     , to_char(c.last_move_date, 'YYYY/mm/dd') as 最終移動日 ";
        $sql .= '     , now()::date - coalesce(c.last_move_date,q.work_date,c.created)::date as 不動日数 ';
        $sql .= "     , to_char(b.modified, 'YYYY/mm/dd hh24:mi:ss') as 最終更新日時 ";
        $sql .= '     , p.sales_price                      as 売上単価 ';
        $sql .= '     , b.recital                          as 備考  ';
        $sql .= '   from ';
        $sql .= '     trn_inventory_headers as a  ';
        $sql .= '     left join trn_inventories as b  ';
        $sql .= '       on a.id = b.trn_inventory_header_id  ';
        $sql .= '     left join trn_stickers as c  ';
        $sql .= '       on c.id = b.trn_sticker_id  ';
        $sql .= '     left join mst_item_units as d  ';
        $sql .= '       on c.mst_item_unit_id = d.id  ';
        $sql .= '     left join mst_facility_items as e  ';
        $sql .= '       on e.id = d.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as f  ';
        $sql .= '       on f.id = e.mst_dealer_id  ';
        $sql .= '     left join mst_unit_names as g  ';
        $sql .= '       on g.id = d.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as h  ';
        $sql .= '       on h.id = d.per_unit_name_id  ';
        $sql .= '     left join mst_facilities as i  ';
        $sql .= '       on i.id = b.mst_facility_id  ';
        $sql .= '     left join mst_departments as j  ';
        $sql .= '       on j.id = b.mst_department_id  ';
        $sql .= '     left join mst_shelf_names as k  ';
        $sql .= '       on k.id = b.mst_shelf_name_id  ';
        $sql .= '     left join mst_departments as l  ';
        $sql .= '       on l.id = c.mst_department_id  ';
        $sql .= '     left join mst_facilities as m  ';
        $sql .= '       on m.id = l.mst_facility_id  ';
        $sql .= '     left join mst_fixed_counts as n  ';
        $sql .= '       on n.mst_item_unit_id = c.mst_item_unit_id  ';
        $sql .= '       and n.mst_department_id = c.mst_department_id  ';
        $sql .= '     left join mst_shelf_names as o  ';
        $sql .= '       on o.id = n.mst_shelf_name_id  ';
        $sql .= '     left join mst_sales_configs as p  ';
        $sql .= '       on p.mst_item_unit_id = d.id  ';
        $sql .= '       and p.mst_facility_id =  ' . $this->request->data['TrnInventory']['mst_facility_id'];
        $sql .= '       and p.partner_facility_id = b.mst_facility_id  ';
        $sql .= '       and p.start_date <= coalesce(a.work_date,now())  ';
        $sql .= '       and p.end_date >= coalesce(a.work_date,now())  ';
        $sql .= '     left join trn_receivings as q ';
        $sql .= '       on q.id = c.trn_receiving_id ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->request->data['TrnInventoryHeader']['id'];
        $sql .= '   order by e.item_name,e.internal_code,j.department_name,k.name,c.facility_sticker_no';

        $this->db_export_csv($sql , "棚卸一覧", 'execution_add');

    }

    function kawatetu($barcode){

        $sql  = ' select ';
        $sql .= '       d.id  ';
        $sql .= '     , a.mst_department_id  ';
        $sql .= '     , e.mst_facility_id  ';
        $sql .= '     , f.mst_shelf_name_id  ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     inner join trn_inventories as d  ';
        $sql .= '       on d.trn_sticker_id = a.id  ';
        $sql .= '     inner join mst_departments as e  ';
        $sql .= '       on a.mst_department_id = e.id  ';
        $sql .= '     inner join mst_fixed_counts as f  ';
        $sql .= '       on a.mst_department_id = f.mst_department_id  ';
        $sql .= '      and b.id = f.mst_item_unit_id  ';
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .= '     and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');

        if($barcode['quantity'] == 1 || $barcode['quantity'] == null ){
            $sql .= '     and b.per_unit = 1  ';
        }else{
            $sql .= '     and b.per_unit = ' . $barcode['quantity'];
        }

        if($barcode['lot_no'] != ''){
            $sql .= "     and a.lot_no = '" .$barcode['lot_no']. "'";
        }else{
            $sql .= "     and ( a.lot_no = '' or a.lot_no is null ) ";
        }

        if($barcode['validated_date'] != '' ){
            $sql .= "     and a.validated_date = '" . $this->getFormatedValidateDate($barcode['validated_date']) . "'";
        }else{
            $sql .= "     and a.validated_date is null ";
        }

        $sql .= "     and c.jan_code like '" . $barcode['pure_jan_code'] ."%'";
        $sql .= "     and a.facility_sticker_no = ''  ";
        $sql .= "     and a.hospital_sticker_no = ''  ";

        $sql .= '     and d.trn_inventory_header_id = ' . $this->request->data['TrnInventoryHeader']['id'];

        $ret = $this->TrnSticker->query($sql);

        return (isset($ret[0][0]['id'])?$ret[0][0]:false);
    }


    public function getShelfList( $where , $where2 , $limit=null ){
        $sql  = ' select ';
        $sql .= '     id              as "inventory__id"';
        $sql .= '   , facility_name   as "inventory__facility_name"';
        $sql .= '   , department_name as "inventory__department_name"';
        $sql .= '   , facility_code   as "inventory__facility_code"';
        $sql .= '   , department_code as "inventory__department_code"';
        $sql .= '   , name            as "inventory__name"';
        $sql .= ' from ';
        $sql .= '   (  ';
        $sql .= '     select ';
        $sql .= '         d.id ';
        $sql .= '       , b.facility_name ';
        $sql .= '       , c.department_name ';
        $sql .= '       , b.facility_code ';
        $sql .= '       , c.department_code ';
        $sql .= '       , d.name  ';
        $sql .= '     from ';
        $sql .= '       mst_facility_relations as a  ';
        $sql .= '       left join mst_facilities as b  ';
        $sql .= '         on b.id = a.mst_facility_id  ';
        $sql .= '         or b.id = a.partner_facility_id  ';
        $sql .= '       left join mst_departments as c  ';
        $sql .= '         on c.mst_facility_id = b.id  ';
        $sql .= '       inner join mst_shelf_names as d  ';
        $sql .= '         on d.mst_department_id = c.id  ';
        $sql .= '     where ';
        $sql .= '       (  ';
        $sql .= '         a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '         or a.partner_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '       )  ';
        $sql .= '       and b.facility_type in (' . Configure::read('FacilityType.center') . ' , ' . Configure::read('FacilityType.hospital')  . ')  ';
        $sql .= $where;
        $sql .= '     group by ';
        $sql .= '       d.id ';
        $sql .= '       , b.facility_name ';
        $sql .= '       , c.department_name ';
        $sql .= '       , b.facility_code ';
        $sql .= '       , c.department_code ';
        $sql .= '       , d.name  ';
        $sql .= '     union all  ';
        $sql .= '     select ';
        $sql .= '         c.id * - 1               as id ';
        $sql .= '       , b.facility_name ';
        $sql .= '       , c.department_name ';
        $sql .= '       , b.facility_code ';
        $sql .= '       , c.department_code ';
        $sql .= "       , '-'                      as name  ";
        $sql .= '     from ';
        $sql .= '       mst_facility_relations as a  ';
        $sql .= '       left join mst_facilities as b  ';
        $sql .= '         on b.id = a.mst_facility_id  ';
        $sql .= '         or b.id = a.partner_facility_id  ';
        $sql .= '       left join mst_departments as c  ';
        $sql .= '         on c.mst_facility_id = b.id  ';
        $sql .= '     where ';
        $sql .= '       (  ';
        $sql .= '         a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '         or a.partner_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '       )  ';
        $sql .= '       and b.facility_type in (' . Configure::read('FacilityType.center') . ' , ' . Configure::read('FacilityType.hospital')  . ')  ';
        $sql .= $where2;
        $sql .= '     group by ';
        $sql .= '       b.facility_name ';
        $sql .= '       , c.id ';
        $sql .= '       , c.department_name ';
        $sql .= '       , b.facility_code ';
        $sql .= '       , c.department_code ';
        $sql .= '   ) as x  ';
        $sql .= ' order by ';
        $sql .= '   facility_code ';
        $sql .= '   , department_code ';
        $sql .= '   , name ';
        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'MstShelfName' , false ));
            $sql .= " limit " . $limit;
        }
        $result = $this->MstShelfName->query($sql);
        return $result;
    }

    public function getShelfList2( $data ){
        $where = $where2 = "";
        $department = $shelf = array();
        foreach($data['inventory']['id'] as $id){
            if($id < 0){
                //部署ID
                array_push($department, ($id*-1));
            }else{
                //棚ID
                array_push($shelf, $id);
            }
        }
        if(count($shelf) > 0){
            $where = " and d.id in ( " . join(',',$shelf). ")";
        } else {
            $where = " and 1=0";
        }
        
        
        if(count($department) > 0){
            $where2 = " and c.id in ( " . join(',',$department). ")";
        } else {
            $where2 = " and 1=0";
        }
        
        //棚卸検索実行
        $SearchResult = $this->getShelfList($where , $where2);

        return $SearchResult;
    }
    
    public function getItemList($where , $limit=null){
        $sql  = ' select ';
        $sql .= '     c.id             as "inventory__id"';
        $sql .= '   , a.internal_code  as "inventory__internal_code"';
        $sql .= '   , a.item_name      as "inventory__item_name"';
        $sql .= '   , a.standard       as "inventory__standard"';
        $sql .= '   , a.item_code      as "inventory__item_code"';
        $sql .= '   , b.dealer_name    as "inventory__dealer_name"';
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when c.per_unit = 1  ';
        $sql .= '       then c1.unit_name  ';
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '       end ';
        $sql .= '   )                  as "inventory__unit_name"';
        $sql .= ' from ';
        $sql .= '   mst_facility_items as a  ';
        $sql .= '   left join mst_dealers as b  ';
        $sql .= '     on b.id = a.mst_dealer_id  ';
        $sql .= '   left join mst_item_units as c  ';
        $sql .= '     on c.mst_facility_item_id = a.id  ';
        $sql .= '   left join mst_unit_names as c1  ';
        $sql .= '     on c1.id = c.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as c2  ';
        $sql .= '     on c2.id = c.per_unit_name_id  ';
        $sql .= ' where ';
        $sql .= '   a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        
        $sql .= $where;
        
        $sql .= ' order by ';
        $sql .= '   a.internal_code ';
        $sql .= '   , c.per_unit ';
        
        if(!is_null($limit)){
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));
            $sql .= " limit " . $limit;
        }
        $result = $this->MstFacilityItem->query($sql);
        return $result;
    }

    function getInventoryList($where, $limit=null){
        
        $sql  = ' select';
        $sql .= '       a.id               as "TrnInventoryHeader__id"';
        $sql .= '     , a.work_no          as "TrnInventoryHeader__work_no"';
        $sql .= "     , to_char(a.work_date, 'YYYY/mm/dd') ";
        $sql .= '                          as "TrnInventoryHeader__work_date"';
        $sql .= "     , to_char(a.created, 'YYYY/mm/dd HH24:mi:ss') ";
        $sql .= '                          as "TrnInventoryHeader__created"';
        $sql .= '     , a.title            as "TrnInventoryHeader__title"';
        $sql .= '    , ( case a.inventory_type  ';
        foreach(Configure::read('InventoryTypes') as $key => $val){
            $sql .= "           when " . $key . " then '" . $val ."' ";
        }
        
        $sql .= '     end)                 as "TrnInventoryHeader__inventory_type" ';
        $sql .= '     , a.difference_count as "TrnInventoryHeader__difference_count"';
        $sql .= '   from';
        $sql .= '     trn_inventory_headers as a ';
        $sql .= '   where';
        $sql .= '     a.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and a.is_deleted = false ';
        $sql .= $where;
        $sql .= '   order by';
        $sql .= '     a.id ';
        if(!is_null($limit)){
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnSticker'));
            
            /* LIMIT */
            $sql .= ' limit ' . $this->request->data['Inventory']['limit'];
        }
        //検索実行
        $SearchResult = $this->TrnInventoryHeader->query($sql);
        return $SearchResult;
    }
    
    function getInventoryHeader($id){
        $sql  = ' select ';
        $sql .= '     a.id                  as "TrnInventoryHeader__id" ';
        $sql .= '   , a.work_no             as "TrnInventoryHeader__work_no" ';
        $sql .= '   , a.title               as "TrnInventoryHeader__title" ';
        $sql .= '   , ( case a.inventory_type  ';
        foreach(Configure::read('InventoryTypes') as $key => $val){
            $sql .= "           when " . $key . " then '" . $val ."' ";
        }
        $sql .= '      end)                 as "TrnInventoryHeader__inventory_type" ';
        $sql .= ' from ';
        $sql .= '   trn_inventory_headers as a  ';
        $sql .= ' where ';
        $sql .= '   a.id = ' . $id;
        
        $result = $this->TrnInventoryHeader->query($sql);
        
        return $result;
    }
    
}
?>