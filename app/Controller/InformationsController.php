<?php
/**
 * TrnInformationsController
 *
 * @version 1.0.0
 * @since 2010/07/20
 */

class InformationsController extends AppController {
    var $name = 'Informations';
    
    /**
     *
     * @var array $uses
     */
    var $uses = array('TrnInformation','MstDepartment','MstFacility');
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;
    
    /**
     * @var Departments
     */
    var $Departments;
    
    function index(){
        $this->redirect('informations_list');
    }


    /**
     * お知らせ一覧
     */
    function informations_list($id = null) {
        $this->setRoleFunction(95); //お知らせ
        if(!is_null($id)){
            //削除処理
            $now = date('Y/m/d H:i:s.u');
            $info_data = array();
            
            //トランザクションを開始
            $this->TrnInformation->begin();
            //TrnInformationオブジェクトをcreate
            $this->TrnInformation->create();
            
            $info_data['TrnInformation']['id']         = $id;
            $info_data['TrnInformation']['is_deleted'] = true;
            $info_data['TrnInformation']['modifier']   = $this->Session->read('Auth.MstUser.id');
            $info_data['TrnInformation']['modified']   = $now;
            
            //SQL実行
            if (!$this->TrnInformation->save($info_data)) {
                //ロールバック
                $this->TrnInformation->rollback();
                //エラーメッセージ設定
                $this->Session->setFlash('削除に失敗しました。もう一度やり直してください。', 'growl', array('type'=>'error') );
            } else {
                //コミット
                $this->TrnInformation->commit();
            }
        }
        
        //お知らせ情報の取得
        $sql  = ' select ';
        $sql .= '       a.id                                as "TrnInformation__id"';
        $sql .= '     , to_char(a.start_date, \'YYYY/mm/dd\') as "TrnInformation__start_date"';
        $sql .= '     , to_char(a.end_date, \'YYYY/mm/dd\')   as "TrnInformation__end_date"';
        $sql .= '     , coalesce(b.facility_name, \'すべて\') as "TrnInformation__facility_name"';
        $sql .= '     , a.title                             as "TrnInformation__title"';
        $sql .= '     , a.message                           as "TrnInformation__message"';
        $sql .= '     , a.is_deleted                        as "TrnInformation__is_deleted"';
        $sql .= '   from ';
        $sql .= '     trn_informations as a  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.mst_facility_id ';
        $sql .= '   where a.is_deleted = false  ';
        $sql .= '     and a.trn_rfp_header_id is null ';
        $sql .= '   order by a.start_date desc , coalesce(b.id,0) asc , a.id ';
        
        //SQL実行
        $this->set('Information_List', $this->TrnInformation->query($sql) );
    }

    
    /**
     * Add ユーザ新規登録
     */
    function add() {
        //センター一覧を取得
        $this->set('CenterList' , $this->getFacilityList(null , array(1) , false , array('id' , 'facility_name')));
    }
    
    /**
     * Mod ユーザ情報編集
     */
    function mod($id = null) {
        //センター一覧を取得
        $this->set('CenterList' , $this->getFacilityList(null , array(1) , false , array('id' , 'facility_name')));

        //listから引き継いできたidでお知らせ情報を取得
        $sql  = ' select ';
        $sql .= '       a.id              as "TrnInformation__id"';
        $sql .= '     , a.mst_facility_id as "TrnInformation__mst_facility_id"';
        $sql .= '     , to_char(a.start_date , \'YYYY/mm/dd\')      as "TrnInformation__start_date"';
        $sql .= '     , to_char(a.end_date ,\'YYYY/mm/dd\')       as "TrnInformation__end_date"';
        $sql .= '     , a.title           as "TrnInformation__title"';
        $sql .= '     , a.message         as "TrnInformation__message"';
        $sql .= '   from ';
        $sql .= '     trn_informations as a  ';
        $sql .= '   where ';
        $sql .= '     id = ' . $id;
        $tmp = $this->TrnInformation->query($sql);
        $this->request->data = $tmp[0];
    }
    
    /**
     * result
     *
     * 施設情報更新（新規登録・更新）
     */
    function result() {
        $now = date('Y/m/d H:i:s.u');
        //トランザクションを開始
        $this->TrnInformation->begin();
        //行ロック（更新時のみ）
        if(isset( $this->request->data['TrnInformation']['id']) && !empty( $this->request->data['TrnInformation']['id'])){
            $this->TrnInformation->query( ' select * from trn_informations as a where a.id = ' . $this->request->data['TrnInformation']['id'] . ' for update ');
        }
        
        $info_data = array();
        if(isset($this->request->data['TrnInformation']['id'])){
            //更新
            $info_data['TrnInformation']['id'] = $this->request->data['TrnInformation']['id'];
        }else{
            //新規登録
            $info_data['TrnInformation']['creater'] = $this->Session->read('Auth.MstUser.id');
            $info_data['TrnInformation']['created'] = $now;
        }
        $info_data['TrnInformation']['mst_facility_id'] = $this->request->data['TrnInformation']['mst_facility_id'];
        $info_data['TrnInformation']['title']           = $this->request->data['TrnInformation']['title'];
        $info_data['TrnInformation']['message']         = $this->request->data['TrnInformation']['message'];
        $info_data['TrnInformation']['start_date']      = $this->request->data['TrnInformation']['start_date'];
        $info_data['TrnInformation']['end_date']        = $this->request->data['TrnInformation']['end_date'];
        $info_data['TrnInformation']['modifier']        = $this->Session->read('Auth.MstUser.id');
        $info_data['TrnInformation']['modified']        = $now;
        
        if(!$this->TrnInformation->save($info_data)){

            //ロールバック
            $this->TrnInformation->rollback();
            //エラーメッセージ
            $this->Session->setFlash('お知らせ情報の登録に失敗しました。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('informations_list');
        }
        //コミット
        $this->TrnInformation->commit();
        
        //センター一覧を取得
        $this->set('CenterList' , $this->getFacilityList(null , array(1) , false , array('id' , 'facility_name')));
    }
}
