<?php
/**
 * MstFacilitiesController
 * @version 1.0.0
 * @since 2010/07/08
 */

class MstBaseFacilitiesController extends AppController {
    var $name = 'MstBaseFacilities';

    /**
     *
     * @var array $uses
     */
    var $uses = array('MstFacility',
                      'MstFacilityOption',
                      'MstDepartment',
                      'MstFacilityRelation');


    /**
     * @var array $components
     */
    var $components = array('CsvWriteUtils');


    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;

    /**
     * @var MstFacilities
     */
    var $MstFacilities;

    public function beforeFilter(){
        parent::beforeFilter();
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    /**
     * facilities_list
     *
     * 施設一覧
     */
    //引数mode
    //センター:mode0
    //施主・得意先・仕入先:mode1
    function facilities_list($mode = null) {
        App::import('Sanitize');

        $Facilities_List = array();
        $facility_type =array();

        if(!isset($this->request->data['MstFacility']['type'])){
            $this->request->data['MstFacility']['type'] = $mode;
        }

        if($this->request->data['MstFacility']['type'] == 'mode0'){
            $this->setRoleFunction(78); //センター
            $this->request->data['MstFacility']['title']  = "センター";
        }elseif($this->request->data['MstFacility']['type'] == 'mode1'){
            $this->setRoleFunction(79); //施主・病院・仕入先
            $this->request->data['MstFacility']['title']  = "施主・得意先・仕入先";
            $unsetType = array(Configure::read('FacilityType.center'), Configure::read('FacilityType.outside'));
            //施設区分を取得
            $facility_type = Configure::read('FacilityType.typelist');
            $facility_type = $this->array_unset_keys($facility_type , $unsetType);
        }

        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }

        //初期表示以外の場合のみデータを取得する
        if(isset($this->request->data['MstFacility']['is_search'])){
            //センターと仕入先・施主・得意先で条件の追加
            if($this->request->data['MstFacility']['type'] == "mode0"){
                $where = 'MstFacility.facility_type = ' . Configure::read('FacilityType.center');
            } elseif($this->request->data['MstFacility']['type'] == "mode1"){
                $where = 'MstFacility.facility_type <> ' .  Configure::read('FacilityType.center');
            }
            //管理外施設は表示しない
            $where .= ' and MstFacility.facility_type <> ' .  Configure::read('FacilityType.outside');

            //ユーザ入力値による検索条件の作成--------------------------------------------

            //施設コード(LIKE検索)
            if(($this->request->data['MstFacility']['search_facility_code'] != "")){
                $where .= " and MstFacility.facility_code LIKE '%".Sanitize::escape($this->request->data['MstFacility']['search_facility_code'])."%'";
            }
            //施設名称(LIKE検索)
            if(($this->request->data['MstFacility']['search_facility_name'] != "")){
                $where .= " and MstFacility.facility_name LIKE '%".Sanitize::escape($this->request->data['MstFacility']['search_facility_name'])."%'";
            }

            //施設区分(完全一致)
            if((isset($this->request->data['MstFacility']['search_facility_type'])) && ($this->request->data['MstFacility']['search_facility_type'] != "")){
                $where .= ' and MstFacility.facility_type = ' . Sanitize::escape($this->request->data['MstFacility']['search_facility_type']) ;
            }
            //削除済み表示
            if(!isset($this->request->data['MstFacility']['search_is_deleted'])){
                $where .= ' and MstFacility.is_deleted = false ';
            }

            //関連施設のみ表示
            if(isset($this->request->data['MstFacility']['search_relation'])){
                $where .= ' and MstFacilityRelation.id is not null ';
            }

            //検索条件の作成終了---------------------------------------------------------
            $order = 'MstFacility.id ASC';

            $sql  = ' select ';
            $sql .= '       MstFacility.id            as "MstFacility__id" ';
            $sql .= '     , MstFacility.facility_code as "MstFacility__facility_code" ';
            $sql .= '     , MstFacility.facility_name as "MstFacility__facility_name" ';
            $sql .= '     , (  ';
            $sql .= '       case MstFacility.facility_type  ';
            foreach( Configure::read('FacilityType.typelist') as $key => $value ){
                $sql .= ' when ' . $key . " then '" . $value . "'";
              }

            $sql .= '         end ';
            $sql .= '     )                 as "MstFacility__facility_type" ';

            $sql .= '     , MstFacility.is_deleted    as "MstFacility__is_deleted"  ';
            $sql .= '   from ';
            $sql .= '     mst_facilities as MstFacility  ';
            $sql .= '   left join ';
            $sql .= '     mst_facility_relations as MstFacilityRelation ';
            $sql .= '     on MstFacilityRelation.mst_facility_id =  ' . $this->Session->read('Auth.facility_id_selected') ;
            $sql .= '     and MstFacilityRelation.partner_facility_id = MstFacility.id' ;
            $sql .= '     and MstFacilityRelation.is_deleted = false ';
            $sql .= '   where ' .  $where;
            $sql .= '   order by ' . $order;

            //全件取得
            $this->set('max' , $this->getMaxCount($sql ,'MstFacility'));

            $sql .= '   limit ' . $this->_getLimitCount();

            //SQL実行
            $Facilities_List   = $this->MstFacility->query($sql);

        }
        $this->set('Facilities_List',$Facilities_List);
        $this->set('facility_type' , $facility_type);
    }


    /**
     * Add 施設新規登録
     */
    function add() {
        if($this->request->data['MstFacility']['type'] == "mode0"){
            $this->setRoleFunction(78);
            //センターの場合紐付けのため施設一覧を取得
            $this->set('Facility_List' , $this->getFacilityRelationList());
        } elseif($this->request->data['MstFacility']['type'] == "mode1") {
            $this->setRoleFunction(79);
            //施設区分を取得
            $unsetType = array(Configure::read('FacilityType.center'), Configure::read('FacilityType.outside') );
            //施設区分を取得
            $facility_type = Configure::read('FacilityType.typelist');
            $facility_type = $this->array_unset_keys($facility_type , $unsetType);

            $this->set('facility_type' , $facility_type);
        }

        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }

    /**
     * Mod 施設情報編集
     */
    function mod() {
        $type  = $this->request->data['MstFacility']['type'];
        $title = $this->request->data['MstFacility']['title'];
        //更新チェック用にmod画面に入った瞬間の時間を保持
        $this->Session->write('Facility.readTime',date('Y-m-d H:i:s'));

        if($this->request->data['MstFacility']['type'] == "mode0"){
            $this->setRoleFunction(78);
            //自身以外の施設一覧を取得（全ての区分）
            $this->set('Facility_List',$this->getFacilityRelationList($this->request->data['MstFacility']['id']));
        }else{
            $this->setRoleFunction(79);
        }

        $sql  = ' select ';
        $sql .= '       a.id                     as "MstFacility__id" ';
        $sql .= '     , a.facility_code          as "MstFacility__facility_code" ';
        $sql .= '     , a.facility_type          as "MstFacility__facility_type" ';
        $sql .= '     , a.facility_name          as "MstFacility__facility_name" ';
        $sql .= '     , a.facility_formal_name   as "MstFacility__facility_formal_name" ';
        $sql .= '     , a.zip                    as "MstFacility__zip" ';
        $sql .= '     , a.address                as "MstFacility__address" ';
        $sql .= '     , a.tel                    as "MstFacility__tel" ';
        $sql .= '     , a.fax                    as "MstFacility__fax" ';
        $sql .= '     , a.subcode                as "MstFacility__subcode" ';
        $sql .= '     , a.days_before_expiration as "MstFacility__days_before_expiration" ';
        $sql .= '     , a.round                  as "MstFacility__round" ';
        $sql .= '     , a.gross                  as "MstFacility__gross" ';
        $sql .= '     , a.is_deleted             as "MstFacility__is_deleted" ';
        $sql .= '     , a.email                  as "MstFacility__email" ';
        $sql .= '     , b.export_csv             as "MstFacility__ExportCsv" ';
        $sql .= '     , b.import_csv             as "MstFacility__ImportCsv" ';
        $sql .= '     , b.shipping               as "MstFacility__Shipping" ';
        $sql .= '     , b.d2consumes             as "MstFacility__D2consumes" ';
        $sql .= '     , b.shipping_consumes      as "MstFacility__ShippingConsumes" ';
        $sql .= '     , b.ex_shipping_consumes   as "MstFacility__ExShippingConsumes" ';
        $sql .= '     , b.ms_corporate           as "MstFacility__MScorporate" ';
        $sql .= '     , b.order_mail             as "MstFacility__OrderMail"  ';
        $sql .= '     , b.catalog_order          as "MstFacility__CatalogOrder"  ';
        $sql .= '     , b.catalog_price          as "MstFacility__CatalogPrice"  ';
        $sql .= '     , b.temporary_claim_type   as "MstFacility__TemporaryClaimType"  ';
        $sql .= '     , b.stock_type             as "MstFacility__StockType"';
        $sql .= '     , b.code_length            as "MstFacility__CodeLength"';
        $sql .= '     , b.layout_type            as "MstFacility__LayoutType"';
        $sql .= '     , b.ope_set_use_type       as "MstFacility__OpeSetUseType"';
        $sql .= '     , b.lowlevel_sales_type    as "MstFacility__LowlevelSalesType"';
        $sql .= '     , b.inner_move_type_change as "MstFacility__InnerMoveTypeChange"';
        for ($i = 1; $i <= 30; $i++) { //予備キー1～30まで
        $sql .= '     , b.spare_key'.$i.'_title       as "MstFacility__SpareKey'.$i.'Title"';
        $sql .= '     , b.spare_key'.$i.'_class       as "MstFacility__SpareKey'.$i.'Class"';
        $sql .= '     , b.spare_key'.$i.'_select      as "MstFacility__SpareKey'.$i.'Select"';
        }
        $sql .= '   from ';
        $sql .= '     mst_facilities as a ';
        $sql .= '   left join mst_facility_options as b ';
        $sql .= '     on b.mst_facility_id = a.id ';
        $sql .= '   where ';
        $sql .= '     a.id = '.$this->request->data['MstFacility']['id'];
        
        $data = $this->MstFacility->query($sql);
        
        $this->request->data = $data[0];
        $this->request->data['MstFacility']['type']  = $type;
        $this->request->data['MstFacility']['title'] = $title;

        //サブコードの分割
        if($this->request->data['MstFacility']['subcode'] != ""){
            list($this->request->data['MstFacility']['subcode_1'] ,
                 $this->request->data['MstFacility']['subcode_2'] ,
                 $this->request->data['MstFacility']['subcode_3'])= explode(Configure::read('subcodeSplitter'),$this->request->data['MstFacility']['subcode']);
        }
    }


    /**
     * result
     *
     * ユーザ情報更新（新規登録・更新）
     */
    function result() {
        $now = date('Y/m/d H:i:s.u');
        //トランザクション
        $this->MstFacility->begin();

        //行ロック(更新時のみ)
        if(isset($this->request->data['MstFacility']['id'])){
            $this->MstFacility->query('select * from mst_facilities as a where a.id = ' . $this->request->data['MstFacility']['id'] . ' for update ');
        }

        $this->MstFacility->create();
        $facility_data = array();
        $facility_option = array();

        //保存データの整形
        if(isset($this->request->data['MstFacility']['id'])){
            //更新の場合
            $facility_data['MstFacility']['id']                    = $this->request->data['MstFacility']['id'];
        }else{
            //新規の場合
            $facility_data['MstFacility']['creater']               = $this->Session->read('Auth.MstUser.id');
            $facility_data['MstFacility']['created']               = $now;
        }
        $facility_data['MstFacility']['facility_name']          = $this->request->data['MstFacility']['facility_name'];
        $facility_data['MstFacility']['facility_formal_name']   = $this->request->data['MstFacility']['facility_formal_name'];
        $facility_data['MstFacility']['facility_code']          = $this->request->data['MstFacility']['facility_code'];
        $facility_data['MstFacility']['facility_type']          = $this->request->data['MstFacility']['facility_type'];
        $facility_data['MstFacility']['days_before_expiration'] = $this->request->data['MstFacility']['days_before_expiration'];

        $facility_data['MstFacility']['zip']                    = $this->request->data['MstFacility']['zip'];
        $facility_data['MstFacility']['address']                = $this->request->data['MstFacility']['address'];
        $facility_data['MstFacility']['tel']                    = $this->request->data['MstFacility']['tel'];
        $facility_data['MstFacility']['fax']                    = $this->request->data['MstFacility']['fax'];
        $facility_data['MstFacility']['email']                  = $this->request->data['MstFacility']['email'];

        $facility_data['MstFacility']['gross']                  = $this->request->data['MstFacility']['gross'];
        $facility_data['MstFacility']['round']                  = $this->request->data['MstFacility']['round'];
        $facility_data['MstFacility']['subcode']                = $this->request->data['MstFacility']['subcode_1'] .Configure::read('subcodeSplitter').$this->request->data['MstFacility']['subcode_2'] .Configure::read('subcodeSplitter'). $this->request->data['MstFacility']['subcode_3'];
        $facility_data['MstFacility']['is_deleted']             = (isset($this->request->data['MstFacility']['is_deleted'])?true:false);
        $facility_data['MstFacility']['modifier']               = $this->Session->read('Auth.MstUser.id');
        $facility_data['MstFacility']['modified']               = $now;
        
        // センターの場合
        if($this->request->data['MstFacility']['facility_type'] == Configure::read('FacilityType.center') ) {
            $facility_option['MstFacilityOption']['export_csv']             = $this->request->data['MstFacility']['ExportCsv'];
            $facility_option['MstFacilityOption']['import_csv']             = $this->request->data['MstFacility']['ImportCsv'];
            $facility_option['MstFacilityOption']['shipping']               = $this->request->data['MstFacility']['Shipping'];
            $facility_option['MstFacilityOption']['d2consumes']             = $this->request->data['MstFacility']['D2Consumes'];
            $facility_option['MstFacilityOption']['shipping_consumes']      = $this->request->data['MstFacility']['ShippingConsumes'];
            $facility_option['MstFacilityOption']['ex_shipping_consumes']   = $this->request->data['MstFacility']['ExShippingConsumes'];
            $facility_option['MstFacilityOption']['ms_corporate']           = $this->request->data['MstFacility']['MScorporate'];
            $facility_option['MstFacilityOption']['order_mail']             = $this->request->data['MstFacility']['OrderMail'];
            $facility_option['MstFacilityOption']['catalog_order']          = $this->request->data['MstFacility']['CatalogOrder'];
            $facility_option['MstFacilityOption']['catalog_price']          = $this->request->data['MstFacility']['CatalogPrice'];
            $facility_option['MstFacilityOption']['temporary_claim_type']   = $this->request->data['MstFacility']['TemporaryClaimType'];
            $facility_option['MstFacilityOption']['stock_type']             = $this->request->data['MstFacility']['StockType'];
            $facility_option['MstFacilityOption']['code_length']            = $this->request->data['MstFacility']['CodeLength'];
            $facility_option['MstFacilityOption']['layout_type']            = $this->request->data['MstFacility']['LayoutType'];
            $facility_option['MstFacilityOption']['ope_set_use_type']       = $this->request->data['MstFacility']['OpeSetUseType'];
            $facility_option['MstFacilityOption']['inner_move_type_change'] = $this->request->data['MstFacility']['InnerMoveTypeChange'];
            $facility_option['MstFacilityOption']['lowlevel_sales_type']    = $this->request->data['MstFacility']['LowlevelSalesType'];
            
            // 予備キー1～30まで
            for($i=1;$i<=30;$i++){
                $facility_option['MstFacilityOption']["spare_key{$i}_title"]  = $this->request->data['MstFacility']["SpareKey{$i}Title"];
                $facility_option['MstFacilityOption']["spare_key{$i}_class"]  = $this->request->data['MstFacility']["SpareKey{$i}Class"];
                $facility_option['MstFacilityOption']["spare_key{$i}_select"] = $this->request->data['MstFacility']["SpareKey{$i}Select"];
            }
        }
        
        //SQL実行
        if(!$this->MstFacility->save($facility_data)){
            //ロールバック
            $this->MstFacility->rollback();
            //エラーメッセージ
            $this->Session->setFlash('施設情報の登録に失敗しました。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('facilities_list' . $this->request->data['MstFacility']['type']);
        }
        
        $mst_facility_id = $this->MstFacility->getLastInsertID();
        
        //新規かつ、センター、仕入先の場合部署を自動的に作成する。
        if(!isset($this->request->data['MstFacility']['id']) &&
           ($this->request->data['MstFacility']['facility_type'] == Configure::read('FacilityType.center') || $this->request->data['MstFacility']['facility_type'] == Configure::read('FacilityType.supplier') )
           ){
            $department_data = array();

            $department_data['MstDepartment']['mst_facility_id']        = (isset($this->request->data['MstFacility']['id'])?$this->request->data['MstFacility']['id']:$this->MstFacility->getLastInsertId());
            $department_data['MstDepartment']['department_name']        = (($this->request->data['MstFacility']['facility_type']==Configure::read('FacilityType.center'))?'ｾﾝﾀｰ倉庫':'仕入先倉庫');
            $department_data['MstDepartment']['department_formal_name'] = (($this->request->data['MstFacility']['facility_type']==Configure::read('FacilityType.center'))?'センター倉庫':'仕入先倉庫');
            $department_data['MstDepartment']['department_code']        = '00000';
            $department_data['MstDepartment']['department_type']        = (($this->request->data['MstFacility']['facility_type'] == Configure::read('FacilityType.center'))?Configure::read('DepartmentType.warehouse'):Configure::read('DepartmentType.supplier'));
            $department_data['MstDepartment']['priority']               = 0;
            $department_data['MstDepartment']['is_deleted']             = false;
            $department_data['MstDepartment']['modifier']               = $this->Session->read('Auth.MstUser.id');
            $department_data['MstDepartment']['modified']               = $now;
            $department_data['MstDepartment']['creater']                = $this->Session->read('Auth.MstUser.id');
            $department_data['MstDepartment']['created']                = $now;

            //SQL実行
            $this->MstDepartment->create();
            if(!$this->MstDepartment->save($department_data)){
                //ロールバック
                $this->MstFacility->rollback();
                //エラーメッセージ
                $this->Session->setFlash('部署情報の登録に失敗しました。', 'growl', array('type'=>'error') );
                //リダイレクト
                $this->redirect('facilities_list' . $this->request->data['MstFacility']['type']);
            }
        }

        //センターの場合、。
        if($this->request->data['MstFacility']['facility_type'] == Configure::read('FacilityType.center') ) {
            // 施設オプション
            if(isset($this->request->data['MstFacility']['id'])){
                // 更新の場合
                $facility_option['MstFacilityOption']['mst_facility_id'] = $this->request->data['MstFacility']['id'];
                $facility_option['MstFacilityOption']['id'] = $this->getOptionId($facility_option['MstFacilityOption']['mst_facility_id']);
            }else{
                // 新規の場合
                $facility_option['MstFacilityOption']['mst_facility_id'] = $mst_facility_id;
            }
            
            $this->MstFacilityOption->create();
            if(!$this->MstFacilityOption->save($facility_option)){
                //ロールバック
                $this->MstFacility->rollback();
                //エラーメッセージ
                $this->Session->setFlash('施設設定情報の登録に失敗しました。', 'growl', array('type'=>'error') );
                //リダイレクト
                $this->redirect('facilities_list' . $this->request->data['MstFacility']['type']);
            }
            
            // 施設関係
            if(isset($this->request->data['MstFacility']['id'])){
                //更新の場合関連テーブルをいったんFALSEで更新
                $this->MstFacilityRelation->query('update mst_facility_relations set is_deleted = true where mst_facility_id = ' . $this->request->data['MstFacility']['id']);
                foreach($this->request->data['MstFacility']['Relation_Facility'] as $key => $id ){
                    //センターIDと、POSTされたIDとで関連付けテーブルを検索
                    $relation_id = $this->getRelationId($this->request->data['MstFacility']['id'] , $id);
                    $this->MstFacilityRelation->create();
                    if($relation_id){
                        //該当組み合わせアリ => 更新処理
                        $this->MstFacilityRelation->query('update mst_facility_relations set is_deleted = false where id = ' . $relation_id);
                    }else{
                        //該当組み合わせナシ => 追加処理
                        $relation_data = array();

                        $relation_data['MstFacilityRelation']['mst_facility_id']     = $this->request->data['MstFacility']['id'];
                        $relation_data['MstFacilityRelation']['partner_facility_id'] = $id;
                        $relation_data['MstFacilityRelation']['is_deleted']          = false;
                        $relation_data['MstFacilityRelation']['creater']             = $this->Session->read('Auth.MstUser.id');
                        $relation_data['MstFacilityRelation']['created']             = $now;
                        $relation_data['MstFacilityRelation']['modifier']            = $this->Session->read('Auth.MstUser.id');
                        $relation_data['MstFacilityRelation']['modified']            = $now;
                        if(!$this->MstFacilityRelation->save($relation_data)){
                            //ロールバック
                            $this->MstFacility->rollback();
                            //エラーメッセージ
                            $this->Session->setFlash('施設関連情報の登録に失敗しました。', 'growl', array('type'=>'error') );
                            //リダイレクト
                            $this->redirect('facilities_list' . $this->request->data['MstFacility']['type']);
                        }
                    }
                }
            }else{
                //新規の場合まとめて追加する。
                $relation_data = array();
                foreach($this->request->data['MstFacility']['Relation_Facility'] as $key => $id ){
                    $relation_data[] = array(
                        'MstFacilityRelation' => array(
                            'mst_facility_id'     => $this->MstFacility->getLastInsertId(),
                            'partner_facility_id' => $id,
                            'is_deleted'          => false,
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now
                            )
                        );
                }
                if(!$this->MstFacilityRelation->saveAll($relation_data , array('validates' => true,'atomic' => false))){
                    //ロールバック
                    $this->MstFacility->rollback();
                    //エラーメッセージ
                    $this->Session->setFlash('施設関連情報の登録に失敗しました。', 'growl', array('type'=>'error') );
                    //リダイレクト
                    $this->redirect('facilities_list' . $this->request->data['MstFacility']['type']);
                }
            }
            
            // オペセット用倉庫を利用する場合、作成する
            if($facility_option['MstFacilityOption']['ope_set_use_type'] == '0'){
                // 利用しない。
                $this->MstDepartment->del(array('mst_facility_id' => $facility_option['MstFacilityOption']['mst_facility_id']
                                                , 'department_type' => Configure::read('DepartmentType.opestock') ));
            } else {
                // 利用する。
                // 存在チェック
                $sql = ' select a.id from mst_departments as a where a.mst_facility_id = ' . $facility_option['MstFacilityOption']['mst_facility_id'] . ' and department_type = ' . Configure::read('DepartmentType.opestock');
                $result = $this->MstDepartment->query($sql);
                if(isset($result[0][0]['id'])){
                    // 既にある場合
                    // 削除フラグを無効にする。
                    $this->MstDepartment->create();
                    $ret = $this->MstDepartment->updateAll(
                        array(
                            'MstDepartment.is_deleted' => "'false'",
                            'MstDepartment.modifier'   => $this->Session->read('Auth.MstUser.id'),
                            'MstDepartment.modified'   => "'" . $now . "'"
                            ),
                        array(
                            'MstDepartment.id' => $result[0][0]['id'],
                            ),
                        -1
                        );
                    if(!$ret){
                        $this->MstFacility->rollback();
                        $this->Session->setFlash('オペセット用情報の更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        //リダイレクト
                        $this->redirect('facilities_list' . $this->request->data['MstFacility']['type']);
                    }
                }else{
                    // ない場合
                    // 新規作成
                    $MstDepartment['MstDepartment']['mst_facility_id']        = $facility_option['MstFacilityOption']['mst_facility_id'];
                    $MstDepartment['MstDepartment']['department_name']        = 'ｵﾍﾟ用倉庫';
                    $MstDepartment['MstDepartment']['department_formal_name'] = 'オペ用倉庫';
                    $MstDepartment['MstDepartment']['department_code']        = '10000';
                    $MstDepartment['MstDepartment']['department_type']        = Configure::read('DepartmentType.opestock');
                    $MstDepartment['MstDepartment']['priority']               = 0;
                    $MstDepartment['MstDepartment']['is_deleted']             = false;
                    $MstDepartment['MstDepartment']['modifier']               = $this->Session->read('Auth.MstUser.id');
                    $MstDepartment['MstDepartment']['modified']               = $now;
                    $MstDepartment['MstDepartment']['creater']                = $this->Session->read('Auth.MstUser.id');
                    $MstDepartment['MstDepartment']['created']                = $now;
                    
                    $this->MstDepartment->create();
                    // SQL実行
                    if (!$this->MstDepartment->save($MstDepartment)) {
                        $this->MstFacility->rollback();
                        $this->Session->setFlash('オペセット用情報の更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        //リダイレクト
                        $this->redirect('facilities_list' . $this->request->data['MstFacility']['type']);
                    }
                }
            }
            
            // Configを取り直す。
            //$this->getConfigSetting($this->Session->read('Auth.facility_id_selected'));
        }
        
        // 新規でセンターの場合、分割テーブル関連のもろもろを処理する。
        if($this->request->data['MstFacility']['facility_type'] == Configure::read('FacilityType.center') &&
            !isset($this->request->data['MstFacility']['id'])){
            foreach(Configure::read('split_tables') as $table){
                // 分割子テーブルを作成する。
                $sql  = ' CREATE TABLE  ' . $table . '_' .  $mst_facility_id . ' (';
                $sql .= '         PRIMARY KEY (id), ';
                $sql .= '         CHECK (center_facility_id = ' .  $mst_facility_id . ' )';
                $sql .= ' ) INHERITS (' . $table . ');';
                $this->MstFacility->query($sql , false ,false );

                //テーブルコメント
                $sql  = ' select ';
                $sql .= '    psut.relname   as "TABLE_NAME" ';
                $sql .= '   ,pd.description as "TABLE_COMMENT" ';
                $sql .= 'from ';
                $sql .= '     pg_stat_user_tables psut ';
                $sql .= '    ,pg_description pd ';
                $sql .= 'where ';
                $sql .= "     psut.relname='" . $table . "'";
                $sql .= '    and psut.relid=pd.objoid ';
                $sql .= '    and pd.objsubid=0 ';
                $table_comment = $this->MstFacility->query($sql , false , false );
                foreach($table_comment as $tc){
                    $sql = "COMMENT ON TABLE " . $table . '_' .$mst_facility_id . " IS '" . $tc[0]['TABLE_COMMENT'] . "';";
                    $this->MstFacility->query($sql , false , false );
                }
                
                //カラムコメント
                $sql  = ' select ';
                $sql .= '       psat.relname   as "TABLE_NAME" ';
                $sql .= '     , pa.attname     as "COLUMN_NAME" ';
                $sql .= '     , pd.description as "COLUMN_COMMENT"  ';
                $sql .= '   from ';
                $sql .= '     pg_stat_all_tables psat ';
                $sql .= '     , pg_description pd ';
                $sql .= '     , pg_attribute pa  ';
                $sql .= '   where ';
                $sql .= '     psat.schemaname = (  ';
                $sql .= '       select ';
                $sql .= '             schemaname  ';
                $sql .= '         from ';
                $sql .= '           pg_stat_user_tables  ';
                $sql .= '         where ';
                $sql .= "           relname = '" . $table ."'";
                $sql .= '     )  ';
                $sql .= "     and psat.relname = '" . $table . "'";
                $sql .= '     and psat.relid = pd.objoid  ';
                $sql .= '     and pd.objsubid >= 0  ';
                $sql .= '     and pd.objoid = pa.attrelid  ';
                $sql .= '     and pd.objsubid = pa.attnum  ';
                $sql .= '   order by ';
                $sql .= '     pd.objsubid ';
                $comment = $this->MstFacility->query($sql , false , false);
                
                foreach($comment as $c){
                    $sql = " COMMENT ON COLUMN " . $table . '_' . $mst_facility_id . "." . $c[0]['COLUMN_NAME'] ." IS '" . $c[0]['COLUMN_COMMENT'] .  "';";
                    $this->MstFacility->query($sql , false ,false );
                }
                
                // テーブル名から、インデックスをひろう。
                $sql  = ' SELECT ';
                $sql .= '     indexdef ';
                $sql .= ' FROM ';
                $sql .= '     pg_indexes ';
                $sql .= " WHERE tablename = '" . $table  . "'";
                $index = $this->MstFacility->query($sql, false , false );
                foreach( $index as $i ){
                    $tmp = $i[0]['indexdef'];
                    if(strpos($tmp, 'UNIQUE') ===false ){ // PKインデックスは除く
                        $sql = str_replace($table, $table . '_' . $mst_facility_id , $tmp);
                        $this->MstFacility->query($sql , false, false );
                    }
                }
            }
        }
        $this->MstFacility->commit();

        // センターの場合
        if($this->request->data['MstFacility']['facility_type'] == Configure::read('FacilityType.center') ) {
            if(isset($this->request->data['MstFacility']['id'])){
                $mst_center_id = $this->request->data['MstFacility']['id'];
            }else{
                $mst_center_id = $this->MstFacility->getLastInsertId();
            }
            $this->set('Facility_List',$this->getFacilityRelationList($mst_center_id , true));
        }
        // コンフィグを取り直す
        //$this->getConfigSetting($this->Session->read('Auth.facility_id_selected'));
    }

    /**
     * 更新時間チェック&エラー処理
     * @param string $updateTime
     * @param string $nowTime
     * @return val
     */
    private function checkUpdateTime($updateTime, $nowTime, $url = null){
        if($nowTime < $updateTime){
            if(!$url){
                return false;
            } else {
                $this->Session->setFlash('処理中のデータに更新がかけられました。', 'growl', array('type'=>'error') );
                $this->redirect(array('action' => $url,$_POST['type']));
                exit;
                return false;
            }
        }
        return true;
    }

    /* センター追加/編集時の関連性リスト */
    private function getFacilityRelationList($facility_id = null , $inner=false){
        $sql  = 'select ';
        $sql .= '      a.id             as "MstFacility__id"';
        $sql .= '    , a.facility_code  as "MstFacility__facility_code"';
        $sql .= '    , a.facility_name  as "MstFacility__facility_name"';
        $sql .= '     , (  ';
        $sql .= '       case a.facility_type  ';
        foreach( Configure::read('FacilityType.typelist') as $key => $value ){
            $sql .= ' when ' . $key . " then '" . $value . "'";
        }
        $sql .= '         end ';
        $sql .= '     )                 as "MstFacility__facility_type" ';
        if($facility_id != null){
            $sql .= '    , b.id             as "MstFacility__relation_id"';
        }
        $sql .= '  from ';
        $sql .= '    mst_facilities as a  ';
        if($facility_id != null){
            if($inner){
                $sql .= ' inner ';
            }else{
                $sql .= ' left ';
            }
            $sql .= '    join mst_facility_relations as b  ';
            $sql .= '      on b.partner_facility_id = a.id ';
            $sql .= '      and b.mst_facility_id = ' . $facility_id ;
            $sql .= '      and b.is_deleted = false ';

        }
        $sql .= '  where a.is_deleted = false ';
        $sql .= '  and a.facility_type not in ( ' . Configure::read('FacilityType.center') .','  .Configure::read('FacilityType.outside') .') ';
        if($facility_id != null){
            $sql .= ' and a.id <> ' . $facility_id ;
        }
        $sql .= ' order by a.facility_type , a.facility_code ';

        return $this->MstFacility->query($sql);
    }

    /*
     * センターIDと、相手施設のIDで、施設関連テーブルのIDを返す
     */
    private function getRelationId($facility_id , $partner_id){
        $params = array (
            'conditions' => array('MstFacilityRelation.mst_facility_id' => $facility_id,
                                  'MstFacilityRelation.partner_facility_id' => $partner_id,
                                  ),
            'fields'     => array('MstFacilityRelation.id'),
            'recursive'  => -1
            );

        $tmp = $this->MstFacilityRelation->find('first', $params);
        return (isset($tmp['MstFacilityRelation']['id'])?$tmp['MstFacilityRelation']['id']:false);
    }

    public function export_csv(){
        App::import('Sanitize');

        if($this->request->data['MstFacility']['type'] == 'mode0'){
            //センター
            $where = ' a.facility_type = ' . Configure::read('FacilityType.center');
        }else{
            //施主、病院、仕入先
            $where = ' a.facility_type in ( '.Configure::read('FacilityType.hospital') .' ,'.Configure::read('FacilityType.supplier').' , '.Configure::read('FacilityType.donor').') ';
        }

        //検索条件追加

        //施設コード(LIKE検索)
        if(($this->request->data['MstFacility']['search_facility_code'] != "")){
            $where .= " and a.facility_code LIKE '%".Sanitize::escape($this->request->data['MstFacility']['search_facility_code'])."%'";
        }
        //施設名称(LIKE検索)
        if(($this->request->data['MstFacility']['search_facility_name'] != "")){
            $where .= " and a.facility_name LIKE '%".Sanitize::escape($this->request->data['MstFacility']['search_facility_name'])."%'";
        }

        //施設区分(完全一致)
        if((isset($this->request->data['MstFacility']['search_facility_type'])) && ($this->request->data['MstFacility']['search_facility_type'] != "")){
            $where .= ' and a.facility_type = ' . Sanitize::escape($this->request->data['MstFacility']['search_facility_type']) ;
        }
        //削除済み表示
        if(!isset($this->request->data['MstFacility']['search_is_deleted'])){
            $where .= ' and a.is_deleted = false ';
        }

        //関連施設のみ表示
        if(isset($this->request->data['MstFacility']['search_relation'])){
            $where .= ' and b.id is not null ';
        }

        $sql  = ' select ';
        $sql .= '       a.facility_code          as 施設コード ';
        $sql .= '     , ( case  a.facility_type ';
        foreach(Configure::read('FacilityType.typelist') as $k => $v ){
            $sql .= " when ${k} then '" . $v ."'" ;
        }
        $sql .= '       end )                    as 施設区分';
        $sql .= '     , a.facility_name          as 施設名 ';
        $sql .= '     , a.facility_formal_name   as 正式名 ';
        $sql .= '     , a.zip                    as 郵便番号 ';
        $sql .= '     , a.address                as 住所 ';
        $sql .= '     , a.tel                    as 電話番号 ';
        $sql .= '     , a.fax                    as FAX番号 ';
        $sql .= '     , a.days_before_expiration as 期限切れ警告日数 ';
        $sql .= '     , a.subcode                as サブコード ';
        if($this->request->data['MstFacility']['type'] == 'mode1'){
            $sql .= '   , ( case a.gross ';
            foreach(Configure::read('Gross') as $k => $v ){
                $sql .= " when '" . $k . "' then '" . $v . "'";
            }
            $sql .= '     end )                  as まとめ区分 ';
            $sql .= '   , ( case a.round ';
            foreach(Configure::read('Round') as $k => $v ){
                $sql .= " when '" . $k ."' then '" . $v . "'";
            }
            $sql .= '     end )                  as 丸め区分 ';
        }
        $sql .= "     , ( case when a.is_deleted  = true then '○' else '' end ) as 削除フラグ  ";
        $sql .= '     , c.user_name              as 更新者 ';
        $sql .= '     , a.modified               as 更新日時 ';
        $sql .= '   from ';
        $sql .= '     mst_facilities as a  ';
        $sql .= '   left join ';
        $sql .= '     mst_facility_relations as b ';
        $sql .= '     on b.mst_facility_id =  ' . $this->request->data['MstFacility']['mst_facility_id'] ;
        $sql .= '     and b.partner_facility_id = a.id' ;
        $sql .= '     and b.is_deleted = false ';
        $sql .= '   left join ';
        $sql .= '     mst_users as c ';
        $sql .= '     on a.modifier = c.id ';
        $sql .= '   where ';
        $sql .= $where;
        $sql .= '   order by ';
        $sql .= '     a.facility_type ';
        $sql .= '     , a.facility_code ';

        $this->db_export_csv($sql , "施設一覧", '/mst_facilities/facilities_list/'.$this->request->data['MstFacility']['type']);
    }


    private function getOptionId($facility_id){
        $sql = ' select id from mst_facility_options where mst_facility_id = ' . $facility_id ;
        $ret = $this->MstFacilityOption->query($sql);
        if(is_null($ret)){
            return null;
        }else{
            return $ret[0][0]['id'];
        }
    }

}


