<?php
/**
 * MstRepaymentsController
 *
 * @version 1.0.0
 * @since 2010/09/06
 */

class RepaymentsController extends AppController {
    var $name = 'Repayments';

    /**
     *
     * @var array $uses
     */
    var $uses = array('MstClass',                       //作業区分
                      'MstMenu',                        //
                      'MstFacilityItem',                //施設採用品
                      'MstFacilityRelation',            //施設関係
                      'MstFacility',                    //施設
                      'MstDepartment',                  //部署
                      'MstDealer',                      //販売元
                      'TrnCloseHeader',                 //締め
                      'TrnClaim',                       //請求テーブル（売上データ）
                      'TrnConsume',                     //消費明細
                      'TrnSticker',                     //シール
                      'MstItemUnit',                    //包装単位マスタ
                      'TrnReceiving',                   //入荷
                      'TrnShipping',                    //出荷
                      'TrnShippingHeader',
                      'MstUnitName',                    //単位名
                      'TrnStock',                       //在庫
                      'TrnRepayHeader',                 //返却ヘッダ
                      'TrnReceivingHeader',             //入荷ヘッダ
                      'TrnReceiving',                   //入荷明細
                      'TrnMoveHeader',                  //移動ヘッダ
                      'TrnStickerRecord',               //シール移動履歴
                      'MstUser',                        //ユーザ
                      'MstTransactionConfig',           //仕入設定
                      'TrnOrder',                       //発注明細
                      );

    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;

    /**
     * @var MstClasses
     */
    var $MstClasses;


    //ログインなしでの印刷を許可
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
    }

    /**
     * 返却登録
     * 初期画面表示＆検索結果表示
     */
    function index() {
        $this->setRoleFunction(28); //返却登録

        $this->request->data['MstRepayment']['work_date'] = date('Y/m/d');

        //得意先プルダウン用データ取得
        $this->set('facility_list',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected'),
                       array(Configure::read('FacilityType.hospital')),
                       false
                       )
                   );

        //作業区分プルダウン用データ取得
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 12 ));
    }

    /*
     * 返却登録
     * 売上選択
     */
    function RepaymentList(){
        $SearchList = array();
        $CartList = array();

        App::import('Sanitize');
        if(isset($this->request->data['search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $search_where = " and d.id = " . $this->request->data['MstRepayment']['departmentId'];
            $cart_where = "";

            //売上日(開始)
            if(!empty($this->request->data['MstRepayment']['search_start_date'])){
                $search_where .= " and a.claim_date >=  '" . $this->request->data['MstRepayment']['search_start_date'] . "'";
            }
            //売上日(終了)
            if(!empty($this->request->data['MstRepayment']['search_end_date'])){
                $search_where .= " and a.claim_date <=  '" . $this->request->data['MstRepayment']['search_end_date'] . "'";
            }

            //商品ID
            if(!empty($this->request->data['MstRepayment']['search_internal_code'])){
                $search_where .= " and c.internal_code =  '" . $this->request->data['MstRepayment']['search_internal_code'] . "'";
            }
            //商品名
            if(!empty($this->request->data['MstRepayment']['search_item_name'])){
                $search_where .= " and c.item_name like '%" . $this->request->data['MstRepayment']['search_item_name'] . "%'";
            }

            //製品番号
            if(!empty($this->request->data['MstRepayment']['search_item_code'])){
                $search_where .= " and c.item_code like  '%" . $this->request->data['MstRepayment']['search_item_code'] . "%'";
            }

            //規格
            if(!empty($this->request->data['MstRepayment']['search_standard'])){
                $search_where .= " and c.standard like '%" . $this->request->data['MstRepayment']['search_standard'] . "%'";
            }

            //販売元
            if(!empty($this->request->data['MstRepayment']['search_dealer_name'])){
                $search_where .= " and c1.dealer_name like '%" . $this->request->data['MstRepayment']['search_dealer_name'] . "%'";
            }

            //ロット番号
            if(!empty($this->request->data['MstRepayment']['search_lot_no'])){
                $search_where .= " and e.lot_no like '%" . $this->request->data['MstRepayment']['search_lot_no'] . "%'";
            }

            //シール番号
            if(!empty($this->request->data['MstRepayment']['search_sticker_no'])){
                $search_where .= " and ( a.hospital_sticker_no = '" . $this->request->data['MstRepayment']['search_sticker_no'] . "'";
                $search_where .= "    or a.facility_sticker_no = '" . $this->request->data['MstRepayment']['search_sticker_no'] . "' ) ";
            }

            //カートに入っているものは検索に表示しない
            if(!empty($this->request->data['search']['tmpId'])){
                $search_where .= ' and a.id not in ('  . $this->request->data['search']['tmpId']. ')';
            }
            //カート内の商品を取得
            if(!empty($this->request->data['search']['tmpId'])){
                $cart_where = ' and a.id in ('  . $this->request->data['search']['tmpId']. ')';
            }else{
                //選択されているものがない場合はヒットさせない
                $cart_where = ' and 1=0';
            }

            $SearchList = $this->getRepaymentList($search_where ,   $this->request->data['limit']);
            $CartList = $this->getRepaymentList($cart_where);
            $this->request->data = $data;
        } else {
            //初回遷移時のみ締めチェック
            $_res = 0;
            $f_id = $this->Session->read('Auth.facility_id_selected');
            if(!$this->canUpdateAfterClose($this->request->data['MstRepayment']['work_date'],$f_id)){
                $this->Session->setFlash('指定した返却日にすでに締めが行われています', 'growl', array('type'=>'error'));
                //締め済みなので、以降の処理は行わず戻る
                $this->redirect('index');
            }

            $this->request->data['MstRepayment']['facilityId'] = $this->getFacilityId($this->request->data['MstRepayment']['facilityCode']  , array(Configure::read('FacilityType.hospital')));
            $this->request->data['MstRepayment']['departmentId'] = $this->getDepartmentId($this->request->data['MstRepayment']['facilityId'] , Configure::read('DepartmentType.hospital') , $this->request->data['MstRepayment']['departmentCode']);
        }

        $this->set('CartList' , $CartList);
        $this->set('SearchList', $SearchList);
    }

    /**
     * 返却登録
     * バーコード読込
     */
    function read() {
        $_res = 0;
        $f_id = $this->Session->read('Auth.facility_id_selected');
        if(!$this->canUpdateAfterClose($this->request->data['MstRepayment']['work_date'],$f_id)){
            $_res++;
        }

        if($_res != 0){
            $this->Session->setFlash('指定した返却日にすでに締めが行われています', 'growl', array('type'=>'error'));
            //締め済みなので、以降の処理は行わず戻る
            $this->redirect('index');
        } else {
            $this->request->data['MstRepayment']['facilityId']   = $this->getFacilityId($this->request->data['MstRepayment']['facilityCode'] , array(Configure::read('FacilityType.hospital')));
            $this->request->data['MstRepayment']['departmentId'] = $this->getDepartmentId($this->request->data['MstRepayment']['facilityId'] , Configure::read('DepartmentType.hospital') , $this->request->data['MstRepayment']['departmentCode']);

            $this->render('read_stickers');
        }
    }

    /**
     * 返却登録
     * 確認画面表示
     */
    function conf() {
        //更新時間チェック用に画面を開いた時間を保存
        $this->Session->write('Repayment.readTime',date('Y-m-d H:i:s'));
        $result = array();
        $errlist = array();
        /*
        //シール読み込みからの場合
        if(isset($this->request->data['codez'])){
            $dummy = array('MstRepayment'=>
                           array(
                               'id'              => '',
                               'work_no'         => '',
                               'work_date'       => '',
                               'internal_code'   => '',
                               'item_name'       => '',
                               'item_code'       => '',
                               'standard'        => '',
                               'unit_name'       => '',
                               'unit_price'      => '',
                               'quantity'        => '',
                               'is_lowlevel'     => '',
                               'lot_no'          => '',
                               'validated_date'  => '',
                               'trade_type_name' => '',
                               'dealer_name'     => '',
                               'facility_name'   => '',
                               'recital'         => '',
                               )
                );

            $i= 0;
            foreach(explode(',',substr($this->request->data['codez'],0,-1)) as $sticker_no ){
                //一枚づつチェック
                $ret = $this->_readStickerCheck($sticker_no);
                switch($ret[0][0]['status']){
                  case 2:
                    $errlist[$i] = $dummy;
                    $errlist[$i]['MstRepayment']['hospital_sticker_no'] = $sticker_no;
                    $errlist[$i]['MstRepayment']['status']              = $ret[0][0]['status'];
                    $errlist[$i]['MstRepayment']['recital']             = "存在しないシール番号です";
                    $i++;
                    break;
                  case 3:
                    $errlist[$i] = $dummy;
                    $errlist[$i]['MstRepayment']['hospital_sticker_no'] = $sticker_no;
                    $errlist[$i]['MstRepayment']['status']              = $ret[0][0]['status'];
                    $errlist[$i]['MstRepayment']['recital']             = "部署違いです";
                    $i++;
                    break;
                  case 4:
                    $errlist[$i] = $dummy;
                    $errlist[$i]['MstRepayment']['hospital_sticker_no'] = $sticker_no;
                    $errlist[$i]['MstRepayment']['status']              = $ret[0][0]['status'];
                    $errlist[$i]['MstRepayment']['recital']             = "売上データの無いシール番号です";
                    $i++;
                    break;
                  default:
                    $this->request->data['SelectedTrnRepaymentID'][] = $ret[0][0]['id'];
                    break;
                }
            }
        }
          */
        
        //作業区分プルダウン用データ取得
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 12 ));

        //選択したシールから情報を取得
        if(isset($this->request->data['RepaymentList']['selected_id'])){
            $where = ' and a.id in ( ' . $this->request->data['RepaymentList']['selected_id'] . ') ';
            $result = $this->getRepaymentList($where);
        } else if(isset($this->request->data['SelectedTrnRepaymentID'])){
            $where = ' and a.id in ( ' . join(',' , $this->request->data['SelectedTrnRepaymentID']) . ') ';
            $result = $this->getRepaymentList($where);

        }
        /*
        //シール読み込みからの場合エラーリストをマージする。
        if(isset($this->request->data['codez'])){
            $result = array_merge($result , $errlist);
        }
          */

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnRepayments.token' , $token);
        $this->request->data['TrnRepayments']['token'] = $token;

        $this->set('result', $result);
    }

    /**
     * 完了画面
     */
    function result() {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['TrnRepayments']['token'] === $this->Session->read('TrnRepayments.token')) {
            $this->Session->delete('TrnRepayments.token');

            //トランザクションの開始
            $this->TrnSticker->begin();

            //登録処理
            $repay_data = $this->repayment_regist();

            if(!$repay_data){
                //ロールバック
                $this->TrnSticker->rollback();
                //個別のエラーメッセージを上書きしてしまっているのでコメントアウト
                $this->redirect('index');
            } else {
                //コミット
                $this->TrnSticker->commit();
                // 表示用のデータを取得
                $this->set('result' , $repay_data);
                $this->Session->write('TrnRepayment.result' , $repay_data);
            }
        }else{
            // トランザクショントークン不正
            // リロード or ２度押し
            $this->set('result' , $this->Session->read('TrnRepayment.result'));
        }
    }
    
    /*
     * 返却登録処理メイン
     */
    private function repayment_regist(){
        $now = date('Y/m/d H:i:s.u');
        //POSTデータから必要情報を取得
        // $regist_data = $this->_getRegistData();
        $repay_header_ids = array();
        $ids = array();
        //行ロック（請求）
        $this->TrnClaim->query(' select * from trn_claims as a where a.id in (' . join(',',$this->request->data['MstRepayment']['id']) . ') for update ' );

        //更新チェック
        $ret = $this->TrnClaim->query(" select count(*) from trn_claims as a where a.modified > '" . $this->request->data['TrnRepayments']['time'] . "' and a.id in (" .join(',',$this->request->data['MstRepayment']['id']). ")");
        if($ret[0][0]['count'] > 0){
            $this->Session->setFlash('処理開始更新チェックエラー', 'growl', array('type'=>'error') );
            return false;
        }
        
        // 関連情報を取得
        $sql  = ' select ';
        $sql .= '       a.id                       as sale_id ';
        $sql .= '     , a.unit_price               as sale_unit_price ';
        $sql .= '     , a.claim_price              as sale_claim_price ';
        $sql .= '     , a.count                    as sale_count ';
        $sql .= '     , a.gross                    as sale_gross ';
        $sql .= '     , a.round                    as sale_round ';
        $sql .= '     , c.is_lowlevel ';
        $sql .= '     , c.buy_flg ';
        $sql .= '     , c.item_type ';
        $sql .= '     , d.trade_type  ';
        $sql .= '     , ( case when d.trade_type = ' . Configure::read('ClassesType.Deposit') . ' then g.mst_facility_id else c.mst_facility_id end ) as facility_id';
        $sql .= '     , ( case when d.trade_type = ' . Configure::read('ClassesType.Deposit') . ' then g.id else c1.id end ) as department_id';
        $sql .= '     , coalesce(a.count , d.quantity )   as quantity';
        $sql .= '     , coalesce(d.id, a.trn_sticker_id)  as sticker_id';
        $sql .= '     , coalesce(d.hospital_sticker_no , a.hospital_sticker_no) as hospital_sticker_no';
        $sql .= '     , d.facility_sticker_no';
        $sql .= '     , a.mst_item_unit_id ';
        $sql .= '   from ';
        $sql .= '     trn_claims as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_departments as c1 ';
        $sql .= '       on c1.mst_facility_id = c.mst_facility_id';
        $sql .= '      and c1.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '     left join trn_stickers as d  ';
        $sql .= '       on d.sale_claim_id = a.id  ';
        $sql .= '     left join trn_orders as e  ';
        $sql .= '       on e.id = d.trn_order_id  ';
        $sql .= '     left join trn_receivings as f  ';
        $sql .= '       on f.id = d.trn_receiving_id '; 
        $sql .= '     left join mst_departments as g ';
        $sql .= '       on g.id = f.department_id_to ';
        $sql .= '   where ';
        $sql .= '     a.id in ('. join(',',$this->request->data['MstRepayment']['id']) .') ';

        $regist_data = $this->TrnClaim->query($sql);
        
        $pre_id = null;
        $repay_header_id = null;

        $repay_count = 1;

        foreach($regist_data as $r){
            //返却ヘッダ作成
            if($r[0]['facility_id'] != $pre_id) {
                $repay_count = 1;
                $move_count = 1;

                //返却ヘッダ明細件数を入れなおす
                if(!is_null($repay_header_id)){
                    $this->TrnRepayHeader->create();
                    $this->TrnRepayHeader->recursive = -1;
                    if(!$this->TrnRepayHeader->updateAll(array('TrnRepayHeader.detail_count' => $repay_count) ,
                                                         array('TrnRepayHeader.id' => $repay_header_id)
                                                         )){
                        $this->Session->setFlash('返却ヘッダ更新エラー', 'growl', array('type'=>'error') );
                        return false;
                    }
                }


                //返却ヘッダ用の作業番号
                $repay_work_no = $this->setWorkNo4Header($this->request->data['MstRepayment']['work_date'],'12');
                $work_seq = 1;

                $TrnRepayHeader = array(
                    'TrnRepayHeader'=> array(
                        'work_no'            => $repay_work_no ,
                        'work_date'          => $this->request->data['MstRepayment']['work_date'] ,
                        'recital'            => $this->request->data['MstRepayment']['recital'],
                        'repay_type'         => Configure::read('RepayType.normal'),
                        'department_id_from' => $this->request->data['MstRepayment']['departmentId'], //病院部署
                        'department_id_to'   => $r[0]['department_id'],
                        'detail_count'       => 0, //後で更新
                        'is_deleted'         => false ,
                        'creater'            => $this->Session->read('Auth.MstUser.id'),
                        'modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'created'            => $now,
                        'modified'           => $now,
                        )
                    );

                $this->TrnRepayHeader->create();
                if(!$this->TrnRepayHeader->save($TrnRepayHeader)){
                    $this->Session->setFlash('返却ヘッダ作成エラー', 'growl', array('type'=>'error') );
                    return false;
                }
                $repay_header_id = $this->TrnRepayHeader->getLastInsertID();
                $pre_id = $r[0]['facility_id'];

                //結果画面で表示のためIDを確保
                $repay_header_ids[] = $repay_header_id;

            }else{
                $repay_count = $repay_count + 1;
            }
            
            if( $r[0]['is_lowlevel'] == true ){
                $hospital_sticker_no = '';
                $facility_sticker_no = '';
            } elseif($r[0]['trade_type'] != Configure::read('ShippingType.deposit')){
                //倉庫に戻す場合には、部署シール番号を空にする
                $hospital_sticker_no = '';
                $trade_type = Configure::read('ShippingType.fixed');
                if(empty($r[0]['facility_sticker_no'])){
                    // センターシール番号が空の場合、采番しなおす。
                    $facility_sticker_no = $this->getCenterStickerNo($this->request->data['MstRepayment']['work_date'],
                                                                     $this->getFacilityCode($this->Session->read('Auth.facility_id_selected')));
                    
                }else{
                    // センターシール番号がある場合、そのまま。
                    $facility_sticker_no = $r[0]['facility_sticker_no'];
                }
                
            }else{
                //病院の残す場合には、部署シール番号はそのまま
                $hospital_sticker_no = $r[0]['hospital_sticker_no'];
                $facility_sticker_no = $r[0]['facility_sticker_no'];
                $trade_type = Configure::read('ShippingType.deposit');
            }

            
            //返却明細レコード作成
            $TrnReceiving = array(
                'TrnReceiving'=> array(
                    'work_no'              => $repay_work_no ,
                    'work_seq'             => $work_seq,
                    'work_date'            => $this->request->data['MstRepayment']['work_date'],
                    'work_type'            => $this->request->data['MstRepayment']['work_type'][$r[0]['sale_id']],        //作業区分
                    'recital'              => $this->request->data['MstRepayment']['detail_recital'][$r[0]['sale_id']],
                    'receiving_type'       => Configure::read('ReceivingType.repay'),                                     //入荷区分:返却
                    'receiving_status'     => Configure::read('ReceivingStatus.stock'),                                   //入荷状態（入庫済）
                    'mst_item_unit_id'     => $r[0]['mst_item_unit_id'],                                                  //包装単位参照キー
                    'department_id_from'   => $this->request->data['MstRepayment']['departmentId'],                       //移動元（病院）
                    'department_id_to'     => $r[0]['department_id'],                                                     //移動先(預託品：病院、預託品以外：センタ
                    'quantity'             => $this->request->data['MstRepayment']['quantity'][$r[0]['sale_id']],         //数量
                    'stocking_price'       => 0,                                                                          //仕入価格
                    'creater'              => $this->Session->read('Auth.MstUser.id'),
                    'modifier'             => $this->Session->read('Auth.MstUser.id'),
                    'hospital_sticker_no'  => $r[0]['hospital_sticker_no'],                                               //部署シール番号
                    'trn_repay_header_id'  => $repay_header_id,                                                           //返却ヘッダ外部キー
                    'facility_sticker_no'  => $facility_sticker_no,                                                       //センターシール番号
                    'trn_sticker_id'       => $r[0]['sticker_id'],
                    )
                );

            $this->TrnReceiving->create();
            if(!$this->TrnReceiving->save($TrnReceiving)){
                $this->Session->setFlash('返却明細作成エラー', 'growl', array('type'=>'error') );
                return false;
            }
            $repay_id = $this->TrnReceiving->getLastInsertID();

            //マイナス売上作成
            if($r[0]['item_type'] == Configure::read('Items.item_types.setitem')){
                //セット品の場合
                

            }else{
                //通常品の場合
                $TrnClaim = array(
                    'TrnClaim' => array(
                        'department_id_from' => $r[0]['department_id'],                                //請求元参照キー
                        'department_id_to'   => $this->request->data['MstRepayment']['departmentId'],  //請求先参照キー
                        'mst_item_unit_id'   => $r[0]['mst_item_unit_id'],                             //包装単位参照キー
                        'claim_date'         => $this->request->data['MstRepayment']['work_date'],            //請求年月
                        'count'              => $this->request->data['MstRepayment']['quantity'][$r[0]['sale_id']],//請求数
                        'unit_price'         => $r[0]['sale_unit_price'], //請求単価
                        'round'              => $r[0]['sale_round'],                                //丸め区分
                        'gross'              => $r[0]['sale_gross'],                                //まとめ区分
                        'is_not_retroactive' => false,                                               //遡及除外フラグ
                        'claim_price'        => (0-$r[0]['sale_claim_price']), //請求金額※マイナス
                        'trn_receiving_id'   => $repay_id,                                           //返却明細(入荷)参照キー
                        'creater'            => $this->Session->read('Auth.MstUser.id'),
                        'modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'is_stock_or_sale'   => Configure::read('Claim.sales'),
                        'facility_sticker_no'=> $facility_sticker_no,
                        'hospital_sticker_no'=> $hospital_sticker_no,
                        'trn_sticker_id'     => $r[0]['sticker_id'],
                        'repay_id'           => $r[0]['sale_id'],
                        'claim_type'         => Configure::read('ClaimType.repay')
                        )
                    );

                //TrnClaimrオブジェクトをcreate
                $this->TrnClaim->create();
                if (!$this->TrnClaim->save($TrnClaim)) {
                    $this->Session->setFlash('マイナス売上作成エラー', 'growl', array('type'=>'error') );
                    return false;
                }
                $repay_claim_id = $this->TrnClaim->getLastInsertID();
                $ids[] = $repay_claim_id;
                
                if(!$this->TrnClaim->updateAll(array( 'TrnClaim.repay_id'  =>$repay_claim_id,
                                                      'TrnClaim.modifier'  => $this->Session->read('Auth.MstUser.id'),
                                                      'TrnClaim.modified'  => "'" . $now . "'",
                                                      ) ,
                                               array(
                                                   'TrnClaim.id'  => $r[0]['sale_id']
                                                   ) ,
                                               -1
                                               )){
                    $this->Session->setFlash('請求更新エラー', 'growl', array('type'=>'error'));
                    return false;
                }
            }

            //マイナス仕入作成（預託品）
            if($r[0]['trade_type'] == Configure::read('ShippingType.deposit')){
                
            }

            if($r[0]['is_lowlevel'] == false ) {
                //シール移動履歴（返却）
                //返却用のシール移動履歴作成
                $TrnStickerRecord = array(
                    'TrnStickerRecord'    => array(
                        'move_date'           => $this->request->data['MstRepayment']['work_date'],
                        'move_seq'            => $this->getNextRecord($r[0]['sticker_id']),
                        'record_type'         => Configure::read('RecordType.repay'),
                        'facility_sticker_no' => $facility_sticker_no,
                        'hospital_sticker_no' => $hospital_sticker_no,
                        'record_id'           => $repay_id, //返却明細(=入荷明細)
                        'trn_sticker_id'      => $r[0]['sticker_id'],
                        'mst_department_id'   => $r[0]['department_id'], //移動先部署
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modified'            => $now,
                        'is_deleted'          => false,
                        )
                    );
                //TrnStickerRecordオブジェクトをcreate
                $this->TrnStickerRecord->create();
                if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                    $this->Session->setFlash('シール移動履歴(返却)作成エラー', 'growl', array('type'=>'error') );
                    return false;
                }
            }
            //シール復活
            if($r[0]['is_lowlevel'] == true ){
                //低レベル品

            }else{
                // 通常品

                if(!$this->TrnSticker->updateAll(array( 'TrnSticker.is_deleted'          => 'false',
                                                        'TrnSticker.sale_claim_id'       => 'null',
                                                        'TrnSticker.quantity'            => 1,
                                                        'TrnSticker.modifier'            => $this->Session->read('Auth.MstUser.id'),
                                                        'TrnSticker.modified'            => "'" . $now . "'",
                                                        'TrnSticker.last_move_date'      => "'" . $this->request->data['MstRepayment']['work_date'] . "'",
                                                        'TrnSticker.trade_type'          => $trade_type,
                                                        'TrnSticker.mst_department_id'   => $r[0]['department_id'], //移動先部署
                                                        'TrnSticker.hospital_sticker_no' => "'" . $hospital_sticker_no . "'",
                                                        'TrnSticker.facility_sticker_no' => "'" . $facility_sticker_no . "'",
                                                        ) ,
                                                 array(
                                                     'TrnSticker.id'  => $r[0]['sticker_id']
                                                     ) ,
                                                 -1
                                                 )){
                    $this->Session->setFlash('シール更新エラー', 'growl', array('type'=>'error'));
                    return false;
                }
            }

            //在庫調整
            if($r[0]['quantity'] > 0 ){ // シールに数量が合った場合、病院部署から減
                if(!$this->TrnStock->updateAll(array('TrnStock.stock_count' => 'TrnStock.stock_count - ' . $r[0]['quantity'],
                                                     'TrnStock.modified'    => "'" . $now . "'",
                                                     'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                     ) ,
                                               array(
                                                   'TrnStock.id'  => $this->getStockRecode($r[0]['mst_item_unit_id'] ,
                                                                                           $this->request->data['MstRepayment']['departmentId'])
                                                   ) ,
                                               -1
                                               )){
                    $this->Session->setFlash('在庫更新エラー', 'growl', array('type'=>'error') );
                    return false;
                }
                
            }
            
            // 返却分の在庫を増やす
            if(!$this->TrnStock->updateAll(array('TrnStock.stock_count' => 'TrnStock.stock_count + ' . $r[0]['quantity'],
                                                 'TrnStock.modified'    => "'" . $now . "'",
                                                 'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                 ) ,
                                           array(
                                               'TrnStock.id'  => $this->getStockRecode($r[0]['mst_item_unit_id'] , $r[0]['department_id'])
                                               ) ,
                                           -1
                                           )){
                $this->Session->setFlash('在庫更新エラー', 'growl', array('type'=>'error') );
                return false;
            }
        }

        //返却ヘッダ明細件数を入れなおす
        if(!is_null($repay_header_id)){
            $this->TrnRepayHeader->create();
            $this->TrnRepayHeader->recursive = -1;
            if(!$this->TrnRepayHeader->updateAll(array('TrnRepayHeader.detail_count' => $repay_count) ,
                                                 array('TrnRepayHeader.id' => $repay_header_id)
                                                 )){
                $this->Session->setFlash('返却ヘッダ更新エラー', 'growl', array('type'=>'error') );
                return false;
            }
        }


        //更新チェック
        $ret = $this->TrnSticker->query(" select count(*) from trn_stickers as a where a.modified > '" . $this->request->data['TrnRepayments']['time'] . "' and a.modified <> '" .$now. "' and a.id in (" .join(',',$this->request->data['MstRepayment']['id']). ")");
        if($ret[0][0]['count'] > 0){
            $this->Session->setFlash('コミット前更新チェックエラー', 'growl', array('type'=>'error') );
            return false;
        }

        return $this->_getResultData($ids);
    }

    /**
     * print_invoice
     *
     * 納品書
     */
    function print_invoice(){
        App::import('Sanitize');

        //トランザクション開始
        $this->TrnRepayHeader->begin();
        //行ロック
        $this->TrnRepayHeader->query(" select * from trn_repay_headers as a where a.id in (" . join(',',$this->request->data['MstRepayment']['id']) . ") for update  ");

        $result = $this->getPrintData($this->request->data['MstRepayment']['id'] , 2);

        //納品書
        $columns = array("納品先名",
                         "納品番号",
                         "納品先部署名",
                         "施主名",
                         "納品日",
                         "納品先住所",
                         "施主住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "部署シール",
                         "ロット番号",
                         "明細備考",
                         "数量",
                         "包装単位",
                         "管理区分",
                         "商品ID",
                         "単価",
                         "金額",
                         "ヘッダ備考",
                         "丸め区分",
                         "印刷年月日",
                         "改ページ番号"
                         );

        $work_seq = 1;
        $work_seq_check = "";
        $now = date('Y/m/d');
        foreach($result as $r){

            if($work_seq_check != $r[0]['page_break'] ){
                $work_seq = 1;
            }

            $output[] = array(
                 $r[0]['hospital_name']         //納品先名
                ,$r[0]['work_no']               //納品番号
                ,$r[0]['department_name']       //納品先部署名
                ,$r[0]['owner_name']            //施主名
                ,$r[0]['work_date']             //納品日
                ,$r[0]['hospital_address']      //納品先住所
                ,$r[0]['owner_address']         //施主住所
                ,$work_seq                      //行番号
                ,$r[0]['item_name']             //商品名
                ,$r[0]['standard']              //規格
                ,$r[0]['item_code']             //製品番号
                ,$r[0]['dealer_name']           //販売元
                ,$r[0]['hospital_sticker_no']   //部署シール
                ,$r[0]['lot_no']                //ロット番号
                ,$r[0]['recital']               //明細備考
                ,(0-$r[0]['quantity'])          //数量
                ,$r[0]['unit_name']             //包装単位
                ,$r[0]['type_name']             //管理区分
                ,$r[0]['internal_code']         //商品ID
                ,$r[0]['unit_price']            //単価（請求テーブル.請求単価）
                ,$r[0]['claim_price']           //金額（請求テーブル.請求金額）
                ,$r[0]['header_recital']        //ヘッダ備考
                ,$r[0]['round']                 //得意先丸め区分
                ,$now                           //印刷年月日
                ,$r[0]['page_break']            //改ページ番号
                );

            $work_seq_check = $r[0]['page_break'];
            $work_seq++;

            //再印刷用にヘッダの印刷日時を更新
            $condtion = array( 'TrnRepayHeader.id' => $this->request->data['MstRepayment']['id'] );
            $updatefield = array( 'printed_date'   => "'".$now."'",  //印刷日時
                                  );

            //TrnRepayHeaderオブジェクトをcreate
            $this->TrnRepayHeader->create();

            //SQL実行
            if (!$this->TrnRepayHeader->updateAll( $updatefield, $condtion )) {
                //ロールバック
                $this->TrnRepayHeader->rollback();
                $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                $this->redirect('index');
            }
            $this->TrnRepayHeader->commit();
            unset($condtion,$updatefield);
        }

        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'納品書');
        $this->xml_output($output,join("\t",$columns),$layout_name['09'],$fix_value,'invoice');

    }
    /**
     * xml出力テスト用
     * @param
     * @todo 各コントローラの共通関数化
     */
    function xml_output($data,$columns,$layout_name,$fix_value,$layout_file){

        $this->layout = 'xml/default';
        $this->set('columns',$columns);
        $this->set('posted',$data);
        $this->set('layout_name',$layout_name);
        $this->set('fix_value',$fix_value); //連想配列のキーにname属性を指定

        $this->render('/repayments/'.$layout_file);

    }

    /*
     * 帳票印刷
     */
    function report($mode){
        if($mode == "invoice"){
            $this->print_invoice();
        } elseif($mode == "requestsales"){
            $this->print_requestsales();
        }
    }

    /**
     * print_requestsales
     *
     * 売上依頼表印刷
     */
    function print_requestsales(){

        //トランザクション開始
        $this->TrnRepayHeader->begin();
        //行ロック
        $this->TrnRepayHeader->query(" select * from trn_repay_headers as a where a.id in (" . join(',',$this->request->data['MstRepayment']['id']) . ") for update  ");

        $result = $this->getPrintData($this->request->data['MstRepayment']['id'] , 1);

        $columns = array("売上依頼番号",
                         "仕入先名",
                         "売上番号",
                         "売上部署名",
                         "施主名",
                         "売上日",
                         "施主住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "金額",
                         "単価",
                         "明細備考",
                         "数量",
                         "包装単位",
                         "課税区分",
                         "部署シール",
                         "ロット番号",
                         "ヘッダ備考",
                         "丸め区分",
                         "印刷年月日",
                         "改ページ番号"
                         );
        App::import('Sanitize');

        //売上依頼票
        $work_seq = 1;
        $now = date('Y/m/d H:i:s.u');
        $output = array();

        $work_seq_check = "";
        foreach($result as $r){

            if($work_seq_check != $r[0]['page_break']){
                $work_seq = 1;
            }

            $output[] = array(
                $r[0]['work_no']
                ,$r[0]['supplier_name']."様"    //仕入先名
                ,$r[0]['work_no']               //売上番号
                ,$r[0]['hospital_name']         //売上部署名
                ,$r[0]['owner_name']            //施主名
                ,$r[0]['work_date']             //売上日
                ,$r[0]['owner_address']         //施主住所
                ,$work_seq                      //行番号
                ,$r[0]['item_name']             //商品名
                ,$r[0]['standard']              //規格
                ,$r[0]['item_code']             //製品番号
                ,$r[0]['dealer_name']           //販売元
                ,$r[0]['internal_code']         //商品ID
                ,$r[0]['claim_price']           //金額（請求テーブル.請求金額）
                ,$r[0]['unit_price']            //単価（請求テーブル.請求単価）
                ,$r[0]['recital']               //明細備考
                ,(0-$r[0]['quantity'])          //数量
                ,$r[0]['unit_name']             //包装単位
                ,$r[0]['tax_name']              //課税区分
                ,$r[0]['hospital_sticker_no']   //部署シール
                ,$r[0]['lot_no']                //ロット番号
                ,$r[0]['header_recital']        //ヘッダ備考
                ,$r[0]['round']                 //仕入先丸め区分
                ,$r[0]['printed_date2']         //印刷年月日：再印刷判定用
                ,$r[0]['page_break']            //改ページ番号
                );

            $work_seq_check = $r[0]['page_break'];
            $work_seq++;
        }

        //再印刷用にヘッダの印刷日時を更新
        $condtion = array( 'TrnRepayHeader.id' => $this->request->data['MstRepayment']['id']);
        $updatefield = array( 'printed_date2'  => "'" . $now . "'");

        //TrnPromiseオブジェクトをcreate
        $this->TrnRepayHeader->create();

        //SQL実行
        if (!$this->TrnRepayHeader->updateAll( $updatefield, $condtion )) {
            //ロールバック
            $this->TrnRepayHeader->rollback();//返却ヘッダ
            $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
            $this->redirect('index');
        }
        $this->TrnRepayHeader->commit();
        unset($condtion,$updatefield);

        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'売上依頼票');
        $this->xml_output($output,join("\t",$columns),$layout_name['12'],$fix_value,'requestsales');

    }

    /* 返却一覧を取得 */
    private function getRepaymentList($where , $limit = null){
        $sql  = ' select ';
        $sql .= '       a.id                  as "MstRepayment__id"';
        $sql .= "     , to_char(a.claim_date, 'yyyy/mm/dd') ";
        $sql .= '                             as "MstRepayment__claim_date" ';
        $sql .= '     , c.internal_code       as "MstRepayment__internal_code"';
        $sql .= '     , c.item_name           as "MstRepayment__item_name"';
        $sql .= '     , c.item_code           as "MstRepayment__item_code"';
        $sql .= '     , c.standard            as "MstRepayment__standard" ';
        $sql .= '     , c1.dealer_name        as "MstRepayment__dealer_name"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= "         then a.count || b1.unit_name  ";
        $sql .= "         else a.count || b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                       as "MstRepayment__unit_name" ';
        $sql .= '     , a.hospital_sticker_no as "MstRepayment__hospital_sticker_no"';
        $sql .= '     , e.lot_no              as "MstRepayment__lot_no"';
        $sql .= "     , to_char(e.validated_date, 'yyyy/mm/dd') ";
        $sql .= '                             as "MstRepayment__validated_date" ';
        $sql .= '     , ( case a.trade_type ';
        foreach( Configure::read('Classes') as $k => $v){
            $sql .= "        when ". $k ." then '" . $v . "'";
        }
        $sql .= '       end)                  as "MstRepayment__trade_type_name" ';
        $sql .= '     , a.unit_price          as "MstRepayment__unit_price"';
        $sql .= '     , a.count               as "MstRepayment__quantity"';

        $sql .= '   from ';
        $sql .= '     trn_claims as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as c1  ';
        $sql .= '       on c1.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_departments as d  ';
        $sql .= '       on d.id = a.department_id_to  ';
        $sql .= '     left join trn_stickers as e  ';
        $sql .= '       on e.sale_claim_id = a.id  ';
        $sql .= '   where ';
        $sql .= "     a.is_stock_or_sale = '" . Configure::read('Claim.sales') . "'";
        $sql .= '     and c.item_type = ' . Configure::read('Items.item_types.normalitem');
        $sql .= '     and a.is_deleted = false  ';
        $sql .= '     and a.repay_id is null  ';
        $sql .= '     and c.is_lowlevel = false ';
        $sql .= '     and c.buy_flg = false ';
        $sql .= '     and a.claim_type = ' . Configure::read('ClaimType.sale');
        $sql .= $where;
        $sql .= '   order by ';
        $sql .= '     a.claim_date ';
        $sql .= '     , a.id  ';
        if(!is_null($limit)){
            $this->set('max',$this->getMaxCount($sql , 'TrnClaim'));
            $sql .= ' limit ' . $limit;
        }
        
        return $this->TrnClaim->query($sql);
    }

    /*
     * 登録用に必要データを取得する
     */
    private function _getRegistData(){
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , d.id                           as receiving_id';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.trade_type = ' . Configure::read('ShippingType.deposit');
        $sql .= '         then f.mst_facility_id  ';
        $sql .= '         else c.mst_facility_id  ';
        $sql .= '         end ';
        $sql .= '     )                                as repayment_facility_id ';
        $sql .= '     , a.trade_type ';
        $sql .= '     , a.id                           as trn_sticker_id ';
        $sql .= '     , a.mst_item_unit_id ';
        $sql .= '     , c.is_lowlevel ';
        $sql .= '     , c.item_type ';
        $sql .= '     , a.facility_sticker_no ';
        $sql .= '     , a.hospital_sticker_no ';
        $sql .= '     , a.trn_order_id ';
        $sql .= '     , a.trn_shipping_id ';
        $sql .= '     , a.trn_consume_id ';
        $sql .= '     , g.id                           as sales_id ';
        $sql .= '     , g.claim_price                  as sales_price ';
        $sql .= '     , g.unit_price                   as sales_unit_price ';
        $sql .= '     , g.round                        as sales_round ';
        $sql .= '     , g.gross                        as sales_gross ';
        $sql .= '     , coalesce(g.count , a.quantity) as sales_count ';
        $sql .= '     , h.id                           as tran_id ';
        $sql .= '     , coalesce(h.claim_price,0)      as tran_price ';
        $sql .= '     , h.unit_price                   as tran_unit_price ';
        $sql .= '     , h.round                        as tran_round ';
        $sql .= '     , h.gross                        as tran_gross ';
        $sql .= '     , i.id                           as hospital_stock_id ';
        $sql .= '     , a.mst_department_id            as hospital_department_id ';
        $sql .= '     , k.id                           as center_stock_id ';
        $sql .= '     , j.id                           as center_department_id ';
        $sql .= '     , l.id                           as lowlevel_stock_id ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a ';
        $sql .= '     left join mst_item_units as b ';
        $sql .= '       on b.id = a.mst_item_unit_id ';
        $sql .= '     left join mst_facility_items as c ';
        $sql .= '       on c.id = b.mst_facility_item_id ';
        $sql .= '     left join trn_receivings as d ';
        $sql .= '       on a.trn_receiving_id = d.id ';
        $sql .= '     left join trn_orders as e ';
        $sql .= '       on e.id = a.trn_order_id ';
        $sql .= '     left join mst_departments as f ';
        $sql .= '       on f.id = e.department_id_to ';
        $sql .= '     left join trn_claims as g ';
        $sql .= '       on g.id = a.sale_claim_id ';
        $sql .= '     left join trn_claims as h  ';
        $sql .= '       on h.id = a.buy_claim_id  ';
        $sql .= '     left join trn_stocks as i  ';
        $sql .= '       on i.mst_item_unit_id = b.id  ';
        $sql .= '       and i.mst_department_id = a.mst_department_id  ';
        $sql .= '     left join mst_departments as j  ';
        $sql .= '       on j.mst_facility_id = c.mst_facility_id  ';
        $sql .= '     left join trn_stocks as k  ';
        $sql .= '       on k.mst_item_unit_id = b.id  ';
        $sql .= '       and k.mst_department_id = j.id  ';
        $sql .= '     left join trn_stickers as l  ';
        $sql .= '       on l.mst_item_unit_id = b.id  ';
        $sql .= "       and l.lot_no = ' ' ";
        $sql .= "       and l.trade_type = " . Configure::read('ClassesType.LowLevel');
        $sql .= '   where ';
        $sql .= "     g.id in (" .join(',',$this->request->data['MstRepayment']['id']). ")";
        $sql .  '   order by repayment_facility_id ';

        return $this->TrnSticker->query($sql);
    }


    /*
     * 返却登録結果表示用データ取得
     */
    private function _getResultData($ids){
        $sql  = ' select ';
        $sql .= '       a.id                  as "MstRepayment__id"';
        $sql .= '     , f.work_no             as "MstRepayment__work_no"';
        $sql .= '     , g.name                as "MstRepayment__work_name"';
        $sql .= '     , f.recital             as "MstRepayment__recital"';
        $sql .= "     , to_char(a.claim_date, 'yyyy/mm/dd') ";
        $sql .= '                             as "MstRepayment__claim_date" ';
        $sql .= '     , c.internal_code       as "MstRepayment__internal_code"';
        $sql .= '     , c.item_name           as "MstRepayment__item_name"';
        $sql .= '     , c.item_code           as "MstRepayment__item_code"';
        $sql .= '     , c.standard            as "MstRepayment__standard" ';
        $sql .= '     , c1.dealer_name        as "MstRepayment__dealer_name"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= "         then a.count || b1.unit_name  ";
        $sql .= "         else a.count || b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                       as "MstRepayment__unit_name" ';
        $sql .= "     , ( case when a.hospital_sticker_no = '' then  a.facility_sticker_no else a.hospital_sticker_no end ) ";
        $sql .= '                             as "MstRepayment__sticker_no"';
        $sql .= '     , e.lot_no              as "MstRepayment__lot_no"';
        $sql .= "     , to_char(e.validated_date, 'yyyy/mm/dd') ";
        $sql .= '                             as "MstRepayment__validated_date" ';
        $sql .= '     , ( case a.trade_type ';
        foreach( Configure::read('Classes') as $k => $v){
            $sql .= "        when ". $k ." then '" . $v . "'";
        }
        $sql .= '       end)                  as "MstRepayment__trade_type_name" ';
        $sql .= '     , a.unit_price          as "MstRepayment__unit_price"';
        $sql .= '     , a.count               as "MstRepayment__quantity"';

        $sql .= '   from ';
        $sql .= '     trn_claims as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as c1  ';
        $sql .= '       on c1.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_departments as d  ';
        $sql .= '       on d.id = a.department_id_to  ';
        $sql .= '     left join trn_stickers as e  ';
        $sql .= '       on e.sale_claim_id = a.id  ';
        $sql .= '     left join trn_receivings as f ';
        $sql .= '       on f.id = a.trn_receiving_id ';
        $sql .= '     left join mst_classes as g ';
        $sql .= '       on g.id = f.work_type ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',',$ids) . ')';
        $sql .= '   order by ';
        $sql .= '     a.claim_date ';
        $sql .= '     , a.id  ';
        return $this->TrnClaim->query($sql);
    }

    /*
     * シール読み込み処理
     * 読み込んだシールをチェックして返却に使えるかをチェック
     */
    private function _readStickerCheck($sticker_no){
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.mst_department_id != ' . $this->request->data['MstRepayment']['departmentId'];
        $sql .= '         then 3  ';
        $sql .= '         when a.sale_claim_id is null  ';
        $sql .= '         then 4  ';
        $sql .= '         else 0  ';
        $sql .= '         end ';
        $sql .= '     )                        as status  ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join trn_receivings as b  ';
        $sql .= '       on b.id = a.receipt_id  ';
        $sql .= '   where ';
        $sql .= "     a.hospital_sticker_no = '"  .$sticker_no. "'";
        $sql .= "     or a.facility_sticker_no = '" .$sticker_no. "'";

        $ret = $this->TrnSticker->query($sql);
        if(empty($ret)){
            $ret[0][0]['status'] = 2;
        }

        return $ret;
    }


    /*
     * 印刷用データ取得
     */
    private function getPrintData($header_id = array() , $type){
        if(empty($header_id)) {
            return false;
        }

        $sql  = ' select ';
        $sql .= '       d.facility_name as hospital_name';
        $sql .= '     , a.work_no ';
        $sql .= '     , c.department_name ';
        $sql .= '     , g.facility_name as owner_name';
        $sql .= "     , to_char(a.work_date , 'YYYY年mm月dd日') as work_date";
        $sql .= "     , '〒' || d.zip || '　' || d.address || '\nTel:' || d.tel || '／Fax:' || d.fax";
        $sql .= "       as hospital_address  ";
        $sql .= "     , '〒' || g.zip || '　' || g.address || '\nTel:' || g.tel || '／Fax:' || g.fax";
        $sql .= "       as owner_address  ";
        $sql .= "     , '〒' || o.zip || '　' || o.address || '\nTel:' || o.tel || '／Fax:' || o.fax";
        $sql .= "       as supplier_address  ";
        $sql .= '     , o.facility_name as supplier_name';
        $sql .= '     , f.item_name ';
        $sql .= '     , f.standard ';
        $sql .= '     , f.item_code ';
        $sql .= '     , h.dealer_name';
        $sql .= '     , coalesce(b.hospital_sticker_no, i.hospital_sticker_no) as hospital_sticker_no';
        $sql .= '     , i.lot_no ';
        $sql .= '     , b.recital ';
        $sql .= '     , b.quantity ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when e.per_unit = 1  ';
        $sql .= '         then j.unit_name  ';
        $sql .= "         else j.unit_name || '(' || e.per_unit || k.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                            as unit_name ';
        $sql .= '     , ( ';
        $sql .= '       case i.trade_type ';
        foreach( Configure::read('Classes') as $k => $v){
            $sql .= "        when ". $k ." then '" . $v . "'";
        }
        $sql .= '       end)                      as  type_name';
        $sql .= '     , f.internal_code ';
        $sql .= '     , sum(l.unit_price)   as unit_price';
        $sql .= '     , sum(l.claim_price)  as claim_price';
        $sql .= '     , a.recital as header_recital ';
        $sql .= '     , d.round ';


        $sql .= '     , ( ';
        $sql .= '       case f.tax_type ';
        foreach(Configure::read('FacilityItems.taxes') as $k => $v){
            $sql .= "        when ". $k ." then '" . $v . "'";
        }
        $sql .= "       else '' ";
        $sql .= '       end)                      as  tax_name';

        $sql .= '     , a.printed_date2 ';
        $sql .= "     , a.work_no || '_' || coalesce(g.id , 0) as page_break ";
        $sql .= '   from ';
        $sql .= '     trn_repay_headers as a  ';
        $sql .= '     left join trn_receivings as b  ';
        $sql .= '       on b.trn_repay_header_id = a.id  ';
        $sql .= '     left join mst_departments as c  ';
        $sql .= '       on c.id = b.department_id_from  ';
        $sql .= '     left join mst_facilities as d  ';
        $sql .= '       on d.id = c.mst_facility_id  ';
        $sql .= '     left join mst_item_units as e  ';
        $sql .= '       on e.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as f  ';
        $sql .= '       on f.id = e.mst_facility_item_id  ';
        $sql .= '     left join mst_facilities as g  ';
        $sql .= '       on g.id = f.mst_owner_id  ';
        $sql .= '     left join mst_dealers as h  ';
        $sql .= '       on h.id = f.mst_dealer_id  ';
        $sql .= '     left join trn_stickers as i  ';
        $sql .= '       on i.id = b.trn_sticker_id  ';
        $sql .= '     left join mst_unit_names as j  ';
        $sql .= '       on j.id = e.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as k  ';
        $sql .= '       on k.id = e.per_unit_name_id  ';
        $sql .= '     left join trn_claims as l  ';
        $sql .= '       on l.trn_receiving_id = b.id  ';

        $sql .= '     left join trn_orders as m ';
        $sql .= '       on m.id = i.trn_order_id  ';
        $sql .= '     left join mst_departments as n  ';
        $sql .= '       on n.id = m.department_id_to  ';
        $sql .= '     left join mst_facilities as o  ';
        $sql .= '       on o.id = n.mst_facility_id  ';

        $sql .= '   where a.id in (' . join(',',$header_id) . ')  ';
        $sql .= "     and l.is_stock_or_sale = '".$type."'";
        $sql .= '   group by ';
        $sql .= '     d.facility_name ';
        $sql .= '     , a.work_no ';
        $sql .= '     , c.department_name ';
        $sql .= '     , g.facility_name ';
        $sql .= '     , a.work_date ';
        $sql .= '     , d.zip ';
        $sql .= '     , d.address ';
        $sql .= '     , d.tel ';
        $sql .= '     , d.fax ';
        $sql .= '     , g.zip ';
        $sql .= '     , g.address ';
        $sql .= '     , g.tel ';
        $sql .= '     , g.fax ';
        $sql .= '     , f.item_name ';
        $sql .= '     , f.standard ';
        $sql .= '     , f.tax_type ';
        $sql .= '     , f.item_code ';
        $sql .= '     , h.dealer_name ';
        $sql .= '     , b.hospital_sticker_no ';
        $sql .= '     , i.hospital_sticker_no ';
        $sql .= '     , i.lot_no ';
        $sql .= '     , b.recital ';
        $sql .= '     , b.quantity ';
        $sql .= '     , j.unit_name ';
        $sql .= '     , e.per_unit ';
        $sql .= '     , k.unit_name ';
        $sql .= '     , i.trade_type ';
        $sql .= '     , f.internal_code ';
        $sql .= '     , a.recital ';
        $sql .= '     , d.round ';
        $sql .= '     , g.id  ';

        $sql .= '     , o.zip ';
        $sql .= '     , o.address ';
        $sql .= '     , o.tel ';
        $sql .= '     , o.fax ';
        $sql .= '     , o.facility_name ';
        $sql .= '     , a.printed_date2  ';

        $sql .= '   order by ';
        $sql .= '     a.work_no ';
        $sql .= '     , g.id ';
        $sql .= '     , f.internal_code ';

        return $this->TrnRepayHeader->query($sql);
    }
}
