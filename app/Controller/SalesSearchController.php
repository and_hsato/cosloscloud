<?php
/**
 * SakesSearchController
 * 　売上検索　
 * @version 1.0.0
 * @since 2010/05/18
 */
class SalesSearchController extends AppController {

    /**
     * @var $name
     */
    var $name = 'SalesSearch';
    /**
     * @var array $uses
     */
    var $uses = array(
        'TrnClaim',
        'TrnReceiving',
        'TrnOrder',
        'MstDepartment',
        'MstTransactionConfig',
        'MstFacility',
        'MstItemUnit',
        'MstFacilityItem',
        'MstUserBelonging',
        'MstAccountlist'
    );


    /**
     * @var bool $scaffold
     */
    //var $scaffold;
    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');
    /**
     * @var array $components
     */
    var $components = array('RequestHandler', 'xml','CsvWriteUtils');

    /**
     * @var $paginate
     */
    //var $paginate ;

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    public function index() {
        $this->setRoleFunction(61); //売上検索
        $this->redirect('search');
    }

    public function search() {
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        App::import("sanitize");
        $_SearchResult = array();

        // 勘定科目
        $this->set('accountlists' , $this->MstAccountlist->find('list' ,
                                                     array(
                                                         'conditions' => array(
                                                             'MstAccountlist.mst_facility_id' => $this->Session->read('Auth.facility_id_selected')
                                                             ),
                                                         'order' => array('MstAccountlist.id')
                                                         )
                                                     )
                   );

        //施主取得
        $_donor = $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                         array(Configure::read('FacilityType.donor')) ,
                                         false);
        $this->set('donor', $_donor);

        //得意先取得
        $_facilities = $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                              array(Configure::read('FacilityType.hospital')) ,
                                              true);
        $this->set('facilities', $_facilities);

        $_departments = array();
        //施設が選択されていた場合、部署のプルダウンをセットする
        if(isset($this->request->data['MstFacility']['facilityCode']) && ($this->request->data['MstFacility']['facilityCode'] !== "" )) {
            $_departments = $this->getDepartmentsList($this->request->data['MstFacility']['facilityCode'] , false);
        }

        $this->set('departments', $_departments);

        //検索ボタンが押されたら
        if (isset($this->request->data['IsSearch']) && ($this->request->data['IsSearch'] == '1')) {
            App::import('Sanitize');
            $where = '';
            
            if($this->request->data['TrnClaim']['is_deleted'] != '0'){
                $where .= " and a.is_deleted = false";
            }
            
            if($this->request->data['MstFacility']['facilityCode'] != ''){
                $where .= " and a3.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['facilityCode'])."'";
            }
            
            if ($this->request->data['MstFacilityItem']['internal_code'] != '') {
                $where .= " and c.internal_code LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']) . "%'";
            }
            
            if ($this->request->data['MstFacilityItem']['item_code'] != '') {
                $where .= " and c.item_code LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_code']) . "%'";
            }
            
            if ($this->request->data['MstFacilityItem']['item_name'] != '') {
                $where .= " and c.item_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%'";
            }
            
            if ($this->request->data['MstFacilityItem']['standard'] != '') {
                $where .= " and c.standard LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['standard']) . "%'";
            }
            //JAN(前方一致)
            if ($this->request->data['MstFacilityItem']['jan_code'] != '') {
                $where .= " and c.jan_code LIKE '" . Sanitize::escape($this->request->data['MstFacilityItem']['jan_code']) . "%'";
            }
            if ($this->request->data['MstFacilityItem']['mst_accountlist_id'] != '') {
                $where .= " and c.mst_accountlist_id = " . $this->request->data['MstFacilityItem']['mst_accountlist_id'];
            }
            
            if ($this->request->data['TrnClaim']['claim_date_from'] != '') {
                $where .= " and a.claim_date >= '" . Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnClaim']['claim_date_from']))) . "'";
            }
            
            if ($this->request->data['TrnClaim']['claim_date_to'] != '') {
                $where .= " and a.claim_date < '" . Sanitize::escape(date('Y/m/d', strtotime('1 day', strtotime($this->request->data['TrnClaim']['claim_date_to'])))) . "'";
            }
            
            if ($this->request->data['MstDealer']['dealer_name'] != '') {
                $where .= " and c1.dealer_name LIKE '%" . Sanitize::escape($this->request->data['MstDealer']['dealer_name']) . "%'";
            }
            
            if ($this->request->data['TrnSticker']['lot_no'] != '') {
                $where .= " and d.lot_no LIKE '%" . Sanitize::escape($this->request->data['TrnSticker']['lot_no']) . "%'";
            }
            
            if ($this->request->data['TrnSticker']['hospital_sticker_no'] != '') {
                $where .= " and (a.hospital_sticker_no ILIKE '%" . Sanitize::escape($this->request->data['TrnSticker']['hospital_sticker_no']) . "%'";
                $where .= " or a.facility_sticker_no ILIKE '%" . Sanitize::escape($this->request->data['TrnSticker']['hospital_sticker_no']) . "%')";
            }
            
            if($this->request->data['MstFacility']['departmentCode'] != ''){
                $where .= " and a2.department_code = '".Sanitize::escape($this->request->data['MstFacility']['departmentCode'])."'";
            }
            
            if($this->request->data['MstFacility']['ownerCode'] != ''){
                $where .= " and c4.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['ownerCode'])."'";
            }
            
            $_limit = $this->_getLimitCount();
            $_SearchResult = $this->TrnClaim->query($this->getDetailSQL($where, $_limit));
            $search = true;
            $this->set('search',$search);
        }else{
            // 初期値設定
            $this->request->data['TrnClaim']['claim_date_from'] = date('Y/m/01');
            $this->request->data['TrnClaim']['claim_date_to']   = date('Y/m/t');
        }

        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set('SearchResult', $_SearchResult);
    }

    private function getSearchConditions(){
        $_conditions = array('header' => '', 'detail' => '');
        App::import('Sanitize');

        if($this->request->data['TrnClaim']['is_deleted'] != '0'){
        $_conditions['header'] .= " and a.is_deleted = false";
            $check = true;

        }else{
            $check = false;
        }

        if($this->request->data['TrnClaim']['is_deleted'] != '0'){
            $_conditions['header'] .= " and a.is_deleted = false";
        }

        if($this->request->data['MstFacility']['facilityCode'] != ''){
            $_conditions['header'] .= " and a3.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['facilityCode'])."'";
        }

        if ($this->request->data['MstFacilityItem']['internal_code'] != '') {
            $_conditions['header'] .= " and c.internal_code LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']) . "%'";
        }

        if ($this->request->data['MstFacilityItem']['item_code'] != '') {
            $_conditions['header'] .= " and c.item_code LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_code']) . "%'";
        }

        if ($this->request->data['MstFacilityItem']['item_name'] != '') {
            $_conditions['header'] .= " and c.item_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%'";
        }

        if ($this->request->data['MstFacilityItem']['standard'] != '') {
            $_conditions['header'] .= " and c.standard LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['standard']) . "%'";
        }
        //JAN(前方一致)
        if ($this->request->data['MstFacilityItem']['jan_code'] != '') {
            $_conditions['header'] .= " and c.jan_code LIKE '" . Sanitize::escape($this->request->data['MstFacilityItem']['jan_code']) . "%'";
        }
        if ($this->request->data['MstFacilityItem']['mst_accountlist_id'] != '') {
            $_conditions['header'] .= " and c.mst_accountlist_id = " . $this->request->data['MstFacilityItem']['mst_accountlist_id'];
        }

        if ($this->request->data['TrnClaim']['claim_date_from'] != '') {
            $_conditions['detail'] .= " and a.claim_date >= '" . Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnClaim']['claim_date_from']))) . "'";
            $this->set('date_from',$this->request->data['TrnClaim']['claim_date_from']);
        }

        if ($this->request->data['TrnClaim']['claim_date_to'] != '') {
            $_conditions['detail'] .= " and a.claim_date < '" . Sanitize::escape(date('Y/m/d', strtotime('1 day', strtotime($this->request->data['TrnClaim']['claim_date_to'])))) . "'";
            $this->set('date_to',$this->request->data['TrnClaim']['claim_date_to']);
        }

        if ($this->request->data['MstDealer']['dealer_name'] != '') {
            $_conditions['header'] .= " and c1.dealer_name LIKE '%" . Sanitize::escape($this->request->data['MstDealer']['dealer_name']) . "%'";
        }

        if ($this->request->data['TrnSticker']['lot_no'] != '') {
            $_conditions['header'] .= " and d.lot_no LIKE '%" . Sanitize::escape($this->request->data['TrnSticker']['lot_no']) . "%'";
        }

        if ($this->request->data['TrnSticker']['hospital_sticker_no'] != '') {
            $_conditions['header'] .= " and (a.hospital_sticker_no ILIKE '%" . Sanitize::escape($this->request->data['TrnSticker']['hospital_sticker_no']) . "%'";
            $_conditions['header'] .= " or a.facility_sticker_no ILIKE '%" . Sanitize::escape($this->request->data['TrnSticker']['hospital_sticker_no']) . "%')";
        }

        if($this->request->data['MstFacility']['departmentCode'] != ''){
            $_conditions['header'] .= " and a2.department_code = '".Sanitize::escape($this->request->data['MstFacility']['departmentCode'])."'";
        }

        if($this->request->data['MstFacility']['ownerCode'] != ''){
            $_conditions['detail'] .= " and c4.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['ownerCode'])."'";
        }


        $this->set('check',$check);
        return $_conditions;
    }


    public function confirm() {
        $where = ' and a.id in ( ' .join(',',$this->request->data['TrnClaim']['id']). ')';
        $_SearchResult = $this->TrnClaim->query($this->getDetailSQL($where));
        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set('SearchResult', $_SearchResult);
    }

    public function result() {
        $now = date('Y/m/d H:i:s.u');

        //トランザクション
        $this->TrnClaim->begin();
        
        $ids = join(',',$this->request->data['TrnClaim']['id']);
        //行ロック
        $this->TrnClaim->query("select * from trn_claims as a where a.id in ( " . $ids . ") for update  " );
        
        // 必要情報を取得
        $sql  = ' select ';
        $sql .= '   a.id         as "TrnClaim__id" ';
        $sql .= ' , a.count      as "TrnClaim__count" ';
        $sql .= ' , a.unit_price as "TrnClaim__unit_price" ';
        $sql .= ' , a.round      as "TrnClaim__round" ';
        $sql .= ' , a.gross      as "TrnClaim__gross" ';
        $sql .= ' from trn_claims as a ';
        $sql .= ' where id in (' . $ids . ')' ;
        $ret = $this->TrnClaim->query($sql);
        
        foreach($ret as $r ){
            if(isset($this->request->data['TrnClaim']['unit_price'][$r['TrnClaim']['id']])){
                $unit_price = $this->request->data['TrnClaim']['unit_price'][$r['TrnClaim']['id']];
            }else{
                $unit_price = $r['TrnClaim']['unit_price'];
            }

            if(isset($this->request->data['TrnClaim']['retroactive'][$r['TrnClaim']['id']])){
                $is_not_retroactive = true;
            }else{
                $is_not_retroactive = false;
            }
            
            // 請求データ
            $this->TrnClaim->create();
            $result = $this->TrnClaim->updateAll(
                array(
                    'TrnClaim.unit_price'          => $unit_price,
                    'TrnClaim.claim_price'         => $this->getFacilityUnitPrice($r['TrnClaim']['round'], $unit_price , $r['TrnClaim']['count']),
                    'TrnClaim.is_not_retroactive'  => $is_not_retroactive,
                    'TrnClaim.modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'TrnClaim.modified'            => "'" . $now . "'"
                    ),
                array(
                    'TrnClaim.id' => $r['TrnClaim']['id'],
                    ),
                -1
                );
            if(!$result){
                $this->TrnClaim->rollback();
                $this->Session->setFlash('売上編集処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('search');
            }
        }

        //コミット前に更新チェック
        $ret = $this->TrnClaim->query("select count(*) from trn_claims as a where a.id in ( " . $ids . ")  and a.modified <> '" .$now. "' and a.modified > '" . $this->request->data['TrnClaim']['time'] . "'" );
        if($ret[0][0]['count'] > 0){
            // ロールバック
            $this->TrnClaim->rollback();
            $this->Session->setFlash('ほかのユーザによって更新されています。最初からやり直してください', 'growl', array('type'=>'error') );
            $this->redirect('search');
        }
        $this->TrnClaim->commit();
        //表示データを取得
        $_SearchResult = $this->TrnClaim->query($this->getDetailSQL(' and a.id in ( ' . $ids . ')' ));
        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set('SearchResult', $_SearchResult);
        $this->render('result');
    }

    private function getDetailSQL($where , $limit=0) {
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , a.unit_price ';
        $sql .= '     , a.claim_price ';
        $sql .= '     , a.count ';
        $sql .= '     , a.round ';
        $sql .= '     , a.is_deleted ';
        $sql .= '     , a.retroact_execution_flag ';
        $sql .= '     , cast (a.is_not_retroactive as varchar)  as is_not_retroactive ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then b2.unit_name  ';
        $sql .= "         else b2.unit_name || '(' || b.per_unit || b3.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                         as packing_name ';
        $sql .= "     , to_char(a.claim_date , 'YYYY/mm/dd') as claim_date ";
        $sql .= "     , a3.facility_name || ' / ' || a2.department_name  as facility_name";
        $sql .= '     , a4.sales_price ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , c.item_name ';
        $sql .= '     , c.standard ';
        $sql .= '     , c.item_code ';
        $sql .= '     , c1.dealer_name ';
        $sql .= '     , c2.name                                 as accountlists_name ';
        $sql .= '     , c3.const_nm as insurance_claim_department_name ';
        $sql .= '     , coalesce(d2.lot_no, d.lot_no)           as lot_no ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when length(trim(a.hospital_sticker_no)) >0 ';
        $sql .= '         then a.hospital_sticker_no ';
        $sql .= '         when length(trim(d2.hospital_sticker_no)) > 0  ';
        $sql .= '         then d2.hospital_sticker_no  ';
        $sql .= '         when length(d2.facility_sticker_no) > 0  ';
        $sql .= "         then '【' || d2.facility_sticker_no || '】'  ";
        $sql .= '         when length(trim(d.hospital_sticker_no)) > 0  ';
        $sql .= '         then d.hospital_sticker_no  ';
        $sql .= '         when length(d.facility_sticker_no) > 0  ';
        $sql .= "         then '【' || d.facility_sticker_no || '】'  ";
        $sql .= '         end ';
        $sql .= '     )                                         as hospital_sticker_no ';

        $sql .= '     , (  ';
        $sql .= '       case a.claim_type ';
        foreach(Configure::read('ClaimTypes') as $k => $v ){
            $sql .= "   when {$k} then '${v}'";
        }
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                                        as type_name ';

        $sql .= '    , ( ';
        $sql .= '      case  ';
        $sql .= '        when a5.retroact_type in ( 2 , 4 , 5 )  ';
        $sql .= '        then false  ';
        $sql .= '        when a.is_deleted = true  ';
        $sql .= '        then false  ';
        $sql .= '        when a.stocking_close_type <> 0  ';
        $sql .= '        then false  ';
        $sql .= '        when a.facility_close_type <> 0  ';
        $sql .= '        then false  ';
        $sql .= '        else true  ';
        $sql .= '        end ';
        $sql .= '    )                                        as status ';
        $sql .= '   from ';
        $sql .= '     trn_claims as a  ';
        $sql .= '     inner join mst_departments a2  ';
        $sql .= '       on a.department_id_to = a2.id  ';
        $sql .= '     inner join mst_facilities as a3  ';
        $sql .= '       on a2.mst_facility_id = a3.id  ';
        $sql .= '     left join mst_sales_configs as a4  ';
        $sql .= '       on a.mst_item_unit_id = a4.mst_item_unit_id  ';
        $sql .= '       and a2.mst_facility_id = a4.partner_facility_id  ';
        $sql .= '       and a4.start_date <= a.claim_date  ';
        $sql .= '       and a4.end_date > a.claim_date  ';
        $sql .= '     left join trn_retroact_records as a5  ';
        $sql .= '       on a5.id = a.trn_retroact_record_id  ';
        $sql .= '     inner join mst_item_units as b  ';
        $sql .= '       on a.mst_item_unit_id = b.id  ';
        $sql .= '     inner join mst_facility_items as c  ';
        $sql .= '       on b.mst_facility_item_id = c.id  ';
        $sql .= '     left join trn_consumes as d0  ';
        $sql .= '       on d0.id = a.trn_consume_id  ';
        $sql .= '     left join trn_stickers as d  ';
        $sql .= '       on d0.trn_sticker_id = d.id  ';
        $sql .= '     left join trn_receivings as d1  ';
        $sql .= '       on a.trn_receiving_id = d1.id  ';
        $sql .= '     left join trn_stickers as d2  ';
        $sql .= '       on d1.trn_sticker_id = d2.id  ';
        $sql .= '     left join mst_dealers as c1  ';
        $sql .= '       on c.mst_dealer_id = c1.id  ';
        $sql .= '     left join mst_accountlists as c2  ';
        $sql .= '       on c.mst_accountlist_id = c2.id  ';
        $sql .= '     left join mst_consts as c3  ';
        $sql .= '       on c.insurance_claim_type::varchar = c3.const_cd  ';
        $sql .= "       and c3.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
        $sql .= '     left join mst_facilities as c4  ';
        $sql .= '       on c.mst_owner_id = c4.id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b.mst_unit_name_id = b2.id  ';
        $sql .= '     left join mst_unit_names as b3  ';
        $sql .= '       on b.per_unit_name_id = b3.id  ';
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .= "     and a.is_stock_or_sale = '" . Configure::read('Claim.sales') . "'";
        $sql .= $where;
        $sql .= '   order by ';
        $sql .= '     a.id asc ';
        
        if ((integer) $limit > 0) {
            $this->set('max' , $this->getMaxCount($sql , 'TrnClaim'));
            $sql .= " limit {$limit} ";
        }
        
        return $sql;
    }

    public function report(){
        //帳票ヘッダ
        $head = array(
            '請求先名',
            '期間',
            '材料種別',
            '行番号',
            '商品名',
            '規格',
            '製品番号',
            '販売元',
            '部署名',
            '金額',
            '単価',
            '区分',
            '包装単位',
            '課税区分',
            'ロット番号',
            '売上日',
            '商品ID',
            '数量',
            '印刷年月日',
        );

        $cond = $this->getSearchConditions();
        $res = $this->getReports($cond);

        $SearchResult = array();
        $cnt = 1;
        $i = 0;
        $preAcount = null;
        $prename = null;
        $before  = "0";
        foreach($res as $row){
            if($row[0]['金額'] != ""){
                 switch($row[0]['round']){
                    case Configure::read('RoundType.Round'):
                        $row[0]['金額'] = number_format(round($row[0]['金額']));
                        break;
                    case Configure::read('RoundType.Ceil'):
                        $row[0]['金額'] = number_format(ceil($row[0]['金額']));
                        break;
                    case Configure::read('RoundType.Floor'):
                        $row[0]['金額'] = number_format(floor($row[0]['金額']));
                        break;
                }
            }

            if($row[0]['単価'] != ""){
                number_format($row[0]['単価'],2);
            }

            //課税区分設定
            $tax_type = Configure::read('PrintFacilityItems.taxes');
            if(isset($row[0]['課税区分']) && $row[0]['課税区分'] != null){
              $row[0]['課税区分'] = $tax_type[$row[0]['課税区分']];
            }else{
              $row[0]['課税区分'] = "";
            }

            $row[0]['区分'] = $row[0]['管理区分'];
            if($row[0]['管理区分'] != "" && $row[0]['保険請求区分'] != ""){$row[0]['区分'].= "・";}
            $row[0]['区分'].=  $row[0]['保険請求区分'];
            $reports[] = $row[0];

            //請求先もしくは材料種別(勘定科目)が切り替わったらカウントを1からにする
            if( ($prename != null && $prename != $row[0]['請求先名']) || ($preAcount != null && $preAcount != $row[0]['材料種別'])){
                $cnt = 1;
            }


            $SearchResult[$i]['請求先名']   = $row[0]['請求先名'];
            $SearchResult[$i]['期間']       = $this->request->data['TrnClaim']['claim_date_from'] . '～' . $this->request->data['TrnClaim']['claim_date_to'];
            $SearchResult[$i]['材料種別']   = $row[0]['材料種別'];
            $SearchResult[$i]['行番号']     = $cnt;
            $SearchResult[$i]['商品名']     = $row[0]['商品名'];
            $SearchResult[$i]['規格']       = $row[0]['規格'];
            $SearchResult[$i]['製品番号'] = $row[0]['製品番号'];
            $SearchResult[$i]['販売元']     = $row[0]['販売元'];
            $SearchResult[$i]['部署名']     = $row[0]['部署名'];
            $SearchResult[$i]['金額']       = $row[0]['金額'];
            $SearchResult[$i]['単価']       = $row[0]['単価'];
            $SearchResult[$i]['区分']       = $row[0]['区分'];
            $SearchResult[$i]['包装単位']   = $row[0]['包装単位'];
            $SearchResult[$i]['課税区分']   = $row[0]['課税区分'];
            $SearchResult[$i]['ロット番号'] = $row[0]['ロット番号'];
            $SearchResult[$i]['売上日']     = date('Y/m/d', strtotime($row[0]['売上日']));
            $SearchResult[$i]['商品ID']     = $row[0]['商品id'];
            $SearchResult[$i]['数量']       = $row[0]['数量'];
            $SearchResult[$i]['印刷年月日'] = date('Y/m/d');
            $cnt++;
            $i++;

            $prename   = $row[0]['請求先名'];
            $preAcount = $row[0]['材料種別'];
        }

        $this->layout = 'xml/default';
        $this->set('head', $head);
        $this->set('result', $SearchResult);
        $this->render('print');
    }

    private function getReports($cond){
        $sql =
            'select ' .
                'a.unit_price as 単価, ' .
                'a.claim_price as 金額, ' .
                'a.count as 数量, ' .
                'a.is_not_retroactive as 遡及除外, ' .
                'a.claim_date as 売上日, ' .
                'a.round, ' .
                'a2.department_name as 部署名, ' .
                'a3.facility_name as 請求先名, ' .
                'c.internal_code as 商品id, ' .
                'c.item_name as 商品名, ' .
                'c.standard as 規格, ' .
                'c.item_code as 製品番号, ' .
                'c.tax_type as 課税区分, ' .
                "(case when b.per_unit = 1 then b2.unit_name else b2.unit_name || '(' || b.per_unit || b3.unit_name || ')' end) as 包装単位," .
                'c1.dealer_name as 販売元, ' .
                'c2.name as 材料種別, ' .
                'c3.const_nm as 保険請求区分, ' .
                'd.lot_no as ロット番号, ' .
                "(CASE LENGTH(d.hospital_sticker_no) WHEN 0 THEN '【' || d.facility_sticker_no || '】' ELSE d.hospital_sticker_no END ) as 部署シール," .
                '(case d.trade_type ' .
                    " when 1 then '定数'" .
                    " when 2 then '準定数'" .
                    " when 3 then '預託'" .
                    " when 4 then '臨時'" .
                " else '' end) as 管理区分 " .
            'from ' .
                ' trn_claims as a ' .
                ' left join trn_stickers as d on d.sale_claim_id = a.id ' .
                ' inner join mst_departments a2 on a.department_id_to = a2.id ' .
                ' inner join mst_facilities as a3 on a2.mst_facility_id = a3.id ' .
                ' inner join mst_user_belongings as e on e.mst_facility_id = a3.id and e.mst_user_id = ' .$this->request->data['user_id'] . ' and e.is_deleted = false ' .
                ' left join mst_sales_configs as a4 on a.mst_item_unit_id = a4.mst_item_unit_id ' .
                    'and a2.mst_facility_id = a4.partner_facility_id ' .
                    'and a4.start_date <= a.claim_date ' .
                    'and a4.end_date > a.claim_date, ' .
                ' mst_item_units as b, ' .
                ' mst_unit_names as b2,' .
                ' mst_unit_names as b3,' .
                ' mst_facility_items as c ' .
                ' left join mst_dealers as c1 on c.mst_dealer_id = c1.id ' .
                ' left join mst_accountlists as c2 on c.mst_accountlist_id = c2.id ' .
                " left join mst_consts as c3 on c.insurance_claim_type::varchar = c3.const_cd and c3.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'" .
                ' left join mst_facilities as c4 on c.mst_owner_id=c4.id' .
            ' where ' .
                ' a.mst_item_unit_id = b.id ' .
                ' and b.mst_facility_item_id = c.id ' .
                ' and c.mst_facility_id = '.$this->request->data['session'].
                " and a.is_stock_or_sale = '2' " .
                ' and b.mst_unit_name_id = b2.id and b.per_unit_name_id = b3.id '.
                $cond['header'] .
                $cond['detail'] .
            ' order by ' .
                ' 請求先名 asc, 材料種別 asc, 部署名 asc';
         return $this->TrnClaim->query($sql);

    }

    public function export_csv() {
        App::import("sanitize");
        //条件の作成
        $where = "";
        if($this->request->data['TrnClaim']['is_deleted'] != '0'){
            $where .= " and a.is_deleted = false";
        }
        if($this->request->data['TrnClaim']['claim_date_from'] != ''){
            $where .= " and a.claim_date >= '".Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnClaim']['claim_date_from'])))."'";
        }
        if($this->request->data['TrnClaim']['claim_date_to'] != ''){
            $where .= " and a.claim_date < '".Sanitize::escape(date('Y/m/d', strtotime('1 day',strtotime($this->request->data['TrnClaim']['claim_date_to']))))."'";
        }
        if($this->request->data['MstFacilityItem']['internal_code'] != ''){
            $where .= " and c.internal_code LIKE '".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."%'";
        }
        if($this->request->data['MstFacilityItem']['item_code'] != ''){
            $where .= " and c.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        if($this->request->data['MstFacilityItem']['item_name'] != ''){
            $where .= " and c.item_name LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
        }
        if($this->request->data['MstFacilityItem']['standard'] != ''){
            $where .= " and c.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }
        //JAN(前方一致)
        if($this->request->data['MstFacilityItem']['jan_code'] != ''){
            $where .= " and c.jan_code LIKE '".Sanitize::escape($this->request->data['MstFacilityItem']['jan_code'])."%'";
        }
        if($this->request->data['MstFacilityItem']['mst_accountlist_id'] != ''){
            $where .= " and c.mst_accountlist_id = ". $this->request->data['MstFacilityItem']['mst_accountlist_id'];
        }
        
        if($this->request->data['MstDealer']['dealer_name'] != ''){
            $where .= " and c1.dealer_name LIKE '%".Sanitize::escape($this->request->data['MstDealer']['dealer_name'])."%'";
        }
        // 得意先
        if($this->request->data['MstFacility']['facilityCode'] != ''){
            $where .= " and a3.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['facilityCode'])."'";
        }
        // 施主
        if($this->request->data['MstFacility']['ownerCode'] != ''){
            $where .= " and c4.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['ownerCode'])."'";
        }
        if ($this->request->data['TrnSticker']['lot_no'] != '') {
            $where .= " and d.lot_no LIKE '%" . Sanitize::escape($this->request->data['TrnSticker']['lot_no']) . "%'";
        }
        if ($this->request->data['TrnSticker']['hospital_sticker_no'] != '') {
            $where .= " and (d.hospital_sticker_no LIKE '%" . Sanitize::escape($this->request->data['TrnSticker']['hospital_sticker_no']) . "%'";
            $where .= " or d.facility_sticker_no LIKE '%" . Sanitize::escape($this->request->data['TrnSticker']['hospital_sticker_no']) . "%'";
            $where .= " or d2.hospital_sticker_no LIKE '%" . Sanitize::escape($this->request->data['TrnSticker']['hospital_sticker_no']) . "%'";
            $where .= " or d2.facility_sticker_no LIKE '%" . Sanitize::escape($this->request->data['TrnSticker']['hospital_sticker_no']) . "%')";
        }
        // 部署コード
        if($this->request->data['MstFacility']['departmentCode'] != ''){
            $where .= " and a2.department_code = '".Sanitize::escape($this->request->data['MstFacility']['departmentCode'])."'";
        }

        if($this->Session->read('Auth.facility_id_selected') != ''){
            $where .= " and c5.id = " . $this->Session->read('Auth.facility_id_selected');
        }

        $sql  = ' select ';
        $sql .= "       a3.facility_code                     as 得意先コード";
        $sql .= "     , a3.facility_name                     as 得意先名";
        $sql .= "     , a2.department_code                   as 部署コード";
        $sql .= "     , a2.department_name                   as 部署名";
        $sql .= "     , to_char(a.claim_date , 'YYYY/mm/dd') as 売上日 ";
        $sql .= "     , c5.facility_code                     as 施設コード"; 
        $sql .= '     , c.internal_code                      as 商品ID';
        $sql .= '     , c.item_name                          as 商品名';
        $sql .= '     , c.standard                           as 規格';
        $sql .= '     , c.item_code                          as 製品番号';
        $sql .= '     , c1.dealer_name                       as 販売元名';
        $sql .= '     , b2.unit_name                         as 包装単位名';
        $sql .= '     , b.per_unit                           as 包装単位入数';
        $sql .= '     , b3.unit_name                         as 基本単位名';
        $sql .= '     , c2.name                              as 集計科目';
        $sql .= '     , c3.const_nm                          as 保険区分';
        $sql .= '     , c4.facility_code                     as 施主コード';
        $sql .= '     , c4.facility_name                     as 施主名';
        $sql .= '     , a.count                              as 数量';
        $sql .= '     , a.unit_price                         as 単価';
        $sql .= '     , a.claim_price                        as 金額';
        $sql .= '     , a4.sales_price                       as マスタ単価';
        $sql .= '     , c.refund_price                       as 償還価格';
        $sql .= '     , (case c.tax_type' ;
        foreach(Configure::read('FacilityItems.taxes') as $k => $v){
            $sql .= "            when '" . $k . "' then '" . $v . "'" ;
        }
        $sql .= "            else '未設定' end)              as 課税区分";
        $sql .= '     , (  ';
        $sql .= '       case a.claim_type ';
        foreach(Configure::read('ClaimTypes') as $k => $v ){
            $sql .= "   when {$k} then '${v}'";
        }
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                                        as 管理区分';
        $sql .= '     , coalesce(d2.lot_no, d.lot_no)          as ロット番号 ';
        $sql .= '     , d.facility_sticker_no                  as センターシール番号';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when length(trim(a.hospital_sticker_no)) >0 ';
        $sql .= '         then a.hospital_sticker_no ';
        $sql .= '         when length(trim(d2.hospital_sticker_no)) > 0  ';
        $sql .= '         then d2.hospital_sticker_no  ';
        $sql .= '         when length(d2.facility_sticker_no) > 0  ';
        $sql .= "         then '【' || d2.facility_sticker_no || '】'  ";
        $sql .= '         when length(trim(d.hospital_sticker_no)) > 0  ';
        $sql .= '         then d.hospital_sticker_no  ';
        $sql .= '         when length(d.facility_sticker_no) > 0  ';
        $sql .= "         then '【' || d.facility_sticker_no || '】'  ";
        $sql .= '         end ';
        $sql .= '     )                                         as 部署シール番号';
        $sql .= '     , (case when a.is_deleted = true         then 1 end) as 削除フラグ' ;
        $sql .= '     , (case when a.is_not_retroactive = true then 1 end) as 遡及除外' ;

        $sql .= '   from ';
        $sql .= '     trn_claims as a  ';
        $sql .= '     inner join mst_departments a2  ';
        $sql .= '       on a.department_id_to = a2.id  ';
        $sql .= '     inner join mst_facilities as a3  ';
        $sql .= '       on a2.mst_facility_id = a3.id  ';
        $sql .= '     left join mst_sales_configs as a4  ';
        $sql .= '       on a.mst_item_unit_id = a4.mst_item_unit_id  ';
        $sql .= '       and a2.mst_facility_id = a4.partner_facility_id  ';
        $sql .= '       and a4.start_date <= a.claim_date  ';
        $sql .= '       and a4.end_date > a.claim_date  ';
        $sql .= '     left join trn_retroact_records as a5  ';
        $sql .= '       on a5.id = a.trn_retroact_record_id  ';
        $sql .= '     inner join mst_item_units as b  ';
        $sql .= '       on a.mst_item_unit_id = b.id  ';
        $sql .= '     inner join mst_facility_items as c  ';
        $sql .= '       on b.mst_facility_item_id = c.id  ';
        $sql .= '     left join mst_facilities as c5 ';
        $sql .= '       on c5.id = c.mst_facility_id ';
        $sql .= '     left join trn_consumes as d0  ';
        $sql .= '       on d0.id = a.trn_consume_id  ';
        $sql .= '     left join trn_stickers as d  ';
        $sql .= '       on d0.trn_sticker_id = d.id  ';
        $sql .= '     left join trn_receivings as d1  ';
        $sql .= '       on a.trn_receiving_id = d1.id  ';
        $sql .= '     left join trn_stickers as d2  ';
        $sql .= '       on d1.trn_sticker_id = d2.id  ';
        $sql .= '     left join mst_dealers as c1  ';
        $sql .= '       on c.mst_dealer_id = c1.id  ';
        $sql .= '     left join mst_accountlists as c2  ';
        $sql .= '       on c.mst_accountlist_id = c2.id  ';
        $sql .= '     left join mst_consts as c3  ';
        $sql .= '       on c.insurance_claim_type::varchar = c3.const_cd  ';
        $sql .= "       and c3.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
        $sql .= '     left join mst_facilities as c4  ';
        $sql .= '       on c.mst_owner_id = c4.id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b.mst_unit_name_id = b2.id  ';
        $sql .= '     left join mst_unit_names as b3  ';
        $sql .= '       on b.per_unit_name_id = b3.id  ';
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .= "     and a.is_stock_or_sale = '" . Configure::read('Claim.sales') . "'";
        $sql .= $where;
        $sql .= '   order by ';
        $sql .= '     a.claim_date asc ';

        $this->db_export_csv($sql , "売上一覧", 'index');
        
    }
    
    //メッカルデータExcel出力
    function export_excel(){
        $sql  = ' select ';
        $sql .= "       to_char(a.claim_date, 'YYYYMM') as 年月 ";
        $sql .= "     , 'アステムメディカル'            as 仕入先 ";
        $sql .= '     , d.dealer_name                   as ﾒｰｶｰ名 ';
        $sql .= '     , c.item_name                     as 商品名 ';
        $sql .= '     , c.standard                      as 規格 ';
        $sql .= '     , c.item_code                     as 商品ｺｰﾄﾞ ';
        $sql .= '     , c.jan_code                      as "JANｺｰﾄﾞ" ';
        $sql .= '     , c.internal_code                 as 院内ｺｰﾄﾞ ';
        $sql .= '     , b1.unit_name                    as 単位 ';
        $sql .= '     , b.per_unit                      as 入数 ';
        $sql .= '     , c.unit_price                    as 定価単価 ';
        $sql .= '     , sum(a.count)                    as 購入数 ';
        $sql .= '     , a.unit_price                    as 購入単価 ';
        $sql .= '     , sum(a.claim_price)              as 購入金額 ';
        $sql .= '   from ';
        $sql .= '     trn_claims as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as d  ';
        $sql .= '       on d.id = c.mst_dealer_id  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';

        if($this->request->data['TrnClaim']['claim_date_from'] != ''){
            $sql .= " and a.claim_date >= '".Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnClaim']['claim_date_from'])))."'";
        }
        if($this->request->data['TrnClaim']['claim_date_to'] != ''){
            $sql .= " and a.claim_date < '".Sanitize::escape(date('Y/m/d', strtotime('1 day',strtotime($this->request->data['TrnClaim']['claim_date_to']))))."'";
        }
        
        $sql .= '     and a.center_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= "     and a.is_stock_or_sale = '" . Configure::read('Claim.sales') . "' ";
        $sql .= '   group by ';
        $sql .= "     to_char(a.claim_date, 'YYYYMM') ";
        $sql .= '     , d.dealer_name ';
        $sql .= '     , c.item_name ';
        $sql .= '     , c.standard ';
        $sql .= '     , c.item_code ';
        $sql .= '     , c.jan_code ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , b1.unit_name ';
        $sql .= '     , b.per_unit ';
        $sql .= '     , c.unit_price ';
        $sql .= '     , a.count ';
        $sql .= '     , a.unit_price ';
        $sql .= '     , a.claim_price  ';
        $sql .= '   order by ';
        $sql .= '     c.internal_code ';
        $sql .= '     , b.per_unit ';

        $ret = $this->TrnClaim->query($sql);
        
        //テンプレートファイルフルパス
        $template = realpath(TMP);
        $template .= DS . 'excel' . DS;
        $template_path = $template . "template11.xls";
        
        // Excelオブジェクト作成
        $PHPExcel = $this->createExcelObj($template_path);

        //表紙への入力
        //シートの設定
        $PHPExcel->setActiveSheetIndex(0);  //一番左のシート
        $sheet = $PHPExcel->getActiveSheet();
        // Excel書込処理
        foreach($ret as $num => $r){
            $cell =  (string)($num+2);
            $sheet->setCellValue("A" . $cell, $r[0]['年月']);     // 年月
            $sheet->setCellValue("B" . $cell, $r[0]['仕入先']);   // 仕入先
            $sheet->setCellValue("C" . $cell, $r[0]['ﾒｰｶｰ名']);   // メーカー名
            $sheet->setCellValue("D" . $cell, $r[0]['商品名']);   // 商品名
            $sheet->setCellValue("E" . $cell, $r[0]['規格']);     // 規格
            $sheet->setCellValue("F" . $cell, $r[0]['商品ｺｰﾄﾞ']); // 製品番号
            $sheet->setCellValueExplicit("G" . $cell, $r[0]['JANｺｰﾄﾞ'] , PHPExcel_Cell_DataType::TYPE_STRING);  // JANコード
            $sheet->setCellValueExplicit("H" . $cell, $r[0]['院内ｺｰﾄﾞ'] , PHPExcel_Cell_DataType::TYPE_STRING); // 院内コード
            $sheet->setCellValue("I" . $cell, $r[0]['単位']);     // 単位
            $sheet->setCellValue("J" . $cell, $r[0]['入数']);     // 入数
            $sheet->setCellValue("K" . $cell, $r[0]['定価単価']); // 定価単価
            $sheet->setCellValue("L" . $cell, $r[0]['購入数']);   // 購入数
            $sheet->setCellValue("M" . $cell, $r[0]['購入単価']); // 購入単価
            $sheet->setCellValue("N" . $cell, $r[0]['購入金額']); // 購入金額
        }
        
        //保存ファイル名
        $filename = "メッカルデータ_"  . date('YmdHis')  . ".xls";

        $filename = mb_convert_encoding($filename, 'sjis', 'utf-8');
        
        // 保存ファイルパス
        $upload = realpath( TMP );
        $upload .= DS . '..' . DS . 'webroot' . DS . 'rfp' . DS;
        $path = $upload . $filename;
        
        $objWriter = new PHPExcel_Writer_Excel5( $PHPExcel );   //2003形式で保存
        $objWriter->save( $path );
        // 保存したExcelをダウンロード
        $result = file_get_contents(WWW_ROOT . DS . 'rfp' . DS . $filename);
        
        Configure::write('debug',0);
        header("Content-disposition: attachment; filename=" . $filename);
        header("Content-type: application/octet-stream; name=" . $filename);
        print($result);
        unlink(WWW_ROOT . DS . 'rfp' . DS . $filename);
        exit;
    }
}
