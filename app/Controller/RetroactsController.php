<?php
/**
 * 遡及 Controller
 * @version 1.0.0
 * @since 2010/09/06
 */
App::import('Sanitize');
class RetroactsController extends AppController {

    /**
     *
     * @var string $name
     */
    var $name = 'Retroacts';

    /**
     *
     * @var array $uses
     */
    var $uses = array(
        'MstFacility',
        'MstItemUnit',
        'MstUser',
        'MstUserBelonging',
        'Supplier',
        'Hospital',
        'Center',
        'Owner',
        'MstClass',
        'TrnClaim',
        'TrnRetroactHeader',
        'TrnRetroactRecord',
        'MstDepartment',
        'MstDealer',
        'MstTransactionConfig',
        'MstSalesConfig',
        'MstFacilityItem',
        'TrnSticker',
        'MstFacilityRelation',
        'TrnCloseHeader',
        'TrnReceiving',
        'TrnConsume'
        );


    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','RetroactUtils','CsvWriteUtils','CsvReadUtils');

    /**
     *
     * @var array errors
     */
    var $errors;

    /**
     *
     * @var integer
     */
    var $RETROACT_TYPE_STOCK   = 1;
    var $RETROACT_TYPE_SALES   = 2;
    var $RETROACT_TYPE_MSSTOCK = 3;
    var $EDIT_PAGE_LIMIT = 10;

    /**
     * 定数：作業区分
     * Enter description here ...
     * @var unknown_type
     */
    const DEFAULT_WORK_CLASSES_RETROACT = 24;

    var $work_type    = self::DEFAULT_WORK_CLASSES_RETROACT;
    var $center_type  = 1;
    var $hospital_type= 2;

    //ログインなしでの印刷を許可
    public function beforeFilter(){
        parent::beforeFilter();
        //CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    /**
     * index: function of history(). grazie!
     */
    function index () {
        $this->history ();
    }

    /**
     * 遡及履歴
     */
    function history(){
        $this->setRoleFunction(69); //遡及履歴
        App::import("sanitize");    //サニタイズ用
        $result = array();
        
        //検索ボタン押下
        if(isset($this->request->data['search_options']) && $this->request->data['search_options'] == '1'){
            $center_dept_id = null;
            $supplier_id = null;
            $hospital_id = null;

            //センターの部署ID取得
            $center_dept_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'),
                                                     array(Configure::read('DepartmentType.warehouse'))
                                                     );
            
            //仕入先の部署id取得
            if(!empty($this->request->data['TrnRetroactRecord']['department_code_from'])){
                $supplier_id = $this->getFacilityId($this->request->data['TrnRetroactRecord']['department_code_from'],
                                                    array(Configure::read('FacilityType.supplier'))
                                                    );
            }

            //得意先の部署ID取得
            if(!empty($this->request->data['TrnRetroactRecord']['department_code_to'])){
                $supplier_id = $this->getFacilityId($this->request->data['TrnRetroactRecord']['department_code_to'],
                                                    array(Configure::read('FacilityType.hospital'))
                                                    );
            }

            //画面表示用データ取得
            $sql  = 'SELECT TrnRetroactHeader.id           as "TrnRetroactHeader__id"';
            $sql .= '      ,TrnRetroactHeader.work_no      as "TrnRetroactHeader__work_no"';
            $sql .= "      ,( CASE TrnRetroactHeader.retroact_type";
            foreach(Configure::read('Retroacts') as $k=>$v){
                $sql .= "           WHEN '".$k."'   THEN '".$v."' ";
            }
            $sql .= '       END )                          as "TrnRetroactHeader__retroact_type"';
            $sql .= "      ,TO_CHAR(TrnRetroactHeader.work_date,'YYYY/MM/DD') ";
            $sql .= '                                      as "TrnRetroactHeader__work_date"';
            $sql .= '      ,MstFacility.facility_name      as "MstFacility__facility_name"';
            $sql .= '      ,MstUser.user_name              as "MstUser__user_name"';
            $sql .= "      ,TO_CHAR(TrnRetroactHeader.created,'YYYY/MM/DD') ";
            $sql .= '                                      as "TrnRetroactHeader__created"';
            $sql .= '      ,TrnRetroactHeader.recital      as "TrnRetroactHeader__recital"';
            $sql .= "      ,COUNT(TrnRetroactRecord.id) || '/' || TrnRetroactHeader.detail_count ";
            $sql .= '                                      as "TrnRetroactHeader__count"';
            $sql .= "  FROM trn_retroact_headers AS TrnRetroactHeader";
            $sql .= "  LEFT JOIN mst_facilities AS MstFacility";
            $sql .= "    ON TrnRetroactHeader.mst_facility_id = MstFacility.id";
            $sql .= "  LEFT JOIN mst_users AS MstUser";
            $sql .= "    ON CAST(TrnRetroactHeader.creater AS INTEGER) = MstUser.id";
            $sql .= "  LEFT JOIN trn_retroact_records AS TrnRetroactRecord";
            $sql .= "    ON TrnRetroactHeader.id = TrnRetroactRecord.trn_retroact_header_id";
            $sql .= "  LEFT JOIN mst_item_units AS MstItemUnit";
            $sql .= "    ON TrnRetroactRecord.mst_item_unit_id = MstItemUnit.id";
            $sql .= "   AND MstItemUnit.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
            $sql .= "  LEFT JOIN mst_facility_items AS MstFacilityItem";
            $sql .= "    ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id";
            $sql .= "  LEFT JOIN mst_dealers AS MstDealer";
            $sql .= "    ON MstFacilityItem.mst_dealer_id = MstDealer.id";
            $sql .= " WHERE TrnRetroactHeader.is_deleted = false";

            //作業番号
            if(!empty($this->request->data['TrnRetroactHeader']['work_no'])){
                $sql .= " AND TrnRetroactHeader.work_no = '".Sanitize::escape($this->request->data['TrnRetroactHeader']['work_no'])."'";
            }
            //計上日
            if(!empty($this->request->data['TrnRetroactHeader']['start_date'])){
                $sql .= " AND TrnRetroactHeader.work_date >= '".Sanitize::escape($this->request->data['TrnRetroactHeader']['start_date'])."'";
            }
            if(!empty($this->request->data['TrnRetroactHeader']['end_date'])){
                $sql .= " AND TrnRetroactHeader.work_date <= '".Sanitize::escape($this->request->data['TrnRetroactHeader']['end_date'])."'";
            }
            //仕入先
            if(!empty($supplier_id)){
                $sql .= " AND TrnRetroactHeader.mst_facility_id = ".$supplier_id;
            }
            //得意先
            if(!empty($hospital_id)){
                $sql .= " AND TrnRetroactHeader.mst_facility_id = ".$hospital_id;
            }
            //仕入先、得意先共に未指定の場合、自センターの取引のみを表示
            if(empty($supplier_id) && empty($hospital_id)){
                $sql .= " AND (TrnRetroactRecord.department_id_from = ".$center_dept_id;
                $sql .= "  OR  TrnRetroactRecord.department_id_to = ".$center_dept_id.")";
            }
            //商品ID
            if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
                $sql .= " AND MstFacilityItem.internal_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."%'";
            }
            //商品名
            if(!empty($this->request->data['MstFacilityItem']['item_name'])){
                $sql .= " AND MstFacilityItem.item_name ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
            }
            //規格
            if(!empty($this->request->data['MstFacilityItem']['standard'])){
                $sql .= " AND MstFacilityItem.standard ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
            }
            //製品番号
            if(!empty($this->request->data['MstFacilityItem']['item_code'])){
                $sql .= " AND MstFacilityItem.item_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
            }
            //販売元
            if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
                $sql .= " AND MstDealer.dealer_name ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name'])."%'";
            }
            //JANコード
            if(!empty($this->request->data['MstFacilityItem']['jan_code'])){
                $sql .= " AND MstFacilityItem.jan_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['jan_code'])."%'";
            }
            //作業区分
            if(!empty($this->request->data['TrnRetroactRecord']['work_type'])){
                $sql .= " AND TrnRetroactHeader.work_type = ".$this->request->data['TrnRetroactRecord']['work_type'];
            }
            //遡及種別
            if(!empty($this->request->data['TrnRetroactRecord']['retroact_type'])){
                $sql .= " AND TrnRetroactHeader.retroact_type = ".$this->request->data['TrnRetroactRecord']['retroact_type'];
            }

            //操作可能病院で絞り込む
            $sql .='   and ( ';
            $sql .='      mstfacility.facility_type != ' . Configure::read('FacilityType.hospital');
            $sql .='      or mstfacility.id in ( ';
            $sql .='        select';
            $sql .='              a.mst_facility_id ';
            $sql .='          from';
            $sql .='            mst_user_belongings as a ';
            $sql .='          where';
            $sql .='            a.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .='            and a.is_deleted  = false ';
            $sql .='      )';
            $sql .='    ) ';
            $sql .= " GROUP BY ";
            $sql .= "       TrnRetroactHeader.id";
            $sql .= "      ,TrnRetroactHeader.work_no";
            $sql .= "      ,TrnRetroactHeader.retroact_type";
            $sql .= "      ,TrnRetroactHeader.work_date";
            $sql .= "      ,MstFacility.facility_name";
            $sql .= "      ,MstUser.user_name";
            $sql .= "      ,TrnRetroactHeader.created";
            $sql .= "      ,TrnRetroactHeader.recital";
            $sql .= "      ,TrnRetroactHeader.detail_count";
            $sql .= " ORDER BY ";
            $sql .= "       TrnRetroactHeader.work_no DESC";
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnRetroactHeader') );
            if ($this->_getLimitCount() > 0) {
                $sql .= " LIMIT ".$this->_getLimitCount();
            }

            $result = $this->TrnRetroactHeader->query($sql);

        }else{
            //初期処理
            $this->request->data['TrnRetroactHeader']['start_date'] = date("Y/m/d",mktime(0, 0, 0, date("m")  , date("d")-7, date("Y")));
            $this->request->data['TrnRetroactHeader']['end_date'] = date('Y/m/d');
        }
        
        $this->set('RetroactHeaders', $result);
        $this->__renderSelectBoxes();
        $this->render('history');
    }

    /**
     * Rendering select options for view.
     * @access private
     */
    private function __renderSelectBoxes () {
        // 仕入先一覧取得
        $this->set('suppliers',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected'),
                       array(Configure::read('FacilityType.supplier')),
                       false
                       )
                   );
        // 病院一覧取得
        $this->set('customers',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected'),
                       array(Configure::read('FacilityType.hospital')),
                       false
                       )
                   );

        // 作業区分取得
        $this->set('work_types', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),24));
        // 遡及種別取得
        $this->set('retroact_types', Configure::read('Retroacts'));
    }

    /**
     * 遡及履歴からの変更(取消)
     */
    function edit(){
        if(isset($this->request->data['TrnRetroactHeader'])){
            $this->Session->write('Retroact.header',$this->request->data);
        }else{
            if($this->Session->check('Retroact.header')){
                $this->request->data = $this->Session->read('Retroact.header');
            }
        }
        $this->__renderSelectBoxes ();
        $params = $retroacts = array();
        $this->TrnRetroactHeader->unbindModel(array('hasMany'=>array('TrnRetroactRecord')));
        $params['fields'] = array(
            'TrnRetroactHeader.id',
            'TrnRetroactHeader.work_no',
            'to_char("TrnRetroactHeader"."work_date",\'YYYY/mm/dd\') as "TrnRetroactHeader__work_date"',
            'TrnRetroactHeader.retroact_type',
            'TrnRetroactHeader.work_type',
            'TrnRetroactHeader.recital',
            'TrnRetroactHeader.detail_count',
            'TrnRetroactHeader.modified',
            'to_char("TrnRetroactHeader"."start_date",\'YYYY/mm/dd\') as "TrnRetroactHeader__start_date"',
            'to_char("TrnRetroactHeader"."end_date",\'YYYY/mm/dd\') as "TrnRetroactHeader__end_date"',
            'TrnRetroactRecord.id',
            'TrnRetroactRecord.retroact_type',
            'TrnRetroactRecord.retroact_price',
            'MstFacilityItem.internal_code',
            'MstFacilityItem.item_name',
            'MstFacilityItem.item_code',
            'MstFacilityItem.standard',
            'MstFacilityItem.jan_code',
            'MstDealer.dealer_name',
            '(case when "MstItemUnit"."per_unit" = 1 then "MstUnitName"."unit_name" else "MstUnitName"."unit_name" || \'(\' || "MstItemUnit"."per_unit" || "PerUnitName"."unit_name" || \')\' end ) as "MstFacilityItem__unit_name"',
            'Supplier.facility_name',
            'Hospital.facility_name',
            'MstClass.name'
            );

        $params['joins'][] = array(
            'table' => 'trn_retroact_records',
            'alias' => 'TrnRetroactRecord',
            'type' => 'INNER',
            'conditions' => array(
                'TrnRetroactRecord.trn_retroact_header_id = TrnRetroactHeader.id',
                'TrnRetroactRecord.is_deleted = FALSE'
                )
            );
        $page_params['joins'][] = array(
            'table' => 'trn_retroact_headers',
            'alias' => 'TrnRetroactHeader',
            'type' => 'INNER',
            'conditions' => array(
                'TrnRetroactHeader.id = TrnRetroactRecord.trn_retroact_header_id',
                'TrnRetroactHeader.is_deleted = FALSE'
                )
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_item_units',
            'alias' => 'MstItemUnit',
            'type' => 'INNER',
            'conditions' => array(
                'MstItemUnit.id = TrnRetroactRecord.mst_item_unit_id',
                )
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_facility_items',
            'alias' => 'MstFacilityItem',
            'type' => 'INNER',
            'conditions' => array(
                'MstFacilityItem.id = MstItemUnit.mst_facility_item_id',
                )
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_dealers',
            'alias' => 'MstDealer',
            'type' => 'LEFT',
            'conditions' => array(
                'MstDealer.id = MstFacilityItem.mst_dealer_id',
                'MstDealer.is_deleted = FALSE'
                )
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_unit_names',
            'alias' => 'MstUnitName',
            'type' => 'inner',
            'conditions' => array('MstUnitName.id = MstItemUnit.mst_unit_name_id')
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_unit_names',
            'alias' => 'PerUnitName',
            'type' => 'inner',
            'conditions' => array('PerUnitName.id = MstItemUnit.per_unit_name_id')
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_facilities',
            'alias' => 'Supplier',
            'type' => 'left',
            'conditions' => array(
                'Supplier.id = TrnRetroactHeader.mst_facility_id',
                'Supplier.facility_type'=>Configure::read('FacilityType.supplier')
                )
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_facilities',
            'alias' => 'Hospital',
            'type' => 'left',
            'conditions' => array(
                'Hospital.id = TrnRetroactHeader.mst_facility_id',
                'Hospital.facility_type'=>Configure::read('FacilityType.hospital')
                )
            );

        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_classes',
            'alias' => 'MstClass',
            'type' => 'left',
            'conditions' => array(
                'MstClass.id = TrnRetroactHeader.work_type'
                )
            );


        $params['conditions']['TrnRetroactHeader.id'] = $this->request->data['TrnRetroactHeader']['id'];
        $params['conditions']['TrnRetroactHeader.is_deleted'] = false;
        if(isset($this->request->data['TrnRetroactRecord']['retroact_type']) && $this->request->data['TrnRetroactRecord']['retroact_type'] != ''){
            $params['conditions']['TrnRetroactRecord.retroact_type'] = $this->request->data['TrnRetroactRecord']['retroact_type'];
        }
        if(isset($this->request->data['TrnRetroactRecord']['department_code_from']) && $this->request->data['TrnRetroactRecord']['department_code_from'] != ''){
            $_aDepartmentIdFrom = $this->RetroactUtils->getSupplierDepartmentIdWithFacilityCode($this->request->data['TrnRetroactRecord']['department_code_from']);
            $params['conditions']['TrnRetroactRecord.department_id_from'] = $_aDepartmentIdFrom;
        }
        // for TrnRetroactRecord.
        if(isset($this->request->data['TrnRetroactRecord']['department_code_to']) && $this->request->data['TrnRetroactRecord']['department_code_to'] != ''){
            $_aDepartmentIdTo = $this->RetroactUtils->getHospitalDepartmentIdWithFacilityCode($this->request->data['TrnRetroactRecord']['department_code_to']);
            $params['conditions']['TrnRetroactRecord.department_id_to'] = $_aDepartmentIdTo;
        }
        if(isset($this->request->data['TrnRetroactRecord']['work_type']) && $this->request->data['TrnRetroactRecord']['work_type'] != ''){
            $params['conditions']['TrnRetroactHeader.work_type'] = $this->request->data['TrnRetroactRecord']['work_type'];
        }
        if(isset($this->request->data['TrnRetroactHeader']['retroact_type']) && $this->request->data['TrnRetroactHeader']['retroact_type'] != ''){
            $params['conditions']['TrnRetroactHeader.retroact_type'] = $this->request->data['TrnRetroactHeader']['retroact_type'];
        }
        if(isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] != ''){
            $params['conditions']['MstFacilityItem.internal_code'] = $this->request->data['MstFacilityItem']['internal_code'];
        }
        if(isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] != ''){
            $params['conditions']['MstFacilityItem.item_code LIKE '] = '%'.Sanitize::escape($this->request->data['MstFacilityItem']['item_code']).'%';
        }
        if(isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] != ''){
            $params['conditions']['MstFacilityItem.item_name LIKE '] = '%'.Sanitize::escape($this->request->data['MstFacilityItem']['item_name']).'%';
        }
        if(isset($this->request->data['MstFacilityItem']['dealer_name']) && $this->request->data['MstFacilityItem']['dealer_name'] != ''){
            $params['conditions']['MstDealer.dealer_name LIKE '] = '%'.Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name']).'%';
        }
        if(isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] != ''){
            $params['conditions']['MstFacilityItem.standard LIKE '] = '%'.Sanitize::escape($this->request->data['MstFacilityItem']['standard']).'%';
        }
        if(isset($this->request->data['MstFacilityItem']['jan_code']) && $this->request->data['MstFacilityItem']['jan_code'] != ''){
            $params['conditions']['MstFacilityItem.jan_code'] = Sanitize::escape($this->request->data['MstFacilityItem']['jan_code']);
        }

        $params['recursive'] = -1;
        $params['limit'] = (int)$this->EDIT_PAGE_LIMIT;
        $retroactHeader = $this->TrnRetroactHeader->find('all', $params);
        $this->set('retroactHeader',$retroactHeader[0]);

        $retroacts = array();
        $params['conditions']['TrnRetroactRecord.is_deleted'] = false;

        $this->paginate = array(
            'fields'=>$params['fields'],
            'limit'=>(int)$this->EDIT_PAGE_LIMIT,
            'recursive'=>-1,
            'order'=>array('MstFacilityItem.internal_code',"MstItemUnit.id","TrnRetroactRecord.retroact_type"),
            'joins'=>$page_params['joins']
            );//2);
        $tmp_retroacts = $this->paginate('TrnRetroactRecord',$params['conditions']);
        for ($i = 0;$i < count($tmp_retroacts);$i++) {
            if($tmp_retroacts[$i]['TrnRetroactHeader']['id'] != $this->request->data['TrnRetroactHeader']['id']){
                continue;
            }
            $retroacts[$i] = $tmp_retroacts[$i];
        }
        $this->set('retroacts',$retroacts);
        $this->set('retroact_types', Configure::read('RetroactTypeDisplay'));
        $this->render('edit');
    }

    /**
     * 遡及履歴からの変更
     */
    function edit_result(){
        $now = date('Y/m/d H:i:s.u');
        //エラーメッセージ
        $error_msg = "";
        //エラーフラグ
        $is_error = false;

        //画面から渡された遡及ヘッダIDが取消可能なものかチェック
        $sql  = "SELECT TO_CHAR(work_date,'YYYY-MM-DD') AS work_date";
        $sql .= "      ,retroact_type";
        $sql .= "      ,modified";
        $sql .= "  FROM trn_retroact_headers";
        $sql .= " WHERE id = ".$this->request->data['TrnRetroactHeader']['id'];
        $sql .= "   AND is_deleted = false";
        $res = $this->TrnRetroactHeader->query($sql);

        if(!empty($res) && $res[0][0]['retroact_type'] ==  Configure::read('Retroact.change_price') ){
            //価格変更は取消しできない
            $this->Session->setFlash("価格変更は取消しできません。再度価格変更を行ってください。", 'growl', array('type'=>'error') );
            $this->request->data = array();
            $this->history();
            return;
        }

        if(empty($res) || $res[0][0]['work_date'] == ""){
            //対象データ無しのため履歴画面を再表示
            $this->Session->setFlash("取消不可能なデータです", 'growl', array('type'=>'error') );
            $this->request->data = array();
            $this->history();
            return;
        }else{
            //取得した計上日で締めチェック
            if(!$this->canUpdateAfterClose($res[0][0]['work_date'],$this->Session->read("Auth.facility_id_selected"))){
                //仮締め更新権限がない場合は再度条件設定
                $this->Session->setFlash("締めが実施されているため取消できません", 'growl', array('type'=>'error') );
                $this->history();
                return;
            }

            //取得した更新日時で更新チェック
            if($res[0][0]['modified'] > $this->request->data['TrnRetroactHeader']['chk_date']){
                //更新されている場合は再度条件設定
                $this->Session->setFlash("別ユーザによって更新されています", 'growl', array('type'=>'error') );
                $this->history();
                return;
            }
        }

        //トランザクション開始
        $this->TrnRetroactRecord->begin();

        //行ロック
        $this->TrnRetroactHeader->query('select * from trn_retroact_headers as a where a.id = ' . $this->request->data['TrnRetroactHeader']['id'] . ' for update ');
        //更新チェック
        $ret = $this->TrnRetroactHeader->query('select count(*) from trn_retroact_headers as a where a.id = ' . $this->request->data['TrnRetroactHeader']['id'] . " and a.modified > '" . $this->request->data['TrnRetroactHeader']['chk_date'] . "'");
        if($ret[0][0]['count'] > 0){
            $this->Session->setFlash("ほかユーザによって更新されています。最初からやり直してください。", 'growl', array('type'=>'error') );
            $this->history();
            return;
        }

        //遡及ヘッダ更新
        $ret = $this->TrnRetroactHeader->del(array('id'=>$this->request->data['TrnRetroactHeader']['id']));
        if(!$ret) {
            $this->TrnRetroactRecord->rollback();
            $this->Session->setFlash('遡及データ更新中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('history');
        }
        
        //遡及明細更新
        $ret = $this->TrnRetroactRecord->del(array('trn_retroact_header_id'=>$this->request->data['TrnRetroactHeader']['id']));
        if(!$ret) {
            $this->TrnRetroactRecord->rollback();
            $this->Session->setFlash('遡及データ更新中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('history');
        }

        
        $sql  = ' update trn_claims  ';
        $sql .= ' set ';
        $sql .= '   is_deleted = true  ';
        $sql .= ' where ';
        $sql .= '   id in (  ';
        $sql .= '     select ';
        $sql .= '         c.id  ';
        $sql .= '     from ';
        $sql .= '       trn_retroact_headers as a  ';
        $sql .= '       left join trn_retroact_records as b  ';
        $sql .= '         on b.trn_retroact_header_id = a.id  ';
        $sql .= '       left join trn_claims as c  ';
        $sql .= '         on c.trn_retroact_record_id = b.id ';
        $sql .= '     where a.id = ' . $this->request->data['TrnRetroactHeader']['id'];
        $sql .= '   )  ';
        $this->TrnRetroactRecord->query($sql);

        //コミット前に更新チェック
        $ret = $this->TrnRetroactHeader->query('select count(*) from trn_retroact_headers as a where a.id = ' . $this->request->data['TrnRetroactHeader']['id'] . " and a.modified > '" . $this->request->data['TrnRetroactHeader']['chk_date'] . "' and a.modified <> '" . $now . "'");
        if($ret[0][0]['count'] > 0){
            $this->TrnRetroactRecord->rollback();
            $this->Session->setFlash("ほかユーザによって更新されています。最初からやり直してください。", 'growl', array('type'=>'error') );
            $this->history();
            return;
        }
        
        
        //コミット
        $this->TrnRetroactRecord->commit();
        //画面表示用データ取得
        $sql = '';
        $sql .= 'SELECT TrnRetroactHeader.work_no AS "TrnRetroactHeader__work_no"';
        $sql .= "      ,( CASE TrnRetroactHeader.retroact_type";
        $sql .= "              WHEN '".Configure::read('Retroact.stock_retroact')."' THEN '仕入遡及'";
        $sql .= "              WHEN '".Configure::read('Retroact.sales_retroact')."' THEN '売上遡及'";
        $sql .= '        END )                                            AS "TrnRetroactHeader__retroact_type"';
        $sql .= "      ,TO_CHAR(TrnRetroactHeader.work_date,'YYYY/MM/DD') AS ".'"TrnRetroactHeader__work_date"';
        $sql .= '      ,MstFacility.facility_name                         AS "MstFacility__facility_name"';
        $sql .= '      ,MstUser.user_name                                 AS "MstUser__user_name"';
        $sql .= "      ,TO_CHAR(TrnRetroactHeader.created,'YYYY/MM/DD')   AS ".'"TrnRetroactHeader__created"';
        $sql .= '      ,TrnRetroactHeader.recital                         AS "TrnRetroactHeader__recital"';
        $sql .= "      ,COUNT(TrnRetroactRecord.id) || '/' || TrnRetroactHeader.detail_count AS ".'"TrnRetroactHeader__count"';
        $sql .= "  FROM trn_retroact_headers AS TrnRetroactHeader";
        $sql .= " INNER JOIN mst_facilities AS MstFacility";
        $sql .= "    ON TrnRetroactHeader.mst_facility_id = MstFacility.id";
        $sql .= " INNER JOIN mst_users AS MstUser";
        $sql .= "    ON CAST(TrnRetroactHeader.creater AS INTEGER) = MstUser.id";
        $sql .= " INNER JOIN trn_retroact_records AS TrnRetroactRecord";
        $sql .= "    ON TrnRetroactHeader.id = TrnRetroactRecord.trn_retroact_header_id";
        $sql .= " WHERE TrnRetroactHeader.id = ".$this->request->data['TrnRetroactHeader']['id'];
        $sql .= " GROUP BY ";
        $sql .= "       TrnRetroactHeader.work_no";
        $sql .= "      ,TrnRetroactHeader.retroact_type";
        $sql .= "      ,TrnRetroactHeader.work_date";
        $sql .= "      ,MstFacility.facility_name";
        $sql .= "      ,MstUser.user_name";
        $sql .= "      ,TrnRetroactHeader.created";
        $sql .= "      ,TrnRetroactHeader.recital";
        $sql .= "      ,TrnRetroactHeader.detail_count";
        
        $result = $this->TrnRetroactHeader->query($sql);
        $this->Session->setFlash("遡及取消が完了しました",'growl',array('type'=>'star'));
        $this->set("result",$result);
        
        $this->set('date',$this->request->data);
        $this->render('edit_result');
    }
    
    /**
     * 仕入価格変更
     */
    function set_stockprice () {
        $this->setRoleFunction(65); //仕入価格変更
        // 仕入先
        $this->set('suppliers' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('FacilityType.supplier')),
                                                         true
                                                         ));
        // 施主
        $this->set('owners' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                      array(Configure::read('FacilityType.donor')),
                                                      true
                                                      ));


        $work_no = Configure::read('workNoModels');
        $this->set('work_types', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),array_search('TrnRetroactHeader', $work_no)));
        $this->render('set_stockprice');
    }

    function stock_master(){
        // 条件を元に対象件数を取得
        $ret = $this->getClaimData($this->request->data ,  Configure::read('Claim.stock') , true );
        
        $this->set('count' , $ret[0][0]['count']);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Retroact.token' , $token);
        $this->request->data['Retroact']['token'] = $token;
        
    }
    
    function getClaimData($data , $type , $flg){
        $sql  = ' select ';
        if($flg){
            $sql .= '       count(*) as count  ';
        }else{
            $sql .= '      a.id';
            $sql .= '    , a.count';
            $sql .= '    , a.mst_item_unit_id';
            if($type == Configure::read('Claim.stock') ){
                $sql .= '    , g.transaction_price as price';
            }elseif($type == Configure::read('Claim.sales')){
                $sql .= '    , g.sales_price as price';
            }elseif($type == Configure::read('Claim.ms_stock')){
                $sql .= '    , g.ms_transaction_price as price';
            }
            $sql .= '    , f.round';
            $sql .= '    , e.id as department_id_from ';
            $sql .= '    , c1.id as department_id_to ';
            $sql .= '    , a.claim_price ';
            $sql .= '    , a.unit_price ';
        }
        $sql .= '   from ';
        $sql .= '     trn_claims as a ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_departments as c1 ';
        $sql .= '       on c1.mst_facility_id = c.mst_facility_id';
        $sql .= '     left join mst_facilities as d  ';
        $sql .= '       on d.id = c.mst_owner_id  ';
        $sql .= '     left join mst_departments as e  ';
        if($type == Configure::read('Claim.stock') || $type == Configure::read('Claim.ms_stock')){
            $sql .= '       on e.id = a.department_id_from  ';
        }elseif($type == Configure::read('Claim.sales')){
            $sql .= '       on e.id = a.department_id_to  ';
        }
        $sql .= '     left join mst_facilities as f  ';
        $sql .= '       on f.id = e.mst_facility_id  ';
        if($type == Configure::read('Claim.stock') || $type == Configure::read('Claim.ms_stock') ){
            $sql .= '     inner join mst_transaction_configs as g  ';
            $sql .= '       on g.mst_item_unit_id = b.id  ';
            $sql .= '       and g.partner_facility_id = f.id  ';
            $sql .= '       and g.is_deleted = false  ';
        } elseif($type == Configure::read('Claim.sales')) {
            $sql .= '     inner join mst_sales_configs as g  ';
            $sql .= '       on g.mst_item_unit_id = b.id  ';
            $sql .= '       and g.partner_facility_id = f.id  ';
            $sql .= '       and g.is_deleted = false  ';
        }
        $sql .= "       and g.start_date = '".$data['TrnRetroactHeader']['master_start_date']."'";
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= "     and a.is_stock_or_sale = '" . $type . "'";
        $sql .= '     and a.is_not_retroactive = false ';
        $sql .= '     and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= "     and a.claim_date between '" . $data['TrnRetroactHeader']['start_date'] ."' and '" . $data['TrnRetroactHeader']['end_date'] ."'";
        $sql .= "     and d.facility_code = '".$data['TrnRetroactHeader']['ownerCode']."'"; 
        $sql .= "     and f.facility_code = '".$data['TrnRetroactHeader']['facilityCode']."'";
        
        return $this->TrnClaim->query($sql);
    }
    
    function stock_master_result(){
        if($this->request->data['Retroact']['token'] === $this->Session->read('Retroact.token')) {
            $this->Session->delete('Retroact.token');
            $work_no = $this->setWorkNo4Header( date('Ymd') , "24" );
            $now     = date('Y/m/d H:i:s');
            $today   = date('Y/m/d');
            $facility_id = $this->getFacilityId($this->request->data['TrnRetroactHeader']['facilityCode'] ,
                                                Configure::read('FacilityType.supplier')
                                                );
            
            // トランザクション開始
            $this->TrnRetroactHeader->begin();
            
            // 必要情報の取得
            $result = $this->getClaimData($this->request->data ,Configure::read('Claim.stock') , false );
            // 行ロック
            
            // 遡及ヘッダ
            $TrnRetroactHeader = array(
                'work_no'         => $work_no,
                'work_date'       => $today,
                'work_type'       => $this->request->data['TrnRetroactHeader']['work_class'],
                'recital'         => $this->request->data['TrnRetroactHeader']['recital'],
                'retroact_type'   => Configure::read('Retroact.change_price'),
                'mst_facility_id' => $facility_id,
                'start_date'      => $this->request->data['TrnRetroactHeader']['start_date'],
                'end_date'        => $this->request->data['TrnRetroactHeader']['end_date'],
                'is_deleted'      => false,
                'creater'         => $this->Session->read('Auth.MstUser.id'),
                'modifier'        => $this->Session->read('Auth.MstUser.id'),
                'created'         => $now,
                'modified'        => $now,
                'detail_count'    => count($result)
                );
            
            $this->TrnRetroactHeader->create();
            // SQL実行
            if (!$this->TrnRetroactHeader->save($TrnRetroactHeader)) {
                $this->TrnRetroactHeader->rollback();
                $this->Session->setFlash('仕入価格変更処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('set_stockprice');
            }
            $trn_retroact_header_id = $this->TrnRetroactHeader->getLastInsertID();
            
            foreach($result as $r){
                // 遡及明細
                $TrnRetroactRecord = array(
                    'work_no'                => $work_no ,
                    'retroact_type'          => Configure::read('Retroact.change_price'),
                    'department_id_from'     => $r[0]['department_id_from'],
                    'department_id_to'       => $r[0]['department_id_to'],
                    'mst_item_unit_id'       => $r[0]['mst_item_unit_id'],
                    'retroact_price'         => $r[0]['price'],
                    'trn_close_header_id'    => $trn_retroact_header_id,
                    'is_deleted'             => false, 
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'modified'               => $now,
                    'trn_retroact_header_id' => $trn_retroact_header_id
                    );
                $this->TrnRetroactRecord->create();
                // SQL実行
                if (!$this->TrnRetroactRecord->save($TrnRetroactRecord)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('仕入価格変更処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('set_stockprice');
                }
                $trn_retroact_id = $this->TrnRetroactRecord->getLastInsertID();
                
                $this->TrnClaim->create();
                $ret = $this->TrnClaim->updateAll(
                    array(
                        'TrnClaim.unit_price'             => $r[0]['price'],
                        'TrnClaim.claim_price'            => $this->getFacilityUnitPrice($r[0]['round'] , $r[0]['price'] , $r[0]['count']),
                        'TrnClaim.round'                  => $r[0]['round'],
                        'TrnClaim.trn_retroact_record_id' => $trn_retroact_id,
                        'TrnClaim.modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'TrnClaim.modified'               => "'" . $now . "'"
                        ),
                    array(
                        'TrnClaim.id' => $r[0]['id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('仕入価格変更処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('set_stockprice');
                }
            }
            
            // 更新チェック
            
            // コミット
            $this->TrnRetroactHeader->commit();
            $this->Session->setFlash('仕入価格変更が完了しました', 'growl', array('type'=>'star') );
        } else {
        }
    }

    /**
     * MS法人仕入価格変更
     */
    function set_msstockprice () {
        $this->setRoleFunction(70); //MS法人仕入価格変更
        // 仕入先
        $this->set('suppliers' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('FacilityType.supplier')),
                                                         true
                                                         ));
        // 施主
        $this->set('owners' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                      array(Configure::read('FacilityType.donor')),
                                                      true
                                                      ));


        $work_no = Configure::read('workNoModels');
        $this->set('work_types', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),array_search('TrnRetroactHeader', $work_no)));

        $this->render('set_msstockprice');
    }

    function msstock_master(){
        // 条件を元に対象件数を取得
        $ret = $this->getClaimData($this->request->data ,  Configure::read('Claim.ms_stock') , true );
        
        $this->set('count' , $ret[0][0]['count']);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Retroact.token' , $token);
        $this->request->data['Retroact']['token'] = $token;
        
    }

    function msstock_master_result(){
        if($this->request->data['Retroact']['token'] === $this->Session->read('Retroact.token')) {
            $this->Session->delete('Retroact.token');
            $work_no = $this->setWorkNo4Header( date('Ymd') , "24" );
            $now     = date('Y/m/d H:i:s');
            $today   = date('Y/m/d');
            $facility_id = $this->getFacilityId($this->request->data['TrnRetroactHeader']['facilityCode'] ,
                                                Configure::read('FacilityType.supplier')
                                                );
            
            // トランザクション開始
            $this->TrnRetroactHeader->begin();
            
            // 必要情報の取得
            $result = $this->getClaimData($this->request->data ,Configure::read('Claim.ms_stock') , false );
            // 行ロック
            
            // 遡及ヘッダ
            $TrnRetroactHeader = array(
                'work_no'         => $work_no,
                'work_date'       => $today,
                'work_type'       => $this->request->data['TrnRetroactHeader']['work_class'],
                'recital'         => $this->request->data['TrnRetroactHeader']['recital'],
                'retroact_type'   => Configure::read('Retroact.change_price'),
                'mst_facility_id' => $facility_id,
                'start_date'      => $this->request->data['TrnRetroactHeader']['start_date'],
                'end_date'        => $this->request->data['TrnRetroactHeader']['end_date'],
                'is_deleted'      => false,
                'creater'         => $this->Session->read('Auth.MstUser.id'),
                'modifier'        => $this->Session->read('Auth.MstUser.id'),
                'created'         => $now,
                'modified'        => $now,
                'detail_count'    => count($result)
                );
            
            $this->TrnRetroactHeader->create();
            // SQL実行
            if (!$this->TrnRetroactHeader->save($TrnRetroactHeader)) {
                $this->TrnRetroactHeader->rollback();
                $this->Session->setFlash('MS仕入価格変更処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('set_msstockprice');
            }
            $trn_retroact_header_id = $this->TrnRetroactHeader->getLastInsertID();
            
            foreach($result as $r){
                // 遡及明細
                $TrnRetroactRecord = array(
                    'work_no'                => $work_no ,
                    'retroact_type'          => Configure::read('Retroact.change_price'),
                    'department_id_from'     => $r[0]['department_id_from'],
                    'department_id_to'       => $r[0]['department_id_to'],
                    'mst_item_unit_id'       => $r[0]['mst_item_unit_id'],
                    'retroact_price'         => $r[0]['price'],
                    'trn_close_header_id'    => $trn_retroact_header_id,
                    'is_deleted'             => false, 
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'modified'               => $now,
                    'trn_retroact_header_id' => $trn_retroact_header_id
                    );
                $this->TrnRetroactRecord->create();
                // SQL実行
                if (!$this->TrnRetroactRecord->save($TrnRetroactRecord)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('MS仕入価格変更処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('set_msstockprice');
                }
                $trn_retroact_id = $this->TrnRetroactRecord->getLastInsertID();
                
                $this->TrnClaim->create();
                $ret = $this->TrnClaim->updateAll(
                    array(
                        'TrnClaim.unit_price'             => $r[0]['price'],
                        'TrnClaim.claim_price'            => $this->getFacilityUnitPrice($r[0]['round'] , $r[0]['price'] , $r[0]['count']),
                        'TrnClaim.round'                  => $r[0]['round'],
                        'TrnClaim.trn_retroact_record_id' => $trn_retroact_id,
                        'TrnClaim.modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'TrnClaim.modified'               => "'" . $now . "'"
                        ),
                    array(
                        'TrnClaim.id' => $r[0]['id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('MS仕入価格変更処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('set_msstockprice');
                }
            }
            
            // 更新チェック
            
            // コミット
            $this->TrnRetroactHeader->commit();
            $this->Session->setFlash('MS仕入価格変更が完了しました', 'growl', array('type'=>'star') );
        } else {
        }
    }
    
    /**
     * 売上価格変更
     */
    function set_salesprice () {
        $this->setRoleFunction(66); //売上価格変更
        // 施主
        $this->set('owners' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('FacilityType.donor')),
                                                         true
                                                         ));
        // 病院
        $this->set('customers' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('FacilityType.hospital')),
                                                         true
                                                         ));
        $work_no = Configure::read('workNoModels');
        $this->set('work_types', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),array_search('TrnRetroactHeader', $work_no)));
        $this->render('set_salesprice');
    }


    function sales_master(){
        // 条件を元に対象件数を取得
        $ret = $this->getClaimData($this->request->data ,  Configure::read('Claim.sales') , true );
        $this->set('count' , $ret[0][0]['count']);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Retroact.token' , $token);
        $this->request->data['Retroact']['token'] = $token;
    }
    
    function sales_master_result(){
        if($this->request->data['Retroact']['token'] === $this->Session->read('Retroact.token')) {
            $this->Session->delete('Retroact.token');
            
            $work_no = $this->setWorkNo4Header( date('Ymd') , "24" );
            $now     = date('Y/m/d H:i:s');
            $today   = date('Y/m/d');
            $facility_id = $this->getFacilityId($this->request->data['TrnRetroactHeader']['facilityCode'] ,
                                                Configure::read('FacilityType.hospital')
                                                );
            
            // トランザクション開始
            $this->TrnRetroactHeader->begin();
            
            // 必要情報の取得
            $result = $this->getClaimData($this->request->data ,Configure::read('Claim.sales') , false );
            // 行ロック
            
            // 遡及ヘッダ
            $TrnRetroactHeader = array(
                'work_no'         => $work_no,
                'work_date'       => $today,
                'work_type'       => $this->request->data['TrnRetroactHeader']['work_class'],
                'recital'         => $this->request->data['TrnRetroactHeader']['recital'],
                'retroact_type'   => Configure::read('Retroact.change_price'),
                'mst_facility_id' => $facility_id,
                'start_date'      => $this->request->data['TrnRetroactHeader']['start_date'],
                'end_date'        => $this->request->data['TrnRetroactHeader']['end_date'],
                'is_deleted'      => false,
                'creater'         => $this->Session->read('Auth.MstUser.id'),
                'modifier'        => $this->Session->read('Auth.MstUser.id'),
                'created'         => $now,
                'modified'        => $now,
                'detail_count'    => count($result)
                );
            
            $this->TrnRetroactHeader->create();
            // SQL実行
            if (!$this->TrnRetroactHeader->save($TrnRetroactHeader)) {
                $this->TrnRetroactHeader->rollback();
                $this->Session->setFlash('売上価格変更処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('set_salesprice');
            }
            $trn_retroact_header_id = $this->TrnRetroactHeader->getLastInsertID();
            
            foreach($result as $r){
                // 遡及明細
                $TrnRetroactRecord = array(
                    'work_no'                => $work_no ,
                    'retroact_type'          => Configure::read('Retroact.change_price'),
                    'department_id_from'     => $r[0]['department_id_from'],
                    'department_id_to'       => $r[0]['department_id_to'],
                    'mst_item_unit_id'       => $r[0]['mst_item_unit_id'],
                    'retroact_price'         => $r[0]['price'],
                    'trn_close_header_id'    => $trn_retroact_header_id,
                    'is_deleted'             => false, 
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'modified'               => $now,
                    'trn_retroact_header_id' => $trn_retroact_header_id
                    );
                $this->TrnRetroactRecord->create();
                // SQL実行
                if (!$this->TrnRetroactRecord->save($TrnRetroactRecord)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('売上価格変更処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('set_salesprice');
                }
                $trn_retroact_id = $this->TrnRetroactRecord->getLastInsertID();
                
                $this->TrnClaim->create();
                $ret = $this->TrnClaim->updateAll(
                    array(
                        'TrnClaim.unit_price'             => $r[0]['price'],
                        'TrnClaim.claim_price'            => $this->getFacilityUnitPrice($r[0]['round'] , $r[0]['price'] , $r[0]['count']),
                        'TrnClaim.round'                  => $r[0]['round'],
                        'TrnClaim.trn_retroact_record_id' => $trn_retroact_id,
                        'TrnClaim.modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'TrnClaim.modified'               => "'" . $now . "'"
                        ),
                    array(
                        'TrnClaim.id' => $r[0]['id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('売上価格変更処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('set_salesprice');
                }
            }
            
            // 更新チェック
            
            // コミット
            $this->TrnRetroactHeader->commit();
            $this->Session->setFlash('売上価格変更が完了しました', 'growl', array('type'=>'star') );
        }else{
            
        }
    }
        
    /**
     * 仕入赤黒遡及
     * 初期画面
     */
    function stock(){
        $this->setRoleFunction(67); //仕入遡及

        // 仕入先
        $this->set('suppliers' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('FacilityType.supplier')),
                                                         true
                                                         ));
        // 施主
        $this->set('owners' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                      array(Configure::read('FacilityType.donor')),
                                                      true
                                                      ));

        $work_no = Configure::read('workNoModels');
        $this->set('work_types', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),array_search('TrnRetroactHeader', $work_no)));
    }

    /**
     * 仕入赤黒遡及
     * マスター一括確認画面
     */
    function stock_with_master(){
        $ret = $this->getClaimData($this->request->data ,  Configure::read('Claim.stock') , true );
        $this->set('count' , $ret[0][0]['count']);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Retroact.token' , $token);
        $this->request->data['Retroact']['token'] = $token;
    }

    /**
     * 仕入遡及
     * マスター一括完了画面
     */
    function stock_with_master_result() {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['Retroact']['token'] === $this->Session->read('Retroact.token')) {
            $this->Session->delete('Retroact.token');
            
            $work_no = $this->setWorkNo4Header( date('Ymd') , "24" );
            $now     = date('Y/m/d H:i:s');
            $facility_id = $this->getFacilityId($this->request->data['TrnRetroactHeader']['facilityCode'] ,
                                                Configure::read('FacilityType.supplier')
                                                );
            
            
            // トランザクション開始
            $this->TrnRetroactHeader->begin();
            
            // 必要情報の取得
            $result = $this->getClaimData($this->request->data ,Configure::read('Claim.stock') , false );
            
            // 行ロック
            // 対象行が0だったらエラー
            if( count($result) == 0 ) {
                $this->TrnRetroactHeader->rollback();
                $this->Session->setFlash('仕入遡及対象データがありません。', 'growl', array('type'=>'error') );
                $this->redirect('stock');
            }
            
            // 計上日が締め済みの日付だったらエラー
            
            // 遡及期間に仮締め、未締めがあったらエラー
            
            
            // 遡及ヘッダ作成
            $TrnRetroactHeader = array(
                'work_no'         => $work_no,
                'work_date'       => $this->request->data['TrnRetroactHeader']['claim_date'],
                'work_type'       => $this->request->data['TrnRetroactHeader']['work_class'],
                'recital'         => $this->request->data['TrnRetroactHeader']['recital'],
                'retroact_type'   => Configure::read('Retroact.stock_retroact'),
                'mst_facility_id' => $facility_id,
                'start_date'      => $this->request->data['TrnRetroactHeader']['start_date'],
                'end_date'        => $this->request->data['TrnRetroactHeader']['end_date'],
                'is_deleted'      => false,
                'creater'         => $this->Session->read('Auth.MstUser.id'),
                'modifier'        => $this->Session->read('Auth.MstUser.id'),
                'created'         => $now,
                'modified'        => $now,
                'detail_count'    => count($result)
                );
            
            $this->TrnRetroactHeader->create();
            // SQL実行
            if (!$this->TrnRetroactHeader->save($TrnRetroactHeader)) {
                $this->TrnRetroactHeader->rollback();
                $this->Session->setFlash('仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('stock');
            }
            $trn_retroact_header_id = $this->TrnRetroactHeader->getLastInsertID();
            
            foreach($result as $r){
                //赤遡及明細作成
                $TrnRetroactRecord = array(
                    'work_no'                => $work_no,
                    'retroact_type'          => Configure::read('Retroact.stock_retroact_minus'),
                    'department_id_from'     => $r[0]['department_id_from'],
                    'department_id_to'       => $r[0]['department_id_to'],
                    'mst_item_unit_id'       => $r[0]['mst_item_unit_id'],
                    'retroact_price'         => $r[0]['unit_price'], //請求作成時単価
                    'trn_close_header_id'    => 0,
                    'is_deleted'             => false,
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'modified'               => $now,
                    'trn_retroact_header_id' => $trn_retroact_header_id,
                    );
                $this->TrnRetroactRecord->create();
                if (!$this->TrnRetroactRecord->save($TrnRetroactRecord)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('stock');
                }
                $trn_retroact_aka_id = $this->TrnRetroactRecord->getLastInsertID();
                
                //赤請求明細作成
                $TrnClaim = array(
                    'department_id_from'      => $r[0]['department_id_from'],
                    'department_id_to'        => $r[0]['department_id_to'],
                    'mst_item_unit_id'        => $r[0]['mst_item_unit_id'],
                    'claim_date'              => $this->request->data['TrnRetroactHeader']['claim_date'],
                    'claim_price'             => 0 - abs($r[0]['claim_price']),
                    'trn_retroact_record_id'  => $trn_retroact_aka_id,
                    'is_stock_or_sale'        => Configure::read('Claim.stock'),
                    'count'                   => $r[0]['count'],
                    'unit_price'              => $r[0]['unit_price'],
                    'round'                   => $r[0]['round'],
                    'is_not_retroactive'      => true, //遡及できないようにする仕様
                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                 => $now,
                    'modified'                => $now,
                    'retroact_execution_flag' => true,
                    'claim_type'              => Configure::read('ClaimType.stock_retroact_minus')
                    );
                $this->TrnClaim->create();
                if (!$this->TrnClaim->save($TrnClaim)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('stock');
                }
                
                //黒遡及明細作成
                $TrnRetroactRecord = array(
                    'work_no'                => $work_no,
                    'retroact_type'          => Configure::read('Retroact.stock_retroact_plus'),
                    'department_id_from'     => $r[0]['department_id_from'],
                    'department_id_to'       => $r[0]['department_id_to'],
                    'mst_item_unit_id'       => $r[0]['mst_item_unit_id'],
                    'retroact_price'         => $r[0]['price'], 
                    'trn_close_header_id'    => 0,
                    'is_deleted'             => false,
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'modified'               => $now,
                    'trn_retroact_header_id' => $trn_retroact_header_id,
                    );
                $this->TrnRetroactRecord->create();
                if (!$this->TrnRetroactRecord->save($TrnRetroactRecord)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('stock');
                }
                $trn_retroact_kuro_id = $this->TrnRetroactRecord->getLastInsertID();
                
                //黒請求明細作成
                $TrnClaim = array(
                    'department_id_from'      => $r[0]['department_id_from'],
                    'department_id_to'        => $r[0]['department_id_to'],
                    'mst_item_unit_id'        => $r[0]['mst_item_unit_id'],
                    'claim_date'              => $this->request->data['TrnRetroactHeader']['claim_date'],
                    'claim_price'             => $this->getFacilityUnitPrice($r[0]['round'] , $r[0]['price'] , $r[0]['count']),
                    'trn_retroact_record_id'  => $trn_retroact_kuro_id,
                    'is_stock_or_sale'        => Configure::read('Claim.stock'),
                    'count'                   => $r[0]['count'],
                    'unit_price'              => $r[0]['price'],
                    'round'                   => $r[0]['round'],
                    'is_not_retroactive'      => true,
                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                 => $now,
                    'modified'                => $now,
                    'retroact_execution_flag' => true,
                    'claim_type'              => Configure::read('ClaimType.stock_retroact_plus')
                    );
                $this->TrnClaim->create();
                if (!$this->TrnClaim->save($TrnClaim)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('stock');
                }
                
                // 元請求レコードを遡及不可
                $this->TrnClaim->create();
                $ret = $this->TrnClaim->updateAll(
                    array(
                        'TrnClaim.is_not_retroactive' => "'true'",
                        'TrnClaim.modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'TrnClaim.modified'           => "'" . $now . "'"
                        ),
                    array(
                        'TrnClaim.id' => $r[0]['id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('stock');
                }
            }
            
            $this->set('count' , count($result));
            $this->Session->write('Retroact.count' , count($result));
            
            //コミット
            $this->TrnRetroactHeader->commit();
            $this->Session->setFlash('仕入赤黒遡及が完了しました', 'growl', array('type'=>'star') );
        }else{
            $this->set('count' , $this->Session->read('Retroact.count'));
        }
    }
    
    /**
     * 売上赤黒遡及
     */
    function sales(){
        $this->setRoleFunction(68); //売上遡及
        // 施主
        $this->set('owners' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('FacilityType.donor')),
                                                         true
                                                         ));
        // 病院
        $this->set('customers' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('FacilityType.hospital')),
                                                         true
                                                         ));
        $work_no = Configure::read('workNoModels');
        $this->set('work_types', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),array_search('TrnRetroactHeader', $work_no)));
        $this->render('sales');
    }

    /**
     * 売上赤黒遡及確認
     */
    function sales_with_master() {
        $ret = $this->getClaimData($this->request->data ,  Configure::read('Claim.sales') , true );
        $this->set('count' , $ret[0][0]['count']);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Retroact.token' , $token);
        $this->request->data['Retroact']['token'] = $token;
    }

    /**
     * 売上赤黒遡及完了
     */
    function sales_with_master_result() {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['Retroact']['token'] === $this->Session->read('Retroact.token')) {
            $this->Session->delete('Retroact.token');
            
            $work_no = $this->setWorkNo4Header( date('Ymd') , "24" );
            $now     = date('Y/m/d H:i:s');
            $facility_id = $this->getFacilityId($this->request->data['TrnRetroactHeader']['facilityCode'] ,
                                                Configure::read('FacilityType.hospital')
                                                );
            
            
            // トランザクション開始
            $this->TrnRetroactHeader->begin();
            
            // 必要情報の取得
            $result = $this->getClaimData($this->request->data ,Configure::read('Claim.sales') , false );
            
            // 行ロック
            // 対象行が0だったらエラー
            if( count($result) == 0 ) {
                $this->TrnRetroactHeader->rollback();
                $this->Session->setFlash('売上遡及対象データがありません。', 'growl', array('type'=>'error') );
                $this->redirect('sales');
            }
            
            // 計上日が締め済みの日付だったらエラー
            
            // 遡及期間に仮締め、未締めがあったらエラー
            
            
            // 遡及ヘッダ作成
            $TrnRetroactHeader = array(
                'work_no'         => $work_no,
                'work_date'       => $this->request->data['TrnRetroactHeader']['claim_date'],
                'work_type'       => $this->request->data['TrnRetroactHeader']['work_class'],
                'recital'         => $this->request->data['TrnRetroactHeader']['recital'],
                'retroact_type'   => Configure::read('Retroact.sales_retroact'),
                'mst_facility_id' => $facility_id,
                'start_date'      => $this->request->data['TrnRetroactHeader']['start_date'],
                'end_date'        => $this->request->data['TrnRetroactHeader']['end_date'],
                'is_deleted'      => false,
                'creater'         => $this->Session->read('Auth.MstUser.id'),
                'modifier'        => $this->Session->read('Auth.MstUser.id'),
                'created'         => $now,
                'modified'        => $now,
                'detail_count'    => count($result)
                );
            
            $this->TrnRetroactHeader->create();
            // SQL実行
            if (!$this->TrnRetroactHeader->save($TrnRetroactHeader)) {
                $this->TrnRetroactHeader->rollback();
                $this->Session->setFlash('売上赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('sales');
            }
            $trn_retroact_header_id = $this->TrnRetroactHeader->getLastInsertID();
            
            foreach($result as $r){
                //赤遡及明細作成
                $TrnRetroactRecord = array(
                    'work_no'                => $work_no,
                    'retroact_type'          => Configure::read('Retroact.sales_retroact_minus'),
                    'department_id_from'     => $r[0]['department_id_from'],
                    'department_id_to'       => $r[0]['department_id_to'],
                    'mst_item_unit_id'       => $r[0]['mst_item_unit_id'],
                    'retroact_price'         => $r[0]['unit_price'], //請求作成時単価
                    'trn_close_header_id'    => 0,
                    'is_deleted'             => false,
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'modified'               => $now,
                    'trn_retroact_header_id' => $trn_retroact_header_id,
                    );
                $this->TrnRetroactRecord->create();
                if (!$this->TrnRetroactRecord->save($TrnRetroactRecord)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('売上赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('sales');
                }
                $trn_retroact_aka_id = $this->TrnRetroactRecord->getLastInsertID();
                
                //赤請求明細作成
                $TrnClaim = array(
                    'department_id_from'      => $r[0]['department_id_from'],
                    'department_id_to'        => $r[0]['department_id_to'],
                    'mst_item_unit_id'        => $r[0]['mst_item_unit_id'],
                    'claim_date'              => $this->request->data['TrnRetroactHeader']['claim_date'],
                    'claim_price'             => 0 - abs($r[0]['claim_price']),
                    'trn_retroact_record_id'  => $trn_retroact_aka_id,
                    'is_stock_or_sale'        => Configure::read('Claim.sales'),
                    'count'                   => $r[0]['count'],
                    'unit_price'              => $r[0]['unit_price'],
                    'round'                   => $r[0]['round'],
                    'is_not_retroactive'      => true, //遡及できないようにする仕様
                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                 => $now,
                    'modified'                => $now,
                    'retroact_execution_flag' => true,
                    'claim_type'              => Configure::read('ClaimType.sale_retroact_minus')
                    );
                $this->TrnClaim->create();
                if (!$this->TrnClaim->save($TrnClaim)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('売上赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('sales');
                }
                
                //黒遡及明細作成
                $TrnRetroactRecord = array(
                    'work_no'                => $work_no,
                    'retroact_type'          => Configure::read('Retroact.sales_retroact_plus'),
                    'department_id_from'     => $r[0]['department_id_from'],
                    'department_id_to'       => $r[0]['department_id_to'],
                    'mst_item_unit_id'       => $r[0]['mst_item_unit_id'],
                    'retroact_price'         => $r[0]['price'], 
                    'trn_close_header_id'    => 0,
                    'is_deleted'             => false,
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'modified'               => $now,
                    'trn_retroact_header_id' => $trn_retroact_header_id,
                    );
                $this->TrnRetroactRecord->create();
                if (!$this->TrnRetroactRecord->save($TrnRetroactRecord)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('売上赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('sales');
                }
                $trn_retroact_kuro_id = $this->TrnRetroactRecord->getLastInsertID();
                
                //黒請求明細作成
                $TrnClaim = array(
                    'department_id_from'      => $r[0]['department_id_from'],
                    'department_id_to'        => $r[0]['department_id_to'],
                    'mst_item_unit_id'        => $r[0]['mst_item_unit_id'],
                    'claim_date'              => $this->request->data['TrnRetroactHeader']['claim_date'],
                    'claim_price'             => $this->getFacilityUnitPrice($r[0]['round'] , $r[0]['price'] , $r[0]['count']),
                    'trn_retroact_record_id'  => $trn_retroact_kuro_id,
                    'is_stock_or_sale'        => Configure::read('Claim.sales'),
                    'count'                   => $r[0]['count'],
                    'unit_price'              => $r[0]['price'],
                    'round'                   => $r[0]['round'],
                    'is_not_retroactive'      => true,
                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                 => $now,
                    'modified'                => $now,
                    'retroact_execution_flag' => true,
                    'claim_type'              => Configure::read('ClaimType.sale_retroact_plus')
                    );
                $this->TrnClaim->create();
                if (!$this->TrnClaim->save($TrnClaim)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('売上赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('sales');
                }
                
                // 元請求レコードを遡及不可
                $this->TrnClaim->create();
                $ret = $this->TrnClaim->updateAll(
                    array(
                        'TrnClaim.is_not_retroactive' => "'true'",
                        'TrnClaim.modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'TrnClaim.modified'           => "'" . $now . "'"
                        ),
                    array(
                        'TrnClaim.id' => $r[0]['id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('売上赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('sales');
                }
            }
            
            $this->set('count' , count($result));
            $this->Session->write('Retroact.count' , count($result));
            
            //コミット
            $this->TrnRetroactHeader->commit();
            $this->Session->setFlash('売上赤黒遡及が完了しました', 'growl', array('type'=>'star') );
        }else{
            $this->set('count' , $this->Session->read('Retroact.count'));
        }
    }
    

    /**
     * 遡及履歴CSV出力
     */
    function export_csv() {
        $where = $this->_getCSVWhere();
        $sql = $this->_getCSVSql($where);

        $this->db_export_csv($sql , "遡及履歴", 'history');
    }


    /**
     * 遡及履歴CSV出力用SQL作成
     */
    private function _getCSVSql($where) {
        $sql  = "";
        $sql .= " select";
        $sql .= "      TrnRetroactHeader.work_no                              as 遡及番号";
        $sql .= "     ,(CASE TrnRetroactHeader.retroact_type ";
        foreach(Configure::read('Retroacts') as $k=>$v){
            $sql .= "           WHEN '".$k."'   THEN '".$v."' ";
        }
        $sql .= "       END)                                                  as 遡及種別 ";
        $sql .= "     ,TO_CHAR(TrnRetroactHeader.work_date,'YYYY/MM/DD')      as 計上日 ";
        $sql .= "     ,MstFacility.facility_name                              as 対象施設 ";
        $sql .= "     ,MstClass.name                                          as 作業区分 ";
        $sql .= "     ,MstUser.user_name                                      as 登録者 ";
        $sql .= "     ,TO_CHAR(TrnRetroactHeader.created,'YYYY/MM/DD')        as 登録日 ";
        $sql .= "     ,TrnRetroactHeader.recital                              as 備考 ";
        $sql .= '     ,MstFacilityItem.internal_code                          as "商品ID" ';
        $sql .= "     ,MstFacilityItem.item_name                              as 商品名 ";
        $sql .= "     ,MstFacilityItem.standard                               as 規格 ";
        $sql .= "     ,MstFacilityItem.item_code                              as 製品番号 ";
        $sql .= "     ,MstDealer.dealer_name                                  as 販売元 ";
        $sql .= "     ,(case when  MstItemUnit.per_unit = 1 then  MstUnitName.unit_name ";
        $sql .= "           else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || PerUnitName.unit_name || ')' ";
        $sql .= "       end)                                                  as 包装単位名 ";
        $sql .= "     ,TrnRetroactRecord.retroact_price                       as 単価 ";
        $sql .= "     ,TO_CHAR(TrnRetroactHeader.start_date, 'YYYY/MM/DD')    as 適用開始日 ";
        $sql .= "     ,TO_CHAR(TrnRetroactHeader.end_date, 'YYYY/MM/DD')      as 適用終了日 ";
        $sql .= " from";
        $sql .= "     trn_retroact_headers AS TrnRetroactHeader inner join trn_retroact_records AS TrnRetroactRecord ";
        $sql .= "         on TrnRetroactRecord.trn_retroact_header_id = TrnRetroactHeader.id ";
        $sql .= "         and TrnRetroactRecord.is_deleted = FALSE ";
        $sql .= "     inner join mst_facilities AS MstFacility ";
        $sql .= "         on TrnRetroactHeader.mst_facility_id = MstFacility.id ";
        $sql .= "     inner join mst_users AS MstUser ";
        $sql .= "         on CAST(TrnRetroactHeader.creater AS INTEGER) = MstUser.id ";
        $sql .= "     inner join mst_item_units AS MstItemUnit ";
        $sql .= "         on TrnRetroactRecord.mst_item_unit_id = MstItemUnit.id ";
        $sql .= "         and MstItemUnit.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= "     inner join mst_unit_names AS MstUnitName ";
        $sql .= "         on MstUnitName.id = MstItemUnit.mst_unit_name_id ";
        $sql .= "     inner join mst_facility_items AS MstFacilityItem ";
        $sql .= "         on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ";
        $sql .= "     left join mst_dealers AS MstDealer ";
        $sql .= "         on MstFacilityItem.mst_dealer_id = MstDealer.id ";
        $sql .= "     inner join mst_unit_names AS PerUnitName ";
        $sql .= "         on PerUnitName.id = MstItemUnit.per_unit_name_id ";
        $sql .= "     left join mst_classes AS MstClass ";
        $sql .= "         on MstClass.id = TrnRetroactHeader.work_type ";
        $sql .= " where ";
        $sql .= $where;

        return $sql;
    }

    /**
     * 遡及履歴CSV出力用条件作成
     */
    private function _getCSVWhere() {

        $center_dept_id = null;
        $supplier_id = null;
        $hospital_id = null;

        //センターの部署ID取得
        $center_dept_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'),
                                                 array(Configure::read('DepartmentType.warehouse'))
                                                 );
        //仕入先の部署id取得
        if(!empty($this->request->data['TrnRetroactRecord']['department_code_from'])){
            $supplier_id = $this->getFacilityId($this->request->data['TrnRetroactRecord']['department_code_from'],
                                                array(Configure::read('FacilityType.supplier'))
                                                );
        }

        //得意先の部署ID取得
        if(!empty($this->request->data['TrnRetroactRecord']['department_code_to'])){
            $hospital_id = $this->getFacilityId($this->request->data['TrnRetroactRecord']['department_code_from'],
                                                array(Configure::read('FacilityType.hospital'))
                                                );
        }
        
        $where = " TrnRetroactHeader.is_deleted = false ";
        //作業番号
        if(!empty($this->request->data['TrnRetroactHeader']['work_no'])){
            $where .= " AND TrnRetroactHeader.work_no = '".Sanitize::escape($this->request->data['TrnRetroactHeader']['work_no'])."'";
        }
        //計上日
        if(!empty($this->request->data['TrnRetroactHeader']['start_date'])){
            $where .= " AND TrnRetroactHeader.work_date >= '".Sanitize::escape($this->request->data['TrnRetroactHeader']['start_date'])."'";
        }
        if(!empty($this->request->data['TrnRetroactHeader']['end_date'])){
            $where .= " AND TrnRetroactHeader.work_date <= '".Sanitize::escape($this->request->data['TrnRetroactHeader']['end_date'])."'";
        }
        //仕入先
        if(!empty($supplier_id)){
            $where .= " AND TrnRetroactHeader.mst_facility_id = ".$supplier_id;
        }
        //得意先
        if(!empty($hospital_id)){
            $where .= " AND TrnRetroactHeader.mst_facility_id = ".$hospital_id;
        }
        //仕入先、得意先共に未指定の場合、自センターの取引のみを表示
        if(empty($supplier_id) && empty($hospital_id)){
            $where .= " AND (TrnRetroactRecord.department_id_from = ".$center_dept_id;
            $where .= "  OR  TrnRetroactRecord.department_id_to = ".$center_dept_id.")";
        }
        //商品ID
        if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
            $where .= " AND MstFacilityItem.internal_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."%'";
        }
        //商品名
        if(!empty($this->request->data['MstFacilityItem']['item_name'])){
            $where .= " AND MstFacilityItem.item_name ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
        }
        //規格
        if(!empty($this->request->data['MstFacilityItem']['standard'])){
            $where .= " AND MstFacilityItem.standard ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }
        //製品番号
        if(!empty($this->request->data['MstFacilityItem']['item_code'])){
            $where .= " AND MstFacilityItem.item_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        //販売元
        if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
            $where .= " AND MstDealer.dealer_name ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name'])."%'";
        }
        //JANコード
        if(!empty($this->request->data['MstFacilityItem']['jan_code'])){
            $where .= " AND MstFacilityItem.jan_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['jan_code'])."%'";
        }
        //作業区分
        if(!empty($this->request->data['TrnRetroactRecord']['work_type'])){
            $where .= " AND TrnRetroactHeader.work_type = ".$this->request->data['TrnRetroactRecord']['work_type'];
        }
        //遡及種別
        if(!empty($this->request->data['TrnRetroactRecord']['retroact_type'])){
            $where .= " AND TrnRetroactHeader.retroact_type = ".$this->request->data['TrnRetroactRecord']['retroact_type'];
        }

        //操作可能病院で絞り込む
        $where .='   and ( ';
        $where .='      mstfacility.facility_type != '. Configure::read('FacilityType.hospital');
        $where .='      or mstfacility.id in ( ';
        $where .='        select';
        $where .='              a.mst_facility_id ';
        $where .='          from';
        $where .='            mst_user_belongings as a ';
        $where .='          where';
        $where .='            a.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $where .='            and a.is_deleted  = false ';
        $where .='      )';
        $where .='    ) ';

        return $where;
    }


    /**
     * MS法人仕入遡及
     * 初期画面
     */
    function ms_stock(){
        $this->setRoleFunction(71); //MS仕入遡及
        // 仕入先
        $this->set('suppliers' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('FacilityType.supplier')),
                                                         true
                                                         ));
        // 施主
        $this->set('owners' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                      array(Configure::read('FacilityType.donor')),
                                                      true
                                                      ));

        $work_no = Configure::read('workNoModels');
        $this->set('work_types', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),array_search('TrnRetroactHeader', $work_no)));
        $this->render('ms_stock');
    }


    /**
     * MS法人仕入遡及
     * マスター一括確認画面
     */
    function ms_stock_with_master(){
        $ret = $this->getClaimData($this->request->data ,  Configure::read('Claim.ms_stock') , true );
        $this->set('count' , $ret[0][0]['count']);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Retroact.token' , $token);
        $this->request->data['Retroact']['token'] = $token;
        $this->render('ms_stock_with_master');
    }

    /**
     * MS法人仕入遡及
     * マスター一括完了画面
     */
    function ms_stock_with_master_result() {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['Retroact']['token'] === $this->Session->read('Retroact.token')) {
            $this->Session->delete('Retroact.token');
            
            $work_no = $this->setWorkNo4Header( date('Ymd') , "24" );
            $now     = date('Y/m/d H:i:s');
            $facility_id = $this->getFacilityId($this->request->data['TrnRetroactHeader']['facilityCode'] ,
                                                Configure::read('FacilityType.supplier')
                                                );
            
            
            // トランザクション開始
            $this->TrnRetroactHeader->begin();
            
            // 必要情報の取得
            $result = $this->getClaimData($this->request->data ,Configure::read('Claim.ms_stock') , false );
            
            // 行ロック
            // 対象行が0だったらエラー
            if( count($result) == 0 ) {
                $this->TrnRetroactHeader->rollback();
                $this->Session->setFlash('MS仕入遡及対象データがありません。', 'growl', array('type'=>'error') );
                $this->redirect('stock');
            }
            
            // 在庫締めチェック
            
            // 仕入締めチェック
            
            // 遡及期間に仮締め、未締めがあったらエラー
            
            
            // 遡及ヘッダ作成
            $TrnRetroactHeader = array(
                'work_no'         => $work_no,
                'work_date'       => $this->request->data['TrnRetroactHeader']['claim_date'],
                'work_type'       => $this->request->data['TrnRetroactHeader']['work_class'],
                'recital'         => $this->request->data['TrnRetroactHeader']['recital'],
                'retroact_type'   => Configure::read('Retroact.ms_stock_retroact'),
                'mst_facility_id' => $facility_id,
                'start_date'      => $this->request->data['TrnRetroactHeader']['start_date'],
                'end_date'        => $this->request->data['TrnRetroactHeader']['end_date'],
                'is_deleted'      => false,
                'creater'         => $this->Session->read('Auth.MstUser.id'),
                'modifier'        => $this->Session->read('Auth.MstUser.id'),
                'created'         => $now,
                'modified'        => $now,
                'detail_count'    => count($result)
                );
            
            $this->TrnRetroactHeader->create();
            // SQL実行
            if (!$this->TrnRetroactHeader->save($TrnRetroactHeader)) {
                $this->TrnRetroactHeader->rollback();
                $this->Session->setFlash('MS仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ms_stock');
            }
            $trn_retroact_header_id = $this->TrnRetroactHeader->getLastInsertID();
            
            foreach($result as $r){
                //赤遡及明細作成
                $TrnRetroactRecord = array(
                    'work_no'                => $work_no,
                    'retroact_type'          => Configure::read('Retroact.ms_stock_retroact_minus'),
                    'department_id_from'     => $r[0]['department_id_from'],
                    'department_id_to'       => $r[0]['department_id_to'],
                    'mst_item_unit_id'       => $r[0]['mst_item_unit_id'],
                    'retroact_price'         => $r[0]['unit_price'], //請求作成時単価
                    'trn_close_header_id'    => 0,
                    'is_deleted'             => false,
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'modified'               => $now,
                    'trn_retroact_header_id' => $trn_retroact_header_id,
                    );
                $this->TrnRetroactRecord->create();
                if (!$this->TrnRetroactRecord->save($TrnRetroactRecord)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('MS仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ms_stock');
                }
                $trn_retroact_aka_id = $this->TrnRetroactRecord->getLastInsertID();
                
                //赤請求明細作成
                $TrnClaim = array(
                    'department_id_from'      => $r[0]['department_id_from'],
                    'department_id_to'        => $r[0]['department_id_to'],
                    'mst_item_unit_id'        => $r[0]['mst_item_unit_id'],
                    'claim_date'              => $this->request->data['TrnRetroactHeader']['claim_date'],
                    'claim_price'             => 0 - abs($r[0]['claim_price']),
                    'trn_retroact_record_id'  => $trn_retroact_aka_id,
                    'is_stock_or_sale'        => Configure::read('Claim.ms_stock'),
                    'count'                   => $r[0]['count'],
                    'unit_price'              => $r[0]['unit_price'],
                    'round'                   => $r[0]['round'],
                    'is_not_retroactive'      => true, //遡及できないようにする仕様
                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                 => $now,
                    'modified'                => $now,
                    'retroact_execution_flag' => true,
                    'claim_type'              => Configure::read('ClaimType.ms_stock_retroact_minus')
                    );
                $this->TrnClaim->create();
                if (!$this->TrnClaim->save($TrnClaim)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('MS仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ms_stock');
                }
                
                //黒遡及明細作成
                $TrnRetroactRecord = array(
                    'work_no'                => $work_no,
                    'retroact_type'          => Configure::read('Retroact.ms_stock_retroact_plus'),
                    'department_id_from'     => $r[0]['department_id_from'],
                    'department_id_to'       => $r[0]['department_id_to'],
                    'mst_item_unit_id'       => $r[0]['mst_item_unit_id'],
                    'retroact_price'         => $r[0]['price'], 
                    'trn_close_header_id'    => 0,
                    'is_deleted'             => false,
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'modified'               => $now,
                    'trn_retroact_header_id' => $trn_retroact_header_id,
                    );
                $this->TrnRetroactRecord->create();
                if (!$this->TrnRetroactRecord->save($TrnRetroactRecord)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('MS仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ms_stock');
                }
                $trn_retroact_kuro_id = $this->TrnRetroactRecord->getLastInsertID();
                
                //黒請求明細作成
                $TrnClaim = array(
                    'department_id_from'      => $r[0]['department_id_from'],
                    'department_id_to'        => $r[0]['department_id_to'],
                    'mst_item_unit_id'        => $r[0]['mst_item_unit_id'],
                    'claim_date'              => $this->request->data['TrnRetroactHeader']['claim_date'],
                    'claim_price'             => $this->getFacilityUnitPrice($r[0]['round'] , $r[0]['price'] , $r[0]['count']),
                    'trn_retroact_record_id'  => $trn_retroact_kuro_id,
                    'is_stock_or_sale'        => Configure::read('Claim.ms_stock'),
                    'count'                   => $r[0]['count'],
                    'unit_price'              => $r[0]['price'],
                    'round'                   => $r[0]['round'],
                    'is_not_retroactive'      => true,
                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                 => $now,
                    'modified'                => $now,
                    'retroact_execution_flag' => true,
                    'claim_type'              => Configure::read('ClaimType.ms_stock_retroact_plus')
                    );
                $this->TrnClaim->create();
                if (!$this->TrnClaim->save($TrnClaim)) {
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('MS仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ms_stock');
                }
                
                // 元請求レコードを遡及不可
                $this->TrnClaim->create();
                $ret = $this->TrnClaim->updateAll(
                    array(
                        'TrnClaim.is_not_retroactive' => "'true'",
                        'TrnClaim.modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'TrnClaim.modified'           => "'" . $now . "'"
                        ),
                    array(
                        'TrnClaim.id' => $r[0]['id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnRetroactHeader->rollback();
                    $this->Session->setFlash('MS仕入赤黒遡及処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ms_stock');
                }
            }
            
            $this->set('count' , count($result));
            $this->Session->write('Retroact.count' , count($result));
            
            //コミット
            $this->TrnRetroactHeader->commit();
            $this->Session->setFlash('MS仕入赤黒遡及が完了しました', 'growl', array('type'=>'star') );
        }else{
            $this->set('count' , $this->Session->read('Retroact.count'));
        }
    }
}
// EOF
