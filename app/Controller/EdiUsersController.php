<?php
/**
 * EdiUsersController
 *
 * @version 1.0.0
 * @since 2010/07/08
 */

class EdiUsersController extends AppController {
    var $name = 'EdiUsers';
    
    /**
     *
     * @var array $uses
     */
    var $uses = array('MstUser','MstDepartment','MstFacility','MstRole','MstUserBelonging');

    /**
     * @var array $components
     */
    var $components = array('CsvWriteUtils');
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;
    
    /**
     * @var MstUser
     */
    //var $MstUser;

    function beforeFilter() {
        parent::beforeFilter();
        //暗号化種別をMD5にする。
        if(Configure::read('Password.Security') == 1){
            Security::setHash('md5');
        }
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    
    /**
     * userlist
     *
     * ユーザ一覧
     */
    function userlist() {
        App::import('Sanitize');
        $this->setRoleFunction(91); //ユーザー
        $User_List = array();
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        //プルダウン作成用データ取得--------------------------------------------------
        $this->set('Role_List',$this->setRole());
        
        //施設プルダウン用データ取得
        $this->set('facility_enabled',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.supplier')),
                       true
                       )
                   );
        
        //初期表示以外の場合のみデータを取得する
        if(isset($this->request->data['MstUser']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            
            //ユーザ入力値による検索条件の作成------------------------------------
            $where = '';
            if(Configure::read('Password.Security') == 1 ){
                //暗号化しない場合、全ユーザが無制限ユーザなので
                $where .= ' and  a.is_indefinite = false ';
            }
            
            //ログインID(LIKE検索)
            if(($this->request->data['MstUser']['search_login_id'] != "")){
                $where .= " and a.login_id LIKE '%".$this->request->data['MstUser']['search_login_id']."%'";
            }
            
            //ユーザ名(LIKE検索)
            if(($this->request->data['MstUser']['search_user_name'] != "")){
                $where .= " and a.user_name LIKE '%" .$this->request->data['MstUser']['search_user_name'] ."%'";
            }
            
            //施設(完全一致・施設コード)
            if($this->request->data['MstUser']['search_facility_cd'] != ""){
                $where .= " and b.facility_code = '" . $this->request->data['MstUser']['search_facility_cd'] . "'";
            }
            //ロール(完全一致)
            if($this->request->data['MstUser']['search_role_id'] != ""){
                $where .= ' and a.mst_role_id = ' . $this->request->data['MstUser']['search_role_id'];
            }
            
            //削除済み表示
            if(!isset($this->request->data['MstUser']['search_is_deleted'])){
                $where .= ' and a.is_deleted = false ';
            }

            //検索条件の作成終了-------------------------------------------------
            
            //ユーザリスト取得
            $User_List = $this->getUserList($where , $this->request->data['limit'] );
            $this->request->data = $data;
        }
        $this->set('User_List',$User_List);
    }
    
    
    /**
     * Add ユーザ新規登録
     */
    function add() {
        $this->set('Role_List',$this->setRole());
        
        $this->set('facilities_enabled',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.supplier')),
                       true
                       )
                   );
        
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }
    
    /**
     * Mod ユーザ情報編集
     */
    function mod() {
        //更新チェック用にmod画面に入った瞬間の時間を保持
        $this->Session->write('Usermod.readTime',date('Y-m-d H:i:s'));
        $this->set('Role_List',$this->setRole());
        
        $this->set('facilities_enabled',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.supplier')),
                       true
                       )
                   );
        
        //userlistから引き継いできたidでユーザ情報を取得
        $params = array (
            'conditions' => array('MstUser.id' => $this->request->data['MstUser']['id'],
                                  ),
            'fields'     => array('MstUser.login_id',
                                  'MstUser.id',
                                  'MstUser.user_name',
                                  'MstUser.password',
                                  'MstUser.mst_facility_id',
                                  'MstUser.mst_role_id',
                                  'MstUser.is_deleted',
                                  'MstUser.is_temp_issue',
                                  'MstUser.is_indefinite',
                                  'MstFacility.facility_name',
                                  'MstFacility.facility_code',
                                  'MstRole.role_name',
                                  'MstRole.id'
                                  ),
            'order'     => array('MstUser.id'),
            'recursive' => 1
            );
        
        $user_data = $this->MstUser->find('first', $params);
        
        $this->request->data = $user_data;
        $this->request->data['MstFacility']['select_facility_code'] = $user_data['MstFacility']['facility_code'];
    }
    
    /**
     * result
     *
     * ユーザ情報更新（新規登録・更新）
     */
    function result() {
        //施設コードから、施設IDを取得
        $this->request->data['MstUser']['mst_facility_id'] = $this->getFacilityId($this->request->data['MstFacility']['select_facility_code'] ,
                                                                                  Configure::read('FacilityType.supplier')
                                                                                  ) ;
        
        //ロールIDからロール名称を取得
        $this->request->data['MstRole']['role_name'] = $this->getRoleName($this->request->data['MstUser']['mst_role_id']);
        
        $now = date('Y/m/d H:i:s.u');
        //トランザクションを開始
        $this->MstUser->begin();
        //行ロック（更新時のみ）
        if(isset( $this->request->data['MstUser']['id']) && !empty( $this->request->data['MstUser']['id'])){
            $this->MstUser->query( ' select * from mst_users as a where a.id = ' . $this->request->data['MstUser']['id'] . ' for update ');
        }
        
        $user_data = array();
        //データ整形
        if(isset($this->request->data['MstUser']['id'])){
            //更新の場合
            $user_data['MstUser']['id'] = $this->request->data['MstUser']['id'];
        }else{
            //新規の場合
            $user_data['MstUser']['creater']        = $this->Session->read('Auth.MstUser.id');
            $user_data['MstUser']['created']        = $now;
            $user_data['MstUser']['effective_date'] = $this->getNextEffectiveDate(date('Y/m/d'));
            $user_data['MstUser']['user_type']      = Configure::read('UserType.edi');
        }
        //仮パスワード発行の場合
        if(isset($this->request->data['MstUser']['is_temp_issue']) && $this->request->data['MstUser']['is_update']==1){
            if(Configure::read('Password.Security') == 1 ){
                $user_data['MstUser']['password']          = $this->request->data['MstUser']['password'];
            }else{
                $user_data['MstUser']['password']          = $_POST['data']['MstUser']['password'];
            }
            $user_data['MstUser']['is_temp_issue']     = true;
            $user_data['MstUser']['password_modified'] = $now;
        }
        
        if(Configure::read('Password.Security') == 0 ){
            //暗号化しない場合、全ユーザが無制限ユーザ
            $user_data['MstUser']['is_indefinite']     = true;
            $user_data['MstUser']['password']          = $_POST['data']['MstUser']['password'];
            $user_data['MstUser']['is_temp_issue']     = false;
            $user_data['MstUser']['password_modified'] = $now;
        }
        
        $user_data['MstUser']['login_id']          = $this->request->data['MstUser']['login_id'];
        $user_data['MstUser']['user_name']         = $this->request->data['MstUser']['user_name'];
        $user_data['MstUser']['mst_facility_id']   = $this->request->data['MstUser']['mst_facility_id'];
        $user_data['MstUser']['mst_department_id'] = $this->getDepartmentId($this->request->data['MstUser']['mst_facility_id'] ,
                                                                            array(Configure::read('FacilityType.supplier'))
                                                                            );
        $user_data['MstUser']['mst_role_id']       = $this->request->data['MstUser']['mst_role_id'];
        $user_data['MstUser']['is_deleted']        = (isset($this->request->data['MstUser']['is_deleted'])?true:false);
        $user_data['MstUser']['modifier']          = $this->Session->read('Auth.MstUser.id');
        $user_data['MstUser']['modified']          = $now;
        if(!$this->MstUser->save($user_data)){
            //ロールバック
            $this->MstUser->rollback();
            //エラーメッセージ
            $this->Session->setFlash('ユーザ情報の登録に失敗しました。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('userlist');
        }

        //ユーザ操作管理
        if(isset($this->request->data['MstUser']['id'])){
            //更新の場合
            //いったん全部関係を削除
            $this->MstUserBelonging->query(' update mst_user_belongings set is_deleted = true where mst_user_id = ' . $this->request->data['MstUser']['id'] );
            
            //選択した施設IDとで関連付けテーブルを検索
            $belonging_id = $this->getBelongingId($this->request->data['MstUser']['id'] , $this->request->data['MstUser']['mst_facility_id']);
            if($belonging_id){
                //該当組み合わせアリ => 更新処理
                $this->MstUserBelonging->query('update mst_user_belongings set is_deleted = false where id = ' . $belonging_id);
            }else{
                //該当組み合わせナシ => 追加処理
                $belonging_data = array();
                
                $belonging_data['MstUserBelonging']['mst_user_id']     = $this->request->data['MstUser']['id'];
                $belonging_data['MstUserBelonging']['mst_facility_id'] = $this->request->data['MstUser']['mst_facility_id'];
                $belonging_data['MstUserBelonging']['is_deleted']      = false;
                $belonging_data['MstUserBelonging']['creater']         = $this->Session->read('Auth.MstUser.id');
                $belonging_data['MstUserBelonging']['created']         = $now;
                $belonging_data['MstUserBelonging']['modifier']        = $this->Session->read('Auth.MstUser.id');
                $belonging_data['MstUserBelonging']['modified']        = $now;
                $this->MstUserBelonging->create();
                if(!$this->MstUserBelonging->save($belonging_data)){
                    //ロールバック
                    $this->MstUser->rollback();
                    //エラーメッセージ
                    $this->Session->setFlash('ユーザ操作情報の登録に失敗しました。', 'growl', array('type'=>'error') );
                    //リダイレクト
                    $this->redirect('userlist');
                }
            }
        }else{
            //新規の場合
            $this->request->data['MstUser']['id'] = $this->MstUser->getLastInsertId();
            $belonging_data = array();
            $belonging_data[] = array(
                'MstUserBelonging' => array(
                    'mst_user_id'     => $this->request->data['MstUser']['id'],
                    'mst_facility_id' => $this->request->data['MstUser']['mst_facility_id'],
                    'is_deleted'      => false,
                    'creater'         => $this->Session->read('Auth.MstUser.id'),
                    'created'         => $now,
                    'modifier'        => $this->Session->read('Auth.MstUser.id'),
                    'modified'        => $now
                    )
                );
            if(!$this->MstUserBelonging->saveAll($belonging_data , array('validates' => true,'atomic' => false))){
                //ロールバック
                $this->MstUser->rollback();
                //エラーメッセージ
                $this->Session->setFlash('ユーザ操作情報の登録に失敗しました。', 'growl', array('type'=>'error') );
                //リダイレクト
                $this->redirect('userlist');
            }
        }
        //コミット
        $this->MstUser->commit();
    }
    
    
    /**
     * setRole
     * ロール選択用プルダウン作成用データ取得
     */
    function setRole() {
        //ロール名の取得
        $params = array (
            'fields'    => array('MstRole.id','MstRole.role_name'),
            'order'     => array('MstRole.id'),
            'recursive' => 1
            );
        
        $params['conditions'] = array('MstRole.is_deleted' => FALSE);
        $Role_List = Set::Combine($this->MstRole->find('all',$params),
                                  "{n}.MstRole.id",
                                  "{n}.MstRole.role_name"
                                  );
        return $Role_List;
    }
    
    /*
     * ユーザIDと、施設IDで、ユーザ操作管理のIDを返す
     */
    private function getBelongingId($user_id , $facility_id){
        $params = array (
            'conditions' => array('MstUserBelonging.mst_user_id'     => $user_id,
                                  'MstUserBelonging.mst_facility_id' => $facility_id,
                                  ),
            'fields'     => array('MstUserBelonging.id'),
            'recursive'  => -1
            );
        
        $tmp = $this->MstUserBelonging->find('first', $params);
        return (isset($tmp['MstUserBelonging']['id'])?$tmp['MstUserBelonging']['id']:false);
    }

    
    /* ロールIDからロールの名称を取得 */
    private function getRoleName($id){
        $params = array (
            'conditions' => array('MstRole.id' => $id),
            'fields'     => array('MstRole.role_name'),
            'recursive'  => -1
            );
        
        $tmp = $this->MstRole->find('first', $params);
        return (isset($tmp['MstRole']['role_name'])?$tmp['MstRole']['role_name']:false);
    }


    
    /**
     * ユーザリストを返す。
     */
    private function getUserList($where , $limit = null){
        $sql  = ' select ';
        $sql .= '       a.id              as "MstUser__id"';
        $sql .= '     , a.login_id        as "MstUser__login_id"';
        $sql .= '     , a.user_name       as "MstUser__user_name"';
        $sql .= '     , b.facility_name   as "MstFacility__facility_name"';
        $sql .= '     , c.role_name       as "MstRole__role_name"';
        $sql .= "     , ( case when a.is_temp_issue = true then '○' else '' end ) ";
        $sql .= '                         as "MstUser__is_temp_issue"';
        $sql .= "     , ( case when a.is_deleted = true then '○' else '' end ) ";
        $sql .= '                         as "MstUser__is_deleted"';
        $sql .= "     , to_char(a.password_modified , 'YYYY/mm/dd') ";
        $sql .= '                         as "MstUser__password_modified"';
        $sql .= "     , ( case when a.effective_date < now()::date then '' else '○' end ) " ;
        $sql .= '                         as "MstUser__update_check"';
        
        $sql .= '   from ';
        $sql .= '     mst_users as a  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.mst_facility_id  ';
        $sql .= '     left join mst_roles as c  ';
        $sql .= '       on c.id = a.mst_role_id  ';
        $sql .= '   where 1=1 ';
        $sql .= '     and a.user_type = ' . Configure::read('UserType.edi');
        $sql .= $where ;
        $sql .= '   order by ';
        $sql .= '     b.id asc ';
        $sql .= '     , c.id asc ';
        $sql .= '     , a.login_id asc ';
        
        $this->set('max' , $this->getMaxCount($sql , 'MstUser'));
        if(!is_null($limit)){
            $sql .= '   limit ' . $limit;
        }
        return $this->MstUser->query($sql);
    }
}
