<?php
/**
 * MstSalesConfigsController
 *
 * @version 1.0.0
 * @since 2010/08/05
 */

class MstSalesConfigsController extends AppController {
    var $name = 'MstSalesConfigs';
    
    /**
     *
     * @var array $uses
     */
    var $uses = array('MstSalesConfig',
                      'MstItemUnit',
                      'MstUnitName',
                      'MstFacility',
                      'MstFacilityItem',
                      'MstUserBelonging',
                      'MstDealer',
                      'MstFacilityRelation',
                      'MstUser',
                      'MstInsuranceClaim',
                      'MstInsuranceClaimDepartment'
                      );
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;
    
    /**
     * @var MstSalesConfigs
     */
    var $MstSalesConfigs;
    
    //ログインなしでの印刷を許可
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
    }

    /**
     * sales_configs_list
     *
     * 施設採用品選択
     */
    function sales_configs_list() {
        $this->setRoleFunction(82); //売上設定

        $SalesConfig_Info = array();
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        
        $this->set('facilities',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected') ,
                       array(Configure::read('FacilityType.hospital'))
                       )
                   );
        
        if(isset($this->request->data['MstSalesConfig']['is_search'])){
            $params = $this->getSearchParamater();
            
            //検索---------------------------------------------------------
            $SalesConfig_Info = $this->getItems($this->Session->read('Auth.facility_id_selected'), $params);
        }
        $SalesConfig_Info = AppController::outputFilter($SalesConfig_Info);
        $this->set('SalesConfig_Info',$SalesConfig_Info);
    }
    
    /**
     *  得意先選択
     */
    function customer_list() {
        $this->set('facilityList',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected') ,
                       array(Configure::read('FacilityType.hospital')) ,
                       true ,
                       $field = array('id' , 'facility_name')
                       )
                   );
        
        //採用品情報を取得
        $sql  = ' select ';
        $sql .= '       a.id                     as "MstFacilityItem__id"';
        $sql .= '     , a.internal_code          as "MstFacilityItem__internal_code"';
        $sql .= '     , a.item_name              as "MstFacilityItem__item_name"';
        $sql .= '     , a.item_code              as "MstFacilityItem__item_code"';
        $sql .= '     , a.standard               as "MstFacilityItem__standard"';
        $sql .= '     , a.jan_code               as "MstFacilityItem__jan_code"';
        $sql .= '     , b.dealer_name            as "MstFacilityItem__dealer_name"';
        $sql .= '     , a.refund_price           as "MstFacilityItem__refund_price"';
        $sql .= '     , c.insurance_claim_name_s as "MstFacilityItem__insurance_claim_name_s"';
        $sql .= '     , d.const_nm               as "MstFacilityItem__insurance_claim_department_name"';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_dealers as b  ';
        $sql .= '       on a.mst_dealer_id = b.id  ';
        $sql .= '     left join mst_insurance_claims as c  ';
        $sql .= '       on c.insurance_claim_code = a.insurance_claim_code  ';
        $sql .= '     left join mst_consts as d  ';
        $sql .= '       on d.const_cd = a.insurance_claim_type::varchar  ';
        $sql .= "       and d.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->request->data['MstFacilityItem']['id'];
        
        $tmp = $this->MstFacilityItem->query($sql);
        
        $result = array();
        //売上げ設定を取得
        $sql  = 'select ';
        $sql .= '      c.id            as "MstFacility__id"';
        $sql .= '    , c.facility_name as "MstSalesConfig__facility_name"';
        $sql .= '    , TO_CHAR(d.start_date,\'yyyy/mm/dd\')    as "MstSalesConfig__start_date"';
        $sql .= '    , (  ';
        $sql .= '      case  ';
        $sql .= '        when e.per_unit = 1  ';
        $sql .= '        then f.unit_name  ';
        $sql .= "        else f.unit_name || '(' || e.per_unit || g.unit_name || ')'  ";
        $sql .= '        end ';
        $sql .= '    )                 as "MstSalesConfig__unit_name" ';
        $sql .= '    , d.sales_price   as "MstSalesConfig__sales_price" ';
        $sql .= '  from ';
        $sql .= '    mst_facility_items as a  ';
        $sql .= '    left join mst_facility_relations as b  ';
        $sql .= '      on b.mst_facility_id = a.mst_facility_id  ';
        $sql .= '    left join mst_facilities as c  ';
        $sql .= '      on c.id = b.partner_facility_id  ';
        $sql .= '    left join mst_sales_configs as d  ';
        $sql .= '      on d.partner_facility_id = c.id  ';
        $sql .= '      and d.mst_facility_item_id = ' . $this->request->data['MstFacilityItem']['id'];
        $sql .= '      and ( now() between d.start_date and d.end_date or d.id is null )';
        $sql .= '    left join mst_item_units as e  ';
        $sql .= '      on e.id = d.mst_item_unit_id  ';
        $sql .= '    left join mst_unit_names as f  ';
        $sql .= '      on f.id = e.mst_unit_name_id  ';
        $sql .= '    left join mst_unit_names as g  ';
        $sql .= '      on g.id = e.per_unit_name_id  ';
        $sql .= '    inner join mst_user_belongings as h ';
        $sql .= '      on h.mst_facility_id = c.id ';
        $sql .= '      and h.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '      and h.is_deleted = false ';
        
        $sql .= '  where ';
        $sql .= '    a.id = ' . $this->request->data['MstFacilityItem']['id'];
        $sql .= '    and c.facility_type = ' . Configure::read('FacilityType.hospital');
        
        if(!empty($this->request->data['MstFacilityItem']['customer'])){
            $sql .= '    and c.id = ' . $this->request->data['MstFacilityItem']['customer'];
        }
        $sql .= ' order by c.facility_code ';
        $this->set('max' , $this->getMaxCount($sql , 'MstSalesConfig'));
        $sql .= ' limit ' . $this->_getLimitCount();
        $tmp_customer = (isset($this->request->data['MstFacilityItem']['customer'])?$this->request->data['MstFacilityItem']['customer']:'');
        $result = $this->MstSalesConfig->query($sql);
        $this->set('result' , $result);
        $this->request->data = $tmp[0];
        $this->request->data['MstFacilityItem']['customer'] = $tmp_customer;
    }
    
    /**
     * Mod 売上情報編集
     */
    function mod() {
        //更新時間チェック用に時間を保存
        $this->Session->write('SalesConfig.readTime',date('Y-m-d H:i:s'));
        
        $this->request->data['MstFacility']['facility_name'] = $this->getFacilityName($this->request->data['MstFacility']['id']);
        
        $sql  = 'select ';
        $sql .= '      a.id                                                       as "MstSalesConfig__id"';
        $sql .= '    , TO_CHAR(a.start_date,\'yyyy/mm/dd\')                       as "MstSalesConfig__start_date"';
        $sql .= '    , TO_CHAR(a.end_date,\'yyyy/mm/dd\')                         as "MstSalesConfig__end_date"';
        $sql .= '    , a.mst_item_unit_id                                         as "MstSalesConfig__mst_item_unit_id"';
        $sql .= '    , a.sales_price                                              as "MstSalesConfig__sales_price"';
        $sql .= '    , ( case when a.is_deleted = true then false else true end)  as "MstSalesConfig__is_deleted"';
        $sql .= '    , (  ';
        $sql .= '      case  ';
        $sql .= '        when a.end_date < now()  ';
        $sql .= '        then 0  ';
        $sql .= '        when a.start_date > now()  ';
        $sql .= '        then 0  ';
        $sql .= '        else 1  ';
        $sql .= '        end ';
        $sql .= '    )                                                            as "MstSalesConfig__status"';
        $sql .= '  from ';
        $sql .= '    mst_sales_configs as a  ';
        $sql .= '  where ';
        $sql .= '    a.partner_facility_id = ' . $this->request->data['MstFacility']['id'];
        $sql .= '    and a.mst_facility_item_id = ' . $this->request->data['MstFacilityItem']['id'];
        $sql .= '  order by a.mst_item_unit_id , a.start_date  ' ;
        $result = $this->MstSalesConfig->query($sql);

        $this->set('result' , $result);
        $this->set('UnitNameList' , $this->setItemUnits());
    }
    
    private function getSearchParamater(){
        App::import('Sanitize');
        $params = array('conditions' => array());
        //得意先(完全一致)
        if((isset($this->request->data['MstFacilityItem']['facilityCode'])) && ($this->request->data['MstFacilityItem']['facilityCode'] != "")){
            $where = array('MstPartnerFacility.facility_code' => Sanitize::escape($this->request->data['MstFacilityItem']['facilityCode']));
            $params['conditions'] = array_merge($params['conditions'],$where);
        }
        //商品ID(前方一致)
        if((isset($this->request->data['MstFacilityItem']['internal_code'])) && ($this->request->data['MstFacilityItem']['internal_code'] != "")){
            $where = array('MstFacilityItem.internal_code LIKE' => Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }
        //商品名(LIKE検索)
        if((isset($this->request->data['MstFacilityItem']['item_name'])) && ($this->request->data['MstFacilityItem']['item_name'] != "")){
            $where = array('MstFacilityItem.item_name LIKE' => "%". Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }
        
        //規格(LIKE検索)
        if((isset($this->request->data['MstFacilityItem']['standard'])) && ($this->request->data['MstFacilityItem']['standard'] != "")){
            $where = array('MstFacilityItem.standard LIKE' => "%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }
        //製品番号(LIKE検索)
        if((isset($this->request->data['MstFacilityItem']['item_code'])) && ($this->request->data['MstFacilityItem']['item_code'] != "")){
            $where = array('MstFacilityItem.item_code LIKE' => "%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }
        
        //販売元(LIKE検索)
        if((isset($this->request->data['MstFacilityItem']['dealer_name'])) && ($this->request->data['MstFacilityItem']['dealer_name'] != "")){
            $where = array('MstDealer.dealer_name LIKE' => "%".Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }
        
        //JANコード(前方一致)
        if((isset($this->request->data['MstFacilityItem']['jan_code'])) && ($this->request->data['MstFacilityItem']['jan_code'] != "")){
            $where = array('MstFacilityItem.jan_code LIKE' => Sanitize::escape($this->request->data['MstFacilityItem']['jan_code'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }
        
        //日付
        $s_date = $e_date = null;
        if((isset($this->request->data['MstSalesConfig']['start_date'])) && ($this->request->data['MstSalesConfig']['start_date'] != "")){
            $s_date = $this->request->data['MstSalesConfig']['start_date'];
        }
        
        if((isset($this->request->data['MstSalesConfig']['end_date'])) && ($this->request->data['MstSalesConfig']['end_date'] != "")){
            $e_date = $this->request->data['MstSalesConfig']['end_date'];
        }
        
        if(isset($s_date) && isset($e_date)){
            $params['conditions'] = array_merge($params['conditions'],array('MstSalesConfig.start_date <= ' => $e_date));
            $params['conditions'] = array_merge($params['conditions'],array('MstSalesConfig.end_date > ' => $s_date));
        }elseif(false === isset($s_date) && isset($e_date)){
            $params['conditions'] = array_merge($params['conditions'],array('MstSalesConfig.end_date <= ' => $e_date));
        }elseif(isset($s_date) && false === isset($e_date)){
            $params['conditions'] = array_merge($params['conditions'],array('MstSalesConfig.start_date >= ' => $s_date));
        }
        
        return $params;
    }
    
    /**
     * result
     * 情報更新（新規登録・更新）
     */
    function result() {
        $this->MstSalesConfig->begin();
        
        $now = date('Y/m/d H:i:s');
        $save_date = array();
        $ids = array();
        
        $this->set('UnitNameList' , $this->setItemUnits());
        
        //チェック & 適用終了日の調整
        foreach ($this->request->data['MstSalesConfig']['id'] as $line => $id){
            $a[$line] = $this->request->data['MstSalesConfig']['mst_item_unit_id'][$line] ."_". $this->request->data['MstSalesConfig']['start_date'][$line];
            if($id){
                //更新IDの取得
                $ids[] = $id;
            }
        }
        
        if($ids){
            //行ロック
            $this->MstSalesConfig->query(' select * from mst_sales_configs as a where a.id in (' . join(',',$ids) . ') for update ');
        }
        
        asort($a);
        
        $pre_v = null;
        $pre_k = null;
        
        foreach($a as $k => $v ){
            if(!is_null($pre_v)){
                if($pre_v === $v){
                    //同じ包装単位で同じ開始日なのでエラーにする
                    $this->Session->setFlash('適用開始日・包装単位が同一の売上設定があります。', 'growl', array('type'=>'error') );
                    //POSTデータを表示用に整形する
                    foreach($this->request->data['MstSalesConfig']['id'] as $line => $id ){
                        $result[$line]['MstSalesConfig']['id']               = $this->request->data['MstSalesConfig']['id'][$line];
                        $result[$line]['MstSalesConfig']['start_date']       = $this->request->data['MstSalesConfig']['start_date'][$line];
                        $result[$line]['MstSalesConfig']['end_date']         = $this->request->data['MstSalesConfig']['end_date'][$line];
                        $result[$line]['MstSalesConfig']['mst_item_unit_id'] = $this->request->data['MstSalesConfig']['mst_item_unit_id'][$line];
                        $result[$line]['MstSalesConfig']['is_deleted']       = isset($this->request->data['MstSalesConfig']['is_deleted'][$line])?true:false;
                        $result[$line]['MstSalesConfig']['status']           = $this->request->data['MstSalesConfig']['status'][$line];
                        $result[$line]['MstSalesConfig']['sales_price']      = $this->request->data['MstSalesConfig']['sales_price'][$line];
                    }
                    $this->set('result' , $result);
                    $this->render('mod');
                    return false;
                }
                list($per_item_unit_id , $pre_start_date) =explode('_' , $pre_v);
                list($item_unit_id , $start_date) = explode('_' , $v);
                if($per_item_unit_id == $item_unit_id){
                    //同じ包装単位で開始日が違うものがあったら終了日を調整する
                    $this->request->data['MstSalesConfig']['end_date'][$pre_k] = $start_date;
                }
            }
            
            $pre_v = $v;
            $pre_k = $k;
        }
        
        //データの詰め込み
        foreach($this->request->data['MstSalesConfig']['id'] as $line => $id ){
            if($id){
                //更新
                $save_data[] = array(
                    'MstSalesConfig' => array(
                        'id'                       => $id,
                        'mst_item_unit_id'         => $this->request->data['MstSalesConfig']['mst_item_unit_id'][$line],
                        'start_date'               => $this->request->data['MstSalesConfig']['start_date'][$line],
                        'end_date'                 => $this->request->data['MstSalesConfig']['end_date'][$line],
                        'mst_facility_relation_id' => $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $this->request->data['MstFacility']['id']),
                        'mst_facility_item_id'     => $this->request->data['MstFacilityItem']['id'],
                        'mst_facility_id'          => $this->Session->read('Auth.facility_id_selected'),
                        'partner_facility_id'      => $this->request->data['MstFacility']['id'],
                        'sales_price'              => $this->request->data['MstSalesConfig']['sales_price'][$line],
                        'is_main'                  => false,
                        'is_deleted'               => (isset($this->request->data['MstSalesConfig']['is_deleted'][$line])?false:true),
                        'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                        'modified'                 => $now
                        )
                    );
            }else{
                //新規追加
                $save_data[] = array(
                    'MstSalesConfig' => array(
                        'id'                       => false,
                        'mst_item_unit_id'         => $this->request->data['MstSalesConfig']['mst_item_unit_id'][$line],
                        'start_date'               => $this->request->data['MstSalesConfig']['start_date'][$line],
                        'end_date'                 => $this->request->data['MstSalesConfig']['end_date'][$line],
                        'mst_facility_relation_id' => $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $this->request->data['MstFacility']['id']),
                        'mst_facility_item_id'     => $this->request->data['MstFacilityItem']['id'],
                        'mst_facility_id'          => $this->Session->read('Auth.facility_id_selected'),
                        'partner_facility_id'      => $this->request->data['MstFacility']['id'],
                        'sales_price'              => $this->request->data['MstSalesConfig']['sales_price'][$line],
                        'is_main'                  => false,
                        'is_deleted'               => (isset($this->request->data['MstSalesConfig']['is_deleted'][$line])?false:true),
                        'creater'                  => $this->Session->read('Auth.MstUser.id'),
                        'created'                  => $now,
                        'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                        'modified'                 => $now
                        )
                    );
            }
        }

        if (!$this->MstSalesConfig->saveAll($save_data , array('validates' => true,'atomic' => false))) {
            //更新失敗のためロールバック
            $this->MstSalesConfig->rollback();
            //エラーメッセージ
            $this->Session->setFlash('登録が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('mod');
        }
        
        $this->MstSalesConfig->commit();
        $this->set('result' , $save_data);
    }
    
    //編集画面用包装単位プルダウン作成用データ
    function setItemUnits() {
        
        $sql = ' select ';
        $sql.= '      a.id                   as "MstItemUnit__id"';
        $sql.= '    , (  ';
        $sql.= '      case  ';
        $sql.= '        when a.per_unit = 1  ';
        $sql.= '        then b.unit_name  ';
        $sql.= "        else b.unit_name || '(' || a.per_unit || c.unit_name || ')'  ";
        $sql.= '        end ';
        $sql.= '    )                        as "MstItemUnit__unit_name"  ';
        $sql.= '  from ';
        $sql.= '    mst_item_units as a  ';
        $sql.= '    left join mst_unit_names as b  ';
        $sql.= '      on b.id = a.mst_unit_name_id  ';
        $sql.= '    left join mst_unit_names as c  ';
        $sql.= '      on c.id = a.per_unit_name_id ';
        $sql.= '      where ';
        $sql.= '      a.mst_facility_item_id = ' . $this->request->data['MstFacilityItem']['id'];
        $sql.= ' order by a.per_unit ';
        
        $list = Set::Combine(
             $this->MstItemUnit->query($sql),
            "{n}.MstItemUnit.id",
            "{n}.MstItemUnit.unit_name"
            );
        
        return $list;
    }
    
    

    function report(){
        $params = $this->getSearchParamater();
        $SalesConfig_Info = $this->getReports($this->request->data['MstSalesConfig']['mst_facility_id'], $params);
        $this->print_config($SalesConfig_Info);
    }
    
    /**
     * 売上設定印刷
     * @param
     * @todo
     */
    function print_config($print_data){
        
        $columns = "センター名\t商品名\t規格\t製品番号\t販売元\t商品ID\t得意先\t包装単位\t適用開始日\t売上課税区分\t売上単価\t印刷年月日";

        $today = date("Y/m/d");//印刷年月日
        $tax_type = Configure::read('PrintFacilityItems.taxes');
        $output = array();
        
        $Search_condition  = "商品ID = ".$this->request->data['MstFacilityItem']['internal_code'];
        $Search_condition .= "・製品番号 = ".$this->request->data['MstFacilityItem']['item_code'];
        $Search_condition .= "・商品名 = ".$this->request->data['MstFacilityItem']['item_name'];
        $Search_condition .= "・販売元 = ".$this->request->data['MstFacilityItem']['dealer_name'];
        $Search_condition .= "・規格 = ".$this->request->data['MstFacilityItem']['standard'];
        $Search_condition .= '・JANコード = ' . $this->request->data['MstFacilityItem']['jan_code'];
        $Search_condition .= "・得意先 = ".$this->request->data['MstFacilityItem']['facilityName'];
        
        foreach($print_data as $data){
            //課税区分設定

            $taxType = "";
            if(isset($data['mstfacilityitem']['tax_type']) && $data['mstfacilityitem']['tax_type'] != null){
                $taxType = $tax_type[$data['mstfacilityitem']['tax_type']];
            }
            $output[] = array(
                $data['mstfacility']['facility_formal_name'],
                $data['mstfacilityitem']['item_name'],
                $data['mstfacilityitem']['standard'],
                $data['mstfacilityitem']['item_code'],
                $data['mstdealer']['dealer_name'],
                $data['mstfacilityitem']['internal_code'],
                $data['mstpartnerfacility']['facility_formal_name'],
                $data[0]['packing_name'],
                $data['mstsalesconfig']['start_date'],
                $taxType,
                $data['mstsalesconfig']['sales_price'],
                $today,
                );
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'売上設定印刷','検索条件'=>h($Search_condition));
        $layout_file = 'xml_output';
        $this->xml_output($output,$columns,$layout_name['21'],$fix_value,$layout_file);
    }
    
    
    /**
     * xml出力テスト用
     * @param
     * @todo 各コントローラの共通関数化

     */
    function xml_output($data,$columns,$layout_name,$fix_value,$layout_file){
        
        $this->layout = 'xml/default';
        $this->set('columns',$columns);
        $this->set('posted',$data);
        $this->set('layout_name',$layout_name);
        $this->set('fix_value',$fix_value); //連想配列のキーにname属性を指定
        
        $this->render($layout_file);
        
    }
    
    
    private function getItems($fid, $params){
        $where = ' 1 = 1 ';
        foreach($params['conditions'] as $_key => $_val){
            $v = (strlen(trim($_val)) == 0 ? '' : $_val);
            if(0 === preg_match('/LIKE|<|>/i', $_key)){
                $where .= " and {$_key} = '{$v}' ";
            }else{
                $where .= " and {$_key} '{$v}' ";
            }
        }
        
        $order = ' MstFacilityItem.internal_code asc';
        $limit = $this->_getLimitCount();
        
        $sql =
          ' select ' .
          ' MstFacilityItem.id              as "MstFacilityItem__id"'.
          ',MstFacilityItem.item_code       as "MstFacilityItem__item_code"'.
          ',MstFacilityItem.internal_code   as "MstFacilityItem__internal_code"'.
          ',MstFacilityItem.item_name       as "MstFacilityItem__item_name"'.
          ',MstFacilityItem.standard        as "MstFacilityItem__standard"'.
          ',MstFacilityItem.mst_facility_id as "MstFacilityItem__mst_facility_id"'.
          ',MstFacilityItem.tax_type        as "MstFacilityItem__tax_type"'.
          ',MstDealer.dealer_name           as "MstDealer__dealer_name"'.
          ' from ' .
          ' mst_facility_items as MstFacilityItem ' .
          ' left join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id ' .
          ' left join mst_sales_configs as MstSalesConfig on MstSalesConfig.mst_facility_item_id = MstFacilityItem.id' .
          ' left join mst_facilities as MstPartnerFacility on MstSalesConfig.partner_facility_id = MstPartnerFacility.id' .
          ' where ' .
          $where .
          ' and MstFacilityItem.is_deleted = false ' .
          ' and MstFacilityItem.item_type = ' . Configure::read('Items.item_types.normalitem') .
          " and MstFacilityItem.mst_facility_id = {$fid}" .
          ' group by ' .
          ' MstFacilityItem.id'.
          ',MstFacilityItem.item_code'.
          ',MstFacilityItem.internal_code'.
          ',MstFacilityItem.item_name'.
          ',MstFacilityItem.standard'.
          ',MstFacilityItem.mst_facility_id'.
          ',MstFacilityItem.tax_type'.
          ',MstDealer.dealer_name'.
          ' order by ' .
          $order ;
        //全件数取得
        $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));
        $sql.= " limit {$limit}";
        
        return $this->MstFacilityItem->query($sql);
    }
    
    
    private function getReports($fid, $params){
        $where = ' 1 = 1 ';
        foreach($params['conditions'] as $_key => $_val){
            if(0 === preg_match('/LIKE|<|>/i', $_key)){
                $where .= " and {$_key} = '{$_val}' ";
            }else{
                $where .= " and {$_key} '{$_val}' ";
            }
        }
        $sql =
          ' select ' .
          ' MstFacilityItem.id as MstFacilityItem__id'.
          ',MstFacilityItem.item_code as MstFacilityItem__item_code'.
          ',MstFacilityItem.internal_code as MstFacilityItem__internal_code'.
          ',MstFacilityItem.item_name as MstFacilityItem__item_name'.
          ',MstFacilityItem.standard as MstFacilityItem__standard'.
          ',MstFacilityItem.mst_facility_id as MstFacilityItem__mst_facility_id'.
          ',MstFacilityItem.tax_type as MstFacilityItem__tax_type'.
          ',MstDealer.dealer_name as MstDealer__dealer_name'.
          ",(case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')' end) as packing_name".
          ",to_char(MstSalesConfig.start_date,'yyyy/mm/dd') as MstSalesConfig__start_date" .
          ',MstSalesConfig.sales_price as MstSalesConfig__sales_price' .
          ',MstFacility.facility_formal_name as MstFacility__facility_formal_name'.
          ',MstPartnerFacility.facility_formal_name as MstPartnerFacility__facility_formal_name'.
          ' from ' .
          ' mst_facility_items as MstFacilityItem ' .
          ' left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id ' .
          ' inner join mst_item_units as MstItemUnit on MstFacilityItem.id = MstItemUnit.mst_facility_item_id'.
          ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id'.
          ' inner join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id'.
          ' inner join mst_sales_configs as MstSalesConfig on MstSalesConfig.mst_facility_item_id = MstFacilityItem.id and MstSalesConfig.mst_item_unit_id = MstItemUnit.id '.
          ' inner join mst_facilities as MstFacility on MstSalesConfig.mst_facility_id = MstFacility.id and MstFacility.id = '. $fid .
          ' inner join mst_facilities as MstPartnerFacility on MstSalesConfig.partner_facility_id =MstPartnerFacility.id'.
          ' inner join mst_facility_relations as MstFacilityRelation on MstFacilityRelation.mst_facility_id ='. $fid .' and MstFacilityRelation.partner_facility_id = MstPartnerFacility.id '.
     
          ' where ' .
          $where .
          ' and MstFacilityItem.is_deleted = false ' .
          ' and MstSalesConfig.is_deleted = false ' .
          ' order by ' .
          ' MstFacility.id asc, MstFacilityItem.id asc, MstSalesConfig.start_date asc' ;
        return $this->MstFacilityItem->query($sql);
    }
    
    
    // センターID、病院IDから施設関連IDを取得する
    private function _getRelationID( $c_id , $h_id ){
        $sql  = ' select ';
        $sql .= '       id  ';
        $sql .= '   from ';
        $sql .= '     mst_facility_relations as a  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_id = ' . $c_id;
        $sql .= '     and a.partner_facility_id = ' . $h_id;
        
        $ret = $this->MstFacilityRelation->query($sql);

        return $ret[0][0]['id'];
    }
}
