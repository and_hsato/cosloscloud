<?php
/**
 * FixedCountsController
 * マスタ管理系 定数管理
 * @version 1.0.0
 * @since 2010/05/18
 */
class FixedCountsController extends AppController {
    
    /**
     * @var $name
     */
    var $name = 'FixedCounts';
    
    /**
     * @var array $uses
     */
    var $uses = array('MstUser'
                      ,'MstDealer'
                      ,'TrnStock'
                      ,'MstItemUnit'
                      ,'MstUnitName'
                      ,'MstShelfName'
                      ,'MstFixedCount'
                      ,'MstFacility'
                      ,'MstDepartment'
                      ,'MstFacilityItem'
                      ,'MstSalesConfig'
                      ,'MstConst'
                      );
    
    /**
     * @var bool $scaffold
     */
    //var $scaffold;
    
    /**
     * @var array $helpers
   */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');
    
    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Common');
    
    /**
     * @var array $
     */
    //ログインなしでの印刷を許可
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
    }
    
    /**
     *
     */
    function index (){
    }
    
    /**
     * @param
     * @return
     */
    function list_by_item() {
        App::import('Sanitize');
        $this->setRoleFunction(83); //商品別病院定数
        
        $result = array();
        
        //部署コンボ
        $this->set('department_list' , $this->getHospitalDepartmentList($this->Session->read('Auth.facility_id_selected')));

        $where = '';
        if(isset($this->request->data['search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            
            //商品ID(完全一致)
            if(isset($this->request->data['MstFixedCount']['internal_code']) && $this->request->data['MstFixedCount']['internal_code'] != '' ){
                $where .= " and a.internal_code = '" . $this->request->data['MstFixedCount']['internal_code'] . "'";
            }
            //jan_code(前方一致)
            if(isset($this->request->data['MstFixedCount']['jan_code']) && $this->request->data['MstFixedCount']['jan_code'] != ''){
                $where .= " and a.jan_code like '". $this->request->data['MstFixedCount']['jan_code'] ."%'";
            }
            //製品番号
            if(isset($this->request->data['MstFixedCount']['item_code']) && $this->request->data['MstFixedCount']['item_code'] != ''){
                $where .= " and a.item_code like '%".$this->request->data['MstFixedCount']['item_code']."%'";
            }
            //商品名
            if(isset($this->request->data['MstFixedCount']['item_name']) && $this->request->data['MstFixedCount']['item_name'] != ''){
                $where .= " and a.item_name like '%" .$this->request->data['MstFixedCount']['item_name']."%'";
            }
            //規格
            if(isset($this->request->data['MstFixedCount']['standard']) && $this->request->data['MstFixedCount']['standard'] != ''){
                $where .= " and a.standard like '%".$this->request->data['MstFixedCount']['standard']."%'";
            }
            //販売元
            if(isset($this->request->data['MstFixedCount']['dealer_name']) && $this->request->data['MstFixedCount']['dealer_name'] != ''){
                $where .= " and d.dealer_name like '%".$this->request->data['MstFixedCount']['dealer_name']."%'";
            }

            //部署
            if(isset($this->request->data['MstFixedCount']['mst_department_id']) && $this->request->data['MstFixedCount']['mst_department_id'] != ''){
                $where .= " and b.mst_department_id = ".$this->request->data['MstFixedCount']['mst_department_id'];
            }

            $sql  = ' select ';
            $sql .= '       a.id            as "MstFixedCount__id"';
            $sql .= '     , a.internal_code as "MstFixedCount__internal_code"';
            $sql .= '     , a.item_name     as "MstFixedCount__item_name"';
            $sql .= '     , a.standard      as "MstFixedCount__standard"';
            $sql .= '     , a.item_code     as "MstFixedCount__item_code"';
            $sql .= '     , a.jan_code      as "MstFixedCount__jan_code"';
            $sql .= '     , d.dealer_name   as "MstFixedCount__dealer_name"';
            $sql .= '   from ';
            $sql .= '     mst_facility_items as a ';
            $sql .= '     left join mst_dealers as d ';
            $sql .= '       on d.id = a.mst_dealer_id ';
            $sql .= '     left join mst_fixed_counts as b ';
            $sql .= '       on b.mst_facility_item_id = a.id ';
            $sql .= '   where ';
            $sql .= '     a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     and a.is_lowlevel = false ';
            $sql .= $where;
            $sql .= '   group by ';
            $sql .= '     a.id ';
            $sql .= '     , a.internal_code ';
            $sql .= '     , a.item_name ';
            $sql .= '     , a.standard ';
            $sql .= '     , a.item_code ';
            $sql .= '     , a.jan_code ';
            $sql .= '     , d.dealer_name ';
            $sql .= '   order by ';
            $sql .= '     a.internal_code ';
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFixedCount'));
            
            $sql .= (isset($this->request->data['MstFixedCount']['limit'])?' limit ' .$this->request->data['MstFixedCount']['limit'] : '' );
            
            $result = $this->MstFixedCount->query($sql);
            $this->request->data = $data;
        }
        
        //初期表示
        $this->set('SearchResult',$result);
    }
    
    /**
     * 施設採用品に紐付く包装単位一覧を取得
     * @param
     * @return
     */
    function _unitlistFromFasItemId($facility_item_id){
        $sql  = ' select ';
        $sql .= '       a.id                   as "MstItemUnit__id"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.per_unit = 1  ';
        $sql .= '         then b.unit_name  ';
        $sql .= "         else b.unit_name || '(' || a.per_unit || c.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                        as "MstItemUnit__name"';
        $sql .= '   from ';
        $sql .= '     mst_item_units as a  ';
        $sql .= '     left join mst_unit_names as b  ';
        $sql .= '       on b.id = a.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on c.id = a.per_unit_name_id  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_item_id = ' . $facility_item_id;
        $sql .= '     and a.is_deleted = false  ';
        $sql .= '   order by ';
        $sql .= '     a.per_unit ';
        
        $list = Set::Combine(
            $this->MstItemUnit->query($sql),
            "{n}.MstItemUnit.id",
            "{n}.MstItemUnit.name"
            );
        return $list;
    }
    
    /**
     * @param
     * @return
     */
    function edit_by_item() {
        //更新時間チェック用
        $this->Session->write('FixedByItem.readTime',date('Y-m-d H:i:s'));

        //包装単位セレクト
        $this->set('unitSelecter',$this->_unitlistFromFasItemId($this->request->data['MstFacilityItem']['id']));
        
        $list = array();
        if(isset($this->request->data['MstFixedCount']['is_search'])){
            //検索ボタンが押された
            $params = $displayData = array();
            //セレクトでの表示件数操作
            $params['limit'] = $this->request->data['MstFixedCount']['limit'];
            //初期値
            if(!isset($this->request->data['selected']['onlySetDepts'])) $this->request->data['selected']['onlySetDepts'] = 1;
            
            $sql  =' select  ';
            $sql .='     a.department_name       as "MstFacility__Facility_Name" , ';
            $sql .='     f.id                    as "MstFixedCount__mst_facility_id" , ';
            $sql .='     a.id                    as "MstFixedCount__department_id" , ';
            $sql .='     b.id                    as "MstFixedCount__id" , ';
            $sql .='     b.trn_stock_id          as "MstFixedCount__trn_stock_id" , ';
            $sql .='     b.mst_item_unit_id      as "MstFixedCount__mst_item_unit_id" , ';
            $sql .='     c.id                    as "MstFixedCount__mst_facility_item_id" , ';
            $sql .='     b.fixed_count           as "MstFixedCount__fixed_count", ';
            $sql .='     b.holiday_fixed_count   as "MstFixedCount__holiday_fixed_count", ';
            $sql .='     b.spare_fixed_count     as "MstFixedCount__spare_fixed_count", ';
            $sql .='     b.trade_type            as "MstFixedCount__trade_type", ';
            $sql .='     b.print_type            as "MstFixedCount__print_type",';
            $sql .='     b.mst_shelf_name_id     as "MstFixedCount__mst_shelf_name_id", ';
            $sql .='     g.name                  as "MstFixedCount__shelf_name", ';
            $sql .='     b.start_date            as "MstFixedCount__start_date", ';
            $sql .='     b.substitute_unit_id    as "MstFixedCount__substitute_unit_id", ';
            $sql .='     b.is_deleted            as "MstFixedCount__is_deleted" , ';
            
            $sql .='     ( case ';
            $sql .='           when b.substitute_unit_id is not null then 0 ';
            $sql .='           when i.id is not null then 0 ';
            $sql .='           else 1 ';
            $sql .='       end )                 as "MstFixedCount__check"';
            
            $sql .='   , ( case when h.requested_count > 0 then 1 else 0 end ) ';
            $sql .='                             as "MstFixedCount__err"';
            $sql .=' from ';
            $sql .='     mst_departments as a ';
            $sql .=' inner join ';
            $sql .='     mst_facility_relations as r ';
            $sql .='     on r.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
            $sql .=' inner join ';
            $sql .='     mst_facilities as f ';
            $sql .='     on a.mst_facility_id = f.id ';
            
            if(isset($this->request->data['selected']['partner_facility']) && !empty($this->request->data['selected']['partner_facility'])){
                $sql .="     and f.facility_code = '" . $this->request->data['selected']['partner_facility'] . "' ";
            }
            $sql .='     and r.partner_facility_id = f.id ';
            $sql .='     and f.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .=' inner join  ';
            $sql .='     mst_facility_items as c ';
            $sql .='     on c.mst_facility_id = r.mst_facility_id ';
            
            //設定済みを表示
            if($this->request->data['selected']['onlySetDepts'] == "1"){
                $sql .=' inner join  ';
            }else{
                $sql .=' left join  ';
            }
            $sql .='     mst_fixed_counts as b ';
            $sql .='     on b.mst_department_id = a.id ';
            $sql .='     and b.mst_facility_item_id = c.id ';
            $sql .=' left join  ';
            $sql .='     mst_shelf_names as g ';
            $sql .='     on g.id = b.mst_shelf_name_id ';
            $sql .=' left join  ';
            $sql .='     trn_stocks as h ';
            $sql .='     on h.id = b.trn_stock_id ';

            $sql .=' left join ';
            $sql .='     mst_fixed_counts as i';
            $sql .='     on i.substitute_unit_id = b.id ';

            $sql .=' inner join mst_user_belongings as j ';
            $sql .='     on j.mst_facility_id = f.id ';
            $sql .='     and j.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .='     and j.is_deleted = false';
            
            $sql .=' where  ';
            $sql .='     a.is_deleted = false ';
            
            if($this->request->data['selected']['onlyValidDepts'] == "1"){
                $sql .='     and b.is_deleted = false ';
                $sql .='     and b.fixed_count > 0 ';
            }
            
            $sql .=' and c.id = '.$this->request->data['MstFacilityItem']['id'];
            $sql .=' order by f.facility_code , a.department_code ';
            
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFixedCount'));
            
            $sql .=' limit ' . $params['limit'];
            
            $list = $this->MstFixedCount->query($sql);


            foreach($list as &$item){
                //部署に紐付く棚を取得
                $item['MstFixedCount']['selecter'] =  $this->_getShelfList($item['MstFixedCount']['department_id']);
                //売上設定を確認
                $item['MstFixedCount']['salesConfig'] = $this->_getSalesConfigList($item['MstFixedCount']['mst_facility_id'] ,
                                                                                  $item['MstFixedCount']['mst_facility_item_id']
                                                                                  );
            }
            
            unset($item);
            
        }else{
            //初期表示
            //ヘッダに表示する商品情報
            $sql  = ' select ';
            $sql .= '       a.id            as "MstFacilityItem__id"';
            $sql .= '     , a.internal_code as "MstFacilityItem__internal_code"';
            $sql .= '     , a.item_code     as "MstFacilityItem__item_code"';
            $sql .= '     , a.item_name     as "MstFacilityItem__item_name"';
            $sql .= '     , a.standard      as "MstFacilityItem__standard"';
            $sql .= '     , a.jan_code      as "MstFacilityItem__jan_code"';
            $sql .= '     , a.is_lowlevel   as "MstFacilityItem__is_lowlevel"';
            $sql .= '     , a.item_type     as "MstFacilityItem__item_type"';
            $sql .= '     , b.dealer_name   as "MstFacilityItem__dealer_name"';
            $sql .= '   from ';
            $sql .= '     mst_facility_items as a  ';
            $sql .= '     left join mst_dealers as b  ';
            $sql .= '       on b.id = a.mst_dealer_id  ';
            $sql .= '   where ';
            $sql .= '     a.id = ' . $this->request->data['MstFacilityItem']['id'];
            $tmp = $this->MstFacilityItem->query($sql);
            $this->request->data = $tmp[0];
        }

        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
        
        $this->set('displayData',$list);
    }
    
    /**
     * 定数設定商品別結果画面
     */
    function result_by_item () {
        //トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            $result = $this->Session->read('result');
            $this->Session->delete('result');
            $this->Set('result', $result);
            $this->render('result_by_item');
            return;
        }
        
        $result = array();
        $mst_fixed_count_ids = array();
        $now = date('Y/m/d H:i:s');

        $this->MstFixedCount->begin();

        //更新データのみ再取得
        $recheckedId = array();
        foreach($this->request->data['MstFixedCount']['id'] as $key => $val){
            if(!empty($val) && $val !== 'a'){
                $recheckedId[] = $val;
            }
        }
        //更新データがある場合
        if (!empty($recheckedId)){
            //行ロック
            $this->MstFixedCount->query(' select * from mst_fixed_counts as a where a.id in (' . join(',',$recheckedId) . ') for update ');
            //更新チェック
            $ret = $this->MstFixedCount->query(" select count(*) from mst_fixed_counts as a where a.id in (" . join(',',$recheckedId) . ") and a.modified > '" . $this->request->data['MstFacilityItem']['time'] . "'");
            if( $ret[0][0]['count'] > 0 ){
                $this->MstFixedCount->rollback();
                $this->Session->setFlash('別ユーザによって更新されています', 'growl', array('icon'=>'error'));
                $this->redirect('list_by_item');
                return;
            }
        }
        foreach($this->request->data['MstFixedCount']['id'] as $line => $id){
            //在庫レコード存在チェック
            $stock_id = $this->getStockRecode($this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                                              $this->request->data['MstFixedCount']['mst_department_id'][$line] );
            if($id == 'a'){
                $order_point = 0;
                if($this->request->data['MstFixedCount']['trade_type'][$line]== Configure::read('TeisuType.Deposit') ||
                   $this->request->data['MstFixedCount']['trade_type'][$line]== Configure::read('TeisuType.NonStock')){
                    $order_point = $this->request->data['MstFixedCount']['fixed_count'][$line];
                }
                
                //新規作成
                $fixedcount = array(
                    'MstFixedCount' => array(
                        'trn_stock_id'         => $stock_id,
                        'mst_facility_item_id' => $this->request->data['MstFacilityItem']['id'],
                        'mst_item_unit_id'     => $this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                        'mst_facilities_id'    => $this->request->data['MstFixedCount']['mst_facility_id'][$line],
                        'mst_department_id'    => $this->request->data['MstFixedCount']['mst_department_id'][$line],
                        'order_point'          => $order_point,
                        'fixed_count'          => $this->request->data['MstFixedCount']['fixed_count'][$line],
                        'holiday_fixed_count'  => $this->request->data['MstFixedCount']['holiday_fixed_count'][$line],
                        'spare_fixed_count'    => $this->request->data['MstFixedCount']['spare_fixed_count'][$line],
                        'trade_type'           => $this->request->data['MstFixedCount']['trade_type'][$line],
                        'start_date'           => $this->request->data['MstFixedCount']['start_date'][$line],
                        'creater'              => $this->Session->read('Auth.MstUser.id'),
                        'created'              => $now,
                        'modifier'             => $this->Session->read('Auth.MstUser.id'),
                        'modified'             => $now,
                        )
                    );
                
            }else{
                $order_point = 0;
                if($this->request->data['MstFixedCount']['trade_type'][$line]== Configure::read('TeisuType.Deposit') ||
                   $this->request->data['MstFixedCount']['trade_type'][$line]== Configure::read('TeisuType.NonStock')){
                    $order_point = $this->request->data['MstFixedCount']['fixed_count'][$line];
                }
                
                //更新
                $fixedcount = array(
                    'MstFixedCount' => array(
                        'id'                   => $id,
                        'trn_stock_id'         => $stock_id,
                        'mst_facility_item_id' => $this->request->data['MstFacilityItem']['id'],
                        'mst_item_unit_id'     => $this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                        'mst_facilities_id'    => $this->request->data['MstFixedCount']['mst_facility_id'][$line],
                        'mst_department_id'    => $this->request->data['MstFixedCount']['mst_department_id'][$line],
                        'order_point'          => $order_point,
                        'fixed_count'          => $this->request->data['MstFixedCount']['fixed_count'][$line],
                        'holiday_fixed_count'  => $this->request->data['MstFixedCount']['holiday_fixed_count'][$line],
                        'spare_fixed_count'    => $this->request->data['MstFixedCount']['spare_fixed_count'][$line],
                        'trade_type'           => $this->request->data['MstFixedCount']['trade_type'][$line],
                        'start_date'           => $this->request->data['MstFixedCount']['start_date'][$line],
                        'modifier'             => $this->Session->read('Auth.MstUser.id'),
                        'modified'             => $now,
                        )
                    );
            }
            $this->MstFixedCount->create();
            $fRes = $this->MstFixedCount->save($fixedcount);
            if($fRes === FALSE){
                $this->MstFixedCount->rollback();
                $this->Session->setFlash('定数作成処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('list_by_item');
            }
            if($id == 0) {
                $id = $this->MstFixedCount->getLastInsertID();
            }
            
            $mst_fixed_count_ids[] = $id;
            
        }
        //更新データがある場合
        if (!empty($recheckedId)){
            //コミット直前に更新チェック
            $ret = $this->MstFixedCount->query(" select count(*) from mst_fixed_counts as a where a.id in (" . join(',',$recheckedId) . ") and a.modified > '" . $this->request->data['MstFacilityItem']['time'] . "' and a.modified <> '" . $now . "'");
            if( $ret[0][0]['count'] > 0 ){
                $this->MstFixedCount->rollback();
                $this->Session->setFlash('別ユーザによって更新されています', 'growl', array('icon'=>'error'));
                $this->redirect('list_by_item');
                return;
            }
        }
        
        //部署_包装単位で重複していたらダメ
        $sql  = ' select ';
        $sql .= '       count(a.*)  ';
        $sql .= '   from ';
        $sql .= '     mst_fixed_counts a  ';
        $sql .= '     inner join mst_facilities b  ';
        $sql .= '       on a.mst_facilities_id = b.id  ';
        $sql .= '       and b.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = a.mst_facility_item_id  ';
        $sql .= '   where ';
        $sql .= '     c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
        $sql .= '   group by ';
        $sql .= '     a.mst_department_id ';
        $sql .= '     , a.mst_item_unit_id  ';
        $sql .= '   having ';
        $sql .= '     count(a.*) > 1 ';
        $ret = $this->MstFixedCount->query($sql);
        if( !empty($ret)){
            $this->MstFixedCount->rollback();
            $this->Session->setFlash('同じ部署、同じ包装単位が重複しています。', 'growl', array('icon'=>'error'));
            $this->redirect('list_by_item');
            return;
        }
        
        $this->MstFixedCount->commit();
        
        /* 完了画面の描画用データ取得 */
        $result = $this->getResultData($mst_fixed_count_ids);
        
        $this->set('result' , $result);
        $this->Session->write('result',$result);
    }
    
    /**
     * 部署別定数設定初期画面
     * @param
     * @return
     */
    function list_by_depart() {
        App::import('Sanitize');
        $this->setRoleFunction(84); //部署別病院定数
        //施設のコンボ
        $this->set('FacilityList',$this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                         array(Configure::read('FacilityType.hospital')) ,
                                                         true,
                                                         array('id' , 'facility_name')
                                                         ));
        $result = array();
        //検索ボタン押下
        if(isset($this->request->data['MstFixedCount']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            
            $where = '';
            //画面から入力された検索項目
            if($this->request->data['MstFixedCount']['mst_facility_id'] != ""){
                $where .=' and a.partner_facility_id = ' . $this->request->data['MstFixedCount']['mst_facility_id'];
            }

            if($this->request->data['MstFixedCount']['internal_code'] != ""){
                $where .=" and f.internal_code like '%" . $this->request->data['MstFixedCount']['internal_code'] . "%'";
            }
            
            if($this->request->data['MstFixedCount']['item_name'] != ""){
                $where .=" and f.item_name like '%" . $this->request->data['MstFixedCount']['item_name'] . "%'";
            }
            if($this->request->data['MstFixedCount']['item_code'] != ""){
                $where .=" and f.item_code like '%" . $this->request->data['MstFixedCount']['item_code'] . "%'";
            }
            if($this->request->data['MstFixedCount']['standard'] != ""){
                $where .=" and f.standard like '%" . $this->request->data['MstFixedCount']['standard'] . "%'";
            }
            if($this->request->data['MstFixedCount']['jan_code'] != ""){
                $where .=" and f.jan_code like '" . $this->request->data['MstFixedCount']['jan_code'] . "%'";
            }
            if($this->request->data['MstFixedCount']['dealer_name'] != ""){
                $where .=" and g.dealer_name like '%" . $this->request->data['MstFixedCount']['dealer_name'] . "'";
            }

            $sql  = ' select ';
            $sql .= '       c.id              as "MstFixedCount__mst_department_id"';
            $sql .= '     , b.facility_name   as "MstFixedCount__facility_name"';
            $sql .= '     , c.department_name as "MstFixedCount__department_name" ' ;
            $sql .= '   from ';
            $sql .= '     mst_facility_relations as a  ';
            $sql .= '     left join mst_facilities as b  ';
            $sql .= '       on b.id = a.partner_facility_id  ';
            $sql .= '     left join mst_departments as c  ';
            $sql .= '       on c.mst_facility_id = b.id  ';
            $sql .= '     left join mst_fixed_counts as d  ';
            $sql .= '       on d.mst_department_id = c.id  ';
            $sql .= '     left join mst_item_units as e  ';
            $sql .= '       on e.id = d.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as f  ';
            $sql .= '       on f.id = e.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as g  ';
            $sql .= '       on g.id = f.mst_dealer_id  ';
            $sql .= '     inner join mst_user_belongings as h  ';
            $sql .= '       on h.mst_facility_id = b.id  ';
            $sql .= '       and h.mst_user_id =  ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '       and h.is_deleted = false ';
            $sql .= '   where ';
            $sql .= '     a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     and b.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .= $where;
            $sql .= '   group by ';
            $sql .= '     c.id ';
            $sql .= '     , b.facility_name ';
            $sql .= '     , c.department_name ';
            $sql .= '     , b.facility_code ';
            $sql .= '     , c.department_code  ';
            $sql .= '   order by ';
            $sql .= '     b.facility_code ';
            $sql .= '     , c.department_code ';

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFixedCount'));
            
            $sql .= '   limit ' . $this->request->data['limit'];
            
            $result = $this->MstFixedCount->query($sql);
            $this->request->data = $data;
        }
        
        $this->set('result' , $result);
        $this->render('list_by_depart');
    }
    
    /**
     * 帳票出力処理
     * @param
     * @return
     */
    function report() {
        //帳票データ
        $sql  = ' select ';
        $sql .= '       a.department_code  as "MstFixedCount__department_code"';
        $sql .= '     , h.facility_name    as "MstFixedCount__facility_name"';
        $sql .= '     , a.department_name  as "MstFixedCount__department_name"';
        $sql .= '     , d.internal_code    as "MstFixedCount__internal_code"';
        $sql .= '     , d.item_name        as "MstFixedCount__item_name"';
        $sql .= '     , d.item_code        as "MstFixedCount__item_code"';
        $sql .= '     , d.standard         as "MstFixedCount__standard" ';
        $sql .= '     , e.dealer_name      as "MstFixedCount__dealer_name"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when c.per_unit = 1  ';
        $sql .= '         then f.unit_name  ';
        $sql .= "         else f.unit_name || '(' || c.per_unit || g.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                            as "MstFixedCount__unit_name" ';
        $sql .= '     , b.fixed_count              as "MstFixedCount__fixed_count"';
        $sql .= '     , b.holiday_fixed_count      as "MstFixedCount__holiday_fixed_count"';
        $sql .= '     , b.spare_fixed_count        as "MstFixedCount__spare_fixed_count"';
        $sql .= '     , l.name                     as "MstFixedCount__shelf_name"';
        $sql .= '     , (  ';
        $sql .= '       case b.trade_type  ';
        foreach(Configure::read('TeisuType.List') as $k => $v){
            $sql .= "         when " . $k . " then '" . $v . "'";
        }
        $sql .= '         end ';
        $sql .= '     )                           as "MstFixedCount__trade_type"';
        $sql .= '     , k.internal_code           as "MstFixedCount__sub_internal_code"';
        $sql .= '   from ';
        $sql .= '     mst_departments as a  ';
        $sql .= '     left join mst_fixed_counts as b  ';
        $sql .= '       on b.mst_department_id = a.id  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on c.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as d  ';
        $sql .= '       on d.id = c.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on e.id = d.mst_dealer_id  ';
        $sql .= '     left join mst_unit_names as f  ';
        $sql .= '       on f.id = c.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as g  ';
        $sql .= '       on g.id = c.per_unit_name_id  ';
        $sql .= '     left join mst_facilities as h  ';
        $sql .= '       on h.id = a.mst_facility_id  ';
        $sql .= '     left join mst_fixed_counts as i  ';
        $sql .= '       on i.id = b.substitute_unit_id  ';
        $sql .= '     left join mst_item_units as j  ';
        $sql .= '       on j.id = i.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as k  ';
        $sql .= '       on k.id = j.mst_facility_item_id  ';
        $sql .= '     left join mst_shelf_names as l  ';
        $sql .= '       on l.id = b.mst_shelf_name_id  ';
        $sql .= '   where ';
        $sql .= '     a.id =  ' . $this->request->data['MstFixedCount']['mst_department_id'];
        $sql .= '     and b.is_deleted = false   ';
        $sql .= '   order by ';
        $sql .= '     l.name ,';
        $sql .= '     d.internal_code ';
        $result = $this->MstFixedCount->query($sql);
        
        $output = array();
        foreach($result as $res){
            $output[] = array( $res['MstFixedCount']['department_code']//施設部署コード
                              ,$res['MstFixedCount']['facility_name'] .' '. $res['MstFixedCount']['department_name']//施設部署名
                              ,$res['MstFixedCount']['item_name']//商品名
                              ,$res['MstFixedCount']['standard']//規格
                              ,$res['MstFixedCount']['item_code']//製品番号
                              ,$res['MstFixedCount']['dealer_name']//販売元
                              ,$res['MstFixedCount']['trade_type']//管理区分
                              ,$res['MstFixedCount']['shelf_name']//棚番号
                              ,$res['MstFixedCount']['unit_name']//包装単位
                              ,$res['MstFixedCount']['sub_internal_code']//代替商品Id
                              ,$res['MstFixedCount']['internal_code']//商品ID
                              ,$res['MstFixedCount']['fixed_count']//定数
                              ,$res['MstFixedCount']['holiday_fixed_count']//休日定数
                              ,$res['MstFixedCount']['spare_fixed_count']//予備定数
                              ,date('Y/m/d')
                              );
        }
        
        $columns = array(
            "施設部署コード",
            "施設部署名",
            "商品名",
            "規格",
            "製品番号",
            "販売元",
            "管理区分",
            "棚番号",
            "包装単位",
            "代替商品ID",
            "商品ID",
            "定数",
            "休日定数",
            "予備定数",
            "印刷年月日"
            );
        $layout_name = Configure::read('layoutnameModels');
        $layout_file = "xml_output";
        $fix_value = array('タイトル'=>'部署別定数一覧');
        $this->xml_output($output,join("\t",$columns),$layout_name[22],$fix_value,$layout_file);
    }
    
    /**
     * 部署別定数設定部署選択画面
     * @param
     * @return
     */
    function select_by_depart() {
        App::import('Sanitize');
        
        $where = '';
        $where2 = '';
        $result = array();
        $cart = array();
        
        //検索ボタン押下
        if(isset($this->request->data['MstFixedCount']['add_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            $where .= ' and a.is_lowlevel = false ';
            //商品ID
            if(isset($this->request->data['MstFixedCount']['internal_code']) &&  $this->request->data['MstFixedCount']['internal_code'] != '' ){
                $where .= " and a.internal_code like  '%" . $this->request->data['MstFixedCount']['internal_code'] . "%'";
            }
            //商品名
            if(isset($this->request->data['MstFixedCount']['item_name']) &&  $this->request->data['MstFixedCount']['item_name'] != '' ){
                $where .= " and a.item_name like  '%" . $this->request->data['MstFixedCount']['item_name'] . "%'";
            }
            //製品番号
            if(isset($this->request->data['MstFixedCount']['item_code']) &&  $this->request->data['MstFixedCount']['item_code'] != '' ){
                $where .= " and a.item_code like  '%" . $this->request->data['MstFixedCount']['item_code'] . "%'";
            }
            //規格
            if(isset($this->request->data['MstFixedCount']['standard']) &&  $this->request->data['MstFixedCount']['standard'] != '' ){
                $where .= " and a.standard like  '%" . $this->request->data['MstFixedCount']['standard'] . "%'";
            }
            //JANコード
            if(isset($this->request->data['MstFixedCount']['jan_code']) &&  $this->request->data['MstFixedCount']['jan_code'] != '' ){
                $where .= " and a.jan_code like  '%" . $this->request->data['MstFixedCount']['jan_code'] . "%'";
            }
            //販売元
            if(isset($this->request->data['MstFixedCount']['dealer_name']) &&  $this->request->data['MstFixedCount']['dealer_name'] != '' ){
                $where .= " and b.dealer_name like  '%" . $this->request->data['MstFixedCount']['dealer_name'] . "%'";
            }
            
            if($this->request->data['MstFixedCount']['tempId'] != ''){
                $where  .= ' and a.id not in (' . $this->request->data['MstFixedCount']['tempId']. ') ';
                $where2 .= ' and a.id in (' . $this->request->data['MstFixedCount']['tempId']. ')';
            }else{
                $where2 .= ' and 0 = 1 ';
            }

            
            
            $result = $this->_getFacilityItems($where , $this->request->data['limit']);
            $tmp_cart = $this->_getFacilityItems($where2);
            // カートに入れた順に並べ替える。
            if(!empty($tmp_cart)){
                //カートに追加した順に並べなおす。
                foreach( explode( ',', $this->request->data['MstFixedCount']['tempId']) as $id){
                    foreach($tmp_cart as $tmp){
                        if($tmp['MstFixedCount']['id'] == $id){
                            $cart[] = $tmp;
                            break;
                        }
                    }
                }
            }
            
            $this->request->data = $data;
            
        }else{
            //編集ボタンで画面遷移した場合には、施設,部署を取得する
            if(isset($this->request->data['MstFixedCount']['mst_facility_id'])){
                $this->_getHeaderData($this->request->data['MstFixedCount']['mst_department_id']);
            }
        }
        
        $this->set('result' , $result);
        $this->set('cart' , $cart);
    }
    
    /**
     * 部署別定数設定編集画面
     * @param
     * @return
     */
    function edit_by_depart() {
        App::import('Sanitize');
        
        $result =$editArray= array();
        //更新時間チェック用
        $this->Session->write('FixedByItem.readTime',date('Y-m-d H:i:s'));
        //取引区分
        $this->set('setPromsType',Configure::read('TeisuType.List'));

        // 印刷設定
        $this->set('print_type', $this->MstConst->find('list' ,
                                                          array(
                                                              'conditions'=> array('const_group_cd'=>Configure::read('ConstGroupType.printerSetting')) ,
                                                              'fields'=> array('const_cd',
                                                                               'const_nm'),
                                                              'order' => array('const_cd')
                                                              )
                                                      )
                   );

        
        //新規 or 編集で検索ボタン押下で検索実施
        if(isset($this->request->data['MstFacilityItem']) || isset($this->request->data['MstFixedCount']['is_search'])){
            
            //編集ボタンで画面遷移した場合には、施設,部署を取得する
            if(!isset($this->request->data['MstFacilityItem'])){
                $this->_getHeaderData($this->request->data['MstFixedCount']['mst_department_id']);
            }
            
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            
            //画面から入力した検索項目
            $where = '';
            //商品ID
            if(isset($this->request->data['MstFixedCount']['internal_code']) &&  $this->request->data['MstFixedCount']['internal_code'] != '' ){
                $where .= " and a.internal_code like  '%" . $this->request->data['MstFixedCount']['internal_code'] . "%'";
            }
            //商品名
            if(isset($this->request->data['MstFixedCount']['item_name']) &&  $this->request->data['MstFixedCount']['item_name'] != '' ){
                $where .= " and a.item_name like  '%" . $this->request->data['MstFixedCount']['item_name'] . "%'";
            }
            //製品番号
            if(isset($this->request->data['MstFixedCount']['item_code']) &&  $this->request->data['MstFixedCount']['item_code'] != '' ){
                $where .= " and a.item_code like  '%" . $this->request->data['MstFixedCount']['item_code'] . "%'";
            }
            //規格
            if(isset($this->request->data['MstFixedCount']['standard']) &&  $this->request->data['MstFixedCount']['standard'] != '' ){
                $where .= " and a.standard like  '%" . $this->request->data['MstFixedCount']['standard'] . "%'";
            }
            //JANコード
            if(isset($this->request->data['MstFixedCount']['jan_code']) &&  $this->request->data['MstFixedCount']['jan_code'] != '' ){
                $where .= " and a.jan_code like  '%" . $this->request->data['MstFixedCount']['jan_code'] . "%'";
            }
            //販売元
            if(isset($this->request->data['MstFixedCount']['dealer_name']) &&  $this->request->data['MstFixedCount']['dealer_name'] != '' ){
                $where .= " and f.dealer_name like  '%" . $this->request->data['MstFixedCount']['dealer_name'] . "%'";
            }
            //管理区分
            if(isset($this->request->data['MstFixedCount']['type']) &&  $this->request->data['MstFixedCount']['type'] != '' ){
                $where .= " and b.trade_type = " . $this->request->data['MstFixedCount']['type'] ;
            }
            //有効な定数設定のみ表示
            if(isset($this->request->data['selected']['onlyValidItems']) && $this->request->data['selected']['onlyValidItems'] == '1'){
                $where .= ' and b.is_deleted = false ';
                $where .= ' and b.fixed_count > 0 ';
            }
            
            //新規検索
            $sql  = ' select ';
            $sql .= '       a.id                   as "MstFixedCount__mst_facility_item_id" ';
            $sql .= '     , a.internal_code        as "MstFixedCount__internal_code" ';
            $sql .= '     , a.item_name            as "MstFixedCount__item_name" ';
            $sql .= '     , a.item_code            as "MstFixedCount__item_code" ';
            $sql .= '     , a.standard             as "MstFixedCount__standard" ';
            $sql .= '     , f.dealer_name          as "MstFixedCount__dealer_name" ';
            $sql .= '     , a.is_lowlevel          as "MstFixedCount__is_lowlevel" ';
            $sql .= '     , b.id                   as "MstFixedCount__id" ';
            $sql .= '     , b.mst_shelf_name_id    as "MstFixedCount__mst_shelf_name_id" ';
            $sql .= '     , b.mst_item_unit_id     as "MstFixedCount__mst_item_unit_id" ';
            $sql .= '     , b.print_type           as "MstFixedCount__print_type" ';
            $sql .= '     , b.trade_type           as "MstFixedCount__trade_type" ';
            $sql .= '     , b.fixed_count          as "MstFixedCount__fixed_count" ';
            $sql .= '     , b.holiday_fixed_count  as "MstFixedCount__holiday_fixed_count" ';
            $sql .= '     , b.spare_fixed_count    as "MstFixedCount__spare_fixed_count" ';
            $sql .= "     , to_char(b.start_date, 'YYYY/mm/dd') ";
            $sql .= '                              as "MstFixedCount__start_date" ';
            $sql .= '     , b.trn_stock_id         as "MstFixedCount__trn_stock_id" ';
            $sql .= '     , ( case when b.is_deleted = true then false else true  end  ) ';
            $sql .= '                              as "MstFixedCount__is_deleted" ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when d.requested_count > 0  ';
            $sql .= '         then true  ';
            $sql .= '         else false  ';
            $sql .= '         end ';
            $sql .= '     )                        as "MstFixedCount__err" ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when b.substitute_unit_id is not null  ';
            $sql .= '         then 0  ';
            $sql .= '         when e.id is not null  ';
            $sql .= '         then 0  ';
            $sql .= '         else 1  ';
            $sql .= '         end ';
            $sql .= '     )                        as "MstFixedCount__check"  ';
            $sql .= '   from ';
            $sql .= '     mst_facility_items as a  ';
            if(isset($this->request->data['MstFixedCount']['is_search'])){
                $sql .= '     inner join mst_fixed_counts as b  ';
            }else{
                $sql .= '     left join mst_fixed_counts as b  ';
            }
            $sql .= '       on b.mst_facility_item_id = a.id  ';
            $sql .= '      and b.mst_department_id = ' . $this->request->data['MstFixedCount']['mst_department_id'];
            $sql .= '     left join mst_departments as c  ';
            $sql .= '       on c.id = a.mst_dealer_id  ';
            $sql .= '     left join trn_stocks as d  ';
            $sql .= '       on d.id = b.trn_stock_id  ';
            $sql .= '     left join mst_fixed_counts as e  ';
            $sql .= '       on b.id = e.substitute_unit_id  ';
            $sql .= '     left join mst_dealers as f  ';
            $sql .= '       on f.id = a.mst_dealer_id  ';
            $sql .= '   where ';
            $sql .= '     a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            if(isset($this->request->data['MstFacilityItem']['id'])){
                $sql .= '     and a.id in ('. join(',',$this->request->data['MstFacilityItem']['id']) .') ';
            }
            
            $sql .= $where;
            $sql .= ' order by a.item_name ';
            
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFixedCount'));
            
            //Limit
            if(isset($this->request->data['MstFixedCount']['is_search'])){
                if(isset($this->request->data['limit'])){
                    $sql .= ' limit ' . $this->request->data['limit'];
                }else{
                    $sql .= ' limit ' . min(array_keys(Configure::read('displaycounts_combobox')));
                }
            }
            $result = $this->MstFixedCount->query($sql);

            // カートで選択した場合は順番を並べ替える。
            if(isset($this->request->data['MstFacilityItem']['id'])){
                // 配列を入れ替える
                $tmp_result = $result;
                $result = array();
                
                //カートに追加した順に並べなおす。
                foreach( $this->request->data['MstFacilityItem']['id'] as $id){
                    foreach($tmp_result as $tmp){
                        if($tmp['MstFixedCount']['mst_facility_item_id'] == $id){
                            $result[] = $tmp;
                            break;
                        }
                    }
                }
            }
            
            $this->request->data = $data;
            foreach($result as &$r ){
                //商品ごと、包装単位リストを取得
                $r['MstFixedCount']['unitList'] = $this->_unitlistFromFasItemId($r['MstFixedCount']['mst_facility_item_id']);
                //売上設定を確認
                $r['MstFixedCount']['salesConfig'] = $this->_getSalesConfigList($this->request->data['MstFixedCount']['mst_facility_id'] ,
                                                                                $r['MstFixedCount']['mst_facility_item_id']
                                                                                );

            }
            
            unset($r);
            
            // 棚番号取得
            $this->set('shelf',$this->_getShelfList( $this->request->data['MstFixedCount']['mst_department_id']));
            
        }
        
        
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
        
        $this->set('result' , $result);
        $this->set('selectedData',$this->request->data);
    }
    
    /**
     * 部署別定数設定完了画面
     */
    function result_by_depart () {
        //トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            $result = $this->Session->read('result');
            $this->Session->delete('result');
            $this->set('result',$result);
            $this->render('result_by_depart');
            return;
        }
        $now = date('Y/m/d H:i:s');
        
        $this->MstFixedCount->begin();
        
        //定数保存
        //更新データのみ再取得
        $recheckedId = array();
        foreach($this->request->data['MstFixedCount']['id'] as $key => $val){
            if(!empty($val) && $val !== 'a'){
                $recheckedId[] = $val;
            }
        }
        //更新データがある場合
        if (!empty($recheckedId)){
            //行ロック
            $this->MstFixedCount->query(' select * from mst_fixed_counts as a where a.id in (' . join(',',$recheckedId) . ') for update ');
            //更新チェック
            $ret = $this->MstFixedCount->query(" select count(*) from mst_fixed_counts as a where a.id in (" . join(',',$recheckedId) . ") and a.modified > '" . $this->request->data['MstFacilityItem']['time'] . "'");
            if( $ret[0][0]['count'] > 0 ){
                $this->MstFixedCount->rollback();
                $this->Session->setFlash('別ユーザによって更新されています', 'growl', array('icon'=>'error'));
                $this->redirect('list_by_depart');
                return;
            }
        }
        foreach($this->request->data['MstFixedCount']['id'] as $line => $id){
            //在庫レコード存在チェック
            $stock_id = $this->getStockRecode($this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                                              $this->request->data['MstFixedCount']['mst_department_id'] );
            if($id == 'a'){
                $order_point = 0;
                if($this->request->data['MstFixedCount']['trade_type'][$line]== Configure::read('TeisuType.Deposit') ||
                   $this->request->data['MstFixedCount']['trade_type'][$line]== Configure::read('TeisuType.NonStock')){
                    $order_point = $this->request->data['MstFixedCount']['fixed_count'][$line];
                }
                //新規作成
                $fixedcount = array(
                    'MstFixedCount' => array(
                        'trn_stock_id'         => $stock_id,
                        'mst_facility_item_id' => $this->request->data['MstFixedCount']['mst_facility_item_id'][$line],
                        'mst_item_unit_id'     => $this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                        'mst_facilities_id'    => $this->request->data['MstFixedCount']['mst_facility_id'],
                        'mst_department_id'    => $this->request->data['MstFixedCount']['mst_department_id'],
                        'order_point'          => $order_point,
                        'fixed_count'          => $this->request->data['MstFixedCount']['fixed_count'][$line],
                        'holiday_fixed_count'  => $this->request->data['MstFixedCount']['holiday_fixed_count'][$line],
                        'spare_fixed_count'    => $this->request->data['MstFixedCount']['spare_fixed_count'][$line],
                        'mst_shelf_name_id'    => (($this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]!='')?$this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]:null),
                        'print_type'           => $this->request->data['MstFixedCount']['print_type'][$line],
                        'trade_type'           => $this->request->data['MstFixedCount']['trade_type'][$line],
                        'substitute_unit_id'   => null,
                        'start_date'           => $this->request->data['MstFixedCount']['start_date'][$line],
                        'is_deleted'           => (isset($this->request->data['MstFixedCount']['is_deleted'][$line])?false:true),
                        'creater'              => $this->Session->read('Auth.MstUser.id'),
                        'created'              => $now,
                        'modifier'             => $this->Session->read('Auth.MstUser.id'),
                        'modified'             => $now,
                        )
                    );
                
            }else{
                $order_point = 0;
                if($this->request->data['MstFixedCount']['trade_type'][$line]== Configure::read('TeisuType.Deposit') ||
                   $this->request->data['MstFixedCount']['trade_type'][$line]== Configure::read('TeisuType.NonStock')){
                    $order_point = $this->request->data['MstFixedCount']['fixed_count'][$line];
                }
                
                //更新
                $fixedcount = array(
                    'MstFixedCount' => array(
                        'id'                   => $id,
                        'trn_stock_id'         => $stock_id,
                        'mst_facility_item_id' => $this->request->data['MstFixedCount']['mst_facility_item_id'][$line],
                        'mst_item_unit_id'     => $this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                        'mst_facilities_id'    => $this->request->data['MstFixedCount']['mst_facility_id'],
                        'mst_department_id'    => $this->request->data['MstFixedCount']['mst_department_id'],
                        'order_point'          => $order_point,
                        'fixed_count'          => $this->request->data['MstFixedCount']['fixed_count'][$line],
                        'holiday_fixed_count'  => $this->request->data['MstFixedCount']['holiday_fixed_count'][$line],
                        'spare_fixed_count'    => $this->request->data['MstFixedCount']['spare_fixed_count'][$line],
                        'mst_shelf_name_id'    => (($this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]!='')?$this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]:null),
                        'print_type'           => $this->request->data['MstFixedCount']['print_type'][$line],
                        'trade_type'           => $this->request->data['MstFixedCount']['trade_type'][$line],
                        'start_date'           => $this->request->data['MstFixedCount']['start_date'][$line],
                        'is_deleted'           => (isset($this->request->data['MstFixedCount']['is_deleted'][$line])?false:true),
                        'modifier'             => $this->Session->read('Auth.MstUser.id'),
                        'modified'             => $now,
                        )
                    );
            }
            $this->MstFixedCount->create();
            $fRes = $this->MstFixedCount->save($fixedcount);
            if($fRes === FALSE){
                $this->MstFixedCount->rollback();
                $this->Session->setFlash('定数作成処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('list_by_depart');
            }
            if($id == 0) {
                $id = $this->MstFixedCount->getLastInsertID();
            }
            
            $mst_fixed_count_ids[] = $id;
            
        }
        //更新データがある場合
        if (!empty($recheckedId)){
            //コミット直前に更新チェック
            $ret = $this->MstFixedCount->query(" select count(*) from mst_fixed_counts as a where a.id in (" . join(',',$recheckedId) . ") and a.modified > '" . $this->request->data['MstFacilityItem']['time'] . "' and a.modified <> '" . $now . "'");
            if( $ret[0][0]['count'] > 0 ){
                $this->Session->setFlash('別ユーザによって更新されています', 'growl', array('icon'=>'error'));
                $this->redirect('list_by_depart');
                return;
            }
        }

        //部署_包装単位で重複していたらダメ
        $sql  = ' select ';
        $sql .= '       count(a.*)  ';
        $sql .= '   from ';
        $sql .= '     mst_fixed_counts a  ';
        $sql .= '     inner join mst_facilities b  ';
        $sql .= '       on a.mst_facilities_id = b.id  ';
        $sql .= '       and b.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = a.mst_facility_item_id  ';
        $sql .= '   where ';
        $sql .= '     c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
        $sql .= '   group by ';
        $sql .= '     a.mst_department_id ';
        $sql .= '     , a.mst_item_unit_id  ';
        $sql .= '   having ';
        $sql .= '     count(a.*) > 1 ';
        $ret = $this->MstFixedCount->query($sql);
        if( !empty($ret)){
            $this->MstFixedCount->rollback();
            $this->Session->setFlash('同じ部署、同じ包装単位が重複しています。', 'growl', array('icon'=>'error'));
            $this->redirect('list_by_depart');
            return;
        }
        
        //コミット
        $this->MstFixedCount->commit();
        
        /* 完了画面の描画用データ取得 */
        $result = $this->getResultData($mst_fixed_count_ids);
        
        $this->set('result' , $result);
        $this->Session->write('result' , $result );
    }
    
    /**
     * @param
     * @return
     */
    function list_by_center() {
        $this->setRoleFunction(85); //倉庫定数
        
        $this->set('facilities',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected')
                       ,array(Configure::read('FacilityType.center'))
                       ,true
                       ,array('id' , 'facility_name')
                       )
                   );
    }

    /**
     * @param
     * @return
     */
    function select_by_center() {
        App::import('Sanitize');
        //部署ID
        $this->request->data['MstFixedCount']['mst_department_id'] = $this->getDepartmentId($this->request->data['MstFixedCount']['mst_facility_id'] , Configure::read('DepartmentType.warehouse') );
        
        if(isset($this->request->data['MstFixedCount']['itemSearch'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            //検索時
            
            $where  = '  and a.mst_facility_id = '. $this->request->data['MstFixedCount']['mst_facility_id'];
            
            //商品ID
            if(isset($this->request->data['MstFixedCount']['internal_code']) && $this->request->data['MstFixedCount']['internal_code'] !== ''){
                $where .= "  and a.internal_code = '" . $this->request->data['MstFixedCount']['internal_code'] . "'";
            }
            //製品番号
            if(isset($this->request->data['MstFixedCount']['item_code']) && $this->request->data['MstFixedCount']['item_code'] !== ''){
                $where .= "  and a.item_code like '%" . $this->request->data['MstFixedCount']['item_code'] . "%'";
            }
            //商品名
            if(isset($this->request->data['MstFixedCount']['item_name']) && $this->request->data['MstFixedCount']['item_name'] !== ''){
                $where .= "  and a.item_name like '%" . $this->request->data['MstFixedCount']['item_name'] . "%'";
            }
            //規格
            if(isset($this->request->data['MstFixedCount']['standard']) && $this->request->data['MstFixedCount']['standard'] !== ''){
                $where .= "  and a.standard like '%" . $this->request->data['MstFixedCount']['standard'] . "%'";
            }

            //販売元
            if(isset($this->request->data['MstFixedCount']['dealer_name']) && $this->request->data['MstFixedCount']['dealer_name'] !== ''){
                $where .= "  and e.dealer_name like '%" . $this->request->data['MstFixedCount']['dealer_name'] . "%'";
            }
            
            //JANコード(前方一致）
            if(isset($this->request->data['MstFixedCount']['jan_code']) && $this->request->data['MstFixedCount']['jan_code'] !== ''){
                $where .= "  and a.jan_code like '" . $this->request->data['MstFixedCount']['jan_code'] . "%'";
            }
            
            if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and b.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 = " and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 = " and 0 = 1 ";
            }
            
            $this->set('SearchResult'     , $this->_getData($where  , $this->request->data['limit'] ));
            $this->set('CartSearchResult' , $this->_getData($where2 , null ));
            $this->request->data = $data;
            
        }else{
            //初期
            $this->request->data['MstFixedCount']['facility_name'] = $this->getFacilityName($this->request->data['MstFixedCount']['mst_facility_id']);
        }
        
    }

    
    /**
     * @param
     * @return
     */
    function edit_by_center() {
        App::import('Sanitize');
        //施設名
        $this->request->data['MstFixedCount']['facility_name'] = $this->getFacilityName($this->request->data['MstFixedCount']['mst_facility_id']);
        //センター倉庫部署ID
        $this->request->data['MstFixedCount']['mst_department_id'] = $this->getDepartmentId($this->request->data['MstFixedCount']['mst_facility_id'] , Configure::read('DepartmentType.warehouse') );
        //センター倉庫棚一覧
        $this->set('ShelfList' , $this->_getShelfList( $this->request->data['MstFixedCount']['mst_department_id'] ));

        $where  = '  and a.mst_facility_id = '. $this->request->data['MstFixedCount']['mst_facility_id'];
        
        //カートで選択した包装単位
        if(isset($this->request->data['cart']['tmpid']) && $this->request->data['cart']['tmpid'] !== ''){
            $where .= "  and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
            $this->set('result' , $this->_getData($where));
            
        }else{
            
            $this->request->data['MstFixedCount']['edit'] = 1;
            
            //検索ボタン押下
            if(isset($this->request->data['MstFixedCount']['is_search'])){
                //編集
                $data = $this->request->data;
                $this->request->data = Sanitize::clean($this->request->data);
                //商品ID
                if(isset($this->request->data['MstFixedCount']['internal_code']) && $this->request->data['MstFixedCount']['internal_code'] !== ''){
                    $where .= "  and a.internal_code = '" . $this->request->data['MstFixedCount']['internal_code'] . "'";
                }
                //製品番号
                if(isset($this->request->data['MstFixedCount']['item_code']) && $this->request->data['MstFixedCount']['item_code'] !== ''){
                    $where .= "  and a.item_code like '%" . $this->request->data['MstFixedCount']['item_code'] . "%'";
                }
                //商品名
                if(isset($this->request->data['MstFixedCount']['item_name']) && $this->request->data['MstFixedCount']['item_name'] !== ''){
                    $where .= "  and a.item_name like '%" . $this->request->data['MstFixedCount']['item_name'] . "%'";
                }
                //規格
                if(isset($this->request->data['MstFixedCount']['standard']) && $this->request->data['MstFixedCount']['standard'] !== ''){
                    $where .= "  and a.standard like '%" . $this->request->data['MstFixedCount']['standard'] . "%'";
                }
                
                //販売元
                if(isset($this->request->data['MstFixedCount']['dealer_name']) && $this->request->data['MstFixedCount']['dealer_name'] !== ''){
                    $where .= "  and e.dealer_name like '%" . $this->request->data['MstFixedCount']['dealer_name'] . "%'";
                }
                
                //JANコード(前方一致）
                if(isset($this->request->data['MstFixedCount']['jan_code']) && $this->request->data['MstFixedCount']['jan_code'] !== ''){
                    $where .= "  and a.jan_code like '" . $this->request->data['MstFixedCount']['jan_code'] . "%'";
                }
                $this->set('result' , $this->_getData($where , (isset($this->request->data['limit'])?$this->request->data['limit']:min(array_keys(Configure::read('displaycounts_combobox'))))));
                $this->request->data = $data;
            }else{
                $this->set('result' , array());
            }
        }
        
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }
    
    /**
     * 定数設定（センター）登録結果
     */
    function result_by_center () {
        //トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            $result = $this->Session->read('result');
            $this->Session->delete('result');
            $this->set('result',$result);
            $this->render('result_by_center');
            return;
        }
        
        $result = array();
        $mst_fixed_count_ids = array();
        $now = date('Y/m/d H:i:s');
        
        $this->MstFixedCount->begin();

        //更新データのみ再取得
        $recheckedId = array();
        foreach($this->request->data['MstFixedCount']['id'] as $key => $val){
            if(!empty($val) && $val !== 'a'){
                $recheckedId[] = $val;
            }
        }
        //更新データがある場合
        if (!empty($recheckedId)){
            //行ロック
            $this->MstFixedCount->query(' select * from mst_fixed_counts as a where a.id in (' . join(',',$recheckedId) . ') for update ');
            //更新チェック
            $ret = $this->MstFixedCount->query(" select count(*) from mst_fixed_counts as a where a.id in (" . join(',',$recheckedId) . ") and a.modified > '" . $this->request->data['MstFacilityItem']['time'] . "'");
            if( $ret[0][0]['count'] > 0 ){
                $this->MstFixedCount->rollback();
                $this->Session->setFlash("別ユーザによって更新されています", 'growl', array('icon'=>'error'));
                $this->redirect('list_by_item');
                return;
            }
        }
        foreach($this->request->data['MstFixedCount']['id'] as $line => $id){
            //在庫レコード存在チェック
            $stock_id = $this->getStockRecode($this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                                              $this->request->data['MstFixedCount']['mst_department_id'] );
            if($id == 'a'){
                //新規作成
                $fixedcount = array(
                    'MstFixedCount' => array(
                        'trn_stock_id'         => $stock_id,
                        'mst_facility_item_id' => $this->request->data['MstFacilityItem']['id'][$line],
                        'mst_item_unit_id'     => $this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                        'mst_facilities_id'    => $this->request->data['MstFixedCount']['mst_facility_id'],
                        'mst_department_id'    => $this->request->data['MstFixedCount']['mst_department_id'],
                        'order_point'          => $this->request->data['MstFixedCount']['order_point'][$line],
                        'fixed_count'          => $this->request->data['MstFixedCount']['fixed_count'][$line],
                        'holiday_fixed_count'  => $this->request->data['MstFixedCount']['holiday_fixed_count'][$line],
                        'spare_fixed_count'    => $this->request->data['MstFixedCount']['spare_fixed_count'][$line],
                        'mst_shelf_name_id'    => (($this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]!='')?$this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]:null),
                        'trade_type'           => Configure::read('TeisuType.Constant'),
                        'substitute_unit_id'   => null,
                        'start_date'           => $this->request->data['MstFixedCount']['start_date'][$line],
                        'is_deleted'           => (isset($this->request->data['MstFixedCount']['is_deleted'][$line])?false:true),
                        'creater'              => $this->Session->read('Auth.MstUser.id'),
                        'created'              => $now,
                        'modifier'             => $this->Session->read('Auth.MstUser.id'),
                        'modified'             => $now,
                        )
                    );
                
            }else{
                //更新
                $fixedcount = array(
                    'MstFixedCount' => array(
                        'id'                   => $id,
                        'trn_stock_id'         => $stock_id,
                        'mst_facility_item_id' => $this->request->data['MstFacilityItem']['id'][$line],
                        'mst_item_unit_id'     => $this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                        'mst_facilities_id'    => $this->request->data['MstFixedCount']['mst_facility_id'],
                        'mst_department_id'    => $this->request->data['MstFixedCount']['mst_department_id'],
                        'order_point'          => $this->request->data['MstFixedCount']['order_point'][$line],
                        'fixed_count'          => $this->request->data['MstFixedCount']['fixed_count'][$line],
                        'holiday_fixed_count'  => $this->request->data['MstFixedCount']['holiday_fixed_count'][$line],
                        'spare_fixed_count'    => $this->request->data['MstFixedCount']['spare_fixed_count'][$line],
                        'mst_shelf_name_id'    => (($this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]!='')?$this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]:null),
                        'trade_type'           => Configure::read('TeisuType.Constant'),
                        'start_date'           => $this->request->data['MstFixedCount']['start_date'][$line],
                        'is_deleted'           => (isset($this->request->data['MstFixedCount']['is_deleted'][$line])?false:true),
                        'modifier'             => $this->Session->read('Auth.MstUser.id'),
                        'modified'             => $now,
                        )
                    );
            }
            $this->MstFixedCount->create();
            $fRes = $this->MstFixedCount->save($fixedcount);
            if($fRes === FALSE){
                $this->MstFixedCount->rollback();
                $this->Session->setFlash('定数作成処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('list_by_item');
            }
            if($id == 0) {
                $id = $this->MstFixedCount->getLastInsertID();
            }
            
            $mst_fixed_count_ids[] = $id;
            
        }
        //更新データがある場合
        if (!empty($recheckedId)){
            //コミット直前に更新チェック
            $ret = $this->MstFixedCount->query(" select count(*) from mst_fixed_counts as a where a.id in (" . join(',',$recheckedId) . ") and a.modified > '" . $this->request->data['MstFacilityItem']['time'] . "' and a.modified <> '" . $now . "'");
            if( $ret[0][0]['count'] > 0 ){
                $this->Session->setFlash("別ユーザによって更新されています", 'growl', array('icon'=>'error'));
                $this->redirect('list_by_item');
                return;
            }
        }
        $this->MstFixedCount->commit();
        
        /* 完了画面の描画用データ取得 */
        $where = "and f.id in (" . join(",",$mst_fixed_count_ids) . ")";
        $this->set('result' , $this->_getData($where));
        
        $this->Session->write('result',$this->_getData($where));
    }

    /**
     * @param
     * @return
     */
    function list_by_ope() {
        if($this->Session->read('Auth.Config.OpeSetUseType') == '0' ){
            // オペ倉庫が有効でない場合はTOPにリダイレクト
            $this->Session->setFlash("オペセットが有効ではありません。", 'growl', array('icon'=>'error'));
            $this->redirect('/');
            return;
        }
        
        $this->setRoleFunction(135); //オペ倉庫定数
        
        $this->set('facilities',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected')
                       ,array(Configure::read('FacilityType.center'))
                       ,true
                       ,array('id' , 'facility_name')
                       )
                   );
    }
    /**
     * @param
     * @return
     */
    function select_by_ope() {
        App::import('Sanitize');
        //部署ID
        $this->request->data['MstFixedCount']['mst_department_id'] = $this->getDepartmentId($this->request->data['MstFixedCount']['mst_facility_id'] , Configure::read('DepartmentType.opestock') );
        
        if(isset($this->request->data['MstFixedCount']['itemSearch'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            //検索時
            
            $where  = '  and a.mst_facility_id = '. $this->request->data['MstFixedCount']['mst_facility_id'];
            
            //商品ID
            if(isset($this->request->data['MstFixedCount']['internal_code']) && $this->request->data['MstFixedCount']['internal_code'] !== ''){
                $where .= "  and a.internal_code = '" . $this->request->data['MstFixedCount']['internal_code'] . "'";
            }
            //製品番号
            if(isset($this->request->data['MstFixedCount']['item_code']) && $this->request->data['MstFixedCount']['item_code'] !== ''){
                $where .= "  and a.item_code like '%" . $this->request->data['MstFixedCount']['item_code'] . "%'";
            }
            //商品名
            if(isset($this->request->data['MstFixedCount']['item_name']) && $this->request->data['MstFixedCount']['item_name'] !== ''){
                $where .= "  and a.item_name like '%" . $this->request->data['MstFixedCount']['item_name'] . "%'";
            }
            //規格
            if(isset($this->request->data['MstFixedCount']['standard']) && $this->request->data['MstFixedCount']['standard'] !== ''){
                $where .= "  and a.standard like '%" . $this->request->data['MstFixedCount']['standard'] . "%'";
            }

            //販売元
            if(isset($this->request->data['MstFixedCount']['dealer_name']) && $this->request->data['MstFixedCount']['dealer_name'] !== ''){
                $where .= "  and e.dealer_name like '%" . $this->request->data['MstFixedCount']['dealer_name'] . "%'";
            }
            
            //JANコード(前方一致）
            if(isset($this->request->data['MstFixedCount']['jan_code']) && $this->request->data['MstFixedCount']['jan_code'] !== ''){
                $where .= "  and a.jan_code like '" . $this->request->data['MstFixedCount']['jan_code'] . "%'";
            }
            
            if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and b.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 = " and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 = " and 0 = 1 ";
            }
            
            $this->set('SearchResult'     , $this->_getData($where  , $this->request->data['limit'] ));
            $this->set('CartSearchResult' , $this->_getData($where2 , null ));
            $this->request->data = $data;
            
        }else{
            //初期
            $this->request->data['MstFixedCount']['facility_name'] = $this->getFacilityName($this->request->data['MstFixedCount']['mst_facility_id']);
        }
        
    }

    
    /**
     * @param
     * @return
     */
    function edit_by_ope() {
        App::import('Sanitize');
        //施設名
        $this->request->data['MstFixedCount']['facility_name'] = $this->getFacilityName($this->request->data['MstFixedCount']['mst_facility_id']);
        //センター倉庫部署ID
        $this->request->data['MstFixedCount']['mst_department_id'] = $this->getDepartmentId($this->request->data['MstFixedCount']['mst_facility_id'] , Configure::read('DepartmentType.opestock') );
        //センター倉庫棚一覧
        $this->set('ShelfList' , $this->_getShelfList( $this->request->data['MstFixedCount']['mst_department_id'] ));

        $where  = '  and a.mst_facility_id = '. $this->request->data['MstFixedCount']['mst_facility_id'];
        
        //カートで選択した包装単位
        if(isset($this->request->data['cart']['tmpid']) && $this->request->data['cart']['tmpid'] !== ''){
            $where .= "  and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
            $this->set('result' , $this->_getData($where));
            
        }else{
            
            $this->request->data['MstFixedCount']['edit'] = 1;
            
            //検索ボタン押下
            if(isset($this->request->data['MstFixedCount']['is_search'])){
                //編集
                $data = $this->request->data;
                $this->request->data = Sanitize::clean($this->request->data);
                //商品ID
                if(isset($this->request->data['MstFixedCount']['internal_code']) && $this->request->data['MstFixedCount']['internal_code'] !== ''){
                    $where .= "  and a.internal_code = '" . $this->request->data['MstFixedCount']['internal_code'] . "'";
                }
                //製品番号
                if(isset($this->request->data['MstFixedCount']['item_code']) && $this->request->data['MstFixedCount']['item_code'] !== ''){
                    $where .= "  and a.item_code like '%" . $this->request->data['MstFixedCount']['item_code'] . "%'";
                }
                //商品名
                if(isset($this->request->data['MstFixedCount']['item_name']) && $this->request->data['MstFixedCount']['item_name'] !== ''){
                    $where .= "  and a.item_name like '%" . $this->request->data['MstFixedCount']['item_name'] . "%'";
                }
                //規格
                if(isset($this->request->data['MstFixedCount']['standard']) && $this->request->data['MstFixedCount']['standard'] !== ''){
                    $where .= "  and a.standard like '%" . $this->request->data['MstFixedCount']['standard'] . "%'";
                }
                
                //販売元
                if(isset($this->request->data['MstFixedCount']['dealer_name']) && $this->request->data['MstFixedCount']['dealer_name'] !== ''){
                    $where .= "  and e.dealer_name like '%" . $this->request->data['MstFixedCount']['dealer_name'] . "%'";
                }
                
                //JANコード(前方一致）
                if(isset($this->request->data['MstFixedCount']['jan_code']) && $this->request->data['MstFixedCount']['jan_code'] !== ''){
                    $where .= "  and a.jan_code like '" . $this->request->data['MstFixedCount']['jan_code'] . "%'";
                }
                $this->set('result' , $this->_getData($where , (isset($this->request->data['limit'])?$this->request->data['limit']:min(array_keys(Configure::read('displaycounts_combobox'))))));
                $this->request->data = $data;
            }else{
                $this->set('result' , array());
            }
        }
        
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }
    
    /**
     * 定数設定（オペ）登録結果
     */
    function result_by_ope () {
        //トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            $result = $this->Session->read('result');
            $this->Session->delete('result');
            $this->set('result',$result);
            $this->render('result_by_center');
            return;
        }
        
        $result = array();
        $mst_fixed_count_ids = array();
        $now = date('Y/m/d H:i:s');
        
        $this->MstFixedCount->begin();

        //更新データのみ再取得
        $recheckedId = array();
        foreach($this->request->data['MstFixedCount']['id'] as $key => $val){
            if(!empty($val) && $val !== 'a'){
                $recheckedId[] = $val;
            }
        }
        //更新データがある場合
        if (!empty($recheckedId)){
            //行ロック
            $this->MstFixedCount->query(' select * from mst_fixed_counts as a where a.id in (' . join(',',$recheckedId) . ') for update ');
            //更新チェック
            $ret = $this->MstFixedCount->query(" select count(*) from mst_fixed_counts as a where a.id in (" . join(',',$recheckedId) . ") and a.modified > '" . $this->request->data['MstFacilityItem']['time'] . "'");
            if( $ret[0][0]['count'] > 0 ){
                $this->MstFixedCount->rollback();
                $this->Session->setFlash("別ユーザによって更新されています", 'growl', array('icon'=>'error'));
                $this->redirect('list_by_item');
                return;
            }
        }
        foreach($this->request->data['MstFixedCount']['id'] as $line => $id){
            //在庫レコード存在チェック
            $stock_id = $this->getStockRecode($this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                                              $this->request->data['MstFixedCount']['mst_department_id'] );
            if($id == 'a'){
                //新規作成
                $fixedcount = array(
                    'MstFixedCount' => array(
                        'trn_stock_id'         => $stock_id,
                        'mst_facility_item_id' => $this->request->data['MstFacilityItem']['id'][$line],
                        'mst_item_unit_id'     => $this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                        'mst_facilities_id'    => $this->request->data['MstFixedCount']['mst_facility_id'],
                        'mst_department_id'    => $this->request->data['MstFixedCount']['mst_department_id'],
                        'order_point'          => $this->request->data['MstFixedCount']['order_point'][$line],
                        'fixed_count'          => $this->request->data['MstFixedCount']['fixed_count'][$line],
                        'holiday_fixed_count'  => $this->request->data['MstFixedCount']['holiday_fixed_count'][$line],
                        'spare_fixed_count'    => $this->request->data['MstFixedCount']['spare_fixed_count'][$line],
                        'mst_shelf_name_id'    => (($this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]!='')?$this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]:null),
                        'trade_type'           => Configure::read('TeisuType.Constant'),
                        'substitute_unit_id'   => null,
                        'start_date'           => $this->request->data['MstFixedCount']['start_date'][$line],
                        'is_deleted'           => (isset($this->request->data['MstFixedCount']['is_deleted'][$line])?false:true),
                        'creater'              => $this->Session->read('Auth.MstUser.id'),
                        'created'              => $now,
                        'modifier'             => $this->Session->read('Auth.MstUser.id'),
                        'modified'             => $now,
                        )
                    );
                
            }else{
                //更新
                $fixedcount = array(
                    'MstFixedCount' => array(
                        'id'                   => $id,
                        'trn_stock_id'         => $stock_id,
                        'mst_facility_item_id' => $this->request->data['MstFacilityItem']['id'][$line],
                        'mst_item_unit_id'     => $this->request->data['MstFixedCount']['mst_item_unit_id'][$line],
                        'mst_facilities_id'    => $this->request->data['MstFixedCount']['mst_facility_id'],
                        'mst_department_id'    => $this->request->data['MstFixedCount']['mst_department_id'],
                        'order_point'          => $this->request->data['MstFixedCount']['order_point'][$line],
                        'fixed_count'          => $this->request->data['MstFixedCount']['fixed_count'][$line],
                        'holiday_fixed_count'  => $this->request->data['MstFixedCount']['holiday_fixed_count'][$line],
                        'spare_fixed_count'    => $this->request->data['MstFixedCount']['spare_fixed_count'][$line],
                        'mst_shelf_name_id'    => (($this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]!='')?$this->request->data['MstFixedCount']['mst_shelf_name_id'][$line]:null),
                        'trade_type'           => Configure::read('TeisuType.Constant'),
                        'start_date'           => $this->request->data['MstFixedCount']['start_date'][$line],
                        'is_deleted'           => (isset($this->request->data['MstFixedCount']['is_deleted'][$line])?false:true),
                        'modifier'             => $this->Session->read('Auth.MstUser.id'),
                        'modified'             => $now,
                        )
                    );
            }
            $this->MstFixedCount->create();
            $fRes = $this->MstFixedCount->save($fixedcount);
            if($fRes === FALSE){
                $this->MstFixedCount->rollback();
                $this->Session->setFlash('定数作成処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('list_by_item');
            }
            if($id == 0) {
                $id = $this->MstFixedCount->getLastInsertID();
            }
            
            $mst_fixed_count_ids[] = $id;
            
        }
        //更新データがある場合
        if (!empty($recheckedId)){
            //コミット直前に更新チェック
            $ret = $this->MstFixedCount->query(" select count(*) from mst_fixed_counts as a where a.id in (" . join(',',$recheckedId) . ") and a.modified > '" . $this->request->data['MstFacilityItem']['time'] . "' and a.modified <> '" . $now . "'");
            if( $ret[0][0]['count'] > 0 ){
                $this->Session->setFlash("別ユーザによって更新されています", 'growl', array('icon'=>'error'));
                $this->redirect('list_by_item');
                return;
            }
        }
        $this->MstFixedCount->commit();
        
        /* 完了画面の描画用データ取得 */
        $where = "and f.id in (" . join(",",$mst_fixed_count_ids) . ")";
        $this->set('result' , $this->_getData($where));
        
        $this->Session->write('result',$this->_getData($where));
    }
    
    /* 完了画面表示用のデータを取得 */
    private function getResultData($ids){
        $sql  = ' select ';
        $sql .= '       f.department_name                              as "MstFixedCount__fasAndDept"';
        $sql .='      , h.internal_code                                as "MstFixedCount__internal_code"  ';
        $sql .='      , h.item_name                                    as "MstFixedCount__item_name"  ';
        $sql .='      , h.item_code                                    as "MstFixedCount__item_code"  ';
        $sql .='      , h.standard                                     as "MstFixedCount__standard"  ';
        $sql .='      , i.dealer_name                                  as "MstFixedCount__dealer_name"  ';
        $sql .='      , j.name                                         as "MstFixedCount__shelf_name"  ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                                as "MstFixedCount__unit_name" ';
        $sql .= '     , a.fixed_count                                  as "MstFixedCount__fixed_count" ';
        $sql .= '     , a.holiday_fixed_count                          as "MstFixedCount__holiday_fixed_count" ';
        $sql .= '     , a.spare_fixed_count                            as "MstFixedCount__spare_fixed_count" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when h.is_lowlevel = true  ';
        $sql .= "         then '低レベル品'  ";
        foreach(Configure::read('TeisuType.List') as $k=>$v){
            $sql .= '         when a.trade_type = ' . $k;
            $sql .= "         then '".$v."'  ";
        }
        $sql .= '         end ';
        $sql .= '     )                                               as "MstFixedCount__trade_type" ';
        $sql .= '     , k.const_nm                                    as "MstFixedCount__print_type" ';
        $sql .= '     , g.name                                        as "MstFixedCount__shelf_name"';
        $sql .= '     , TO_CHAR(a.start_date,\'yyyy/mm/dd\')          as "MstFixedCount__start_date"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.is_deleted = false  ';
        $sql .= "         then '○'  ";
        $sql .= "         else '×'  ";
        $sql .= '         end ';
        $sql .= '     )                                               as "MstFixedCount__is_deleted"  ';
        $sql .= '   from ';
        $sql .= '     mst_fixed_counts as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on a.mst_item_unit_id = b.id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on b.mst_unit_name_id = c.id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on b.per_unit_name_id = d.id  ';
        $sql .= '     left join mst_facilities as e  ';
        $sql .= '       on e.id = a.mst_facilities_id  ';
        $sql .= '     left join mst_departments as f  ';
        $sql .= '       on f.id = a.mst_department_id  ';
        $sql .= '     left join mst_shelf_names as g  ';
        $sql .= '       on g.id = a.mst_shelf_name_id  ';
        $sql .= '     left join mst_facility_items as h  ';
        $sql .= '       on h.id = a.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as i  ';
        $sql .= '       on i.id = h.mst_dealer_id  ';
        $sql .= '     left join mst_shelf_names as j  ';
        $sql .= '       on j.id = a.mst_shelf_name_id  ';
        $sql .= '     left join mst_consts as k ';
        $sql .= '       on k.const_cd = a.print_type::varchar ';
        $sql .= "      and k.const_group_cd = '" . Configure::read('ConstGroupType.printerSetting') . "'";
        $sql .= '   where ';
        $sql .= '     a.id in (' .join(',',$ids). ') ';
        $sql .= '   order by  ';
        $sql .= '     h.internal_code ';
        
        return $this->MstFixedCount->query($sql);
    }


    private function  _getData($where , $limit=null ){
        $sql  = ' select ';
        $sql .= '       b.id                                 as "MstFixedCount__mst_item_unit_id"';
        $sql .= '     , f.id                                 as "MstFixedCount__id"';
        $sql .= '     , a.id                                 as "MstFacilityItem__id"';
        $sql .= '     , a.internal_code                      as "MstFacilityItem__internal_code"';
        $sql .= '     , a.item_name                          as "MstFacilityItem__item_name"';
        $sql .= '     , a.item_code                          as "MstFacilityItem__item_code"';
        $sql .= '     , a.standard                           as "MstFacilityItem__standard"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                      as "MstFacilityItem__unit_name" ';
        $sql .= '     , e.dealer_name                        as "MstFacilityItem__dealer_name"';
        $sql .= '     , f.order_point                        as "MstFixedCount__order_point"';
        $sql .= '     , f.fixed_count                        as "MstFixedCount__fixed_count"';
        $sql .= '     , f.holiday_fixed_count                as "MstFixedCount__holiday_fixed_count"';
        $sql .= '     , f.spare_fixed_count                  as "MstFixedCount__spare_fixed_count"';
        $sql .= '     , TO_CHAR(f.start_date,\'yyyy/mm/dd\') as "MstFixedCount__start_date"';
        $sql .= '     , (';
        $sql .= '       case';
        $sql .= '         when f.is_deleted = true then false';
        $sql .= '         else true';
        $sql .= '         end';
        $sql .= '       )                                    as "MstFixedCount__is_deleted"';
        $sql .= '     , (';
        $sql .= '       case';
        $sql .= "         when f.is_deleted = true then '×'";
        $sql .= "         else '○'";
        $sql .= '         end';
        $sql .= '       )                                    as "MstFixedCount__is_deleted_view"';
        $sql .= '     , f.mst_shelf_name_id                  as "MstFixedCount__mst_shelf_name_id"';
        $sql .= '     , g.name                               as "MstFixedCount__mst_shelf_name"';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a ';
        $sql .= '     left join mst_item_units as b ';
        $sql .= '       on b.mst_facility_item_id = a.id ';
        $sql .= '     left join mst_unit_names as c ';
        $sql .= '       on c.id = b.mst_unit_name_id ';
        $sql .= '     left join mst_unit_names as d ';
        $sql .= '       on d.id = b.per_unit_name_id ';
        $sql .= '     left join mst_dealers as e ';
        $sql .= '      on e.id = a.mst_dealer_id ';
        if(isset($this->request->data['MstFixedCount']['edit'])){
            $sql .= '    inner join mst_fixed_counts as f ';
        }else{
            $sql .= '    left join mst_fixed_counts as f ';
        }
        $sql .= '      on f.mst_item_unit_id = b.id ';
        $sql .= '     and f.mst_department_id = ' . $this->request->data['MstFixedCount']['mst_department_id'];
        $sql .= '    left join mst_shelf_names as g ';
        $sql .= '      on g.id = f.mst_shelf_name_id ';
        
        $sql .= '   where 1=1 ';
        $sql .= '   and ( ( a.is_lowlevel = true and b.per_unit = 1 ) or a.is_lowlevel = false )';
        $sql .= $where;
        $sql .= '   order by ';
        $sql .= '     a.internal_code ';
        $sql .= '     , b.per_unit ';

        if(!is_null($limit)){
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));
            $sql .= ' limit ' . $limit;
        }
        return $this->MstFacilityItem->query($sql);
    }
    
    
    /* 部署にひもづく棚リストを返す */
    private function _getShelfList($mst_department_id){
        $sql  = ' select ';
        $sql .= '      a.id          as "MstShelfName__id"';
        $sql .= '    , a.name        as "MstShelfName__name" ';
        $sql .= '  from';
        $sql .= '    mst_shelf_names as a ';
        $sql .= '  where';
        $sql .= '    a.is_deleted = false ';
        $sql .= '    and a.mst_department_id =' . $mst_department_id;
        $sql .= '    order by a.name';
        
        $list = Set::Combine($this->MstShelfName->query($sql),
            "{n}.MstShelfName.id",
            "{n}.MstShelfName.name"
            );
        return $list;
        
    }
    
    
    
    /* 部署IDから、部署名、施設名、施設IDを取得する */
    private function _getHeaderData($department_id){
        $sql  = ' select ';
        $sql .= '       b.id                   as "MstFixedCount__mst_facility_id" ';
        $sql .= '     , b.facility_name        as "MstFixedCount__facility_name" ';
        $sql .= '     , a.id                   as "MstFixedCount__mst_department_id" ';
        $sql .= '     , a.department_name      as "MstFixedCount__department_name"  ';
        $sql .= '   from ';
        $sql .= '     mst_departments as a  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.mst_facility_id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $department_id; 
        $tmp = $this->MstDepartment->query($sql);
        
        $this->request->data['MstFixedCount']['mst_facility_id']   = $tmp[0]['MstFixedCount']['mst_facility_id'];
        $this->request->data['MstFixedCount']['facility_name']     = $tmp[0]['MstFixedCount']['facility_name'];
        $this->request->data['MstFixedCount']['mst_department_id'] = $tmp[0]['MstFixedCount']['mst_department_id'];
        $this->request->data['MstFixedCount']['department_name']   = $tmp[0]['MstFixedCount']['department_name'];
        
    }
    
    /*
     * 商品一覧取得
     * 部署別のカートで使用
     */
    private function _getFacilityItems($where , $limit = null){
        $sql  = ' select ';
        $sql .= '       a.id             as "MstFixedCount__id"';
        $sql .= '     , a.internal_code  as "MstFixedCount__internal_code"';
        $sql .= '     , a.item_name      as "MstFixedCount__item_name"';
        $sql .= '     , a.item_code      as "MstFixedCount__item_code"';
        $sql .= '     , a.standard       as "MstFixedCount__standard"';
        $sql .= '     , b.dealer_name    as "MstFixedCount__dealer_name"';
        $sql .= '     , a.jan_code       as "MstFixedCount__jan_code"';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_dealers as b  ';
        $sql .= '       on b.id = a.mst_dealer_id  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and a.item_type = ' . Configure::read('Items.item_types.normalitem');
        $sql .= $where ;
        $sql .= ' order by a.internal_code';
        if(!is_null($limit)){
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFixedCount'));
            $sql .= ' limit ' . $limit;
        }
        
        return $this->MstFixedCount->query($sql);
        
    }


    
    /*
     * 部署別、商品別定数設定で、売上設定がない商品の定数は設定できないようにする
     * チェック用に
     */
    private function _getSalesConfigList($facility_id , $item_id){
        //セット品の場合、売上設定はないのですべての包装単位をOKとして返す
        $sql = ' select item_type from mst_facility_items where id = ' . $item_id;
        $ret =  $this->MstFacilityItem->query($sql);
        
        if($ret[0][0]['item_type'] == Configure::read('Items.item_types.setitem')){
            $sql  = ' select ';
            $sql .= '       a.id  as "MstSalesConfig__mst_item_unit_id"';
            $sql .= '   from ';
            $sql .= '     mst_item_units as a  ';
            $sql .= '   where ';
            $sql .= '     a.mst_facility_item_id = ' . $item_id;
            return $this->MstSalesConfig->query($sql);
        }else{
            $sql  = ' select ';
            $sql .= '       a.mst_item_unit_id  as "MstSalesConfig__mst_item_unit_id"';
            $sql .= '   from ';
            $sql .= '     mst_sales_configs as a  ';
            $sql .= '   where ';
            $sql .= '     a.partner_facility_id = ' . $facility_id;
            $sql .= '     and a.mst_facility_item_id = ' . $item_id;
            $sql .= '     and a.is_deleted = false ';
            return $this->MstSalesConfig->query($sql);
        }
    }
}


