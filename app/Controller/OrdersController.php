<?php
/**
 * OrdersController
 * 発注
 * @version 1.0.0
 * @since 2010/05/18
 */
class OrdersController extends AppController {
    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';

        // Ajax用の設定
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            if ($this->action == 'get_facilities') {
                $this->RequestHandler->setContent('json','text/x-json');
                $this->RequestHandler->respondAs('application/json; charset=UTF-8');
            }
        }
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }


    /**
     * @var $name
     */
    var $name = 'Orders';

    /**
     * @var array $uses
     */
    var $uses = array('TrnOrder'
                      ,'TrnOrderHeader'
                      ,'MstClass'
                      ,'MstDepartment'
                      ,'MstFacility'
                      ,'MstFacilityItem'
                      ,'MstFacilityRelation'
                      ,'MstItemUnit'
                      ,'MstUnitName'
                      ,'MstUser'
                      ,'MstUserBelonging'
                      ,'MstTransactionConfig'
                      ,'TrnStock'
                      ,'TrnSticker'
                      ,'TrnReceiving'
                      ,'TrnShipping'
                      ,'TrnStorage'
                      ,'TrnReceivingHeader'
                      ,'TrnShippingHeader'
                      ,'TrnStorageHeader'
                      ,'MstFixedCount'
                      ,'TrnConsume'
                      ,'TrnClaim'
                      ,'TrnSalesRequestHeader'
                      ,'TrnStickerIssue'
                      ,'TrnStickerIssueHeader'
                      ,'TrnStickerRecord'
                      ,'TrnCloseHeader'
                      ,'MstShelfName'
                      ,'MstInsuranceClaimDepartment'
                      );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    /**
     * @var array $components
     */
    var $components = array(
            'RequestHandler',
            'xml',
            'Stickers',
            'CsvWriteUtils',
            'AptageDataChecker',
            'PdfReport',
            'Mail', 
    );

    /**
     *
     */
    function index (){
        $this->add();
    }

    /**
     * 定数：作業区分
     * Enter description here ...
     * @var unknown_type
     */
    const WORK_CLASSES_ORDER = 8;

    /**
     * @param
     * @return
     */
    function add() {
        $this->setRoleFunction(16); //発注・補充要求作成
        
        $this->Session->delete('TrnOrder');
        //作業区分データセット
        $order_classes_List = $this->_getOrderClassesList();
        $this->set('order_classes_List',$order_classes_List);
        $department_list = array();
        //施設データセット
        // センターだけ取得する
        $facility_list = $this->getFacilityList(
            $this->Session->read('Auth.facility_id_selected') ,
            array(Configure::read('FacilityType.center')) ,
            false
            );

        //部署データセット
        if(isset($this->request->data['Order']['facilityCode'])){
            $department_list = $this->getDepartmentsList($this->request->data['Order']['facilityCode']);
        }

        // トークンの作成
        $this->_createTransactionToken();
        $this->set('facility_list' , $facility_list);
        $this->set('department_list' , $department_list);
    }


    /**
     * 発注要求作成
     */
    function add_result () {
        $fixed_count_name_array = Configure::read('FixedType.List');
        $this->request->data['TrnOrder']['fixed_count_name'] = $fixed_count_name_array[$this->request->data['TrnOrder']['fixed_count']];
        if($this->request->data['TrnOrder']['order_type']==Configure::read('OrderTypes.nomal') ){
            $order_type_name = "通常品発注";
        }elseif($this->request->data['TrnOrder']['order_type']==Configure::read('OrderTypes.replenish')){
            $order_type_name = "預託品発注";
        }elseif($this->request->data['TrnOrder']['order_type']==Configure::read('OrderTypes.nostock') ){
            $order_type_name = "非在庫品発注";
        }elseif($this->request->data['TrnOrder']['order_type']==Configure::read('OrderTypes.opestock') ){
            $order_type_name = "オペ倉庫品発注";
        }
        $this->request->data['TrnOrder']['order_type_name'] = $order_type_name;

        $_updated_stocks = array();
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['TrnOrder']['token'] !== $this->Session->read('TrnOrder.token')) {
            $this->Session->write('TrnOrder.error', true);
            $this->_createTransactionToken();
            $this->add();
            return;
        }

        $this->Session->delete('TrnOrder.token');

        $userId = $this->Session->read('Auth.MstUser.id');
        
        //施設コードから施設IDを取得
        $tmp = $this->MstFacility->find('first', array('fields' => 'id',
                                                       'conditions' => array('MstFacility.facility_code' => $this->request->data['TrnOrder']['facilityCode'] ,
                                                                             'MstFacility.facility_type'=> array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')) ),
                                                       'recursive' => -1));
        $facility_id = $tmp['MstFacility']['id'];
        $department_id= '';
        //部署コードから部署IDを取得
        if($this->request->data['TrnOrder']['order_type'] == Configure::read('OrderTypes.nomal') ){
            //通常発注
            $department_id = $this->getDepartmentId($facility_id , Configure::read('DepartmentType.warehouse') );
        }else if($this->request->data['TrnOrder']['order_type'] == Configure::read('OrderTypes.opestock') ){
            $department_id = $this->getDepartmentId($facility_id , Configure::read('DepartmentType.opestock') );
        }else{
            //預託、非在庫
            if($this->request->data['TrnOrder']['departmentCode'] != ''){
                $department_id = $this->getDepartmentId($facility_id , Configure::read('DepartmentType.hospital') , $this->request->data['TrnOrder']['departmentCode']);
            }
        }
        //トランザクション開始
        $this->TrnOrder->begin();
        //要求数の取得
        $request_data  = $this->_getRequestAmount($this->request->data['TrnOrder']['fixed_count'], $facility_id ,$department_id, $this->request->data['TrnOrder']['order_type'] );
        $save_data = array();
        $result_view = array(); //結果画面用
        $i = 0;
        foreach($request_data as $r ){
            if($r['TrnOrder']['quantity'] > 0){
                //発注データ
                $save_data[$i]['TrnOrder']['id']                 = false;
                $save_data[$i]['TrnOrder']['work_no']            = "";
                $save_data[$i]['TrnOrder']['work_seq']           = 0;
                $save_data[$i]['TrnOrder']['work_type']          = $this->request->data['TrnOrder']['work_type'];
                $save_data[$i]['TrnOrder']['order_type']         = $this->request->data['TrnOrder']['order_type'];
                $save_data[$i]['TrnOrder']['recital']            = $this->request->data['TrnOrder']['recital'];
                $save_data[$i]['TrnOrder']['order_status']       = Configure::read('OrderStatus.notYetOrder');
                $save_data[$i]['TrnOrder']['mst_item_unit_id']   = $r['TrnOrder']['mst_item_unit_id'];
                $save_data[$i]['TrnOrder']['department_id_from'] = $r['TrnOrder']['department_id_from'];
                $save_data[$i]['TrnOrder']['is_deleted']         = false;
                $save_data[$i]['TrnOrder']['fixed_num_used']     = $this->request->data['TrnOrder']['fixed_count'];
                $save_data[$i]['TrnOrder']['creater']            = $userId;
                $save_data[$i]['TrnOrder']['modifier']           = $userId;
                $save_data[$i]['TrnOrder']['before_quantity']    = $r['TrnOrder']['quantity'];
                $save_data[$i]['TrnOrder']['quantity']           = $r['TrnOrder']['quantity'];
                $save_data[$i]['TrnOrder']['remain_count']       = $r['TrnOrder']['quantity'];
                $save_data[$i]['TrnOrder']['stocking_price']     = $r['TrnOrder']['price'];
                $save_data[$i]['TrnOrder']['department_id_to']   = $r['TrnOrder']['department_id_to'];
                $i++;
                
                //在庫データ更新
                $this->_updateStockData($r['TrnOrder']['department_id_from'], $r['TrnOrder']['mst_item_unit_id'], $r['TrnOrder']['quantity']);
                //表示用データまとめ
                if(isset($result_view[$r['TrnOrder']['department_id_to']])){
                    $result_view[$r['TrnOrder']['department_id_to']]['count'] = $result_view[$r['TrnOrder']['department_id_to']]['count'] + 1;
                    $result_view[$r['TrnOrder']['department_id_to']]['price'] = $result_view[$r['TrnOrder']['department_id_to']]['price'] + $r['TrnOrder']['price'] * $r['TrnOrder']['quantity'];
                }else{
                    $result_view[$r['TrnOrder']['department_id_to']]['name']  = $r['TrnOrder']['facility_name'];
                    $result_view[$r['TrnOrder']['department_id_to']]['type']  = $this->request->data['TrnOrder']['order_type_name'];
                    $result_view[$r['TrnOrder']['department_id_to']]['count'] = 1;
                    $result_view[$r['TrnOrder']['department_id_to']]['price'] = $r['TrnOrder']['price'] * $r['TrnOrder']['quantity'];
                }
            }
        }
        if(count($save_data) > 0 ){
            if(!$this->TrnOrder->saveAll($save_data  , array('validates' => true,'atomic' => false))){
                $this->TrnOrder->rollback();
                $this->set('message' , '要求の作成に失敗しました。');
            }else{
                $this->TrnOrder->commit();
                $this->set('message' , '要求を作成しました。');
            }
        }else{
            $this->TrnOrder->commit();
            $this->set('message' , '不足している材料は有りません。');
        }

        $this->set('order_List' , $result_view);
        $this->render('add_result');
    }

    /**
     * 発注履歴CSV出力
     */
    function export_csv() {
        $this->request->data['TrnOrderHeader']['id']=array();
        //検索
        $where = $this->_createSearchWhere();
        $where  .= '  AND A.order_type <> '.Configure::read('OrderTypes.direct');

        $sql = $this->_getOrderCSV($where);
        $this->db_export_csv($sql , "発注履歴", '/orders/order_history');
    }

    private function _getOrderCSV($where) {
        $sql  = 'SELECT ';
        $sql .= '       A.work_no                           AS 発注番号 ';
        $sql .= "      ,TO_CHAR(A.work_date,'YYYY/MM/DD')   AS 発注日 ";
        $sql .= '      ,F.facility_name                     AS 仕入先名 ';
        $sql .= "      ,C.department_name                   AS 部署名 ";
        $sql .= '      ,J.internal_code                     AS "商品ID" ';
        $sql .= '      , ( case A.order_type  ';
        foreach(Configure::read('OrderTypes.orderTypeName') as $key => $val){
            $sql .= "           when " . $key . " then '" . $val ."' ";
        }
        $sql .= '        end )                              AS 発注区分名';
        $sql .= '      ,J.item_name                         AS 商品名 ';
        $sql .= '      ,J.standard                          AS 規格 ';
        $sql .= '      ,J.item_code                         AS 製品番号 ';
        $sql .= '      ,K.dealer_name                       AS 販売元名 ';
        $sql .= "      ,(case when I.per_unit = 1 then M.unit_name else M.unit_name || '(' || I.per_unit || N.unit_name || ')' end )AS 包装単位";
        $sql .= '      ,A.stocking_price                    AS 仕入価格 ';
        $sql .= '      ,A.quantity                          AS 発注数 ';
        $sql .= '      ,A.remain_count                      AS 残数 ';
        $sql .= '      ,L.name                              AS 作業区分名 ';
        $sql .= '      ,G.user_name                         AS 更新者 ';
        $sql .= '      ,A.recital                           AS 備考 ';
        $sql .= '      ,J.spare_key1                        as ' . $this->Session->read('Auth.Config.SpareKey1Title') ;// 予備キー1
        $sql .= '      ,J.spare_key2                        as ' . $this->Session->read('Auth.Config.SpareKey2Title') ;// 予備キー2
        $sql .= '      ,J.spare_key3                        as ' . $this->Session->read('Auth.Config.SpareKey3Title') ;// 予備キー3
        
        $sql .= '  FROM trn_orders A ';
        $sql .= '          LEFT JOIN trn_order_headers B ';
        $sql .= '                 ON A.trn_order_header_id = B.id ';
        $sql .= '         INNER JOIN mst_departments C ';
        $sql .= '                 ON A.department_id_from = C.id ';
        $sql .= '         INNER JOIN mst_facilities D ';
        $sql .= '                 ON C.mst_facility_id = D.id ';
        $sql .= '                AND D.facility_type in ('.Configure::read('FacilityType.center').','.Configure::read('FacilityType.hospital').')';
        $sql .= '         INNER JOIN mst_departments E ';
        $sql .= '                 ON A.department_id_to = E.id ';
        $sql .= '         INNER JOIN mst_facilities F ';
        $sql .= '                 ON E.mst_facility_id = F.id ';
        $sql .= '                AND F.facility_type = ' . Configure::read('FacilityType.supplier');
        $sql .= '         INNER JOIN mst_users G ';
        $sql .= '                 ON A.modifier = G.id ';
        $sql .= '         INNER JOIN mst_item_units I ';
        $sql .= '                 ON A.mst_item_unit_id = I.id ';
        $sql .= '         INNER JOIN mst_facility_items J ';
        $sql .= '                 ON I.mst_facility_item_id = J.id ';
        $sql .= '          LEFT JOIN mst_dealers K ';
        $sql .= '                 ON J.mst_dealer_id = K.id ';
        $sql .= '          LEFT JOIN mst_classes L ';
        $sql .= '                 ON A.work_type = L.id ';
        $sql .= '          LEFT JOIN mst_unit_names M ';
        $sql .= '                 ON I.mst_unit_name_id = M.id ';
        $sql .= '          LEFT JOIN mst_unit_names N ';
        $sql .= '                 ON I.per_unit_name_id = N.id ';
        $sql .= '        INNER JOIN mst_facilities O ';
        $sql .= '                ON I.mst_facility_id = O.id ';
        $sql .= '               AND O.facility_type = '.Configure::read('FacilityType.center');
        $sql .= '               AND O.id = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= '         INNER JOIN mst_departments P ';
        $sql .= '                 ON A.department_id_to = P.id ';
        $sql .= '         INNER JOIN mst_facilities Q ';
        $sql .= '                 ON P.mst_facility_id = Q.id ';
        $sql .= '                AND Q.facility_type = ' . Configure::read('FacilityType.supplier');
        $sql .= $where;
        $sql .= ' ORDER BY A.trn_order_header_id, J.mst_owner_id , K.dealer_name , J.item_name , J.item_code ';

        return $sql;
    }


    //発注確定
    function order_decision () {
        $this->setRoleFunction(18); //発注・補充確定
        $this->set('order_type_List',Configure::read('OrderTypes.orderTypeName'));
        $this->set('order_classes_List',$this->_getOrderClassesList());
        $order_List = array();
        //仕入先データセット
        $this->set('supplier_list' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                             array(Configure::read('FacilityType.supplier'))
                                                             ));

        //情報の取得
        if(isset($this->request->data['search']['is_search'])){
            $sql  = ' select ';
            $sql .= '     a.order_type ';
            $sql .= '   , ( case ';
            foreach(Configure::read('OrderTypes.orderTypeName') as $k => $v){
                $sql .= '  when a.order_type = ' . $k . " then '" . $v . "'";
            }
            $sql .= '     end )                   as order_type_name';
            $sql .= '   , c.id                    as from_id ';
            $sql .= '   , b.id                    as from_department_id';
            $sql .= '   , ( case ';
            $sql .= '       when c.facility_type = ' . Configure::read('FacilityType.center') . ' then c.facility_name ';
            $sql .= "       when c.facility_type = " . Configure::read('FacilityType.hospital')  . " then b.department_name ";
            $sql .= '       end )                 as from_name ';
            $sql .= '   , e.id                    as to_id ';
            $sql .= '   , e.facility_name         as to_name  ';
            $sql .= ' from ';
            $sql .= '   trn_orders as a  ';
            $sql .= '   left join mst_departments as b  ';
            $sql .= '     on b.id = a.department_id_from  ';
            $sql .= '   left join mst_facilities as c  ';
            $sql .= '     on c.id = b.mst_facility_id  ';
            $sql .= '   left join mst_departments as d  ';
            $sql .= '     on d.id = a.department_id_to  ';
            $sql .= '   left join mst_facilities as e  ';
            $sql .= '     on e.id = d.mst_facility_id  ';
            $sql .= ' where ';
            $sql .= '   a.trn_order_header_id is null  ';
            $sql .= '   and a.order_status = ' . Configure::read('OrderStatus.notYetOrder');
            $sql .= '   and a.is_deleted = false  ';
            $sql .= '   and a.order_type not in (' . Configure::read('OrderTypes.direct') . ')  ';
            
            //ユーザ入力値による検索条件の作成--------------------------------------------
            //仕入先コード(完全一致)
            if((isset($this->request->data['TrnOrder']['supplierCode'])) && ($this->request->data['TrnOrder']['supplierCode'] != "")){
                $sql .= " and e.facility_code = '". $this->request->data['TrnOrder']['supplierCode'] . "'";
            }
            //発注種別(完全一致)
            if((isset($this->request->data['TrnOrder']['orders_type'])) && ($this->request->data['TrnOrder']['orders_type'] != "")){
                $sql .= ' and a.order_type = '. $this->request->data['TrnOrder']['orders_type'];
            }
            //作業区分(完全一致)
            if((isset($this->request->data['TrnOrder']['classId'])) && ($this->request->data['TrnOrder']['classId'] != "")){
                $sql .= ' and a.work_type = '. $this->request->data['TrnOrder']['classId'];
            }
            //備考(部分一致)
            if((isset($this->request->data['TrnOrder']['comment'])) && ($this->request->data['TrnOrder']['comment'] != "")){
                $sql .= " and a.recital ILIKE '%". $this->request->data['TrnOrder']['comment'] ."%'";
            }
            
            $sql .= ' group by ';
            $sql .= '   a.order_type ';
            $sql .= '   , c.id ';
            $sql .= '   , c.facility_name ';
            $sql .= '   , b.id ';
            $sql .= '   , b.department_name ';
            $sql .= '   , e.id ';
            $sql .= '   , e.facility_name ';
            $sql .= ' order by ';
            $sql .= '   c.facility_name ';
            $sql .= '   , a.order_type ';
            //検索条件の作成終了---------------------------------------------------------

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnOrder'));
            $sql .= ' LIMIT '.$this->_getLimitCount();
            $order_List = $this->TrnOrder->query($sql);
        }else{
            $this->request->data['TrnOrder']['work_date'] = date('Y/m/d');
        }
        $this->set('order_List',$order_List);
    }

    //発注確定確認
    function order_decision_confirm () {
        $this->set('order_classes_List',$this->_getOrderClassesList());

        //情報の取得
        foreach($this->request->data['TrnOrder']['id'] as $id){
            $conditions = explode("_",$id);
            //仕入先id(完全一致)
            $supplier_id = $conditions[0];
            //要求元施設id(完全一致)
            $order_facility_id = $conditions[1];
            //要求元部署id(完全一致)
            $order_department_id = $conditions[2];
            //発注種別(完全一致)
            $order_type = $conditions[3];
            // 仕入れ先
            $where  = '   and f.id = ' . $supplier_id ;
            //発注種別(完全一致)
            $where .= '   and a.order_type = ' . $order_type ;
            //作業区分(完全一致)
            if((isset($this->request->data['TrnOrder']['orders_class'])) && ($this->request->data['TrnOrder']['orders_class'] != "")){
                $where .= '   and a.order_type = ' . $order_type ;
            }
            //備考(部分一致)
            if((isset($this->request->data['TrnOrder'])) && ($this->request->data['TrnOrder'] != "")){
                $where .= " and a.recital ILIKE '%". $this->request->data['TrnOrder']['comment'] ."%'";
            }
            $where .= ' and a.trn_order_header_id is null ';
            $ret = $this->order_decision_search($supplier_id ,$order_facility_id, $order_department_id ,$order_type ,$where );
            
            $result[] = $ret;
        }

        $this->set('result',$result);
        // 2度押し対策
        $this->_createTransactionToken();
    }

    //発注確定結果
    function order_decision_result () {
        $this->TrnOrder->begin();
        $deposit_flg = false; //預託フラグ
        $order_header_ids = array();
        $order_header_ids_per_supplier = array(); // 業者毎の発注書を作成する際に使用する。
        
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['TrnOrder']['token'] !== $this->Session->read('TrnOrder.token')) {
            $this->Session->write('TrnOrder.error', true);
            $this->_createTransactionToken();
            $this->render('order_decision_confirm');
            return;
        }else{
            // 行ロック
            $this->TrnOrder->query(' select * from trn_orders as a where a.id in (' . join(',',$this->request->data['TrnOrder']['id']).  ') for update ');
            
            $userId = $this->Session->read('Auth.MstUser.id');
            $this->set('order_classes_List',$this->_getOrderClassesList());
            $now = date('Y/m/d H:i:s');
            $work_date = $this->request->data['TrnOrder']['work_date'];

            //センター部署を取得
            $center_department_id = $this->getDepartmentId( $this->Session->read('Auth.facility_id_selected') ,
                                                            array(Configure::read('DepartmentType.warehouse'))
                                                            );

            // 選択した発注IDから発注ヘッダ作成元ネタを取得
            $sql  = ' select ';
            $sql .= '     a.department_id_from ';
            $sql .= '   , a.department_id_to ';
            $sql .= '   , a.order_type ';
            $sql .= '   , e.id as supplier_id ';
            $sql .= '   , e.facility_formal_name as supplier_name ';
            $sql .= '   , e.email as supplier_email ';
            $sql .= '   , count(*) as count  ';
            $sql .= '   , sum(a.quantity) as sum';
            $sql .= ' from ';
            $sql .= '   trn_orders as a  ';
            $sql .= '   left join mst_departments as b';
            $sql .= '     on b.id = a.department_id_from ';
            $sql .= '   left join mst_facilities as c';
            $sql .= '     on c.id = b.mst_facility_id ';
            $sql .= '   left join mst_departments as d ';
            $sql .= '     on d.id = a.department_id_to ';
            $sql .= '   left join mst_facilities as e';
            $sql .= '     on e.id = d.mst_facility_id ';
            $sql .= ' where ';
            $sql .= '   a.id in (' . join(',',$this->request->data['TrnOrder']['id']).  ')  ';
            $sql .= ' group by ';
            $sql .= '   a.department_id_from ';
            $sql .= '   , a.department_id_to ';
            $sql .= '   , a.order_type  ';
            $sql .= '   , e.id ';
            $sql .= '   , e.facility_formal_name ';
            $sql .= '   , e.email ';
            $sql .= ' order by ';
            $sql .= '   a.department_id_from ';
            $sql .= '   , a.department_id_to ';
            $sql .= '   , a.order_type ';
            $header_data = $this->TrnOrder->query($sql);

            //明細情報と、日付項目チェック
            $sql  = ' select ';
            $sql .= '     a.id';
            $sql .= '   , a.department_id_from ';
            $sql .= '   , a.department_id_to ';
            $sql .= '   , a.order_type ';
            $sql .= '   , a.before_quantity ';
            $sql .= '   , a.quantity ';
            $sql .= '   , a.mst_item_unit_id';
            $sql .= '   , c.facility_code';
            $sql .= '   , ( case ';
            $sql .= "       when f.id is null then '有効な仕入設定がありません。'";
            $sql .= "       when g.start_date > '" . $work_date .  "' then '包装単位適用日以前の発注です。'";
            $sql .= "       when g.end_date < '" . $work_date . "' then '包装単位中止日以降の発注です。'";
            $sql .= "       when h.start_date > '" . $work_date . "' then '商品採用開始日以前の発注です。'";
            $sql .= "       when h.end_date < '" . $work_date . "' then '商品採用中止日以降の発注です。'";
            $sql .= "       else '' ";
            $sql .= '     end ) as alert_message';
            $sql .= '   , f.transaction_price';
            $sql .= ' from ';
            $sql .= '   trn_orders as a  ';
            $sql .= '   left join mst_departments as b';
            $sql .= '     on b.id = a.department_id_from ';
            $sql .= '   left join mst_facilities as c';
            $sql .= '     on c.id = b.mst_facility_id ';
            $sql .= '   left join mst_departments as d ';
            $sql .= '     on d.id = a.department_id_to ';
            $sql .= '   left join mst_facilities as e';
            $sql .= '     on e.id = d.mst_facility_id ';
            $sql .= '   left join mst_transaction_configs as f';
            $sql .= '     on f.mst_item_unit_id = a.mst_item_unit_id ';
            $sql .= '    and f.partner_facility_id = e.id ';
            $sql .= '    and f.is_deleted = false ';
            $sql .= "    and f.start_date <= '" . $work_date . "'";
            $sql .= "    and f.end_date > '" . $work_date . "'";
            $sql .= '   left join mst_item_units as g ';
            $sql .= '     on g.id = a.mst_item_unit_id ';
            $sql .= '   left join mst_facility_items as h ';
            $sql .= '     on h.id = g.mst_facility_item_id ';
            $sql .= '   left join mst_transaction_configs as k';
            $sql .= '     on k.mst_item_unit_id = a.mst_item_unit_id ';
            $sql .= '    and k.partner_facility_id = e.id ';
            $sql .= '    and k.is_deleted = false ';
            $sql .= "    and k.start_date <= '" . $work_date . "'";
            $sql .= "    and k.end_date > '" . $work_date . "'";
            $sql .= ' where ';
            $sql .= '   a.id in (' . join(',',$this->request->data['TrnOrder']['id']).  ')  ';
            $sql .= ' order by ';
            $sql .= '   a.department_id_from ';
            $sql .= '   , a.department_id_to ';
            $sql .= '   , a.order_type ';
            $order= $this->TrnOrder->query($sql);
            
            $shipping_type = '';
            $trade_type = '';
            
            //ヘッダごとにループ
            foreach($header_data as $h){
                $work_no = $this->setWorkNo4Header($work_date,"08");
                //発注ヘッダ作成
                $TrnOrderHeader = array(
                    'TrnOrderHeader'=> array(
                        'work_no'            => $work_no,
                        'work_date'          => $work_date,
                        'order_type'         => $h[0]['order_type'],
                        'order_status'       => Configure::read('OrderStatus.ordered'),
                        'department_id_from' => $h[0]['department_id_from'],
                        'department_id_to'   => $h[0]['department_id_to'],
                        'detail_count'       => $h[0]['count'],
                        'creater'            => $userId,
                        'created'            => $now,
                        'modifier'           => $userId,
                        'modified'           => $now,
                        )
                    );

                $this->TrnOrderHeader->create();
                // SQL実行
                if (!$this->TrnOrderHeader->save($TrnOrderHeader)) {
                    $this->TrnOrder->rollback();
                    $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('order_decision');
                }
                $order_header_id = $this->TrnOrderHeader->getLastInsertID();
                $order_header_ids[] = $order_header_id;
                $order_header_ids_per_supplier[$h[0]['supplier_id']]['order_header'] = $h[0];
                $order_header_ids_per_supplier[$h[0]['supplier_id']]['order_header_ids'][] = $order_header_id;
                
                
                if($h[0]['order_type'] == Configure::read('OrderTypes.replenish') ||
                   $h[0]['order_type'] == Configure::read('OrderTypes.nostock') ||
                   $h[0]['order_type'] == Configure::read('OrderTypes.ExNostock')){
                    //預託品 or 非在庫品
                    if($h[0]['order_type'] == Configure::read('OrderTypes.replenish') ){
                        $shipping_type = Configure::read('ShippingType.deposit');
                        $trade_type = Configure::read('ClassesType.Deposit');
                        $deposit_flg = true; //預託フラグON
                    }elseif($h[0]['order_type'] == Configure::read('OrderTypes.nostock')){
                        $shipping_type = Configure::read('ShippingType.nostock');
                        $trade_type = Configure::read('ClassesType.NoStock');
                    }elseif($h[0]['order_type'] == Configure::read('OrderTypes.ExNostock')){
                        $shipping_type = Configure::read('ShippingType.nostock');
                        $trade_type = Configure::read('ClassesType.ExNoStock');
                    }
                    
                    //入荷ヘッダ
                    $TrnReceivingHeader = array(
                        'TrnReceivingHeader'=> array(
                            'work_no'            => $work_no,
                            'work_date'          => $work_date,
                            'receiving_type'     => Configure::read('ReceivingType.arrival'),   //入荷区分
                            'receiving_status'   => Configure::read('ReceivingStatus.arrival'), //入荷状態 1:入荷済、2:入庫済
                            'department_id_from' => $h[0]['department_id_to'],                  //移動元部署参照キー
                            'department_id_to'   => $h[0]['department_id_from'],                //移動先部署参照キー
                            'detail_count'       => $h[0]['count'],
                            'creater'            => $userId,
                            'modifier'           => $userId,
                            'created'            => $now,
                            'modified'           => $now,
                            )
                        );

                    $this->TrnReceivingHeader->create();
                    // SQL実行
                    if (!$this->TrnReceivingHeader->save($TrnReceivingHeader)) {
                        $this->TrnOrder->rollback();
                        $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('order_decision');
                    }
                    $receiving_header_id = $this->TrnReceivingHeader->getLastInsertID();

                    //入庫ヘッダ
                    $TrnStorageHeader = array(
                        'TrnStorageHeader'=> array(
                            'work_no'            => $work_no,
                            'work_date'          => $work_date,
                            'storage_type'       => Configure::read('StorageType.normal'),    //入庫区分
                            'storage_status'     => Configure::read('StorageStatus.arrival'), //入庫状態
                            'mst_department_id'  => $h[0]['department_id_from'] ,             //部署参照キー
                            'detail_count'       => $h[0]['sum'],
                            'creater'            => $userId,
                            'modifier'           => $userId,
                            'created'            => $now,
                            'modified'           => $now,
                            )
                        );

                    $this->TrnStorageHeader->create();
                    // SQL実行
                    if (!$this->TrnStorageHeader->save($TrnStorageHeader)) {
                        $this->TrnOrder->rollback();
                        $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('order_decision');
                    }
                    $storage_header_id = $this->TrnStorageHeader->getLastInsertID();

                    //出荷ヘッダ
                    $TrnShippingHeader = array(
                        'TrnShippingHeader'=> array(
                            'work_no'            => $work_no,
                            'work_date'          => $work_date,
                            'shipping_type'      => $shipping_type,                            //出荷区分
                            'shipping_status'    => Configure::read('ShippingStatus.ordered'), //出荷状態
                            'department_id_from' => $center_department_id,                     //移動元部署参照キー
                            'department_id_to'   => $h[0]['department_id_from'],               //移動先部署参照キー
                            'detail_count'       => $h[0]['sum'],
                            'creater'            => $userId,
                            'created'            => $now,
                            'modifier'           => $userId,
                            'modified'           => $now,
                            )
                        );

                    $this->TrnShippingHeader->create();
                    // SQL実行
                    if (!$this->TrnShippingHeader->save($TrnShippingHeader)) {
                        $this->TrnOrder->rollback();
                        $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('order_decision');
                    }
                    $shipping_header_id = $this->TrnShippingHeader->getLastInsertID();
                }
                $seq = 1;
                $seq2= 1;
                foreach($order as $o){
                    if( $h[0]['department_id_from'] == $o[0]['department_id_from'] &&
                        $h[0]['department_id_to']   == $o[0]['department_id_to'] &&
                        $h[0]['order_type']         == $o[0]['order_type']
                        ) {

                        $o[0]['quantity']  = $this->request->data['TrnOrder']['quantity'][$o[0]['id']];
                        $o[0]['recital']   = $this->request->data['TrnOrder']['comment'][$o[0]['id']];
                        if($o[0]['alert_message'] == '' ) {
                            //発注明細
                            $order_data = array();
                            $order_data['TrnOrder.work_no']             = $work_no;
                            $order_data['TrnOrder.work_seq']            = $seq;
                            $order_data['TrnOrder.work_date']           = "'" .$work_date . "'";
                            if(!empty($o[0]['recital'])){
                                $order_data['TrnOrder.recital']             = "'" . $o[0]['recital'] . "'";
                            }
                            $order_data['TrnOrder.order_status']        = Configure::read('OrderStatus.ordered');
                            $order_data['TrnOrder.stocking_price']      = $o[0]['transaction_price'];
                            $order_data['TrnOrder.quantity']            = $o[0]['quantity'];
                            $order_data['TrnOrder.remain_count']        = $o[0]['quantity'];
                            $order_data['TrnOrder.trn_order_header_id'] = $order_header_id;
                            $order_data['TrnOrder.modifier']            = $userId;
                            $order_data['TrnOrder.modified']            = "'" . $now . "'";
                            
                            $this->TrnOrder->create();
                            $ret = $this->TrnOrder->updateAll(
                                $order_data,
                                array(
                                    'TrnOrder.id' => $o[0]['id'],
                                    ),
                                -1
                                );
                            if(!$ret) {
                                $this->TrnOrder->rollback();
                                $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                $this->redirect('order_decision');
                            }

                            
                            //在庫更新 発注確定時に、数量を変更した場合に対応
                            $this->TrnStock->create();
                            $ret = $this->TrnStock->updateAll(
                                array(
                                    'TrnStock.requested_count' => 'TrnStock.requested_count - ' . $o[0]['before_quantity'] . ' + ' . $o[0]['quantity'],
                                    'TrnStock.modifier'        => $userId,
                                    'TrnStock.modified'        => "'" . $now . "'"
                                    ),
                                array(
                                    'TrnStock.id' => $this->getStockRecode( $o[0]['mst_item_unit_id'],
                                                                            $o[0]['department_id_from'])
                                    ),
                                -1
                                );
                            if(!$ret) {
                                $this->TrnOrder->rollback();
                                $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                $this->redirect('order_decision');
                            }
                            
                            if($o[0]['order_type'] == Configure::read('OrderTypes.replenish') ||
                               $o[0]['order_type'] == Configure::read('OrderTypes.nostock') ||
                               $o[0]['order_type'] == Configure::read('OrderTypes.ExNostock') 
                               ) {
                                //預託品 or 非在庫品

                                //入荷明細
                                $TrnReceiving = array(
                                    'TrnReceiving'=> array(
                                        'work_no'                 => $work_no,                                    //作業番号
                                        'work_seq'                => $seq,                                        //作業連番
                                        'work_date'               => $work_date,                                  //作業日時
                                        'receiving_type'          => Configure::read('ReceivingType.arrival'),    //入荷区分
                                        'receiving_status'        => Configure::read('ReceivingStatus.arrival'),  //入荷状態
                                        'mst_item_unit_id'        => $o[0]['mst_item_unit_id'],                   //包装単位参照キー
                                        'department_id_from'      => $o[0]['department_id_to'],                   //移動元参照キー
                                        'department_id_to'        => $o[0]['department_id_from'],                 //移動先参照キー
                                        'quantity'                => $o[0]['quantity'],
                                        'stocking_price'          => $o[0]['transaction_price'],                  //仕入価格
                                        'sales_price'             => $o[0]['transaction_price'],                  //売上価格
                                        'trn_order_id'            => $o[0]['id'],
                                        'is_deleted'              => FALSE,
                                        'creater'                 => $userId,
                                        'created'                 => $now,
                                        'modifier'                => $userId,
                                        'modified'                => $now,
                                        'trn_receiving_header_id' => $receiving_header_id,
                                        )
                                    );
                                $this->TrnReceiving->create();

                                if (!$this->TrnReceiving->save($TrnReceiving)) {
                                    $this->TrnOrder->rollback();
                                    $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                    $this->redirect('order_decision');
                                }
                                //IDを取得
                                $receiving_id = $this->TrnReceiving->getInsertID();

                                //数量分ループ
                                for($i = 1; $i<=$o[0]['quantity']; $i++){
                                    //シール作成
                                    $hospital_sticker_no = $this->getHospitalStickerNo(date('Y/m/d'), $o[0]['facility_code']);
                                    $TrnSticker = array(
                                        'TrnSticker'=> array(
                                            'facility_sticker_no'  => '',
                                            'hospital_sticker_no' => $hospital_sticker_no,
                                            'mst_item_unit_id'    => $o[0]['mst_item_unit_id'],
                                            'mst_department_id'   => $center_department_id,
                                            'trade_type'          => $trade_type,
                                            'quantity'            => 1,
                                            'trn_receiving_id'    => $receiving_id,
                                            'original_price'      => $o[0]['transaction_price'],
                                            'transaction_price'   => $o[0]['transaction_price'],
                                            'trn_order_id'        => $o[0]['id'],
                                            'is_deleted'          => TRUE,
                                            'has_reserved'        => FALSE,
                                            'creater'             => $userId,
                                            'created'             => $now,
                                            'modifier'            => $userId,
                                            'modified'            => $now,
                                            )
                                        );
                                    $this->TrnSticker->create();

                                    if (!$this->TrnSticker->save($TrnSticker)) {
                                        $this->TrnOrder->rollback();
                                        $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                        $this->redirect('order_decision');
                                    }
                                    //IDを取得
                                    $sticker_id = $this->TrnSticker->getInsertID();

                                    //入庫明細
                                    $TrnStorage = array(
                                        'TrnStorage'=> array(
                                            'work_no'               => $work_no,
                                            'work_seq'              => $seq2,
                                            'work_date'             => $work_date,
                                            'storage_type'          => Configure::read('StorageType.normal'),
                                            'storage_status'        => Configure::read('StorageStatus.arrival'),
                                            'mst_item_unit_id'      => $o[0]['mst_item_unit_id'],
                                            'mst_department_id'     => $center_department_id,
                                            'quantity'              => 1,
                                            'trn_sticker_id'        => $sticker_id,
                                            'trn_receiving_id'      => $receiving_id,
                                            'is_deleted'            => FALSE,
                                            'creater'               => $userId,
                                            'created'               => $now,
                                            'modifier'              => $userId,
                                            'modified'              => $now,
                                            'trn_storage_header_id' => $storage_header_id,
                                            )
                                        );
                                    $this->TrnStorage->create();

                                    if (!$this->TrnStorage->save($TrnStorage)) {
                                        $this->TrnOrder->rollback();
                                        $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                        $this->redirect('order_decision');
                                    }
                                    //IDを取得
                                    $storage_id = $this->TrnStorage->getInsertID();

                                    //出荷明細
                                    $TrnShipping = array(
                                        'TrnShipping'=> array(
                                            'work_no'                => $work_no,
                                            'work_seq'               => $seq2,
                                            'work_date'              => $work_date,
                                            'shipping_type'          => $shipping_type,
                                            'shipping_status'        => Configure::read('ShippingStatus.ordered'),
                                            'mst_item_unit_id'       => $o[0]['mst_item_unit_id'],
                                            'department_id_from'     => $center_department_id,
                                            'department_id_to'       => $o[0]['department_id_from'],
                                            'quantity'               => 1,
                                            'trn_sticker_id'         => $sticker_id,
                                            'is_deleted'             => FALSE,
                                            'creater'                => $userId,
                                            'modifier'               => $userId,
                                            'created'                => $now,
                                            'modified'               => $now,
                                            'trn_shipping_header_id' => $shipping_header_id,
                                            )
                                        );
                                    $this->TrnShipping->create();

                                    if (!$this->TrnShipping->save($TrnShipping)) {
                                        $this->TrnOrder->rollback();
                                        $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                        $this->redirect('order_decision');
                                    }
                                    //IDを取得
                                    $shipping_id = $this->TrnShipping->getInsertID();
                                    
                                    //シール移動履歴（発注）
                                    $TrnStickerRecord = array(
                                        'TrnStickerRecord'=> array(
                                            'move_date'         => $work_date,
                                            'move_seq'          => $this->getNextRecord($sticker_id),
                                            'record_type'       => Configure::read('RecordType.order'),
                                            'record_id'         => $o[0]['id'],
                                            'trn_sticker_id'    => $sticker_id,
                                            'mst_department_id' => $o[0]['department_id_to'],
                                            'is_deleted'        => false,
                                            'creater'           => $userId,
                                            'created'           => $now,
                                            'modifier'          => $userId,
                                            'modified'          => $now,
                                            )
                                        );
                                    $this->TrnStickerRecord->create();

                                    if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                                        $this->TrnOrder->rollback();
                                        $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                        $this->redirect('order_decision');
                                    }

                                    //シール更新（出荷ID、入庫ID）
                                    $ret = $this->TrnSticker->updateAll(
                                        array(
                                            'trn_storage_id'   => $storage_id,
                                            'trn_shipping_id'  => $shipping_id,
                                            )
                                        ,array(
                                            'TrnSticker.id' => $sticker_id,
                                            )
                                        ,-1);

                                    if(!$ret) {
                                        $this->TrnOrder->rollback();
                                        $this->Session->setFlash('発注確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                        $this->redirect('order_decision');
                                    }
                                    $seq2++;
                                }
                            }
                            $seq++;
                        }
                    }
                }
            }
            
            $this->TrnOrder->commit();
            
            // 表示用のデータを取得
            $sql  = ' select ';
            $sql .= '     a.order_type ';
            $sql .= '   , c.id as from_id ';
            $sql .= '   , b.id as from_department_id';
            $sql .= '   , e.id as to_id ';
            $sql .= ' from ';
            $sql .= '   trn_orders as a ';
            $sql .= '   left join mst_departments as b ';
            $sql .= '     on b.id = a.department_id_from ';
            $sql .= '   left join mst_facilities as c ';
            $sql .= '     on c.id = b.mst_facility_id ';
            $sql .= '   left join mst_departments as d ';
            $sql .= '     on d.id = a.department_id_to ';
            $sql .= '   left join mst_facilities as e ';
            $sql .= '     on e.id = d.mst_facility_id  ';

            $sql .= ' where ';
            $sql .= '   a.id in (' . join(',',$this->request->data['TrnOrder']['id']) . ') ';
            $sql .= ' group by ';
            $sql .= '     a.order_type ';
            $sql .= '   , c.id ';
            $sql .= '   , b.id ';
            $sql .= '   , c.facility_name';
            $sql .= '   , e.id ';
            $sql .= ' order by ';
            $sql .= '     c.facility_name';
            $sql .= '   , a.order_type';
            $header = $this->TrnOrder->query($sql);
            $where = '   and a.id in (' . join(',',$this->request->data['TrnOrder']['id']) . ') ';
            $result = array();
            $supplier_ids = array();
            foreach($header as $r){
                $result[] = $this->order_decision_search(
                                $r[0]['to_id'],
                                $r[0]['from_id'],
                                $r[0]['from_department_id'],
                                $r[0]['order_type'],$where
                            );
                $supplier_ids[] = $r[0]['to_id'];
            }
            
            if ($this->Session->read('Auth.Config.OrderMail') == '1') {
                $this->sendOrderMail($order_header_ids_per_supplier);
            }
            $this->set('result' , $result);
            $this->set('order_ids' , $this->request->data['TrnOrder']['id']);
            $this->set('order_header_ids' , $order_header_ids);
            $this->set('deposit_flg' , $deposit_flg);
        }
        
        $this->render('order_decision_result_view');
    }

    /**
     *
     *　@param string $keyword
     */
    function order_history () {
        App::import('Sanitize');
        $this->setRoleFunction(19); //発注履歴
        $result = array();
        
        $order  =  'A.work_no';
        
        $this->set('order_type_List',Configure::read('OrderTypes.orderTypeName'));
        $this->set('order_classes_List',$this->_getOrderClassesList());

        //仕入先データセット
        $this->set('supplier_list' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                            array(Configure::read('FacilityType.supplier'))
                                                            ));
        
        //部署リスト
        $department_list = $this->getHospitalDepartmentList($this->Session->read('Auth.facility_id_selected'));
        $this->set('department_list' , $department_list);
        
        if(isset($this->request->data['search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            //ユーザ入力値による検索条件の作成--------------------------------------------
            $where  = ' where 1 = 1 ';
            $where  .= 'and a.order_type <> '.Configure::read('OrderTypes.direct');
            //発注番号(完全一致)
            if((isset($this->request->data['search']['work_no'])) && ($this->request->data['search']['work_no'] != '')){
                if(!isset($this->request->data['search']['yet'])){
                    $where .= " and b.work_no = '".$this->request->data['search']['work_no']."'";
                }else{
                    $where .= " and ( b.work_no = '".$this->request->data['search']['work_no']."' or b.work_no is null )";
                }
            }

            //発注日(期間)
            if((isset($this->request->data['search']['work_date_start'])) && ($this->request->data['search']['work_date_start'] != "")){
                if(!isset($this->request->data['search']['yet'])){
                    $where .= " and b.work_date >=  '".$this->request->data['search']['work_date_start']."'";
                }else{
                    $where .= " and ( b.work_date >=  '".$this->request->data['search']['work_date_start']."' or b.work_date is null ) ";
                }
            }
            if((isset($this->request->data['search']['work_date_end'])) && ($this->request->data['search']['work_date_end'] != "")){
                if(!isset($this->request->data['search']['yet'])){
                    $where .= " and b.work_date <=  '".$this->request->data['search']['work_date_end']."'";
                }else{
                    $where .= " and ( b.work_date <=  '".$this->request->data['search']['work_date_end']."' or b.work_date is null ) ";
                }
            }
            //未発注
            if(!isset($this->request->data['search']['yet'])){
                $where .= ' and a.trn_order_header_id is not null';
            }
            //取消
            if(isset($this->request->data['search']['is_deleted'])){
                $where .= ' and a.is_deleted = FALSE';
            }
            //未入荷
            if(isset($this->request->data['search']['remain_count'])){
                $where .= ' and a.remain_count > 0 ';
            }
            //仕入先id(完全一致)
            if((isset($this->request->data['search']['supplierCode'])) && ($this->request->data['search']['supplierCode'] != "")){
                $where .= " and f.facility_code = '". $this->request->data['search']['supplierCode'] . "'";
            }
            //要求元id(部署)(完全一致)
            if((isset($this->request->data['search']['departmentCode'])) && ($this->request->data['search']['departmentCode'] != "")){
                $where .= " and c.department_code = '". $this->request->data['search']['departmentCode'] . "'";
            }
            //商品ID(部分一致)
            if((isset($this->request->data['search']['internal_code'])) && ($this->request->data['search']['internal_code'] != "")){
                $where .= " and j.internal_code = '".$this->request->data['search']['internal_code']."'";
            }
            //製品番号(部分一致)
            if((isset($this->request->data['search']['item_code'])) && ($this->request->data['search']['item_code'] != "")){
                $where .= " and j.item_code ILIKE '%".$this->request->data['search']['item_code']."%'";
            }
            //作業区分(完全一致)
            if((isset($this->request->data['search']['work_type'])) && ($this->request->data['search']['work_type'] != "")){
                $where .= ' and a.work_type = '. $this->request->data['search']['work_type'];
            }
            //商品名(部分一致)
            if((isset($this->request->data['search']['item_name'])) && ($this->request->data['search']['item_name'] != "")){
                $where .= " and j.item_name ILIKE '%".$this->request->data['search']['item_name']."%'";
            }
            //販売元(部分一致)
            if((isset($this->request->data['search']['dealer_name'])) && ($this->request->data['search']['dealer_name'] != "")){
                $where .= " and k.dealer_name ILIKE '%".$this->request->data['search']['dealer_name']."%'";
            }
            //規格(部分一致)
            if((isset($this->request->data['search']['standard'])) && ($this->request->data['search']['standard'] != "")){
                $where .= " AND J.standard ILIKE '%".$this->request->data['search']['standard']."%'";
            }
            //---------------------------------------------------------------------------

            $sql  = 'select  count(a.*)             as "TrnOrder__count" ';
            $sql .= '       ,a.work_no              as "TrnOrder__work_no" ';
            $sql .= '       ,a.order_type           as "TrnOrder__order_type" ';
            $sql .= '       ,(case ';
            foreach( Configure::read('OrderTypes.orderTypeName') as $k => $v){
                $sql .= '         when a.order_type = ' . $k . " then '" . $v . "'";
            }
            $sql .= '        end)                   as "TrnOrder__order_type_name"';
            $sql .= "       ,to_char(A.work_date,'YYYY/mm/dd')";
            $sql .= '                               as "TrnOrder__work_date" ';
            $sql .= '       ,d.facility_name        as "TrnOrder__facility_name_from" ';
            $sql .= '       ,c.department_name      as "TrnOrder__department_name_from" ';
            $sql .= '       ,c.id                   as "TrnOrder__department_id_from" ';
            $sql .= '       ,f.facility_name        as "TrnOrder__facility_name_to"';
            $sql .= '       ,f.id                   as "TrnOrder__facility_id_to" ';
            $sql .= '       ,e.department_name      as "TrnOrder__department_name_to" ';
            $sql .= '       ,e.id                   as "TrnOrder__department_id_to" ';
            $sql .= '       ,g.user_name            as "TrnOrder__user_name"';
            $sql .= "       ,to_char(B.created,'yyyy/mm/dd HH24:MI:SS') ";
            $sql .= '                               as "TrnOrder__created"';
            $sql .= '       ,b.recital              as "TrnOrder__recital"';
            $sql .= "       ,coalesce(B.detail_count::varchar,'未発注') ";
            $sql .= '                               as "TrnOrder__detail_count"';
            $sql .= '       ,a.trn_order_header_id  as "TrnOrder__trn_order_header_id" ';
            $sql .= '  from trn_orders a ';
            $sql .= '     left join trn_order_headers b ';
            $sql .= '       on a.trn_order_header_id = b.id ';
            $sql .= '    inner join mst_departments c ';
            $sql .= '       on a.department_id_from = c.id ';
            $sql .= '    inner join mst_facilities d ';
            $sql .= '       on c.mst_facility_id = d.id ';
            $sql .= '    inner join mst_departments e ';
            $sql .= '       on a.department_id_to = e.id ';
            $sql .= '    inner join mst_facilities f ';
            $sql .= '       on e.mst_facility_id = f.id ';
            $sql .= '      and f.facility_type = '. Configure::read('FacilityType.supplier');
            $sql .= '     left join mst_users g ';
            $sql .= '       on b.creater = g.id ';
            $sql .= '    inner join mst_item_units i ';
            $sql .= '       on a.mst_item_unit_id = i.id ';
            $sql .= '      and i.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
            $sql .= '    inner join mst_facility_items j ';
            $sql .= '       on i.mst_facility_item_id = j.id ';
            $sql .= '    left  join mst_dealers k ';
            $sql .= '       on j.mst_dealer_id = k.id ';
            $sql .= '    inner join mst_user_belongings l ';
            $sql .= '       on l.mst_facility_id = d.id ';
            $sql .= '      and l.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '      and l.is_deleted = false';

            $sql .= $where;
            $sql .= ' group by  ';
            $sql .= '       a.trn_order_header_id ';
            $sql .= '      ,a.work_no ';
            $sql .= '      ,a.order_type ';
            $sql .= '      ,a.work_date ';
            $sql .= '      ,a.department_id_to ';
            $sql .= '      ,g.user_name ';
            $sql .= '      ,b.creater ';
            $sql .= '      ,b.created ';
            $sql .= '      ,b.recital ';
            $sql .= '      ,b.detail_count ';
            $sql .= '      ,b.id ';
            $sql .= '      ,c.id ';
            $sql .= '      ,e.id ';
            $sql .= '      ,f.id ';
            $sql .= '      ,d.id ';
            $sql .= '      ,g.id ';
            $sql .= '      ,f.facility_name ';
            $sql .= '      ,d.facility_name ';
            $sql .= '      ,c.department_name ';
            $sql .= ' order by  '.$order;
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnOrder'));
            $limit = $this->_getLimitCount();
            if($limit !== ""){
                $sql .= ' limit '.$limit;
            }
            $result = $this->TrnOrder->query($sql);
            $this->request->data = $data;
        }else{
            //初期設定
            $this->request->data['search']['work_date_start'] = date('Y/m/d',strtotime("-7 day"));
            $this->request->data['search']['work_date_end']   = date('Y/m/d');
            $this->request->data['search']['is_deleted'] = true;
            $this->request->data['search']['yet'] = true;
        }

        $this->set('result',$result);
        $this->render('order_history');
    }


    function order_detail_history ($mode=1) {
        $this->Session->write('Orderhistory.sarch_data', $this->request->data);

        $this->set('order_type_List',Configure::read('OrderTypes.orderTypeName'));

        $order_classes_List = $this->_getOrderClassesList();
        $this->set('order_classes_List',$order_classes_List);

        $where = $this->_createSearchWhere($mode);

        $order_List_Data = $this->_getHistoryListData($where);
        $this->set('order_List',$order_List_Data);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Order.token' , $token);
        $this->request->data['Order']['token'] = $token;

        $this->render('order_detail_history');
    }


    function order_delete() {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['Order']['token'] === $this->Session->read('Order.token')) {
            $this->Session->delete('Order.token');
            
            $this->TrnOrder->begin();
            $userId = $this->Session->read('Auth.MstUser.id');
            $now = date('Y/m/d H:i:s');

            $center_department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'),
                                                           array(Configure::read('FacilityType.center'))
                                                           );

            //選択したIDから関連情報を取得 と同時に行ロック
            $sql  = ' select ';
            $sql .= '     a.id                  as "TrnOrder__id"';
            $sql .= '   , a.order_type          as "TrnOrder__order_type"';
            $sql .= '   , a.order_status        as "TrnOrder__order_status"';
            $sql .= '   , a.remain_count        as "TrnOrder__remain_count"';
            $sql .= '   , a.quantity            as "TrnOrder__quantity"';
            $sql .= '   , a.mst_item_unit_id    as "TrnOrder__mst_item_unit_id"';
            $sql .= '   , a.department_id_from  as "TrnOrder__department_id_from"';
            $sql .= '   , a.trn_order_header_id as "TrnOrder__trn_order_header_id"';
            $sql .= ' from ';
            $sql .= '   trn_orders as a  ';
            $sql .= ' where ';
            $sql .= '   a.id in (' . join(',',$this->request->data['TrnOrder']['id']) . ') ';
            $sql .= ' for update ';

            $result = $this->TrnOrder->query($sql);
            foreach($result as $r){
                if($r['TrnOrder']['order_status'] == Configure::read('OrderStatus.notYetOrder')){
                    //明細削除
                    $ret = $this->TrnOrder->del(array('id'=>$r['TrnOrder']['id']));
                }else{
                    //発注の残数を減らす
                    if($r['TrnOrder']['quantity'] == $r['TrnOrder']['remain_count']){
                        //明細削除
                        $ret = $this->TrnOrder->del(array('id'=>$r['TrnOrder']['id']));
                    }else{
                        //明細の残数を0へ
                        $this->TrnOrder->create();
                        $ret = $this->TrnOrder->updateAll(
                            array(
                                'TrnOrder.remain_count' => 0,
                                'TrnOrder.quantity'     => 'TrnOrder.quantity - ' . $r['TrnOrder']['remain_count'],
                                'TrnOrder.modifier'     => $userId,
                                'TrnOrder.modified'     => "'" . $now . "'"
                                ),
                            array(
                                'TrnOrder.id' => $r['TrnOrder']['id']
                                ),
                            -1
                            );
                    }
                    if(!$ret) {
                        $this->TrnOrder->rollback();
                        $this->Session->setFlash('発注取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('order_history');
                    }

                    //預託品、非在庫品の場合
                    if(Configure::read('OrderTypes.replenish') == $r['TrnOrder']['order_type']  ||
                       Configure::read('OrderTypes.nostock') == $r['TrnOrder']['order_type'] ||
                       Configure::read('OrderTypes.ExNostock') == $r['TrnOrder']['order_type']
                       ){
                        //在庫の残数を調整
                        //在庫の在庫数、予約数を減らす
                        /*
                        $this->TrnStock->create();
                        $ret = $this->TrnStock->updateAll(
                            array(
                                'TrnStock.stock_count'   => 'TrnStock.stock_count - ' . $r['TrnOrder']['remain_count'],
                                'TrnStock.reserve_count' => 'TrnStock.reserve_count - ' . $r['TrnOrder']['remain_count'],
                                'TrnStock.modifier'      => $userId,
                                'TrnStock.modified'      => "'" . $now . "'"
                                ),
                            array(
                                'TrnStock.id' => $this->getStockRecode( $r['TrnOrder']['mst_item_unit_id'],
                                                                        $center_department_id )
                                ),
                            -1
                            );
                        if(!$ret) {
                            $this->TrnOrder->rollback();
                            $this->Session->setFlash('発注取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('order_history');
                        }
                          */

                        //入荷削除
                        $sql = ' update trn_receivings ';
                        $sql .= '  set remain_count = 0 ';
                        $sql .= '    , quantity = quantity - remain_count ';
                        $sql .= '    , modifier = ' .$userId;
                        $sql .= "    , modified = '" . $now . "'";
                        $sql .= '  where ';
                        $sql .= '      remain_count > 0 ';
                        $sql .= '  and trn_order_id =  '. $r['TrnOrder']['id'];
                        $this->TrnReceiving->query($sql);
                        $ret = $this->TrnReceiving->getAffectedRows();
                        if($ret < 0) {
                            $this->TrnOrder->rollback();
                            $this->Session->setFlash('発注取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('order_history');
                        }

                        $sql  = ' select ';
                        $sql .= '     a.id                  as "TrnSticker__id"';
                        $sql .= '   , a.trn_shipping_id     as "TrnSticker__trn_shipping_id"';
                        $sql .= '   , a.trn_storage_id      as "TrnSticker__trn_storage_id"';
                        $sql .= '   , a.hospital_sticker_no as "TrnSticker__hospital_sticker_no"';
                        $sql .= ' from ';
                        $sql .= '   trn_stickers as a  ';
                        $sql .= '   left join mst_departments as b  ';
                        $sql .= '     on b.id = a.mst_department_id  ';
                        $sql .= ' where ';
                        $sql .= '   a.trn_order_id = ' . $r['TrnOrder']['id'];
                        $sql .= '   and b.department_type = ' . Configure::read('DepartmentType.warehouse');
                        $sticker = $this->TrnSticker->query($sql);

                        foreach($sticker as $s){
                            //入庫削除
                            $ret = $this->TrnStorage->del(array('id'=>$s['TrnSticker']['trn_storage_id']));
                            if(!$ret) {
                                $this->TrnOrder->rollback();
                                $this->Session->setFlash('発注取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                $this->redirect('order_history');
                            }
                            //出荷削除
                            $ret = $this->TrnShipping->del(array('id'=>$s['TrnSticker']['trn_shipping_id']));
                            if(!$ret) {
                                $this->TrnOrder->rollback();
                                $this->Session->setFlash('発注取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                $this->redirect('order_history');
                            }
                            //部署シール削除
                            $ret = $this->TrnStickerIssue->del(array('hospital_sticker_no'=>$s['TrnSticker']['hospital_sticker_no']));
                            if(!$ret) {
                                $this->TrnOrder->rollback();
                                $this->Session->setFlash('発注取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                $this->redirect('order_history');
                            }
                            //シール削除
                            $ret = $this->TrnSticker->del(array('id'=>$s['TrnSticker']['id']));
                            if(!$ret) {
                                $this->TrnOrder->rollback();
                                $this->Session->setFlash('発注取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                $this->redirect('order_history');
                            }
                            //シール移動履歴削除
                            $ret = $this->TrnStickerRecord->del(array('trn_sticker_id'=>$s['TrnSticker']['id']));
                            if(!$ret) {
                                $this->TrnOrder->rollback();
                                $this->Session->setFlash('発注取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                $this->redirect('order_history');
                            }
                        }
                    }
                }

                //発注種別が自動計算対象外でなかったら。
                if($r['TrnOrder']['order_type'] != Configure::read('OrderTypes.temporary')
                   && $r['TrnOrder']['order_type'] !=Configure::read('OrderTypes.temporary_calc_notautocalc')
                   && $r['TrnOrder']['order_type'] !=Configure::read('OrderTypes.ExNostock')){
                    //在庫の発注済数を減らす
                    $this->TrnStock->create();
                    $ret = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.requested_count' => 'TrnStock.requested_count - ' . $r['TrnOrder']['remain_count'],
                            'TrnStock.modifier'        => $userId,
                            'TrnStock.modified'        => "'" . $now . "'"
                            ),
                        array(
                            'TrnStock.id' => $this->getStockRecode( $r['TrnOrder']['mst_item_unit_id'],
                                                                    $r['TrnOrder']['department_id_from'])
                            ),
                        -1
                        );
                    if(!$ret) {
                        $this->TrnOrder->rollback();
                        $this->Session->setFlash('発注取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('order_history');
                    }
                }

                //表示用データを取得
                if(!is_null($r['TrnOrder']['trn_order_header_id'])){
                    $sql  = ' update trn_order_headers x  ';
                    $sql .= '   set ';
                    $sql .= '     order_status = y.status  ';
                    $sql .= '   from ';
                    $sql .= '     (  ';
                    $sql .= '       select ';
                    $sql .= '             b.id                      as header_id ';
                    $sql .= '           , b.detail_count ';
                    $sql .= '           , sum(a.quantity)           as quantity ';
                    $sql .= '           , sum(a.remain_count)       as remain_count ';
                    $sql .= '           , (  ';
                    $sql .= '             case  ';
                    $sql .= '               when sum(a.remain_count) = 0  ';
                    $sql .= '               then 4 /* 入庫済み */  ';
                    $sql .= '               when sum(a.remain_count) = sum(a.quantity)  ';
                    $sql .= '               then 2 /* 発注済 */  ';
                    $sql .= '               when sum(a.remain_count) < sum(a.quantity)  ';
                    $sql .= '               then 3 /* 一部入庫済み */  ';
                    $sql .= '               end ';
                    $sql .= '           )                           as status  ';
                    $sql .= '         from ';
                    $sql .= '           trn_orders as a  ';
                    $sql .= '           left join trn_order_headers as b  ';
                    $sql .= '             on b.id = a.trn_order_header_id  ';
                    $sql .= '         where ';
                    $sql .= '           b.id = ' . $r['TrnOrder']['trn_order_header_id'];
                    $sql .= '           and a.is_deleted = false  ';
                    $sql .= '         group by ';
                    $sql .= '           b.id ';
                    $sql .= '           , b.detail_count ';
                    $sql .= '           , a.is_deleted ';
                    $sql .= '     )  as y ';
                    $sql .= '   where ';
                    $sql .= '     x.id = y.header_id ';
                    $this->TrnOrderHeader->query($sql);
                }
            }

            //コミット
            $this->TrnOrder->commit();

            $where  = ' WHERE 1 = 1';
            $where .= '   AND a.id in (' . join(',',$this->request->data['TrnOrder']['id']) . ') ';
            $order_List_Data = $this->_getHistoryListData($where);
            $this->Session->write('Order.listData', $order_List_Data);

        }else{
            $order_List_Data = $this->Session->read('Order.listData');
        }
        $this->set('order_List',$order_List_Data);

        $this->render('order_delete');
    }

    /**
     * order_voluntary 任意発注 商品検索
     * @param
     * @return
     */
    function order_voluntary_search() {
        $this->setRoleFunction(17); //任意発注
        //作業区分プルダウン
        $order_classes_List = $this->_getOrderClassesList();
        $this->set('order_classes_List',$order_classes_List);
        
        App::import('Sanitize');
        $select_item = array();
        $facility_item = array();

        //検索ボタン押下
        if (isset($this->request->data['search']['is_search'])) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            $where = '';
            // 削除済みは除く
            $where .= ' and a.is_deleted = false ';
            $where .= ' and c.is_deleted = false ';
            // 操作日で適用終了になっているものは除く
            $where .= ' and ( a.end_date is null or a.end_date > now() )';
            $where .= ' and ( c.end_date is null or c.end_date > now() )';
            
            //商品ID
            if($this->request->data['MstFacilityItem']['internal_code']!=''){
                $where .= " and a.internal_code = '" . $this->request->data['MstFacilityItem']['internal_code'] . "'";
            }
            //商品名
            if($this->request->data['MstFacilityItem']['item_name']!=''){
                $where .= " and a.item_name like '%" . $this->request->data['MstFacilityItem']['item_name'] . "%'";
            }
            //製品番号
            if($this->request->data['MstFacilityItem']['item_code']!=''){
                $where .= " and a.item_code like '%" . $this->request->data['MstFacilityItem']['item_code'] . "%'";
            }
            //規格
            if($this->request->data['MstFacilityItem']['standard_name']!=''){
                $where .= " and a.standard like '%" . $this->request->data['MstFacilityItem']['standard_name'] . "%'";
            }
            //JANコード
            if($this->request->data['MstFacilityItem']['jan_code']!=''){
                $where .= " and a.jan_code like '" . $this->request->data['MstFacilityItem']['jan_code'] . "%'";
            }
            //販売元
            if($this->request->data['MstFacilityItem']['dealer_name']!=''){
                $where .= " and b.dealer_name like '%" . $this->request->data['MstFacilityItem']['dealer_name'] . "%'";
            }
            // 予備キー1
            if($this->request->data['MstFacilityItem']['spare_key1']!=''){
                $where .= " and a.spare_key1 like '%" . $this->request->data['MstFacilityItem']['spare_key1'] . "%'";
            }
            // 予備キー1
            if($this->request->data['MstFacilityItem']['spare_key2']!=''){
                $where .= " and a.spare_key2 like '%" . $this->request->data['MstFacilityItem']['spare_key2'] . "%'";
            }
            // 予備キー1
            if($this->request->data['MstFacilityItem']['spare_key3']!=''){
                $where .= " and a.spare_key3 like '%" . $this->request->data['MstFacilityItem']['spare_key3'] . "%'";
            }
            
            //カート選択済み
            if(isset($this->request->data['select']['tempId']) && !empty($this->request->data['select']['tempId'])){
                $where .= ' and a.id not in (' . $this->request->data['select']['tempId'] . ')';
            }
            //画面上部
            $facility_item = $this->getFacilityItem($where , $this->request->data['limit']);
            //カート部
            if(isset($this->request->data['select']['tempId']) && !empty($this->request->data['select']['tempId'])){
                $where = ' and a.id in (' . $this->request->data['select']['tempId'] . ')';
            }else{
                $where = ' and 1 = 0 ';
            }
            $tmp_select_item = $this->getFacilityItem($where);
            $this->request->data = $data;
            if(!empty($tmp_select_item)){
                //カートに追加した順に並べなおす。
                foreach( explode( ',', $this->request->data['select']['tempId']) as $id){
                    foreach($tmp_select_item as $tmp){
                        if($tmp['MstFacilityItem']['id'] == $id){
                            $select_item[] = $tmp;
                            break;
                        }
                    }
                }
            }

        }
        $this->set('facility_item', $facility_item);
        $this->set('select_item', $select_item);

        $this->render('order_voluntary_search');
    }

    /**
     * order_voluntary_specified_confirm 任意発注　数量指定　確認画面
     * @param
     * @return
     */
    function order_voluntary_confirm($mode = 1) {
        $order_classes_List = $this->_getOrderClassesList();
        $this->set('order_classes_List' , $order_classes_List);
        
        $sql  = ' select ';
        $sql .= '     a.id             as "MstFacilityItem__id" ';
        $sql .= '   , a.internal_code  as "MstFacilityItem__internal_code" ';
        $sql .= '   , a.item_name      as "MstFacilityItem__item_name" ';
        $sql .= '   , a.item_code      as "MstFacilityItem__item_code" ';
        $sql .= '   , a.standard       as "MstFacilityItem__standard" ';
        $sql .= '   , b.dealer_name    as "MstFacilityItem__dealer_name" ';
        $sql .= ' from ';
        $sql .= '   mst_facility_items as a  ';
        $sql .= '   left join mst_dealers as b  ';
        $sql .= '     on b.id = a.mst_dealer_id  ';
        $sql .= ' where ';
        $sql .= '   a.id in (' . $this->request->data['select']['id'] . ' ) ';
        
        $ret = $this->MstFacilityItem->query($sql);
        foreach($ret as &$r){
            $sql  = ' select ';
            $sql .= '     a.id ';
            $sql .= '   , b.facility_name ';
            $sql .= '   , b2.facility_name as facility_name2 ';
            $sql .= '   , ( case  ';
            $sql .= '       when c.per_unit = 1  ';
            $sql .= '       then c1.unit_name  ';
            $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
            $sql .= '       end ) as unit_name ' ; 
            $sql .= '   , a.transaction_price  ';
            $sql .= '   , ( case when a2.id is null then false else true end ) as is_main  ';
            $sql .= ' from ';
            $sql .= '   mst_transaction_configs as a  ';
            $sql .= '   left join mst_transaction_mains as a2';
            $sql .= '     on a2.mst_item_unit_id = a.mst_item_unit_id ';
            $sql .= '    and a2.mst_facility_id = a.partner_facility_id ';
            $sql .= '   left join mst_facilities as b  ';
            $sql .= '     on b.id = a.partner_facility_id  ';
            $sql .= '   left join mst_facilities as b2  ';
            $sql .= '     on b2.id = a.partner_facility_id2  ';
            $sql .= '   left join mst_item_units as c  ';
            $sql .= '     on c.id = a.mst_item_unit_id  ';
            $sql .= '   left join mst_unit_names as c1  ';
            $sql .= '     on c1.id = c.mst_unit_name_id  ';
            $sql .= '   left join mst_unit_names as c2  ';
            $sql .= '     on c2.id = c.per_unit_name_id  ';
            $sql .= ' where ';
            $sql .= '   a.mst_facility_item_id = ' . $r['MstFacilityItem']['id'];
            $sql .= '   and a.is_deleted = false ';
            
            $tran = $this->MstTransactionConfig->query($sql);
            $r['MstFacilityItem']['TrnsactionData']['is_main'] = null;
            foreach($tran as $t){
                $display_pulldown = $t[0]['unit_name'] . ' / ' . $t[0]['transaction_price'] . ' / ' . $t[0]['facility_name'];
                if($t[0]['facility_name'] != $t[0]['facility_name2']){
                    $display_pulldown .= ' / ' . $t[0]['facility_name2'];
                }
                $r['MstFacilityItem']['TransactionData'][$t[0]['id']] = $display_pulldown;
                if($t[0]['is_main']){ // 代表フラグがついているIDを保存しておく。
                    $r['MstFacilityItem']['TrnsactionData']['is_main'] = $t[0]['id'];
                }
            }
        }
        unset($r);
        //カートに追加した順に並べなおす。
        foreach( explode( ',', $this->request->data['select']['id']) as $id){
            foreach($ret as $r){
                if($r['MstFacilityItem']['id'] == $id){
                    if($mode == 1){
                        //数量計算しない
                        $r['MstFacilityItem']['quantity'] = 0;
                    }else{
                        //数量計算する
                        $r['MstFacilityItem']['quantity'] = $this->_getOrderCount($id);
                    }
                    $result[] = $r;
                    break;
                }
            }
        }
        $this->request->data['TrnOrder']['order_mode'] = $mode;
        
        $this->set('result' , $result);
        // 表示データを取得
        // トークンの作成
        $this->_createTransactionToken();

        $this->render('order_voluntary_confirm');
    }

    /**
     * order_voluntary_result 任意発注　結果画面
     * @param
     * @return
     */
    function order_voluntary_result($calc = 1) {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['TrnOrder']['token'] === $this->Session->read('TrnOrder.token')) {
            $this->Session->delete('TrnOrder.token');
            $now = date('Y/m/d H:i:s');
            $trn_order_ids = array();
            if($this->request->data['TrnOrder']['order_mode']){
                //数量指定
                if($calc == 1){
                    //自動計算含む
                    $type = Configure::read('OrderTypes.temporary_autocalc');
                }else{
                    //自動計算外
                    $type = Configure::read('OrderTypes.temporary');
                }
            }else{
                //数量計算
                if($calc == 1){
                    //自動計算含む
                    $type = Configure::read('OrderTypes.temporary_calc');
                }else{
                    //自動計算外
                    $type = Configure::read('OrderTypes.temporary_calc_notautocalc');
                }
            }
            
            $this->TrnOrder->begin();
            //採用品ID、仕入れ設定IDから、包装単位IDを取得
            foreach($this->request->data['TrnOrder']['id'] as $id){
                $tran_id = $this->request->data['TrnOrder']['transaction'][$id];
                $quantity = $this->request->data['TrnOrder']['quantity'][$id];
                
                $sql  = ' select ';
                $sql .= '     a.id ';
                $sql .= '   , a.transaction_price';
                $sql .= '   , a.mst_item_unit_id ';
                $sql .= '   , a.partner_facility_id ';
                $sql .= '   , b.id                    as department_id_to ';
                $sql .= '   , c.id                    as department_id_from ';
                $sql .= '   , d.id                    as department_id_to2 ';
                $sql .= ' from ';
                $sql .= '   mst_transaction_configs as a  ';
                $sql .= '   left join mst_departments as b  ';
                $sql .= '     on b.mst_facility_id = a.partner_facility_id  ';
                $sql .= '   left join mst_departments as c  ';
                $sql .= '     on c.mst_facility_id = a.mst_facility_id  ';
                $sql .= '    and c.department_type = ' . Configure::read('DepartmentType.warehouse');
                $sql .= '   left join mst_departments as d  ';
                $sql .= '     on d.mst_facility_id = a.partner_facility_id2 ';
                $sql .= ' where ';
                $sql .= '   a.id = ' . $tran_id;
                $ret = $this->MstTransactionConfig->query($sql);
                //発注明細
                $TrnOrder = array(
                    'TrnOrder'=> array(
                        'work_no'             => '',
                        'work_seq'            => 0,
                        'work_date'           => null,
                        'work_type'           => $this->request->data['TrnOrder']['work_type'][$id],
                        'recital'             => $this->request->data['TrnOrder']['recital'][$id],
                        'order_type'          => $type ,
                        'order_status'        => Configure::read('OrderStatus.notYetOrder'),
                        'mst_item_unit_id'    => $ret[0][0]['mst_item_unit_id'],
                        'department_id_from'  => $ret[0][0]['department_id_from'], 
                        'department_id_to'    => $ret[0][0]['department_id_to'],
                        'department_id_to2'   => $ret[0][0]['department_id_to2'],
                        'before_quantity'     => $quantity,
                        'quantity'            => $quantity,
                        'remain_count'        => $quantity,
                        'stocking_price'      => $ret[0][0]['transaction_price'],
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        'trn_order_header_id' => null,
                        'fixed_num_used'      => Configure::read('FixedType1')
                        )
                    );
                $this->TrnOrder->create();
                // SQL実行
                if (!$this->TrnOrder->save($TrnOrder)) {
                    $this->TrnOrder->rollback();
                    $this->Session->setFlash('発注中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('order_voluntary_search');
                }
                $trn_order_ids[] = $this->TrnOrder->getLastInsertId();
                
                //自動計算に含む場合
                if($calc == 1){
                    //在庫レコード更新
                    $stock_id = $this->getStockRecode($ret[0][0]['mst_item_unit_id'],
                                                      $ret[0][0]['department_id_from']);
                    
                    $this->TrnStock->create();
                    $res = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.requested_count' => "TrnStock.requested_count + {$quantity} ",
                            'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'        => "'".$now."'",
                            ),
                        array(
                            'TrnStock.id' => $stock_id,
                            ),
                        -1);
                    
                    if(false === $res){
                        $this->TrnOrder->rollback();
                        $this->Session->setFlash('発注中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('order_voluntary_search');
                    }
                }
            }
            
            $this->TrnOrder->commit();
            //表示用データ取得
            $sql  = ' select ';
            $sql .= '     c.internal_code            as "TrnOrder__internal_code" ';
            $sql .= '   , c.item_name                as "TrnOrder__item_name" ';
            $sql .= '   , c.item_code                as "TrnOrder__item_code" ';
            $sql .= '   , c.standard                 as "TrnOrder__standard" ';
            $sql .= '   , d.dealer_name              as "TrnOrder__dealer_name" ';
            $sql .= '   , a.quantity                 as "TrnOrder__quantity" ';
            $sql .= '   , e.name                     as "TrnOrder__work_type_name" ';
            $sql .= '   , a.recital                  as "TrnOrder__recital" ';
            $sql .= '   , g.facility_name            as "TrnOrder__facility_name" ';
            $sql .= '   , i.facility_name            as "TrnOrder__facility_name2" ';
            $sql .= '   , a.stocking_price           as "TrnOrder__stoking_price" ';
            $sql .= '   , (  ';
            $sql .= '     case  ';
            $sql .= '       when b.per_unit = 1  ';
            $sql .= '       then b1.unit_name  ';
            $sql .= "       else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
            $sql .= '       end ';
            $sql .= '   )                            as "TrnOrder__unit_name"  ';
            $sql .= ' from ';
            $sql .= '   trn_orders as a  ';
            $sql .= '   left join mst_item_units as b  ';
            $sql .= '     on b.id = a.mst_item_unit_id  ';
            $sql .= '   left join mst_unit_names as b1  ';
            $sql .= '     on b1.id = b.mst_unit_name_id  ';
            $sql .= '   left join mst_unit_names as b2  ';
            $sql .= '     on b2.id = b.per_unit_name_id  ';
            $sql .= '   left join mst_facility_items as c  ';
            $sql .= '     on c.id = b.mst_facility_item_id  ';
            $sql .= '   left join mst_dealers as d  ';
            $sql .= '     on d.id = c.mst_dealer_id  ';
            $sql .= '   left join mst_classes as e  ';
            $sql .= '     on e.id = a.work_type  ';
            $sql .= '   left join mst_departments as f  ';
            $sql .= '     on f.id = a.department_id_to  ';
            $sql .= '   left join mst_facilities as g  ';
            $sql .= '     on g.id = f.mst_facility_id  ';
            $sql .= '   left join mst_departments as h  ';
            $sql .= '     on h.id = a.department_id_to2  ';
            $sql .= '   left join mst_facilities as i  ';
            $sql .= '     on i.id = h.mst_facility_id  ';
            $sql .= ' where ';
            $sql .= '   a.trn_order_header_id is null  ';
            $sql .= '   and a.id in (' .join(',',$trn_order_ids). ')';
            $sql .= '   order by a.id ';
            
            $result = $this->TrnOrder->query($sql);
            $this->set('result',$result);
            //セッション格納
            $this->Session->Write('Order.result' , $result);
        } else {
            //2度押し、リロード対策
            
            //セッション読み込み
            $this->set('result' , $this->Session->Read('Order.result'));
        }
    }


    /**
     * sales_print 売上依頼票印刷  検索画面
     * @param
     * @return
     */
    function sales_print() {
        App::import('Sanitize');

        $this->setRoleFunction(20); //売上依頼表印刷

        $res = array();
        $this->set('order_type_List',Configure::read('OrderTypes.orderTypeName'));
        $this->set('order_classes_List',$this->getClassesList($this->Session->read('Auth.facility_id_selected') , 1 ));

        
        $departmentList = array();
        //施設データセット
        $this->set('facilityList' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                           array(Configure::read('FacilityType.hospital'))
                                                           )
                   );

        //検索ボタン押下
        if(isset($this->request->data['search']['is_search']) ){

            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            //検索絞込み条件追加
            $where = '';
            if(isset($this->request->data['TrnSalesRequestHeader']['work_no']) && ($this->request->data['TrnSalesRequestHeader']['work_no'] !== '')){
                $where .= " and s.work_no = '" . $this->request->data['TrnSalesRequestHeader']['work_no'] . "'";
            }
            if(isset($this->request->data['TrnConsume']['work_type']) && ($this->request->data['TrnConsume']['work_type'] !== '')){
                $where .= ' and b.work_type = ' . $this->request->data['TrnConsume']['work_type'];
            }
            if(isset($this->request->data['TrnConsume']['work_date_start']) && ($this->request->data['TrnConsume']['work_date_start'] !== '')){
                $where .= " and b.work_date >='" . $this->request->data['TrnConsume']['work_date_start'] . "'";
            }
            if(isset($this->request->data['TrnConsume']['work_date_end']) && ($this->request->data['TrnConsume']['work_date_end'] !== '')){
                $where .= " and b.work_date <='" .$this->request->data['TrnConsume']['work_date_end'] . "'";
            }
            if(isset($this->request->data['TrnConsume']['trn_consume_headers_id'])){
                $where .= ' and b.trn_sales_request_header_id IS NULL';
            }
            if(isset($this->request->data['MstFacility']['facilityCode']) && ($this->request->data['MstFacility']['facilityCode'] !== '')){
                $where .= " and f.facility_code = '" . $this->request->data['MstFacility']['facilityCode']. "'";
                $departmentList = $this->getDepartmentsList($this->request->data['MstFacility']['facilityCode']);
            }
            if(isset($this->request->data['MstDepartment']['departmentCode']) && ($this->request->data['MstDepartment']['departmentCode'] !== '')){
                $where .= " and e.department_code = '" . $this->request->data['MstDepartment']['departmentCode'] . "'";
            }
            if(isset($this->request->data['TrnSticker']['lot_no']) && ($this->request->data['TrnSticker']['lot_no'] !== '')){
                $where .= " and d.lot_no ILIKE '%".$this->request->data['TrnSticker']['lot_no']."%'";
            }
            if(isset($this->request->data['MstFacilityItem']['internal_code']) && ($this->request->data['MstFacilityItem']['internal_code'] !== '')){
                $where .= " and l.internal_code = '" . $this->request->data['MstFacilityItem']['internal_code'] . "'";
            }
            if(isset($this->request->data['MstFacilityItem']['item_code']) && ($this->request->data['MstFacilityItem']['item_code'] !== '')){
                $where .= " and l.item_code ILIKE '%".$this->request->data['MstFacilityItem']['item_code']."%'";
            }
            if(isset($this->request->data['MstFacilityItem']['item_name']) && ($this->request->data['MstFacilityItem']['item_name'] !== '')){
                $where .= " and l.item_name ILIKE '%" .$this->request->data['MstFacilityItem']['item_name']."%'";
            }
            if(isset($this->request->data['MstFacilityItem']['standard']) && ($this->request->data['MstFacilityItem']['standard'] !== '')){
                $where .= " and l.standard ILIKE '%".$this->request->data['MstFacilityItem']['standard']."%'";
            }
            if(isset($this->request->data['MstDealer']['dealer_name']) && ($this->request->data['MstDealer']['dealer_name'] !== '')){
                $where .= " and n.dealer_name LIKE '%".$this->request->data['MstDealer']['dealer_name']."%'";
            }

            $sql  = ' select ';
            $sql .= '       b.id                                as "TrnConsume__id" ';
            $sql .= '     , b.work_type                         as "TrnConsume__work_type" ';
            $sql .= '     , a.id                                as "TrnClaim__id" ';
            $sql .= '     , a.unit_price                        as "TrnClaim__unit_price" ';
            $sql .= '     , a.claim_price                       as "TrnClaim__claim_price"  ';
            $sql .= '     , to_char(a.claim_date, \'YYYY/mm/dd\') as "TrnClaim__claim_date" ';
            $sql .= '     , f.facility_name                     as "MstFacilitySale__facility_name" ';
            $sql .= '     , e.department_name                   as "MstDepartmentSale__department_name" ';
            $sql .= '     , g.facility_name                     as "MstFacilityTo__facility_name" ';
            $sql .= '     , s.work_no                           as "TrnSalesRequestHeader__work_no" ';
            $sql .= '     , s.id                                as "TrnSalesRequestHeader__id" ';
            $sql .= '     , l.standard                          as "MstFacilityItem__standard" ';
            $sql .= '     , l.internal_code                     as "MstFacilityItem__internal_code" ';
            $sql .= '     , l.item_name                         as "MstFacilityItem__item_name" ';
            $sql .= '     , l.item_code                         as "MstFacilityItem__item_code" ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when k.per_unit = 1  ';
            $sql .= '         then o.unit_name  ';
            $sql .= "         else o.unit_name || '(' || k.per_unit || p.unit_name || ')'  ";
            $sql .= '         end ';
            $sql .= '     )                                     as "MstItemUnit__unit_name" ';
            $sql .= '     , d.hospital_sticker_no               as "TrnSticker__hospital_sticker_no" ';
            $sql .= '     , d.lot_no                            as "TrnSticker__lot_no" ';
            $sql .= '     , t.user_name                         as "MstUser__user_name" ';
            $sql .= '     , n.dealer_name                       as "MstDealer__dealer_name" ';

            $sql .= '   from ';
            $sql .= '     trn_claims as a  ';
            $sql .= '     inner join trn_consumes as b  ';
            $sql .= '       on b.is_deleted = false  ';
            $sql .= '       and b.id = a.trn_consume_id  ';
            $sql .= '     left join mst_transaction_configs as c  '; //初期在庫で預託の場合、発注が無いので仕入先が不明なので、暫定で代表が立っている業者をとる
            $sql .= '       on c.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '       and c.start_date <= a.claim_date  ';
            $sql .= '       and c.end_date > a.claim_date  ';
            $sql .= '     left join mst_transaction_mains as c1 ';
            $sql .= '       on c1.mst_facility_item_id = c.mst_facility_item_id';
            $sql .= '       and c1.mst_facility_id = c.partner_facility_id ';
            $sql .= '       and c1.mst_item_unit_id = c.mst_item_unit_id ';
            $sql .= '     inner join trn_stickers as d  ';
            $sql .= '       on d.id = b.trn_sticker_id  ';
            $sql .= '       and d.is_deleted = false  ';
            $sql .= '    left join trn_orders as d1  ';
            $sql .= '      on d1.id = d.trn_order_id  ';
            $sql .= '    left join mst_departments as d2  ';
            $sql .= '      on d2.id = d1.department_id_to ';
            $sql .= '     inner join mst_departments as e  ';
            $sql .= '       on e.id = b.mst_department_id  ';
            $sql .= '       and e.is_deleted = false  ';
            $sql .= '     inner join mst_facilities as f  ';
            $sql .= '       on f.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .= '       and f.id = e.mst_facility_id  ';
            $sql .= '       and f.is_deleted = false  ';
            $sql .= '     inner join mst_facilities as g  ';
            $sql .= '       on g.facility_type = ' . Configure::read('FacilityType.supplier');
            $sql .= '       and g.id = coalesce( d2.mst_facility_id, c.partner_facility_id ) ';
            $sql .= '     inner join mst_departments as h  ';
            $sql .= '       on h.mst_facility_id = g.id  ';
            $sql .= '     inner join mst_departments as i  ';
            $sql .= '       on i.id = b.mst_department_id  ';
            $sql .= '     inner join mst_facilities as j  ';
            $sql .= '       on j.facility_type = ' . Configure::read('FacilityType.hospital'); 
            $sql .= '       and j.id = i.mst_facility_id  ';
            $sql .= '     inner join mst_item_units as k  ';
            $sql .= '       on a.mst_item_unit_id = k.id  ';
            $sql .= '     inner join mst_facility_items as l  ';
            $sql .= '       on l.id = k.mst_facility_item_id  ';
            $sql .= '       and l.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     left join mst_facilities as m  ';
            $sql .= '       on m.id = l.mst_owner_id  ';
            $sql .= '     left join mst_dealers as n  ';
            $sql .= '       on n.id = l.mst_dealer_id  ';
            $sql .= '     inner join mst_unit_names as o  ';
            $sql .= '       on k.mst_unit_name_id = o.id  ';
            $sql .= '     inner join mst_unit_names as p  ';
            $sql .= '       on k.per_unit_name_id = p.id  ';
            $sql .= '     inner join mst_facility_relations as q  ';
            $sql .= '       on q.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '       and q.partner_facility_id = g.id  ';
            $sql .= '       and q.is_deleted = false  ';
            $sql .= '     inner join mst_facility_relations as r  ';
            $sql .= '       on r.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '       and r.partner_facility_id = j.id  ';
            $sql .= '     left join trn_sales_request_headers as s  ';
            $sql .= '       on s.id = b.trn_sales_request_header_id  ';
            $sql .= '       and s.is_deleted = false  ';
            $sql .= '     inner join mst_users as t  ';
            $sql .= '       on t.id = b.modifier  ';
            $sql .= '       and t.is_deleted = false  ';
            $sql .= '     left join mst_classes as u  ';
            $sql .= '       on u.id = b.work_type  ';
            $sql .= '     inner join mst_user_belongings as v  ';
            $sql .= '       on v.mst_facility_id = j.id  ';
            $sql .= '       and v.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '       and v.is_deleted = false  ';
            $sql .= '   where ';
            $sql .= '     a.is_deleted = false  ';
            $sql .= "     and a.is_stock_or_sale = '1'  ";
            $sql .= $where;
            $sql .= '   group by ';
            $sql .= '     b.id ';
            $sql .= '     , g.facility_name ';
            $sql .= '     , s.work_no ';
            $sql .= '     , s.id ';
            $sql .= '     , l.item_name ';
            $sql .= '     , l.item_code ';
            $sql .= '     , d.lot_no ';
            $sql .= '     , k.per_unit ';
            $sql .= '     , o.unit_name ';
            $sql .= '     , p.unit_name ';
            $sql .= '     , a.unit_price ';
            $sql .= '     , b.work_type ';
            $sql .= '     , t.user_name ';
            $sql .= '     , f.facility_name ';
            $sql .= '     , e.department_name ';
            $sql .= '     , d.hospital_sticker_no ';
            $sql .= '     , l.standard ';
            $sql .= '     , a.claim_date ';
            $sql .= '     , l.internal_code ';
            $sql .= '     , a.claim_price  ';
            $sql .= '     , a.id  ';
            $sql .= '     , n.dealer_name ';
            $sql .= ' order by ';
            $sql .= '      a.claim_date ';
            $sql .= '      , a.id ';

            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnClaim'));

            $sql .= ' limit ' . $this->request->data['limit'];
            $res = $this->TrnClaim->query($sql);
            $this->request->data = $data;

        }else{
            //初期検索条件
            $this->request->data['TrnConsume']['work_date_start']        = date("Y/m/d",mktime(0, 0, 0,date("m"), date("d")-7,date("Y")));
            $this->request->data['TrnConsume']['work_date_end']          = date("Y/m/d");
            $this->request->data['TrnConsume']['trn_consume_headers_id'] = true;
            $this->request->data['TrnOrder']['user_id']                  = $this->Session->read('Auth.MstUser.id');
        }

        
        $data = $this->Session->read('sales_print.request');
        
        $this->set('departmentList' , $departmentList);
        $this->set('sales_request_list',$res);
        $this->set('sales_sarch',$data);

        $this->render('sales_print');
    }


    /**
     * sales_print 売上依頼票印刷
     * @param
     * @return
     */
    function sales_print_detail() {
        
        $this->TrnSalesRequestHeader->begin();
        $now = date('Y/m/d H:i:s');
        
        $sql  = ' select ';
        $sql .= '     a.mst_department_id ';
        $sql .= '   , c.department_id_to ';
        $sql .= '   , a.work_date ';
        $sql .= '   , count(*)   as count  ';
        $sql .= ' from ';
        $sql .= '   trn_consumes as a  ';
        $sql .= '   left join trn_stickers as b ';
        $sql .= '     on b.id = a.trn_sticker_id ';
        $sql .= '   left join trn_orders as c ';
        $sql .= '     on c.id = b.trn_order_id ';
        $sql .= ' where ';
        $sql .= '   a.id in ( ' . join(',', $this->request->data['TrnConsume']['id']) . ' )  ';
        $sql .= ' group by ';
        $sql .= '   a.mst_department_id ';
        $sql .= '   , c.department_id_to ';
        $sql .= '   , a.work_date ';
        $ret = $this->TrnConsume->query($sql);
        
        //ヘッダ作成
        foreach($ret as &$r){
            $r[0]['work_no'] = $this->setWorkNo4Header( date('Ymd') , "28");
            
            // 売上依頼ヘッダ
            $TrnSalesRequestHeader = array(
                'TrnSalesRequestHeader'=> array(
                    'work_no'           => $r[0]['work_no'],
                    'work_date'         => $r[0]['work_date'],
                    'use_type'          => 0,
                    'detail_count'      => $r[0]['count'],
                    'mst_department_id' => $r[0]['mst_department_id'],      // センタ部署ID
                    'is_deleted'        => false,
                    'creater'           => $this->Session->read('Auth.MstUser.id'),
                    'created'           => $now,
                    'modified'          => $this->Session->read('Auth.MstUser.id'),
                    'modified'          => $now
                    )
                );

            $this->TrnSalesRequestHeader->create();
            // SQL実行
            if (!$this->TrnSalesRequestHeader->save($TrnSalesRequestHeader)) {
                $this->TrnOrder->rollback();
                $this->Session->setFlash('処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('sales_print');
            }
            $r[0]['id'] = $this->TrnSalesRequestHeader->getLastInsertID();
        }
        unset($r);
        
        //消費明細更新
        $sql  = ' select ';
        $sql .= '     a.id ';
        $sql .= '   , a.mst_department_id ';
        $sql .= '   , c.department_id_to ';
        $sql .= '   , a.work_date ';
        $sql .= ' from ';
        $sql .= '   trn_consumes as a  ';
        $sql .= '   left join trn_stickers as b ';
        $sql .= '     on b.id = a.trn_sticker_id ';
        $sql .= '   left join trn_orders as c ';
        $sql .= '     on c.id = b.trn_order_id ';
        $sql .= ' where ';
        $sql .= '   a.id in ( ' . join(',', $this->request->data['TrnConsume']['id']) . ' )  ';
        $consume = $this->TrnConsume->query($sql);
        
        foreach($ret as $r){
            foreach($consume as $c){
                if( $r[0]['mst_department_id'] == $c[0]['mst_department_id'] &&
                    $r[0]['department_id_to']  == $c[0]['department_id_to'] &&
                    $r[0]['work_date']         == $c[0]['work_date'] ){
                    
                    $this->TrnConsume->create();
                    $result = $this->TrnConsume->updateAll(
                        array(
                            'TrnConsume.trn_sales_request_header_id' => $r[0]['id'],
                            'TrnConsume.modifier'                    => $this->Session->read('Auth.MstUser.id'),
                            'TrnConsume.modified'                    => "'" . $now . "'"
                            ),
                        array(
                            'TrnConsume.id' => $c[0]['id'],
                            ),
                        -1
                        );
                    if(!$result){
                        $this->TrnConsume->rollback();
                        $this->Session->setFlash('処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('sales_print');
                    }
                }
            }
        }
        
        //コミット
        $this->TrnConsume->commit();
        
        //画面表示用データを取得
        $sql  = ' select ';
        $sql .= '     a.id                              as "TrnOrder__consume_id" ';
        $sql .= '   , b.id                              as "TrnOrder__sale_request_id" ';
        $sql .= '   , h.id                              as "TrnOrder__claim_id" ';
        $sql .= '   , b.work_no                         as "TrnOrder__work_no"';
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when e.per_unit = 1  ';
        $sql .= '       then e1.unit_name  ';
        $sql .= "       else e1.unit_name || '(' || e.per_unit || e2.unit_name || ')'  ";
        $sql .= '       end ';
        $sql .= '   )                                   as "TrnOrder__unit_name" ';
        $sql .= '   , f.internal_code                   as "TrnOrder__internal_code"';
        $sql .= '   , f.item_name                       as "TrnOrder__item_name" ';
        $sql .= '   , f.item_code                       as "TrnOrder__item_code"';
        $sql .= '   , f.standard                        as "TrnOrder__standard"';
        $sql .= '   , g.dealer_name                     as "TrnOrder__dealer_name"';
        $sql .= "   , to_char(h.claim_date,'YYYY/mm/dd')";
        $sql .= '                                       as "TrnOrder__claim_date"';
        $sql .= '   , h.claim_price                     as "TrnOrder__claim_price"';
        $sql .= '   , h.unit_price                      as "TrnOrder__unit_price"';
        $sql .= '   , c.validated_date                  as "TrnOrder__validated_date"';
        $sql .= '   , c.lot_no                          as "TrnOrder__lot_no"';
        $sql .= '   , c.hospital_sticker_no             as "TrnOrder__hospital_sticker_no"';
        $sql .= '   , j.facility_name                   as "TrnOrder__supplier_name"';
        $sql .= "   , l.facility_name || ' / ' || k.department_name ";
        $sql .= '                                       as "TrnOrder__facility_name"';
        $sql .= '   , m.name                            as "TrnOrder__work_type_name"';
        $sql .= '   , n.user_name                       as "TrnOrder__user_name"';
        $sql .= ' from ';
        $sql .= '   trn_consumes as a  ';
        $sql .= '   left join trn_sales_request_headers as b  ';
        $sql .= '     on b.id = a.trn_sales_request_header_id  ';
        $sql .= '   left join trn_stickers as c  ';
        $sql .= '     on c.id = a.trn_sticker_id  ';
        $sql .= '   left join trn_orders as d  ';
        $sql .= '     on d.id = c.trn_order_id  ';
        $sql .= '   left join mst_item_units as e  ';
        $sql .= '     on e.id = a.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as e1  ';
        $sql .= '     on e1.id = e.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as e2  ';
        $sql .= '     on e2.id = e.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as f  ';
        $sql .= '     on f.id = e.mst_facility_item_id  ';
        $sql .= '   left join mst_dealers as g  ';
        $sql .= '     on g.id = f.mst_dealer_id  ';
        $sql .= '   left join trn_claims as h  ';
        $sql .= '     on h.trn_consume_id = a.id  ';
        $sql .= "    and h.is_stock_or_sale = '1'";
        $sql .= '   left join mst_departments as i ';
        $sql .= '     on i.id = d.department_id_to ';
        $sql .= '   left join mst_facilities as j ';
        $sql .= '     on j.id = i.mst_facility_id ';
        $sql .= '   left join mst_departments as k ';
        $sql .= '     on k.id = a.mst_department_id';
        $sql .= '   left join mst_facilities as l ';
        $sql .= '     on l.id = k.mst_facility_id ';
        $sql .= '   left join mst_classes as m';
        $sql .= '     on m.id = a.work_type ';
        $sql .= '   left join mst_users as n';
        $sql .= '     on n.id = a.modifier ';
        $sql .= ' where ';
        $sql .= '   a.id in ( ' . join(',',$this->request->data['TrnConsume']['id']) . ' ) ';
        $result = $this->TrnConsume->query($sql);

        $this->set('result' , $result);
        $this->render('sales_print_detail_view');
    }

    public function report($reportType){
        switch($reportType){
          case 'order':
            $this->print_order();
            break;
          case 'summary':
            $this->print_summary();
            break;
          case 'replenish':
            $this->print_replenish();
            break;
          case 'sales':
            $this->print_sales();
            break;
          default:
            break;
        }
    }

    /**
     * sales_print 発注書  印刷処理
     * @param
     * @return
     */
    function print_order() {
        $now = date("Y/m/d H:i:s");
        $today = date("Y/m/d");
        $tax_type = Configure::read('PrintFacilityItems.taxes');
        
        $header_id = $this->request->data['TrnOrderHeader']['id'];
        $uid = $this->request->data['User']['user_id'];
        $order = $this->_getPrintData($header_id,1);

        $columns = array("発注番号",
                         "仕入先名",
                         "納品先名",
                         "施設名",
                         "発注日",
                         "納品先住所",
                         "施設住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "金額",
                         "単価",
                         "明細備考",
                         "包装単位",
                         "課税区分",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $tmpHeader = "";
        $i = 1;
        $tmp_page_no = '';

        foreach($order as $detail){
            $page_no = $detail['TrnOrder']['work_no'].'_'.$detail['MstOwner']['id'].'_'.$detail['MstFacilityItem']['buy_flg'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }
            //補充発注は納品書を印刷しない
            if($tmpHeader !== $detail['TrnOrderHeader']['id']){
                $tmpHeader = $detail['TrnOrderHeader']['id'];
                $this->setPrintedDate($detail['TrnOrderHeader']['id'],$now,$uid);
            }

            $price = $detail['TrnOrder']['stocking_price'] * $detail['TrnOrder']['quantity'];
            if($detail['FacilityTo']['round'] == Configure::read("RoundType.Round") ){
                $price = round($price);
            }else if( $detail['FacilityTo']['round'] == Configure::read("RoundType.Ceil") ){
                $price = ceil($price);
            }else if( $detail['FacilityTo']['round'] == Configure::read("RoundType.Floor") ){
                $price = floor($price);
            }else{
                $price = (int)$price;
            }

            $facility_name = $detail['FacilityTo']['facility_formal_name'];

            $facility_item_code = $detail['MstFacilityItem']['internal_code'];

            $data[] = array($detail['TrnOrder']['work_no']                                              //発注番号
                            ,$facility_name."様"                                                        //仕入先名
                            ,$detail['FacilityFrom']['name']                                            //納品先名
                            ,$detail['MstOwner']['facility_formal_name']                                //施主名
                            ,$detail['TrnOrder']['work_date']                                           //発注日
                            ,$detail['FacilityFrom']['address']                                         //納品先住所
                            ,$detail['MstOwner']['address']                                             //施設住所
                            ,$i                                                                         //行番号
                            ,$detail['MstFacilityItem']['item_name']                                    //商品名
                            ,$detail['MstFacilityItem']['standard']                                     //規格
                            ,$detail['MstFacilityItem']['item_code']                                    //製品番号
                            ,$detail['MstDealer']['dealer_name']                                        //販売元
                            ,$facility_item_code                                                        //商品ID
                            ,number_format($price)                                                      //金額
                            ,$detail['TrnOrder']['stocking_price']                                      //単価
                            ,$detail['TrnOrder']['recital']                                             //明細備考
                            ,$detail['TrnOrder']['quantity'] . $detail['MstFacilityItem']['unit_name']  //包装単位
                            ,$detail['MstFacilityItem']['tax_type_name']                                //課税区分
                            ,$detail['TrnOrder']['quantity']                                            //数量
                            ,$detail['TrnOrderHeader']['recital']                                       //ヘッダ備考
                            ,$detail['TrnOrderHeader']['printed_date']                                  //印刷年月日
                            ,$detail['FacilityTo']['round']                                             //丸め区分
                            ,$detail['TrnOrder']['is_deleted']                                          //削除フラグ
                            ,$detail['TrnOrder']['change']                                              //変更フラグ
                            ,$page_no                                                                   //改ページ番号
                            );
            $i++;
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'発注書');
        $this->xml_output($data,join("\t",$columns),$layout_name['10'],$fix_value);

        $errors = array();
        $errors = array_merge($errors
                              ,is_array($this->validateErrors($this->TrnOrderHeader)) ? $this->validateErrors($this->TrnOrderHeader) : array());
        if(count($errors) > 0){
            $this->TrnOrderHeader->rollback();
        }else{
            $this->TrnOrderHeader->commit();
        }

    }

    /**
     * sales_print 発注書サマリ  印刷処理
     * @param
     * @return
     */
    function print_summary() {
        $now = date("Y/m/d H:i:s");
        $today = date("Y/m/d");
        $tax_type = Configure::read('PrintFacilityItems.taxes');
        
        $header_id = $this->request->data['TrnOrderHeader']['id'];
        $uid = $this->request->data['User']['user_id'];
        $order = $this->_getSummaryData($header_id);

        $columns = array("発注番号",
                         "仕入先名",
                         "納品先名",
                         "施設名",
                         "発注日",
                         "納品先住所",
                         "施設住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "金額",
                         "単価",
                         "明細備考",
                         "包装単位",
                         "課税区分",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $tmpHeader = "";
        $i = 1;
        $tmp_page_no = '';

        foreach($order as $detail){

            $facility_name = $detail['FacilityTo']['facility_formal_name'];
            if( !empty($detail['FacilityTo2']['facility_formal_name']) && 
                $detail['FacilityTo']['facility_formal_name'] != $detail['FacilityTo2']['facility_formal_name']
            ){
                $facility_name .= "／" . $detail['FacilityTo2']['facility_formal_name'];
            }
            
            $page_no = $facility_name.'_'.$detail['MstOwner']['id'].'_'.$detail['MstFacilityItem']['buy_flg'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }

            $price = $detail['TrnOrder']['stocking_price'] * $detail['TrnOrder']['quantity'];
            if($detail['FacilityTo']['round'] == Configure::read("RoundType.Round") ){
                $price = round($price);
            }else if( $detail['FacilityTo']['round'] == Configure::read("RoundType.Ceil") ){
                $price = ceil($price);
            }else if( $detail['FacilityTo']['round'] == Configure::read("RoundType.Floor") ){
                $price = floor($price);
            }else{
                $price = (int)$price;
            }



            $facility_item_code = $detail['MstFacilityItem']['internal_code'];
            if(!empty($detail['MstFacilityItem']['aptage_code'])){
                $facility_item_code .= "／" . $detail['MstFacilityItem']['aptage_code'];
            }

            $data[] = array(''                                                                          //発注番号
                            ,$facility_name."様"                                                        //仕入先名
                            ,''                                                                         //納品先名
                            ,$detail['MstOwner']['facility_formal_name']                                //施主名
                            ,$detail['TrnOrder']['work_date']                                           //発注日
                            ,''                                                                         //納品先住所
                            ,$detail['MstOwner']['address']                                             //施設住所
                            ,$i                                                                         //行番号
                            ,$detail['MstFacilityItem']['item_name']                                    //商品名
                            ,$detail['MstFacilityItem']['standard']                                     //規格
                            ,$detail['MstFacilityItem']['item_code']                                    //製品番号
                            ,$detail['MstDealer']['dealer_name']                                        //販売元
                            ,$facility_item_code                                                        //商品ID
                            ,number_format($price)                                                      //金額
                            ,$detail['TrnOrder']['stocking_price']                                      //単価
                            ,''                                                                         //明細備考
                            ,$detail['TrnOrder']['quantity'] . $detail['MstFacilityItem']['unit_name']  //包装単位
                            ,$detail['MstFacilityItem']['tax_type_name']                                //課税区分
                            ,$detail['TrnOrder']['quantity']                                            //数量
                            ,''                                                                         //ヘッダ備考
                            ,''                                                                         //印刷年月日
                            ,$detail['FacilityTo']['round']                                             //丸め区分
                            ,$detail['TrnOrder']['is_deleted']                                          //削除フラグ
                            ,$detail['TrnOrder']['change']                                              //変更フラグ
                            ,$page_no                                                                   //改ページ番号
                            );
            $i++;
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'発注書サマリ');
        $this->xml_output($data,join("\t",$columns),$layout_name['10'],$fix_value);
    }

    
    /**
     * sales_print 補充依頼票  印刷処理
     * @param
     * @return
     */
    function print_replenish() {

        $now = date("Y/m/d H:i:s");
        $today = date("Y/m/d");
        
        $uid = $this->request->data['User']['user_id'];
        $header_id = $this->request->data['TrnOrderHeader']['id'];
        $order = $this->_getPrintData($header_id,2);
        
        $columns = array("補充依頼番号",
                         "仕入先名",
                         "補充先名",
                         "施主名",
                         "補充依頼日",
                         "納品先住所",
                         "施主住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "明細備考",
                         "包装単位",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $tmpHeader = "";
        $i = 1;
        $tmp_page_no = '';
        foreach($order as $detail){
            $page_no = $detail['TrnOrder']['work_no'].$detail['MstOwner']['id'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }

            if($tmpHeader !== $detail['TrnOrderHeader']['id']){
                $tmpHeader = $detail['TrnOrderHeader']['id'];
                $this->setPrintedDate($detail['TrnOrderHeader']['id'],$now,$uid);
            }

            $facility_name = $detail['FacilityTo']['facility_formal_name'];
            if($detail['FacilityTo']['facility_formal_name'] != $detail['FacilityTo2']['facility_formal_name']){
                $facility_name .= "／" . $detail['FacilityTo2']['facility_formal_name'];
            }
            $facility_item_code = $detail['MstFacilityItem']['internal_code'];
            if(!empty($detail['MstFacilityItem']['aptage_code'])){
                $facility_item_code .= "／" . $detail['MstFacilityItem']['aptage_code'];
            }

            $data[] = array( $detail['TrnOrder']['work_no']                                             //補充依頼番号
                            ,$facility_name                                                             //仕入先名
                            ,$detail['FacilityFrom']['name']                                            //補充先名
                            ,$detail['MstOwner']['facility_formal_name']                                //施主名
                            ,$detail['TrnOrder']['work_date']                                           //補充依頼日
                            ,$detail['FacilityFrom']['address']                                         //納品先住所
                            ,$detail['MstOwner']['address']                                             //施主住所
                            ,$i                                                                         //行番号
                            ,$detail['MstFacilityItem']['item_name']                                    //商品名
                            ,$detail['MstFacilityItem']['standard']                                     //規格
                            ,$detail['MstFacilityItem']['item_code']                                    //製品番号
                            ,$detail['MstDealer']['dealer_name']                                        //販売元
                            ,$facility_item_code                                                //商品ID
                            ,$detail['TrnOrder']['recital']                                             //明細備考
                            ,$detail['MstFacilityItem']['unit_name']                                    //包装単位
                            ,$detail['TrnOrder']['quantity']                                            //数量
                            ,$detail['TrnOrderHeader']['recital']                                       //ヘッダ備考
                            ,$detail['TrnOrderHeader']['printed_date']                                  //印刷年月日
                            ,$detail['FacilityTo']['round']                                             //丸め区分
                            ,$detail['TrnOrder']['is_deleted']                                          //削除フラグ
                            ,$detail['TrnOrder']['change']                                              //変更フラグ
                            ,$page_no                                                                   //改ページ番号
                            );
            $i++;
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'補充依頼票');
        $this->xml_output($data,join("\t",$columns),$layout_name['11'],$fix_value);
        
        $errors = array();
        $errors = array_merge($errors
                              ,is_array($this->validateErrors($this->TrnOrderHeader)) ? $this->validateErrors($this->TrnOrderHeader) : array());
        if(count($errors) > 0){
            $this->TrnOrderHeader->rollback();
        }else{
            $this->TrnOrderHeader->commit();
        }

    }


    /**
     * seal 部署シール  印刷処理
     * @param
     * @return
     */
    function seal($dummy = null) {

        if($dummy == 'cost'){
            $this->print_cost_seal();
            // 発注履歴からのコストシール印字
        }elseif ($dummy == 'cost_history'){
            $this->print_cost_seal('history');
        }else{
            App::import('Sanitize');
            //発注ヘッダから、部署シール番号を取得する。
            $sql  = ' select ';
            $sql .= '       c.hospital_sticker_no  ';
            $sql .= '   from ';
            $sql .= '     trn_order_headers as a  ';
            $sql .= '     left join trn_orders as b  ';
            $sql .= '       on b.trn_order_header_id = a.id  ';
            $sql .= '     left join trn_stickers as c  ';
            $sql .= '       on c.trn_order_id = b.id  ';
            $sql .= '     left join mst_item_units as d ';
            $sql .= '       on d.id = b.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as e ';
            $sql .= '       on e.id = d.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as f ';
            $sql .= '       on f.id = e.mst_dealer_id  ';
            $sql .= '     left join mst_departments as g ';
            $sql .= '       on g.id = b.department_id_from  ';
            $sql .= '     left join mst_departments as h ';
            $sql .= '       on h.id = b.department_id_to  ';
            $sql .= '   where ';
            $sql .= '     a.order_type in (  ' . Configure::read('OrderTypes.replenish') . ' , ' . Configure::read('OrderTypes.nostock') . ' , ' . Configure::read('OrderTypes.ExNostock') . ' ) ';
            $sql .= '     and b.is_deleted = false  ';
            $sql .= '     and a.id in ( ' . join(',',$this->request->data['TrnOrderHeader']['id']) . ') ';

            /* 絞込み条件追加 */

            //発注番号(完全一致)
            if((isset($this->request->data['TrnOrder']['work_no'])) && ($this->request->data['TrnOrder']['work_no'] != "")){
                $sql .= " AND a.work_no = '".$this->request->data['TrnOrder']['work_no']."'";
            }

            //発注日(期間)
            if((isset($this->request->data['TrnOrder']['work_date_start'])) && ($this->request->data['TrnOrder']['work_date_start'] != "")){
                $sql .= " AND a.work_date >=  '".$this->request->data['TrnOrder']['work_date_start']."'";
            }
            if((isset($this->request->data['TrnOrder']['work_date_end'])) && ($this->request->data['TrnOrder']['work_date_end'] != "")){
                $sql .= " AND a.work_date <=  '".$this->request->data['TrnOrder']['work_date_end']."'";
            }

            //取消
            if((isset($this->request->data['TrnOrder']['is_deleted']))){
                $sql .= ' AND a.is_deleted = FALSE';
                $sql .= ' AND b.is_deleted = FALSE';
            }
            //仕入先id(完全一致)
            if((isset($this->request->data['TrnOrder']['user_supplier'])) && ($this->request->data['TrnOrder']['user_supplier'] != "")){
                $sql .= ' AND h.mst_facility_id = '. $this->request->data['TrnOrder']['user_supplier'];
            }
            //要求元id(施設)(完全一致)
            if((isset($this->request->data['MstFacilitie']['facility'])) && ($this->request->data['MstFacilitie']['facility'] != "")){
                $sql .= ' AND g.mst_facility_id = '. $this->request->data["MstFacilitie"]["facility"];
            }
            //発注種別(完全一致)
            if((isset($this->request->data['TrnOrder']['order_type'])) && ($this->request->data['TrnOrder']['order_type'] != "")){
                $sql .= ' AND a.order_type = '. $this->request->data['TrnOrder']['order_type'];
            }
            //商品ID(部分一致)
            if((isset($this->request->data['MstFacilitieItem']['internal_code'])) && ($this->request->data['MstFacilitieItem']['internal_code'] != "")){
                $sql .= " AND e.internal_code = '".$this->request->data['MstFacilitieItem']['internal_code']."'";
            }
            //製品番号(部分一致)
            if((isset($this->request->data['MstFacilitieItem']['item_code'])) && ($this->request->data['MstFacilitieItem']['item_code'] != "")){
                $sql .= " AND e.item_code ILIKE '%".$this->request->data['MstFacilitieItem']['item_code']."%'";
            }
            //作業区分(完全一致)
            if((isset($this->request->data['TrnOrder']['work_type'])) && ($this->request->data['TrnOrder']['work_type'] != "")){
                $sql .= ' AND b.work_type = '. $this->request->data['TrnOrder']['work_type'];
            }
            //商品名(部分一致)
            if((isset($this->request->data['MstFacilitieItem']['item_name'])) && ($this->request->data['MstFacilitieItem']['item_name'] != "")){
                $sql .= " AND e.item_name ILIKE '%".$this->request->data['MstFacilitieItem']['item_name']."%'";
            }
            //販売元(部分一致)
            if((isset($this->request->data['MstDealers']['dealer_name'])) && ($this->request->data['MstDealers']['dealer_name'] != "")){
                $sql .= " AND f.dealer_name ILIKE '%".$this->request->data['MstDealers']['dealer_name']."%'";
            }
            //規格(部分一致)
            if((isset($this->request->data['MstFacilitieItem']['standard'])) && ($this->request->data['MstFacilitieItem']['standard'] != "")){
                $sql .= " AND e.standard ILIKE '%".$this->request->data['MstFacilitieItem']['standard']."%'";
            }

            $ret = $this->TrnOrder->query($sql);
            if(empty($ret)){
                $data = array();
            } else {
                $_hno = array();
                foreach ($ret as $r) {
                    $_hno[] = $r[0]['hospital_sticker_no'];
                }
                $order = 'MstDepartment.department_name asc, MstShelfNameTo.code asc';
                $data = $this->Stickers->getHospitalStickers($_hno, $order,2);
            }

            $this->set('sortkey' , '病院情報,センター棚番号,商品ID');
            $this->set('Sticker' , $data);
            $this->layout = 'xml/default';
            $this->render('/stickerissues/hospital_sticker');
        }
    }

    /**
     * コストシール印字
     **/
    function print_cost_seal($type = null){
        $ids = array();
        App::import('Sanitize');
        if($type == 'history'){
            //発注履歴画面からの印字
            $sql  = ' select ';
            $sql .= '       b.id  ';
            $sql .= '   from ';
            $sql .= '     trn_order_headers as a  ';
            $sql .= '     left join trn_orders as b  ';
            $sql .= '       on b.trn_order_header_id = a.id  ';
            $sql .= '     left join trn_stickers as c  ';
            $sql .= '       on c.trn_order_id = b.id  ';
            $sql .= '     left join mst_item_units as d ';
            $sql .= '       on d.id = b.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as e ';
            $sql .= '       on e.id = d.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as f ';
            $sql .= '       on f.id = e.mst_dealer_id  ';

            $sql .= '     left join mst_departments as g ';
            $sql .= '       on g.id = b.department_id_from  ';

            $sql .= '     left join mst_departments as h ';
            $sql .= '       on h.id = b.department_id_to  ';

            $sql .= '   where ';
            $sql .= '     a.order_type in ( ' . Configure::read('OrderTypes.replenish') . ',' . Configure::read('OrderTypes.nostock') . ' , ' . Configure::read('OrderTypes.ExNostock') . ')';
            $sql .= '     and b.is_deleted = false  ';
            $sql .= '     and a.id in ( ' . join(',',$this->request->data['TrnOrderHeader']['id']) . ') ';

            /* 絞込み条件追加 */
            //発注番号(完全一致)
            if((isset($this->request->data['TrnOrder']['work_no'])) && ($this->request->data['TrnOrder']['work_no'] != "")){
                $sql .= " AND a.work_no = '".$this->request->data['TrnOrder']['work_no']."'";
            }

            //発注日(期間)
            if((isset($this->request->data['TrnOrder']['work_date_start'])) && ($this->request->data['TrnOrder']['work_date_start'] != "")){
                $sql .= " AND a.work_date >=  '".$this->request->data['TrnOrder']['work_date_start']."'";
            }
            if((isset($this->request->data['TrnOrder']['work_date_end'])) && ($this->request->data['TrnOrder']['work_date_end'] != "")){
                $sql .= " AND a.work_date <=  '".$this->request->data['TrnOrder']['work_date_end']."'";
            }

            //取消
            if((isset($this->request->data['TrnOrder']['is_deleted']))){
                $sql .= ' AND a.is_deleted = FALSE';
                $sql .= ' AND b.is_deleted = FALSE';
            }
            //仕入先id(完全一致)
            if((isset($this->request->data['TrnOrder']['user_supplier'])) && ($this->request->data['TrnOrder']['user_supplier'] != "")){
                $sql .= ' AND h.mst_facility_id = '. $this->request->data['TrnOrder']['user_supplier'];
            }
            //要求元id(施設)(完全一致)
            if((isset($this->request->data['MstFacilitie']['facility'])) && ($this->request->data['MstFacilitie']['facility'] != "")){
                $sql .= ' AND g.mst_facility_id = '. $this->request->data['MstFacilitie']['facility'];
            }
            //発注種別(完全一致)
            if((isset($this->request->data['TrnOrder']['order_type'])) && ($this->request->data['TrnOrder']['order_type'] != "")){
                $sql .= ' AND a.order_type = '. $this->request->data['TrnOrder']['order_type'];
            }
            //商品ID(部分一致)
            if((isset($this->request->data['MstFacilitieItem']['internal_code'])) && ($this->request->data['MstFacilitieItem']['internal_code'] != "")){
                $sql .= " AND e.internal_code = '".$this->request->data['MstFacilitieItem']['internal_code']."'";
            }
            //製品番号(部分一致)
            if((isset($this->request->data['MstFacilitieItem']['item_code'])) && ($this->request->data['MstFacilitieItem']['item_code'] != "")){
                $sql .= " AND e.item_code ILIKE '%".$this->request->data['MstFacilitieItem']['item_code']."%'";
            }
            //作業区分(完全一致)
            if((isset($this->request->data['TrnOrder']['work_type'])) && ($this->request->data['TrnOrder']['work_type'] != "")){
                $sql .= ' AND b.work_type = '. $this->request->data['TrnOrder']['work_type'];
            }
            //商品名(部分一致)
            if((isset($this->request->data['MstFacilitieItem']['item_name'])) && ($this->request->data['MstFacilitieItem']['item_name'] != "")){
                $sql .= " AND e.item_name ILIKE '%".$this->request->data['MstFacilitieItem']['item_name']."%'";
            }
            //販売元(部分一致)
            if((isset($this->request->data['MstDealers']['dealer_name'])) && ($this->request->data['MstDealers']['dealer_name'] != "")){
                $sql .= " AND f.dealer_name ILIKE '%".$this->request->data['MstDealers']['dealer_name']."%'";
            }
            //規格(部分一致)
            if((isset($this->request->data['MstFacilitieItem']['standard'])) && ($this->request->data['MstFacilitieItem']['standard'] != "")){
                $sql .= " AND e.standard ILIKE '%".$this->request->data['MstFacilitieItem']['standard']."%'";
            }
            $res = $this->TrnSticker->query($sql);
            foreach($res as $r){
                $ids[] = $r[0]['id'];
            }
        }else{
            //確定画面からの印字
            $ids = $this->request->data['TrnOrder']['id'];
        }
        // コストシールデータ取得
        $records = array();
        if(!empty($ids)){
            $records = $this->Stickers->getCostStickers($ids,4);
        }
        // 空なら初期化
        if(!$records){ $records = array();}
        $this->layout = 'xml/default';
        $this->set('records', $records);
        $this->render('/stickerissues/cost_sticker');
    }

    /**
     * sales_print 売上依頼票  印刷処理
     * @param
     * @return
     */
    function print_sales() {
        if(isset($this->request->data['TrnOrder']['selected_facility_id']) && $this->request->data['TrnOrder']['selected_facility_id']!==""){
            $facility_id = $this->request->data['TrnOrder']['selected_facility_id'];
        }else{
            $facility_id = $this->Session->read('Auth.facility_id_selected');
        }

        $columns = array("売上依頼番号",
                         "仕入先名",
                         "売上番号",
                         "売上部署名",
                         "施主名",
                         "売上日",
                         "施主住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "金額",
                         "単価",
                         "明細備考",
                         "包装単位",
                         "課税区分",
                         "部署シール",
                         "ロット番号",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );

        if($this->Session->check('TrnConsume.id')){
            $id = $this->Session->read('TrnConsume.id');
        }else{
            $id = $this->request->data['TrnConsume']['id'];
        }
        if($this->Session->check('TrnConsume.trn_sales_request_header_id')){
            $request_headers_id = $this->Session->read('TrnConsume.trn_sales_request_header_id');
        }else{
            $request_headers_id = $this->request->data['TrnSalesRequestHeader']['id'];
        }


        $cond = array('TrnClaim'   => array('TrnClaim.is_deleted' => FALSE)
                      ,'TrnConsume' => array('TrnConsume.is_deleted' => FALSE
                                             ,'TrnConsume.id in ('. $id[0]. ')'));
        $order = array('TrnSalesRequestHeader.id','MstOwner.id');
        $cond = $this->_getSalesRequestCond($cond);
        $salesRequest = $this->_getSalesRequest($cond,$order);

        $i = 1;
        $tmp = '';
        $tax_type = Configure::read('PrintFacilityItems.taxes');
        $today = date("Y/m/d");
        $data = array();
        $errorFlg = false;
        $seq = 1; //行番号
        $seq_check = ""; //行番号変更チェック用
        $seq_check2 = "";

        $this->TrnSalesRequestHeader->begin();
        foreach($salesRequest as $sales){

            if(!isset($sales['MstOwner']['id'])){
                $sales['MstOwner'] = array('id' => ""
                                           ,'zip' => ""
                                           ,'address' => ""
                                           ,'tel' => ""
                                           ,'fax' => ""
                                           ,'facility_formal_name' => ""
                                           );
            }
            $page_no = $sales['TrnSalesRequestHeader']['work_no'].$sales['MstOwner']['id'];

            //改ページ時に行番号を初期化
            if($seq_check != $sales['TrnSalesRequestHeader']['work_no']){
                $seq = 1;
            } else {
                if($seq_check2 != $sales['MstOwner']['id']){
                    $seq = 1;
                } else {
                    $seq++;
                }
            }

            if($tmp !== $sales['TrnSalesRequestHeader']['work_no']){
                $tmp = $sales['TrnSalesRequestHeader']['work_no'];
                $i = 1;

                $res2 = $this->TrnSalesRequestHeader->updateAll(
                    array('recital_date' => "'".$today."'"
                          , 'modified'   => "'" . date('Y-m-d H:i:s') . "'"
                          , 'modifier'   => $sales['MstUser']['id']
                          ),
                    array(
                        'TrnSalesRequestHeader.id' => $sales['TrnSalesRequestHeader']['id']
                        )
                    ,-1
                    );
            }

            if($sales['TrnClaim']['is_deleted']){
                $delete = 1;
            }else{
                $delete = 0;
            }
            if($sales['TrnConsume']['quantity'] == $sales['TrnConsume']['before_quantity']){
                $change = 0;
            }else{
                $change = 1;
            }

            if(!isset($sales['TrnSalesRequestHeader']['recital_date'])){
                $sales['TrnSalesRequestHeader']['recital_date'] = null;
            }

            $taxType = "";
            if(isset($sales['MstFacilityItem']['tax_type']) && $sales['MstFacilityItem']['tax_type'] != null){
                $taxType = $tax_type[$sales['MstFacilityItem']['tax_type']];
            }

            $ownerAddress = "〒".$sales['MstOwner']['zip']."　".$sales['MstOwner']['address']."\nTel:".$sales['MstOwner']['tel']."／Fax:".$sales['MstOwner']['fax'];

            $data[] = array($sales['TrnSalesRequestHeader']['work_no']                            //売上依頼番号
                            ,$sales['MstFacilityTo']['facility_formal_name']                       //仕入先名
                            ,$sales['TrnSalesRequestHeader']['work_no']                            //売上番号
                            ,$sales['MstFacilitySale']['facility_formal_name']
                            ."　".$sales['MstDepartmentSale']['department_formal_name']            //売上部署名
                            ,$sales['MstOwner']['facility_formal_name']                            //施主名
                            ,$sales['TrnClaim']['claim_date']                                      //売上日
                            ,$ownerAddress                                                         //施主住所
                            ,$seq
                            ,$sales['MstFacilityItem']['item_name']                                //商品名
                            ,$sales['MstFacilityItem']['standard']                                 //規格
                            ,$sales['MstFacilityItem']['item_code']                                //製品番号
                            ,$sales['MstDealer']['dealer_name']                                    //販売元
                            ,$sales['MstFacilityItem']['internal_code']                            //商品ID
                            ,number_format($sales['TrnClaim']['claim_price'])                       //金額
                            ,$sales['TrnClaim']['unit_price']                                      //単価
                            ,$this->request->data['TrnSalesRequest']['recital'][$sales['TrnClaim']['id']]   //明細備考
                            ,$sales['MstItemUnit']['unit_name']                                     //包装単位
                            ,$taxType                                                              //課税区分
                            ,$sales['TrnSticker']['hospital_sticker_no']                           //部署シール
                            ,$sales['TrnSticker']['lot_no']                                        //ロット番号
                            ,$sales['TrnClaim']['count']                                           //数量
                            ,$sales['TrnSalesRequestHeader']['recital']                            //ヘッダ備考
                            ,$sales['TrnSalesRequestHeader']['recital_date']                       //印刷年月日
                            ,$sales['TrnClaim']['round']                                           //丸め区分
                            ,$delete                                                               //削除フラグ
                            ,$change                                                               //変更フラグ
                            ,$page_no                                                              //改ページ番号
                            );
            $i++;
            $seq_check = $sales['TrnSalesRequestHeader']['work_no'];
            $seq_check2 = $sales['MstOwner']['id'];

        }
        $errors = array();
        $errors = array_merge($errors
                              ,is_array($this->validateErrors($this->TrnSalesRequestHeader)) ? $this->validateErrors($this->TrnSalesRequestHeader) : array());

        if(count($errors) > 0 || $errorFlg){
            $this->TrnSalesRequestHeader->rollback();
            $this->Session->setFlash("データ更新に失敗しました", 'growl', array('type'=>'error'));
        }else{
            $this->TrnSalesRequestHeader->commit();
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'売上依頼票');
        $this->xml_output($data,join("\t",$columns),$layout_name['12'],$fix_value);

    }


    //発注帳票データ取得
    function _getPrintData ($header_id , $type) {
        $sql  = ' select ';
        $sql .= '     a.id                     as "TrnOrder__id"';
        $sql .= '   , a.work_no                as "TrnOrder__work_no"';
        $sql .= '   , a.work_seq               as "TrnOrder__work_seq"';
        $sql .= "   , to_char(a.work_date,'YYYY年MM月dd日')";
        $sql .= '                              as "TrnOrder__work_date"';
        $sql .= '   , a.work_type              as "TrnOrder__work_type"';
        $sql .= '   , a.recital                as "TrnOrder__recital"';
        $sql .= '   , a.order_type             as "TrnOrder__order_type"';
        $sql .= '   , a.order_status           as "TrnOrder__order_status"';
        $sql .= '   , a.quantity               as "TrnOrder__quantity"';
        $sql .= '   , ( case ';
        $sql .= '       when a.before_quantity = a.quantity then 0 ';
        $sql .= '       else 1 ';
        $sql .= '       end )                  as "TrnOrder__change"';
        $sql .= '   , a.remain_count           as "TrnOrder__remain_count"';
        $sql .= '   , a.stocking_price         as "TrnOrder__stocking_price"';
        $sql .= '   , ( case ';
        $sql .= '       when a.is_deleted = true then 1 ';
        $sql .= '       else 0 ';
        $sql .= '       end )                  as "TrnOrder__is_deleted"';
        $sql .= '   , a1.id                    as "TrnOrderHeader__id"';
        $sql .= '   , a1.recital               as "TrnOrderHeader__recital"';
        $sql .= '   , a1.printed_date          as "TrnOrderHeader__printed_date"';
        $sql .= '   , ( case when b.per_unit = 1 then b1.unit_name ';
        $sql .= "       else b1.unit_name || '(' ||  b.per_unit || b2.unit_name || ')' ";
        $sql .= '       end )                  as "MstFacilityItem__unit_name"';
        $sql .= '   , c.internal_code          as "MstFacilityItem__internal_code"';
        $sql .= '   , c.item_name              as "MstFacilityItem__item_name"';
        $sql .= '   , c.standard               as "MstFacilityItem__standard"';
        $sql .= '   , c.item_code              as "MstFacilityItem__item_code"';
        $sql .= '   , ( case when c.buy_flg = true then 1 else 0 end ) ';
        $sql .= '                              as "MstFacilityItem__buy_flg"';
        $sql .= '   , c.spare_key2             as "MstFacilityItem__aptage_code"';
        $sql .= '   , ( case ';
        foreach(Configure::read('PrintFacilityItems.taxes') as $k=>$v){
            $sql .= '       when c.tax_type =' . $k . " then '" . $v . "'";
        }
        $sql .= "       else '' ";
        $sql .= '      end )                   as "MstFacilityItem__tax_type_name"';
        $sql .= '   , d.dealer_name            as "MstDealer__dealer_name"';
        $sql .= '   , e.id                     as "MstOwner__id"';
        $sql .= '   , e.facility_formal_name   as "MstOwner__facility_formal_name"';
        $sql .= "   , '〒' || coalesce(e.zip,'') || ' ' || coalesce(e.address,'') || '\nTel:' || coalesce(e.tel,'') || '／Fax:' || coalesce(e.fax,'')";
        $sql .= '                              as "MstOwner__address"';
        $sql .= '   , f.id                     as "DepartmentFrom__id"';
        $sql .= '   , f.department_formal_name as "DepartmentFrom__department_formal_name"';
        $sql .= '   , ( case ';
        $sql .= '       when g.facility_type = ' . Configure::read('FacilityType.hospital') ;
        $sql .= "       then g.facility_formal_name || ' ' || f.department_formal_name ";
        $sql .= '       else g.facility_formal_name ';
        $sql .= '       end )                 as "FacilityFrom__name"';
        
        $sql .= '   , g.id                     as "FacilityFrom__id"';
        $sql .= '   , g.facility_formal_name   as "FacilityFrom__facility_formal_name"';
        $sql .= "   , '〒' || coalesce(g.zip,'') || ' ' || coalesce(g.address,'') || '\nTel:' || coalesce(g.tel,'') || '／Fax:' || coalesce(g.fax,'')";
        $sql .= '                              as "FacilityFrom__address"';
        $sql .= '   , h.id                     as "DepartmentTo__id"';
        $sql .= '   , h.department_formal_name as "DepartmentTo__department_formal_name"';
        $sql .= '   , j.id                     as "DepartmentTo2__id"';
        $sql .= '   , j.department_formal_name as "DepartmentTo2__department_formal_name"';
        $sql .= '   , i.id                     as "FacilityTo__id"';
        $sql .= '   , i.facility_formal_name   as "FacilityTo__facility_formal_name"';
        $sql .= '   , i.facility_type          as "FacilityTo__facility_type"';
        $sql .= '   , i.zip                    as "FacilityTo__zip"';
        $sql .= '   , i.address                as "FacilityTo__address"';
        $sql .= '   , i.tel                    as "FacilityTo__tel"';
        $sql .= '   , i.fax                    as "FacilityTo__fax"';
        $sql .= '   , i.gross                  as "FacilityTo__gross"';
        $sql .= '   , i.round                  as "FacilityTo__round"';
        $sql .= '   , k.id                     as "FacilityTo2__id"';
        $sql .= '   , k.facility_formal_name   as "FacilityTo2__facility_formal_name"';
        $sql .= ' from ';
        $sql .= '   trn_orders as a  ';
        $sql .= '   left join trn_order_headers as a1  ';
        $sql .= '     on a1.id = a.trn_order_header_id  ';
        $sql .= '   left join mst_item_units as b  ';
        $sql .= '     on b.id = a.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as b1  ';
        $sql .= '     on b1.id = b.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as b2  ';
        $sql .= '     on b2.id = b.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as c  ';
        $sql .= '     on c.id = b.mst_facility_item_id  ';
        $sql .= '   left join mst_dealers as d  ';
        $sql .= '     on d.id = c.mst_dealer_id  ';
        $sql .= '   left join mst_facilities as e  ';
        $sql .= '     on e.id = c.mst_owner_id  ';
        $sql .= '     and e.facility_type = ' . Configure::read('FacilityType.donor');
        $sql .= '   left join mst_departments as f  ';
        $sql .= '     on f.id = a.department_id_from  ';
        $sql .= '   left join mst_facilities as g  ';
        $sql .= '     on g.id = f.mst_facility_id  ';
        $sql .= '   left join mst_departments as h  ';
        $sql .= '     on h.id = a.department_id_to  ';
        $sql .= '   left join mst_facilities as i  ';
        $sql .= '     on i.id = h.mst_facility_id  ';
        $sql .= '   left join mst_departments as j  ';
        $sql .= '     on j.id = a.department_id_to2  ';
        $sql .= '   left join mst_facilities as k  ';
        $sql .= '     on k.id = j.mst_facility_id  ';

        $sql .= ' where ';
        if(is_array($header_id)){
            $sql .= '   a.trn_order_header_id in (' . join(',',$header_id) . ')';
        }else{
            $sql .= '   a.trn_order_header_id in ( ' . $header_id . ')';
        }
        if($type == 1 ){
            $sql .= ' and a.order_type <> ' . Configure::read('OrderTypes.replenish');
        }else{
            $sql .= ' and a.order_type = ' . Configure::read('OrderTypes.replenish');
        }
        $sql .= ' and a.is_deleted = false ';
        $sql .= ' order by ';
        $sql .= '   a.trn_order_header_id ';
        $sql .= '   , e.id ';
        $sql .= '   , d.dealer_name ';
        $sql .= '   , c.item_name ';
        $sql .= '   , c.item_code ';

        $order = $this->TrnOrder->query($sql);

        return $order;
    }

    //発注帳票データ取得
    function _getSummaryData ($header_id) {
        $sql  = ' select ';
        $sql .= "     to_char(a.work_date,'YYYY年MM月dd日')";
        $sql .= '                              as "TrnOrder__work_date"';
        $sql .= '   , sum(a.quantity)          as "TrnOrder__quantity"';
        $sql .= '   , ( case ';
        $sql .= '       when sum(a.before_quantity) = sum(a.quantity) then 0 ';
        $sql .= '       else 1 ';
        $sql .= '       end )                  as "TrnOrder__change"';
        $sql .= '   , sum(a.remain_count)      as "TrnOrder__remain_count"';
        $sql .= '   , a.stocking_price         as "TrnOrder__stocking_price"';
        $sql .= '   , ( case ';
        $sql .= '       when a.is_deleted = true then 1 ';
        $sql .= '       else 0 ';
        $sql .= '       end )                  as "TrnOrder__is_deleted"';
        $sql .= '   , ( case when b.per_unit = 1 then b1.unit_name ';
        $sql .= "       else b1.unit_name || '(' ||  b.per_unit || b2.unit_name || ')' ";
        $sql .= '       end )                  as "MstFacilityItem__unit_name"';
        $sql .= '   , c.internal_code          as "MstFacilityItem__internal_code"';
        $sql .= '   , c.item_name              as "MstFacilityItem__item_name"';
        $sql .= '   , c.standard               as "MstFacilityItem__standard"';
        $sql .= '   , c.item_code              as "MstFacilityItem__item_code"';
        $sql .= '   , ( case when c.buy_flg = true then 1 else 0 end ) ';
        $sql .= '                              as "MstFacilityItem__buy_flg"';
        $sql .= '   , c.spare_key2             as "MstFacilityItem__aptage_code"';
        $sql .= '   , ( case ';
        foreach(Configure::read('PrintFacilityItems.taxes') as $k=>$v){
            $sql .= '       when c.tax_type =' . $k . " then '" . $v . "'";
        }
        $sql .= "       else '' ";
        $sql .= '      end )                   as "MstFacilityItem__tax_type_name"';
        $sql .= '   , d.dealer_name            as "MstDealer__dealer_name"';
        $sql .= '   , e.id                     as "MstOwner__id"';
        $sql .= '   , e.facility_formal_name   as "MstOwner__facility_formal_name"';
        $sql .= "   , '〒' || coalesce(e.zip,'') || ' ' || coalesce(e.address,'') || '\nTel:' || coalesce(e.tel,'') || '／Fax:' || coalesce(e.fax,'')";
        $sql .= '                              as "MstOwner__address"';
        $sql .= '   , h.id                     as "DepartmentTo__id"';
        $sql .= '   , h.department_formal_name as "DepartmentTo__department_formal_name"';
        $sql .= '   , j.id                     as "DepartmentTo2__id"';
        $sql .= '   , j.department_formal_name as "DepartmentTo2__department_formal_name"';
        $sql .= '   , i.id                     as "FacilityTo__id"';
        $sql .= '   , i.facility_formal_name   as "FacilityTo__facility_formal_name"';
        $sql .= '   , i.facility_type          as "FacilityTo__facility_type"';
        $sql .= '   , i.zip                    as "FacilityTo__zip"';
        $sql .= '   , i.address                as "FacilityTo__address"';
        $sql .= '   , i.tel                    as "FacilityTo__tel"';
        $sql .= '   , i.fax                    as "FacilityTo__fax"';
        $sql .= '   , i.gross                  as "FacilityTo__gross"';
        $sql .= '   , i.round                  as "FacilityTo__round"';
        $sql .= '   , k.id                     as "FacilityTo2__id"';
        $sql .= '   , k.facility_formal_name   as "FacilityTo2__facility_formal_name"';
        $sql .= ' from ';
        $sql .= '   trn_orders as a  ';
        $sql .= '   left join trn_order_headers as a1  ';
        $sql .= '     on a1.id = a.trn_order_header_id  ';
        $sql .= '   left join mst_item_units as b  ';
        $sql .= '     on b.id = a.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as b1  ';
        $sql .= '     on b1.id = b.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as b2  ';
        $sql .= '     on b2.id = b.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as c  ';
        $sql .= '     on c.id = b.mst_facility_item_id  ';
        $sql .= '   left join mst_dealers as d  ';
        $sql .= '     on d.id = c.mst_dealer_id  ';
        $sql .= '   left join mst_facilities as e  ';
        $sql .= '     on e.id = c.mst_owner_id  ';
        $sql .= '     and e.facility_type = ' . Configure::read('FacilityType.donor');
        $sql .= '   left join mst_departments as f  ';
        $sql .= '     on f.id = a.department_id_from  ';
        $sql .= '   left join mst_facilities as g  ';
        $sql .= '     on g.id = f.mst_facility_id  ';
        $sql .= '   left join mst_departments as h  ';
        $sql .= '     on h.id = a.department_id_to  ';
        $sql .= '   left join mst_facilities as i  ';
        $sql .= '     on i.id = h.mst_facility_id  ';
        $sql .= '   left join mst_departments as j  ';
        $sql .= '     on j.id = a.department_id_to2  ';
        $sql .= '   left join mst_facilities as k  ';
        $sql .= '     on k.id = j.mst_facility_id  ';

        $sql .= ' where ';
        if(is_array($header_id)){
            $sql .= '   a.trn_order_header_id in (' . join(',',$header_id) . ')';
        }else{
            $sql .= '   a.trn_order_header_id = ' . $header_id;
        }
        $sql .= ' and a.order_type <> ' . Configure::read('OrderTypes.replenish');
        
        $sql .= ' group by  ';
        $sql .= '     a.work_date';
        $sql .= '   , a.stocking_price ';
        $sql .= '   , a.is_deleted ';
        $sql .= '   , b1.unit_name ';
        $sql .= '   , b.per_unit ';
        $sql .= '   , b2.unit_name ';
        $sql .= '   , c.internal_code ';
        $sql .= '   , c.item_name ';
        $sql .= '   , c.standard ';
        $sql .= '   , c.item_code ';
        $sql .= '   , c.buy_flg ';
        $sql .= '   , c.spare_key2 ';
        $sql .= '   , c.tax_type ';
        $sql .= '   , d.dealer_name ';
        $sql .= '   , e.id ';
        $sql .= '   , e.facility_formal_name ';
        $sql .= '   , e.zip ';
        $sql .= '   , e.address '; 
        $sql .= '   , e.tel ';
        $sql .= '   , e.fax ';
        $sql .= '   , h.id ';
        $sql .= '   , h.department_formal_name ';
        $sql .= '   , j.id ';
        $sql .= '   , j.department_formal_name ';
        $sql .= '   , i.id ';
        $sql .= '   , i.facility_formal_name ';
        $sql .= '   , i.facility_type ';
        $sql .= '   , i.zip ';
        $sql .= '   , i.address ';
        $sql .= '   , i.tel ';
        $sql .= '   , i.fax ';
        $sql .= '   , i.gross ';
        $sql .= '   , i.round ';
        $sql .= '   , k.id ';
        $sql .= '   , k.facility_formal_name ';

        $sql .= ' order by ';
        $sql .= '   e.id ';
        $sql .= '   , c.buy_flg ';
        $sql .= '   , i.facility_formal_name ';
        $sql .= '   , k.facility_formal_name ';
        $sql .= '   , d.dealer_name ';
        $sql .= '   , c.item_name ';
        $sql .= '   , c.item_code ';

        $order = $this->TrnOrder->query($sql);

        return $order;
    }

    
    //履歴画面用明細一覧表示データ
    function _getHistoryListData ($where) {

        $sql  = 'SELECT A.id';
        $sql .= '      ,A.work_no                           AS 発注番号 ';
        $sql .= '      ,A.order_type                        AS 発注種別 ';
        $sql .= "      ,TO_CHAR(A.work_date,'YYYY/MM/DD')   AS 発注日 ";
        $sql .= '      ,A.order_status                      AS 発注状態 ';
        $sql .= '      ,A.modified                          AS 更新日時 ';
        $sql .= '      ,D.facility_name                     AS 施設 ';
        $sql .= '      ,C.department_name                   AS 部署 ';
        $sql .= '      ,F.facility_name                     AS 仕入先 ';
        $sql .= '      ,F.id                                AS 仕入先ID ';
        $sql .= '      ,G.user_name                         AS 更新者 ';
        $sql .= '      ,A.quantity                          AS 発注数 ';
        $sql .= '      ,A.remain_count                      AS 残数 ';
        $sql .= "      ,( case " ;
        $sql .= "             when A.remain_count = 0 then A.recital ";
        $sql .= "             when A.quantity != Q.count then '一部出荷済み（' || Q.count || '）' ";
        $sql .= "             when Q.trn_edi_receiving_id is not null then '出荷済み' ";
        $sql .= "             when P.id is not null then '受注済み' ";
        $sql .= "             else A.recital end )          AS 備考 ";
        $sql .= '      ,A.stocking_price                    AS 仕入価格 ';
        $sql .= '      ,L.name                              AS 作業区分名 ';
        $sql .= '      ,J.internal_code                     AS 商品id ';
        $sql .= '      ,J.item_name                         AS 商品名 ';
        $sql .= '      ,J.standard                          AS 規格 ';
        $sql .= '      ,J.item_code                         AS 製品番号 ';
        $sql .= '      ,I.per_unit                          AS 入数 ';
        $sql .= '      ,M.unit_name                         AS 単位名 ';
        $sql .= '      ,N.unit_name                         AS 入数単位名 ';
        $sql .= "      ,(case when I.per_unit = 1 then M.unit_name else M.unit_name || '(' || I.per_unit || N.unit_name || ')' end )AS 包装単位";
        $sql .= '      ,K.dealer_name                       AS 販売元名 ';
        $sql .= '      ,A.is_deleted                        AS 削除フラグ ';
        $sql .= '      ,( case when Q.trn_edi_receiving_id is not null then false ';
        $sql .= '              when A.remain_count < 1 then false ';
        $sql .= '              when A.is_deleted = true then false ';
        $sql .= '              else true ';
        $sql .= '       end )                               as チェック';
        $sql .= '  FROM trn_orders A ';
        $sql .= '          LEFT JOIN trn_order_headers B ';
        $sql .= '                 ON A.trn_order_header_id = B.id ';
        $sql .= '          LEFT JOIN mst_departments C ';
        $sql .= '                 ON A.department_id_from = C.id ';
        $sql .= '          LEFT JOIN mst_facilities D ';
        $sql .= '                 ON C.mst_facility_id = D.id ';
        $sql .= '                AND D.facility_type in ('.Configure::read('FacilityType.center').','.Configure::read('FacilityType.hospital').')';
        $sql .= '          LEFT JOIN mst_departments E ';
        $sql .= '                 ON A.department_id_to = E.id ';
        $sql .= '          LEFT JOIN mst_facilities F ';
        $sql .= '                 ON E.mst_facility_id = F.id ';
        $sql .= '                AND F.facility_type = ' . Configure::read('FacilityType.supplier');
        $sql .= '          LEFT JOIN mst_users G ';
        $sql .= '                 ON A.modifier = G.id ';
        $sql .= '          LEFT JOIN mst_item_units I ';
        $sql .= '                 ON A.mst_item_unit_id = I.id ';
        $sql .= '          LEFT JOIN mst_facility_items J ';
        $sql .= '                 ON I.mst_facility_item_id = J.id ';
        $sql .= '          LEFT JOIN mst_dealers K ';
        $sql .= '                 ON J.mst_dealer_id = K.id ';
        $sql .= '          LEFT JOIN mst_classes L ';
        $sql .= '                 ON A.work_type = L.id ';
        $sql .= '          LEFT JOIN mst_unit_names M ';
        $sql .= '                 ON I.mst_unit_name_id = M.id ';
        $sql .= '          LEFT JOIN mst_unit_names N ';
        $sql .= '                 ON I.per_unit_name_id = N.id ';
        $sql .= '         INNER JOIN mst_facilities O ';
        $sql .= '                 ON I.mst_facility_id = O.id ';
        $sql .= '                AND O.facility_type = '.Configure::read('FacilityType.center');
        $sql .= '                AND O.id = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= '          LEFT JOIN trn_edi_receivings as P ';
        $sql .= '                 ON P.trn_order_id = A.id';
        $sql .= '                AND P.is_deleted = false ';
        $sql .= '          LEFT JOIN ( select xx.trn_edi_receiving_id ,sum(xx.quantity) as count from trn_edi_shippings as xx where xx.is_deleted = false group by xx.trn_edi_receiving_id ) as Q ';
        $sql .= '                 ON Q.trn_edi_receiving_id = P.id';
        $sql .= $where;
        $sql .= ' ORDER BY A.trn_order_header_id, J.mst_owner_id , K.dealer_name , J.item_name , J.item_code ';

        $order_List_Data = $this->TrnOrder->query($sql);

        return $order_List_Data;
    }

    /**
     * 発注履歴明細where句作成
     */
    private function _createSearchWhere($mode=1){
        //ユーザ入力値による検索条件の作成--------------------------------------------
        $where  = ' WHERE 1 = 1';

        if($mode==1){
            $deleat_ids = "";

            $target = explode(",",$this->request->data['target']);
            $to_id="";
            $from_id="";
            $i=0;
            foreach($target as $condition){
                if($condition == ""){
                    continue;
                }
                $data = explode("_",$condition);
                $to_id[$i]      = $data[0];
                $from_id[$i]    = $data[1];
                $trade_type[$i] = $data[2];
                $i++;
            }
            $i=0;
            $not_decision = "";
            foreach($this->request->data['TrnOrderHeader']['id'] as $id){
                if(!isset($id)||$id == ""){

                    if($not_decision ==  ""){
                        $not_decision .= '(A.trn_order_header_id IS NULL AND A.order_type = '.$trade_type[$i] 
                                        .' AND A.department_id_from = '.$from_id[$i] 
                                        .' AND A.department_id_to = '. $to_id[$i].')';
                    }
                    $not_decision .= ' OR (A.trn_order_header_id IS NULL AND A.order_type = '.$trade_type[$i]
                                        .' AND A.department_id_from = '.$from_id[$i]
                                        .' AND A.department_id_to = '. $to_id[$i].')';
                }else{
                    $deleat_ids .= $id.',';
                }
                $i++;
            }

            $deleat_ids = rtrim($deleat_ids,',');
            if($deleat_ids == ""){
                if($not_decision !== ""){
                    $where .= ' AND ('.$not_decision.')';
                }
            }else{
                $where .= ' AND (A.trn_order_header_id in ('.$deleat_ids.')';
                if($not_decision !== ""){
                    $where .= ' OR '.$not_decision.')';
                }else{
                    $where .= ')';
                }
            }

            $this->request->data = $this->outputFilter($this->request->data);
            //発注番号(完全一致)
            if((isset($this->request->data['search']['work_no'])) && ($this->request->data['search']['work_no'] != "")){
                $where .= " AND A.work_no = '".$this->request->data['search']['work_no']."'";
            }

            //未発注
            if(isset($this->request->data['search']['yet'])){
                $where .= ' AND (( 1 = 1';
            }
            //発注日(期間)
            if(isset($this->request->data['search']['work_date_start']) && $this->request->data['search']['work_date_start'] != ""){
                $where .= " AND B.work_date >=  '".$this->request->data['search']['work_date_start']."'";
            }
            if((isset($this->request->data['search']['work_date_end'])) && ($this->request->data['search']['work_date_end'] != "")){
                //---------指定日の１日後より前で検索(時間が入るため)-----------------
                $end_day = explode('/',$this->request->data['search']['work_date_end']);
                $end_day[2] = $end_day[2] + 1;
                $end_date = date("Y/m/d",mktime(0, 0, 0, $end_day[1], ($end_day[2]), $end_day[0]));
                $where .= " AND B.work_date <  '".$end_date."'";
            }
            //未発注
            if((isset($this->request->data['search']['yet']))){
                $where .= ') OR A.trn_order_header_id IS NULL )';
            }
            //取消
            if((isset($this->request->data['search']['is_deleted']))){
                $where .= ' AND A.is_deleted = FALSE';
            }
            //未入荷
            if(isset($this->request->data['search']['remain_count'])){
                $where .= ' and a.remain_count > 0  ';
            }
            //仕入先id(完全一致)
            if((isset($this->request->data['search']['supplierCode'])) && ($this->request->data['search']['supplierCode'] != "")){
                $where .= " AND F.facility_code = '". $this->request->data['search']['supplierCode'] . "'";
            }
            //要求元id(施設)(完全一致)
            if((isset($this->request->data['search']['facilityCode'])) && ($this->request->data['search']['facilityCode'] != "")){
                $where .= " AND D.facility_code = '". $this->request->data['search']['facilityCode'] . "'";
            }
            //要求元id(部署)(完全一致)
            if((isset($this->request->data['search']['departmentCode'])) && ($this->request->data['search']['departmentCode'] != "")){
                $where .= " AND C.department_code = '". $this->request->data['search']['departmentCode'] . "'";
            }
            //発注種別(完全一致)
            if((isset($this->request->data['search']['order_type'])) && ($this->request->data['search']['order_type'] != "")){
                $where .= ' AND A.order_type = '. $this->request->data['search']['order_type'];
            }
            //商品ID(部分一致)
            if((isset($this->request->data['search']['internal_code'])) && ($this->request->data['search']['internal_code'] != "")){
                $where .= " AND J.internal_code = '".$this->request->data['search']['internal_code']."'";
            }
            //製品番号(部分一致)
            if((isset($this->request->data['search']['item_code'])) && ($this->request->data['search']['item_code'] != "")){
                $where .= " AND J.item_code ILIKE '%".$this->request->data['search']['item_code']."%'";
            }
            //作業区分(完全一致)
            if((isset($this->request->data['search']['work_type'])) && ($this->request->data['search']['work_type'] != "")){
                $where .= ' AND A.work_type = '. $this->request->data['search']['work_type'];
            }
            //商品名(部分一致)
            if((isset($this->request->data['search']['item_name'])) && ($this->request->data['search']['item_name'] != "")){
                $where .= " AND J.item_name ILIKE '%".$this->request->data['search']['item_name']."%'";
            }
            //販売元(部分一致)
            if((isset($this->request->data['search']['dealer_name'])) && ($this->request->data['search']['dealer_name'] != "")){
                $where .= " AND K.dealer_name ILIKE '%".$this->request->data['search']['dealer_name']."%'";
            }
            //規格(部分一致)
            if((isset($this->request->data['search']['standard'])) && ($this->request->data['search']['standard'] != "")){
                $where .= " AND J.standard ILIKE '%".$this->request->data['search']['standard']."%'";
            }
            //---------------------------------------------------------------------------
        }elseif($mode==2){
            //発注番号(完全一致)
            if((isset($this->request->data['search']['work_no'])) && ($this->request->data['search']['work_no'] != "")){
                $where .= " AND A.work_no = '".$this->request->data['search']['work_no']."'";
            }
        }

        return $where;
    }

    /**
     *  定数発注数量情報取得
     *  @param $fixed_type = 使用する定数(定数、休日定数、臨時定数)　数量
     *  @param $facilities = 選択されている施設
     *  @param $order_type = 発注区分
     *  @return array
     */
    function _getRequestAmount($fixed_type ,$facility_id ,$department_id ,$order_type ) {

        $today = date('Y/m/d');

        $fix = 'fixed_count';
        //選択した定数区分によって取得するカラムが変わる
        switch($fixed_type){
          case Configure::read('FixedType,Constant'):
            $fix = 'fixed_count';
            break;
          case Configure::read('FixedType.Holiday'):
            $fix = 'holiday_fixed_count';
            break;
          case Configure::read('FixedType.Spare'):
            $fix = 'spare_fixed_count';
            break;
          default:
            $fix = 'fixed_count';
            break;
        }
        
        if($order_type == Configure::read('OrderTypes.nomal')){
            // 通常発注
            $sql  = ' select ';
            $sql .= '       a.internal_code ';
            $sql .= '     , d.per_unit ';
            $sql .= '     , trunc(  ';
            $sql .= '       (coalesce(x.count, 0) + coalesce(y.count, 0)) / cast (d.per_unit as dec) +.9999 ';
            $sql .= '     )                                  as "TrnOrder__quantity" ';
            $sql .= '     , c.transaction_price              as "TrnOrder__price" ';
            $sql .= '     , e.id                             as "TrnOrder__department_id_from" ';
            $sql .= '     , f.id                             as "TrnOrder__department_id_to" ';
            $sql .= '     , d.id                             as "TrnOrder__mst_item_unit_id" ';
            $sql .= '     , g.facility_name                  as "TrnOrder__facility_name"  ';
            $sql .= '   from ';
            $sql .= '     mst_facility_items as a  ';
            $sql .= '     left join (  ';
            $sql .= '       select ';
            $sql .= '             d.mst_facility_item_id ';
            $sql .= '           , sum(  ';
            $sql .= '             (  ';
            $sql .= '               case  ';
            $sql .= '                 when coalesce(b.order_point, 0) > (  ';
            $sql .= '                   a.stock_count - a.promise_count - a.reserve_count + a.requested_count + a.receipted_count ';
            $sql .= '                 )  ';
            $sql .= "                 then coalesce(b.{$fix}, 0) - ( ";
            $sql .= '                   a.stock_count - a.promise_count - a.reserve_count + a.requested_count + a.receipted_count ';
            $sql .= '                 )  ';
            $sql .= '                 else 0  ';
            $sql .= '                 end ';
            $sql .= '             ) * d.per_unit ';
            $sql .= '           )                          as count  ';
            $sql .= '         from ';
            $sql .= '           trn_stocks as a  ';
            $sql .= '           left join mst_fixed_counts as b  ';
            $sql .= '             on b.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '             and b.mst_department_id = a.mst_department_id  ';
            $sql .= '             and b.is_deleted = false ';
            $sql .= "             and b.start_date <= '" .$today. "' ";
            $sql .= '           left join mst_departments as c  ';
            $sql .= '             on c.id = a.mst_department_id  ';
            $sql .= '           left join mst_item_units as d  ';
            $sql .= '             on d.id = a.mst_item_unit_id  ';
            $sql .= '         where ';
            $sql .= '           c.department_type = ' . Configure::read('DepartmentType.warehouse');
            $sql .= '         group by ';
            $sql .= '           d.mst_facility_item_id  ';
            $sql .= '         having ';
            $sql .= '           sum(  ';
            $sql .= '             (  ';
            $sql .= '               case  ';
            $sql .= "                 when coalesce(b.order_point, 0) > ( ";
            $sql .= '                   a.stock_count - a.promise_count - a.reserve_count + a.requested_count + a.receipted_count ';
            $sql .= '                 )  ';
            $sql .= "                 then coalesce(b.{$fix}, 0) - ( ";
            $sql .= '                   a.stock_count - a.promise_count - a.reserve_count + a.requested_count + a.receipted_count ';
            $sql .= '                 )  ';
            $sql .= '                 else 0  ';
            $sql .= '                 end ';
            $sql .= '             ) * d.per_unit ';
            $sql .= '           ) > 0 ';
            $sql .= '     ) as x  '; // 不足分
            $sql .= '       on x.mst_facility_item_id = a.id  ';
            $sql .= '     left join (  ';
            $sql .= '       select ';
            $sql .= '             d.mst_facility_item_id ';
            $sql .= '           , sum(  ';
            $sql .= '             (  ';
            $sql .= '               case  ';
            $sql .= '                 when 0 < (  ';
            $sql .= '                   a.stock_count - a.promise_count - a.reserve_count + a.requested_count + a.receipted_count ';
            $sql .= '                 )  ';
            $sql .= "                 then ( case when coalesce(b.{$fix}, 0) - ( ";
            $sql .= '                   a.stock_count - a.promise_count - a.reserve_count + a.requested_count + a.receipted_count ';
            $sql .= '                 )  < 0 then ';
            $sql .= "                 ( coalesce(b.{$fix}, 0) - ( ";
            $sql .= '                   a.stock_count - a.promise_count - a.reserve_count + a.requested_count + a.receipted_count ';
            $sql .= '                 ) ) else 0 end ) ';
            $sql .= '                 else 0  ';
            $sql .= '                 end ';
            $sql .= '             ) * d.per_unit ';
            $sql .= '           )                          as count  ';
            $sql .= '         from ';
            $sql .= '           trn_stocks as a  ';
            $sql .= '           left join mst_fixed_counts as b  ';
            $sql .= '             on b.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '             and b.mst_department_id = a.mst_department_id  ';
            $sql .= '             and b.is_deleted = false ';
            $sql .= "             and b.start_date <= '" .$today. "' ";
            $sql .= '           left join mst_departments as c  ';
            $sql .= '             on c.id = a.mst_department_id  ';
            $sql .= '           left join mst_item_units as d  ';
            $sql .= '             on d.id = a.mst_item_unit_id  ';
            $sql .= '         where ';
            $sql .= '           c.department_type = ' . Configure::read('DepartmentType.warehouse');
            $sql .= '         group by ';
            $sql .= '           d.mst_facility_item_id  ';
            $sql .= '         having ';
            $sql .= '            sum(  ';
            $sql .= '             (  ';
            $sql .= '               case  ';
            $sql .= '                 when 0 < (  ';
            $sql .= '                   a.stock_count - a.promise_count - a.reserve_count + a.requested_count + a.receipted_count ';
            $sql .= '                 )  ';
            $sql .= "                 then ( case when coalesce(b.{$fix}, 0) - ( ";
            $sql .= '                   a.stock_count - a.promise_count - a.reserve_count + a.requested_count + a.receipted_count ';
            $sql .= '                 )  < 0 then ';
            $sql .= "                 ( coalesce(b.{$fix}, 0) - ( ";
            $sql .= '                   a.stock_count - a.promise_count - a.reserve_count + a.requested_count + a.receipted_count ';
            $sql .= '                 ) ) else 0 end ) ';
            $sql .= '                 else 0  ';
            $sql .= '                 end ';
            $sql .= '             ) * d.per_unit ';
            $sql .= '           ) < 0  ';
            $sql .= '     ) as y  '; // 余剰分
            $sql .= '       on y.mst_facility_item_id = a.id  ';
            $sql .= '     inner join mst_transaction_mains as b  ';
            $sql .= '       on b.mst_facility_item_id = a.id  ';
            $sql .= '     inner join mst_transaction_configs as c  ';
            $sql .= '       on c.partner_facility_id = b.mst_facility_id  ';
            $sql .= '       and c.mst_item_unit_id = b.mst_item_unit_id  ';
            $sql .= '       and c.is_deleted = false  ';
            $sql .= "       and c.start_date <= '" .$today. "' ";
            $sql .= "       and c.end_date > '" .$today . "' ";
            $sql .= '     left join mst_item_units as d ';
            $sql .= '       on d.id = c.mst_item_unit_id ';
            $sql .= '     left join mst_departments as e ';
            $sql .= '       on c.mst_facility_id = e.mst_facility_id  ';
            $sql .= '      and e.department_type = ' . Configure::read('DepartmentType.warehouse');
            $sql .= '      and e.is_deleted = false ';
            $sql .= '     left join mst_departments as f ';
            $sql .= '       on c.partner_facility_id = f.mst_facility_id  ';
            $sql .= '     left join mst_facilities as g ';
            $sql .= '       on g.id = c.partner_facility_id  ';
            $sql .= '     left join mst_departments as h ';
            $sql .= '       on c.partner_facility_id2 = h.mst_facility_id  ';
            $sql .= '     left join mst_facilities as i ';
            $sql .= '       on i.id = c.partner_facility_id2 ';
            $sql .= '   where ';
            $sql .= '     trunc((coalesce(x.count, 0) + coalesce(y.count, 0)) / cast (d.per_unit as dec) +.9999 ) > 0 ';
            $sql .= '     and a.is_deleted = false ';
            $sql .= "     and a.start_date <= '" . $today . "'";
            $sql .= "     and ( a.end_date >= '" . $today . "' or a.end_date is null )";
            $sql .= '   order by ';
            $sql .= '     a.internal_code ';
        }elseif($order_type == Configure::read('OrderTypes.opestock') ){
            // オペ倉庫発注
            $sql  = ' select ';
            $sql .= '       e.internal_code ';
            $sql .= '     , h.per_unit ';
            $sql .= '     , trunc(  ';
            $sql .= '       ( (a.fixed_count * d.per_unit ) - ( c.stock_count * d.per_unit ) + ( c.reserve_count * d.per_unit ) - ( coalesce(k.requested_count,0) * h.per_unit )) / cast (h.per_unit as dec) +.9999 ';
            $sql .= '     )                                 as "TrnOrder__quantity"';
            $sql .= '     , g.transaction_price             as "TrnOrder__price"';
            $sql .= '     , a.mst_department_id             as "TrnOrder__department_id_from"';
            $sql .= '     , j.id                            as "TrnOrder__department_id_to"';
            $sql .= '     , h.id                            as "TrnOrder__mst_item_unit_id"';
            $sql .= '     , i.facility_name                 as "TrnOrder__facility_name"';
            $sql .= '   from ';
            $sql .= '     mst_fixed_counts as a  ';
            $sql .= '     left join mst_departments as b  ';
            $sql .= '       on b.id = a.mst_department_id  ';
            $sql .= '     left join trn_stocks as c  ';
            $sql .= '       on c.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '       and c.mst_department_id = a.mst_department_id  ';
            $sql .= '     left join mst_item_units as d  ';
            $sql .= '       on d.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as e  ';
            $sql .= '       on e.id = d.mst_facility_item_id  ';
            $sql .= '     left join mst_transaction_mains as f  ';
            $sql .= '       on f.mst_facility_item_id = e.id  ';
            $sql .= '     left join mst_transaction_configs as g  ';
            $sql .= '       on g.mst_item_unit_id = f.mst_item_unit_id  ';
            $sql .= '       and g.partner_facility_id = f.mst_facility_id  ';
            $sql .= '       and g.is_deleted = false  ';
            $sql .= "       and g.start_date <= '" . $today . "'";
            $sql .= "       and g.end_date > '" . $today . "'";
            $sql .= '     left join mst_item_units as h  ';
            $sql .= '       on h.id = g.mst_item_unit_id  ';
            $sql .= '     left join mst_facilities as i  ';
            $sql .= '       on i.id = g.partner_facility_id  ';
            $sql .= '     left join mst_departments as j  ';
            $sql .= '       on j.mst_facility_id = i.id  ';
            $sql .= '     left join mst_facilities as i2  ';
            $sql .= '       on i2.id = g.partner_facility_id2  ';
            $sql .= '     left join mst_departments as j2  ';
            $sql .= '       on j2.mst_facility_id = i2.id  ';
            $sql .= '     left join trn_stocks as k ';
            $sql .= '       on k.mst_department_id = b.id ';
            $sql .= '      and k.mst_item_unit_id = f.mst_item_unit_id ';
            $sql .= '   where ';
            $sql .= '     b.department_type = ' . Configure::read('DepartmentType.opestock');
            $sql .= '     and a.is_deleted = false  ';
            $sql .= '     and ( (a.fixed_count * d.per_unit ) - ( c.stock_count * d.per_unit ) + ( c.reserve_count * d.per_unit ) - ( coalesce(k.requested_count,0) * h.per_unit )) > 0 ';
            $sql .= '   order by e.internal_code ';
        }else{
            // 預託、非在庫発注
            $sql  = ' select ';
            $sql .= '       c.internal_code ';
            $sql .= '     , f.per_unit ';
            $sql .= "     , ( case when b.{$fix} > ( coalesce(a2.stock_count,0) + a.requested_count ) ";
            $sql .= "         then b.{$fix} - ( coalesce(a2.stock_count,0) + a.requested_count ) ";
            $sql .= '         else 0 ';
            $sql .= '        end )                                 as "TrnOrder__quantity" ';
            $sql .= '     , d.transaction_price                    as "TrnOrder__price" ';
            $sql .= '     , a.mst_department_id                    as "TrnOrder__department_id_from" ';
            $sql .= '     , h.id                                   as "TrnOrder__department_id_to" ';
            $sql .= '     , d.mst_item_unit_id                     as "TrnOrder__mst_item_unit_id" ';
            $sql .= '     , g.facility_name                        as "TrnOrder__facility_name"  ';
            $sql .= '   from ';
            $sql .= '     trn_stocks as a  ';
            if($this->Session->read('Auth.Config.StockType') == 0 ){
                // 臨時在庫を判定する
                $sql .= '     left join ( ';
                $sql .= '       select a.mst_department_id , a.mst_item_unit_id , sum(a.quantity) as stock_count from trn_stickers as a ';
                $sql .= '       where a.is_deleted = false ';
                $sql .= '             and a.quantity > 0 ';
                $sql .= "             and ( a.facility_sticker_no != '' or a.hospital_sticker_no != '' ) ";
                $sql .= '             and a.trade_type not in ( ' . Configure::read('ClassesType.Temporary') . ' , ' . Configure::read('ClassesType.ExShipping') . ' , ' . Configure::read('ClassesType.ExNoStock') . ' )';
                $sql .= '       group by a.mst_department_id , a.mst_item_unit_id ';
                $sql .= '     ) as a2 ';
                $sql .= '     on a2.mst_department_id = a.mst_department_id ';
                $sql .= '     and a2.mst_item_unit_id = a.mst_item_unit_id';
            }else{
                // 臨時在庫を判定しない
                $sql .= '     left join trn_stocks as a2 ';
                $sql .= '     on a2.mst_department_id = a.mst_department_id ';
                $sql .= '     and a2.mst_item_unit_id = a.mst_item_unit_id';
            }
            $sql .= '     inner join mst_fixed_counts as b  ';
            $sql .= '       on b.trn_stock_id = a.id  ';
            $sql .= '      and b.is_deleted = false ';
            $sql .= "      and b.start_date <= '$today'" ;
            $sql .= '     left join mst_departments as e  ';
            $sql .= '       on e.id = a.mst_department_id  ';
            $sql .= '      and e.is_deleted = false ';
            $sql .= '     left join mst_item_units as f  ';
            $sql .= '       on f.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as c  ';
            $sql .= '       on c.id = f.mst_facility_item_id  ';
            $sql .= '     inner join mst_transaction_configs as d  ';
            $sql .= '       on d.mst_item_unit_id = f.id  ';
            $sql .= '       and d.is_deleted = false  ';
            $sql .= "       and d.start_date <= '$today'";
            $sql .= "       and d.end_date > '$today' ";
            $sql .= '     left join mst_transaction_mains as d2';
            $sql .= '        on d2.mst_facility_item_id = c.id ';
            $sql .= '      and d.partner_facility_id = d2.mst_facility_id ';
            $sql .= '     left join mst_facilities as g ';
            $sql .= '        on g.id = d.partner_facility_id ';
            $sql .= '     left join mst_departments as h ';
            $sql .= '        on g.id = h.mst_facility_id ';
            $sql .= '     left join mst_facilities as i ';
            $sql .= '        on i.id = d.partner_facility_id2 ';
            $sql .= '     left join mst_departments as j ';
            $sql .= '        on i.id = j.mst_facility_id ';
            
            $sql .= '   where 1=1';
            
            //$sql .= '     and e.mst_facility_id = ' . $facility_id;
            $sql .= '     and e.department_type = ' . Configure::read('DepartmentType.hospital');
            
            //発注区分
            if($order_type == Configure::read('OrderTypes.replenish')){
                //預託品
                $sql .= '     and b.trade_type = ' . Configure::read('TeisuType.Deposit');
            }else{
                //非在庫品
                $sql .= '     and b.trade_type = ' . Configure::read('TeisuType.NonStock');
            }
            //選択部署
            if($department_id){
                $sql .= '     and a.mst_department_id = ' . $department_id;
            }
            $sql .= '     and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            //在庫
            $sql .= '     and a.is_deleted = false ' ;
            //包装単位
            $sql .= '     and f.is_deleted = false ' ;
            $sql .= "     and ( f.end_date >= '$today' or f.end_date is null ) " ;
            //商品
            $sql .= '     and c.is_deleted = false ' ;
            $sql .= "     and ( c.end_date >= '$today' or c.end_date is null ) " ;
            

            $sql .= '  order by ';
            $sql .= '    c.internal_code ';
        }
        return $this->TrnOrder->query($sql);
    }
    
    //売上依頼条件取得
    function _getSalesRequestCond ($cond = array()) {
        $tmpCond = array('TrnClaim.is_deleted' => FALSE
                         ,'TrnClaim.is_stock_or_sale'=>1);
        if(isset($cond['TrnClaim'])){
            $cond['TrnClaim'] = array_merge($cond['TrnClaim'],$tmpCond);
        }else{
            $cond['TrnClaim'] = $tmpCond;
        }

        $tmpCond = array('TrnConsume.is_deleted' => FALSE
                         ,'TrnConsume.id = TrnClaim.trn_consume_id');
        if(isset($cond['TrnConsume'])){
            $cond['TrnConsume'] = array_merge($cond['TrnConsume'],$tmpCond);
        }else{
            $cond['TrnConsume'] = $tmpCond;
        }

        $tmpCond = array('MstFacilitySale.facility_type' => Configure::read('FacilityType.hospital')
                         ,'MstFacilitySale.id = MstDepartmentSale.mst_facility_id'
                         ,'MstFacilitySale.is_deleted' => FALSE);
        if(isset($cond['MstFacilitySale'])){
            $cond['MstFacilitySale'] = array_merge($cond['MstFacilitySale'],$tmpCond);
        }else{
            $cond['MstFacilitySale'] = $tmpCond;
        }

        $tmpCond = array('MstDepartmentSale.id = TrnConsume.mst_department_id'
                         ,'MstDepartmentSale.is_deleted' => FALSE);
        if(isset($cond['MstDepartmentSale'])){
            $cond['MstDepartmentSale'] = array_merge($cond['MstDepartmentSale'],$tmpCond);
        }else{
            $cond['MstDepartmentSale'] = $tmpCond;
        }

        $tmpCond = array('TrnSalesRequestHeader.id = TrnConsume.trn_sales_request_header_id'
                         ,'TrnSalesRequestHeader.is_deleted' => FALSE);
        if(isset($cond['TrnSalesRequestHeader'])){
            $cond['TrnSalesRequestHeader'] = array_merge($cond['TrnSalesRequestHeader'],$tmpCond);
        }else{
            $cond['TrnSalesRequestHeader'] = $tmpCond;
        }

        $tmpCond = array('TrnSticker.id = TrnConsume.trn_sticker_id'
                         ,'TrnSticker.is_deleted' => FALSE);
        if(isset($cond['TrnSticker'])){
            $cond['TrnSticker'] = array_merge($cond['TrnSticker'],$tmpCond);
        }else{
            $cond['TrnSticker'] = $tmpCond;
        }

        $cond['MstDepartmentTo'] = array('MstDepartmentTo.mst_facility_id = MstFacilityTo.id');

        $cond['MstFacilityTo'] = array('MstFacilityTo.facility_type' => Configure::read('FacilityType.supplier')
                                       ,'MstFacilityTo.id = coalesce(MstDepartmentOrder.mst_facility_id , MstTransactionConfig.partner_facility_id)');

        //発注元（病院）にも施設関係があるか検索条件に含める
        $cond['MstDepartmentFrom'] = array('MstDepartmentFrom.id = TrnConsume.mst_department_id');
        $cond['MstFacilityFrom'] = array('MstFacilityFrom.facility_type' => Configure::read('FacilityType.hospital')
                                         ,'MstFacilityFrom.id = MstDepartmentFrom.mst_facility_id');

        $cond['MstItemUnit'] = array('TrnClaim.mst_item_unit_id = MstItemUnit.id');
        $cond['MstUnitName'] = array('MstItemUnit.mst_unit_name_id = MstUnitName.id');
        $cond['MstParUnitName'] = array('MstItemUnit.per_unit_name_id = MstParUnitName.id');

        if($this->Session->check('Auth.facility_id_selected')){
            $center = $this->Session->read('Auth.facility_id_selected');
        }else{
            $center = $this->request->data['TrnOrder']['selected_facility_id'];
        }

        $tmpCond = array('MstFacilityItem.id = MstItemUnit.mst_facility_item_id'
                         ,'MstFacilityItem.mst_facility_id' => $center
                         );
        if(isset($cond['MstFacilityItem'])){
            $cond['MstFacilityItem'] = array_merge($cond['MstFacilityItem'],$tmpCond);
        }else{
            $cond['MstFacilityItem'] = $tmpCond;
        }

        $tmpCond = array('MstOwner.id = MstFacilityItem.mst_owner_id');
        if(isset($cond['MstOwner'])){
            $cond['MstOwner'] = array_merge($cond['MstOwner'],$tmpCond);
        }else{
            $cond['MstOwner'] = $tmpCond;
        }

        $tmpCond = array('MstDealer.id = MstFacilityItem.mst_dealer_id');
        if(isset($cond['MstDealer'])){
            $cond['MstDealer'] = array_merge($cond['MstDealer'],$tmpCond);
        }else{
            $cond['MstDealer'] = $tmpCond;
        }

        $cond['MstFacilityRelation'] = array('MstFacilityRelation.mst_facility_id = '.$center
                                             ,'MstFacilityRelation.partner_facility_id = MstFacilityTo.id'
                                             ,'MstFacilityRelation.is_deleted' => FALSE);

        //発注元（病院）にも施設関係があるか検索条件に含める
        $cond['MstFacilityRelationTo'] = array('MstFacilityRelationTo.mst_facility_id = '.$center
                                               ,'MstFacilityRelationTo.partner_facility_id = MstFacilityFrom.id'
                                               );

        $cond['MstTransactionConfig'] = array('MstTransactionConfig.mst_item_unit_id=TrnClaim.mst_item_unit_id'
                                              ,'MstTransactionConfig.start_date<=TrnClaim.claim_date'
                                              ,'MstTransactionConfig.end_date>=TrnClaim.claim_date'
                                              ,'MstTransactionConfig.is_main=true'
                                              );

        $cond['MstUser'] = array('MstUser.id = TrnConsume.modifier'
                                 ,'MstUser.is_deleted' => FALSE);
        return $cond;
    }

    //売上依頼情報取得
    function _getSalesRequest ($cond,$order=array()) {
        $this->TrnClaim->Behaviors->attach('Containable');
        $field = array('TrnClaim.id'
                       ,'to_char("TrnClaim"."claim_date" , \'YYYY年mm月dd日\') as "TrnClaim__claim_date"'
                       ,'TrnClaim.claim_price'
                       ,'TrnClaim.unit_price'
                       ,'TrnClaim.count'
                       ,'TrnClaim.round'
                       ,'TrnClaim.gross'
                       ,'TrnClaim.is_deleted'
                       ,'TrnConsume.id'
                       ,'TrnConsume.work_type'
                       ,'TrnConsume.mst_item_unit_id'
                       ,'TrnConsume.work_seq'
                       ,'TrnConsume.mst_department_id'
                       ,'TrnConsume.quantity'
                       ,'TrnConsume.trn_sticker_id'
                       ,'TrnConsume.work_date'
                       ,'TrnConsume.trn_sales_request_header_id'
                       ,'TrnConsume.before_quantity'
                       ,'TrnSticker.id'
                       ,'TrnSticker.hospital_sticker_no'
                       ,'TrnSticker.lot_no'
                       ,'MstFacilityTo.id'
                       ,'MstFacilityTo.facility_formal_name'
                       ,'MstFacilityTo.facility_name'
                       ,'MstDepartmentTo.id'
                       ,'MstDepartmentTo.department_name'
                       ,'MstDepartmentTo.department_formal_name'
                       ,'MstDepartmentTo.mst_facility_id'
                       ,'MstFacilitySale.id'
                       ,'MstFacilitySale.facility_formal_name'
                       ,'MstFacilitySale.facility_name'
                       ,'MstDepartmentSale.id'
                       ,'MstDepartmentSale.department_name'
                       ,'MstDepartmentSale.department_formal_name'
                       ,'MstDepartmentSale.mst_facility_id'
                       ,'MstUser.id'
                       ,'MstUser.user_name'
                       ,'(case when "MstItemUnit"."per_unit" = 1 then "MstUnitName"."unit_name" else "MstUnitName"."unit_name" || \'(\' || "MstItemUnit"."per_unit" || "MstParUnitName"."unit_name" || \')\' end ) as "MstItemUnit__unit_name"'
                       ,'MstItemUnit.id'
                       ,'MstItemUnit.mst_facility_item_id'
                       ,'MstItemUnit.mst_unit_name_id'
                       ,'MstItemUnit.per_unit'
                       ,'MstItemUnit.per_unit_name_id'
                       ,'MstItemUnit.mst_facility_id'
                       ,'MstFacilityItem.id'
                       ,'MstFacilityItem.mst_facility_id'
                       ,'MstFacilityItem.internal_code'
                       ,'MstFacilityItem.item_name'
                       ,'MstFacilityItem.item_code'
                       ,'MstFacilityItem.standard'
                       ,'MstFacilityItem.unit_price'
                       ,'MstFacilityItem.per_unit'
                       ,'MstFacilityItem.tax_type'
                       ,'MstOwner.id'
                       ,'MstOwner.facility_formal_name'
                       ,'MstOwner.zip'
                       ,'MstOwner.address'
                       ,'MstOwner.tel'
                       ,'MstOwner.fax'
                       ,'MstDealer.dealer_name'
                       ,'MstClass.name'
                       ,'TrnSalesRequestHeader.id'
                       ,'TrnSalesRequestHeader.work_no'
                       ,'TrnSalesRequestHeader.recital'
                       ,'TrnSalesRequestHeader.recital_date'
                       ,'TrnSalesRequestHeader.detail_count');

        $join[] = array('table' => 'trn_consumes'
                        ,'alias' => 'TrnConsume'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['TrnConsume']);

        $join[] = array('table' => 'mst_transaction_configs'
                        ,'alias' => 'MstTransactionConfig'
                        ,'type' => 'LEFT'
                        ,'conditions' => $cond['MstTransactionConfig']);

        $join[] = array('table' => 'trn_stickers'
                        ,'alias' => 'TrnSticker'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['TrnSticker']);

        $join[] = array('table' => 'trn_orders'
                        ,'alias' => 'TrnOrder'
                        ,'type' => 'LEFT'
                        ,'conditions' => 'TrnSticker.trn_order_id = TrnOrder.id');

        $join[] = array('table' => 'mst_departments'
                        ,'alias' => 'MstDepartmentOrder'
                        ,'type' => 'LEFT'
                        ,'conditions' => 'MstDepartmentOrder.id = TrnOrder.department_id_to');

        $join[] = array('table' => 'mst_departments'
                        ,'alias' => 'MstDepartmentSale'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstDepartmentSale']);

        $join[] = array('table' => 'mst_facilities'
                        ,'alias' => 'MstFacilitySale'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstFacilitySale']);

        $join[] = array('table' => 'mst_facilities'
                        ,'alias' => 'MstFacilityTo'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstFacilityTo']);

        $join[] = array('table' => 'mst_departments'
                        ,'alias' => 'MstDepartmentTo'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstDepartmentTo']);

        $join[] = array('table' => 'mst_departments'
                        ,'alias' => 'MstDepartmentFrom'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstDepartmentFrom']);

        $join[] = array('table' => 'mst_facilities'
                        ,'alias' => 'MstFacilityFrom'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstFacilityFrom']);

        $join[] = array('table' => 'mst_item_units'
                        ,'alias' => 'MstItemUnit'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstItemUnit']);

        $join[] = array('table' => 'mst_facility_items'
                        ,'alias' => 'MstFacilityItem'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstFacilityItem']);

        $join[] = array('table' => 'mst_facilities'
                        ,'alias' => 'MstOwner'
                        ,'type' => 'LEFT'
                        ,'conditions' => $cond['MstOwner']);


        $join[] = array('table' => 'mst_dealers'
                        ,'alias' => 'MstDealer'
                        ,'type' => 'LEFT'
                        ,'conditions' => $cond['MstDealer']);

        $join[] = array('table' => 'mst_unit_names'
                        ,'alias' => 'MstUnitName'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstUnitName']);

        $join[] = array('table' => 'mst_unit_names'
                        ,'alias' => 'MstParUnitName'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstParUnitName']);

        $join[] = array('table' => 'mst_facility_relations'
                        ,'alias' => 'MstFacilityRelation'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstFacilityRelation']);
        $join[] = array('table' => 'mst_facility_relations'
                        ,'alias' => 'MstFacilityRelationTo'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstFacilityRelationTo']);

        $join[] = array('table' => 'trn_sales_request_headers'
                        ,'alias' => 'TrnSalesRequestHeader'
                        ,'type' => 'LEFT'
                        ,'conditions' => $cond['TrnSalesRequestHeader']);

        $join[] = array('table' => 'mst_users'
                        ,'alias' => 'MstUser'
                        ,'type' => 'INNER'
                        ,'conditions' => $cond['MstUser']);

        $join[] = array('table' => 'mst_classes'
                        ,'alias' => 'MstClass'
                        ,'type' => 'LEFT'
                        ,'conditions' => array('MstClass.id = TrnConsume.work_type'));



        $join[] = array('table' => 'mst_user_belongings'
                        ,'alias' => 'MstUserBelonging'
                        ,'type' => 'INNER'
                        ,'conditions' => array('MstUserBelonging.mst_facility_id = MstFacilityFrom.id' ,
                                               'MstUserBelonging.mst_user_id = ' . $this->request->data['TrnOrder']['user_id'],
                                               'MstUserBelonging.is_deleted = false'
                                               ));


        if(isset($cond['TrnSalesRequestHeader']['TrnSalesRequestHeader.work_no'])){
            $cond['TrnClaim']['TrnSalesRequestHeader.work_no'] = $cond['TrnSalesRequestHeader']['TrnSalesRequestHeader.work_no'];
        }
        if(!isset($order) || $order !== array()){
            $order = array('TrnSalesRequestHeader.work_no','TrnClaim.department_id_to','TrnConsume.mst_department_id','TrnConsume.work_date');
        }

        $param = array('conditions'=>$cond['TrnClaim']
                       ,'fields' => $field
                       ,'order' => array('TrnSalesRequestHeader.work_no','TrnClaim.department_id_to','TrnConsume.mst_department_id','TrnConsume.work_date')
                       ,'joins' => $join);

        if(isset($this->request->data['limit'])){
            $limit = $this->request->data['limit'];
            if($limit !== ""){
                $param['limit'] = $limit;
            }
        }else{
            $param['limit'] = min(array_keys(Configure::read('displaycounts_combobox')));
        }
        $param['group'] = array(
            'TrnClaim.id',
            'TrnClaim.claim_date',
            'TrnClaim.claim_price',
            'TrnClaim.unit_price',
            'TrnClaim.count',
            'TrnClaim.round',
            'TrnClaim.gross',
            'TrnClaim.is_deleted',
            'TrnClaim.department_id_to',
            'TrnConsume.id',
            'TrnConsume.work_type',
            'TrnConsume.mst_item_unit_id',
            'TrnConsume.work_seq',
            'TrnConsume.mst_department_id',
            'TrnConsume.quantity',
            'TrnConsume.trn_sticker_id',
            'TrnConsume.work_date',
            'TrnConsume.trn_sales_request_header_id',
            'TrnConsume.before_quantity',
            'TrnSticker.id',
            'TrnSticker.hospital_sticker_no',
            'TrnSticker.lot_no',
            'MstFacilityTo.id',
            'MstFacilityTo.facility_formal_name',
            'MstFacilityTo.facility_name',
            'MstDepartmentTo.id',
            'MstDepartmentTo.department_name',
            'MstDepartmentTo.department_formal_name',
            'MstDepartmentTo.mst_facility_id',
            'MstFacilitySale.id',
            'MstFacilitySale.facility_formal_name',
            'MstFacilitySale.facility_name',
            'MstDepartmentSale.id',
            'MstDepartmentSale.department_name',
            'MstDepartmentSale.department_formal_name',
            'MstDepartmentSale.mst_facility_id',
            'MstUser.id',
            'MstUser.user_name',
            'MstItemUnit.id',
            'MstItemUnit.mst_facility_item_id',
            'MstItemUnit.mst_unit_name_id',
            'MstItemUnit.per_unit',
            'MstItemUnit.per_unit_name_id',
            'MstItemUnit.mst_facility_id',
            'MstFacilityItem.id',
            'MstFacilityItem.mst_facility_id',
            'MstFacilityItem.internal_code',
            'MstFacilityItem.item_name',
            'MstFacilityItem.item_code',
            'MstFacilityItem.standard',
            'MstFacilityItem.unit_price',
            'MstFacilityItem.per_unit',
            'MstFacilityItem.tax_type',
            'MstOwner.id',
            'MstOwner.facility_formal_name',
            'MstOwner.zip',
            'MstOwner.address',
            'MstOwner.tel',
            'MstOwner.fax',
            'MstDealer.id',
            'MstDealer.dealer_name',
            'MstUnitName.id',
            'MstUnitName.unit_name',
            'MstParUnitName.id',
            'MstParUnitName.unit_name',
            'MstClass.name',
            'TrnSalesRequestHeader.id',
            'TrnSalesRequestHeader.work_no',
            'TrnSalesRequestHeader.recital',
            'TrnSalesRequestHeader.recital_date',
            'TrnSalesRequestHeader.detail_count'
            );
        $result = $this->TrnClaim->find('all',$param);
        return $result;
    }
    
    //在庫情報 未入荷数を更新
    function _updateStockData ($department,$unit,$quantity) {
        $userId = $this->Session->read('Auth.MstUser.id');
        $date = date('Y-m-d H:i:s');
        
        //在庫情報を更新
        $trnStock = array('requested_count'  => 'requested_count + ' . $quantity
                          , 'modifier'  => $userId
                          ,  'modified'  => "'$date'"
                          
                          );
        $stock_id['TrnStock']['modified'] = $date;
        
        $stock_update = $this->TrnStock->updateAll($trnStock,array(
            'TrnStock.id' => $this->getStockRecode($unit ,$department )
            ),-1);
        return $stock_update == false ? false  : $stock_id;
    }

    /**
     * _getOrderClassesList
     *
     * 作業区分データ取得
     */
    function _getOrderClassesList($fid = null) {
        $order_classes_List = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),self::WORK_CLASSES_ORDER);
        return $order_classes_List;
    }

    /**
     * 発注明細の印刷日時更新
     */
    private function setPrintedDate($id,$date = null,$uid = null){
        if(isset($uid)){
            $userId = $uid;
        }else{
            $userId = $this->Session->read('Auth.MstUser.id');
        }
        if(isset($date) && $date!==""){
            $date = date('Y-m-d H:i:s');
        }
        
        //在庫情報を更新
        $printed_date = array('modified'       => "'" . $date . "'"
                              ,'printed_date'  => "'" . $date . "'"
                              ,'modifier'      => $userId
                              );
        $this->TrnOrderHeader->updateAll($printed_date,array(
            'TrnOrderHeader.id' => $id
            ),-1);
    }
    
    /**
     * xml出力テスト用
     * @param
     * @todo 各コントローラの共通関数化
     */
    function xml_output($data,$columns,$layout_name,$fix_value){
        $this->layout = 'xml/default';
        $this->set('columns',$columns);
        $this->set('posted',$data);
        $this->set('layout_name',$layout_name);
        $this->set('fix_value',$fix_value); //連想配列のキーにname属性を指定
        $this->render('xml_output');
    }

    /**
     * コストシール印字用SQL
     **/
    private function getCostStickerSQL(){
        $sql  = 'select ';
        $sql .= '      a3.facility_name ';
        $sql .= '    , c2.medical_code ';
        $sql .= '    , c.internal_code ';
        $sql .= '    , c.item_name ';
        $sql .= '    , c.item_code ';
        $sql .= '    , c.standard ';
        $sql .= '    , c.refund_price ';
        $sql .= '    , c.biogenous_type ';
        $sql .= '    , d.dealer_name ';
        $sql .= '    , e.insurance_claim_name_s ';
        $sql .= '    , f.const_nm as insurance_claim_department_name ';
        $sql .= '    , a.quantity ';
        $sql .= '    , b.per_unit  ';
        $sql .= '  from ';
        $sql .= '    trn_orders as a  ';
        $sql .= '    left join mst_departments as a2  ';
        $sql .= '      on a2.id = a.department_id_from  ';
        $sql .= '    left join mst_facilities as a3  ';
        $sql .= '      on a3.id = a2.mst_facility_id  ';
        $sql .= '    left join mst_item_units as b  ';
        $sql .= '      on b.id = a.mst_item_unit_id  ';
        $sql .= '    left join mst_facility_items as c  ';
        $sql .= '      on c.id = b.mst_facility_item_id  ';
        $sql .= '    left join mst_medical_codes as c2  ';
        $sql .= '      on c.id = c2.mst_facility_item_id  ';
        $sql .= '      and c2.mst_facility_id = a3.id  ';
        $sql .= '    left join mst_dealers as d  ';
        $sql .= '      on d.id = c.mst_dealer_id  ';
        $sql .= '    left join mst_insurance_claims as e  ';
        $sql .= '      on c.insurance_claim_code = e.insurance_claim_code  ';
        $sql .= '    left join mst_consts as f  ';
        $sql .= '      on c.insurance_claim_type::varchar = f.const_cd  ';
        $sql .= '  where ';
        $sql .= '    1 = 1  ';
        $sql .= "    and c.sealissue_medicalcode = true  ";

        return $sql;
    }

    // 2度押し対策
    function _createTransactionToken() {
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnOrder.token' , $token);
        $this->request->data['TrnOrder']['token'] = $token;
    }

    //自動計算発注数を取得
    function _getOrderCount($facility_item_id){
        $sql  = ' select ';
        $sql .= '       sum( ';
        $sql .= '       case  ';
        $sql .= '         when (  ';
        $sql .= '           b.stock_count - b.reserve_count - b.promise_count + b.receipted_count ';
        $sql .= '         ) <= coalesce(c.order_point, 0)  ';
        $sql .= '         then (  ';
        $sql .= '           (  ';
        $sql .= '             (  ';
        $sql .= '               coalesce(c.fixed_count, 0) - (  ';
        $sql .= '                 b.stock_count - b.reserve_count - b.promise_count + b.receipted_count ';
        $sql .= '               ) ';
        $sql .= '             ) * a.per_unit ';
        $sql .= '           ) / e.per_unit + (  ';
        $sql .= '             case  ';
        $sql .= '               when (  ';
        $sql .= '                 (  ';
        $sql .= '                   coalesce(c.fixed_count, 0) - (  ';
        $sql .= '                     b.stock_count - b.reserve_count - b.promise_count + b.receipted_count ';
        $sql .= '                   ) ';
        $sql .= '                 ) * a.per_unit ';
        $sql .= '               ) % e.per_unit > 0  ';
        $sql .= '               then 1  ';
        $sql .= '               else 0  ';
        $sql .= '               end ';
        $sql .= '           ) ';
        $sql .= '         )  ';
        $sql .= '         else 0  ';
        $sql .= '         end ';
        $sql .= '     )  - f.requested_count          as order_count  ';
        $sql .= '   from ';
        $sql .= '     mst_item_units as a  ';
        $sql .= '     left join trn_stocks as b  ';
        $sql .= '       on b.mst_item_unit_id = a.id ';
        $sql .= '    left join mst_departments as b2 ';
        $sql .= '      on b2.id = b.mst_department_id ';
        $sql .= '     left join mst_fixed_counts as c ';
        $sql .= '       on c.trn_stock_id = b.id ';
        $sql .= '     inner join mst_transaction_mains as d  ';
        $sql .= '       on d.mst_facility_item_id = a.mst_facility_item_id  ';
        $sql .= '     left join mst_item_units as e ';
        $sql .= '       on e.id = d.mst_item_unit_id ';
        $sql .= '    left join trn_stocks as f ';
        $sql .= '      on f.mst_item_unit_id = e.id ';
        $sql .= '    left join mst_departments as g ';
        $sql .= '      on g.id = f.mst_department_id ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_item_id = ' . $facility_item_id;
        $sql .= '     and g.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '     and b2.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '  group by ';
        $sql .= '    f.requested_count ';

        $ret = $this->MstItemUnit->query($sql);
        return (isset($ret[0][0]['order_count'])?$ret[0][0]['order_count']:0);

    }

    //商品検索：任意発注カート部分
    function getFacilityItem($where, $limit=null){
        $sql  = ' select ';
        $sql .= '       a.id                             as "MstFacilityItem__id" ';
        $sql .= '     , a.internal_code                  as "MstFacilityItem__internal_code" ';
        $sql .= '     , a.jan_code                       as "MstFacilityItem__jan_code" ';
        $sql .= '     , a.item_name                      as "MstFacilityItem__item_name" ';
        $sql .= '     , a.item_code                      as "MstFacilityItem__item_code" ';
        $sql .= '     , a.standard                       as "MstFacilityItem__standard" ';
        $sql .= '     , b.dealer_name                    as "MstDealer__dealer_name"  ';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_dealers as b  ';
        $sql .= '       on a.mst_dealer_id = b.id  ';
        $sql .= '     inner join mst_item_units as c  ';
        $sql .= '       on c.mst_facility_item_id = a.id  ';
        $sql .= '       and c.is_deleted = false  ';
        $sql .= '     inner join mst_transaction_configs as d  ';
        $sql .= '       on d.mst_item_unit_id = c.id  ';
        $sql .= '       and d.is_deleted = false  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_id =' .  $this->Session->read('Auth.facility_id_selected');
        $sql .= $where;
        $sql .= '   group by ';
        $sql .= '     a.id ';
        $sql .= '     , a.internal_code ';
        $sql .= '     , a.jan_code ';
        $sql .= '     , a.item_name ';
        $sql .= '     , a.item_code ';
        $sql .= '     , a.standard ';
        $sql .= '     , b.dealer_name  ';
        $sql .= '   order by ';
        $sql .= '     a.internal_code  ';

        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));
            $sql .= '   limit ' . $limit;
        }
        return $this->MstFacilityItem->query($sql);
    }

    
    function order_decision_search( $supplier_id , $order_facility_id , $order_department_id , $order_type , $where = null ){
        $sql  = ' select ';
        $sql .= '     a.id                       as "TrnOrder__id"';
        $sql .= '   , a.work_type                as "TrnOrder__work_type"';
        $sql .= '   , a.recital                  as "TrnOrder__recital"';
        $sql .= '   , a.before_quantity          as "TrnOrder__before_quantity"';
        $sql .= '   , c.internal_code            as "TrnOrder__internal_code"';
        $sql .= '   , c.item_name                as "TrnOrder__item_name"';
        $sql .= '   , c.item_code                as "TrnOrder__item_code"';
        $sql .= '   , c.standard                 as "TrnOrder__standard"';
        $sql .= '   , c.spare_key2               as "TrnOrder__aptage_item_code"';
        $sql .= '   , c.spare_key6               as "TrnOrder__aptage_facility_code"';
        $sql .= '   , c.is_lowlevel              as "TrnOrder__is_lowlevel"';
        $sql .= '   , d.dealer_name              as "TrnOrder__dealer_name"';
        $sql .= '   , b1.internal_code           as "TrnOrder__unit_code"';
        $sql .= '   , ( case  ';
        $sql .= '       when b.per_unit = 1  ';
        $sql .= '       then b1.unit_name  ';
        $sql .= "       else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '       end )                    as "TrnOrder__unit_name"';
        $sql .= '   , xxx2.unit_name             as "TrnOrder__aptage_unit_name"';
        $sql .= '   , ( case  ';
        foreach(Configure::read('OrderTypes.orderTypeName') as $k => $v){
            $sql .= '  when a.order_type = ' . $k . " then '" . $v . "'";
        }
        $sql .= '     end )                      as "TrnOrder__order_type_name"';
        $sql .= '   , a.order_type               as "TrnOrder__order_type"';
        $sql .= '   , f.id                       as "TrnOrder__supplier_id"';
        $sql .= '   , f.facility_name            as "TrnOrder__supplier_name"';
        $sql .= '   , g.transaction_price        as "TrnOrder__transaction_price"';
        $sql .= '   , h.stock_count              as "TrnOrder__stock_count"';
        $sql .= '   , coalesce(y.remain_count,0) as "TrnOrder__remain_count"';
        $sql .= '   , x.work_date                as "TrnOrder__last_order_date"';
        
        $sql .= '   , ( case ';
        $sql .= "      when g.id is null then '仕入設定が無効です。'";
        $sql .= "      when b.is_deleted = true then '包装単位が無効です。'";
        $sql .= "      when c.is_deleted = true then '採用品が無効です。'";
        $sql .="       else '' ";
        $sql .= '      end )                     as "TrnOrder__alert_message"';
        $sql .= '   , e2.id                      as "TrnOrder__department_id"';
        $sql .= '   , e2.department_name         as "TrnOrder__department_name"';
        $sql .= ' from ';
        $sql .= '   trn_orders as a ';
        $sql .= '   left join mst_item_units as b ';
        $sql .= '     on b.id = a.mst_item_unit_id ';
        $sql .= '   left join mst_unit_names as b1 ';
        $sql .= '     on b1.id = b.mst_unit_name_id ';
        $sql .= '   left join mst_unit_names as b2 ';
        $sql .= '     on b2.id = b.per_unit_name_id ';
        $sql .= '   left join mst_facility_items as c ';
        $sql .= '     on c.id = b.mst_facility_item_id ';
        $sql .= '   left join mst_item_units as xxx';
        $sql .= '     on xxx.mst_facility_item_id = c.id ';
        $sql .= '    and xxx.per_unit = 1 ';
        $sql .= '   left join mst_unit_names as xxx2 ';
        $sql .= '     on xxx2.id = xxx.mst_unit_name_id ';
        $sql .= '   left join mst_dealers as d ';
        $sql .= '     on d.id = c.mst_dealer_id ';
        $sql .= '   left join mst_departments as e ';
        $sql .= '     on e.id = a.department_id_to ';
        $sql .= '   left join mst_facilities as f ';
        $sql .= '     on f.id = e.mst_facility_id ';
        $sql .= '   left join mst_departments as e2 ';
        $sql .= '     on e2.id = a.department_id_from ';
        $sql .= '   left join mst_facilities as f2 ';
        $sql .= '     on f2.id = e2.mst_facility_id ';
        $sql .= '   left join mst_departments as e3 ';
        $sql .= '     on e3.id = a.department_id_to2 ';
        $sql .= '   left join mst_facilities as f3 ';
        $sql .= '     on f3.id = e3.mst_facility_id ';
        $sql .= '   left join mst_transaction_configs as g ';
        $sql .= '     on g.mst_item_unit_id = b.id ';
        $sql .= '     and g.partner_facility_id = ' . $supplier_id;
        $sql .= '     and g.is_deleted = false  ';
        $sql .= "     and g.start_date <= '" . $this->request->data['TrnOrder']['work_date'] . "'";
        $sql .= "     and g.end_date > '" . $this->request->data['TrnOrder']['work_date'] . "'";
        $sql .= '   left join trn_stocks as h  ';
        $sql .= '     on h.mst_department_id = a.department_id_from';
        $sql .= '     and h.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '   left join ( ';
        $sql .= '     select ';
        $sql .= '         max(xa.work_date)       as work_date ';
        $sql .= '       , xa.mst_item_unit_id  ';
        $sql .= '     from ';
        $sql .= '       trn_orders as xa  ';
        $sql .= '       left join mst_departments as xb  ';
        $sql .= '         on xb.id = xa.department_id_from  ';
        $sql .= '       left join mst_departments as xc  ';
        $sql .= '         on xc.id = xa.department_id_to  ';
        $sql .= '     where ';
        $sql .= '       xa.is_deleted = false  ';
        $sql .= '       and xa.trn_order_header_id is not null  ';
        $sql .= '       and xa.order_type = ' . $order_type;
        $sql .= '       and xb.mst_facility_id = ' . $supplier_id;
        $sql .= '       and xc.mst_facility_id =  ' .$order_facility_id;
        $sql .= '     group by ';
        $sql .= '       xa.mst_item_unit_id ';
        $sql .= '   ) as x  ';
        $sql .= '     on x.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '   left join (  ';
        $sql .= '     select ';
        $sql .= '         mst_item_unit_id ';
        $sql .= '       , sum(remain_count)          as remain_count  ';
        $sql .= '     from ';
        $sql .= '       trn_orders as a  ';
        $sql .= '       left join mst_item_units as b  ';
        $sql .= '         on b.id = a.mst_item_unit_id  ';
        $sql .= '       left join mst_facility_items as c  ';
        $sql .= '         on c.id = b.mst_facility_item_id  ';
        $sql .= '     where ';
        $sql .= '       a.is_deleted = false  ';
        $sql .= '       and a.remain_count > 0  ';
        $sql .= '       and a.trn_order_header_id is not null  ';
        $sql .= '       and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     group by ';
        $sql .= '       a.mst_item_unit_id ';
        $sql .= '   ) y  ';
        $sql .= '     on y.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= ' where ';
        $sql .= '   a.is_deleted = false  ';
        $sql .= '   and f.id = ' . $supplier_id;
        $sql .= '   and f2.id = ' . $order_facility_id;
        $sql .= '   and e2.id = ' . $order_department_id;
        $sql .= '   and a.order_type = '. $order_type;
        if(!is_null($where)){
            $sql .= $where;
        }
        
        $sql .= ' order by ';
        $sql .= '   f.facility_name ';
        $sql .= '   , a.order_type ';
        $sql .= '   , c.internal_code';
        $sql .= '   , b.per_unit ';
        $sql .= '   , e2.department_name';
        
        $ret = $this->TrnOrder->query($sql);
        return $ret;
    }
    
    /**
     * 発注メールを送信する。
     */
    private function sendOrderMail($order_header_ids_per_supplier) {
        foreach ($order_header_ids_per_supplier as $supplier_id => $info) {
            $supplier_email = $info['order_header']['supplier_email'];
            if (empty($supplier_email)) continue;
            
            $supplier_name = $info['order_header']['supplier_name'];
            $order_header_ids = $info['order_header_ids'];
            $content = $this->PdfReport->getPdfDataOfOrder($order_header_ids);
            
            $mailParams = array(
                    MailComponent::PARAM_CONFIG       => 'and',
                    MailComponent::PARAM_TO           => array($supplier_email,),
                    MailComponent::PARAM_ATTACHMENTS  => array('order.pdf' => array('data' => $content),),
                    MailComponent::PARAM_VIEW_VARS    => array('supplier_name' => $supplier_name),
            );
            if ($this->Mail->sendOrderMail($mailParams)) {
                // 成功時の処理
            } else {
                // 失敗時の処理
            }
        }
    }

    //発注履歴Excel出力
    function export_excel(){
        $this->request->data['TrnOrderHeader']['id']=array();
        //検索
        $where = $this->_createSearchWhere();
        $where  .= '  AND A.order_type <> '.Configure::read('OrderTypes.direct');

        $sql = $this->_getOrderCSV($where);
        $ret = $this->TrnOrder->query($sql);

        //テンプレートファイルフルパス
        $template = realpath(TMP);
        $template .= DS . 'excel' . DS;
        $template_path = $template . "template10.xls";
        
        // Excelオブジェクト作成
        $PHPExcel = $this->createExcelObj($template_path);

        //表紙への入力
        //シートの設定
        $PHPExcel->setActiveSheetIndex(0);  //一番左のシート
        $sheet = $PHPExcel->getActiveSheet();
        // Excel書込処理
        foreach($ret as $num => $r){
            $cell =  (string)($num+3);
            
            $sheet->setCellValue("A" . $cell, $num + 1);              // No
            $sheet->setCellValueExplicit("B" . $cell, $r[0]['発注番号'] , PHPExcel_Cell_DataType::TYPE_STRING);  // 発注番号
            $sheet->setCellValue("C" . $cell, $r[0]['発注日']);       // 発注日
            $sheet->setCellValue("D" . $cell, $r[0]['仕入先名']);     // 仕入先名
            $sheet->setCellValue("E" . $cell, $r[0]['部署名']);       // 施設／部署名
            $sheet->setCellValue("F" . $cell, $r[0]['商品ID']);       // 商品ID
            $sheet->setCellValue("G" . $cell, $r[0]['商品名']);       // 商品名
            $sheet->setCellValue("H" . $cell, $r[0]['規格']);         // 規格
            $sheet->setCellValue("I" . $cell, $r[0]['製品番号']);   // 製品番号
            $sheet->setCellValue("J" . $cell, $r[0]['販売元名']);     // 販売元名
            $sheet->setCellValue("K" . $cell, $r[0]['包装単位']);     // 包装単位
            $sheet->setCellValue("L" . $cell, $r[0]['仕入価格']);     // 仕入価格
            $sheet->setCellValue("M" . $cell, $r[0]['発注数']);       // 発注数
            $sheet->setCellValue("N" . $cell, $r[0]['残数']);         // 残数
            $sheet->setCellValue("O" . $cell, $r[0]['更新者']);       // 更新者
            $sheet->setCellValue("P" . $cell, $r[0]['備考']);         // 備考
        }
        
        //保存ファイル名
        $filename = "発注履歴_"  . date('YmdHis')  . ".xls";

        $filename = mb_convert_encoding($filename, 'sjis', 'utf-8');
        
        // 保存ファイルパス
        $upload = realpath( TMP );
        $upload .= DS . '..' . DS . 'webroot' . DS . 'rfp' . DS;
        $path = $upload . $filename;
        
        $objWriter = new PHPExcel_Writer_Excel5( $PHPExcel );   //2003形式で保存
        $objWriter->save( $path );
        // 保存したExcelをダウンロード
        $result = file_get_contents(WWW_ROOT . DS . 'rfp' . DS . $filename);
        
        Configure::write('debug',0);
        header("Content-disposition: attachment; filename=" . $filename);
        header("Content-type: application/octet-stream; name=" . $filename);
        print($result);
        unlink(WWW_ROOT . DS . 'rfp' . DS . $filename);
        exit;
    }
}

