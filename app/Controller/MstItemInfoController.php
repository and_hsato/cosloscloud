<?php
/**
 * MstItemInfoController
 * @version 1.0.0
 * @since 2015/06/26
 */

class MstItemInfoController extends AppController {
    var $name = 'MstItemInfo';

    /**
     *
     * @var array $uses
     */
    var $uses = array(
        'MstTransactionConfig',
        'MstTransactionMain',
        'MstItemUnit',
        'MstUnitName',
        'MstFacility',
        'MstFacilityItem',
        'MstUserBelonging',
        'MstDealer',
        'MstFacilityRelation',
        'MstUser',
        'MstSalesConfig',
        'MstInsuranceClaim',
        'MstInsuranceClaimDepartment'
        );

    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;

    /**
     * @var MstTransactionConfigs
     */
    var $MstTransactionConfigs;


    //ログインなしでの印刷を許可
    public function beforeFilter(){
        parent::beforeFilter();
    }

    public function index(){
        $this->redirect('search');
    }
    
    /**
     * search
     */
    function search() {
        $this->setRoleFunction(138); //商品情報設定
        $result = array();

        //検索ボタン押下
        if(isset($this->request->data['MstItemInfo']['is_search'])){
            App::import('Sanitize');
            $where = "";
            //ユーザ入力値による検索条件の作成------------------------------------------
            //仕入先(完全一致)
            if((isset($this->request->data['MstFacilityItem']['supplierCode'])) && ($this->request->data['MstFacilityItem']['supplierCode'] != "")){
                $where .= " and d.facility_code = '" . $this->request->data['MstFacilityItem']['supplierCode'] . "'";
            }
            
            //商品ID(前方一致)
            if((isset($this->request->data['MstFacilityItem']['search_internal_code'])) && ($this->request->data['MstFacilityItem']['search_internal_code'] != "")){
                $where .= " and a.internal_code  LIKE '" . $this->request->data['MstFacilityItem']['search_internal_code'] . "%'";
            }
            
            //商品名(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_item_name'])) && ($this->request->data['MstFacilityItem']['search_item_name'] != "")){
                $where .= " and a.item_name LIKE '%". $this->request->data['MstFacilityItem']['search_item_name']."%'";
            }

            //規格(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_standard'])) && ($this->request->data['MstFacilityItem']['search_standard'] != "")){
                $where .= " and a.standard LIKE '%" . $this->request->data['MstFacilityItem']['search_standard'] . "%'";
            }
            
            //製品番号(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_item_code'])) && ($this->request->data['MstFacilityItem']['search_item_code'] != "")){
                $where .= " and a.item_code  LIKE '%" . $this->request->data['MstFacilityItem']['search_item_code'] ."%'";
            }
            //販売元(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_dealer_name'])) && ($this->request->data['MstFacilityItem']['search_dealer_name'] != "")){
                $where .=  " and b.dealer_name LIKE '%" . $this->request->data['MstFacilityItem']['search_dealer_name'] . "%'";
            }

            //JANコード(前方一致)
            if((isset($this->request->data['MstFacilityItem']['search_jan_code'])) && ($this->request->data['MstFacilityItem']['search_jan_code'] != "")){
                $where .= " and a.jan_code LIKE '" . $this->request->data['MstFacilityItem']['search_jan_code'] ."%'";
            }

            //検索----------------------------------------------------------------------
            $result = $this->getItems($where);
        }

        $result = AppController::outputFilter($result);
        //************************************************************************
        //Viewへ遷移（検索の場合）
        $this->set('result',$result);
    }

    /**
     * Mod 仕入設定情報編集
     */
    function mod() {
        $this->Session->write('TransactionConfig.readTime',date('Y-m-d H:i:s'));
        //選択した商品情報を取得
        $sql  = ' select ';
        $sql .= '       a.id                                    as "MstFacilityItem__id" ';
        $sql .= '     , a.internal_code                         as "MstFacilityItem__internal_code" ';
        $sql .= '     , a.item_name                             as "MstFacilityItem__item_name" ';
        $sql .= '     , a.item_code                             as "MstFacilityItem__item_code" ';
        $sql .= '     , a.standard                              as "MstFacilityItem__standard" ';
        $sql .= '     , a.jan_code                              as "MstFacilityItem__jan_code" ';
        $sql .= '     , b.dealer_name                           as "MstFacilityItem__dealer_name" ';
        $sql .= '     , a.refund_price                          as "MstFacilityItem__refund_price" ';
        $sql .= '     , d.insurance_claim_name_s                as "MstFacilityItem__insurance_claim_name_s"  ';
        $sql .= '     , e.const_nm                              as "MstFacilityItem__insurance_claim_department_name"';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_dealers as b  ';
        $sql .= '       on b.id = a.mst_dealer_id  ';
        $sql .= '     left join mst_insurance_claims as d  ';
        $sql .= '       on d.insurance_claim_code = a.insurance_claim_code  ';
        $sql .= '     left join mst_consts as e ';
        $sql .= "       on e.const_cd = a.insurance_claim_type ::varchar ";
        $sql .= "      and e.const_group_cd = '" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->request->data['MstFacilityItem']['id'];

        $tmp = $this->MstFacilityItem->query($sql);
        $this->request->data = $tmp[0];
        
        //仕入設定の取得
        $TransactionConfig = $this->getTransactionConfig($this->request->data['MstFacilityItem']['id']);
        
        //売上設定の取得
        $SalesConfig = $this->getSalesConfig($this->request->data['MstFacilityItem']['id']);
        
        $this->set('SalesConfig',$SalesConfig);
        $this->set('TransactionConfig',$TransactionConfig);
    }
    
    //
    //検索
    //
    private function getItems($where){
        $limit = $this->_getLimitCount();

        $sql  = ' select ';
        $sql .= '       a.id                          as "MstFacilityItem__id" ';
        $sql .= '     , a.item_code                   as "MstFacilityItem__item_code" ';
        $sql .= '     , a.internal_code               as "MstFacilityItem__internal_code" ';
        $sql .= '     , a.item_name                   as "MstFacilityItem__item_name" ';
        $sql .= '     , a.standard                    as "MstFacilityItem__standard" ';
        $sql .= '     , b.dealer_name                 as "MstFacilityItem__dealer_name" ';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_dealers as b  ';
        $sql .= '       on a.mst_dealer_id = b.id  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.item_type = ' . Configure::read('Items.item_types.normalitem');
        $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and a.start_date <= now()::date ';
        $sql .= '     and ( a.end_date > now()::date or a.end_date is null )';
        $sql .= $where;

        $sql .= '   order by ';
        $sql .= '     a.internal_code asc  ';
        
        $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));
        $sql .= " limit {$limit}";
        
        return $this->MstFacilityItem->query($sql);
    }

    /*
     * 施設採用品IDから仕入設定を取得する
     */
    private function getTransactionConfig($id){
        $sql  = ' select ';
        $sql .= '       a.id                                  as "MstTransactionConfig__id" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when d.per_unit = 1  ';
        $sql .= '         then d1.unit_name  ';
        $sql .= "         else d1.unit_name || '(' || d.per_unit || d2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                       as "MstTransactionConfig__unit_name" ';
        $sql .= '     , to_char(a.start_date, \'YYYY/mm/dd\') as "MstTransactionConfig__start_date" ';
        $sql .= '     , to_char(a.end_date, \'YYYY/mm/dd\')   as "MstTransactionConfig__end_date" ';
        $sql .= '     , b.facility_name                       as "MstTransactionConfig__partner_facility_name" ';
        $sql .= '     , c.facility_name                       as "MstTransactionConfig__partner_facility_name2" ';
        $sql .= "     , ( case when a.is_deleted = false then '○' else '' end ) ";
        $sql .= '                                             as "MstTransactionConfig__is_deleted" ';
        $sql .= '   from ';
        $sql .= '     mst_transaction_configs as a  ';
        $sql .= '   left join mst_facilities as b ';
        $sql .= '     on b.id = a.partner_facility_id ';
        $sql .= '   left join mst_facilities as c ';
        $sql .= '     on c.id = a.partner_facility_id2 ';
        $sql .= '   left join mst_item_units as d ';
        $sql .= '     on d.id = a.mst_item_unit_id ';
        $sql .= '   left join mst_unit_names as d1 ';
        $sql .= '     on d1.id = d.mst_unit_name_id ';
        $sql .= '   left join mst_unit_names as d2 ';
        $sql .= '     on d2.id = d.per_unit_name_id ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_item_id = ' . $id;
        $sql .= '   order by a.start_date ';

        return $this->MstTransactionConfig->query($sql);
    }

    /*
     * 施設採用品IDから売上設定を取得する
     */
    private function getSalesConfig($id){
        $sql  = ' select ';
        $sql .= '       e.facility_name                       as "MstSalesConfig__facility_name" ';
        $sql .= '     , to_char(a.start_date, \'YYYY/mm/dd\') as "MstSalesConfig__start_date" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                       as "MstSalesConfig__unit_name" ';
        $sql .= '     , a.sales_price                         as "MstSalesConfig__sales_price"  ';
        $sql .= '     , ( coalesce(g.unit_price,0) * b.per_unit ) ';
        $sql .= '                                             as "MstSalesConfig__unit_price"';
        $sql .= '     , ( coalesce(g.unit_price,0) * b.per_unit ) - a.sales_price ';
        $sql .= '                                             as "MstSalesConfig__cut_price"';
        $sql .= '     , ( case when coalesce(g.unit_price,0) = 0 then 0 ' ;
        $sql .= '              else ( ( coalesce(g.unit_price,0) * b.per_unit ) - a.sales_price ) / ( coalesce(g.unit_price,0) * b.per_unit ) * 100 '; 
        $sql .= '         end )                               as "MstSalesConfig__cut_rate"';
        $sql .= '   from ';
        $sql .= '     mst_sales_configs as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on a.mst_item_unit_id = b.id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on b.mst_unit_name_id = c.id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on b.per_unit_name_id = d.id  ';
        $sql .= '     left join mst_facilities as e  ';
        $sql .= '       on e.id = a.partner_facility_id  ';
        $sql .= '     inner join mst_user_belongings as f ';
        $sql .= '       on f.mst_user_id = ' . $this->Session->read('Auth.MstUser.id') ;
        $sql .= '      and f.mst_facility_id = e.id ' ;
        $sql .= '      and f.is_deleted = false' ;
        $sql .= '     left join mst_facility_items as g ';
        $sql .= '       on g.id = a.mst_facility_item_id ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_item_id = ' . $id ;
        $sql .= '     and a.is_deleted = false ';
        $sql .= '     and a.start_date < now()  ';
        $sql .= '     and a.end_date > now() ';

        return $this->MstSalesConfig->query($sql);
    }
}
