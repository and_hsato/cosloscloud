<?php
/**
 * MstSystemUsersController
 *
 * @version 1.0.0
 * @since 2010/07/08
 */

class MstSystemUsersController extends AppController {
    var $name = 'MstSystemUsers';
    
    /**
     *
     * @var array $uses
     */
    var $uses = array('MstUser','MstDepartment','MstFacility','MstRole','MstUserBelonging');

    /**
     * @var array $components
     */
    var $components = array('CsvWriteUtils');
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;
    
    /**
     * @var MstUser
     */
    //var $MstUser;

    function beforeFilter() {
        parent::beforeFilter();
        //暗号化種別をMD5にする。
        if(Configure::read('Password.Security') == 1){
            Security::setHash('md5');
        }
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    
    /**
     * userlist
     *
     * ユーザ一覧
     */
    function userlist() {
        App::import('Sanitize');
        $this->setRoleFunction(88); //ユーザー
        $User_List = array();
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        //プルダウン作成用データ取得--------------------------------------------------
        $this->set('Role_List',$this->setRole());
        
        //施設プルダウン用データ取得
        $this->set('facility_enabled',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.center')),
                       true
                       )
                   );
        
        //初期表示以外の場合のみデータを取得する
        if(isset($this->request->data['MstUser']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            
            //ユーザ入力値による検索条件の作成------------------------------------
            $where = '';
            if(Configure::read('Password.Security') == 1 ){
                //暗号化しない場合、全ユーザが無制限ユーザなので
                $where .= ' and  a.is_indefinite = false ';
            }
            
            //ログインID(LIKE検索)
            if(($this->request->data['MstUser']['search_login_id'] != "")){
                $where .= " and a.login_id LIKE '%".$this->request->data['MstUser']['search_login_id']."%'";
            }
            
            //ユーザ名(LIKE検索)
            if(($this->request->data['MstUser']['search_user_name'] != "")){
                $where .= " and a.user_name LIKE '%" .$this->request->data['MstUser']['search_user_name'] ."%'";
            }
            
            //施設(完全一致・施設コード)
            if($this->request->data['MstUser']['search_facility_cd'] != ""){
                $where .= " and b.facility_code = '" . $this->request->data['MstUser']['search_facility_cd'] . "'";
            }
            //ロール(完全一致)
            if($this->request->data['MstUser']['search_role_id'] != ""){
                $where .= ' and a.mst_role_id = ' . $this->request->data['MstUser']['search_role_id'];
            }
            
            //削除済み表示
            if(!isset($this->request->data['MstUser']['search_is_deleted'])){
                $where .= ' and a.is_deleted = false ';
            }

            //検索条件の作成終了-------------------------------------------------
            
            //ユーザリスト取得
            $User_List = $this->getUserList($where , $this->request->data['limit'] );
            $this->request->data = $data;
        }
        $this->set('User_List',$User_List);
    }
    
    
    /**
     * Add ユーザ新規登録
     */
    function add() {
        //プルダウン作成用データ取得------------------------------------------------
        $sql  = ' select ';
        $sql .= '       a.id             as "MstFacility__id"';
        $sql .= '     , a.facility_code  as "MstFacility__facility_code"';
        $sql .= '     , a.facility_name  as "MstFacility__facility_name"';
        $sql .= '     , a.facility_type  as "MstFacility__facility_type"';
        $sql .= '     , ( case ';
        foreach(Configure::read('FacilityType.typelist') as $k => $v){
            $sql .= ' when a.facility_type = ' . $k . " then '" . $v . "'" ;
        }
        $sql .= '       end )            as "MstFacility__facility_type_name"';
        $sql .= '   from ';
        $sql .= '     mst_facilities as a  ';
        $sql .= '   where ';
        $sql .= '     a.facility_type in ( ' . Configure::read('FacilityType.center') . ',' . Configure::read('FacilityType.hospital') . ')';
        $sql .= '     and a.is_deleted = false  ';
        $sql .= '   order by ';
        $sql .= '     a.id ';
        $ret = $this->MstFacility->query($sql);
        $this->set('Facility_List',$ret);
        
        $this->set('Role_List',$this->setRole());
        
        $this->set('center_list',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.center')),
                       true
                       )
                   );
        
        $this->set('hospital_list',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.hospital')),
                       true
                       )
                   );
        
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }
    
    /**
     * Mod ユーザ情報編集
     */
    function mod() {
        //更新チェック用にmod画面に入った瞬間の時間を保持
        $this->Session->write('Usermod.readTime',date('Y-m-d H:i:s'));
        
        //プルダウン作成用データ取得------------------------------------------------
        $this->set('Role_List',$this->setRole());
        
        $this->set('center_list',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.center')),
                       true
                       )
                   );
        
        $this->set('hospital_list',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.hospital')),
                       true
                       )
                   );
        
        $department_list = array();
        
        //userlistから引き継いできたidでユーザ情報を取得

        $sql  = ' select ';
        $sql .= '       a.login_id              as "MstUser__login_id" ';
        $sql .= '     , a.id                    as "MstUser__id" ';
        $sql .= '     , a.user_name             as "MstUser__user_name" ';
        $sql .= '     , a.password              as "MstUser__password" ';
        $sql .= '     , a.mst_facility_id       as "MstUser__mst_facility_id" ';
        $sql .= '     , a.mst_role_id           as "MstUser__mst_role_id" ';
        $sql .= '     , a.is_deleted            as "MstUser__is_deleted" ';
        $sql .= '     , a.is_temp_issue         as "MstUser__is_temp_issue" ';
        $sql .= '     , a.is_indefinite         as "MstUser__is_indefinite" ';
        $sql .= '     , b.role_name             as "MstRole__role_name" ';
        $sql .= '     , b.id                    as "MstRole__id" ';
        $sql .= '     , c.facility_code         as "MstFacility__centerText" ';
        $sql .= '     , c.facility_code         as "MstFacility__centerCode" ';
        $sql .= '     , c.facility_name         as "MstFacility__centerName" ';
        $sql .= '     , e.facility_code         as "MstFacility__facilityCode" ';
        $sql .= '     , e.facility_code         as "MstFacility__facilityText" ';
        $sql .= '     , e.facility_name         as "MstFacility__facilityName" ';
        $sql .= '     , d.department_code       as "MstDepartment__departmentCode" ';
        $sql .= '     , d.department_code       as "MstDepartment__departmentText" ';
        $sql .= '     , d.department_name       as "MstDepartment__departmentName"  ';
        $sql .= '   from ';
        $sql .= '     mst_users as a  ';
        $sql .= '     left join mst_roles as b  ';
        $sql .= '       on b.id = a.mst_role_id  ';
        $sql .= '     left join mst_facilities as c  ';
        $sql .= '       on c.id = a.mst_facility_id  ';
        $sql .= '     left join mst_departments as d  ';
        $sql .= '       on d.id = a.mst_department_id  ';
        $sql .= '     left join mst_facilities as e  ';
        $sql .= '       on e.id = d.mst_facility_id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->request->data['MstUser']['id'];
        $ret = $this->MstUser->query($sql);
        $this->request->data = $ret[0];
        
        if(!empty($ret[0]['MstFacility']['facilityCode'])){
            $department_list = $this->getDepartmentsList($ret[0]['MstFacility']['facilityCode']);
            
        }
        $this->set('department_list' , $department_list);
        
        $sql  = ' select ';
        $sql .= '       a.id             as "MstFacility__id"';
        $sql .= '     , a.facility_code  as "MstFacility__facility_code"';
        $sql .= '     , a.facility_name  as "MstFacility__facility_name"';
        $sql .= '     , b.id             as "MstFacility__check"';
        $sql .= '     , a.facility_type  as "MstFacility__facility_type"';
        $sql .= '     , ( case ';
        foreach(Configure::read('FacilityType.typelist') as $k => $v){
            $sql .= ' when a.facility_type = ' . $k . " then '" . $v . "'" ;
        }
        $sql .= '       end )            as "MstFacility__facility_type_name"';
        $sql .= '   from ';
        $sql .= '     mst_facilities as a  ';
        $sql .= '     left join mst_user_belongings as b  ';
        $sql .= '       on b.mst_facility_id = a.id  ';
        $sql .= '       and b.mst_user_id = ' . $this->request->data['MstUser']['id'];
        $sql .= '       and b.is_deleted = false';
        $sql .= '   where ';
        $sql .= '     a.facility_type in ( ' . Configure::read('FacilityType.center') . ',' . Configure::read('FacilityType.hospital') . ')';
        $sql .= '     and a.is_deleted = false  ';
        $sql .= '   order by ';
        $sql .= '     a.id ';
        $ret = $this->MstFacility->query($sql);
        
        $this->set('Facility_List' , $ret);
    }
    
    /**
     * result
     *
     * ユーザ情報更新（新規登録・更新）
     */
    function result() {
        //施設コードから、施設IDを取得
        $this->request->data['MstUser']['mst_facility_id'] = $this->getFacilityId($this->request->data['MstFacility']['centerCode'] , Configure::read('FacilityType.center')) ;
        // カタログ用部署
        $mst_department_id = null;
        if(!empty($this->request->data['MstDepartment']['departmentCode'])){
            $hospital_id = $this->getFacilityId($this->request->data['MstFacility']['facilityCode'] , Configure::read('FacilityType.hospital')) ;
            $mst_department_id = $this->getDepartmentId($hospital_id,
                                                        array(Configure::read('DepartmentType.hospital')) ,
                                                        $this->request->data['MstDepartment']['departmentCode']);
        }
        //ロールIDからロール名称を取得
        $this->request->data['MstRole']['role_name'] = $this->getRoleName($this->request->data['MstUser']['mst_role_id']);
        
        $now = date('Y/m/d H:i:s.u');
        //トランザクションを開始
        $this->MstUser->begin();
        //行ロック（更新時のみ）
        if(isset( $this->request->data['MstUser']['id']) && !empty( $this->request->data['MstUser']['id'])){
            $this->MstUser->query( ' select * from mst_users as a where a.id = ' . $this->request->data['MstUser']['id'] . ' for update ');
        }
        
        $user_data = array();
        //データ整形
        if(isset($this->request->data['MstUser']['id'])){
            //更新の場合
            $user_data['MstUser']['id'] = $this->request->data['MstUser']['id'];
        }else{
            //新規の場合
            $user_data['MstUser']['creater']        = $this->Session->read('Auth.MstUser.id');
            $user_data['MstUser']['created']        = $now;
            $user_data['MstUser']['effective_date'] = $this->getNextEffectiveDate(date('Y/m/d'));
            $user_data['MstUser']['user_type']      = Configure::read('UserType.hospital');
        }
        //仮パスワード発行の場合
        if(isset($this->request->data['MstUser']['is_temp_issue']) && $this->request->data['MstUser']['is_update']==1){
            if(Configure::read('Password.Security') == 1 ){
                $user_data['MstUser']['password']          = $this->request->data['MstUser']['password'];
            }else{
                $user_data['MstUser']['password']          = $_POST['data']['MstUser']['password'];
            }
            $user_data['MstUser']['is_temp_issue']     = true;
            $user_data['MstUser']['password_modified'] = $now;
        }
        
        if(Configure::read('Password.Security') == 0 ){
            //暗号化しない場合、全ユーザが無制限ユーザ
            $user_data['MstUser']['is_indefinite']     = true;
            $user_data['MstUser']['password']          = $_POST['data']['MstUser']['password'];
            $user_data['MstUser']['is_temp_issue']     = false;
            $user_data['MstUser']['password_modified'] = $now;
        }
        
        $user_data['MstUser']['login_id']          = $this->request->data['MstUser']['login_id'];
        $user_data['MstUser']['user_name']         = $this->request->data['MstUser']['user_name'];
        $user_data['MstUser']['mst_facility_id']   = $this->request->data['MstUser']['mst_facility_id'];
        $user_data['MstUser']['mst_department_id'] = $mst_department_id;
        $user_data['MstUser']['mst_role_id']       = $this->request->data['MstUser']['mst_role_id'];
        $user_data['MstUser']['is_deleted']        = (isset($this->request->data['MstUser']['is_deleted'])?true:false);
        $user_data['MstUser']['modifier']          = $this->Session->read('Auth.MstUser.id');
        $user_data['MstUser']['modified']          = $now;
        if(!$this->MstUser->save($user_data)){
            //ロールバック
            $this->MstUser->rollback();
            //エラーメッセージ
            $this->Session->setFlash('ユーザ情報の登録に失敗しました。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('userlist');
        }
        
        //ユーザ操作管理
        if(isset($this->request->data['MstUser']['id'])){
            //更新の場合
            //いったん全部関係を削除
            $this->MstUserBelonging->query(' update mst_user_belongings set is_deleted = true where mst_user_id = ' . $this->request->data['MstUser']['id'] );
            
            //チェックの入った施設を更新
            foreach($this->request->data['MstUser']['Usable_Facility'] as $key => $id ){
                //センターIDと、POSTされたIDとで関連付けテーブルを検索
                $belonging_id = $this->getBelongingId($this->request->data['MstUser']['id'] , $id);
                if($belonging_id){
                    //該当組み合わせアリ => 更新処理
                    $this->MstUserBelonging->query('update mst_user_belongings set is_deleted = false where id = ' . $belonging_id);
                }else{
                    //該当組み合わせナシ => 追加処理
                    $belonging_data = array();
                    
                    $belonging_data['MstUserBelonging']['mst_user_id']     = $this->request->data['MstUser']['id'];
                    $belonging_data['MstUserBelonging']['mst_facility_id'] = $id;
                    $belonging_data['MstUserBelonging']['is_deleted']      = false;
                    $belonging_data['MstUserBelonging']['creater']         = $this->Session->read('Auth.MstUser.id');
                    $belonging_data['MstUserBelonging']['created']         = $now;
                    $belonging_data['MstUserBelonging']['modifier']        = $this->Session->read('Auth.MstUser.id');
                    $belonging_data['MstUserBelonging']['modified']        = $now;
                    $this->MstUserBelonging->create();
                    if(!$this->MstUserBelonging->save($belonging_data)){
                        //ロールバック
                        $this->MstUser->rollback();
                        //エラーメッセージ
                        $this->Session->setFlash('ユーザ操作情報の登録に失敗しました。', 'growl', array('type'=>'error') );
                        //リダイレクト
                        $this->redirect('userlist');
                    }
                }
            }
        }else{
            //新規の場合
            $this->request->data['MstUser']['id'] = $this->MstUser->getLastInsertId();
            $belonging_data = array();
            foreach($this->request->data['MstUser']['Usable_Facility'] as $key => $id ){
                $belonging_data[] = array(
                    'MstUserBelonging' => array(
                        'mst_user_id'     => $this->request->data['MstUser']['id'],
                        'mst_facility_id' => $id,
                        'is_deleted'      => false,
                        'creater'         => $this->Session->read('Auth.MstUser.id'),
                        'created'         => $now,
                        'modifier'        => $this->Session->read('Auth.MstUser.id'),
                        'modified'        => $now
                        )
                    );
            }
            if(!$this->MstUserBelonging->saveAll($belonging_data , array('validates' => true,'atomic' => false))){
                //ロールバック
                $this->MstUser->rollback();
                //エラーメッセージ
                $this->Session->setFlash('ユーザ操作情報の登録に失敗しました。', 'growl', array('type'=>'error') );
                //リダイレクト
                $this->redirect('userlist');
            }
        }
        //コミット
        $this->MstUser->commit();
        //ユーザ操作施設一覧を取得
        $this->set('Facility_List' , $this->getUseFacilityList($this->request->data['MstUser']['id']));
    }
    
    
    /**
     * ロール選択用プルダウン作成用データ取得
     */
    function setRole() {
        //ロール名の取得
        $params = array (
            'fields'    => array('MstRole.id','MstRole.role_name'),
            'order'     => array('MstRole.id'),
            'recursive' => 1
            );
        
        $params['conditions'] = array('MstRole.is_deleted' => FALSE);
        $Role_List = Set::Combine($this->MstRole->find('all',$params),
                                  "{n}.MstRole.id",
                                  "{n}.MstRole.role_name"
                                  );
        return $Role_List;
    }
    
    /**
     * ユーザIDと、施設IDで、ユーザ操作管理のIDを返す
     */
    private function getBelongingId($user_id , $facility_id){
        $params = array (
            'conditions' => array('MstUserBelonging.mst_user_id'     => $user_id,
                                  'MstUserBelonging.mst_facility_id' => $facility_id,
                                  ),
            'fields'     => array('MstUserBelonging.id'),
            'recursive'  => -1
            );
        
        $tmp = $this->MstUserBelonging->find('first', $params);
        return (isset($tmp['MstUserBelonging']['id'])?$tmp['MstUserBelonging']['id']:false);
    }

    
    /* ロールIDからロールの名称を取得 */
    private function getRoleName($id){
        $params = array (
            'conditions' => array('MstRole.id' => $id),
            'fields'     => array('MstRole.role_name'),
            'recursive'  => -1
            );
        
        $tmp = $this->MstRole->find('first', $params);
        return (isset($tmp['MstRole']['role_name'])?$tmp['MstRole']['role_name']:false);
    }
    
    /* ユーザIDから操作可能な施設名の一覧を取得 */
    private function getUseFacilityList($user_id){

        $sql  = ' select ';
        $sql .= '       b.facility_name as "MstFacility__facility_name"';
        $sql .= '     , ( case ';
        foreach(Configure::read('FacilityType.typelist') as $k => $v){
            $sql .= ' when b.facility_type = ' . $k . " then '" . $v . "'" ;
        }
        $sql .= '       end )           as "MstFacility__facility_type_name"';
        $sql .= '   from ';
        $sql .= '     mst_user_belongings as a  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.mst_facility_id  ';
        $sql .= '   where ';
        $sql .= '     a.mst_user_id = ' . $user_id;
        $sql .= '     and a.is_deleted = false  ';
        $sql .= '     and b.is_deleted = false  ';
        $sql .= '   order by ';
        $sql .= '     b.id ';
        
        return $this->MstUserBelonging->query($sql); 
    }

    /**
     * passwordlist
     *
     * パスワード一覧
     */
    function passwordlist() {
        App::import('Sanitize');
        $this->setRoleFunction(90); //パスワード一覧
        $User_List = array();
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        //プルダウン作成用データ取得--------------------------------------------------
        $this->set('Role_List',$this->setRole());
        
        //施設プルダウン用データ取得
        $this->set('facility_enabled',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.center')),
                       true
                       )
                   );
        
        //更新周期
        $Update_Cycle = '';
        $month = Configure::read('UserPassword.updateMonth');
        sort($month); //念のため、昇順に並べ直しておく
        foreach($month as $m){
            if($Update_Cycle == ''){
                $Update_Cycle .= date("m月d日", mktime(0, 0, 0, $m+1, 0, date('Y')));
            }else{
                $Update_Cycle .= ' / ' . date("m月d日", mktime(0, 0, 0, $m+1, 0, date('Y')));
            }
        }
        $this->set('Update_Cycle' , $Update_Cycle);
        
        //初期表示以外の場合のみデータを取得する
        if(isset($this->request->data['MstUser']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            //ユーザ情報の取得
            //ユーザ入力値による検索条件の作成------------------------------------
            $where = ' and  a.is_indefinite = false ';

            //ログインID(LIKE検索)
            if(($this->request->data['MstUser']['search_login_id'] != "")){
                $where .= " and a.login_id LIKE '%".$this->request->data['MstUser']['search_login_id']."%'";
            }
            
            //ユーザ名(LIKE検索)
            if(($this->request->data['MstUser']['search_user_name'] != "")){
                $where .= " and a.user_name LIKE '%" .$this->request->data['MstUser']['search_user_name'] ."%'";
            }
            
            //施設(完全一致・施設コード)
            if($this->request->data['MstUser']['search_facility_cd'] != ""){
                $where .= " and b.facility_code = '" . $this->request->data['MstUser']['search_facility_cd'] . "'";
            }
            //ロール(完全一致)
            if($this->request->data['MstUser']['search_role_id'] != ""){
                $where .= ' and a.mst_role_id = ' . $this->request->data['MstUser']['search_role_id'];
            }
            
            //削除済み表示
            if(!isset($this->request->data['MstUser']['search_is_deleted'])){
                $where .= ' and a.is_deleted = false ';
            }
            //更新済み表示
            if(!isset($this->request->data['MstUser']['search_is_update'])){
                $where .= ' and a.effective_date < now() ';
            }
            //仮パスワード表示
            if(!isset($this->request->data['MstUser']['search_is_temp'])){
                $where .= ' and a.is_temp_issue = false ';
            }
            //検索条件の作成終了-------------------------------------------------
            
            //ユーザリスト取得
            $User_List = $this->getUserList($where , $this->request->data['limit'] );
            $this->request->data = $data;
        }else{
            //初期でチェックボックスON
            $this->request->data['MstUser']['search_is_update'] = true;
            $this->request->data['MstUser']['search_is_temp'] = true;
        }
        $this->set('User_List',$User_List);
    }
    
    /**
     * ユーザリストを返す。
     */
    private function getUserList($where , $limit = null){
        $sql  = ' select ';
        $sql .= '       a.id              as "MstUser__id"';
        $sql .= '     , a.login_id        as "MstUser__login_id"';
        $sql .= '     , a.user_name       as "MstUser__user_name"';
        $sql .= '     , b.facility_name   as "MstFacility__facility_name"';
        $sql .= '     , c.role_name       as "MstRole__role_name"';
        $sql .= "     , ( case when a.is_temp_issue = true then '○' else '' end ) ";
        $sql .= '                         as "MstUser__is_temp_issue"';
        $sql .= "     , ( case when a.is_deleted = true then '○' else '' end ) ";
        $sql .= '                         as "MstUser__is_deleted"';
        $sql .= "     , to_char(a.password_modified , 'YYYY/mm/dd') ";
        $sql .= '                         as "MstUser__password_modified"';
        $sql .= "     , ( case when a.effective_date < now()::date then '' else '○' end ) " ;
        $sql .= '                         as "MstUser__update_check"';
        
        $sql .= '   from ';
        $sql .= '     mst_users as a  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.mst_facility_id  ';
        $sql .= '     left join mst_roles as c  ';
        $sql .= '       on c.id = a.mst_role_id  ';
        $sql .= '   where 1=1 ';
        $sql .= '     and a.user_type = ' . Configure::read('UserType.hospital');
        $sql .= $where ;
        $sql .= '   order by ';
        $sql .= '     b.id asc ';
        $sql .= '     , c.id asc ';
        $sql .= '     , a.login_id asc ';
        
        $this->set('max' , $this->getMaxCount($sql , 'MstUser'));
        if(!is_null($limit)){
            $sql .= '   limit ' . $limit;
        }
        return $this->MstUser->query($sql);
    }

    /**
     * CSV出力
     */
    public function export_csv(){
        App::import('Sanitize');
        
        //現在有効な病院一覧を取得
        $sql  = ' select ';
        $sql .= '       a.id             as "MstFacility__id"';
        $sql .= '     , a.facility_name  as "MstFacility__facility_name"';
        $sql .= '   from ';
        $sql .= '     mst_facilities as a  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '   order by ';
        $sql .= '     a.id ';
        $hospital_list = $this->MstFacility->query($sql);
        
        //ユーザ入力値による検索条件の作成------------------------------------
        $where = ' and  a.is_indefinite = false ';
        
        //ログインID(LIKE検索)
        if(($this->request->data['MstUser']['search_login_id'] != "")){
            $where .= " and a.login_id LIKE '%".Sanitize::clean($this->request->data['MstUser']['search_login_id'])."%'";
        }
        
        //ユーザ名(LIKE検索)
        if(($this->request->data['MstUser']['search_user_name'] != "")){
            $where .= " and a.user_name LIKE '%" .Sanitize::clean($this->request->data['MstUser']['search_user_name']) ."%'";
        }
        
        //施設(完全一致・施設コード)
        if($this->request->data['MstUser']['search_facility_cd'] != ""){
            $where .= " and b.facility_code = '" . $this->request->data['MstUser']['search_facility_cd'] . "'";
        }
        //ロール(完全一致)
        if($this->request->data['MstUser']['search_role_id'] != ""){
            $where .= ' and a.mst_role_id = ' . $this->request->data['MstUser']['search_role_id'];
        }
        
        //削除済み表示
        if(!isset($this->request->data['MstUser']['search_is_deleted'])){
            $where .= ' and a.is_deleted = false ';
        }
        //更新済み表示
        if(!isset($this->request->data['MstUser']['search_is_update'])){
            $where .= ' and a.effective_date < now() ';
        }
        //仮パスワード表示
        if(!isset($this->request->data['MstUser']['search_is_temp'])){
            $where .= ' and a.is_temp_issue = false ';
        }
        
        //ユーザ一覧を取得
        $sql  = ' select ';
        $sql .= '       b.facility_name        as 事業会社 ';
        $sql .= '     , a.login_id             as "ユーザID" ';
        $sql .= '     , a.user_name            as ユーザ名 ';
        foreach($hospital_list as $h){
            $sql .= '     , ( case when c' .$h['MstFacility']['id'].  '.mst_user_id is not null then ' ;
            $sql .= '    a1.role_name_s end )  as "'. $h['MstFacility']['facility_name'] . '"';
        }
        $sql .= "     , '' as \"" . date('Y/m/d') . '"';
        $sql .= '   from ';
        $sql .= '     mst_users as a  ';
        $sql .= '     left join mst_roles as a1 ';
        $sql .= '       on a.mst_role_id = a1.id';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.mst_facility_id  ';
        foreach($hospital_list as $h){
            $sql .= '     left join (  ';
            $sql .= '       select ';
            $sql .= '             a.mst_user_id  ';
            $sql .= '         from ';
            $sql .= '           mst_user_belongings as a  ';
            $sql .= '         where ';
            $sql .= '           a.is_deleted = false  ';
            $sql .= '           and a.mst_facility_id = ' . $h['MstFacility']['id'];
            $sql .= '     ) as c'. $h['MstFacility']['id'];
            $sql .= '       on c'. $h['MstFacility']['id'] .'.mst_user_id = a.id  ';
        }
        $sql .= '   where 1=1 ';
        $sql .= '     and a.user_type = ' . Configure::read('UserType.hospital');
        $sql .= $where ;
        $sql .= '   order by ';
        $sql .= '     b.facility_name asc ';
        $sql .= '     , a.mst_role_id asc ';
        
        $this->db_export_csv($sql , "ユーザ一覧", 'passwordlist');
    }
}
