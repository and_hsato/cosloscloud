<?php
/**
 * OpeController
 *  オペ
 * @since 2015/03/11
 */
class OpeController extends AppController {
    var $name = 'Ope';
    
    /**
     * @var array $uses
     */
    var $uses = array('TrnSticker',
                      'TrnStickerRecord',
                      'MstFacilityItem',
                      'MstFacility',
                      'MstDepartment',
                      'MstOpeAnesthesia',
                      'MstOpeDepartment',
                      'MstOpePosition',
                      'MstOpePerson',
                      'MstOpeRoom',
                      'MstOpeMethodName',
                      'MstOpeMethodSet',
                      'MstKatheWard',
                      'MstOpeItemSetHeader',
                      'MstOpeItemSet',
                      'TrnMoveHeader',
                      'TrnShippingHeader',
                      'TrnReceivingHeader',
                      'TrnShipping',
                      'TrnSticker',
                      'TrnStock',
                      'TrnReceiving',
                      'TrnOpeHeader',
                      'TrnOpeItem',
                      'TrnOpeShipping',
                      'TrnOpeReturn',
                      'TrnConsumeHeader',
                      'TrnConsume',
                      'TrnClaim',
                      );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker','common');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Common','Barcode','Stickers','CsvWriteUtils','CsvReadUtils');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        //CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }
    }
    
    /**
     *
     */
    function index (){
        $this->ope_search();
    }

    
    /**
     * オペ倉庫移動 シール読み込み
     */
    function ope_move_read_sticker () {
        $this->setRoleFunction(131); //オペ倉庫移動
        $this->request->data['OpeMove']['work_date'] = date('Y/m/d');
    }

    /**
     * オペ倉庫移動確認
     */
    function ope_move_confirm () {
        // セッション処理
        $this->Session->write('OpeMove.readTime',date('Y-m-d H:i:s'));

        // ループ開始
        // シール番号から、商品情報を取得
        $result = array();
        $i = 0;
        foreach(explode(',',substr($this->request->data['codez'],0,-1)) as $barcbode){
            $r = $this->getStickerData($barcbode);
            if(empty($r)){
                //シールが存在しない場合ダミーで埋める
                $dummy = array(
                    'TrnSticker' => array(
                        'id'                  => null,
                        'internal_code'       => '',
                        'item_name'           => '該当なし',
                        'item_code'           => '' ,
                        'standard'            => '' ,
                        'unit_name'           => '',
                        'dealer_name'         => '',
                        'facility_sticker_no' => $barcbode,
                        'hospital_sticker_no' => '',
                        'is_check'            => false,
                        'alert'               => '無効なシールです',
                        'validated_date'      => '',
                        'lot_no'              => '',
                        'sales_price'         => '',
                        )
                    );
                $result[$i] = $dummy;
            }else{
                //シール情報を一覧に移す
                $result[$i] = $r[0];
            }
            $i++;
        }

        $this->set('result' , $result);

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('OpeMove.token' , $token);
        $this->request->data['OpeMove']['token'] = $token;
    }

    /**
     * オペ倉庫移動結果
     */
    function ope_move_result () {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['OpeMove']['token'] === $this->Session->read('OpeMove.token')) {
            $now = date('Y/m/d H:i:s');
            $this->Session->delete('OpeMove.token');
            $result = array();
            // トランザクション開始
            $this->TrnSticker->begin();
            // 行ロック
            $this->TrnSticker->query(' select * from trn_stickers as a where a.id in ( ' . join(',',$this->request->data['TrnSticker']['id']) .')  for update ' );

            //更新チェックを行う
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $this->request->data['TrnSticker']['id']). ')';
            $sql .= "     and a.modified > '" . $this->request->data['OpeMove']['time'] ."'";

            $ret = $this->TrnSticker->query($sql);

            if($ret[0][0]['count']> 0 ){
                $this->TrnSticker->rollback();
                $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('ope_move_read_sticker');
            }
            
            $center_department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') , array(Configure::read('DepartmentType.warehouse')));
            $ope_department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') , array(Configure::read('DepartmentType.opestock')));
            $facility_code = $this->getFacilityCode($this->Session->read('Auth.facility_id_selected'));
              
            $workDate = $this->request->data['OpeMove']['work_date'];
            $workNo = $this->setWorkNo4Header($workDate, 14);
            $recital = $this->request->data['OpeMove']['recital'];
            // 移動ヘッダ
            $TrnMoveHeader = array(
                'TrnMoveHeader'=> array(
                    'work_no'               => $workNo,
                    'work_date'             => $workDate,
                    'recital'               => $recital,
                    'move_status'           => Configure::read('MoveStatus.complete'),
                    'department_id_from'    => $center_department_id,
                    'department_id_to'      => $ope_department_id,
                    'detail_count'          => 0,
                    'detail_total_quantity' => 0,
                    'is_deleted'            => false,
                    'creater'               => $this->Session->read('Auth.MstUser.id'),
                    'created'               => $now,
                    'modifier'              => $this->Session->read('Auth.MstUser.id'),
                    'modified'              => $now,
                    'move_type'             => Configure::read('MoveType.move2ope'),
                    )
                );
            
            $this->TrnMoveHeader->create();
            if(!$this->TrnMoveHeader->save($TrnMoveHeader)){
                $this->TrnSticker->rollback();
                $this->Session->setFlash('移動登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ope_move_read_sticker');
            }
            $TrnMoveHeaderId = $this->TrnMoveHeader->getLastInsertID();
            
            // シールIDから必要情報を取得
            $sql  = ' select ';
            $sql .= '       a.id                       as sticker_id ';
            $sql .= '     , a.mst_item_unit_id ';
            $sql .= '     , a.mst_department_id ';
            $sql .= '     , a.facility_sticker_no ';
            $sql .= '     , a.hospital_sticker_no ';
            $sql .= '     , trunc(a.transaction_price / b.per_unit , 2 ) as price ';
            $sql .= '     , trunc(a.sales_price / b.per_unit , 2 ) as sales_price ';
            $sql .= '     , a.original_price ';
            $sql .= '     , a.quantity ';
            $sql .= '     , a.trn_order_id ';
            $sql .= '     , a.lot_no ';
            $sql .= "     , to_char(a.validated_date,'YYYY/mm/dd') as validated_date ";
            $sql .= '     , b.per_unit ';
            $sql .= '     , d.id                       as item_unit_id  ';
            $sql .= '     , a.class_name';
            $sql .= '     , a.type_name';
            $sql .= '     , a.sales_flg';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '     left join mst_item_units as b  ';
            $sql .= '       on b.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as c  ';
            $sql .= '       on c.id = b.mst_facility_item_id  ';
            $sql .= '     left join mst_item_units as d  ';
            $sql .= '       on d.mst_facility_item_id = c.id  ';
            $sql .= '       and d.per_unit = 1  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',', $this->request->data['TrnSticker']['id'] ).  ') ';
            
            $stickers = $this->TrnSticker->query($sql);
            $fromCnt = 0;
            $toCnt = 0;
            
            foreach($stickers as $s){
                // 出荷レコード
                $TrnShipping = array(
                    'TrnShipping'=> array(
                        'work_no'            => $workNo,
                        'work_seq'           => $fromCnt,
                        'work_date'          => $workDate,
                        'shipping_type'      => Configure::read('ShippingType.move'),
                        'shipping_status'    => Configure::read('ShippingStatus.loaded'),
                        'mst_item_unit_id'   => $s[0]['mst_item_unit_id'],
                        'department_id_from' => $center_department_id,
                        'department_id_to'   => $ope_department_id,
                        'quantity'           => 1,
                        'trn_sticker_id'     => $s[0]['sticker_id'],
                        'recital'            => $this->request->data['TrnSticker']['recital'][$s[0]['sticker_id']],
                        'is_deleted'         => false,
                        'creater'            => $this->Session->read('Auth.MstUser.id'),
                        'created'            => $now,
                        'modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'modified'           => $now,
                        'trn_move_header_id' => $TrnMoveHeaderId,
                        'move_type'          => Configure::read('MoveType.move2ope'),
                        )
                    );
                $this->TrnShipping->create();
                if(!$this->TrnShipping->save($TrnShipping)){
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('移動登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ope_move_read_sticker');
                }
                $shipping_id = $this->TrnShipping->getLastInsertID();
                // 旧シール無効化
                $this->TrnSticker->create();
                $this->TrnSticker->updateAll(array(
                    'TrnSticker.quantity' => 'TrnSticker.quantity -1 ' ,
                    'TrnSticker.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnSticker.modified' => "'" . $now ."'",
                    ),array(
                        'TrnSticker.id' => $s[0]['sticker_id'],
                ),-1);
                
                // 旧シール移動履歴
                $this->TrnStickerRecord->create();
                $TrnStickerRecord = array(
                    'TrnStickerRecord' => array(
                        'move_date'           => $workDate,
                        'move_seq'            => $this->getNextRecord($s[0]['sticker_id']),
                        'record_type'         => Configure::read('RecordType.move2ope'),
                        'record_id'           => $shipping_id,
                        'trn_sticker_id'      => $s[0]['sticker_id'],
                        'mst_department_id'   => $center_department_id,
                        'facility_sticker_no' => $s[0]['facility_sticker_no'],
                        'hospital_sticker_no' => $s[0]['hospital_sticker_no'],
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        )
                    );
                if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('移動登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ope_move_read_sticker');
                }
                
                // 旧在庫レコード更新
                $this->TrnStock->create();
                $res = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.stock_count' => 'TrnStock.stock_count - ' . $s[0]['quantity'],
                        'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'    => "'".$now."'",
                        ),
                    array(
                        'TrnStock.id' => $this->getStockRecode($s[0]['mst_item_unit_id'],
                                                               $center_department_id
                                                               )
                        ),
                    -1);
                
                if(!$res){
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('移動登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ope_move_read_sticker');
                }
                
                $fromCnt++;
                // シールの入数分ループ
                for($i = 0; $i < $s[0]['per_unit']; $i++){
                    // 新シール作成
                    $facility_sticker_no = $this->getCenterStickerNo($workDate, $facility_code);
                    $this->TrnSticker->create();
                    $this->TrnSticker->save(array(
                        'facility_sticker_no' => $facility_sticker_no,
                        'mst_item_unit_id'    => $s[0]['item_unit_id'],
                        'mst_department_id'   => $ope_department_id,
                        'trade_type'          => Configure::read('ClassesType.Constant'),
                        'quantity'            => 1,
                        'lot_no'              => $s[0]['lot_no'],
                        'validated_date'      => $s[0]['validated_date'],
                        'original_price'      => $s[0]['original_price'],
                        'transaction_price'   => $s[0]['price'],
                        'sales_price'         => $s[0]['sales_price'],
                        'last_move_date'      => $workDate,
                        'trn_order_id'        => $s[0]['trn_order_id'],
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        'has_reserved'        => false,
                        'trn_move_header_id'  => $TrnMoveHeaderId,
                        'class_name'          => $s[0]['class_name'],
                        'type_name'           => $s[0]['type_name'],
                        'salse_flg'           => $s[0]['sales_flg'],
                        ));
                    
                    $sticker_id = $this->TrnSticker->getLastInsertID();

                    
                    //入荷レコード
                    $TrnReceiving = array(
                        'TrnReceiving' => array(
                            'work_no'             => $workNo,
                            'work_seq'            => $toCnt,
                            'work_date'           => $workDate,
                            'receiving_type'      => Configure::read('ReceivingType.move2ope'),
                            'receiving_status'    => Configure::read('ReceivingStatus.arrival'),
                            'mst_item_unit_id'    => $s[0]['item_unit_id'],
                            'department_id_from'  => $ope_department_id,
                            'department_id_to'    => $center_department_id,
                            'quantity'            => 1,
                            'remain_count'        => 1,
                            'stocking_price'      => $s[0]['price'],
                            'sales_price'         => $s[0]['price'],
                            'trn_sticker_id'      => $sticker_id,
                            'trn_shipping_id'     => $shipping_id,
                            'stocking_close_type' => Configure::read('StockingCloseType.none'),
                            'sales_close_type'    => Configure::read('SalesCloseType.none'),
                            'facility_close_type' => Configure::read('FacilityCloseType.none'),
                            'trn_move_header_id'  => $TrnMoveHeaderId,
                            'is_deleted'          => false,
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now,
                            ));

                    $this->TrnReceiving->create();
                    if(!$this->TrnReceiving->save($TrnReceiving)){
                        $this->TrnSticker->rollback();
                        $this->Session->setFlash('移動登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('ope_move_read_sticker');
                    }
                    
                    $reciving_id = $this->TrnReceiving->getLastInsertID();
                    
                    // 新シール移動履歴
                    $this->TrnStickerRecord->create();
                    $TrnStickerRecord = array(
                        'TrnStickerRecord' => array(
                            'move_date'           => $workDate,
                            'move_seq'            => $this->getNextRecord($sticker_id),
                            'record_type'         => Configure::read('RecordType.move2ope'),
                            'record_id'           => $reciving_id,
                            'trn_sticker_id'      => $sticker_id,
                            'mst_department_id'   => $ope_department_id,
                            'facility_sticker_no' => $facility_sticker_no,
                            'hospital_sticker_no' => '',
                            'is_deleted'          => false,
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now,
                            )
                        );
                    if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                        $this->TrnSticker->rollback();
                        $this->Session->setFlash('移動登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('ope_move_read_sticker');
                    }
                    
                    // 新在庫レコード更新
                    $this->TrnStock->create();
                    $res = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.stock_count'     => 'TrnStock.stock_count + 1 ',
                            'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'        => "'".$now."'",
                            ),
                        array(
                            'TrnStock.id' => $this->getStockRecode($s[0]['item_unit_id'],
                                                                   $ope_department_id
                                                                   )
                            ),
                    -1);
                    
                    if(!$res){
                        $this->TrnSticker->rollback();
                        $this->Session->setFlash('移動登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('ope_move_read_sticker');
                    }
                    $tmp = $this->getStickerData($facility_sticker_no);
                    $result[] = $tmp[0];
                    
                    $toCnt++;
                }
            }
            // 移動ヘッダ明細件数更新
            $this->TrnMoveHeader->create();
            $res = $this->TrnMoveHeader->updateAll(
                array(
                    'TrnMoveHeader.detail_count'          => $fromCnt ,
                    'TrnMoveHeader.detail_total_quantity' => $fromCnt + $toCnt,
                    'TrnMoveHeader.modifier'              => $this->Session->read('Auth.MstUser.id'),
                    'TrnMoveHeader.modified'              => "'".$now."'",
                    ),
                array(
                    'TrnMoveHeader.id' => $TrnMoveHeaderId
                    ),
                -1);
            
            if(!$res){
                $this->TrnSticker->rollback();
                $this->Session->setFlash('移動登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ope_move_read_sticker');
            }
            
            //コミット直前に更新チェックを行う
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $this->request->data['TrnSticker']['id']). ')';
            $sql .= "     and a.modified > '" . $this->request->data['OpeMove']['time'] ."'";
            $sql .= "     and a.modified <> '" . $now ."'";

            $ret = $this->TrnSticker->query($sql);

            if($ret[0][0]['count']> 0 ){
                $this->TrnSticker->rollback();
                $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('ope_move_read_sticker');
            }

            // コミット
            $this->TrnSticker->commit();
            $this->Session->write('OpeMove.result',$result);
            // 結果表示用データ取得
            $this->set('result' , $result);
        }else{
            // トランザクショントークン不正
            // リロード or ２度押し
            $this->set('result',$this->Session->read('OpeMove.result'));
        }
    }
    
    function getStickerData($sticker_no){
        $sql  = ' select ';
        $sql .= '       a.id                       as "TrnSticker__id"';
        $sql .= '     , c.internal_code            as "TrnSticker__internal_code"';
        $sql .= '     , c.item_name                as "TrnSticker__item_name"';
        $sql .= '     , c.item_code                as "TrnSticker__item_code"';
        $sql .= '     , c.standard                 as "TrnSticker__standard"';
        $sql .= '     , d.dealer_name              as "TrnSticker__dealer_name"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then b1.unit_name  ';
        $sql .= "         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .= '         end ';
        $sql .= '     )                            as "TrnSticker__unit_name" ';
        $sql .= '     , a.facility_sticker_no      as "TrnSticker__facility_sticker_no"';
        $sql .= '     , a.hospital_sticker_no      as "TrnSticker__hospital_sticker_no"';
        $sql .= '     , a.lot_no                   as "TrnSticker__lot_no"';
        $sql .= "     , to_char(a.validated_date, 'YYYY/mm/dd') " ;
        $sql .= '                                  as "TrnSticker__validated_date" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.id is null  ';
        $sql .= '         then false  ';
        $sql .= '         when e.id <> f.id  ';
        $sql .= '         then false  ';
        $sql .= '         when a.quantity = 0  ';
        $sql .= '         then false  ';
        $sql .= '         when a.is_deleted = true  ';
        $sql .= '         then false  ';
        $sql .= '         when a.has_reserved = true  ';
        $sql .= '         then false  ';
        $sql .= '         else true  ';
        $sql .= '         end ';
        $sql .= '     )                            as "TrnSticker__is_check" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.id is null  ';
        $sql .= "         then '無効なシール番号です' ";
        $sql .= '         when e.id <> f.id  ';
        $sql .= "         then '別部署に存在するシールです。' ";
        $sql .= '         when a.quantity = 0  ';
        $sql .= "         then '使用済みシールです'  ";
        $sql .= '         when a.is_deleted = true  ';
        $sql .= "         then '使用済みシールです'  ";
        $sql .= '         when a.has_reserved = true  ';
        $sql .= "         then '予約状態です'  ";
        $sql .= "         else '' ";
        $sql .= '         end ';
        $sql .= '     )                            as "TrnSticker__alert" ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as d  ';
        $sql .= '       on d.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_departments as e  ';
        $sql .= '       on e.id = a.mst_department_id  ';
        $sql .= '     left join mst_departments as f  ';
        $sql .= '       on f.mst_facility_id = c.mst_facility_id  ';
        $sql .= '      and f.department_type = ' . Configure::read('DepartmentType.warehouse') ;
        $sql .= '   where ';
        $sql .= "     a.facility_sticker_no = '" . $sticker_no . "'";
        return $this->TrnSticker->query($sql);
    }

    // オペ倉庫移動履歴
    public function ope_move_search(){
        $this->setRoleFunction(139); //オペ倉庫移動履歴
        $result = array();
        
        if(isset($this->request->data['ope_move_search']['is_search'])){
            $where = '';
            /* 絞り込み条件追加 */

            // 作業番号
            if (isset($this->request->data['TrnOpeMove']['work_no']) && $this->request->data['TrnOpeMove']['work_no'] != '') {
                $where .= " and a.work_no LIKE ". "'%".Sanitize::escape($this->request->data['TrnOpeMove']['work_no'])."%'";
            }

            // 作業日From
            if (isset($this->request->data['TrnOpeMove']['work_date_from']) && $this->request->data['TrnOpeMove']['work_date_from'] != ''){
                $where .= " and a.work_date >= '". Sanitize::escape($this->request->data['TrnOpeMove']['work_date_from'])."'";
            }

            // 作業日To
            if (isset($this->request->data['TrnOpeMove']['work_date_to']) && $this->request->data['TrnOpeMove']['work_date_to'] != ''){
                $where .= " and a.work_date <= '". Sanitize::escape($this->request->data['TrnOpeMove']['work_date_to'])."'";
            }

            // 商品ID
            if (isset($this->request->data['TrnOpeMove']['internal_code']) && $this->request->data['TrnOpeMove']['internal_code'] != '') {
                $where .= " and c2.internal_code = '".  Sanitize::escape($this->request->data['TrnOpeMove']['internal_code'])."'";
            }

            // 製品番号
            if (isset($this->request->data['TrnOpeMove']['item_code']) && $this->request->data['TrnOpeMove']['item_code'] != '') {
                $where .= " and c2.item_code LIKE '".  "%".Sanitize::escape($this->request->data['TrnOpeMove']['item_code'])."%'";
            }

            // 商品名
            if (isset($this->request->data['TrnOpeMove']['item_name']) && $this->request->data['TrnOpeMove']['item_name'] != '') {
                $where .= " and c2.item_name LIKE '".  "%".Sanitize::escape($this->request->data['TrnOpeMove']['item_name'])."%'";
            }

            // 規格
            if (isset($this->request->data['TrnOpeMove']['standard']) && $this->request->data['TrnOpeMove']['standard'] != '') {
                $where .= " and c2.standard LIKE '".  "%".Sanitize::escape($this->request->data['TrnOpeMove']['standard'])."%'";
            }

            // 販売元
            if (isset($this->request->data['TrnOpeMove']['dealer_name']) && $this->request->data['TrnOpeMove']['dealer_name'] != '') {
                $where .= " and ( c3.dealer_name LIKE '".  "%".Sanitize::escape($this->request->data['TrnOpeMove']['dealer_name'])."%'";
            }
            
            // ロット番号
            if (isset($this->request->data['TrnOpeMove']['lot_no']) && $this->request->data['TrnOpeMove']['lot_no'] != '') {
                $where .= " and ( c4.lot_no   LIKE ".  "'%".Sanitize::escape($this->request->data['TrnOpeMove']['lot_no'])."%'";
                $where .= " or d4.lot_no   LIKE ".  "'%".Sanitize::escape($this->request->data['TrnOpeMove']['lot_no'])."%' ) ";
            }

            // シール番号
            if (isset($this->request->data['TrnOpeMove']['facility_sticker_no']) && $this->request->data['TrnOpeMove']['facility_sticker_no'] != '') {
                $where .= " and ( c4.facility_sticker_no   LIKE ".  "'%".Sanitize::escape($this->request->data['TrnOpeMove']['facility_sticker_no'])."%'";
                $where .= " or d4.facility_sticker_no   LIKE ".  "'%".Sanitize::escape($this->request->data['TrnOpeMove']['facility_sticker_no'])."%' ) ";
            }
            
            
            $sql  = ' select ';
            $sql .= '       a.id           as "TrnMove__id"';
            $sql .= '     , a.work_no      as "TrnMove__work_no"';
            $sql .= "     , to_char(a.work_date,'YYYY/MM/DD') ";
            $sql .= '                      as "TrnMove__work_date"';
            $sql .= '     , b.user_name    as "TrnMove__user_name"';
            $sql .= "     , to_char(a.created , 'YYYY/mm/dd hh24:mi:ss')";
            $sql .= '                      as "TrnMove__created"';
            $sql .= '     , a.recital      as "TrnMove__recital"';
            $sql .= '     , a.detail_count as "TrnMove__detail_count" ';
            $sql .= '   from ';
            $sql .= '     trn_move_headers as a  ';
            $sql .= '     left join mst_users as b  ';
            $sql .= '       on b.id = a.creater  ';
            $sql .= '     left join trn_shippings as c  ';
            $sql .= '       on c.trn_move_header_id = a.id  ';
            $sql .= '     left join mst_item_units as c1  ';
            $sql .= '       on c1.id = c.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as c2  ';
            $sql .= '       on c2.id = c1.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as c3  ';
            $sql .= '       on c3.id = c2.mst_dealer_id  ';
            $sql .= '     left join trn_stickers as c4  ';
            $sql .= '       on c4.id = c.trn_sticker_id  ';
            $sql .= '     left join trn_receivings as d  ';
            $sql .= '       on d.trn_move_header_id = a.id  ';
            $sql .= '     left join mst_item_units as d1  ';
            $sql .= '       on d1.id = d.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as d2  ';
            $sql .= '       on d2.id = d1.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as d3  ';
            $sql .= '       on d3.id = d2.mst_dealer_id  ';
            $sql .= '     left join trn_stickers as d4  ';
            $sql .= '       on d4.id = d.trn_sticker_id  ';
            $sql .= '   where ';
            $sql .= '     a.move_type = ' . Configure::read('MoveType.move2ope');
            $sql .= '     and a.is_deleted = false ';
            $sql .= $where;
            $sql .= '    group by ';
            $sql .= '       a.id ';
            $sql .= '     , a.work_no';
            $sql .= '     , a.work_date';
            $sql .= '     , b.user_name';
            $sql .= '     , a.created';
            $sql .= '     , a.recital';
            $sql .= '     , a.detail_count';
            
            $sql .= '   order by ';
            $sql .= '     a.work_no ';
            
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnMoveHeader'));
            $sql .= '   limit ' . $this->request->data['limit'];

            $result = $this->TrnMoveHeader->query($sql);
        }

        $this->set('result' , $result);
    }
    
    // オペ倉庫移動履歴明細
    public function ope_move_detail(){
        // 旧シール
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , a.work_no ';
        $sql .= "     , to_char(a.work_date,'YYYY/MM/DD') as work_date ";
        $sql .= '     , d.internal_code ';
        $sql .= '     , d.item_name ';
        $sql .= '     , d.standard ';
        $sql .= '     , d.item_code ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when c.per_unit = 1  ';
        $sql .= '         then c1.unit_name  ';
        $sql .= "         else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                            as unit_name ';
        $sql .= '     , e.facility_sticker_no  ';
        $sql .= '     , b.id as trn_shipping_id ';
        $sql .= '   from ';
        $sql .= '     trn_move_headers as a  ';
        $sql .= '     left join trn_shippings as b  ';
        $sql .= '       on b.trn_move_header_id = a.id  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on c.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as c1  ';
        $sql .= '       on c1.id = c.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as c2  ';
        $sql .= '       on c2.id = c.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as d  ';
        $sql .= '       on d.id = c.mst_facility_item_id  ';
        $sql .= '     left join trn_stickers as e  ';
        $sql .= '       on e.id = b.trn_sticker_id  ';
        $sql .= '   where ';
        $sql .= '     a.id in ( ' . join(',',$this->request->data['TrnMove']['id']) . ') ';
        $sql .= '     and a.center_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '   order by a.work_no , e.facility_sticker_no ';
        
        $result = $this->TrnMoveHeader->query($sql);
        foreach($result as &$r){
            // 新シール
            $sql  = ' select ';
            $sql .= '      (  ';
            $sql .= '       case  ';
            $sql .= '         when c.per_unit = 1  ';
            $sql .= '         then c1.unit_name  ';
            $sql .= "         else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
            $sql .= '         end ';
            $sql .= '     )                            as unit_name ';
            $sql .= '     , e.facility_sticker_no  ';
            $sql .= '   from ';
            $sql .= '     trn_move_headers as a  ';
            $sql .= '     left join trn_receivings as b  ';
            $sql .= '       on b.trn_move_header_id = a.id  ';
            $sql .= '     left join mst_item_units as c  ';
            $sql .= '       on c.id = b.mst_item_unit_id  ';
            $sql .= '     left join mst_unit_names as c1  ';
            $sql .= '       on c1.id = c.mst_unit_name_id  ';
            $sql .= '     left join mst_unit_names as c2  ';
            $sql .= '       on c2.id = c.per_unit_name_id  ';
            $sql .= '     left join mst_facility_items as d  ';
            $sql .= '       on d.id = c.mst_facility_item_id  ';
            $sql .= '     left join trn_stickers as e  ';
            $sql .= '       on e.id = b.trn_sticker_id  ';
            $sql .= '   where ';
            $sql .= '     a.id = ' . $r[0]['id'];
            $sql .= '     and b.trn_shipping_id = ' . $r[0]['trn_shipping_id'];
            $sql .= '     and a.center_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '   order by e.facility_sticker_no';
            
            $r['new'] = $this->TrnMoveHeader->query($sql);
        }
        
        unset($r);
        $this->set('result' , $result);
    }
    
    // オペ予定入力検索
    public function ope_search(){
        $this->setRoleFunction(130); //オペ予定入力
        $result = array();
        // プルダウン作成
        $this->getPullDown();
        $this->set('status',Configure::read('OpeStatus.list'));
        
        if(isset($this->request->data['search']['is_search'])){
            $sql  = ' select ';
            $sql .= '       a.id                     as "TrnOpeHeader__id" ';
            $sql .= "     , to_char(a.work_date, 'YYYY/MM/DD') ";
            $sql .= '                                as "TrnOpeHeader__work_date" ';
            $sql .= '     , a.work_no                as "TrnOpeHeader__work_no" ';
            $sql .= '     , f.ope_method_name        as "TrnOpeHeader__method_name" ';
            $sql .= '     , b.ope_department_name    as "TrnOpeHeader__department_name" ';
            $sql .= '     , c.department_name        as "TrnOpeHeader__ward_name" ';
            $sql .= '     , a.name                   as "TrnOpeHeader__name" ';
            $sql .= '     , d.ope_person_name        as "TrnOpeHeader__person_name" ';
            $sql .= "     , to_char(a.created, 'YYYY/mm/dd hh24:mi:ss') ";
            $sql .= '                                as "TrnOpeHeader__created" ';
            $sql .= '     , e.user_name              as "TrnOpeHeader__user_name" ';
            $sql .= '     , ( case a.ope_status ';
            foreach(Configure::read('OpeStatus.list') as $k => $v ){
                $sql .= " when '" . $k . "' then '" . $v . "'";
            }
            $sql .= '        end )                   as "TrnOpeHeader__status_name"';
            $sql .= '     , a.ope_status             as "TrnOpeHeader__status"';
            $sql .= '   from ';
            $sql .= '     trn_ope_headers as a  ';
            $sql .= '     left join mst_ope_departments as b  ';
            $sql .= '       on b.id = a.mst_ope_department_id  ';
            $sql .= '     left join mst_departments as c  ';
            $sql .= '       on c.id = a.mst_ope_ward_id  ';
            $sql .= '     left join mst_ope_people as d  ';
            $sql .= '       on d.id = a.mst_ope_person_id  ';
            $sql .= '     left join mst_users as e  ';
            $sql .= '       on e.id = a.creater  ';
            $sql .= '     left join mst_ope_method_names as f ';
            $sql .= '       on f.id = a.mst_ope_method_id ';
            $sql .= '   where ';
            $sql .= '     a.is_deleted = false  ';
            if(isset($this->request->data['TrnOpeHeader']['search_code']) && $this->request->data['TrnOpeHeader']['search_code'] != ''){
                $sql .= " and a.work_no = '" . $this->request->data['TrnOpeHeader']['search_code'] . "'";
            }
            if(isset($this->request->data['TrnOpeHeader']['search_date']) && $this->request->data['TrnOpeHeader']['search_date'] != ''){
                $sql .= " and a.work_date = '" . $this->request->data['TrnOpeHeader']['search_date'] . "'";
            }
            if(isset($this->request->data['TrnOpeHeader']['search_department']) && $this->request->data['TrnOpeHeader']['search_department'] != ''){
                $sql .= " and a.mst_ope_department_id = '" . $this->request->data['TrnOpeHeader']['search_department'] . "'";
            }
            if(isset($this->request->data['TrnOpeHeader']['search_ward']) && $this->request->data['TrnOpeHeader']['search_ward'] != ''){
                $sql .= " and a.mst_ope_ward_id = '" . $this->request->data['TrnOpeHeader']['search_ward'] . "'";
            }
            if(isset($this->request->data['TrnOpeHeader']['search_status']) && $this->request->data['TrnOpeHeader']['search_status'] != ''){
                $sql .= " and a.ope_status = '" . $this->request->data['TrnOpeHeader']['search_status'] . "'";
            }
            if(isset($this->request->data['TrnOpeHeader']['search_method_name']) && $this->request->data['TrnOpeHeader']['search_method_name'] != ''){
                $sql .= " and f.ope_method_name like '%" . $this->request->data['TrnOpeHeader']['search_method_name'] . "%'";
            }
            
            $sql .= ' and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            
            $sql .= '   order by ';
            $sql .= '     a.work_date , a.work_no';
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnOpeHeader'));

            $sql .= " LIMIT ".$this->request->data['limit'];

            $result = $this->TrnOpeHeader->query($sql , false);
            
        }
        $this->set('result' , $result);
    }
    
    // オペ予定編集
    public function ope_edit($id=null){
        // プルダウン作成
        $this->getPullDown();
        $items = array();
        $item_set = array();
        $department_list = array();
        
        if(!is_null($id)){
            // 編集
            $sql  = ' select ';
            $sql .= '       a.id ';
            $sql .= '     , a.work_no                         as ope_code';
            $sql .= "     , to_char(a.work_date,'YYYY/MM/DD') as work_date ";
            $sql .= '     , a.mst_ope_room_id                 as room';
            $sql .= '     , a.mst_ope_department_id           as "opeDepartmentCode"';
            $sql .= '     , a.mst_ope_ward_id                 as "wardCode"';
            $sql .= '     , a.mst_ope_person_id               as person';
            $sql .= '     , a.mst_ope_anesthesia_id           as anesthesia';
            $sql .= '     , a.mst_ope_position_id             as position';
            $sql .= '     , a.mst_ope_method_id               as methodid';
            $sql .= '     , d.department_code                 as "departmentCode"';
            $sql .= '     , d.department_code                 as "departmentText"';
            $sql .= '     , e.facility_code                   as "facilityCode"';
            $sql .= '     , e.facility_code                   as "facilityText"';
            $sql .= '     , b.ope_method_name                 as method';
            $sql .= '     , b.ope_method_set_code             as method_set_code';
            $sql .= '     , a.number ';
            $sql .= '     , a.name ';
            $sql .= '     , a.age ';
            $sql .= '     , a.sex ';
            $sql .= '     , a.start_time                      as start_time';
            $sql .= '   from ';
            $sql .= '     trn_ope_headers as a  ';
            $sql .= '     left join mst_ope_method_names as b ';
            $sql .= '       on b.id = a.mst_ope_method_id';
            $sql .= '     left join mst_departments as d ';
            $sql .= '       on d.id = a.mst_department_id';
            $sql .= '     left join mst_facilities as e ';
            $sql .= '       on e.id = d.mst_facility_id';
            $sql .= '   where ';
            $sql .= '     a.id = ' . $id;
            $sql .= '     and a.is_deleted = false  ';
            
            $tmp = $this->TrnOpeHeader->query($sql , false);

            $this->request->data['TrnOpe'] = $tmp[0][0];
            
            $sql  = ' select ';
            $sql .= '       a.id                as "Item__id"';
            $sql .= '     , b.id                as "Item__mst_id"';
            $sql .= '     , c.internal_code     as "Item__internal_code"';
            $sql .= '     , c.item_name         as "Item__item_name"';
            $sql .= '     , c.standard          as "Item__standard"';
            $sql .= '     , c.item_code         as "Item__item_code"';
            $sql .= '     , a.quantity          as "Item__quantity"';
            $sql .= '     , a.ope_item_set_code as "Item__code"';
            $sql .= '     , a.recital           as "Item__recital" ';
            $sql .= '   from ';
            $sql .= '     trn_ope_items as a  ';
            $sql .= '     left join mst_item_units as b ';
            $sql .= '       on b.id = a.mst_item_unit_id ';
            $sql .= '     left join mst_facility_items as c ';
            $sql .= '       on c.id = b.mst_facility_item_id ';
            $sql .= '   where ';
            $sql .= '     a.is_deleted = false ';
            $sql .= '     and a.trn_ope_header_id = ' . $id;
            $sql .= '   order by ';
            $sql .= '     a.work_seq ';
            
            $items = $this->TrnOpeItem->query($sql);

            $sql  = ' select ';
            $sql .= '       a.ope_item_set_code as "Item__code"';
            $sql .= '     , b.ope_item_set_name as "Item__name"';
            $sql .= '   from ';
            $sql .= '     trn_ope_items as a ';
            $sql .= '   left join mst_ope_item_set_headers as b';
            $sql .= '     on b.ope_item_set_code = a.ope_item_set_code';
            $sql .= '   where ';
            $sql .= '     a.is_deleted = false ';
            $sql .= '     and a.trn_ope_header_id = ' . $id;
            $sql .= "     and a.ope_item_set_code != ''";
            $sql .= '   group by a.ope_item_set_code , b.ope_item_set_name';
            $sql .= '   order by a.ope_item_set_code ';
            $item_set = $this->TrnOpeItem->query($sql);
            
            $department_list = $this->getDepartmentsList($this->request->data['TrnOpe']['facilityCode']);
        }
        
        $this->set('items' , $items);
        $this->set('item_set' , $item_set);
        $this->set('facility_list', $this->getFacilityList( $this->Session->read('Auth.facility_id_selected') ,
                                                                 array(Configure::read('FacilityType.hospital'))
                                                                 ));
        $this->set('department_list', $department_list);

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnOpe.token' , $token);
        $this->request->data['TrnOpe']['token'] = $token;
    }
    
    // オペ予定結果
    public function ope_result(){
        $this->getPullDown();
        $department_list = $this->getDepartmentsList($this->request->data['TrnOpe']['facilityCode']);
        $this->set('facility_list', $this->getFacilityList( $this->Session->read('Auth.facility_id_selected') ,
                                                                 array(Configure::read('FacilityType.hospital'))
                                                                 ));
        $this->set('department_list', $department_list);

        
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if(isset($this->request->data['TrnOpe']['token']) &&
           $this->request->data['TrnOpe']['token'] === $this->Session->read('TrnOpe.token')) {
            $this->Session->delete('TrnOpe.token');
            $now = date('Y/m/d H:i:s.u');
            
            if(!empty($this->request->data['TrnOpe']['id'])){
                // 更新のときは行ロックする。
                $this->TrnOpeHeader->query(' select * from trn_ope_headers as a where a.id = ' . $this->request->data['TrnOpe']['id'] );
                $trn_ope_header_id = $this->request->data['TrnOpe']['id'];
            }else{
                $trn_ope_header_id = null;
            }
            
            $this->TrnOpeHeader->begin();
            
            // オペヘッダ作成
            if(empty($trn_ope_header_id)){
                // 新規の場合
                // 作業番号を取得
                $ret = $this->TrnOpeHeader->query(" select lpad((max(a.id) + 1) ::varchar, 6, '0') as work_no from trn_ope_headers as a ");
                
                $work_no = $ret[0][0]['work_no'];
                if(empty($work_no)){
                    $work_no = '000001';
                }
                $this->request->data['TrnOpe']['ope_code']  = $work_no;
                $TrnOpeHeader['TrnOpeHeader']['work_no']    = $work_no;
                $TrnOpeHeader['TrnOpeHeader']['is_deleted'] = false;
                $TrnOpeHeader['TrnOpeHeader']['ope_status'] = Configure::read('OpeStatus.init');
                $TrnOpeHeader['TrnOpeHeader']['creater']    = $this->Session->read('Auth.MstUser.id');
                $TrnOpeHeader['TrnOpeHeader']['created']    = $now;
            }else{
                // 更新の場合
                $TrnOpeHeader['TrnOpeHeader']['id'] = $trn_ope_header_id;
            }
            // 消費施設IDを取得
            $facility_id = $this->getFacilityId($this->request->data['TrnOpe']['facilityCode'] ,
                                                array(Configure::read('FacilityType.hospital')),
                                                $this->Session->read('Auth.facility_id_selected')
                                                );
            // 消費部署IDを取得
            $department_id = $this->getDepartmentId($facility_id ,
                                                array(Configure::read('DepartmentType.hospital')),
                                                $this->request->data['TrnOpe']['departmentCode']
                                                );
            
            $TrnOpeHeader['TrnOpeHeader']['work_date']             = $this->request->data['TrnOpe']['work_date'];
            $TrnOpeHeader['TrnOpeHeader']['mst_ope_room_id']       = $this->request->data['TrnOpe']['room'];
            $TrnOpeHeader['TrnOpeHeader']['mst_ope_department_id'] = $this->request->data['TrnOpe']['opeDepartmentCode'];
            $TrnOpeHeader['TrnOpeHeader']['mst_ope_ward_id']       = $this->request->data['TrnOpe']['wardCode'];
            $TrnOpeHeader['TrnOpeHeader']['mst_ope_anesthesia_id'] = $this->request->data['TrnOpe']['anesthesia'];
            $TrnOpeHeader['TrnOpeHeader']['mst_ope_position_id']   = $this->request->data['TrnOpe']['position'];
            $TrnOpeHeader['TrnOpeHeader']['mst_ope_method_id']     = $this->request->data['TrnOpe']['methodid'];
            $TrnOpeHeader['TrnOpeHeader']['mst_department_id']     = $department_id;
            $TrnOpeHeader['TrnOpeHeader']['start_time']            = $this->request->data['TrnOpe']['start_time'];
            $TrnOpeHeader['TrnOpeHeader']['status']                = Configure::read('OpeStatus.init');
            $TrnOpeHeader['TrnOpeHeader']['modifier']              = $this->Session->read('Auth.MstUser.id');
            $TrnOpeHeader['TrnOpeHeader']['modified']              = $now;
            $TrnOpeHeader['TrnOpeHeader']['mst_facility_id']       = $this->Session->read('Auth.facility_id_selected');
            
            
            // 術式備考を取得
            $sql = ' select ope_method_comment from mst_ope_method_names where id = ' . $TrnOpeHeader['TrnOpeHeader']['mst_ope_method_id'];
            $comment = $this->MstOpeMethodName->query($sql);
            $TrnOpeHeader['TrnOpeHeader']['ope_method_comment']    = $comment[0][0]['ope_method_comment'];
            
            $this->TrnOpeHeader->create();
            if(!$this->TrnOpeHeader->save($TrnOpeHeader)){
                $this->TrnOpeHeader->rollback();
                $this->Session->setFlash('手術予定情報登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ope_search');
            }
            if(empty($this->request->data['TrnOpe']['id'])){
                $trn_ope_header_id = $this->TrnOpeHeader->getLastInsertID();
            }else{
                $trn_ope_header_id = $this->request->data['TrnOpe']['id'];
            }

            
            // 材料
            // いったん削除フラグを立てる。
            $res = $this->TrnOpeItem->updateAll(array(
                'TrnOpeItem.is_deleted' => 'true',
                'TrnOpeItem.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnOpeItem.modified'   => "'". $now ."'",
            ), array(
                'TrnOpeItem.trn_ope_header_id' => $trn_ope_header_id,
            ), -1);

            if(false === $res){
                $this->TrnOpeHeader->rollback();
                $this->Session->setFlash('手術材料登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ope_search');
            }
            if(isset($this->request->data['TrnOpe']['item_mst_id'])){
                $seq = 1;
                foreach($this->request->data['TrnOpe']['item_mst_id'] as $line => $mst_id){
                    $TrnOpeItem = array();
                    if(isset($this->request->data['TrnOpe']['item_id'][$line])){
                        // 更新
                        $TrnOpeItem = array(
                            'TrnOpeItem' => array(
                                'id'                => $this->request->data['TrnOpe']['item_id'][$line],
                                'trn_ope_header_id' => $trn_ope_header_id,
                                'work_seq'          => $seq,
                                'mst_item_unit_id'  => $mst_id,
                                'quantity'          => $this->request->data['TrnOpe']['quantity'][$line],
                                'is_deleted'        => false,
                                'modifier'          => $this->Session->read('Auth.MstUser.id'),
                                'modified'          => $now,
                                )
                            );
                    }else{
                        // 新規
                        $TrnOpeItem = array(
                            'TrnOpeItem' => array(
                                'trn_ope_header_id' => $trn_ope_header_id,
                                'work_seq'          => $seq,
                                'mst_item_unit_id'  => $mst_id,
                                'quantity'          => $this->request->data['TrnOpe']['quantity'][$line],
                                'ope_item_set_code' => $this->request->data['TrnOpe']['code'][$line],
                                'recital'           => $this->request->data['TrnOpe']['recital'][$line],
                                'ope_status'        => Configure::read('OpeStatus.init'),
                                'is_deleted'        => false,
                                'creater'           => $this->Session->read('Auth.MstUser.id'),
                                'created'           => $now,
                                'modifier'          => $this->Session->read('Auth.MstUser.id'),
                                'modified'          => $now,
                                )
                            );
                    }
                    $seq++;
                    $this->TrnOpeItem->create();
                    if(!$this->TrnOpeItem->save($TrnOpeItem)){
                        $this->TrnOpeHeader->rollback();
                        $this->Session->setFlash('手術予定情報登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('ope_search');
                    }
                }
            }
            
            $this->TrnOpeHeader->commit();
            $this->request->data['TrnOpeHeader']['id'] = $trn_ope_header_id;
            
        }else{
            
        }
    }
    
    function ope_shipping($id){
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , b.id                               as ope_item_id';
        $sql .= '     , a.work_no                          as code ';
        $sql .= "     , to_char(a.work_date, 'YYYY/MM/DD') as work_date ";
        $sql .= '     , b.ope_item_set_code                as set_code ';
        $sql .= '     , d.internal_code ';
        $sql .= '     , d.item_name ';
        $sql .= '     , d.standard ';
        $sql .= '     , d.item_code ';
        $sql .= '     , b.quantity  ';
        $sql .= '     , b.mst_item_unit_id ';
        $sql .= '     , c1.unit_name';
        $sql .= '   from ';
        $sql .= '     trn_ope_headers as a  ';
        $sql .= '     left join trn_ope_items as b  ';
        $sql .= '       on b.trn_ope_header_id = a.id  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on c.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as c1 ';
        $sql .= '       on c1.id = c.mst_unit_name_id ';
        $sql .= '     left join mst_facility_items as d  ';
        $sql .= '       on d.id = c.mst_facility_item_id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $id;
        $sql .= '     and a.is_deleted = false ';
        $sql .= '     and b.is_deleted = false ';
        $sql .= '   order by ';
        $sql .= "     ( case when b.ope_item_set_code = '' then 'ZZZZZZ' else b.ope_item_set_code end ) ";
        $sql .= '     , d.item_name ';
        $sql .= '     , d.standard ';
        $sql .= '     , d.item_code ';
        
        $ope_data = $this->TrnOpeHeader->query($sql);
        $result = array();
        $chk = array();
        $i = 0;
        foreach($ope_data as $o){
            for($cnt = 1 ; $o[0]['quantity'] >= $cnt ; $cnt++){
                $result[$i]['id']            = $o[0]['id'];
                $result[$i]['ope_item_id']   = $o[0]['ope_item_id'];
                $result[$i]['code']          = $o[0]['code'];
                $result[$i]['work_date']     = $o[0]['work_date'];
                $result[$i]['set_code']      = $o[0]['set_code'];
                $result[$i]['internal_code'] = $o[0]['internal_code'];
                $result[$i]['item_name']     = $o[0]['item_name'];
                $result[$i]['standard']      = $o[0]['standard'];
                $result[$i]['item_code']     = $o[0]['item_code'];
                $result[$i]['unit_name']     = $o[0]['unit_name'];
                if(isset($chk[$o[0]['mst_item_unit_id']])){
                    $chk[$o[0]['mst_item_unit_id']]++;
                }else{
                    $chk[$o[0]['mst_item_unit_id']] = 0;
                }
                
                // シール検索
                $sql  = ' select ';
                $sql .= '       a.id , ';
                $sql .= '       a.facility_sticker_no , ';
                $sql .= '       a.modified ';
                $sql .= '   from ';
                $sql .= '     trn_stickers as a  ';
                $sql .= '     left join mst_departments as b  ';
                $sql .= '       on b.id = a.mst_department_id  ';
                $sql .= '   where ';
                $sql .= '     a.mst_item_unit_id = ' . $o[0]['mst_item_unit_id'];
                $sql .= '     and a.is_deleted = false ';
                $sql .= '     and a.has_reserved = false ';
                $sql .= '     and a.quantity > 0 ';
                $sql .= '     and b.department_type = ' . Configure::read('DepartmentType.opestock');
                $sql .= '   order by a.id ';
                $sql .= '   limit 1 ';
                $sql .= '   offset ' . $chk[$o[0]['mst_item_unit_id']];
                
                $sticker = $this->TrnSticker->query($sql);
                
                if(empty($sticker)){
                    // センター倉庫から移動すれば払出可能か？確認
                    $sql  = ' select ';
                    $sql .= '       c.internal_code ';
                    $sql .= '     , sum(a.quantity * b.per_unit ) as quantity  ';
                    $sql .= '   from ';
                    $sql .= '     trn_stickers as a  ';
                    $sql .= '     left join mst_item_units as b  ';
                    $sql .= '       on b.id = a.mst_item_unit_id  ';
                    $sql .= '     left join mst_facility_items as c  ';
                    $sql .= '       on c.id = b.mst_facility_item_id  ';
                    $sql .= '     left join mst_departments as d  ';
                    $sql .= '       on c.id = a.mst_department_id  ';
                    $sql .= '       and d.department_type = ' . Configure::read('DepartmentType.warehouse');
                    $sql .= '   where ';
                    $sql .= '     a.is_deleted = false  ';
                    $sql .= '     and a.quantity > 0  ';
                    $sql .= '     and a.has_reserved = false  ';
                    $sql .= "     and c.internal_code = '" . $o[0]['internal_code'] . "'";
                    $sql .= '   group by ';
                    $sql .= '     c.internal_code ';
                    
                    $ret = $this->TrnSticker->query($sql);
                    if(empty($ret)){
                        $result[$i]['trn_sticker_id']      = null;
                        $result[$i]['facility_sticker_no'] = '';
                    }else{
                        $result[$i]['trn_sticker_id']      = null;
                        $result[$i]['facility_sticker_no'] = '☆組替';
                    }
                }else{
                    $result[$i]['trn_sticker_id']      = $sticker[0][0]['id'];
                    $result[$i]['facility_sticker_no'] = $sticker[0][0]['facility_sticker_no'];
                    $result[$i]['modified']            = $sticker[0][0]['modified'];
                }
                $i++;
            }
        }

        $this->set('id' , $id);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('OpeShipping.token' , $token);
        $this->request->data['OpeShipping']['token'] = $token;
        
        $this->set('result' , $result);
    }
    
    function ope_shipping_result(){
        if(isset($this->request->data['OpeShipping']['token']) &&
           $this->request->data['OpeShipping']['token'] === $this->Session->read('OpeShipping.token')) {
            $this->Session->delete('OpeShipping.token');
            $now = date('Y/m/d H:i:s.u');
            
            $this->TrnOpeShipping->begin();
            $i = 1;
            // 手術ヘッダ行ロック
            $this->TrnOpeHeader->query(' select * from trn_ope_headers as a where a.id = ' . $this->request->data['TrnOpeHeader']['id'] . ' for update ');
            
            // 手術ヘッダ情報のステータスを更新する。
            $res = $this->TrnOpeHeader->updateAll(array(
                'TrnOpeHeader.ope_status' => Configure::read('OpeStatus.shipping'),
                'TrnOpeHeader.modifier'     => $this->Session->read('Auth.MstUser.id'),
                'TrnOpeHeader.modified'     => "'". $now ."'",
                ), array(
                    'TrnOpeHeader.id' =>$this->request->data['TrnOpeHeader']['id'],
                ), -1);
            
            if(false === $res){
                $this->TrnOpeShipping->rollback();
                $this->Session->setFlash('手術払出登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ope_search');
            }
            
            foreach($this->request->data['ope']['id'] as $tmp){
                list($trn_ope_item_id , $trn_sticker_id) = explode("_",$tmp);
                
                // シールレコード行ロック
                $ret = $this->TrnSticker->query(' select * from trn_stickers where id = ' . $trn_sticker_id . " and has_reserved = false  and quantity > 0 and modified = '" . $this->request->data['sticker']['modified'][$trn_sticker_id] . "' for update ");
                
                if(empty($ret)){
                    $this->TrnOpeShipping->rollback();
                    $this->Session->setFlash('シール情報が更新されています。もう一度やり直してください。', 'growl', array('type'=>'error') );
                    $this->redirect('ope_search');
                }
                
                // 手術払出情報を作成
                $TrnOpeShipping = array(
                    'TrnOpeShipping' => array(
                        'trn_ope_header_id'  => $this->request->data['TrnOpeHeader']['id'],
                        'work_seq'           => $i,
                        'trn_ope_item_id'    => $trn_ope_item_id,
                        'trn_sticker_id'     => $trn_sticker_id,
                        'is_deleted'         => false,
                        'creater'            => $this->Session->read('Auth.MstUser.id'),
                        'created'            => $now,
                        'modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'modified'           => $now,
                        )
                    );
                $this->TrnOpeShipping->create();
                if(!$this->TrnOpeShipping->save($TrnOpeShipping)){
                    $this->TrnOpeShipping->rollback();
                    $this->Session->setFlash('手術払出登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ope_search');
                }
                
                
                // シールを予約状態にする。
                $res = $this->TrnSticker->updateAll(array(
                    'TrnSticker.has_reserved' => 'true',
                    'TrnSticker.modifier'     => $this->Session->read('Auth.MstUser.id'),
                    'TrnSticker.modified'     => "'". $now ."'",
                    ), array(
                        'TrnSticker.id' =>$trn_sticker_id,
                        ), -1);
                
                if(false === $res){
                    $this->TrnOpeShipping->rollback();
                    $this->Session->setFlash('手術払出登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ope_search');
                }
                
                // 在庫予約数△
                $this->TrnStock->create();
                $c = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.reserve_count' => 'TrnStock.reserve_count + 1 ',
                        'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'    => "'" . $now . "'"
                        ),
                    array(
                        'TrnStock.id' => $this->getStockId($trn_sticker_id),
                        ),
                    -1
                    );
                if(!$c){
                    $this->TrnOpeShipping->rollback();
                    $this->Session->setFlash('手術払出登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ope_search');
                }
                
                $i++;
            }
            
            $this->TrnOpeShipping->commit();
        } else {
            
        }
    }
    
    function ope_return($id){
        $sql  =' select ';
        $sql .='       c.id ';
        $sql .='     , a.work_no ';
        $sql .="     , to_char(a.work_date, 'YYYY/MM/DD') as work_date ";
        $sql .='     , a.start_time ';
        $sql .='     , a1.ope_department_name ';
        $sql .='     , a2.ope_method_name ';
        $sql .='     , e.internal_code ';
        $sql .='     , e.item_name ';
        $sql .='     , e.standard ';
        $sql .='     , e.item_code ';
        $sql .='     , c.quantity ';
        $sql .='     , d1.unit_name ';
        $sql .='     , c.facility_sticker_no ';
        $sql .='     , b.trn_ope_item_id';
        $sql .='     , b1.id as trn_ope_return_id';
        $sql .='   from ';
        $sql .='     trn_ope_headers as a  ';
        $sql .='     left join mst_ope_departments as a1  ';
        $sql .='       on a1.id = a.mst_ope_department_id  ';
        $sql .='     left join mst_ope_method_names as a2  ';
        $sql .='       on a2.id = a.mst_ope_method_id  ';
        $sql .='     left join trn_ope_shippings as b  ';
        $sql .='       on b.trn_ope_header_id = a.id  ';
        $sql .='     left join trn_ope_items as b2';
        $sql .='       on b2.id = b.trn_ope_item_id ';
        $sql .='     left join trn_ope_returns as b1 ';
        $sql .='       on b1.trn_ope_item_id = b.trn_ope_item_id ';
        $sql .='      and b1.trn_sticker_id = b.trn_sticker_id ';
        $sql .='      and b1.is_deleted = false ';
        $sql .='     left join trn_stickers as c ';
        $sql .='       on c.id = b.trn_sticker_id ';
        $sql .='     left join mst_item_units as d ';
        $sql .='       on d.id = c.mst_item_unit_id ';
        $sql .='     left join mst_unit_names as d1 ';
        $sql .='       on d1.id = d.mst_unit_name_id ';
        $sql .='     left join mst_facility_items as e ';
        $sql .='       on e.id = d.mst_facility_item_id ';
        $sql .='   where ';
        $sql .='     a.id = ' . $id;
        $sql .= '   order by ';
        $sql .= "     ( case when b2.ope_item_set_code = '' then 'ZZZZZZ' else b2.ope_item_set_code end ) ";
        $sql .= '     , e.item_name ';
        $sql .= '     , e.standard ';
        $sql .= '     , e.item_code ';
        $sql .= '     , c.facility_sticker_no ';

        
        $result = $this->TrnOpeReturn->query($sql);
        
        $this->set('result' , $result);
        $this->set('id' , $id);
        
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('OpeReturn.token' , $token);
        $this->request->data['OpeReturn']['token'] = $token;
    }
    
    
    function ope_return_result($mode = "regist") {
        if(isset($this->request->data['OpeReturn']['token']) &&
           $this->request->data['OpeReturn']['token'] === $this->Session->read('OpeReturn.token')) {
            $this->Session->delete('OpeReturn.token');
            $now = date('Y/m/d H:i:s.u');
            
            $ope_department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') , array(Configure::read('DepartmentType.opestock')));
            
            $this->TrnOpeReturn->begin();
            $ope_status = Configure::read('OpeStatus.complete');
            $i = 1;
            $j = 1;
            if(isset($this->request->data['ope']['id'])){
                $ope_id = $this->request->data['ope']['id'];
            }else{
                $ope_id = array();
            }
            
            // 中止のときは全選択と同等
            if($mode == 'abort'){
                $ope_id = $this->request->data['sticker']['id'];
                $ope_status = Configure::read('OpeStatus.abort');
            }else if($mode == 'save'){
                $ope_status = Configure::read('OpeStatus.return');
            }
            
            $sticker_id = $this->request->data['sticker']['id'];
            
            $sql  = ' select ';
            $sql .= '       a.mst_department_id ';
            $sql .= '     , b.trn_ope_item_id ';
            $sql .= '     , b1.id as trn_ope_return_id';
            $sql .= "     , to_char(a.work_date, 'YYYY/MM/DD') as work_date ";
            $sql .= '     , c.facility_sticker_no ';
            $sql .= '     , c.id ';
            $sql .= '     , c.mst_item_unit_id';
            $sql .= '     , c.quantity';
            $sql .= '     , c.transaction_price';
            $sql .= '     , c.sales_price';
            $sql .= '     , c.trn_order_id';
            $sql .= '     , a2.facility_code';
            $sql .= '   from ';
            $sql .= '     trn_ope_headers as a  ';
            $sql .= '     left join mst_departments as a1  ';
            $sql .= '       on a1.id = a.mst_department_id  ';
            $sql .= '     left join mst_facilities as a2';
            $sql .= '       on a2.id = a1.mst_facility_id';
            $sql .= '     left join trn_ope_shippings as b  ';
            $sql .= '       on b.trn_ope_header_id = a.id  ';
            $sql .= '     left join trn_ope_returns as b1 ';
            $sql .= '       on b.trn_ope_item_id = b1.trn_ope_item_id ';
            $sql .= '      and b.trn_sticker_id = b1.trn_sticker_id ';
            $sql .= '     left join trn_stickers as c  ';
            $sql .= '       on c.id = b.trn_sticker_id  ';
            $sql .= '   where ';
            $sql .= '     a.id = ' . $this->request->data['TrnOpeHeader']['id'];
            $sql .= '     and c.id in (' . join(',',$this->request->data['sticker']['id']) . ') ';

            $head = $this->TrnOpeHeader->query($sql);
            
            // 手術ヘッダ情報のステータスを更新する。
            $res = $this->TrnOpeHeader->updateAll(array(
                'TrnOpeHeader.ope_status' => $ope_status,
                'TrnOpeHeader.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnOpeHeader.modified'   => "'". $now ."'",
                ), array(
                    'TrnOpeHeader.id' =>$this->request->data['TrnOpeHeader']['id'],
                ), -1);
            
            if(false === $res){
                $this->TrnOpeReturn->rollback();
                $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ope_search');
            }
            
            if((count($sticker_id) - count($ope_id) > 0) && $mode != 'save'){ 
                // 消費枚数が1枚以上の場合
                $work_no = $this->setWorkNo4Header( date('Ymd') , "01");


                $TrnShippingHeader = array(
                    'TrnShippingHeader'=> array(
                        'work_no'            => $work_no,
                        'work_date'          => $head[0][0]['work_date'],
                        'shipping_type'      => Configure::read('ShippingType.fixed'),
                        'shipping_status'    => Configure::read('ShippingStatus.loaded'), //受領済
                        'department_id_from' => $ope_department_id,
                        'department_id_to'   => $head[0][0]['mst_department_id'],
                        'detail_count'       => count($sticker_id) - count($ope_id),
                        'is_deleted'         => false,
                        'creater'            => $this->Session->read('Auth.MstUser.id'),
                        'created'            => $now,
                        'modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'modified'           => $now,
                        )
                    );
                
                $this->TrnShippingHeader->create();
                // SQL実行
                if (!$this->TrnShippingHeader->save($TrnShippingHeader)) {
                    $this->TrnOpeReturn->rollback();
                    $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ope_search');
                }
                
                $shippingHeaderId = $this->TrnShippingHeader->getLastInsertID();
                // 受領ヘッダ作成
                $TrnReceivingHeader = array(
                    'TrnReceivingHeader'=> array(
                        'work_no'                => $work_no,
                        'work_date'              => $head[0][0]['work_date'],
                        'receiving_type'         => Configure::read('ReceivingType.receipt'),
                        'receiving_status'       => Configure::read('ReceivingStatus.stock'),
                        'department_id_from'     => $ope_department_id,
                        'department_id_to'       => $head[0][0]['mst_department_id'],
                        'detail_count'           => count($sticker_id) - count($ope_id),
                        'creater'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                => $now,
                        'trn_shipping_header_id' => $shippingHeaderId,//出荷ヘッダ参照キー
                        'modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'modified'               => $now,
                        )
                    );
                
                $this->TrnReceivingHeader->create();
                // SQL実行
                if (!$this->TrnReceivingHeader->save($TrnReceivingHeader)) {
                    $this->TrnOpeReturn->rollback();
                    $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ope_search');
                }
                
                $receivingHeaderId = $this->TrnReceivingHeader->getLastInsertID();
                
                // 消費ヘッダ
                $TrnConsumeHeader = array(
                    'TrnConsumeHeader' => array(
                        'work_no'           => $work_no,
                        'work_date'         => $head[0][0]['work_date'],
                        'use_type'          => Configure::read('UseType.ope'),     //消費区分：オペセット使用
                        'mst_department_id' => $head[0][0]['mst_department_id'],   //部署参照キー
                        'detail_count'      => count($sticker_id) - count($ope_id),
                        'creater'           => $this->Session->read('Auth.MstUser.id'),
                        'created'           => $now,
                        'modifier'          => $this->Session->read('Auth.MstUser.id'),
                        'modified'          => $now,
                        ));
                
                $this->TrnConsumeHeader->create();
                if(!$this->TrnConsumeHeader->save($TrnConsumeHeader)) {
                    $this->TrnOpeReturn->rollback();
                    $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ope_search');
                }
                
                $consumeHeaderId = $this->TrnConsumeHeader->getLastInsertID();
            }
            
            if($mode == 'save') {
                // いったん削除
                $this->TrnOpeReturn->create();
                $this->TrnOpeReturn->updateAll(array(
                    'TrnOpeReturn.is_deleted' => true,
                    'TrnOpeReturn.modifier'   => $this->Session->read('Auth.MstUser.id'),
                    'TrnOpeReturn.modified'   => "'" . $now ."'",
                    ),array(
                        'TrnOpeReturn.trn_ope_header_id' => $this->request->data['TrnOpeHeader']['id'],
                    ),-1);
                foreach($head as $h){
                    if( $this->checkReturnId($h[0]['id'] , $ope_id )){
                        if(empty($h[0]['trn_ope_return_id'])){
                            // 返却明細がない場合は新規追加
                            $TrnOpeReturn = array(
                                'TrnOpeReturn' => array(
                                    'trn_ope_header_id'  => $this->request->data['TrnOpeHeader']['id'],
                                    'work_seq'           => $j,
                                    'trn_ope_item_id'    => $h[0]['trn_ope_item_id'],
                                    'trn_sticker_id'     => $h[0]['id'],
                                    'is_deleted'         => false,
                                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                                    'created'            => $now,
                                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                                    'modified'           => $now,
                                    )
                                );
                            $this->TrnOpeReturn->create();
                            if(!$this->TrnOpeReturn->save($TrnOpeReturn)){
                                $this->TrnOpeReturn->rollback();
                                $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                                $this->redirect('ope_search');
                            }
                        }else{
                            // 返却明細がある場合は更新
                            $this->TrnOpeReturn->create();
                            $this->TrnOpeReturn->updateAll(array(
                                'TrnOpeReturn.work_seq'   => $j,
                                'TrnOpeReturn.is_deleted' => false,
                                'TrnOpeReturn.modifier'   => $this->Session->read('Auth.MstUser.id'),
                                'TrnOpeReturn.modified'   => "'" . $now ."'",
                                ),array(
                                    'TrnOpeReturn.id' => $h[0]['trn_ope_return_id'],
                               ),-1);
                            
                        }
                    }
                }
            }else{
                // いったん削除
                $this->TrnOpeReturn->create();
                $this->TrnOpeReturn->updateAll(array(
                    'TrnOpeReturn.is_deleted' => true,
                    'TrnOpeReturn.modifier'   => $this->Session->read('Auth.MstUser.id'),
                    'TrnOpeReturn.modified'   => "'" . $now ."'",
                    ),array(
                        'TrnOpeReturn.trn_ope_header_id' => $this->request->data['TrnOpeHeader']['id'],
                    ),-1);
                
                foreach($head as $h){
                    if( !$this->checkReturnId($h[0]['id'] , $ope_id )){
                        // 病院シール番号発版
                        $hospital_sticker_no = $this->getHospitalStickerNo($h[0]['work_date'],$h[0]['facility_code'],$this->Session->read('Auth.facility_id_selected'));
                        
                        // 出荷明細
                        $TrnShipping = array(
                            'TrnShipping'=> array(
                                'work_no'                => $work_no,
                                'work_seq'               => $i,
                                'work_date'              => $h[0]['work_date'],
                                'shipping_type'          => Configure::read('ShippingType.fixed'),
                                'shipping_status'        => Configure::read('ShippingStatus.loaded'), //受領済
                                'mst_item_unit_id'       => $h[0]['mst_item_unit_id'],
                                'department_id_from'     => $ope_department_id,
                                'department_id_to'       => $h[0]['mst_department_id'],
                                'quantity'               => $h[0]['quantity'],
                                'trn_sticker_id'         => $h[0]['id'],
                                'is_deleted'             => false,
                                'creater'                => $this->Session->read('Auth.MstUser.id'),
                                'created'                => $now,
                                'modifier'               => $this->Session->read('Auth.MstUser.id'),
                                'modified'               => $now,
                                'trn_shipping_header_id' => $shippingHeaderId,
                                'facility_sticker_no'    => $h[0]['facility_sticker_no'],
                                'hospital_sticker_no'    => $hospital_sticker_no,
                                )
                            );
                        
                        $this->TrnShipping->create();
                        // SQL実行
                        if (!$this->TrnShipping->save($TrnShipping)) {
                            $this->TrnOpeReturn->rollback();
                            $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('ope_search');
                        }
                        $shippingId = $this->TrnShipping->getLastInsertID();
                        
                        
                        // 受領明細
                        $TrnReceiving = array(
                            'TrnReceiving' => array(
                                'work_no'                 => $work_no,
                                'work_seq'                => $i,
                                'receiving_type'          => Configure::read('ReceivingType.receipt'),    //入荷区分:受領による入荷
                                'receiving_status'        => Configure::read('ShippingStatus.loaded'),    //入荷状態（受領済み）
                                'mst_item_unit_id'        => $h[0]['mst_item_unit_id'],        //包装単位参照キー
                                'department_id_from'      => $ope_department_id,
                                'department_id_to'        => $h[0]['mst_department_id'],
                                'quantity'                => $h[0]['quantity'],                //数量
                                'trade_type'              => Configure::read('ClassesType.Temporary') ,   //臨時品固定
                                'stocking_price'          => $h[0]['transaction_price'],       //仕入れ価格
                                'sales_price'             => $h[0]['sales_price'],             //売上価格
                                'trn_sticker_id'          => $h[0]['id'],                      //シール参照キー
                                'trn_shipping_id'         => $shippingId,                              //出荷参照キー
                                'trn_order_id'            => $h[0]['trn_order_id'],            //発注参照キー
                                'trn_receiving_header_id' => $receivingHeaderId,                          //入荷ヘッダ外部キー
                                'facility_sticker_no'     => $h[0]['facility_sticker_no'],
                                'hospital_sticker_no'     => $hospital_sticker_no,
                                'creater'                 => $this->Session->read('Auth.MstUser.id'),
                                'modifier'                => $this->Session->read('Auth.MstUser.id'),
                                'created'                 => $now,
                                'modified'                => $now,
                                )
                            );
                        
                        $this->TrnReceiving->create();
                        // SQL実行
                        if (!$this->TrnReceiving->save($TrnReceiving)) {
                            $this->TrnOpeReturn->rollback();
                            $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('ope_search');
                        }
                        
                        $receivingId = $this->TrnReceiving->getLastInsertID();
                        
                        // 消費明細
                        $TrnConsume = array(
                            'TrnConsume' => array(
                                'work_no'               => $work_no,
                                'work_seq'              => $i,
                                'work_date'             => $h[0]['work_date'],
                                'use_type'              => Configure::read('UseType.ope'), //消費区分：オペセット使用
                                'mst_item_unit_id'      => $h[0]['mst_item_unit_id'],      //包装単位参照キー
                                'mst_department_id'     => $h[0]['mst_department_id'],     //部署参照キー
                                'quantity'              => $h[0]['quantity'],              //数量
                                'trn_sticker_id'        => $h[0]['id'],                    //シール参照キー
                                'trn_consume_header_id' => $consumeHeaderId,               //消費ヘッダ外部キー
                                'is_retroactable'       => false,
                                'creater'               => $this->Session->read('Auth.MstUser.id'),
                                'created'               => $now,
                                'modifier'              => $this->Session->read('Auth.MstUser.id'),
                                'modified'              => $now,
                                'is_deleted'            => false,
                                'facility_sticker_no'   => $h[0]['facility_sticker_no'],
                                'hospital_sticker_no'   => $hospital_sticker_no,
                                ));
                        
                        
                        $this->TrnConsume->create();
                        if(!$this->TrnConsume->save($TrnConsume)){
                            $this->TrnOpeReturn->rollback();
                            $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('ope_search');
                        }
                        
                        $consumeId = $this->TrnConsume->getLastInsertID();
                        
                        // 在庫減
                        $this->TrnStock->create();
                        $c = $this->TrnStock->updateAll(
                            array(
                                'TrnStock.reserve_count' => 'TrnStock.reserve_count - 1 ',
                                'TrnStock.stock_count'   => 'TrnStock.stock_count - 1 ',
                                'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                                'TrnStock.modified'      => "'" . $now . "'"
                                ),
                            array(
                                'TrnStock.id' => $this->getStockId($h[0]['id']),
                                ),
                            -1
                            );
                        if(!$c){
                            $this->TrnOpeReturn->rollback();
                            $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('ope_search');
                        }
                        
                        // シール更新
                        $this->TrnSticker->create();
                        $res = $this->TrnSticker->updateAll(array(
                            'TrnSticker.hospital_sticker_no' => $hospital_sticker_no,
                            'TrnSticker.has_reserved'        => 'false',
                            'TrnSticker.trn_consume_id'      => $consumeId,
                            'TrnSticker.trn_shipping_id'     => $shippingId,
                            'TrnSticker.receipt_id'          => $receivingId,
                            'TrnSticker.mst_department_id'   => $h[0]['mst_department_id'],
                            'TrnSticker.quantity'            => 0,
                            'TrnSticker.modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'TrnSticker.modified'            => "'". $now ."'",
                            ), array(
                                'TrnSticker.id' =>$h[0]['id'],
                                ), -1);
                        
                        if(false === $res){
                            $this->TrnOpeReturn->rollback();
                            $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('ope_search');
                        }
                        
                        // 売上作成
                        $claim_id = $this->createSalesData($h[0]['id'],$h[0]['mst_department_id'],$h[0]['work_date']);
                        
                        // シール移動履歴
                        $TrnStickerRecord = array(
                            'TrnStickerRecord' => array(
                                'move_date'           => $h[0]['work_date'],
                                'move_seq'            => $this->getNextRecord($h[0]['id']),
                                'record_type'         => Configure::read('RecordType.consume'),
                                'record_id'           => $consumeId,
                                'trn_sticker_id'      => $h[0]['id'],
                                'mst_department_id'   => $h[0]['mst_department_id'],
                                'facility_sticker_no' => $h[0]['facility_sticker_no'],
                                'hospital_sticker_no' => $hospital_sticker_no,
                                'is_deleted'          => false,
                                'creater'             => $this->Session->read('Auth.MstUser.id'),
                                'created'             => $now,
                                'modifier'            => $this->Session->read('Auth.MstUser.id'),
                                'modified'            => $now,
                                )
                            );
                        $this->TrnStickerRecord->create();
                        if(!$this->TrnStickerRecord->save($TrnStickerRecord)){
                            $this->TrnOpeReturn->rollback();
                            $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('ope_search');
                        }
                        $i++;
                    }else{
                        // オペ返却
                        $TrnOpeReturn = array(
                            'TrnOpeReturn' => array(
                                'trn_ope_header_id'  => $this->request->data['TrnOpeHeader']['id'],
                                'work_seq'           => $j,
                                'trn_ope_item_id'    => $h[0]['trn_ope_item_id'],
                                'trn_sticker_id'     => $h[0]['id'],
                                'is_deleted'         => false,
                                'creater'            => $this->Session->read('Auth.MstUser.id'),
                                'created'            => $now,
                                'modifier'           => $this->Session->read('Auth.MstUser.id'),
                                'modified'           => $now,
                                )
                            );
                        $this->TrnOpeReturn->create();
                        if(!$this->TrnOpeReturn->save($TrnOpeReturn)){
                            $this->TrnOpeReturn->rollback();
                            $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('ope_search');
                        }
                        
                        // シールを予約状態を解除する。
                        $this->TrnSticker->create();
                        $res = $this->TrnSticker->updateAll(array(
                            'TrnSticker.has_reserved' => 'false',
                            'TrnSticker.modifier'     => $this->Session->read('Auth.MstUser.id'),
                            'TrnSticker.modified'     => "'". $now ."'",
                            ), array(
                                'TrnSticker.id' =>$h[0]['id'],
                                ), -1);
                        
                        if(false === $res){
                            $this->TrnOpeReturn->rollback();
                            $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('ope_search');
                        }
                        
                        // 在庫予約数▽
                        $this->TrnStock->create();
                        $c = $this->TrnStock->updateAll(
                            array(
                                'TrnStock.reserve_count' => 'TrnStock.reserve_count - 1 ',
                                'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                'TrnStock.modified'    => "'" . $now . "'"
                                ),
                            array(
                                'TrnStock.id' => $this->getStockId($h[0]['id']),
                                ),
                            -1
                            );
                        if(!$c){
                            $this->TrnOpeReturn->rollback();
                            $this->Session->setFlash('手術返却登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('ope_search');
                        }
                        
                        $j++;
                    }
                }
            }
            $this->TrnOpeReturn->commit();
        } else {
            
        }
    }
    
    /**
     * 術式検索 Ajax用
     */
    public function search_method($field = null, $value = null) {
        App::import('Sanitize');
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($field) && isset($value)) {
            $_name = ($_POST['value']!='')? $_POST['value']:$value;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $_ret = array();

                //検索条件に該当する件数の取得
                $sql = "select count(*) from mst_ope_method_names as a ";
                $sql .= ' where 1=1';
                if (isset($_name)) {
                    $sql .= " and ( a.ope_method_name LIKE '%". Sanitize::clean($_name) . "%'";
                    $sql .= " or a.ope_method_set_code LIKE '%". Sanitize::clean($_name) . "%' ) ";
                }
                $sql .= ' and a.is_deleted = false ';
                $sql .= " and a.mst_facility_id = " . $this->Session->read('Auth.facility_id_selected');
                

                $ret = $this->MstOpeMethodName->query($sql , false);
                $_counts = $ret[0][0]['count'];
                if ($_counts > 50) {
                    $_ret[0]['msg'] = '検索結果が50件以上存在します['.$_counts.'件'.']';
                }

                $sql  = ' select ';
                $sql .= '       a.id ';
                $sql .= '     , a.ope_method_name      as method_name ';
                $sql .= '     , a.ope_method_set_code  as method_code';
                $sql .= '   from ';
                $sql .= '     mst_ope_method_names as a  ';
                $sql .= '   where ';
                $sql .= "     ( a.ope_method_name like '%" . $_name . "%' ";
                $sql .= "     or a.ope_method_set_code like '%" . $_name . "%' ) ";
                $sql .= "     and a.is_deleted = false ";
                $sql .= "     and a.mst_facility_id = " . $this->Session->read('Auth.facility_id_selected');
                $sql .= " limit 50";
                $_result = $this->MstOpeMethodName->query($sql);
                $_counts = count($_result);
                for ($_i = 0;$_i < $_counts;$_i++) {
                    $_ret[$_i]['id']          = $_result[$_i][0]['id'];
                    $_ret[$_i]['method_code'] = $_result[$_i][0]['method_code'];
                    $_ret[$_i]['method_name'] = $_result[$_i][0]['method_name'];
                }
                return (json_encode($_ret));
            } else {
                throw new Exception('error');
            }
        }
    }

    /**
     * 材料検索 Ajax用
     */
    public function get_item_set($field = null, $value = null) {
        App::import('Sanitize');
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($field) && isset($value)) {
            $_code = ($_POST['value']!='')? $_POST['value']:$value;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $_ret = array();

                $sql  = ' select ';
                $sql .= '       b.id';
                $sql .= '     , a.ope_item_set_code';
                $sql .= '     , a.ope_item_set_name';
                $sql .= '     , c.internal_code';
                $sql .= "     , c.item_name ";
                $sql .= '     , c.standard ';
                $sql .= '     , c.item_code ';
                $sql .= '     , a1.quantity ';
                $sql .= "     , coalesce(a1.recital,'') as recital ";
                $sql .= '   from ';
                $sql .= '     mst_ope_method_names as x ';
                $sql .= '     left join mst_ope_method_sets as y ';
                $sql .= '       on y.mst_ope_method_name_id = x.id ';
                $sql .= '     left join mst_ope_item_set_headers as a ';
                $sql .= '       on a.id = y.mst_ope_item_set_header_id ';
                $sql .= '     left join mst_ope_item_sets as a1 ';
                $sql .= '       on a.id =  a1.mst_ope_item_set_header_id ';
                $sql .= '     left join mst_item_units as b ';
                $sql .= '       on b.id = a1.mst_item_unit_id ';
                $sql .= '     left join mst_facility_items as c ';
                $sql .= '       on c.id = b.mst_facility_item_id ';
                $sql .= '   where ';
                $sql .= "     x.ope_method_set_code = '" . $_code . "'";
                $sql .= '     and a.is_deleted = false ';
                $sql .= '     and a1.is_deleted = false ';
                $sql .= '     and y.is_deleted = false ';
                $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
                $sql .= '   order by a.ope_item_set_code , c.item_name , c.standard , c.item_code ';

                $_result = $this->MstOpeMethodSet->query($sql);
                $_counts = count($_result);
                for ($_i = 0;$_i < $_counts;$_i++) {
                    $_ret[$_i]['id']            = $_result[$_i][0]['id'];
                    $_ret[$_i]['code']          = $_result[$_i][0]['ope_item_set_code'];
                    $_ret[$_i]['name']          = $_result[$_i][0]['ope_item_set_name'];
                    $_ret[$_i]['internal_code'] = $_result[$_i][0]['internal_code'];
                    $_ret[$_i]['item_name']     = $_result[$_i][0]['item_name'];
                    $_ret[$_i]['standard']      = $_result[$_i][0]['standard'];
                    $_ret[$_i]['item_code']     = $_result[$_i][0]['item_code'];
                    $_ret[$_i]['quantity']      = $_result[$_i][0]['quantity'];
                    $_ret[$_i]['recital']       = $_result[$_i][0]['recital'];
                }
                return (json_encode($_ret));
            } else {
                throw new Exception('error');
            }
        }
    }


    /**
     * 材料検索 Ajax用
     */
    public function get_item_set_one($field = null, $value = null) {
        App::import('Sanitize');
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($field) && isset($value)) {
            $_code = ($_POST['value']!='')? $_POST['value']:$value;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $_ret = array();

                $sql  = ' select ';
                $sql .= '       b.id';
                $sql .= '     , a.ope_item_set_code';
                $sql .= '     , a.ope_item_set_name';
                $sql .= '     , c.internal_code';
                $sql .= "     , c.item_name ";
                $sql .= '     , c.standard ';
                $sql .= '     , c.item_code ';
                $sql .= '     , a1.quantity ';
                $sql .= "     , coalesce(a1.recital,'') as recital ";
                $sql .= '   from ';
                $sql .= '     mst_ope_item_set_headers as a ';
                $sql .= '     left join mst_ope_item_sets as a1 ';
                $sql .= '       on a.id = a1.mst_ope_item_set_header_id ';
                $sql .= '     left join mst_item_units as b ';
                $sql .= '       on b.id = a1.mst_item_unit_id ';
                $sql .= '     left join mst_facility_items as c ';
                $sql .= '       on c.id = b.mst_facility_item_id ';
                $sql .= '   where ';
                $sql .= "     a.ope_item_set_code = '" . $_code . "'";
                $sql .= '     and a1.is_deleted = false ';
                $sql .= '     and a1.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
                $sql .= '   order by a.ope_item_set_code , c.item_name , c.standard , c.item_code ';

                $_result = $this->MstOpeMethodSet->query($sql);
                $_counts = count($_result);
                for ($_i = 0;$_i < $_counts;$_i++) {
                    $_ret[$_i]['id']            = $_result[$_i][0]['id'];
                    $_ret[$_i]['code']          = $_result[$_i][0]['ope_item_set_code'];
                    $_ret[$_i]['name']          = $_result[$_i][0]['ope_item_set_name'];
                    $_ret[$_i]['internal_code'] = $_result[$_i][0]['internal_code'];
                    $_ret[$_i]['item_name']     = $_result[$_i][0]['item_name'];
                    $_ret[$_i]['standard']      = $_result[$_i][0]['standard'];
                    $_ret[$_i]['item_code']     = $_result[$_i][0]['item_code'];
                    $_ret[$_i]['quantity']      = $_result[$_i][0]['quantity'];
                    $_ret[$_i]['recital']       = $_result[$_i][0]['recital'];
                }
                return (json_encode($_ret));
            } else {
                throw new Exception('error');
            }
        }
    }
    

    /**
     * 物品検索 Ajax用
     */
    public function get_item($field = null, $value = null) {
        App::import('Sanitize');
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($field) && isset($value)) {
            $_code = ($_POST['value']!='')? $_POST['value']:$value;
            
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $_ret = array();

                $sql = "select count(*) from mst_facility_items as a ";
                $sql .= ' where 1=1';
                if(!empty($_code)){
                    $sql .= "     and ( a.internal_code like '%" . $_code . "%'";
                    $sql .= "     or a.item_name like '%" . $_code . "%'";
                    $sql .= "     or a.item_code like '%" . $_code . "%'";
                    $sql .= "     or a.standard like '%" . $_code . "%' ) ";
                }
                $sql .= ' and a.is_deleted = false ';

                $ret = $this->MstFacilityItem->query($sql , false);
                $_counts = $ret[0][0]['count'];
                if ($_counts > 50) {
                    $_ret[0]['msg'] = '検索結果が50件以上存在します['.$_counts.'件'.']';
                }
                
                
                $sql  = ' select ';
                $sql .= '       b.id ';
                $sql .= '     , a.internal_code ';
                $sql .= '     , a.item_name ';
                $sql .= '     , a.standard ';
                $sql .= '     , a.item_code  ';
                $sql .= '   from ';
                $sql .= '     mst_facility_items as a  ';
                $sql .= '     left join mst_item_units as b  ';
                $sql .= '       on b.mst_facility_item_id = a.id  ';
                $sql .= '   where ';
                $sql .= '     a.is_deleted = false  ';
                $sql .= '     and b.per_unit = 1  ';
                if(!empty($_code)){
                    $sql .= "     and ( a.internal_code like '%" . $_code . "%'";
                    $sql .= "     or a.item_name like '%" . $_code . "%'";
                    $sql .= "     or a.item_code like '%" . $_code . "%'";
                    $sql .= "     or a.standard like '%" . $_code . "%' ) ";
                }
                $sql .= ' limit 50 ';
                $_result = $this->MstFacilityItem->query($sql);
                $_counts = count($_result);
                for ($_i = 0;$_i < $_counts;$_i++) {
                    $_ret[$_i]['id']            = $_result[$_i][0]['id'];
                    $_ret[$_i]['internal_code'] = $_result[$_i][0]['internal_code'];
                    $_ret[$_i]['item_name']     = $_result[$_i][0]['item_name'];
                    $_ret[$_i]['standard']      = $_result[$_i][0]['standard'];
                    $_ret[$_i]['item_code']     = $_result[$_i][0]['item_code'];
                }
                return (json_encode($_ret));
            } else {
                throw new Exception('error');
            }
        }
    }

    /**
     * 材料セット一覧検索 Ajax用
     */
    public function get_item_set_list($field = null, $value = null) {
        App::import('Sanitize');
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($field) && isset($value)) {
            $_code = ($_POST['value']!='')? $_POST['value']:$value;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $_ret = array();

                $sql  = ' select ';
                $sql .= '       a.ope_item_set_code ';
                $sql .= '     , a.ope_item_set_name  ';
                $sql .= '   from ';
                $sql .= '     mst_ope_item_set_headers as a  ';
                $sql .='   where ';
                $sql .='     a.is_deleted = false ';
                $sql .='     and a.mst_facility_id =' . $this->Session->read('Auth.facility_id_selected');
                if(!empty($_code)){
                    $sql .="     and ( a.ope_item_set_code like '%" . $_code . "%'";
                    $sql .="     or a.ope_item_set_code like '%" . $_code .  "%' ) ";
                }
                $sql .= '   order by ';
                $sql .= '     a.ope_item_set_code ';
                
                $_result = $this->MstOpeMethodSet->query($sql);
                $_counts = count($_result);
                for ($_i = 0;$_i < $_counts;$_i++) {
                    $_ret[$_i]['code'] = $_result[$_i][0]['ope_item_set_code'];
                    $_ret[$_i]['name'] = $_result[$_i][0]['ope_item_set_name'];
                }
                return (json_encode($_ret));
            } else {
                throw new Exception('error');
            }
        }
    }

    /**
     * プルダウンデータ作成
     */
    private function getPullDown(){
        // 部屋
        $sql  = 'select ';
        $sql .= '      a.id            as "MstOpeRoom__id"';
        $sql .= '    , a.ope_room_code as "MstOpeRoom__ope_room_code"';
        $sql .= '  from';
        $sql .= '    mst_ope_rooms as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.ope_room_code ';
        
        $this->set('room' , Set::Combine(
            $this->MstOpeRoom->query($sql),
            "{n}.MstOpeRoom.id",
            "{n}.MstOpeRoom.ope_room_code"
            ));

        //術者
        $sql  = 'select ';
        $sql .= '      a.id              as "MstOpePerson__id"';
        $sql .= '    , a.ope_person_name as "MstOpePerson__ope_person_name"';
        $sql .= '  from';
        $sql .= '    mst_ope_people as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.id ';
        $this->set('person' , Set::Combine(
            $this->MstOpePerson->query($sql),
            "{n}.MstOpePerson.id",
            "{n}.MstOpePerson.ope_person_name"
            ));
        
        // 麻酔
        $sql  = 'select ';
        $sql .= '      a.id                  as "MstOpeAnesthesia__id"';
        $sql .= '    , a.ope_anesthesia_name as "MstOpeAnesthesia__ope_anesthesia_name"';
        $sql .= '  from';
        $sql .= '    mst_ope_anesthesias as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.ope_anesthesia_code';
        $this->set('anesthesia' , Set::Combine(
            $this->MstOpeAnesthesia->query($sql),
            "{n}.MstOpeAnesthesia.id",
            "{n}.MstOpeAnesthesia.ope_anesthesia_name"
            ));
        
        // 体位
        $sql  = 'select ';
        $sql .= '      a.id                as "MstOpePosition__id"';
        $sql .= '    , a.ope_position_name as "MstOpePosition__ope_position_name"';
        $sql .= '  from';
        $sql .= '    mst_ope_positions as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.ope_position_code ';
        $this->set('position', Set::Combine(
            $this->MstOpePosition->query($sql),
            "{n}.MstOpePosition.id",
            "{n}.MstOpePosition.ope_position_name"
            ));

        //病棟
        $sql  = 'select ';
        $sql .= '      a.id              as "MstOpeWard__id"';
        $sql .= '    , a.department_name as "MstOpeWard__ope_ward_name"';
        $sql .= '  from';
        $sql .= '    mst_departments as a ';
        $sql .= '    left join mst_facility_relations as b';
        $sql .= '    on b.partner_facility_id = a.mst_facility_id';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '    and a.department_type = ' . Configure::read('DepartmentType.hospital');
        $sql .= '  order by a.department_code ';
        $this->set('ward' , Set::Combine(
            $this->MstKatheWard->query($sql),
            "{n}.MstOpeWard.id",
            "{n}.MstOpeWard.ope_ward_name"
            ));

        //科
        $sql  = 'select ';
        $sql .= '      a.id                  as "MstOpeDepartment__id"';
        $sql .= '    , a.ope_department_name as "MstOpeDepartment__ope_department_name"';
        $sql .= '  from';
        $sql .= '    mst_ope_departments as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.ope_department_code ';
        $this->set('department' , Set::Combine(
            $this->MstOpeDepartment->query($sql),
            "{n}.MstOpeDepartment.id",
            "{n}.MstOpeDepartment.ope_department_name"
            ));

    }
    
    
    public function report($mode){
        if(isset($this->request->data['TrnOpeHeader']['id'])){
            $id = $this->request->data['TrnOpeHeader']['id'];
            if(empty($id)){
                return false;
            }
        }
        switch($mode){
          case 'picking':
            $this->print_pickingList($id);
            break;
          case 'shipping':
            $this->print_shippingList($id);
            break;
          case 'return':
            $this->print_returnList($id);
            break;
          default:
            break;
        }
    }

    
    // ピッキングリスト印刷
    public function print_pickingList($id){
        $now = date("Y/m/d H:i:s");
        $today = date("Y/m/d");

        $sql  = ' select ';
        $sql .= "       to_char(a.work_date, 'YYYY年MM月DD日') as work_date ";
        $sql .= '     , a.start_time ';
        $sql .= '     , c.ope_room_code ';
        $sql .= '     , d.ope_method_set_code ';
        $sql .= '     , d.ope_method_name ';
        $sql .= '     , e.ope_position_name ';
        $sql .= '     , f.ope_anesthesia_name ';
        $sql .= '     , a.number ';
        $sql .= '     , (  ';
        $sql .= '       case a.sex  ';
        $sql .= "         when '1' then 'M' ";
        $sql .= "         when '2' then 'F' ";
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                                        as sex ';
        $sql .= '     , a.age ';
        $sql .= '     , a.name ';
        $sql .= '     , a.work_no ';
        $sql .= '     , h.internal_code ';
        $sql .= '     , h.item_name ';
        $sql .= '     , h.standard ';
        $sql .= '     , h.item_code ';
        $sql .= '     , b.quantity ';
        $sql .= '     , g1.unit_name ';
        $sql .= '     , b.recital ';
        $sql .= '     , i.name                                 as location ';
        $sql .= '     , b.ope_item_set_code ';
        $sql .= '     , d1.ope_item_set_name ';
        $sql .= '     , a.ope_method_comment';
        $sql .= '   from ';
        $sql .= '     trn_ope_headers as a  ';
        $sql .= '     left join trn_ope_items as b  ';
        $sql .= '       on b.trn_ope_header_id = a.id  ';
        $sql .= '      and b.is_deleted = false ';
        $sql .= '     left join mst_ope_rooms as c  ';
        $sql .= '       on c.id = a.mst_ope_room_id  ';
        $sql .= '     left join mst_ope_method_names as d  ';
        $sql .= '       on d.id = a.mst_ope_method_id  ';
        $sql .= '     left join mst_ope_item_set_headers as d1  ';
        $sql .= '       on d1.ope_item_set_code = b.ope_item_set_code  ';
        $sql .= '      and d1.is_deleted = false ';
        $sql .= '     left join mst_ope_positions as e  ';
        $sql .= '       on e.id = a.mst_ope_position_id  ';
        $sql .= '     left join mst_ope_anesthesias as f  ';
        $sql .= '       on f.id = a.mst_ope_anesthesia_id  ';
        $sql .= '     left join mst_item_units as g  ';
        $sql .= '       on g.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as g1  ';
        $sql .= '       on g1.id = g.mst_unit_name_id  ';
        $sql .= '     left join mst_facility_items as h  ';
        $sql .= '       on h.id = g.mst_facility_item_id  ';
        $sql .= '     left join (  ';
        $sql .= '       select ';
        $sql .= '             x.mst_item_unit_id ';
        $sql .= '           , k.name  ';
        $sql .= '         from ';
        $sql .= '           mst_fixed_counts as x  ';
        $sql .= '           inner join mst_departments as j  ';
        $sql .= '             on j.id = x.mst_department_id  ';
        $sql .= '             and j.department_type = ' . Configure::read('DepartmentType.opestock');
        $sql .= '           left join mst_shelf_names as k  ';
        $sql .= '             on k.id = x.mst_shelf_name_id ';
        $sql .= '     ) as i  ';
        $sql .= '       on i.mst_item_unit_id = g.id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $id;
        $sql .= '   order by ';
        $sql .= "     ( case when b.ope_item_set_code = '' then 'ZZZZZZ' else b.ope_item_set_code end ) ";
        $sql .= '     , h.item_name ';
        $sql .= '     , h.standard ';
        $sql .= '     , h.item_code ';

        $result = $this->TrnOpeHeader->query($sql);
        $columns = array(
            '術式コード',
            '術式名',
            'セットバーコード',
            'セットバーコードガイド',
            '体位',
            '麻酔',
            '術式',
            '手術日',
            '開始時間',
            '患者番号',
            '性別',
            '年齢',
            '患者名',
            '室番号',
            '備考',
            'セットコード',
            'セット名称',
            'ロケーション',
            '略称',
            '材料名',
            '規格',
            '製品番号',
            '数量',
            'ページ番号',
            '印刷日',
            '範囲'
            );
        $i=1;
        $break = '';
        foreach($result as $r){
            if($break != $r[0]['ope_item_set_code']){
                $break = $r[0]['ope_item_set_code'];
            }
            
            $data[] = array(
                $r[0]['ope_method_set_code'],   // 術式コード
                $r[0]['ope_method_name'],       // 術式名
                $r[0]['work_no'],               // セットバーコード
                $r[0]['work_no'],               // セットバーコードガイド
                $r[0]['ope_position_name'],     // 体位
                $r[0]['ope_anesthesia_name'],   // 麻酔
                $r[0]['ope_method_name'],       // 術式
                $r[0]['work_date'],             // 手術日
                $r[0]['start_time'],            // 開始時間
                $r[0]['number'],                // 患者番号
                $r[0]['sex'],                   // 性別
                $r[0]['age'],                   // 年齢
                $r[0]['name'],                  // 患者名
                $r[0]['ope_room_code'] ,        // 室番号
                $r[0]['recital'] ,              // 備考
                $r[0]['ope_item_set_code'],     // セットコード
                $r[0]['ope_item_set_name'],     // セット名称
                $r[0]['location'],              // ロケーション
                $r[0]['internal_code'],         // 略称
                $r[0]['item_name'],             // 材料名
                $r[0]['standard'],              // 規格
                $r[0]['item_code'],             // 製品番号
                $r[0]['quantity'] . $r[0]['unit_name'], // 数量
                $i,                             // ページ番号
                $today,                         // 印刷日
                $break
                );
            $i++;
        }
        
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'OPセットピッキングリスト','備考1'=>$result[0][0]['ope_method_comment']);
        $this->xml_output($data,join("\t",$columns),$layout_name['36'],$fix_value);
    }
    
    // 払出リスト印刷
    public function print_shippingList($id){
        $today = date("Y/m/d");
        
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , a.work_no ';
        $sql .= "     , to_char(a.work_date, 'YYYY/MM/DD') as work_date ";
        $sql .= '     , a.start_time ';
        $sql .= '     , e.ope_room_code ';
        $sql .= '     , f.ope_method_set_code ';
        $sql .= '     , f.ope_method_name ';
        $sql .= '     , d.ope_position_name ';
        $sql .= '     , c.ope_anesthesia_name ';
        $sql .= '     , a.name ';
        $sql .= '     , a.number ';
        $sql .= '     , a.age ';
        $sql .= '     , (  ';
        $sql .= '       case a.sex  ';
        $sql .= "         when '1' then 'M'";
        $sql .= "         when '2' then 'F'";
        $sql .= '         end ';
        $sql .= '     )                                    as sex ';
        $sql .= '     , i.internal_code ';
        $sql .= '     , i.item_name ';
        $sql .= '     , i.standard ';
        $sql .= '     , i.item_code ';
        $sql .= '     , (  ';
        $sql .= '       case i.enable_medicalcode  ';
        $sql .= "         when null then '' ";
        $sql .= "         when 0 then '' ";
        $sql .= "         when 1 then '★' ";
        $sql .= "         when 2 then '★' ";
        $sql .= '         end ';
        $sql .= '     )                                    as refund ';
        $sql .= '     , g.quantity ';
        $sql .= '     , h1.unit_name ';
        $sql .= '     , g.facility_sticker_no ';
        $sql .= '     , b2.ope_item_set_code ';
        $sql .= '     , b2.ope_item_set_name ';
        $sql .= '     , j.department_name ';
        $sql .= "     , a.ope_method_comment";
        $sql .= '   from ';
        $sql .= '     trn_ope_headers as a  ';
        $sql .= '     left join trn_ope_shippings as b  ';
        $sql .= '       on b.trn_ope_header_id = a.id  ';
        $sql .= '     left join trn_ope_items as b1  ';
        $sql .= '       on b1.id = b.trn_ope_item_id  ';
        $sql .= '     left join mst_ope_item_set_headers as b2  ';
        $sql .= '       on b2.ope_item_set_code = b1.ope_item_set_code  ';
        $sql .= '      and b2.is_deleted = false ';
        $sql .= '     left join mst_ope_anesthesias as c  ';
        $sql .= '       on c.id = a.mst_ope_anesthesia_id  ';
        $sql .= '     left join mst_ope_positions as d  ';
        $sql .= '       on d.id = a.mst_ope_position_id  ';
        $sql .= '     left join mst_ope_rooms as e  ';
        $sql .= '       on e.id = a.mst_ope_room_id  ';
        $sql .= '     left join mst_ope_method_names as f  ';
        $sql .= '       on f.id = a.mst_ope_method_id  ';
        $sql .= '     left join trn_stickers as g  ';
        $sql .= '       on g.id = b.trn_sticker_id  ';
        $sql .= '     left join mst_item_units as h  ';
        $sql .= '       on h.id = g.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as h1  ';
        $sql .= '       on h1.id = h.mst_unit_name_id  ';
        $sql .= '     left join mst_facility_items as i  ';
        $sql .= '       on i.id = h.mst_facility_item_id  ';
        $sql .= '     left join mst_departments as j  ';
        $sql .= '       on j.id = a.mst_department_id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $id;
        $sql .= '     and b1.is_deleted = false ';
        $sql .= '   order by ';
        $sql .= "     ( case when b2.ope_item_set_code = '' then 'ZZZZZZ' else b2.ope_item_set_code end ) ";
        $sql .= '     , i.item_name ';
        $sql .= '     , i.standard ';
        $sql .= '     , i.item_code ';
        $sql .= '     , g.facility_sticker_no ';

        $result = $this->TrnOpeShipping->query($sql);

        $columns = array(
            '部署名',
            '室番号',
            '入室時間',
            '術式コード',
            '術式名',
            'セットバーコード',
            'セットバーコードガイド',
            '体位',
            '麻酔',
            '術式',
            '手術日',
            '開始時間',
            '患者番号',
            '性別',
            '年齢',
            '患者名',
            'セットコード',
            'セット名称',
            '数量',
            '材料名',
            '略称',
            '医事コード',
            'バーコード',
            'バーコードガイド',
            'ページ番号',
            '印刷日',
            '範囲'
            );
        $i=1;
        
        $break = '';
        foreach($result as $r){
            if($break != $r[0]['ope_item_set_code']){
                $break = $r[0]['ope_item_set_code'];
            }
            
            $data[] = array(
                $r[0]['department_name'],//部署名
                $r[0]['ope_room_code'],// '室番号',
                $r[0]['start_time'],// '入室時間',
                $r[0]['ope_method_set_code'],// '術式コード',
                $r[0]['ope_method_name'],// '術式名',
                $r[0]['work_no'],// 'セットバーコード',
                $r[0]['work_no'],// 'セットバーコードガイド',
                $r[0]['ope_position_name'], // '体位',
                $r[0]['ope_anesthesia_name'],// '麻酔',
                $r[0]['ope_method_name'],// '術式',
                $r[0]['work_date'],// '手術日',
                $r[0]['start_time'],// '開始時間',
                $r[0]['number'],// '患者番号',
                $r[0]['sex'],// '性別',
                $r[0]['age'],// '年齢',
                $r[0]['name'],// '患者名',
                $r[0]['ope_item_set_code'],// 'セットコード',
                $r[0]['ope_item_set_name'],// 'セット名称',
                $r[0]['quantity'] . $r[0]['unit_name'],// '数量',
                $r[0]['item_name'] . ' '. $r[0]['standard'] . ' '  . $r[0]['item_code'],// '材料名',
                $r[0]['internal_code'],// '略称',
                $r[0]['refund'],// '医事コード',
                $r[0]['facility_sticker_no'],// 'バーコード',
                $r[0]['facility_sticker_no'],// 'バーコードガイド',
                $i , // 'ページ番号',
                $today, // '印刷日'
                $break
                );
            $i++;
        }
        
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'セット払出明細リスト' , '備考1' =>$result[0][0]['ope_method_comment'] );
        $this->xml_output($data,join("\t",$columns),$layout_name['35'],$fix_value);
    }

    // 返却リスト印刷
    public function print_returnList($id){
        
        $today = date("Y/m/d");

        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , a.work_no ';
        $sql .= "     , to_char(a.work_date, 'YYYY/MM/DD') as work_date ";
        $sql .= '     , a.start_time ';
        $sql .= '     , e.ope_room_code ';
        $sql .= '     , f.ope_method_set_code ';
        $sql .= '     , f.ope_method_name ';
        $sql .= '     , d.ope_position_name ';
        $sql .= '     , c.ope_anesthesia_name ';
        $sql .= '     , a.name ';
        $sql .= '     , a.number ';
        $sql .= '     , a.age ';
        $sql .= '     , (  ';
        $sql .= '       case a.sex  ';
        $sql .= "         when '1' then 'M' ";
        $sql .= "         when '2' then 'F' ";
        $sql .= '         end ';
        $sql .= '     )                                    as sex ';
        $sql .= '     , i.internal_code ';
        $sql .= '     , i.item_name ';
        $sql .= '     , i.standard ';
        $sql .= '     , i.item_code ';
        $sql .= '     , (  ';
        $sql .= '       case i.enable_medicalcode  ';
        $sql .= "         when null then '' ";
        $sql .= "         when 0 then '' ";
        $sql .= "         when 1 then '★' ";
        $sql .= "         when 2 then '★' ";
        $sql .= '         end ';
        $sql .= '     )                                    as refund ';
        $sql .= '     , g.quantity ';
        $sql .= '     , h1.unit_name ';
        $sql .= '     , g.facility_sticker_no ';
        $sql .= '     , d2.ope_item_set_code ';
        $sql .= '     , d2.ope_item_set_name ';
        $sql .= '     , j.department_name ';
        $sql .= '     , a.ope_method_comment  ';
        $sql .= '   from ';
        $sql .= '     trn_ope_headers as a  ';
        $sql .= '     left join trn_ope_returns as b  ';
        $sql .= '       on b.trn_ope_header_id = a.id  ';
        $sql .= '     left join trn_ope_items as b1  ';
        $sql .= '       on b1.id = b.trn_ope_item_id  ';
        $sql .= '     left join mst_ope_anesthesias as c  ';
        $sql .= '       on c.id = a.mst_ope_anesthesia_id  ';
        $sql .= '     left join mst_ope_positions as d  ';
        $sql .= '       on d.id = a.mst_ope_position_id  ';
        $sql .= '     left join mst_ope_rooms as e  ';
        $sql .= '       on e.id = a.mst_ope_room_id  ';
        $sql .= '     left join mst_ope_method_names as f  ';
        $sql .= '       on f.id = a.mst_ope_method_id  ';
        $sql .= '     left join trn_stickers as g  ';
        $sql .= '       on g.id = b.trn_sticker_id  ';
        $sql .= '     left join mst_item_units as h  ';
        $sql .= '       on h.id = g.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as h1  ';
        $sql .= '       on h1.id = h.mst_unit_name_id  ';
        $sql .= '     left join mst_facility_items as i  ';
        $sql .= '       on i.id = h.mst_facility_item_id  ';
        $sql .= '     left join mst_ope_item_set_headers as d2  ';
        $sql .= '       on d2.ope_item_set_code = b1.ope_item_set_code  ';
        $sql .= '      and d2.is_deleted = false ';
        $sql .= '     left join mst_departments as j  ';
        $sql .= '       on j.id = a.mst_department_id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $id;
        $sql .= '     and b.is_deleted = false ';
        $sql .= '   order by ';
        $sql .= "     ( case when d2.ope_item_set_code = '' then 'ZZZZZZ' else d2.ope_item_set_code end ) ";
        $sql .= '     , i.item_name ';
        $sql .= '     , i.standard ';
        $sql .= '     , i.item_code ';
        $sql .= '     , g.facility_sticker_no ';

        $result = $this->TrnOpeReturn->query($sql);

        $columns = array(
            '部署名',
            '室番号',
            '入室時間',
            '術式コード',
            '術式名',
            'セットバーコード',
            'セットバーコードガイド',
            '体位',
            '麻酔',
            '術式',
            '手術日',
            '開始時間',
            '患者番号',
            '性別',
            '年齢',
            '患者名',
            'セットコード',
            'セット名称',
            '数量',
            '材料名',
            '略称',
            '医事コード',
            'バーコード',
            'バーコードガイド',
            'ページ番号',
            '印刷日',
            '範囲'
            );
        $i=1;
        
        $break = '';
        foreach($result as $r){
            if($break != $r[0]['ope_item_set_code']){
                $break = $r[0]['ope_item_set_code'];
            }
            $data[] = array(
                   $r[0]['department_name'],//部署名
                   $r[0]['ope_room_code'],// '室番号',
                   $r[0]['start_time'],// '入室時間',
                   $r[0]['ope_method_set_code'],// '術式コード',
                   $r[0]['ope_method_name'],// '術式名',
                   $r[0]['work_no'],// 'セットバーコード',
                   $r[0]['work_no'],// 'セットバーコードガイド',
                   $r[0]['ope_position_name'], // '体位',
                   $r[0]['ope_anesthesia_name'],// '麻酔',
                   $r[0]['ope_method_name'],// '術式',
                   $r[0]['work_date'],// '手術日',
                   $r[0]['start_time'],// '開始時間',
                   $r[0]['number'],// '患者番号',
                   $r[0]['sex'],// '性別',
                   $r[0]['age'],// '年齢',
                   $r[0]['name'],// '患者名',
                   $r[0]['ope_item_set_code'],// 'セットコード',
                   $r[0]['ope_item_set_name'],// 'セット名称',
                   $r[0]['quantity'] . $r[0]['unit_name'],// '数量',
                   $r[0]['item_name'] . ' '. $r[0]['standard'] . ' '  . $r[0]['item_code'],// '材料名',
                   $r[0]['internal_code'],// '略称',
                   $r[0]['refund'],// '医事コード',
                   $r[0]['facility_sticker_no'],// 'バーコード',
                   $r[0]['facility_sticker_no'],// 'バーコードガイド',
                   $i , // 'ページ番号',
                   $today,// '印刷日'
                   $break
                );
            $i++;
        }

        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'セット返却明細リスト' , '備考1' =>$result[0][0]['ope_method_comment'] );
        $this->xml_output($data,join("\t",$columns),$layout_name['35'],$fix_value);
        
    }

    function checkReturnId($sticker_id , $ids){
        foreach($ids as $id){
            if($sticker_id == $id){
                return true;
            }
        }
        return false;
    }

    function del($id){
        $this->TrnOpeHeader->begin();
        $this->TrnOpeHeader->create();
        $res = $this->TrnOpeHeader->updateAll(array(
            'TrnOpeHeader.ope_status' => Configure::read('OpeStatus.delete'),
            'TrnOpeHeader.is_deleted' => 'true',
            'TrnOpeHeader.modifier'   => $this->Session->read('Auth.MstUser.id'),
            'TrnOpeHeader.modified'   => "'" . date('Y/m/d H:i:s.u') . "'",
            ),array(
                'TrnOpeHeader.id' => $id,
            ),-1);
        
        if(!$res){
            $this->TrnOpeHeader->rollback();
            $this->Session->setFlash('手術予定削除処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('ope_search');
        }
        
        $this->TrnOpeHeader->commit();
        $this->Session->setFlash('手術予定情報登録を削除しました。', 'growl', array('type'=>'star') );
        $this->redirect('ope_search');
    }

    /**
     * 材料セット 初期画面
     */
    function ope_item_set_search(){
        App::import('Sanitize');
        $this->setRoleFunction(132);
        $result = array();
        if(isset($this->request->data['search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $where = "";
            if(isset($this->request->data['MstOpeItemSetHeader']['ope_item_set_code']) && $this->request->data['MstOpeItemSetHeader']['ope_item_set_code'] !== ''){
                $where .=" and a.ope_item_set_code ilike '%" .$this->request->data['MstOpeItemSetHeader']['ope_item_set_code']."%'";
            }
            if(isset($this->request->data['MstOpeItemSetHeader']['ope_item_set_name']) && $this->request->data['MstOpeItemSetHeader']['ope_item_set_name'] !== '' ){
                $where .=" and a.ope_item_set_name ilike '%" .$this->request->data['MstOpeItemSetHeader']['ope_item_set_name']."%'";
            }
            $sql  = ' select ';
            $sql .= '       a.id                as "MstOpeItemSetHeader__id" ';
            $sql .= '     , a.ope_item_set_code as "MstOpeItemSetHeader__code" ';
            $sql .= '     , a.ope_item_set_name as "MstOpeItemSetHeader__name" ';
            $sql .= ' from  ';
            $sql .= '    mst_ope_item_set_headers as a ';
            $sql .= ' where  ';
            $sql .= '     a.is_deleted = false ';
            $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= $where;
            $sql .= ' order by a.ope_item_set_code, a.ope_item_set_name ';

            $this->set('max' , $this->getMaxCount($sql , 'MstOpeItemSetHeader'));

            $sql .= ' limit ' .$this->request->data['limit'];
            $result = $this->MstOpeItemSetHeader->query($sql);
            $this->request->data = $data;
        }
        $this->set('result' , $result);
        $this->set('data',$this->request->data );
    }
    
    function ope_item_set_del($id = null){
        $this->MstOpeItemSetHeader->del(array('id'=>$id));
        $this->Session->setFlash('手術材料セットを削除しました。', 'growl', array('type'=>'star') );
        $this->redirect('ope_item_set_search');
    }

    /**
     * 材料セット カート
     */
    function ope_item_set_cart($id = null){
        $items = array();
        $SearchResult = array();
        $CartSearchResult = array();
        if(!empty($id)){
            $this->request->data['MstOpeItemSetHeader']['id'] = $id;
        }
        if(isset($this->request->data['add_search']['is_search'])){
            
            $where = $where2 = '';
            
            //商品ID
            if(isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] !== ''){
                $where .= "  and a.internal_code = '" . $this->request->data['MstFacilityItem']['internal_code'] . "'";
            }
            //製品番号
            if(isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] !== ''){
                $where .= "  and a.item_code like '%" . $this->request->data['MstFacilityItem']['item_code'] . "%'";
            }
            //商品名
            if(isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] !== ''){
                $where .= "  and a.item_name like '%" . $this->request->data['MstFacilityItem']['item_name'] . "%'";
            }
            //規格
            if(isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] !== ''){
                $where .= "  and a.standard like '%" . $this->request->data['MstFacilityItem']['standard'] . "%'";
            }

            //販売元
            if(isset($this->request->data['MstFacilityItem']['dealer_name']) && $this->request->data['MstFacilityItem']['dealer_name'] !== ''){
                $where .= "  and d.dealer_name like '%" . $this->request->data['MstFacilityItem']['dealer_name'] . "%'";
            }

            //JANコード(前方一致）
            if(isset($this->request->data['MstFacilityItem']['jan_code']) && $this->request->data['MstFacilityItem']['jan_code'] !== ''){
                $where .= "  and a.jan_code like '" . $this->request->data['MstFacilityItem']['jan_code'] . "%'";
            }

           if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and b.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 = " and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 = " and 0 = 1 ";
            }
            
            // 検索
            $SearchResult = $this->_getItemData($where, $this->request->data['limit']);
            
            // カート検索
            $CartSearchResult = $this->_getItemData($where2);
            
        }
        if(isset($this->request->data['MstOpeItemSetHeader']['id']) && !empty($this->request->data['MstOpeItemSetHeader']['id'])){
            // 編集の場合、既存一覧を出しておく
            
            $items = $this->itemSetData($this->request->data['MstOpeItemSetHeader']['id']);
            
            $this->request->data['MstOpeItemSetHeader'] = $items[0]['MstOpeItemSetHeader'];
        }
        
        $this->set('items' , $items);
        
        $this->set('SearchResult'     , $SearchResult);
        $this->set('CartSearchResult' , $CartSearchResult);
    }

    private function itemSetData($id){
        $sql  = ' select ';
        $sql .= '       a.id                  as "MstOpeItemSetHeader__id"';
        $sql .= '     , a.ope_item_set_code   as "MstOpeItemSetHeader__ope_item_set_code"';
        $sql .= '     , a.ope_item_set_name   as "MstOpeItemSetHeader__ope_item_set_name"';
        $sql .= '     , d.internal_code       as "MstOpeItemSet__internal_code"';
        $sql .= '     , d.item_name           as "MstOpeItemSet__item_name"';
        $sql .= '     , d.standard            as "MstOpeItemSet__standard"';
        $sql .= '     , d.item_code           as "MstOpeItemSet__item_code"';
        $sql .= '     , c1.unit_name          as "MstOpeItemSet__unit_name"';
        $sql .= '     , e.dealer_name         as "MstOpeItemSet__dealer_name"';
        $sql .= '     , b.quantity            as "MstOpeItemSet__quantity"';
        $sql .= '     , b.recital             as "MstOpeItemSet__recital" ';
        $sql .= '     , c.id                  as "MstOpeItemSet__item_unit_id"';
        $sql .= '   from ';
        $sql .= '     mst_ope_item_set_headers as a ';
        $sql .= '     left join mst_ope_item_sets as b ';
        $sql .= '       on b.mst_ope_item_set_header_id = a.id ';
        $sql .= '     left join mst_item_units as c ';
        $sql .= '       on c.id = b.mst_item_unit_id ';
        $sql .= '     left join mst_unit_names as c1 ';
        $sql .= '       on c1.id = c.mst_unit_name_id ';
        $sql .= '     left join mst_facility_items as d ';
        $sql .= '       on d.id = c.mst_facility_item_id ';
        $sql .= '     left join mst_dealers as e ';
        $sql .= '       on e.id = d.mst_dealer_id ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $id;
        $sql .= '     and b.is_deleted = false ';
        $sql .= '   order by ';
        $sql .= '     d.item_name , d.standard , d.item_code , b.id ';
        
        return $this->MstOpeItemSetHeader->query($sql);
    }
    
    
    private function _getItemData($where = '' , $limit = null){
        $sql  = ' select ';
        $sql .= '       b.id            as "MstFacilityItem__id"';
        $sql .= '     , a.internal_code as "MstFacilityItem__internal_code"';
        $sql .= '     , a.item_name     as "MstFacilityItem__item_name"';
        $sql .= '     , a.standard      as "MstFacilityItem__standard"';
        $sql .= '     , a.item_code     as "MstFacilityItem__item_code"';
        $sql .= '     , a.jan_code      as "MstFacilityItem__jan_code"';
        $sql .= '     , c.unit_name     as "MstFacilityItem__unit_name"';
        $sql .= '     , d.dealer_name   as "MstFacilityItem__dealer_name"';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.mst_facility_item_id = a.id  ';
        $sql .= '       and b.per_unit = 1  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on c.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_dealers as d  ';
        $sql .= '       on d.id = a.mst_dealer_id  ';
        $sql .= '   where ';
        $sql .= '     a.is_lowlevel = false  ';
        $sql .= '     and a.item_type = 1 ';
        $sql .= $where;
        $sql .= '  order by a.item_name , a.standard , a.item_code , a.internal_code ';
        
        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));
            $sql .= ' limit ' . $limit;
        }
        return $this->MstFacilityItem->query($sql);
    }
    
    /**
     * 材料セット 確認
     */
    function ope_item_set_confirm(){
        $item = array();
        $add_item = array();
        if(!empty($this->request->data['MstOpeItemSetHeader']['id'])){
            $item = $this->itemSetData($this->request->data['MstOpeItemSetHeader']['id']);
        }
        
        if(!empty($this->request->data['cart']['tmpid'])){
            $add_item = $this->_getItemData(" and b.id in ( " . $this->request->data['cart']['tmpid'] . ") ");
        }
        
        $this->set('items' , $item);
        $this->set('add_items' , $add_item);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('OpeItemSet.token' , $token);
        $this->request->data['OpeItemSet']['token'] = $token;
    }
    
    /**
     * 材料セット 完了
     */
    function ope_item_set_result(){
        if($this->request->data['OpeItemSet']['token'] === $this->Session->read('OpeItemSet.token')) {
            $now = date('Y/m/d H:i:s');
            $this->Session->delete('OpeItemSet.token');
            $this->MstOpeItemSetHeader->begin();
            
            // 数量、備考を取得
            if(!empty($this->request->data['MstOpeItemSetHeader']['id'])){
                // 更新
                $MstOpeItemSetHeader['MstOpeItemSetHeader']['id'] = $this->request->data['MstOpeItemSetHeader']['id'];
                // 行ロック
                $this->MstOpeItemSetHeader->query(' select * from mst_ope_item_set_headers as a where a.id = ' . $this->request->data['MstOpeItemSetHeader']['id'] .' for update ' );
            } else {
                // 新規作成
                $MstOpeItemSetHeader['MstOpeItemSetHeader']['is_deleted']      = false ;
                $MstOpeItemSetHeader['MstOpeItemSetHeader']['creater']         = $this->Session->read('Auth.MstUser.id');
                $MstOpeItemSetHeader['MstOpeItemSetHeader']['created']         = $now;
                $MstOpeItemSetHeader['MstOpeItemSetHeader']['mst_facility_id'] = $this->Session->read('Auth.facility_id_selected');
            }
            $MstOpeItemSetHeader['MstOpeItemSetHeader']['ope_item_set_code'] = $this->request->data['MstOpeItemSetHeader']['ope_item_set_code'];
            $MstOpeItemSetHeader['MstOpeItemSetHeader']['ope_item_set_name'] = $this->request->data['MstOpeItemSetHeader']['ope_item_set_name'];
            $MstOpeItemSetHeader['MstOpeItemSetHeader']['modifier']          = $this->Session->read('Auth.MstUser.id');
            $MstOpeItemSetHeader['MstOpeItemSetHeader']['modified']          = $now;
            
            $this->MstOpeItemSetHeader->create();
            // SQL実行
            if (!$this->MstOpeItemSetHeader->save($MstOpeItemSetHeader)) {
                $this->MstOpeItemSetHeader->rollback();
                $this->Session->setFlash('手術材料セット登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ope_item_set_search');
            }
            
            if(empty($this->request->data['MstOpeItemSetHeader']['id'])){
                $this->request->data['MstOpeItemSetHeader']['id'] = $this->MstOpeItemSetHeader->getLastInsertID();
            }
            
            // 明細情報をいったん削除
            $res = $this->MstOpeItemSet->updateAll(array(
                'MstOpeItemSet.is_deleted' => 'true',
                'MstOpeItemSet.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'MstOpeItemSet.modified'   => "'". $now ."'",
                ), array(
                    'MstOpeItemSet.mst_ope_item_set_header_id' => $this->request->data['MstOpeItemSetHeader']['id'],
                ), -1);
            
            if(false === $res){
                $this->MstOpeItemSetHeader->rollback();
                $this->Session->setFlash('手術材料セット登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ope_item_set_search');
            }
            
            foreach($this->request->data['MstOpeItemSet']['id'] as $cnt => $id ){
                // 明細情報を追加
                $MstOpeItemSet = array(
                    'MstOpeItemSet'=> array(
                        'mst_item_unit_id'           => $id ,
                        'quantity'                   => $this->request->data['MstOpeItemSet']['quantity'][$cnt],
                        'recital'                    => $this->request->data['MstOpeItemSet']['recital'][$cnt],
                        'mst_ope_item_set_header_id' => $this->request->data['MstOpeItemSetHeader']['id'],
                        'is_deleted'                 => false,
                        'creater'                    => $this->Session->read('Auth.MstUser.id'),
                        'created'                    => $now,
                        'modifier'                   => $this->Session->read('Auth.MstUser.id'),
                        'modified'                   => $now,
                        'mst_facility_id'            => $this->Session->read('Auth.facility_id_selected')
                        )
                    );
                
                $this->MstOpeItemSet->create();
                // SQL実行
                if (!$this->MstOpeItemSet->save($MstOpeItemSet)) {
                    $this->MstOpeItemSetHeader->rollback();
                    $this->Session->setFlash('手術材料セット登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ope_item_set_search');
                }
            }
            $this->MstOpeItemSetHeader->commit();
            
            $result = $this->itemSetData($this->request->data['MstOpeItemSetHeader']['id']);
            $this->set('result' , $result);
        }
    }


    /**
     * 術式セット 初期画面
     */
    function ope_method_set_search(){
        App::import('Sanitize');
        $this->setRoleFunction(131);
        $result = array();
        if(isset($this->request->data['search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $where = "";
            if(isset($this->request->data['MstOpeMethod']['code']) && $this->request->data['MstOpeMethod']['code'] !== ''){
                $where .=" and a.ope_method_set_code ilike '%" .$this->request->data['MstOpeMethod']['code']."%'";
            }
            if(isset($this->request->data['MstOpeMethod']['name']) && $this->request->data['MstOpeMethod']['name'] !== '' ){
                $where .=" and a.ope_method_name ilike '%" .$this->request->data['MstOpeMethod']['name']."%'";
            }
            $sql  = ' select ';
            $sql .= '       a.id                  as "MstOpeMethod__id" ';
            $sql .= '     , a.ope_method_set_code as "MstOpeMethod__code" ';
            $sql .= '     , a.ope_method_name     as "MstOpeMethod__name" ';
            $sql .= '     , a.ope_method_comment  as "MstOpeMethod__comment"';
            $sql .= ' from  ';
            $sql .= '    mst_ope_method_names as a ';
            $sql .= ' where  ';
            $sql .= '     a.is_deleted = false ';
            $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= $where;
            $sql .= ' order by a.ope_method_set_code, a.ope_method_name ';

            $this->set('max' , $this->getMaxCount($sql , 'MstOpeMethodName'));

            $sql .= ' limit ' .$this->request->data['limit'];
            $result = $this->MstOpeMethodName->query($sql);
            $this->request->data = $data;
        }
        $this->set('result' , $result);
        $this->set('data',$this->request->data );
    }

    function ope_method_set_del($id = null){
        $this->MstOpeMethodName->del(array('id'=>$id));
        $this->Session->setFlash('手術術式セットを削除しました。', 'growl', array('type'=>'star') );
        $this->redirect('ope_method_set_search');
    }
    
    /**
     * 術式セット カート
     */
    function ope_method_set_cart($id = null){
        $method = array();
        $SearchResult = array();
        $CartSearchResult = array();
        if(!empty($id)){
            $this->request->data['MstOpeMethod']['id'] = $id;
        }
        if(isset($this->request->data['add_search']['is_search'])){
            
            $where = $where2 = '';
            
            //製品番号
            if(isset($this->request->data['MstOpeItemSet']['search_code']) && $this->request->data['MstOpeItemSet']['search_code'] !== ''){
                $where .= "  and a.ope_item_set_code like '%" . $this->request->data['MstOpeItemSet']['search_code'] . "%'";
            }
            //商品名
            if(isset($this->request->data['MstOpeItemSet']['search_name']) && $this->request->data['MstOpeItemSet']['search_name'] !== ''){
                $where .= "  and a.ope_item_set_name like '%" . $this->request->data['MstOpeItemSet']['search_name'] . "%'";
            }

           if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and a.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 = " and a.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 = " and 0 = 1 ";
            }
            
            // 検索
            $SearchResult = $this->_getMethodData($where, $this->request->data['limit']);
            
            // カート検索
            $CartSearchResult = $this->_getMethodData($where2);
            
        }
        if(isset($this->request->data['MstOpeMethod']['id']) && !empty($this->request->data['MstOpeMethod']['id'])){
            // 編集の場合、既存一覧を出しておく
            
            $method = $this->MethodSetData($this->request->data['MstOpeMethod']['id']);
            if(empty($method)){
                // ヘッダだけあって明細がないので、ヘッダを取り直す。
                $sql  = ' select ';
                $sql .= '   a.id                  as "MstOpeMethod__id" ';
                $sql .= ' , a.ope_method_set_code as "MstOpeMethod__code" ';
                $sql .= ' , a.ope_method_name     as "MstOpeMethod__name" ';
                $sql .= ' , a.ope_method_comment  as "MstOpeMethod__comment"';
                $sql .= ' from ';
                $sql .= ' mst_ope_method_names as a ';
                $sql .= ' where id = ' . $this->request->data['MstOpeMethod']['id'];

                $method = $this->MstOpeMethodName->query($sql);
                $this->request->data['MstOpeMethod'] = $method[0]['MstOpeMethod'];
            }else{
                $this->request->data['MstOpeMethod'] = $method[0]['MstOpeMethod'];
            }
        }
        
        $this->set('method' , $method);
        
        $this->set('SearchResult'     , $SearchResult);
        $this->set('CartSearchResult' , $CartSearchResult);
    }

    private function MethodSetData($id){
        $sql  = ' select ';
        $sql .= '       a.id                  as "MstOpeMethod__id"';
        $sql .= '     , a.ope_method_name     as "MstOpeMethod__name"';
        $sql .= '     , a.ope_method_set_code as "MstOpeMethod__code"';
        $sql .= '     , a.ope_method_comment  as "MstOpeMethod__comment"';
        $sql .= '     , c.ope_item_set_code   as "MstOpeItemSet__code"';
        $sql .= '     , c.ope_item_set_name   as "MstOpeItemSet__name"';
        $sql .= '     , c.id                  as "MstOpeItemSet__id"';
        $sql .= '   from ';
        $sql .= '     mst_ope_method_names as a ';
        $sql .= '     left join mst_ope_method_sets as b ';
        $sql .= '       on b.mst_ope_method_name_id = a.id ';
        $sql .= '     left join mst_ope_item_set_headers as c ';
        $sql .= '       on c.id = b.mst_ope_item_set_header_id ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $id;
        $sql .= '     and b.is_deleted = false ';
        $sql .= '     and c.is_deleted = false ';
        $sql .= '   order by ';
        $sql .= '     c.id ';
        
        return $this->MstOpeItemSetHeader->query($sql);
    }
    
    
    private function _getMethodData($where = '' , $limit = null){
        $sql  = ' select ';
        $sql .= '       a.id                as "MstOpeItemSetHeader__id"';
        $sql .= '     , a.ope_item_set_code as "MstOpeItemSetHeader__code"';
        $sql .= '     , a.ope_item_set_name as "MstOpeItemSetHeader__name"';
        $sql .= '   from ';
        $sql .= '     mst_ope_item_set_headers as a  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= $where;
        $sql .= '  order by a.ope_item_set_code ';
        
        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'MstOpeItemSetHeader'));
            $sql .= ' limit ' . $limit;
        }
        return $this->MstOpeItemSetHeader->query($sql);
    }


    /**
     * 術式 確認
     */
    function ope_method_set_confirm(){
        $item = array();
        $add_item = array();
        if(!empty($this->request->data['MstOpeMethod']['id'])){
            $item = $this->MethodSetData($this->request->data['MstOpeMethod']['id']);
            if(empty($item)){
                // ヘッダだけあって明細がないので、ヘッダを取り直す。
                $sql  = ' select ';
                $sql .= '   a.id                  as "MstOpeMethod__id" ';
                $sql .= ' , a.ope_method_set_code as "MstOpeMethod__code" ';
                $sql .= ' , a.ope_method_name     as "MstOpeMethod__name" ';
                $sql .= ' , a.ope_method_comment  as "MstOpeMethod__comment"';
                $sql .= ' from ';
                $sql .= ' mst_ope_method_names as a ';
                $sql .= ' where id = ' . $this->request->data['MstOpeMethod']['id'];

                $method = $this->MstOpeMethodName->query($sql);
                $this->request->data['MstOpeMethod'] = $method[0]['MstOpeMethod'];
                
            }else{
                $this->request->data['MstOpeMethod'] = $item[0]['MstOpeMethod'];
            }
        }else{
            // 術式セットコードの初期値を設定
            $max = $this->MstOpeMethodName->query(' select max(ope_method_set_code) as max from mst_ope_method_names ');
            $maxcode = (int)$max[0][0]['max'];
            $maxcode++;
            $this->request->data['MstOpeMethod']['code']    = $maxcode;
            $this->request->data['MstOpeMethod']['comment'] = '';
        }
        
        if(!empty($this->request->data['cart']['tmpid'])){
            $add_item = $this->_getMethodData(" and a.id in ( " . $this->request->data['cart']['tmpid'] . ") ");
        }
        
        $this->set('items' , $item);
        $this->set('add_items' , $add_item);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('OpeMethod.token' , $token);
        $this->request->data['OpeMethod']['token'] = $token;
    }

    /**
     * 術式セット 完了
     */
    function ope_method_set_result(){
        if($this->request->data['OpeMethod']['token'] === $this->Session->read('OpeMethod.token')) {
            $now = date('Y/m/d H:i:s');
            $this->Session->delete('OpeMethod.token');
            $this->MstOpeMethodName->begin();
            
            if(!empty($this->request->data['MstOpeMethod']['id'])){
                // 更新
                $MstOpeMethodName['MstOpeMethodName']['id'] = $this->request->data['MstOpeMethod']['id'];
                // 行ロック
                $this->MstOpeMethodName->query(' select * from mst_ope_method_names as a where a.id = ' . $this->request->data['MstOpeMethod']['id'] .' for update ' );
            } else {
                // 新規作成
                $MstOpeMethodName['MstOpeMethodName']['is_deleted']          = false ;
                $MstOpeMethodName['MstOpeMethodName']['creater']             = $this->Session->read('Auth.MstUser.id');
                $MstOpeMethodName['MstOpeMethodName']['created']             = $now;
                $MstOpeMethodName['MstOpeMethodName']['mst_facility_id']     = $this->Session->read('Auth.facility_id_selected');
                $MstOpeMethodName['MstOpeMethodName']['ope_department_code'] = '0000';

            }
            $MstOpeMethodName['MstOpeMethodName']['ope_method_set_code'] = $this->request->data['MstOpeMethod']['code'];
            $MstOpeMethodName['MstOpeMethodName']['ope_method_name']     = $this->request->data['MstOpeMethod']['name'];
            $MstOpeMethodName['MstOpeMethodName']['ope_method_comment']  = $this->request->data['MstOpeMethod']['comment'];
            $MstOpeMethodName['MstOpeMethodName']['modifier']            = $this->Session->read('Auth.MstUser.id');
            $MstOpeMethodName['MstOpeMethodName']['modified']            = $now;
            
            $this->MstOpeItemSetHeader->create();
            // SQL実行
            if (!$this->MstOpeMethodName->save($MstOpeMethodName)) {
                $this->MstOpeMethodName->rollback();
                $this->Session->setFlash('手術術式セット登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ope_method_set_search');
            }
            
            if(empty($this->request->data['MstOpeMethod']['id'])){
                $this->request->data['MstOpeMethod']['id'] = $this->MstOpeMethodName->getLastInsertID();
            }
            
            // 明細情報をいったん削除
            $res = $this->MstOpeMethodSet->updateAll(array(
                'MstOpeMethodSet.is_deleted' => 'true',
                'MstOpeMethodSet.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'MstOpeMethodSet.modified'   => "'". $now ."'",
                ), array(
                    'MstOpeMethodSet.mst_ope_method_name_id' => $this->request->data['MstOpeMethod']['id'],
                ), -1);
            
            if(false === $res){
                $this->MstOpeMethodName->rollback();
                $this->Session->setFlash('手術術式セット登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ope_method_set_search');
            }
            
            foreach($this->request->data['MstOpeItemSet']['id'] as $id ){
                // 明細情報を追加
                $MstOpeMethodSet = array(
                    'MstOpeMethodSet'=> array(
                        'id'                         => $this->getOpeRelationId( $this->request->data['MstOpeMethod']['id'] , $id),
                        'mst_ope_item_set_header_id' => $id,
                        'mst_ope_method_name_id'     => $this->request->data['MstOpeMethod']['id'],
                        'is_deleted'                 => false,
                        'creater'                    => $this->Session->read('Auth.MstUser.id'),
                        'created'                    => $now,
                        'modifier'                   => $this->Session->read('Auth.MstUser.id'),
                        'modified'                   => $now,
                        'mst_facility_id'            => $this->Session->read('Auth.facility_id_selected')
                        )
                    );
                
                $this->MstOpeMethodSet->create();
                // SQL実行
                if (!$this->MstOpeMethodSet->save($MstOpeMethodSet)) {
                $this->MstOpeMethodName->rollback();
                    $this->Session->setFlash('手術術式セット登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ope_method_set_search');
                }
            }
            $this->MstOpeMethodName->commit();
            
        }
        
        $result = $this->MethodSetData($this->request->data['MstOpeMethod']['id']);
        $this->set('result' , $result);
        
    }
    
    private function getOpeRelationId($method_id , $item_id){
        $sql  = ' select ';
        $sql .= '       a.id  ';
        $sql .= '   from ';
        $sql .= '     mst_ope_method_sets as a  ';
        $sql .= '   where ';
        $sql .= '     a.mst_ope_method_name_id = ' . $method_id;
        $sql .= '     and a.mst_ope_item_set_header_id = ' . $item_id;

        $ret  = $this->MstOpeMethodName->query($sql);
        if(isset($ret[0][0]['id'])){
            return $ret[0][0]['id'];
        }else{
            return null;
        }
    }
    
    public function export_csv($mode=""){
        switch($mode){
          case 'ope_method' :
            $this->export_method();
            break;
          case 'ope_item':
            $this->export_item();
            break;
          default:
            break;
        }
    }

    /**
     * 術式セットCSV出力
     */
    public function export_method(){
        $sql  = ' select';
        $sql .= '       a.ope_method_set_code   as "術式コード"';
        $sql .= '     , a.ope_method_name       as "術式名"';
        $sql .= '     , c.ope_item_set_code     as "材料セットコード"';
        $sql .= '     , c.ope_item_set_name     as "材料セット名"';
        $sql .= '     , a.ope_method_comment    as "コメント"';
        $sql .= '   from';
        $sql .= '     mst_ope_method_names as a ';
        $sql .= '     left join mst_ope_method_sets as b ';
        $sql .= '       on b.mst_ope_method_name_id = a.id ';
        $sql .= '     left join mst_ope_item_set_headers as c ';
        $sql .= '       on c.id = b.mst_ope_item_set_header_id ';
        $sql .= '   where';
        $sql .= '     a.is_deleted = false ';
        $sql .= '     and b.is_deleted = false ';
        $sql .= '     and c.is_deleted = false ';
        $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '   order by';
        $sql .= '     a.ope_method_set_code';
        $sql .= '     , c.ope_item_set_code ';
        
        $this->db_export_csv($sql , "術式一覧", 'ope_method_set_search');
    }

    /**
     * 材料セットCSV出力
     */
    public function export_item(){
        $sql  = ' select ';
        $sql .= '       a.ope_item_set_code as "材料セットコード"';
        $sql .= '     , a.ope_item_set_name as "材料セット名"';
        $sql .= '     , d.internal_code     as "商品ID"';
        $sql .= '     , d.item_name         as "商品名"';
        $sql .= '     , d.standard          as "規格"';
        $sql .= '     , d.item_code         as "製品番号"';
        $sql .= '     , b.quantity          as "数量"';
        $sql .= '     , c1.unit_name        as "単位"';
        $sql .= '     , b.recital           as "備考"';
        $sql .= '   from ';
        $sql .= '     mst_ope_item_set_headers as a  ';
        $sql .= '     left join mst_ope_item_sets as b  ';
        $sql .= '       on b.mst_ope_item_set_header_id = a.id  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on c.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as c1  ';
        $sql .= '       on c1.id = c.mst_unit_name_id  ';
        $sql .= '     left join mst_facility_items as d  ';
        $sql .= '       on d.id = c.mst_facility_item_id  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and b.is_deleted = false  ';
        $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '   order by ';
        $sql .= '     a.ope_item_set_code ';
        $sql .= '     , d.item_name ';
        $sql .= '     , d.standard ';
        $sql .= '     , d.item_code ';
        
        $this->db_export_csv($sql , "材料セット一覧", 'ope_item_set_search');
    }
}
?>
