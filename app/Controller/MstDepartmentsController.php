<?php
/**
 * MstDepartmentsController
 * @version 1.0.0
 * @since 2010/07/08
 */

class MstDepartmentsController extends AppController {
    var $name = 'MstDepartments';
    
    /**
     *
     * @var array $uses
     */
    var $uses = array('MstDepartment','MstFacility','MstUserBelonging','MstFacilityRelation');

    /**
     * @var array $components
     */
    var $components = array('CsvWriteUtils');
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;
    
    /**
     * @var Departments
     */
    var $Departments;

    public function beforeFilter(){
        parent::beforeFilter();
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }
    
    /**
     * departments_list
     *
     * 部署一覧
     */
    function departments_list() {
        App::import('Sanitize');
        $this->setRoleFunction(80); //部署
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        $Departments_List = array();
        
        $this->set('Facility_List' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                            array(Configure::read('FacilityType.hospital')),
                                                            true,
                                                            array('id' , 'facility_name')
                                                            )
                   );
        
        //検索ボタン押下
        if(isset($this->request->data['MstDepartment']['is_search'])) {
            $where = '';
            //部署名(LIKE検索)
            if(isset($this->request->data['MstDepartment']['search_department_name']) && $this->request->data['MstDepartment']['search_department_name'] != ""){
                $where .= " and a.department_name LIKE '%".Sanitize::clean($this->request->data['MstDepartment']['search_department_name'])."%'";
            }
            
            //施設(完全一致・施設コードではない)
            if(isset($this->request->data['MstDepartment']['search_facility_id']) && $this->request->data['MstDepartment']['search_facility_id'] != ""){
                $where .= ' and a.mst_facility_id = ' . $this->request->data['MstDepartment']['search_facility_id'];
            }
            
            //削除済み表示
            if(isset($this->request->data['search_is_deleted']) && $this->request->data['search_is_deleted'] == 0){
                $where .= ' and a.is_deleted = FALSE';
            }

            $sql = ' select ';
            $sql .= '       a.id                           as "MstDepartment__id" ';
            $sql .= '     , a.department_name              as "MstDepartment__department_name" ';
            $sql .= '     , a.department_type              as "MstDepartment__department_type" ';
            $sql .= '     , a.department_code              as "MstDepartment__department_code" ';
            $sql .= "     , ( case when a.is_deleted = true then '○' ";
            $sql .= "              else '' ";
            $sql .= '         end )                        as "MstDepartment__is_deleted" ';
            $sql .= '     , b.facility_type                as "MstFacility__facility_type" ';
            $sql .= '     , b.facility_name                as "MstFacility__facility_name" ';
            $sql .= '     , b.id                           as "MstFacility__id"  ';
            $sql .= '   from ';
            $sql .= '     mst_departments as a  ';
            $sql .= '     left join mst_facility_relations as x  ';
            $sql .= '       on x.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '       and x.is_deleted = false  ';
            $sql .= '     inner join mst_user_belongings as z  ';
            $sql .= '       on z.mst_facility_id = a.mst_facility_id  ';
            $sql .= '       and z.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '       and z.is_deleted = false  ';
            $sql .= '     left join mst_facilities as b  ';
            $sql .= '       on a.mst_facility_id = b.id  ';
            $sql .= '   where ';
            $sql .= '     b.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .= '     and x.partner_facility_id = b.id  ';
            $sql .= $where;
            $sql .= '   order by ';
            if($this->getSortOrder() == null ){
                $sql .= '     b.facility_code asc ';
                $sql .= '     , a.department_code asc  ';
            }else{
                $sql .= join(',' , $this->getSortOrder());
            }
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'MstDepartment'));
            if(isset($this->request->data['limit'])){
                $sql .= '   limit ' . $this->request->data['limit'];
            }else{
                $sql .= '   limit 100 ';
            }
            $Departments_List = $this->MstDepartment->query($sql);

        }
        $this->set('Departments_List',$this->outputFilter($Departments_List));
    }
    
    /**
     * Add ユーザ新規登録
     */
    function add($mode = '') {
        $this->setRoleFunction(80); //部署
        
        // ユーザから病院IDを取得
        $sql  = ' select ';
        $sql .= '       b.id  ';
        $sql .= '   from ';
        $sql .= '     mst_user_belongings as a  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.mst_facility_id  ';
        $sql .= '   where ';
        $sql .= '     a.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '     and b.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '   limit 1 ';
        
        $tmp = $this->MstFacility->query($sql);
        
        $this->request->data['MstDepartment']['mst_facility_id'] = $tmp[0][0]['id'];
        
        // 部署コード初期値を用意
        $sql  = ' select ';
        $sql .= '       max(department_code) as max  ';
        $sql .= '   from ';
        $sql .= '     mst_departments as a  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_id = ' . $this->request->data['MstDepartment']['mst_facility_id'];
        
        $max_code = $this->MstFacility->query($sql);
        $max = (int)(ltrim($max_code[0][0]['max'],'0'));
        $max++;
        $this->request->data['MstDepartment']['department_code'] = str_pad($max,(4),'0',STR_PAD_LEFT);
        
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }
    
    /*
     * 施設に追加
     */
    
    function addmod(){
        $this->setRoleFunction(80); //部署
        
        $this->add('mod');
    }
    
    /**
     * Mod ユーザ情報編集
     */
    function mod() {
        $this->setRoleFunction(80); //部署
        
        //更新チェック用にmod画面に入った瞬間の時間を保持
        $this->Session->write('Department.readTime',date('Y-m-d H:i:s'));
        
        //departmentlistから引き継いできたidでユーザ情報を取得
        $params = array (
            'conditions' => array('MstDepartment.id' => $this->request->data['MstDepartment']['id'],
                                  ),
            'fields'     => array('MstDepartment.id',
                                  'MstDepartment.mst_facility_id',
                                  'MstDepartment.department_name',
                                  'MstDepartment.department_formal_name',
                                  'MstDepartment.department_code',
                                  'MstDepartment.department_type',
                                  'MstDepartment.priority',
                                  'MstDepartment.is_deleted'
                                  ),
            'order'      => array('MstDepartment.id'),
            'recursive'  => -1
            );
        
        $this->request->data = $this->MstDepartment->find('first', $params);

        $Department_List = Set::Combine(
            $this->MstDepartment->query('select a.department_name as "MstDepartment__department_name", a.department_code as "MstDepartment__department_code" from mst_departments as a where a.mst_facility_id = '.$this->request->data['MstDepartment']['mst_facility_id']. ' and a.id <> ' .$this->request->data['MstDepartment']['id']  . "  order by a.department_code"),
            "{n}.MstDepartment.department_code",
            "{n}.MstDepartment.department_name"
            );
        
        $this->set('Department_List' , $Department_List);
        //施設IDから施設名を取得
        $this->request->data['MstDepartment']['mst_facility_name'] = $this->getFacilityName($this->request->data['MstDepartment']['mst_facility_id']);
        $this->request->data['MstDepartment']['mst_facility_id']   = $this->request->data['MstDepartment']['mst_facility_id'];
    }
    
    /**
     * result
     *
     * ユーザ情報更新（新規登録・更新）
     */
    function result() {
        $now = date('Y/m/d H:i:s');
        
        //トークン検証
        if($this->request->data['mode'] == "add"){
            if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
                $Department_List = $this->Session->read('Department_List');
                $this->Session->delete('Department_List');
                $this->set('Department_List' , $Department_List);
                $this->render('result');
                return;
            }
        }
        
        $this->MstDepartment->create();
        $this->MstDepartment->set($this->request->data['MstDepartment']);
        
        //病院一覧を取得
        $Facility_List = $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                array(Configure::read('FacilityType.hospital')),
                                                true,
                                                array('id' , 'facility_name')
                                                );
        
        //バリデートチェック
        if (!$this->MstDepartment->validates()) {
            //エラーメッセージ
                
            $err = implode("<br />",$this->MstDepartment->validationErrors);
            $this->Session->setFlash('エラーがあります<br />'.$err, 'growl', array('type'=>'important') );
            
            if($this->request->data['mode'] == "add"){
                $this->render('add');
                
                //--------------------------------------------------------------------
            }elseif($this->request->data['mode'] == "mod") {
                $this->render('mod');
            }
            //--------------------------------------------------------------------
        } else {
            //バリデートエラーなし

            //病院の施設IDから名称を取得
            $this->request->data['MstDepartment']['mst_facility_name'] = $this->getFacilityName($this->request->data['MstDepartment']['mst_facility_id']);
            
            //トランザクションを開始
            $this->MstDepartment->begin();
            //行ロック（更新時のみ）
            if(isset($this->request->data['MstDepartment']['id'])){
                $this->MstDepartment->query('select * from mst_departments as a where a.id = ' .$this->request->data['MstDepartment']['id']. ' for update ');
            }
            //MstDepartmentオブジェクトをcreate
            $this->MstDepartment->create();
            
            $department_data = array();
            
            
            //保存データの整形
            if(isset($this->request->data['MstDepartment']['id'])){
                //更新の場合
                $department_data['MstDepartment']['id']                     = $this->request->data['MstDepartment']['id'];
            }else{
                //新規の場合
                $department_data['MstDepartment']['creater']               = $this->Session->read('Auth.MstUser.id');
                $department_data['MstDepartment']['created']               = $now;
            }
            
            $department_data['MstDepartment']['mst_facility_id']        = $this->request->data['MstDepartment']['mst_facility_id'];
            $department_data['MstDepartment']['department_name']        = $this->request->data['MstDepartment']['department_name'];
            $department_data['MstDepartment']['department_formal_name'] = $this->request->data['MstDepartment']['department_name'];
            $department_data['MstDepartment']['department_code']        = $this->request->data['MstDepartment']['department_code'];
            $department_data['MstDepartment']['department_type']        = Configure::read('DepartmentType.hospital');
            $department_data['MstDepartment']['priority']               = 0;
            $department_data['MstDepartment']['is_deleted']             = (isset($this->request->data['MstDepartment']['is_deleted'])?true:false);
            $department_data['MstDepartment']['modifier']               = $this->Session->read('Auth.MstUser.id');
            $department_data['MstDepartment']['modified']               = $now;

            //SQL実行
            if(!$this->MstDepartment->save($department_data)){
                //ロールバック
                $this->MstDepartment->rollback();
                //エラーメッセージ
                $this->Session->setFlash('部署情報の登録に失敗しました。', 'growl', array('type'=>'error') );
                //リダイレクト
                $this->redirect('departments_list');
            }
            $this->MstDepartment->commit();
            
            $this->Session->delete('Department.readTime');//更新時間チェック用sessionの削除
            $Department_List = $this->getDepartmentsList( $this->request->data['MstDepartment']['mst_facility_id'] ,
                                                          true  ,
                                                          'id' ,
                                                          array('department_code' , 'department_name') ,
                                                          array(Configure::read('FacilityType.hospital'))
                                                          );
            
            $this->set('Department_List',$Department_List);
            
            
            $this->Session->write('Department_List',$Department_List);
            $this->render('result');
        }
    }
    public function export_csv(){
        App::import('Sanitize');
        //絞り込み条件
        $where = '';
        //部署名(LIKE検索)
        if($this->request->data['MstDepartment']['search_department_name'] != ""){
            $where .= " and  a.department_name LIKE '%".Sanitize::clean($this->request->data['MstDepartment']['search_department_name'])."%'";
        }
        
        //施設(完全一致・施設コードではない)
        if($this->request->data['MstDepartment']['search_facility_id'] != ""){
            $where .= ' and a.mst_facility_id = ' . $this->request->data['MstDepartment']['search_facility_id'];
        }
        
        //削除済み表示
        if($this->request->data['search_is_deleted'] == 0){
            $where .= ' and a.is_deleted = false ';
        }

        $sql  =' select ';
        $sql .='      d.facility_name                as 施設名 ';
        $sql .='    , a.department_code              as 部署コード ';
        $sql .='    , a.department_name              as 部署名 ';
        $sql .='    , a.department_formal_name       as 正式名 ';
        $sql .='    , a.priority                     as 引当優先度 ';
        $sql .='    , (  ';
        $sql .='      case  ';
        $sql .='        when a.is_deleted = true  ';
        $sql .="        then '○'  ";
        $sql .="        else ''  ";
        $sql .='        end ';
        $sql .='    )                                as 削除  ';
        $sql .='  from ';
        $sql .='    mst_departments as a  ';
        $sql .='    left join mst_facility_relations as b  ';
        $sql .='      on b.mst_facility_id = ' . $this->request->data['MstDepartment']['mst_facility_id'];
        $sql .='      and b.is_deleted = false  ';
        $sql .='    inner join mst_user_belongings as c  ';
        $sql .='      on c.mst_facility_id = a.mst_facility_id  ';
        $sql .='      and c.mst_user_id = ' . $this->request->data['MstDepartment']['mst_user_id'];
        $sql .='      and c.is_deleted = false  ';
        $sql .='    left join mst_facilities as d '; 
        $sql .='      on a.mst_facility_id = d.id  ';
        $sql .='  where ';
        $sql .="    d.facility_type = " . Configure::read('FacilityType.hospital') ;
        $sql .='    and b.partner_facility_id = d.id  ';
        $sql .=$where ;
        $sql .='  order by ';
        $sql .='    d.facility_code asc ';
        $sql .='    , a.department_code asc ';

        $this->db_export_csv($sql , "部署一覧", 'departments_list');
    }
}
