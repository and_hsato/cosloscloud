<?php
/**
 * D2ReceiptsController
 *　直納受領
 * @version 1.0.0
 * @since 2013/12/12
 */
class D2ReceiptsController extends AppController {

    /**
     * @var $name
     */
    var $name = 'd2receipts';

    /**
     * @var array $uses
     */
    var $uses = array('MstUser',
                      'MstFacilityRelation',
                      'MstFacility',
                      'MstFacilityItem',
                      'MstDepartment',
                      'MstClass',
                      'TrnShippingHeader',     //出荷ヘッダ
                      'TrnShipping',           //出荷明細
                      'MstItemUnit',
                      'MstDealer',             //販売元
                      'TrnSticker',            //シール
                      'TrnPromiseHeader',      //引当ヘッダ
                      'TrnPromise',            //引当明細
                      'MstTransactionConfig',  //仕入設定
                      'MstSalesConfig',        //売上設定
                      'TrnReceivingHeader',    //入荷ヘッダ
                      'TrnReceiving',          //入荷明細
                      'TrnStorageHeader',      //入庫ヘッダ
                      'TrnStorage',            //入庫明細
                      'TrnStock',              //在庫テーブル
                      'TrnClaim',              //請求テーブル（売上データ）
                      'MstUnitName',           //単位名
                      'TrnConsumeHeader',      //消費ヘッダ
                      'TrnConsume',            //消費明細
                      'TrnCloseHeader',        //締め
                      'TrnStickerRecord',      //シール移動履歴
                      'TrnOrderHeader',        //発注ヘッダ
                      'TrnOrder',              //発注明細
                      'TrnEdiShippingHeader',  //業者出荷ヘッダ
                      'TrnEdiShipping',        //業者出荷
                      );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html','Time', 'DatePicker');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Barcode');

    /**
     *
     */
    function index (){
        $this->setRoleFunction(52); //直納品要求受領登録

        //初期値
        $this->request->data['d2receipts']['work_date'] = date('Y/m/d');

        //作業区分プルダウン用データ取得
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 7));
        //得意先プルダウン用データ取得
        $this->set('customers_enabled',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected'),
                       array(Configure::read('FacilityType.hospital')),
                       true
                       )
                   );
    }

    /**
     *受領予定一覧
     */
    function ShippingsList (){

        $ShippingList = array();
        App::import('Sanitize');

        if(isset($this->request->data['search']['is_search'])){
            $sql  = ' select ';
            $sql .= '     f.id              as "d2receipts__id"';
            $sql .= '   , f.work_no         as "d2receipts__work_no"';
            $sql .= "   , to_char(f.work_date,'yyyy/mm/dd')";
            $sql .= '                       as "d2receipts__work_date"';
            $sql .= '   , h.facility_name   as "d2receipts__facility_name"';
            $sql .= '   , g.department_name as "d2receipts__department_name"';
            $sql .= '   , f.detail_count    as "d2receipts__detail_count"';
            $sql .= '   , count(*)          as "d2receipts__count"';
            $sql .= ' from ';
            $sql .= '   trn_edi_shippings as a  ';
            $sql .= '   left join trn_edi_receivings as b  ';
            $sql .= '     on b.id = a.trn_edi_receiving_id  ';
            $sql .= '   left join trn_orders as c  ';
            $sql .= '     on c.id = b.trn_order_id  ';
            $sql .= '   left join mst_item_units as d  ';
            $sql .= '     on d.id = c.mst_item_unit_id  ';
            $sql .= '   left join mst_facility_items as e  ';
            $sql .= '     on e.id = d.mst_facility_item_id  ';
            $sql .= '   left join mst_dealers as e1  ';
            $sql .= '     on e1.id = e.mst_dealer_id  ';
            $sql .= '   left join trn_edi_shipping_headers as f  ';
            $sql .= '     on f.id = a.trn_edi_shipping_header_id  ';
            $sql .= '   left join mst_departments as g  ';
            $sql .= '     on g.id = f.department_id_to  ';
            $sql .= '   left join mst_facilities as h  ';
            $sql .= '     on h.id = g.mst_facility_id  ';
            $sql .= ' where ';
            $sql .= '   a.is_deleted = false  ';
            $sql .= '   and c.order_type = '. Configure::read('OrderTypes.direct');
            $sql .= '   and h.id = ' . $this->request->data['d2receipts']['facilityId'];
            $sql .= '   and a.shipping_status = ' . Configure::read('ShippingStatus.ordered');
            // 絞込条件追加
            //部署：初期画面で部署が選択されている場合は検索条件に追加
            if($this->request->data['d2receipts']['departmentId'] != ""){
                $sql .= ' and g.id = ' .$this->request->data['d2receipts']['departmentId'];
            }

            //出荷番号(完全一致)-------------------------------------------------------
            if((isset($this->request->data['d2receipts']['search_shippings_no'])) && ($this->request->data['d2receipts']['search_shippings_no'] != "")){
                $sql .= ' and f.work_no = ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_shippings_no'])."' ";
            }

            //出荷日------------------------------------------------------------------
            if((isset($this->request->data['d2receipts']['search_start_date'])) && ($this->request->data['d2receipts']['search_start_date'] != "")){
                //開始日と終了日の両方に入力あり
                if((isset($this->request->data['d2receipts']['search_end_date'])) && ($this->request->data['d2receipts']['search_end_date'] != "")){
                    $sql .= ' and f.work_date >= ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_start_date'])."' ";
                    $sql .= ' and f.work_date <= ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_end_date'])."' ";

                    //開始日のみ入力あり
                } else {
                    $sql .= ' and f.work_date >= ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_start_date'])."' ";
                }
                //終了日のみ入力あり
            } else {
                if((isset($this->request->data['d2receipts']['search_end_date'])) && ($this->request->data['d2receipts']['search_end_date'] != "")){
                    $sql .= ' and f.work_date <= ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_end_date'])."' ";
                }
            }

            //ロット番号(完全一致)---------------------------------------------------
            if((isset($this->request->data['d2receipts']['search_lot_no'])) && ($this->request->data['d2receipts']['search_lot_no'] != "")){
                $sql .= ' and a.lot_no = ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_lot_no'])."' ";
            }
            //商品名(LIKE検索)-------------------------------------------------------
            if((isset($this->request->data['d2receipts']['search_item_name'])) && ($this->request->data['d2receipts']['search_item_name'] != "")){
                $sql .= ' and e.item_name LIKE ' ."'%".Sanitize::escape($this->request->data['d2receipts']['search_item_name'])."%' ";
            }

            //商品ID(完全一致)------------------------------------------------------------
            if((isset($this->request->data['d2receipts']['search_internal_code'])) && ($this->request->data['d2receipts']['search_internal_code'] != "")){
                $sql .= ' and e.internal_code = ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_internal_code'])."' ";
            }

            //規格(LIKE検索)------------------------------------------------------------
            if((isset($this->request->data['d2receipts']['search_standard'])) && ($this->request->data['d2receipts']['search_standard'] != "")){
                $sql .= ' and e.standard LIKE ' ."'%".Sanitize::escape($this->request->data['d2receipts']['search_standard'])."%' ";
            }

            //製品番号(LIKE検索)------------------------------------------------------------
            if((isset($this->request->data['d2receipts']['search_item_code'])) && ($this->request->data['d2receipts']['search_item_code'] != "")){
                $sql .= ' and e.item_code LIKE ' ."'%".Sanitize::escape($this->request->data['d2receipts']['search_item_code'])."%' ";
            }

            //販売元(LIKE検索)------------------------------------------------------------
            if((isset($this->request->data['d2receipts']['search_dealer_name'])) && ($this->request->data['d2receipts']['search_dealer_name'] != "")){
                $sql .= ' and e1.dealer_name LIKE ' ."'%".Sanitize::escape($this->request->data['d2receipts']['search_dealer_name'])."%' ";
            }
            
            $sql .= ' group by ';
            $sql .= '   f.id ';
            $sql .= '   , f.work_no ';
            $sql .= '   , f.work_date ';
            $sql .= '   , h.facility_name ';
            $sql .= '   , g.department_name ';
            $sql .= '   , f.detail_count ';
            $sql .= '  order by f.id ';
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnEdiShippingHeader'));
            $sql .= ' limit ' ;
            $sql .=        $this->_getLimitCount();

            $ShippingList = $this->TrnEdiShippingHeader->query($sql);
            
        } else {
            //初期表示データ
            $this->request->data['d2receipts']['search_start_date'] = date("Y/m/d", mktime(0, 0, 0, date("m"), date("d")-7, date("Y")));
            $this->request->data['d2receipts']['search_end_date']   = date('Y/m/d');

            // 施設コードから施設IDを取得
            $this->request->data['d2receipts']['facilityId'] = $this->getFacilityId($this->request->data['d2receipts']['facilityCode'] ,
                                                                                    Configure::read('FacilityType.hospital'));
            // 部署コードから部署IDを取得
            if($this->request->data['d2receipts']['departmentCode']!=''){
                $this->request->data['d2receipts']['departmentId'] = $this->getDepartmentId($this->request->data['d2receipts']['facilityId'],
                                                                                            Configure::read('DepartmentType.hospital'),
                                                                                            $this->request->data['d2receipts']['departmentCode']
                                                                                            );
            }
        }

        $this->request->data = AppController::outputFilter($this->request->data);
        $this->set('ShippingList', $ShippingList);
    }

    /**
     * 受領確認
     */
    function conf (){

        //更新時間チェック用に画面を開いた時間を格納
        $this->Session->write('d2receipts.readTime',date('Y-m-d H:i:s'));


        // 絞込条件追加
        $where = "";
        // 前画面選択項目
        $where .= ' and f.id in (' . join(',',$this->request->data['d2receipts']['id']). ') ';
        
        //部署：初期画面で部署が選択されている場合は検索条件に追加
        if($this->request->data['d2receipts']['departmentId'] != ""){
            $where .= ' and g.id = ' .$this->request->data['d2receipts']['departmentId'];
        }

        //出荷番号(完全一致)-------------------------------------------------------
        if((isset($this->request->data['d2receipts']['search_shippings_no'])) && ($this->request->data['d2receipts']['search_shippings_no'] != "")){
            $where .= ' and f.work_no = ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_shippings_no'])."' ";
        }

        //出荷日------------------------------------------------------------------
        if((isset($this->request->data['d2receipts']['search_start_date'])) && ($this->request->data['d2receipts']['search_start_date'] != "")){
            //開始日と終了日の両方に入力あり
            if((isset($this->request->data['d2receipts']['search_end_date'])) && ($this->request->data['d2receipts']['search_end_date'] != "")){
                $where .= ' and f.work_date >= ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_start_date'])."' ";
                $where .= ' and f.work_date <= ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_end_date'])."' ";

                //開始日のみ入力あり
            } else {
                $where .= ' and f.work_date >= ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_start_date'])."' ";
            }
            //終了日のみ入力あり
        } else {
            if((isset($this->request->data['d2receipts']['search_end_date'])) && ($this->request->data['d2receipts']['search_end_date'] != "")){
                $where .= ' and f.work_date <= ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_end_date'])."' ";
            }
        }

        //ロット番号(完全一致)---------------------------------------------------
        if((isset($this->request->data['d2receipts']['search_lot_no'])) && ($this->request->data['d2receipts']['search_lot_no'] != "")){
            $where .= ' and a.lot_no = ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_lot_no'])."' ";
        }
        //商品名(LIKE検索)-------------------------------------------------------
        if((isset($this->request->data['d2receipts']['search_item_name'])) && ($this->request->data['d2receipts']['search_item_name'] != "")){
            $where .= ' and e.item_name LIKE ' ."'%".Sanitize::escape($this->request->data['d2receipts']['search_item_name'])."%' ";
        }

        //商品ID(完全一致)------------------------------------------------------------
        if((isset($this->request->data['d2receipts']['search_internal_code'])) && ($this->request->data['d2receipts']['search_internal_code'] != "")){
            $where .= ' and e.internal_code = ' ."'".Sanitize::escape($this->request->data['d2receipts']['search_internal_code'])."' ";
        }

        //規格(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['d2receipts']['search_standard'])) && ($this->request->data['d2receipts']['search_standard'] != "")){
            $where .= ' and e.standard LIKE ' ."'%".Sanitize::escape($this->request->data['d2receipts']['search_standard'])."%' ";
        }

        //製品番号(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['d2receipts']['search_item_code'])) && ($this->request->data['d2receipts']['search_item_code'] != "")){
            $where .= ' and e.item_code LIKE ' ."'%".Sanitize::escape($this->request->data['d2receipts']['search_item_code'])."%' ";
        }

        //販売元(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['d2receipts']['search_dealer_name'])) && ($this->request->data['d2receipts']['search_dealer_name'] != "")){
            $where .= ' and e1.dealer_name LIKE ' ."'%".Sanitize::escape($this->request->data['d2receipts']['search_dealer_name'])."%' ";
        }

        $result = $this->getEdiShipping($where);
        //**************************************************************************

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('d2receipts.token' , $token);
        $this->request->data['d2receipts']['token'] = $token;

        $this->set('result', $result);
    }

    /**
     * 登録処理
     */
    function result () {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['d2receipts']['token'] === $this->Session->read('d2receipts.token')) {
            $this->Session->delete('d2receipts.token');
            
            //トランザクション開始
            $this->TrnReceivingHeader->begin();

            //行ロック
            $sql = " select * from trn_edi_shippings as a where a.id in ( " . join(',',$this->request->data['d2receipts']['id']) . ")";
            //作業番号
            $work_no = $this->setWorkNo4Header( $this->request->data['d2receipts']['work_date'], '18');
            $work_date = $this->request->data['d2receipts']['work_date'];
            
            //現在時間を取得
            $now = date('Y/m/d H:i:s');
            // 必要情報を取得
            $commonData = $this->getCommonData($this->request->data);
            
            // 入荷ヘッダ
            $TrnReceivingHeader = array(
                'TrnReceivingHeader'=> array(
                    'work_no'            => $work_no,
                    'work_date'          => $work_date,
                    'recital'            => $this->request->data['d2receipts']['recital'][0],
                    'receiving_type'     => Configure::read('ReceivingType.arrival'),            //入荷区分：入荷
                    'receiving_status'   => Configure::read('ReceivingStatus.stock'),            //入荷状態：入庫済
                    'department_id_from' => $commonData[0]['common']['supplier_department_id'],  //要求元部署参照キー（仕入先）
                    'department_id_to'   => $commonData[0]['common']['center_department_id'],    //要求先部署参照キー（センター）
                    'detail_count'       => count($this->request->data['d2receipts']['id']),     //明細数
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'created'            => $now,
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'modified'           => $now,
                    )
                );
            $this->TrnReceivingHeader->create(false);
            if (!$this->TrnReceivingHeader->save($TrnReceivingHeader)) {
                $this->TrnReceivingHeader->rollback();
                $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
            $last_receiving_header_id = $this->TrnReceivingHeader->getLastInsertID();
            
            $work_seq=1;
            foreach($commonData as &$c){
                // 発注の残数を0に
                $this->TrnOrder->updateAll(
                    array(
                        'TrnOrder.remain_count' => 'TrnOrder.remain_count - ' . $c['common']['quantity'],
                        'TrnOrder.order_status' => Configure::read('OrderStatus.partLoaded'), // 一部入庫済
                        ),
                    array(
                        'TrnOrder.id' => $c['common']['trn_order_id'],
                        )
                    ,-1);
                
                // 発注ステータス更新
                $this->TrnOrder->updateAll(
                    array(
                        'TrnOrder.order_status' => Configure::read('OrderStatus.loaded'), // 入庫済
                        ),
                    array(
                        'TrnOrder.id' => $c['common']['trn_order_id'],
                        'TrnOrder.remain_count' => 0,
                        )
                    ,-1);
                
                
                // 入荷明細
                $TrnReceiving = array(
                    'TrnReceiving'=> array(
                        'work_no'                 => $work_no,
                        'work_seq'                => $work_seq,
                        'work_date'               => $work_date,
                        'receiving_type'          => Configure::read('ReceivingType.arrival'), //入荷区分：入荷
                        'receiving_status'        => Configure::read('ReceivingStatus.stock'), //入荷状態：入庫済
                        'mst_item_unit_id'        => $c['common']['mst_item_unit_id'],         //包装単位参照キー
                        'department_id_from'      => $c['common']['supplier_department_id'],   //要求元部署参照キー（仕入先）
                        'department_id_to'        => $c['common']['center_department_id'],     //要求先部署参照キー（センター）
                        'quantity'                => $c['common']['quantity'],                 //数量
                        'remain_count'            => 0,
                        'stocking_price'          => $c['common']['stocking_price'],           //仕入れ価格
                        'trn_sticker_id'          => '',                                       //シール参照キー
                        'trn_order_id'            => $c['common']['trn_order_id'],             //発注参照キー
                        'trn_receiving_header_id' => $last_receiving_header_id,                //入荷ヘッダ外部キー
                        'creater'                 => $this->Session->read('Auth.MstUser.id'),
                        'created'                 => $now,
                        'modifier'                => $this->Session->read('Auth.MstUser.id'),
                        'modified'                => $now,
                        )
                    );
                $this->TrnReceiving->create(false);
                if (!$this->TrnReceiving->save($TrnReceiving)) {
                    $this->TrnReceivingHeader->rollback();
                    $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
                $c['common']['trn_receiving_id'] = $this->TrnReceiving->getLastInsertID();

                // 仕入
                $TrnClaim = array(
                    'TrnClaim'=> array(
                        'department_id_from'  => $c['common']['supplier_department_id'], //要求元部署参照キー（仕入先）
                        'department_id_to'    => $c['common']['center_department_id'],   //要求先部署参照キー（センター）
                        'mst_item_unit_id'    => $c['common']['mst_item_unit_id'],       //包装単位参照キー
                        'claim_date'          => $this->request->data['d2receipts']['work_date'],
                        'claim_price'         => $c['common']['stocking_price'] * $c['common']['quantity'], // TODO 丸め区分を使って丸める
                        'trn_receiving_id'    => $c['common']['trn_receiving_id'],
                        'stocking_close_type' => Configure::read('StockingCloseType.none'),
                        'sales_close_type'    => Configure::read('SalesCloseType.none'),
                        'facility_close_type' => Configure::read('FacilityCloseType.none'),
                        'is_deleted'          => false,
                        'is_stock_or_sale'    => Configure::read('Claim.stock'),
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        'count'               => $c['common']['quantity'],
                        'unit_price'          => $c['common']['stocking_price'], //仕入価格
                        'round'               => Configure::read('RoundType.Normal'),
                        'gross'               => Configure::read('GrossType.Seal'),
                        'is_not_retroactive'  => false,
                        )
                    );
                $this->TrnClaim->create(false);
                if (!$this->TrnClaim->save($TrnClaim)) {
                    $this->TrnReceivingHeader->rollback();
                    $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
                $c['common']['trn_claim_id'] = $this->TrnClaim->getLastInsertID();
                $work_seq++;
                $this->createMsClaimRecode($c['common']['trn_claim_id']);

                // 業者出荷のステータスを受領済みにする。
                $this->TrnEdiShipping->updateAll(
                    array(
                        'TrnEdiShipping.shipping_status' => Configure::read('ShippingStatus.loaded') ,
                        'TrnEdiShipping.trn_receiving_id' => $c['common']['trn_receiving_id'],
                        ),
                    array(
                        'TrnEdiShipping.id' => $c['common']['trn_edi_shipping_id'],
                        )
                    ,-1);
            }
            unset($c);
            
            $stickers = array();
            $work_seq=1;
            foreach($commonData as &$c){
                for($j = 0; $j < (integer)$c['common']['quantity']; $j++){
                    // シール
                    $stickerNo = $this->getCenterStickerNo($work_date, $c['common']['center_code']);
                    $hospitalStickerNo = $this->getHospitalStickerNo($work_date,$c['common']['hospital_code']);
                    $TrnSticker = array(
                        'TrnSticker'=> array(
                            'facility_sticker_no' => $stickerNo,
                            'hospital_sticker_no' => $hospitalStickerNo,
                            'mst_item_unit_id'    => $c['common']['mst_item_unit_id'],
                            'mst_department_id'   => $c['common']['hospital_department_id'],
                            'trade_type'          => Configure::read('ClassesType.PayDirectly'),
                            'quantity'            => 1,
                            'lot_no'              => trim($c['common']['lot_no']),
                            'validated_date'      => $c['common']['validated_date'],
                            'original_price'      => $c['common']['stocking_price'],
                            'transaction_price'   => $c['common']['stocking_price'],
                            'sales_price'         => 0,
                            'trn_order_id'        => $c['common']['trn_order_id'],
                            'trn_receiving_id'    => $c['common']['trn_receiving_id'],
                            'buy_claim_id'        => $c['common']['trn_claim_id'],
                            'is_deleted'          => false,
                            'has_reserved'        => false,
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now,
                            )
                        );
                    $this->TrnSticker->create(false);
                    if (!$this->TrnSticker->save($TrnSticker)) {
                        $this->TrnReceivingHeader->rollback();
                        $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('/');
                    }
                    $lastStickerId = $this->TrnSticker->getLastInsertID();
                    $stickers[$work_seq]['trn_sticker_id'] = $lastStickerId;
                    $stickers[$work_seq]['facility_sticker_no'] = $stickerNo;
                    $stickers[$work_seq]['hospital_sticker_no'] = $hospitalStickerNo;
                    $stickers[$work_seq]['mst_item_unit_id'] =  $c['common']['mst_item_unit_id'];
                    $stickers[$work_seq]['trn_order_id'] =  $c['common']['trn_order_id'];
                    
                    
                    //入庫明細
                    $TrnStorage = array(
                        'TrnStorage'=> array(
                        'work_no'             => $work_no,
                        'work_seq'            => $work_seq,
                        'work_date'           => $work_date,
                        'storage_type'        => Configure::read('StorageType.direct'),
                        'storage_status'      => Configure::read('StorageStatus.arrival'),
                        'mst_item_unit_id'    => $c['common']['mst_item_unit_id'],
                        'mst_department_id'   => $c['common']['center_department_id'],
                        'quantity'            => 1,
                        'trn_sticker_id'      => $lastStickerId,
                        'trn_receiving_id'    => $c['common']['trn_receiving_id'],
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        'facility_sticker_no' => $stickerNo,
                        'hospital_sticker_no' => $hospitalStickerNo,
                        )
                        );
                    $this->TrnStorage->create(false);
                    if (!$this->TrnStorage->save($TrnStorage)) {
                        $this->TrnReceivingHeader->rollback();
                        $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('/');
                    }
                    $lastStorageId = $this->TrnStorage->getLastInsertID();
                    $stickers[$work_seq]['trn_storage_id'] = $lastStorageId;
                    $work_seq++;
                    //シール更新
                    $this->TrnSticker->updateAll(
                        array(
                            'TrnSticker.trn_storage_id' => $lastStorageId,
                            ),
                        array(
                            'TrnSticker.id' => $lastStickerId,
                            )
                        ,-1);
                }
            }
            
            unset($c);
            
            // 入庫ヘッダ
            $TrnStorageHeader = array(
                'TrnStorageHeader'=> array(
                    'work_no'                 => $work_no,
                    'work_date'               => $work_date,
                    'storage_type'            => Configure::read('StorageType.direct'),
                    'storage_status'          => Configure::read('StorageStatus.arrival'),
                    'mst_department_id'       => $commonData[0]['common']['center_department_id'],
                    'detail_count'            => count($stickers),
                    'is_deleted'              => false,
                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                    'created'                 => $now,
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'modified'                => $now,
                    'trn_receiving_header_id' => $last_receiving_header_id,
                    )
                );
            $this->TrnStorageHeader->create(false);
            if (!$this->TrnStorageHeader->save($TrnStorageHeader)) {
                $this->TrnReceivingHeader->rollback();
                $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
            
            // 出荷ヘッダ
            $TrnShippingHeader = array(
                'TrnShippingHeader'=> array(
                    'work_no'            => $work_no,
                    'work_date'          => $work_date,
                    'shipping_type'      => Configure::read('ShippingType.direct'),
                    'shipping_status'    => Configure::read('ShippingStatus.loaded'), //受領済
                    'department_id_from' => $commonData[0]['common']['center_department_id'],
                    'department_id_to'   => $commonData[0]['common']['hospital_department_id'],
                    'detail_count'       => count($stickers),
                    'is_deleted'         => false,
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'created'            => $now,
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'modified'           => $now,
                    )
                );
            $this->TrnShippingHeader->create(false);
            if (!$this->TrnShippingHeader->save($TrnShippingHeader)) {
                $this->TrnReceivingHeader->rollback();
                $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
            
            $last_shipping_header_id = $this->TrnShippingHeader->getLastInsertID();
            
            // 出荷
            foreach($stickers as $work_seq => &$s){
                // 出荷
                $TrnShipping = array(
                    'TrnShipping'=> array(
                        'work_no'                => $work_no,
                        'work_seq'               => $work_seq,
                        'work_date'              => $work_date,
                        'shipping_type'          => Configure::read('ShippingType.direct'),
                        'shipping_status'        => Configure::read('ShippingStatus.loaded'), //受領済
                        'mst_item_unit_id'       => $s['mst_item_unit_id'],
                        'department_id_from'     => $commonData[0]['common']['center_department_id'],
                        'department_id_to'       => $commonData[0]['common']['hospital_department_id'],
                        'quantity'               => 1,
                        'trn_sticker_id'         => $s['trn_sticker_id'],
                        'is_deleted'             => false,
                        'creater'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                => $now,
                        'modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'modified'               => $now,
                        'trn_shipping_header_id' => $last_shipping_header_id,
                        'facility_sticker_no'    => $s['facility_sticker_no'],
                        'hospital_sticker_no'    => $s['hospital_sticker_no'],
                        )
                    );
                $this->TrnShipping->create(false);
                if (!$this->TrnShipping->save($TrnShipping)) {
                    $this->TrnReceivingHeader->rollback();
                    $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
                $s['trn_shipping_id'] = $this->TrnShipping->getLastInsertID();
                
            }
            unset($s);
            
            // 受領ヘッダ
            $TrnReceivingHeader = array(
                'TrnReceivingHeader'=> array(
                    'work_no'                => $work_no,
                    'work_date'              => $work_date,
                    'receiving_type'         => Configure::read('ReceivingType.receipt'),
                    'receiving_status'       => Configure::read('ReceivingStatus.stock'),
                    'department_id_from'     => $commonData[0]['common']['center_department_id'],
                    'department_id_to'       => $commonData[0]['common']['hospital_department_id'],
                    'detail_count'           => count($stickers),
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'trn_shipping_header_id' => $last_shipping_header_id,//出荷ヘッダ参照キー
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'modified'               => $now,
                    )
                );
            $this->TrnReceivingHeader->create(false);
            if (!$this->TrnReceivingHeader->save($TrnReceivingHeader)) {
                $this->TrnReceivingHeader->rollback();
                $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
            
            $last_recipt_header_id = $this->TrnReceivingHeader->getLastInsertID();
            
            // 病院側入庫ヘッダ
            $TrnStorageHeader = array(
                'TrnStorageHeader'=> array(
                    'work_no'                 => $work_no,
                    'work_date'               => $work_date,
                    'storage_type'            => Configure::read('ShippingType.direct'),       //入庫区分
                    'storage_status'          => Configure::read('StorageStatus.stock'), //入庫状態
                    'mst_department_id'       => $commonData[0]['common']['hospital_department_id'],        //部署参照キー
                    'detail_count'            => count($stickers),
                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                    'created'                 => $now,
                    'trn_receiving_header_id' => $last_recipt_header_id,                 //入荷ヘッダ参照キー
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'modified'                => $now,
                    )
                );
            $this->TrnStorageHeader->create(false);
            if (!$this->TrnStorageHeader->save($TrnStorageHeader)) {
                $this->TrnReceivingHeader->rollback();
                $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }

            $last_reciptStorageHeader_id = $this->TrnStorageHeader->getLastInsertID();
            
            // 受領
            foreach($stickers as $work_seq => &$s){
                // 受領
                $TrnReceiving = array(
                    'TrnReceiving'=> array(
                        'work_no'                 => $work_no,
                        'work_seq'                => $work_seq,
                        'work_date'               => $work_date,
                        'receiving_type'          => Configure::read('ReceivingType.receipt'), //入荷区分:受領による入荷
                        'receiving_status'        => Configure::read('ShippingStatus.loaded'), //入荷状態（受領済み）
                        'mst_item_unit_id'        => $s['mst_item_unit_id'],                   //包装単位参照キー
                        'department_id_from'      => $commonData[0]['common']['center_department_id'],
                        'department_id_to'        => $commonData[0]['common']['hospital_department_id'],
                        'quantity'                => 1,                                        //数量
                        'stocking_price'          => 0,                                        //仕入れ価格
                        'sales_price'             => 0,                                        //売上価格
                        'trn_sticker_id'          => $s['trn_sticker_id'],                     //シール参照キー
                        'trn_shipping_id'         => $s['trn_shipping_id'],                    //出荷参照キー
                        'trn_order_id'            => $s['trn_order_id'],                       //発注参照キー
                        'trn_receiving_header_id' => $last_recipt_header_id,                   //入荷ヘッダ外部キー
                        'creater'                 => $this->Session->read('Auth.MstUser.id'),
                        'created'                 => $now,
                        'modifier'                => $this->Session->read('Auth.MstUser.id'),
                        'modified'                => $now,
                        'facility_sticker_no'     => $s['facility_sticker_no'],
                        'hospital_sticker_no'     => $s['hospital_sticker_no'],
                        )
                    );
                $this->TrnReceiving->create(false);
                if (!$this->TrnReceiving->save($TrnReceiving)) {
                    $this->TrnReceivingHeader->rollback();
                    $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
                $last_recipt_id = $this->TrnReceiving->getLastInsertID();
                
                //病院側入庫明細
                $TrnStorage = array(
                    'TrnStorage'=> array(
                        'work_no'               => $work_no,
                        'work_seq'              => $work_seq,
                        'work_date'             => $work_date,
                        'storage_type'          => Configure::read('ShippingType.direct'),    //入庫区分
                        'storage_status'        => Configure::read('ReceivingStatus.stock'),  //入庫状態
                        'mst_item_unit_id'      => $s['mst_item_unit_id'],                    //包装単位参照キー
                        'mst_department_id'     => $commonData[0]['common']['hospital_department_id'], //部署参照キー
                        'quantity'              => 1,                                         //数量
                        'trn_sticker_id'        => $s['trn_sticker_id'],                      //シール参照キー
                        'trn_receiving_id'      => $last_recipt_id,                           //入荷参照キー
                        'trn_storage_header_id' => $last_reciptStorageHeader_id,              //入庫ヘッダ外部キー
                        'creater'               => $this->Session->read('Auth.MstUser.id'),
                        'created'               => $now,
                        'modifier'              => $this->Session->read('Auth.MstUser.id'),
                        'modified'              => $now,
                        'facility_sticker_no'   => $s['facility_sticker_no'],
                        'hospital_sticker_no'   => $s['hospital_sticker_no'],
                        )
                    );
                    if (!$this->TrnStorage->save($TrnStorage)) {
                        $this->TrnReceivingHeader->rollback();
                        $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('/');
                    }

                // シール移動履歴
                $TrnStickerRecord = array(
                    'TrnStickerRecord'=> array(
                        'move_date'           => $work_date,
                        'move_seq'            => $this->getNextRecord($s['trn_sticker_id']),
                        'record_type'         => Configure::read('RecordType.direct'),
                        'record_id'           => $s['trn_storage_id'],
                        'trn_sticker_id'      => $s['trn_sticker_id'],
                        'mst_department_id'   => $commonData[0]['common']['hospital_department_id'],
                        'facility_sticker_no' => $s['facility_sticker_no'],
                        'hospital_sticker_no' => $s['hospital_sticker_no'],
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        )
                    );
                if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                    $this->TrnReceivingHeader->rollback();
                    $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
            }
            unset($s);
            
            // 直納フラグ判定
            if($this->Session->read('Auth.Config.D2Consumes') == '0' ){
                // 消費ありの場合
                
                // 消費ヘッダ
                $TrnConsumeHeader = array(
                    'TrnConsumeHeader'=> array(
                        'work_no'           => $work_no,
                        'work_date'         => $work_date,
                        'use_type'          => Configure::read('UseType.direct'), //消費区分：直納品
                        'mst_department_id' => $commonData[0]['common']['hospital_department_id'],   //部署参照キー
                        'detail_count'      => count($stickers),
                        'creater'           => $this->Session->read('Auth.MstUser.id'),
                        'created'           => $now,
                        'modifier'          => $this->Session->read('Auth.MstUser.id'),
                        'modified'          => $now,
                        'is_deleted'        => false,
                        )
                    );
                $this->TrnConsumeHeader->create(false);
                if (!$this->TrnConsumeHeader->save($TrnConsumeHeader)) {
                    $this->TrnReceivingHeader->rollback();
                    $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
                
                $last_consumeHeader_id = $this->TrnConsumeHeader->getLastInsertID();
                foreach($stickers as $work_seq => $s){
                    // 消費
                    $TrnConsume = array(
                        'TrnConsume'=> array(
                            'work_no'               => $work_no,
                            'work_seq'              => $work_seq,
                            'work_date'             => $work_date,
                            'use_type'              => Configure::read('UseType.direct'), //消費区分：直納品
                            'mst_item_unit_id'      => $s['mst_item_unit_id'],            //包装単位参照キー
                            'mst_department_id'     => $commonData[0]['common']['hospital_department_id'],   //部署参照キー
                            'quantity'              => 1,                                 //数量
                            'trn_sticker_id'        => $s['trn_sticker_id'],              //シール参照キー
                            'trn_storage_id'        => $s['trn_storage_id'],              //入庫参照キー
                            'trn_consume_header_id' => $last_consumeHeader_id,            //消費ヘッダ外部キー
                            'is_retroactable'       => false,
                            'creater'               => $this->Session->read('Auth.MstUser.id'),
                            'created'               => $now,
                            'modifier'              => $this->Session->read('Auth.MstUser.id'),
                            'modified'              => $now,
                            'is_deleted'            => false,
                            'facility_sticker_no'   => $s['facility_sticker_no'],
                            'hospital_sticker_no'   => $s['hospital_sticker_no'],
                            )
                        );
                    $this->TrnConsume->create(false);
                    if (!$this->TrnConsume->save($TrnConsume)) {
                        $this->TrnReceivingHeader->rollback();
                        $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('/');
                    }
                    
                    $lastConsumeId = $this->TrnConsume->getLastInsertID();
                    $salse_price = $this->getSalesPrice($s['mst_item_unit_id'] , $commonData[0]['common']['hospital_department_id'] , $work_date);
                    // 売上
                    $TrnClaim = array(
                        'TrnClaim'=> array(
                            'department_id_from'  => $commonData[0]['common']['center_department_id'],   //要求元部署参照キー（センター）
                            'department_id_to'    => $commonData[0]['common']['hospital_department_id'], //要求先部署参照キー（病院）
                            'mst_item_unit_id'    => $s['mst_item_unit_id'],      //包装単位参照キー
                            'claim_date'          => $work_date,
                            'claim_price'         => $salse_price, // 数量x単価
                            'trn_shipping_id'     => $s['trn_shipping_id'],
                            'trn_consume_id'      => $lastConsumeId,
                            'stocking_close_type' => Configure::read('StockingCloseType.none'),
                            'sales_close_type'    => Configure::read('SalesCloseType.none'),
                            'facility_close_type' => Configure::read('FacilityCloseType.none'),
                            'is_deleted'          => false,
                            'is_stock_or_sale'    => Configure::read('Claim.sales'),
                            'trade_type'          => Configure::read('ClassesType.PayDirectly'),
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now,
                            'count'               => 1,
                            'unit_price'          => $salse_price, //価格
                            'round'               => Configure::read('RoundType.Normal'),
                            'gross'               => Configure::read('GrossType.Seal'),
                            'is_not_retroactive'  => false,
                            'facility_sticker_no' => $s['facility_sticker_no'],
                            'hospital_sticker_no' => $s['hospital_sticker_no'],
                            )
                        );
                    $this->TrnClaim->create(false);
                    if (!$this->TrnClaim->save($TrnClaim)) {
                        $this->TrnReceivingHeader->rollback();
                        $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('/');
                    }
                    
                    //シール更新 数量0
                    $this->TrnSticker->updateAll(
                        array(
                            'TrnSticker.quantity' => 0,
                            ),
                        array(
                            'TrnSticker.id' => $s['trn_sticker_id'],
                            )
                        ,-1);


                    // シール移動履歴
                    $TrnStickerRecord = array(
                        'TrnStickerRecord'=> array(
                            'move_date'           => $work_date,
                            'move_seq'            => $this->getNextRecord($s['trn_sticker_id']),
                            'record_type'         => Configure::read('RecordType.consume'),
                            'record_id'           => $lastConsumeId,
                            'trn_sticker_id'      => $s['trn_sticker_id'],
                            'mst_department_id'   => $commonData[0]['common']['hospital_department_id'],
                            'facility_sticker_no' => $s['facility_sticker_no'],
                            'hospital_sticker_no' => $s['hospital_sticker_no'],
                            'is_deleted'          => false,
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now,
                            )
                        );
                    if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                        $this->TrnReceivingHeader->rollback();
                        $this->Session->setFlash('直納要求受領登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('/');
                    }
                }
            }else{
                // 病院在庫を増やす
                foreach($stickers as $s){
                    $this->updateTrnStock($s['trn_sticker_id'] , $now);
                }
            }
            
            //コミット
            $this->TrnReceivingHeader->commit();
            
            //完了画面遷移用に出荷データを再取得
            $where = ' and a.id in ( ' . join(',',$this->request->data['d2receipts']['id']) . ')';
            $result = $this->getEdiShipping($where);
            
            $this->set('result', $result);
            $this->Session->Write('d2recepts.result' , $result );
        }else{
            $result = $this->Session->read('d2recepts.result');
            $this->set('result' , $result);
        }
    }

    /**
     * 履歴
     */
    function history(){
        $this->setRoleFunction(53); //直納品要求受領履歴
        $result = array();
        $facility_list = $this->getFacilityList($this->Session->read('Auth.facility_id_selected') , array(Configure::read('FacilityType.hospital')));
        $department_list = array();
        $class_list = $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 18);
        
        // 施設コードが入力済みの場合、部署リストを取得しておく
        if(isset($this->request->data['d2receipts']['facilityCode']) && $this->request->data['d2receipts']['facilityCode'] != "" ){
            $department_list = $this->getDepartmentsList($this->request->data['d2receipts']['facilityCode']);
        }
        
        if(isset($this->request->data['d2receipts']['is_search'])){
            $where = "";
            // 絞り込み条件追加

            //要求番号
            if(isset($this->request->data['d2receipts']['work_no']) && $this->request->data['d2receipts']['work_no'] != "" ){
                $where .= " and b.work_no = '" . $this->request->data['d2receipts']['work_no'] . "'";
            }
            //要求日From
            if(isset($this->request->data['d2receipts']['work_date_from']) && $this->request->data['d2receipts']['work_date_from'] != "" ){
                $where .= " and b.work_date >= '" . $this->request->data['d2receipts']['work_date_from'] . "'";
            }
            //要求日To
            if(isset($this->request->data['d2receipts']['work_date_to']) && $this->request->data['d2receipts']['work_date_to'] != "" ){
                $where .= " and b.work_date <= '" . $this->request->data['d2receipts']['work_date_to'] . "'";
            }

            //取消も表示する
            if(isset($this->request->data['d2receipts']['is_deleted']) && $this->request->data['d2receipts']['is_deleted'] != "1" ){
                $where .= " and a.is_deleted = false ";
                $where .= " and b.is_deleted = false ";
            }
            
            //要求施設
            if(isset($this->request->data['d2receipts']['facilityCode']) && $this->request->data['d2receipts']['facilityCode'] != "" ){
                $where .= " and i.facility_code = '" . $this->request->data['d2receipts']['facilityCode'] . "'";
            }
            
            //要求部署
            if(isset($this->request->data['d2receipts']['departmentCode']) && $this->request->data['d2receipts']['departmentCode'] != "" ){
                $where .= " and h.department_code = '" . $this->request->data['d2receipts']['departmentCode'] . "'";
            }
            
            //商品ID
            if(isset($this->request->data['d2receipts']['internal_code']) && $this->request->data['d2receipts']['internal_code'] != "" ){
                $where .= " and d.internal_code = '" . $this->request->data['d2receipts']['internal_code'] . "'";
            }
            
            //製品番号
            if(isset($this->request->data['d2receipts']['item_code']) && $this->request->data['d2receipts']['item_code'] != "" ){
                $where .= " and d.item_code = '%" . $this->request->data['d2receipts']['item_code'] . "%'";
            }
            
            //商品名
            if(isset($this->request->data['d2receipts']['item_name']) && $this->request->data['d2receipts']['item_name'] != "" ){
                $where .= " and d.item_name = '" . $this->request->data['d2receipts']['item_name'] . "'";
            }
            
            //販売元
            if(isset($this->request->data['d2receipts']['dealer_name']) && $this->request->data['d2receipts']['dealer_name'] != "" ){
                $where .= " and e.dealer_name = '%" . $this->request->data['d2receipts']['dealer_name'] . "%'";
            }
            
            //作業区分
            if(isset($this->request->data['d2receipts']['work_type']) && $this->request->data['d2receipts']['work_type'] != "" ){
                $where .= " and b.work_type = '" . $this->request->data['d2receipts']['work_type'] . "'";
            }
            
            //規格
            if(isset($this->request->data['d2receipts']['standard']) && $this->request->data['d2receipts']['standard'] != "" ){
                $where .= " and d.standard like '%" . $this->request->data['d2receipts']['standard'] . "%'";
            }
            
            //JANコード
            if(isset($this->request->data['d2receipts']['jan_code']) && $this->request->data['d2receipts']['jan_code'] != "" ){
                $where .= " and d.jan_code = '" . $this->request->data['d2receipts']['jan_code'] . "'";
            }

            
            $sql  = ' select ';
            $sql .= '     b.id              as "d2receipts__id"';
            $sql .= '   , b.work_no         as "d2receipts__work_no"';
            $sql .= "   , to_char(b.work_date,'YYYY/mm/dd') ";
            $sql .= '                       as "d2receipts__work_date"';
            $sql .= '   , i.facility_name   as "d2receipts__facility_name"';
            $sql .= '   , i.facility_code   as "d2receipts__facility_code"';
            $sql .= '   , h.department_name as "d2receipts__department_name" ';
            $sql .= '   , h.department_code as "d2receipts__department_code"';
            $sql .= '   , j.user_name       as "d2receipts__user_name"';
            $sql .= "   , to_char(b.created,'YYYY/mm/dd HH24:MI:SS') ";
            $sql .= '                       as "d2receipts__created"';
            $sql .= '   , b.recital         as "d2receipts__recital"';
            $sql .= '   , b.detail_count    as "d2receipts__detail_count"';
            $sql .= '   , count(*)          as "d2receipts__count"';
            $sql .= ' from ';
            $sql .= '   trn_receivings as a  ';
            $sql .= '   left join trn_receiving_headers as b  ';
            $sql .= '     on b.id = a.trn_receiving_header_id  ';
            $sql .= '   left join mst_item_units as c  ';
            $sql .= '     on c.id = a.mst_item_unit_id  ';
            $sql .= '   left join mst_facility_items as d  ';
            $sql .= '     on d.id = c.mst_facility_item_id  ';
            $sql .= '   left join mst_dealers as e  ';
            $sql .= '     on e.id = d.mst_dealer_id  ';
            $sql .= '   left join trn_orders as f  ';
            $sql .= '     on f.id = a.trn_order_id  ';
            $sql .= '   left join trn_edi_receivings as g  ';
            $sql .= '     on g.trn_order_id = f.id  ';
            $sql .= '   left join mst_departments as h  ';
            $sql .= '     on h.id = f.department_id_from  ';
            $sql .= '   left join mst_facilities as i  ';
            $sql .= '     on i.id = h.mst_facility_id  ';
            $sql .= '   left join mst_users as j  ';
            $sql .= '     on j.id = a.creater  ';
            $sql .= ' where ';
            $sql .= '   c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '   and a.receiving_type = ' . Configure::read('ReceivingType.arrival');
            $sql .= '   and g.id is not null  ';
            $sql .= '   and f.order_type = ' . Configure::read('OrderTypes.direct');
            $sql .= $where;
            $sql .= ' group by ';
            $sql .= '   b.id ';
            $sql .= '   , b.work_no ';
            $sql .= '   , b.work_date ';
            $sql .= '   , i.facility_name ';
            $sql .= '   , i.facility_code ';
            $sql .= '   , h.department_name ';
            $sql .= '   , h.department_code ';
            $sql .= '   , j.user_name ';
            $sql .= '   , b.created ';
            $sql .= '   , b.recital ';
            $sql .= '   , b.detail_count ';
            $sql .= ' order by b.work_no ';
            $result = $this->TrnReceiving->query($sql);
        }
        
        $this->set('result' , $result);
        $this->set('facility_list' , $facility_list);
        $this->set('department_list' , $department_list);
        $this->set('class_list' , $class_list);
    }

    /**
     * 履歴明細
     */
    function history_confirm(){

        $where = '';
        //要求番号
        if(isset($this->request->data['d2receipts']['work_no']) && $this->request->data['d2receipts']['work_no'] != "" ){
            $where .= " and b.work_no = '" . $this->request->data['d2receipts']['work_no'] . "'";
        }
        //要求日From
        if(isset($this->request->data['d2receipts']['work_date_from']) && $this->request->data['d2receipts']['work_date_from'] != "" ){
            $where .= " and b.work_date >= '" . $this->request->data['d2receipts']['work_date_from'] . "'";
        }
        //要求日To
        if(isset($this->request->data['d2receipts']['work_date_to']) && $this->request->data['d2receipts']['work_date_to'] != "" ){
            $where .= " and b.work_date <= '" . $this->request->data['d2receipts']['work_date_to'] . "'";
        }
        
        //取消も表示する
        if(isset($this->request->data['d2receipts']['is_deleted']) && $this->request->data['d2receipts']['is_deleted'] != "1" ){
            $where .= " and a.is_deleted = false ";
            $where .= " and b.is_deleted = false ";
        }
        
        //要求施設
        if(isset($this->request->data['d2receipts']['facilityCode']) && $this->request->data['d2receipts']['facilityCode'] != "" ){
            $where .= " and i.facility_code = '" . $this->request->data['d2receipts']['facilityCode'] . "'";
        }
        
        //要求部署
        if(isset($this->request->data['d2receipts']['departmentCode']) && $this->request->data['d2receipts']['departmentCode'] != "" ){
            $where .= " and h.department_code = '" . $this->request->data['d2receipts']['departmentCode'] . "'";
        }
        
        //商品ID
        if(isset($this->request->data['d2receipts']['internal_code']) && $this->request->data['d2receipts']['internal_code'] != "" ){
            $where .= " and d.internal_code = '" . $this->request->data['d2receipts']['internal_code'] . "'";
        }
        
        //製品番号
        if(isset($this->request->data['d2receipts']['item_code']) && $this->request->data['d2receipts']['item_code'] != "" ){
            $where .= " and d.item_code = '%" . $this->request->data['d2receipts']['item_code'] . "%'";
        }

        //商品名
        if(isset($this->request->data['d2receipts']['item_name']) && $this->request->data['d2receipts']['item_name'] != "" ){
            $where .= " and d.item_name = '" . $this->request->data['d2receipts']['item_name'] . "'";
        }

        //販売元
        if(isset($this->request->data['d2receipts']['dealer_name']) && $this->request->data['d2receipts']['dealer_name'] != "" ){
            $where .= " and e.dealer_name = '%" . $this->request->data['d2receipts']['dealer_name'] . "%'";
        }

        //作業区分
        if(isset($this->request->data['d2receipts']['work_type']) && $this->request->data['d2receipts']['work_type'] != "" ){
            $where .= " and b.work_type = '" . $this->request->data['d2receipts']['work_type'] . "'";
        }

        //規格
        if(isset($this->request->data['d2receipts']['standard']) && $this->request->data['d2receipts']['standard'] != "" ){
            $where .= " and d.standard like '%" . $this->request->data['d2receipts']['standard'] . "%'";
        }

        //JANコード
        if(isset($this->request->data['d2receipts']['jan_code']) && $this->request->data['d2receipts']['jan_code'] != "" ){
            $where .= " and d.jan_code = '" . $this->request->data['d2receipts']['jan_code'] . "'";
        }
        //選択ID
        $where .= ' and b.id in ( ' . join(',',$this->request->data['d2receipts']['id']) . ')';
        $sql  = ' select ';
        $sql .= '     a.id              as "d2receipts__id"';
        $sql .= '   , a.work_no         as "d2receipts__work_no"';
        $sql .= "   , to_char(a.work_date,'YYYY/mm/dd') ";
        $sql .= '                       as "d2receipts__work_date"';
        $sql .= '   , i.facility_name   as "d2receipts__facility_name"';
        $sql .= '   , i.facility_code   as "d2receipts__facility_code"';
        $sql .= '   , h.department_name as "d2receipts__department_name" ';
        $sql .= '   , h.department_code as "d2receipts__department_code"';
        $sql .= '   , j.user_name       as "d2receipts__user_name"';
        $sql .= '   , a.recital         as "d2receipts__recital"';
        $sql .= '   , a.quantity        as "d2receipts__quantity"';
        
        $sql .= '   , d.internal_code   as "d2receipts__internal_code"';
        $sql .= '   , d.item_name       as "d2receipts__item_name"';
        $sql .= '   , d.item_code       as "d2receipts__item_code"';
        $sql .= '   , d.standard        as "d2receipts__standard"';
        $sql .= '   , e.dealer_name     as "d2receipts__dealer_name"';
        $sql .= '   , k.name            as "d2receipts__class_name"';
        $sql .= '   , ( case ';
        $sql .= '       when c.per_unit = 1 then c1.unit_name ';
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '        end )          as "d2receipts__unit_name"';
        $sql .= '   , ( case ';
        $sql .= '       when a.is_deleted = true then false ';
        $sql .= '       else true ';
        $sql .= '       end )           as "d2receipts__canCancel"';
        $sql .= ' from ';
        $sql .= '   trn_receivings as a  ';
        $sql .= '   left join trn_receiving_headers as b  ';
        $sql .= '     on b.id = a.trn_receiving_header_id  ';
        $sql .= '   left join mst_item_units as c  ';
        $sql .= '     on c.id = a.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as c1  ';
        $sql .= '     on c1.id = c.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as c2  ';
        $sql .= '     on c2.id = c.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as d  ';
        $sql .= '     on d.id = c.mst_facility_item_id  ';
        $sql .= '   left join mst_dealers as e  ';
        $sql .= '     on e.id = d.mst_dealer_id  ';
        $sql .= '   left join trn_orders as f  ';
        $sql .= '     on f.id = a.trn_order_id  ';
        $sql .= '   left join trn_edi_receivings as g  ';
        $sql .= '     on g.trn_order_id = f.id  ';
        $sql .= '   left join mst_departments as h  ';
        $sql .= '     on h.id = f.department_id_from  ';
        $sql .= '   left join mst_facilities as i  ';
        $sql .= '     on i.id = h.mst_facility_id  ';
        $sql .= '   left join mst_users as j  ';
        $sql .= '     on j.id = a.creater  ';
        $sql .= '   left join mst_classes as k';
        $sql .= '     on k.id = a.work_type';
        $sql .= ' where ';
        $sql .= '   c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '   and a.receiving_type = ' . Configure::read('ReceivingType.arrival');
        $sql .= '   and g.id is not null  ';
        $sql .= '   and f.order_type = ' . Configure::read('OrderTypes.direct');
        $sql .= $where;
        $sql .= ' order by a.work_no , a.work_seq';
        $result = $this->TrnReceiving->query($sql);

        $this->set('result' , $result);
    }

    /**
     * 履歴編集結果
     */
    function history_result(){
        
        $this->TrnReceiving->begin();
        //行ロック
        $this->TrnReceiving->query(' select * from trn_receivings as a where a.id in ( ' . join(',',$this->request->data['d2receipts']['id']) . ' ) for update ');

        $sql  = ' select ';
        $sql .= '       a.id                   as receiving_id ';
        $sql .= '     , b.id                   as storage_id ';
        $sql .= '     , c.id                   as sticker_id ';
        $sql .= '     , d.id                   as shipping_id ';
        $sql .= '     , e.id                   as receipt_id ';
        $sql .= '     , f.id                   as consume_id ';
        $sql .= '     , g.id                   as sale_id ';
        $sql .= '     , h.id                   as stock_id ';
        $sql .= '     , c.quantity  ';
        $sql .= '   from ';
        $sql .= '     trn_receivings as a  ';
        $sql .= '     left join trn_storages as b  ';
        $sql .= '       on b.trn_receiving_id = a.id  ';
        $sql .= '     left join trn_stickers as c  ';
        $sql .= '       on c.id = b.trn_sticker_id  ';
        $sql .= '     left join trn_shippings as d  ';
        $sql .= '       on d.trn_sticker_id = c.id  ';
        $sql .= '     left join trn_receivings as e  ';
        $sql .= '       on e.trn_sticker_id = c.id  ';
        $sql .= '     left join trn_consumes as f  ';
        $sql .= '       on f.trn_sticker_id = c.id  ';
        $sql .= '     left join trn_claims as g  ';
        $sql .= '       on g.trn_consume_id = f.id  ';
        $sql .= '     left join trn_stocks as h ';
        $sql .= '       on h.mst_item_unit_id = c.mst_item_unit_id ';
        $sql .= '      and h.mst_department_id = c.mst_department_id ';
        $sql .= '   where ';
        $sql .= '     a.id in ( ' . join(',',$this->request->data['d2receipts']['id']) . ')';
        
        $res = $this->TrnReceiving->query($sql);
        foreach($res as $r){
            //入庫
            $ret = $this->TrnStorage->del(array('id'=>$r[0]['storage_id']));
            if(!$ret) {
                $this->TrnReceiving->rollback();
                $this->Session->setFlash('直納要求受領取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('order_history');
            }
            
            //出荷
            $ret = $this->TrnShipping->del(array('id'=>$r[0]['shipping_id']));
            if(!$ret) {
                $this->TrnReceiving->rollback();
                $this->Session->setFlash('直納要求受領取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('order_history');
            }
            //受領
            $ret = $this->TrnReceiving->del(array('id'=>$r[0]['receipt_id']));
            if(!$ret) {
                $this->TrnReceiving->rollback();
                $this->Session->setFlash('直納要求受領取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('order_history');
            }
            //直納フラグONの場合
            if($this->Session->read('Auth.Config.D2Consumes') == '0' ){
                //消費
                $ret = $this->TrnConsume->del(array('id'=>$r[0]['consume_id']));
                if(!$ret) {
                    $this->TrnReceiving->rollback();
                    $this->Session->setFlash('直納要求受領取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('order_history');
                }
                
                //売上
                $ret = $this->TrnClaim->del(array('id'=>$r[0]['sale_id']));
                if(!$ret) {
                    $this->TrnReceiving->rollback();
                    $this->Session->setFlash('直納要求受領取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('order_history');
                }
                
            }else{
                //病院在庫減
                $this->TrnStock->create();
                $ret = $this->TrnStock->updateAll(
                    array( 'TrnStock.stock_count' => 'TrnStock.stock_count - 1 ' ),
                    array( 'TrnStock.id' => $r[0]['stock_id'] ),
                    -1
                    );
                if(!$ret) {
                    $this->TrnReceiving->rollback();
                    $this->Session->setFlash('直納要求受領取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('order_history');
                }
            }
            //シール
            $ret = $this->TrnConsume->del(array('id'=>$r[0]['consume_id']));
            if(!$ret) {
                $this->TrnReceiving->rollback();
                $this->Session->setFlash('直納要求受領取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('order_history');
            }
        }
        //入荷
        $ret = $this->TrnReceiving->del(array('id'=>$this->request->data['d2receipts']['id']));
        if(!$ret) {
            $this->TrnReceiving->rollback();
            $this->Session->setFlash('直納要求受領取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('order_history');
        }
        
        //仕入
        $ret = $this->TrnClaim->del(array('trn_receiving_id'=>$this->request->data['d2receipts']['id']));
        if(!$ret) {
            $this->TrnReceiving->rollback();
            $this->Session->setFlash('直納要求受領取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('order_history');
        }
        
        //業者出荷のステータスを変更
        $this->TrnEdiShipping->create();
        $ret = $this->TrnEdiShipping->updateAll(
            array( 'TrnEdiShipping.shipping_status' => Configure::read('ShippingStatus.ordered')),
            array( 'TrnEdiShipping.trn_receiving_id' => $this->request->data['d2receipts']['id'] ),
            -1
            );
        if(!$ret) {
            $this->TrnReceiving->rollback();
            $this->Session->setFlash('直納要求受領取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('order_history');
        }
        
        $this->TrnReceiving->commit();
        
        
        //表示用データを取得
        $where = ' and a.id in ( ' . join(',',$this->request->data['d2receipts']['id']) . ')';
        
        $sql  = ' select ';
        $sql .= '     a.id              as "d2receipts__id"';
        $sql .= '   , a.work_no         as "d2receipts__work_no"';
        $sql .= "   , to_char(a.work_date,'YYYY/mm/dd') ";
        $sql .= '                       as "d2receipts__work_date"';
        $sql .= '   , i.facility_name   as "d2receipts__facility_name"';
        $sql .= '   , i.facility_code   as "d2receipts__facility_code"';
        $sql .= '   , h.department_name as "d2receipts__department_name" ';
        $sql .= '   , h.department_code as "d2receipts__department_code"';
        $sql .= '   , j.user_name       as "d2receipts__user_name"';
        $sql .= '   , a.recital         as "d2receipts__recital"';
        $sql .= '   , a.quantity        as "d2receipts__quantity"';
        $sql .= '   , d.internal_code   as "d2receipts__internal_code"';
        $sql .= '   , d.item_name       as "d2receipts__item_name"';
        $sql .= '   , d.item_code       as "d2receipts__item_code"';
        $sql .= '   , d.standard        as "d2receipts__standard"';
        $sql .= '   , e.dealer_name     as "d2receipts__dealer_name"';
        $sql .= '   , k.name            as "d2receipts__class_name"';
        $sql .= '   , ( case ';
        $sql .= '       when c.per_unit = 1 then c1.unit_name ';
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '        end )          as "d2receipts__unit_name"';
        $sql .= '   , ( case ';
        $sql .= '       when a.is_deleted = true then false ';
        $sql .= '       else true ';
        $sql .= '       end )           as "d2receipts__canCancel"';
        $sql .= ' from ';
        $sql .= '   trn_receivings as a  ';
        $sql .= '   left join trn_receiving_headers as b  ';
        $sql .= '     on b.id = a.trn_receiving_header_id  ';
        $sql .= '   left join mst_item_units as c  ';
        $sql .= '     on c.id = a.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as c1  ';
        $sql .= '     on c1.id = c.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as c2  ';
        $sql .= '     on c2.id = c.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as d  ';
        $sql .= '     on d.id = c.mst_facility_item_id  ';
        $sql .= '   left join mst_dealers as e  ';
        $sql .= '     on e.id = d.mst_dealer_id  ';
        $sql .= '   left join trn_orders as f  ';
        $sql .= '     on f.id = a.trn_order_id  ';
        $sql .= '   left join trn_edi_receivings as g  ';
        $sql .= '     on g.trn_order_id = f.id  ';
        $sql .= '   left join mst_departments as h  ';
        $sql .= '     on h.id = f.department_id_from  ';
        $sql .= '   left join mst_facilities as i  ';
        $sql .= '     on i.id = h.mst_facility_id  ';
        $sql .= '   left join mst_users as j  ';
        $sql .= '     on j.id = a.creater  ';
        $sql .= '   left join mst_classes as k';
        $sql .= '     on k.id = a.work_type';
        $sql .= ' where ';
        $sql .= '   c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '   and a.receiving_type = ' . Configure::read('ReceivingType.arrival');
        $sql .= '   and g.id is not null  ';
        $sql .= '   and f.order_type = ' . Configure::read('OrderTypes.direct');
        $sql .= $where;
        $sql .= ' order by a.work_no , a.work_seq';
        $result = $this->TrnReceiving->query($sql);

        $this->set('result' , $result);
    }
    
    function getCommonData($data){

        $sql  = ' select ';
        $sql .= '     a.id                       as "common__trn_edi_shipping_id" ';
        $sql .= '   , b.id                       as "common__trn_edi_receiving_id" ';
        $sql .= '   , c.id                       as "common__trn_order_id" ';
        $sql .= '   , a.department_id_from       as "common__supplier_department_id"  ';
        $sql .= '   , a.department_id_to         as "common__hospital_department_id" ';
        $sql .= '   , f.id                       as "common__center_department_id"';
        $sql .= '   , a.quantity                 as "common__quantity" ';
        $sql .= '   , a.mst_item_unit_id         as "common__mst_item_unit_id"';
        $sql .= '   , c.stocking_price           as "common__stocking_price"';
        $sql .= '   , a.lot_no                   as "common__lot_no"';
        $sql .= '   , a.validated_date           as "common__validated_date"';
        $sql .= '   , h.facility_code            as "common__hospital_code"';
        $sql .= '   , e.facility_code            as "common__center_code"';
        
        $sql .= ' from ';
        $sql .= '   trn_edi_shippings as a  ';
        $sql .= '   left join trn_edi_receivings as b  ';
        $sql .= '     on b.id = a.trn_edi_receiving_id  ';
        $sql .= '   left join trn_orders as c  ';
        $sql .= '     on c.id = b.trn_order_id  ';
        $sql .= '   left join mst_item_units as d ';
        $sql .= '     on d.id = a.mst_item_unit_id';
        $sql .= '   left join mst_facilities as e';
        $sql .= '     on e.id = d.mst_facility_id';
        $sql .= '   left join mst_departments as f ';
        $sql .= '     on e.id = f.mst_facility_id';
        $sql .= '   left join mst_departments as g ';
        $sql .= '     on g.id =  a.department_id_to';
        $sql .= '   left join mst_facilities as h';
        $sql .= '     on h.id = g.mst_facility_id';
        
        $sql .= ' where ';
        $sql .= '   a.id in (' . join(',' ,$data['d2receipts']['id']) . ') ';
        $sql .= ' order by a.work_no , a.work_seq ';
        $ret = $this->TrnEdiShipping->query($sql);
        return $ret;
    }

    /**
     * 病院在庫増加
     */
    public function updateTrnStock($sticker_id , $now){
        // シールIDから、在庫IDを取得する。
        $sql = " select b.id , a.mst_item_unit_id , a.mst_department_id from trn_stickers as a left join trn_stocks as b on a.mst_item_unit_id = b.mst_item_unit_id and a.mst_department_id = b.mst_department_id where a.id = " . $sticker_id;
        $ret = $this->TrnSticker->query($sql);
        
        // 在庫数を増加する。
        if(is_null($ret[0][0]['id'])){
            // 在庫レコードがない場合
            $TrnStock = array(
                'TrnStock'=> array(
                    'mst_item_unit_id'  => $ret[0][0]['mst_item_unit_id'],
                    'mst_department_id' => $ret[0][0]['mst_department_id'],
                    'stock_count'       => 1,
                    'reserve_count'     => 0,
                    'promise_count'     => 0,
                    'requested_count'   => 0,
                    'receipted_count'   => 0,
                    'is_deleted'        => false,
                    'creater'           => $this->Session->read('Auth.MstUser.id'),
                    'created'           => $now,
                    'modifier'          => $this->Session->read('Auth.MstUser.id'),
                    'modified'          => $now
                    )
                );

            $this->TrnStock->create();
            // SQL実行
            $this->TrnStock->save($TrnStock);

        }else{
            // 在庫レコードがある場合
            $this->TrnStock->updateAll(array(
                'TrnStock.stock_count' => 'TrnStock.stock_count + 1 ',
                'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                'TrnStock.modified'    => "'" .$now . "'",
                ),array(
                    'TrnStock.id' => $ret[0][0]['id'],
                ),-1);
        }
    }
    
    function getSalesPrice($mst_item_unit_id , $mst_department_id , $work_date){
        $sql  = ' select ';
        $sql .= '     a.sales_price  ';
        $sql .= ' from ';
        $sql .= '   mst_sales_configs as a  ';
        $sql .= '   left join mst_departments as b  ';
        $sql .= '     on b.mst_facility_id = a.partner_facility_id  ';
        $sql .= ' where ';
        $sql .= '   a.mst_item_unit_id = ' . $mst_item_unit_id;
        $sql .= '   and b.id = ' . $mst_department_id;
        $sql .= "   and a.start_date <= '" . $work_date . "'";
        $sql .= "   and a.end_date > '" . $work_date . "'";
        $ret = $this->MstSalesConfig->query($sql);
        if($ret){
            return $ret[0][0]['sales_price'];
        }else{
            return 0;
        }
    }


    function getEdiShipping($where){

        $sql  = ' select ';
        $sql .= '     a.id              as "d2receipts__id"';
        $sql .= '   , f.work_no         as "d2receipts__work_no"';
        $sql .= "   , to_char(f.work_date,'yyyy/mm/dd')";
        $sql .= '                       as "d2receipts__work_date"';
        $sql .= '   , h.facility_name   as "d2receipts__facility_name"';
        $sql .= '   , g.department_name as "d2receipts__department_name"';
        $sql .= '   , e.internal_code   as "d2receipts__internal_code"';
        $sql .= '   , e.item_name       as "d2receipts__item_name"';
        $sql .= '   , e.item_code       as "d2receipts__item_code"';
        $sql .= '   , e.standard        as "d2receipts__standard"';
        $sql .= '   , e1.dealer_name    as "d2receipts__dealer_name"';
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when d.per_unit = 1  ';
        $sql .= '       then d1.unit_name  ';
        $sql .= "       else d1.unit_name || '(' || d.per_unit || d2.unit_name || ')'  ";
        $sql .= '       end ';
        $sql .= '   )                   as "d2receipts__unit_name"  ';
        $sql .= '   , a.lot_no          as "d2receipts__lot_no"';
        $sql .= "   , to_char(a.validated_date,'yyyy/mm/dd')";
        $sql .= '                       as "d2receipts__validated_date"';
        $sql .= '   , i.user_name       as "d2receipts__user_name"';
        $sql .= '   , a.quantity        as "d2receipts__quantity"';
        $sql .= ' from ';
        $sql .= '   trn_edi_shippings as a  ';
        $sql .= '   left join trn_edi_receivings as b  ';
        $sql .= '     on b.id = a.trn_edi_receiving_id  ';
        $sql .= '   left join trn_orders as c  ';
        $sql .= '     on c.id = b.trn_order_id  ';
        $sql .= '   left join mst_item_units as d  ';
        $sql .= '     on d.id = c.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as d1';
        $sql .= '     on d.mst_unit_name_id = d1.id';
        $sql .= '   left join mst_unit_names as d2';
        $sql .= '     on d.per_unit_name_id = d2.id';
        $sql .= '   left join mst_facility_items as e  ';
        $sql .= '     on e.id = d.mst_facility_item_id  ';
        $sql .= '   left join mst_dealers as e1  ';
        $sql .= '     on e1.id = e.mst_dealer_id  ';
        $sql .= '   left join trn_edi_shipping_headers as f  ';
        $sql .= '     on f.id = a.trn_edi_shipping_header_id  ';
        $sql .= '   left join mst_departments as g  ';
        $sql .= '     on g.id = f.department_id_to  ';
        $sql .= '   left join mst_facilities as h  ';
        $sql .= '     on h.id = g.mst_facility_id  ';
        $sql .= '   left join mst_users as i';
        $sql .= '     on i.id = a.modifier';
        $sql .= ' where ';
        $sql .= '   a.is_deleted = false  ';
        $sql .= '   and c.order_type = '. Configure::read('OrderTypes.direct');
        $sql .= '   and h.id = ' . $this->request->data['d2receipts']['facilityId'];
        $sql .= $where;
        $sql .= '  order by f.id , a.id ';
        //全件取得

        $result = $this->TrnEdiShippingHeader->query($sql);

        return $result;
    }
}
