<?php
/**
 * MstAnalyzesController
 * 分析マスタメンテ
 * @since 2011/02/01
 */
class MstAnalyzesController extends AppController {
    var $name = 'MstAnalyzes';

    var $uses = array(
        'MstUser'
       ,'MstAnalyze'
       ,'MstAnalyzeRole'
       ,'MstFacility'
    );

    var $Auth;
    var $Session;
    var $MstRoles;

    var $components = array('RequestHandler');

    public function  beforeFilter() {
        parent::beforeFilter();
    }

    public function index(){
        $this->analyzes_list();
    }

    public function analyzes_list() {
        $this->setRoleFunction(98); //分析SQL編集
        $Analyzes_List = array();
        App::import('Sanitize');
        //施設プルダウン用データ取得
        $this->set('facility_enabled',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.center')),
                       true,
                       array('id' , 'facility_name')
                       )
                   );
        
        // 初回時でない場合のみデータを検索する
        if(isset($this->request->data['MstAnalyze']['is_search'])){
            $sql  = "SELECT A.id            As {MstAnalyze__id} ";
            $sql .= "      ,A.title         As {MstAnalyze__title} ";
            $sql .= "      ,coalesce(B.facility_name,'すべて') ";
            $sql .= "                       As {MstAnalyze__center_name} ";
            $sql .= "      ,A.is_deleted    As {MstAnalyze__is_deleted} ";
            $sql .= "  FROM mst_analyzes A ";
            $sql .= "  LEFT JOIN mst_facilities B ";
            $sql .= "  ON B.id = A.mst_facility_id ";
            $sql .= " WHERE 1 = 1 ";

            // ユーザ入力値による検索条件の作成--------------------------------------------

            // 分析ID(完全一致)
            if ( (isset($this->request->data['MstAnalyze']['id'])) && ($this->request->data['MstAnalyze']['id'] != "") ) {
                $sql .= " AND A.id = '" . Sanitize::escape( $this->request->data['MstAnalyze']['id'] ) . "' ";
            }

            // タイトル(部分一致)
            if ( (isset($this->request->data['MstAnalyze']['title'])) && ($this->request->data['MstAnalyze']['title'] != "") ) {
                $sql .= " AND A.title ILIKE '%" . Sanitize::escape( $this->request->data['MstAnalyze']['title'] ) . "%' ";
            }

            // 削除フラグ
            if( (isset($this->request->data['MstAnalyze']['is_deleted'])) && ($this->request->data['MstAnalyze']['is_deleted'] == 1) ){
            }else{
                $sql .= " AND A.is_deleted = false ";
            }
            
            // センター
            if( (isset($this->request->data['MstAnalyze']['mst_facility_id'])) && ($this->request->data['MstAnalyze']['mst_facility_id'] != '' ) ){
                $sql .= ' AND B.id = ' . $this->request->data['MstAnalyze']['mst_facility_id'];
            }
            
            $sql .= " ORDER BY ";
            $sql .= "       A.id ";
            $sql = str_replace("{","\"",$sql);
            $sql = str_replace("}","\"",$sql);
            
            $this->set('max' , $this->getMaxCount($sql , 'MstAnalyze'));
            $sql .= " LIMIT " . $this->_getLimitCount();

            $Analyzes_List = $this->MstAnalyze->query( $sql );

        }
        $this->set('Analyzes_List',$Analyzes_List);
    }
    
    public function mod() {
        // 新規更新フラグ
        $mode = "mod";

        //更新チェック用にmod画面に入った瞬間の時間を保持
        $this->Session->write('AnalyzeMod.readTime',date('Y-m-d H:i:s'));

        // ラジオボタン項目取得
        $ConditionSelectableRadio = $this->getConditionSelectableRadio();

        // mst_analyzes＆mst_analyze_roles取得
        $id = $this->request->data['MstAnalyze']['id'];
        $AnalyzesData = $this->getAnalyzeData($id);
        $AnalyzesRoleData = $this->getAnalyzeRoleData($id,false);

        // VIEWに設定
        $this->set("mode"                     , $mode);                         // 新規更新フラグ
        $this->set("ConditionSelectableRadio" , $ConditionSelectableRadio);     // ラジオボタン
        $this->set("AnalyzesData"             , $AnalyzesData);                 // mst_analyzes
        $this->set("AnalyzesRoleData"         , $AnalyzesRoleData);             // mst_analyze_roles

        //施設プルダウン用データ取得
        $this->set('facility_enabled',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.center')),
                       true ,
                       array('id' , 'facility_name')
                       )
                   );
        $this->render("edit");
        
    }

    public function add() {
        // 新規更新フラグ
        $mode = "add";

        // ラジオボタン項目取得
        $ConditionSelectableRadio = $this->getConditionSelectableRadio();

        // mst_analyzes＆mst_analyze_roles取得
        $AnalyzesData = $this->getAnalyzeDataInit();                            // 初期値用分析データ取得
        $AnalyzesRoleData = $this->getAnalyzeRoleData(-1,false);                // -1等ありえないidにより初期値取得

        // VIEWに設定
        $this->set('mode'                     , $mode);                         // 新規更新フラグ
        $this->set('ConditionSelectableRadio' , $ConditionSelectableRadio);     // ラジオボタン
        $this->set('AnalyzesData'             , $AnalyzesData);                 // mst_analyzes
        $this->set('AnalyzesRoleData'         , $AnalyzesRoleData);             // mst_analyze_roles
        //施設プルダウン用データ取得
        $this->set('facility_enabled',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.center')),
                       true ,
                       array('id' , 'facility_name')
                       )
                   );
        $this->render("edit");
    }

    public function result(){
        $now = date('Y/m/d H:i:s.u');
        // 新規更新フラグ
        $mode = $this->request->data['mode'];

        // 削除フラグ
        $delete_flag = false;

        // データ編集
        $MstAnalyzeSqlData = mb_ereg_replace("'","''",$this->request->data['MstAnalyze']['sql_data']);

        // 削除フラグ指定
        $MstAnalyzeIsDeleted = "false";
        if ( isset( $this->request->data['MstAnalyze']['is_deleted'] ) ){
            if ( $this->request->data['MstAnalyze']['is_deleted'] == '1' ) {
                $MstAnalyzeIsDeleted = 'true';
                $delete_flag =true;
            }
        }

        // 施主指定
        $MstAnalyzeDonorSelectable = 'false';
        if ( $this->request->data['MstAnalyze']['donor_selectable'] == '1' ) {
            $MstAnalyzeDonorSelectable = 'true';
        }

        // 施設指定
        $MstAnalyzeFacilitySelectable = 'false';
        if ( $this->request->data['MstAnalyze']['facility_selectable'] == '1' ) {
            $MstAnalyzeFacilitySelectable = 'true';
        }

        // 部署指定
        $MstAnalyzeDepartmentSelectable = 'false';
        if ( $this->request->data['MstAnalyze']['department_selectable'] == '1' ) {
            $MstAnalyzeDepartmentSelectable = 'true';
        }

        // 日付指定
        $MstAnalyzeDatesSelectable = 'false';
        if ( $this->request->data['MstAnalyze']['dates_selectable'] == '1' ) {
            $MstAnalyzeDatesSelectable = 'true';
        }
        
        // 日数指定
        $MstAnalyzeDayNumberSelectable = 'false';
        if ( $this->request->data['MstAnalyze']['daynumber_selectable'] == '1' ) {
            $MstAnalyzeDayNumberSelectable = 'true';
        }

        // タイトル行
        $MstAnalyzeTitleSelectable = 'true';
        if ( $this->request->data['MstAnalyze']['title_selectable'] == '0' ) {
            $MstAnalyzeTitleSelectable = 'false';
        }
        
        if(Configure::read('SplitTable.flag') == 1){
            // 分割対応
            $MstAnalyzeSplitTableFlg = 'false';
            if ( $this->request->data['MstAnalyze']['split_table_flg'] == '1' ) {
                $MstAnalyzeSplitTableFlg = 'true';
            }
        }
        
        //センター指定
        $MstAnalyzeMstFacilityId = 'null';
        if ( $this->request->data['MstAnalyze']['mst_facility_id'] != '' ) {
            $MstAnalyzeMstFacilityId = $this->request->data['MstAnalyze']['mst_facility_id'];
        }

        //区切り文字
        $DelimiterType = 0;
        if ( $this->request->data['MstAnalyze']['delimiter_type'] != '' ) {
            $DelimiterType = $this->request->data['MstAnalyze']['delimiter_type'];
        }

        //囲み文字
        $QuoteType = 0;
        if ( $this->request->data['MstAnalyze']['quote_type'] != '' ) {
            $QuoteType = $this->request->data['MstAnalyze']['quote_type'];
        }

        // トランザクション開始
        $this->MstAnalyze->begin();
        //行ロック
        if ( $mode == 'mod' ) {
            $sql = 'select * from mst_analyzes where id = ' . $this->request->data['MstAnalyze']['id'] . ' for update ';
            $this->MstAnalyze->query($sql);
        }
        $error_flag = false;
        try {
            // 排他チェック
            if ( $mode == 'mod' ) {
                $sql  = "SELECT * ";
                $sql .= "  FROM mst_analyzes A ";
                $sql .= " WHERE A.id = " . $this->request->data["MstAnalyze"]["id"];
                $sql .= "   AND A.modified >= '" . $this->Session->read('AnalyzeMod.readTime') . "' ";
                
                $res_modified = $this->MstAnalyze->query( $sql );
                if ( $res_modified === false ) {
                    throw new Exception('分析マスタ更新時間取得エラー');
                }

                if ( count( $res_modified ) > 0 ) {
                    throw new Exception('処理中のデータに更新がかけられました。最初からやり直してください。');
                }
            }

            if ( $mode == 'add' ) {
                // 新規idを取得 他のテーブルも更新するのに必要
                $this->request->data['MstAnalyze']['id'] = $this->getAnalyzeNewId();

                // mst_analyzes追加
                $sql  = "INSERT INTO mst_analyzes ( ";
                $sql .= "     id ";
                $sql .= "    ,title ";
                $sql .= "    ,sql_data ";
                $sql .= "    ,is_deleted ";
                $sql .= "    ,donor_selectable ";
                $sql .= "    ,facility_selectable ";
                $sql .= "    ,department_selectable ";
                $sql .= "    ,dates_selectable ";
                $sql .= "    ,daynumber_selectable ";
                $sql .= "    ,title_selectable ";
                if(Configure::read('SplitTable.flag') == 1){
                    $sql .= "    ,split_table_flg ";
                }
                $sql .= "    ,mst_facility_id ";
                $sql .= "    ,delimiter_type ";
                $sql .= "    ,quote_type ";
                $sql .= "    ,creater ";
                $sql .= "    ,created ";
                $sql .= "    ,modifier ";
                $sql .= "    ,modified ";
                $sql .= ") VALUES ( ";
                $sql .= "   " . $this->request->data["MstAnalyze"]["id"] . " ";
                $sql .= " ,'" . $this->request->data["MstAnalyze"]["title"] . "' ";
                $sql .= " ,'" . $MstAnalyzeSqlData . "' ";
                $sql .= " , " . $MstAnalyzeIsDeleted . " ";
                $sql .= " , " . $MstAnalyzeDonorSelectable . " ";
                $sql .= " , " . $MstAnalyzeFacilitySelectable . " ";
                $sql .= " , " . $MstAnalyzeDepartmentSelectable . " ";
                $sql .= " , " . $MstAnalyzeDatesSelectable . " ";
                $sql .= " , " . $MstAnalyzeDayNumberSelectable . " ";
                $sql .= " , " . $MstAnalyzeTitleSelectable . " ";
                $sql .= " , " . $MstAnalyzeSplitTableFlg . " ";
                $sql .= " , " . $MstAnalyzeMstFacilityId . " ";
                $sql .= " , " . $DelimiterType . " ";
                $sql .= " , " . $QuoteType . " ";
                $sql .= " , " . $this->Session->read('Auth.MstUser.id') . " ";
                $sql .= " , '" . $now . "'";
                $sql .= " , " . $this->Session->read('Auth.MstUser.id') . " ";
                $sql .= " , '" . $now . "'";
                $sql .= ") ";
            }else{
                // mst_analyzes更新
                $sql  = "UPDATE mst_analyzes ";
                $sql .= "   SET title                  = '" . $this->request->data["MstAnalyze"]["title"] . "' ";
                $sql .= "      ,sql_data               = '" . $MstAnalyzeSqlData . "' ";
                $sql .= "      ,is_deleted             =  " . $MstAnalyzeIsDeleted;
                $sql .= "      ,donor_selectable       =  " . $MstAnalyzeDonorSelectable;
                $sql .= "      ,facility_selectable    =  " . $MstAnalyzeFacilitySelectable;
                $sql .= "      ,department_selectable  =  " . $MstAnalyzeDepartmentSelectable;
                $sql .= "      ,dates_selectable       =  " . $MstAnalyzeDatesSelectable;
                $sql .= "      ,daynumber_selectable   =  " . $MstAnalyzeDayNumberSelectable;
                $sql .= "      ,title_selectable       =  " . $MstAnalyzeTitleSelectable;
                if(Configure::read('SplitTable.flag') == 1){
                    $sql .= "      ,split_table_flg        =  " . $MstAnalyzeSplitTableFlg;
                }
                $sql .= "      ,mst_facility_id        =  " . $MstAnalyzeMstFacilityId;
                $sql .= "      ,delimiter_type         =  " . $DelimiterType;
                $sql .= "      ,quote_type             =  " . $QuoteType;
                $sql .= "      ,modifier               =  " . $this->Session->read('Auth.MstUser.id');
                $sql .= "      ,modified               = '" . $now . "' " ;
                $sql .= " WHERE id = " . $this->request->data["MstAnalyze"]["id"];

            }

            $res1 = $this->MstAnalyze->query( $sql ,false ,false );
            if ( $res1 === false ) {
                throw new Exception('分析マスタ更新エラー');
            }

            // mst_analyze_roles更新
            // 全て一度削除更新
            $sql  = "UPDATE mst_analyze_roles ";
            $sql .= "   SET is_deleted = true ";
            $sql .= "      ,modifier   = " . $this->Session->read('Auth.MstUser.id');
            $sql .= "      ,modified   = now() ";
            $sql .= " WHERE mst_analyze_id = " . $this->request->data["MstAnalyze"]["id"] . " ";

            $res2 = $this->MstAnalyze->query( $sql );
            if ( $res2 === false ) {
                throw new Exception('分析ロールマスタ削除エラー');
            }

            if ( isset($this->request->data['MstAnalyzeRole']['mst_role_id']) ) {
                foreach($this->request->data['MstAnalyzeRole']['mst_role_id'] As $MstRoleId){

                    // mst_analyze_rolesに存在しているかどうか
                    $MstAnalyzeRoleExist= $this->getAnalyzeRoleExist( $this->request->data['MstAnalyze']['id'],$MstRoleId );

                    if ( $MstAnalyzeRoleExist ) {
                        $sql  = "UPDATE mst_analyze_roles ";
                        $sql .= "   SET is_deleted = false ";
                        $sql .= "      ,modifier   = " . $this->Session->read('Auth.MstUser.id');
                        $sql .= "      ,modified   = now() ";
                        $sql .= " WHERE mst_analyze_id = " . $this->request->data['MstAnalyze']['id'];
                        $sql .= "   AND mst_role_id    = " . $MstRoleId;
                    }else{
                        $sql  = "INSERT INTO mst_analyze_roles(";
                        $sql .= "     mst_analyze_id ";
                        $sql .= "    ,mst_role_id ";
                        $sql .= "    ,is_deleted ";
                        $sql .= "    ,creater ";
                        $sql .= "    ,created ";
                        $sql .= "    ,modifier ";
                        $sql .= "    ,modified ";
                        $sql .= ") VALUES ( ";
                        $sql .= "   " . $this->request->data['MstAnalyze']['id'] . " ";
                        $sql .= " , " . $MstRoleId . " ";
                        $sql .= " , false ";
                        $sql .= " , " . $this->Session->read('Auth.MstUser.id') . " ";
                        $sql .= " , now() ";
                        $sql .= " , " . $this->Session->read('Auth.MstUser.id') . " ";
                        $sql .= " , now() ";
                        $sql .= ") ";
                    }

                    $res3 = $this->MstAnalyze->query( $sql );
                    if ( $res3 === false ) {
                        throw new Exception('分析ロールマスタ更新エラー');
                    }
                }
            }

        }catch (Exception $e) {
            $error_flag = true;
            $error_msg = $e->getMessage();
        }

        if ( $error_flag ) {
            $this->MstAnalyze->rollback();
            $_result_message = $error_msg."　分析マスタ更新に失敗しました";
            $this->Session->setFlash($_result_message, 'growl', array('type'=>'error') );
        }else{
            $this->MstAnalyze->commit();
            $this->Session->setFlash('分析マスタを更新しました', 'growl', array('type'=>'star'));

            $_result_message = "以下の内容で設定しました";
            if ( $delete_flag ) {
                $_result_message = "データを削除しました";
            }else{
                // 結果データ取得
                $AnalyzesData = $this->getAnalyzeData($this->request->data['MstAnalyze']['id']);
                $AnalyzesRoleData = $this->getAnalyzeRoleData($this->request->data['MstAnalyze']['id'],true);

                $this->set("AnalyzesData", $AnalyzesData);
                $this->set("AnalyzesRoleData",$AnalyzesRoleData);
            }
        }

        $this->set('ErrorFlag'               , $error_flag);       // エラーフラグ
        $this->set('DeleteFlag'              , $delete_flag);      // 削除フラグ
        $this->set('ResultMessage'           , $_result_message);  // 登録メッセージ
    }

    // mst_analyze_roles存在チェック
    private function getAnalyzeRoleExist($mst_analyze_id
                                        ,$mst_role_id
                                        ){
        $sql  = "SELECT * ";
        $sql .= "  FROM mst_analyze_roles A ";
        $sql .= " WHERE A.mst_analyze_id =  " . $mst_analyze_id . " ";
        $sql .= "   AND A.mst_role_id    =  " . $mst_role_id . " ";

        $res2 = $this->MstAnalyze->query( $sql );
        if ( count($res2) > 0 ) {
            return true;
        }else{
            return false;
        }
    }

    // mst_analyzes.id新規採番
    private function getAnalyzeNewId() {
        $sql  = "";
        $sql .= "SELECT nextval('mst_analyzes_id_seq'::regclass) As id ";
        
        $res_id = $this->MstAnalyze->query( $sql );
        return $res_id[0][0]["id"];
    }

    // mst_analyzes初期値
    private function getAnalyzeDataInit() {
        $data = array();
        $data[0]['MstAnalyze']['id']                    = "";
        $data[0]['MstAnalyze']['title']                 = "";
        $data[0]['MstAnalyze']['sql_data']              = "";
        $data[0]['MstAnalyze']['is_deleted']            = "";
        $data[0]['MstAnalyze']['donor_selectable']      = "";
        $data[0]['MstAnalyze']['facility_selectable']   = "";
        $data[0]['MstAnalyze']['department_selectable'] = "";
        $data[0]['MstAnalyze']['dates_selectable']      = "";
        $data[0]['MstAnalyze']['daynumber_selectable']  = "";
        $data[0]['MstAnalyze']['title_selectable']      = true;
        if(Configure::read('SplitTable.flag') == 1){
            $data[0]['MstAnalyze']['split_table_flg']   = "";
        }
        $data[0]['MstAnalyze']['delimiter_type']        = "";
        $data[0]['MstAnalyze']['quote_type']            = "";
        return $data;
    }

    // mst_analyzes取得
    private function getAnalyzeData($id) {
        $sql  = "SELECT A.id                     As {MstAnalyze__id} ";
        $sql .= "      ,A.title                  As {MstAnalyze__title} ";
        $sql .= "      ,A.sql_data               As {MstAnalyze__sql_data} ";
        $sql .= "      ,A.is_deleted             As {MstAnalyze__is_deleted} ";
        $sql .= "      ,A.donor_selectable       As {MstAnalyze__donor_selectable} ";
        $sql .= "      ,A.facility_selectable    As {MstAnalyze__facility_selectable} ";
        $sql .= "      ,A.department_selectable  As {MstAnalyze__department_selectable} ";
        $sql .= "      ,A.dates_selectable       As {MstAnalyze__dates_selectable} ";
        $sql .= "      ,A.daynumber_selectable   As {MstAnalyze__daynumber_selectable} ";
        $sql .= "      ,A.title_selectable       As {MstAnalyze__title_selectable}";
        $sql .= "      ,A.mst_facility_id        As {MstAnalyze__mst_facility_id} ";
        $sql .= "      ,A.delimiter_type         As {MstAnalyze__delimiter_type} ";
        $sql .= "      ,( case ";
        foreach(Configure::read('AnalyzeDelimiterType.typelist') as $k => $v){
            $sql .= "  when A.delimiter_type = {$k} then '{$v}'" ;
        }
        $sql .= "       end )                    As {MstAnalyze__delimiter_type_name}";
        $sql .= "      ,A.quote_type             As {MstAnalyze__quote_type} ";
        $sql .= "      ,( case ";
        foreach(Configure::read('AnalyzeQuoteType.typelist') as $k => $v){
            $sql .= "  when A.delimiter_type = {$k} then '{$v}'" ;
        }
        $sql .= "       end )                    As {MstAnalyze__quote_type_name}";

        if(Configure::read('SplitTable.flag') == 1){
            $sql .= "      ,A.split_table_flg        As {MstAnalyze__split_table_flg} ";
        }
        $sql .= "      ,coalesce(B.facility_name,'すべて') ";
        $sql .= "                                As {MstAnalyze__facility_name} ";
        $sql .= "  FROM mst_analyzes A ";
        $sql .= "  LEFT JOIN mst_facilities B ";
        $sql .= "  ON B.id = A.mst_facility_id";
        $sql .= " WHERE A.id = " . $id;

        $sql = str_replace("{","\"",$sql);
        $sql = str_replace("}","\"",$sql);

        $res1 = $this->MstAnalyze->query( $sql );

        return $res1;
    }

    // mst_analyze_roles取得
    private function getAnalyzeRoleData($id,$ResultFlag = false) {
        $sql  = "";
        $sql .= "SELECT A.id                              As {MstRole__id} ";
        $sql .= "      ,A.role_name                       As {MstRole__role_name} ";
        $sql .= "      ,CASE ";
        $sql .= "           WHEN B.id IS NULL THEN '0' ";
        $sql .= "           ELSE '1' ";
        $sql .= "       END                               As {MstAnalyzeRole__checked} ";
        $sql .= "  FROM mst_roles A ";
        $sql .= "            LEFT JOIN mst_analyze_roles B ";
        $sql .= "                   ON A.id = B.mst_role_id ";
        $sql .= "                  AND B.mst_analyze_id = " . $id;
        $sql .= "                  AND B.is_deleted     = false ";
        $sql .= " WHERE A.is_deleted = false ";
        // 結果表示用
        if ( $ResultFlag ) {
            // INNER JOIN(強制)
            $sql .= " AND B.mst_analyze_id = " . $id;
        }
        $sql .= " ORDER BY ";
        $sql .= "       A.id ";

        $sql = str_replace("{","\"",$sql);
        $sql = str_replace("}","\"",$sql);

        $res1 = $this->MstAnalyze->query( $sql );
        
        return $res1;

    }

    // ラジオボタンの項目
    private function getConditionSelectableRadio() {
        $condition_selectable = array( '1' => '有り'
                                      ,'0' => '無し'
                                );
        return $condition_selectable;
    }
}

?>
