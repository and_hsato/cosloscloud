<?php
/**
 * MyPageController
 *
 * @version 1.0.0
 * @since 2016/01/21
 */


class MyPageController extends AppController {
    
    var $name = 'MyPage';
    var $uses = array('MstUser', 'MstDepartment', 'MstFacilityOption',);
    
    public $components = array('Cookie');
    
    public function index() {
        $this->redirect('profile');
    }
    
    public function profile() {
        // セッション情報からMstUser、MstDepartmentを取得
        $department = $this->Session->read('Auth.MstDepartment');
        $mstUser = $this->Session->read('Auth.MstUser');
        
        // View用にデータをセット
        $this->request->data['MstUser'] = $mstUser;
        $this->request->data['MstDepartment'] = $department;
        $department_list = $this->getHospitalDepartmentList($mstUser['mst_facility_id']);
        $this->set('is_lite_user' , $this->AuthSession->isLiteUserRole());
        $this->set('department_list' , $department_list);
    }

    public function update() {
        if (isset($this->request->data['MstUser']) && isset($this->request->data['MstDepartment'])) {
            $mstUser = $this->request->data['MstUser'];
            $department = $this->request->data['MstDepartment'];
            
            // リクエストされたdepartment_codeからdepartment_idを取得
            $department_id = $this->getHospitalDepartmentId($department['department_code'], $mstUser['mst_facility_id']);
            $mstUser['mst_department_id'] = $department_id;
            
            // トランザクションを開始
            $this->MstUser->begin();
            if(!$this->MstUser->save($mstUser)){
                // ロールバック
                $this->MstUser->rollback();
                // エラーメッセージ
                $this->Session->setFlash('マイページ情報の更新に失敗しました。', 'growl', array('type'=>'error') );
            } else {
                // トランザクションを終了
                $this->MstUser->commit();
                $this->Session->setFlash('マイページ情報を更新しました。', 'growl', array('type'=>'star') );
                
                // 更新後はセッション内のプロファイル情報も更新する
                $this->AuthSession->updateProfileOfLoginUser($mstUser['id']);
            }
        }
        $this->redirect('profile');
    }
}
