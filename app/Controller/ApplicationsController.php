<?php
/**
 * ApplicationsController
 *　マスタ申請
 * @version 1.0.0
 * @since 2013/12/04
 */
class ApplicationsController extends AppController {
    
    /**
     * @var $name
     */
    var $name = 'Applications';
    
    /**
     * @var array $uses
     */
    var $uses = array('MstGrandmaster',
                      'MstInsuranceClaimDepartment',
                      'MstClassSeparation',
                      'MstFacility',
                      'MstUnitName',
                      'MstAccountlist',
                      'MstDealer',
                      'TrnApplication');
    
    /**
     * @var bool $scaffold
     */
    //var $scaffold;
    
    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');
    
    /**
     * @var array $components
     */
    var $components = array('RequestHandler');
    
    /**
     *
     */
    function index(){
        $this->redirect('application');
    }

    /**
     * マスタ申請登録
     */
    function application(){
        $this->set('dealer_list' , $this->getDelaers());
    }

    /**
     * マスタ申請確認
     */
    function application_confirm(){
        
    }

    /**
     * マスタ申請結果
     */
    function application_result(){
        $now = date('Y/m/d H:i:s');
        $this->TrnApplication->begin();
        
        $work_no = $this->setWorkNo4Header( date('Ymd') , "32");
        // 見積依頼ヘッダ
        $TrnApplication = array(
            'TrnApplication'=> array(
                'work_no'       => $work_no,
                'work_date'     => date('Y/m/d'),
                'recital'       => $this->request->data['Application']['recital'],
                'item_name'     => $this->request->data['Application']['item_name'] ,
                'standard'      => $this->request->data['Application']['standard'] ,
                'item_code'     => $this->request->data['Application']['item_code'] ,
                'jan_code'      => $this->request->data['Application']['jan_code'] ,
                'mst_dealer_id' => $this->request->data['Application']['mst_dealer_id'] ,
                'status'        => Configure::read('ApplicationStatus.Pending') ,
                'master_id'     => null , 
                'is_deleted'    => false,
                'creater'       => $this->Session->read('Auth.MstUser.id'),
                'created'       => $now,
                'modifier'      => $this->Session->read('Auth.MstUser.id'),
                'modified'      => $now
                )
            );
        
        $this->TrnApplication->create();
        // SQL実行
        if (!$this->TrnApplication->save($TrnApplication)) {
            $this->TrnApplication->rollback();
            $this->Session->setFlash('マスタ申請登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        
        $this->TrnApplication->commit();
        
    }

    /**
     * 申請状況照会
     */
    function application_history(){
        $result = array();
        if(isset($this->request->data['Application']['is_search'])){
            $where = "";
            
            // 絞込条件追加
            // 申請番号
            if($this->request->data['Application']['work_no'] != "" ){
                $where .= " and a.work_no = '" .$this->request->data['Application']['work_no']. "'";
            }
            // 申請日From
            if($this->request->data['Application']['work_date_from'] != "" ){
                $where .= " and a.work_date >= '" .$this->request->data['Application']['work_date_from']. "'";
            }
            // 申請日To
            if($this->request->data['Application']['work_date_to'] != "" ){
                $where .= " and a.work_date <= '" .$this->request->data['Application']['work_date_to']. "'";
            }
            // 商品名
            if($this->request->data['Application']['item_name'] != "" ){
                $where .= " and a.item_name like '%" .$this->request->data['Application']['item_name']. "%'";
            }
            // 規格
            if($this->request->data['Application']['standard'] != "" ){
                $where .= " and a.standard like '%" .$this->request->data['Application']['standard']. "%'";
            }
            // 製品番号
            if($this->request->data['Application']['item_code'] != "" ){
                $where .= " and a.item_code like'%" .$this->request->data['Application']['item_code']. "'%";
            }
            // 販売元
            if($this->request->data['Application']['mst_dealer_id'] != "" ){
                $where .= " and a.mst_dealer_id = '" .$this->request->data['Application']['mst_dealer_id']. "'";
            }
            // JANコード
            if($this->request->data['Application']['jan_code'] != "" ){
                $where .= " and a.jan_code = '" .$this->request->data['Application']['jan_code']. "'";
            }
            // 削除済みも表示
            if($this->request->data['Application']['is_deleted'] != "1" ){
                $where .= ' and a.is_deleted = false ';
            }

            // 自分が申請したもののみ表示させる
            $where .= ' and a.creater = ' . $this->Session->read('Auth.MstUser.id');
            
            $result = $this->getApplicationList($where , $this->request->data['limit']);
        }
        $this->set('dealer_list' , $this->getDelaers());
        $this->set('result' , $result);
    }
    
    /**
     * 申請明細
     */
    function application_history_detail(){

        $where = " and c.id in (" . join(',',$this->request->data['Application']['id']) . ")";
        $result = $this->getGrandmaster($where);
        if(count($result) > 0){
            $this->request->data['MstGrandmaster'] = $result[0]['Grand'];
        }
        $where = " and a.id in ( " . join(',',$this->request->data['Application']['id']) . ")";
        $app = $this->getApplicationList($where);
        $this->request->data['Application'] = $app[0]['Application'];
        
        $this->_createPullDown();
    }

    /**
     * 申請取り消し
     */
    function application_history_result(){
        $now = date('Y/m/d H:i:s');
        $this->TrnApplication->begin();
        // 行ロック
        $this->TrnApplication->query(" select * from trn_applications as a where a.id in (" . join(',', $this->request->data['Application']['id']) . ") for update " );
        $this->TrnApplication->create();
        $ret = $this->TrnApplication->updateAll(
            array(
                'TrnApplication.is_deleted' => "'true'",
                'TrnApplication.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnApplication.modified'   => "'" . $now . "'"
                ),
            array(
                'TrnApplication.id'  => $this->request->data['Application']['id'] ,
                ),
            -1
            );
        if(!$ret){
            $this->TrnApplication->rollback();
            $this->Session->setFlash('申請取り消し処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        
        $this->TrnApplication->commit();
        $where = " and a.id in ( " . join(',',$this->request->data['Application']['id']) . ")";
        $result = $this->getApplicationList($where);
        $this->set('result' , $result);
    }

    /**
     * 申請検索
     */
    function application_search(){
        $result = array();
        if(isset($this->request->data['Application']['is_search'])){
            $where = "";
            
            // 絞込条件追加
            // 申請番号
            if($this->request->data['Application']['work_no'] != "" ){
                $where .= " and a.work_no = '" .$this->request->data['Application']['work_no']. "'";
            }
            // 申請日From
            if($this->request->data['Application']['work_date_from'] != "" ){
                $where .= " and a.work_date >= '" .$this->request->data['Application']['work_date_from']. "'";
            }
            // 申請日To
            if($this->request->data['Application']['work_date_to'] != "" ){
                $where .= " and a.work_date <= '" .$this->request->data['Application']['work_date_to']. "'";
            }
            // 商品名
            if($this->request->data['Application']['item_name'] != "" ){
                $where .= " and a.item_name like '%" .$this->request->data['Application']['item_name']. "%'";
            }
            // 規格
            if($this->request->data['Application']['standard'] != "" ){
                $where .= " and a.standard like '%" .$this->request->data['Application']['standard']. "%'";
            }
            // 製品番号
            if($this->request->data['Application']['item_code'] != "" ){
                $where .= " and a.item_code like'%" .$this->request->data['Application']['item_code']. "'%";
            }
            // 販売元
            if($this->request->data['Application']['mst_dealer_id'] != "" ){
                $where .= " and a.mst_dealer_id = '" .$this->request->data['Application']['mst_dealer_id']. "'";
            }
            // JANコード
            if($this->request->data['Application']['jan_code'] != "" ){
                $where .= " and a.jan_code = '" .$this->request->data['Application']['jan_code']. "'";
            }
            
            $where .= ' and a.is_deleted = false ';
            
            $result = $this->getApplicationList($where , $this->request->data['limit']);
        }
        $this->set('dealer_list' , $this->getDelaers());
        $this->set('status_list' , Configure::read('ApplicationStatusList'));
        $this->set('result' , $result);
        
    }

    /**
     * 申請新規登録確認
     */
    function application_edit_confirm(){
        $where = " and a.id in ( " . join(',',$this->request->data['Application']['id']) . ")";
        $ret = $this->getApplicationList($where);
        $this->request->data['Application'] = $ret[0]['Application'];
        $this->_createPullDown();
        
        $this->request->data['MstGrandmaster']['item_code'] = $this->request->data['Application']['item_code'];
        $this->request->data['MstGrandmaster']['item_name'] = $this->request->data['Application']['item_name'];
        $this->request->data['MstGrandmaster']['jan_code']  = $this->request->data['Application']['jan_code'];
        $this->request->data['MstGrandmaster']['standard']  = $this->request->data['Application']['standard'];
        $this->request->data['MstGrandmaster']['recital']   = $this->request->data['Application']['recital'];
    }

    /**
     * 申請新規登録結果
     */
    function application_edit_result(){
        $now = date('Y/m/d H:i:s');
        // 行ロック
        $this->TrnApplication->query(" select * from trn_applications as a where a.id = '" . $this->request->data['Application']['id'] . "' for update " );
        $this->TrnApplication->begin();
        // グランドマスタに新規追加
        $MstGrandmaster['MstGrandmaster'] = $this->request->data['MstGrandmaster'];
        $MstGrandmaster['MstGrandmaster']['dealer_code'] = '';
        $this->MstGrandmaster->create();
        // SQL実行
        if (!$this->MstGrandmaster->save($MstGrandmaster , false)) {
            $this->TrnApplication->rollback();
            $this->Session->setFlash('マスタ新規登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('application_search');
        }
        
        // 申請情報を更新
        $this->TrnApplication->create();
        $ret = $this->TrnApplication->updateAll(
            array(
                'TrnApplication.status'    => Configure::read('ApplicationStatus.Complete') ,
                'TrnApplication.master_id' => "'" . $this->request->data['MstGrandmaster']['master_id'] . "'",
                'TrnApplication.modifier'  => $this->Session->read('Auth.MstUser.id'),
                'TrnApplication.modified'  => "'" . $now . "'"
                ),
            array(
                'TrnApplication.id'  => $this->request->data['Application']['id'] ,
                ),
            -1
            );
        if(!$ret){
            $this->TrnApplication->rollback();
            $this->Session->setFlash('マスタ新規登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('application_search');
        }
        $this->TrnApplication->commit();
        $this->_createPullDown();
    }
    /**
     * 申請紐づけ
     */
    function application_update(){
        $result = array();
        if(isset($this->request->data['Grand']['is_search'])){
            /* 絞り込み条件追加 */
            $where = '';
            //マスタID
            if($this->request->data['Grand']['master_id'] != ""){
                $where .= " and a.master_id = '" . $this->request->data['Grand']['master_id'] . "'";
            }
            //商品名
            if($this->request->data['Grand']['item_name'] != ""){
                $where .= " and a.item_name like '%" . $this->request->data['Grand']['item_name'] . "%'";
            }
            //規格
            if($this->request->data['Grand']['standard'] != ""){
                $where .= " and a.standard_name like '%" . $this->request->data['Grand']['standard'] . "%'";
            }
            //製品番号
            if($this->request->data['Grand']['item_code'] != ""){
                $where .= " and a.item_code like '%" . $this->request->data['Grand']['item_code'] . "%'";
            }
            //JANコード
            if($this->request->data['Grand']['jan_code'] != ""){
                $where .= " and a.jan_code = '" . $this->request->data['Grand']['jan_code'] . "'";
            }
            
            $result = $this->getGrandMaster($where ,$this->request->data['limit'] );
        }else{
            // 初期表示
            $where = " and a.id in ( " . join(',',$this->request->data['Application']['id']) . ")";
            $item = $this->getApplicationList($where);
            
            $this->request->data['Application'] = $item[0]['Application'];
        }
        $this->set('dealer_list' ,$this->getDelaers());
        $this->set('result' , $result);
    }

    /**
     * 申請紐づけ結果
     */
    function application_update_result(){
        $now = date('Y/m/d H:i:s');
        $this->TrnApplication->begin();
        // 行ロック
        $this->TrnApplication->query(" select * from trn_applications as a where a.id = " . $this->request->data['Application']['id'] . " for update " );
        $this->TrnApplication->create();
        $ret = $this->TrnApplication->updateAll(
            array(
                'TrnApplication.status'    => Configure::read('ApplicationStatus.Complete') ,
                'TrnApplication.master_id' => "'" .$this->request->data['Application']['master_id'] . "'",
                'TrnApplication.modifier'  => $this->Session->read('Auth.MstUser.id'),
                'TrnApplication.modified'  => "'" . $now . "'"
                ),
            array(
                'TrnApplication.id'  => $this->request->data['Application']['id'] ,
                ),
            -1
            );
        if(!$ret){
            $this->TrnApplication->rollback();
            $this->Session->setFlash('申請ステータス更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        
        $this->TrnApplication->commit();
        
        $where = " and a.master_id = '" . $this->request->data['Application']['master_id'] . "'";
        $result = $this->getGrandmaster($where);
        $this->set('result' , $result);
    }

    /**
     * 申請ステータス変更
     */
    function application_status_confirm(){
        $where = " and a.id in ( " . join(',',$this->request->data['Application']['id']) . ") ";
        $result = $this->getApplicationList($where);
        $this->set('status_list' , Configure::read('ApplicationStatusList'));
        $this->set('result' , $result); 
    }

    /**
     * 申請ステータス変更結果
     */
    function application_status_result(){
        $now = date('Y/m/d H:i:s');
        $this->TrnApplication->begin();
        // 行ロック
        $this->TrnApplication->query(" select * from trn_applications as a where a.id in (" . join(',', $this->request->data['Application']['id']) . ") for update " );
        foreach($this->request->data['Application']['id'] as $id){
        
            $this->TrnApplication->create();
            $ret = $this->TrnApplication->updateAll(
                array(
                    'TrnApplication.status'   => $this->request->data['Application']['status'][$id],
                    'TrnApplication.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnApplication.modified' => "'" . $now . "'"
                    ),
                array(
                    'TrnApplication.id'  => $id ,
                    ),
                -1
                );
            if(!$ret){
                $this->TrnApplication->rollback();
                $this->Session->setFlash('申請ステータス更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
        }
        $this->TrnApplication->commit();
        $where = " and a.id in ( " . join(',',$this->request->data['Application']['id']) . ") ";
        $result = $this->getApplicationList($where);
        $this->set('result' , $result); 
    }

    function getDelaers(){
        $sql  = ' select ';
        $sql .= '     id          as "MstDealer__id" ';
        $sql .= '   , dealer_name as "MstDealer__dealer_name" ';
        $sql .= ' from ';
        $sql .= '     mst_dealers as a ';
        $sql .= ' where ';
        $sql .= '     a.is_deleted = false ';
        $sql .= '     and ( (  ';
        $sql .= '      dealer_type = ' . Configure::read('DealerTypes.donor');
        $sql .= '      and length(dealer_code) >= 6  ';
        $sql .= "      and dealer_code not like 'ZZZ%' ";
        $sql .= "      and dealer_code not like 'KON%' ";
        $sql .= '    )  ';
        $sql .= '    or dealer_type = ' . Configure::read('DealerTypes.cooperation') . ') ';
        $sql .= ' order by ';
        $sql .= '     a.dealer_code ';
        
        $dealer_list = Set::Combine(
            $this->MstDealer->query($sql),
            "{n}.MstDealer.id",
            "{n}.MstDealer.dealer_name"
            );
        return $dealer_list;
    }


    function getApplicationList($where , $limit=null){
        $sql  = ' select ';
        $sql .= '     a.id           as "Application__id"';
        $sql .= '   , a.work_no      as "Application__work_no"';
        $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') ";
        $sql .= '                    as "Application__work_date"';
        $sql .= '   , a.recital      as "Application__recital"';
        $sql .= '   , a.item_name    as "Application__item_name"';
        $sql .= '   , a.standard     as "Application__standard"';
        $sql .= '   , a.item_code    as "Application__item_code"';
        $sql .= '   , a.jan_code     as "Application__jan_code"';
        $sql .= '   , b.dealer_name  as "Application__dealer_name"';
        $sql .= '   , ( case a.status ';
        foreach (Configure::read('ApplicationStatusList') as $k=>$v ){
            $sql .= "           when " . $k . " then '" . $v ."' ";
        }
        $sql .= "     else '' ";
        $sql .= '     end )          as "Application__status_name" ';
        $sql .= '   , a.status       as "Application__status" ';
        $sql .= ' from ';
        $sql .= '   trn_applications as a  ';
        $sql .= '   left join mst_dealers as b  ';
        $sql .= '     on b.id = a.mst_dealer_id  ';
        $sql .= ' where 1=1';
        $sql .= $where;
        $sql .= " order by a.work_no ";
        if(!is_null($limit)){
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnApplication' , false ));
            $sql .= ' limit ' . $limit;
        }
        
        $result = $this->TrnApplication->query($sql);
        return $result;
    }

    function getGrandMaster($where , $limit=null){
        $sql  = ' select ';
        $sql .= '     a.master_id     as "Grand__master_id"';
        $sql .= '   , b.dealer_name   as "Grand__dealer_name"';
        $sql .= '   , a.item_name     as "Grand__item_name"';
        $sql .= '   , a.standard_name as "Grand__standard"';
        $sql .= '   , a.item_code     as "Grand__item_code"';
        $sql .= '   , a.jan_code      as "Grand__jan_code"';
        $sql .= '   , a.per_unit      as "Grand__per_unit"';
        $sql .= '   , a.unit_name     as "Grand__unit_name"';
        $sql .= '   , a.unit_price    as "Grand__unit_price"';
        $sql .= '   , a.price         as "Grand__price"';
        $sql .= '   , a.refund_price  as "Grand__refund_price"';
        $sql .= '   , a.spare_key1    as "Grand__spare_key1"';
        $sql .= '   , a.spare_key2    as "Grand__spare_key2"';
        $sql .= '   , a.spare_key3    as "Grand__spare_key3"';
        $sql .= '   , a.spare_key4    as "Grand__spare_key4"';
        $sql .= ' from ';
        $sql .= '   mst_grandmasters as a  ';
        $sql .= '   left join mst_dealers as b  ';
        $sql .= '     on b.dealer_code = a.dealer_code ';
        $sql .= '   left join trn_applications as c ';
        $sql .= '     on c.master_id = a.master_id';
        $sql .= ' where 1=1';
        $sql .= $where;
        if(!is_null($limit)){
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstGrandmaster' , false ));
            $sql .= ' limit ' . $limit;
        }
        $result = $this->MstGrandmaster->query($sql);
        return $result;
    }

}
