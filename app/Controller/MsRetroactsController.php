<?php
/**
 * 遡及 Controller
 * @version 1.0.0
 * @since 2010/09/06
 */
App::import('Sanitize');
class MsRetroactsController extends AppController {

    /**
     *
     * @var string $name
     */
    var $name = 'MsRetroacts';

    /**
     *
     * @var array $uses
     */
    var $uses = array(
        'MstFacility',
        'MstItemUnit',
        'MstUser',
        'MstUserBelonging',
        'Supplier',
        'Hospital',
        'Center',
        'Owner',
        'MstClass',
        'TrnClaim',
        'TrnRetroactHeader',
        'TrnRetroactRecord',
        'MstDepartment',
        'MstDealer',
        'MstTransactionConfig',
        'MstSalesConfig',
        'MstFacilityItem',
        'TrnSticker',
        'MstFacilityRelation',
        'TrnCloseHeader',
        'TrnReceiving',
        'TrnConsume'
        );


    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','RetroactUtils','CsvWriteUtils','CsvReadUtils');

    /**
     *
     * @var array errors
     */
    var $errors;

    /**
     *
     * @var integer
     */
    var $RETROACT_TYPE_STOCK   = 1;
    var $RETROACT_TYPE_SALES   = 2;
    var $RETROACT_TYPE_MSSTOCK = 3;
    var $EDIT_PAGE_LIMIT = 10;

    /**
     * 定数：作業区分
     * Enter description here ...
     * @var unknown_type
     */
    const DEFAULT_WORK_CLASSES_RETROACT = 24;

    var $work_type    = self::DEFAULT_WORK_CLASSES_RETROACT;
    var $center_type  = 1;
    var $hospital_type= 2;

    //ログインなしでの印刷を許可
    public function beforeFilter(){
        parent::beforeFilter();
        //CSV入力方式の切替
        if(Configure::read('ImportCsv.type')==0){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }
        //CSV出力方式の切替
        if(Configure::read('ExportCsv.type')==0){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }

    }

    /**
     * index: function of history(). grazie!
     */
    function index () {
        $this->history ();
    }

    /**
     * 遡及履歴
     */
    function history(){
        $this->setRoleFunction(69); //遡及履歴
        App::import("sanitize");    //サニタイズ用
        $RetroactHeaders = array(); //画面表示用

        if(isset($this->request->data['search_options']) && $this->request->data['search_options'] == '1'){
            $center_dept_id = null;
            $supplier_id = null;
            $hospital_id = null;

            //センターの部署ID取得
            $sql  = "SELECT id";
            $sql .= "  FROM mst_departments";
            $sql .= " WHERE mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
            $sql .= "   AND is_deleted = false";
            $res = array();
            $res = $this->MstDepartment->query($sql);
            $center_dept_id = $res[0][0]["id"];

            //仕入先の部署id取得
            if(!empty($this->request->data['TrnRetroactRecord']['department_code_from'])){
                //関数使用のためにデータ詰め替え
                $tempData['Condition']['supplier_code'] = $this->request->data['TrnRetroactRecord']['department_code_from'];
                $res = array();
                $res = $this->getSupplierId($tempData);
                if(!empty($res)){
                    $supplier_id = $res[0]['MstFacility']['id'];
                }
            }

            //得意先の部署ID取得
            if(!empty($this->request->data['TrnRetroactRecord']['department_code_to'])){
                //関数使用のためにデータ詰め替え
                $tempData['Condition']['hospital_code'] = $this->request->data['TrnRetroactRecord']['department_code_to'];
                $res = array();
                $res = $this->getHospitalId($tempData);
                if(!empty($res)){
                    $hospital_id = $res[0]['MstFacility']['id'];
                }
            }

            //画面表示用データ取得
            $sql  = 'SELECT TrnRetroactHeader.id                              AS "TrnRetroactHeader__id"';
            $sql .= '      ,TrnRetroactHeader.work_no                         AS "TrnRetroactHeader__work_no"';
            $sql .= "      ,( CASE TrnRetroactHeader.retroact_type";
            $sql .= "          WHEN '".Configure::read('Retroact.change_price')."'   THEN '価格変更'";
            $sql .= "          WHEN '".Configure::read('Retroact.stock_retroact')."' THEN '仕入遡及'";
            $sql .= "          WHEN '".Configure::read('Retroact.sales_retroact')."' THEN '売上遡及'";
            $sql .= '       END )                                             AS "TrnRetroactHeader__retroact_type"';
            $sql .= "      ,TO_CHAR(TrnRetroactHeader.work_date,'YYYY/MM/DD') AS ".'"TrnRetroactHeader__work_date"';
            $sql .= '      ,MstFacility.facility_name                         AS "MstFacility__facility_name"';
            $sql .= '      ,MstUser.user_name                                 AS "MstUser__user_name"';
            $sql .= "      ,TO_CHAR(TrnRetroactHeader.created,'YYYY/MM/DD')   AS ".'"TrnRetroactHeader__created"';
            $sql .= '      ,TrnRetroactHeader.recital                         AS "TrnRetroactHeader__recital"';
            $sql .= "      ,COUNT(TrnRetroactRecord.id) || '/' || TrnRetroactHeader.detail_count AS ".'"TrnRetroactHeader__count"';
            $sql .= "  FROM trn_retroact_headers AS TrnRetroactHeader";
            $sql .= " INNER JOIN mst_facilities AS MstFacility";
            $sql .= "    ON TrnRetroactHeader.mst_facility_id = MstFacility.id";
            $sql .= " INNER JOIN mst_users AS MstUser";
            $sql .= "    ON CAST(TrnRetroactHeader.creater AS INTEGER) = MstUser.id";
            $sql .= " INNER JOIN trn_retroact_records AS TrnRetroactRecord";
            $sql .= "    ON TrnRetroactHeader.id = TrnRetroactRecord.trn_retroact_header_id";
            $sql .= " INNER JOIN mst_item_units AS MstItemUnit";
            $sql .= "    ON TrnRetroactRecord.mst_item_unit_id = MstItemUnit.id";
            $sql .= "   AND MstItemUnit.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
            $sql .= " INNER JOIN mst_facility_items AS MstFacilityItem";
            $sql .= "    ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id";
            $sql .= "  LEFT JOIN mst_dealers AS MstDealer";
            $sql .= "    ON MstFacilityItem.mst_dealer_id = MstDealer.id";
            $sql .= " WHERE TrnRetroactHeader.is_deleted = false";

            //作業番号
            if(!empty($this->request->data["TrnRetroactHeader"]["work_no"])){
                $sql .= " AND TrnRetroactHeader.work_no = '".Sanitize::escape($this->request->data["TrnRetroactHeader"]["work_no"])."'";
            }
            //計上日
            if(!empty($this->request->data['TrnRetroactHeader']['start_date'])){
                $sql .= " AND TrnRetroactHeader.work_date >= '".Sanitize::escape($this->request->data['TrnRetroactHeader']['start_date'])."'";
            }
            if(!empty($this->request->data['TrnRetroactHeader']['end_date'])){
                $sql .= " AND TrnRetroactHeader.work_date <= '".Sanitize::escape($this->request->data['TrnRetroactHeader']['end_date'])."'";
            }
            //仕入先
            if(!empty($supplier_id)){
                $sql .= " AND TrnRetroactHeader.mst_facility_id = ".$supplier_id;
            }
            //得意先
            if(!empty($hospital_id)){
                $sql .= " AND TrnRetroactHeader.mst_facility_id = ".$hospital_id;
            }
            //仕入先、得意先共に未指定の場合、自センターの取引のみを表示
            if(empty($hospital_id) && empty($hospital_id)){
                $sql .= " AND (TrnRetroactRecord.department_id_from = ".$center_dept_id;
                $sql .= "  OR  TrnRetroactRecord.department_id_to = ".$center_dept_id.")";
            }
            //商品ID
            if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
                $sql .= " AND MstFacilityItem.internal_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."%'";
            }
            //商品名
            if(!empty($this->request->data['MstFacilityItem']['item_name'])){
                $sql .= " AND MstFacilityItem.item_name ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
            }
            //規格
            if(!empty($this->request->data['MstFacilityItem']['standard'])){
                $sql .= " AND MstFacilityItem.standard ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
            }
            //製品番号
            if(!empty($this->request->data['MstFacilityItem']['item_code'])){
                $sql .= " AND MstFacilityItem.item_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
            }
            //販売元
            if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
                $sql .= " AND MstDealer.dealer_name ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name'])."%'";
            }
            //JANコード
            if(!empty($this->request->data['MstFacilityItem']['jan_code'])){
                $sql .= " AND MstFacilityItem.jan_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['jan_code'])."%'";
            }
            //作業区分
            if(!empty($this->request->data['TrnRetroactRecord']['work_type'])){
                $sql .= " AND TrnRetroactHeader.work_type = ".$this->request->data['TrnRetroactRecord']['work_type'];
            }
            //遡及種別
            if(!empty($this->request->data['TrnRetroactRecord']['retroact_type'])){
                $sql .= " AND TrnRetroactHeader.retroact_type = ".$this->request->data['TrnRetroactRecord']['retroact_type'];
            }

            //操作可能病院で絞り込む
            $sql .='   and ( ';
            $sql .='      mstfacility.facility_type != ' . Configure::read('FacilityType.hospital');
            $sql .='      or mstfacility.id in ( ';
            $sql .='        select';
            $sql .='              a.mst_facility_id ';
            $sql .='          from';
            $sql .='            mst_user_belongings as a ';
            $sql .='          where';
            $sql .='            a.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .='            and a.is_deleted  = false ';
            $sql .='      )';
            $sql .='    ) ';


            $sql .= " GROUP BY ";
            $sql .= "       TrnRetroactHeader.id";
            $sql .= "      ,TrnRetroactHeader.work_no";
            $sql .= "      ,TrnRetroactHeader.retroact_type";
            $sql .= "      ,TrnRetroactHeader.work_date";
            $sql .= "      ,MstFacility.facility_name";
            $sql .= "      ,MstUser.user_name";
            $sql .= "      ,TrnRetroactHeader.created";
            $sql .= "      ,TrnRetroactHeader.recital";
            $sql .= "      ,TrnRetroactHeader.detail_count";
            $sql .= " ORDER BY ";
            $sql .= "       TrnRetroactHeader.work_no DESC";
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnRetroactHeader') );
            if ($this->_getLimitCount() > 0) {
                $sql .= " LIMIT ".$this->_getLimitCount();
            }

            $result = $this->TrnRetroactHeader->query($sql);

            $this->set('RetroactHeaders', $result);
        }
        $this->set('data' , $this->request->data);
        $this->__renderSelectBoxes();
        $this->render('history');
    }

    /**
     * Rendering select options for view.
     */
    private function __renderSelectBoxes () {
        $fields=array('MstFacility.facility_code','MstFacility.facility_name');
        $joins[] =array('table' => 'mst_facility_relations',
                        'alias' => 'MstFacilityRelation',
                        'type'  => 'inner',
                        'conditions' => array('MstFacilityRelation.partner_facility_id = MstFacility.id'
                                              ,'MstFacilityRelation.mst_facility_id' => $this->Session->read('Auth.facility_id_selected')
                                              ,'MstFacility.is_deleted = false'));
        // get suppliers
        $conditions = array('MstFacility.facility_type'=>Configure::read('FacilityType.supplier'));
        $this->set('suppliers', $this->MstFacility->find('list',array('fields'=>$fields,'joins'=>$joins,'conditions'=>$conditions , 'order'=>'MstFacility.facility_name, MstFacility.facility_code')));

        // get customers(hospital)
        $this->set('customers',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected'),
                       array(Configure::read('FacilityType.hospital')),
                       false
                       )
                   );

        // get work types.
        $this->set('work_types', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),24));
        // get retroact types.
        $this->set('retroact_types', Configure::read('Retroacts'));
    }

    /**
     * 遡及履歴からの変更(取消)
     */
    function edit(){
        if($this->request->data['TrnRetroactHeader']){
            $this->Session->write('Retroact.header',$this->request->data);
        }else{
            if($this->Session->check('Retroact.header')){
                $this->request->data = $this->Session->read('Retroact.header');
            }
        }
        $this->__renderSelectBoxes ();
        $params = $retroacts = array();
        $this->TrnRetroactHeader->unbindModel(array('hasMany'=>array('TrnRetroactRecord')));
        $params['fields'] = array(
            'TrnRetroactHeader.id',
            'TrnRetroactHeader.work_no',
            'to_char("TrnRetroactHeader"."work_date",\'YYYY/mm/dd\') as "TrnRetroactHeader__work_date"',
            'TrnRetroactHeader.retroact_type',
            'TrnRetroactHeader.work_type',
            'TrnRetroactHeader.recital',
            'TrnRetroactHeader.detail_count',
            'TrnRetroactHeader.modified',
            'TrnRetroactHeader.start_date',
            'TrnRetroactHeader.end_date',
            'TrnRetroactRecord.id',
            'TrnRetroactRecord.retroact_type',
            'TrnRetroactRecord.retroact_price',
            'MstFacilityItem.internal_code',
            'MstFacilityItem.item_name',
            'MstFacilityItem.item_code',
            'MstFacilityItem.standard',
            'MstFacilityItem.jan_code',
            'MstDealer.dealer_name',
            'MstItemUnit.per_unit',
            'MstUnitName.unit_name',
            'PerUnitName.unit_name',
            'Supplier.facility_name',
            'Hospital.facility_name',
            'MstClass.name'
            );

        $params['joins'][] = array(
            'table' => 'trn_retroact_records',
            'alias' => 'TrnRetroactRecord',
            'type' => 'INNER',
            'conditions' => array(
                'TrnRetroactRecord.trn_retroact_header_id = TrnRetroactHeader.id',
                'TrnRetroactRecord.is_deleted = FALSE'
                )
            );
        $page_params['joins'][] = array(
            'table' => 'trn_retroact_headers',
            'alias' => 'TrnRetroactHeader',
            'type' => 'INNER',
            'conditions' => array(
                'TrnRetroactHeader.id = TrnRetroactRecord.trn_retroact_header_id',
                'TrnRetroactHeader.is_deleted = FALSE'
                )
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_item_units',
            'alias' => 'MstItemUnit',
            'type' => 'INNER',
            'conditions' => array(
                'MstItemUnit.id = TrnRetroactRecord.mst_item_unit_id',
                'MstItemUnit.is_deleted = FALSE'
                )
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_facility_items',
            'alias' => 'MstFacilityItem',
            'type' => 'INNER',
            'conditions' => array(
                'MstFacilityItem.id = MstItemUnit.mst_facility_item_id',
                'MstItemUnit.is_deleted = FALSE'
                )
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_dealers',
            'alias' => 'MstDealer',
            'type' => 'LEFT',
            'conditions' => array(
                'MstDealer.id = MstFacilityItem.mst_dealer_id',
                'MstDealer.is_deleted = FALSE'
                )
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_unit_names',
            'alias' => 'MstUnitName',
            'type' => 'inner',
            'conditions' => array('MstUnitName.id = MstItemUnit.mst_unit_name_id')
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_unit_names',
            'alias' => 'PerUnitName',
            'type' => 'inner',
            'conditions' => array('PerUnitName.id = MstItemUnit.per_unit_name_id')
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_facilities',
            'alias' => 'Supplier',
            'type' => 'left',
            'conditions' => array(
                'Supplier.id = TrnRetroactHeader.mst_facility_id',
                'Supplier.facility_type'=>Configure::read('FacilityType.supplier')
                )
            );
        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_facilities',
            'alias' => 'Hospital',
            'type' => 'left',
            'conditions' => array(
                'Hospital.id = TrnRetroactHeader.mst_facility_id',
                'Hospital.facility_type'=>Configure::read('FacilityType.hospital')
                )
            );

        $page_params['joins'][] = $params['joins'][] = array(
            'table' => 'mst_classes',
            'alias' => 'MstClass',
            'type' => 'left',
            'conditions' => array(
                'MstClass.id = TrnRetroactHeader.work_type'
                )
            );


        $params['conditions']['TrnRetroactHeader.id'] = $this->request->data['TrnRetroactHeader']['id'];
        $params['conditions']['TrnRetroactHeader.is_deleted'] = false;
        if(isset($this->request->data['TrnRetroactRecord']['retroact_type']) && $this->request->data['TrnRetroactRecord']['retroact_type'] != ''){
            $params['conditions']['TrnRetroactRecord.retroact_type'] = $this->request->data['TrnRetroactRecord']['retroact_type'];
        }
        if(isset($this->request->data['TrnRetroactRecord']['department_code_from']) && $this->request->data['TrnRetroactRecord']['department_code_from'] != ''){
            $_aDepartmentIdFrom = $this->RetroactUtils->getSupplierDepartmentIdWithFacilityCode($this->request->data['TrnRetroactRecord']['department_code_from']);
            $params['conditions']['TrnRetroactRecord.department_id_from'] = $_aDepartmentIdFrom;
        }
        // for TrnRetroactRecord.
        if(isset($this->request->data['TrnRetroactRecord']['department_code_to']) && $this->request->data['TrnRetroactRecord']['department_code_to'] != ''){
            $_aDepartmentIdTo = $this->RetroactUtils->getHospitalDepartmentIdWithFacilityCode($this->request->data['TrnRetroactRecord']['department_code_to']);
            $params['conditions']['TrnRetroactRecord.department_id_to'] = $_aDepartmentIdTo;
        }
        if(isset($this->request->data['TrnRetroactRecord']['work_type']) && $this->request->data['TrnRetroactRecord']['work_type'] != ''){
            $params['conditions']['TrnRetroactHeader.work_type'] = $this->request->data['TrnRetroactRecord']['work_type'];
        }
        if(isset($this->request->data['TrnRetroactHeader']['retroact_type']) && $this->request->data['TrnRetroactHeader']['retroact_type'] != ''){
            $params['conditions']['TrnRetroactHeader.retroact_type'] = $this->request->data['TrnRetroactHeader']['retroact_type'];
        }
        // with facility item's conditions.
        if(isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] != ''){
            $params['conditions']['MstFacilityItem.internal_code'] = $this->request->data['MstFacilityItem']['internal_code'];
        }
        if(isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] != ''){
            $params['conditions']['MstFacilityItem.item_code LIKE '] = '%'.Sanitize::escape($this->request->data['MstFacilityItem']['item_code']).'%';
        }
        if(isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] != ''){
            $params['conditions']['MstFacilityItem.item_name LIKE '] = '%'.Sanitize::escape($this->request->data['MstFacilityItem']['item_name']).'%';
        }
        if(isset($this->request->data['MstFacilityItem']['dealer_name']) && $this->request->data['MstFacilityItem']['dealer_name'] != ''){
            $params['conditions']['MstDealer.dealer_name LIKE '] = '%'.Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name']).'%';
        }
        if(isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] != ''){
            $params['conditions']['MstFacilityItem.standard LIKE '] = '%'.Sanitize::escape($this->request->data['MstFacilityItem']['standard']).'%';
        }
        if(isset($this->request->data['MstFacilityItem']['jan_code']) && $this->request->data['MstFacilityItem']['jan_code'] != ''){
            $params['conditions']['MstFacilityItem.jan_code'] = Sanitize::escape($this->request->data['MstFacilityItem']['jan_code']);
        }

        $params['recursive'] = -1;
        $params['limit'] = (int)$this->EDIT_PAGE_LIMIT;
        $retroactHeader = $this->TrnRetroactHeader->find('all', $params);
        $this->set('retroactHeader',$retroactHeader[0]);

        $retroacts = array();
        $params['conditions']['TrnRetroactRecord.is_deleted'] = false;

        $this->paginate = array(
            'fields'=>$params['fields'],
            'limit'=>(int)$this->EDIT_PAGE_LIMIT,
            'recursive'=>-1,
            'order'=>array('MstFacilityItem.internal_code','MstItemUnit.id','TrnRetroactRecord.retroact_type'),
            'joins'=>$page_params['joins']
            );
        $tmp_retroacts = $this->paginate('TrnRetroactRecord',$params['conditions']);

        for ($i = 0;$i < count($tmp_retroacts);$i++) {
            if($tmp_retroacts[$i]['TrnRetroactHeader']['id'] !== $this->request->data['TrnRetroactHeader']['id']){
                continue;
            }
            $retroacts[$i] = $tmp_retroacts[$i];
        }
        $this->set('retroacts',$retroacts);
        $this->set('retroact_types', Configure::read('RetroactTypeDisplay'));
        $this->render('edit');
    }

    /**
     * 遡及履歴からの変更
     */
    function edit_result(){
        $now = date('Y/m/d H:i:s.u');
        //エラーメッセージ
        $error_msg = "";
        //エラーフラグ
        $is_error = false;

        //画面から渡された遡及ヘッダIDが取消可能なものかチェック
        $sql  = "SELECT TO_CHAR(work_date,'YYYY-MM-DD') AS work_date";
        $sql .= "      ,retroact_type";
        $sql .= "      ,modified";
        $sql .= "  FROM trn_retroact_headers";
        $sql .= " WHERE id = ".$this->request->data['TrnRetroactHeader']['id'];
        $sql .= "   AND is_deleted = false";
        $res = $this->TrnRetroactHeader->query($sql);

        if(!empty($res) && $res[0][0]['retroact_type'] == '1' ){
            //価格変更は取消しできない
            $this->Session->setFlash("価格変更は取消しできません。再度価格変更を行ってください。", 'growl', array('type'=>'error') );
            $this->request->data = array();
            $this->history();
            return;
        }

        if(empty($res) || $res[0][0]['work_date'] == ""){
            //対象データ無しのため履歴画面を再表示
            $this->Session->setFlash("取消不可能なデータです", 'growl', array('type'=>'error') );
            $this->request->data = array();
            $this->history();
            return;
        }else{
            //取得した計上日で締めチェック
            if(!$this->canUpdateAfterClose($res[0][0]['work_date'],$this->Session->read("Auth.facility_id_selected"))){
                //仮締め更新権限がない場合は再度条件設定
                $this->Session->setFlash("締めが実施されているため取消できません", 'growl', array('type'=>'error') );
                $this->history();
                return;
            }

            //取得した更新日時で更新チェック
            if($res[0][0]['modified'] > $this->request->data['TrnRetroactHeader']['chk_date']){
                //更新されている場合は再度条件設定
                $this->Session->setFlash("別ユーザによって更新されています", 'growl', array('type'=>'error') );
                $this->history();
                return;
            }
        }

        //トランザクション開始
        $this->TrnRetroactRecord->begin();

        //行ロック
        $this->TrnRetroactHeader->query('select * from trn_retroact_headers as a where a.id = ' . $this->request->data['TrnRetroactHeader']['id'] . ' for update ');
        //更新チェック
        $ret = $this->TrnRetroactHeader->query('select count(*) from trn_retroact_headers as a where a.id = ' . $this->request->data['TrnRetroactHeader']['id'] . " and a.modified > '" . $this->request->data['TrnRetroactHeader']['chk_date'] . "'");
        if($ret[0][0]['count'] > 0){
            $this->Session->setFlash("ほかユーザによって更新されています。最初からやり直してください。", 'growl', array('type'=>'error') );
            $this->history();
            return;
        }

        //取消処理に必要なデータを取得する
        $sql  = 'SELECT TrnRetroactHeader.modified      AS "TrnRetroactHeader__modified"';
        $sql .= '      ,TrnRetroactRecord.id            AS "TrnRetroactRecord__id"';
        $sql .= '      ,TrnRetroactRecord.retroact_type AS "TrnRetroactRecord__retroact_type"';
        $sql .= '      ,TrnClaim.id                     AS "TrnClaim__id"';
        $sql .= '      ,TrnClaim.trn_receiving_id       AS "TrnClaim__trn_receiving_id"';
        $sql .= '      ,TrnClaim.trn_consume_id         AS "TrnClaim__trn_consume_id"';
        $sql .= '  FROM trn_retroact_headers  AS TrnRetroactHeader';
        $sql .= ' INNER JOIN trn_retroact_records AS TrnRetroactRecord';
        $sql .= '    ON TrnRetroactHeader.id = TrnRetroactRecord.trn_retroact_header_id';
        $sql .= ' INNER JOIN trn_claims AS TrnClaim';
        $sql .= '    ON TrnRetroactRecord.id = TrnClaim.trn_retroact_record_id';
        $sql .= ' WHERE TrnRetroactHeader.id = '.$this->request->data['TrnRetroactHeader']['id'];
        $sql .= '   AND TrnRetroactHeader.is_deleted = false';
        $res1 = $this->TrnRetroactHeader->query($sql);
        //一応、存在チェック
        if(!$res1){
            $this->Session->setFlash("更新情報取得エラー", 'growl', array('type'=>'error') );
            $this->history();
            return;
        }

        //遡及ヘッダ更新
        $params = array();
        $params = array(
            "id"         => $this->request->data['TrnRetroactHeader']['id'],
            "is_deleted" => true,
            "modifier"   => $this->Session->read('Auth.MstUser.id'),
            "modified"   => $now
            );
        $this->TrnRetroactHeader->create();
        $res2 = $this->TrnRetroactHeader->save($params,false);
        if(!$res2){
            //登録失敗
            $this->TrnRetroactRecord->rollback();
            $this->Session->setFlash("遡及ヘッダ更新エラー", 'growl', array('type'=>'error') );
            $is_error = true;
            $this->history();
            return;
        }

        /*** 各テーブル更新 ***/
        foreach($res1 as $row){
            //遡及明細更新
            $params = array();
            $params = array(
                'id'         => $row['TrnRetroactRecord']['id'],
                'is_deleted' => true,
                'modifier'   => $this->Session->read('Auth.MstUser.id'),
                'modified'   => $now
                );
            $this->TrnRetroactRecord->create();
            $res3 = $this->TrnRetroactRecord->save($params,false);
            if(!$res3){
                //更新失敗
                $error_msg = "遡及明細更新エラー";
                $is_error = true;
                break;
            }

            //請求明細更新
            $params = array();
            $params = array(
                'id'                      => $row['TrnClaim']['id'],
                'is_not_retroactive'      => false,
                'modifier'                => $this->Session->read('Auth.MstUser.id'),
                'modified'                => $now,
                'is_deleted'              => (($row['TrnRetroactRecord']['retroact_type'] > 3)?'true':'false'),
                'retroact_execution_flag' => false
                );
            $this->TrnClaim->create();
            $res4 = $this->TrnClaim->save($params,false);
            if(!$res4){
                //更新失敗
                $error_msg = "請求明細更新エラー";
                $is_error = true;
                break;
            }

            //入荷明細更新
            if(!empty($row['TrnClaim']['trn_receiving_id'])){
                $params = array();
                $params = array(
                    'id'                      => $row['TrnClaim']['trn_receiving_id'],
                    'is_retroactable'         => true,
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'modified'                => $now,
                    'retroact_execution_flag' => false
                    );
                $this->TrnReceiving->create();
                $res5 = $this->TrnReceiving->save($params,false);
                if(!$res5){
                    //更新失敗
                    $error_msg = "入荷明細更新エラー";
                    $is_error = true;
                    break;
                }
            }

            //消費明細更新
            if(!empty($row['TrnConsume']['trn_consume_id'])){
                $params = array();
                $params = array(
                    'id'                      => $row['TrnClaim']['trn_consume_id'],
                    'is_retroactable'         => true,
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'modified'                => $now,
                    'retroact_execution_flag' => false
                    );
                $this->TrnConsume->create();
                $res6 = $this->TrnConsume->save($params,false);
                if(!$res6){
                    //更新失敗
                    $error_msg = "消費明細更新エラー";
                    $is_error = true;
                    break;
                }
            }
        }

        //コミット前に更新チェック
        $ret = $this->TrnRetroactHeader->query('select count(*) from trn_retroact_headers as a where a.id = ' . $this->request->data['TrnRetroactHeader']['id'] . " and a.modified > '" . $this->request->data['TrnRetroactHeader']['chk_date'] . "' and a.modified <> '" . $now . "'");
        if($ret[0][0]['count'] > 0){
            $this->TrnRetroactRecord->rollback();
            $this->Session->setFlash("ほかユーザによって更新されています。最初からやり直してください。", 'growl', array('type'=>'error') );
            $this->history();
            return;
        }

        if($is_error){
            //ロールバック
            $this->TrnRetroactRecord->rollback();

            //画面遷移
            $this->Session->setFlash($error_msg,'growl',array('type'=>'error'));
            $this->history();
            return;
        }else{
            //コミット
            $this->TrnRetroactRecord->commit();
            //画面表示用データ取得
            $sql = '';
            $sql .= 'SELECT TrnRetroactHeader.work_no AS "TrnRetroactHeader__work_no"';
            $sql .= "      ,CASE TrnRetroactHeader.retroact_type";
            $sql .= "       WHEN '".Configure::read('Retroact.stock_retroact')."' THEN '仕入遡及'";
            $sql .= "       WHEN '".Configure::read('Retroact.sales_retroact')."' THEN '売上遡及'";
            $sql .= '       END AS "TrnRetroactHeader__retroact_type"';
            $sql .= "      ,TO_CHAR(TrnRetroactHeader.work_date,'YYYY/MM/DD') AS ".'"TrnRetroactHeader__work_date"';
            $sql .= '      ,MstFacility.facility_name AS "MstFacility__facility_name"';
            $sql .= '      ,MstUser.user_name AS "MstUser__user_name"';
            $sql .= "      ,TO_CHAR(TrnRetroactHeader.created,'YYYY/MM/DD') AS ".'"TrnRetroactHeader__created"';
            $sql .= '      ,TrnRetroactHeader.recital AS "TrnRetroactHeader__recital"';
            $sql .= "      ,COUNT(TrnRetroactRecord.id) || '/' || TrnRetroactHeader.detail_count AS ".'"TrnRetroactHeader__count"';
            $sql .= "  FROM trn_retroact_headers AS TrnRetroactHeader";
            $sql .= " INNER JOIN mst_facilities AS MstFacility";
            $sql .= "    ON TrnRetroactHeader.mst_facility_id = MstFacility.id";
            $sql .= " INNER JOIN mst_users AS MstUser";
            $sql .= "    ON CAST(TrnRetroactHeader.creater AS INTEGER) = MstUser.id";
            $sql .= " INNER JOIN trn_retroact_records AS TrnRetroactRecord";
            $sql .= "    ON TrnRetroactHeader.id = TrnRetroactRecord.trn_retroact_header_id";
            $sql .= " WHERE TrnRetroactHeader.id = ".$this->request->data["TrnRetroactHeader"]["id"];
            $sql .= " GROUP BY ";
            $sql .= "       TrnRetroactHeader.work_no";
            $sql .= "      ,TrnRetroactHeader.retroact_type";
            $sql .= "      ,TrnRetroactHeader.work_date";
            $sql .= "      ,MstFacility.facility_name";
            $sql .= "      ,MstUser.user_name";
            $sql .= "      ,TrnRetroactHeader.created";
            $sql .= "      ,TrnRetroactHeader.recital";
            $sql .= "      ,TrnRetroactHeader.detail_count";
            $result = $this->TrnRetroactHeader->query($sql);
            $this->Session->setFlash("遡及取消が完了しました",'growl',array('type'=>'star'));
            $this->set("result",$result);
        }
        $this->set("date",$this->request->data);
        $this->render('edit_result');
    }

    /**
     * 遡及履歴からの変更(取消)結果表示
     */
    function edit_result_view () {
        $this->render('edit_result_view');
    }

    /**
     * 遡及履歴からの変更(取消)印刷
     */
    function history_print () {

    }

    /**
     * 仕入価格変更
     */
    function set_stockprice () {
        $this->setRoleFunction(65); //仕入価格変更
        if($this->Session->check('Retroact')){
            $this->request->data = $this->Session->read('Retroact.search');
            $this->Session->delete('Retroact');
        }

        $this->set('suppliers' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('FacilityType.supplier')),
                                                         true,
                                                         array('facility_code' , 'facility_name'),
                                                         'facility_name, facility_code'
                                                         ));

        $this->set('owners' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                      array(Configure::read('FacilityType.donor')),
                                                      true
                                                      ));


        $work_no = Configure::read('workNoModels');
        $this->set('work_types', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),array_search('TrnRetroactHeader', $work_no)));

        $this->render('set_stockprice');
    }

    /**
     * マスタ設定による変更登録
     */
    function reflect_with_master ($_id = null) {
        if($_id && !$this->Session->check('Retroact.retroact_type')){
            $this->Session->write('Retroact.retroact_type',$_id);
        }
        if(count($this->request->data)){
            $this->Session->write('Retroact.search',$this->request->data);
        } else {
            $this->request->data = $this->Session->read('Retroact.confirm');
        }

        //締めチェック
        //開始～終了の期間で一部でも、本締めされていたらNGとする。
        if(!$this->closedCheck($this->request->data['TrnRetroactHeader']['start_date'], $this->request->data['TrnRetroactHeader']['end_date'])){
            $this->Session->setFlash('締め済みの日付が含まれます。変更する場合は締めを取り消してください。', 'growl', array('type'=>'error'));
            if($_id == 1){
                $this->redirect('set_stockprice');
            }else{
                $this->redirect('set_salesprice');
            }

        }
        $master_table = array();
        $facility_type = '';
        // 設定テーブル(仕入or売上)を指定
        if(!$this->Session->check('Retroact.master_table')
           && (int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_STOCK){
            $master_table = array('table_name'     => 'mst_transaction_configs'
                                  , 'table_alias'  => 'MstTransactionConfig'
                                  , 'price_column' => 'transaction_price');
            $facility_type  = Configure::read('FacilityType.supplier');
        } else if(!$this->Session->check('Retroact.master_table')
                  && (int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_MSSTOCK){
            $master_table = array('table_name'     => 'mst_transaction_configs'
                                  , 'table_alias'  => 'MstTransactionConfig'
                                  , 'price_column' => 'ms_transaction_price');
            $facility_type  = Configure::read('FacilityType.supplier');
        } else if(!$this->Session->check('Retroact.master_table')
                  && (int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_SALES){
            $master_table = array('table_name'     => 'mst_sales_configs'
                                  , 'table_alias'  => 'MstSalesConfig'
                                  , 'price_column' => 'sales_price');
            $facility_type  = Configure::read('FacilityType.hospital');
        }
        
        $this->Session->write('Retroact.master_table',$master_table);

        //得意先 or 仕入先
        $this->request->data['TrnRetroactHeader']['facility_to_id'] =
          $this->getFacilityId(
              $this->request->data['TrnRetroactHeader']['facility_to'] ,
              $facility_type ,
              $this->Session->read('Auth.facility_id_selected')
              );
        $this->request->data['TrnRetroactHeader']['facility_to_name']  = $this->getFacilityName($this->request->data['TrnRetroactHeader']['facility_to_id']);
        //作業区分
        $this->request->data['TrnRetroactHeader']['work_class_txt'] = $this->getWorkTypeName($this->request->data['TrnRetroactHeader']['work_class']);

        $this->set('retroact_type', $this->Session->read('Retroact.retroact_type'));
        $this->set('table_type', $this->Session->read('Retroact.master_table'));
        $this->render('reflect_with_master');
    }

    /**
     * マスタ設定による変更登録確認
     */
    function reflect_with_master_confirm () {

        //更新時間チェック用に変更条件確認画面を開いた時間を保存
        $this->Session->write('Reflect.readTime',date('Y-m-d H:i:s'));

        $master_table = $this->Session->read('Retroact.master_table');
        if(count($this->request->data)){
            $this->Session->write('Retroact.confirm',$this->request->data);
        } else {
            $this->request->data = $this->Session->read('Retroact.result');
        }
        if ($this->request->data) {
            $data = $this->request->data;
            $master_table = $this->Session->read('Retroact.master_table');

            $_itemunits = $this->$master_table['table_alias']->find('all',$this->__getSearchConfigSQL($data,$master_table));
        } else {
            $this->Session->setFlash('遡及を行うために必要なデータがありません。', 'growl', array('type'=>'error') );
            $this->redirect('reflect_with_unitprice_confirm');
            return;
        }
        if(!is_array($_itemunits)){
            $this->Session->setFlash('遡及を行うために必要なデータがありません。', 'growl', array('type'=>'error') );
            $this->redirect('reflect_with_unitprice_confirm');
            return;
        }

        $itemunits = array();
        foreach($_itemunits as $item_data){
            $itemunits[] = array('mst_item_unit_id' => $item_data['MstItemUnit']['id']
                                 , 'price' => $item_data[$master_table['table_alias']][$master_table['price_column']]
                                 );
        }
        $this->set('retroact_type', $this->Session->read('Retroact.retroact_type'));
        $this->set('table_type', $this->Session->read('Retroact.master_table'));
        $this->set('item_units',$itemunits);
        $this->render('reflect_with_master_confirm');
    }


    /**
     * マスタ設定による変更登録確認結果表示
     */
    function reflect_with_master_result () {
        if(count($this->request->data)){
            $this->Session->write('Retroact.result',$this->request->data);
        }

        $_success = true;
        // 単価変更ロジック
        $_success = $this->__retroactLogic( $this->request->data['TrnRetroactHeader'] , $this->request->data['TrnRetroactRecord'], false);

        if($_success === false) {
            $this->Session->setFlash('遡及処理に失敗しました', 'growl', array('type'=>'error') );

            $this->redirect('reflect_with_master_confirm');
            return;
            exit;
        } else {
            $this->set('count',$_success);
        }

        $this->set('retroact_type', $this->Session->read('Retroact.retroact_type'));
        $this->set('table_type', $this->Session->read('Retroact.master_table'));
        if($this->Session->check('Retroact')){
            $this->Session->delete('Retroact');
        }

        $this->render('reflect_with_master_result_view');
    }

    /**
     * 単価による変更登録
     */
    function reflect_with_unitprice ($retroact_type = null) {
        App::import('Sanitize');
        if($retroact_type && !$this->Session->check('Retroact.retroact_type')){
            $this->Session->write('Retroact.retroact_type',$retroact_type);
        }

        if(count($this->request->data)){
            $this->Session->write('Retroact.search',$this->request->data);
        } else {
            $this->request->data = $this->Session->read('Retroact.confirm');
        }


        $master_table = array();
        // 設定テーブル(仕入or売上)を指定
        if(!$this->Session->check('Retroact.master_table')
           && (int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_STOCK){
            $master_table = array('table_name'     => 'mst_transaction_configs'
                                  , 'table_alias'  => 'MstTransactionConfig'
                                  , 'price_column' => 'transaction_price');
            $this->Session->write('Retroact.master_table',$master_table);
        } else if(!$this->Session->check('Retroact.master_table')
                  && (int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_MSSTOCK){
            $master_table = array('table_name'     => 'mst_transaction_configs'
                                  , 'table_alias'  => 'MstTransactionConfig'
                                  , 'price_column' => 'ms_transaction_price');
            $this->Session->write('Retroact.master_table',$master_table);
        } else if(!$this->Session->check('Retroact.master_table')
                  && (int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_SALES){
            $master_table = array('table_name'     => 'mst_sales_configs'
                                  , 'table_alias'  => 'MstSalesConfig'
                                  , 'price_column' => 'sales_price');
            $this->Session->write('Retroact.master_table',$master_table);
        }

        // 商品取得
        $SearchResult     = array(); //画面上
        $CartSearchResult = array(); //画面下
        //検索ボタン押下
        if(isset($this->request->data['search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $limit = '' ;
            if(isset($this->request->data['FacilityItems']['limit'])){
                $limit .= 'limit ' . $this->request->data['FacilityItems']['limit'];
            }

            $order =' order by d.internal_code ,c.per_unit ';

            $where = '';

            $where .=' and d.item_type = ' . Configure::read('Items.item_types.normalitem'); // 通常品のみ
            $where .=' and d.is_deleted = false '; //施設採用品で削除されているものは除外
            $where .=' and c.is_deleted = false '; //包装単位で適用が外れているものは除外

            $where2 = $where;

            if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and a.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 .= " and a.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 .= " and 0 = 1 ";
            }


            if(isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] !== ''){
                $where .=" and d.internal_code ilike '%" .$this->request->data['MstFacilityItem']['internal_code']."%'";
            }
            if(isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] !== '' ){
                $where .=" and d.standard ilike '%" .$this->request->data['MstFacilityItem']['standard']."%'";
            }
            if(isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] !== ''){
                $where .=" and d.item_name ilike '%" .$this->request->data['MstFacilityItem']['item_name']."%'";
            }
            if(isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] !== ''){
                $where .=" and d.item_code ilike '%" .$this->request->data['MstFacilityItem']['item_code']."%'";
            }
            if(isset($this->request->data['MstDealer']['dealer_name']) && $this->request->data['MstDealer']['dealer_name'] !== ''){
                $where .=" and h.dealer_name ilike '%" .$this->request->data['MstDealer']['dealer_name']."%'";
            }
            //JAN(前方一致)
            if(isset($this->request->data['MstFacilityItem']['jan_code']) && $this->request->data['MstFacilityItem']['jan_code'] !== ''){
                $where .=" and d.jan_code ilike '" .$this->request->data['MstFacilityItem']['jan_code']."%'";
            }

            if(isset($this->request->data['MstFacilityItem']['start_date']) && $this->request->data['MstFacilityItem']['start_date'] !== ''){
                $where .=" and a.start_date = '" .$this->request->data['MstFacilityItem']['start_date'] . "'";
            }

            //売上の場合病院のIDで絞り込む
            if((int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_SALES){
                $where .= ' and a.partner_facility_id = ' . $this->request->data['TrnRetroactHeader']['facility_to_id'];

            }

            // 検索（画面上）用一覧取得
            $SearchResult = $this->MstFacilityItem->query($this->searchFacilityItems($where , $order , $limit));

            // カート（画面下）用一覧取得
            $CartSearchResultTmp = $this->MstFacilityItem->query($this->searchFacilityItems($where2 , $order ));

            if(!empty($this->request->data['cart']['tmpid'])){
                //カートに追加した順に並べなおす。
                foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
                    foreach($CartSearchResultTmp as $tmp){
                        if($tmp['TrnRetroact']['id'] == $id){
                            $CartSearchResult[] = $tmp;
                            break;
                        }
                    }
                }
            }
            $this->request->data = $data;
        }else{
            //検索ボタンが押されていない => 画面遷移初回
            //締めチェック
            //開始～終了の期間で一部でも、本締めされていたらNGとする。
            if(!$this->closedCheck($this->request->data['TrnRetroactHeader']['start_date'], $this->request->data['TrnRetroactHeader']['end_date'])){
                $this->Session->setFlash('締め済みの日付が含まれます。変更する場合は締めを取り消してください。', 'growl', array('type'=>'error'));
                if($retroact_type == 1 ){
                    $this->redirect('set_stockprice');
                }else{
                    $this->redirect('set_salesprice');
                }
            }

            //施主
            $this->request->data['TrnRetroactHeader']['mst_facility_id']  =
                  $this->getFacilityId(
                      $this->request->data['TrnRetroactHeader']['mst_facility'] ,
                      Configure::read('FacilityType.donor') ,
                      $this->Session->read('Auth.facility_id_selected')
                      );

            $this->request->data['TrnRetroactHeader']['mst_facility_name']  = $this->getFacilityName($this->request->data['TrnRetroactHeader']['mst_facility_id']);

            $facility_type = "";
            if((int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_STOCK){
                $facility_type = Configure::read('FacilityType.supplier');
            }elseif((int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_MSSTOCK){
                $facility_type = Configure::read('FacilityType.supplier');
            }elseif((int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_SALES){
                $facility_type = Configure::read('FacilityType.hospital');
            }
            //得意先 or 仕入先
            $this->request->data['TrnRetroactHeader']['facility_to_id'] =
              $this->getFacilityId(
                  $this->request->data['TrnRetroactHeader']['facility_to'] ,
                  $facility_type , 
                  $this->Session->read('Auth.facility_id_selected')
                  );
            $this->request->data['TrnRetroactHeader']['facility_to_name']  = $this->getFacilityName($this->request->data['TrnRetroactHeader']['facility_to_id']);
            //作業区分
            $this->request->data['TrnRetroactHeader']['work_class_txt'] = $this->getWorkTypeName($this->request->data['TrnRetroactHeader']['work_class']);
        }

        $this->set('SearchResult', $SearchResult);
        $this->set('CartSearchResult', $CartSearchResult);

        $this->set('retroact_type', $this->Session->read('Retroact.retroact_type'));
        $this->set('table_type', $this->Session->read('Retroact.master_table'));

        $this->render('reflect_with_unitprice');

    }

    /**
     * 単価による変更登録確認
     */
    function reflect_with_unitprice_confirm () {
        //更新時間チェック用に変更条件確認画面を開いた時間を保存
        $this->Session->write('Reflect.readTime',date('Y-m-d H:i:s'));

        $ResultTmp = $this->MstFacilityItem->query($this->searchFacilityItems(' and a.id in (' . $this->request->data['cart']['tmpid'] . ')' ));

        if(!empty($this->request->data['cart']['tmpid'])){
            //カートに追加した順に並べなおす。
            foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
                foreach($ResultTmp as $tmp){
                    if($tmp['TrnRetroact']['id'] == $id){
                        $Result[] = $tmp;
                        break;
                    }
                }
            }
        }


        $this->set('itemunits',$Result);

        $this->set('retroact_type', $this->Session->read('Retroact.retroact_type'));
        $this->set('table_type', $this->Session->read('Retroact.master_table'));
        $this->render('reflect_with_unitprice_confirm');
    }

    /**
     * 単価による変更登録結果
     */
    function reflect_with_unitprice_result() {
        if(count($this->request->data)){
            $this->Session->write('Retroact.result',$this->request->data);
        }
        $_success = true;
        $_render_page = 'reflect_with_unitprice_result_view';

        // 単価変更ロジック
        $_success = $this->__retroactLogic( $this->request->data['TrnRetroactHeader'] , $this->request->data['TrnRetroactRecord'], true);
        if($_success === false) {
            $this->Session->setFlash('遡及処理に失敗しました', 'growl', array('type'=>'error') );
            $this->redirect('reflect_with_unitprice_confirm');
            return;
            exit;
        } else {
            $this->set('count',$_success);
        }
        $this->set('retroact_type', $this->Session->read('Retroact.retroact_type'));
        if($this->Session->check('Retroact')){
            $this->Session->delete('Retroact');
        }
        $this->render('reflect_with_unitprice_result_view');
    }

    /**
     * CSVによる変更登録
     */
    function reflect_with_csv ($mode = "") {
        if(count($this->request->data)){
            $this->Session->write('Retroact.search',$this->request->data);
        } else {
            $this->request->data = $this->Session->read('Retroact.confirm');
        }

        if($mode && !$this->Session->check('Retroact.retroact_type')){
            $this->Session->write('Retroact.retroact_type',$mode);
        }

        //締めチェック
        //開始～終了の期間で一部でも、本締めされていたらNGとする。
        if(!$this->closedCheck($this->request->data['TrnRetroactHeader']['start_date'], $this->request->data['TrnRetroactHeader']['end_date'])){
            $this->Session->setFlash('締め済みの日付が含まれます。変更する場合は締めを取り消してください。', 'growl', array('type'=>'error'));
            if($mode==1){
                $this->redirect('set_stockprice');
            }else{
                $this->redirect('set_salesprice');
            }
        }

        $master_table = array();
        // 設定テーブル(仕入or売上)を指定
        if(!$this->Session->check('Retroact.master_table')
           && (int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_STOCK){
            $master_table = array('table_name'     => 'mst_transaction_configs'
                                  , 'table_alias'  => 'MstTransactionConfig'
                                  , 'price_column' => 'transaction_price');
            $this->Session->write('Retroact.master_table',$master_table);
            $facility_type = Configure::read('FacilityType.supplier');
        }elseif(!$this->Session->check('Retroact.master_table')
           && (int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_MSSTOCK){
            $master_table = array('table_name'     => 'mst_transaction_configs'
                                  , 'table_alias'  => 'MstTransactionConfig'
                                  , 'price_column' => 'ms_transaction_price');
            $this->Session->write('Retroact.master_table',$master_table);
            $facility_type = Configure::read('FacilityType.supplier');
        } else if(!$this->Session->check('Retroact.master_table')
                  && (int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_SALES){
            $master_table = array('table_name'     => 'mst_sales_configs'
                                  , 'table_alias'  => 'MstSalesConfig'
                                  , 'price_column' => 'sales_price');
            $this->Session->write('Retroact.master_table',$master_table);
            $facilty_type = Configure::read('FacilityType.hospital');
        }

        //得意先 or 仕入先
        $this->request->data['TrnRetroactHeader']['facility_to_id'] =
          $this->getFacilityId(
              $this->request->data['TrnRetroactHeader']['facility_to'] ,
              $facility_type ,
              $this->Session->read('Auth.facility_id_selected')
              );
        $this->request->data['TrnRetroactHeader']['facility_to_name']  = $this->getFacilityName($this->request->data['TrnRetroactHeader']['facility_to_id']);
        //作業区分
        $this->request->data['TrnRetroactHeader']['work_class_txt'] = $this->getWorkTypeName($this->request->data['TrnRetroactHeader']['work_class']);

        $this->set('mode', $mode);
        $this->set('retroact_type', $this->Session->read('Retroact.retroact_type'));
        $this->render('reflect_with_csv');
    }

    /**
     * CSVによる変更登録確認
     */
    function reflect_with_csv_confirm ($mode = "") {

        //更新時間チェック用に変更条件確認画面を開いた時間を保存
        $this->Session->write('Reflect.readTime',date('Y-m-d H:i:s'));

        if(count($this->request->data)){
            $this->Session->write('Retroact.confirm',$this->request->data);
        } else {
            $this->request->data = $this->Session->read('Retroact.result');
        }

        // CSVデータアップロード
        $_upfile = $this->request->data['result'];

        if (is_uploaded_file($_upfile['tmp_name'])){
            $this->CsvReadUtils->setFileName( $this->request->data['result']['tmp_name'] );
        }else{
            $this->Session->setFlash('CSVデータを確認してください(upload)', 'growl', array('type'=>'error') );
            $this->redirect('reflect_with_csv');
        }

        // CsvReadUtils初期設定
        $this->CsvReadUtils->setLineMaxLength( 2000 );

        // CSVオープン
        $this->CsvReadUtils->open();

        // ハッシュキー設定
        $this->CsvReadUtils->hashKeyClear();
        $this->CsvReadUtils->hashKeyAdd( "internal_code" );
        $this->CsvReadUtils->hashKeyAdd( "per_unit" );
        $this->CsvReadUtils->hashKeyAdd( "price" );
        $this->CsvReadUtils->hashKeyAdd( "center_code" );
        $this->CsvReadUtils->hashKeyAdd( "facility_code" );

        $i = 0;
        $item_unit_id = array();
        $error_mes = array();

        //1行目を取得
        $header_data = $this->CsvReadUtils->readCsvLine();

        if((int)$this->Session->read('Retroact.retroact_type') === $this->RETROACT_TYPE_STOCK){
            //ヘッダ項目をチェック
            if($header_data != array('internal_code' => '商品ID',
                                     'per_unit'      => '入り数',
                                     'price'         => '単価',
                                     'center_code'   => '商品施設コード',
                                     'facility_code' => '仕入先施設コード'
                                     )){

                $this->Session->setFlash('仕入価格変更用のCSVデータではありません。ヘッダ行をご確認ください。', 'growl', array('type'=>'error') );
                $this->redirect('reflect_with_csv');
            }
            $partner_name = '仕入先';
            $facility_type = Configure::read('FacilityType.supplier');


        } elseif((int)$this->Session->read('Retroact.retroact_type') === $this->RETROACT_TYPE_MSSTOCK){
            //ヘッダ項目をチェック
            if($header_data != array('internal_code' => '商品ID',
                                     'per_unit'      => '入り数',
                                     'price'         => '単価',
                                     'center_code'   => '商品施設コード',
                                     'facility_code' => '仕入先施設コード'
                                     )){

                $this->Session->setFlash('MS法人仕入価格変更用のCSVデータではありません。ヘッダ行をご確認ください。', 'growl', array('type'=>'error') );
                $this->redirect('reflect_with_csv');
            }
            $partner_name = '仕入先';
            $facility_type = Configure::read('FacilityType.supplier');

        } else if((int)$this->Session->read('Retroact.retroact_type') === $this->RETROACT_TYPE_SALES){
            //ヘッダ項目をチェック
            if($header_data != array('internal_code' => '商品ID',
                                     'per_unit'      => '入り数',
                                     'price'         => '単価',
                                     'center_code'   => '商品施設コード',
                                     'facility_code' => '得意先施設コード'
                                     )){

                $this->Session->setFlash('売上価格変更用のCSVデータではありません。ヘッダ行をご確認ください。', 'growl', array('type'=>'error') );
                $this->redirect('reflect_with_csv');
            }
            $partner_name = '得意先';
            $facility_type = Configure::read('FacilityType.hospital');
        }

        while ( ! $this->CsvReadUtils->eof() ) {
            // CSV一行読込
            $line_data = $this->CsvReadUtils->readCsvLine();

            // EOFだけの行を読み込んでしまうため
            if ( $line_data == "" ) {
                continue;
            }

            $i++; //CSV件数カウント（エラー分含む）

            if(count($line_data) <> 5){
                // データ配列が空の場合、登録CSVの行数が一致しない場合(不特定データ)はreflect_with_csvへリダイレクト
                $this->Session->setFlash('CSVデータを確認してください(empty)', 'growl', array('type'=>'error') );
                $this->redirect('reflect_with_csv');
            }

            //商品ID存在チェック
            $_fitem_info = $this->MstFacilityItem->find('first',array('fields'     =>'id',
                                                                      'recursive'  => -1,
                                                                      'conditions' => array('internal_code'   => $line_data['internal_code'],
                                                                                            'mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
                                                                                            )
                                                                      )
                                                        );
            if((empty($_fitem_info)) or (count($_fitem_info)==0) ){
                $error_mes[] = $_i."件目：商品IDが存在しません";
                continue;
            }

            //包装単位ID存在チェック
            $_itemunit_info = $this->MstItemUnit->find('first',array('fields'     => array('id','mst_facility_item_id'),
                                                                     'recursive'  => -1,
                                                                     'conditions' => array('mst_facility_item_id' => $_fitem_info['MstFacilityItem']['id'],
                                                                                           'per_unit'             => $line_data['per_unit']
                                                                                           )
                                                                     )
                                                       );

            if((empty($_itemunit_info)) or (count($_itemunit_info)==0) ){
                $error_mes[] = $_i."件目：入り数が存在しません";
                continue;
            }


            //単価フォーマットチェック(0許可・小数点以下2位まで)
            $match = "\d+(\.\d+)?";
            if(!preg_match("/".$match."/", $line_data['price'])){
                $error_mes[] = $_i."件目：単価は小数点以下2位までの半角数値で入力してください";
                continue;
            }


            //商品の施設ID存在チェック(区分がセンターの該当IDの施設が存在するかどうか)
            $_facility_info = $this->MstFacility->find('first',array('fields'     => array('id','facility_type','facility_code'),
                                                                     'recursive'  => -1,
                                                                     'conditions' => array('facility_type' => Configure::read('FacilityType.center'),
                                                                                           'facility_code' => $line_data['center_code']
                                                                                           )
                                                                     )
                                                       );

            if((empty($_facility_info)) or (count($_facility_info)==0) ){
                $error_mes[] = $_i."件目：商品施設コードが存在しません";
                continue;
            }

            //仕入先の施設ID存在チェック(区分が仕入先の該当IDの施設が存在するかどうか)
            $_supplier_info = $this->MstFacility->find('first',array('fields'     => array('id','facility_type'),
                                                                     'recursive'  => -1,
                                                                     'conditions' => array('facility_type' => $facility_type,
                                                                                           'facility_code' => $line_data['facility_code']
                                                                                           )
                                                                     )
                                                       );
            if((empty($_supplier_info)) or (count($_supplier_info)==0) ){
                $error_mes[] = $_i."件目：".$partner_name."施設コードが存在しません";
                continue;
            }

            //TODO ヘッダの仕入先と同じかどうか
            if($line_data['facility_code'] != $this->request->data['TrnRetroactHeader']['facility_to_code']){
                $error_mes[] = $_i."件目：".$partner_name."施設コードが最初に選択した".$partner_name."と異なります";
                continue;
            }

            //エラーなしの場合該当の商品を検索
            $sql = ' select ';
            $sql .= ' MstFacilityItem.id             as MstFacilityItem__id';
            $sql .= ',MstFacilityItem.internal_code  as MstFacilityItem__internal_code';
            $sql .= ',MstItemUnit.id                 as MstItemUnit__id';
            $sql .= ',MstItemUnit.id                 as retroact__mst_item_unit_id';
            $sql .= ',' . Sanitize::escape($line_data['price']) . ' as retroact__price';
            $sql .= ' from ';
            $sql .= ' mst_item_units as MstItemUnit ';
            $sql .= ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ' ;
            $sql .= ' left join mst_facilities as MstFacilityOwner on MstFacilityItem.mst_owner_id = MstFacilityOwner.id';
            $sql .= ' where ';
            $sql .= ' MstItemUnit.per_unit = ' . Sanitize::escape($line_data['per_unit']) ;
            $sql .= ' and MstFacilityItem.internal_code = '. "'" . Sanitize::escape($line_data['internal_code']) . "'" ;
            $sql .= ' and MstFacilityOwner.facility_code = '. "'" . (string)$this->request->data['TrnRetroactHeader']['mst_facility']. "'" ;
            $sql .= ' and MstFacilityItem.mst_facility_id = ' . $_facility_info['MstFacility']['id'];

            $itemList = array();
            //**********************************************************************
            $itemList = $this->MstItemUnit->query($sql);
            //**********************************************************************

            if((empty($itemList)) or (count($itemList)==0) ){
                $error_mes[] = $_i."件目：該当する商品データが存在しません";
                continue;
            }

            $current_list = current($itemList);
            if(array_search($current_list['mstitemunit']['id'], $item_unit_id) !== false){
                $error_mes[] = $_i."件目：商品データが重複しています";
                $item_unit_id[] = $current_list['mstitemunit']['id'];
                continue;
            }

            $item_unit_id[] = $current_list['mstitemunit']['id'];
            $output[] = $itemList;
        }

        $this->Session->write('Retroact.output', $output);

        $this->set('retroact_type', $this->Session->read('Retroact.retroact_type'));
        $this->set('error_mes', $error_mes);
        $this->set('input_count',$i);
        $this->render('reflect_with_csv_confirm');
    }

    /**
     * CSV読み込みメソッド
     * @param strings $_files
     * @return array
     */
    public function readCsv ($_files = null) {
        $csv = array();
        $data = mb_convert_encoding(file_get_contents($_files['tmp_name']), "UTF-8", "SJIS-win");
        $fp = fopen('php://memory', 'r+');
        fwrite($fp, $data);
        rewind($fp);
        $current_locale = setlocale(LC_ALL, '0');
        setlocale(LC_ALL, 'ja_JP.UTF-8');
        while ($csv[] = fgetcsv($fp, 10000)) {}
        foreach($csv as $key => $val){
            if(isset($val[0])){$resdata[] = $val[0];}
        }
        setlocale(LC_ALL, $current_locale);
        fclose($fp);
        return $resdata;
    }

    /**
     * CSVによる変更登録結果
     */
    function reflect_with_csv_result ($mode = "") {
        if(count($this->request->data)){
            $this->Session->write('Retroact.result',$this->Session->read('Retroact.confirm'));
        }

        $retroact_session_data = $this->Session->read('Retroact.output');

        $retroact_data = array();
        foreach($retroact_session_data as $data){
            $current_data = current($data);
            $retroact_data[] = $current_data['retroact'];
        }

        $_success = true;
        $_render_page = 'reflect_with_csv_result_view';

        // 単価変更ロジック
        $_success = $this->__retroactLogic( $this->request->data['TrnRetroactHeader'] , $retroact_data);

        // エラー判定
        if($_success === false) {
            $this->Session->setFlash('遡及処理に失敗しました', 'growl', array('type'=>'error') );
            $this->redirect('reflect_with_csv');
            return;
            exit;
        } else {
            $this->set('count',$_success);
        }

        $this->set('retroact_type', $this->Session->read('Retroact.retroact_type'));
        $this->set('mode', $mode);
        $this->set('item_count',count($retroact_data));
        if($this->Session->check('Retroact')){
            $this->Session->delete('Retroact');
        }
        $this->render('reflect_with_csv_result_view');
    }


    /**
     * 遡及ロジック
     * Enter description here ...
     * @param unknown_type 遡及ヘッダに該当するデータ
     * @param unknown_type 遡及対象となる包装単位
     */
    private function __retroActLogic( $retroact_header , $retroact_record, $recode_check = false) {
        // トランザクション開始
        $_success = false;
        $this->TrnRetroactHeader->begin();

        // 処理
        $_item_unit_ids = array();
        $index_id_retroact_record = array();
        foreach( $retroact_record as $_retroact ) {
            if($recode_check === false || $_retroact['checked'] != 0 ){
                $_item_unit_ids[] = $_retroact['mst_item_unit_id'];
                $index_id_retroact_record[$_retroact['mst_item_unit_id']] = $_retroact;
            }
        }

        // 変更対象の請求データを取得
        $claim_datas = $this->__getClaimIds($retroact_header , $_item_unit_ids);

        if($claim_datas === false) {
            $this->TrnRetroactHeader->rollback();
            return false;
        }
        // 変更処理
        $_success = $this->__changePriceAndCreateRetroact($claim_datas,$retroact_header , $index_id_retroact_record);

        // トランザクション終了
        if(!$_success) {
            $this->TrnRetroactHeader->rollback();
        } else {
            $this->TrnRetroactHeader->commit();
        }
        return $_success;
    }

    private function __changePriceAndCreateRetroact($claim_datas, $retroact_header, $retroact_record){
        $master_table = $this->Session->read('Retroact.master_table');
        $facility_type = array();
        if($master_table['price_column'] == 'sales_price' ){
            $facility_type = Configure::read('FacilityType.hospital'); 
        }else{
            $facility_type = Configure::read('FacilityType.supplier');
        }

        // センターと仕入先or得意先の部署IDを取得
        $sql  = ' select ';
        $sql .= '      b.id                    as "ret__facility_id" ';
        $sql .= '    , c.id                    as "ret__department_id" ';
        $sql .= '  from ';
        $sql .= '    mst_facility_relations as a  ';
        $sql .= '    left join mst_facilities as b  ';
        $sql .= '      on b.id = a.partner_facility_id  ';
        $sql .= '    left join mst_departments as c  ';
        $sql .= '      on c.mst_facility_id = b.id  ';
        $sql .= '  where ';
        $sql .= "    b.facility_code = '" . $retroact_header['facility_to'] . "'";
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '    and b.facility_type = ' . $facility_type;

        $ret = $this->MstFacility->query($sql);

        $facility_to_department_id = $ret[0]['ret']['department_id'];
        $facility_to_id            = $ret[0]['ret']['facility_id'];
        $center_department_id      = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') , Configure::read('DepartmentType.warehouse'));
        // 遡及ヘッダ作成
        if($facility_to_id === '' || !($retroact_header_id = $this->__createRetroactHeader($retroact_header, $facility_to_id))){
            return false;
        }
        // 遡及データ作成
        if(($retroact_id = $this->__createRetroact( $retroact_record, $retroact_header_id, $facility_to_department_id, $center_department_id)) === false){
            return false;
        } else if(count($retroact_id) === 0 ){
            return 0;
        }

        // 請求データ&シール情報更新
        if(($this->__updateClaim($claim_datas, $retroact_record, $retroact_id)) === false){
            return false;
        }

        return count($retroact_id);
    }

    /**
     * 遡及ヘッダデータ作成
     * Enter description here ...
     * @param unknown_type $retroact_header
     */
    private function __createRetroactHeader( $retroact_header, $facility_to_id) {
        $workNos = Configure::read('workNoModels');
        $work_no = $this->setWorkNo4Header(date('Ymd'), array_search('TrnRetroactHeader', $workNos));
        $retroact_type = array_search('価格変更',Configure::read('Retroacts'));
        $retroact_header_insert = array('work_no'           => $work_no
                                        , 'work_date'       => date('Y-m-d')
                                        , 'work_type'       => $retroact_header['work_class']
                                        , 'recital'         => $retroact_header['recital']
                                        , 'retroact_type'   => $retroact_type
                                        , 'mst_facility_id' => $facility_to_id
                                        , 'start_date'      => $retroact_header['start_date']
                                        , 'end_date'        => $retroact_header['end_date']
                                        , 'is_deleted'      => FALSE
                                        , 'creater'         => $this->Session->read('Auth.MstUser.id')
                                        , 'modifier'        => $this->Session->read('Auth.MstUser.id')
                                        );
        $this->TrnRetroactHeader->create();
        $result = $this->TrnRetroactHeader->save($retroact_header_insert,false);
        if(!$result){
            return $result;
        } else {
            return array('id' => $this->TrnRetroactHeader->getLastInsertID(), 'work_no' => $work_no);
        }
    }

    /**
     * 遡及データ作成
     * Enter description here ...
     * @param unknown_type $retroact_header
     */
    private function __createRetroact($retroact_record, $retroact_header_id,$facility_to_department_id, $center_department_id) {
        $master_table = $this->Session->read('Retroact.master_table');
        // 部署と包装単位の組み合わせのユニークデータ
        if((int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_STOCK){
            $department_id_from = $facility_to_department_id;
            $department_id_to = $center_department_id;
        } elseif((int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_MSSTOCK){
            $department_id_from = $facility_to_department_id;
            $department_id_to = $center_department_id;
        } elseif((int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_SALES) {
            $department_id_from = $center_department_id;
            $department_id_to = $facility_to_department_id;
        }

        $retroact_type = array_search('価格変更',Configure::read('Retroacts'));
        $retroact_unique_deoartment_data = array();

        foreach($retroact_record as $retroact_data){
            $retroact_insert = array('work_no' => $retroact_header_id['work_no']
                                     , 'retroact_type'          => $retroact_type
                                     , 'department_id_from'     => $department_id_from
                                     , 'department_id_to'       => $department_id_to
                                     , 'mst_item_unit_id'       => $retroact_data['mst_item_unit_id']
                                     , 'retroact_price'         => $retroact_data['price']
                                     , 'trn_retroact_header_id' => $retroact_header_id['id']
                                     , 'is_deleted'             => false
                                     , 'creater'                => $this->Session->read('Auth.MstUser.id')
                                     , 'modifier'               => $this->Session->read('Auth.MstUser.id')
                                     , 'trn_close_header_id'    => $retroact_header_id['id']
                                     );

            $this->TrnRetroactRecord->create();
            if(!$this->TrnRetroactRecord->save($retroact_insert,false)){
                return false;
            } else {
                $retroact_unique_deoartment_data[$retroact_data['mst_item_unit_id']] = $this->TrnRetroactRecord->getLastInsertID();
            }
        }
        // 遡及ヘッダの明細数更新
        $header_update = array('id' => $retroact_header_id['id'],
                               'detail_count' => count($retroact_record),
                               'modifier' => $this->Session->read('Auth.MstUser.id')
                               );
        if(!$this->TrnRetroactHeader->save($header_update,false)){
            return false;
        }
        return $retroact_unique_deoartment_data;
    }

    /**
     * 請求データ更新
     * Enter description here ...
     * @param unknown_type $retroact_header
     */
    private function __updateClaim($claim_datas, $retroact_record, $retroact_id) {
        $now = date('Y/m/d H:i:s');

        $claim_update = array();
        $sticker_update = array();
        $sticker_update2 = array();
        foreach($claim_datas as $claim_data) {

            // 請求に入っているマスタ単価と、現在登録されてる仕入/売上マスタの価格が同じか確認する。
            if($retroact_record[$claim_data['TrnClaim']['mst_item_unit_id']]['price'] !== $claim_data['TrnClaim']['unit_price']){

                if(strtotime($claim_data['TrnClaim']['modified']) > strtotime($this->Session->read('Reflect.readTime'))){
                    return false;
                }
                // 金額成型
                $original_claim_price = $retroact_record[$claim_data['TrnClaim']['mst_item_unit_id']]['price'] * $claim_data['TrnClaim']['count'];
                $claim_price = $original_claim_price;
                if($claim_data['TrnClaim']['is_stock_or_sale'] == '1'){
                    $round = $claim_data['MstFacilityFrom']['round'];
                }else{
                    $round = $claim_data['MstFacilityTo']['round'];
                }
                switch($round){
                  case Configure::read('RoundType.Round'):
                    $claim_price = round($claim_price);
                    break;
                  case Configure::read('RoundType.Ceil'):
                    $claim_price = ceil($claim_price);
                    break;
                  case Configure::read('RoundType.Floor'):
                    $claim_price = floor($claim_price);
                    break;
                  case Configure::read('RoundType.Normal'):
                  default:
                    $claim_price = $claim_price;
                    break;
                }

                //返却・返品で作成されたマイナス請求はマイナスのまま価格変更
                //マイナス売上かどうかは請求に紐付く入荷明細の入荷区分で判断
                //入荷区分：2・返品、3・返却、4・入荷返品、5・入庫返品、6・預託品返品、7・組替、8・受領
                $minus_flg = false;
                if(($claim_data['TrnReceiving']['receiving_type'] > Configure::read('ReceivingType.arrival'))
                   &&
                   ($claim_data['TrnReceiving']['receiving_type'] < Configure::read('ReceivingType.receipt'))){
                    $claim_price = 0-$claim_price;
                    $minus_flg = true;
                }

                $claim_update[] = array('id'                       => $claim_data['TrnClaim']['id']
                                        , 'unit_price'             => $retroact_record[$claim_data['TrnClaim']['mst_item_unit_id']]['price']
                                        , 'claim_price'            => $claim_price
                                        , 'round'                  => $round
                                        , 'trn_retroact_record_id' => $retroact_id[$claim_data['TrnClaim']['mst_item_unit_id']]
                                        , 'is_deleted'             => false
                                        , 'modifire'               => $this->Session->read('Auth.MstUser.id')
                                        , 'modified'               => "'".$now."'",
                                        );


                $update_sticker = array();
                if($claim_data['TrnStickerReceiving']['id'] !== null
                   || $claim_data['TrnStickerShipping']['id'] !== null
                   ){
                    $sticker_table = $claim_data['TrnStickerReceiving']['id'] !== null ? 'TrnStickerReceiving' : 'TrnStickerShipping';

                    //仕入価格変更の場合は、入荷時のシールと入庫時に作成したシールの両方を価格変更対象とする
                    if($this->Session->read('Retroact.retroact_type') == $this->RETROACT_TYPE_STOCK || 
                       $this->Session->read('Retroact.retroact_type') == $this->RETROACT_TYPE_MSSTOCK){
                        $update_price = $claim_data[$sticker_table]['transaction_price'];
                        $update_coulmn = 'transaction_price';

                        //預託品以外の場合
                        if($claim_data['TrnStickerReceiving']['trade_type'] != Configure::read('ClassesType.Deposit')){
                            $sql  = ' SELECT ';
                            $sql .= '    "TrnSticker"."id" as "TrnSticker__id" ';
                            $sql .= ' FROM ';
                            $sql .= '   trn_stickers as "TrnSticker"';
                            $sql .= ' WHERE ';
                            $sql .= '   "TrnSticker".trn_receiving_id = '.$claim_data['TrnReceiving']['id'];
                            $sql .= '   AND ';
                            $sql .= '      "TrnSticker".is_deleted = FALSE';
                            $sql .= '   AND ';
                            $sql .= '      "TrnSticker".id != '.$claim_data['TrnStickerReceiving']['id'];//入荷時のシールは除く

                            $pre_data = $this->TrnSticker->query($sql);
                            foreach($pre_data as $data){
                                $update_sticker[] = $data['TrnSticker']['id'];
                            }
                            //仕入価格変更時用、入庫時に作成したシールのUPDATE文作成
                            foreach($update_sticker as $id){
                                if($update_price !== $original_claim_price){
                                    $sticker_update2[] = array('id'             => $id
                                                               , $update_coulmn => $retroact_record[$claim_data['TrnClaim']['mst_item_unit_id']]['price']
                                                               , 'is_deleted'   => false
                                                               , 'modifire'     => $this->Session->read('Auth.MstUser.id')
                                                               , 'modified'     => "'".$now."'",
                                                               );
                                }
                            }

                            //預託品の場合、1シールに1仕入請求が紐付くので別処理
                        } elseif($claim_data['TrnStickerReceiving']['trade_type'] == Configure::read('ClassesType.Deposit')
                                 && (!$minus_flg)) {
                            $sticker_update2[] = array('id'             => $claim_data[$sticker_table]['id']
                                                       , $update_coulmn => $claim_price
                                                       , 'is_deleted'   => false
                                                       , 'modifire'     => $this->Session->read('Auth.MstUser.id')
                                                       , 'modified'     => "'".$now."'",
                                                       );
                        }
                        //仕入価格変更入荷時のシール用UPADTE文（プラス仕入と紐付くシールのみ）
                        if(!$minus_flg){
                            $sticker_update = array('id'             => $claim_data[$sticker_table]['id']
                                                    , $update_coulmn => $claim_price
                                                    , 'is_deleted'   => false
                                                    , 'modifire'     => $this->Session->read('Auth.MstUser.id')
                                                    , 'modified'     => "'".$now."'",
                                                    );
                        }

                    } elseif( $this->Session->read('Retroact.retroact_type') == $this->RETROACT_TYPE_SALES) {
                        $update_price = $claim_data[$sticker_table]['sales_price'];
                        $update_coulmn = 'sales_price';

                        //売上価格変更入荷時のシール用UPADTE文（プラス売上と紐付くシールのみ）
                        if(!$minus_flg){
                            $sticker_update2[] = array('id'             => $claim_data[$sticker_table]['id']
                                                       , $update_coulmn => $claim_price
                                                       , 'is_deleted'   => false
                                                       , 'modifire'     => $this->Session->read('Auth.MstUser.id')
                                                       , 'modified'     => "'".$now."'",
                                                       );
                        }
                    }
                }
            }
        }

        if(!empty($claim_update) && !$this->TrnClaim->saveAll($claim_update , array('validates' => true,'atomic' => false))){
            return false;
        } else if(!empty($sticker_update) && !$this->TrnSticker->saveAll($sticker_update, array('validates' => true,'atomic' => false))){
            return false;
        }
        //入庫後のシール価格変更
        foreach($sticker_update2 as $update){
            if(!$this->TrnSticker->saveAll($update , array('validates' => true,'atomic' => false))){ return false; }
        }
        return true;
    }

    /**
     * 請求IDから変更対象のデータを取得
     */
    private function __getClaimIdsById( $claim_ids) {
        $options = $this->__getClaimSQLFrom();
        $options['conditions'] = array('TrnClaim.id' => $claim_ids);

        return $this->TrnClaim->find('all',$options);
    }

    /**
     * 変更対象の請求データを取得
     */
    private function __getClaimIds( $retroact_header , $item_units = array()) {
        $options = $this->__getClaimSQLFrom();
        $options['conditions'] = $this->__getClaimSQLWhere( $retroact_header , $item_units);

        return $this->TrnClaim->find('all',$options);
    }

    /**
     * 請求データ取得テーブル結合作成
     * @return array
     */
    private function __getClaimSQLFrom() {
        $options = array();

        $options['fields']=array('TrnClaim.id'
                                 ,'TrnClaim.mst_item_unit_id'
                                 ,'TrnClaim.department_id_from'
                                 ,'TrnClaim.department_id_to'
                                 ,'TrnClaim.unit_price'
                                 ,'TrnClaim.count'
                                 ,'TrnClaim.round'
                                 ,'TrnClaim.modified'
                                 ,'TrnClaim.is_stock_or_sale'
                                 ,'TrnStickerReceiving.id'
                                 ,'TrnStickerReceiving.transaction_price'
                                 ,'TrnStickerReceiving.sales_price'
                                 ,'TrnStickerReceiving.quantity'
                                 ,'TrnStickerReceiving.trade_type'
                                 ,'TrnStickerShipping.id'
                                 ,'TrnStickerShipping.transaction_price'
                                 ,'TrnStickerShipping.sales_price'
                                 ,'TrnStickerShipping.quantity'
                                 ,'TrnReceiving.id'
                                 ,'TrnReceiving.receiving_type'
                                 ,'MstFacilityFrom.round'
                                 ,'MstFacilityTo.round'
                                 );
        // joinテーブル
        $options['joins'] = array(
            array(
                'type' => 'LEFT',
                'alias' => 'MstdepartmentFrom',
                'table' => 'mst_departments',
                'conditions' => array('TrnClaim.department_id_from = MstdepartmentFrom.id',
                                      'MstdepartmentFrom.is_deleted != TRUE')
                ),
            array(
                'type' => 'LEFT',
                'alias' => 'MstFacilityFrom',
                'table' => 'mst_facilities',
                'conditions' => array('MstdepartmentFrom.mst_facility_id = MstFacilityFrom.id',
                                      'MstFacilityFrom.is_deleted != TRUE')
                ),
            array(
                'type' => 'LEFT',
                'alias' => 'MstDepartmentTo',
                'table' => 'mst_departments',
                'conditions' => array('TrnClaim.department_id_to = MstDepartmentTo.id',
                                      'MstDepartmentTo.is_deleted != TRUE')
                ),
            array(
                'type' => 'LEFT',
                'alias' => 'MstFacilityTo',
                'table' => 'mst_facilities',
                'conditions' => array('MstDepartmentTo.mst_facility_id = MstFacilityTo.id',
                                      'MstFacilityTo.is_deleted != TRUE')
                ),
            array(
                'type' => 'LEFT',
                'alias' => 'MstItemUnit',
                'table' => 'mst_item_units',
                'conditions' => array('TrnClaim.mst_item_unit_id = MstItemUnit.id',
                                      'MstItemUnit.is_deleted != TRUE')
                ),
            array(
                'type' => 'LEFT',
                'alias' => 'TrnReceiving',
                'table' => 'trn_receivings',
                'conditions' => array('TrnClaim.trn_receiving_id = TrnReceiving.id')
                ),
            array(
                'type' => 'LEFT',
                'alias' => 'TrnStickerReceiving',
                'table' => 'trn_stickers',
                'conditions' => array('TrnReceiving.trn_sticker_id = TrnStickerReceiving.id')
                ),
            array(
                'type' => 'LEFT',
                'alias' => 'TrnShipping',
                'table' => 'trn_shippings',
                'conditions' => array('TrnClaim.trn_shipping_id = TrnShipping.id')
                ),
            array(
                'type' => 'LEFT',
                'alias' => 'TrnStickerShipping',
                'table' => 'trn_stickers',
                'conditions' => array('TrnShipping.trn_sticker_id = TrnStickerShipping.id')
                ),
            );

        return $options;
    }

    /**
     * 請求データ取得条件作成
     *
     * @param array $retroact_header
     * @param array $item_units
     * @return array
     */
    private function __getClaimSQLWhere($retroact_header , $item_units = array()) {
        if($this->Session->read('Retroact.retroact_type') == $this->RETROACT_TYPE_STOCK || 
           $this->Session->read('Retroact.retroact_type') == $this->RETROACT_TYPE_MSSTOCK){
            //仕入価格変更
            $view_no = 72;
        }else{
            //売上価格変更
            $view_no = 73;
        }
        $sql  = ' select ';
        $sql .= '       a.updatable  ';
        $sql .= '   from ';
        $sql .= '     mst_privileges as a  ';
        $sql .= '     left join mst_users as b  ';
        $sql .= '       on b.mst_role_id = a.mst_role_id  ';
        $sql .= '   where ';
        $sql .= '     b.id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '     and a.view_no = '. $view_no;

        $ret = $this->MstUser->query($sql);
        if($ret[0][0]['updatable']){
            $_where =array('TrnClaim.is_stock_or_sale'      => (string)$this->Session->read('Retroact.retroact_type')
                           , 'TrnClaim.claim_date >= '      => $retroact_header['start_date']
                           , 'TrnClaim.claim_date <= '      => $retroact_header['end_date']
                           , 'TrnClaim.mst_item_unit_id'    => $item_units
                           , 'TrnClaim.is_not_retroactive'  => FALSE
                           , 'TrnClaim.is_deleted != '      => TRUE
                           );
        }else{
            $_where =array('TrnClaim.is_stock_or_sale'      => (string)$this->Session->read('Retroact.retroact_type')
                           , 'TrnClaim.claim_date >= '      => $retroact_header['start_date']
                           , 'TrnClaim.claim_date <= '      => $retroact_header['end_date']
                           , 'TrnClaim.mst_item_unit_id'    => $item_units
                           , 'TrnClaim.stocking_close_type' => 0
                           , 'TrnClaim.sales_close_type'    => 0
                           , 'TrnClaim.facility_close_type' => 0
                           , 'TrnClaim.is_not_retroactive'  => FALSE
                           , 'TrnClaim.is_deleted != '      => TRUE
                           );
        }
        if((int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_STOCK ||
           (int)$this->Session->read('Retroact.retroact_type') === (int)$this->RETROACT_TYPE_MSSTOCK ){
            $_where['MstFacilityFrom.facility_code'] = $retroact_header['facility_to_code'];
            $_where['MstFacilityTo.id'] = (string)$this->Session->read('Auth.facility_id_selected');
        } else {
            $_where['MstFacilityFrom.id'] = (string)$this->Session->read('Auth.facility_id_selected');
            $_where['MstFacilityTo.facility_code'] = $retroact_header['facility_to_code'];
        }
        return $_where;
    }

    /**
     * 設定情報取得
     *
     * @param array $form_data
     * @param boolean $only_check
     * @return array
     */
    private function __getConfigFromIds($form_data, $only_check = true) {
        $_ids = array();
        $master_table = $this->Session->read('Retroact.master_table');
        foreach($form_data['TrnRetroactRecord'] as $data ) {
            if(($only_check === false && isset($data['save_check']) && $data['save_check'] != 0) || $only_check === true){
                $_ids[] = $data[$master_table['table_name'].'_id'];
            }
        }

        $options = $this->__getSearchConfigSelectSQL($this->Session->read('Retroact.master_table'));
        // 条件文
        $_where = array($master_table['table_alias'].'.mst_facility_id' => $this->Session->read('Auth.facility_id_selected')
                        , $master_table['table_alias'].'.is_deleted !=' => TRUE
                        , 'MstFacilityPartner.facility_code' => $form_data['TrnRetroactHeader']['facility_to_code']
                        , 'MstFacilityOwner.facility_code' => $form_data['TrnRetroactHeader']['mst_facility_code']
                        );
        if( isset($_ids) && count($_ids) > 0 ) {
            $_where[$master_table['table_alias'].'.id']= $_ids;
        }
        $options['conditions'] = $_where;

        $_itemunits = $this->$master_table['table_alias']->find('all',$options);
        return $_itemunits;
    }


    /**
     * 設定情報取得条件作成
     *
     * @param array $search_conditions
     * @param array $master_table
     * @return array
     */
    private function __getSearchConfigSQL( $search_conditions,$master_table,$limit_flag = false) {

        $options = $this->__getSearchConfigSelectSQL($master_table);

        // 条件文
        $_where = array($master_table['table_alias'].'.mst_facility_id' => $this->Session->read('Auth.facility_id_selected')
                        , $master_table['table_alias'].'.is_deleted !=' => TRUE
                        , 'MstFacilityPartner.facility_code' => $search_conditions['TrnRetroactHeader']['facility_to_code']
                        , 'MstFacilityOwner.facility_code' => $search_conditions['TrnRetroactHeader']['mst_facility_code']
                        );

        // 商品ID
        if( isset($search_conditions['MstFacilityItem']['internal_code']) &&  $search_conditions['MstFacilityItem']['internal_code'] != '') {
            $_where['MstFacilityItem.internal_code'] =  pg_escape_string($search_conditions['MstFacilityItem']['internal_code']);
        }

        // 製品番号
        if( isset($search_conditions['MstFacilityItem']['item_code']) &&  $search_conditions['MstFacilityItem']['item_code'] != '') {
            $_where['MstFacilityItem.item_code'] =  pg_escape_string($search_conditions['MstFacilityItem']['item_code']);
        }

        // 商品名
        if( isset($search_conditions['MstFacilityItem']['item_name']) &&  $search_conditions['MstFacilityItem']['item_name'] != '') {
            $value = mb_convert_kana($search_conditions['MstFacilityItem']['item_name'],'k');
            $_where['MstFacilityItem.item_name like '] = "%" . pg_escape_string($value) . "%";
        }

        // 規格
        if( isset($search_conditions['MstFacilityItem']['standard']) &&  $search_conditions['MstFacilityItem']['standard'] != '') {
            $value = mb_convert_kana($search_conditions['MstFacilityItem']['standard'],'k');
            $_where['MstFacilityItem.standard like '] = "%" . pg_escape_string($value) . "%";
        }

        // 販売元
        if( isset($search_conditions['MstDealer']['dealer_name']) &&  $search_conditions['MstDealer']['dealer_name'] != '') {
            $value = mb_convert_kana($search_conditions['MstDealer']['dealer_name'],'k');
            $_where['MstDealer.dealer_name like '] =  '%' . pg_escape_string($value) . "%";
        }

        // JANコード
        if( isset($search_conditions['MstFacilityItem']['jan_code']) &&  $search_conditions['MstFacilityItem']['jan_code'] != '') {
            $_where['MstFacilityItem.jan_code like '] = "%" . pg_escape_string($search_conditions['MstFacilityItem']['jan_code']) . "%";
        }

        // 適用日
        if( isset($search_conditions[$master_table['table_alias']]['start_date']) &&  $search_conditions[$master_table['table_alias']]['start_date'] != '') {
            $_where[$master_table['table_alias'].'.start_date = '] = pg_escape_string($search_conditions[$master_table['table_alias']]['start_date']);
        }

        $options['conditions'] = $_where;
        if($limit_flag === true){
            $options['limit'] = $this->_getLimitCount();
        }

        return $options;
    }

    /**
     * 仕入or売上設定情報取得SELECT文作成
     *
     * @param array $master_table
     * @return array
     *
     */
    private function __getSearchConfigSelectSQL($master_table = array()) {
        $name = $master_table['table_alias'];

        $options = array();

        $options['fields']=array(  $name . '.' . 'id'
                                   ,$name . '.' . $master_table['price_column']
                                   ,$name . '.' . 'start_date'
                                   ,'MstUnitName.unit_name'
                                   ,'MstItemUnit.per_unit'
                                   ,'MstItemUnit.id'
                                   ,'MstPerUnitName.unit_name'
                                   ,'MstFacilityItem.internal_code'
                                   ,'MstFacilityItem.item_name'
                                   ,'MstFacilityItem.item_code'
                                   ,'MstFacilityItem.standard'
                                   ,'MstFacilityItem.mst_owner_id'
                                   ,'MstDealer.dealer_name'
                                   );
        // joinテーブル
        $options['joins'] = array(
            array(
                'type' => 'LEFT',
                'alias' => 'MstFacilityPartner',
                'table' => 'mst_facilities',
                'conditions' => array($name.'.partner_facility_id = MstFacilityPartner.id',
                                      'MstFacilityPartner.is_deleted != TRUE')
                ),
            array(
                'type' => 'LEFT',
                'alias' => 'MstItemUnit',
                'table' => 'mst_item_units',
                'conditions' => array($name.'.mst_item_unit_id = MstItemUnit.id',
                                      'MstItemUnit.is_deleted != TRUE')
                ),

            array(
                'type' => 'LEFT',
                'alias' => 'MstFacilityItem',
                'table' => 'mst_facility_items',
                'conditions' => array('MstItemUnit.mst_facility_item_id = MstFacilityItem.id',
                                      'MstFacilityItem.is_deleted != TRUE')
                ),

            array(
                'type' => 'LEFT',
                'alias' => 'MstFacilityOwner',
                'table' => 'mst_facilities',
                'conditions' => array('MstFacilityItem.mst_owner_id = MstFacilityOwner.id',
                                      'MstFacilityOwner.is_deleted != TRUE')
                ),

            array(
                'type' => 'LEFT',
                'alias' => 'MstUnitName',
                'table' => 'mst_unit_names',
                'conditions' => array('MstUnitName.id = MstItemUnit.mst_unit_name_id',
                                      'MstUnitName.is_deleted != TRUE')
                ),
            array(
                'type' => 'LEFT',
                'alias' => 'MstPerUnitName',
                'table' => 'mst_unit_names',
                'conditions' => array('MstPerUnitName.id = MstItemUnit.per_unit_name_id',
                                      'MstPerUnitName.is_deleted != TRUE')
                ),
            array(
                'type' => 'LEFT',
                'alias' => 'MstDealer',
                'table' => 'mst_dealers',
                'conditions' => array('MstDealer.id = MstFacilityItem.mst_dealer_id',
                                      'MstDealer.is_deleted != TRUE')
                ),
            );

        return $options;

    }

    /**
     * 仕入遡及
     * 初期画面
     */
    function stock(){
        $this->setRoleFunction(67); //仕入遡及

        $this->set('supplier_name_list' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('FacilityType.supplier')),
                                                         false,
                                                         array('facility_code' , 'facility_name'),
                                                         'facility_name, facility_code'
                                                         ));

        $this->set('owner_name_list' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                      array(Configure::read('FacilityType.donor')),
                                                      false
                                                      ));

        $work_no = Configure::read('workNoModels');
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),array_search('TrnRetroactHeader', $work_no)));

        $this->render('stock');
    }

    /**
     * 仕入遡及
     * マスター一括確認画面
     */
    function stock_with_master(){
        //施主コードから施設IDを取得
        $owner_id = $this->getFacilityId($data['Condition']['owner_code'] , Configure::read('FacilityType.donor') ,  $this->Session->read('Auth.facility_id_selected'));
        
        //使いまわし用に$this->request->dataへ格納
        $this->request->data['Condition']['owner_id'] = $owner_id;

        //仕入先コードから施設ID、部署IDを取得
        $res2 = $this->getSupplierId($this->request->data);
        //使いまわし用に$this->request->dataへ格納
        $this->request->data['Condition']['supplier_id']            = $res2[0]['MstFacility']['id'];
        $this->request->data['Condition']['supplier_department_id'] = $res2[0]['MstDepartment']['id'];

        //対象期間内の請求レコード件数取得
        $sql = '';
        $sql .= 'SELECT COUNT(TrnClaim.id) AS "TrnClaim__count"';
        $sql .= "  FROM trn_claims AS TrnClaim";
        $sql .= " INNER JOIN mst_departments AS MstDepartmentFrom";
        $sql .= "    ON TrnClaim.department_id_from = MstDepartmentFrom.id";
        $sql .= " INNER JOIN mst_facilities AS MstFacilityFrom";
        $sql .= "    ON MstDepartmentFrom.mst_facility_id = MstFacilityFrom.id";
        $sql .= "   AND MstFacilityFrom.id = ".$this->request->data['Condition']['supplier_id'];
        $sql .= " INNER JOIN mst_departments AS MstDepartmentTo";
        $sql .= "    ON TrnClaim.department_id_to = MstDepartmentTo.id";
        $sql .= " INNER JOIN mst_facilities AS MstFacilityTo";
        $sql .= "    ON MstDepartmentTo.mst_facility_id = MstFacilityTo.id";
        $sql .= "   AND MstFacilityTo.id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= " INNER JOIN mst_item_units AS MstItemUnit";
        $sql .= "    ON TrnClaim.mst_item_unit_id = MstItemUnit.id";
        $sql .= "   AND MstItemUnit.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= " INNER JOIN mst_facility_items AS MstFacilityItem";
        $sql .= "    ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id";
        $sql .= "   AND MstFacilityItem.mst_owner_id = ".$owner_id;
        $sql .= " WHERE TrnClaim.is_deleted = false";
        $sql .= "   AND TrnClaim.claim_date >= '".$this->request->data['Condition']['start_date']."'";
        $sql .= "   AND TrnClaim.claim_date <= '".$this->request->data['Condition']['end_date']."'";
        $sql .= "   AND TrnClaim.is_not_retroactive = false";
        $sql .= "   AND TrnClaim.is_stock_or_sale = '".Configure::read('Claim.stock')."'";
        $sql .= "   and ( TrnClaim.stocking_close_type = 2 or TrnClaim.facility_close_type = 2 )";

        $res3 = $this->TrnClaim->query($sql);

        $this->set('count',$res3[0]['TrnClaim']['count']);
        $this->set('data',$this->request->data);
        $this->render('stock_with_master');
    }

    /**
     * 仕入遡及
     * マスター一括完了画面
     */
    function stock_with_master_result() {
        //エラーメッセージ
        $error_msg = "";

        //計上日での締めチェック
        $registFlag = $this->checkCloseAuth($this->request->data,Configure::read("Claim.stock"));

        $claim_ids = array();

        if(!$registFlag){
            //計上日が締められているため再度条件設定してもらう
            $this->Session->setFlash("計上日はすでに締められています", 'growl', array('type'=>'error') );
            $this->stock();
            return;
        }else{
            /* 遡及データ作成 */
            //トランザクション開始
            $this->TrnRetroactHeader->begin();

            //赤黒明細作成用データ取得
            $sql = "";
            //取得項目
            $sql .= $this->getSQL_RetroactDataDetail(Configure::read("Claim.stock"));
            //結合テーブル
            $sql .= $this->getSQL_StockRetroactDataTable($this->request->data);
            $sql .= ' GROUP BY ';
            $sql .= '       "TrnClaim__department_id_from"';
            $sql .= '      ,"TrnClaim__department_id_to"';
            $sql .= '      ,"TrnClaim__mst_item_unit_id"';
            $sql .= '      ,"TrnClaim__unit_price"';
            $sql .= '      ,"TrnClaim__round"';
            $sql .= '      ,"MstConfig__config_price"';
            $sql .= '      ,"MstFacilityFrom__round"';
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';
            $res1 = $this->TrnClaim->query($sql);

            //各明細用データ取得
            $sql = '';
            $sql .= $this->getSQL_EachDataDetail(Configure::read("Claim.stock"));
            $sql .= $this->getSQL_StockRetroactDataTable($this->request->data);
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';
            $sql .= '         ,TrnClaim.id';
            $res2 = $this->TrnClaim->query($sql);

            //遡及対象データ0件の場合、設定からやり直してもらう
            if(count($res2) === 0){
                $this->Session->setFlash("遡及対象データがありません", 'growl', array('type'=>'error') );
                $this->stock();
                return;
            }

            /* 遡及対象の請求レコードを行ロック */
            foreach($res2 as $r){
                $claim_ids[] = $r['TrnClaim']['id'];
            }
            $this->TrnClaim->query('select * from trn_claims as a where a.id in ('.join(',',$claim_ids).') for update ');

            //遡及ヘッダ作成
            $header_key = Configure::read("workNoModels");
            $work_no = $this->setWorkNo4Header(date("Ymd"), array_search("TrnRetroactHeader", $header_key));
            $res3 = $this->crerateRetroactHeader($this->request->data,count($res1)*2+count($res2),$work_no,Configure::read("Claim.stock"));

            //遡及ヘッダid
            $TrnRetroactHeaderId = null;
            if($res3){
                //登録成功
                $TrnRetroactHeaderId = $this->TrnRetroactHeader->getLastInsertID();
            }else{
                //登録失敗
                $is_error_flag = true;
                $error_msg .= "遡及ヘッダ作成エラー\n";
            }

            /* 赤黒遡及明細と赤黒請求明細を作成 */
            $error_msg .= $this->createRetroactDataDetail($this->request->data,$res1,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.stock"));

            /* 各明細更新 */
            $error_msg .= $this->createEachDataDetail($this->request->data,$res2,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.stock"));
        }

        if(!empty($error_msg)){
            //エラーありのためロールック＆やり直し
            $this->TrnRetroactHeader->rollback();
            //画面遷移
            $this->Session->setFlash($error_msg, 'growl', array('type'=>'error') );
            $this->stock();
            return;
        }else{
            //コミット
            $this->TrnRetroactHeader->commit();

            $this->Session->setFlash("遡及が完了しました", 'growl', array('type'=>'star') );
        }
        $this->set('data',$this->request->data);
        $this->set('execution_count',count($res2));
        $this->render('stock_with_master_result');
    }

    /**
     * 仕入遡及
     * 商品選択画面
     */
    function stock_with_unit_search(){
        App::import('Sanitize');
        //施主コードから施設IDを取得
        $owner_id = $this->getFacilityId($data["Condition"]["owner_code"] , Configure::read('FacilityType.donor') ,  $this->Session->read('Auth.facility_id_selected'));
        //使いまわし用に$this->request->dataへ格納
        $this->request->data["Condition"]["owner_id"] = $owner_id;

        //仕入先コードから施設ID、部署IDを取得
        $result = $this->getSupplierId($this->request->data);
        //使いまわし用に$this->request->dataへ格納
        $this->request->data["Condition"]["supplier_id"] = $result[0]["MstFacility"]["id"];
        $this->request->data["Condition"]["supplier_department_id"] = $result[0]["MstDepartment"]["id"];

        //包装単位でグループ化された請求明細一覧を取得
        if(isset($this->request->data["Search"]["flag"]) && $this->request->data["Search"]["flag"] === "1"){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $sql = "";
            $sql = $this->getSQL_StockRetroactData_unit($this->request->data);
            /* 画面からの検索条件を追加 */
            //商品ID
            if(!empty($this->request->data["Search"]["internal_code"])){
                $sql .= " AND MstFacilityItem.internal_code ILIKE '%".$this->request->data["Search"]["internal_code"]."%'";
            }
            //商品名
            if(!empty($this->request->data["Search"]["item_name"])){
                $sql .= " AND MstFacilityItem.item_name ILIKE '%".$this->request->data["Search"]["item_name"]."%'";
            }
            //規格
            if(!empty($this->request->data["Search"]["standard"])){
                $sql .= " AND MstFacilityItem.standard ILIKE '%".$this->request->data["Search"]["standard"]."%'";
            }
            //製品番号
            if(!empty($this->request->data["Search"]["item_code"])){
                $sql .= " AND MstFacilityItem.item_code ILIKE '%".$this->request->data["Search"]["item_code"]."%'";
            }
            //販売元名
            if(!empty($this->request->data["Search"]["dealer_name"])){
                $sql .= " AND MstDealer.dealer_name ILIKE '%".$this->request->data["Search"]["dealer_name"]."%'";
            }
            //JANコード
            if(!empty($this->request->data["Search"]["jan_code"])){
                $sql .= " AND MstFacilityItem.jan_code = '".$this->request->data["Search"]["jan_code"]."'";
            }
            //適用開始日
            if(!empty($this->request->data["Search"]["start_date"])){
                $sql .= " AND MstTransactionConfig.start_date = '".$this->request->data["Search"]["start_date"]."'";
            }

            if(!empty($this->request->data["Search"]["selectedItemId"])){
                $ids = substr($this->request->data["Search"]["selectedItemId"],0,-1);
                $idList = explode(",", $ids);
                $sql .= " AND MstTransactionConfig.id not in ( ".join(',',$idList)." ) ";
            }

            $sql .= ' GROUP BY ';
            $sql .= '       TrnClaim.department_id_from';
            $sql .= '      ,TrnClaim.department_id_to';
            $sql .= '      ,TrnClaim.mst_item_unit_id';
            $sql .= '      ,"MstFacilityItem__internal_code"';
            $sql .= '      ,"MstFacilityItem__item_name"';
            $sql .= '      ,"MstFacilityItem__standard"';
            $sql .= '      ,"MstFacilityItem__item_code"';
            $sql .= '      ,"MstDealer__dealer_name"';
            $sql .= '      ,"MstTransactionConfig__id"';
            $sql .= '      ,"MstTransactionConfig__start_date"';
            $sql .= '      ,"MstConfig__config_price"';
            $sql .= '      ,"MstUnitName__unit_name"';
            $sql .= ' ORDER BY ';
            $sql .= '       "MstFacilityItem__internal_code"';
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnClaim'));
            //リミット
            if(empty($this->request->data["limit"])){
                $sql .= " LIMIT " . min(array_keys(Configure::read('displaycounts_combobox')));
            }else{
                $sql .= " LIMIT ".$this->request->data["limit"];
            }
            $SearchResult = $this->TrnClaim->query($sql);
            $this->set("SearchResult",$SearchResult);

            //カートの内容を取得
            if(!empty($this->request->data["Search"]["selectedItemId"])){
                $CartResult = array();
                //TODO 選択順番保持のため、一件ずつ取得する(遅い場合はINへ変更予定)
                foreach($idList as $id){
                    $sql = '';
                    $sql .= $this->getSQL_StockRetroactData_unit($this->request->data);
                    $sql .= ' AND MstTransactionConfig.id = '.$id;
                    $sql .= ' GROUP BY ';
                    $sql .= '       TrnClaim.department_id_from';
                    $sql .= '      ,TrnClaim.department_id_to';
                    $sql .= '      ,TrnClaim.mst_item_unit_id';
                    $sql .= '      ,"MstFacilityItem__internal_code"';
                    $sql .= '      ,"MstFacilityItem__item_name"';
                    $sql .= '      ,"MstFacilityItem__standard"';
                    $sql .= '      ,"MstFacilityItem__item_code"';
                    $sql .= '      ,"MstDealer__dealer_name"';
                    $sql .= '      ,"MstTransactionConfig__id"';
                    $sql .= '      ,"MstTransactionConfig__start_date"';
                    $sql .= '      ,"MstConfig__config_price"';
                    $sql .= '      ,"MstUnitName__unit_name"';
                    $sql .= ' ORDER BY ';
                    $sql .= '       "MstFacilityItem__internal_code"';

                    $res = $this->TrnClaim->query($sql);
                    $CartResult[] = $res[0];
                }
                //画面表示用に設定
                $this->set("CartResult",$CartResult);
            }
            $this->request->data = $data;

        }
        $this->set("data",$this->request->data);
        $this->render('stock_with_unit_search');
    }

    /**
     * 仕入遡及
     * 商品確認画面
     */
    function stock_with_unit_confirm() {
        //選択内容を取得
        foreach($this->request->data["Confirm"] as $id){
            $sql = '';
            $sql .= $this->getSQL_StockRetroactData_unit($this->request->data);
            $sql .= ' AND MstTransactionConfig.id = '.$id;
            $sql .= ' GROUP BY ';
            $sql .= '       TrnClaim.department_id_from';
            $sql .= '      ,TrnClaim.department_id_to';
            $sql .= '      ,TrnClaim.mst_item_unit_id';
            $sql .= '      ,"MstFacilityItem__internal_code"';
            $sql .= '      ,"MstFacilityItem__item_name"';
            $sql .= '      ,"MstFacilityItem__standard"';
            $sql .= '      ,"MstFacilityItem__item_code"';
            $sql .= '      ,"MstDealer__dealer_name"';
            $sql .= '      ,"MstTransactionConfig__id"';
            $sql .= '      ,"MstTransactionConfig__start_date"';
            $sql .= '      ,"MstConfig__config_price"';
            $sql .= '      ,"MstUnitName__unit_name"';
            $sql .= ' ORDER BY ';
            $sql .= '       "MstFacilityItem__internal_code"';

            $res = $this->TrnClaim->query($sql);
            $result[] = $res[0];
        }
        //画面表示用に設定
        $this->set("result",$result);
        $this->set("data",$this->request->data);
        $this->render('stock_with_unit_confirm');
    }

    /**
     * 仕入遡及
     * 商品選択完了画面
     */
    function stock_with_unit_result(){
        //エラーメッセージ
        $error_msg = "";
        //画面から渡されたid
        $ids = implode(",",$this->request->data["Confirm"]);
        //終了画面表示データ
        $result = array();
        $claim_ids = array();
        //計上日での締めチェック
        $registFlag = $this->checkCloseAuth($this->request->data,Configure::read("Claim.stock"));
        if(!$registFlag){
            //計上日が締められているため再度条件設定してもらう
            $this->Session->setFlash("計上日はすでに締められています", 'growl', array('type'=>'error') );
            $this->stock();
            return;
        }else{
            //トランザクション開始
            $this->TrnRetroactHeader->begin();

            //正常終了時に表示するデータを先に取得しておく
            foreach($this->request->data["Confirm"] as $id){
                $sql = '';
                $sql .= $this->getSQL_StockRetroactData_unit($this->request->data);
                $sql .= ' AND MstTransactionConfig.id = '.$id;
                $sql .= ' GROUP BY ';
                $sql .= '       TrnClaim.department_id_from';
                $sql .= '      ,TrnClaim.department_id_to';
                $sql .= '      ,TrnClaim.mst_item_unit_id';
                $sql .= '      ,"MstFacilityItem__internal_code"';
                $sql .= '      ,"MstFacilityItem__item_name"';
                $sql .= '      ,"MstFacilityItem__standard"';
                $sql .= '      ,"MstFacilityItem__item_code"';
                $sql .= '      ,"MstDealer__dealer_name"';
                $sql .= '      ,"MstTransactionConfig__id"';
                $sql .= '      ,"MstTransactionConfig__start_date"';
                $sql .= '      ,"MstConfig__config_price"';
                $sql .= '      ,"MstUnitName__unit_name"';
                $sql .= ' ORDER BY ';
                $sql .= '       "MstFacilityItem__internal_code"';
                $res = $this->TrnClaim->query($sql);
                $result[] = $res[0];
            }

            /* 赤黒明細作成用データ取得 */
            $sql = "";
            //取得項目
            $sql .= $this->getSQL_RetroactDataDetail(Configure::read("Claim.stock"));
            //結合テーブル
            $sql .= $this->getSQL_StockRetroactDataTable($this->request->data);
            //各種条件
            $sql .= ' AND MstTransactionConfig.id IN ('.$ids.')';
            $sql .= ' GROUP BY ';
            $sql .= '       "TrnClaim__department_id_from"';
            $sql .= '      ,"TrnClaim__department_id_to"';
            $sql .= '      ,"TrnClaim__mst_item_unit_id"';
            $sql .= '      ,"TrnClaim__unit_price"';
            $sql .= '      ,"TrnClaim__round"';
            $sql .= '      ,"MstConfig__config_price"';
            $sql .= '      ,"MstFacilityFrom__round"';
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';
            $res1 = $this->TrnClaim->query($sql);

            //各明細用データ取得
            $sql = '';
            $sql .= $this->getSQL_EachDataDetail(Configure::read("Claim.stock"));
            $sql .= $this->getSQL_StockRetroactDataTable($this->request->data);
            $sql .= ' AND MstTransactionConfig.id IN ('.$ids.')';
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';
            $sql .= '         ,TrnClaim.id';
            $res2 = $this->TrnClaim->query($sql);

            /* 遡及対象の請求レコードを行ロック */
            foreach($res2 as $r){
                $claim_ids[] = $r['TrnClaim']['id'];
            }
            $this->TrnClaim->query('select * from trn_claims as a where a.id in ('.join(',',$claim_ids).') for update ');


            //遡及ヘッダ作成
            $header_key = Configure::read("workNoModels");
            $work_no = $this->setWorkNo4Header(date("Ymd"), array_search("TrnRetroactHeader", $header_key));
            $res3 = $this->crerateRetroactHeader($this->request->data,count($res1)*2+count($res2),$work_no,Configure::read("Claim.stock"));

            //遡及ヘッダid
            $TrnRetroactHeaderId = null;
            if($res3){
                //登録成功
                $TrnRetroactHeaderId = $this->TrnRetroactHeader->getLastInsertID();
            }else{
                //登録失敗
                $is_error_flag = true;
                $error_msg .= "遡及ヘッダ作成エラー\n";
            }

            //赤黒遡及データ作成
            $error_msg .= $this->createRetroactDataDetail($this->request->data,$res1,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.stock"));

            //各テーブル更新＆遡及明細作成
            $error_msg .= $this->createEachDataDetail($this->request->data,$res2,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.stock"));
        }

        if(!empty($error_msg)){
            //エラーありのためロールック＆やり直し
            $this->TrnRetroactHeader->rollback();
            //画面遷移
            $this->Session->setFlash($error_msg, 'growl', array('type'=>'error') );
            $this->stock();
            return;
        }else{
            //コミット
            $this->TrnRetroactHeader->commit();

            //画面表示用に設定
            $this->set("result",$result);
            $this->Session->setFlash("遡及が完了しました", "growl", array("type"=>"star") );
        }
        $this->set("data",$this->request->data);
        $this->render('stock_with_unit_result');
    }

    /**
     * 仕入遡及
     * CSV入力画面
     */
    function stock_with_csv() {
        //施主コードから施設IDを取得
        $owner_id = $this->getFacilityId($data["Condition"]["owner_code"] , Configure::read('FacilityType.donor') ,  $this->Session->read('Auth.facility_id_selected'));
        //使いまわし用に$this->request->dataへ格納
        $this->request->data["Condition"]["owner_id"] = $owner_id;

        //仕入先コードから施設ID、部署IDを取得
        $result = $this->getSupplierId($this->request->data);
        //使いまわし用に$this->request->dataへ格納
        $this->request->data["Condition"]["supplier_id"] = $result[0]["MstFacility"]["id"];
        $this->request->data["Condition"]["supplier_department_id"] = $result[0]["MstDepartment"]["id"];

        $this->set("data",$this->request->data);
        $this->render('stock_with_csv');
    }

    /**
     * 仕入遡及
     * CSV確認画面
     */
    function stock_with_csv_confirm(){
        //エラーメッセージ
        $error_msg = array();
        //対象請求明細IDリスト
        $idList = array();
        //センター部署ID取得
        $sql = "";
        $sql .= "SELECT id";
        $sql .= "  FROM mst_departments";
        $sql .= " WHERE mst_facility_id = ".$this->Session->read("Auth.facility_id_selected");
        $sql .= "   AND is_deleted = false";

        $res = $this->MstDepartment->query($sql);
        $center_department_id = $res[0][0]["id"];

        //送信ファイルチェック
        if(is_uploaded_file($this->request->data['Condition']['csv_file']['tmp_name'])){
            $this->CsvReadUtils->setFileName( $this->request->data['Condition']['csv_file']['tmp_name'] );
        }else{
            $this->Session->setFlash("CSVファイルが不正です", 'growl', array('type'=>'error') );
            $this->stock();
            return;
        }

        // CsvReadUtils初期設定
        $this->CsvReadUtils->setLineMaxLength( 2000 );

        // CSVオープン
        $this->CsvReadUtils->open();

        // ハッシュキー設定
        $this->CsvReadUtils->hashKeyClear();
        $this->CsvReadUtils->hashKeyAdd( "internal_code" );
        $this->CsvReadUtils->hashKeyAdd( "per_unit" );
        $this->CsvReadUtils->hashKeyAdd( "start_date" );
        $this->CsvReadUtils->hashKeyAdd( "center_code" );
        $this->CsvReadUtils->hashKeyAdd( "facility_code" );

        //1行目を取得
        $header_data = $this->CsvReadUtils->readCsvLine();
        if($header_data != array('internal_code' => '商品ID',
                                 'per_unit'      => '入り数',
                                 'start_date'    => '適用開始日',
                                 'center_code'   => '商品施設コード',
                                 'facility_code' => '仕入先施設コード'
                                 )){
            $this->Session->setFlash('CSVファイルが不正です。ヘッダ行をご確認ください。', 'growl', array('type'=>'error') );
            $this->stock();
            return;
        }

        $i = 0;
        $tempDate = '';
        $idList = array();

        while ( ! $this->CsvReadUtils->eof() ) {
            // CSV一行読込
            $line_data = $this->CsvReadUtils->readCsvLine();

            // EOFだけの行を読み込んでしまうため
            if ( $line_data == "" ) {
                continue;
            }

            $i++; //CSV件数カウント（エラー分含む）
            if(count($line_data) !== 5){
                //項目数チェック不正
                $this->Session->setFlash("CSVファイルが不正です", 'growl', array('type'=>'error') );
                $this->stock();
                return;
            }

            //商品ID存在チェック
            $fitem_info = $this->MstFacilityItem->find('first',array(
                'fields'     => 'id',
                'recursive'  => -1,
                'conditions' => array(
                    'internal_code'   => $line_data['internal_code'],
                    'item_type'       => array(
                        Configure::read('Items.item_types.normalitem') ,
                        Configure::read('Items.item_types.setitem')
                        ),
                    'mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
                    )
                ));
            if(empty($fitem_info)){
                $error_msg[] = $i."件目：商品IDが存在しません";
                continue;
            }

            //適用開始日フォーマットチェック
            $date = explode("/",$line_data['start_date']);
            if(!checkdate($date[1],$date[2],$date[0])){
                $error_msg[] = $i."件目：適用開始日はYYYY/MM/DDの形式で入力してください";
                continue;
            }


            //CSV内で複数適用開始日は不許可にする
            if($i === 1){
                $tempDate = $line_data['start_date'];
            }else{
                if($tempDate !== $line_data['start_date']){
                    $error_msg[] = $i."件目：適用開始日は全て同じ日付けにしてください";
                    continue;
                }
            }

            //包装単位ID存在チェック
            $sql  = 'SELECT mst_item_unit_id';
            $sql .= '  FROM mst_transaction_configs A';
            $sql .= ' INNER JOIN mst_item_units B';
            $sql .= '    ON A.mst_item_unit_id = B.id';
            $sql .= '   AND B.per_unit = '.$line_data['per_unit'];
            $sql .= '   AND B.mst_facility_item_id = '.$fitem_info['MstFacilityItem']['id'];
            $sql .= '   AND B.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
            $sql .= " WHERE A.start_date = '".$line_data['start_date'] . "'";
            $sql .= '   AND A.mst_facility_item_id = '.$fitem_info['MstFacilityItem']['id'];
            $sql .= '   AND A.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
            $sql .= '   AND A.partner_facility_id = '.$this->request->data['Condition']['supplier_id'];
            $sql .= '   AND A.is_deleted = false';
            $itemunit_info = $this->MstTransactionConfig->query($sql);
            if(empty($itemunit_info)){
                $error_msg[] = $i."件目：包装単位が存在しません";
                continue;
            }

            //施設コード存在チェック
            $facility_info = $this->MstFacility->find('all',array(
                'conditions' => array(
                    'facility_type'   => Configure::read('FacilityType.center'),
                    'facility_code'   => $line_data['center_code'],
                    'id'              => $this->Session->read('Auth.facility_id_selected')
                    ),
                'recursive' => -1
                ));

            if(empty($facility_info)){
                $error_msg[] = $i."件目：商品施設コードが存在しません";
                continue;
            }

            //仕入先コードチェック
            if($this->request->data['Condition']['supplier_code'] != $line_data['facility_code']){
                $error_msg[] = $i."件目：施設コードが存在しません";
                continue;
            }

            //エラーなしの場合該当の請求明細を検索
            if(count($error_msg) === 0){
                $sql  = 'SELECT TrnClaim.id AS "TrnClaim__id"';
                $sql .= '  FROM trn_claims AS TrnClaim';
                $sql .= ' INNER JOIN mst_item_units AS MstItemUnit';
                $sql .= '    ON TrnClaim.mst_item_unit_id = MstItemUnit.id';
                $sql .= '   AND MstItemUnit.id = '.$itemunit_info[0][0]['mst_item_unit_id'];
                $sql .= ' WHERE TrnClaim.department_id_from = '.$this->request->data['Condition']['supplier_department_id'];
                $sql .= '   AND TrnClaim.department_id_to = '.$center_department_id;
                $sql .= "   AND TrnClaim.is_deleted = false";
                $sql .= "   AND TrnClaim.claim_date >= '".$this->request->data['Condition']['start_date']."'";
                $sql .= "   AND TrnClaim.claim_date <= '".$this->request->data['Condition']['end_date']."'";
                $sql .= "   AND TrnClaim.is_not_retroactive = false";
                $sql .= "   AND TrnClaim.is_stock_or_sale = '".Configure::read('Claim.stock')."'";
                $sql .= "   and ( TrnClaim.stocking_close_type = 2 or TrnClaim.facility_close_type = 2 )";

                $this->TrnClaim->create();
                $result = $this->TrnClaim->query($sql);

                if(empty($result) or count($result) === 0){
                    //取得データなし
                    $error_msg[] = $i."件目：対象データが存在しません";
                    continue;
                }else{
                    foreach($result as $value){
                        if(in_array($value['TrnClaim']['id'], $idList)){
                            $error_msg[] = $i."件目：対象データが重複しています";
                                break;
                        }else{
                            $idList[] = $value['TrnClaim']['id'];
                        }
                    }
                }
            }
        }

        //取得した請求明細IDリストを文字列へ
        $ids = implode(',', $idList);

        $this->set('ids',$ids);
        $this->set('read_count',$i);//ヘッダ件数除く
        $this->set('regist_count',count($idList));
        $this->set('stocking_start_date',$tempDate);
        $this->set('error_msg',$error_msg);

        $this->set('data',$this->request->data);
        $this->render('stock_with_csv_confirm');
    }

    /**
     * 仕入遡及
     * CSV完了画面
     */
    function stock_with_csv_result() {
        //エラーメッセージ
        $error_msg = "";
        $claim_ids = array();
        //計上日での締めチェック
        $registFlag = $this->checkCloseAuth($this->request->data,Configure::read("Claim.stock"));
        if(!$registFlag){
            //計上日が締められているため再度条件設定してもらう
            $this->Session->setFlash("計上日はすでに締められています", 'growl', array('type'=>'error') );
            $this->stock();
            return;
        }else{
            //トランザクション開始
            $this->TrnRetroactHeader->begin();

            /* 赤黒明細作成用データ取得 */
            $sql = "";
            //取得項目
            $sql .= $this->getSQL_RetroactDataDetail(Configure::read("Claim.stock"));
            //結合テーブル
            $sql .= $this->getSQL_StockRetroactDataTable($this->request->data);
            //各種条件
            $sql .= ' AND TrnClaim.id IN ('.$this->request->data["Condition"]["trn_claims_id"].')';
            $sql .= ' GROUP BY ';
            $sql .= '       "TrnClaim__department_id_from"';
            $sql .= '      ,"TrnClaim__department_id_to"';
            $sql .= '      ,"TrnClaim__mst_item_unit_id"';
            $sql .= '      ,"TrnClaim__unit_price"';
            $sql .= '      ,"TrnClaim__round"';
            $sql .= '      ,"MstConfig__config_price"';
            $sql .= '      ,"MstFacilityFrom__round"';
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';
            $res1 = $this->TrnClaim->query($sql);

            //各明細用データ取得
            $sql = '';
            $sql .= $this->getSQL_EachDataDetail(Configure::read("Claim.stock"));
            $sql .= $this->getSQL_StockRetroactDataTable($this->request->data);
            $sql .= ' AND TrnClaim.id IN ('.$this->request->data["Condition"]["trn_claims_id"].')';
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';
            $sql .= '         ,TrnClaim.id';
            $res2 = $this->TrnClaim->query($sql);

            /* 遡及対象の請求レコードを行ロック */
            foreach($res2 as $r){
                $claim_ids[] = $r['TrnClaim']['id'];
            }
            $this->TrnClaim->query('select * from trn_claims as a where a.id in ('.join(',',$claim_ids).') for update ');


            //遡及ヘッダ作成
            $header_key = Configure::read("workNoModels");
            $work_no = $this->setWorkNo4Header(date("Ymd"), array_search("TrnRetroactHeader", $header_key));
            $res3 = $this->crerateRetroactHeader($this->request->data,count($res1)*2+count($res2),$work_no,Configure::read("Claim.stock"));

            //遡及ヘッダid
            $TrnRetroactHeaderId = null;
            if($res3){
                //登録成功
                $TrnRetroactHeaderId = $this->TrnRetroactHeader->getLastInsertID();
            }else{
                //登録失敗
                $is_error_flag = true;
                $error_msg .= "遡及ヘッダ作成エラー\n";
            }

            //赤黒遡及データ作成
            $error_msg .= $this->createRetroactDataDetail($this->request->data,$res1,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.stock"));

            //各テーブル更新＆遡及明細作成
            $error_msg .= $this->createEachDataDetail($this->request->data,$res2,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.stock"));
        }

        if(!empty($error_msg)){
            //エラーありのためロールック＆やり直し
            $this->TrnRetroactHeader->rollback();
            //画面遷移
            $this->Session->setFlash($error_msg, 'growl', array('type'=>'error') );
            $this->stock();
            return;
        }else{
            //コミット
            $this->TrnRetroactHeader->commit();
            $tempList = explode(",",$this->request->data["Condition"]["trn_claims_id"]);

            //画面表示用に設定
            $this->set("read_count",$this->request->data["Condition"]["read_count"]);
            $this->set("regist_count",count($tempList));
            $this->Session->setFlash("遡及が完了しました", "growl", array("type"=>"star") );
        }
        $this->set("data",$this->request->data);
        $this->render('stock_with_csv_result');
    }

    /**
     * 売上遡及
     * 初期画面表示
     */
    function sales(){
        $this->setRoleFunction(68); //売上遡及
        $this->set('hospital_name_list' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('FacilityType.hospital')),
                                                         false
                                                         ));

        $this->set('owner_name_list' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                      array(Configure::read('FacilityType.donor')),
                                                      false
                                                      ));

        $work_no = Configure::read('workNoModels');
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),array_search('TrnRetroactHeader', $work_no)));
        $this->render('sales');
    }

    /**
     * 売上遡及画面
     * マスター一括遡及確認画面
     */
    function sales_with_master() {
        //施主コードから施設IDを取得
        $owner_id = $this->getFacilityId($data["Condition"]["owner_code"] , Configure::read('FacilityType.donor') ,  $this->Session->read('Auth.facility_id_selected'));
        //使いまわし用に$this->request->dataへ格納
        $this->request->data["Condition"]["owner_id"] = $owner_id;

        //得意先先コードから施設ID、部署IDを取得
        $res2 = $this->getHospitalId($this->request->data);
        //使いまわし用に$this->request->dataへ格納
        $this->request->data["Condition"]["hospital_id"] = $res2[0]["MstFacility"]["id"];

        //対象期間内の請求レコード件数取得
        $sql = '';
        $sql .= 'SELECT COUNT(TrnClaim.id) AS "TrnClaim__count"';
        $sql .= "  FROM trn_claims AS TrnClaim";
        $sql .= " INNER JOIN mst_departments AS MstDepartmentFrom";
        $sql .= "    ON TrnClaim.department_id_from = MstDepartmentFrom.id";
        $sql .= " INNER JOIN mst_facilities AS MstFacilityFrom";
        $sql .= "    ON MstDepartmentFrom.mst_facility_id = MstFacilityFrom.id";
        $sql .= "   AND MstFacilityFrom.id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= " INNER JOIN mst_departments AS MstDepartmentTo";
        $sql .= "    ON TrnClaim.department_id_to = MstDepartmentTo.id";
        $sql .= " INNER JOIN mst_facilities AS MstFacilityTo";
        $sql .= "    ON MstDepartmentTo.mst_facility_id = MstFacilityTo.id";
        $sql .= "   AND MstFacilityTo.id = ".$this->request->data["Condition"]["hospital_id"];
        $sql .= " INNER JOIN mst_item_units AS MstItemUnit";
        $sql .= "    ON TrnClaim.mst_item_unit_id = MstItemUnit.id";
        $sql .= "   AND MstItemUnit.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= " INNER JOIN mst_facility_items AS MstFacilityItem";
        $sql .= "    ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id";
        $sql .= "   AND MstFacilityItem.mst_owner_id = ".$owner_id;
        $sql .= " WHERE TrnClaim.is_deleted = false";
        $sql .= "   AND TrnClaim.claim_date >= '".$this->request->data["Condition"]["start_date"]."'";
        $sql .= "   AND TrnClaim.claim_date <= '".$this->request->data["Condition"]["end_date"]."'";
        $sql .= "   AND TrnClaim.is_not_retroactive = false";
        $sql .= "   AND TrnClaim.is_stock_or_sale = '".Configure::read('Claim.sales')."'";
        $sql .= "   and ( TrnClaim.sales_close_type = 2 or TrnClaim.facility_close_type = 2 )";

        $res3 = $this->TrnClaim->query($sql);

        $this->set("count",$res3[0]["TrnClaim"]["count"]);
        $this->set("data",$this->request->data);
        $this->render('sales_with_master');
    }

    /**
     * 売上遡及画面
     * マスター一括遡及完了画面
     */
    function sales_with_master_result() {
        //エラーメッセージ
        $error_msg = "";
        $claim_ids = array();

        //計上日での締めチェック
        $registFlag = $this->checkCloseAuth($this->request->data,Configure::read("Claim.sales"));

        if(!$registFlag){
            //計上日が締められているため再度条件設定してもらう
            $this->Session->setFlash("計上日はすでに締められています", 'growl', array('type'=>'error') );
            $this->sales();
            return;
        }else{
            /* 遡及データ作成 */
            //トランザクション開始
            $this->TrnRetroactHeader->begin();

            //赤黒明細作成用データ取得
            $sql = "";
            //取得項目
            $sql .= $this->getSQL_RetroactDataDetail(Configure::read("Claim.sales"));
            //結合テーブル
            $sql .= $this->getSQL_SalesRetroactDataTable($this->request->data);
            $sql .= ' GROUP BY ';
            $sql .= '       "TrnClaim__department_id_from"';
            $sql .= '      ,"TrnClaim__department_id_to"';
            $sql .= '      ,"TrnClaim__mst_item_unit_id"';
            $sql .= '      ,"TrnClaim__unit_price"';
            $sql .= '      ,"TrnClaim__round"';
            $sql .= '      ,"MstConfig__config_price"';
            $sql .= '      ,"MstFacilityFrom__round"';
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';

            $res1 = $this->TrnClaim->query($sql);

            //各明細用データ取得
            $sql = '';
            $sql .= $this->getSQL_EachDataDetail(Configure::read("Claim.sales"));
            $sql .= $this->getSQL_SalesRetroactDataTable($this->request->data);
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';
            $sql .= '         ,TrnClaim.id';
            $res2 = $this->TrnClaim->query($sql);

            //遡及対象データ0件の場合、設定からやり直してもらう
            if(count($res2) === 0){
                $this->Session->setFlash("遡及対象データがありません", 'growl', array('type'=>'error') );
                $this->sales();
                return;
            }

            /* 遡及対象の請求レコードを行ロック */
            foreach($res2 as $r){
                $claim_ids[] = $r['TrnClaim']['id'];
            }
            $this->TrnClaim->query('select * from trn_claims as a where a.id in ('.join(',',$claim_ids).') for update ');

            //遡及ヘッダ作成
            $header_key = Configure::read("workNoModels");
            $work_no = $this->setWorkNo4Header(date("Ymd"), array_search("TrnRetroactHeader", $header_key));
            $res3 = $this->crerateRetroactHeader($this->request->data,count($res1)*2+count($res2),$work_no,Configure::read("Claim.sales"));

            //遡及ヘッダid
            $TrnRetroactHeaderId = null;
            if($res3){
                //登録成功
                $TrnRetroactHeaderId = $this->TrnRetroactHeader->getLastInsertID();
            }else{
                //登録失敗
                $is_error_flag = true;
                $error_msg .= "遡及ヘッダ作成エラー\n";
            }

            /* 赤黒遡及明細と赤黒請求明細を作成 */
            $error_msg .= $this->createRetroactDataDetail($this->request->data,$res1,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.sales"));

            /* 各明細更新 */
            $error_msg .= $this->createEachDataDetail($this->request->data,$res2,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.sales"));
        }

        if(!empty($error_msg)){
            //エラーありのためロールック＆やり直し
            $this->TrnRetroactHeader->rollback();
            //画面遷移
            $this->Session->setFlash($error_msg, 'growl', array('type'=>'error') );
            $this->sales();
            return;
        }else{
            //コミット
            $this->TrnRetroactHeader->commit();

            $this->Session->setFlash("遡及が完了しました", 'growl', array('type'=>'star') );
        }
        $this->set('data',$this->request->data);
        $this->set('execution_count',count($res2));
        $this->render('sales_with_master_result');

    }

    /**
     * 売上遡及
     * 商品選択画面
     */
    function sales_with_unit_search() {
        App::import('Sanitize');
        //施主コードから施設IDを取得
        $owner_id = $this->getFacilityId($data["Condition"]["owner_code"] , Configure::read('FacilityType.donor') ,  $this->Session->read('Auth.facility_id_selected'));
        //使いまわし用に$this->request->dataへ格納
        $this->request->data["Condition"]["owner_id"] = $owner_id;

        //得意先コードから施設IDを取得
        $result = $this->getHospitalId($this->request->data);
        //使いまわし用に$this->request->dataへ格納
        $this->request->data["Condition"]["hospital_id"] = $result[0]["MstFacility"]["id"];

        //包装単位でグループ化された請求明細一覧を取得
        if(isset($this->request->data["Search"]["flag"]) && $this->request->data["Search"]["flag"] === "1"){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $sql = "";
            $sql = $this->getSQL_SalesRetroactData_unit($this->request->data);
            /* 画面からの検索条件を追加 */
            //商品ID
            if(!empty($this->request->data["Search"]["internal_code"])){
                $sql .= " AND MstFacilityItem.internal_code ILIKE '%".$this->request->data["Search"]["internal_code"]."%'";
            }
            //商品名
            if(!empty($this->request->data["Search"]["item_name"])){
                $sql .= " AND MstFacilityItem.item_name ILIKE '%".$this->request->data["Search"]["item_name"]."%'";
            }
            //規格
            if(!empty($this->request->data["Search"]["standard"])){
                $sql .= " AND MstFacilityItem.standard ILIKE '%".$this->request->data["Search"]["standard"]."%'";
            }
            //製品番号
            if(!empty($this->request->data["Search"]["item_code"])){
                $sql .= " AND MstFacilityItem.item_code ILIKE '%".$this->request->data["Search"]["item_code"]."%'";
            }
            //販売元名
            if(!empty($this->request->data["Search"]["dealer_name"])){
                $sql .= " AND MstDealer.dealer_name ILIKE '%".$this->request->data["Search"]["dealer_name"]."%'";
            }
            //JANコード
            if(!empty($this->request->data["Search"]["jan_code"])){
                $sql .= " AND MstFacilityItem.jan_code = '".$this->request->data["Search"]["jan_code"]."'";
            }
            //適用開始日
            if(!empty($this->request->data["Search"]["start_date"])){
                $sql .= " AND MstSalesConfig.start_date = '".$this->request->data["Search"]["start_date"]."'";
            }
            //カートに入っているものは出さない！
            if(!empty($this->request->data["Search"]["selectedItemId"])){
                $ids = substr($this->request->data["Search"]["selectedItemId"],0,-1);
                $idList = explode(",", $ids);
                $sql .= ' AND MstSalesConfig.id not in (' . join(',',$idList) . ')';
            }

            $sql .= ' GROUP BY ';
            $sql .= '       TrnClaim.department_id_from';
            $sql .= '      ,TrnClaim.mst_item_unit_id';
            $sql .= '      ,"MstFacilityItem__internal_code"';
            $sql .= '      ,"MstFacilityItem__item_name"';
            $sql .= '      ,"MstFacilityItem__standard"';
            $sql .= '      ,"MstFacilityItem__item_code"';
            $sql .= '      ,"MstDealer__dealer_name"';
            $sql .= '      ,"MstSalesConfig__id"';
            $sql .= '      ,"MstSalesConfig__start_date"';
            $sql .= '      ,"MstConfig__config_price"';
            $sql .= '      ,"MstUnitName__unit_name"';
            $sql .= ' ORDER BY ';
            $sql .= '       "MstFacilityItem__internal_code"';
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnClaim'));
            //リミット
            if(empty($this->request->data["limit"])){
                $sql .= " LIMIT " . min(array_keys(Configure::read('displaycounts_combobox')));
            }else{
                $sql .= " LIMIT ".$this->request->data["limit"];
            }
            $SearchResult = $this->TrnClaim->query($sql);
            $this->set("SearchResult",$SearchResult);

            //カートの内容を取得
            if(!empty($this->request->data["Search"]["selectedItemId"])){
                $CartResult = array();
                //TODO 選択順番保持のため、一件ずつ取得する(遅い場合はINへ変更予定)
                foreach($idList as $id){
                    $sql = '';
                    $sql .= $this->getSQL_SalesRetroactData_unit($this->request->data);
                    $sql .= ' AND MstSalesConfig.id = '.$id;
                    $sql .= ' GROUP BY ';
                    $sql .= '       TrnClaim.department_id_from';
                    $sql .= '      ,TrnClaim.mst_item_unit_id';
                    $sql .= '      ,"MstFacilityItem__internal_code"';
                    $sql .= '      ,"MstFacilityItem__item_name"';
                    $sql .= '      ,"MstFacilityItem__standard"';
                    $sql .= '      ,"MstFacilityItem__item_code"';
                    $sql .= '      ,"MstDealer__dealer_name"';
                    $sql .= '      ,"MstSalesConfig__id"';
                    $sql .= '      ,"MstSalesConfig__start_date"';
                    $sql .= '      ,"MstConfig__config_price"';
                    $sql .= '      ,"MstUnitName__unit_name"';
                    $sql .= ' ORDER BY ';
                    $sql .= '       "MstFacilityItem__internal_code"';

                    $res = $this->TrnClaim->query($sql);
                    $CartResult[] = $res[0];
                }
                //画面表示用に設定
                $this->set("CartResult",$CartResult);
            }
            $this->request->data = $data;
        }
        $this->set("data",$this->request->data);
        $this->render('sales_with_unit_search');
    }

    /**
     * 売上遡及
     * 商品確認画面
     */
    function sales_with_unit_confirm() {
        //選択内容を取得
        foreach($this->request->data["Confirm"] as $id){
            $sql = '';
            $sql .= $this->getSQL_SalesRetroactData_unit($this->request->data);
            $sql .= ' AND MstSalesConfig.id = '.$id;
            $sql .= ' GROUP BY ';
            $sql .= '       TrnClaim.department_id_from';
            $sql .= '      ,TrnClaim.mst_item_unit_id';
            $sql .= '      ,"MstFacilityItem__internal_code"';
            $sql .= '      ,"MstFacilityItem__item_name"';
            $sql .= '      ,"MstFacilityItem__standard"';
            $sql .= '      ,"MstFacilityItem__item_code"';
            $sql .= '      ,"MstDealer__dealer_name"';
            $sql .= '      ,"MstSalesConfig__id"';
            $sql .= '      ,"MstSalesConfig__start_date"';
            $sql .= '      ,"MstConfig__config_price"';
            $sql .= '      ,"MstUnitName__unit_name"';
            $sql .= ' ORDER BY ';
            $sql .= '       "MstFacilityItem__internal_code"';

            $res = $this->TrnClaim->query($sql);
            $result[] = $res[0];
        }
        //画面表示用に設定
        $this->set("result",$result);
        $this->set("data",$this->request->data);
        $this->render('sales_with_unit_confirm');
    }

    /**
     * 売上遡及
     * 商品選択完了画面
     */
    function sales_with_unit_result() {
        //エラーメッセージ
        $error_msg = "";
        //画面から渡されたid
        $ids = implode(",",$this->request->data["Confirm"]);
        //終了画面表示データ
        $result = array();

        $claim_ids = array();

        //計上日での締めチェック
        $registFlag = $this->checkCloseAuth($this->request->data,Configure::read("Claim.sales"));
        if(!$registFlag){
            //計上日が締められているため再度条件設定してもらう
            $this->Session->setFlash("計上日はすでに締められています", 'growl', array('type'=>'error') );
            $this->sales();
            return;
        }else{
            //トランザクション開始
            $this->TrnRetroactHeader->begin();

            //正常終了時に表示するデータを先に取得しておく
            foreach($this->request->data["Confirm"] as $id){
                $sql = '';
                $sql .= $this->getSQL_SalesRetroactData_unit($this->request->data);
                $sql .= ' AND MstSalesConfig.id = '.$id;
                $sql .= ' GROUP BY ';
                $sql .= '       TrnClaim.department_id_from';
                $sql .= '      ,TrnClaim.mst_item_unit_id';
                $sql .= '      ,"MstFacilityItem__internal_code"';
                $sql .= '      ,"MstFacilityItem__item_name"';
                $sql .= '      ,"MstFacilityItem__standard"';
                $sql .= '      ,"MstFacilityItem__item_code"';
                $sql .= '      ,"MstDealer__dealer_name"';
                $sql .= '      ,"MstSalesConfig__id"';
                $sql .= '      ,"MstSalesConfig__start_date"';
                $sql .= '      ,"MstConfig__config_price"';
                $sql .= '      ,"MstUnitName__unit_name"';
                $sql .= ' ORDER BY ';
                $sql .= '       "MstFacilityItem__internal_code"';
                $res = $this->TrnClaim->query($sql);
                $result[] = $res[0];
            }

            /* 赤黒明細作成用データ取得 */
            $sql = "";
            //取得項目
            $sql .= $this->getSQL_RetroactDataDetail(Configure::read("Claim.sales"));
            //結合テーブル
            $sql .= $this->getSQL_SalesRetroactDataTable($this->request->data);
            //各種条件
            $sql .= ' AND MstSalesConfig.id IN ('.$ids.')';
            $sql .= ' GROUP BY ';
            $sql .= '       "TrnClaim__department_id_from"';
            $sql .= '      ,"TrnClaim__department_id_to"';
            $sql .= '      ,"TrnClaim__mst_item_unit_id"';
            $sql .= '      ,"TrnClaim__unit_price"';
            $sql .= '      ,"TrnClaim__round"';
            $sql .= '      ,"MstConfig__config_price"';
            $sql .= '      ,"MstFacilityFrom__round"';
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';
            $res1 = $this->TrnClaim->query($sql);

            //各明細用データ取得
            $sql = '';
            $sql .= $this->getSQL_EachDataDetail(Configure::read("Claim.sales"));
            $sql .= $this->getSQL_SalesRetroactDataTable($this->request->data);
            $sql .= ' AND MstSalesConfig.id IN ('.$ids.')';
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';
            $sql .= '         ,TrnClaim.id';
            $res2 = $this->TrnClaim->query($sql);

            /* 遡及対象の請求レコードを行ロック */
            foreach($res2 as $r){
                $claim_ids[] = $r['TrnClaim']['id'];
            }
            $this->TrnClaim->query('select * from trn_claims as a where a.id in ('.join(',',$claim_ids).') for update ');

            //遡及ヘッダ作成
            $header_key = Configure::read("workNoModels");
            $work_no = $this->setWorkNo4Header(date("Ymd"), array_search("TrnRetroactHeader", $header_key));
            $res3 = $this->crerateRetroactHeader($this->request->data,count($res1)*2+count($res2),$work_no,Configure::read("Claim.sales"));

            //遡及ヘッダid
            $TrnRetroactHeaderId = null;
            if($res3){
                //登録成功
                $TrnRetroactHeaderId = $this->TrnRetroactHeader->getLastInsertID();
            }else{
                //登録失敗
                $is_error_flag = true;
                $error_msg .= "遡及ヘッダ作成エラー\n";
            }

            //赤黒遡及データ作成
            $error_msg .= $this->createRetroactDataDetail($this->request->data,$res1,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.sales"));

            //各テーブル更新＆遡及明細作成
            $error_msg .= $this->createEachDataDetail($this->request->data,$res2,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.sales"));
        }

        if(!empty($error_msg)){
            //エラーありのためロールック＆やり直し
            $this->TrnRetroactHeader->rollback();

            //画面遷移
            $this->Session->setFlash($error_msg, 'growl', array('type'=>'error') );
            $this->sales();
            return;
        }else{
            //コミット
            $this->TrnRetroactHeader->commit();

            //画面表示用に設定
            $this->set("result",$result);
            $this->Session->setFlash("遡及が完了しました", "growl", array("type"=>"star") );
        }
        $this->set('data',$this->request->data);
        $this->render('sales_with_unit_result');
    }

    /**
     * 売上遡及
     * CSV入力画面
     */
    function sales_with_csv(){
        //施主コードから施設IDを取得
        $owner_id = $this->getFacilityId($data["Condition"]["owner_code"] , Configure::read('FacilityType.donor') ,  $this->Session->read('Auth.facility_id_selected'));
        //使いまわし用に$this->request->dataへ格納
        $this->request->data["Condition"]["owner_id"] = $owner_id;

        //得意先コードから施設ID、部署IDを取得
        $result = $this->getHospitalId($this->request->data);
        //使いまわし用に$this->request->dataへ格納
        $this->request->data["Condition"]["hospital_id"] = $result[0]["MstFacility"]["id"];
        $this->set('data',$this->request->data);

        $this->render('sales_with_csv');
    }

    /**
     * 売上遡及
     * CSV確認画面
     */
    function sales_with_csv_confirm() {
        //エラーメッセージ
        $error_msg = array();
        //対象請求明細IDリスト
        $idList = array();
        //センター部署ID取得
        $sql = "";
        $sql .= "SELECT id";
        $sql .= "  FROM mst_departments";
        $sql .= " WHERE mst_facility_id = ".$this->Session->read("Auth.facility_id_selected");
        $sql .= "   AND is_deleted = false";

        $res = $this->MstDepartment->query($sql);
        $center_department_id = $res[0][0]["id"];

        //送信ファイルチェック
        if(is_uploaded_file($this->request->data['Condition']['csv_file']['tmp_name'])){
            $this->CsvReadUtils->setFileName( $this->request->data['Condition']['csv_file']['tmp_name'] );
        }else{
            $this->Session->setFlash("CSVファイルが不正です", 'growl', array('type'=>'error') );
            $this->sales();
            return;
        }

        // CsvReadUtils初期設定
        $this->CsvReadUtils->setLineMaxLength( 2000 );

        // CSVオープン
        $this->CsvReadUtils->open();

        // ハッシュキー設定
        $this->CsvReadUtils->hashKeyClear();
        $this->CsvReadUtils->hashKeyAdd( "internal_code" );
        $this->CsvReadUtils->hashKeyAdd( "per_unit" );
        $this->CsvReadUtils->hashKeyAdd( "start_date" );
        $this->CsvReadUtils->hashKeyAdd( "center_code" );
        $this->CsvReadUtils->hashKeyAdd( "facility_code" );

        //1行目を取得
        $header_data = $this->CsvReadUtils->readCsvLine();
        if($header_data != array('internal_code' => '商品ID',
                                 'per_unit'      => '入り数',
                                 'start_date'    => '適用開始日',
                                 'center_code'   => '商品施設コード',
                                 'facility_code' => '得意先施設コード'
                                 )){
            $this->Session->setFlash('CSVファイルが不正です。ヘッダ行をご確認ください。', 'growl', array('type'=>'error') );
            $this->stock();
            return;
        }

        $i = 0;
        $tempDate = '';
        $idList = array();

        while ( ! $this->CsvReadUtils->eof() ) {
            // CSV一行読込
            $line_data = $this->CsvReadUtils->readCsvLine();

            // EOFだけの行を読み込んでしまうため
            if ( $line_data == "" ) {
                continue;
            }

            $i++; //CSV件数カウント（エラー分含む）
            if(count($line_data) !== 5){
                //項目数チェック不正
                $this->Session->setFlash("CSVファイルが不正です", 'growl', array('type'=>'error') );
                $this->stock();
                return;
            }

            //商品ID存在チェック
            $fitem_info = $this->MstFacilityItem->find('first',array(
                'fields'     => 'id',
                'recursive'  => -1,
                'conditions' => array(
                    'internal_code'   => $line_data['internal_code'],
                    'item_type'       => array(
                        Configure::read('Items.item_types.normalitem') ,
                        Configure::read('Items.item_types.setitem')
                        ),
                    'mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
                    )
                ));
            if(empty($fitem_info)){
                $error_msg[] = $i."件目：商品IDが存在しません";
                continue;
            }

            //適用開始日フォーマットチェック
            $date = explode("/",$line_data['start_date']);
            if(!checkdate($date[1],$date[2],$date[0])){
                $error_msg[] = $i."件目：適用開始日はYYYY/MM/DDの形式で入力してください";
                continue;
            }


            //CSV内で複数適用開始日は不許可にする
            if($i === 1){
                $tempDate = $line_data['start_date'];
            }else{
                if($tempDate !== $line_data['start_date']){
                    $error_msg[] = $i."件目：適用開始日は全て同じ日付けにしてください";
                    continue;
                }
            }

            //包装単位ID存在チェック
            $sql  = 'SELECT mst_item_unit_id';
            $sql .= '  FROM mst_sales_configs A';
            $sql .= ' INNER JOIN mst_item_units B';
            $sql .= '    ON A.mst_item_unit_id = B.id';
            $sql .= '   AND B.per_unit = '.$line_data['per_unit'];
            $sql .= '   AND B.mst_facility_item_id = '.$fitem_info['MstFacilityItem']['id'];
            $sql .= '   AND B.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
            $sql .= " WHERE A.start_date = '".$line_data['start_date'] . "'";
            $sql .= '   AND A.mst_facility_item_id = '.$fitem_info['MstFacilityItem']['id'];
            $sql .= '   AND A.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
            $sql .= '   AND A.partner_facility_id = '.$this->request->data['Condition']['hospital_id'];
            $sql .= '   AND A.is_deleted = false';
            $itemunit_info = $this->MstTransactionConfig->query($sql);
            if(empty($itemunit_info)){
                $error_msg[] = $i."件目：包装単位が存在しません";
                continue;
            }

            //施設コード存在チェック
            $facility_info = $this->MstFacility->find('all',array(
                'conditions' => array(
                    'facility_type'   => Configure::read('FacilityType.center'),
                    'facility_code'   => $line_data['center_code'],
                    'id'              => $this->Session->read('Auth.facility_id_selected')
                    ),
                'recursive' => -1
                ));

            if(empty($facility_info)){
                $error_msg[] = $i."件目：商品施設コードが存在しません";
                continue;
            }

            //得意先コードチェック
            if($this->request->data['Condition']['hospital_code'] != $line_data['facility_code']){
                $error_msg[] = $i."件目：施設コードが存在しません";
                continue;
            }

            //エラーなしの場合該当の請求明細を検索
            if(count($error_msg) === 0){
                $sql  = 'SELECT TrnClaim.id AS "TrnClaim__id"';
                $sql .= '  FROM trn_claims AS TrnClaim';
                $sql .= ' INNER JOIN mst_item_units AS MstItemUnit';
                $sql .= '    ON TrnClaim.mst_item_unit_id = MstItemUnit.id';
                $sql .= '   AND MstItemUnit.id = '.$itemunit_info[0][0]['mst_item_unit_id'];
                $sql .= '  LEFT JOIN mst_departments AS MstDepartment ';
                $sql .= '    ON MstDepartment.id = TrnClaim.department_id_to';
                $sql .= ' WHERE TrnClaim.department_id_from = '.$center_department_id;
                $sql .= "   AND MstDepartment.mst_facility_id = " . $this->request->data['Condition']['hospital_id'];
                $sql .= "   AND TrnClaim.is_deleted = false";
                $sql .= "   AND TrnClaim.claim_date >= '".$this->request->data['Condition']['start_date']."'";
                $sql .= "   AND TrnClaim.claim_date <= '".$this->request->data['Condition']['end_date']."'";
                $sql .= "   AND TrnClaim.is_not_retroactive = false";
                $sql .= "   AND TrnClaim.is_stock_or_sale = '".Configure::read('Claim.sales')."'";
                $sql .= "   and ( TrnClaim.stocking_close_type = 2 or TrnClaim.facility_close_type = 2 )";

                $this->TrnClaim->create();
                $result = $this->TrnClaim->query($sql);

                if(empty($result) or count($result) === 0){
                    //取得データなし
                    $error_msg[] = $i."件目：対象データが存在しません";
                    continue;
                }else{
                    foreach($result as $value){
                        if(in_array($value['TrnClaim']['id'], $idList)){
                            $error_msg[] = $i."件目：対象データが重複しています";
                                break;
                        }else{
                            $idList[] = $value['TrnClaim']['id'];
                        }
                    }
                }
            }
        }

        //取得した請求明細IDリストを文字列へ
        $ids = implode(',', $idList);

        $this->set('ids',$ids);
        $this->set('read_count',$i); //ヘッダ件数除く
        $this->set('regist_count',count($idList));
        $this->set('stocking_start_date',$tempDate);
        $this->set('error_msg',$error_msg);

        $this->set('data',$this->request->data);
        $this->render('sales_with_csv_confirm');
    }

    /**
     * 売上遡及
     * CSV完了画面
     */
    function sales_with_csv_result() {
        //エラーメッセージ
        $error_msg = "";

        //計上日での締めチェック
        $registFlag = $this->checkCloseAuth($this->request->data,Configure::read("Claim.sales"));
        if(!$registFlag){
            //計上日が締められているため再度条件設定してもらう
            $this->Session->setFlash("計上日はすでに締められています", 'growl', array('type'=>'error') );
            $this->stock();
            return;
        }else{
            //トランザクション開始
            $this->TrnRetroactHeader->begin();

            /* 赤黒明細作成用データ取得 */
            $sql = "";
            //取得項目
            $sql .= $this->getSQL_RetroactDataDetail(Configure::read("Claim.sales"));
            //結合テーブル
            $sql .= $this->getSQL_SalesRetroactDataTable($this->request->data);
            //各種条件
            $sql .= ' AND TrnClaim.id IN ('.$this->request->data["Condition"]["trn_claims_id"].')';
            $sql .= ' GROUP BY ';
            $sql .= '       "TrnClaim__department_id_from"';
            $sql .= '      ,"TrnClaim__department_id_to"';
            $sql .= '      ,"TrnClaim__mst_item_unit_id"';
            $sql .= '      ,"TrnClaim__unit_price"';
            $sql .= '      ,"TrnClaim__round"';
            $sql .= '      ,"MstConfig__config_price"';
            $sql .= '      ,"MstFacilityFrom__round"';
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';
            $res1 = $this->TrnClaim->query($sql);

            //各明細用データ取得
            $sql = '';
            $sql .= $this->getSQL_EachDataDetail(Configure::read("Claim.sales"));
            $sql .= $this->getSQL_SalesRetroactDataTable($this->request->data);
            $sql .= ' AND TrnClaim.id IN ('.$this->request->data["Condition"]["trn_claims_id"].')';
            $sql .= ' ORDER BY TrnClaim.mst_item_unit_id';
            $sql .= '         ,TrnClaim.id';
            $res2 = $this->TrnClaim->query($sql);

            /* 遡及対象の請求レコードを行ロック */
            foreach($res2 as $r){
                $claim_ids[] = $r['TrnClaim']['id'];
            }
            $this->TrnClaim->query('select * from trn_claims as a where a.id in ('.join(',',$claim_ids).') for update ');

            //遡及ヘッダ作成
            $header_key = Configure::read("workNoModels");
            $work_no = $this->setWorkNo4Header(date("Ymd"), array_search("TrnRetroactHeader", $header_key));
            $res3 = $this->crerateRetroactHeader($this->request->data,count($res1)*2+count($res2),$work_no,Configure::read("Claim.sales"));

            //遡及ヘッダid
            $TrnRetroactHeaderId = null;
            if($res3){
                //登録成功
                $TrnRetroactHeaderId = $this->TrnRetroactHeader->getLastInsertID();
            }else{
                //登録失敗
                $is_error_flag = true;
                $error_msg .= "遡及ヘッダ作成エラー\n";
            }

            //赤黒遡及データ作成
            $error_msg .= $this->createRetroactDataDetail($this->request->data,$res1,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.sales"));

            //各テーブル更新＆遡及明細作成
            $error_msg .= $this->createEachDataDetail($this->request->data,$res2,$TrnRetroactHeaderId,$work_no,Configure::read("Claim.sales"));
        }

        if(!empty($error_msg)){
            //エラーありのためロールック＆やり直し
            $this->TrnRetroactHeader->rollback();
            //画面遷移
            $this->Session->setFlash($error_msg, 'growl', array('type'=>'error') );
            $this->sales();
            return;
        }else{
            //コミット
            $this->TrnRetroactHeader->commit();

            $tempList = explode(",",$this->request->data["Condition"]["trn_claims_id"]);

            //画面表示用に設定
            $this->set("read_count",$this->request->data["Condition"]["read_count"]);
            $this->set("regist_count",count($tempList));
            $this->Session->setFlash("遡及が完了しました", "growl", array("type"=>"star") );
        }
        $this->set('data',$this->request->data);
        $this->render('sales_with_csv_result');
    }

    /**
     * 遡及データ作成用　赤黒明細作成データ取得SQL生成
     */
    private function getSQL_RetroactDataDetail($type){
        $sql = "";
        $sql .= 'SELECT TrnClaim.department_id_from AS "TrnClaim__department_id_from"';
        $sql .= '      ,TrnClaim.department_id_to AS "TrnClaim__department_id_to"';
        $sql .= '      ,TrnClaim.mst_item_unit_id AS "TrnClaim__mst_item_unit_id"';
        $sql .= '      ,SUM(0 - TrnClaim.claim_price) AS "TrnClaim__claim_price"'; //赤明細用の符号反転金額
        $sql .='       ,SUM( ';
        $sql .= '           CASE ';
        $sql .= '           WHEN TrnReceiving.receiving_type = '.Configure::read("ReceivingType.arrival");
        $sql .= '             OR TrnReceiving.receiving_type = '.Configure::read("ReceivingType.receipt");
        $sql .= '             OR TrnReceiving.receiving_type = '.Configure::read("ReceivingType.move");
        $sql .= '             OR TrnReceiving.receiving_type is null  ';
        $sql .= '           THEN TrnClaim.count';
        $sql .= '           ELSE 0- TrnClaim.count';
        $sql .= '           END ) AS "TrnClaim__count"';                    //黒明細計算用の数量
        $sql .='       ,TrnClaim.unit_price AS "TrnClaim__unit_price"';     //請求作成時の単価
        $sql .= '      ,TrnClaim.round AS "TrnClaim__round"';               //赤明細作成用の丸め区分
        $sql .= '      ,MstFacilityFrom.round AS "MstFacilityFrom__round"'; //黒明細作成用の丸め区分
        //黒明細計算用の仕入単価
        if($type == Configure::read("Claim.stock")){
            $sql .='   ,MstTransactionConfig.transaction_price AS "MstConfig__config_price"';
        }elseif($type == Configure::read("Claim.sales")){
            $sql .='   ,MstSalesConfig.sales_price AS "MstConfig__config_price"';
        }
        return $sql;
    }

    /**
     * 遡及データ作成用　各明細細更新データ取得SQL生成
     */
    private function getSQL_EachDataDetail($type){
        $sql  = 'SELECT TrnClaim.id                 AS "TrnClaim__id"';
        $sql .= '      ,TrnClaim.department_id_from AS "TrnClaim__department_id_from"';
        $sql .= '      ,TrnClaim.department_id_to   AS "TrnClaim__department_id_to"';
        $sql .= '      ,TrnClaim.mst_item_unit_id   AS "TrnClaim__mst_item_unit_id"';
        $sql .= '      ,TrnClaim.trn_consume_id     AS "TrnClaim__trn_consume_id"';
        $sql .= '      ,TrnClaim.trn_receiving_id   AS "TrnClaim__trn_receiving_id"';
        $sql .= '      ,TrnClaim.modified           AS "TrnClaim__modified"';
        $sql .= '      ,TrnClaim.unit_price         AS "TrnClaim__unit_price"';
        if($type == Configure::read("Claim.stock")){
            $sql .='   ,MstTransactionConfig.transaction_price AS "MstConfig__config_price"';
        }elseif($type == Configure::read("Claim.sales")){
            $sql .='   ,MstSalesConfig.sales_price AS "MstConfig__config_price"';
        }

        return $sql;
    }

    /**
     * 仕入遡及データ作成用　テーブル結合SQL生成
     */
    private function getSQL_StockRetroactDataTable($data){
        $sql  = "  FROM trn_claims AS TrnClaim";
        $sql .= " INNER JOIN mst_departments AS MstDepartmentFrom";
        $sql .= "    ON TrnClaim.department_id_from = MstDepartmentFrom.id";
        $sql .= " INNER JOIN mst_facilities AS MstFacilityFrom";
        $sql .= "    ON MstDepartmentFrom.mst_facility_id = MstFacilityFrom.id";
        $sql .= "   AND MstFacilityFrom.id = ".$data["Condition"]["supplier_id"];
        $sql .= " INNER JOIN mst_departments AS MstDepartmentTo";
        $sql .= "    ON TrnClaim.department_id_to = MstDepartmentTo.id";
        $sql .= " INNER JOIN mst_facilities AS MstFacilityTo";
        $sql .= "    ON MstDepartmentTo.mst_facility_id = MstFacilityTo.id";
        $sql .= "   AND MstFacilityTo.id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= " INNER JOIN mst_item_units AS MstItemUnit";
        $sql .= "    ON TrnClaim.mst_item_unit_id = MstItemUnit.id";
        $sql .= "   AND MstItemUnit.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= " INNER JOIN mst_facility_items AS MstFacilityItem";
        $sql .= "    ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id";
        $sql .= "   AND MstFacilityItem.mst_owner_id = ".$data["Condition"]["owner_id"];
        $sql .= " INNER JOIN mst_transaction_configs AS MstTransactionConfig";
        $sql .= "    ON MstItemUnit.id = MstTransactionConfig.mst_item_unit_id";
        $sql .= "   AND MstTransactionConfig.is_deleted = false";
        $sql .= "   AND MstTransactionConfig.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= "   AND MstTransactionConfig.start_date = '".$data["Condition"]["stocking_start_date"]."'";
        $sql .= '   AND MstTransactionConfig.partner_facility_id = '.$data["Condition"]["supplier_id"];
        $sql .= "  LEFT JOIN trn_receivings AS TrnReceiving";
        $sql .= "    ON TrnClaim.trn_receiving_id = TrnReceiving.id";
        $sql .= "   AND TrnReceiving.is_deleted = false";
        $sql .= "   AND TrnReceiving.is_retroactable = true";
        $sql .= " WHERE TrnClaim.is_deleted = false";
        $sql .= "   AND TrnClaim.claim_date >= '".$data["Condition"]["start_date"]."'";
        $sql .= "   AND TrnClaim.claim_date <= '".$data["Condition"]["end_date"]."'";
        $sql .= "   AND TrnClaim.is_not_retroactive = false";
        $sql .= "   AND TrnClaim.is_stock_or_sale = '".Configure::read('Claim.stock')."'";
        $sql .= "   and ( TrnClaim.stocking_close_type = 2 or TrnClaim.facility_close_type = 2 )";

        return $sql;
    }

    /**
     * 売上遡及データ作成用　テーブル結合SQL生成
     */
    private function getSQL_SalesRetroactDataTable($data){
        $sql  = "  FROM trn_claims AS TrnClaim";
        $sql .= " INNER JOIN mst_departments AS MstDepartmentFrom";
        $sql .= "    ON TrnClaim.department_id_from = MstDepartmentFrom.id";
        $sql .= " INNER JOIN mst_facilities AS MstFacilityFrom";
        $sql .= "    ON MstDepartmentFrom.mst_facility_id = MstFacilityFrom.id";
        $sql .= "   AND MstFacilityFrom.id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= " INNER JOIN mst_departments AS MstDepartmentTo";
        $sql .= "    ON TrnClaim.department_id_to = MstDepartmentTo.id";
        $sql .= " INNER JOIN mst_facilities AS MstFacilityTo";
        $sql .= "    ON MstDepartmentTo.mst_facility_id = MstFacilityTo.id";
        $sql .= "   AND MstFacilityTo.id = ".$data["Condition"]["hospital_id"];
        $sql .= " INNER JOIN mst_item_units AS MstItemUnit";
        $sql .= "    ON TrnClaim.mst_item_unit_id = MstItemUnit.id";
        $sql .= "   AND MstItemUnit.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= " INNER JOIN mst_facility_items AS MstFacilityItem";
        $sql .= "    ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id";
        $sql .= "   AND MstFacilityItem.mst_owner_id = ".$data["Condition"]["owner_id"];
        $sql .= " INNER JOIN mst_sales_configs AS MstSalesConfig";
        $sql .= "    ON MstItemUnit.id = MstSalesConfig.mst_item_unit_id";
        $sql .= "   AND MstSalesConfig.is_deleted = false";
        $sql .= "   AND MstSalesConfig.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= "   AND MstSalesConfig.start_date = '".$data["Condition"]["stocking_start_date"]."'";
        $sql .= '   AND MstSalesConfig.partner_facility_id = '.$data["Condition"]["hospital_id"];
        $sql .= "  LEFT JOIN trn_receivings AS TrnReceiving";
        $sql .= "    ON TrnClaim.trn_receiving_id = TrnReceiving.id";
        $sql .= "   AND TrnReceiving.is_deleted = false";
        $sql .= "   AND TrnReceiving.is_retroactable = true";
        $sql .= " WHERE TrnClaim.is_deleted = false";
        $sql .= "   AND TrnClaim.claim_date >= '".$data["Condition"]["start_date"]."'";
        $sql .= "   AND TrnClaim.claim_date <= '".$data["Condition"]["end_date"]."'";
        $sql .= "   AND TrnClaim.is_not_retroactive = false";
        $sql .= "   AND TrnClaim.is_stock_or_sale = '".Configure::read('Claim.sales')."'";
        $sql .= "   and ( TrnClaim.sales_close_type = 2 or TrnClaim.facility_close_type = 2 )";

        return $sql;
    }

    /**
     * 仕入遡及データ作成用SQL生成
     */
    private function getSQL_StockRetroactData_unit($data){
        $sql = '';
        $sql .= 'SELECT MstFacilityItem.internal_code                         AS "MstFacilityItem__internal_code"';
        $sql .= '      ,MstFacilityItem.item_name                             AS "MstFacilityItem__item_name"';
        $sql .= '      ,MstFacilityItem.standard                              AS "MstFacilityItem__standard"';
        $sql .= '      ,MstFacilityItem.item_code                             AS "MstFacilityItem__item_code"';
        $sql .= '      ,MstDealer.dealer_name                                 AS "MstDealer__dealer_name"';
        $sql .= '      ,MstTransactionConfig.id                               AS "MstTransactionConfig__id"';
        $sql .= "      ,TO_CHAR(MstTransactionConfig.start_date,'YYYY/MM/DD') AS ".'"MstTransactionConfig__start_date"';
        $sql .= '      ,MstTransactionConfig.transaction_price                AS "MstConfig__config_price"';
        $sql .= '      ,(CASE MstItemUnit.per_unit';
        $sql .= '       WHEN 1 THEN MstUnitName.unit_name';
        $sql .= "       ELSE MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name ||')'";
        $sql .= '       END)                                                  AS "MstUnitName__unit_name"';
        $sql .= '  FROM trn_claims AS TrnClaim';
        $sql .= ' INNER JOIN mst_departments AS MstDepartmentFrom';
        $sql .= '    ON TrnClaim.department_id_from = MstDepartmentFrom.id';
        $sql .= ' INNER JOIN mst_facilities AS MstFacilityFrom';
        $sql .= '    ON MstDepartmentFrom.mst_facility_id = MstFacilityFrom.id';
        $sql .= '   AND MstFacilityFrom.id = '.$data["Condition"]["supplier_id"];
        $sql .= ' INNER JOIN mst_departments AS MstDepartmentTo';
        $sql .= '    ON TrnClaim.department_id_to = MstDepartmentTo.id';
        $sql .= ' INNER JOIN mst_facilities AS MstFacilityTo';
        $sql .= '    ON MstDepartmentTo.mst_facility_id = MstFacilityTo.id';
        $sql .= '   AND MstFacilityTo.id = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= ' INNER JOIN mst_item_units AS MstItemUnit';
        $sql .= '    ON TrnClaim.mst_item_unit_id = MstItemUnit.id';
        $sql .= '   AND MstItemUnit.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= ' INNER JOIN mst_unit_names AS MstUnitName';
        $sql .= '    ON MstItemUnit.mst_unit_name_id = MstUnitName.id';
        $sql .= '   AND MstUnitName.is_deleted = false';
        $sql .= ' INNER JOIN mst_unit_names AS MstPerUnitName';
        $sql .= '    ON MstItemUnit.per_unit_name_id = MstPerUnitName.id';
        $sql .= '   AND MstPerUnitName.is_deleted = false';
        $sql .= ' INNER JOIN mst_facility_items AS MstFacilityItem';
        $sql .= '    ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
        $sql .= '   AND MstFacilityItem.mst_owner_id = '.$data["Condition"]["owner_id"];
        $sql .= '  LEFT JOIN mst_dealers AS MstDealer';
        $sql .= '    ON MstFacilityItem.mst_dealer_id = MstDealer.id';
        $sql .= '   AND MstDealer.is_deleted = false';
        $sql .= ' INNER JOIN mst_transaction_configs AS MstTransactionConfig';
        $sql .= '    ON MstItemUnit.id = MstTransactionConfig.mst_item_unit_id';
        $sql .= '   AND MstTransactionConfig.is_deleted = false';
        $sql .= '   AND MstTransactionConfig.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= '   AND MstTransactionConfig.partner_facility_id = '.$data["Condition"]["supplier_id"];
        $sql .= '  LEFT JOIN trn_receivings AS TrnReceiving';
        $sql .= '    ON TrnClaim.trn_receiving_id = TrnReceiving.id';
        $sql .= '   AND TrnReceiving.is_deleted = false';
        $sql .= '   AND TrnReceiving.is_retroactable = true';
        $sql .= ' WHERE TrnClaim.is_deleted = false';
        $sql .= "   AND TrnClaim.claim_date >= '".$this->request->data["Condition"]["start_date"]."'";
        $sql .= "   AND TrnClaim.claim_date <= '".$this->request->data["Condition"]["end_date"]."'";
        $sql .= '   AND TrnClaim.is_not_retroactive = false';
        $sql .= "   AND TrnClaim.is_stock_or_sale = '".Configure::read('Claim.stock')."'";
        $sql .= "   and ( TrnClaim.stocking_close_type = 2 or TrnClaim.facility_close_type = 2 )";

        return $sql;
    }

    /**
     * 仕入遡及データ作成用SQL生成
     */
    private function getSQL_SalesRetroactData_unit($data){
        $sql = '';
        $sql .= 'SELECT MstFacilityItem.internal_code                   AS "MstFacilityItem__internal_code"';
        $sql .= '      ,MstFacilityItem.item_name                       AS "MstFacilityItem__item_name"';
        $sql .= '      ,MstFacilityItem.standard                        AS "MstFacilityItem__standard"';
        $sql .= '      ,MstFacilityItem.item_code                       AS "MstFacilityItem__item_code"';
        $sql .= '      ,MstDealer.dealer_name                           AS "MstDealer__dealer_name"';
        $sql .= '      ,MstSalesConfig.id                               AS "MstSalesConfig__id"';
        $sql .= "      ,TO_CHAR(MstSalesConfig.start_date,'YYYY/MM/DD') AS ".'"MstSalesConfig__start_date"';
        $sql .= '      ,MstSalesConfig.sales_price                      AS "MstConfig__config_price"';
        $sql .= '      ,(CASE MstItemUnit.per_unit';
        $sql .= '       WHEN 1 THEN MstUnitName.unit_name';
        $sql .= "       ELSE MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name ||')'";
        $sql .= '       END)                                            AS "MstUnitName__unit_name"';
        $sql .= '  FROM trn_claims AS TrnClaim';
        $sql .= ' INNER JOIN mst_departments AS MstDepartmentFrom';
        $sql .= '    ON TrnClaim.department_id_from = MstDepartmentFrom.id';
        $sql .= ' INNER JOIN mst_facilities AS MstFacilityFrom';
        $sql .= '    ON MstDepartmentFrom.mst_facility_id = MstFacilityFrom.id';
        $sql .= '   AND MstFacilityFrom.id = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= ' INNER JOIN mst_departments AS MstDepartmentTo';
        $sql .= '    ON TrnClaim.department_id_to = MstDepartmentTo.id';
        $sql .= ' INNER JOIN mst_facilities AS MstFacilityTo';
        $sql .= '    ON MstDepartmentTo.mst_facility_id = MstFacilityTo.id';
        $sql .= '   AND MstFacilityTo.id = '.$data["Condition"]["hospital_id"];
        $sql .= ' INNER JOIN mst_item_units AS MstItemUnit';
        $sql .= '    ON TrnClaim.mst_item_unit_id = MstItemUnit.id';
        $sql .= '   AND MstItemUnit.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= ' INNER JOIN mst_unit_names AS MstUnitName';
        $sql .= '    ON MstItemUnit.mst_unit_name_id = MstUnitName.id';
        $sql .= '   AND MstUnitName.is_deleted = false';
        $sql .= ' INNER JOIN mst_unit_names AS MstPerUnitName';
        $sql .= '    ON MstItemUnit.per_unit_name_id = MstPerUnitName.id';
        $sql .= '   AND MstPerUnitName.is_deleted = false';
        $sql .= ' INNER JOIN mst_facility_items AS MstFacilityItem';
        $sql .= '    ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
        $sql .= '   AND MstFacilityItem.mst_owner_id = '.$data["Condition"]["owner_id"];
        $sql .= '  LEFT JOIN mst_dealers AS MstDealer';
        $sql .= '    ON MstFacilityItem.mst_dealer_id = MstDealer.id';
        $sql .= '   AND MstDealer.is_deleted = false';
        $sql .= ' INNER JOIN mst_sales_configs AS MstSalesConfig';
        $sql .= '    ON MstItemUnit.id = MstSalesConfig.mst_item_unit_id';
        $sql .= '   AND MstSalesConfig.is_deleted = false';
        $sql .= '   AND MstSalesConfig.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= '   AND MstSalesConfig.partner_facility_id = '.$data["Condition"]["hospital_id"];
        $sql .= '  LEFT JOIN trn_receivings AS TrnReceiving';
        $sql .= '    ON TrnClaim.trn_receiving_id = TrnReceiving.id';
        $sql .= '   AND TrnReceiving.is_deleted = false';
        $sql .= '   AND TrnReceiving.is_retroactable = true';
        $sql .= ' WHERE TrnClaim.is_deleted = false';
        $sql .= "   AND TrnClaim.claim_date >= '".$this->request->data["Condition"]["start_date"]."'";
        $sql .= "   AND TrnClaim.claim_date <= '".$this->request->data["Condition"]["end_date"]."'";
        $sql .= '   AND TrnClaim.is_not_retroactive = false';
        $sql .= "   AND TrnClaim.is_stock_or_sale = '".Configure::read('Claim.sales')."'";
        $sql .= "   and ( TrnClaim.sales_close_type = 2 or TrnClaim.facility_close_type = 2 )";

        return $sql;
    }
    
    /**
     * 仕入先コードから仕入先施設ID,仕入先部署IDを取得
     */
    private function getSupplierId($data){
        $sql = '';
        $sql .= 'SELECT MstFacility.id   AS "MstFacility__id"';
        $sql .= '      ,MstDepartment.id AS "MstDepartment__id"';
        $sql .= '  FROM mst_facility_relations AS MstFacilityRelation';
        $sql .= ' INNER JOIN mst_facilities AS MstFacility';
        $sql .= '    ON MstFacilityRelation.partner_facility_id = MstFacility.id';
        $sql .= '   AND MstFacility.facility_type = '.Configure::read('FacilityType.supplier');
        $sql .= '   AND MstFacility.is_deleted = false';
        $sql .= "   AND MstFacility.facility_code = '".$data["Condition"]["supplier_code"]."'";
        $sql .= " INNER JOIN mst_departments AS MstDepartment";
        $sql .= "    ON MstFacility.id = MstDepartment.mst_facility_id";
        $sql .= ' WHERE MstFacilityRelation.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= '   AND MstFacilityRelation.is_deleted = false';
        $sql .= ' ORDER BY MstFacility.facility_code';

        $result = $this->MstFacilityRelation->query($sql);

        return $result;
    }

    /**
     * 得意先コードから得意先施設IDを取得
     */
    private function getHospitalId($data){
        $sql = '';
        $sql .= 'SELECT MstFacility.id AS "MstFacility__id"';
        $sql .= '  FROM mst_facility_relations AS MstFacilityRelation';
        $sql .= ' INNER JOIN mst_facilities AS MstFacility';
        $sql .= '    ON MstFacilityRelation.partner_facility_id = MstFacility.id';
        $sql .= '   AND MstFacility.facility_type = '.Configure::read('FacilityType.hospital');
        $sql .= '   AND MstFacility.is_deleted = false';
        $sql .= "   AND MstFacility.facility_code = '".$data["Condition"]["hospital_code"]."'";
        $sql .= " INNER JOIN mst_departments AS MstDepartment";
        $sql .= "    ON MstFacility.id = MstDepartment.mst_facility_id";
        $sql .= ' WHERE MstFacilityRelation.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= '   AND MstFacilityRelation.is_deleted = false';
        $sql .= ' ORDER BY MstFacility.facility_code';

        $result = $this->MstFacilityRelation->query($sql);

        return $result;
    }
    /**
     * 遡及計上日の締めチェック
     */
    private function checkCloseAuth($data,$type){
        //相手先のID
        $partner_id = null;
        if($type == Configure::read("Claim.stock")){
            $partner_id = $data["Condition"]["supplier_id"];
        }else if($type == Configure::read("Claim.sales")){
            $partner_id = $data["Condition"]["hospital_id"];
        }

        $sql = "";
        $sql .= "SELECT COUNT(*)";
        $sql .= "  FROM trn_close_headers AS TrnCloseHeader";
        $sql .= " WHERE TrnCloseHeader.start_date <= '".$data["Condition"]["posted_sun"]."'";
        $sql .= "   AND TrnCloseHeader.end_date >= '".$data["Condition"]["posted_sun"]."'";
        $sql .= "   AND TrnCloseHeader.is_deleted = false";
        $sql .= "   AND TrnCloseHeader.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= " AND TrnCloseHeader.partner_facility_id = ".$partner_id;
        $res = $this->TrnCloseHeader->query($sql);

        $regist_flag = true;
        if(is_array($res) && $res[0][0]["count"] == 1){
            //仮締め更新権限チェック
            if(!$this->canUpdateAfterClose($data["Condition"]["posted_sun"],$partner_id)){
                //仮締め更新権限がない場合は再度条件設定
                $this->Session->setFlash("仮締め更新権限がありません", 'growl', array('type'=>'error') );
                $this->stock();
                return;
            }
        }else if(is_array($res) && $res[0][0]["count"] == 2){
            $regist_flag = false;
        }

        return $regist_flag;
    }

    /**
     * 遡及ヘッダ作成
     */
    private function crerateRetroactHeader($data,$count,$work_no,$type){
        //相手先のID
        $partner_id = null;
        $retroact_type = null;
        if($type == Configure::read('Claim.stock')){
            $partner_id = $data['Condition']['supplier_id'];
            $retroact_type = Configure::read('Retroact.stock_retroact');
        }else if($type == Configure::read('Claim.sales')){
            $partner_id = $data['Condition']['hospital_id'];
            $retroact_type = Configure::read('Retroact.sales_retroact');
        }

        $params = array();
        $params = array(
            "work_no"         => $work_no,
            "work_date"       => $data['Condition']['posted_sun'],
            "work_type"       => $data['Condition']['class_id'],
            "recital"         => $data['Condition']['recital'],
            "retroact_type"   => $retroact_type,
            "mst_facility_id" => $partner_id,
            "start_date"      => $data['Condition']['start_date'],
            "end_date"        => $data['Condition']['end_date'],
            "is_deleted"      => FALSE,
            "creater"         => $this->Session->read('Auth.MstUser.id'),
            "modifier"        => $this->Session->read('Auth.MstUser.id'),
            "detail_count"    => $count //赤黒遡及明細プラス通常の遡及明細数
            );
        $this->TrnRetroactHeader->create();
        $res = $this->TrnRetroactHeader->save($params,false);

        return $res;
    }

    /**
     * 赤黒遡及データ作成
     */
    private function createRetroactDataDetail($data,$list,$header_id,$work_no,$type){
        $retroact_type_aka = null;
        $retroact_type_kuro = null;
        if($type == Configure::read('Claim.stock')){
            $retroact_type_aka = Configure::read('Retroact.stock_retroact_minus');
            $retroact_type_kuro = Configure::read('Retroact.stock_retroact_plus');
        }else if($type == Configure::read('Claim.sales')){
            $retroact_type_aka = Configure::read('Retroact.sales_retroact_minus');
            $retroact_type_kuro = Configure::read('Retroact.sales_retroact_plus');
        }
        //エラーメッセージ
        $error_msg = "";
        foreach ($list as $row) {
            //赤遡及明細作成
            $params = array();
            $params = array(
                "work_no"                => $work_no,
                "retroact_type"          => $retroact_type_aka,
                "department_id_from"     => $row['TrnClaim']['department_id_from'],
                "department_id_to"       => $row['TrnClaim']['department_id_to'],
                "mst_item_unit_id"       => $row['TrnClaim']['mst_item_unit_id'],
                "retroact_price"         => $row['TrnClaim']['unit_price'],//請求作成時単価
                "trn_close_header_id"    => "0", //締め情報に紐付かないため、仮の値として0を入れておく
                "is_deleted"             => false,
                "creater"                => $this->Session->read('Auth.MstUser.id'),
                "modifier"               => $this->Session->read('Auth.MstUser.id'),
                "trn_retroact_header_id" => $header_id
                );
            $this->TrnRetroactRecord->create();
            $res1 = $this->TrnRetroactRecord->save($params,false);
            $TrnRetroactRrecordId_aka = null;
            if($res1){
                //登録成功
                $TrnRetroactRrecordId_aka = $this->TrnRetroactRecord->getLastInsertID();
            } else {
                //登録失敗
                $error_msg .= "赤遡及明細作成エラー\n";
                break;
            }

            //赤請求明細作成
            $params = array();
            $params = array(
                "department_id_from"      => $row['TrnClaim']['department_id_from'],
                "department_id_to"        => $row['TrnClaim']['department_id_to'],
                "mst_item_unit_id"        => $row['TrnClaim']['mst_item_unit_id'],
                "claim_date"              => $data['Condition']['posted_sun'],
                "claim_price"             => $row['TrnClaim']['claim_price'],
                "trn_retroact_record_id"  => $TrnRetroactRrecordId_aka,
                "stocking_close_type"     => 0,
                "sales_close_type"        => 0,
                "facility_close_type"     => 0,
                "is_deleted"              => false,
                "is_stock_or_sale"        => $type,
                "count"                   => $row['TrnClaim']['count'],
                "unit_price"              => $row['TrnClaim']['unit_price'],
                "round"                   => $row['TrnClaim']['round'],
                "is_not_retroactive"      => true, //遡及できないようにする仕様
                "creater"                 => $this->Session->read('Auth.MstUser.id'),
                "modifier"                => $this->Session->read('Auth.MstUser.id'),
                "retroact_execution_flag" => true
                );
            $this->TrnClaim->create();
            $res2 = $this->TrnClaim->save($params,false);
            if(!$res2){
                //登録失敗
                $error_msg .= "赤請求明細作成エラー\n";
                break;
            }

            //黒遡及明細作成
            $params = array();
            $params = array(
                "work_no"                => $work_no,
                "retroact_type"          => $retroact_type_kuro,
                "department_id_from"     => $row['TrnClaim']['department_id_from'],
                "department_id_to"       => $row['TrnClaim']['department_id_to'],
                "mst_item_unit_id"       => $row['TrnClaim']['mst_item_unit_id'],
                "retroact_price"         => $row['MstConfig']['config_price'], //遡及時単価
                "trn_close_header_id"    => "0",
                "is_deleted"             => false,
                "creater"                => $this->Session->read('Auth.MstUser.id'),
                "modifier"               => $this->Session->read('Auth.MstUser.id'),
                "trn_retroact_header_id" => $header_id
                );
            $this->TrnClaim->create();
            $this->TrnRetroactRecord->create();
            $res3 = $this->TrnRetroactRecord->save($params,false);
            $TrnRetroactRrecordId_kuro = null;
            if($res3){
                //登録成功
                $TrnRetroactRrecordId_kuro = $this->TrnRetroactRecord->getLastInsertID();
            } else {
                //登録失敗
                $error_msg .= "黒遡及明細作成エラー\n";
                break;
            }

            //黒請求明細作成
            //遡及実行時の丸め区分で金額を計算
            $price = 0;
            switch($row['MstFacilityFrom']['round']){
              case Configure::read('RoundType.Round'):
                //四捨五入
                $price = round($row['MstConfig']['config_price'] * $row['TrnClaim']['count']);
                break;
              case Configure::read('RoundType.Ceil'):
                //切上げ
                $price = ceil($row['MstConfig']['config_price'] * $row['TrnClaim']['count']);
                break;
              case Configure::read('RoundType.Floor'):
                //切捨て
                $price = floor($row['MstConfig']['config_price'] * $row['TrnClaim']['count']);
                break;
              default:
                //何もしない
                $price = $row['MstConfig']['config_price'] * $row['TrnClaim']['count'];
                break;
            }

            $params = array();
            $params = array(
                'department_id_from'      => $row['TrnClaim']['department_id_from'],
                'department_id_to'        => $row['TrnClaim']['department_id_to'],
                'mst_item_unit_id'        => $row['TrnClaim']['mst_item_unit_id'],
                'claim_date'              => $data['Condition']['posted_sun'],
                'claim_price'             => $price,
                'trn_retroact_record_id'  => $TrnRetroactRrecordId_kuro,
                'stocking_close_type'     => 0,
                'sales_close_type'        => 0,
                'facility_close_type'     => 0,
                'is_deleted'              => false,
                'is_stock_or_sale'        => $type,
                'count'                   => $row['TrnClaim']['count'],
                'unit_price'              => $row['MstConfig']['config_price'],
                'round'                   => $row['TrnClaim']['round'],
                'is_not_retroactive'      => true, //遡及できないようにする仕様なのでTrue
                'creater'                 => $this->Session->read('Auth.MstUser.id'),
                'modifier'                => $this->Session->read('Auth.MstUser.id'),
                'retroact_execution_flag' => true
                );
            $this->TrnClaim->create();
            $res4 = $this->TrnClaim->save($params,false);
            if(!$res4){
                //登録失敗
                $error_msg .= "赤請求明細作成エラー\n";
                break;
            }
        }//end of foreach

        return $error_msg;
    }

    /**
     * 遡及明細作成＆各テーブル更新
     */
    private function createEachDataDetail($data,$list,$header_id,$work_no,$type){
        $retroact_type = null;
        if($type == Configure::read('Claim.stock')){
            $retroact_type = Configure::read('Retroact.stock_retroact');
        }elseif($type == Configure::read('Claim.sales')){
            $retroact_type = Configure::read('Retroact.sales_retroact');
        }

        $error_msg = "";
        foreach ($list as $row){
            //更新時間チェック
            if($row['TrnClaim']['modified'] > $data['Condition']['chk_date']){
                //更新チェックエラーの場合、最初からやり直し
                $error_msg .= "別ユーザによってデータが更新されています\n";
                break;
            }

            //請求明細に紐付く遡及明細作成
            $params = array();
            $params = array(
                "work_no"                => $work_no,
                "retroact_type"          => $retroact_type,
                "department_id_from"     => $row['TrnClaim']['department_id_from'],
                "department_id_to"       => $row['TrnClaim']['department_id_to'],
                "mst_item_unit_id"       => $row['TrnClaim']['mst_item_unit_id'],
                "retroact_price"         => $row['MstConfig']['config_price'], //遡及時単価
                "trn_close_header_id"    => "0",
                "is_deleted"             => false,
                "creater"                => $this->Session->read('Auth.MstUser.id'),
                "modifier"               => $this->Session->read('Auth.MstUser.id'),
                "trn_retroact_header_id" => $header_id
                );
            $this->TrnClaim->create();
            $this->TrnRetroactRecord->create();
            $res1 = $this->TrnRetroactRecord->save($params,false);
            $TrnRetroactRrecordId_kuro = null;
            if($res1){
                //登録成功
                $TrnRetroactRrecordId = $this->TrnRetroactRecord->getLastInsertID();
            } else {
                //登録失敗
                $error_msg .= "遡及明細作成エラー\n";
                break;
            }

            //請求データ更新
            $params = array();
            $params = array(
                "id"                      => $row['TrnClaim']['id'],
                "trn_retroact_record_id"  => $TrnRetroactRrecordId,
                "is_not_retroactive"      => true, //遡及できないようにする仕様なのでTrue
                "modifier"                => $this->Session->read('Auth.MstUser.id'),
                "retroact_execution_flag" => true
                );
            $this->TrnClaim->create();
            $res2 = $this->TrnClaim->save($params,false);
            if(!$res2){
                //登録失敗
                $error_msg .= "請求明細更新エラー\n";
                break;
            }

            //請求データに紐付く入荷明細更新
            if(!empty($row['TrnClaim']['trn_receiving_id'])){
                $params = array();
                $params = array(
                    "id"                      => $row['TrnClaim']['trn_receiving_id'],
                    "is_retroactable"         => false, //遡及できないようにする仕様なのでfalse
                    "modifier"                => $this->Session->read('Auth.MstUser.id'),
                    "retroact_execution_flag" => true
                    );
                $this->TrnReceiving->create();
                $res3 = $this->TrnReceiving->save($params,false);
                if(!$res3){
                    //登録失敗
                    $error_msg .= "入荷明細更新エラー\n";
                    break;
                }
            }

            //売上遡及で消費明細が紐付いていれば、消費明細更新
            if($type === Configure::read('Claim.sales') && !empty($row['TrnClaim']['trn_consume_id'])){
                $params = array();
                $params = array(
                    "id"                      => $row['TrnClaim']['trn_consume_id'],
                    "is_retroactable"         => false,
                    "modifier"                => $this->Session->read('Auth.MstUser.id'),
                    "retroact_execution_flag" => true
                    );
                $this->TrnConsume->create();
                $res4 = $this->TrnConsume->save($params,false);
                if(!$res4){
                    //登録失敗
                    $error_msg .= "消費明細更新エラー\n";
                    break;
                }
            }
        }
        return $error_msg;
    }


    //検索
    private function searchFacilityItems($where = null , $order = null , $limit = null ){
        $sql  = ' select ';
        $sql .= '       a.id                       as "TrnRetroact__id" ';
        $sql .= '     , c.id                       as "TrnRetroact__item_unit_id" ';
        if($this->Session->read('Retroact.retroact_type') == $this->RETROACT_TYPE_STOCK){
            //仕入の場合
            $sql .= '     , a.transaction_price        as "TrnRetroact__price" ';
        }else{
            //売上の場合
            $sql .= '     , a.sales_price              as "TrnRetroact__price" ';
        }
        $sql .= '     , to_char(a.start_date , \'YYYY/mm/dd\') as "TrnRetroact__start_date" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when c.per_unit = 1  ';
        $sql .= '         then f.unit_name  ';
        $sql .= "         else f.unit_name || '(' || c.per_unit || g.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                            as "TrnRetroact__unit_name" ';
        $sql .= '     , d.internal_code            as "TrnRetroact__internal_code" ';
        $sql .= '     , d.item_name                as "TrnRetroact__item_name" ';
        $sql .= '     , d.item_code                as "TrnRetroact__item_code" ';
        $sql .= '     , d.standard                 as "TrnRetroact__standard" ';
        $sql .= '     , h.dealer_name              as "TrnRetroact__dealer_name"  ';
        $sql .= '   from ';
        if($this->Session->read('Retroact.retroact_type') == $this->RETROACT_TYPE_STOCK){
            //仕入の場合
            $sql .= '     mst_transaction_configs as a  ';
        }else{
            //売上の場合
            $sql .= '     mst_sales_configs as a  ';
        }
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on a.partner_facility_id = b.id  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on a.mst_item_unit_id = c.id  ';
        $sql .= '     left join mst_facility_items as d  ';
        $sql .= '       on c.mst_facility_item_id = d.id  ';
        $sql .= '     left join mst_facilities as e  ';
        $sql .= '       on d.mst_owner_id = e.id  ';
        $sql .= '     left join mst_unit_names as f  ';
        $sql .= '       on f.id = c.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as g  ';
        $sql .= '       on g.id = c.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as h  ';
        $sql .= '       on h.id = d.mst_dealer_id  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= $where;

        if(!is_null($order)){
            $sql .=' ' . $order;
        }
        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'TrnRetroactRecord'));
            $sql .=' ' .$limit;
        }
        return $sql;
    }


    //指定期間が本締めされていたらNGを返す
    function closedCheck($start_date , $end_date){
        $sql  =' select ';
        $sql .='       count(*)        as count  ';
        $sql .='   from ';
        $sql .='     trn_close_headers as a  ';
        $sql .='   where ';
        $sql .='     a.is_deleted = false  ';
        $sql .='     and a.is_provisional = false  ';
        $sql .='     and a.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
        $sql .='     and a.close_type = 3  ';
        $sql .='     and (  ';
        $sql .='       (  ';
        $sql .="         a.start_date <= '".$start_date."'  ";
        $sql .="         and a.end_date >= '".$start_date."' ";
        $sql .='       )  ';
        $sql .='       or (  ';
        $sql .="         a.start_date <= '".$end_date."'  ";
        $sql .="         and a.end_date >= '".$end_date."' ";
        $sql .='       ) ';
        $sql .='     ) ';

        $ret = $this->TrnCloseHeader->query($sql);
        if($ret[0][0]['count'] == 0){
            return true;
        }else{
            return false;
        }
    }


    /**
     * 遡及履歴CSV出力
     */
    function export_csv() {
        $where = $this->_getCSVWhere();
        $sql = $this->_getCSVSql($where);

        $this->db_export_csv($sql , "遡及履歴", 'history');
    }


    /**
     * 遡及履歴CSV出力用SQL作成
     */
    private function _getCSVSql($where) {
        $sql  = "";
        $sql .= " select";
        $sql .= "      TrnRetroactHeader.work_no                              as 遡及番号";
        $sql .= "     ,(CASE TrnRetroactHeader.retroact_type ";
        $sql .= "           WHEN '".Configure::read('Retroact.change_price')."'   THEN '価格変更' ";
        $sql .= "           WHEN '".Configure::read('Retroact.stock_retroact')."' THEN '仕入遡及' ";
        $sql .= "           WHEN '".Configure::read('Retroact.sales_retroact')."' THEN '売上遡及' ";
        $sql .= "       END)                                                  as 遡及種別 ";
        $sql .= "     ,TO_CHAR(TrnRetroactHeader.work_date,'YYYY/MM/DD')      as 計上日 ";
        $sql .= "     ,MstFacility.facility_name                              as 対象施設 ";
        $sql .= "     ,MstClass.name                                          as 作業区分 ";
        $sql .= "     ,MstUser.user_name                                      as 登録者 ";
        $sql .= "     ,TO_CHAR(TrnRetroactHeader.created,'YYYY/MM/DD')        as 登録日 ";
        $sql .= "     ,TrnRetroactHeader.recital                              as 備考 ";
        $sql .= '     ,MstFacilityItem.internal_code                          as "商品ID" ';
        $sql .= "     ,MstFacilityItem.item_name                              as 商品名 ";
        $sql .= "     ,MstFacilityItem.standard                               as 規格 ";
        $sql .= "     ,MstFacilityItem.item_code                              as 製品番号 ";
        $sql .= "     ,MstDealer.dealer_name                                  as 販売元 ";
        $sql .= "     ,(case when  MstItemUnit.per_unit = 1 then  MstUnitName.unit_name ";
        $sql .= "           else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || PerUnitName.unit_name || ')' ";
        $sql .= "       end)                                                  as 包装単位名 ";
        $sql .= "     ,TrnRetroactRecord.retroact_price                       as 単価 ";
        $sql .= "     ,TO_CHAR(TrnRetroactHeader.start_date, 'YYYY/MM/DD')    as 適用開始日 ";
        $sql .= "     ,TO_CHAR(TrnRetroactHeader.end_date, 'YYYY/MM/DD')    as 適用開始日 ";
        $sql .= " from";
        $sql .= "     trn_retroact_headers AS TrnRetroactHeader inner join trn_retroact_records AS TrnRetroactRecord ";
        $sql .= "         on TrnRetroactRecord.trn_retroact_header_id = TrnRetroactHeader.id ";
        $sql .= "         and TrnRetroactRecord.is_deleted = FALSE ";
        $sql .= "     inner join mst_facilities AS MstFacility ";
        $sql .= "         on TrnRetroactHeader.mst_facility_id = MstFacility.id ";
        $sql .= "     inner join mst_users AS MstUser ";
        $sql .= "         on CAST(TrnRetroactHeader.creater AS INTEGER) = MstUser.id ";
        $sql .= "     inner join mst_item_units AS MstItemUnit ";
        $sql .= "         on TrnRetroactRecord.mst_item_unit_id = MstItemUnit.id ";
        $sql .= "         and MstItemUnit.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= "     inner join mst_unit_names AS MstUnitName ";
        $sql .= "         on MstUnitName.id = MstItemUnit.mst_unit_name_id ";
        $sql .= "     inner join mst_facility_items AS MstFacilityItem ";
        $sql .= "         on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ";
        $sql .= "     left join mst_dealers AS MstDealer ";
        $sql .= "         on MstFacilityItem.mst_dealer_id = MstDealer.id ";
        $sql .= "     inner join mst_unit_names AS PerUnitName ";
        $sql .= "         on PerUnitName.id = MstItemUnit.per_unit_name_id ";
        $sql .= "     left join mst_classes AS MstClass ";
        $sql .= "         on MstClass.id = TrnRetroactHeader.work_type ";
        $sql .= " where ";
        $sql .= $where;

        return $sql;
    }

    /**
     * 遡及履歴CSV出力用条件作成
     */
    private function _getCSVWhere() {

        $center_dept_id = null;
        $supplier_id = null;
        $hospital_id = null;

        //センターの部署ID取得
        $sql  = "SELECT id";
        $sql .= "  FROM mst_departments";
        $sql .= " WHERE mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= "   AND is_deleted = false";
        $res = array();
        $res = $this->MstDepartment->query($sql);
        $center_dept_id = $res[0][0]['id'];

        //仕入先の部署id取得
        if(!empty($this->request->data['TrnRetroactRecord']['department_code_from'])){
            //関数使用のためにデータ詰め替え
            $tempData['Condition']['supplier_code'] = $this->request->data['TrnRetroactRecord']['department_code_from'];
            $res = array();
            $res = $this->getSupplierId($tempData);
            if(!empty($res)){
                $supplier_id = $res[0]['MstFacility']['id'];
            }
        }

        //得意先の部署ID取得
        if(!empty($this->request->data['TrnRetroactRecord']['department_code_to'])){
            //関数使用のためにデータ詰め替え
            $tempData['Condition']['hospital_code'] = $this->request->data['TrnRetroactRecord']['department_code_to'];
            $res = array();
            $res = $this->getHospitalId($tempData);
            if(!empty($res)){
                $hospital_id = $res[0]['MstFacility']['id'];
            }
        }

        $where = " TrnRetroactHeader.is_deleted = false ";
        //作業番号
        if(!empty($this->request->data['TrnRetroactHeader']['work_no'])){
            $where .= " AND TrnRetroactHeader.work_no = '".Sanitize::escape($this->request->data['TrnRetroactHeader']['work_no'])."'";
        }
        //計上日
        if(!empty($this->request->data['TrnRetroactHeader']['start_date'])){
            $where .= " AND TrnRetroactHeader.work_date >= '".Sanitize::escape($this->request->data['TrnRetroactHeader']['start_date'])."'";
        }
        if(!empty($this->request->data['TrnRetroactHeader']['end_date'])){
            $where .= " AND TrnRetroactHeader.work_date <= '".Sanitize::escape($this->request->data['TrnRetroactHeader']['end_date'])."'";
        }
        //仕入先
        if(!empty($supplier_id)){
            $where .= " AND TrnRetroactHeader.mst_facility_id = ".$supplier_id;
        }
        //得意先
        if(!empty($hospital_id)){
            $where .= " AND TrnRetroactHeader.mst_facility_id = ".$hospital_id;
        }
        //仕入先、得意先共に未指定の場合、自センターの取引のみを表示
        if(empty($hospital_id) && empty($hospital_id)){
            $where .= " AND (TrnRetroactRecord.department_id_from = ".$center_dept_id;
            $where .= "  OR  TrnRetroactRecord.department_id_to = ".$center_dept_id.")";
        }
        //商品ID
        if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
            $where .= " AND MstFacilityItem.internal_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."%'";
        }
        //商品名
        if(!empty($this->request->data['MstFacilityItem']['item_name'])){
            $where .= " AND MstFacilityItem.item_name ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
        }
        //規格
        if(!empty($this->request->data['MstFacilityItem']['standard'])){
            $where .= " AND MstFacilityItem.standard ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }
        //製品番号
        if(!empty($this->request->data['MstFacilityItem']['item_code'])){
            $where .= " AND MstFacilityItem.item_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        //販売元
        if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
            $where .= " AND MstDealer.dealer_name ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name'])."%'";
        }
        //JANコード
        if(!empty($this->request->data['MstFacilityItem']['jan_code'])){
            $where .= " AND MstFacilityItem.jan_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['jan_code'])."%'";
        }
        //作業区分
        if(!empty($this->request->data['TrnRetroactRecord']['work_type'])){
            $where .= " AND TrnRetroactHeader.work_type = ".$this->request->data['TrnRetroactRecord']['work_type'];
        }
        //遡及種別
        if(!empty($this->request->data['TrnRetroactRecord']['retroact_type'])){
            $where .= " AND TrnRetroactHeader.retroact_type = ".$this->request->data['TrnRetroactRecord']['retroact_type'];
        }

        //操作可能病院で絞り込む
        $where .='   and ( ';
        $where .='      mstfacility.facility_type != '. Configure::read('FacilityType.hospital');
        $where .='      or mstfacility.id in ( ';
        $where .='        select';
        $where .='              a.mst_facility_id ';
        $where .='          from';
        $where .='            mst_user_belongings as a ';
        $where .='          where';
        $where .='            a.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $where .='            and a.is_deleted  = false ';
        $where .='      )';
        $where .='    ) ';

        return $where;
    }
}
// EOF
