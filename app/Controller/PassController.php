<?php
/**
 * PassController
 *
 * @version 1.0.0
 * @since 2010/05/17
 */

class PassController extends AppController {
    var $name = 'Pass';
    
    /**
     *
     * @var array $uses
   */
    var $uses = array(
        'MstUser',
        );
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;
    
    /**
     * @var MstUser
     */
    var $MstUser;
    
    function beforeFilter() {
        parent::beforeFilter();
        //ログインを必要としない処理の設定
        $this->Auth->allow('index');
        
        $this->Auth->fields = array('username'=>'login_id','password'=> 'password');
        Security::setHash('md5');
    }
    
    public function index(){
        $sql  = ' select ';
        $sql .= '       id       as "MstUser__id" ';
        $sql .= '     , password as "MstUser__password"  ';
        $sql .= '   from ';
        $sql .= '     mst_users ';
        
        $ret = $this->MstUser->query($sql);
        
        foreach($ret as $r){
            $this->MstUser->query(" update mst_users set password = '" . $this->getHashPassword($r['MstUser']['password']) . "' where id = " . $r['MstUser']['id']);
        }
    }
    
}

