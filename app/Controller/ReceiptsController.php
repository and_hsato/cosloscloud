<?php
/**
 * ReceiptsController
 *　受領
 * @version 1.0.0
 * @since 2010/08/11
 */
class ReceiptsController extends AppController {

    /**
     * @var $name
     */
    var $name = 'Receipts';

    /**
     * @var array $uses
     */
    var $uses = array('MstUser',
                      'MstFacilityRelation',
                      'MstFacility',
                      'MstFacilityItem',
                      'MstDepartment',
                      'MstClass',
                      'TrnShippingHeader',     //出荷ヘッダ
                      'TrnShipping',           //出荷明細
                      'MstItemUnit',
                      'MstDealer',             //販売元
                      'TrnSticker',            //シール
                      'TrnPromiseHeader',      //引当ヘッダ
                      'TrnPromise',            //引当明細
                      'MstTransactionConfig',  //仕入設定
                      'MstSalesConfig',        //売上設定
                      'TrnReceivingHeader',    //入荷ヘッダ
                      'TrnReceiving',          //入荷明細
                      'TrnStorageHeader',      //入庫ヘッダ
                      'TrnStorage',            //入庫明細
                      'TrnStock',              //在庫テーブル
                      'TrnClaim',              //請求テーブル（売上データ）
                      'MstUnitName',           //単位名
                      'TrnConsumeHeader',      //消費ヘッダ
                      'TrnConsume',            //消費明細
                      'TrnCloseHeader',        //締め
                      'TrnStickerRecord',      //シール移動履歴
                      'TrnOrderHeader',        //発注ヘッダ
                      'TrnOrder',              //発注明細
                      );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker','common');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Barcode','Stickers','TemporaryItem');

    public function beforeFilter(){
      parent::beforeFilter();
      $this->Auth->allowedActions[] = 'report';
      $this->Auth->allowedActions[] = 'seal';
    }
    
    /**
     *
     */
    function index (){
        $this->setRoleFunction(14); //受領登録
        $ShippingList = array();
        
        if(isset($this->request->data['search']['is_search'])){
            $where = "";
            //**************************************************************************
            //ユーザ入力値による検索条件の追加
            //部署：初期画面で部署が選択されている場合は検索条件に追加
            if(isset($this->request->data['MstReceipts']['departmentCode']) && $this->request->data['MstReceipts']['departmentCode'] != ""){
                $where .= ' and j.department_code = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['departmentCode'])."' ";
            }
            
            //発注番号(完全一致)-------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_shippings_no'])) && ($this->request->data['MstReceipts']['search_shippings_no'] != "")){
                $where .= ' and a.work_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_shippings_no'])."' ";
            }
            //出荷日------------------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_start_date'])) && ($this->request->data['MstReceipts']['search_start_date'] != "")){
                //開始日と終了日の両方に入力あり
                if((isset($this->request->data['MstReceipts']['search_end_date'])) && ($this->request->data['MstReceipts']['search_end_date'] != "")){
                    $where .= ' and a.work_date >= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_start_date'])."' ";
                    $where .= ' and a.work_date <= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_end_date'])."' ";

                    //開始日のみ入力あり
                } else {
                    $where .= ' and a.work_date >= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_start_date'])."' ";
                }
                //終了日のみ入力あり
            } else {
                if((isset($this->request->data['MstReceipts']['search_end_date'])) && ($this->request->data['MstReceipts']['search_end_date'] != "")){
                    $where .= ' and a.work_date <= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_end_date'])."' ";
                }
            }
            //商品名(LIKE検索)-------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_item_name'])) && ($this->request->data['MstReceipts']['search_item_name'] != "")){
                $where .= ' and d.item_name LIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_item_name'])."%' ";
            }

            //商品ID(完全一致)------------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_internal_code'])) && ($this->request->data['MstReceipts']['search_internal_code'] != "")){
                $where .= ' and d.internal_code = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_internal_code'])."' ";
            }

            //規格(LIKE検索)------------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_standard'])) && ($this->request->data['MstReceipts']['search_standard'] != "")){
                $where .= ' and d.standard LIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_standard'])."%' ";
            }

            //製品番号(LIKE検索)------------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_item_code'])) && ($this->request->data['MstReceipts']['search_item_code'] != "")){
                $where .= ' and d.item_code LIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_item_code'])."%' ";
            }

            //販売元(LIKE検索)------------------------------------------------------------
            if((isset($this->request->data['MstReceipts']['search_dealer_name'])) && ($this->request->data['MstReceipts']['search_dealer_name'] != "")){
                $where .= ' and e.dealer_name LIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_dealer_name'])."%' ";
            }

            //仕入先
            if((isset($this->request->data['MstReceipts']['supplierCode'])) && ($this->request->data['MstReceipts']['supplierCode'] != "")){
                $where .= " and i.facility_code = '" .  $this->request->data['MstReceipts']['supplierCode'] . "'";
            }

            
            $sql  = ' select ';
            $sql .= '       a.id               as "MstReceipts__id"';
            $sql .= '     , a.work_no          as "MstReceipts__work_no"';
            $sql .= "     , to_char(a.work_date,'YYYY/MM/DD')";
            $sql .= '                          as "MstReceipts__work_date"';
            $sql .= '     , i.facility_name    as "MstReceipts__facility_name"';
            $sql .= '     , j.department_name  as "MstReceipts__department_name"';
            $sql .= '     , a.detail_count     as "MstReceipts__detail_count"';
            $sql .= '     , count(b.id)        as "MstReceipts__count"  ';
            $sql .= '   from ';
            $sql .= '     trn_shipping_headers as a  ';
            $sql .= '     left join trn_shippings as b  ';
            $sql .= '       on b.trn_shipping_header_id = a.id  ';
            $sql .= '     left join mst_item_units as c  ';
            $sql .= '       on c.id = b.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as d  ';
            $sql .= '       on d.id = c.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as e  ';
            $sql .= '       on e.id = d.mst_dealer_id  ';
            $sql .= '     left join trn_stickers as f  ';
            $sql .= '       on f.id = b.trn_sticker_id  ';
            $sql .= '     left join trn_orders as g  ';
            $sql .= '       on g.id = f.trn_order_id  ';
            $sql .= '     left join mst_departments as h  ';
            $sql .= '       on h.id = g.department_id_to  ';
            $sql .= '     left join mst_facilities as i  ';
            $sql .= '       on i.id = h.mst_facility_id  ';
            $sql .= '     left join mst_departments as j  ';
            $sql .= '       on j.id = g.department_id_from  ';
            $sql .= '   where ';
            $sql .= '     a.is_deleted = false  ';
            $sql .= '     and b.is_deleted = false  ';
            $sql .= '     and d.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
            $sql .= '     and b.shipping_status < 3  ';
            $sql .= '     and b.shipping_type = 8  ';
            $sql .= $where;
            $sql .= '   group by ';
            $sql .= '     a.id ';
            $sql .= '     , a.work_no ';
            $sql .= '     , a.work_date ';
            $sql .= '     , i.facility_name ';
            $sql .= '     , j.department_name ';
            $sql .= '     , a.detail_count  ';
            $sql .= '   order by ';
            $sql .= '     a.work_date ';
            $sql .= '     , a.work_no ';
            
            
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnShippingHeader'));
            $sql .= ' limit ' ;
            $sql .=        $this->_getLimitCount();
            
            $ShippingList = $this->TrnShippingHeader->query($sql);
            //初回遷移時のみ締めチェック
        } else {
            //初期表示データ
            $this->request->data['MstReceipts']['search_start_date'] = date("Y/m/d", mktime(0, 0, 0, date("m"), date("d")-7, date("Y")));
            $this->request->data['MstReceipts']['search_end_date']   = date('Y/m/d');
        }
        // 仕入先一覧取得
        $this->set('supplier_list' , $this->getFacilityList( $this->Session->read('Auth.facility_id_selected'),
                                                             array(Configure::read('FacilityType.supplier'))
            ));
        
        // 部署一覧取得
        $this->set('department_list' , $this->getHospitalDepartmentList($this->Session->read('Auth.facility_id_selected')));
        
        $this->request->data = AppController::outputFilter($this->request->data);
        $this->set('ShippingList', $ShippingList);
        
        }

    /**
     * 受領確認（商品選択からの遷移の場合）
     */
    function conf(){
        //更新時間チェック用に画面を開いた時間を格納
        $this->Session->write('Receipt.readTime',date('Y-m-d H:i:s'));
        
        $sql  =' select ';
        $sql .='       b.id              as "TrnShipping__id" ';
        $sql .='     , b.modified        as "TrnShipping__modified" ';
        $sql .='     , a.id              as "TrnShipping__header_id" ';
        $sql .='     , e.id              as "TrnShipping__item_unit_id" ';
        $sql .='     , a.work_no         as "TrnShipping__work_no" ';
        $sql .="     , to_char(a.work_date , 'YYYY/mm/dd') ";
        $sql .='                         as "TrnShipping__work_date" ';
        $sql .='     , g.internal_code   as "TrnShipping__internal_code" ';
        $sql .='     , g.item_name       as "TrnShipping__item_name" ';
        $sql .='     , g.item_code       as "TrnShipping__item_code" ';
        $sql .='     , g.standard        as "TrnShipping__standard" ';
        $sql .='     , g.jan_code        as "TrnShipping__jan_code"';
        $sql .='     , substr(g.jan_code,0,13) ';
        $sql .='                         as "TrnShipping__pure_jan_code"';
        $sql .='     , i.id              as "TrnShipping__sticker_id" ';
        $sql .='     , j.stocking_price  as "TrnShipping__sales_price" ';
        $sql .='     , i.trade_type      as "TrnShipping__trade_type" ';
        $sql .='     , (  ';
        $sql .='       case  ';
        $sql .='         when d.per_unit = 1  ';
        $sql .='         then e.unit_name  ';
        $sql .="         else e.unit_name || '(' || d.per_unit || f.unit_name || ')'  ";
        $sql .='         end ';
        $sql .='     )                   as "TrnShipping__unit_name" ';
        $sql .='     , c.user_name       as "TrnShipping__user_name" ';
        $sql .='     , k.department_name as "TrnShipping__department_name" ';
        $sql .='     , h.dealer_name     as "TrnShipping__dealer_name" ';
        $sql .='     , (  ';
        $sql .='       case  ';
        
        foreach( Configure::read('ShippingTypes') as $k => $v){
            $sql .='         when b.shipping_type = ' . $k;
            $sql .="         then '" . $v ."'";
        }
        
        $sql .="         else ''  ";
        $sql .='         end ';
        $sql .='     )                   as "TrnShipping__item_type" ';
        $sql .='     , g';
        $sql .='   from ';
        $sql .='     trn_shipping_headers as a  ';
        $sql .='     left join trn_shippings as b  ';
        $sql .='       on a.id = b.trn_shipping_header_id  ';
        $sql .='     left join mst_users as c  ';
        $sql .='       on c.id = a.modifier  ';
        $sql .='     left join mst_item_units as d  ';
        $sql .='       on d.id = b.mst_item_unit_id  ';
        $sql .='     left join mst_unit_names as e  ';
        $sql .='       on e.id = d.mst_unit_name_id  ';
        $sql .='     left join mst_unit_names as f  ';
        $sql .='       on f.id = d.per_unit_name_id  ';
        $sql .='     left join mst_facility_items as g  ';
        $sql .='       on g.id = d.mst_facility_item_id  ';
        $sql .='     left join mst_dealers as h  ';
        $sql .='       on h.id = g.mst_dealer_id  ';
        $sql .='     left join trn_stickers as i  ';
        $sql .='       on i.id = b.trn_sticker_id  ';
        $sql .='     left join trn_orders as j  ';
        $sql .='       on j.id = i.trn_order_id  ';
        $sql .='     left join mst_departments as k  ';
        $sql .='       on k.id = a.department_id_to  ';
        $sql .='   where ';
        $sql .="     a.id in ('" . join("','" , $this->request->data['MstReceipts']['id']) . "')";
        $sql .="    and b.is_deleted = false ";
        $sql .="    and b.shipping_status < " . Configure::read('ShippingStatus.loaded');
        
        //前画面での絞り込み条件
        //部署：初期画面で部署が選択されている場合は検索条件に追加
        if($this->request->data['MstReceipts']['departmentCode'] != ""){
            $sql .= ' and k.department_code = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['departmentCode'])."' ";
        }
        
        //出荷番号(完全一致)-------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_shippings_no'])) and ($this->request->data['MstReceipts']['search_shippings_no'] != "")){
                $sql .= ' and a.work_no = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_shippings_no'])."' ";
        }
        
        //出荷日------------------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_start_date'])) and ($this->request->data['MstReceipts']['search_start_date'] != "")){
            //開始日と終了日の両方に入力あり
            if((isset($this->request->data['MstReceipts']['search_end_date'])) and ($this->request->data['MstReceipts']['search_end_date'] != "")){
                $sql .= ' and a.work_date >= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_start_date'])."' ";
                $sql .= ' and a.work_date <= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_end_date'])."' ";
                
                //開始日のみ入力あり
            } else {
                $sql .= ' and a.work_date >= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_start_date'])."' ";
            }
            //終了日のみ入力あり
        } else {
            if((isset($this->request->data['MstReceipts']['search_end_date'])) and ($this->request->data['MstReceipts']['search_end_date'] != "")){
                $sql .= ' and a.work_date <= ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_end_date'])."' ";
            }
        }
        //商品名(LIKE検索)-------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_item_name'])) and ($this->request->data['MstReceipts']['search_item_name'] != "")){
            $sql .= ' and g.item_name ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_item_name'])."%' ";
        }
        
        //商品ID(完全一致)------------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_internal_code'])) and ($this->request->data['MstReceipts']['search_internal_code'] != "")){
            $sql .= ' and g.internal_code = ' ."'".Sanitize::escape($this->request->data['MstReceipts']['search_internal_code'])."' ";
        }
        
        //規格(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_standard'])) and ($this->request->data['MstReceipts']['search_standard'] != "")){
            $sql .= ' and g.standard ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_standard'])."%' ";
            }
        
        //製品番号(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_item_code'])) and ($this->request->data['MstReceipts']['search_item_code'] != "")){
            $sql .= ' and g.item_code ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_item_code'])."%' ";
        }

        //販売元(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['MstReceipts']['search_dealer_name'])) and ($this->request->data['MstReceipts']['search_dealer_name'] != "")){
            $sql .= ' and h.dealer_name ILIKE ' ."'%".Sanitize::escape($this->request->data['MstReceipts']['search_dealer_name'])."%' ";
        }
        $sql .= '  order by ';
        $sql .= '    a.id ';
        $sql .= '    , g.mst_owner_id ';
        $sql .= '    , h.dealer_name ';
        $sql .= '    , g.item_name ';
        $sql .= '    , g.item_code ';
        $sql .= '    , i.id ';
        //**************************************************************************

        $result = $this->TrnShippingHeader->query($sql);
        if(empty($result)){
            // 空なのでリダイレクト
            $this->Session->setFlash('該当情報がありません。', 'growl', array('type'=>'error'));
            $this->redirect('index');
        }
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Receipts.token' , $token);
        $this->request->data['Receipts']['token'] = $token;
        $this->set('result', $result);
        $this->request->data['MstReceipts']['work_date'] = date('Y/m/d');

        $this->render('conf');
    }

    /**
     * 登録処理
     */
    function result ($mode=1) {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['Receipts']['token'] === $this->Session->read('Receipts.token')) {
            $this->Session->delete('Receipts.token');
            
            //トランザクション開始
            $this->TrnReceivingHeader->begin();

            //行ロック
            $sql = ' select * from trn_shippings as a where a.id in ('.join(',',$this->request->data['MstReceipts']['id']).') for update ';
            $this->TrnShipping->query($sql);

            //現在時間を取得
            $this->request->data['now'] = date('Y/m/d H:i:s');

            /* 更新時間チェック */
            foreach($this->request->data['MstReceipts']['id'] as $id){
                $sql = 'select count(*) as count from trn_shippings as a where a.id = ' . $id . " and a.modified = '" . $this->request->data['MstReceipts']['modified'][$id] . "'";
                $check = $this->TrnShipping->query($sql);
                if($check[0][0]['count'] == 0){
                    $this->TrnReceivingHeader->rollback();
                    $this->Session->setFlash('処理中のデータに更新がかけられました。最初からやり直してください。', 'growl', array('type'=>'error') );
                    $this->redirect('index');
                }
            }

            //登録処理を実施
            try{
                $this->createReceiptData();
            } catch (Exception $ex) {
                $this->TrnReceivingHeader->rollback();
                $this->Session->setFlash('受領登録処理中に失敗しました。', 'growl', array('type'=>'error') );
                $this->redirect('index');
            }

            //コミット
            $this->TrnReceivingHeader->commit();
            
            //完了画面遷移用に出荷データを再取得
            //******************************************************************************
            $sql  = ' select ';
            $sql .= '       a.id                       as "MstReceipts__id"';
            $sql .= '     , a.work_no                  as "MstReceipts__work_no"';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when a.shipping_type = ' . Configure::read('ShippingType.deposit');
            $sql .= '         then m.facility_name  ';
            $sql .= '         else j.department_name  ';
            $sql .= '         end ';
            $sql .= '     )                            as "MstReceipts__name" ';
            $sql .= "     , to_char(a.work_date,'YYYY/mm/dd')";
            $sql .= '                                  as "MstReceipts__work_date"';
            $sql .= '     , c.internal_code            as "MstReceipts__internal_code"';
            $sql .= '     , c.item_code                as "MstReceipts__item_code"';
            $sql .= '     , c.item_name                as "MstReceipts__item_name"';
            $sql .= '     , c.standard                 as "MstReceipts__standard"';
            $sql .= '     , c.jan_code                 as "MstReceipts__jan_code"';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when b.per_unit = 1  ';
            $sql .= '         then d.unit_name  ';
            $sql .= "         else d.unit_name || '(' || b.per_unit || e.unit_name || ')'  ";
            $sql .= '         end ';
            $sql .= '     )                            as "MstReceipts__unit_name" ';
            $sql .= '     , f.dealer_name              as "MstReceipts__dealer_name"';
            $sql .= '     , g.id                       as "MstReceipts__sticker_id"';
            $sql .= '     , g.lot_no                   as "MstReceipts__lot_no"';
            $sql .= "     , to_char(g.validated_date,'YYYY/mm/dd')";
            $sql .= '                                  as "MstReceipts__validate_date"';
            $sql .= '     , g.facility_sticker_no      as "MstReceipts__facility_sticker_no"';
            $sql .= '     , g.hospital_sticker_no      as "MstReceipts__hospital_sticker_no"';
            $sql .= '     , h.user_name                as "MstReceipts__user_name"';
            $sql .= '     , i.name                     as "MstReceipts__class_name" ';
            $sql .= '     , a.shipping_type ';
            $sql .= '     , (case ';
            foreach( Configure::read('ShippingTypes') as $k => $v ) {
                $sql .= "      when a.shipping_type = '" . $k . "' then '" . $v ."'";
            }
            $sql .= "       else '' ";
            $sql .= '       end )                      as "MstReceipts__type_name"';
            $sql .= '     , a.recital                  as "MstReceipts__recital"';
            $sql .= '     , k.trn_order_header_id      as "MstReceipts__trn_order_header_id"';
            $sql .= '     , n.sales_price              as "MstReceipts__sales_price"';
            $sql .= '   from ';
            $sql .= '     trn_shippings as a  ';
            $sql .= '     left join mst_item_units as b  ';
            $sql .= '       on b.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as c  ';
            $sql .= '       on c.id = b.mst_facility_item_id  ';
            $sql .= '     left join mst_unit_names as d  ';
            $sql .= '       on d.id = b.mst_unit_name_id  ';
            $sql .= '     left join mst_unit_names as e  ';
            $sql .= '       on e.id = b.per_unit_name_id  ';
            $sql .= '     left join mst_dealers as f  ';
            $sql .= '       on f.id = c.mst_dealer_id  ';
            $sql .= '     left join trn_stickers as g  ';
            $sql .= '       on g.id = a.trn_sticker_id  ';
            $sql .= '     left join mst_users as h  ';
            $sql .= '       on h.id = a.modifier  ';
            $sql .= '     left join mst_classes as i  ';
            $sql .= '       on i.id = a.work_type  ';
            $sql .= '     left join mst_departments as j  ';
            $sql .= '       on j.id = a.department_id_to  ';
            $sql .= '     left join trn_orders as k  ';
            $sql .= '       on k.id = g.trn_order_id  ';
            $sql .= '     left join mst_departments as l  ';
            $sql .= '       on l.id = k.department_id_to  ';
            $sql .= '     left join mst_facilities as m  ';
            $sql .= '       on m.id = l.mst_facility_id  ';
            $sql .= '     left join mst_sales_configs as n ';
            $sql .= '       on n.mst_item_unit_id = b.id ';
            $sql .='       and n.partner_facility_id = j.mst_facility_id  ';
            $sql .="       and n.start_date <= '".$this->request->data['MstReceipts']['work_date']."'";
            $sql .="       and n.end_date >  '".$this->request->data['MstReceipts']['work_date']."'";
            $sql .="       and n.is_deleted = false ";
            $sql .= '   where ';
            $sql .= '     a.id in ( ' . join(',', $this->request->data['MstReceipts']['id'] ) . '  )  ';
            $sql .= '   order by ';
            $sql .= '     a.work_no ';
            $sql .= '     , a.work_seq ';

            $result = $this->TrnShipping->query($sql);
            //**************************************************************************

            //セッション削除
            $this->Session->delete('Receipt.readTime');
            $this->request->data['result'] = $result;
            $this->Session->write('data', $this->request->data);
            $this->render('result');
        }else{
            $this->request->data = $this->Session->read('data');
            $this->render('result');
        }
    }


    //****************************************************************************
    /**
     * createReceiptData
     *
     * 受領時に必要な処理
     */
    function createReceiptData(){
        $now = $this->request->data['now'];
        // 明細情報取得
        $sql  = ' select ';
        $sql .= '       a.trn_shipping_header_id as id';
        $sql .= '     , a.department_id_to';
        $sql .= '     , a.department_id_from';
        $sql .= '     , count(*) as count ';
        $sql .= '     , sum( case when a.shipping_type = ' . Configure::read('ShippingType.edidirect') .' then 1 ';
        $sql .= '       else 0 ';
        $sql .= '       end ) as consume_count ';
        $sql .= '   from ';
        $sql .= '     trn_shippings as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',',$this->request->data['MstReceipts']['id']) . ') ';
        $sql .= '   group by a.department_id_to , ';
        $sql .= '            a.department_id_from ,';
        $sql .= '            a.trn_shipping_header_id ';
        $head = $this->TrnShipping->query($sql);
        foreach($head as $h){
            $receiving_work_no = $this->setWorkNo4Header($this->request->data['MstReceipts']['work_date'],'09');
            // 受領ヘッダ
            $TrnReceivingHeader = array(
                'TrnReceivingHeader' => array(
                    'work_no'                => $receiving_work_no,
                    'work_date'              => $this->request->data['MstReceipts']['work_date'],
                    'receiving_type'         => Configure::read('ReceivingType.receipt'), //入荷区分:受領による入荷
                    'receiving_status'       => Configure::read('ReceivingStatus.stock'), //入荷状態（受領済）
                    'department_id_from'     => $h[0]['department_id_from'],
                    'department_id_to'       => $h[0]['department_id_to'],
                    'detail_count'           => $h[0]['count'],                    //明細数
                    'trn_shipping_header_id' => $h[0]['id'],
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'modified'               => $now,
                    )
                );
            
            //TrnReceivingHeaderオブジェクトをcreate
            $this->TrnReceivingHeader->create();
            
            //SQL実行
            if (!$this->TrnReceivingHeader->save($TrnReceivingHeader)) {
                return false;
            }

            
            $receiving_header_id = $this->TrnReceivingHeader->getLastInsertID();//入荷ヘッダID
            // 消費ヘッダ
            if($h[0]['consume_count'] > 0){
                $consume_work_no = $this->setWorkNo4Header($this->request->data['MstReceipts']['work_date'],'01');
                //***************
                //消費ヘッダの登録
                //***************
                $TrnConsumeHeader = array(
                    'TrnConsumeHeader' => array(
                        'work_no'               => $consume_work_no,
                        'work_date'             => $this->request->data['MstReceipts']['work_date'],
                        'recital'               => $this->request->data['MstReceipts']['recital'][0],
                        'use_type'              => Configure::read('UseType.direct'), //消費区分：臨時
                        'mst_department_id'     => $h[0]['department_id_to'], //部署参照キー
                        'detail_count'          => $h[0]['consume_count'],
                        'creater'               => $this->Session->read('Auth.MstUser.id'),
                        'modifier'              => $this->Session->read('Auth.MstUser.id'),
                        'created'               => $now,
                        'modified'              => $now,
                        'is_deleted'            => false
                        )
                    );

                //TrnConsumeHeaderオブジェクトをcreate
                $this->TrnConsumeHeader->create();

                //SQL実行
                if (!$this->TrnConsumeHeader->save($TrnConsumeHeader)) {
                    return false;
                }
                //ヘッダの登録に成功した場合
                $consume_header_id = $this->TrnConsumeHeader->getLastInsertID();
            }
            
            // 明細情報取得
            $sql  = ' select ';
            $sql .= '       a.id ';
            $sql .= '     , a.trn_sticker_id ';
            $sql .= '     , b.trn_order_id ';
            $sql .= '     , b.trn_storage_id';
            $sql .= '     , a.mst_item_unit_id ';
            $sql .= '     , b.quantity';
            $sql .= '     , a.shipping_type';
            $sql .= '     , c.order_type';
            $sql .= '     , e.buy_flg';
            $sql .= '     , b.hospital_sticker_no';
            $sql .= '     , b.facility_sticker_no';
            $sql .= '     , a.department_id_to ';
            $sql .= '     , a.department_id_from ';
            $sql .= '     , b.transaction_price';
            $sql .= '     , b.sales_price';
            $sql .= '   from ';
            $sql .= '     trn_shippings as a  ';
            $sql .= '     left join trn_stickers as b  ';
            $sql .= '       on b.id = a.trn_sticker_id  ';
            $sql .= '     left join trn_orders as c ';
            $sql .= '       on c.id = b.trn_order_id ';
            $sql .= '     left join mst_item_units as d ';
            $sql .= '       on d.id = a.mst_item_unit_id ';
            $sql .= '     left join mst_facility_items as e ';
            $sql .= '       on e.id = d.mst_facility_item_id ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',',$this->request->data['MstReceipts']['id']) . ') ';
            $sql .= '     and a.trn_shipping_header_id = ' . $h[0]['id'];

            $data = $this->TrnShipping->query($sql);

            foreach($data as $d){
                $consume_seq = 1;
                $receiving_seq = 1;
                $SalesId = null;
                // 売上データ作成
                // 臨時非在庫、業者直納
                if(( $d[0]['shipping_type'] == Configure::read('ShippingType.edidirect') ||
                     $d[0]['order_type'] == Configure::read('OrderTypes.ExNostock') ) &&
                     $d[0]['buy_flg'] == false ) {
                    $sales_price = null;
                    if(isset($this->request->data['MstReceipts']['sales_price'][$d[0]['id']])){
                        $sales_price = $this->request->data['MstReceipts']['sales_price'][$d[0]['id']];
                    }
                    $SalesId = $this->createSalesData($d[0]['trn_sticker_id'],$d[0]['department_id_to'],$this->request->data['MstReceipts']['work_date'], $sales_price);
                }
                // 仕入データ作成
                // 非在庫、臨時非在庫、業者直納
                $this->createStocking($h[0]['id'], $d[0]['id']);
                // 病院在庫増
                // 預託、非在庫、臨時費在庫
                if($d[0]['shipping_type'] == Configure::read('ShippingType.nostock') ||
                   $d[0]['shipping_type'] == Configure::read('ShippingType.deposit')
                   ){
                    if( $d[0]['order_type'] == Configure::read('OrderTypes.ExNostock')){
                        $requested_count = 0;
                    }else{
                        $requested_count = $d[0]['quantity'];
                    }
                    
                    $this->TrnStock->create();
                    $c = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.stock_count'     => 'TrnStock.stock_count + ' . $d[0]['quantity'],
                            'TrnStock.requested_count' => 'TrnStock.requested_count - ' . $requested_count ,
                            'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'        => "'" . $now . "'"
                            ),
                        array(
                            'TrnStock.id' => $this->getStockRecode($d[0]['mst_item_unit_id'] ,
                                                                   $d[0]['department_id_to']),
                            ),
                        -1
                        );
                    if(!$c){
                        return false;
                    }
                }
                
                // 発注データ更新残数▼
                // 預託、非在庫、臨時非在庫
                if($d[0]['shipping_type'] == Configure::read('ShippingType.nostock') ||
                   $d[0]['shipping_type'] == Configure::read('ShippingType.deposit')
                   ){
                    $this->TrnOrder->create();
                    $c = $this->TrnOrder->updateAll(
                        array(
                            'TrnOrder.remain_count' => 'TrnOrder.remain_count - ' . $d[0]['quantity'],
                            'TrnOrder.modifier'    => $this->Session->read('Auth.MstUser.id'),
                            'TrnOrder.modified'    => "'" . $now . "'"
                            ),
                        array(
                            'TrnOrder.id' => $d[0]['trn_order_id']
                            ),
                        -1
                        );
                    if(!$c){
                        return false;
                    }
                }
                
                // 出荷データ更新ステータス更新
                $this->TrnShipping->create();
                $c = $this->TrnShipping->updateAll(
                    array(
                        'TrnShipping.shipping_status' => Configure::read('ShippingStatus.loaded'),
                        'TrnShipping.modifier'    => $this->Session->read('Auth.MstUser.id'),
                        'TrnShipping.modified'    => "'" . $now . "'"
                        ),
                    array(
                        'TrnShipping.id' => $d[0]['id']
                        ),
                    -1
                    );
                if(!$c){
                    return false;
                }

                // 受領データ作成
                $TrnReceiving = array(
                    'TrnReceiving' => array(
                        'work_no'                 => $receiving_work_no,
                        'work_seq'                => $receiving_seq,
                        'work_date'               => $this->request->data['MstReceipts']['work_date'],
                        'receiving_type'          => Configure::read('ReceivingType.receipt'),    //入荷区分:受領による入荷
                        'receiving_status'        => Configure::read('ShippingStatus.loaded'),    //入荷状態（受領済み）
                        'mst_item_unit_id'        => $d[0]['mst_item_unit_id'],                 //包装単位参照キー
                        'department_id_from'      => $d[0]['department_id_from'],
                        'department_id_to'        => $d[0]['department_id_to'],
                        'quantity'                => $d[0]['quantity'],                         //数量
                        'trade_type'              => Configure::read('ClassesType.Temporary') , //臨時品固定
                        'stocking_price'          => $d[0]['transaction_price'],                //仕入れ価格
                        'sales_price'             => $d[0]['sales_price'],                      //売上価格
                        'trn_sticker_id'          => $d[0]['trn_sticker_id'],                   //シール参照キー
                        'trn_shipping_id'         => $d[0]['id'],                               //出荷参照キー
                        'trn_order_id'            => $d[0]['trn_order_id'],                     //発注参照キー
                        'trn_receiving_header_id' => $receiving_header_id,                      //入荷ヘッダ外部キー
                        'facility_sticker_no'     => $d[0]['facility_sticker_no'],
                        'hospital_sticker_no'     => $d[0]['hospital_sticker_no'],
                        'creater'                 => $this->Session->read('Auth.MstUser.id'),
                        'modifier'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                 => $now,
                        'modified'                => $now,
                        )
                    );

                //TrnReceivingオブジェクトをcreate
                $this->TrnReceiving->create();

                if (!$this->TrnReceiving->save($TrnReceiving)) {
                    return false;
                }
                
                $receiving_seq++;
                $receiving_id = $this->TrnReceiving->getLastInsertID();
                // シール移動履歴（受領）
                $TrnStickerRecord = array(
                    'TrnStickerRecord'=> array(
                        'move_date'           => $this->request->data['MstReceipts']['work_date'],
                        'move_seq'            => $this->getNextRecord($d[0]['trn_sticker_id']),
                        'record_type'         => Configure::read('RecordType.receipts'),
                        'record_id'           => $receiving_id,
                        'trn_sticker_id'      => $d[0]['trn_sticker_id'],
                        'mst_department_id'   => $d[0]['department_id_to'],
                        'facility_sticker_no' => $d[0]['facility_sticker_no'],
                        'hospital_sticker_no' => $d[0]['hospital_sticker_no'],
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        )
                    );
                $this->TrnStickerRecord->create();
                if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                    return false;
                }
                $consume_id = null;
                // 消費データ作成
                if($d[0]['shipping_type'] == Configure::read('ShippingType.edidirect') ){
                    // 直納（無条件）
                    $TrnConsume = array(
                        'TrnConsume'=> array(
                            'work_no'               => $consume_work_no,
                            'work_seq'              => $consume_seq,
                            'work_date'             => $this->request->data['MstReceipts']['work_date'],
                            'use_type'              => Configure::read('UseType.direct'),  //消費区分：直納品
                            'mst_item_unit_id'      => $d[0]['mst_item_unit_id'],          //包装単位参照キー
                            'mst_department_id'     => $d[0]['department_id_to'],          //部署参照キー
                            'quantity'              => 1,                                  //数量
                            'trn_sticker_id'        => $d[0]['trn_sticker_id'],            //シール参照キー
                            'trn_storage_id'        => $d[0]['trn_storage_id'],            //入庫参照キー
                            'trn_consume_header_id' => $consume_header_id,                 //消費ヘッダ外部キー
                            'is_retroactable'       => false,
                            'creater'               => $this->Session->read('Auth.MstUser.id'),
                            'created'               => $now,
                            'modifier'              => $this->Session->read('Auth.MstUser.id'),
                            'modified'              => $now,
                            'is_deleted'            => false,
                            'facility_sticker_no'   => $d[0]['facility_sticker_no'],
                            'hospital_sticker_no'   => $d[0]['hospital_sticker_no'],
                            )
                        );
                    $this->TrnConsume->create(false);
                    if (!$this->TrnConsume->save($TrnConsume)) {
                        return false;
                    }
                    
                    $d[0]['quantity'] = 0;
                    $consume_seq++;
                    $consume_id = $this->TrnConsume->getLastInsertID();
                    // シール移動履歴（消費）
                    $TrnStickerRecord = array(
                        'TrnStickerRecord'=> array(
                            'move_date'           => $this->request->data['MstReceipts']['work_date'],
                            'move_seq'            => $this->getNextRecord($d[0]['trn_sticker_id']),
                            'record_type'         => Configure::read('RecordType.direct'),
                            'record_id'           => $consume_id,
                            'trn_sticker_id'      => $d[0]['trn_sticker_id'],
                            'mst_department_id'   => $d[0]['department_id_to'],
                            'facility_sticker_no' => $d[0]['facility_sticker_no'],
                            'hospital_sticker_no' => $d[0]['hospital_sticker_no'],
                            'is_deleted'          => false,
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now,
                            )
                        );
                    $this->TrnStickerRecord->create();
                    if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                        return false;
                    }
                }
                if(empty($this->request->data['MstReceipts']['validated_date'][$d[0]['id']])){
                    $validated_date = null;
                }else{
                    $validated_date = "'" . $this->request->data['MstReceipts']['validated_date'][$d[0]['id']] . "'";
                }
                // シール情報更新
                $this->TrnSticker->create();
                $ret = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.mst_department_id' => $d[0]['department_id_to'],
                        'TrnSticker.trn_consume_id'    => $consume_id,
                        'TrnSticker.receipt_id'        => $receiving_id,
                        'TrnSticker.sale_claim_id'     => $SalesId,
                        'TrnSticker.quantity'          => $d[0]['quantity'],
                        'TrnSticker.last_move_date'    => "'" . $this->request->data['MstReceipts']['work_date'] . "'",
                        'TrnSticker.is_deleted'        => "'false'",
                        'TrnSticker.modifier'          => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'          => "'" . $now . "'"
                        ),
                    array(
                        'TrnSticker.id' => $d[0]['trn_sticker_id'],
                        ),
                    -1
                    );
                if(!$ret){
                    return false;
                }
            }
        }
        return true;
    }
    //****************************************************************************

    function seal($type=1){
        if($type == 1){
            // 部署シールの場合
            $order = 'MstDepartment.department_name asc, MstShelfNameTo.code asc';
            $data = $this->Stickers->getHospitalStickers($this->request->data['TrnSticker']['hospital_sticker_no'], $order,2);

            $this->set('sortkey' , '病院情報,センター棚番号,商品ID');
            $this->set('Sticker' , $data);
            $this->layout = 'xml/default';
            $this->render('/stickerissues/hospital_sticker');

        }else if($type == 2){
            // コストシールの場合
            $records = $this->Stickers->getCostStickers($this->request->data['TrnSticker']['id']);
            // 空なら初期化
            if(!$records){ $records = array();}
            
            $this->layout = 'xml/default';
            $this->set('records', $records);
            $this->render('/stickerissues/cost_sticker');
        }
    }


    function createStocking($trn_shipping_header_id , $trn_shipping_id) {
        $sql  = ' select ';
        $sql .= '     a.id ';
        $sql .= '   , a.mst_item_unit_id ';
        $sql .= '   , c.department_id_from ';
        $sql .= '   , c.department_id_to ';
        $sql .= '   , b.trn_receiving_id ';
        $sql .= '   , a.quantity ';
        $sql .= '   , e.gross ';
        $sql .= '   , e.round ';
        $sql .= '   , xx.transaction_price  ';
        $sql .= '   , b.id as sticker_id';
        $sql .= '   , b.hospital_sticker_no  ';
        $sql .= '   , b.facility_sticker_no  ';
        $sql .= ' from ';
        $sql .= '   trn_shippings as a  ';
        $sql .= '   left join trn_stickers as b  ';
        $sql .= '     on b.trn_shipping_id = a.id  ';
        $sql .= '   left join trn_orders as c  ';
        $sql .= '     on c.id = b.trn_order_id  ';
        $sql .= '   left join mst_departments as d  ';
        $sql .= '     on d.id = c.department_id_to  ';
        $sql .= '   left join mst_facilities as e  ';
        $sql .= '     on e.id = d.mst_facility_id  ';
        $sql .= '   left join mst_transaction_configs as xx  ';
        $sql .= '     on xx.partner_facility_id = e.id  ';
        $sql .= '     and xx.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '     and xx.start_date <= c.work_date  ';
        $sql .= '     and xx.end_date > c.work_date  ';
        $sql .= ' where ';
        $sql .= '   a.id = ' . $trn_shipping_id;
        $sql .= '   and a.trn_shipping_header_id = ' . $trn_shipping_header_id;
        $sql .= '   and a.shipping_type in( ' . Configure::read('ShippingType.nostock')  . ' , ' . Configure::read('ShippingType.edidirect') . ')';
        $ret = $this->TrnShipping->query($sql);
        foreach($ret as $r){
            $TrnClaim = array(
                'TrnClaim'=> array(
                    'department_id_from' => $r[0]['department_id_to'],   //請求元参照キー
                    'department_id_to'   => $r[0]['department_id_from'], //請求先参照キー
                    'mst_item_unit_id'   => $r[0]['mst_item_unit_id'],   //包装単位参照キー
                    'claim_date'         => $this->request->data['MstReceipts']['work_date'],//請求年月
                    'count'              => $r[0]['quantity'],           //請求数
                    'unit_price'         => $r[0]['transaction_price'],  //請求単価
                    'round'              => $r[0]['round'],              //丸め区分
                    'gross'              => $r[0]['gross'],              //まとめ区分
                    'is_not_retroactive' => false,                       //遡及除外フラグ
                    'claim_price'        => $this->getFacilityUnitPrice($r[0]['round'],$r[0]['transaction_price'],$r[0]['quantity']),  //請求金額
                    'trn_receiving_id'   => $r[0]['trn_receiving_id'],   //入荷参照キー
                    'trn_shipping_id'    => $r[0]['id'],                 //出荷参照キー
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'created'            => $this->request->data['now'],
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'modified'           => $this->request->data['now'],
                    'is_stock_or_sale'   => Configure::read('Claim.stock'),  //仕入売上判定フラグ 1:仕入;2:売上
                    'is_deleted'         => false,
                    'stocking_close_type'=> 0,                           //仕入締め区分
                    'sales_close_type'   => 0,                           //売上締め区分
                    'facility_close_type'=> 0,                           //在庫締め区分
                    'facility_sticker_no'=> $r[0]['facility_sticker_no'],
                    'hospital_sticker_no'=> $r[0]['hospital_sticker_no'],
                    'claim_type'         => Configure::read('ClaimType.stock')
                    )
                );
            
            $this->TrnClaim->create();
            // SQL実行
            if (!$this->TrnClaim->save($TrnClaim)) {
                return false;
            }
            $buy_claim_id = $this->TrnClaim->getLastInsertID();//シールテーブル更新用　仕入請求参照キー
            $this->createMsClaimRecode($buy_claim_id);
            $this->createSaleClaimRecode($buy_claim_id);
            
            $this->TrnSticker->create();
            $ret = $this->TrnSticker->updateAll(
                array(
                    'TrnSticker.buy_claim_id' => $buy_claim_id,
                    ),
                array(
                    'TrnSticker.id' => $r[0]['sticker_id']
                    ),
                -1
                );
            if(!$ret) {
                return false;
            }
        }
        return true;
    }
}


