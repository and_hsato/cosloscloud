<?php
/**
 * PasswordController
 *
 * @version 1.0.0
 * @since 2016/01/28
 */


class PasswordController extends AppController {
    
    /**
     * @var $name
     */
    var $name = 'password';
    var $uses = array('MstUser');
    
    public $components = array('Cookie');
    
    public function beforeFilter() {
        //ログインを必要としない処理の設定
        $this->Auth->allow('index', 'lost', 'secret', 'checkCode', 'changePassword', 'success');
        
        // 必ずログアウト状態で処理する
        $this->Auth->logout();
        $this->Session->delete('Auth');
        
        $mstUser = $this->getTargetUserFromSession();
        $errorMessage = 'セッションの有効期限が切れました。最初からやり直してください。';
        if ($this->action !== 'index' && $this->action !== 'lost' && $this->action !== 'success') {
            // セッション情報の確認
            if (empty($mstUser)) {
                // セッションからMstUserを取得できな場合は処理を続行できないため最初の画面へ
                $this->Session->setFlash($errorMessage, 'growl', array('type' => 'error'));
                $this->redirect('index');
            }
        }
        
        if ($this->action == 'checkCode') {
            // クッキーから確認コードを取得できない場合は処理を続行できないため最初の画面へ
            $checkCodeFromCookie = $this->getCheckCodeFromCookie();
            if (empty($checkCodeFromCookie)) {
                $this->Session->setFlash($errorMessage, 'growl', array('type' => 'error'));
                $this->redirect('index');
            }
        }
        
        if ($this->action == 'changePassword') {
            // 本人確認済みか？
            if (empty($mstUser['check_code_successful'])) {
                // 本人確認が済んでいない場合は処理を続行できないため最初の画面へ
                $this->Session->setFlash($errorMessage, 'growl', array('type' => 'error'));
                $this->redirect('index');
            }
        }
        
        if ($this->action == 'success') {
            // 完了本人確認済みか？
            if (!empty($mstUser)) {
                // キャッシュがクリアされていない場合は完了していないため最初の画面へ
                $this->Session->setFlash($errorMessage, 'growl', array('type' => 'error'));
                $this->redirect('index');
            }
        }
        
    }
    
    public function index() {
        $this->clearCache();
        $this->redirect('lost');
    }
    
    public function lost() {
        // 初回表示の場合はここで終了
        if (empty($this->request->data['MstUser']['login_id'])) return;

        // 入力されたメールアドレスからMstUserを取得
        $loginId = $this->request->data['MstUser']['login_id'];
        $result = $this->MstUser->find('first',
                array('conditions' => array('login_id' => $loginId), 'recursive' => -1));
        if (empty($result['MstUser'])) {
            $this->Session->setFlash('正しいメールアドレスを入力してください。',
                    'growl', array('type' => 'error'));
            return;
        }
        
        $mstUser = $result['MstUser'];
        //子ユーザは利用不可
        if ($this->AuthSession->isChildUserBy($mstUser)) {
            $this->Session->setFlash('子ユーザのためこの機能は利用できません。',
                    'growl', array('type' => 'error'));
            return;
        }
        
        // MstUserは以降の画面で必須となるのでセッションにセット
        $this->setTargetUserToSession($mstUser);
        $this->redirect('secret');
    }
    
    public function secret() {
        // 初回表示の場合
        if (empty($this->request->data['MstUser']['secret_a'])) {
            $mstUser = $this->getTargetUserFromSession();
            $secret_q = $mstUser['secret_q'];
            if (empty($secret_q)) {
                $questions = Configure::read('secretQuestions');
                $secret_q = $questions[0];
            }
            $this->set('secret_q', $secret_q);
            return;
        }

        // 入力された秘密の答えを検証
        $reqSecretA = $this->request->data['MstUser']['secret_a'];
        $mstUser = $this->getTargetUserFromSession();
        $secretA = $mstUser['secret_a'];
        if (empty($secretA)) {
            $secretA = $mstUser['user_name'];
        }
        if (strcmp($secretA, $reqSecretA) !== 0) {
            $this->Session->setFlash('正しい答えを入力してください。',
                                     'growl', array('type' => 'error'));
            return;
        }
        
        // 秘密の答えの検証が成功した場合は確認コードを発行
        $checkCode = $this->createCheckCode();
        
        //TODO: 確認コードをメールで送信するための処理を入れる
        $res = true;
        $email = null;
        App::uses('CakeEmail', 'Network/Email');
        try {
            $email = new CakeEmail('and');
        } catch (Exception $e) {
            // 現状は暫定実装なので Exceptionがスローされてもエラーにしない
            $this->log($e->getMessage());
        }
        try {
            if (!empty($email)) {
                $email->subject('【コスロスクラウド】確認コード送信');
                $email->to($mstUser['login_id']);
                $email->send('確認コード： ' . $checkCode);
            }
        } catch (Exception $e) {
            $this->log($e->getMessage());
            $res = false;
        }
        // ここまで暫定実装
        
        if (!$res) {
            $this->Session->setFlash('確認コードのメール送信に失敗しました。最初からやり直してください。',
                    'growl', array('type' => 'error'));
            $this->redirect('index');
        }
        
        // 確認コードをセッション、およびクッキーに保存 (確認コード入力画面で本人確認を行うために使用する)
        $mstUser['check_code'] = $checkCode;
        $this->setTargetUserToSession($mstUser);
        $this->setCheckCodeToCookie($checkCode);
        
        $this->redirect('checkCode');
    }
    
    public function checkCode() {
        // 初回表示の場合
        if (empty($this->request->data['MstUser']['check_code'])) {
            $mstUser = $this->Session->read('pwdRemainderTargetUser');
            $this->set('mstUser', $mstUser);
            return;
        }
        
        // 確認コード検証のための情報取得
        $mstUser = $this->getTargetUserFromSession();
        $checkCodeFromSession = $mstUser['check_code'];
        $checkCodeFromCookie = $this->getCheckCodeFromCookie();
        $checkCodeFromRequest = $this->request->data['MstUser']['check_code'];
        
        // リクエストデータ、セッションデータ、クッキーデータの3つの情報で検証する
        if (strcmp($checkCodeFromCookie, $checkCodeFromRequest) !== 0 ||
                strcmp($checkCodeFromSession, $checkCodeFromRequest) !== 0) {
            $this->Session->setFlash('正しい確認コードを入力してください。',
                    'growl', array('type' => 'error'));
            return;
        }
        
        // 本人確認成功
        $mstUser['check_code_successful'] = true;
        $this->setTargetUserToSession($mstUser);
        
        $this->redirect('changePassword');
    }
    
    public function changePassword() {
        // 初回表示の場合はここで終了
        if (empty($this->request->data['MstUser']['password'])) return;
        
        $mstUser = $this->getTargetUserFromSession();
        $password = $this->request->data['MstUser']['password'];
        $mstUser['password'] = $password;
        
        // トランザクションを開始
        $this->MstUser->begin();
        if(!$this->MstUser->save($mstUser)){
            // ロールバック
            $this->MstUser->rollback();
            // エラーメッセージ
            $this->Session->setFlash('パスワードの更新に失敗しました。',
                    'growl', array('type' => 'error'));
            return;
        }
        // トランザクションを終了
        $this->MstUser->commit();
        $this->Session->setFlash('パスワードを更新しました。',
                'growl', array('type' => 'star'));
        
        // 更新成功後はキャッシュをクリア
        $this->clearCache();
        
        $this->redirect('success');
    }
    
    public function success() {
    }
    
    
    private $SESSION_TARGET_USER = 'pwdRemainderTargetUser';
    private function setTargetUserToSession($mstUser) {
        $this->Session->write($this->SESSION_TARGET_USER, $mstUser);
    }

    private function getTargetUserFromSession() {
        $mstUser = $this->Session->read($this->SESSION_TARGET_USER);
        if (empty($mstUser)) return null;
        
        return $mstUser;
    }
    
    private function deleteTargetUserFromSession() {
        if (!$this->Session->check($this->SESSION_TARGET_USER)) return;
        $this->Session->delete($this->SESSION_TARGET_USER);
    }
    
    
    private $COOKIE_CHECK_CODE = 'code';
    private $COOKIE_CHECK_CODE_EXPIRES = 900; // 15分
    
    private function setCheckCodeToCookie($checkCode) {
        $this->Cookie->path = Router::url( '/') . $this->name;
        $this->Cookie->write($this->COOKIE_CHECK_CODE, $checkCode, true, $this->COOKIE_CHECK_CODE_EXPIRES);
    }

    private function getCheckCodeFromCookie() {
        $checkCode = $this->Cookie->read($this->COOKIE_CHECK_CODE);
        if (empty($checkCode)) return null;
        
        return $checkCode;
    }
    
    private function deleteCheckCodeFromCookie() {
        if (!$this->Cookie->check($this->COOKIE_CHECK_CODE)) return;
        $this->Cookie->delete($this->COOKIE_CHECK_CODE);
    }
    
    
    private function clearCache() {
        $this->deleteTargetUserFromSession();
        $this->deleteCheckCodeFromCookie();
    }
    
    private function createCheckCode() {
        return rand(100000, 999999);
    }
    
    
}
