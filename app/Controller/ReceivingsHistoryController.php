<?php
/**
 * 入荷履歴
 * ReceivingsHistoryController
 * @since 2010/08/30
 * @version 1.0.0
 */
class ReceivingsHistoryController extends AppController {
    var $name = 'ReceivingsHistory';
    var $uses = array(
        'TrnReceiving',
        'TrnReceivingHeader',
        'MstFacility',
        'MstClass',
        'TrnOrder',
        'TrnOrderHeader',
        'TrnClaim',
        'MstTransactionConfig',
        'TrnSticker',
        'TrnStock',
        'TrnStorage',
    );

    /**
     *
     * @var array  $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    /**
     * @var array $components
    */
    var $components = array('CsvWriteUtils');


    public function beforeFilter(){
        parent::beforeFilter();
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }


    public function index(){
        $this->setRoleFunction(22); //入荷履歴
        $this->redirect('search');
    }

    public function search(){
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        
        //仕入先一覧
        $_facilities = $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                              array(Configure::read('FacilityType.supplier'))
                                              );
        $this->set('facilities', $_facilities);

        //発注区分
        $this->set('orderTypes', Configure::read('OrderTypes.orderTypeName'));

        $_classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'09');
        $this->set('classes', $_classes);

        $_SearchResult = array();

        if(isset($this->request->data['IsSearch'])){
            App::import('Sanitize');
            $_to = $_rh = $_rs = $_fi = $_md = $_mf = $_tc = array();

            //conditions for TrnOrder
            if($this->request->data['TrnOrder']['work_no'] != ''){
                $_to['work_no LIKE '] = "%" . Sanitize::escape($this->request->data['TrnOrder']['work_no']) . "%";
            }

            if($this->request->data['TrnOrder']['work_date_from'] != ''){
                $_to['work_date >= '] = date('Y/m/d', strtotime($this->request->data['TrnOrder']['work_date_from']));
            }

            if($this->request->data['TrnOrder']['work_date_to'] != ''){
                $_to['work_date < '] = date('Y/m/d', strtotime('1 day', strtotime($this->request->data['TrnOrder']['work_date_to'])));
            }

            if($this->request->data['TrnOrder']['order_type'] != ''){
                $_to['order_type'] = $this->request->data['TrnOrder']['order_type'];
            }

            //conditions for TrnReceivinHeader
            if($this->request->data['TrnReceivingHeader']['work_no'] != ''){
                $_rh['work_no LIKE '] = "%" . Sanitize::escape($this->request->data['TrnReceivingHeader']['work_no']) . "%";
            }

            if($this->request->data['TrnReceivingHeader']['work_date_from'] != ''){
                $_rh['work_date >= '] = date('Y/m/d', strtotime($this->request->data['TrnReceivingHeader']['work_date_from']));
            }

            if($this->request->data['TrnReceivingHeader']['work_date_to'] != ''){
                $_rh['work_date < '] = date('Y/m/d', strtotime('1 day', strtotime($this->request->data['TrnReceivingHeader']['work_date_to'])));
            }


            //conditions for TrnReceiving
            if($this->request->data['TrnReceiving']['work_type'] != ''){
                $_rs['work_type'] = $this->request->data['TrnReceiving']['work_type'];
            }

            if($this->request->data['showDeleted'] == '0'){
                $_rs['is_deleted'] = 'false';
            }

            //conditions for MstFacilityItem
            if($this->request->data['MstFacilityItem']['internal_code'] != ''){
                $_fi['internal_code'] = Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']);
            }

            if($this->request->data['MstFacilityItem']['item_code'] != ''){
                $_fi['item_code LIKE '] = "%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_code']) . "%";
            }

            if($this->request->data['MstFacilityItem']['item_name'] != ''){
                $_fi['item_name LIKE '] = "%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%";
            }

            if($this->request->data['MstFacilityItem']['standard'] != ''){
                $_fi['standard LIKE '] = "%" . Sanitize::escape($this->request->data['MstFacilityItem']['standard']) . "%";
            }

            if($this->request->data['MstFacilityItem']['jan_code'] != ''){
                $_fi['jan_code'] = Sanitize::escape($this->request->data['MstFacilityItem']['jan_code']);
            }

            //conditions for MstDealer
            if($this->request->data['MstDealer']['dealer_name'] != '') {
                $_md['dealer_name LIKE '] = "%" . Sanitize::escape($this->request->data['MstDealer']['dealer_name']) . "%";
            }

            //conditions for MstFacility
            if($this->request->data['FacilityCodeInput'] != ''){
                $_mf['facility_code'] = Sanitize::escape($this->request->data['FacilityCodeInput']);
            }

            if($this->request->data['hasDifference'] == '1'){
                $_tc['unit_price <> '] =
                    '(select TC.transaction_price from mst_transaction_configs as TC ' .
                    ' where TC.is_deleted = false and TC.start_date <= cast(TrnOrder.work_date as date) and TC.end_date > cast(TrnOrder.work_date as date) + 1 and TC.mst_item_unit_id = MstItemUnit.id limit 1) or '.
                    '(select TC.transaction_price from mst_transaction_configs as TC ' .
                    ' where TC.is_deleted = false and TC.start_date <= cast(TrnOrder.work_date as date) and TC.end_date > cast(TrnOrder.work_date as date) + 1 and TC.mst_item_unit_id = MstItemUnit.id limit 1) is null ';
            }

            $_limit = $this->_getLimitCount();
            $_params = $this->getSearchParamater($_to, $_rh, $_rs, $_fi, $_md, $_mf, $_tc);
            //表示データ取得
            $_res = $this->getReceivingHeaders($_params,$_limit);
            foreach($_res as $_row){
                $_r = array();
                array_walk_recursive($_row, array('ReceivingsHistoryController','BuildSearchResult'), &$_r);
                $_SearchResult[] = $_r;
            }
        }else{
            $this->request->data['TrnReceivingHeader']['work_date_from'] = date('Y/m/d', strtotime('-7 day', time()));
            $this->request->data['TrnReceivingHeader']['work_date_to'] = date('Y/m/d', time());
        }
        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set('SearchResult', $_SearchResult);
        $this->set('data', $this->request->data);
    }

    public function confirm(){
        $_headerCondition = array();
        foreach($this->request->data['TrnReceivingHeader']['Rows'] as $_key=>$_value){
            if($_value == '1'){
                $_headerCondition[] = $_key;
            }
        }

        $_to = $_rs = $_fi = $_md = $_mf = $_tc = array();
        $_param = $this->_getReceivingDetailParam();

        if (array_key_exists('to', $_param)) {
            $_to = $_param['to'];
        }

        if (array_key_exists('rs', $_param)) {
            $_rs = $_param['rs'];
        }

        if (array_key_exists('fi', $_param)) {
            $_fi = $_param['fi'];
        }

        if (array_key_exists('md', $_param)) {
            $_md = $_param['md'];
        }

        if (array_key_exists('tc', $_param)) {
            $_tc = $_param['tc'];
        }

        $_params = $this->getSearchParamater($_to, array('id' => $_headerCondition), $_rs, $_fi, $_md, $_mf, $_tc);
        $this->Session->write('TrnReceiving.preConditions', $_params);
        $_SearchResult = $this->getReceivings($_params);
        $this->Session->write('TrnReceiving.preSerach', $_SearchResult);

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnReceiving.token' , $token);
        $this->request->data['TrnReceiving']['token'] = $token;

        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set('SearchResult', $_SearchResult);
        $this->set('IsShowFinal', true);
        $this->render('confirm');
    }

    public function results(){
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['TrnReceiving']['token'] === $this->Session->read('TrnReceiving.token')) {
            $this->Session->delete('TrnReceiving.token');
            $now = date('Y/m/d H:i:s');
            $_conditions = array();

            //更新の有無を確認
            $_preConditions = $this->Session->read('TrnReceiving.preConditions');
            $_nowResult = $this->getReceivings($_preConditions);
            if(true === $this->hasModifyDifference($_nowResult, $this->Session->read('TrnReceiving.preSerach'))){
                //$this->render('/receivings_history/confirm');
                $this->set('IsShowFinal', false);
                $this->request->data['IsSearch'] = '0';
                $this->set('data', $this->request->data);
                $this->search();
                return;
            }

            
            $targetDateAry = array();
            foreach($this->request->data['TrnReceiving']['Rows'] as $_key => $_value){
                if($_value['selected'] == '1'){
                    $_conditions[] = $_key;
                    $params = array();
                    $params['conditions'] = array('TrnReceiving.id'=>$_key);
                    $params['recursive']  = -1;
                    $present_c = $this->TrnReceiving->find('first',$params);
                    $targetDateAry[] = $present_c['TrnReceiving']['work_date'];
                }
            }

            
            //仮締更新権限チェック
            //[選択した明細を全チェックする]
            $type = Configure::read('ClosePrivileges.update');
            if(false === $this->canUpdateAfterCloseByDateAry($type ,$targetDateAry, $this->Session->read('Auth.facility_id_selected'))){
                $this->redirect('search');
                return;
            }

            //入荷キーを確保しておく
            $receivingId = $_conditions;

            $_params = $this->getSearchParamater(array(), array(), array('id' => $_conditions));
            $_res = $this->getReceivings($_params);
            $this->set('SearchResult', $_res);

            //更新処理
            $this->TrnReceiving->begin();

            $_receivingHeaderCondition = $_orderHeaderCondition = array();
            foreach($_res as $_row){
                //trn_receivingの更新
                if($_row['remain_count'] === $_row['quantity'] ||
                   $_row['order_type'] == Configure::read('OrderTypes.opestock') ){
                    $this->TrnReceiving->updateAll(array(
                        'TrnReceiving.is_deleted' => 'true',
                        'TrnReceiving.modified'   => "'" . $now . "'",
                        'TrnReceiving.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        ),array(
                            'TrnReceiving.id' => $_row['id'],
                            ), -1);
                }else{
                    $this->TrnReceiving->updateAll(array(
                        'TrnReceiving.quantity'     => "TrnReceiving.quantity - " . $_row['remain_count'],
                        'TrnReceiving.remain_count' => 0,
                        'TrnReceiving.modified'     => "'" . $now . "'",
                        'TrnReceiving.modifier'     => $this->Session->read('Auth.MstUser.id'),
                        ),array(
                            'TrnReceiving.id' => $_row['id'],
                            ), -1);
                }
                if($_row['order_type'] == Configure::read('OrderTypes.opestock')){
                    $remain_count = ((integer)$_row['trn_order_remain_count'] + (integer)$_row['quantity']);
                }else{
                    $remain_count = ((integer)$_row['trn_order_remain_count'] + (integer)$_row['remain_count']);
                }

                //trn_orderの更新
                $this->TrnOrder->updateAll(array(
                    'TrnOrder.remain_count' => $remain_count,
                    'TrnOrder.order_status' => ((integer)$_row['trn_order_quantity'] === $remain_count ? Configure::read('OrderStatus.ordered') : Configure::read('OrderStatus.partLoaded')),
                    'TrnOrder.modified'     => "'" . $now . "'",
                    'TrnOrder.modifier'     => $this->Session->read('Auth.MstUser.id')
                    ),array(
                        'TrnOrder.id' => $_row['trn_order_id'],
                        ), -1);

                //trn_claimの更新
                if($_row['remain_count'] === $_row['quantity'] ){
                    $this->TrnClaim->updateAll(array(
                        'TrnClaim.is_deleted' => 'true',
                        'TrnClaim.modified'   => "'" . $now . "'",
                        'TrnClaim.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        ),array(
                            'TrnClaim.trn_receiving_id' => $_row['id'],
                            ), -1);
                }else{
                    $this->TrnClaim->updateAll(array(
                        'TrnClaim.count'       => ((integer)$_row['quantity'] - (integer)$_row['remain_count'] ),
                        'TrnClaim.claim_price' => "TrnClaim.unit_price * " . ((integer)$_row['quantity'] - (integer)$_row['remain_count'] ),
                        'TrnClaim.modified'    => "'" . $now . "'" ,
                        'TrnClaim.modifier'    => $this->Session->read('Auth.MstUser.id'),
                        ),array(
                            'TrnClaim.trn_receiving_id' => $_row['id'],
                            ), -1);
                }


                if(true == $_row['is_lowlevel']){
                    $target_cnt = $_row['per_unit'];
                }else{
                    $target_cnt = 1;
                }

                //trn_stickerの更新
                $this->TrnSticker->updateAll(array(
                    'TrnSticker.quantity' => "TrnSticker.quantity  - " . $_row['remain_count'] * $target_cnt,
                    'TrnSticker.modified' => "'" . $now . "'",
                    'TrnSticker.modifier' => $this->Session->read('Auth.MstUser.id'),
                    ),array(
                        "TrnSticker.id" => $_row['trn_sticker_id'],
                        ), -1);

                //trn_stocksの更新
                if(true == $_row['is_lowlevel']){
                    //低レベル品の場合は基本単位の在庫マイナス
                    $this->TrnStock->updateAll(array(
                        'TrnStock.stock_count'          => "TrnStock.stock_count - " . $_row['remain_count'] * $target_cnt,
                        'TrnStock.lowlevel_stock_count' => "TrnStock.lowlevel_stock_count - " . $_row['remain_count'] * $target_cnt,
                        'TrnStock.modifier'             => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'             => "'" . $now . "'",
                        ),array(
                            'TrnStock.mst_item_unit_id'  => $_row['lowlevel_unit_id'], //$_row['mst_item_unit_id'],
                            'TrnStock.mst_department_id' => $_row['department_id_to'],
                            ), -1);
                    //入荷単位の入庫数加算
                    $this->TrnStock->updateAll(array(
                        'TrnStock.receipted_count' => "TrnStock.receipted_count + {$_row['remain_count']}",
                        'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'        => "'" . $now . "'",
                        ),array(
                            'TrnStock.mst_item_unit_id'  => $_row['mst_item_unit_id'],
                            'TrnStock.mst_department_id' => $_row['department_id_to'],
                            ), -1);
                }
                
                // オペ倉庫発注の場合
                if($_row['order_type'] == Configure::read('OrderTypes.opestock') ){
                    $sql  = ' select ';
                    $sql .= '       b.id                   as storage_id ';
                    $sql .= '     , c.id                   as sticker_id ';
                    $sql .= '     , c.mst_department_id ';
                    $sql .= '     , c.mst_item_unit_id ';
                    $sql .= '     , c.quantity  ';
                    $sql .= '   from ';
                    $sql .= '     trn_receivings as a  ';
                    $sql .= '     left join mst_item_units as a1  ';
                    $sql .= '       on a1.id = a.mst_item_unit_id  ';
                    $sql .= '     left join trn_storages as b  ';
                    $sql .= '       on b.trn_receiving_id = a.id  ';
                    $sql .= '     left join trn_stickers as c  ';
                    $sql .= '       on c.trn_storage_id = b.id  ';
                    $sql .= '   where ';
                    $sql .= '     a.id = ' . $_row['id'];
                    $ope = $this->TrnReceiving->query($sql);
                    foreach($ope as $o){ 
                        // 入庫明細削除
                        $this->TrnStorage->del(array('id'=>$o[0]['storage_id']));

                        // シール削除
                        $this->TrnSticker->del(array('id'=>$o[0]['sticker_id']));

                        // 在庫減
                        $this->TrnStock->updateAll(array(
                            'TrnStock.stock_count' => "TrnStock.stock_count - {$o[0]['quantity']}",
                            'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'        => "'" . $now . "'",
                        ),array(
                            'TrnStock.mst_item_unit_id'  => $o[0]['mst_item_unit_id'],
                            'TrnStock.mst_department_id' => $o[0]['mst_department_id'],
                        ), -1);
                    }
                }
                
                
                if($_row['order_type'] == Configure::read('OrderTypes.temporary') ||
                   $_row['order_type'] == Configure::read('OrderTypes.temporary_calc_notautocalc') ){
                    $req_count = 0;
                }else{
                    $req_count = $_row['remain_count'];
                }
                $this->TrnStock->updateAll(array(
                    'TrnStock.requested_count' => "TrnStock.requested_count + {$req_count}",
                    'TrnStock.receipted_count' => "TrnStock.receipted_count - {$_row['remain_count']}",
                    'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                    'TrnStock.modified'        => "'" . $now . "'",
                    ),array(
                        'TrnStock.mst_item_unit_id'  => $_row['mst_item_unit_id'],
                        'TrnStock.mst_department_id' => $_row['department_id_to'],
                        ), -1);


                if(false === in_array($_row['trn_receiving_header_id'], $_receivingHeaderCondition)){
                    $_receivingHeaderCondition[] = $_row['trn_receiving_header_id'];
                }

                if(false === in_array($_row['trn_order_header_id'], $_orderHeaderCondition)){
                    $_orderHeaderCondition[] = $_row['trn_order_header_id'];
                }
            }

            //trn_receiving_headerの更新
            foreach($_receivingHeaderCondition as $_key){
                $_count = $this->TrnReceiving->find('count', array(
                    'conditions' => array(
                        'TrnReceiving.is_deleted' => false,
                        'TrnReceiving.trn_receiving_header_id' => $_key,
                        ),
                    'recursive' => -1,
                    ));
                if($_count === 0){
                    $this->TrnReceivingHeader->updateAll(array(
                        'TrnReceivingHeader.is_deleted'       => 'true',
                        'TrnReceivingHeader.receiving_status' => Configure::read('ReceivingStatus.arrival') ,
                        'TrnReceivingHeader.modified'         => "'" . $now . "'",
                        'TrnReceivingHeader.modifier'         => $this->Session->read('Auth.MstUser.id'),
                        ),array(
                            'TrnReceivingHeader.id' => $_key,
                            ), -1);
                }
            }

            //trn_order_headerの更新
            foreach($_orderHeaderCondition as $_key){
                $_count = $this->TrnOrder->find('count', array(
                    'conditions' => array(
                        'TrnOrder.order_status' => Configure::read('OrderStatus.partLoaded'),
                        'TrnOrder.is_deleted' => false,
                        ),
                    'recursive' => -1,
                    ));
                if($_count > 0){
                    $this->TrnOrderHeader->updateAll(array(
                        'TrnOrderHeader.order_status' => Configure::read('OrderStatus.partLoaded'),
                        'TrnOrderHeader.modified'     => "'" . $now . "'",
                        'TrnOrderHeader.modifier'     => $this->Session->read('Auth.MstUser.id'),
                        ),array(
                            'TrnOrderHeader.id' => $_key,
                            'TrnOrderHeader.order_status <> ' => Configure::read('OrderStatus.partLoaded'),
                            ), -1);
                }else{
                    $this->TrnOrderHeader->updateAll(array(
                        'TrnOrderHeader.order_status' => Configure::read('OrderStatus.ordered'),
                        'TrnOrderHeader.modified'     => "'" . $now . "'",
                        'TrnOrderHeader.modifier'     => $this->Session->read('Auth.MstUser.id'),
                        ),array(
                            'TrnOrderHeader.id' => $_key,
                            ), -1);
                }
            }

            $errors = array();
            $errors = array_merge(
                $errors,
                is_array($this->validateErrors($this->TrnReceiving)) ? $this->validateErrors($this->TrnReceiving) : array(),
                is_array($this->validateErrors($this->TrnReceivingHeader)) ? $this->validateErrors($this->TrnReceivingHeader) : array(),
                is_array($this->validateErrors($this->TrnOrder)) ? $this->validateErrors($this->TrnOrder) : array(),
                is_array($this->validateErrors($this->TrnOrderHeader)) ? $this->validateErrors($this->TrnOrderHeader) : array(),
                is_array($this->validateErrors($this->TrnClaim)) ? $this->validateErrors($this->TrnClaim) : array(),
                is_array($this->validateErrors($this->TrnStock)) ? $this->validateErrors($this->TrnStock) : array(),
                is_array($this->validateErrors($this->TrnSticker)) ? $this->validateErrors($this->TrnSticker) : array()
                );

            if(false === empty($errors)){
                $this->TrnReceiving->rollback();
                throw new Exception(print_r($errors,true));
                return;
            }

            //コミット直前に更新チェック
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_receivings as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $receivingId). ')';
            $sql .= "     and a.modified > '" . $this->request->data['TrnReceiving']['time'] ."'";
            $sql .= "     and a.modified <> '" . $now ."'";

            $ret = $this->TrnReceiving->query($sql);

            if($ret[0][0]['count']> 0 ){
                $this->TrnReceiving->rollback();
                $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('add');
            }

            $this->TrnReceiving->commit();
            $_res = $this->getReceivings($_params);
            $this->set('SearchResult', $_res);

            $this->Session->setFlash('入荷取消が完了しました', 'growl', array('type' => 'star'));
            $this->Session->delete('TrnReceiving');
            $this->Session->write('TrnReceiving.result' , $_res);
            $this->Session->write('TrnReceiving.data' , $this->request->data);
            $this->render('results');
        }else{
            //2度押し本体策

            $this->set('SearchResult', $this->Session->Read('TrnReceiving.result'));
            $this->request->data = $this->Session->Read('TrnReceiving.data');
            $this->render('results');

        }
    }

    private function hasModifyDifference(array $nowResult, array $preResult){
         if(count($nowResult) !== count($preResult)){
            $this->Session->setFlash('入荷情報が更新されています。再度実行してください');
            return true;
        }else{
            for($i = 0; $i < count($nowResult); $i++){
                if($nowResult[$i]['modified'] !== $preResult[$i]['modified']){
                    $this->Session->setFlash('入荷情報が更新されています。再度実行してください', 'growl', array('type'=>'error'));
                    return true;
                }
            }
        }
        return false;
    }

    private function getReceivingHeaders($cond, $limit){
        $o = $this->getSortOrder();
        if(isset($o)){
            $order = $o[0];
        }else{
            $order = 'TrnReceivingHeader.id desc';
        }

        $sql =
            ' select ' .
                ' count(TrnReceivingHeader.id) AS hit '.
                ',TrnReceivingHeader.id' .
                ',TrnReceivingHeader.work_no' .
                ',TrnReceivingHeader.work_date' .
                ',TrnReceivingHeader.recital' .
                ',TrnReceivingHeader.receiving_type' .
                ',TrnReceivingHeader.receiving_status' .
                ',TrnReceivingHeader.department_id_from' .
                ',TrnReceivingHeader.department_id_to' .
                ',TrnReceivingHeader.detail_count' .
                ',TrnReceivingHeader.is_deleted' .
                ',TrnReceivingHeader.creater' .
                ',TrnReceivingHeader.created' .
                ',TrnReceivingHeader.modifier' .
                ',TrnReceivingHeader.modified' .
                ',TrnReceivingHeader.trn_order_header_id' .
                ',TrnReceiving.trn_receiving_header_id' .
                ',MstFacility.facility_name' .
                ',MstUser.user_name'.
            ' from ' .
                ' trn_receiving_headers as TrnReceivingHeader' .
                ' inner join trn_receivings as TrnReceiving on TrnReceivingHeader.id = TrnReceiving.trn_receiving_header_id'.
                ' inner join trn_orders as TrnOrder on TrnReceiving.trn_order_id = TrnOrder.id'.
                ' inner join mst_item_units as MstItemUnit on MstItemUnit.id = TrnReceiving.mst_item_unit_id' .
                ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id '.
                ' left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id' .
                ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id'.
                ' inner join mst_unit_names as MstParUnitName on MstItemUnit.per_unit_name_id = MstParUnitName.id'.
                ' inner join mst_departments as MstDepartment on TrnReceivingHeader.department_id_from = MstDepartment.id ' .
                ' inner join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id' .
                " inner join trn_claims as TrnClaim on TrnReceiving.id = TrnClaim.trn_receiving_id and TrnClaim.is_stock_or_sale = '1'".
                ' inner join mst_users as MstUser on TrnReceivingHeader.creater = MstUser.id ' .
                ' inner join mst_departments as MstDepartmentTo on TrnReceivingHeader.department_id_to = MstDepartmentTo.id ' .
            ' where ' .
                $cond .
                ' and MstFacility.facility_type = ' . Configure::read('FacilityType.supplier') .
                ' and TrnReceiving.receiving_type = '.Configure::read('ReceivingType.arrival') .
                ' and MstDepartmentTo.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') .
            ' group by ' .
                ' TrnReceivingHeader.id' .
                ',TrnReceivingHeader.work_no' .
                ',TrnReceivingHeader.work_date' .
                ',TrnReceivingHeader.recital' .
                ',TrnReceivingHeader.receiving_type' .
                ',TrnReceivingHeader.receiving_status' .
                ',TrnReceivingHeader.department_id_from' .
                ',TrnReceivingHeader.department_id_to' .
                ',TrnReceivingHeader.detail_count' .
                ',TrnReceivingHeader.is_deleted' .
                ',TrnReceivingHeader.creater' .
                ',TrnReceivingHeader.created' .
                ',TrnReceivingHeader.modifier' .
                ',TrnReceivingHeader.modified' .
                ',TrnReceivingHeader.trn_order_header_id' .
                ',TrnReceiving.trn_receiving_header_id' .
                ',MstFacility.facility_name' .
                ',MstUser.user_name' .
            ' order by ' .
                $order ;

        //全件取得
        $this->set('max' , $this->getMaxCount($sql , 'TrnReceivingHeader'));

        $sql .= ' limit ' . $limit;
        return $this->TrnReceivingHeader->query($sql);
    }

    /**
     * 検索結果を取得
     * @param array $_param
     * @access private
     * @return array
     */
    private function getReceivings($_param, $_mode = null){
        $_orderType = Configure::read('OrderTypes.orderTypeName');

        $sql  = ' select ';
        $sql .= '     TrnReceiving.id';
        $sql .= '    ,TrnReceiving.work_no';
        $sql .= '    ,TrnReceiving.work_seq';
        $sql .= "    ,to_char(TrnReceiving.work_date,'YYYY/mm/dd') as \"TrnReceiving__work_date\"";
        $sql .= '    ,TrnReceiving.work_type';
        $sql .= '    ,MstClasses.name as work_type_name';
        $sql .= '    ,TrnReceiving.recital';
        $sql .= '    ,TrnReceiving.receiving_type';
        $sql .= '    ,TrnReceiving.receiving_status';
        $sql .= '    ,TrnReceiving.mst_item_unit_id';
        $sql .= '    ,TrnReceiving.department_id_from';
        $sql .= '    ,TrnReceiving.department_id_to';
        $sql .= '    ,TrnReceiving.quantity';
        $sql .= '    ,TrnReceiving.remain_count ';
        $sql .= '    ,TrnReceiving.stocking_price';
        $sql .= '    ,TrnReceiving.sales_price';
        $sql .= '    ,TrnReceiving.trn_sticker_id';
        $sql .= '    ,TrnReceiving.trn_shipping_id';
        $sql .= '    ,TrnReceiving.trn_order_id';
        $sql .= '    ,TrnReceiving.trn_receiving_header_id';
        $sql .= '    ,TrnReceiving.hospital_sticker_no';
        $sql .= '    ,TrnReceiving.is_retroactable';
        $sql .= '    ,TrnReceiving.is_deleted';
        $sql .= '    ,TrnReceiving.creater';
        $sql .= '    ,TrnReceiving.created';
        $sql .= '    ,TrnReceiving.modifier';
        $sql .= '    ,TrnReceiving.modified';
        $sql .= '    ,MstFacilityItem.mst_facility_id';
        $sql .= '    ,MstFacilityItem.internal_code';
        $sql .= '    ,MstFacilityItem.item_name';
        $sql .= '    ,MstFacilityItem.standard';
        $sql .= '    ,MstFacilityItem.item_code';
        $sql .= '    ,MstFacilityItem.jan_code';
        $sql .= '    ,MstFacilityItem.unit_price';
        $sql .= '    ,MstFacilityItem.price';
        $sql .= '    ,MstFacilityItem.refund_price';
        $sql .= '    ,MstFacilityItem.tax_type';
        $sql .= '    ,MstFacilityItem.converted_num';
        $sql .= '    ,MstFacilityItem.is_lowlevel';
        $sql .= '    ,MstFacilityItem.medical_code';
        $sql .= '    ,MstDealer.dealer_name';
        $sql .= "    ,(case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstParUnitName.unit_name || ')' end ) as packing_name";
        $sql .= '    ,MstFacility.facility_name';
        $sql .= '    ,TrnOrder.order_type as TrnOrder__order_type';
        $sql .= '    ,TrnOrder.work_no as TrnOrder__work_no';
        $sql .= '    ,TrnOrder.work_date as TrnOrder__work_date';
        $sql .= '    ,TrnOrder.remain_count as TrnOrder__remain_count';
        $sql .= '    ,TrnOrder.quantity as TrnOrder__quantity';
        $sql .= '    ,TrnOrder.trn_order_header_id';
        $sql .= '    ,MstUser.user_name';
        $sql .= '    ,MstItemUnit.per_unit';
        $sql .= '    ,TrnStockOne.lowlevel_stock_count as lowlevel_cnt';
        $sql .= '    ,MstItemUnitOne.id as lowlevel_unit_id';
        $sql .= ' from ';
        $sql .= '     trn_receiving_headers as TrnReceivingHeader';
        $sql .= '     inner join trn_receivings as TrnReceiving on TrnReceivingHeader.id = TrnReceiving.trn_receiving_header_id and TrnReceivingHeader.work_no = TrnReceiving.work_no';
        $sql .= '     inner join trn_orders as TrnOrder on TrnReceiving.trn_order_id = TrnOrder.id';
        $sql .= '     inner join mst_item_units as MstItemUnit on MstItemUnit.id = TrnReceiving.mst_item_unit_id';
        $sql .= '     inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ';
        $sql .= '     left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id';
        $sql .= '     inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id';
        $sql .= '     inner join mst_unit_names as MstParUnitName on MstItemUnit.per_unit_name_id = MstParUnitName.id';
        $sql .= '     inner join mst_departments as MstDepartment on TrnReceiving.department_id_from = MstDepartment.id ';
        $sql .= '     inner join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id';
        $sql .= '     inner join mst_users as MstUser on TrnReceiving.modifier = MstUser.id ';
        $sql .= "     inner join trn_claims as TrnClaim on TrnReceiving.id = TrnClaim.trn_receiving_id and TrnClaim.is_stock_or_sale='1'";
        $sql .= '     left join trn_stickers as TrnSticker on TrnReceiving.trn_sticker_id = TrnSticker.id ';
        $sql .= '     left join mst_item_units as MstItemUnitOne on MstItemUnitOne.per_unit = 1 and MstItemUnitOne.mst_facility_item_id = MstFacilityItem.id ';
        $sql .= '     left join trn_stocks as TrnStockOne on TrnStockOne.mst_item_unit_id = MstItemUnitOne.id and TrnReceiving.department_id_to = TrnStockOne.mst_department_id ';
        $sql .= '     left join mst_classes as MstClasses on TrnReceiving.work_type = MstClasses.id ';

        if ($_mode != null) {
            $sql .= '     inner join mst_departments as MstDepartmentTo on TrnReceivingHeader.department_id_to = MstDepartmentTo.id ';
        }

        $sql .= ' where ';
        $sql .= $_param;
        $sql .= ' order by ';
        $sql .= '     TrnReceiving.id asc ';

        $_res = $this->TrnReceiving->query($sql , false);
        $_results = array();
        foreach($_res as $_row){
            $_r = array();
            array_walk_recursive($_row, array('ReceivingsHistoryController','BuildSearchResult'), &$_r);
            //取消可能かどうか
            $_r['IsSelectable'] = (
                       ($_r['is_deleted'] == false)
                    && ((integer)$_r['remain_count'] != 0)
            );

            //低レベルの場合は在庫が存在するか
            if( true === $_r['is_lowlevel'] ){
                if( $_r['lowlevel_cnt'] < $_r['per_unit'] ) $_r['IsSelectable'] = false;
            }
            
            // オペ倉庫発注の場合、入荷から発生したシールがすべて有効な状態か確認する。
            if($_r['order_type'] == Configure::read('OrderTypes.opestock')){
                $sql  = ' select ';
                $sql .= '       a.id  ';
                $sql .= '   from ';
                $sql .= '     trn_receivings as a  ';
                $sql .= '     left join mst_item_units as a1  ';
                $sql .= '       on a1.id = a.mst_item_unit_id  ';
                $sql .= '     left join trn_storages as b  ';
                $sql .= '       on b.trn_receiving_id = a.id  ';
                $sql .= '     left join trn_stickers as c  ';
                $sql .= '       on c.trn_storage_id = b.id  ';
                $sql .= '   where ';
                $sql .= '     a.id = ' . $_r['id'];
                $sql .= '     and c.is_deleted = false  ';
                $sql .= '     and c.has_reserved = false  ';
                $sql .= '     and c.quantity > 0  ';
                $sql .= '     and c.mst_department_id = b.mst_department_id  ';
                $sql .= '   group by ';
                $sql .= '     a.id ';
                $sql .= '     , a.quantity ';
                $sql .= '     , a1.per_unit  ';
                $sql .= '   having ';
                $sql .= '     a.quantity * a1.per_unit = sum(c.quantity) ';
                $chk = $this->TrnReceiving->query($sql);
                if(!empty($chk)){
                    $_r['IsSelectable'] = true;
                }
            }
            
            //取消し可能な場合締めチェック
            if($_r['IsSelectable'] == true){
                $sql  = ' select ';
                $sql .= '       sum(  ';
                $sql .= '       case  ';
                $sql .= '         when a.is_provisional = false  ';
                $sql .= '         then 1  ';
                $sql .= '         else 0  ';
                $sql .= '         end ';
                $sql .= '     )                 as count1 ';
                $sql .= '     , sum(  ';
                $sql .= '       case  ';
                $sql .= '         when a.is_provisional = true  ';
                $sql .= '         then 1  ';
                $sql .= '         else 0  ';
                $sql .= '         end ';
                $sql .= '     )                 as count2 ';
                $sql .= '     , x.updatable  ';
                $sql .= '   from ';
                $sql .= '     trn_close_headers as a  ';
                $sql .= '     cross join (  ';
                $sql .= '       select ';
                $sql .= '             a.updatable  ';
                $sql .= '         from ';
                $sql .= '           mst_privileges as a  ';
                $sql .= '           left join mst_roles as b  ';
                $sql .= '             on b.id = a.mst_role_id  ';
                $sql .= '           left join mst_users as c  ';
                $sql .= '             on c.mst_role_id = b.id  ';
                $sql .= '         where ';
                $sql .= '           c.id = ' . $this->Session->read('Auth.MstUser.id');
                $sql .= '           and a.view_no = 23 ';
                $sql .= '     ) as x  ';
                $sql .= '   where ';
                $sql .= '     a.is_deleted = false  ';
                $sql .= '     and a.close_type = ' . Configure::read('CloseType.stock');
                $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
                $sql .= "     and a.start_date <= '" . $_r['work_date'] . "'";
                $sql .= "     and a.end_date >= '" . $_r['work_date'] . "'";
                $sql .= '   group by ';
                $sql .= '     x.updatable ';

                $close_check = $this->TrnReceiving->query($sql);

                if(!empty($close_check)){
                    //本締め？
                    if($close_check[0][0]['count1'] > 0){
                        $_r['IsSelectable'] = false;
                    }else if($close_check[0][0]['count2'] > 0 && $close_check[0][0]['updatable'] == false ){
                        //仮締め？
                        $_r['IsSelectable'] = false;
                    }
                }
            }

            //更新者名
            $_r['modifier_name'] = $_row[0]['user_name'];
            //発注区分名
            $_r['order_type_name'] = $_orderType[$_row['trnorder']['order_type']];
            //発注の残数
            $_r['trn_order_remain_count'] = $_row['trnorder']['remain_count'];
            //発注数
            $_r['trn_order_quantity'] = $_row['trnorder']['quantity'];
            //発注番号
            $_r['trn_order_work_no'] = $_row['trnorder']['work_no'];

            $_results[] = $_r;
        }
        return $_results;
    }

    /**
     * 検索条件を取得
     *
     * @param array $_to
     * @param array $_rh
     * @param array $_rs
     * @param array $_fi
     * @param array $_md
     * @access private
     * @return array
     */
    private function getSearchParamater($_to = array(), $_rh = array(), $_rs = array(), $_fi = array(), $_md = array(), $_mf = array(), $_tc=array()){
        $where = ' 1 = 1 ';
        foreach($_to as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnOrder.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnOrder.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND TrnOrder.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_rh as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnReceivingHeader.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnReceivingHeader.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND TrnReceivingHeader.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_rs as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnReceiving.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnReceiving.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND TrnReceiving.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_fi as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND MstFacilityItem.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND MstFacilityItem.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND MstFacilityItem.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_md as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND MstDealer.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND MstDealer.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND MstDealer.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_mf as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND MstFacility.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND MstFacility.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND MstFacility.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_tc as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnClaim.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnClaim.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND (TrnClaim.{$_key} {$_val}) ";
                }
            }
        }
        return $where;
    }

    /**
     * Use this with php function: array_walk_recursive
     * @param mixed $item
     * @param string $key
     * @param array $res
     * @access public
     * @static
     */
    public static function BuildSearchResult($item, $key, $res){
        //HACK:  最上位の配列値を優先するようにしてます・・
        if(false === isset($res[$key])){
            $res[$key] = $item;
        }
    }

    /**
     * 入荷明細取得条件設定
     */
    private function _getReceivingDetailParam(){

        $return = array();

        App::import('Sanitize');
        $_to = $_rs = $_fi = $_md = $_mf = $_tc = array();
        //conditions for TrnOrder
        if($this->request->data['TrnOrder']['work_no'] != ''){
            $_to['work_no LIKE '] = "%" . Sanitize::escape($this->request->data['TrnOrder']['work_no']) . "%";
        }

        if($this->request->data['TrnOrder']['work_date_from'] != ''){
            $_to['work_date >= '] = date('Y/m/d', strtotime($this->request->data['TrnOrder']['work_date_from']));
        }

        if($this->request->data['TrnOrder']['work_date_to'] != ''){
            $_to['work_date < '] = date('Y/m/d', strtotime('1 day', strtotime($this->request->data['TrnOrder']['work_date_to'])));
        }

        if($this->request->data['TrnOrder']['order_type'] != ''){
            $_to['order_type'] = $this->request->data['TrnOrder']['order_type'];
        }

        //conditions for TrnReceiving
        if($this->request->data['TrnReceiving']['work_type'] != ''){
            $_rs['work_type'] = $this->request->data['TrnReceiving']['work_type'];
        }

        if($this->request->data['showDeleted'] == '0'){
            $_rs['is_deleted'] = 'false';
        }

        //conditions for MstFacilityItem
        if($this->request->data['MstFacilityItem']['internal_code'] != ''){
            $_fi['internal_code'] = Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']);
        }

        if($this->request->data['MstFacilityItem']['item_code'] != ''){
            $_fi['item_code LIKE '] = "%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_code']) . "%";
        }

        if($this->request->data['MstFacilityItem']['item_name'] != ''){
            $_fi['item_name LIKE '] = "%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%";
        }

        if($this->request->data['MstFacilityItem']['standard'] != ''){
            $_fi['standard LIKE '] = "%" . Sanitize::escape($this->request->data['MstFacilityItem']['standard']) . "%";
        }

        if($this->request->data['MstFacilityItem']['jan_code'] != ''){
            $_fi['jan_code'] = Sanitize::escape($this->request->data['MstFacilityItem']['jan_code']);
        }

        //conditions for MstDealer
        if($this->request->data['MstDealer']['dealer_name'] != '') {
            $_md['dealer_name LIKE '] = "%" . Sanitize::escape($this->request->data['MstDealer']['dealer_name']) . "%";
        }

        //conditions for MstFacility
        if($this->request->data['FacilityCodeInput'] != ''){
            $_mf['facility_code'] = Sanitize::escape($this->request->data['FacilityCodeInput']);
        }

        if($this->request->data['hasDifference'] == '1'){
            $_tc['unit_price <> '] =
            '(select TC.transaction_price from mst_transaction_configs as TC ' .
            ' where TC.is_deleted = false and TC.start_date <= cast(TrnOrder.work_date as date) and TC.end_date > cast(TrnOrder.work_date as date) + 1 and TC.mst_item_unit_id = MstItemUnit.id limit 1) or '.
            '(select TC.transaction_price from mst_transaction_configs as TC ' .
            ' where TC.is_deleted = false and TC.start_date <= cast(TrnOrder.work_date as date) and TC.end_date > cast(TrnOrder.work_date as date) + 1 and TC.mst_item_unit_id = MstItemUnit.id limit 1) is null ';
        }

        //戻り値設定(参照渡しをやりたくないため）
        if (!empty($_to)) {
            $return['to'] = $_to;
        }

        if (!empty($_rs)) {
            $return['rs'] = $_rs;
        }

        if (!empty($_fi)) {
            $return['fi'] = $_fi;
        }

        if (!empty($_md)) {
            $return['md'] = $_md;
        }

        if (!empty($_mf)) {
            $return['mf'] = $_mf;
        }

        if (!empty($_tc)) {
            $return['tc'] = $_tc;
        }

        return $return;

    }


    /**
     * 入荷履歴CSV出力
     */
    function export_csv() {

        $_rh = $_to = $_rs = $_fi = $_md = $_mf = $_tc = array();
        $_param = $this->_getReceivingDetailParam();

        if (array_key_exists('to', $_param)) {
            $_to = $_param['to'];
        }

        if (array_key_exists('rs', $_param)) {
            $_rs = $_param['rs'];
        }

        if (array_key_exists('fi', $_param)) {
            $_fi = $_param['fi'];
        }

        if (array_key_exists('md', $_param)) {
            $_md = $_param['md'];
        }

        if (array_key_exists('tc', $_param)) {
            $_tc = $_param['tc'];
        }

        if($this->request->data['TrnReceivingHeader']['work_date_from'] != ''){
            $_rh['work_date >= '] = date('Y/m/d', strtotime($this->request->data['TrnReceivingHeader']['work_date_from']));
        }

        if($this->request->data['TrnReceivingHeader']['work_date_to'] != ''){
            $_rh['work_date < '] = date('Y/m/d', strtotime('1 day', strtotime($this->request->data['TrnReceivingHeader']['work_date_to'])));
        }

        if($this->request->data['FacilityCodeInput'] != ''){
            $_mf['facility_code'] = Sanitize::escape($this->request->data['FacilityCodeInput']);
        }

        $where  = $this->getSearchParamater($_to, $_rh,  $_rs, $_fi, $_md, $_mf, $_tc);

        //一覧固有で指定している条件を追加
        $where .= ' and MstFacility.facility_type = ' . Configure::read('FacilityType.supplier');
        $where .= ' and TrnReceiving.receiving_type = '.Configure::read('ReceivingType.arrival');
        $where .= ' and MstDepartmentTo.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');

        $sql = $this->_getReceivingCSV($where);
        $this->db_export_csv($sql , "入荷履歴", 'search');

    }

    private function _getReceivingCSV($_param){
        $sql  = ' select ';
        $sql .= '     TrnReceiving.work_no                         as 入荷番号';
        $sql .= '    ,TrnOrder.work_no                             as 発注番号';
        $sql .= "    ,to_char(TrnReceiving.work_date,'YYYY/mm/dd') as 入荷日";
        $sql .= '    ,MstFacility.facility_name                    as 仕入先名';
        $sql .= '    ,MstFacilityItem.internal_code                as "商品ID"';
        $sql .= '    ,( case TrnOrder.order_type ';
        foreach(Configure::read('OrderTypes.orderTypeName') as $key => $val){
            $sql .= "           when " . $key . " then '" . $val ."' ";
        }
        $sql .= '      end )                                       as 発注区分';
        $sql .= '    ,MstFacilityItem.item_name                    as 商品名';
        $sql .= '    ,MstFacilityItem.standard                     as 規格';
        $sql .= '    ,MstFacilityItem.item_code                    as 製品番号';
        $sql .= '    ,MstDealer.dealer_name                        as 販売元名';
        $sql .= '    , ( case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name ';
        $sql .= "              else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstParUnitName.unit_name || ')' ";
        $sql .= '      end )                                       as 包装単位名';
        $sql .= '    ,TrnReceiving.stocking_price                  as 仕入単価';
        $sql .= '    ,TrnReceiving.quantity                        as 入荷数';
        $sql .= '    ,TrnReceiving.remain_count                    as 残数';
        $sql .= '    ,MstClass.name                                as 作業区分';
        $sql .= '    ,MstUser.user_name                            as 更新者名';
        $sql .= '    ,TrnReceiving.recital                         as 備考';

        $sql .= ' from ';
        $sql .= '     trn_receiving_headers as TrnReceivingHeader';
        $sql .= '     inner join trn_receivings as TrnReceiving on TrnReceivingHeader.id = TrnReceiving.trn_receiving_header_id and TrnReceivingHeader.work_no = TrnReceiving.work_no';
        $sql .= '     inner join trn_orders as TrnOrder on TrnReceiving.trn_order_id = TrnOrder.id';
        $sql .= '     inner join mst_item_units as MstItemUnit on MstItemUnit.id = TrnReceiving.mst_item_unit_id';
        $sql .= '     inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ';
        $sql .= '     left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id';
        $sql .= '     inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id';
        $sql .= '     inner join mst_unit_names as MstParUnitName on MstItemUnit.per_unit_name_id = MstParUnitName.id';
        $sql .= '     inner join mst_departments as MstDepartment on TrnReceiving.department_id_from = MstDepartment.id ';
        $sql .= '     inner join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id';
        $sql .= '     inner join mst_users as MstUser on TrnReceiving.modifier = MstUser.id ';
        $sql .= '     left join mst_classes as MstClass on MstClass.id = TrnReceiving.work_type ';
        $sql .= '     inner join mst_departments as MstDepartmentTo on TrnReceivingHeader.department_id_to = MstDepartmentTo.id ';
        $sql .= '     inner join trn_claims as TrnClaim on TrnReceiving.id = TrnClaim.trn_receiving_id';

        $sql .= ' where ';
        $sql .= $_param;

        $sql .= ' group by ';
        $sql .= '     TrnReceiving.id';
        $sql .= '     ,TrnReceiving.work_no';
        $sql .= '     ,TrnReceiving.work_date';
        $sql .= '     ,TrnReceiving.work_type';
        $sql .= '     ,TrnReceiving.recital';
        $sql .= '     ,TrnReceiving.receiving_type';
        $sql .= '     ,TrnReceiving.receiving_status';
        $sql .= '     ,TrnReceiving.quantity';
        $sql .= '     ,TrnReceiving.remain_count';
        $sql .= '     ,TrnReceiving.stocking_price';
        $sql .= '     ,TrnReceiving.hospital_sticker_no';
        $sql .= '     ,MstItemUnit.per_unit';
        $sql .= '     ,MstFacilityItem.internal_code';
        $sql .= '     ,MstFacilityItem.item_name';
        $sql .= '     ,MstFacilityItem.standard';
        $sql .= '     ,MstFacilityItem.item_code';
        $sql .= '     ,MstDealer.dealer_name';
        $sql .= '     ,MstUnitName.unit_name';
        $sql .= '     ,MstParUnitName.unit_name';
        $sql .= '     ,MstFacility.facility_name';
        $sql .= '     ,TrnOrder.order_type';
        $sql .= '     ,TrnOrder.work_no';
        $sql .= '     ,MstUser.user_name';
        $sql .= '     ,MstClass.name';
        $sql .= ' order by ';
        $sql .= '     TrnReceiving.id asc ';

        return $sql;

    }

}
?>
