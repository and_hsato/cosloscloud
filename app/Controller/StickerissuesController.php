<?php
/**
 * StickersIssueController
 *　部署シール管理 
 * @version 1.0.0
 */
class StickerIssuesController extends AppController {
    
    /**
     * @var $name
     */
    var $name = 'stickerissues';
    
    /**
     * @var array $uses
     */
    var $uses = array('TrnStickerIssue',
                      'TrnStickerRecord',
                      'TrnStickerIssueHeader',
                      'MstFacility',
                      'MstFacilityRelation',
                      'MstClass',
                      'MstUser',
                      'MstItemUnit',
                      'MstUnitName',
                      'MstFacilityItem',
                      'MstDepartment',
                      'MstFixedCount',
                      'TrnSticker',
                      'TrnFixedPromise',
                      'MstShelfName');
    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time','common');
    
    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Stickers','CsvWriteUtils');
    
    
    /**
     * 定数：作業区分
     * Enter description here ...
     * @var unknown_type
     */
    const WORK_CLASSES_STICKER =5;
    const BLANK_HOSPITAL_STICK_NO = '　';
    
    /**
     * before filter : callback method
     */
    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }
    
    /**
     * Common method: print
     * @param integer $_id
     */
    public function seal ($_id) {
        if($_id == '1'){
            //部署シール発行完了画面から
            $this->print_hospital_sticker();
        }elseif($_id == '2'){
            $this->print_cost_sticker();
        }elseif($_id == '3'){
            $this->printCostStickers();
        }else{
            //部署シール発行履歴画面から
            $this->republish_hospital_stikers();
        }
    }
    
    /* 
     * ピッキングリスト印刷
     */
    public function report(){
        /* 絞り込み条件 */
        $where = '';
        //商品ID
        if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
            $where .= " and internal_code = '" . Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']) . "'";
        }
        //製品番号
        if(!empty($this->request->data['MstFacilityItem']['item_code'])){
            $where .= " and item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        //規格
        if(!empty($this->request->data['MstFacilityItem']['standard'])){
            $where .= " and standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }

        //発行施設
        if(!empty($this->request->data['TrnStickerIssueHeader']['facilityCode'])){
            $where .= " and facility_code = '". $this->request->data['TrnStickerIssueHeader']['facilityCode'] ."'";
            //発行部署
            if(!empty($this->request->data['TrnStickerIssueHeader']['departmentCode'])){
                $where .= " and department_code = '" . $this->request->data['TrnStickerIssueHeader']['departmentCode']."'";
            }

        }
        //商品名
        if(!empty($this->request->data['MstFacilityItem']['item_name'])){
            $where .= " and item_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%'";
        }
        //販売元
        if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
            $where .= " and dealer_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name']) . "%'";
        }
        //管理区分
        if(!empty($this->request->data['TrnFixedPromise']['promise_type'])){
            $where .= " and promise_type = " . $this->request->data['TrnFixedPromise']['promise_type'];
        }
        
        $sql  = ' select ';
        $sql .= '   facility_name ';
        $sql .= '   , department_name ';
        $sql .= '   , internal_code ';
        $sql .= '   , item_name ';
        $sql .= '   , item_code ';
        $sql .= '   , standard ';
        $sql .= '   , unit_name ';
        $sql .= '   , dealer_name ';
        $sql .= '   , sum(count) as count ';
        $sql .= '   , fixed_count  ';
        $sql .= ' from ';
        $sql .= '   (  ';
        $sql .= '     select ';
        $sql .= '       f.facility_name ';
        $sql .= '       , e.department_name ';
        $sql .= '       , f.facility_code ';
        $sql .= '       , e.department_code ';
        $sql .= '       , c.internal_code ';
        $sql .= '       , c.item_name ';
        $sql .= '       , c.item_code ';
        $sql .= '       , c.standard ';
        $sql .= '       , (  ';
        $sql .= '         case  ';
        $sql .= '           when b.per_unit = 1  ';
        $sql .= '           then b1.unit_name  ';
        $sql .= "           else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '           end ';
        $sql .= '       ) as unit_name ';
        $sql .= '       , d.dealer_name ';
        $sql .= '       , a.quantity as count ';
        $sql .= '       , (  ';
        $sql .= '         case  ';
        $sql .= '           when h.promise_type = 1  ';
        $sql .= "           then '定数 : ' || h.fixed_count  ";
        $sql .= "           else '臨時'  ";
        $sql .= '           end ';
        $sql .= '       ) as fixed_count  ';
        $sql .= '       , h.promise_type';
        $sql .= '     from ';
        $sql .= '       trn_sticker_issues as a  ';
        $sql .= '       left join mst_item_units as b  ';
        $sql .= '         on b.id = a.mst_item_unit_id  ';
        $sql .= '       left join mst_unit_names as b1  ';
        $sql .= '         on b1.id = b.mst_unit_name_id  ';
        $sql .= '       left join mst_unit_names as b2  ';
        $sql .= '         on b2.id = b.per_unit_name_id  ';
        $sql .= '       left join mst_facility_items as c  ';
        $sql .= '         on c.id = b.mst_facility_item_id  ';
        $sql .= '       left join mst_dealers as d  ';
        $sql .= '         on d.id = c.mst_dealer_id  ';
        $sql .= '       left join mst_departments as e  ';
        $sql .= '         on e.id = a.department_id_to  ';
        $sql .= '       left join mst_facilities as f  ';
        $sql .= '         on f.id = e.mst_facility_id  ';
        $sql .= '       left join trn_fixed_promises as g  ';
        $sql .= '         on g.id = a.trn_fixed_promise_id  ';
        $sql .= '       left join trn_promises as h  ';
        $sql .= '         on h.id = g.trn_promise_id  ';
        $sql .= '     where ';
        $sql .= '       a.quantity > 0  ';
        $sql .= '       and a.is_deleted = false  ';
        $sql .= '       and c.mst_facility_id = ' . $this->request->data['MstFacility']['id'];
        $sql .= '     union all  ';
        $sql .= '     select ';
        $sql .= '       e.facility_name ';
        $sql .= '       , d.department_name ';
        $sql .= '       , e.facility_code ';
        $sql .= '       , d.department_code ';
        $sql .= '       , c.internal_code ';
        $sql .= '       , c.item_name ';
        $sql .= '       , c.item_code ';
        $sql .= '       , c.standard ';
        $sql .= '       , (  ';
        $sql .= '         case  ';
        $sql .= '           when b.per_unit = 1  ';
        $sql .= '           then b1.unit_name  ';
        $sql .= "           else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '           end ';
        $sql .= '       ) as unit_name ';
        $sql .= '       , c1.dealer_name ';
        $sql .= '       , a.remain_count as count ';
        $sql .= "       , '一般消耗品' as fixed_count  ";
        $sql .= '       , f.promise_type';
        $sql .= '     from ';
        $sql .= '       trn_fixed_promises as a ';
        $sql .= '       left join mst_item_units as b ';
        $sql .= '         on b.id = a.mst_item_unit_id ';
        $sql .= '       left join mst_unit_names as b1 ';
        $sql .= '         on b1.id = b.mst_unit_name_id ';
        $sql .= '       left join mst_unit_names as b2 ';
        $sql .= '         on b2.id = b.per_unit_name_id ';
        $sql .= '       left join mst_facility_items as c ';
        $sql .= '         on c.id = b.mst_facility_item_id ';
        $sql .= '       left join mst_dealers as c1  ';
        $sql .= '         on c1.id = c.mst_dealer_id  ';
        $sql .= '       left join mst_departments as d  ';
        $sql .= '         on d.id = a.department_id_from  ';
        $sql .= '       left join mst_facilities as e  ';
        $sql .= '         on e.id = d.mst_facility_id  ';
        $sql .= '       left join trn_promises as f ';
        $sql .= '         on f.id = a.trn_promise_id ';
        $sql .= '     where ';
        $sql .= '       a.remain_count > 0  ';
        $sql .= '       and a.is_deleted = false  ';
        $sql .= '       and c.is_lowlevel = true  ';
        $sql .= '       and c.mst_facility_id = ' . $this->request->data['MstFacility']['id'];
        $sql .= '   ) as x  ';
        $sql .= ' where 1=1';
        $sql .= $where;
        $sql .= ' group by ';
        $sql .= '   facility_name ';
        $sql .= '   , department_name ';
        $sql .= '   , facility_code ';
        $sql .= '   , department_code ';
        $sql .= '   , internal_code ';
        $sql .= '   , item_name ';
        $sql .= '   , item_code ';
        $sql .= '   , standard ';
        $sql .= '   , unit_name ';
        $sql .= '   , dealer_name ';
        $sql .= '   , fixed_count  ';
        $sql .= '   , promise_type';
        $sql .= ' order by ';
        $sql .= '   facility_name ';
        $sql .= '   , department_name ';
        $sql .= '   , item_name ';
        $sql .= '   , unit_name ';
        
        $ret = $this->TrnStickerIssue->query($sql);
        
        $columns = array("補充依頼番号",
                         "仕入先名",
                         "補充先名",
                         "施主名",
                         "補充依頼日",
                         "納品先住所",
                         "施主住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "明細備考",
                         "包装単位",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $tmpHeader = "";
        $i = 1;
        $tmp_page_no = '';
        foreach($ret as $r){
            $page_no = $r[0]['facility_name'].$r[0]['department_name'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }

            $data[] = array(  ''                                                      //補充依頼番号
                             ,''                                                      //仕入先名
                             ,$r[0]['facility_name'] . ' ' . $r[0]['department_name'] //補充先名
                             ,''                                                      //施主名
                             ,''                                                      //補充依頼日
                             ,''                                                      //納品先住所
                             ,''                                                      //施主住所
                             ,$i                                                      //行番号
                             ,$r[0]['item_name']                                      //商品名
                             ,$r[0]['standard']                                       //規格
                             ,$r[0]['item_code']                                      //製品番号
                             ,$r[0]['dealer_name']                                    //販売元
                             ,$r[0]['internal_code']                                  //商品ID
                             ,$r[0]['fixed_count']                                    //明細備考
                             ,$r[0]['unit_name']                                      //包装単位
                             ,$r[0]['count']                                          //数量
                             ,''                                                      //ヘッダ備考
                             ,''                                                      //印刷年月日
                             ,''                                                      //丸め区分
                             ,''                                                      //削除フラグ
                             ,''                                                      //変更フラグ
                             ,$page_no                                                //改ページ番号
                             );
            $i++;
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'ピッキングリスト');
        $this->xml_output($data,join("\t",$columns),$layout_name['11'],$fix_value);
    }
    
    /**
     *
     */
    function index (){
        $this->add_search();
    }
    
    /**
     * シール発行：検索
     */
    function add_search () {
        App::import('Sanitize');
        $this->setRoleFunction(9); //部署シール発行
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }

        // 印刷順序設定
        $this->_printType = array("部署、棚番号","棚番号、部署");
        $this->set('printType',$this->_printType);

        // 管理区分セット
        $this->__getWorkClasses();
        $work_classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected') ,  self::WORK_CLASSES_STICKER);

        //要求作業区分
        $this->set('promise_class' , $this->getClassesList($this->Session->read('Auth.facility_id_selected'),array(2,3)));

        // 検索実行
        $_SearchResult = array();
        $search_option = array();
        if(isset($this->request->data['IsSearch'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            // 検索条件のテーブル問い合わせ設定
            $search_option = array();
            $where = '';
            // conditions for MstFacility
            if(isset($this->request->data['MstFacility']['facilityCode']) && $this->request->data['MstFacility']['facilityCode'] != ''){
                $where .= " and e.facility_code = '" . $this->request->data['MstFacility']['facilityCode'] . "'";
                $search_option["conditions"][] = array("MstFacility.facility_code = " => $this->request->data['MstFacility']['facilityCode']);
            }
            // 引当日from
            if(isset($this->request->data['TrnFixedPromise']['work_date_start']) && $this->request->data['TrnFixedPromise']['work_date_start'] != ''){
                $where .= " and TrnFixedPromise.work_date >= '" . $this->request->data['TrnFixedPromise']['work_date_start'] . "'";
                $search_option["conditions"][] = array("TrnFixedPromise.work_date >= " =>date('Y/m/d', strtotime($this->request->data['TrnFixedPromise']['work_date_start'])));
            }
            // 引当日to
            if(isset($this->request->data['TrnFixedPromise']['work_date_end']) && $this->request->data['TrnFixedPromise']['work_date_end'] != ''){
                $where .= " and TrnFixedPromise.work_date <= '" . $this->request->data['TrnFixedPromise']['work_date_end'] . "'";
                $search_option["conditions"][] = array("TrnFixedPromise.work_date <= " =>date('Y/m/d', strtotime($this->request->data['TrnFixedPromise']['work_date_end'])));
            }
            // 定数・臨時
            if(isset($this->request->data['TrnFixedPromise']['promise_type']) && $this->request->data['TrnFixedPromise']['promise_type'] != ''){
                $where .= ' and TrnFixedPromise.promise_type = ' . $this->request->data['TrnFixedPromise']['promise_type'];
                $search_option["conditions"][] = array("TrnFixedPromise.promise_type = " => $this->request->data['TrnFixedPromise']['promise_type']);
            }
            // 商品ID(完全一致)
            if(isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] != ''){
                $where .= " and c.internal_code ='" . $this->request->data['MstFacilityItem']['internal_code'] . "'";
                $search_option["conditions"][] = array("MstFacilityItem.internal_code =" => $this->request->data['MstFacilityItem']['internal_code']);
            }
            // 製品番号(前方一致)
            if(isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] != ''){
                $where .= " and c.item_code ILIKE '" . $this->request->data['MstFacilityItem']['item_code'] ."%'";
                $search_option["conditions"][] =  array("MstFacilityItem.item_code ILIKE " => $this->request->data['MstFacilityItem']['item_code']."%");
            }
            // 商品名(部分一致)
            if(isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] != ''){
                $where .= " and c.item_name ILIKE '%" . $this->request->data['MstFacilityItem']['item_name'] . "%'";
                $search_option["conditions"][] = array("MstFacilityItem.item_name ILIKE " => "%".$this->request->data['MstFacilityItem']['item_name']."%");
            }
            // 規格(部分一致)
            if(isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] != ''){
                $where .= " and c.standard ILIKE '%" . $this->request->data['MstFacilityItem']['standard'] . "%'";
                $search_option["conditions"][] = array("MstFacilityItem.standard ILIKE " => "%".$this->request->data['MstFacilityItem']['standard']."%");
            }
            // JANコード(前方一致)
            if(isset($this->request->data['MstFacilityItem']['jan_code']) && $this->request->data['MstFacilityItem']['jan_code'] != ''){
                $where .= " and c.jan_code ILIKE '"  .$this->request->data['MstFacilityItem']['jan_code'] . "%'";
                $search_option["conditions"][] = array("MstFacilityItem.jan_code ILIKE " => "" .$this->request->data['MstFacilityItem']['jan_code']."%");
            }
            // 販売元(部分一致)
            if(isset($this->request->data['MstDealer']['dealer_name']) && $this->request->data['MstDealer']['dealer_name'] != '') {
                $where .= " and f.dealer_name ILIKE '%" .$this->request->data['MstDealer']['dealer_name'] . "%'";
                $search_option["conditions"][] = array("MstDealer.dealer_name ILIKE " => "%".$this->request->data['MstDealer']['dealer_name']."%");
            }
            // 定数区分
            if(isset($this->request->data['MstFixedCount']['trade_type']) && $this->request->data['MstFixedCount']['trade_type'] != '') {
                $where .= ' and g.trade_type = ' . $this->request->data['MstFixedCount']['trade_type'];
                $search_option["conditions"][] = array("MstFixedCount.trade_type" => $this->request->data['MstFixedCount']['trade_type']);
            }

            //要求時作業区分
            if(isset($this->request->data['TrnPromise']['work_type']) && $this->request->data['TrnPromise']['work_type'] != '') {
                $where .= ' and j.work_type = ' . $this->request->data['TrnPromise']['work_type'];
                $search_option["conditions"][] = array("TrnPromise.work_type" => $this->request->data['TrnPromise']['work_type']);
            }
            if($this->Session->read('Auth.Config.Shipping') == 1){
                $where .= ' and 1 = 0 ';
            }
            
            $sql  = ' select ';
            $sql .= '       e.id                            as "TrnStickerIssue__id" ';
            $sql .= '     , e.facility_name                 as "TrnStickerIssue__facility_name" ';
            $sql .= '     , e.facility_code                 as "TrnStickerIssue__facility_code" ';
            $sql .= '     , TrnFixedPromise.department_id_from as "TrnStickerIssue__department_id" ';
            $sql .= '     , d.department_name               as "TrnStickerIssue__department_name" ';
            $sql .= "     , to_char(TrnFixedPromise.work_date , 'YYYY/mm/dd') ";
            $sql .= '                                       as "TrnStickerIssue__work_date" ';
            $sql .= '     , sum(TrnFixedPromise.remain_count) as "TrnStickerIssue__count" ';
            $sql .= '   from ';
            $sql .= '     trn_fixed_promises as TrnFixedPromise  ';
            $sql .= '     inner join mst_item_units as b  ';
            $sql .= '       on TrnFixedPromise.mst_item_unit_id = b.id  ';
            $sql .= '     inner join mst_facility_items as c  ';
            $sql .= '       on b.mst_facility_item_id = c.id  ';
            $sql .= '      and c.is_lowlevel = false ';
            $sql .= '     inner join mst_departments as d  ';
            $sql .= '       on TrnFixedPromise.department_id_from = d.id  ';
            $sql .= '     inner join mst_facilities as e  ';
            $sql .= '       on d.mst_facility_id = e.id  ';
            $sql .= '     left join mst_dealers as f  ';
            $sql .= '       on c.mst_dealer_id = f.id  ';
            $sql .= '     left join mst_fixed_counts as g  ';
            $sql .= '       on g.mst_department_id = d.id  ';
            $sql .= '       and g.mst_item_unit_id = b.id  ';
            $sql .= '     inner join mst_facility_relations as h  ';
            $sql .= '       on h.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '       and h.partner_facility_id = e.id  ';
            $sql .= '       and h.is_deleted = false  ';
            $sql .= '     inner join mst_user_belongings as i  ';
            $sql .= '       on i.mst_facility_id = e.id  ';
            $sql .= '       and i.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '       and i.is_deleted = false  ';

            $sql .= '     left join trn_promises as j  ';
            $sql .= '       on j.id = TrnFixedPromise.trn_promise_id  ';

            $sql .= '   where ';
            $sql .= '     TrnFixedPromise.remain_count > 0  ';
            $sql .= '     and TrnFixedPromise.is_deleted = false  ';
            $sql .= '     and c.is_lowlevel = false ';
            $sql .= $where;
            $sql .= '   group by ';
            $sql .= '     TrnFixedPromise.work_date ';
            $sql .= '     , e.id ';
            $sql .= '     , e.facility_name ';
            $sql .= '     , e.facility_code ';
            $sql .= '     , d.department_code ';
            $sql .= '     , TrnFixedPromise.department_id_from ';
            $sql .= '     , d.department_name  ';
            $sql .= '   order by ';
            if(is_null($this->getSortOrder())){
                $sql .= '     e.facility_code asc ';
                $sql .= '     , d.department_code asc ';
                $sql .= '     , TrnFixedPromise.work_date asc ';
            }else{
                $sql .= join(',' , $this->getSortOrder());
            }
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnFixedPromise'));
            $sql .= ' limit ' . $this->_getLimitCount();

            $_SearchResult = $this->TrnFixedPromise->query($sql);
            $this->request->data = $data;
        }else{
            //初期設定
            $this->request->data['TrnFixedPromise']['work_date_start'] = date('Y/m/d',strtotime("-6 day"));
            $this->request->data['TrnFixedPromise']['work_date_end']   = date('Y/m/d');
            $this->request->data['TrnStickerIssue']['print_type']      = "1";
        }
        
        //施設取得
        $this->set('facility_List', $this->getFacilityList(
            $this->Session->read('Auth.facility_id_selected'), // 操作しているセンター施設ID
            array(Configure::read('FacilityType.hospital'))      // 病院のみ
            )
        );
        $this->Session->write('UserSearchOption', $search_option);
        $this->set('SearchResult', $_SearchResult);
        $this->render('/stickerissues/add_search');
    }

    /**
     * シール発行確認
     */
    function add_confirm () {
        $this->setRoleFunction(9); //部署シール発行
        //施設名
        if(!empty($this->request->data['MstFacility']['facility_id'])){
            $this->request->data['MstFacility']['facility_name'] = $this->getFacilityName($this->request->data['MstFacility']['facility_id']);
        }

        //作業区分データセット
        $this->__getWorkClasses();
        $_stickerissuesCondition = array();
        // 行検索条件の取得
        foreach($this->request->data['TrnStickerIssue']['Rows'] as $_key=>$_value){
            if($_value == '1'){
                $_stickerissuesCondition[] = $_key.",".$this->request->data['TrnFixedPromise']['promise_type'];
            }
        }

        // 行検索条件の保存
        $this->Session->write('TrnStickerIssue.stickerissuesCondition', $_stickerissuesCondition);
        // ユーザー検索条件の取得
        $_UserSarch = $this->Session->read('UserSearchOption');
        //workdate set
        $this->request->data['TrnStickerIssue']['work_date'] = date('Y/m/d');

        // リスト表示
        $_searchResult = array();
        for($i = 0; $i < count($_stickerissuesCondition); $i++){
            $_r = array();
            $_arr_seach = array();
            $search_option["conditions"] = array();
            $_arr_seach = explode(",",$_stickerissuesCondition[$i]);

            // レコード毎に検索条件を設定し検索
            $search_option["conditions"][] = array("MstFacility.id = " => $_arr_seach[0]);
            $search_option["conditions"][] = array("TrnFixedPromise.department_id_from = " => $_arr_seach[1]);
            $search_option["conditions"][] = array("TrnFixedPromise.work_date = " => $_arr_seach[2]);
            if($_arr_seach[3] !== ''){
                $search_option["conditions"][] = array("TrnFixedPromise.promise_type = " => $_arr_seach[3]);
            }

            if (isset($_UserSarch['conditions'])) {
                // ユーザー検索条件結合
                array_push($search_option['conditions'], $_UserSarch['conditions']);
            }
            $_res = $this->getSearchGroupParamater($search_option);
            array_walk_recursive($_res, array('StickerIssuesController','BuildSearchResult'), &$_r);
            if($_res !== array()){
                $_searchResult[] = $_r;
            }
        }
        $this->Session->write('TrnStickerIssue.SearchResult', $_searchResult);
        $this->Session->write('UserSearchOption', $_UserSarch);
        $_searchResult = AppController::outputFilter($_searchResult);
        $this->set('SearchResult', $_searchResult);
        $this->set('IsShowFinal', true);

        
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
        $this->render('/stickerissues/add_confirm');
    }

    /**
     * シール発行結果(飛び)
     */
    function add_result_view () {
        $errorFlg = false;
        $_searchResult = $this->Session->read('TrnSticker.SearchResult');
        $this->request->data = $this->Session->read('TrnStickerIssue.data');
        $_printtype = $this->Session->read('TrnStickerIssue.print_type');
        $_searchResult = AppController::outputFilter($_searchResult);
        $this->set('stickerNos', $this->Session->read('TrnStickerIssue.stickerNos'));
        $this->set('printOrder', $this->Session->read('TrnStickerIssue.printOrder'));
        $this->set('SearchResult', $_searchResult);
        $this->set('printType',$_printtype);
        $this->set('ErrorFlg', $errorFlg);
        $this->set('IsShowFinal', true);
        $this->render('/stickerissues/add_result_view');
    }

    /**
     * シール発行処理
     */
    function add_result () {
        //トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            $SearchResult = $this->Session->read('SearchResult');
            $this->Session->delete('SearchResult');
            $Msg = $this->Session->read('Msg');
            $this->Session->delete('Msg');
            $ErrorFlg = $this->Session->read('ErrorFlg');
            $this->Session->delete('ErrorFlg');
            $IsShowFinal = $this->Session->read('IsShowFinal');
            $this->Session->delete('IsShowFinal');
            $this->set('SearchResult', $SearchResult);
            $this->set('Msg', $Msg);
            $this->set('ErrorFlg', $ErrorFlg);
            $this->set('IsShowFinal', $IsShowFinal);
            $this->redirect('/stickerissues/add_result_view');
            return;
        }

        $now = date('Y/m/d H:i:s');
        //トランザクション開始
        $this->TrnStickerIssue->begin();

        // 同一情報検索/DIFF
        $_searchResult = array();
        //etc
        $msg = "";
        $errorFlg = false;

        $stickerNos = array();

        $_preSearch = $this->Session->read('TrnStickerIssue.SearchResult');
        $_stickerissuesCondition = $this->Session->read('TrnStickerIssue.stickerissuesCondition');
        // ユーザー検索条件の取得
        $_UserSarch = $this->Session->read('UserSearchOption');

        // 表示以後にシールデータが更新されていないか
        if(null === $_preSearch || null === $_stickerissuesCondition){
            $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。','growl',array('type'=>'error'));
            $this->redirect('add_search');
            return;
        }

        // oder type 部署、棚番号 or 棚番号、部
        // TODO 詳細情報取得時に下記sortを行う必要があるのか？
        if($this->request->data['TrnStickerIssue']['print_type'] == 1) {
            $_search_order['order'] = array('MstDepartment_from.department_name,MstShelfName_from.code,TrnFixedPromise.promise_type');
            $_print_order['print_order'] = '病院部署名,病院棚番号';

        }else if($this->request->data['TrnStickerIssue']['print_type'] == 0) {
            $_search_order['order'] = array('MstShelfName_from.code,MstDepartment_from.department_name,TrnFixedPromise.promise_type');
            $_print_order['print_order'] = '病院棚番号,病院部署名';

        }else if($this->request->data['TrnStickerIssue']['print_type'] == 2) {
            $_search_order['order'] = array('MstShelfName_from.code,MstFacility.facility_code,MstDepartment_from.department_name,TrnFixedPromise.promise_type');
            $_print_order['print_order'] = '病院棚番号,商品ID,病院部署名';
        }

        $this->Session->write('TrnStickerIssue.printOrder',$_print_order['print_order']);
        $this->Session->write('TrnStickerIssue.print_type',$this->request->data['TrnStickerIssue']['print_type']);

        // 行検索項目保存
        $this->Session->write('TrnStickerIssue.stickerissuesCondition', $_stickerissuesCondition);

        $_searchResult = array();
        for($i = 0; $i < count($_stickerissuesCondition); $i++){
            $_r = array();
            $_arr_seach = array();
            $search_option['conditions'] = array();
            $_arr_seach = explode(",",$_stickerissuesCondition[$i]);

            // レコード毎に検索条件を設定し検索
            $search_option['conditions'][] = array("MstFacility.id = " => $_arr_seach[0]);
            $search_option['conditions'][] = array("TrnFixedPromise.department_id_from = " => $_arr_seach[1]);
            $search_option['conditions'][] = array("TrnFixedPromise.work_date = " => $_arr_seach[2]);
            if($_arr_seach[3] !== ''){
                $search_option["conditions"][] = array("TrnFixedPromise.promise_type = " => $_arr_seach[3]);
            }
            // ユーザー検索条件結合
            if (isset($_UserSarch['conditions'])) {
                array_push($search_option['conditions'], $_UserSarch['conditions']);
            }

            $_res = $this->getSearchGroupParamater($search_option);

            // 作業番号取得
            $work_no = $this->setWorkNo4Header(date('Y/m/d'),'05');

            // 設定した作業区分と備考を配列に追加
            $_res[$i][] =  $this->request->data['TrnStickerIssue']['Rows'][$i];
            $_res[$i][]['work_no'] = $work_no;
            // 配列の整列
            array_walk_recursive($_res, array('StickerIssuesController','BuildSearchResult'), &$_r);

            if($_res !== array()){
                $_searchResult[] = $_r;
            }
        }
        $this->Session->write('TrnSticker.SearchResult', $_searchResult);

        // 表示以後に引当確定データが更新されていないか
        if(true === $this->hasRemainsDifference($_searchResult, $_preSearch)){
            $this->redirect('add_search');
            $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。','growl',array('type'=>'error'));
            $this->set('IsShowFinal', false);
            return;
        }

        for($i = 0; $i < count($_stickerissuesCondition); $i++){
            $_r = array();
            $_arr_seach = array();
            $search_option['conditions'] = array();

            $_arr_seach = explode(",",$_stickerissuesCondition[$i]);

            // レコード毎に検索条件を設定し検索
            $search_option['conditions'][] = array("MstFacility.id = " => $_arr_seach[0]);
            $search_option['conditions'][] = array("TrnFixedPromise.department_id_from = " => $_arr_seach[1]);
            $search_option['conditions'][] = array("TrnFixedPromise.work_date = " => $_arr_seach[2]);
            if($_arr_seach[3] !== ''){
                $search_option['conditions'][] = array("TrnFixedPromise.promise_type = " => $_arr_seach[3]);
            }

            // ユーザー検索条件結合
            if (isset($_UserSarch['conditions'])) {
                array_push($search_option['conditions'], $_UserSarch['conditions']);
            }

            // シール作成履歴ヘッダー作成
            $this->TrnStickerIssueHeader->create();
            $res1 = $this->TrnStickerIssueHeader->save(array(
                'work_no'                  => $_searchResult[$i]['work_no']
                ,'work_date'               => date('Y/m/d')
                ,'mst_department_id'       => $_searchResult[$i]['department_id_from']
                ,'work_type'               => $this->request->data['TrnStickerIssueHeader']['work_class']
                ,'recital'                 => $this->request->data['TrnStickerIssueHeader']['recital']
                ,'creater'                 => $this->Session->read('Auth.MstUser.id')
                ,'created'                 => $now
                ,'modifier'                => $this->Session->read('Auth.MstUser.id')
                ,'modified'                => $now
                ,'detail_count'            => $_searchResult[$i]['sum_remains']
                ,'is_deleted'              => false
                ));
            if(!$res1){
                //TODO エラー処理
                $msg .= "シール発行履歴ヘッダー登録エラー";
                $errorFlg = true;
                return false;
            }

            // インサートしたヘッダID取得
            $_lastId = $this->TrnStickerIssueHeader->getLastInsertID();

            // 明細情報検索
            $_res = $this->getSearchDetailParamater($search_option,$_search_order);

            // シール作成履歴登録
            // TODO Please check the key array of this loop.
            // foreach ($_res as $key) {
            $tmp=array();
            for($_rescnt = 0; $_rescnt < count($_res); $_rescnt++){
                // 引当残数分のシールを発行する（発行履歴レコード作成）
                for($n = 0; $n < $_res[$_rescnt]['TrnFixedPromise']['remain_count']; $n++){
                    // 部署シール番号取得
                    $_hospital_sticker_no = $this->getHospitalStickerNo(date('ymd'),$_searchResult[0]['facility_code']);
                    $stickerNos[] = $_hospital_sticker_no;
                    $this->TrnStickerIssue->create();
                    $param = array(
                        'id'                           => false
                        ,'work_no'                     => $_searchResult[$i]['work_no']
                        ,'work_seq'                    => $n+1
                        ,'work_date'                   => date('Y/m/d')
                        ,'mst_item_unit_id'            => $_res[$_rescnt]['MstItemUnit']['id']
                        ,'department_id_to'            => $_res[$_rescnt]['TrnFixedPromise']['department_id_from']
                        ,'department_id_from'          => $_res[$_rescnt]['TrnFixedPromise']['department_id_to']
                        ,'trn_fixed_promise_id'        => $_res[$_rescnt]['TrnFixedPromise']['id']
                        ,'promise_type'                => $_res[$_rescnt]['TrnFixedPromise']['promise_type']
                        ,'fixed_promise_status'        => $_res[$_rescnt]['TrnFixedPromise']['fixed_promise_status']
                        ,'recital'                     => $this->request->data['TrnStickerIssue']['Rows'][$i]['recital']
                        ,'trn_sticker_issue_header_id' => $_lastId
                        ,'hospital_sticker_no'         => $_hospital_sticker_no
                        ,'creater'                     => $this->Session->read('Auth.MstUser.id')
                        ,'created'                     => $now
                        ,'modifier'                    => $this->Session->read('Auth.MstUser.id')
                        ,'modified'                    => $now
                        ,'quantity'                    => 1
                        ,'is_deleted'                  => false
                        );
                    if(isset($this->request->data['TrnStickerIssue']['Rows'][$i]['work_class'])){
                        $param['work_type'] = $this->request->data['TrnStickerIssue']['Rows'][$i]['work_class'];
                    }
                    $res2 = $this->TrnStickerIssue->save($param);
                    if(!$res2){
                        //TODO エラー処理
                        $msg .= "シール発行履歴登録エラー,";
                        $errorFlg = true;
                        return false;
                    }
                    // 印刷枚数合計カウント
                    $_sum_stickers = $n+1;
                }
                // 引当のIDを持っておく
                $tmp[] = $_res[$_rescnt]['TrnFixedPromise']['id'];
                // 引当確定残数を減らす
                $condtion = array( 'TrnFixedPromise.id' => $_res[$_rescnt]['TrnFixedPromise']['id'],);
                $updatefield = array(  // 在庫数減らす
                                       'remain_count'     => (int)$_res[$_rescnt]['TrnFixedPromise']['remain_count'] - (int)$_sum_stickers,
                                       // シール発行状態に変更
                                       'fixed_promise_status' => Configure::read('PromiseStatus.SealIssue'),
                                       'modifier'             => $this->Session->read('Auth.MstUser.id'),
                                       'modified'             => "'" . $now . "'",
                                       );
                //MstUserオブジェクトをcreate
                $this->TrnFixedPromise->create();

                //SQL実行
                if (!$this->TrnFixedPromise->updateAll( $updatefield, $condtion )) {
                    $msg .= "引当確定残数エラー,";
                    $errorFlg = true;
                    return false;
                }
            }
        }

        $this->set('SearchResult', $_searchResult);
        $this->set('Msg', $msg);
        $this->set('ErrorFlg', $errorFlg);
        $this->set('IsShowFinal', true);

        if($errorFlg){
            $this->TrnStickerIssue->rollback();
            $this->Session->setFlash('登録処理に失敗しました。最初から作業をやり直してください。','growl',array('type'=>'error'));
            $this->redirect('add_search');
            return;
        }else{
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_fixed_promises as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $tmp). ')';
            $sql .= "     and a.modified > '" . $this->request->data['TrnStickerIssue']['time'] ."'";
            $sql .= "     and a.modified <> '" . $now ."'";

            $ret = $this->TrnStickerIssue->query($sql);

            if($ret[0][0]['count']> 0){
                $this->TrnStickerIssue->rollback();
                $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。','growl',array('type'=>'error'));
                $this->redirect('add_search');
                return;
            }

            $this->TrnStickerIssue->commit();
            // シール印刷用データ保存
            $this->Session->write('TrnStickerIssue.data', $this->request->data);
            $this->Session->write('TrnStickerIssue.stickerNos', $stickerNos);

            
            $this->Session->write('SearchResult', $_searchResult);
            $this->Session->write('Msg', $msg);
            $this->Session->write('ErrorFlg', $errorFlg);
            $this->Session->write('IsShowFinal', true);

            // 結果表示用画面へ遷移
            $this->redirect('/stickerissues/add_result_view');
        }
        $this->render('/stickerissues/add_result');
    }

  /**
   * シール発行履歴
   */
    function history () {
        $this->setRoleFunction(10);//シール発行履歴
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        //作業区分データセット
        $this->__getWorkClasses();
        //施設取得
        $this->set('facility_List', $this->getFacilityList(
            $this->Session->read('Auth.facility_id_selected'), // 操作しているセンター施設ID
            array(Configure::read('FacilityType.hospital')),     // 病院のみ
            false                                              // 取引関係削除フラグは見ない
            )
        );

        $department_List = array();

        if(isset($this->request->data['TrnStickerIssueHeader']['facilityCode'])) {
            $department_List =  $this->getDepartmentsList(
                $this->request->data['TrnStickerIssueHeader']['facilityCode'], // 選択している施設コード
                false                                                 // 削除フラグは見ない
                );
        }
        $this->set('department_List' , $department_List);

        //初期値
        $result = array();

        if(isset($this->request->data['selected']['is_search'])) {
            //検索ボタン押下
            //最大件数取得
            $this->_historySQL($this->request->data , $this->_getLimitCount() , false , true );
            //履歴情報取得
            $result = $this->TrnStickerIssueHeader->query($this->_historySQL($this->request->data , $this->_getLimitCount() , false ));
        }else{
            $this->request->data['TrnStickerIssueHeader']['work_date_start'] = date('Y/m/d',strtotime("-6 day"));
            $this->request->data['TrnStickerIssueHeader']['work_date_end'] = date('Y/m/d');
        }
        $this->set('result',$result);
        $this->render('/stickerissues/history');
    }

  /**
   * シール発行履歴：取消
   */
    function edit () {
        // 管理区分取得
        $this->set('result' , $this->TrnStickerIssue->query($this->_historySQL($this->request->data , $this->_getLimitCount() , true )));
    }

    /**
     * シール発行履歴：取消結果(飛び)
     */
    function edit_result () {
        $now = date('Y/m/d H:i:s');

        //トランザクション開始
        $this->TrnStickerIssue->begin();

        $strTargetIds = implode(",",$this->request->data['TrnStickerIssue']['id']);
        //行ロック
        $this->TrnStickerIssue->query('select * from trn_sticker_issues as a where a.id in ('.$strTargetIds.') for update ');

        //更新前チェック
        $opeDate = $this->request->data['operationDate'];
        $sql  = 'SELECT TrnStickerIssue.modified AS "TrnStickerIssue__modified"';
        $sql .= '  FROM trn_sticker_issues AS TrnStickerIssue';
        $sql .= ' WHERE TrnStickerIssue.id IN ('.$strTargetIds.")";

        $modifiedList = $this->TrnStickerIssue->query($sql);

        foreach($modifiedList as $row){
            //更新時間のチェック
            if($opeDate <= $row['TrnStickerIssue']['modified']){
                $this->Session->setFlash('別ユーザによってデータが更新されています', 'growl', array('type'=>'error') );
                $this->history();
                return;
            }
        }

        //更新
        $_res1 = $this->TrnStickerIssue->updateAll(
            array(
                'TrnStickerIssue.is_deleted' => 'true',
                'TrnStickerIssue.modified'   => "'" . $now ."'",
                'TrnStickerIssue.modifier'   => $this->Session->read('Auth.MstUser.id')
                ),
            array('TrnStickerIssue.id'=>$this->request->data['TrnStickerIssue']['id']));

        if ($_res1) {
            $_list = array();
            $_delete_all_chk = false;
            $_quantity = 0;
            $_remain_count = 0;
            $_fxpromise_id = '';
            /**
             * 引当確定データの更新
             * 部署シール削除数分、引当確定のremain_countを増やし
             * ステータスを戻す（すべて戻す＞引当済み、一部戻す＞一部処理済み）
             */
            $sql  = ' select ';
            $sql .= '     a.id as trn_sticker_issue_id ';
            $sql .= '   , b.id as trn_fixed_promise_id ';
            $sql .= '   , b.remain_count ';
            $sql .= '   , b.quantity  ';
            $sql .= ' from ';
            $sql .= '   trn_sticker_issues as a  ';
            $sql .= '   left join trn_fixed_promises as b  ';
            $sql .= '     on b.id = a.trn_fixed_promise_id  ';
            $sql .= ' where ';
            $sql .= '   a.id in (' . join(',',$this->request->data['TrnStickerIssue']['id']) . ') ';
            
            $_fixed_promise_info = $this->TrnStickerIssue->query($sql);
            
            // 同一IDなら数を加算し、IDが切り替わる際にUPDATE処理
            for($_i=0; $_i < count($_fixed_promise_info); $_i++){
                $_fxpromise_id = $_fixed_promise_info[$_i][0]['trn_fixed_promise_id'];
                $_remain_count++;
                if(!isset($_fixed_promise_info[$_i+1][0]) || $_fxpromise_id !== $_fixed_promise_info[$_i+1][0]['trn_fixed_promise_id']){
                    // 現時点の引当確定残数を取得
                    $_now_remain = $_fixed_promise_info[$_i][0]['remain_count'];
                    $_res2;
                    if(intval($_fixed_promise_info[$_i][0]['quantity']) === (intval($_now_remain) + intval($_remain_count))){
                        // 初期
                        $_status = Configure::read('PromiseStatus.Init');
                    }else{
                        // 一部処理済み
                        $_status = Configure::read('PromiseStatus.PartMatching');
                    }
                    $_set_param = array('TrnFixedPromise.remain_count'=>(int)$_now_remain + (int)$_remain_count,
                                        'TrnFixedPromise.fixed_promise_status'=>$_status,
                                        'modifier'        => $this->Session->read('Auth.MstUser.id'),
                                        'modified'        => "'".$now."'"
                                        );
                    $this->TrnFixedPromise->create();
                    $_res2 = $this->TrnFixedPromise->updateAll($_set_param,array('TrnFixedPromise.id'=>$_fxpromise_id));
                    if($_res2){
                        // 部署シールのすべての明細が取り消されているか確認(すべて取り消されている場合、ヘッダーIDが返る)
                        $_delete_all_chk = $this->chkStickerIssueDeleteAll($_fixed_promise_info[$_i][0]['trn_sticker_issue_id']);
                        if($_delete_all_chk){
                            $_res3 = $this->TrnStickerIssueHeader->updateAll(array('TrnStickerIssueHeader.is_deleted'=>'true'),array('TrnStickerIssueHeader.id'=>$_delete_all_chk));
                            if(!$_res3){
                                $this->TrnStickerIssue->rollback();
                                $this->Session->setFlash('取消が失敗しました。', 'growl', array('type'=>'error') );
                                $this->redirect('history');
                            }
                        }
                        $_remain_count = 0;
                    }else{
                        $this->TrnStickerIssue->rollback();
                        $this->Session->setFlash('取消が失敗しました。', 'growl', array('type'=>'error') );
                        $this->redirect('history');
                    }
                }
            }
            //コミット直前に更新チェック
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_sticker_issues as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $this->request->data['TrnStickerIssue']['id']). ')';
            $sql .= "     and a.modified > '" . $opeDate ."'";
            $sql .= "     and a.modified <> '" . $now ."'";

            $ret = $this->TrnStickerIssue->query($sql);

            if($ret[0][0]['count']> 0){
                $this->TrnStickerIssue->rollback();
                $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。','growl',array('type'=>'error'));
                $this->redirect('history');
                return;
            }


            $this->TrnStickerIssue->commit();
            $this->set("result" , $this->TrnStickerIssue->query($this->_historySQL( $this->request->data , $this->_getLimitCount() , true )));
            $this->Session->setFlash('取消が行われました。', 'growl', array('type'=>'star') );
            $this->render('/stickerissues/edit_result_view');
        }else{
            $this->TrnStickerIssue->rollback();
            $this->Session->setFlash('取消が失敗しました。', 'growl', array('type'=>'error') );
            $this->redirect('history');
        }
    }

    /**
     * シール発行履歴結果表示
     */
    function edit_result_view () {
        if ($this->Session->check('StickerIssues.target_stickers') && $this->Session->check('StickerIssues.target_ids')) {
            $_sticker_issues = array();
            $_target_ids = $this->Session->read('StickerIssues.target_ids');
            foreach ($this->Session->read('StickerIssues.target_stickers') as $data) {
                if (in_array($data['id'], $_target_ids)) {
                    $_sticker_issues[] = $data;
                }
            }
            $this->set('sticker_issues', $_sticker_issues);
        } else {
            $this->set('sticker_issues', array());
            $this->redirect('history');
        }

        $this->Session->delete('StickerIssues');
        $this->render('/stickerissues/edit_result_view');
    }

    /**
     * 部署シール発行履歴CSV出力
     */
    function export_csv() {
        $sql = $this->_getStickerIssueCSV($this->request->data);
        $this->db_export_csv($sql , "部署シール発行履歴", '/stickerissues/history');
    }

    private function _getStickerIssueCSV($data) {
        $this->request->data = $data;

        App::import('Sanitize');
        // 検索処理
        $where = " 1 = 1 and ( c.shipping_type != " . Configure::read('ShippingType.deposit') . " or c.shipping_type is null ) ";

        //発行番号
        if( (!empty($this->request->data['TrnStickerIssueHeader']['work_no']) ) ){
            $where .=" and a.work_no LIKE '%".Sanitize::escape($this->request->data['TrnStickerIssueHeader']['work_no'])."%'";
        }
        //要求日FROM
        if( (!empty($this->request->data['TrnStickerIssueHeader']['work_date_start']) ) ){
            $where .= " and a.work_date >= '" . $this->request->data['TrnStickerIssueHeader']['work_date_start'] ."'";
        }
        //要求日TO
        if( (!empty($this->request->data['TrnStickerIssueHeader']['work_date_end']) ) ){
            $where .= " and a.work_date <= '" . $this->request->data['TrnStickerIssueHeader']['work_date_end'] ."'";
        }
        //未出荷を表示
        if(isset($this->request->data['TrnStickerIssueHeader']['no_shipping']) && $this->request->data['TrnStickerIssueHeader']['no_shipping'] == "1"){
            $where .= ' and a.quantity >= 1 ';
        }
        //取消も表示
        if(isset($this->request->data['TrnStickerIssueHeader']['delete_opt']) && $this->request->data['TrnStickerIssueHeader']['delete_opt'] == "0"){
            $where .= ' and a.is_deleted = false ';
            $where .= ' and o.is_deleted = false ';
        }
        //商品ID
        if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
            $where .= " and e.internal_code = '" . Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']) . "'";
        }
        //製品番号
        if(!empty($this->request->data['MstFacilityItem']['item_code'])){
            $where .= " and e.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        //規格
        if(!empty($this->request->data['MstFacilityItem']['standard'])){
            $where .= " and e.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }

        //発行施設
        if(!empty($this->request->data['TrnStickerIssueHeader']['facility_id'])){
            $where .= " and k.facility_code = '". $this->request->data['TrnStickerIssueHeader']['facility_id'] ."'";
            //発行部署
            if(!empty($this->request->data['TrnStickerIssueHeader']['department_code'])){
                $where .= " and j.department_code = '" . $this->request->data['TrnStickerIssueHeader']['department_code']."'";
                $where .= ' and j.mst_facility_id = k.id';
            }

        }
        //管理区分
        if(!empty($this->request->data['TrnFixedPromise']['promise_type'])){
            $where .= ' and a.promise_type = ' . $this->request->data['TrnFixedPromise']['promise_type'];
        }
        //商品名
        if(!empty($this->request->data['MstFacilityItem']['item_name'])){
            $where .= " and e.item_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%'";
        }
        //販売元
        if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
            $where .= " and i.dealer_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name']) . "%'";
        }
        //作業区分
        if(!empty($this->request->data['TrnStickerIssueHeader']['work_class'])) {
            $where .= ' and l.id = ' . $this->request->data['TrnStickerIssueHeader']['work_class'];
        }
        //シール番号
        if(!empty($this->request->data['TrnStickerIssue']['hospital_sticker_no']) && !is_array($this->request->data['TrnStickerIssue']['hospital_sticker_no'])) {
            $where .= " and a.hospital_sticker_no  = '" . $this->request->data['TrnStickerIssue']['hospital_sticker_no'] . "'";
        }
        //定数区分
        if(!empty($this->request->data['MstFixedCount']['trade_type'])) {
            $where .= ' and a1.trade_type = ' . $this->request->data['MstFixedCount']['trade_type'];
        }

        $sql  = 'select ';
        $sql .= '      a.work_no             as "発行番号"';
        $sql .= "    , to_char(a.work_date , 'YYYY/mm/dd') ";
        $sql .= '                            as "発行日"';
        $sql .= "    , k.facility_name || '/' || j.department_name ";
        $sql .= '                            as "施設／部署名"';
        $sql .= '    , e.item_name           as 商品名';
        $sql .= '    , e.standard            as 規格';
        $sql .= '    , e.item_code           as 製品番号';
        $sql .= '    , i.dealer_name         as 販売元名';
        $sql .= '    , (case ';
        $sql .= '          when d.per_unit = 1  then m.unit_name ';
        $sql .= "          else m.unit_name || '(' || d.per_unit || f.unit_name || ')'";
        $sql .= '      end )                 as 包装単位名';
        $sql .= '    , e.internal_code       as "商品ID"';
        $sql .= '    , n.medical_code        as 医事コード';
        $sql .= '    , a.hospital_sticker_no as 部署シール';
        $sql .= '    , b.lot_no              as ロット番号';
        $sql .= "    , to_char(b.validated_date , 'YYYY/mm/dd') ";
        $sql .= '                            as 有効期限';
        $sql .= '    , l.name                as 作業区分名';
        $sql .= '    , ( case ';
        $sql .= "          when a3.promise_type = " . Configure::read('PromiseType.Constant') . " then '定数' ";
        $sql .= "          when a3.promise_type = " . Configure::read('PromiseType.Temporary'). " then '臨時' ";
        $sql .= "          else '' ";
        $sql .= '      end )                 as 管理区分名 ';
        $sql .= '    , h.user_name           as 更新者名';
        $sql .= '    , a.recital             as 備考';

        $sql .= '  from ';
        $sql .= '    trn_sticker_issues as a  ';
        $sql .= '    left join mst_fixed_counts as a1 ';
        $sql .= '      on a1.mst_department_id = a.department_id_to ';
        $sql .= '      and a1.mst_item_unit_id = a.mst_item_unit_id ';
        $sql .= '    left join trn_fixed_promises as a2 ';
        $sql .= '      on a.trn_fixed_promise_id = a2.id ';
        $sql .= '    left join trn_promises as a3 ';
        $sql .= '      on a2.trn_promise_id = a3.id ';
        $sql .= '    left join trn_stickers as b  ';
        $sql .= '      on a.hospital_sticker_no = b.hospital_sticker_no  ';
        $sql .= '      and b.is_deleted = false  ';
        $sql .= '    left join trn_shippings as c  ';
        $sql .= '      on b.trn_shipping_id = c.id  ';
        $sql .= '    left join mst_item_units as d  ';
        $sql .= '      on d.id = a.mst_item_unit_id  ';
        $sql .= '    left join mst_facility_items as e  ';
        $sql .= '      on e.id = d.mst_facility_item_id  ';
        $sql .= '    left join mst_unit_names as f  ';
        $sql .= '      on f.id = d.per_unit_name_id  ';
        $sql .= '    left join mst_users as h  ';
        $sql .= '      on h.id = a.modifier  ';
        $sql .= '    left join mst_dealers as i  ';
        $sql .= '      on i.id = e.mst_dealer_id  ';
        $sql .= '    left join mst_departments as j  ';
        $sql .= '      on j.id = a.department_id_to  ';
        $sql .= '    left join mst_facilities as k  ';
        $sql .= '      on k.id = j.mst_facility_id  ';
        $sql .= '    left join mst_classes as l  ';
        $sql .= '      on l.id = a.work_type  ';
        $sql .= '    left join mst_unit_names as m  ';
        $sql .= '      on m.id = d.mst_unit_name_id  ';
        $sql .= '    left join mst_medical_codes as n  ';
        $sql .= '      on n.mst_facility_item_id = e.id  ';
        $sql .= '      and n.mst_facility_id = k.id  ';
        $sql .= '    left join trn_sticker_issue_headers as o  ';
        $sql .= '      on o.id = a.trn_sticker_issue_header_id  ';
        $sql .= '    left join mst_users as p  ';
        $sql .= '      on o.modifier = p.id  ';
        $sql .= '    inner join mst_facility_relations as q  ';
        $sql .= '      on q.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '      and q.partner_facility_id = k.id  ';
        $sql .= '      and k.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '    inner join mst_user_belongings as r  ';
        $sql .= '      on r.mst_facility_id = k.id ';
        $sql .= '      and r.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '      and r.is_deleted = false';

        $sql .= '  where ';
        $sql .= $where ;

        $sql .= '  order by a.work_no desc ';
        $sql .=', a.hospital_sticker_no ';

        return $sql ;
    }

    /**
     * シール再発行
     */
    function republish_hospital_stikers () {
        App::import('Sanitize');
        $_hno = array();
        foreach ($this->request->data['TrnStickerIssue']['id'] as $k => $v) {
            $_hno[] = $this->request->data['TrnStickerIssue']['hospital_sticker_no'][$k];
        }

        if ($this->request->data['TrnStickerIssue']['print_type'] == 0) {
            $this->set('sortkey' , 'センター棚番号,病院情報,商品ID');
            $order = 'MstShelfNameTo.code asc,MstFacility.facility_name asc,MstDepartment.department_name asc,TrnFixedPromise.promise_type asc , TrnStickerIssue.hospital_sticker_no';

        } else if($this->request->data['TrnStickerIssue']['print_type'] == 1) {
            $this->set('sortkey' , '病院情報,センター棚番号,商品ID');
            $order = 'MstFacility.facility_name asc,MstDepartment.department_name asc, MstShelfNameTo.code asc,TrnFixedPromise.promise_type asc , TrnStickerIssue.hospital_sticker_no';

        } else if($this->request->data['TrnStickerIssue']['print_type'] == 2) {
            $this->set('sortkey' , 'センター棚番号,商品ID,病院情報');

            $order = 'MstShelfNameTo.code asc, MstFacility.facility_name asc,MstDepartment.department_name asc, TrnFixedPromise.promise_type asc , TrnStickerIssue.hospital_sticker_no';
        }

        $data = $this->Stickers->getHospitalStickers($_hno, $order);

        //$this->set('sortkey' , '病院情報,センター棚番号,商品ID');
        $this->set('Sticker' , $data);
        $this->layout = 'xml/default';
        $this->render('/stickerissues/hospital_sticker');
    }

    //編集画面用施設プルダウン作成用データ
    function _setFacilityEnabled () {
        $facilityId = $this->Session->read('Auth.facility_id_selected');

        $sql  = 'SELECT C.facility_code ';
        $sql .= '      ,C.facility_name ';
        $sql .= '      ,C.id ';
        $sql .= '  FROM mst_facility_relations B ';
        $sql .= '        INNER JOIN mst_facilities C ';
        $sql .= '                ON B.partner_facility_id = C.id ';
        $sql .= '        inner join mst_user_belongings as d ';
        $sql .= '                on d.mst_facility_id = C.id ';
        $sql .= '                and d.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '                and d.is_deleted = false ';
        $sql .= ' WHERE B.mst_facility_id = '.$facilityId;
        $sql .= '               AND C.facility_type = '.Configure::read('FacilityType.hospital');
        $sql .= '               AND B.is_deleted != TRUE'; 
        $sql .= ' ORDER BY C.facility_code  ';
        $_facility_enabled = $this->MstUser->query($sql);



        $_facility_enabled_out  = array();
        $_facility_enabled_id  = array();
        $_facility_enabled_code = array();

        for ($_i = 0;$_i < count($_facility_enabled);$_i++){
            $_facility_enabled_out[$_facility_enabled[$_i][0]['id']] = $_facility_enabled[$_i][0]['facility_name'];
            $_facility_enabled_id[$_facility_enabled[$_i][0]['id']] = $_facility_enabled[$_i][0]['facility_code'];
            $_facility_enabled_code[$_facility_enabled[$_i][0]['facility_code']] = $_facility_enabled[$_i][0]['id'];
        }
        $facility_enabled = array($_facility_enabled_out,$_facility_enabled_id,$_facility_enabled_code);
        return $facility_enabled;
    }

    private function getSearchGroupParamater($_search_option = array(),$_search_order = array()){
        /* 表示カラム */
        $options['fields'] = array( 'MstFacility.id'
                                    , 'MstFacility.facility_name'
                                    , 'MstFacility.facility_code'
                                    , 'TrnFixedPromise.department_id_from'
                                    , 'MstDepartment_from.department_name'
                                    , 'TrnFixedPromise.work_date'
                                    , 'sum(TrnFixedPromise.remain_count) as sum_remains'
                                    );

        /* JOINテーブル */
        //包装単位
        $options['joins'][] = array('type'=>'INNER', 'table'=>'mst_item_units'    ,'alias'=>'MstItemUnit', 'conditions'=>'TrnFixedPromise.mst_item_unit_id = MstItemUnit.id');
        //施設採用品
        $options['joins'][] = array('type'=>'INNER', 'table'=>'mst_facility_items','alias'=>'MstFacilityItem', 'conditions'=> array('MstItemUnit.mst_facility_item_id = MstFacilityItem.id' , 'MstFacilityItem.is_lowlevel = False '));
        //引当要求元部署（病院）
        $options['joins'][] = array('type'=>'INNER', 'table'=>'mst_departments'   ,'alias'=>'MstDepartment_from', 'conditions'=>'TrnFixedPromise.department_id_from = MstDepartment_from.id');
        //施設
        $options['joins'][] = array('type'=>'INNER', 'table'=>'mst_facilities'    ,'alias'=>'MstFacility', 'conditions'=>'MstDepartment_from.mst_facility_id = MstFacility.id');
        //取引先マスタ
        $options['joins'][] = array('type'=>'LEFT', 'table'=>'mst_dealers'  ,'alias'=>'MstDealer',  'conditions'=>'MstFacilityItem.mst_dealer_id = MstDealer.id');
        //定数設定
        $options['joins'][] = array('type'=>'LEFT', 'table'=>'mst_fixed_counts'  ,'alias'=>'MstFixedCount', 'conditions'=> array('MstFixedCount.mst_department_id = MstDepartment_from.id' , 'MstFixedCount.mst_item_unit_id=MstItemUnit.id'));

        //ユーザ権限
        $options['joins'][] = array('type'=>'INNER', 'table'=>'mst_facility_relations'  ,'alias'=>'MstFacilityRelation', 'conditions'=> array('MstFacilityRelation.mst_facility_id'=>$this->Session->read('Auth.facility_id_selected') , 'MstFacilityRelation.partner_facility_id=MstFacility.id'  , 'MstFacilityRelation.is_deleted=false'));
        //操作権限
        $options['joins'][] = array('type'=>'INNER', 'table'=>'mst_user_belongings'  ,'alias'=>'MstUserBelonging', 'conditions'=> array('MstUserBelonging.mst_facility_id = MstFacility.id' , 'MstUserBelonging.mst_user_id'=>$this->Session->read('Auth.MstUser.id')  , 'MstUserBelonging.is_deleted=false'));
        //要求
        $options['joins'][] = array('type'=>'LEFT', 'table'=>'trn_promises'  ,'alias'=>'TrnPromise', 'conditions'=> array('TrnPromise.id = TrnFixedPromise.trn_promise_id'));
        /* WHERE句 */
        //TODO タイプとステータスは後で変更
        $options["conditions"] = array("TrnFixedPromise.remain_count <>" => "0"
                                       ,"TrnFixedPromise.is_deleted" => "false"
                                       );
        /* ユーザー設定サーチオプション追加 */
        if(isset($_search_option["conditions"])){
            array_push($options["conditions"], $_search_option["conditions"]);
        }

        /* GROUP句 */
        $options["group"] = array('TrnFixedPromise.work_date','MstFacility.id','MstFacility.facility_name','MstFacility.facility_code','MstDepartment_from.department_code','TrnFixedPromise.department_id_from','MstDepartment_from.department_name');
        /* order */
        $options["order"] = (is_null($this->getSortOrder())?'MstFacility.facility_code , MstDepartment_from.department_code , TrnFixedPromise.work_date':$this->getSortOrder());
        /* LIMIT */
        $this->request->data['limit'] = $this->_getLimitCount();
        $options["limit"] = isset($this->request->data['limit']) ? (int)$this->request->data['limit'] : min(array_keys(Configure::read('displaycounts_combobox')));
        /* recursive */
        $options["recursive"] = -1;
        $SearchResult = $this->TrnFixedPromise->find('all', $options);
        return $SearchResult;
    }

    private function getSearchDetailParamater($_search_option = array(),$_search_order = array(),$editflag = FALSE){
        /* 部署シール用明細取得 */
        $options['fields'] = array( 'TrnFixedPromise.id'
                                    , 'TrnFixedPromise.department_id_to'
                                    , 'TrnFixedPromise.department_id_from'
                                    , 'MstFacility.id'
                                    , 'MstFacility.facility_code'
                                    , 'MstFacility.facility_formal_name'
                                    , 'MstItemUnit.id'
                                    , 'MstFacilityItem.internal_code'
                                    , 'MstFacilityItem.price'
                                    , 'MstFacilityItem.id'
                                    , 'MstFacilityItem.item_name'
                                    , 'MstFacilityItem.standard'
                                    , 'MstFacilityItem.item_code'
                                    , 'MstFacilityItem.insurance_claim_code'
                                    , 'MstFacilityItem.biogenous_type'
                                    , 'MstUnitName.unit_name'
                                    , 'MstFacility.facility_name'
                                    , 'MstDepartment_from.department_name'
                                    , 'MstShelfName_from.code'
                                    , 'MstShelfName_to.code'
                                    , 'MstDealer.dealer_name'
                                    , 'TrnFixedPromise.id'
                                    , 'TrnFixedPromise.fixed_promise_status'
                                    , 'TrnFixedPromise.promise_type'
                                    , 'TrnFixedPromise.trn_fixed_promise_header_id'
                                    , 'TrnFixedPromise.work_date'
                                    , 'TrnFixedPromise.remain_count'
                                    , 'TrnPromise.fixed_count'
                                    , 'TrnPromise.fixed_num_used'
                                    );
        /* JOINテーブル */
        //包装単位
        $options['joins'][] = array('type'=>'INNER', 'table'=>'mst_item_units'    ,'alias'=>'MstItemUnit', 'conditions'=>'TrnFixedPromise.mst_item_unit_id = MstItemUnit.id');
        //包装単位名称
        $options['joins'][] = array('type'=>'INNER', 'table'=>'mst_unit_names'    ,'alias'=>'MstUnitName', 'conditions'=>'MstItemUnit.mst_unit_name_id = MstUnitName.id');
        //施設採用品
        $options['joins'][] = array('type'=>'INNER', 'table'=>'mst_facility_items','alias'=>'MstFacilityItem', 'conditions'=> array('MstItemUnit.mst_facility_item_id = MstFacilityItem.id' , 'MstFacilityItem.is_lowlevel = false '));
        //病院部署（引当要求元）
        $options['joins'][] = array('type'=>'INNER', 'table'=>'mst_departments'   ,'alias'=>'MstDepartment_from', 'conditions'=>'TrnFixedPromise.department_id_from = MstDepartment_from.id');
        //施設
        $options['joins'][] = array('type'=>'INNER', 'table'=>'mst_facilities'    ,'alias'=>'MstFacility', 'conditions'=>'MstDepartment_from.mst_facility_id = MstFacility.id');
        //取引先マスタ
        $options['joins'][] = array('type'=>'LEFT', 'table'=>'mst_dealers'       ,'alias'=>'MstDealer', 'conditions'=>'MstFacilityItem.mst_dealer_id = MstDealer.id');
        //引当
        $options['joins'][] = array('type'=>'INNER', 'table'=>'trn_promises'       ,'alias'=>'TrnPromise', 'conditions'=>'TrnFixedPromise.trn_promise_id = TrnPromise.id');
        //定数設定
        $options['joins'][] = array('type'=>'LEFT', 'table'=>'mst_fixed_counts'  ,'alias'=>'MstFixedCount', 'conditions'=> array('MstFixedCount.mst_department_id = MstDepartment_from.id' , 'MstFixedCount.mst_item_unit_id=MstItemUnit.id'));

        $options['joins'][] = array('type' => 'left','table' => 'mst_fixed_counts', 'alias' => 'mst_fixed_center_from', 'conditions' => 'TrnFixedPromise.department_id_from = mst_fixed_center_from.mst_department_id and mst_fixed_center_from.mst_item_unit_id = MstItemUnit.id');
        $options['joins'][] = array('type' => 'left','table' => 'mst_fixed_counts', 'alias' => 'mst_fixed_center_to', 'conditions' => 'TrnFixedPromise.department_id_to = mst_fixed_center_to.mst_department_id and mst_fixed_center_to.mst_item_unit_id = MstItemUnit.id');

        $options['joins'][] = array('type'=>'left', 'table' => 'mst_shelf_names', 'alias'=>'MstShelfName_from', 'conditions'=>'mst_fixed_center_from.mst_shelf_name_id = MstShelfName_from.id');
        $options['joins'][] = array('type'=>'left', 'table' => 'mst_shelf_names', 'alias'=>'MstShelfName_to', 'conditions'=>'mst_fixed_center_to.mst_shelf_name_id = MstShelfName_to.id');

        /* WHERE句 */
        //TODO タイプとステータスは後で変更
        //TODO 残数0の引当確定データを検索する可能性があるため(履歴画面：部署シール再発行)
        if(!$editflag){
            $options["conditions"] = array("TrnFixedPromise.remain_count <>" => "0",
                                           "TrnFixedPromise.is_deleted" => "false",);
        }else{
            $options["conditions"] = array("TrnFixedPromise.is_deleted" => "false");
        }

        /* ユーザー設定サーチオプション追加 */
        array_push($options["conditions"], $_search_option["conditions"]);

        /* order */
        if(!$this->isNNs($_search_order)){
            $options["order"] = $_search_order["order"];
        }
        /* LIMIT */
        $options["recursive"] = -1;

        $SearchResult = $this->TrnFixedPromise->find('all', $options);

        return $SearchResult;
    }

    /**
     * Use this with php function: array_walk_recursive
     * @param mixed $item
     * @param string $key
     * @param array $res
     * @access public
     * @static
     */
    public static function BuildSearchResult($item, $key, $res){
        //HACK:  最上位の配列値を優先するようにしてます・・
        if(false === isset($res[$key])){
            $res[$key] = $item;
        }
    }

    /**
     * 作業区分を取得する
     */
    private function __getWorkClasses() {
        $work_classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'), self::WORK_CLASSES_STICKER);

        $this->set('work_classes',$work_classes);

        if (!$this->Session->check('TrnStickerIssue.work_classes')) {
            $this->Session->write('TrnStickerIssue.work_classes',$work_classes);
        }
    }

    /**
     * オブジェクト比較：シール発行前に最新オブジェクトと比較
     * @param
     * @todo
     */
    private function hasRemainsDifference(array $nowResult,array $preResult){
        if(count($nowResult) !== count($preResult)){
            $this->Session->setFlash('引当確定情報が更新されています。再度検索を行ってください');
            return true;
        }else{
            for($i = 0; $i < count($nowResult); $i++){
                // TODO FIXME sum_remainsプロパティが無いため、以下の条件分岐でエラー発生。
                // sum_remainsは常にあるのか？無い場合は、その処理を記述すること。以下応急処置。
                $_now = (isset($nowResult[$i]['sum_remains']) && $nowResult[$i]['sum_remains'] != '') ? $nowResult[$i]['sum_remains']:0;
                $_pre = (isset($preResult[$i]['sum_remains']) && $preResult[$i]['sum_remains'] != '') ? $preResult[$i]['sum_remains']:0;
                //if($nowResult[$i]['sum_remains'] !== $preResult[$i]['sum_remains']){
                if($_now !== $_pre || $_now == 0 ){
                    $this->Session->setFlash('引当確定情報が更新されています。再度検索を行ってください', 'growl', array('type'=>'error'));
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * sales_print 部署シール  印刷
     * @param
     * @return
     */
    function print_hospital_sticker() {
        App::import('Sanitize');

        $o = $this->request->data['Sticker']['StickerOrder'];
        if($o == '病院部署名,病院棚番号'){
            $order = 'MstDepartment.department_name asc, MstShelfNameTo.code asc,TrnFixedPromise.promise_type asc , TrnStickerIssue.hospital_sticker_no';
            $o = "病院情報,センター棚番号,商品ID";
        }else if($o == '病院棚番号,病院部署名'){
            $order = 'MstShelfNameTo.code asc,MstDepartment.department_name asc,TrnFixedPromise.promise_type asc , TrnStickerIssue.hospital_sticker_no';
            $o = "センター棚番号,病院情報,商品ID";
        }else if($o == '病院棚番号,商品ID,病院部署名'){
            $order = 'MstShelfNameTo.code asc,MstFacility.facility_code asc,MstDepartment.department_name asc,TrnFixedPromise.promise_type asc , TrnStickerIssue.hospital_sticker_no';
            $o = "センター棚番号,商品ID,病院情報";
        }

        $_hno = array();
        $_hno = $this->request->data['Sticker']['StickerNo'];
        $data = $this->Stickers->getHospitalStickers($_hno, $order);

        $this->set('sortkey' , $o);
        $this->set('Sticker' , $data);
        $this->layout = 'xml/default';
        $this->render('/stickerissues/hospital_sticker');

    }

    function print_cost_sticker() {
        App::import('Sanitize');
        // コストシールデータ取得
        $records = $this->Stickers->getCostStickers($this->request->data['TrnStickerIssue']['id'] , 2 );
        // 空なら初期化
        if(!$records){ $records = array();}

        $this->layout = 'xml/default';
        $this->set('records', $records);
        $this->render('/stickerissues/cost_sticker');
    }

    /**
     * 医事シール発行
     * ※部署シール発行画面に後付でつけたもの＠履歴とは動きが異なります！
     */
    public function printCostStickers(){
        App::import('Sanitize');
        $sql = "select id from trn_sticker_issues where hospital_sticker_no in ('" . join('\',\'',$this->request->data['Sticker']['StickerNo']) ."')";
        $ret = $this->TrnStickerIssue->query($sql);
        foreach($ret as $r){
            $ids[] = $r[0]['id'];
        }
        // コストシールデータ取得
        $records = $this->Stickers->getCostStickers($ids , 2 );
        // 空なら初期化
        if(!$records){ $records = array();}

        $this->layout = 'xml/default';
        $this->set('records', $records);
        $this->render('/stickerissues/cost_sticker');
    }

    private function _historySQL($data , $limit , $detail = null , $count=false) {
        $this->request->data = $data;

        App::import('Sanitize');
        // 検索処理
        $where = " 1 = 1 and ( a1.trade_type != 3 or a1.trade_type is null ) ";

        //ヘッダID
        if( !empty($this->request->data['TrnStickerIssueHeader']['id']) && $detail ){
            $where .=' and a.trn_sticker_issue_header_id in ( ' . implode(',' , $this->request->data['TrnStickerIssueHeader']['id']) . ')';
        }
        //明細ID
        if( (!empty($this->request->data['TrnStickerIssue']['id']) ) ){
            $where .=' and a.id in ( ' . implode(',' , $this->request->data['TrnStickerIssue']['id']) . ')';
        }
        //発行番号
        if( (!empty($this->request->data['TrnStickerIssueHeader']['work_no']) ) ){
            $where .=" and a.work_no LIKE '%".Sanitize::escape($this->request->data['TrnStickerIssueHeader']['work_no'])."%'";
        }
        //要求日FROM
        if( (!empty($this->request->data['TrnStickerIssueHeader']['work_date_start']) ) ){
            $where .= " and a.work_date >= '" . $this->request->data['TrnStickerIssueHeader']['work_date_start'] ."'";
        }
        //要求日TO
        if( (!empty($this->request->data['TrnStickerIssueHeader']['work_date_end']) ) ){
            $where .= " and a.work_date <= '" . $this->request->data['TrnStickerIssueHeader']['work_date_end'] ."'";
        }
        //未出荷を表示
        if(isset($this->request->data['TrnStickerIssueHeader']['no_shipping']) && $this->request->data['TrnStickerIssueHeader']['no_shipping'] == "1"){
            $where .= ' and a.quantity >= 1 ';
        }
        //取消も表示
        if(isset($this->request->data['TrnStickerIssueHeader']['delete_opt']) && $this->request->data['TrnStickerIssueHeader']['delete_opt'] == "0"){
            $where .= ' and a.is_deleted = false ';
            $where .= ' and o.is_deleted = false ';
        }
        //商品ID
        if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
            $where .= " and e.internal_code = '" . Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']) . "'";
        }
        //製品番号
        if(!empty($this->request->data['MstFacilityItem']['item_code'])){
            $where .= " and e.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        //規格
        if(!empty($this->request->data['MstFacilityItem']['standard'])){
            $where .= " and e.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }

        //発行施設
        if(!empty($this->request->data['TrnStickerIssueHeader']['facilityCode'])){
            $where .= " and k.facility_code = '". $this->request->data['TrnStickerIssueHeader']['facilityCode'] ."'";
            //発行部署
            if(!empty($this->request->data['TrnStickerIssueHeader']['departmentCode'])){
                $where .= " and j.department_code = '" . $this->request->data['TrnStickerIssueHeader']['departmentCode']."'";
                $where .= ' and j.mst_facility_id = k.id';
            }

        }
        //管理区分
        if(!empty($this->request->data['TrnFixedPromise']['promise_type'])){
            $where .= ' and a.promise_type = ' . $this->request->data['TrnFixedPromise']['promise_type'];
        }
        //商品名
        if(!empty($this->request->data['MstFacilityItem']['item_name'])){
            $where .= " and e.item_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%'";
        }
        //販売元
        if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
            $where .= " and i.dealer_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name']) . "%'";
        }
        //作業区分
        if(!empty($this->request->data['TrnStickerIssueHeader']['work_class'])) {
            $where .= ' and l.id = ' . $this->request->data['TrnStickerIssueHeader']['work_class'];
        }
        //シール番号
        if(!empty($this->request->data['TrnStickerIssue']['hospital_sticker_no']) && !is_array($this->request->data['TrnStickerIssue']['hospital_sticker_no'])) {
            $where .= " and a.hospital_sticker_no  = '" . $this->request->data['TrnStickerIssue']['hospital_sticker_no'] . "'";
        }
        //定数区分
        if(!empty($this->request->data['MstFixedCount']['trade_type'])) {
            $where .= ' and a1.trade_type = ' . $this->request->data['MstFixedCount']['trade_type'];
        }

        if(!empty($limit) && !$detail){
            $limit = " limit " . $limit;
        }else{
            $limit = "";
        }

        $sql  = 'select ';
        $sql .= '      a.work_no         as "TrnStickerIssue__work_no"';
        $sql .= "    , to_char(a.work_date , 'YYYY/mm/dd') ";
        $sql .= '                        as "TrnStickerIssue__work_date"';
        $sql .= '    , k.facility_name   as "TrnStickerIssue__facility_name"';
        $sql .= '    , j.department_name as "TrnStickerIssue__department_name"';

        if(!$detail){
            $sql .= '    , o.id             as "TrnStickerIssueHeader__id"';
            $sql .= '    , o.recital        as "TrnStickerIssueHeader__recital"';
            $sql .= '    , p.user_name      as "TrnStickerIssue__user_name"';
            $sql .= "    , to_char(o.created,'YYYY/MM/DD hh24:mi:ss') ";
            $sql .= '                       as "TrnStickerIssueHeader__created"';
            $sql .= '    , o.detail_count   as "TrnStickerIssueHeader__detail_count"';
            $sql .= '    , count(o.work_no) as "TrnStickerIssueHeader__count"';
        }else{
            $sql .= '    , a.id            as "TrnStickerIssue__id"';
            $sql .= '    , a.recital       as "TrnStickerIssue__recital"';
            $sql .= '    , e.item_name     as "TrnStickerIssue__item_name"';
            $sql .= '    , e.item_code     as "TrnStickerIssue__item_code"';
            $sql .= '    , e.standard      as "TrnStickerIssue__standard"';
            $sql .= '    , e.internal_code as "TrnStickerIssue__internal_code"';
            $sql .= '    , i.dealer_name   as "TrnStickerIssue__dealer_name"';
            $sql .= '    , a.hospital_sticker_no as "TrnStickerIssue__hospital_sticker_no"';
            $sql .= '    , ( case';
            $sql .= "          when a.is_deleted = true then a.recital || '＜取消済＞' ";
            $sql .= "          when a.quantity = 0 then a.recital || '＜出荷済＞' ";
            $sql .= "          when c.shipping_type = " . Configure::read('ShippingType.deposit') . " then a.recital || '＜預託品＞' ";
            $sql .= '          else a.recital ';
            $sql .= '      end ) as "TrnStickerIssue__recital" ';
            $sql .= '    , (case ';
            $sql .= '          when d.per_unit = 1  then m.unit_name ';
            $sql .= "          else m.unit_name || '(' || d.per_unit || f.unit_name || ')'";
            $sql .= '      end ) as "TrnStickerIssue__unit_name"';
            $sql .= '    , b.lot_no         as "TrnStickerIssue__lot_no"';
            $sql .= "    , to_char(b.validated_date , 'YYYY/mm/dd') ";
            $sql .= '                       as "TrnStickerIssue__validated_date"';
            $sql .= '    , h.user_name      as "TrnStickerIssue__user_name"';
            $sql .= '    , n.medical_code   as "TrnStickerIssue__medical_code"';
            $sql .= '    , l.name           as "TrnStickerIssue__work_type"';
            $sql .= '    , ( case';
            $sql .= '          when a.is_deleted = true then 1 ';
            $sql .= '          when a.quantity = 0 then 1 ';
            $sql .= '          when c.shipping_type = ' . Configure::read('ShippingType.deposit') . ' then 1 ';
            $sql .= '          else 0 ';
            $sql .= '      end ) as "TrnStickerIssue__checkFlg" ';
            $sql .= '    , ( case ';
            $sql .= "          when a3.promise_type = " . Configure::read('PromiseType.Constant') . " then '定数' ";
            $sql .= "          when a3.promise_type = " . Configure::read('PromiseType.Temporary') . " then '臨時' ";
            $sql .= "          else '' ";
            $sql .= '      end )            as "TrnStickerIssue__promise_type" ';
        }
        $sql .= '  from ';
        $sql .= '    trn_sticker_issues as a  ';
        $sql .= '    left join mst_fixed_counts as a1 ';
        $sql .= '      on a1.mst_department_id = a.department_id_to ';
        $sql .= '      and a1.mst_item_unit_id = a.mst_item_unit_id ';

        $sql .= '    left join trn_fixed_promises as a2 ';
        $sql .= '      on a.trn_fixed_promise_id = a2.id ';
        $sql .= '    left join trn_promises as a3 ';
        $sql .= '      on a2.trn_promise_id = a3.id ';

        $sql .= '    left join trn_stickers as b  ';
        $sql .= '      on a.hospital_sticker_no = b.hospital_sticker_no  ';
        $sql .= '      and b.is_deleted = false  ';
        $sql .= '    left join trn_shippings as c  ';
        $sql .= '      on b.trn_shipping_id = c.id  ';
        $sql .= '    left join mst_item_units as d  ';
        $sql .= '      on d.id = a.mst_item_unit_id  ';
        $sql .= '    left join mst_facility_items as e  ';
        $sql .= '      on e.id = d.mst_facility_item_id  ';
        $sql .= '    left join mst_unit_names as f  ';
        $sql .= '      on f.id = d.per_unit_name_id  ';
        $sql .= '    left join mst_users as h  ';
        $sql .= '      on h.id = a.modifier  ';
        $sql .= '    left join mst_dealers as i  ';
        $sql .= '      on i.id = e.mst_dealer_id  ';
        $sql .= '    left join mst_departments as j  ';
        $sql .= '      on j.id = a.department_id_to  ';
        $sql .= '    left join mst_facilities as k  ';
        $sql .= '      on k.id = j.mst_facility_id  ';
        $sql .= '    left join mst_classes as l  ';
        $sql .= '      on l.id = a.work_type  ';
        $sql .= '    left join mst_unit_names as m  ';
        $sql .= '      on m.id = d.mst_unit_name_id  ';
        $sql .= '    left join mst_medical_codes as n  ';
        $sql .= '      on n.mst_facility_item_id = e.id ';
        $sql .= '      and n.mst_facility_id = k.id  ';
        $sql .= '    left join trn_sticker_issue_headers as o  ';
        $sql .= '      on o.id = a.trn_sticker_issue_header_id  ';
        $sql .= '    left join mst_users as p  ';
        $sql .= '      on o.modifier = p.id  ';
        $sql .= '    inner join mst_facility_relations as q  ';
        $sql .= '      on q.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '      and q.partner_facility_id = k.id  ';
        $sql .= '      and k.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '    inner join mst_user_belongings as r  ';
        $sql .= '      on r.mst_facility_id = k.id ';
        $sql .= '      and r.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '      and r.is_deleted = false';

        $sql .= '  where ';
        $sql .= $where ;

        if(!$detail){
            $sql .= ' group by ';
            $sql .= '      a.work_no ';
            $sql .= '    , a.work_date ';
            $sql .= '    , k.facility_name ';
            $sql .= '    , j.department_name ';
            $sql .= '    , p.user_name ';
            $sql .= '    , o.id ';
            $sql .= '    , o.recital ';
            $sql .= '    , o.created  ';
            $sql .= '    , o.creater  ';
            $sql .= '    , o.detail_count ';
        }

        $order = $this->getSortOrder();
        if(isset($order[0])){
            $tmp = preg_split ( "/[\s\.]+/" , $order[0] );
            $sql.=
              '  order by "'. $tmp[0] . '"."'.$tmp[1].'" ' . $tmp[2] . ' ';
        }else{
            $sql .= '  order by a.work_no desc ';
        }

        if($detail){
            $sql .=', a.hospital_sticker_no ';
        }
        if($count){
            $this->set('max' , $this->getMaxCount($sql , 'TrnStickerIssue'));
            return;
        }
        if(!$detail){
            $sql .=  $limit;
        }
        return $sql ;
    }

    /**
     * 部署シール明細の合計とis_deletedの合計が等しいかチェックする
     * @param integer $trn_sticker_issue_id
     * @return boolean or trn_sticker_issue_header_id
     */
    public function chkStickerIssueDeleteAll($trn_sticker_issue_id = null) {
        $_params = array();
        $_chk = array();
        $_params['fields'] = array('TrnStickerIssue.trn_sticker_issue_header_id');
        $_params['conditions']['TrnStickerIssue.id'] = $trn_sticker_issue_id;
        $_params['recursive'] = -1;
        $_res = $this->TrnStickerIssue->find('first', $_params);
        
        $_sql1 = 'select count(trn_sticker_issue_header_id) as detail_count from trn_sticker_issues where trn_sticker_issue_header_id = '.$_res['TrnStickerIssue']['trn_sticker_issue_header_id'];
        $_sql2 = ' select count(is_deleted) as delete_sum from trn_sticker_issues where is_deleted = true and trn_sticker_issue_header_id = '.$_res['TrnStickerIssue']['trn_sticker_issue_header_id'];
        
        $_chk1 = $this->TrnStickerIssue->query($_sql1);
        $_chk2 = $this->TrnStickerIssue->query($_sql2);
        
        $_detail_count = $_chk1[0][0]['detail_count'];
        $_delete_sum = $_chk2[0][0]['delete_sum'];
        
        if((int)$_detail_count === (int)$_delete_sum){
            return $_res['TrnStickerIssue']['trn_sticker_issue_header_id'];
        }else{
            return false;
        }
    }
    
}
