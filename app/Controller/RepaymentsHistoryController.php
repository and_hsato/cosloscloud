<?php
/**
 * MstRepaymentsHistoryController
 *
 * @version 1.0.0
 * @since 2010/09/10
 */

class RepaymentsHistoryController extends AppController {
  var $name = 'RepaymentsHistory';

  /**
   *
   * @var array $uses
   */
  var $uses = array('MstClass',                       //作業区分
                    'MstMenu',                        //
                    'MstFacilityItem',                //施設採用品
                    'MstFacilityRelation',            //施設関係
                    'MstFacility',                    //施設
                    'MstDepartment',                  //部署
                    'MstDealer',                      //販売元
                    'TrnCloseHeader',                 //締め
                    'TrnClaim',                       //請求テーブル（売上データ）
                    'TrnConsume',                     //消費明細
                    'TrnSticker',                     //シール
                    'MstItemUnit',                    //包装単位マスタ
                    'TrnReceiving',                   //入荷
                    'TrnShipping',                    //出荷
                    'MstUnitName',                    //単位名
                    'TrnStock',                       //在庫
                    'TrnRepayHeader',                 //返却ヘッダ
                    'TrnReceivingHeader',             //入荷ヘッダ
                    'TrnReceiving',                   //入荷明細
                    'TrnMoveHeader',                  //移動ヘッダ
                    'TrnStickerRecord',               //シール移動履歴
                    'MstUser',                        //ユーザマスタ
                    'MstTransactionConfig',           //仕入設定
                    'TrnOrder',                       //発注
                    );

    /**
     * @var array $components
    */
    var $components = array('CsvWriteUtils');


    /**
     * @var AuthComponent
     */
    var $Auth;

    //ログインなしでの印刷を許可
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    /**
     * @var SessionComponent
     * TODO SQLの共通化
     */
    var $Session;
    function index(){
        $this->setRoleFunction(29); //返却履歴
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        $ReturnList = array();
        $d_where = array();
        $sticker_where = array();
        $department_List = array();
        $this->set('facility_List', $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                               array(Configure::read('FacilityType.hospital')),
                                                               false
                                                               )
                   );

        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 12 ));//作業区分プルダウン用データ取得

        App::import('Sanitize');
        $where = "";

        //初回時は検索を行わない
        if(isset($this->request->data['search']['is_search'])){
            //where句作成
            $where = $this->createSearch_Where($this->Session->read('Auth.facility_id_selected'));

            //帳票印刷とSQLを共用しているため、施設・部署はここで追加
            //施設(完全一致)---------------------------------------------------
            if((isset($this->request->data['MstRepayment']['facilityCode'])) && ($this->request->data['MstRepayment']['facilityCode'] != "")){
                $where .= ' and MstFacility.facility_code = ' ."'".Sanitize::escape($this->request->data['MstRepayment']['facilityCode'])."' ";
                $department_List = $this->getDepartmentsList($this->request->data['MstRepayment']['facilityCode'] , false );
            }

            //部署(完全一致)---------------------------------------------------
            if((isset($this->request->data['MstRepayment']['departmentCode'])) && ($this->request->data['MstRepayment']['departmentCode'] != "")){
                $where .= ' and TrnRepayHeader.department_id_from = ' ."'".Sanitize::escape($this->request->data['MstRepayment']['departmentCode'])."' ";
            }

            $sql  = ' select ';
            $sql .=     ' TrnRepayHeader.id           as "TrnRepayHeader__id"';
            $sql .=     ',TrnRepayHeader.work_no      as "TrnRepayHeader__work_no"';
            $sql .=     ",to_char(TrnRepayHeader.work_date,'YYYY/mm/dd') ";
            $sql .=     '                             as "TrnRepayHeader__work_date"';
            $sql .=     ',TrnRepayHeader.repay_type   as "TrnRepayHeader__repay_type"';
            $sql .=     ',TrnRepayHeader.is_deleted   as "TrnRepayHeader__is_deleted"';
            $sql .=     ',MstFacility.id              as "MstFacility__id"';
            $sql .=     ',MstFacility.facility_name   as "MstFacility__facility_name"';
            $sql .=     ',MstUser.user_name           as "MstUser__user_name"';
            $sql .=     ",to_char(TrnRepayHeader.created,'YYYY/mm/dd hh24:mi:ss')";
            $sql .=     '                             as "TrnRepayHeader__created"';
            $sql .=     ',TrnRepayHeader.creater      as "TrnRepayHeader__creater"';
            $sql .=     ',TrnRepayHeader.recital      as "TrnRepayHeader__recital"';
            $sql .=     ',TrnRepayHeader.detail_count as "TrnRepayHeader__detail_count"';
            $sql .=     ',count(*)                    as "TrnRepayHeader__total_count"';
            $sql .=  ' from ';
            $sql .=     ' trn_repay_headers as TrnRepayHeader ';
            $sql .=     ' left join trn_receivings as TrnReceiving on TrnReceiving.trn_repay_header_id = TrnRepayHeader.id ';
            $sql .=     ' left join trn_stickers as TrnSticker on TrnReceiving.trn_sticker_id = TrnSticker.id ';
            $sql .=     ' left join mst_item_units as MstItemUnit on TrnReceiving.mst_item_unit_id = MstItemUnit.id ';
            $sql .=     ' left join mst_users as MstUser on TrnRepayHeader.creater = MstUser.id';
            $sql .=     ' left join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ' ;
            $sql .=     ' left join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id ' ;
            $sql .=     ' left join mst_departments as MstDepartment on TrnRepayHeader.department_id_from = MstDepartment.id ' ;
            $sql .=     ' left join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id ';
            $sql .=     ' left join mst_facility_relations as MstFacilityRelation on MstFacilityRelation.partner_facility_id = MstFacility.id and MstFacilityRelation.mst_facility_id = '. $this->Session->read('Auth.facility_id_selected') ;
            $sql .=     ' left join mst_user_belongings as MstUserBelonging on MstUserBelonging.mst_facility_id = MstFacility.id and MstUserBelonging.mst_user_id = '. $this->Session->read('Auth.MstUser.id') . ' and MstUserBelonging.is_deleted = false ' ;
            $sql .= ' where  ' ;
            $sql .=     '  1=1 '.$where ;
            $sql .= ' group by ' ;
            $sql .=     ' TrnRepayHeader.id';
            $sql .=     ',TrnRepayHeader.work_no';
            $sql .=     ',TrnRepayHeader.work_date';
            $sql .=     ',TrnRepayHeader.repay_type';
            $sql .=     ',TrnRepayHeader.is_deleted';
            $sql .=     ',MstFacility.id';
            $sql .=     ',MstFacility.facility_name';
            $sql .=     ',MstUser.user_name';
            $sql .=     ',TrnRepayHeader.created';
            $sql .=     ',TrnRepayHeader.creater';
            $sql .=     ',TrnRepayHeader.recital';
            $sql .=     ',TrnRepayHeader.department_id_from';
            $sql .=     ',TrnRepayHeader.detail_count ';

            //ソート条件の追加
            if($this->getSortOrder() === null){
                $sql .= ' order by TrnRepayHeader.work_no desc';
            }else{
                $arrayOrder = $this->getSortOrder();
                $sql .= ' order by '.$arrayOrder[0];
            }

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnRepayHeader'));

            $sql .= ' limit ' .$this->_getLimitCount();

            //**********************************************************************
            $ReturnList = $this->TrnRepayHeader->query($sql);
            //**********************************************************************

            //検索処理終了-------------------------------------------------------------
        }else{
            //初期設定
            $this->request->data['MstRepayment']['search_start_date']      = date('Y/m/d',strtotime("-7 day"));
            $this->request->data['MstRepayment']['search_end_date']        = date('Y/m/d');
            $this->request->data['MstRepayment']['search_display_deleted'] = false;
        }
        $this->set('department_List' ,$department_List );
        $this->set('ReturnList', $ReturnList);
    }

    /**
     * Conf
     */
    function conf() {
        //更新時間チェック用に画面を開いた時間を保存

        $this->Session->write('Repayment.readTime',date('Y-m-d H:i:s'));

        //パンくずで戻る用にセット
        $this->Session->write('search_ReturnList', $this->Session->read('ReturnList'));

        App::import('Sanitize');

        //SQL用に整形
        foreach($this->request->data['TrnRepay']['TrnRepayNo'] as $repay_header){
            $pre[] = "'".Sanitize::escape($repay_header)."'";
        }
        $search_repay_header = implode(",", $pre);

        //sql句作成
        $sql = $this->create_sql($search_repay_header);
        $ReturnList = $this->TrnRepayHeader->query($sql);

        $this->set('ReturnList', $ReturnList);
    }

    /**
     * create_result_view：返却取消完了画面作成
     */
    function create_result_view($data){

        $repay_header = explode(',',$data['MstRepayment']['repay_header']);
        unset($repay_header[(count($data['MstRepayment']['repay_header'])+1)]);

        $search_repay_header = implode(",", $data['MstRepayment']['MstRepaymentNo']);

        //--------------------------------------------------------------------------
        $sql  = ' select ';
        $sql .= '      TrnRepayHeader.work_no                as "TrnRepayHeader__work_no"';
        $sql .= "     ,to_char(TrnRepayHeader.work_date,'YYYY/mm/dd') ";
        $sql .= '                                            as "TrnRepayHeader__work_date"';
        $sql .= '     ,MstFacilityItem.internal_code         as "MstFacilityItem__internal_code"';
        $sql .= '     ,MstFacilityItem.item_name             as "MstFacilityItem__item_name"';
        $sql .= '     ,MstFacilityItem.standard              as "MstFacilityItem__standard"';
        $sql .= '     ,MstFacilityItem.item_code             as "MstFacilityItem__item_code"';
        $sql .= '     ,(case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name';
        $sql .= "            else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')'";
        $sql .= '       end )                                as "MstItemUnit__unit_name"';
        $sql .= '     ,MstDealer.dealer_name                 as "MstDealer__dealer_name"';
        $sql .= '     ,TrnSticker.hospital_sticker_no        as "TrnSticker__hospital_sticker_no"';
        $sql .= '     ,TrnSticker.facility_sticker_no        as "TrnSticker__facility_sticker_no"';
        $sql .= '     ,TrnSticker.trade_type                 as "TrnSticker__trade_type"';
        $sql .= '     ,TrnSticker.lot_no                     as "TrnSticker__lot_no"';
        $sql .= "     ,to_char(TrnSticker.validated_date,'YYYY/mm/dd') ";
        $sql .= '                                            as "TrnSticker__validated_date"';
        $sql .= '     ,TrnReceiving.quantity                 as "TrnReceiving__quantity"';
        $sql .= '     ,TrnReceiving.recital                  as "TrnReceiving__recital"';
        $sql .= '     ,MstClass.name                         as "TrnReceiving__class_name"';
        $sql .= '     ,OrderFacility.facility_name           as "OrderFacility__facility_name"';
        $sql .= '     ,MstUser.user_name                     as "MstUser__user_name"';

        $sql .= ' from ';
        $sql .= '     trn_repay_headers as TrnRepayHeader ';
        $sql .= '     inner join trn_receivings as TrnReceiving on TrnReceiving.trn_repay_header_id = TrnRepayHeader.id ';
        $sql .= '     inner join trn_stickers as TrnSticker on TrnReceiving.trn_sticker_id = TrnSticker.id ';
        $sql .= '     inner join mst_item_units as MstItemUnit on TrnReceiving.mst_item_unit_id = MstItemUnit.id ';
        $sql .= '     inner join mst_users as MstUser on TrnRepayHeader.creater = MstUser.id';
        $sql .= '     inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ' ;
        $sql .= '     left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id ' ;
        $sql .= '     inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id' ;
        $sql .= '     inner join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id' ;
        $sql .= '     left  join trn_orders as TrnOrder on TrnOrder.id = TrnSticker.trn_order_id ';
        $sql .= '     left  join mst_departments as OrderDepartment on OrderDepartment.id = TrnOrder.department_id_to ';
        $sql .= '     left  join mst_facilities as OrderFacility on OrderFacility.id = OrderDepartment.mst_facility_id ';
        $sql .= '     left  join mst_classes as MstClass on MstClass.id = TrnReceiving.work_type ';
        $sql .= ' where  ' ;

        $sql .= ' TrnReceiving.id in ('.$search_repay_header.') ' ;
        $sql .= ' order by TrnRepayHeader.work_no';
        //------------------------------------------------------------------------------

        //**********************************************************************
        $ReturnList = $this->TrnRepayHeader->query($sql);
        //**********************************************************************3
        $this->set('ReturnList', $ReturnList);

        $this->render('result');
    }

    /**
     * Result
     */
    function result() {
        
        $targetDateAry = array();
        foreach($this->request->data['MstRepayment']['MstRepaymentNo'] as $id){
            
            $params = array();
            $params['conditions'] = array('TrnReceiving.id'=>$id);
            $params['recursive']  = -1;
            $present_c = $this->TrnReceiving->find('first',$params);
            $targetDateAry[] = $present_c['TrnReceiving']['work_date'];
        }
        
        //仮締更新権限チェック
        //[選択した明細を全チェックする]
        $type = Configure::read('ClosePrivileges.update');
        if(false === $this->canUpdateAfterCloseByDateAry($type ,$targetDateAry, $this->Session->read('Auth.facility_id_selected'))){
            $this->redirect('index');
            return;
        }

        //トランザクションの開始
        $this->TrnSticker->begin();
        //選択明細を行ロック
        $this->TrnReceiving->query(' select * from trn_receivings as a where a.id in ( ' . join(',',$this->request->data['MstRepayment']['MstRepaymentNo']) . ') for update ' );

        if(!$this->repayment_history()){
            //ロールバック
            $this->TrnSticker->rollback();
            $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
            $this->redirect('index');
            return;

        } else {
            //コミット
            $this->TrnSticker->commit();

            //完了画面作成処理へ
            $this->create_result_view($this->request->data);
        }
    }

    /**
     * repayment
     *
     * 返却取消に必要な処理呼び出し元
     */
    function repayment_history(){

        $this->request->data['now'] = date('Y/m/d H:i:s');

        //在庫を元に戻す
        if(!$this->updateStock($this->request->data)){
            return false;
        }

        //マイナスの売上データ・仕入データの削除
        if(!$this->delete_MinusClaim($this->request->data)){
            return false;
        }

        //シールを元に戻す（病院番号とかも）

        if(!$this->update_Sticker($this->request->data)){
            return false;
        }

        //返却ヘッダ・返却明細・移動ヘッダ・移動明細・移動受領明細・シール移動履歴×2の削除
        if(!$this->deleteAllRepayData($this->request->data)){
            return false;
        }
        return true;
    }

    /**
     * deleteAllRepayData
     *
     * 返却取消時に必要な処理：諸々データの削除
     */
    function deleteAllRepayData($data){
        $move_key = array_keys(Configure::read('workNoModels'),"TrnMoveHeader");//シールレコードのレコード区分（移動）
        $move_header = array();$repay_header = array();
        //出荷明細から移動ヘッダと返却ヘッダを取得（重複を省く）

        foreach($data['MstRepayment']['MstRepaymentNo'] as $id){
            $Receiving_data = $this->TrnReceiving->find('first',array('fields'=>array('trn_move_header_id','trn_repay_header_id'),'recursive' => -1,'conditions'=> array('id'=>$id)));

            if((!in_array($Receiving_data['TrnReceiving']['trn_move_header_id'],$move_header)) and ($Receiving_data['TrnReceiving']['trn_move_header_id'] != "")){
                $move_header[] = $Receiving_data['TrnReceiving']['trn_move_header_id'];
            }

            if((!in_array($Receiving_data['TrnReceiving']['trn_repay_header_id'],$repay_header)) and ($Receiving_data['TrnReceiving']['trn_repay_header_id'] != "")){
                $repay_header[] = $Receiving_data['TrnReceiving']['trn_repay_header_id'];
            }
        }

        foreach($move_header as $m_id){
            //移動受領のシール履歴削除用に入荷明細IDを取得
            $ReceivingList = $this->TrnReceiving->find('all', array('conditions' => array('TrnReceiving.trn_move_header_id' => $m_id),'fields' => array('TrnReceiving.id'),'recursive' => -1));
            //移動受領明細(=入荷明細)の削除
            $condtion = array( 'TrnReceiving.trn_move_header_id' => $m_id,'TrnReceiving.trn_repay_header_id' => NULL);
            $updatefield = array( 'TrnReceiving.is_deleted' => "'".TRUE."'",
                                  'TrnReceiving.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'",
                                  'TrnReceiving.modified'   => "'".$this->request->data['now']."'",
                                  );
            //TrnReceiving
            $this->TrnReceiving->create();
            $this->TrnSticker->recursive = -1;
            if (!$this->TrnReceiving->updateAll($updatefield,$condtion)) {
                return false;
            }

            unset($updatefield,$condtion);
            //移動明細(=出荷明細)の削除
            $condtion = array( 'TrnShipping.trn_move_header_id' => $m_id);
            $updatefield = array( 'TrnShipping.is_deleted' => "'".TRUE."'",
                                  'TrnShipping.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'",
                                  'TrnShipping.modified'   => "'".$this->request->data['now']."'",
                                  );
            //TrnShipping
            $this->TrnShipping->create();
            $this->TrnShipping->recursive = -1;
            if (!$this->TrnShipping->updateAll($updatefield,$condtion)) {
                return false;
            }


            foreach($ReceivingList as $receiving_id){
                //移動時のシール移動履歴を削除
                $condtion = array( 'TrnStickerRecord.record_id' => $receiving_id['TrnReceiving']['id'],'TrnStickerRecord.record_type' => Configure::read('RecordType.move'));
                $updatefield = array( 'TrnStickerRecord.is_deleted' => "'".TRUE."'",
                                      'TrnStickerRecord.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'",
                                      'TrnStickerRecord.modified'   => "'".$this->request->data['now']."'",
                                      );

                $this->TrnStickerRecord->create();
                $this->TrnStickerRecord->recursive = -1;
                if (!$this->TrnStickerRecord->updateAll($updatefield,$condtion)) {
                    return false;
                }
            }

            //移動ヘッダの削除
            $condtion = array( 'TrnMoveHeader.id ='=> $m_id);
            $updatefield = array( 'TrnMoveHeader.is_deleted' => "'".TRUE."'",
                                  'TrnMoveHeader.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'",
                                  'TrnMoveHeader.modified'   => "'".$this->request->data['now']."'",
                                  );
            //TrnMoveHeader
            $this->TrnMoveHeader->create();
            $this->TrnMoveHeader->recursive = -1;
            if (!$this->TrnMoveHeader->updateAll($updatefield,$condtion)) {
                return false;
            }
        }

        foreach($data['MstRepayment']['MstRepaymentNo'] as $receive_id){

            //返却明細(=入荷明細)の削除＋備考の更新
            $condtion = array( 'TrnReceiving.id' => $receive_id);
            $updatefield = array( 'TrnReceiving.recital'    => "'".trim($data['MstRepayment'][$receive_id]['recital'])."'",
                                  'TrnReceiving.is_deleted' => "'".TRUE."'",
                                  'TrnReceiving.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'",
                                  'TrnReceiving.modified'   => "'".$this->request->data['now']."'",
                                  );
            //TrnReceiving
            $this->TrnReceiving->create();
            $this->TrnReceiving->recursive = -1;
            if (!$this->TrnReceiving->updateAll($updatefield,$condtion)) {
                return false;
            }

            //返却時のシール移動履歴の削除
            $condtion = array( 'TrnStickerRecord.record_id' => $receive_id,
                               'TrnStickerRecord.record_type' => Configure::read('RecordType.repay'));
            $updatefield = array( 'TrnStickerRecord.is_deleted' => "'".TRUE."'",
                                  'TrnStickerRecord.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'",
                                  'TrnStickerRecord.modified'   => "'".$this->request->data['now']."'",
                                  );
            //TrnStickerRecord
            $this->TrnStickerRecord->create();
            $this->TrnStickerRecord->recursive = -1;
            if (!$this->TrnStickerRecord->updateAll($updatefield,$condtion)) {
                return false;
            }
        }

        //返却ヘッダ配下の返却明細（=入荷明細）が全て削除されていた場合のみ
        foreach($repay_header as $r_id){
            $r_meisai = $this->TrnReceiving->find('all',array('fields'=>'id',
                                                              'recursive' => -1,
                                                              'conditions' => array('TrnReceiving.trn_repay_header_id = ' => $r_id,
                                                                                    'TrnReceiving.is_deleted' => FALSE
                                                                                    )));
            if(count($r_meisai)==0){
                //更新時間チェック
                if(!$this->check_RepayHeaderModified($r_id,$this->Session->read('Repayment.readTime'))){
                    $this->Session->setFlash('処理中のデータに更新がかけられました。最初からやり直してください。', 'growl', array('type'=>'error') );
                    $this->TrnRepayHeader->rollback();
                    $this->redirect('index');
                }

                //返却ヘッダの削除
                $condtion = array( 'TrnRepayHeader.id' => $r_id);
                $updatefield = array( 'TrnRepayHeader.is_deleted' => "'".TRUE."'",
                                      'TrnRepayHeader.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'",
                                      'TrnRepayHeader.modified'   => "'".$this->request->data['now']."'",
                                      );
                //TrnRepayHeader
                $this->TrnRepayHeader->create();
                $this->TrnRepayHeader->recursive = -1;
                if (!$this->TrnRepayHeader->updateAll($updatefield,$condtion)) {
                    return false;
                }
            }
        }
        return true;
    }

   function check_RepayHeaderModified($id,$readtime){
      //シールIDから更新時間を取得
      $modified = $this->TrnRepayHeader->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnRepayHeader.id = ' => $id)));
      //受領確認画面に入った後に更新が行われていた場合はエラーとする
      if(strtotime($modified['TrnRepayHeader']['modified']) > strtotime($readtime)){
        return false;
      }
      return true;
    }

    /**
     * update_Sticker
     *
     * 返却取消時に必要な処理：シールを元の状態に戻す
     */
    function update_Sticker($data){

        foreach($data['MstRepayment']['MstRepaymentNo'] as $id){

            //必要データを先に取得
            //返却明細IDより移動元・移動先・数量・元のシール番号・シールIDを取得

            $receiving_info = $this->TrnReceiving->find('first',array('fields'=>array('id',
                                                                                      'trn_sticker_id',
                                                                                      'department_id_from',
                                                                                      'department_id_to',
                                                                                      'hospital_sticker_no',
                                                                                      'quantity',
                                                                                      'mst_item_unit_id'),
                                                                      'recursive' => -1,
                                                                      'conditions'=>array('id'=>$id)));
            //シールテーブルより消費参照キーを取得
            $sticker_info = $this->TrnSticker->find('first',array('fields'=>array('id',
                                                                                  'trade_type',//管理区分
                                                                                  'trn_consume_id',//消費参照キー
                                                                                  'has_reserved'),//予約済みフラグ
                                                                  'recursive' => -1,
                                                                  'conditions'=>array('id'=>$receiving_info['TrnReceiving']['trn_sticker_id'])));

            //更新時間チェック
            if(!$this->check_StickerModified($receiving_info['TrnReceiving']['trn_sticker_id'],$this->Session->read('Repayment.readTime'))){
                $this->Session->setFlash('処理中のデータに更新がかけられました。最初からやり直してください。', 'growl', array('type'=>'error') );
                $this->TrnSticker->rollback();
                $this->redirect('index');
            }

            $condtion = array( 'TrnSticker.id ='=> $receiving_info['TrnReceiving']['trn_sticker_id'] );
            //シールの持ち主を元に戻す
            $updatefield = array('TrnSticker.mst_department_id' => $receiving_info['TrnReceiving']['department_id_from'],
                                 'TrnSticker.modifier'          => "'".$this->Session->read('Auth.MstUser.id')."'",
                                 'TrnSticker.modified'          => "'".$this->request->data['now']."'",
                                 );

            //消費済の場合、数量を0に戻す
            if($sticker_info['TrnSticker']['trn_consume_id'] != ""){
                $where = array('TrnSticker.quantity' => '0');
                $updatefield = array_merge($updatefield,$where);
            }


            //預託品・低レベル品以外の場合はシール番号を元に戻す
            if($sticker_info['TrnSticker']['trade_type'] != "3"){
                if($this->request->data['MstRepayment'][$id]['is_lowlevel'] != TRUE){
                    $where = array('TrnSticker.hospital_sticker_no' => "'".$receiving_info['TrnReceiving']['hospital_sticker_no']."'");
                    $updatefield = array_merge($updatefield,$where);
                }
            }
            //再利用不可だった場合は予約フラグを元に戻す
            if($sticker_info['TrnSticker']['has_reserved'] == TRUE){
                $where = array('TrnSticker.has_reserved' => 'FALSE');
                $updatefield = array_merge($updatefield,$where);
            }

            //TrnSticker
            $this->TrnSticker->create();
            $this->TrnSticker->recursive = -1;
            if (!$this->TrnSticker->updateAll($updatefield,$condtion)) {
                return false;
            }
        }


        //低レベル品の場合はセンターの入荷時のシールの数量を減らしにいく
        if($this->request->data['MstRepayment'][$id]['is_lowlevel']){

            //該当のシールを検索(IDと現在の数量を取得)
            $sql  = ' SELECT "TrnSticker"."id" AS "TrnSticker__id" ';
            $sql .= '        ,"TrnSticker"."quantity" AS "TrnSticker__quantity" ';
            $sql .= ' FROM "trn_stickers" AS "TrnSticker"';
            $sql .= '  WHERE "TrnSticker".mst_item_unit_id = '.$receiving_info['TrnReceiving']['mst_item_unit_id'];
            $sql .= '  AND "TrnSticker".trade_type = '.$sticker_info['TrnSticker']['trade_type'];
            $sql .= '  AND "TrnSticker".mst_department_id = '.$receiving_info['TrnReceiving']['department_id_to'];
            $center_sticker = $this->TrnSticker->query($sql);

            $update_quantity = 0;
            $update_quantity = $center_sticker[0]['TrnSticker']['quantity'] - $receiving_info['TrnReceiving']['quantity'];

            $condtion = array( 'TrnSticker.id ='=> $center_sticker[0]['TrnSticker']['id'] );
            $updatefield = array( 'TrnSticker.quantity'          => $update_quantity,//数量

                                  'TrnSticker.modifier'          => "'".$this->Session->read('Auth.MstUser.id')."'",
                                  'TrnSticker.modified'          => "'".$this->request->data['now']."'",
                                  );

            //TrnSticker
            $this->TrnSticker->create();
            $this->TrnSticker->recursive = -1;
            if (!$this->TrnSticker->updateAll($updatefield,$condtion)) {
                print_r($this->TrnSticker->validationErrors);echo "<br />DB登録失敗！(TrnSticker(センターのシール数量減らし))<br />";
                return false;
            }
        }
        return true;
    }

    function check_StickerModified($sticker_id,$readtime){
        //シールIDから更新時間を取得
        $Sticker_modified = $this->TrnSticker->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnSticker.id = ' => $sticker_id)));
        //受領確認画面に入った後に更新が行われていた場合はエラーとする
        if(strtotime($Sticker_modified['TrnSticker']['modified']) > strtotime($readtime)){
            return false;
        }
        return true;
    }

    /**
     * delete_MinusClaim
     *
     * 返却取消時に必要な処理：マイナスの売上データや仕入データの削除
     */
    function delete_MinusClaim($data){
        foreach($data['MstRepayment']['MstRepaymentNo'] as $id){

            $condtion = array( 'TrnClaim.trn_receiving_id ='=> $id);
            $updatefield = array( 'TrnClaim.is_deleted' => "'".TRUE."'",
                                  'TrnClaim.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'",
                                  'TrnClaim.modified'   => "'".$this->request->data['now']."'",
                                  );
            //TrnClaim
            $this->TrnClaim->create();
            $this->TrnClaim->recursive = -1;
            if (!$this->TrnClaim->updateAll($updatefield,$condtion)) {
                return false;
            }
        }
        return true;
    }

    /**
     * updateStock
     *
     * 返却取消時に必要な処理：在庫の更新処理
     */
    function updateStock($data){
        //返却明細ごとに処理
        foreach($data['MstRepayment']['MstRepaymentNo'] as $receiving_id){
            //必要データを先に取得
            //返却明細IDより移動元・移動先・数量・包装単位・シールIDを取得
            $receiving_info = $this->TrnReceiving->find('first',array('fields'=>array('id',
                                                                                      'trn_sticker_id',
                                                                                      'department_id_from',
                                                                                      'department_id_to',
                                                                                      'mst_item_unit_id',
                                                                                      'quantity',),
                                                                      'recursive' => -1,
                                                                      'conditions'=>array('id'=>$receiving_id)));
            //シールテーブルより管理区分・消費参照キー・再利用不可フラグ
            $sticker_info = $this->TrnSticker->find('first',array('fields'=>array('id',
                                                                                  'trn_consume_id',//消費参照キー
                                                                                  'trade_type',//管理区分
                                                                                  'has_reserved',),
                                                                  'recursive' => -1,
                                                                  'conditions'=>array('id'=>$receiving_info['TrnReceiving']['trn_sticker_id'])));

            //消費済みの場合病院在庫マイナス----------------------------
            if($sticker_info['TrnSticker']['trn_consume_id'] != ""){
                //現在の病院在庫数の取得

                $now_info = $this->TrnStock->find('first',array('fields'=>array('id','stock_count'),'recursive' => -1,
                                                                'conditions'=>array('mst_department_id'=>$receiving_info['TrnReceiving']['department_id_from'],
                                                                                    'mst_item_unit_id'=>$receiving_info['TrnReceiving']['mst_item_unit_id'])));
                //病院在庫が無い場合は、空レコードを作成する。（臨時品の返却取消対応）
                if ($now_info == false) {
                    //※在庫が無い場合はデータを作成する
                    $stock = array(
                        'TrnStock' => array(
                            'mst_item_unit_id'  => $receiving_info['TrnReceiving']['mst_item_unit_id'],
                            'mst_department_id' => $receiving_info['TrnReceiving']['department_id_from'],
                            'stock_count'       => 0,
                            'reserve_count'     => 0,
                            'promise_count'     => 0,
                            'requested_count'   => 0,
                            'receipted_count'   => 0,
                            'is_deleted'        => false,
                            'creater'           => $this->Session->read('Auth.MstUser.id'),
                            'created'           => $this->request->data['now'],
                            'modifier'          => $this->Session->read('Auth.MstUser.id'),
                            'modified'          => $this->request->data['now'],
                        )
                    );

                    $this->TrnStock->create();
                    $res = $this->TrnStock->save($stock);
                    if(empty($res)){
                        return false;
                    }
                    $last_id = $this->TrnStock->getLastInsertID();

                } else {
                    $last_id = $now_info['TrnStock']['id'];
                }

                //病院在庫マイナス
                $update_quantity = $now_info['TrnStock']['stock_count']-$receiving_info['TrnReceiving']['quantity'];
                $condtion = array( 'TrnStock.id ='=> $last_id );
                $updatefield = array( 'TrnStock.stock_count' => $update_quantity,//数量
                                      'TrnStock.modifier'    => "'".$this->Session->read('Auth.MstUser.id')."'",
                                      'TrnStock.modified'    => "'".$this->request->data['now']."'",
                                      );
                //TrnStock
                $this->TrnStock->create();
                $this->TrnStock->recursive = -1;
                if (!$this->TrnStock->updateAll($updatefield,$condtion)) {
                    return false;
                }
            }
            //(預託品以外の場合)病院在庫プラス・入庫在庫マイナス・入庫予約数マイナス----------
            if($sticker_info['TrnSticker']['trade_type'] != "3"){
                //現在の病院在庫数の取得
                $now_info = $this->TrnStock->find('first',array('fields'=>array('id','stock_count'),'recursive' => -1,
                                                                'conditions'=>array('mst_department_id'=>$receiving_info['TrnReceiving']['department_id_from'],
                                                                                    'mst_item_unit_id'=>$receiving_info['TrnReceiving']['mst_item_unit_id'])));

                //病院在庫プラス
                $update_quantity = $now_info['TrnStock']['stock_count']+$receiving_info['TrnReceiving']['quantity'];
                $condtion = array( 'TrnStock.id ='=> $now_info['TrnStock']['id'] );
                $updatefield = array( 'TrnStock.stock_count' => $update_quantity,//数量
                                      'TrnStock.modifier'    => "'".$this->Session->read('Auth.MstUser.id')."'",
                                      'TrnStock.modified'    => "'".$this->request->data['now']."'",
                                      );
                //TrnStock
                $this->TrnStock->create();
                $this->TrnStock->recursive = -1;
                if (!$this->TrnStock->updateAll($updatefield,$condtion)) {
                    return false;
                }

                //現在の入庫在庫数・予約数の取得
                $now_info = $this->TrnStock->find('first',array('fields'=> array('id','stock_count','reserve_count'),'recursive' => -1,
                                                                'conditions'=> array('mst_department_id'=>$receiving_info['TrnReceiving']['department_id_to'],
                                                                                    'mst_item_unit_id'=>$receiving_info['TrnReceiving']['mst_item_unit_id'])));

                //低レベル品の場合、在庫数が返却取り消しを行う数量分あるか事前に確認
                if($this->request->data['MstRepayment'][$receiving_id]['is_lowlevel'] == '1'){
                    if($now_info['TrnStock']['stock_count'] < $receiving_info['TrnReceiving']['quantity']){
                        //ロールバック
                        $this->TrnRepayHeader->rollback();//返却ヘッダ
                        $this->Session->setFlash('在庫数が足りません。', 'growl', array('type'=>'error') );
                        $this->redirect('index');
                    }
                }
                //入庫在庫マイナス・入庫予約数マイナス
                $update_quantity1 = $now_info['TrnStock']['stock_count']-$receiving_info['TrnReceiving']['quantity'];
                $condtion = array( 'TrnStock.id ='=> $now_info['TrnStock']['id'] );
                $updatefield = array( 'TrnStock.stock_count' => $update_quantity1,//在庫数
                                      'TrnStock.modifier'    => "'".$this->Session->read('Auth.MstUser.id')."'",
                                      'TrnStock.modified'    => "'".$this->request->data['now']."'",
                                      );
                //再利用不可にチェックが入っている場合かつ低レベル品以外の場合のみ、入庫予約数を減らす
                if( ($sticker_info['TrnSticker']['has_reserved'] == TRUE)
                    and ($this->request->data['MstRepayment'][$receiving_id]['is_lowlevel'])){
                    $update_quantity2 = $now_info['TrnStock']['reserve_count']-$receiving_info['TrnReceiving']['quantity'];
                    $updatefield = array_merge($updatefield,array('TrnStock.reserve_count' => $update_quantity2));
                }
                //TrnStock
                $this->TrnStock->create();
                $this->TrnStock->recursive = -1;
                if (!$this->TrnStock->updateAll($updatefield,$condtion)) {
                    return false;
                }

                //預託品の場合、再利用不可により予約数マイナス----------------------------------
            } else {
                //再利用不可フラグがONの場合のみ
                if($sticker_info['TrnSticker']['has_reserved'] == TRUE ){
                    //現在の病院予約数の取得

                    $now_info = $this->TrnStock->find('first',array('fields'=>array('id','reserve_count'),'recursive' => -1,
                                                                    'conditions'=>array('mst_department_id'=>$receiving_info['TrnReceiving']['department_id_from'],
                                                                                        'mst_item_unit_id'=>$receiving_info['TrnReceiving']['mst_item_unit_id'])));

                    //病院予約数マイナス
                    $update_quantity = $now_info['TrnStock']['reserve_count']-$receiving_info['TrnReceiving']['quantity'];
                    $condtion = array( 'TrnStock.id ='=> $now_info['TrnStock']['id'] );
                    $updatefield = array( 'TrnStock.reserve_count' => $update_quantity,
                                          'TrnStock.modifier'    => "'".$this->Session->read('Auth.MstUser.id')."'",
                                          'TrnStock.modified'    => "'".$this->request->data['now']."'",
                                          );
                    //TrnStock
                    $this->TrnStock->create();
                    $this->TrnStock->recursive = -1;
                    if (!$this->TrnStock->updateAll($updatefield,$condtion)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    function create_sql($search_id){
        $where = $this->createSearch_Where();
        //明細表示用
        $sql  = ' select ';
        $sql .= '       trnrepayheader.id                    as "TrnRepayHeader__id" ';
        $sql .= '     , trnrepayheader.work_no               as "TrnRepayHeader__work_no" ';
        $sql .= "     , to_char(trnrepayheader.work_date,'YYYY/mm/dd')";
        $sql .= '                                            as "TrnRepayHeader__work_date" ';
        $sql .= '     , trnrepayheader.repay_type            as "TrnRepayHeader__repay_type" ';
        $sql .= '     , trnrepayheader.is_deleted            as "TrnRepayHeader__is_deleted" ';
        $sql .= '     , trnrepayheader.recital               as "TrnRepayHeader__recital" ';
        $sql .= '     , mstuser.user_name                    as "MstUser__user_name" ';
        $sql .= '     , mstfacilityitem.internal_code        as "MstFacilityItem__internal_code" ';
        $sql .= '     , mstfacilityitem.item_name            as "MstFacilityItem__item_name" ';
        $sql .= '     , mstfacilityitem.standard             as "MstFacilityItem__standard" ';
        $sql .= '     , mstfacilityitem.item_code            as "MstFacilityItem__item_code" ';
        $sql .= '     , mstfacilityitem.is_lowlevel          as "MstFacilityItem__is_lowlevel" ';
        $sql .= '     , ( case when mstitemunit.per_unit = 1 then mstunitname.unit_name ';
        $sql .= "              else mstunitname.unit_name || '(' || mstitemunit.per_unit || mstperunitname.unit_name || ')' ";
        $sql .= '        end )                               as "MstFacilityItem__unit_name"';
        $sql .= '     , mstdealer.dealer_name                as "MstDealer__dealer_name" ';
        $sql .= '     , ( case  ';
        $sql .= "       when trnreceiving.hospital_sticker_no = ' '  ";
        $sql .= '       then trnsticker.hospital_sticker_no  ';
        $sql .= '       else trnreceiving.hospital_sticker_no  ';
        $sql .= '       end )                                as "TrnSticker__hospital_sticker_no" ';
        $sql .= '     , coalesce(  ';
        $sql .= '       trnreceiving.facility_sticker_no ';
        $sql .= '       , trnsticker.facility_sticker_no ';
        $sql .= '     )                                      as "TrnSticker__facility_sticker_no" ';
        $sql .= '     , trnsticker.trade_type                as "TrnSticker__trade_type" ';
        $sql .= '     , trnsticker.lot_no                    as "TrnSticker__lot_no" ';
        $sql .= "     , to_char(trnsticker.validated_date,'YYYY/mm/dd')";
        $sql .= '                                            as "TrnSticker__validated_date" ';
        $sql .= '     , trnsticker.has_reserved              as "TrnSticker__has_reserved" ';
        $sql .= '     , trnsticker.trn_order_id              as "TrnSticker__trn_order_id" ';
        $sql .= '     , trnsticker.quantity                  as "TrnSticker__quantity" ';
        $sql .= '     , trnsticker.mst_department_id         as "TrnSticker__mst_department_id" ';
        $sql .= '     , trnsticker.trn_move_header_id        as "TrnSticker__trn_move_header_id" ';
        $sql .= '     , trnreceiving.department_id_to        as "TrnReceiving__department_id_to" ';
        $sql .= '     , trnreceiving.quantity                as "TrnReceiving__quantity" ';
        $sql .= '     , trnreceiving.recital                 as "TrnReceiving__recital" ';
        $sql .= '     , trnreceiving.stocking_close_type     as "TrnReceiving__stocking_close_type" ';
        $sql .= '     , trnreceiving.sales_close_type        as "TrnReceiving__sales_close_type" ';
        $sql .= '     , trnreceiving.facility_close_type     as "TrnReceiving__facility_close_type" ';
        $sql .= '     , trnreceiving.is_deleted              as "TrnReceiving__is_deleted" ';
        $sql .= '     , trnreceiving.work_type               as "TrnReceiving__work_type" ';
        $sql .= '     , trnreceiving.receiving_status        as "TrnReceiving__receiving_status" ';
        $sql .= '     , trnreceiving.id                      as "TrnReceiving__id" ';
        $sql .= '     , trnreceiving.recital                 as "TrnReceiving__recital" ';
        $sql .= '     , trnclaim.claim_date                  as "TrnClaim__claim_date" ';
        $sql .= '     , sum(trnclaim.unit_price)             as "TrnClaim__unit_price" ';
        $sql .= '     , sum(trnclaim.claim_price)            as "TrnClaim__claim_price" ';
        $sql .= '     , mstdepartment.department_name        as "MstDepartment__department_name" ';
        $sql .= '     , mstdepartment.department_formal_name as "MstDepartment__department_formal_name" ';
        $sql .= '     , trnshipping.move_status              as "TrnShipping__move_status" ';
        $sql .= '     , trnshipping.trn_move_header_id       as "TrnShipping__trn_move_header_id" ';
        $sql .= '     , returnreceiving.receiving_status     as "ReturnReceiving__receiving_status" ';
        $sql .= '     , orderdepartment.department_name      as "OrderDepartment__department_name" ';
        $sql .= '     , orderfacility.id                     as "OrderFacility__id" ';
        $sql .= '     , orderfacility.facility_name          as "OrderFacility__facility_name" ';
        $sql .= '     , orderfacility.facility_formal_name   as "OrderFacility__facility_formal_name" ';
        $sql .= '     , orderfacility.round                  as "OrderFacility__round"  ';
        $sql .= '     , mstclass.name                        as "TrnReceiving__class_name" ';

        $sql .= '     , ( case  ';
        $sql .= "       when trnreceiving.is_deleted = true ";
        $sql .= "       then '取消済み'  ";
        $sql .= "       when trnsticker.has_reserved = true ";
        $sql .= "       then '予約済み'  ";
        $sql .= "       when trnreceiving.sales_close_type = 2 "; //本締め
        $sql .= "       then '締め済み'  ";
        $sql .= "       when trnreceiving.sales_close_type = 1 and x.updatable = false "; //仮締めで、更新権限無し
        $sql .= "       then '締め済み'  ";
        $sql .= "       when trnsticker.trade_type != " .  Configure::read('ClassesType.LowLevel') . " and trnsticker.quantity = 0 ";
        $sql .= "       then 'シールが存在しません'  ";
        $sql .= "       when trnsticker.trade_type != " .  Configure::read('ClassesType.LowLevel') . " and  trnsticker.trade_type != " .  Configure::read('ClassesType.Deposit') . " and trnsticker.mst_department_id != trnreceiving.department_id_to ";
        $sql .= "       then 'シールが存在しません'  ";
        $sql .= "       when returnreceiving.receiving_status = " . Configure::read('ReceivingType.return') . " and trnsticker.has_reserved = TRUE ";
        $sql .= "       then '返品予約済み'  ";
        $sql .= "       when trnshipping.trn_move_header_id is not null and trnshipping.move_status = " . Configure::read('MoveStatus.reserved') ;
        $sql .= "       then '移動予約済み'  ";
        $sql .= "       else ''";
        $sql .= '       end )                                as "TrnReceiving__status" ';

        $sql .= '   from ';
        $sql .= '     trn_repay_headers as trnrepayheader  ';
        $sql .= '     left join trn_receivings as trnreceiving  ';
        $sql .= '       on trnreceiving.trn_repay_header_id = trnrepayheader.id  ';
        $sql .= '     left join trn_stickers as trnsticker  ';
        $sql .= '       on trnreceiving.trn_sticker_id = trnsticker.id  ';
        $sql .= '     left join trn_claims as trnclaim  ';
        $sql .= '       on trnreceiving.id = trnclaim.trn_receiving_id  ';
        $sql .= '     left join mst_item_units as mstitemunit  ';
        $sql .= '       on trnreceiving.mst_item_unit_id = mstitemunit.id  ';
        $sql .= '     left join mst_users as mstuser  ';
        $sql .= '       on trnrepayheader.creater = mstuser.id  ';
        $sql .= '     left join mst_facility_items as mstfacilityitem  ';
        $sql .= '       on mstitemunit.mst_facility_item_id = mstfacilityitem.id  ';
        $sql .= '     left join mst_dealers as mstdealer  ';
        $sql .= '       on mstfacilityitem.mst_dealer_id = mstdealer.id  ';
        $sql .= '     left join mst_unit_names as mstunitname  ';
        $sql .= '       on mstitemunit.mst_unit_name_id = mstunitname.id  ';
        $sql .= '     left join mst_unit_names as mstperunitname  ';
        $sql .= '       on mstitemunit.per_unit_name_id = mstperunitname.id  ';
        $sql .= '     left join mst_departments as mstdepartment  ';
        $sql .= '       on trnrepayheader.department_id_from = mstdepartment.id  ';
        $sql .= '     left join mst_facilities as mstfacility  ';
        $sql .= '       on mstdepartment.mst_facility_id = mstfacility.id  ';
        $sql .= '     left join trn_shippings as trnshipping  ';
        $sql .= '       on trnsticker.trn_shipping_id = trnshipping.id  ';
        $sql .= '     left join trn_receivings as returnreceiving  ';
        $sql .= '       on returnreceiving.id = trnsticker.trn_return_receiving_id  ';
        $sql .= '     left join trn_orders as trnorder  ';
        $sql .= '       on trnsticker.trn_order_id = trnorder.id  ';
        $sql .= '     left join mst_departments as orderdepartment  ';
        $sql .= '       on trnorder.department_id_to = orderdepartment.id  ';
        $sql .= '     left join mst_facilities as orderfacility  ';
        $sql .= '       on orderdepartment.mst_facility_id = orderfacility.id  ';
        $sql .= '     left join mst_classes as mstclass ';
        $sql .= '       on mstclass.id = trnreceiving.work_type ';
        $sql .= '     cross join (  ';
        $sql .= '       select ';
        $sql .= '             c.updatable  ';
        $sql .= '         from ';
        $sql .= '           mst_users as a  ';
        $sql .= '           left join mst_roles as b  ';
        $sql .= '             on a.mst_role_id = b.id  ';
        $sql .= '           left join mst_privileges as c  ';
        $sql .= '             on c.mst_role_id = b.id  ';
        $sql .= '         where ';
        $sql .= '           a.id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '           and c.view_no = 32'; //返却履歴
        $sql .= '     ) as x  ';

        $sql .= '   where ';

        if (empty($search_id)) {
            //帳票印刷とSQLを共用しているため、施設・部署はここで追加
            //施設(完全一致)---------------------------------------------------
            if((isset($this->request->data['user_customer'])) and ($this->request->data['user_customer'] != "")){
                $_customer = $this->MstFacility->find('first',array('fields'=>'id','conditions'=>array('facility_code'=>$this->request->data['user_customer'] , 'MstFacility.facility_type'=> array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')))));
                $where .= '     and MstFacility.id = ' ."'".Sanitize::escape($_customer['MstFacility']['id'])."' ";
            }

            //部署(完全一致)---------------------------------------------------
            if((isset($this->request->data['MstRepayment']['user_department'])) and ($this->request->data['MstRepayment']['user_department'] != "")){
                $_fromDepartment = $this->MstDepartment->find('first',array('fields'=>'id','conditions'=>array('department_code'=>$this->request->data['MstRepayment']['user_department']
                        ,'mst_facility_id'=>$_customer['MstFacility']['id']   )));
                $_customer = $this->MstFacility->find('first',array('fields'=>'id','conditions'=>array('MstFacility.facility_code'=>$this->request->data['user_customer'] , 'MstFacility.facility_type'=> array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')))));
                $where .= '     and TrnRepayHeader.department_id_from = ' ."'".Sanitize::escape($_fromDepartment['MstDepartment']['id'])."' ";
                $where .= '     and MstFacility.id = ' ."'".Sanitize::escape($_customer['MstFacility']['id'])."' ";
            }

            //ヘッダIDが空(CSV出力)のため、施設の条件を追加
            $where .= '     and MstFacilityItem.mst_facility_id = ' .$this->Session->read('Auth.facility_id_selected');
            $sql .= '     1 = 1  ' . $where ;

        } else {
            $sql .= '     trnrepayheader.id in ('.$search_id.') ';
            $sql .= '     and 1 = 1  ' . $where ;
        }

        $sql .= '     and (  ';
        $sql .= '       trnrepayheader.is_deleted = false  ';
        $sql .= '       or trnrepayheader.is_deleted = true ';
        $sql .= '     )  ';
        $sql .= '     and (  ';
        $sql .= '       trnreceiving.is_deleted = false  ';
        $sql .= '       or trnreceiving.is_deleted = true ';
        $sql .= '     )  ';
        $sql .= "     and trnclaim.is_stock_or_sale = '2'  ";
        $sql .= '   group by ';
        $sql .= '     trnrepayheader.id ';
        $sql .= '     , trnrepayheader.work_no ';
        $sql .= '     , trnrepayheader.work_date ';
        $sql .= '     , trnrepayheader.repay_type ';
        $sql .= '     , trnrepayheader.is_deleted ';
        $sql .= '     , trnrepayheader.recital ';
        $sql .= '     , mstfacility.facility_name ';
        $sql .= '     , mstfacility.facility_formal_name ';
        $sql .= '     , mstuser.user_name ';
        $sql .= '     , trnrepayheader.created ';
        $sql .= '     , trnrepayheader.recital ';
        $sql .= '     , trnrepayheader.detail_count ';
        $sql .= '     , mstfacilityitem.id ';
        $sql .= '     , mstfacilityitem.internal_code ';
        $sql .= '     , mstfacilityitem.item_name ';
        $sql .= '     , mstfacilityitem.standard ';
        $sql .= '     , mstfacilityitem.item_code ';
        $sql .= '     , mstfacilityitem.mst_dealer_id ';
        $sql .= '     , mstfacilityitem.tax_type ';
        $sql .= '     , mstfacilityitem.is_lowlevel ';
        $sql .= '     , mstitemunit.id ';
        $sql .= '     , mstitemunit.per_unit ';
        $sql .= '     , mstitemunit.mst_unit_name_id ';
        $sql .= '     , mstunitname.id ';
        $sql .= '     , mstunitname.unit_name ';
        $sql .= '     , mstperunitname.id ';
        $sql .= '     , mstperunitname.unit_name ';
        $sql .= '     , mstdealer.dealer_name ';
        $sql .= '     , trnreceiving.facility_sticker_no ';
        $sql .= '     , trnsticker.facility_sticker_no ';
        $sql .= '     , trnsticker.hospital_sticker_no ';
        $sql .= '     , trnsticker.trade_type ';
        $sql .= '     , trnsticker.lot_no ';
        $sql .= '     , trnsticker.validated_date ';
        $sql .= '     , trnsticker.has_reserved ';
        $sql .= '     , trnsticker.trn_order_id ';
        $sql .= '     , trnsticker.quantity ';
        $sql .= '     , trnsticker.mst_department_id ';
        $sql .= '     , trnsticker.trn_move_header_id ';
        $sql .= '     , trnreceiving.hospital_sticker_no ';
        $sql .= '     , trnreceiving.department_id_to ';
        $sql .= '     , trnreceiving.quantity ';
        $sql .= '     , trnreceiving.recital ';
        $sql .= '     , trnreceiving.stocking_close_type ';
        $sql .= '     , trnreceiving.sales_close_type ';
        $sql .= '     , trnreceiving.facility_close_type ';
        $sql .= '     , trnreceiving.is_deleted ';
        $sql .= '     , trnreceiving.work_type ';
        $sql .= '     , trnreceiving.receiving_status ';
        $sql .= '     , trnreceiving.id ';
        $sql .= '     , trnreceiving.recital ';
        $sql .= '     , trnclaim.claim_date ';
        $sql .= '     , mstdepartment.department_name ';
        $sql .= '     , mstdepartment.department_formal_name ';
        $sql .= '     , trnshipping.move_status ';
        $sql .= '     , trnshipping.trn_move_header_id ';
        $sql .= '     , returnreceiving.receiving_status ';
        $sql .= '     , orderdepartment.department_name ';
        $sql .= '     , orderfacility.id ';
        $sql .= '     , orderfacility.facility_name ';
        $sql .= '     , orderfacility.facility_formal_name ';
        $sql .= '     , orderfacility.round  ';
        $sql .= '     , mstclass.name';
        $sql .= '     , x.updatable';
        $sql .= '   order by ';
        $sql .= '     trnrepayheader.work_no ';
        return $sql;
    }

    function report($mode){
        if($mode == "invoice"){
            $this->print_invoice();
        } elseif($mode == "requestsales"){
            $this->print_requestsales();
        }
    }

    /**
     * print_invoice
     *
     * 納品書
     */
    function print_invoice(){
        $now = date('Y/m/d H:i:s');
        App::import('Sanitize');
        $where = "";
        //where句作成
        $where = $this->createSearch_Where();

        //選択したものだけと言う条件を追加
        //SQL用に整形
        $tmp = explode(',',$this->request->data['TrnRepay']['TrnRepayNo'][0]);
        foreach($tmp as $repay_header){
            $pre[] = "'".Sanitize::escape($repay_header)."'";
        }
        $search_repay_header = implode(",", $pre);
        $where .= ' and TrnRepayHeader.id in ('.$search_repay_header.') ';

        $sql = ' select '.
                ' TrnRepayHeader.id                        as TrnRepayHeader__id'.
                ',TrnRepayHeader.work_no                   as TrnRepayHeader__work_no'.
                ',TrnRepayHeader.work_date                 as TrnRepayHeader__work_date'.
                ',TrnRepayHeader.repay_type                as TrnRepayHeader__repay_type'.
                ',TrnRepayHeader.recital                   as TrnRepayHeader__recital'.//ヘッダ備考
                ',TrnRepayHeader.department_id_from        as TrnRepayHeader__department_id_from'.
                ',TrnRepayHeader.department_id_to          as TrnRepayHeader__department_id_to'.
                ',TrnRepayHeader.printed_date              as TrnRepayHeader__printed_date'.
                ',MstFacilityTO.facility_name              as MstFacilityTO__facility_name'.
                ',MstFacilityTO.zip                        as MstFacilityTO__zip'.
                ',MstFacilityTO.address                    as MstFacilityTO__address'.
                ',MstFacilityTO.tel                        as MstFacilityTO__tel'.
                ',MstFacilityTO.fax                        as MstFacilityTO__fax'.
                ',MstFacilityTO.facility_formal_name       as MstFacilityTO__facility_formal_name'.
                ',MstFacilityFROM.facility_formal_name     as MstFacilityFROM__facility_formal_name'.
                ',MstFacilityFROM.id                       as MstFacilityFROM__id'.
                ',MstUser.user_name                        as MstUser__user_name'.
                ',TrnRepayHeader.created                   as TrnRepayHeader__created'.
                ',TrnRepayHeader.recital                   as TrnRepayHeader__recital'.
                ',MstDepartmentFROM.department_formal_name as MstDepartmentFROM__department_formal_name'.
                ',TrnReceiving.id                          as TrnReceiving__id'.
                ',TrnReceiving.work_no                     as TrnReceiving__work_no'.//売上番号
                ',TrnReceiving.work_date                   as TrnReceiving__work_date'.//
                ',TrnReceiving.recital                     as TrnReceiving__recital'.//明細備考
                ',TrnReceiving.quantity                    as TrnReceiving__quantity'.//数量
                  ',TrnReceiving.is_deleted                as TrnReceiving__is_deleted'.//削除フラグ
                ',TrnSticker.trade_type                    as TrnSticker__trade_type'.
                ',( CASE '.
                " WHEN TrnReceiving.hospital_sticker_no = ' '".
                " THEN TrnSticker.hospital_sticker_no".
                " ELSE TrnReceiving.hospital_sticker_no".
                " END )                                    as TrnSticker__hospital_sticker_no".//部署シール
                ',TrnSticker.lot_no                        as TrnSticker__lot_no'.//ロット番号
                ',TrnSticker.trn_order_id                  as TrnSticker__trn_order_id'.
                ',MstFacilityItem.internal_code            as MstFacilityItem__internal_code'.//商品ID
                ',MstFacilityItem.item_name                as MstFacilityItem__item_name'.//商品名
                ',MstFacilityItem.standard                 as MstFacilityItem__standard'.
                ',MstFacilityItem.item_code                as MstFacilityItem__item_code'.//製品番号
                ',MstFacilityItem.mst_dealer_id            as MstFacilityItem__mst_dealer_id'.
                ',MstFacilityItem.tax_type                 as MstFacilityItem__tax_type'.//課税区分
                ',MstFacilityItem.mst_owner_id             as MstFacilityItem__mst_owner_id'.//施主
                ',MstItemUnit.per_unit                     as MstItemUnit__per_unit'.
                ',MstItemUnit.mst_unit_name_id             as MstItemUnit__mst_unit_name_id'.
                ',MstUnitName.id                           as MstUnitName__id'.
                ',MstUnitName.unit_name                    as MstUnitName__unit_name'.//包装単位？
                ',MstPerUnitName.id                        as MstPerUnitName__id'.
                ',MstPerUnitName.unit_name                 as MstPerUnitName__unit_name'.
                ',MstDealer.dealer_name                    as MstDealer__dealer_name'.//販売元
                ',TrnClaim.claim_date                      as TrnClaim__claim_date'.//売上日
                ',sum(TrnClaim.claim_price)                as TrnClaim__claim_price'.//金額
                ',sum(TrnClaim.unit_price)                 as TrnClaim__unit_price'.//単価
                ',TrnClaim.round                           as TrnClaim__round'.//丸め区分
                ',TrnClaim.department_id_from              as TrnClaim__department_id_from'.//仕入設定検索用

          ' from '.
            ' trn_repay_headers as TrnRepayHeader '.
            ' inner join trn_receivings as TrnReceiving on TrnReceiving.trn_repay_header_id = TrnRepayHeader.id '.
            ' inner join trn_claims as TrnClaim on TrnClaim.trn_receiving_id = TrnReceiving.id '.
            ' inner join trn_stickers as TrnSticker on TrnReceiving.trn_sticker_id = TrnSticker.id '.
            ' inner join mst_item_units as MstItemUnit on TrnReceiving.mst_item_unit_id = MstItemUnit.id '.
            ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id ' .
            ' inner join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id ' .
            ' inner join mst_users as MstUser on TrnRepayHeader.creater = MstUser.id'.
            ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ' .
            ' left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id ' .
            ' inner join mst_departments as MstDepartmentFROM on TrnRepayHeader.department_id_from = MstDepartmentFROM.id ' .
            ' inner join mst_departments as MstDepartmentTO on TrnRepayHeader.department_id_to = MstDepartmentTO.id ' .
            ' inner join mst_facilities as MstFacilityFROM on MstDepartmentFROM.mst_facility_id = MstFacilityFROM.id '.
            ' inner join mst_facilities as MstFacilityTO on MstDepartmentTO.mst_facility_id = MstFacilityTO.id '.

         ' where  ' .
              '  1=1 '.$where.
              " and TrnClaim.is_stock_or_sale = '2' ".  //マイナス売上データのみ

            ' group by '.
                ' TrnRepayHeader.id '.
                ',TrnRepayHeader.work_no '.
                ',TrnRepayHeader.work_date '.
                ',TrnRepayHeader.repay_type'.
                ',TrnRepayHeader.recital'.//ヘッダ備考
                ',TrnRepayHeader.department_id_from'.
                ',TrnRepayHeader.department_id_to'.
                ',TrnRepayHeader.printed_date'.
                ',MstFacilityTO.facility_name '.
                ',MstFacilityTO.zip '.
                ',MstFacilityTO.address '.
                ',MstFacilityTO.tel '.
                ',MstFacilityTO.fax '.
                ',MstFacilityTO.facility_formal_name '.
                ',MstFacilityFROM.facility_formal_name '.
                ',MstFacilityFROM.id '.
                ',MstUser.user_name '.
                ',TrnRepayHeader.created '.
                ',TrnRepayHeader.recital '.
                ',MstDepartmentFROM.department_formal_name '.
                ',TrnReceiving.id '.
                ',TrnReceiving.work_no './/売上番号
                ',TrnReceiving.work_date './/
                ',TrnReceiving.recital './/明細備考
                ',TrnReceiving.quantity './/数量
                ',TrnReceiving.is_deleted './/削除フラグ
                ',TrnSticker.trade_type '.
                ",TrnReceiving.hospital_sticker_no ".
                ",TrnSticker.hospital_sticker_no".
                ',TrnSticker.lot_no'.//ロット番号
                ',TrnSticker.trn_order_id '.
                ',MstFacilityItem.internal_code'.//商品ID
                ',MstFacilityItem.item_name './/商品名
                ',MstFacilityItem.standard '.
                ',MstFacilityItem.item_code './/製品番号
                ',MstFacilityItem.mst_dealer_id'.
                ',MstFacilityItem.tax_type './/課税区分
                ',MstFacilityItem.mst_owner_id './/施主
                ',MstItemUnit.per_unit '.
                ',MstItemUnit.mst_unit_name_id '.
                ',MstUnitName.id '.
                ',MstUnitName.unit_name'.//包装単位？
                ',MstPerUnitName.id '.
                ',MstPerUnitName.unit_name '.
                ',MstDealer.dealer_name './/販売元
                ',TrnClaim.claim_date './/売上日
                ',TrnClaim.round './/丸め区分
                ',TrnClaim.department_id_from './/仕入設定検索用

         ' order by MstFacilityItem.mst_owner_id ASC, TrnReceiving.work_no ASC';



        //**********************************************************************
        $ReturnList = $this->TrnRepayHeader->query($sql);
        //**********************************************************************

        //納品書
        $columns = array("納品先名",
                         "納品番号",
                         "納品先部署名",
                         "施主名",
                         "納品日",
                         "納品先住所",
                         "施主住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "部署シール",
                         "ロット番号",
                         "明細備考",
                         "数量",
                         "包装単位",
                         "管理区分",
                         "商品ID",
                         "単価",
                         "金額",
                         "ヘッダ備考",
                         "丸め区分",
                         "印刷年月日",
                         "改ページ番号",
                         "削除フラグ"
                         );
        $data = array();
        $today = date("Y/m/d");//印刷年月日
        $work_seq = 1;
        $work_seq_check = "";
        foreach($ReturnList as $data) {
            $chk=0;

            if($work_seq_check != $data['trnrepayheader']['work_no']){
                $work_seq = 1;
            } else {
                if($work_seq_check2 != $data['mstfacilityitem']['mst_owner_id']){
                    $work_seq = 1;
                } else {
                    $work_seq++;
                }
            }

            $page_break = $data['trnrepayheader']['work_no'].$data['mstfacilityitem']['mst_owner_id'];//改ページ番号

            $day = date("Y年m月d日", strtotime($data['trnclaim']['claim_date']));

            //施主関連情報取得

            $ownerID = $data['mstfacilityitem']['mst_owner_id'];
            $ownerInfo = $this->MstFacility->find('first',array('fields'=>'','conditions'=>array('id'=>$ownerID)));
            $ownerAddress = "〒".$ownerInfo['MstFacility']['zip']."　".$ownerInfo['MstFacility']['address']
              ."\nTel:".$ownerInfo['MstFacility']['tel']."／Fax:".$ownerInfo['MstFacility']['fax'];

            $facilityFromAddress = "〒".$data['mstfacilityto']['zip']."　".$data['mstfacilityto']['address']
              ."\nTel:".$data['mstfacilityto']['tel']."／Fax:".$data['mstfacilityto']['fax'];

            //請求テーブルの丸め区分を取得

            if($data['trnclaim']['round'] != NULL and $data['trnclaim']['round'] != ''){
                $data['customer_facility']['round'] = $data['trnclaim']['round'];
            } else {
                $data['customer_facility']['round'] = "0";
            }

            //明細行の金額を丸め区分にしたがって丸める
            //四捨五入
            if($data['customer_facility']['round'] == Configure::read('RoundType.Round')){
                $claim_price = round($data['trnclaim']['claim_price']);
                //切り上げ
            } elseif($data['customer_facility']['round'] == Configure::read('RoundType.Ceil')){
                $claim_price = ceil($data['trnclaim']['claim_price']);
                //切捨て
            } elseif($data['customer_facility']['round'] == Configure::read('RoundType.Floor')){
                $claim_price = floor($data['trnclaim']['claim_price']);
                //何もしない
            } elseif($data['customer_facility']['round'] == Configure::read('RoundType.Normal')){
                $claim_price = $data['trnclaim']['claim_price'];
                //その他（何もしないと同じ）
            } else {
                $claim_price = $data['trnclaim']['claim_price'];
            }
            $output[] = array($data['mstfacilityfrom']['facility_formal_name']//納品先名
                              ,$data['trnrepayheader']['work_no']//納品番号
                              ,$data['mstdepartmentfrom']['department_formal_name']//納品先部署名
                              ,$ownerInfo['MstFacility']['facility_formal_name']//施主名
                              ,$day//納品日
                              ,$facilityFromAddress//納品先住所
                              ,$ownerAddress//施主住所
                              ,$work_seq//行番号
                              ,$data['mstfacilityitem']['item_name']//商品名
                              ,$data['mstfacilityitem']['standard']//規格
                              ,$data['mstfacilityitem']['item_code']//製品番号
                              ,$data['mstdealer']['dealer_name']//販売元
                              ,$data['trnsticker']['hospital_sticker_no']//部署シール
                              ,$data['trnsticker']['lot_no']//ロット番号
                              ,$data['trnreceiving']['recital']//明細備考
                              ,(0-$data['trnreceiving']['quantity'])//数量
                              ,$data['mstunitname']['unit_name']//包装単位
                              ,Configure::read('Classes.'.$data['trnsticker']['trade_type'])//管理区分
                              ,$data['mstfacilityitem']['internal_code']//商品ID
                              ,$data['trnclaim']['unit_price']//単価（請求テーブル.請求単価）
                              ,number_format($claim_price,2)//金額金額（請求テーブル.請求金額）
                              ,$data['trnrepayheader']['recital']//ヘッダ備考
                              ,$data['customer_facility']['round']//得意先丸め区分
                              ,$data['trnrepayheader']['printed_date']//印刷年月日：再印刷判定用
                              ,$page_break//改ページ番号
                              ,$data['trnreceiving']['is_deleted']);//削除フラグ

            $work_seq_check = $data['trnrepayheader']['work_no'];
            $work_seq_check2 = $data['mstfacilityitem']['mst_owner_id'];

            //再印刷用にヘッダの印刷日時を更新
            $condtion = array( 'TrnRepayHeader.id' => $data['trnrepayheader']['id'] );
            $updatefield = array( 'printed_date'   => "'".$now."'",  //印刷日時
                                  'modifier'       => "'".$this->request->data['repay_user_id']."'",
                                  'modified'       => "'".$now."'",
                                  );

            //TrnPromiseオブジェクトをcreate
            $this->TrnRepayHeader->create();

            //SQL実行
            $this->TrnRepayHeader->recursive = -1;
            if (!$this->TrnRepayHeader->updateAll( $updatefield, $condtion )) {
                //ロールバック
                $this->TrnRepayHeader->rollback();//返却ヘッダ
                $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                $this->redirect('index');
            }
            $this->TrnRepayHeader->commit();
            unset($condtion,$updatefield);
        }

        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'納品書');
        $this->xml_output($output,join("\t",$columns),$layout_name['09'],$fix_value,'invoice');
    }

    /**
     * xml出力テスト用 →共通化予定？
     * @param
     * @todo 各コントローラの共通関数化
     */
    function xml_output($data,$columns,$layout_name,$fix_value,$layout_file){
        $this->layout = 'xml/default';
        $this->set('columns',$columns);
        $this->set('posted',$data);
        $this->set('layout_name',$layout_name);
        $this->set('fix_value',$fix_value); //連想配列のキーにname属性を指定
        $this->render($layout_file);
    }

    /**
     * print_requestsales
     * 売上依頼表印刷
     */
    function print_requestsales(){
        $now = date('Y/m/d H:i:s');
        App::import('Sanitize');
        $where = "";
        //where句作成
        $where = $this->createSearch_Where();

        //前画面で選択されたものだけという条件を追加
        //SQL用に整形
        $search_repay_header = implode(",", $this->request->data['TrnRepay']['TrnRepayNo']);
        $where .= ' and TrnRepayHeader.id in ('.$search_repay_header.') ';

        $sql = ' select '.
                ' TrnRepayHeader.id                        as TrnRepayHeader__id'.
                ',TrnRepayHeader.work_no                   as TrnRepayHeader__work_no'.
                ',TrnRepayHeader.work_date                 as TrnRepayHeader__work_date'.
                ',TrnRepayHeader.repay_type                as TrnRepayHeader__repay_type'.
                ',TrnRepayHeader.recital                   as TrnRepayHeader__recital'.//ヘッダ備考
                ',TrnRepayHeader.department_id_from        as TrnRepayHeader__department_id_from'.//debug
                ',TrnRepayHeader.department_id_to          as TrnRepayHeader__department_id_to'.//debug
                ',TrnRepayHeader.printed_date2             as TrnRepayHeader__printed_date2'.
                ',MstFacilityTO.facility_name              as MstFacilityTO__facility_name'.
                ',MstFacilityTO.zip                        as MstFacilityTO__zip'.
                ',MstFacilityTO.address                    as MstFacilityTO__address'.
                ',MstFacilityTO.tel                        as MstFacilityTO__tel'.
                ',MstFacilityTO.fax                        as MstFacilityTO__fax'.
                ',MstFacilityTO.facility_formal_name       as MstFacilityTO__facility_formal_name'.
                ',MstFacilityFROM.facility_formal_name     as MstFacilityFROM__facility_formal_name'.
                ',MstFacilityFROM.id                       as MstFacilityFROM__id'.
                ',MstUser.user_name                        as MstUser__user_name'.
                ',TrnRepayHeader.created                   as TrnRepayHeader__created'.
                ',TrnRepayHeader.recital                   as TrnRepayHeader__recital'.
                ',MstDepartmentFROM.department_formal_name as MstDepartmentFROM__department_formal_name'.
                ',TrnReceiving.id                          as TrnReceiving__id'.
                ',TrnReceiving.work_no                     as TrnReceiving__work_no'.//売上番号
                ',TrnReceiving.work_date                   as TrnReceiving__work_date'.//
                ',TrnReceiving.recital                     as TrnReceiving__recital'.//明細備考
                ',TrnReceiving.quantity                    as TrnReceiving__quantity'.//数量
                ',TrnReceiving.is_deleted                  as TrnReceiving__is_deleted'.//削除フラグ
                ',( CASE '.
                " WHEN TrnReceiving.hospital_sticker_no = ' '".
                " THEN TrnSticker.hospital_sticker_no".
                " ELSE TrnReceiving.hospital_sticker_no".
                " END )                                    as TrnSticker__hospital_sticker_no".//部署シール
                ',TrnSticker.lot_no                        as TrnSticker__lot_no'.//ロット番号
                ',MstFacilityItem.internal_code            as MstFacilityItem__internal_code'.//商品ID
                ',MstFacilityItem.item_name                as MstFacilityItem__item_name'.//商品名
                ',MstFacilityItem.standard                 as MstFacilityItem__standard'.
                ',MstFacilityItem.item_code                as MstFacilityItem__item_code'.//製品番号
                ',MstFacilityItem.mst_dealer_id            as MstFacilityItem__mst_dealer_id'.
                ',MstFacilityItem.tax_type                 as MstFacilityItem__tax_type'.//課税区分
                ',MstFacilityItem.mst_owner_id             as MstFacilityItem__mst_owner_id'.//施主
                ',MstItemUnit.per_unit                     as MstItemUnit__per_unit'.
                ',MstItemUnit.mst_unit_name_id             as MstItemUnit__mst_unit_name_id'.
                ',MstUnitName.id                           as MstUnitName__id'.
                ',MstUnitName.unit_name                    as MstUnitName__unit_name'.//包装単位？
                ',MstPerUnitName.id                        as MstPerUnitName__id'.
                ',MstPerUnitName.unit_name                 as MstPerUnitName__unit_name'.
                ',MstDealer.dealer_name                    as MstDealer__dealer_name'.//販売元
                ',TrnClaim.id                              as TrnClaim__id'.
                ',TrnClaim.claim_date                      as TrnClaim__claim_date'.//売上日
                ',TrnClaim.claim_price                     as TrnClaim__claim_price'.//金額

                ',TrnClaim.unit_price                      as TrnClaim__unit_price'.//単価
                ',TrnClaim.round                           as TrnClaim__round'.//丸め区分

                ',TrnClaim.department_id_from              as TrnClaim__department_id_from'.//仕入設定検索用
                ',TrnClaim.mst_item_unit_id                as TrnClaim__mst_item_unit_id'.//仕入設定検索用

                //仕入先情報
                ',OrderDepartment.department_name          as OrderDepartment__department_name'.
                ',OrderFacility.id                         as OrderFacility__id'.
                ',OrderFacility.facility_name              as OrderFacility__facility_name'.
                ',OrderFacility.facility_formal_name       as OrderFacility__facility_formal_name'.
                ',OrderFacility.round                      as OrderFacility__round'.//丸め区分


          ' from '.
            ' trn_repay_headers as TrnRepayHeader '.
            ' inner join trn_receivings as TrnReceiving on TrnReceiving.trn_repay_header_id = TrnRepayHeader.id '.
            ' inner join trn_claims as TrnClaim on TrnClaim.trn_receiving_id = TrnReceiving.id '.
            ' inner join trn_stickers as TrnSticker on TrnReceiving.trn_sticker_id = TrnSticker.id '.
            ' inner join mst_item_units as MstItemUnit on TrnReceiving.mst_item_unit_id = MstItemUnit.id '.
            ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id ' .
            ' inner join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id ' .
            ' inner join mst_users as MstUser on TrnRepayHeader.creater = MstUser.id'.
            ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ' .
            ' left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id ' .
            ' inner join mst_departments as MstDepartmentFROM on TrnRepayHeader.department_id_from = MstDepartmentFROM.id ' .
            ' inner join mst_departments as MstDepartmentTO on TrnRepayHeader.department_id_to = MstDepartmentTO.id ' .
            ' inner join mst_facilities as MstFacilityFROM on MstDepartmentFROM.mst_facility_id = MstFacilityFROM.id '.
            ' inner join mst_facilities as MstFacilityTO on MstDepartmentTO.mst_facility_id = MstFacilityTO.id '.
            ' inner join trn_orders as TrnOrder on TrnSticker.trn_order_id = TrnOrder.id '.
            ' inner join mst_departments as OrderDepartment on TrnOrder.department_id_to = OrderDepartment.id '.
            ' inner join mst_facilities as OrderFacility on OrderDepartment.mst_facility_id = OrderFacility.id '.

         ' where  ' .
              '  1=1 '.$where.
              " and TrnClaim.is_stock_or_sale = '2' ".  //マイナス売上データのみ
              ' and TrnRepayHeader.repay_type = ' . Configure::read('RepayType.replenish').
         ' order by TrnReceiving.work_no ASC';

        //**********************************************************************
        $ReturnList = $this->TrnRepayHeader->query($sql);
        //**********************************************************************
        //売上依頼票
        $columns = array("売上依頼番号",
                         "仕入先名",
                         "売上番号",
                         "売上部署名",
                         "施主名",
                         "売上日",
                         "施主住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "金額",
                         "単価",
                         "明細備考",
                         "数量",
                         "包装単位",
                         "課税区分",
                         "部署シール",
                         "ロット番号",
                         "ヘッダ備考",
                         "丸め区分",
                         "印刷年月日",
                         "改ページ番号",
                         "削除フラグ"
                         );
        $data = array();
        $today = date("Y/m/d");//印刷年月日
        $work_seq = 1;
        $tax_type = Configure::read('PrintFacilityItems.taxes');
        $work_seq_check = "";
        $output = array();
        foreach($ReturnList as $data){
          $chk=0;

            if($work_seq_check != $data['trnrepayheader']['work_no']){
                $work_seq = 1;
            } else {
                if($work_seq_check2 != $data['mstfacilityitem']['mst_owner_id']){
                    $work_seq = 1;
                } else {
                    $work_seq++;
                }
            }

            //請求テーブルの丸め区分を取得

            if($data['trnclaim']['round'] != NULL and $data['trnclaim']['round'] != ''){
                $data['customer_facility']['round'] = $data['trnclaim']['round'];
            } else {
                $data['customer_facility']['round'] = "0";
            }
            //四捨五入
            if($data['customer_facility']['round'] == Configure::read('RoundType.Round')){
                $claim_price = round($data['trnclaim']['claim_price']);

                //切り上げ
            } elseif($data['customer_facility']['round'] == Configure::read('RoundType.Ceil')){
                $claim_price = ceil($data['trnclaim']['claim_price']);

                //切捨て
            } elseif($data['customer_facility']['round'] == Configure::read('RoundType.Floor')){
                $claim_price = floor($data['trnclaim']['claim_price']);

                //何もしない

            } elseif($data['customer_facility']['round'] == Configure::read('RoundType.Normal')){
                $claim_price = $data['trnclaim']['claim_price'];
                //その他（何もしないと同じ）

            } else {
                $claim_price = $data['trnclaim']['claim_price'];
            }

            $day = date("Y年m月d日", strtotime($data['trnclaim']['claim_date']));
            $page_break = $data['trnrepayheader']['work_no'].$data['mstfacilityitem']['mst_owner_id'];//改ページ番号


            //施主関連情報取得
            $ownerID = $data['mstfacilityitem']['mst_owner_id'];
            $ownerInfo = $this->MstFacility->find('first',array('fields'=>'','conditions'=>array('id'=>$ownerID)));
            $ownerAddress = "〒".$ownerInfo['MstFacility']['zip']."　".$ownerInfo['MstFacility']['address']
              ."\nTel:".$ownerInfo['MstFacility']['tel']."／Fax:".$ownerInfo['MstFacility']['fax'];

            if((!isset($data['mstfacilityitem']['tax_type'])) and ($data['mstfacilityitem']['tax_type'] == '')){
                $output_tax_type = "";
            } else {
                $output_tax_type = $tax_type[$data['mstfacilityitem']['tax_type']];
            }

            $output[] = array($data['trnrepayheader']['work_no']//売上依頼番号
                              ,$data['orderfacility']['facility_formal_name']."様"//仕入先名
                              ,$data['trnrepayheader']['work_no']//売上番号
                              ,$data['mstdepartmentfrom']['department_formal_name']//売上部署名
                              ,$ownerInfo['MstFacility']['facility_formal_name']//施主名
                              ,$day//売上日
                              ,$ownerAddress//施主住所
                              ,$work_seq//行番号
                              ,$data['mstfacilityitem']['item_name']//商品名
                              ,$data['mstfacilityitem']['standard']//規格
                              ,$data['mstfacilityitem']['item_code']//製品番号
                              ,$data['mstdealer']['dealer_name']//販売元
                              ,$data['mstfacilityitem']['internal_code']//商品ID
                              ,$claim_price//金額
                              ,$data['trnclaim']['unit_price']//単価
                              ,$data['trnreceiving']['recital']//明細備考
                              ,(0-$data['trnreceiving']['quantity'])//数量
                              ,$data['mstunitname']['unit_name']//包装単位
                              ,$output_tax_type//課税区分
                              ,$data['trnsticker']['hospital_sticker_no']//部署シール
                              ,$data['trnsticker']['lot_no']//ロット番号
                              ,$data['trnrepayheader']['recital']//ヘッダ備考
                              ,$data['orderfacility']['round']//仕入先丸め区分
                              ,$data['trnrepayheader']['printed_date2']//印刷年月日：再印刷判定用
                              ,$page_break//改ページ番号
                              ,$data['trnreceiving']['is_deleted']);//削除フラグ

            $work_seq_check = $data['trnrepayheader']['work_no'];
            $work_seq_check2 = $data['mstfacilityitem']['mst_owner_id'];

            //再印刷用にヘッダの印刷日時を更新
            $condtion = array( 'TrnRepayHeader.id' => $data['trnrepayheader']['id'] );
            $updatefield = array( 'printed_date2'  => "'".$now."'",  //印刷日時
                                  'modifier'       => "'".$this->request->data['repay_user_id']."'",
                                  'modified'       => "'".$now."'",
                                  );

            //TrnPromiseオブジェクトをcreate
            $this->TrnRepayHeader->create();

            //SQL実行
            $this->TrnRepayHeader->recursive = -1;
            if (!$this->TrnRepayHeader->updateAll( $updatefield, $condtion )) {
                //ロールバック
                $this->TrnRepayHeader->rollback();//返却ヘッダ
                $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                $this->redirect('index');
            }
            $this->TrnRepayHeader->commit();
            unset($condtion,$updatefield);

        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'売上依頼票');
        $layout_file = "requestsales";
        $this->xml_output($output,join("\t",$columns),$layout_name['12'],$fix_value,$layout_file);
    }


    //where句作成用
    function createSearch_Where($f_id = null){
        $where = "";

        //ユーザ入力値による検索条件の追加****************************************
        //返却日----------------------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_start_date'])) and ($this->request->data['MstRepayment']['search_start_date'] != "")){
            $where .= ' and TrnRepayHeader.work_date >= ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_start_date'])."' ";
        }
        if((isset($this->request->data['MstRepayment']['search_end_date'])) and ($this->request->data['MstRepayment']['search_end_date'] != "")){
            $where .= ' and TrnRepayHeader.work_date <= ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_end_date'])."' ";
        }

        //返却番号(完全一致)-------------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_work_no'])) and ($this->request->data['MstRepayment']['search_work_no'] != "")){
            $where .= ' and TrnRepayHeader.work_no = ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_work_no'])."' ";
        }

        //返却種別(完全一致)-------------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_repaymentType'])) and ($this->request->data['MstRepayment']['search_repaymentType'] != "")){
            $where .= ' and TrnRepayHeader.repay_type = ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_repaymentType'])."' ";
        }

        //管理区分(完全一致)-------------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_type'])) and ($this->request->data['MstRepayment']['search_type'] != "")){
            $where .= ' and TrnSticker.trade_type = ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_type'])."' ";
        }

        //取消は表示しない-------------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_display_deleted'])) and ($this->request->data['MstRepayment']['search_display_deleted'] == "1")){
            $where .= ' and TrnRepayHeader.is_deleted = FALSE ';
            $where .= ' and TrnReceiving.is_deleted = FALSE ';
        } else {
            $where .= ' and (TrnRepayHeader.is_deleted = FALSE  or TrnRepayHeader.is_deleted = TRUE) ';
            $where .= ' and (TrnReceiving.is_deleted = FALSE  or TrnReceiving.is_deleted = TRUE) ';
        }

        //作業区分(完全一致)-------------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_classId'])) and ($this->request->data['MstRepayment']['search_classId'] != "")){
            $where .= ' and TrnReceiving.work_type = ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_classId'])."' ";
        }

        //商品名(LIKE検索)-------------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_item_name'])) and ($this->request->data['MstRepayment']['search_item_name'] != "")){
            $where .= ' and MstFacilityItem.item_name LIKE ' ."'%".Sanitize::escape($this->request->data['MstRepayment']['search_item_name'])."%' ";
        }

        //商品ID(完全一致)------------------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_internal_code'])) and ($this->request->data['MstRepayment']['search_internal_code'] != "")){
            $where .= ' and MstFacilityItem.internal_code = ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_internal_code'])."' ";
        }

        //規格(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_standard'])) and ($this->request->data['MstRepayment']['search_standard'] != "")){
            $where .= ' and MstFacilityItem.standard LIKE ' ."'%".Sanitize::escape($this->request->data['MstRepayment']['search_standard'])."%' ";
        }

        //製品番号(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_item_code'])) and ($this->request->data['MstRepayment']['search_item_code'] != "")){
            $where .= ' and MstFacilityItem.item_code LIKE ' ."'%".Sanitize::escape($this->request->data['MstRepayment']['search_item_code'])."%' ";
        }

        //販売元(LIKE検索)------------------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_dealer_name'])) and ($this->request->data['MstRepayment']['search_dealer_name'] != "")){
            $where .= ' and MstDealer.dealer_name LIKE ' ."'%".Sanitize::escape($this->request->data['MstRepayment']['search_dealer_name'])."%' ";
        }

        //シール番号(完全一致)---------------------------------------------------
        //部署シールとセンターシールのOR検索
        if((isset($this->request->data['MstRepayment']['search_sticker_no'])) and ($this->request->data['MstRepayment']['search_sticker_no'] != "")){
            $where .= ' and (TrnSticker.facility_sticker_no ILIKE ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_sticker_no'])."' ";
            $where .= ' or TrnSticker.hospital_sticker_no ILIKE ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_sticker_no'])."'";
            $where .= ' or TrnReceiving.facility_sticker_no ILIKE ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_sticker_no'])."'";
            $where .= ' or TrnReceiving.hospital_sticker_no ILIKE ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_sticker_no'])."') ";
        }

        //ロット番号(完全一致)---------------------------------------------------
        if((isset($this->request->data['MstRepayment']['search_lot_no'])) and ($this->request->data['MstRepayment']['search_lot_no'] != "")){
            $where .= ' and TrnSticker.lot_no = ' ."'".Sanitize::escape($this->request->data['MstRepayment']['search_lot_no'])."' ";
        }

        if($f_id != null){
            $where .= ' and MstFacilityItem.mst_facility_id = ' .$f_id;
        }
        return $where;
    }

    /**
     * 返却履歴CSV出力
     */
    function export_csv() {
        $this->request->data['Returns']['id']=null;

        $sql = $this->_getRepayCSV();
        $this->db_export_csv($sql , "返却履歴", '/repayments_history/index');
    }

    private function _getRepayCSV(){
        $where = $this->createSearch_Where();
        //明細表示用

        $sql  = ' select ';
        $sql .= '       trnrepayheader.work_no               as 返却番号 ';
        $sql .= "     , to_char(trnrepayheader.work_date,'YYYY/mm/dd') as 返却日 ";
        $sql .= '     , mstfacility.facility_name            as 施設名';
        $sql .= '     , mstdepartment.department_name        as 部署名';
        $sql .= '     , orderfacility.facility_name          as 仕入先名 ';
        $sql .= '     , mstfacilityitem.internal_code        as 商品ID ';

        $sql .= '      , ( case trnsticker.trade_type ';
        foreach(Configure::read('Classes') as $key => $val){
            $sql .= "           when " . $key . " then '" . $val ."' ";
        }
        $sql .= "        else '' ";
        $sql .= '        end )                               as 管理区分名';
        $sql .= '     , mstfacilityitem.item_name            as 商品名 ';
        $sql .= '     , mstfacilityitem.standard             as 規格 ';
        $sql .= '     , mstfacilityitem.item_code            as 製品番号 ';
        $sql .= '     , mstdealer.dealer_name                as 販売元名 ';

        $sql .= '     , ( case when mstitemunit.per_unit = 1 then mstunitname.unit_name ';
        $sql .= "              else mstunitname.unit_name || '(' || mstitemunit.per_unit || mstperunitname.unit_name || ')' ";
        $sql .= '        end )                               as 包装単位';
        $sql .= '     , trnreceiving.quantity                as 数量 ';
        $sql .= '     , trnsticker.lot_no                    as ロット番号 ';
        $sql .= "     , to_char(trnsticker.validated_date,'YYYY/mm/dd') as 有効期限 ";

        $sql .= '     , coalesce(  ';
        $sql .= '       trnreceiving.facility_sticker_no ';
        $sql .= '       , trnsticker.facility_sticker_no ';
        $sql .= '     )                                      as センターシール';
        $sql .= '     , ( case  ';
        $sql .= "       when trnreceiving.hospital_sticker_no = ' '  ";
        $sql .= '       then trnsticker.hospital_sticker_no  ';
        $sql .= '       else trnreceiving.hospital_sticker_no  ';
        $sql .= '       end )                                as 部署シール ';
        $sql .= '     , mstclass.name                        as 作業区分名';
        $sql .= '     , mstuser.user_name                    as 更新者名 ';
        $sql .= '     , trnreceiving.recital                 as 備考 ';
        $sql .= '   from ';
        $sql .= '     trn_repay_headers as trnrepayheader  ';
        $sql .= '     inner join trn_receivings as trnreceiving  ';
        $sql .= '       on trnreceiving.trn_repay_header_id = trnrepayheader.id  ';
        $sql .= '     inner join trn_stickers as trnsticker  ';
        $sql .= '       on trnreceiving.trn_sticker_id = trnsticker.id  ';
        $sql .= '     inner join trn_claims as trnclaim  ';
        $sql .= '       on trnreceiving.id = trnclaim.trn_receiving_id  ';
        $sql .= '     inner join mst_item_units as mstitemunit  ';
        $sql .= '       on trnreceiving.mst_item_unit_id = mstitemunit.id  ';
        $sql .= '     inner join mst_users as mstuser  ';
        $sql .= '       on trnrepayheader.creater = mstuser.id  ';
        $sql .= '     inner join mst_facility_items as mstfacilityitem  ';
        $sql .= '       on mstitemunit.mst_facility_item_id = mstfacilityitem.id  ';
        $sql .= '     left join mst_dealers as mstdealer  ';
        $sql .= '       on mstfacilityitem.mst_dealer_id = mstdealer.id  ';
        $sql .= '     inner join mst_unit_names as mstunitname  ';
        $sql .= '       on mstitemunit.mst_unit_name_id = mstunitname.id  ';
        $sql .= '     inner join mst_unit_names as mstperunitname  ';
        $sql .= '       on mstitemunit.per_unit_name_id = mstperunitname.id  ';
        $sql .= '     inner join mst_departments as mstdepartment  ';
        $sql .= '       on trnrepayheader.department_id_from = mstdepartment.id  ';
        $sql .= '     inner join mst_facilities as mstfacility  ';
        $sql .= '       on mstdepartment.mst_facility_id = mstfacility.id  ';
        $sql .= '     left join trn_shippings as trnshipping  ';
        $sql .= '       on trnsticker.trn_shipping_id = trnshipping.id  ';
        $sql .= '     left join trn_receivings as returnreceiving  ';
        $sql .= '       on returnreceiving.id = trnsticker.trn_return_receiving_id  ';
        $sql .= '     left join trn_orders as trnorder  ';
        $sql .= '       on trnsticker.trn_order_id = trnorder.id  ';
        $sql .= '     left join mst_departments as orderdepartment  ';
        $sql .= '       on trnorder.department_id_to = orderdepartment.id  ';
        $sql .= '     left join mst_facilities as orderfacility  ';
        $sql .= '       on orderdepartment.mst_facility_id = orderfacility.id  ';
        $sql .= '     left join mst_classes as mstclass  ';
        $sql .= '       on mstclass.id = returnreceiving.work_type  ';
        $sql .= '   where ';

        //帳票印刷とSQLを共用しているため、施設・部署はここで追加
        //施設(完全一致)---------------------------------------------------
        if((isset($this->request->data['user_customer'])) and ($this->request->data['user_customer'] != "")){
            $_customer = $this->MstFacility->find('first',array('fields'=>'id','conditions'=>array('facility_code'=>$this->request->data['user_customer'] , 'MstFacility.facility_type'=> array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')))));
            $where .= '     and MstFacility.id = ' ."'".Sanitize::escape($_customer['MstFacility']['id'])."' ";
        }

        //部署(完全一致)---------------------------------------------------
        if((isset($this->request->data['MstRepayment']['user_department'])) and ($this->request->data['MstRepayment']['user_department'] != "")){
            $_fromDepartment = $this->MstDepartment->find('first',array('fields'=>'id','conditions'=>array('department_code'=>$this->request->data['MstRepayment']['user_department']
                                                                                                           ,'mst_facility_id'=>$_customer['MstFacility']['id']   )));
            $_customer = $this->MstFacility->find('first',array('fields'=>'id','conditions'=>array('MstFacility.facility_code'=>$this->request->data['user_customer'] , 'MstFacility.facility_type'=> array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')))));
            $where .= '     and TrnRepayHeader.department_id_from = ' ."'".Sanitize::escape($_fromDepartment['MstDepartment']['id'])."' ";
            $where .= '     and MstFacility.id = ' ."'".Sanitize::escape($_customer['MstFacility']['id'])."' ";
        }

        //ヘッダIDが空(CSV出力)のため、施設の条件を追加
        $where .= '     and MstFacilityItem.mst_facility_id = ' .$this->Session->read('Auth.facility_id_selected');
        $sql .= '     1 = 1  ' . $where ;

        $sql .= '     and (  ';
        $sql .= '       trnrepayheader.is_deleted = false  ';
        $sql .= '       or trnrepayheader.is_deleted = true ';
        $sql .= '     )  ';
        $sql .= '     and (  ';
        $sql .= '       trnreceiving.is_deleted = false  ';
        $sql .= '       or trnreceiving.is_deleted = true ';
        $sql .= '     )  ';
        $sql .= "     and trnclaim.is_stock_or_sale = '2'  ";
        $sql .= '   group by ';
        $sql .= '     trnrepayheader.id ';
        $sql .= '     , trnrepayheader.work_no ';
        $sql .= '     , trnrepayheader.work_date ';
        $sql .= '     , trnrepayheader.repay_type ';
        $sql .= '     , trnrepayheader.is_deleted ';
        $sql .= '     , trnrepayheader.recital ';
        $sql .= '     , mstfacility.facility_name ';
        $sql .= '     , mstfacility.facility_formal_name ';
        $sql .= '     , mstuser.user_name ';
        $sql .= '     , trnrepayheader.created ';
        $sql .= '     , trnrepayheader.recital ';
        $sql .= '     , trnrepayheader.detail_count ';
        $sql .= '     , mstfacilityitem.id ';
        $sql .= '     , mstfacilityitem.internal_code ';
        $sql .= '     , mstfacilityitem.item_name ';
        $sql .= '     , mstfacilityitem.standard ';
        $sql .= '     , mstfacilityitem.item_code ';
        $sql .= '     , mstfacilityitem.mst_dealer_id ';
        $sql .= '     , mstfacilityitem.tax_type ';
        $sql .= '     , mstfacilityitem.is_lowlevel ';
        $sql .= '     , mstitemunit.id ';
        $sql .= '     , mstitemunit.per_unit ';
        $sql .= '     , mstitemunit.mst_unit_name_id ';
        $sql .= '     , mstunitname.id ';
        $sql .= '     , mstunitname.unit_name ';
        $sql .= '     , mstperunitname.id ';
        $sql .= '     , mstperunitname.unit_name ';
        $sql .= '     , mstdealer.dealer_name ';
        $sql .= '     , trnreceiving.facility_sticker_no ';
        $sql .= '     , trnsticker.facility_sticker_no ';
        $sql .= '     , trnsticker.hospital_sticker_no ';
        $sql .= '     , trnsticker.trade_type ';
        $sql .= '     , trnsticker.lot_no ';
        $sql .= '     , trnsticker.validated_date ';
        $sql .= '     , trnsticker.has_reserved ';
        $sql .= '     , trnsticker.trn_order_id ';
        $sql .= '     , trnsticker.quantity ';
        $sql .= '     , trnsticker.mst_department_id ';
        $sql .= '     , trnsticker.trn_move_header_id ';
        $sql .= '     , trnreceiving.hospital_sticker_no ';
        $sql .= '     , trnreceiving.department_id_to ';
        $sql .= '     , trnreceiving.quantity ';
        $sql .= '     , trnreceiving.recital ';
        $sql .= '     , trnreceiving.stocking_close_type ';
        $sql .= '     , trnreceiving.sales_close_type ';
        $sql .= '     , trnreceiving.facility_close_type ';
        $sql .= '     , trnreceiving.is_deleted ';
        $sql .= '     , trnreceiving.work_type ';
        $sql .= '     , trnreceiving.receiving_status ';
        $sql .= '     , trnreceiving.id ';
        $sql .= '     , trnreceiving.recital ';
        $sql .= '     , trnclaim.claim_date ';
        $sql .= '     , mstdepartment.department_name ';
        $sql .= '     , mstdepartment.department_formal_name ';
        $sql .= '     , trnshipping.move_status ';
        $sql .= '     , trnshipping.trn_move_header_id ';
        $sql .= '     , returnreceiving.receiving_status ';
        $sql .= '     , orderdepartment.department_name ';
        $sql .= '     , orderfacility.id ';
        $sql .= '     , orderfacility.facility_name ';
        $sql .= '     , orderfacility.facility_formal_name ';
        $sql .= '     , orderfacility.round  ';
        $sql .= '     , mstclass.name';
        $sql .= '   order by ';
        $sql .= '     trnrepayheader.work_no ';
        return $sql;
    }


}
