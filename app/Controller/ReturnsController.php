<?php
class ReturnsController extends AppController {
    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    /**
     * @var $name
     */
    var $name = 'Returns';

    /**
     * @var array $uses
     */
    var $uses = array(
        'TrnClaim',
        'TrnReceiving',
        'TrnReturnHeader',
        'TrnSticker',
        'TrnStickerRecord',
        'TrnStorage',
        'TrnStock',
        'TrnOrderHeader',
        'TrnOrder',
        'MstItemUnit',
        'MstClass',
        'MstDealer',
        'MstDepartment',
        'MstFacility',
        'MstFacilityItem',
        'MstFacilityRelation',
        'MstUser'
        );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time',);

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','xml','Barcode','CsvWriteUtils');

    /**
     *
     * @var array errors
     */
    var $errors;

    /**
     * @var array _work_type
     * 作業区分
     */
    var $_work_type = array();

    /**
     * @var array_management_class
     * 管理区分
     */
    var $_management_class = array();

    /**
     * 入荷返品
     * 初期画面表示 & 検索結果表示処理
     *
     */
    function return2receivings(){
        $this->setRoleFunction(30); //入荷済み返品登録

        //返品作業区分
        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));

        //管理区分
        $this->set('managementClass',Configure::read('Return'));

        //検索結果格納変数
        $SearchResult = array();

        //検索実行フラグ
        $SearchFlg = true;

        if( isset($this->request->data['search']['is_search']) ){
            //検索処理
            $options['recursive']  = -1;
            /* JOINテーブル */
            //発注
            $options['joins'][] = array(
                'type'=>'INNER',
                'table'=>'trn_orders',
                'alias'=>'ODR',
                'conditions'=>array(
                    'TrnReceiving.trn_order_id = ODR.id',
                    'ODR.is_deleted = false'
                    )
                );
            //包装単位
            $options['joins'][] = array(
                'type'=>'INNER',
                'table'=>'mst_item_units',
                'alias'=>'IUN',
                'conditions'=>array(
                    'TrnReceiving.mst_item_unit_id = IUN.id',
                    'IUN.is_deleted = false'
                    )
                );
            //施設採用品
            $options['joins'][] = array(
                'type'=>'INNER',
                'table'=>'mst_facility_items',
                'alias'=>'FIT',
                'conditions'=>array(
                    'IUN.mst_facility_item_id = FIT.id',
                    'FIT.is_deleted = false',
                    'FIT.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected')
                    )
                );
            //部署
            $options['joins'][] = array(
                'type'=>'INNER',
                'table'=>'mst_departments',
                'alias'=>'DPT',
                'conditions'=>array(
                    'TrnReceiving.department_id_from = DPT.id',
                    'DPT.is_deleted = false'
                    )
                );
            //施設
            $options['joins'][] = array(
                'type'=>'INNER',
                'table'=>'mst_facilities',
                'alias'=>'FCT',
                'conditions'=>array(
                    'DPT.mst_facility_id = FCT.id',
                    'FCT.is_deleted = false'
                    )
                );
            //取引先マスタ
            $options['joins'][] = array(
                'type'=>'LEFT',
                'table'=>'mst_dealers',
                'alias'=>'DLR',
                'conditions'=>array(
                    'FIT.mst_dealer_id = DLR.id',
                    'DLR.is_deleted = false'
                    )
                );
            /* WHERE句 */
            $options['conditions'][] =array(
                "TrnReceiving.remain_count >" => "0",
                "TrnReceiving.receiving_type " => Configure::read('ReceivingType.arrival'),
                "TrnReceiving.is_deleted" => false
                );
            /* 画面から入力された検索条件 */
            //発注番号(前方一致)
            if( !empty($this->request->data['search']['work_no1']) ){
                $options['conditions'][] = array("ODR.work_no ILIKE " => $this->request->data['search']['work_no1']."%");
            }
            //発注日from
            if( !empty($this->request->data['search']['work_date1']) ){
                $options['conditions'][] = array("ODR.work_date >= " => date("Y-m-d",strtotime($this->request->data['search']['work_date1'])));
            }
            //発注日to
            if( !empty($this->request->data['search']['work_date2']) ){
                $options['conditions'][] = array("ODR.work_date < " => date('Y/m/d', strtotime('+1 day', strtotime($this->request->data['search']['work_date2']))));
            }
            //入荷番号(前方一致)
            if( !empty($this->request->data['search']['work_no2']) ){
                $options['conditions'][] = array("TrnReceiving.work_no ILIKE " => $this->request->data['search']['work_no2']."%");
            }
            //入荷日from
            if( !empty($this->request->data['search']['work_date3']) ){
                $options['conditions'][] = array("TrnReceiving.work_date >= " => date("Y-m-d",strtotime($this->request->data['search']['work_date3'])));
            }
            //入荷日to
            if( !empty($this->request->data['search']['work_date4']) ){
                $options['conditions'][] = array("TrnReceiving.work_date < " => date('Y/m/d', strtotime('+1 day', strtotime($this->request->data['search']['work_date4']))));
            }
            //商品ID(前方一致)
            if( !empty($this->request->data['search']['item_id']) ){
                $options['conditions'][] = array("FIT.internal_code ILIKE " => $this->request->data['search']['item_id']."%");
            }
            //製品番号(前方一致)
            if( !empty($this->request->data['search']['item_code']) ){
                $options['conditions'][] = array("FIT.item_code ILIKE " => $this->request->data['search']['item_code']."%");
            }
            //商品名(部分一致)
            if( !empty($this->request->data['search']['item_name']) ){
                $options['conditions'][] = array("FIT.item_name ILIKE " => "%".$this->request->data['search']['item_name']."%");
            }
            //規格(部分一致)
            if( !empty($this->request->data['search']['standard']) ){
                $options['conditions'][] = array("FIT.standard ILIKE " => "%".$this->request->data['search']['standard']."%");
            }
            //販売元(部分一致)
            if( !empty($this->request->data['search']['dealer_name']) ){
                $options['conditions'][] = array("DLR.dealer_name ILIKE " => "%".$this->request->data['search']['dealer_name']."%");
            }
            //JANコード(前方一致)
            if( !empty($this->request->data['search']['jan_code']) ){
                $options['conditions'][] = array("FIT.jan_code ILIKE " => $this->request->data['search']['jan_code']."%");
            }
            //作業区分(完全一致)
            if( !empty($this->request->data['search']['work_type']) ){
                $options['conditions'][] = array("TrnReceiving.work_type" => $this->request->data['search']['work_type']);
            }
            //管理区分(完全一致)
            if( !empty($this->request->data['search']['management_class']) ){
                //発注テーブルの準定数品はorder_typeが５なので注意！
                if($this->request->data['search']['management_class'] == Configure::read('TeisuType.Constant')){
                    $this->request->data['search']['management_class'] = Configure::read('OrderTypes.nomal');
                }else if($this->request->data['search']['management_class'] == Configure::read('TeisuType.SemiConstant')){
                    $this->request->data['search']['management_class'] = Configure::read('OrderTypes.semi_nomal');
                }
                $options['conditions'][] = array("ODR.order_type" => $this->request->data['search']['management_class']);
            }

            /* ORDER BY句 */
            $options['order'] = array('TrnReceiving.work_no','TrnReceiving.work_seq','ODR.work_date');

            //カートで持ち越している商品の検索実行
            if(!empty($this->request->data['search']['tempId'])){
                $tempId = substr($this->request->data['search']['tempId'],0,strlen($this->request->data['search']['tempId']) - 1);
                $arrId = explode(",",$tempId);
                $options['conditions'][] = array('not'=> array("TrnReceiving.id" => $arrId));
            }


            //全件数取得
            $this->set('max' , $this->TrnReceiving->find('count', $options));
            //出力カラム
            $options['fields'] = array(
                'TrnReceiving.id',
                'TrnReceiving.work_no',
                'TrnReceiving.work_date',
                'TrnReceiving.remain_count',
                'TrnReceiving.stocking_price',
                'ODR.work_no',
                'ODR.work_seq',
                'ODR.work_date',
                'FIT.internal_code',
                'FIT.item_name',
                'FIT.standard',
                'FIT.item_code',
                'FIT.is_lowlevel',
                'FCT.facility_name',
                'DLR.dealer_name',
                );


            /* LIMIT */
            $options['limit'] = $this->request->data['Return']['limit'];

            //入荷検索実行
            $this->set('SearchResult', AppController::outputFilter($this->TrnReceiving->find('all', $options)));
        }else{
            $SearchFlg = false;

            //初期表示用データ
            $this->request->data['search']['work_date3']  = date("Y/m/d", strtotime("-1 week"));
            $this->request->data['search']['work_date4']  = date("Y/m/d");
        }

        //カートで持ち越している商品の検索実行
        if(!empty($this->request->data['search']['tempId'])){
            $tempId = substr($this->request->data['search']['tempId'],0,strlen($this->request->data['search']['tempId']) - 1);
            $arrId = explode(",",$tempId);
            $this->set('CartSearchResult', AppController::outputFilter($this->search_items($arrId)));
        }

        $this->set("data",$this->request->data);
        $this->set("SearchFlg",$SearchFlg);
    }

    /**
     * 入荷返品
     * 確認画面表示処理
     *
     */
    function return2receivings_confirm(){
        //返品作業区分
        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));

        //管理区分
        $managementClass = Configure::read('Return');

        //検索結果格納配列
        $SearchResult = array();
        //検索条件格納配列
        $conditionsArray = array();

        if( count($this->request->data['SelectedTrnReceivingID']) != 0 ){
            //入荷検索実行
            $SearchResult = $this->search_items($this->request->data['SelectedTrnReceivingID']);
            $SearchResult = AppController::outputFilter($SearchResult);

            //検索結果から施設採用品IDで商品に紐付く仕入先リストを取得
            $traderList = array();
            $traderList = $this->getTraderList();

            $this->set("traderList",$traderList);
            $this->set('data', $this->request->data);
            $this->set('SearchResult', $SearchResult);

            // 2度押し対策用にトランザクショントークンを作る
            mt_srand((double)microtime()*1000000);
            $token = md5((string)mt_rand());
            $this->Session->write('TrnReturn.token' , $token);
            $this->request->data['TrnReturn']['token'] = $token;
        }
    }

    /**
     * 入荷返品
     * 選択商品検索処理
     *
     */
    function search_items($condition){
        $SearchResult = array();
        $options['recursive']  = -1;
        $options['fields'] = array(
            'TrnReceiving.id',
            'TrnReceiving.work_no',
            'TrnReceiving.work_date',
            'TrnReceiving.work_type',
            'TrnReceiving.department_id_from',
            'TrnReceiving.remain_count',
            'TrnReceiving.stocking_price',
            'TrnReceiving.modified',
            'TrnReceiving.is_retroactable',
            'TrnReceiving.retroact_execution_flag',
            'ODR.work_no',
            'ODR.work_seq',
            'ODR.work_date',
            'FIT.id',
            'FIT.internal_code',
            'FIT.item_name',
            'FIT.standard',
            'FIT.item_code',
            'FIT.is_lowlevel',
            'FCT.facility_name',
            'DLR.dealer_name',
            'STC.quantity',
            'STC.modified'
            );
        /* JOINテーブル */
        //発注
        $options['joins'][] = array('type'=>'LEFT',
                                    'table'=>'trn_orders',
                                    'alias'=>'ODR',
                                    'conditions'=>'TrnReceiving.trn_order_id = ODR.id');
        //包装単位
        $options['joins'][] = array('type'=>'INNER',
                                    'table'=>'mst_item_units',
                                    'alias'=>'IUN',
                                    'conditions'=>'TrnReceiving.mst_item_unit_id = IUN.id');
        //施設採用品
        $options['joins'][] = array('type'=>'INNER',
                                    'table'=>'mst_facility_items',
                                    'alias'=>'FIT',
                                    'conditions'=>'IUN.mst_facility_item_id = FIT.id');
        //部署FROM
        $options['joins'][] = array('type'=>'INNER',
                                    'table'=>'mst_departments',
                                    'alias'=>'DPT',
                                    'conditions'=>'TrnReceiving.department_id_from = DPT.id');
        //施設FROM
        $options['joins'][] = array('type'=>'INNER',
                                    'table'=>'mst_facilities',
                                    'alias'=>'FCT',
                                    'conditions'=>'DPT.mst_facility_id = FCT.id');
        //部署TO
        $options['joins'][] = array('type'=>'INNER',
                                    'table'=>'mst_departments',
                                    'alias'=>'DPMT',
                                    'conditions'=>'TrnReceiving.department_id_to = DPMT.id');
        //施設TO
        $options['joins'][] = array('type'=>'INNER',
                                    'table'=>'mst_facilities',
                                    'alias'=>'FCLT',
                                    'conditions'=>array(
                                        'DPMT.mst_facility_id = FCLT.id','FCLT.id = '.$this->Session->read('Auth.facility_id_selected')
                                        ));
        //取引先マスタ
        $options['joins'][] = array('type'=>'LEFT',
                                    'table'=>'mst_dealers',
                                    'alias'=>'DLR',
                                    'conditions'=>'FIT.mst_dealer_id = DLR.id');
        //シール
        $options['joins'][] = array('type'=>'INNER',
                                    'table'=>'trn_stickers',
                                    'alias'=>'STC',
                                    'conditions'=>'TrnReceiving.trn_sticker_id = STC.id');
        /* WHERE句 */
        $options['conditions'] = array("TrnReceiving.id" => $condition);

        /* ORDER BY句 */
        $options['order'] = array('TrnReceiving.work_no','TrnReceiving.work_seq');

        //入荷検索実行
        $this->TrnReceiving->create();

        return $this->TrnReceiving->find('all', $options);
    }

    /**
     * 入荷返品
     * 返品登録処理
     *
     */
    function return2receivings_regist(){
        //返品作業区分
        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));

        //条件格納配列
        $conditionsArray = array();
        //システム時間設定
        $date = date("Y/m/d");
        $now = date('Y/m/d H:i:s.u');
        //結果表示画面に渡す配列
        $RegistResultView = array();
        //etc
        $msg = "";
        $errorFlg = false;
        $isChecked = array();

        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['TrnReturn']['token'] == $this->Session->read('TrnReturn.token')) {
            $this->Session->delete('TrnReturn.token');

            if( count($this->request->data['RegistTrnReceiving']['id']) != 0 ){

                //トランザクション開始
                $this->TrnReceiving->begin();

                //行ロック
                $this->TrnReceiving->query(' select * from trn_receivings as a where a.id in (' . join(',',$this->request->data['RegistTrnReceiving']['id']) . ') for update ');

                //更新チェック
                $ret = $this->TrnReceiving->query(" select count(*) from trn_receivings as a where a.id in (" . join(',',$this->request->data['RegistTrnReceiving']['id']) . ") and a.modified > '" . $this->request->data['TrnReturn']['time'] . "'");
                if( $ret[0][0]['count'] > 0 ){
                    $errorFlg = true;
                }

                for($i=0; $i<count($this->request->data['RegistTrnReceiving']['id']); $i++){
                    if($this->request->data['RegistTrnReceiving']['id'][$i] != "0"){
                        $SearchResult = array();
                        //データを詰め替えなおし
                        $RegistResultView['result']['id'][]                 = $this->request->data['RegistTrnReceiving']['id'][$i];
                        $RegistResultView['result']['OrderWorkNo'][]        = $this->request->data['RegistTrnReceiving']['OrderWorkNo'][$i];
                        $RegistResultView['result']['TrnReceivingWorkNo'][] = $this->request->data['RegistTrnReceiving']['TrnReceivingWorkNo'][$i];
                        $RegistResultView['result']['InternalCode'][]       = $this->request->data['RegistTrnReceiving']['InternalCode'][$i];
                        $RegistResultView['result']['ItemName'][]           = $this->request->data['RegistTrnReceiving']['ItemName'][$i];
                        $RegistResultView['result']['ItemCode'][]           = $this->request->data['RegistTrnReceiving']['ItemCode'][$i];
                        $RegistResultView['result']['RemainCount'][]        = $this->request->data['RegistTrnReceiving']['RemainCount'][$i];
                        $RegistResultView['result']['LotNo'][]              = $this->request->data['RegistTrnReceiving']['Lot_no'][$i];
                        $RegistResultView['result']['ReturnCounts'][]       = $this->request->data['RegistTrnReceiving']['Return_counts'][$i];
                        //作業区分名取得
                        if(!empty($this->request->data['RegistTrnReceiving']['work_type'][$i])){
                            $classesName = $this->MstClass->find('all',array(
                                'conditions'=>array('MstClass.id' => $this->request->data['RegistTrnReceiving']['work_type'][$i]
                                                    )));
                            $RegistResultView['result']['work_type'][] = $classesName[0]['MstClass']['name'];
                        }else{
                            $RegistResultView['result']['work_type'][] = "";
                        }
                        $RegistResultView['result']['FacilityName'][]  = $this->request->data['RegistTrnReceiving']['FacilityName'][$i];
                        $RegistResultView['result']['Standard'][]      = $this->request->data['RegistTrnReceiving']['Standard'][$i];
                        $RegistResultView['result']['DealerName'][]    = $this->request->data['RegistTrnReceiving']['DealerName'][$i];
                        $RegistResultView['result']['StockingPrice'][] = $this->request->data['RegistTrnReceiving']['StockingPrice'][$i];
                        $RegistResultView['result']['ExpiryDate'][]    = $this->request->data['RegistTrnReceiving']['Expiry_date'][$i];
                        $RegistResultView['result']['Remarks'][]       = $this->request->data['RegistTrnReceiving']['Remarks'][$i];
                        if($this->request->data['RegistTrnReceiving']['is_retroactable'][$i] == "0"){
                            $RegistResultView['result']['is_retroactable'][] = "";
                        }else{
                            $RegistResultView['result']['is_retroactable'][] = "○";
                        }
                        $RegistResultView = AppController::outputFilter($RegistResultView);

                        //情報取得
                        $this->TrnReceiving->create();
                        $options = array();
                        $options['fields'] = array(
                            'TrnReceiving.id',
                            'TrnReceiving.work_no',
                            'TrnReceiving.work_seq',
                            'TrnReceiving.work_date',
                            'TrnReceiving.work_type',
                            'TrnReceiving.recital',
                            'TrnReceiving.receiving_type',
                            'TrnReceiving.receiving_status',
                            'TrnReceiving.mst_item_unit_id',
                            'TrnReceiving.department_id_from',
                            'TrnReceiving.department_id_to',
                            'TrnReceiving.quantity',
                            'TrnReceiving.remain_count',
                            'TrnReceiving.stocking_price',
                            'TrnReceiving.sales_price',
                            'TrnReceiving.set_facility_item_id',
                            'TrnReceiving.trn_sticker_id',
                            'TrnReceiving.trn_shipping_id',
                            'TrnReceiving.trn_order_id',
                            'TrnReceiving.stocking_close_type',
                            'TrnReceiving.sales_close_type',
                            'TrnReceiving.facility_close_type',
                            'TrnReceiving.trn_receiving_header_id',
                            'TrnReceiving.trn_move_header_id',
                            'TrnReceiving.trn_return_header_id',
                            'TrnReceiving.hospital_sticker_no',
                            'TrnReceiving.is_retroactable',
                            'TrnReceiving.is_deleted',
                            'TrnReceiving.trn_repay_header_id',
                            'TrnReceiving.modified',
                            'STC.id',
                            'STC.facility_sticker_no',
                            'STC.hospital_sticker_no',
                            'STC.mst_item_unit_id',
                            'STC.mst_department_id',
                            'STC.trade_type',
                            'STC.quantity',
                            'STC.lot_no',
                            'STC.validated_date',
                            'STC.original_price',
                            'STC.transaction_price',
                            'STC.last_move_date',
                            'STC.trn_set_stock_id',
                            'STC.trn_consume_id',
                            'STC.trn_promise_id',
                            'STC.trn_order_id',
                            'STC.trn_receiving_id',
                            'STC.trn_storage_id',
                            'STC.trn_shipping_id',
                            'STC.receipt_id',
                            'STC.buy_claim_id',
                            'STC.sale_claim_id',
                            'STC.is_deleted',
                            'STC.sales_price',
                            'STC.has_reserved',
                            'STC.trn_return_header_id',
                            'STC.trn_move_header_id',
                            'STC.trn_fixed_promise_id',
                            'STC.trn_return_receiving_id',
                            'STC.modified',
                            'FCI.is_lowlevel',
                            'STOC.id',
                            'STOC.receipted_count',
                            'STOC.reserve_count',
                            'ITU.per_unit'
                            );
                        //シールテーブル結合
                        //2011/01/07 mod  シールの数量を足しこんでいく際に入荷キーを最新のものに更新していないため、
                        //シールが持っている入荷IDではなく、入荷データのシールIDで紐付ける
                        $options['joins'][] = array(
                            'type'=>'INNER',
                            'table'=>'trn_stickers',
                            'alias'=>'STC',
                            'conditions'=>array(
                                'TrnReceiving.trn_sticker_id = STC.id',
                                'STC.is_deleted = false'
                                )
                            );

                        //包装単位結合
                        $options['joins'][] = array(
                            "type"=>"inner",
                            "table"=>"mst_item_units",
                            "alias"=>"ITU",
                            "conditions"=>array(
                                "TrnReceiving.mst_item_unit_id = ITU.id",
                                "ITU.is_deleted = false"
                                )
                            );

                        //施設採用品結合
                        $options['joins'][] = array(
                            "type"=>"inner",
                            "table"=>"mst_facility_items",
                            "alias"=>"FCI",
                            "conditions"=>array(
                                "ITU.mst_facility_item_id = FCI.id",
                                "FCI.is_deleted = false"
                                )
                            );

                        //在庫
                        $options['joins'][] = array(
                            "type"=>"inner",
                            "table"=>"trn_stocks",
                            "alias"=>"STOC",
                            "conditions"=>array(
                                "STOC.mst_item_unit_id = STC.mst_item_unit_id",
                                "STOC.mst_department_id = STC.mst_department_id"
                                )
                            );

                        $options['conditions'] = array("TrnReceiving.id " => $this->request->data['RegistTrnReceiving']['id'][$i]);

                        $this->TrnReceiving->recursive = -1;
                        $SearchResult = $this->TrnReceiving->find('all', $options);

                        //取得情報の更新日時チェック
                        if($SearchResult[0]['TrnReceiving']['modified'] !== $this->request->data['RegistTrnReceiving']['Modified'][$i]){
                            $errorFlg = true;
                            $msg = "別ユーザによって更新されています";
                            $this->return2receivings();
                            break;
                        }

                        //低レベル品対応：シール更新時間チェック
                        if(in_array($SearchResult[0]['STC']['id'], $isChecked)){
                            //配列にIDがない場合、更新時間チェック実施
                            if($SearchResult[0]['STC']['modified'] !== $this->request->data['RegistTrnReceiving']['StickerModified'][$i]){
                                $errorFlg = true;
                                $msg = "別ユーザによって更新されています";
                                $this->return2receivings();
                                break;
                            }
                            //更新チェック済みのシールレコードIDを配列に格納
                            $isChecked[] = $SearchResult[0]['STC']['id'];
                        }

                        //入荷の残数をマイナス
                        $this->TrnReceiving->create();
                        $updatecondtion = array();
                        $updatecondtion = array( "TrnReceiving.id =" => $this->request->data['RegistTrnReceiving']['id'][$i] );
                        $updatefield = array();
                        $updatefield = array(
                            "TrnReceiving.remain_count"  => (integer)$SearchResult[0]['TrnReceiving']['remain_count'] - (integer)$this->request->data['RegistTrnReceiving']['Return_counts'][$i],
                            "TrnReceiving.modifier"      => $this->Session->read('Auth.MstUser.id'),
                            "TrnReceiving.modified"      => "'".$now."'"
                            );
                        $this->TrnReceiving->recursive = -1;
                        $res1 = $this->TrnReceiving->updateAll( $updatefield, $updatecondtion, -1);
                        if(false == $res1){
                            $msg .= "入荷数マイナスエラー";
                            $errorFlg = true;
                        }

                        //シールの数量を減算
                        $this->TrnSticker->create();
                        if((integer)$SearchResult[0]['STC']['quantity'] - (integer)$this->request->data['RegistTrnReceiving']['Return_counts'][$i] < 0){
                            //シール数量が0以下になる場合エラー
                            $msg = "シール数量過剰マイナスエラー";
                            $errorFlg = true;
                        }else{
                            $updatecondtion = array();
                            $updatecondtion = array("TrnSticker.id"=>$SearchResult[0]['STC']['id']);
                            $updatefield = array();

                            if($SearchResult[0]['FCI']['is_lowlevel']){
                                //低レベル品
                                $return_count = (integer)$this->request->data['RegistTrnReceiving']['Return_counts'][$i] * $SearchResult[0]['ITU']['per_unit'];
                            }else{
                                //通常品
                                $return_count = (integer)$this->request->data['RegistTrnReceiving']['Return_counts'][$i];
                            }

                            $updatefield = array(
                                "TrnSticker.quantity" => (integer)$SearchResult[0]['STC']['quantity'] - $return_count,
                                "TrnSticker.modifier" => $this->Session->read('Auth.MstUser.id'),
                                "TrnSticker.modified" => "'".$now."'"
                                );
                            $this->TrnSticker->recursive = -1;
                            $res2 = $this->TrnSticker->updateAll( $updatefield, $updatecondtion,-1 );

                            if(false == $res2){
                                $msg .= "シール数量マイナスエラー";
                                $errorFlg = true;
                            }
                        }

                        //低レベル品の場合引当可能数を減らす。
                        if($SearchResult[0]['FCI']['is_lowlevel']){

                            $updatecondtion = array();
                            $updatecondtion = array("TrnStock.id"=>$SearchResult[0]['STOC']['id']);
                            $updatefield = array();

                            $updatefield = array(
                                "TrnStock.reserve_count" => "reserve_count + " . $return_count,
                                "TrnStock.modifier"      => $this->Session->read('Auth.MstUser.id'),
                                "TrnStock.modified"      => "'".$now."'"
                                );
                            $this->TrnStock->recursive = -1;
                            $res3 = $this->TrnStock->updateAll( $updatefield, $updatecondtion,-1 );

                            if(false == $res3){
                                $msg .= "引当可能数マイナスエラー";
                                $errorFlg = true;
                            }
                        }

                        //入荷に返品明細レコード作成
                        $retroactableFlg;
                        if($this->request->data['RegistTrnReceiving']['is_retroactable'][$i] == "0"){
                            $retroactableFlg = "true";
                        }else{
                            $retroactableFlg = "false";
                        }
                        $this->TrnReceiving->create();
                        $res4 = $this->TrnReceiving->save(array(
                            'work_no'                 => '',
                            'work_seq'                => '',
                            'work_date'               => '',
                            'work_type'               => $this->request->data['RegistTrnReceiving']['work_type'][$i],
                            'recital'                 => $this->request->data['RegistTrnReceiving']['Remarks'][$i],
                            'receiving_type'          => Configure::read('ReceivingType.returnReceiving'),
                            'receiving_status'        => Configure::read('ReceivingStatus.arrival'),
                            'mst_item_unit_id'        => $SearchResult[0]['TrnReceiving']['mst_item_unit_id'],
                            'department_id_from'      => $SearchResult[0]['TrnReceiving']['department_id_to'],
                            'department_id_to'        => $this->request->data['RegistTrnReceiving']['DepartmentId'][$i],
                            'quantity'                => $this->request->data['RegistTrnReceiving']['Return_counts'][$i],
                            'remain_count'            => $this->request->data['RegistTrnReceiving']['Return_counts'][$i],
                            'stocking_price'          => $this->request->data['RegistTrnReceiving']['StockingPrice'][$i],
                            'sales_price'             => $SearchResult[0]['TrnReceiving']['sales_price'],
                            'set_facility_item_id'    => $SearchResult[0]['TrnReceiving']['set_facility_item_id'],
                            'trn_sticker_id'          => "",
                            'trn_shipping_id'         => $SearchResult[0]['TrnReceiving']['trn_shipping_id'],
                            'trn_order_id'            => $SearchResult[0]['TrnReceiving']['trn_order_id'],
                            'stocking_close_type'     => Configure::read("StockingCloseType.none"),
                            'sales_close_type'        => Configure::read("SalesCloseType.none"),
                            'facility_close_type'     => Configure::read("FacilityCloseType.none"),
                            'trn_receiving_header_id' => $SearchResult[0]['TrnReceiving']['trn_receiving_header_id'],
                            'trn_move_header_id'      => $SearchResult[0]['TrnReceiving']['trn_move_header_id'],
                            'hospital_sticker_no'     => $SearchResult[0]['TrnReceiving']['hospital_sticker_no'],
                            'is_retroactable'         => $retroactableFlg,
                            'is_deleted'              => false,
                            'creater'                 => $this->Session->read('Auth.MstUser.id'),
                            'modifier'                => $this->Session->read('Auth.MstUser.id'),
                            'created'                 => $now,
                            'modified'                => $now,
                            'trn_repay_header_id'     => $SearchResult[0]['TrnReceiving']['trn_repay_header_id']
                            ));
                        if(false == $res4){
                            $msg .= "入荷返品明細作成エラー";
                            $errorFlg = true;
                        }

                        //作成した返品明細レコードのid取得
                        $TrnReceivingId = $this->TrnReceiving->getLastInsertID();

                        //返品明細に紐付くシールレコード作成
                        $lot_no = " ";
                        if(!empty($this->request->data['RegistTrnReceiving']['Lot_no'][$i])){
                            $lot_no = $this->request->data['RegistTrnReceiving']['Lot_no'][$i];
                        }

                        $this->TrnSticker->create();
                        $res5 = $this->TrnSticker->save(array(
                            'facility_sticker_no'     => $SearchResult[0]['STC']['facility_sticker_no'],
                            'hospital_sticker_no'     => $SearchResult[0]['STC']['hospital_sticker_no'],
                            'mst_item_unit_id'        => $SearchResult[0]['STC']['mst_item_unit_id'],
                            'mst_department_id'       => $SearchResult[0]['TrnReceiving']['department_id_to'],
                            'trade_type'              => $SearchResult[0]['STC']['trade_type'],
                            'quantity'                => (integer)$this->request->data['RegistTrnReceiving']['Return_counts'][$i],
                            'lot_no'                  => $lot_no,
                            'validated_date'          => $this->request->data['RegistTrnReceiving']['Expiry_date'][$i],
                            'original_price'          => $SearchResult[0]['STC']['original_price'],
                            'transaction_price'       => $this->request->data['RegistTrnReceiving']['StockingPrice'][$i],
                            'trn_set_stock_id'        => $SearchResult[0]['STC']['trn_set_stock_id'],
                            'trn_consume_id'          => $SearchResult[0]['STC']['trn_consume_id'],
                            'trn_promise_id'          => $SearchResult[0]['STC']['trn_promise_id'],
                            'trn_order_id'            => $SearchResult[0]['STC']['trn_order_id'],
                            'trn_receiving_id'        => $SearchResult[0]['STC']['trn_receiving_id'],
                            'trn_storage_id'          => $SearchResult[0]['STC']['trn_storage_id'],
                            'trn_shipping_id'         => $SearchResult[0]['STC']['trn_shipping_id'],
                            'receipt_id'              => $SearchResult[0]['STC']['receipt_id'],
                            'buy_claim_id'            => $SearchResult[0]['STC']['buy_claim_id'],
                            'sale_claim_id'           => $SearchResult[0]['STC']['sale_claim_id'],
                            'is_deleted'              => false,
                            'creater'                 => $this->Session->read('Auth.MstUser.id'),
                            'modifier'                => $this->Session->read('Auth.MstUser.id'),
                            'created'                 => $now,
                            'modified'                => $now,
                            'sales_price'             => $SearchResult[0]['STC']['sales_price'],
                            'has_reserved'            => true,
                            'trn_return_header_id'    => $SearchResult[0]['STC']['trn_return_header_id'],
                            'trn_move_header_id'      => $SearchResult[0]['STC']['trn_move_header_id'],
                            'trn_fixed_promise_id'    => $SearchResult[0]['STC']['trn_fixed_promise_id'],
                            'trn_return_receiving_id' => $TrnReceivingId,
                            'trn_adjust_id'           => $this->request->data['RegistTrnReceiving']['id'][$i]
                            ));

                        if(false == $res5){
                            $errorFlg = true;
                            $msg .= "シール作成エラー";
                        }

                        //作成したシールレコードのID取得
                        $TrnStickerId = $this->TrnSticker->getLastInsertID();

                        //シールレコードのIDを返品明細レコードへ登録
                        $this->TrnReceiving->create();
                        $updatecondtion = array();
                        $updatecondtion = array( "TrnReceiving.id =" => $TrnReceivingId );
                        $updatefield = array();
                        $updatefield = array(  "TrnReceiving.trn_sticker_id" => $TrnStickerId
                                              ,"TrnReceiving.modifier" => $this->Session->read('Auth.MstUser.id')
                                              ,"TrnReceiving.modified" => "'".$now."'"
                                              );
                        $this->TrnReceiving->recursive = -1;
                        $res6 = $this->TrnReceiving->updateAll( $updatefield, $updatecondtion );
                        if(false == $res6){
                            $errorFlg = true;
                            $msg .= "シールID登録エラー";
                        }
                    }
                }//end of for


                //コミット直前に更新チェック
                $ret = $this->TrnReceiving->query(" select count(*) from trn_receivings as a where a.id in (" . join(',',$this->request->data['RegistTrnReceiving']['id']) . ") and a.modified > '" . $this->request->data['TrnReturn']['time'] . "' and a.modified <> '" . $now . "'");
                if( $ret[0][0]['count'] > 0 ){
                    $errorFlg = true;
                }

                if($errorFlg){
                    $this->TrnReceiving->rollback();
                    $this->Session->setFlash($msg, 'growl', array('type'=>'error') );
                    $this->redirect('/returns/return2receivings');
                    return;
                }else{
                    $this->TrnReceiving->commit();
                    $this->Session->setFlash("入荷済み返品予約登録が完了しました", 'growl', array('type'=>'star') );
                }
            }

            //結果表示用画面へ遷移
            $this->request->data['RegistResultView'] = $RegistResultView;
            $this->set('RegistResultView' ,$RegistResultView);
            $this->set('data', $this->request->data);
            $this->Session->write('data', $this->request->data);
        }else{

            $this->request->data = $this->Session->read('data');
            $this->set('data' , $this->request->data);
            $this->set('RegistResultView' ,$this->request->data['RegistResultView']);
        }
        $this->render('return2receivings_result');
    }

    /**
     * 入庫返品
     * 返品登録画面初期表示
     *
     */
    function return2storages(){
        $this->setRoleFunction(31); //入庫済み返品登録
        //返品作業区分
        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));
    }

    /**
     * 入庫返品
     * 登録(読込)画面初期表示
     *
     */
    function return2storages_reader(){
        
    }

    /**
     * 入庫返品
     * 在庫検索画面初期表示＆検索結果
     *
     */
    function return2storages_search(){
        $SearchFlg = false;
        if( isset($this->request->data['search']['is_search']) ){
            $options = array(); //初期化

            /* 画面から入力された検索条件 */
            //商品ID(前方一致)
            if( !empty($this->request->data['search']['item_id']) ){
                $options['conditions'][] = array("FIT.internal_code ILIKE " => $this->request->data['search']['item_id']."%");
            }
            //製品番号(前方一致)
            if( !empty($this->request->data['search']['item_code']) ){
                $options['conditions'][] = array("FIT.item_code ILIKE " => "%".$this->request->data['search']['item_code']."%");
            }
            //商品名(部分一致)
            if( !empty($this->request->data['search']['item_name']) ){
                $options['conditions'][] = array("FIT.item_name ILIKE " => "%".$this->request->data['search']['item_name']."%");
            }
            //規格(部分一致)
            if( !empty($this->request->data['search']['standard']) ){
                $options['conditions'][] = array("FIT.standard ILIKE " => "%".$this->request->data['search']['standard']."%");
            }
            //販売元(部分一致)
            if( !empty($this->request->data['search']['dealer_name']) ){
                $options['conditions'][] = array("DLR.dealer_name ILIKE " => "%".$this->request->data['search']['dealer_name']."%");
            }
            //JANコード(前方一致)
            if( !empty($this->request->data['search']['jan_code']) ){
                $options['conditions'][] = array("FIT.jan_code ILIKE " => $this->request->data['search']['jan_code']."%");
            }
            //作業区分
            if( !empty($this->request->data['search']['classId']) ){
                $options['conditions'][] = array("STG.work_type" => $this->request->data['search']['classId']);
            }
            //シール番号
            if( !empty($this->request->data['search']['trn_stickers_id']) ){
                $options['conditions'][] = array("TrnSticker.facility_sticker_no ILIKE " => $this->request->data['search']['trn_stickers_id']."%");
            }
            //ロット番号
            if( !empty($this->request->data['search']['lot_no']) ){
                $options['conditions'][] = array("TrnSticker.lot_no ILIKE " => "%".$this->request->data['search']['lot_no']."%");
            }
            //カートに入れたレコードは除外
            if(!empty($this->request->data['search']['tempId'])){
                $tempId = substr($this->request->data['search']['tempId'],0,strlen($this->request->data['search']['tempId']) - 1);
                $arrId = explode(",",$tempId);
                $options = array();
                $options['conditions'][] = array('not' => array("TrnSticker.id" => $arrId));
            }


            //入庫検索実行
            $this->set('SearchResult', AppController::outputFilter($this->SearchStoragesDetail($this->request->data,$options,$this->request->data['Return']['limit'])));
            $SearchFlg = true;
        }

        //カートで持ち越している商品の検索実行
        if(!empty($this->request->data['search']['tempId'])){
            $tempId = substr($this->request->data['search']['tempId'],0,strlen($this->request->data['search']['tempId']) - 1);
            $arrId = explode(",",$tempId);
            $options = array();
            $options['conditions'][] = array("TrnSticker.id" => $arrId);
            $this->set('CartSearchResult', AppController::outputFilter($this->SearchStoragesDetail($this->request->data,$options)));
        }

        $this->set('SearchFlg', $SearchFlg);
        $this->set('data', $this->request->data);
    }

    /**
     * 入庫返品
     * 確認画面表示
     *
     */
    function return2storages_confirm() {
        //検索結果格納配列
        $SearchResult = array();
        //返品作業区分
        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));

        //元画面のURLによって処理分岐
        if($this->request->data['Search']['Url'] == "return2storages_reader"){
            /* バーコード読取画面から遷移 */
            //読み込まれたバーコードを配列に格納
            $tempBarCode = $this->request->data['Return']['codez'];
            $BarCodeList = explode(",", substr($tempBarCode,0,strlen($tempBarCode) - 1));
            $readFlg = true;
            
            $result = array();
            foreach ($BarCodeList as $barcode){
                //バーコード(シール番号)から商品検索
                
                // シール番号から該当データを取得
                $where = " and a.facility_sticker_no = '" . $barcode . "'";
                $ret = $this->getStickerData($where);
                if(empty($ret)){
                    // 該当データがない場合、ダミーで埋める
                    $dummy = array();
                    $dummy['TrnSticker']['id']                  = '';
                    $dummy['TrnSticker']['internal_code']       = '';
                    $dummy['TrnSticker']['item_name']           = '';
                    $dummy['TrnSticker']['item_code']           = '';
                    $dummy['TrnSticker']['standard']            = '';
                    $dummy['TrnSticker']['unit_name']           = '';
                    $dummy['TrnSticker']['dealer_name']         = '';
                    $dummy['TrnSticker']['facility_sticker_no'] = $barcode;
                    $dummy['TrnSticker']['lot_no']              = '';
                    $dummy['TrnSticker']['validated_date']      = '';
                    $dummy['TrnSticker']['quantity']            = '';
                    $dummy['TrnSticker']['receiving_unit_name'] = '';
                    $dummy['TrnSticker']['facility_id']         = '';
                    $dummy['TrnSticker']['receiving_price']     = '';
                    $dummy['TrnSticker']['price']               = '';
                    $dummy['TrnSticker']['message']             = '無効なバーコードです。';
                    $result[] = $dummy;
                }else{
                    // 商品情報を結果リストに追加する。
                    $result[] = $ret[0];
                }
            }
            
            //パンくず
            $this->request->data['Search']['back'] = "入庫済返品登録(読込)";

        }else if($this->request->data['Search']['Url'] == "return2storages_search"){
            /* 検索画面から遷移 */
            //選択されたID取得
            $readFlg = false;
            if( $this->request->data['search']['temp_id'] != '' ){
                //商品検索
                $options['conditions'][] = array("TrnSticker.id" => explode ( ',' , $this->request->data['search']['temp_id']));
                $SearchResult = $this->SearchStoragesDetail($this->request->data,$options);
            }
            //パンくず
            $this->request->data['Search']['back'] = "在庫検索";
        }

        //取得した施設採用品IDで仕入先リストを取得
        $this->set('traderList',$this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                       array(Configure::read('FacilityType.supplier')) ,
                                                       true ,
                                                       array('id' , 'facility_name') ,
                                                       'facility_code'
                                                       ));
        
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnReturn.token' , $token);
        $this->request->data['TrnReturn']['token'] = $token;
        $this->set('result',AppController::outputFilter($result));
    }
    
    function getStickerData($where){
        $sql  = ' select ';
        $sql .= '       a.id                    as "TrnSticker__id"';
        $sql .= '     , c.internal_code         as "TrnSticker__internal_code"';
        $sql .= '     , c.item_name             as "TrnSticker__item_name"';
        $sql .= '     , c.item_code             as "TrnSticker__item_code"';
        $sql .= '     , c.standard              as "TrnSticker__standard"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then b1.unit_name  ';
        $sql .= "         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                         as "TrnSticker__unit_name" ';
        $sql .= '     , d.dealer_name           as "TrnSticker__dealer_name"';
        $sql .= '     , a.facility_sticker_no   as "TrnSticker__facility_sticker_no"';
        $sql .= '     , a.lot_no                as "TrnSticker__lot_no"';
        $sql .= "     , to_char(a.validated_date, 'YYYY/mm/dd') ";
        $sql .= '                               as "TrnSticker__validated_date" ';
        $sql .= '     , a.quantity              as "TrnSticker__quantity"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when f.per_unit = 1  ';
        $sql .= '         then f1.unit_name  ';
        $sql .= "         else f1.unit_name || '(' || f.per_unit || f2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                         as "TrnSticker__receiving_unit_name" ';
        $sql .= '     , i.id                    as "TrnSticker__facility_id"';
        $sql .= '     , coalesce( j.unit_price , k.transaction_price) ';
        $sql .= '                               as "TrnSticker__receiving_price" ';
        $sql .= '     , trunc( coalesce( j.unit_price , k.transaction_price) / f.per_unit * b.per_unit , 2)  ';
        $sql .= '                               as "TrnSticker__price" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= "         when a.quantity = 0 then '使用済みのシールです。' ";
        $sql .= "         when a.is_deleted = true then '削除済みのシールです。' ";
        $sql .= "         when a.has_reserved = true then '予約済みのシールです。' ";
        $sql .= "         when a2.facility_type != " . Configure::read('FacilityType.center') . " then 'センターにあるシールでは有りません。' ";
        $sql .= "         else '' ";
        $sql .= '         end ';
        $sql .= '     )                         as "TrnSticker__message" ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_departments as a1';
        $sql .= '       on a1.id = a.mst_department_id';
        $sql .= '     left join mst_facilities as a2';
        $sql .= '       on a2.id = a1.mst_facility_id';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as d  ';
        $sql .= '       on d.id = c.mst_dealer_id  ';
        $sql .= '     left join trn_receivings as e  ';
        $sql .= '       on e.id = a.trn_receiving_id  ';
        $sql .= '     left join mst_item_units as f  ';
        $sql .= '       on f.id = e.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as f1  ';
        $sql .= '       on f1.id = f.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as f2  ';
        $sql .= '       on f2.id = f.per_unit_name_id  ';
        $sql .= '     left join trn_orders as g  ';
        $sql .= '       on g.id = a.trn_order_id  ';
        $sql .= '     left join mst_departments as h  ';
        $sql .= '       on h.id = g.department_id_to  ';
        $sql .= '     left join mst_facilities as i  ';
        $sql .= '       on i.id = h.mst_facility_id  ';
        $sql .= '     left join trn_claims as j  ';
        $sql .= '       on a.buy_claim_id = j.id  ';
        $sql .= '     left join mst_transaction_configs as k  ';
        $sql .= '       on k.mst_facility_item_id = c.id  ';
        $sql .= '       and k.start_date <= now() ';
        $sql .= '       and k.end_date >= now() ';
        $sql .= '     inner join mst_transaction_mains as l  ';
        $sql .= '       on l.mst_facility_item_id = k.mst_facility_item_id  ';
        $sql .= '       and l.mst_item_unit_id = k.mst_item_unit_id  ';
        $sql .= '       and l.mst_facility_id = k.partner_facility_id  ';
        $sql .= '     left join mst_item_units as m  ';
        $sql .= '       on m.id = l.mst_item_unit_id ';
        $sql .= '   where ';
        $sql .= '     1 = 1 ';
        $sql .= $where; 

        return $this->TrnSticker->query($sql);
    }

    
    /**
     * 入庫返品
     * 返品登録処理
     *
     */
    function return2storages_regist() {

        $now = date('Y/m/d H:i:s.u');

        //返品作業区分
        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));

        //システム時間設定
        $date = date("Y/m/d");

        if($this->request->data['TrnReturn']['token'] == $this->Session->read('TrnReturn.token')) {
            $this->Session->delete('TrnReturn.token');
            
            //トランザクション開始
            $this->TrnReceiving->begin();
            
            //行ロック
            $this->TrnSticker->query('select * from trn_stickers as a where a.id in (' .join(',',$this->request->data['Regist']['id']). ') for update ');
            
            //更新チェック
            $ret = $this->TrnSticker->query("select count(*) from trn_stickers as a where a.id in (" .join(',',$this->request->data['Regist']['id']). ") and a.modified > '" . $this->request->data['TrnReturn']['time'] . "'");
            if($ret[0][0]['count'] > 0){
                $errorFlg = true;
            }
            
            // 必要情報を取得
            $sql  = ' select ';
            $sql .= '       a.id ';
            $sql .= '     , a.mst_item_unit_id ';
            $sql .= '     , a.mst_department_id ';
            $sql .= '     , a.hospital_sticker_no ';
            $sql .= '     , a.trn_order_id ';
            $sql .= '     , b.trn_receiving_header_id ';
            $sql .= '     , c.id                   as stock_id  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '     left join trn_receivings as b  ';
            $sql .= '       on b.id = a.trn_receiving_id  ';
            $sql .= '     left join trn_stocks as c  ';
            $sql .= '       on c.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '       and c.mst_department_id = a.mst_department_id  ';
            $sql .= '   where ';
            $sql .= '     a.id in ( ' . join(',',$this->request->data['Regist']['id']) .  ' ) ';
            
            $ret = $this->TrnSticker->query($sql);
            
            foreach($ret as $r){
                if(isset($this->data['Regist']['is_retroactable'][$r[0]['id']])){
                    $retroactableFlg = true;
                }else{
                    $retroactableFlg = false;
                }
                
                $TrnReceiving = array(
                    'TrnReceiving'=> array(
                        'work_no'                 => '',
                        'work_seq'                => '',
                        'work_date'               => '',
                        'work_type'               => Configure::read('ClassesType1'),
                        'recital'                 => $this->data['Regist']['recital'][$r[0]['id']],
                        'receiving_type'          => Configure::read('ReceivingType.returnStrage'),
                        'receiving_status'        => Configure::read('ReceivingStatus.stock'),
                        'mst_item_unit_id'        => $r[0]['mst_item_unit_id'],
                        'department_id_from'      => $r[0]['mst_department_id'],
                        'department_id_to'        => $this->getDepartmentId($this->data['Regist']['DepartmentId'][$r[0]['id']] , array(Configure::read('FacilityType.supplier'))), // 業者部署ID
                        'quantity'                => 1,
                        'remain_count'            => 1,
                        'stocking_price'          => $this->data['Regist']['price'][$r[0]['id']],
                        'sales_price'             => 0,
                        'trn_sticker_id'          => $r[0]['id'],
                        'trn_order_id'            => $r[0]['trn_order_id'],
                        'stocking_close_type'     => Configure::read('StockingCloseType.none'),
                        'sales_close_type'        => Configure::read('SalesCloseType.none'),
                        'facility_close_type'     => Configure::read('FacilityCloseType.none'),
                        'trn_receiving_header_id' => $r[0]['trn_receiving_header_id'],
                        'hospital_sticker_no'     => $r[0]['hospital_sticker_no'],
                        'is_retroactable'         => $retroactableFlg,
                        'is_deleted'              => false,
                        'creater'                 => $this->Session->read('Auth.MstUser.id'),
                        'created'                 => $now,
                        'modifier'                => $this->Session->read('Auth.MstUser.id'),
                        'modified'                => $now
                        )
                    );
                $this->TrnReceiving->create();
                // SQL実行
                if (!$this->TrnReceiving->save($TrnReceiving)) {
                    $this->TrnReceiving->rollback();
                    $this->Session->setFlash('返品登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('return2storages');
                }
                $TrnReceivingId = $this->TrnReceiving->getLastInsertID();

                // シール更新
                $this->TrnSticker->create();
                $ret = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.has_reserved'            => "'true'",
                        'TrnSticker.transaction_price'       => $this->data['Regist']['price'][$r[0]['id']],
                        'TrnSticker.trn_return_receiving_id' => $TrnReceivingId,
                        'TrnSticker.modifier'                => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'                => "'" . $now . "'"
                        ),
                    array(
                        'TrnSticker.id' => $r[0]['id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnReceiving->rollback();
                    $this->Session->setFlash('返品登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('return2storages');
                }
                
                // 在庫レコード更新
                // 予約数増加
                
                $this->TrnStock->create();
                $ret = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.reserve_count' => "reserve_count + 1 ",
                        'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'      => "'" . $now . "'"
                        ),
                    array(
                        'TrnStock.id' => $r[0]['stock_id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnReceiving->rollback();
                    $this->Session->setFlash('返品登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('return2storages');
                }
            }
            
            //コミット直前に更新チェック
            $ret = $this->TrnSticker->query("select count(*) from trn_stickers as a where a.id in (" .join(',',$this->request->data['Regist']['id']). ") and a.modified <> '" . $now . "' and a.modified > '" . $this->request->data['TrnReturn']['time'] . "'");
            if($ret[0][0]['count'] > 0){
                $this->TrnReceiving->rollback();
                $this->Session->setFlash('ほかユーザによって更新されています。もう一度やり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('return2storages');
            }
            
            $this->TrnReceiving->commit();
            $this->Session->setFlash("入庫返品の登録が完了しました", 'growl', array('type'=>'star') );
            // 表示用データを取得
            $where =  " and a.id in (" . join(',',$this->request->data['Regist']['id']) . ")";
            $result = $this->getStickerData($where);
            $this->set('result' , $result);
            $this->Session->write('Return.result',$result);
            
        }else{
            // 表示データをセッションから取得
            $this->set('result' ,  $this->Session->read('Return.result'));
        }
        //取得した施設採用品IDで仕入先リストを取得
        $this->set('traderList',$this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                       array(Configure::read('FacilityType.supplier')) ,
                                                       true ,
                                                       array('id' , 'facility_name') ,
                                                       'facility_code'
                                                       ));

        $this->render('return2storages_result');
    }

    /**
     * return_deposits 預託品返品登録画面
     * @param
     * @return
     */
    function return_deposits(){
        $this->setRoleFunction(32); //預託品返品登録
        //施設データセット
        $facility_enabled = $this->_setFacilityEnabled();
        $this->set('facility_enabled', $facility_enabled[0]);
        $this->set('facility_enabled_id', $facility_enabled[1]);
        $this->set('facility_enabled_code', $facility_enabled[2]);

        //返品作業
        $this->set('classes_List', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));
        $this->render('/returns/return_deposits');
    }


    /**
     * return_deposits 預託品返品在庫選択
     * @param
     * @return
     */
    function return_deposits_stock(){
        App::import('Sanitize');
        $SearchResult = array();
        $CartSearchResult = array();
        $this->Session->write('return.fromURL',"return_deposits_stock");
        $this->Session->write('return.MstDepartment.department' ,$this->request->data['MstDepartment']['department']);
        //検索ボタンが押されたら
        if(isset($this->request->data['add_search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $where = "";
            $where2 = "";

            $where .= ' and a.mst_department_id = ' . $this->request->data['MstDepartment']['department'];
            //検索条件の追加
            //商品ID
            if(!empty($this->request->data['MstFacilityItems']['internal_code'])){
                $where .= " and c.internal_code = '" . $this->request->data['MstFacilityItems']['internal_code'] . "'";
            }
            //製品番号
            if(!empty($this->request->data['MstFacilityItems']['item_code'])){
                $where .= " and c.item_code like '%" . $this->request->data['MstFacilityItems']['item_code'] . "%'";
            }
            //ロット番号
            if(!empty($this->request->data['MstFacilityItems']['lot_no'])){
                $where .= " and a.lot_no = '" . $this->request->data['MstFacilityItems']['lot_no'] . "'";
            }
            //商品名
            if(!empty($this->request->data['MstFacilityItems']['item_name'])){
                $where .= " and c.item_name like '%" . $this->request->data['MstFacilityItems']['item_name'] . "%'";
            }
            //販売元
            if(!empty($this->request->data['MstFacilityItems']['dealer_name'])){
                $where .= " and f.dealer_name like '%" . $this->request->data['MstFacilityItems']['dealer_name'] . "%'";
            }
            //規格
            if(!empty($this->request->data['MstFacilityItems']['standard'])){
                $where .= " and c.standard like '%" . $this->request->data['MstFacilityItems']['standard'] . "%'";
            }
            //部署シール
            if(!empty($this->request->data['MstFacilityItems']['hospital_sticker_no'])){
                $where .= " and a.hospital_sticker_no = '" . $this->request->data['MstFacilityItems']['hospital_sticker_no'] . "'";
            }

            if(!empty($this->request->data['cart']['tmpid'])){
                $where .= ' and a.id not in ( ' . $this->request->data['cart']['tmpid'] . ')';
                $where2 = ' and a.id in ( ' . $this->request->data['cart']['tmpid'] . ')';
            }else{
                $where2 = ' and 0 = 1 ';
            }
            //選択
            $SearchResult = $this->getDeposits($where , $this->request->data['limit']);
            //カート
            $CartSearchResult = $this->getDeposits($where2);

            $this->request->data = $data;
        }

        $this->set('SearchResult' , $SearchResult);
        $this->set('CartSearchResult' , $CartSearchResult);

        $this->set('classes_List', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));
        $this->render('/returns/return_deposits_stock');
    }

    /**
     * return_deposits 預託品返品登録画面(シール読込み)
     * @param
     * @return
     */
    function return_deposits_sticker(){
        $this->render('/returns/return_deposits_sticker');
    }

    /**
     * return_deposits 預託品返品登録確認画面
     * @param
     * @return
     */
    function return_deposits_confirm(){
        if($this->request->data['Search']['Url'] == 'return_deposits_sticker'){
            /* バーコード読取画面から遷移 */
            //読み込まれたバーコードを配列に格納
            $tempBarCode = $this->request->data['Return']['codez'];
            $strBarCode = substr($tempBarCode,0,strlen($tempBarCode) - 1);
            $arrBarCode =  explode(',', $strBarCode);
            //検索
            $this->Session->write('return.Search.BarCode',$this->request->data['Return']['codez']);
        }else{
            //シールID
            $this->Session->write('return.TrnSticker.selected_id',$this->request->data['TrnSticker']['id']);
        }

        //検索
        if(isset($arrBarCode)){
            //シール読み込み
            $i=0;
            foreach($arrBarCode as $barcode){
                $where = "     ( a.hospital_sticker_no = '".$barcode."' or a.facility_sticker_no = '".$barcode."' ) ";
                $where .= '     and i.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
                $where .= '     and a.mst_department_id = ' . $this->request->data['MstDepartment']['department'];//部署
                $ret = $this->SearchDepositsDetail($where);

                if(empty($ret)){
                    //シール情報が存在しない場合ダミー情報をつめる
                    $ret[0]['Returns']['id']                  = '';
                    $ret[0]['Returns']['modified']            = '';
                    $ret[0]['Returns']['internal_code']       = '';
                    $ret[0]['Returns']['item_name']           = '';
                    $ret[0]['Returns']['item_code']           = '';
                    $ret[0]['Returns']['standard']            = '';
                    $ret[0]['Returns']['unit_name']           = '';
                    $ret[0]['Returns']['dealer_name']         = '';
                    $ret[0]['Returns']['hospital_sticker_no'] = $barcode;
                    $ret[0]['Returns']['lot_no']              = '';
                    $ret[0]['Returns']['validated_date']      = '';
                    $ret[0]['Returns']['facility_name']       = '';
                    $ret[0]['Returns']['class_name']          = '';
                    $ret[0]['Returns']['is_retroactable']     = '';
                    $ret[0]['Returns']['recital']             = '';
                    $ret[0]['Returns']['status']              = false;
                    $ret[0]['Returns']['message']             = '無効なバーコードです';
                }
                $result[$i] =  $ret[0];
                $i++;
            }

        }else{
            //在庫検索
            $where = '     a.id in (' .join(',',$this->request->data['TrnSticker']['id']). ') ';
            $where .= '     and i.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $where .= '     and a.mst_department_id = ' . $this->request->data['MstDepartment']['department'];//部署
            $result = $this->SearchDepositsDetail($where);
        }


        if(empty($result)){
            $this->set('msg', "返品可能な預託品がありません");
        }else{
            $this->set('msg', "以下の内容で更新します");
        }
        $this->set('result', $result);

        $this->set('classes_List', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnReturn.token' , $token);
        $this->request->data['TrnReturn']['token'] = $token;

        $this->render('/returns/return_deposits_confirm');

    }


    /**
     * return_deposits_result 預託品返品登録結果画面
     * @param
     * @return
     */
    function return_deposits_result(){
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['TrnReturn']['token'] == $this->Session->read('TrnReturn.token')) {
            $this->Session->delete('TrnReturn.token');

            //シールID
            $this->request->data['updated'] = true;

            //登録メイン処理
            if( $this->return_deposits_regist() == false  ) {
                $msg = '返品登録に失敗しました。';
            }else{
                $msg = '以下の内容で更新しました';
            }

            //結果表示用データ取得
            $where = '     a.id in (' .join(',',$this->request->data['Returns']['id']). ') ';
            $result = $this->SearchDepositsDetail($where);

            $this->Session->write('result', $result);
            $this->set('result', $result);

            $this->Session->write('msg', $msg);
            $this->set('msg',$msg);
        }else{
            //2度押しの場合
            $this->set('result', $this->Session->read('result'));
            $this->set('msg',$this->Session->read('msg'));
        }
        $this->render('/returns/return_deposits_result_view');
    }


    /**
     * 預託品返品登録結果情報
     */
    function SearchDepositsDetail($where){
        $sql  = ' select ';
        $sql .= '       a.id                                               as "Returns__id" ';
        $sql .= '     , a.modified                                         as "Returns__modified" ';
        $sql .= '     , i.internal_code                                    as "Returns__internal_code" ';
        $sql .= '     , i.item_name                                        as "Returns__item_name" ';
        $sql .= '     , i.item_code                                        as "Returns__item_code" ';
        $sql .= '     , i.standard                                         as "Returns__standard" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when d.per_unit = 1  ';
        $sql .= '         then e.unit_name  ';
        $sql .= "         else e.unit_name || '(' || d.per_unit || f.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                                    as "Returns__unit_name" ';
        $sql .= '     , j.dealer_name                                      as "Returns__dealer_name" ';
        $sql .= '     , a.hospital_sticker_no                              as "Returns__hospital_sticker_no" ';
        $sql .= '     , a.lot_no                                           as "Returns__lot_no" ';
        $sql .= '     , to_char(a.validated_date, \'YYYY/mm/dd\')          as "Returns__validated_date" ';
        $sql .= '     , h.facility_name                                    as "Returns__facility_name" ';
        $sql .= '     , c.name                                             as "Returns__class_name" ';
        $sql .= '     , (case when b.is_retroactable = true then \'○\' end) as "Returns__is_retroactable" ';
        $sql .= '     , b.recital                                          as "Returns__recital"  ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.quantity = 0 then false ';
        $sql .= '         when a.is_deleted = true then false ';
        $sql .= '         when a.has_reserved = true then false ';
        $sql .= '         when a.trade_type != ' . Configure::read('ClassesType.Deposit') . ' then false ';
        $sql .= '         else true ';
        $sql .= '         end ';
        $sql .= '     )                                                    as "Returns__status" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= "         when a.quantity = 0 then '使用済みのシールです。' ";
        $sql .= "         when a.is_deleted = true then '削除済みのシールです。' ";
        $sql .= "         when a.has_reserved = true then '予約済みのシールです。' ";
        $sql .= "         when a.trade_type != " . Configure::read('ClassesType.Deposit') . " then '預託品では有りません。' ";
        $sql .= "         else '' ";
        $sql .= '         end ';
        $sql .= '     )                                                    as "Returns__message" ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join trn_receivings as b  ';
        $sql .= '       on a.trn_return_receiving_id = b.id  ';
        $sql .= '       and b.is_deleted = false  ';
        $sql .= "       and b.receiving_type in (" . Configure::read('ReceivingType.return') . ", " . Configure::read('ReceivingType.returnDeposit') . ")  ";
        $sql .= '     left join mst_classes as c  ';
        $sql .= '       on c.id = b.work_type  ';
        $sql .= '     left join mst_item_units as d  ';
        $sql .= '       on a.mst_item_unit_id = d.id  ';
        $sql .= '     left join mst_unit_names as e  ';
        $sql .= '       on d.mst_unit_name_id = e.id  ';
        $sql .= '     left join mst_unit_names as f  ';
        $sql .= '       on d.per_unit_name_id = f.id  ';
        $sql .= '     left join trn_orders as o  ';
        $sql .= '       on o.id = a.trn_order_id  ';
        $sql .= '     left join mst_departments as g  ';
        $sql .= '       on o.department_id_to = g.id  ';
        $sql .= '     left join mst_facilities as h  ';
        $sql .= '       on g.mst_facility_id = h.id  ';
        $sql .= '     left join mst_facility_items as i  ';
        $sql .= '       on d.mst_facility_item_id = i.id  ';
        $sql .= '     left join mst_dealers as j  ';
        $sql .= '       on i.mst_dealer_id = j.id  ';
        $sql .= '   where ';
        $sql .= $where;

        return $this->TrnSticker->query($sql);
    }


    /**
     * 預託品返品
     * 返品登録処理
     *
     */
    function return_deposits_regist() {
        //検索結果格納配列
        $SearchResult = array();

        //システム時間設定
        $now = date('Y/m/d H:i:s.u');

        //work_no
        $_workNo = $this->setWorkNo4Header(date("Y/m/d"), '13');
        //etc
        $msg = "";
        $errorFlg = false;
        if( count($this->request->data['Returns']['id']) > 0 ){
            //トランザクション開始
            $this->TrnReceiving->begin();

            //行ロック
            $this->TrnSticker->query('select * from trn_stickers as a where a.id in ( ' .join(',', $this->request->data['Returns']['id'] ). ') for update ');

            //更新チェック
            $ret = $this->TrnSticker->query("select count(*) from trn_stickers as a where a.id in ( " .join(',', $this->request->data['Returns']['id'] ). ") and a.modified >  '" . $this->request->data['TrnReturn']['time'] . "'");
            if($ret[0][0]['count'] > 0){
                $errorFlg = true;
            }

            $sql  = ' select ';
            $sql .= '       a.id                                            as "Returns__sticker_id" ';
            $sql .= '     , a.mst_item_unit_id                              as "Returns__mst_item_unit_id" ';
            $sql .= '     , a.facility_sticker_no                           as "Returns__facility_sticker_no" ';
            $sql .= '     , a.hospital_sticker_no                           as "Returns__hospital_sticker_no" ';
            $sql .= '     , a.mst_department_id                             as "Returns__mst_department_id" ';
            $sql .= '     , a.quantity                                      as "Returns__quantity" ';
            $sql .= '     , a.modified                                      as "Returns__modified" ';
            $sql .= '     , coalesce(  ';
            $sql .= '       e.department_id_from ';
            $sql .= '       , f.department_id_from ';
            $sql .= '       , j.id ';
            $sql .= '       , a.mst_department_id ';
            $sql .= '     )                                                 as "Returns__department_id_to" ';
            $sql .= '       , a.mst_department_id                           as "Returns__department_id_from" ';
            $sql .= '     , coalesce(e.stocking_price, a.transaction_price) as "Returns__stocking_price" ';
            $sql .= '     , coalesce(e.sales_price, a.transaction_price)    as "Returns__sales_price" ';
            $sql .= '     , c.id                                            as "Returns__stock_id"  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '     left join trn_receivings as b  ';
            $sql .= '       on a.trn_receiving_id = b.id  ';
            $sql .= '     left join trn_stocks as c  ';
            $sql .= '       on a.mst_item_unit_id = c.mst_item_unit_id  ';
            $sql .= '       and a.mst_department_id = c.mst_department_id  ';
            $sql .= '       and c.is_deleted = false  ';
            $sql .= '     left join mst_item_units as d  ';
            $sql .= '       on a.mst_item_unit_id = d.id  ';
            $sql .= '     left join trn_receivings as e  ';
            $sql .= '       on a.trn_receiving_id = e.id  ';
            $sql .= '     left join trn_orders as f  ';
            $sql .= '       on f.id = a.trn_order_id  ';
            $sql .= '     left join mst_departments as h  ';
            $sql .= '       on a.mst_department_id = h.id  ';
            $sql .= '     left join mst_transaction_mains as i ';
            $sql .= '       on i.mst_facility_item_id = d.mst_facility_item_id ';
            $sql .= '     left join mst_departments as j ';
            $sql .= '       on j.mst_facility_id = i.mst_facility_id ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',',$this->request->data['Returns']['id']) . ')';

            $ret = $this->TrnSticker->query($sql);

            foreach($ret as $r){
                //取得情報の更新日時チェック
                if($r['Returns']['modified'] != $this->request->data['Returns']['modified'][$r['Returns']['sticker_id']]){
                    $errorFlg = true;
                    $msg = "別ユーザによってデータが更新されています";
                    $this->TrnReceiving->rollback();
                    return $msg;
                    break;
                }

                //入荷テーブルに返品レコード作成
                $this->TrnReceiving->create();
                $res3 = $this->TrnReceiving->save(array(
                    'work_type'               => $this->request->data['Returns']['work_type'][$r['Returns']['sticker_id']],
                    'recital'                 => $this->request->data['Returns']['recital'][$r['Returns']['sticker_id']],
                    'receiving_type'          => Configure::read('ReceivingType.returnDeposit'),
                    'receiving_status'        => Configure::read('ReceivingStatus.arrival'),
                    'mst_item_unit_id'        => $r['Returns']['mst_item_unit_id'],
                    'department_id_from'      => $r['Returns']['department_id_from'],
                    'department_id_to'        => $r['Returns']['department_id_to'],
                    'quantity'                => $r['Returns']['quantity'],
                    'stocking_price'          => $r['Returns']['stocking_price'],
                    'sales_price'             => $r['Returns']['sales_price'],
                    'trn_sticker_id'          => $r['Returns']['sticker_id'],
                    'is_retroactable'         => (isset($this->request->data['Returns']['is_retroactable'][$r['Returns']['sticker_id']])?true:false),
                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                 => $now,
                    'modified'                => $now,
                    'remain_count'            => $r['Returns']['quantity']
                    ));
                if(!$res3){
                    $msg .= "返品入荷情報作成エラー";
                    $errorFlg = true;
                    //exit;
                }

                $this->TrnSticker->recursive = -1;
                $res2 = $this->TrnSticker->updateAll(array('has_reserved'            => 'true',
                                                           'trn_return_receiving_id' => $this->TrnReceiving->getLastInsertID(),
                                                           'modifier'                => $this->Session->read('Auth.MstUser.id'),
                                                           'modified'                => "'".$now."'",
                                                     ),array(
                                                            'TrnSticker.id' => $r['Returns']['sticker_id'],
                                                     ),-1
                );

                if(!$res2){
                    $msg .= "シール予約済みフラグ更新エラー";
                }

                //在庫の予約数△
                $this->TrnStock->create();
                $this->TrnStock->recursive = -1;
                if (!$this->TrnStock->updateAll(array(  'TrnStock.reserve_count' => 'TrnStock.reserve_count + ' . $r['Returns']['quantity'],
                                                        'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                        'TrnStock.modified'    => "'" . $now . "'"
                                                        )
                                                ,
                                                array(
                                                    'TrnStock.id'  => $r['Returns']['stock_id'],
                                                    )
                                                ,
                                                -1
                                                )
                    ) {
                    $this->TrnSticker->rollback();
                    $errorFlg = true;
                }

            }//end of for
            $errors = array();
            $errors = array_merge($errors
                                  ,is_array($this->validateErrors($this->TrnSticker)) ? $this->validateErrors($this->TrnSticker) : array()
                                  ,is_array($this->validateErrors($this->TrnStock)) ? $this->validateErrors($this->TrnStock) : array()
                                  );
            if(count($errors) > 0){
                $error_flg = true;
            }

            //コミット直前に更新チェック
            $ret = $this->TrnSticker->query("select count(*) from trn_stickers as a where a.id in ( " .join(',', $this->request->data['Returns']['id']  ). ") and a.modified <> '" . $now . "' and a.modified >  '" . $this->request->data['TrnReturn']['time'] . "'");
            if($ret[0][0]['count'] > 0){
                $errorFlg = true;
            }

            if($errorFlg){
                $this->TrnReceiving->rollback();
                return false;
            }else{
                $this->TrnReceiving->commit();
                return true;
            }
        }
    }


    //編集画面用施設プルダウン作成用データ
    function _setFacilityEnabled () {
        $facilityId = $this->Session->read('Auth.facility_id_selected');

        $sql  = 'SELECT C.facility_code ';
        $sql .= '      ,C.facility_name ';
        $sql .= '      ,C.id ';
        $sql .= '  FROM mst_facility_relations B ';
        $sql .= '        INNER JOIN mst_facilities C ';
        $sql .= '                ON B.partner_facility_id = C.id ';
        $sql .= '               AND C.facility_type = '.Configure::read('FacilityType.hospital');
        $sql .= '        inner join mst_user_belongings as d ';
        $sql .= '                on d.mst_facility_id = C.id ';
        $sql .= '                and d.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '                and d.is_deleted = false ';
        $sql .= ' WHERE B.mst_facility_id = '.$facilityId;
        $sql .= ' ORDER BY C.facility_code ';
        $_facility_enabled = $this->MstUser->query($sql);

        $params = array(
            'conditions' => array('MstFacility.id' => $facilityId), //条件の配列
            'fields' => array(
                'MstFacility.id',
                'MstFacility.facility_name',
                'MstFacility.facility_code'
                )//フィールド名の配列
            );
        $selected_facility = $this->MstFacility->find('all',$params);

        $_facility_enabled_out = array();
        $_facility_enabled_id = array();
        $_facility_enabled_code = array();

        for ($_i = 0;$_i < count($_facility_enabled);$_i++){
            $_facility_enabled_out[$_facility_enabled[$_i][0]['id']] = $_facility_enabled[$_i][0]['facility_name'];
            $_facility_enabled_id[$_facility_enabled[$_i][0]['id']] = $_facility_enabled[$_i][0]['facility_code'];
            $_facility_enabled_code[$_facility_enabled[$_i][0]['facility_code']] = $_facility_enabled[$_i][0]['id'];
        }
        $facility_enabled = array($_facility_enabled_out,$_facility_enabled_id,$_facility_enabled_code);
        return $facility_enabled;
    }

    /**
     * 返品確認
     * 仕入先選択
     */
    function return_decision() {
        $this->setRoleFunction(33); //返品確認
        //仕入先セレクトボックスの設定
        $this->set('facility_select',$this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                            array(Configure::read('FacilityType.supplier') ),
                                                            false ,
                                                            array('id' , 'facility_name'),
                                                            'facility_name, facility_code'
                                                            ));

        $result = array();
        if(isset($this->request->data['Return']['is_search'])){
            $sql  = ' select ';
            $sql .= '       d1.id                        as "MstDepartmentTo__id" ';
            $sql .= '     , d1.department_name           as "MstDepartmentTo__department_name" ';
            $sql .= '     , e1.id                        as "MstFacilityTo__id" ';
            $sql .= '     , e1.facility_name             as "MstFacilityTo__facility_name" ';
            $sql .= '     , d2.id                        as "MstDepartmentFrom__id" ';
            $sql .= '     , d2.department_name           as "MstDepartmentFrom__department_name" ';
            $sql .= '     , e2.id                        as "MstFacilityFrom__id" ';
            $sql .= '     , e2.facility_name             as "MstFacilityFrom__facility_name"  ';
            $sql .= '   from ';
            $sql .= '     trn_receivings as a  ';
            $sql .= '     inner join trn_stickers as b  ';
            $sql .= '       on b.trn_return_receiving_id = a.id  ';
            $sql .= '       and b.quantity <> 0  ';
            $sql .= '       and b.has_reserved = true  ';
            $sql .= '       and b.is_deleted = false  ';
            $sql .= '     inner join mst_item_units as c  ';
            $sql .= '       on a.mst_item_unit_id = c.id  ';
            $sql .= '       and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     inner join mst_departments as d1  ';
            $sql .= '       on d1.id = a.department_id_to  ';
            $sql .= '     inner join mst_facilities as e1  ';
            $sql .= '       on e1.id = d1.mst_facility_id  ';
            $sql .= "       and e1.facility_type in (".Configure::read('FacilityType.supplier').", ".Configure::read('FacilityType.center').")  ";
            $sql .= '     inner join mst_departments as d2  ';
            $sql .= '       on d2.id = a.department_id_from  ';
            $sql .= '     inner join mst_facilities as e2  ';
            $sql .= '       on e2.id = d2.mst_facility_id  ';
            $sql .= "       and e2.facility_type in (".Configure::read('FacilityType.hospital').", ".Configure::read('FacilityType.center').")  ";
            $sql .= '     inner join mst_user_belongings as z  ';
            $sql .= '       on z.mst_facility_id = e2.id  ';
            $sql .= '       and z.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '       and z.is_deleted = false  ';
            $sql .= '   where ';
            $sql .= "     a.receiving_type in (" . Configure::read('ReceivingType.return'). ", " . Configure::read('ReceivingType.returnReceiving') . ", " . Configure::read('ReceivingType.returnStrage') . ", " . Configure::read('ReceivingType.returnDeposit') .")  ";
            $sql .= '     and a.is_deleted = false  ';
            $sql .= '     and a.trn_return_header_id is null  ';
            if($this->request->data['MstFacility']['id'] != ''){
                $sql .= '     and e1.id = ' . $this->request->data['MstFacility']['id'];
            }
            $sql .= '   group by ';
            $sql .= '     d1.id ';
            $sql .= '     , d1.department_name ';
            $sql .= '     , e1.id ';
            $sql .= '     , e1.facility_name ';
            $sql .= '     , d2.id ';
            $sql .= '     , d2.department_name ';
            $sql .= '     , e2.id ';
            $sql .= '     , e2.facility_name  ';
            $sql .= '   order by ';
            $sql .= '     d2.id asc ';

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnReceiving'));

            $sql .= '  limit ' . $this->request->data['Return']['limit'];
            //表示データ取得
            $result = $this->TrnReceiving->query($sql);
        }

        $this->set('result',$result);
    }

    /**
     * 返品確認
     * 確認画面
     */
    function return_decision_confirm() {
        //作業区分
        $result = array();
        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));
        foreach( $this->request->data['TrnReturn']['id'] as $id ){
            list($departmentFromId , $departmentToId) = explode('_' , $id);
            // 検索
            $where = ' and a.trn_return_header_id is null ';
            $where .= ' and c1.id = ' . $departmentFromId . ' and d1.id = ' . $departmentToId ;
            $ret = $this->getReturnData($where);
            // 配列マージ
            $result = array_merge ( $result , $ret );
        }

        $this->set('result',$result);

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnReturn.token' , $token);
        $this->request->data['TrnReturn']['token'] = $token;
    }
    
    function getReturnData( $where = '' ){
        $sql  = ' select ';
        $sql .= '       a.id                   as "TrnReturn__id"';
        $sql .= '     , f.internal_code        as "TrnReturn__internal_code"';
        $sql .= '     , f.item_name            as "TrnReturn__item_name"';
        $sql .= '     , f.item_code            as "TrnReturn__item_code"';
        $sql .= '     , f.standard             as "TrnReturn__standard"';
        $sql .= '     , f.jan_code             as "TrnReturn__jan_code"';
        $sql .= '     , g.dealer_name          as "TrnReturn__dealer_name"';
        $sql .= '     , ( ';
        $sql .= '       case ';
        $sql .= '         when e.per_unit = 1 ';
        $sql .= '         then e1.unit_name ';
        $sql .= "         else e1.unit_name || '(' || e.per_unit || e2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                        as "TrnReturn__unit_name" ';
        $sql .= '     , ( case when length(b.hospital_sticker_no) = 0 ';
        $sql .= '            then b.facility_sticker_no ';
        $sql .= '            else b.hospital_sticker_no ';
        $sql .= '         end)                 as "TrnReturn__sticker_no"';
        $sql .= '     , b.lot_no               as "TrnReturn__lot_no"';
        $sql .= "     , to_char(b.validated_date, 'yyyy/mm/dd') ";
        $sql .= '                              as "TrnReturn__validated_date" ';
        $sql .= '     , c2.facility_name       as "TrnReturn__facility_name_from"';
        $sql .= '     , d2.facility_name       as "TrnReturn__facility_name_to"';
        $sql .= '     , a.quantity             as "TrnReturn__quantity"';
        $sql .= '     , a.work_type            as "TrnReturn__work_type"';
        $sql .= '     , k.name                 as "TrnReturn__work_type_name"';
        $sql .= '     , a.recital              as "TrnReturn__recital"';
        $sql .= '     , a.receiving_type       as "TrnReturn__receiving_type"';
        $sql .= '     , ( case a.receiving_type';
        foreach(Configure::read('ReceivingType.typeName') as $key => $val){
            $sql .= "           when " . $key . " then '" . $val ."' ";
        }
        $sql .= '       end)                   as "TrnReturn__receiving_type_name"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when i.per_unit = 1  ';
        $sql .= '         then i1.unit_name  ';
        $sql .= "         else i1.unit_name || '(' || i.per_unit || i2.unit_name || ')' ";
        $sql .= '         end ';
        $sql .= '     )                        as "TrnReturn__receiving_unit_name" ';
        $sql .= '     , h.stocking_price       as "TrnReturn__stoking_price" ';
        $sql .= '     , j.transaction_price    as "TrnReturn__transaction_price" ';
        $sql .= '     , a.stocking_price       as "TrnReturn__return_price"  ';
        $sql .= '   from ';
        $sql .= '     trn_receivings as a  ';
        $sql .= '     left join trn_stickers as b  ';
        $sql .= '       on a.id = b.trn_return_receiving_id  ';
        $sql .= '     left join mst_departments as c1  ';
        $sql .= '       on c1.id = a.department_id_from  ';
        $sql .= '     left join mst_facilities as c2  ';
        $sql .= '       on c2.id = c1.mst_facility_id  ';
        $sql .= '     left join mst_departments as d1  ';
        $sql .= '       on d1.id = a.department_id_to  ';
        $sql .= '     left join mst_facilities as d2  ';
        $sql .= '       on d2.id = d1.mst_facility_id  ';
        $sql .= '     left join mst_item_units as e  ';
        $sql .= '       on e.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as e1  ';
        $sql .= '       on e1.id = e.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as e2  ';
        $sql .= '       on e2.id = e.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as f  ';
        $sql .= '       on f.id = e.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as g  ';
        $sql .= '       on g.id = f.mst_dealer_id  ';
        $sql .= '     left join trn_orders as h  ';
        $sql .= '       on h.id = b.trn_order_id  ';
        $sql .= '     left join mst_item_units as i  ';
        $sql .= '       on i.id = h.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as i1  ';
        $sql .= '       on i1.id = i.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as i2  ';
        $sql .= '       on i2.id = i.per_unit_name_id  ';
        $sql .= '     left join mst_transaction_configs as j  ';
        $sql .= '       on j.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '       and j.start_date <= now()  ';
        $sql .= '       and j.end_date >= now()  ';
        $sql .= '       and j.is_deleted = false  ';
        $sql .= '       and j.partner_facility_id = d2.id ';
        $sql .= '     left join mst_classes as k ';
        $sql .= '       on k.id = a.work_type ';
        $sql .= '   where ';
        $sql .= '     a.receiving_type in ( ' . Configure::read('ReceivingType.returnReceiving') . ' , ' . Configure::read('ReceivingType.returnStrage'). ' , ' . Configure::read('ReceivingType.returnDeposit') . ')  ';
        $sql .= '     and a.is_deleted = false  ';
        $sql .= $where;
        
        return $this->TrnReceiving->query($sql);
    }
    
    
    /**
     * 返品確認
     * 確定処理
     */
    function return_decision_result() {
        if($this->request->data['TrnReturn']['token'] == $this->Session->read('TrnReturn.token')) {
            $this->Session->delete('TrnReturn.token');

            $now = date('Y/m/d H:i:s.u');
            $header_ids = array();
            
            //トランザクション開始
            $this->TrnReceiving->begin();

            //行ロック
            $this->TrnReceiving->query('select * from trn_receivings as a where a.id in (' . join(',',$this->request->data['TrnReturn']['id']) .') for update ');

            //更新チェック
            $ret = $this->TrnReceiving->query("select count(*) from trn_receivings as a where a.id in (" . join(',',$this->request->data['TrnReturn']['id']) .") and a.modified > '" . $this->request->data['TrnReturn']['time'] . "'" );
            if($ret[0][0]['count'] > 0){
                // 更新チェックエラー
                $this->TrnReceiving->rollback();
                $this->Session->setFlash('ほかユーザによって更新されています。', 'growl', array('type'=>'error') );
                $this->redirect('return_decision');
            }
            
            // ヘッダまとめる
            $sql  = ' select ';
            $sql .= '       a.receiving_type ';
            $sql .= '     , a.department_id_from ';
            $sql .= '     , a.department_id_to ';
            $sql .= '     , count(*)     as count  ';
            $sql .= '   from ';
            $sql .= '     trn_receivings as a  ';
            $sql .= '   where ';
            $sql .= '      a.id in (' . join(',',$this->request->data['TrnReturn']['id']) .') ';
            $sql .= '   group by ';
            $sql .= '     a.receiving_type ';
            $sql .= '     , a.department_id_from ';
            $sql .= '     , a.department_id_to  ';
            $sql .= '   order by ';
            $sql .= '     a.department_id_from ';
            $sql .= '     , a.department_id_to ';
            $sql .= '     , a.receiving_type ';
            $header_data = $this->TrnReceiving->query($sql);
            foreach($header_data as $h){
                $work_no = $this->setWorkNo4Header($this->request->data['TrnReturn']['date'],'13');
                //返品ヘッダテーブルにレコード作成
                $TrnReturnHeader = array(
                    'TrnReturnHeader'=> array(
                        'work_no'             => $work_no,                                  //作業番号
                        'work_date'           => $this->request->data['TrnReturn']['date'], //作業日時
                        'return_status'       => Configure::read('ReturnStatus.send'),      //返品状態
                        'department_id_from'  => $h[0]['department_id_from'],               //移動元部署参照キー
                        'department_id_to'    => $h[0]['department_id_to'],                 //移動先部署参照キー
                        'detail_count'        => $h[0]['count'],                            //明細数
                        'is_deleted'          => false,                                     //削除フラグ
                        'creater'             => $this->Session->read('Auth.MstUser.id'),   //作成ユーザ参照キー
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),  //更新ユーザ参照キー
                        'created'             => $now,                                      //作成ユーザ
                        'modified'            => $now                                       //更新ユーザ
                        )
                    );
                
                $this->TrnReturnHeader->create();
                if (!$this->TrnReturnHeader->save($TrnReturnHeader)) {
                    $this->TrnReceiving->rollback();
                    $this->Session->setFlash('返品確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('return_decision');
                }
                
                //作成した返品レコードのID取得
                $TrnReturnHeaderId = $this->TrnReturnHeader->getLastInsertID();
                $header_ids[] = $TrnReturnHeaderId;
                
                // 明細分を再取得
                $sql  = ' select ';
                $sql .= '       a.id                 as receiving_id ';
                $sql .= '     , b.id                 as sticker_id ';
                $sql .= '     , a.mst_item_unit_id ';
                $sql .= '     , a.receiving_type ';
                $sql .= '     , a.department_id_from ';
                $sql .= '     , a.department_id_to  ';
                $sql .= '     , a.quantity ';
                $sql .= '     , b.hospital_sticker_no';
                $sql .= '     , b.facility_sticker_no';
                $sql .= '     , d.round';
                $sql .= '     , d.gross';
                $sql .= '   from ';
                $sql .= '     trn_receivings as a  ';
                $sql .= '     left join trn_stickers as b  ';
                $sql .= '       on b.trn_return_receiving_id = a.id  ';
                $sql .= '     left join mst_departments as c ';
                $sql .= '       on c.id = a.department_id_to ';
                $sql .= '     left join mst_facilities as d ';
                $sql .= '       on d.id = c.mst_facility_id ';
                $sql .= '   where ';
                $sql .= '     a.receiving_type = ' . $h[0]['receiving_type'];
                $sql .= '     and a.department_id_from = ' . $h[0]['department_id_from'];
                $sql .= '     and a.department_id_to = ' . $h[0]['department_id_to'];
                $sql .= '     and a.id in (' . join(',',$this->request->data['TrnReturn']['id']) .') ';
                $sql .= '   order by ';
                $sql .= '     a.department_id_from ';
                $sql .= '     , a.department_id_to ';
                $sql .= '     , a.receiving_type ';
                $sql .= '     , a.work_seq ';

                $detail = $this->TrnReceiving->query($sql);
                $cnt = 1;
                foreach($detail as $d){
                    // 返品ヘッダIDを入れる
                    
                    $this->TrnReceiving->create();
                    $ret = $this->TrnReceiving->updateAll(
                        array(
                            'TrnReceiving.work_seq'             => $cnt,
                            'TrnReceiving.work_date'            => "'". $this->request->data['TrnReturn']['date'] . "'",
                            'TrnReceiving.trn_return_header_id' => $TrnReturnHeaderId,
//                            'TrnReceiving.work_type'            => $this->request->data['TrnReturn']['work_type'][$d[0]['receiving_id']],
                            'TrnReceiving.recital'              => "'" . $this->request->data['TrnReturn']['recital'][$d[0]['receiving_id']] . "'",
                            'TrnReceiving.modifier'             => $this->Session->read('Auth.MstUser.id'),
                            'TrnReceiving.modified'             => "'" . $now . "'"
                            ),
                        array(
                            'TrnReceiving.id' => $d[0]['receiving_id'],
                            ),
                        -1
                        );
                    if(!$ret){
                        $this->TrnReceiving->rollback();
                        $this->Session->setFlash('返品確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('return_decision');
                    }
                    
                    
                    // 在庫を減らす
                    // 在庫予約数を減らす
                    $this->TrnStock->create();
                    $ret = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.stock_count'   => 'TrnStock.stock_count - ' . $d[0]['quantity'] ,
                            'TrnStock.reserve_count' => 'TrnStock.reserve_count - ' . $d[0]['quantity'] ,
                            'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'      => "'" . $now . "'"
                            ),
                        array(
                            'TrnStock.id' => $this->getStockRecode( $d[0]['mst_item_unit_id'],
                                                                    $d[0]['department_id_from'])
                            ),
                        -1
                        );
                    if(!$ret) {
                        $this->TrnReceiving->rollback();
                        $this->Session->setFlash('返品確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('return_decision');
                    }

                    if($d[0]['receiving_type'] != Configure::read('ReceivingType.returnReceiving')){
                        // 入荷済み返品以外
                        // シールを減らす
                        $this->TrnSticker->create();
                        if(empty($this->request->data['TrnReturn']['validated_date'][$d[0]['receiving_id']])){
                            $this->request->data['TrnReturn']['validated_date'][$d[0]['receiving_id']] = null;
                        }else{
                            $this->request->data['TrnReturn']['validated_date'][$d[0]['receiving_id']] = "'" . $this->request->data['TrnReturn']['validated_date'][$d[0]['receiving_id']] . "'";
                        }
                        $ret = $this->TrnSticker->updateAll(
                            array(
                                'TrnSticker.lot_no'         => "'". $this->request->data['TrnReturn']['lot_no'][$d[0]['receiving_id']] . "'",
                                'TrnSticker.validated_date' => $this->request->data['TrnReturn']['validated_date'][$d[0]['receiving_id']] ,
                                'TrnSticker.quantity'       => 'TrnSticker.quantity - ' . $d[0]['quantity'] ,
                                'TrnSticker.modifier'       => $this->Session->read('Auth.MstUser.id'),
                                'TrnSticker.modified'       => "'" . $now . "'"
                                ),
                            array(
                                'TrnSticker.id' => $d[0]['sticker_id']
                                ),
                            -1
                            );
                        if(!$ret) {
                            $this->TrnReceiving->rollback();
                            $this->Session->setFlash('返品確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('return_decision');
                        }
                        // シール移動履歴
                        $TrnStickerRecord = array(
                            'TrnStickerRecord'    => array(
                                'move_date'           => $this->request->data['TrnReturn']['date'] ,
                                'move_seq'            => $this->getNextRecord($d[0]['sticker_id']),
                                'record_type'         => Configure::read('RecordType.return'),
                                'facility_sticker_no' => $d[0]['facility_sticker_no'],
                                'hospital_sticker_no' => $d[0]['hospital_sticker_no'],
                                'record_id'           => $d[0]['receiving_id'], //返品明細
                                'trn_sticker_id'      => $d[0]['sticker_id'],
                                'mst_department_id'   => $d[0]['department_id_from'], //移動元部署
                                'creater'             => $this->Session->read('Auth.MstUser.id'),
                                'modifier'            => $this->Session->read('Auth.MstUser.id'),
                                'created'             => $now,
                                'modified'            => $now,
                                'is_deleted'          => false,
                                )
                            );
                        //TrnStickerRecordオブジェクトをcreate
                        $this->TrnStickerRecord->create();
                        if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                            $this->TrnReceiving->rollback();
                            $this->Session->setFlash('返品確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('return_decision');
                        }
                        
                    } else {
                        //入荷済み返品
                        $this->TrnSticker->create();
                        $ret = $this->TrnSticker->updateAll(
                            array(
                                'TrnSticker.quantity'       => 'TrnSticker.quantity - ' . $d[0]['quantity'] ,
                                'TrnSticker.modifier'       => $this->Session->read('Auth.MstUser.id'),
                                'TrnSticker.modified'       => "'" . $now . "'"
                                ),
                            array(
                                'TrnSticker.id' => $this->getReceivingStockRecode( $d[0]['mst_item_unit_id'],
                                                                                   $d[0]['department_id_from'])
                                ),
                            -1
                            );
                        if(!$ret) {
                            $this->TrnReceiving->rollback();
                            $this->Session->setFlash('返品確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('return_decision');
                        }
                    }
                    // 預託品返品以外
                    if($d[0]['receiving_type'] != Configure::read('ReceivingType.returnDeposit')){
                        // マイナス仕入れを立てる
                        
                        $TrnClaim = array(
                            'TrnClaim'=> array(
                                'department_id_from'  => $d[0]['department_id_to'],
                                'department_id_to'    => $d[0]['department_id_from'],
                                'mst_item_unit_id'    => $d[0]['mst_item_unit_id'],
                                'claim_date'          => $this->request->data['TrnReturn']['date'],
                                'claim_price'         => 0 - ( $this->request->data['TrnReturn']['stocking_price'][$d[0]['receiving_id']] * $d[0]['quantity']),
                                'trn_receiving_id'    => $d[0]['receiving_id'],
                                'is_deleted'          => false,
                                'creater'             => $this->Session->read('Auth.MstUser.id'),
                                'modifier'            => $this->Session->read('Auth.MstUser.id'),
                                'created'             => $now,
                                'modified'            => $now,
                                'is_stock_or_sale'    => '1', //仕入売上判定フラグ 1:仕入;2:売上
                                'count'               => $d[0]['quantity'],
                                'unit_price'          => $this->request->data['TrnReturn']['stocking_price'][$d[0]['receiving_id']],
                                'round'               => $d[0]['round'],
                                'gross'               => $d[0]['gross'],
                                'is_not_retroactive'  => true,
                                'facility_sticker_no' => $d[0]['facility_sticker_no'],
                                'hospital_sticker_no' => $d[0]['hospital_sticker_no'],
                                'claim_type'          => Configure::read('ClaimType.return')
                                )
                            );
                        
                        $this->TrnClaim->create();
                        if (!$this->TrnClaim->save($TrnClaim)) {
                            $this->TrnReceiving->rollback();
                            $this->Session->setFlash('返品確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('return_decision');
                        }
                    $this->createMsClaimRecode($this->TrnClaim->getLastInsertID());
                    }
                    $cnt++;
                }
            }
            
            
            //コミット直前に更新チェック
            $ret = $this->TrnReceiving->query("select count(*) from trn_receivings as a where a.id in (" . join(',',$this->request->data['TrnReturn']['id']) .") and a.modified <> '" .$now . "' and a.modified > '" . $this->request->data['TrnReturn']['time'] . "'" );
            
            if($ret[0][0]['count'] > 0){
                $this->TrnReceiving->rollback();
                $this->Session->setFlash('返品確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('return_decision');
            }
            
            $this->TrnReceiving->commit();
            
            // 表示用データを取得
            $where = ' and a.id in ( ' . join(',',$this->request->data['TrnReturn']['id']) . ' )';
            $result = $this->getReturnData($where);
            $this->Session->write('Return.result' , $result);
            $this->Session->write('Return.ids' , $header_ids);
        }else{
            $result = $this->Session->read('Return.result');
            $header_ids = $this->Session->read('Return.ids');
        }
        $this->set('result',$result);
        $this->set('header_ids',$header_ids);
        $this->render('/returns/return_decision_result_view');
    }

    /**
     *  返品履歴
     *  @param
     *  @return array
     */
    function return_history($backFlg = true) {
        App::import('Sanitize');
        $this->setRoleFunction(34); //返品履歴
        //仕入先セレクトボックスの設定
        $this->set('facility_select',$this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                            array(Configure::read('FacilityType.supplier') ),
                                                            true,
                                                            array('facility_code' , 'facility_name'),
                                                            'facility_name, facility_code'
                                                            ));
        //施設セレクトボックスの設定
        $this->set('facility_from_select',$this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                                 array(Configure::read('FacilityType.hospital'),Configure::read('FacilityType.center')  ),
                                                                 false
                                                                 ));

        $result = array();
        if(isset($this->request->data['search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            //検索条件
            $where = '';
            //返品番号
            if($this->request->data['TrnReturnHeader']['work_no'] != '' ){
                if(isset($this->request->data['TrnReceiving']['yet'])){
                    //未確定も含む
                    $where .= " and ( b.work_no like '" . $this->request->data['TrnReturnHeader']['work_no'] . "%'";
                    $where .= " or b.work_no is null ) ";
                }else{
                    //未確定は除く
                    $where .= " and b.work_no like '" . $this->request->data['TrnReturnHeader']['work_no'] . "%'";
                }
            }
            //返品日From
            if($this->request->data['TrnReceiving']['date_select_start'] != '' ){
                if(isset($this->request->data['TrnReceiving']['yet'])){
                    //未確定も含む
                    $where .= " and ( b.work_date >= '" . $this->request->data['TrnReceiving']['date_select_start'] . "'";
                    $where .= " or b.work_date is null ) ";
                }else{
                    //未確定は除く
                    $where .= " and b.work_date >= '" . $this->request->data['TrnReceiving']['date_select_start'] . "'";
                }
            }

            //返品日To
            if($this->request->data['TrnReceiving']['date_select_end'] != '' ){
                if(isset($this->request->data['TrnReceiving']['yet'])){
                    //未確定も含む
                    $where .= " and ( b.work_date <= '" . $this->request->data['TrnReceiving']['date_select_end'] . "'";
                    $where .= " or b.work_date is null ) ";
                }else{
                    //未確定は除く
                    $where .= " and b.work_date <= '" . $this->request->data['TrnReceiving']['date_select_end'] . "'";
                }
            }

            //仕入先
            if($this->request->data['MstFacility']['facility_to_id'] != '' ){
                $where .= " and d1.facility_code = '" . $this->request->data['MstFacility']['facility_to_id'] . "'";
            }

            //施設
            if($this->request->data['MstFacility']['facility_from_id'] != '' ){
                $where .= " and d2.facility_code = '" . $this->request->data['MstFacility']['facility_from_id'] . "'";
            }

            //商品ID
            if($this->request->data['MstFacilityItem']['internal_code'] != '' ){
                $where .= " and f.internal_code like '" . $this->request->data['MstFacilityItem']['internal_code'] . "%'";
            }
            //製品番号
            if($this->request->data['MstFacilityItem']['item_code'] != '' ){
                $where .= " and f.item_code like '%" . $this->request->data['MstFacilityItem']['item_code'] . "%'";
            }
            //商品名
            if($this->request->data['MstFacilityItem']['item_name'] != '' ){
                $where .= " and f.item_name like '%" . $this->request->data['MstFacilityItem']['item_name'] . "%'";
            }
            //規格
            if($this->request->data['MstFacilityItem']['standard'] != '' ){
                $where .= " and f.standard like '%" . $this->request->data['MstFacilityItem']['standard'] . "%'";
            }
            //ロット
            if($this->request->data['TrnSticker']['lot_no'] != '' ){
                $where .= " and h.lot_no like '%" . $this->request->data['TrnSticker']['lot_no'] . "%'";
            }

            //販売元
            if($this->request->data['MstDealer']['dealer_name'] != '' ){
                $where .= " and f1.dealer_name like '%" . $this->request->data['MstDealer']['dealer_name'] . "%'";
            }
            //センターシール
            if($this->request->data['TrnSticker']['facility_sticker_no'] != '' ){
                $where .= " and h.facility_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] . "'";
            }
            //部署シール
            if($this->request->data['TrnSticker']['hospital_sticker_no'] != '' ){
                $where .= " and h.hospital_sticker_no = '" . $this->request->data['TrnSticker']['hospital_sticker_no'] . "'";
            }

            $sql  = ' select ';
            $sql .= '       b.id                                     as "Returns__id"';
            $sql .= '     , b.work_no                                as "Returns__work_no"';
            $sql .= '     , to_char(b.work_date , \'YYYY/mm/dd\')    as "Returns__work_date"';
            $sql .= '     , d1.facility_name                         as "Returns__facility_name_to"';
            $sql .= '     , d1.id                                    as "Returns__facility_id_to"';
            $sql .= '     , d2.facility_name                         as "Returns__facility_name_from"';
            $sql .= '     , d2.id                                    as "Returns__facility_id_from"';
            $sql .= "     , coalesce(b.detail_count ::varchar, '未確定') ";
            $sql .= '                                                as "Returns__detail_count"';
            $sql .= '     , count(*)                                 as "Returns__count"';
            $sql .= '     , g.user_name                              as "Returns__user_name"';
            $sql .= '     , b.recital                                as "Returns__recital"';
            $sql .= "     , to_char(b.created , 'YYYY/mm/dd hh24:mi:ss') ";
            $sql .= '                                                as "Returns__created"';
            $sql .= '   from ';
            $sql .= '     trn_receivings as a  ';
            $sql .= '     left join trn_return_headers as b  ';
            $sql .= '       on b.id = a.trn_return_header_id  ';
            $sql .= '     left join mst_departments as c1  ';
            $sql .= '       on c1.id = a.department_id_to  ';
            $sql .= '     left join mst_facilities as d1  ';
            $sql .= '       on d1.id = c1.mst_facility_id  ';
            $sql .= '     left join mst_departments as c2  ';
            $sql .= '       on c2.id = a.department_id_from  ';
            $sql .= '     left join mst_facilities as d2  ';
            $sql .= '       on d2.id = c2.mst_facility_id  ';
            $sql .= '     left join mst_item_units as e  ';
            $sql .= '       on e.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as f  ';
            $sql .= '       on f.id = e.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as f1  ';
            $sql .= '       on f1.id = f.mst_dealer_id  ';
            $sql .= '     left join mst_users as g  ';
            $sql .= '       on g.id = b.creater  ';
            $sql .= '     left join trn_stickers as h  ';
            $sql .= '       on h.id = a.trn_sticker_id  ';
            $sql .= '   where ';
            $sql .= '     a.receiving_type in (' . Configure::read('ReceivingType.return'). ', ' . Configure::read('ReceivingType.returnReceiving') .', ' . Configure::read('ReceivingType.returnStrage'). ', ' . Configure::read('ReceivingType.returnDeposit') . ')  ';
            $sql .= '     and a.is_deleted = false  ';
            $sql .= '     and f.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= $where;
            $sql .= '   group by ';
            $sql .= '     b.id ';
            $sql .= '     , b.work_no ';
            $sql .= '     , b.work_date ';
            $sql .= '     , b.detail_count ';
            $sql .= '     , b.recital ';
            $sql .= '     , b.created ';
            $sql .= '     , g.user_name ';
            $sql .= '     , d1.facility_name ';
            $sql .= '     , d2.facility_name ';
            $sql .= '     , d1.id ';
            $sql .= '     , d2.id ';
            $sql .= '   order by ';
            $sql .= '     b.id desc ';

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnReceiving'));

            if(isset($this->request->data['Return']['limit'])){
                $sql .= '  limit ' . $this->request->data['Return']['limit'];
            }
            $result = AppController::outputFilter($this->TrnReceiving->query($sql));
            $this->request->data = $data;

        }else{
            //初期パラメータ
            $this->request->data['TrnReceiving']['date_select_start'] = date("Y/m/d",mktime(0, 0, 0, date("m")  , date("d")-7, date("Y")));;
            $this->request->data['TrnReceiving']['date_select_end']   = date("Y/m/d");
            $this->request->data['TrnReceiving']['yet'] = true;
        }

        $this->set('result',$result);
        $this->render('/returns/return_history');
    }


    /**
     *  返品履歴 明細表示
     *  @param
     *  @return array
     */
    function return_history_detail() {
        $where = $this->_createReturnHistoryDetailWhere();
        $sql = $this->_createReturnHistoryDetailSQL($where);
        $result = $this->TrnSticker->query($sql);
        for($j=0;$j<count($result);$j++){
            if (isset($result[$j]['Returns']['work_date'])){
                if($result[$j]['Returns']['receiving_type'] == Configure::read('ReceivingType.returnDeposit') &&
                    $this->_isCloseType($result[$j]['Returns']['work_date'], $result[$j]['Returns']['updatable']) == true){
                    $result[$j]['Returns']['status'] = "締め済み";
                }
            } else {
                //未確定の場合は、締めチェックしない
                $result[$j]['Returns']['status'] = "";
            }
        }

        $this->set('result' , $result);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnReturn.token' , $token);
        $this->request->data['TrnReturn']['token'] = $token;

        $this->render('/returns/return_history_detail');
    }

    /**
     *  返品取消
     *  @param
     *  @return array
     */
    function return_history_result() {

        if($this->request->data['TrnReturn']['token'] == $this->Session->read('TrnReturn.token')) {
            $this->Session->delete('TrnReturn.token');
            $now = date('Y/m/d H:i:s.u');
            
            $targetDateAry = array();
            foreach($this->request->data['TrnReceiving']['id'] as $id){
                
                $params = array();
                $params['conditions'] = array('TrnReceiving.id'=>$id);
                $params['recursive']  = -1;
                $present_c = $this->TrnReceiving->find('first',$params);
                $targetDateAry[] = $present_c['TrnReceiving']['work_date'];
            }
            
            //仮締更新権限チェック
            //[選択した明細を全チェックする]
            $type = Configure::read('ClosePrivileges.update');
            if(false == $this->canUpdateAfterCloseByDateAry($type ,$targetDateAry, $this->Session->read('Auth.facility_id_selected'))){
                $this->redirect('return_history');
                return;
            }
            $return_list = array();
            $param = $this->_getHistoryParam();
            $param['order'] = array('TrnReceiving.trn_return_header_id');
            $param['conditions']['TrnReceiving.id'] = $this->request->data['TrnReceiving']['id'];
            $param['joins'][] = array(
                'type'=>'LEFT',
                'table'=>'trn_claims',
                'alias'=>'TrnClaim',
                'conditions'=>array(
                    'TrnClaim.trn_receiving_id = TrnReceiving.id',
                    'TrnClaim.is_deleted = false',
                    "TrnClaim.is_stock_or_sale = '1'"
                    )
                );
            $param['joins'][] = array(
                'type'=>'LEFT',
                'table'=>'trn_claims',
                'alias'=>'MsTrnClaim',
                'conditions'=>array(
                    'MsTrnClaim.trn_receiving_id = TrnReceiving.id',
                    'MsTrnClaim.is_deleted = false',
                    "MsTrnClaim.is_stock_or_sale = '3'"
                    )
                );
            
            $param['joins'][] = array(
                'type'=>'LEFT',
                'table'=>'trn_sticker_records',
                'alias'=>'TrnStickerRecord',
                'conditions'=>array(
                    'TrnStickerRecord.trn_sticker_id = TrnSticker.id',
                    'TrnStickerRecord.record_type' => Configure::read('RecordType.return'),
                    'TrnStickerRecord.record_id = TrnReceiving.id',
                    'TrnStickerRecord.is_deleted = false'
                    )
                );
            $param['joins'][] = array(
                'type'=>'LEFT',
                'table'=>'trn_receivings',
                'alias'=>'TrnReceivingBefore',
                'conditions'=>array(
                    'TrnReceivingBefore.id = TrnSticker.trn_adjust_id',
                    'TrnReceivingBefore.is_deleted = false'
                    )
                );
            $param['joins'][] = array(
                'type'=>'LEFT',
                'table'=>'trn_storages',
                'alias'=>'TrnStorageBefore',
                'conditions'=>array(
                    'TrnStorageBefore.trn_receiving_id = TrnReceivingBefore.id',
                    'TrnStorageBefore.trn_sticker_id = TrnReceivingBefore.trn_sticker_id',
                    'TrnStorageBefore.is_deleted = false'
                    )
                );
            $param['joins'][] = array(
                'type'=>'LEFT',
                'table'=>'trn_stickers',
                'alias'=>'TrnStickerBefore',
                'conditions'=>array(
                    'TrnStickerBefore.id = TrnReceivingBefore.trn_sticker_id',
                    'TrnStickerBefore.trn_receiving_id = TrnReceivingBefore.id',
                    'TrnStickerBefore.is_deleted = false'
                    )
                );
            $param['joins'][] = array(
                'type'=>'INNER',
                'table'=>'trn_stocks',
                'alias'=>'TrnStock',
                'conditions'=>array(
                    'TrnStock.mst_item_unit_id = TrnSticker.mst_item_unit_id',
                    'TrnStock.mst_department_id = MstDepartmentFrom.id',
                    'TrnStock.is_deleted = false'
                    )
                );

            $param['joins'][] = array(
                'type'=>'left',
                'table'=>'trn_stickers',
                'alias'=>'TrnStockSticker',
                'conditions'=>array(
                    'TrnStock.mst_item_unit_id = TrnStockSticker.mst_item_unit_id',
                    'TrnStock.mst_department_id = TrnStockSticker.mst_department_id',
                    "TrnStockSticker.lot_no= ' '" ,
                    'TrnStockSticker.is_deleted = false',
                    'TrnStockSticker.has_reserved = false'
                    )
                );
            $param['joins'][] = array(
                'type'=>'left',
                'table'=>'mst_item_units',
                'alias'=>'ITU',
                'conditions'=>array(
                    'TrnReceiving.mst_item_unit_id = ITU.id',
                    )
                );

            $param['fields'] = array(
                'TrnReturnHeader.id',
                'TrnReturnHeader.work_no',
                'TrnReturnHeader.work_date',
                'TrnReturnHeader.recital',
                'TrnReturnHeader.detail_count',
                'TrnReturnHeader.created',
                'MstFacilityItem.internal_code',
                'MstFacilityItem.item_name',
                'MstFacilityItem.item_code',
                'MstFacilityItem.standard',
                'MstFacilityItem.is_lowlevel',
                'TrnSticker.id',
                'TrnSticker.facility_sticker_no',
                'TrnSticker.hospital_sticker_no',
                'TrnSticker.lot_no',
                'TrnSticker.quantity',
                'TrnSticker.validated_date',
                'TrnSticker.trn_receiving_id',
                'TrnSticker.trn_return_receiving_id',
                'TrnSticker.modified',
                'TrnSticker.trn_adjust_id',
                'TrnReceiving.id',
                'TrnReceiving.quantity',
                'TrnReceiving.work_type',
                'TrnReceiving.stocking_price',
                'TrnReceiving.remain_count',
                'TrnReceiving.modified',
                'TrnReceiving.receiving_type',
                'TrnReceiving.stocking_close_type',
                'TrnReceiving.sales_close_type',
                'TrnReceiving.facility_close_type',
                'MstDealer.dealer_name',
                'MstUser.user_name',
                'MstFacilityFrom.facility_name',
                'MstFacilityTo.facility_name',
                'TrnClaim.id',
                'TrnClaim.stocking_close_type',
                'TrnClaim.sales_close_type',
                'TrnClaim.facility_close_type',
                'MsTrnClaim.id',
                'MsTrnClaim.stocking_close_type',
                'MsTrnClaim.sales_close_type',
                'MsTrnClaim.facility_close_type',
                'TrnStickerRecord.id',
                'TrnReceivingBefore.id',
                'TrnReceivingBefore.remain_count',
                'TrnReceivingBefore.quantity',
                'TrnStock.id',
                'TrnStock.stock_count',
                'TrnStock.reserve_count',
                'TrnStock.receipted_count',
                'TrnStickerBefore.id',
                'TrnStickerBefore.quantity',
                'TrnStorageBefore.id',
                'TrnStorageBefore.quantity',
                'TrnStockSticker.id',
                'TrnStockSticker.quantity',
                'ITU.per_unit',
                );
            $this->TrnReceiving->unbindModel(array('belongsTo' => array('MstItemUnit'),'hasOne' => array('TrnClaim')));
            $return_list = $this->TrnReceiving->find('all', $param);

            //トランザクション開始
            $this->TrnSticker->begin();
            //行ロック
            $this->TrnReceiving->query(' select * from trn_receivings as a where a.id in (' .join(',' , $this->request->data['TrnReceiving']['id']) .') for update ');

            //更新チェック
            $ret = $this->TrnReceiving->query(" select count(*) from trn_receivings as a where a.id in (" .join(',' , $this->request->data['TrnReceiving']['id']) .") and a.modified > '" . $this->request->data['TrnReturn']['time'] ."'");
            if($ret[0][0]['count'] > 0){
                $errorFlg=true;
            }

            $errorFlg = false;
            $receivingIdList = array();
            $count = array();
            $msg = "";

            foreach($return_list as $list){
                if($list['TrnSticker']['modified'] !== $this->request->data['TrnSticker']['modified'][$list['TrnSticker']['id']]){
                    $errorFlg = true;
                    $msg = "別ユーザによってデータが更新されています";
                    $this->set('error', true);
                }

                /******************************************************
                 * 返品取消処理
                 ******************************************************/

                //現在の在庫数取得
                $stock_count = $this->TrnStock->find('all',array('conditions' => array('TrnStock.id'=> ($list['TrnStock']['id']))));
                
                if(!empty($list['TrnReturnHeader']['id'])) {
                    /*  返品確定したものの取消処理  */
                    $param = array();
                    $TrnStickerId = $list['TrnSticker']['id'];
                    if($list['MstFacilityItem']['is_lowlevel'] == true){
                        //低レベル品の場合、返品用シールの削除フラグをtrue
                        $param = array(
                            'is_deleted' => 'true'
                            );
                    }else if($list['TrnSticker']['facility_sticker_no'] == ""
                             && $list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnStrage')){
                        //川鉄シールの場合(センターシールが空文字、且つ入庫済返品)
                        $param = array(
                            'is_deleted' => 'true'
                            );
                    }else{
                        //通常品の場合
                        if($list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnDeposit')){
                            //預託品
                            $param['quantity'] = $list['TrnReceiving']['remain_count'];
                        }else if($list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnStrage')){
                            //入庫
                            $param['quantity'] = $list['TrnReceiving']['quantity'] + $list['TrnSticker']['quantity'];
                        }else{
                            //入荷
                            $param['is_deleted'] = 'true';
                        }

                        //数量・入荷(返品)参照キーを元に戻す
                        $param['trn_return_header_id']    = null;
                        $param['trn_return_receiving_id'] = null;
                        $param['has_reserved']            = false;
                        $param['modifier']                = $this->Session->read('Auth.MstUser.id');
                        $param['modified']                = "'".$now."'";
                    }

                    $this->TrnSticker->recursive = -1;
                    $res2 = $this->TrnSticker->updateAll($param,array('TrnSticker.id' => $TrnStickerId),-1);
                    if(!$res2){
                        $errorFlg = true;
                    }

                    $TrnReceivingId = $list['TrnReceiving']['id'];
                    $receivingIdList[] = $TrnReceivingId;
                    //入荷(返品)の削除フラグをtrue
                    $param = array(
                        'is_deleted' => 'true',
                        'modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'modified'   => "'".$now."'"
                        );
                    if(isset($this->request->data['TrnReceiving']['modified'][$TrnReceivingId])){
                        $param['recital'] = "'".$this->request->data['TrnReceiving']['recital'][$TrnReceivingId]."'";
                    }
                    $this->TrnReceiving->recursive = -1;
                    $res3 = $this->TrnReceiving->updateAll($param,array('TrnReceiving.id' => $TrnReceivingId),-1);
                    if(!$res3){
                        $errorFlg = true;
                    }

                    //TrnReturnHeaderの削除フラグ・数量を更新
                    $TrnReturnHeaderId = $list['TrnReturnHeader']['id'];
                    if(!isset($count[$TrnReturnHeaderId]) && isset($TrnReturnHeaderId) && $TrnReturnHeaderId !== ""){
                        $detail_count = $this->TrnReceiving->find('count',array(
                            'conditions' => array(
                                'TrnReceiving.trn_return_header_id' => $TrnReturnHeaderId,
                                'TrnReceiving.id' => $this->request->data['TrnReceiving']['id']
                                )
                            ));

                        $count[$TrnReturnHeaderId] = (int)$list['TrnReturnHeader']['detail_count'] - $detail_count;
                    }

                    if($count[$TrnReturnHeaderId] <= 0){
                        $delete = 'true';
                    }else{
                        $delete = 'false';
                    }
                    $this->TrnReturnHeader->recursive = -1;
                     $res4 = $this->TrnReturnHeader->updateAll(array(
                        'is_deleted' => $delete,
                        'modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'modified'   => "'".$now."'"
                        ),array(
                            'TrnReturnHeader.id' => $TrnReturnHeaderId
                            ),-1);
                    if(!$res4){
                        $errorFlg = true;
                    }

                    //TrnClaimの削除フラグを更新
                    $TrnClaimId = $list['TrnClaim']['id'];
                    if(isset($TrnClaimId) && $TrnClaimId !== ""){
                        $this->TrnClaim->recursive = -1;
                        $res5 = $this->TrnClaim->updateAll(array(
                            'is_deleted' => 'true',
                            'modifier'   => $this->Session->read('Auth.MstUser.id'),
                            'modified'   => "'".$now."'"
                            ),array(
                                'TrnClaim.id' => $TrnClaimId
                                ),-1);
                        if(!$res5){
                            $errorFlg = true;
                        }
                    }

                    //MsTrnClaimの削除フラグを更新
                    $TrnClaimId = $list['MsTrnClaim']['id'];
                    if(isset($TrnClaimId) && $TrnClaimId !== ""){
                        $this->TrnClaim->recursive = -1;
                        $res5 = $this->TrnClaim->updateAll(array(
                            'is_deleted' => 'true',
                            'modifier'   => $this->Session->read('Auth.MstUser.id'),
                            'modified'   => "'".$now."'"
                            ),array(
                                'TrnClaim.id' => $TrnClaimId
                                ),-1);
                        if(!$res5){
                            $errorFlg = true;
                        }
                    }
                    

                    //TrnStickerRecordの削除フラグ更新
                    $TrnStickerRecordId = $list['TrnStickerRecord']['id'];
                    if(isset($TrnStickerRecordId) && $TrnStickerRecordId !== ""){
                        $this->TrnStickerRecord->recursive = -1;
                        $res6 = $this->TrnStickerRecord->updateAll(array(
                            'is_deleted' => 'true',
                            'modifier'   => $this->Session->read('Auth.MstUser.id'),
                            'modified'   => "'" . $now . "'"
                            ),array(
                                'TrnStickerRecord.id' => $TrnStickerRecordId
                                ),-1);
                        if(!$res6){
                            $errorFlg = true;
                        }
                    }

                    if($list['MstFacilityItem']['is_lowlevel'] == true) {
                        //低レベル品の場合、入荷の数量を更新
                        $TrnReceivingId = $list['TrnReceivingBefore']['id'];
                        $this->TrnReceiving->recursive = -1;
                        $res7 = $this->TrnReceiving->updateAll(array(
                            'remain_count' => $list['TrnReceivingBefore']['remain_count'] + $list['TrnReceiving']['quantity'],
                            'modifier'   => $this->Session->read('Auth.MstUser.id'),
                            'modified'   => "'" . $now . "'"
                            ),array(
                                'TrnReceiving.id' => $TrnReceivingId
                                ),-1);
                        if(!$res7){
                            $errorFlg = true;
                        }

                        //低レベル品の場合、シールの数量を更新
                        $TrnStickerId = $list['TrnStickerBefore']['id'];
                        $this->TrnSticker->recursive = -1;
                        $res8 = $this->TrnSticker->updateAll(array(
                            'quantity' => $list['TrnStickerBefore']['quantity'] + ( $list['TrnReceiving']['quantity'] * $list['ITU']['per_unit']),
                            'modifier'   => $this->Session->read('Auth.MstUser.id'),
                            'modified'   => "'" . $now . "'"
                            ),array(
                                '   TrnSticker.id' => $TrnStickerId
                                ),-1);
                        if(!$res8){
                            $errorFlg = true;
                        }
                    }else if($list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnReceiving')){
                        //通常品且つ入荷返品場合
                        if(!empty($list['TrnSticker']['trn_adjust_id'])){
                            $TrnReceivingId = $list['TrnSticker']['trn_adjust_id'];

                            $this->TrnReceiving->recursive = -1;
                            $res8 = $this->TrnReceiving->updateAll(array(
                                'remain_count' => $list['TrnReceivingBefore']['remain_count'] + $list['TrnReceiving']['quantity'],
                                'modifier'     => $this->Session->read('Auth.MstUser.id'),
                                'modified'     => "'" . $now . "'"
                                ),array(
                                    'TrnReceiving.id' => $TrnReceivingId
                                    ),-1);
                            if(!$res8){
                                $errorFlg = true;
                            }
                        }
                        // 在庫用のシールレコードを更新
                        $stockfileds = array();
                        $stockfileds = array(
                            'quantity' => "quantity + {$list['TrnReceiving']['quantity']}",
                            'modifier' => $this->Session->read('Auth.MstUser.id'),
                            "modified" => "'".$now."'"
                            );
                        $this->TrnSticker->recursive = -1;
                        $resS = $this->TrnSticker->updateAll($stockfileds,array('TrnSticker.id' => $list['TrnStockSticker']['id']),-1);
                        if(!$resS){
                            $errorFlg = true;
                        }


                    }else if($list['TrnSticker']['facility_sticker_no'] == ""
                             && $list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnStrage')){
                        //川鉄シールの場合、シール数量戻し
                        $TrnStickerId = $list['TrnSticker']['trn_receiving_id'];
                        $this->TrnSticker->recursive = -1;
                        $res8 = $this->TrnSticker->updateAll(array(
                            'quantity' => 'quantity + '.$list['TrnReceiving']['quantity'],
                            'modifier' => $this->Session->read('Auth.MstUser.id'),
                            'modified' => "'" . $now . "'"
                            ),array(
                                'TrnSticker.id' => $TrnStickerId
                                ),-1);
                        if(!$res8){
                            $errorFlg = true;
                        }
                    }

                    //在庫
                    if($list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnReceiving')){
                        //入荷返品
                        if($list['MstFacilityItem']['is_lowlevel'] == true){
                            //低レベル品の場合、在庫数を増やす
                            $stock_count = $stock_count[0]['TrnStock']['stock_count'] + ( $list['TrnReceiving']['remain_count'] * $list['ITU']['per_unit']);
                            $fields =array(
                                'stock_count' => $stock_count,
                                'modifier'    => $this->Session->read('Auth.MstUser.id'),
                                'modified'    => "'" . $now ."'"
                                );
                        }else{
                            $receipted_count = $stock_count[0]['TrnStock']['receipted_count'] + $list['TrnReceiving']['remain_count'];
                            $fields =array(
                                'receipted_count' => $receipted_count,
                                'modifier' => $this->Session->read('Auth.MstUser.id'),
                                'modified' => "'" . $now . "'"
                                );
                        }
                    }else if($list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnStrage')){
                        //入庫返品
                        $stock = $stock_count[0]['TrnStock']['stock_count'] + $list['TrnReceiving']['remain_count'];
                        $fields =array(
                            'stock_count' => $stock,
                            'modifier'    => $this->Session->read('Auth.MstUser.id'),
                            'modified'    => "'" . $now . "'" ,
                            );
                    }else{
                        //預託品返品
                        $stock = $stock_count[0]['TrnStock']['stock_count'] + $list['TrnReceiving']['remain_count'];
                        $fields =array(
                            'stock_count' => $stock,
                            'modifier' => $this->Session->read('Auth.MstUser.id'),
                            'modified' => "'" . $now . "'"
                            );
                    }
                    $this->TrnStock->recursive = -1;
                    $res9 = $this->TrnStock->updateAll($fields,array('TrnStock.id' => $list['TrnStock']['id']),-1);
                    if(!$res9){
                        $errorFlg = true;
                    }


                }else{
                    /*  返品未確定(登録のみ)の取消処理  */
                    $TrnReceivingId = $list['TrnReceiving']['id'];
                    //入荷テーブル戻し
                    if($list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnReceiving')){
                        //入荷返品→入荷テーブルの残数をプラス
                        $remain_count = $list['TrnReceivingBefore']['remain_count'] + $list['TrnSticker']['quantity'];
                        $fields =array(
                            'remain_count' => $remain_count,
                            'modifier'     => $this->Session->read('Auth.MstUser.id'),
                            "modified"     => "'".$now."'"
                            );
                        $this->TrnReceiving->recursive = -1;
                        $resA = $this->TrnReceiving->updateAll($fields,array('TrnReceiving.id' => $list['TrnSticker']['trn_adjust_id']),-1);
                        if(!$resA){
                            $errorFlg = true;
                        }

                        // 在庫用のシールレコードを更新
                        $stockfileds = array();
                        $stockfileds = array(
                            'quantity' => "quantity + {$list['TrnSticker']['quantity']}",
                            'modifier' => $this->Session->read('Auth.MstUser.id'),
                            "modified" => "'".$now."'"
                            );
                        $this->TrnSticker->recursive = -1;
                        $resS = $this->TrnSticker->updateAll($stockfileds,array('TrnSticker.id' => $list['TrnStockSticker']['id']),-1);
                        if(!$resS){
                            $errorFlg = true;
                        }


                        $fields =array();
                        $fields =array(
                            'is_deleted' => "true",
                            'modifier'   => $this->Session->read('Auth.MstUser.id'),
                            "modified"   => "'".$now."'"
                            );
                    }else{
                        //入庫返品、預託品返品→作成した返品明細に削除フラグ
                        $fields =array(
                            'is_deleted' => "true",
                            'modifier' => $this->Session->read('Auth.MstUser.id'),
                            "modified" => "'".$now."'"
                            );
                    }

                    $this->TrnReceiving->recursive = -1;
                    $resA = $this->TrnReceiving->updateAll($fields,array('TrnReceiving.id' => $TrnReceivingId),-1);
                    if(!$resA){
                        $errorFlg = true;
                    }

                    //在庫テーブル戻し(入荷返品は在庫を変更していないので戻し処理なし)
                    if($list['TrnReceiving']['receiving_type'] !== Configure::read('ReceivingType.returnReceiving')){
                        //入庫返品、預託品返品→在庫予約数マイナス
                        $reserve_count = $stock_count[0]['TrnStock']['reserve_count'] - $list['TrnSticker']['quantity'];
                        $fields =array(
                            'reserve_count' => $reserve_count,
                            'modifier' => $this->Session->read('Auth.MstUser.id'),
                            "modified" => "'".$now."'"
                            );
                        $this->TrnStock->recursive = -1;
                        $resB = $this->TrnStock->updateAll($fields,array('TrnStock.id' => $list['TrnStock']['id']),-1);
                        if(!$resB){
                            $errorFlg = true;
                        }
                    }

                    //シールテーブル戻し
                    if($list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnReceiving')){
                        //元シールの数量増加
                        $fields = array();
                        if($list['MstFacilityItem']['is_lowlevel']){
                            //低レベル品
                            $return_count = $list['TrnSticker']['quantity'] * $list['ITU']['per_unit'];
                        }else{
                            //通常品
                            $return_count = $list['TrnSticker']['quantity'];
                        }

                        $quantity = $list['TrnStickerBefore']['quantity'] + $return_count;
                        $fields = array(
                            'quantity' => $quantity,
                            'modifier' => $this->Session->read('Auth.MstUser.id'),
                            "modified" => "'".$now."'"
                            );
                        $this->TrnSticker->recursive = -1;
                        $resC = $this->TrnSticker->updateAll($fields,array('TrnSticker.id' => $list['TrnStickerBefore']['id']),-1);
                        if(!$resC){
                            $errorFlg = true;
                        }

                        //低レベル品の場合。予約数の戻し
                        if($list['MstFacilityItem']['is_lowlevel']){
                            $fields = array(
                                'reserve_count' => 'reserve_count - ' . ( $list['TrnSticker']['quantity'] * $list['ITU']['per_unit']),
                                'modifier'      => $this->Session->read('Auth.MstUser.id'),
                                "modified"      => "'".$now."'"
                                );
                            $this->TrnStock->recursive = -1;
                            $resC = $this->TrnStock->updateAll($fields,array('TrnStock.id' => $list['TrnStock']['id']),-1);
                            if(!$resC){
                                $errorFlg = true;
                            }
                        }
                        //入荷返品→登録処理で作成したレコードに削除フラグ
                        $fields = array();
                        $fields = array(
                            'is_deleted' => "true",
                            'modifier'   => $this->Session->read('Auth.MstUser.id'),
                            "modified"   => "'".$now."'"
                            );
                    }else if($list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnStrage')){
                        /* 入庫返品→通常品と低レベル品と川鉄シールで場合わけ */
                        if($list['MstFacilityItem']['is_lowlevel']) {
                            //低レベル品→元シールの数量プラス・作成シール削除
                            $quantity = $list['TrnStickerBefore']['quantity'] + ( $list['TrnSticker']['quantity'] * $list['ITU']['per_unit']);
                            $fields = array(
                                'quantity' => $quantity,
                                'modifier' => $this->Session->read('Auth.MstUser.id'),
                                "modified" => "'".$now."'"
                                );
                            //元シールの数量増加
                            $this->TrnSticker->recursive = -1;
                            $resC = $this->TrnSticker->updateAll($fields,array('TrnSticker.id' => $list['TrnStickerBefore']['id']),-1);
                            if(!$resC){
                                $errorFlg = true;
                            }
                            //予約数の戻し
                            $fields = array(
                                'reserve_count' => 'reserve_count - ' . ( $list['TrnSticker']['quantity'] * $list['ITU']['per_unit']),
                                'modifier'      => $this->Session->read('Auth.MstUser.id'),
                                "modified"      => "'".$now."'"
                                );
                            $this->TrnStock->recursive = -1;
                            $resC = $this->TrnStock->updateAll($fields,array('TrnStock.id' => $list['TrnStock']['id']),-1);
                            if(!$resC){
                                $errorFlg = true;
                            }


                            $fields =array();
                            $fields = array(
                                'is_deleted' => "false",
                                'modifier'   => $this->Session->read('Auth.MstUser.id'),
                                "modified"   => "'".$now."'"
                                );
                        }else if($list['TrnSticker']['facility_sticker_no'] == ""
                                 && $list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnStrage')){
                            //川鉄シールの場合
                            $fields = array(
                                'quantity' => 'quantity + '.(int)$list['TrnSticker']['quantity'],
                                'modifier' => $this->Session->read('Auth.MstUser.id'),
                                "modified" => "'".$now."'"
                                );
                            //元シールの数量増加
                            $this->TrnSticker->recursive = -1;
                            $resC = $this->TrnSticker->updateAll($fields,array('TrnSticker.id' => $list['TrnSticker']['trn_receiving_id']),-1);
                            if(!$resC){
                                $errorFlg = true;
                            }
                            $fields =array();
                            $fields = array(
                                'is_deleted' => "false",
                                'modifier'   => $this->Session->read('Auth.MstUser.id'),
                                "modified"   => "'".$now."'"
                                );
                        }else{
                            //通常品→予約フラグfalse、返品参照キー削除
                            $fields = array(
                                'has_reserved' => "false",
                                'trn_return_receiving_id' => null,
                                'modifier' => $this->Session->read('Auth.MstUser.id'),
                                "modified" => "'" .$now. "'"
                                );
                        }
                    }else if($list['TrnReceiving']['receiving_type'] == Configure::read('ReceivingType.returnDeposit')){
                        //預託品返品→予約フラグfalse、返品参照キー削除
                        $fields = array(
                            'has_reserved' => "false",
                            'trn_return_receiving_id' => null,
                            'modifier' => $this->Session->read('Auth.MstUser.id'),
                            "modified" => "'".$now."'"
                            );
                    }
                    $this->TrnSticker->recursive = -1;
                    $resD = $this->TrnSticker->updateAll($fields,array('TrnSticker.id' => $list['TrnSticker']['id']),-1);
                    if(!$resD){
                        $errorFlg = true;
                    }
                }//返品確定と未確定の処理わけ終了
            }//end of foreach

            $errors = array();
            $errors = array_merge(
                $errors,
                is_array($this->validateErrors($this->TrnReturnHeader)) ? $this->validateErrors($this->TrnReturnHeader) : array(),
                is_array($this->validateErrors($this->TrnSticker)) ? $this->validateErrors($this->TrnSticker) : array(),
                is_array($this->validateErrors($this->TrnStock)) ? $this->validateErrors($this->TrnStock) : array(),
                is_array($this->validateErrors($this->TrnReceiving)) ? $this->validateErrors($this->TrnReceiving) : array(),
                is_array($this->validateErrors($this->TrnStickerRecord)) ? $this->validateErrors($this->TrnStickerRecord) : array(),
                is_array($this->validateErrors($this->TrnClaim)) ? $this->validateErrors($this->TrnClaim) : array()
                );
            if(count($errors) > 0){
                $errorFlg = true;
            }

            //コミット直前に更新チェック
            $ret = $this->TrnReceiving->query(" select count(*) from trn_receivings as a where a.id in (" .join(',' , $this->request->data['TrnReceiving']['id']) .") and a.modified > '" . $this->request->data['TrnReturn']['time'] . "' and a.modified <> '" . $now ."'");
            if($ret[0][0]['count'] > 0){
                $errorFlg=true;
            }

            if(!$errorFlg){
                $this->TrnSticker->commit();
            }else{
                $this->TrnSticker->rollback();
                $this->Session->setFlash($msg, 'growl', array('type'=>'error') );
                $this->Session->delete('history_sarch');
                $this->return_history(false);
                return;
            }
            $this->set('receivingIdList',$receivingIdList);
            $this->set('data',$this->request->data);

            //返品作業区分
            $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));

            $param = array();
            $param['conditions']['TrnReceiving.id'] = $this->request->data['TrnReceiving']['id'];
            $param['fields'] = array(
                'TrnReturnHeader.id'
                ,'TrnReturnHeader.work_no'
                ,'to_char("TrnReturnHeader"."work_date",\'YYYY/mm/dd\') as "TrnReturnHeader__work_date"'
                ,'TrnReturnHeader.recital'
                ,'TrnReturnHeader.detail_count'
                ,'TrnReturnHeader.created'
                ,'MstFacilityItem.internal_code'
                ,'MstFacilityItem.item_name'
                ,'MstFacilityItem.item_code'
                ,'MstFacilityItem.standard'
                ,'TrnSticker.facility_sticker_no'
                ,'TrnSticker.hospital_sticker_no'
                ,'TrnSticker.lot_no'
                ,'to_char("TrnSticker"."validated_date",\'YYYY/mm/dd\') as "TrnSticker__validated_date"'
                ,'TrnReceiving.id'
                ,'TrnReceiving.quantity'
                ,'TrnReceiving.remain_count'
                ,'TrnReceiving.work_type'
                ,'TrnReceiving.stocking_price'
                ,'TrnReceiving.modified'
                ,'TrnReceiving.recital'
                ,'MstDealer.dealer_name'
                ,'MstUser.user_name'
                ,'MstFacilityFrom.facility_name'
                ,'MstFacilityTo.facility_name'
            );

            $param['joins'][] = array('type'=>'LEFT'
                                      ,'table'=>"trn_return_headers"
                                      ,'alias'=>"TrnReturnHeader"
                                      ,'conditions'=>"TrnReturnHeader.id = TrnReceiving.trn_return_header_id");

            $param['joins'][] = array('type'=>'INNER'
                                      ,'table'=>'mst_item_units'
                                      ,'alias'=>'MstItemUnit'
                                      ,'conditions'=>array('MstItemUnit.id = TrnReceiving.mst_item_unit_id'
                                                           ,'MstItemUnit.is_deleted = false'));

            $param['joins'][] = array('type'=>'INNER'
                                      ,'table'=>'mst_facility_items'
                                      ,'alias'=>'MstFacilityItem'
                                      ,'conditions'=>array('MstFacilityItem.id = MstItemUnit.mst_facility_item_id'
                                                           ,'MstFacilityItem.is_deleted = false'));

            $param['joins'][] = array('type'=>'INNER'
                                      ,'table'=>'mst_departments'
                                      ,'alias'=>'MstDepartmentFrom'
                                      ,'conditions'=>array('TrnReceiving.department_id_from = MstDepartmentFrom.id'
                                                           ,'MstDepartmentFrom.is_deleted = false'));

            $param['joins'][] = array('type'=>'INNER'
                                      ,'table'=>'mst_facilities'
                                      ,'alias'=>'MstFacilityFrom'
                                      ,'conditions'=>array('MstDepartmentFrom.mst_facility_id = MstFacilityFrom.id'
                                                           ,'MstFacilityItem.is_deleted = false'));

            $param['joins'][] = array('type'=>'INNER'
                                      ,'table'=>'mst_departments'
                                      ,'alias'=>'MstDepartmentTo'
                                      ,'conditions'=>array('MstDepartmentTo.id = TrnReceiving.department_id_to'
                                                           ,'MstDepartmentTo.is_deleted = false'));

            $param['joins'][] = array('type'=>'INNER'
                                      ,'table'=>'mst_facilities'
                                      ,'alias'=>'MstFacilityTo'
                                      ,'conditions'=>array('MstDepartmentTo.mst_facility_id = MstFacilityTo.id'
                                                           ,'MstFacilityTo.is_deleted = false'));

            $param['joins'][] = array('type'=>'INNER'
                                      ,'table'=>'trn_stickers'
                                      ,'alias'=>'TrnSticker'
                                      ,'conditions'=>array('TrnSticker.id = TrnReceiving.trn_sticker_id'));

            $param['joins'][] = array('type'=>'LEFT'
                                      ,'table'=>'mst_dealers'
                                      ,'alias'=>'MstDealer'
                                      ,'conditions'=>array('MstDealer.id = MstFacilityItem.mst_dealer_id'
                                                           ,'MstDealer.is_deleted = false'));

            $param['joins'][] = array('type'=>'LEFT'
                                      ,'table'=>'mst_users'
                                      ,'alias'=>'MstUser'
                                      ,'conditions'=>array('MstUser.id = TrnReceiving.modifier'));

            $param['order'] = array('TrnReturnHeader.id','TrnReceiving.id');

            $param['recursive']  = -1;

            $this->TrnReceiving->create();
            $ReturnList = AppController::outputFilter($this->TrnReceiving->find('all', $param));

            $this->set('ReturnList',$ReturnList);
            $this->Session->write('TrnReturn.ReturnList' , $ReturnList);

            if($errorFlg){
                $this->set('msg','返品の取消に失敗しました');
                $this->Session->write('TrnReturn.msg' ,'返品の取消に失敗しました');
            }else{
                $this->set('msg','返品を取消しました');
                $this->Session->write('TrnReturn.msg' ,'返品を取消しました');
            }
            $this->set('data',$this->request->data);
            $this->Session->write('TrnReturn.data', $this->request->data);
        }else{
            $this->set('data' , $this->Session->read('TrnReturn.data'));
            $this->set('ReturnList' , $this->Session->read('TrnReturn.ReturnList'));
            $this->set('msg' , $this->Session->read('TrnReturn.msg'));
            //返品作業区分
            $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected') , 13));
        }
        $this->render('/returns/return_history_result_view');
    }


    /**
     *  条件情報取得
     *  @param
     *  @return array
     */
    function _getHistoryParam(){

        $param['conditions'] = array();
        $param['joins'][] = array('type'=>'LEFT'
                                  ,'table'=>'trn_return_headers'
                                  ,'alias'=>'TrnReturnHeader'
                                  ,'conditions'=>array('TrnReturnHeader.id = TrnReceiving.trn_return_header_id'
                                                       ,'TrnReturnHeader.is_deleted <> true'));
        $param['joins'][] = array('type'=>'INNER'
                                  ,'table'=>'mst_item_units'
                                  ,'alias'=>'MstItemUnit'
                                  ,'conditions'=>array('MstItemUnit.id = TrnReceiving.mst_item_unit_id'
                                                       ,'MstItemUnit.mst_facility_id' => $this->Session->read('Auth.facility_id_selected')
                                                       ,'MstItemUnit.is_deleted = false'));

        $param['joins'][] = array('type'=>'INNER'
                                  ,'table'=>'mst_facility_items'
                                  ,'alias'=>'MstFacilityItem'
                                  ,'conditions'=>array('MstFacilityItem.id = MstItemUnit.mst_facility_item_id'
                                                       ,'MstFacilityItem.is_deleted = false'));
        $param['joins'][] = array('type'=>'INNER'
                                  ,'table'=>'mst_departments'
                                  ,'alias'=>'MstDepartmentFrom'
                                  ,'conditions'=>array('TrnReceiving.department_id_from = MstDepartmentFrom.id'
                                                       ,'MstDepartmentFrom.is_deleted = false'));
        $param['joins'][] = array('type'=>'INNER'
                                  ,'table'=>'mst_facilities'
                                  ,'alias'=>'MstFacilityFrom'
                                  ,'conditions'=>array('MstDepartmentFrom.mst_facility_id = MstFacilityFrom.id'
                                                       ,'MstFacilityItem.is_deleted = false'));
        $param['joins'][] = array('type'=>'INNER'
                                  ,'table'=>'mst_departments'
                                  ,'alias'=>'MstDepartmentTo'
                                  ,'conditions'=>array('MstDepartmentTo.id = TrnReceiving.department_id_to'
                                                       ,'MstDepartmentTo.is_deleted = false'));
        $param['joins'][] = array('type'=>'INNER'
                                  ,'table'=>'mst_facilities'
                                  ,'alias'=>'MstFacilityTo'
                                  ,'conditions'=>array('MstDepartmentTo.mst_facility_id = MstFacilityTo.id'
                                                       ,'MstFacilityTo.is_deleted = false'));

        $param['joins'][] = array('type'=>'left'
                                  ,'table'=>'trn_stickers'
                                  ,'alias'=>'TrnSticker'
                                  ,'conditions'=>array('TrnSticker.trn_return_receiving_id = TrnReceiving.id'
                                                       ,'TrnSticker.is_deleted = false'));
        $param['joins'][] = array('type'=>'LEFT'
                                  ,'table'=>'mst_dealers'
                                  ,'alias'=>'MstDealer'
                                  ,'conditions'=>array('MstDealer.id = MstFacilityItem.mst_dealer_id'
                                                       ,'MstDealer.is_deleted = false'));

        $param['joins'][] = array('type'=>'inner'
                                  ,'table'=>'mst_user_belongings'
                                  ,'alias'=>'MstUserBelonging'
                                  ,'conditions'=>array('MstUserBelonging.mst_facility_id = MstFacilityFrom.id'
                                                       ,'MstUserBelonging.mst_user_id'=>$this->Session->read('Auth.MstUser.id')
                                                       ,'MstUserBelonging.is_deleted = false ' )
                                  );

        return $param;
    }


    public function report($reportType){
        switch($reportType){
          case 'return':
            $this->print_return();
            break;
          default:
            break;
        }
    }


    //返品明細書印刷
    function print_return() {
        $now = date('Y/m/d H:i:s.u');
        // 絞込み条件ヘッダID
        $where = " and a.id in ( " . join(',', $this->request->data['Returns']['id'] ) . " ) ";
        
        $sql  = ' select ';
        $sql .= '       a.id                       as "TrnReturn__id" ';
        $sql .= '     , a.work_no                  as "TrnReturn__work_no" ';
        $sql .= "     , to_char(a.work_date,'YYYY年MM月dd日')";
        $sql .= '                                  as "TrnReturn__work_date" ';
        $sql .= '     , a.recital                  as "TrnReturn__header_recital" ';
        $sql .= '     , a.printed_date             as "TrnReturn__printed_date"';
        $sql .= '     , d.internal_code            as "TrnReturn__internal_code" ';
        $sql .= '     , d.item_name                as "TrnReturn__item_name" ';
        $sql .= '     , d.item_code                as "TrnReturn__item_code" ';
        $sql .= '     , d.standard                 as "TrnReturn__standard" ';
        $sql .= "     , ( case when c.per_unit = 1 then c1.unit_name ";
        $sql .= "         else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')' ";
        $sql .= '         end )                    as "TrnReturn__unit_name"';
        $sql .= '     , e.id                       as "TrnReturn__owner_id" ';
        $sql .= '     , e.facility_formal_name     as "TrnReturn__owner_name" ';
        $sql .= "     ,'〒' || coalesce(e.zip,'') || ' ' || coalesce(e.address,'') || '\nTel:' || coalesce(e.tel,'') || '／Fax:' || coalesce(e.fax,'') ";
        $sql .= '                                  as "TrnReturn__owner_address" ';
        $sql .= '     , f.dealer_name              as "TrnReturn__dealer_name" ';
        $sql .= '     , b.recital                  as "TrnReturn__recital" ';
        $sql .= '     , b.stocking_price           as "TrnReturn__price" ';
        $sql .= '     , b.quantity                 as "TrnReturn__quantity" ';
        $sql .= '     , g.lot_no                   as "TrnReturn__lot_no" ';
        $sql .= '     , i2.facility_formal_name    as "TrnReturn__facility_name_to" ';
        $sql .= '     , h2.facility_formal_name    as "TrnReturn__facility_name_from" ';
        $sql .= "     ,'〒' || coalesce(h2.zip,'') || ' ' || coalesce(h2.address,'') || '\nTel:' || coalesce(h2.tel,'') || '／Fax:' || coalesce(h2.fax,'') ";
        $sql .= '                                  as "TrnReturn__from_address" ';
        $sql .= '   from ';
        $sql .= '     trn_return_headers as a ';
        $sql .= '     left join trn_receivings as b ';
        $sql .= '       on b.trn_return_header_id = a.id ';
        $sql .= '     left join mst_item_units as c ';
        $sql .= '       on c.id = b.mst_item_unit_id ';
        $sql .= '     left join mst_unit_names as c1 ';
        $sql .= '       on c.mst_unit_name_id = c1.id ';
        $sql .= '     left join mst_unit_names as c2 ';
        $sql .= '       on c.per_unit_name_id = c2.id ';
        $sql .= '     left join mst_facility_items as d ';
        $sql .= '       on d.id = c.mst_facility_item_id ';
        $sql .= '     left join mst_facilities as e ';
        $sql .= '       on e.id = d.mst_owner_id ';
        $sql .= '     left join mst_dealers as f ';
        $sql .= '       on f.id = d.mst_dealer_id ';
        $sql .= '     left join trn_stickers as g ';
        $sql .= '       on g.trn_return_receiving_id = b.id ';
        $sql .= '     left join mst_departments as h1 ';
        $sql .= '       on h1.id = b.department_id_from ';
        $sql .= '     left join mst_facilities as h2 ';
        $sql .= '       on h2.id = h1.mst_facility_id ';
        $sql .= '     left join mst_departments as i1 ';
        $sql .= '       on i1.id = b.department_id_to ';
        $sql .= '     left join mst_facilities as i2 ';
        $sql .= '       on i2.id = i1.mst_facility_id ';
        $sql .= '   where 1=1 ';
        $sql .= $where;
        $sql .= '   order by ';
        $sql .= '     a.work_no ';
        $sql .= '     , e.id ';
        $sql .= '     , b.work_seq ';
        
        $result = $this->TrnReturnHeader->query($sql);
        
        $tmpHeader = "";
        $seq = 1;
        $before_page_no = "";//行番号初期化チェック用
        foreach($result as $r){
            $parge_no = $r['TrnReturn']['work_no'].$r['TrnReturn']['owner_id'];
            //改ページ番号が変わるたびに行番号初期化
            if($before_page_no !== $parge_no){
                $seq = 1;
            } else {
                $seq++;
            }
            $before_page_no = $parge_no;

            $data[] = array(
                $parge_no,                                      //改ページ番号
                $r['TrnReturn']['facility_name_to']."様",       //仕入先名
                $r['TrnReturn']['work_no'],                     //返品番号
                $r['TrnReturn']['facility_name_from'],          //納品先名
                $r['TrnReturn']['owner_name'],                  //施主名
                $r['TrnReturn']['work_date'],                   //返品日
                $r['TrnReturn']['from_address'],                //納品先住所
                $r['TrnReturn']['owner_address'],               //施主住所
                $seq,                                           //行番号
                $r['TrnReturn']['item_name'],                   //商品名
                $r['TrnReturn']['standard'],                    //規格
                $r['TrnReturn']['item_code'],                   //製品番号
                $r['TrnReturn']['dealer_name'],                 //販売元
                $r['TrnReturn']['internal_code'],               //商品ID
                $r['TrnReturn']['price'] * $r['TrnReturn']['quantity'],  //金額
                $r['TrnReturn']['price'],                       //単価
                $r['TrnReturn']['recital'],                     //明細備考
                $r['TrnReturn']['quantity'],                    //数量
                $r['TrnReturn']['unit_name'],                   //包装単位
                '',                                             //課税区分
                $r['TrnReturn']['lot_no'],                      //ロット番号
                $r['TrnReturn']['printed_date'],                //印刷年月日
                $r['TrnReturn']['header_recital'] //ヘッダ備考
                );

            if($tmpHeader !== $r['TrnReturn']['id']){
                $tmpHeader = $r['TrnReturn']['id'];

                $this->TrnReturnHeader->recursive  = -1;
                $res2 = $this->TrnReturnHeader->updateAll(
                    array(
                        'modified'     => "'".$now."'",
                        'printed_date' => "'".$now."'",
                        'modifier'     => $this->request->data['User']['user_id']
                        ),
                    array('TrnReturnHeader.id' => $r['TrnReturn']['id']),
                    -1
                    );
            }
        }
        $errors = array();
        $errors = array_merge(
            $errors,
            is_array($this->validateErrors($this->TrnReturnHeader)) ? $this->validateErrors($this->TrnReturnHeader) : array()
            );
        if(count($errors) > 0){
            $this->TrnReturnHeader->rollback();
        }else{
            $this->TrnReturnHeader->commit();
        }
        $columns = array('改ページ番号',
                         '仕入先名',
                         '返品番号',
                         '納品先名',
                         '施主名',
                         '返品日',
                         '納品先住所',
                         '施主住所',
                         '行番号',
                         '商品名',
                         '規格',
                         '製品番号',
                         '販売元',
                         '商品ID',
                         '金額',
                         '単価',
                         '明細備考',
                         '数量',
                         '包装単位',
                         '課税区分',
                         'ロット番号',
                         '印刷年月日',
                         'ヘッダ備考'
                         
                         );
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'返品明細');
        $this->xml_output($data,join("\t",$columns),$layout_name['13'],$fix_value);
    }

    /**
     * xml出力テスト用
     * @param
     * @todo 各コントローラの共通関数化
     */
    function xml_output($data,$columns,$layout_name,$fix_value){

        $this->layout = 'xml/default';
        $this->set('columns',$columns);
        $this->set('posted',$data);
        $this->set('layout_name',$layout_name);
        $this->set('fix_value',$fix_value); //連想配列のキーにname属性を指定

        $this->render('/returns/xml_output');

    }

    /**
     * 返品先業者リスト取得
     * @param 確認画面表示用商品リスト
     * @return 仕入先リスト
     * 返品左記業者を全て表示するため、引数を廃止＆SQLを修正 2011/02/08
     */
    private function getTraderList(){
        $sql = "";
        $sql .= 'SELECT "MstDepartment"."id" AS "MstDepartment__id"';
        $sql .= '      ,"MstFacilitie"."facility_name" AS "MstFacilitie__name"';
        $sql .= ' FROM "mst_facility_relations" AS "MstFacilityRelation"';
        $sql .= ' INNER JOIN "mst_facilities" AS "MstFacilitie"';
        $sql .= ' ON "MstFacilityRelation"."partner_facility_id" = "MstFacilitie"."id"';
        $sql .= ' AND "MstFacilitie"."facility_type" = '.Configure::read('FacilityType.supplier');
        $sql .= ' AND "MstFacilitie"."is_deleted" = false';
        $sql .= ' INNER JOIN "mst_departments" AS "MstDepartment"';
        $sql .= ' ON "MstFacilityRelation"."partner_facility_id" = "MstDepartment"."mst_facility_id"';
        $sql .= ' AND "MstDepartment"."department_type" = '.Configure::read('FacilityType.supplier');
        $sql .= ' AND "MstDepartment"."is_deleted" = false';
        $sql .= ' WHERE "MstFacilityRelation"."mst_facility_id" = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= ' AND "MstFacilityRelation"."is_deleted" = false';
        $sql .= ' ORDER BY "MstFacilityRelation"."partner_facility_id"';

        $traderList = $this->MstFacilityRelation->query($sql);

        return AppController::outputFilter($traderList);
    }

    // 預託品返品在庫検索
    private function getDeposits($where , $limit = null){
        $sql  = ' select ';
        $sql .= '       a.id                                      as "TrnSticker__id"';
        $sql .= '     , c.internal_code                           as "MstFacilityItem__internal_code"';
        $sql .= '     , c.item_name                               as "MstFacilityItem__item_name"';
        $sql .= '     , c.item_code                               as "MstFacilityItem__item_code"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then d.unit_name  ';
        $sql .= "         else d.unit_name || '(' || b.per_unit || e.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                           as "MstFacilityItem__unit_name" ';
        $sql .= '     , a.lot_no                                  as "TrnSticker__lot_no"';
        $sql .= '     , a.hospital_sticker_no                     as "TrnSticker__hospital_sticker_no"';
        $sql .= '     , c.standard                                as "MstFacilityItem__standard"';
        $sql .= '     , f.dealer_name                             as "MstFacilityItem__dealer_name"';
        $sql .= '     , to_char(a.validated_date, \'YYYY/mm/dd\') as "TrnSticker__validated_date" ';
        $sql .= '     , i.facility_name                           as "MstFacilityItem__facility_name"';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on d.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as e  ';
        $sql .= '       on e.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as f  ';
        $sql .= '       on f.id = c.mst_dealer_id  ';
        $sql .= '     left join trn_orders as g  ';
        $sql .= '       on g.id = a.trn_order_id  ';
        $sql .= '     left join mst_departments as h  ';
        $sql .= '       on h.id = g.department_id_to  ';
        $sql .= '     left join mst_facilities as i  ';
        $sql .= '       on i.id = h.mst_facility_id  ';
        $sql .= '   where ';
        $sql .= '     a.trade_type = 3 ';
        $sql .= '     and a.is_deleted = false ';
        $sql .= '     and a.has_reserved = false ';
        $sql .= '     and a.quantity > 0 ';
        $sql .= '     and c.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');

        $sql .= $where;

        $sql .= ' order by c.internal_code , b.per_unit , a.hospital_sticker_no';

        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'TrnSticker'));
            $sql .= ' limit ' . $limit;
        }
        return $this->TrnSticker->query($sql);
    }

    /**
     * 返品履歴明細SQL文作成
     */
    private function _createReturnHistoryDetailSQL($where, $mode = null) {

        $sql = '';
        $base_sql  = ' select ';
        $base_sql .= '       a.id                                      as "Returns__id"';
        $base_sql .= '     , a.work_no                                 as "Returns__work_no"';
        $base_sql .= '     , to_char(a.work_date, \'YYYY/mm/dd\')      as "Returns__work_date"';
        $base_sql .= '     , e.name                                    as "Returns__class_name"';
        $base_sql .= '     , a.recital                                 as "Returns__recital"';
        $base_sql .= '     , c.internal_code                           as "Returns__internal_code"';
        $base_sql .= '     , c.item_name                               as "Returns__item_name"';
        $base_sql .= '     , c.item_code                               as "Returns__item_code"';
        $base_sql .= '     , c.standard                                as "Returns__standard"';
        $base_sql .= '     , a.quantity                                as "Returns__quantity"';
        $base_sql .= '     , a.stocking_price                          as "Returns__stocking_price"';
        $base_sql .= '     , d.lot_no                                  as "Returns__lot_no"';
        $base_sql .= '     , to_char(d.validated_date, \'YYYY/mm/dd\') as "Returns__validated_date" ';
        $base_sql .= '     , c1.dealer_name                            as "Returns__dealer_name"';
        $base_sql .= '     , f.user_name                               as "Returns__user_name"';
        $base_sql .= '     , h1.facility_name                          as "Returns__facility_name_from"';
        $base_sql .= '     , h2.facility_name                          as "Returns__facility_name_to"';
        $base_sql .= '     , d.facility_sticker_no                     as "Returns__facility_sticker_no"';
        $base_sql .= '     , d.hospital_sticker_no                     as "Returns__hospital_sticker_no"';
        $base_sql .= '     , ( case a.receiving_type ' ;
        $base_sql .= '             when ' . Configure::read('ReceivingType.return') . "  then '返品'";
        $base_sql .= '             when ' . Configure::read('ReceivingType.returnReceiving') . " then '入荷済返品'";
        $base_sql .= '             when ' . Configure::read('ReceivingType.returnStrage') . " then '入庫済返品'";
        $base_sql .= '             when ' . Configure::read('ReceivingType.returnDeposit') . " then '預託品返品'";
        $base_sql .= '     end )                                       as "Returns__receiving_type_name"';
        $base_sql .= '     , d.modified                                as "Returns__modified"';
        $base_sql .= '     , d.id                                      as "Returns__sticker_id"';
        $base_sql .= '     , (case ';
        $base_sql .= "            when a.stocking_close_type = 2 then '締め済み' ";
        $base_sql .= "            when a.stocking_close_type = 1 and x.updatable = false then '締め済み' ";
        $base_sql .= '      end )                                      as "Returns__status" ';
        $base_sql .= '     , a.receiving_type                          as "Returns__receiving_type"';
        $base_sql .= '     , x.updatable                               as "Returns__updatable"  ';

        $base_sql .= '   from ';
        $base_sql .= '     trn_receivings as a  ';

        if(!isset($this->request->data['Returns']['id'])){
            $base_sql .= '     left join trn_return_headers as z  ';
            $base_sql .= '       on z.id = a.trn_return_header_id ';
        }

        $base_sql .= '     left join mst_item_units as b  ';
        $base_sql .= '       on b.id = a.mst_item_unit_id  ';
        $base_sql .= '     left join mst_unit_names as b1  ';
        $base_sql .= '       on b1.id = b.mst_unit_name_id  ';
        $base_sql .= '     left join mst_unit_names as b2  ';
        $base_sql .= '       on b2.id = b.per_unit_name_id  ';
        $base_sql .= '     left join mst_facility_items as c  ';
        $base_sql .= '       on c.id = b.mst_facility_item_id  ';
        $base_sql .= '     left join mst_dealers as c1  ';
        $base_sql .= '       on c1.id = c.mst_dealer_id  ';
        $base_sql .= '     left join trn_stickers as d  ';
        $base_sql .= '       on d.id = a.trn_sticker_id  ';
        $base_sql .= '     left join mst_classes as e  ';
        $base_sql .= '       on e.id = a.work_type  ';
        $base_sql .= '     left join mst_users as f  ';
        $base_sql .= '       on f.id = a.modifier  ';
        $base_sql .= '     left join mst_departments as g1  ';
        $base_sql .= '       on g1.id = a.department_id_from  ';
        $base_sql .= '     left join mst_facilities as h1  ';
        $base_sql .= '       on h1.id = g1.mst_facility_id  ';
        $base_sql .= '     left join mst_departments as g2  ';
        $base_sql .= '       on g2.id = a.department_id_to  ';
        $base_sql .= '     left join mst_facilities as h2  ';
        $base_sql .= '       on h2.id = g2.mst_facility_id  ';
        $base_sql .= '     cross join (  ';
        $base_sql .= '       select ';
        $base_sql .= '             c.updatable  ';
        $base_sql .= '         from ';
        $base_sql .= '           mst_users as a  ';
        $base_sql .= '           left join mst_roles as b  ';
        $base_sql .= '             on a.mst_role_id = b.id  ';
        $base_sql .= '           left join mst_privileges as c  ';
        $base_sql .= '             on c.mst_role_id = b.id  ';
        $base_sql .= '         where ';
        $base_sql .= '           a.id = ' . $this->Session->read('Auth.MstUser.id');
        $base_sql .= '           and c.view_no = 37'; //返品履歴
        $base_sql .= '     ) as x  ';

        $base_sql .= '   where ';
        $base_sql .= '     a.is_deleted = false  ';
        $base_sql .= '     and a.receiving_type in (' . Configure::read('ReceivingType.return'). ', ' . Configure::read('ReceivingType.returnReceiving') . ', ' . Configure::read('ReceivingType.returnStrage') . ', '. Configure::read('ReceivingType.returnDeposit') .')  ';
        $base_sql .= '     and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $base_sql .= $where;

        //確定済みの返品データ
        if(isset($this->request->data['Returns']['id'])){
            $sql = $base_sql;
            $sql .= '     and a.trn_return_header_id in (' . join(',',$this->request->data['Returns']['id']) . ')  ';
        } else {

            if ($mode != null && $mode==0) {
                $sql = $base_sql;
                $sql .= '     and a.trn_return_header_id is not null ';
            } else if ($mode==1) {
                $sql = $base_sql;
                $sql .= '     and a.trn_return_header_id is not null ';
                $sql .= ' union all  ';
                $sql .= $base_sql;
                $sql .= '    and a.trn_return_header_id is null ';
            }
        }

        //未確定の返品データ
        if(isset($this->request->data['Returns']['facility'])){
            foreach($this->request->data['Returns']['facility'] as $facility){
                list($from_id , $to_id) = explode('_' , $facility);
                if($sql != ''){
                    $sql .= ' union all  ';
                }

                $sql .= $base_sql;
                $sql .= '    and a.trn_return_header_id is null ';
                $sql .= '    and h1.id = ' . $from_id ;
                $sql .= '    and h2.id = ' . $to_id ;
            }
        }

        return $sql;
    }

    /**
     * 返品履歴明細where句作成
     */
    private function _createReturnHistoryDetailWhere() {
        //絞り込み条件を引き継ぐ
        $where = '';
        if($this->request->data['TrnReturnHeader']['work_no'] != '' ){
            if(isset($this->request->data['TrnReceiving']['yet'])){
                //未確定も含む
                $where .= " and ( a.work_no like '" . $this->request->data['TrnReturnHeader']['work_no'] . "%'";
                $where .= " or a.work_no is null ) ";
            }else{
                //未確定は除く
                $where .= " and a.work_no like '" . $this->request->data['TrnReturnHeader']['work_no'] . "%'";
            }
        }
        //返品日From
        if($this->request->data['TrnReceiving']['date_select_start'] != '' ){
            if(isset($this->request->data['TrnReceiving']['yet'])){
                //未確定も含む
                $where .= " and ( a.work_date >= '" . $this->request->data['TrnReceiving']['date_select_start'] . "'";
                $where .= " or a.work_date is null ) ";
            }else{
                //未確定は除く
                $where .= " and a.work_date >= '" . $this->request->data['TrnReceiving']['date_select_start'] . "'";
            }
        }

        //返品日To
        if($this->request->data['TrnReceiving']['date_select_end'] != '' ){
            if(isset($this->request->data['TrnReceiving']['yet'])){
                //未確定も含む
                $where .= " and ( a.work_date <= '" . $this->request->data['TrnReceiving']['date_select_end'] . "'";
                $where .= " or a.work_date is null ) ";
            }else{
                //未確定は除く
                $where .= " and a.work_date <= '" . $this->request->data['TrnReceiving']['date_select_end'] . "'";
            }
        }

        //仕入先
        if($this->request->data['MstFacility']['facility_to_id'] != '' ){
            $where .= " and h2.facility_code = '" . $this->request->data['MstFacility']['facility_to_id'] . "'";
        }

        //施設
        if($this->request->data['MstFacility']['facility_from_id'] != '' ){
            $where .= " and h1.facility_code = '" . $this->request->data['MstFacility']['facility_from_id'] . "'";
        }

        //商品ID
        if($this->request->data['MstFacilityItem']['internal_code'] != '' ){
            $where .= " and c.internal_code like '" . $this->request->data['MstFacilityItem']['internal_code'] . "%'";
        }
        //製品番号
        if($this->request->data['MstFacilityItem']['item_code'] != '' ){
            $where .= " and c.item_code like '%" . $this->request->data['MstFacilityItem']['item_code'] . "%'";
        }
        //商品名
        if($this->request->data['MstFacilityItem']['item_name'] != '' ){
            $where .= " and c.item_name like '%" . $this->request->data['MstFacilityItem']['item_name'] . "%'";
        }
        //規格
        if($this->request->data['MstFacilityItem']['standard'] != '' ){
            $where .= " and c.standard like '%" . $this->request->data['MstFacilityItem']['standard'] . "%'";
        }
        //ロット
        if($this->request->data['TrnSticker']['lot_no'] != '' ){
            $where .= " and d.lot_no like '%" . $this->request->data['TrnSticker']['lot_no'] . "%'";
        }

        //販売元
        if($this->request->data['MstDealer']['dealer_name'] != '' ){
            $where .= " and c1.dealer_name like '%" . $this->request->data['MstDealer']['dealer_name'] . "%'";
        }
        //センターシール
        if($this->request->data['TrnSticker']['facility_sticker_no'] != '' ){
            $where .= " and d.facility_sticker_no = '" . $this->request->data['TrnSticker']['facility_sticker_no'] . "'";
        }
        //部署シール
        if($this->request->data['TrnSticker']['hospital_sticker_no'] != '' ){
            $where .= " and d.hospital_sticker_no = '" . $this->request->data['TrnSticker']['hospital_sticker_no'] . "'";
        }

        return $where;
    }

    /**
     * 返品履歴CSV出力
     */
    function export_csv() {
        $this->request->data['Returns']['id']=null;
        $this->request->data['Returns']['facility'] = null;

        $where = $this->_createReturnHistoryDetailWhere(); //前画面の検索条件を引き継いでwhere句作成

        $is_yet = '0';
        if(isset($this->request->data['TrnReceiving']['yet'])){
            $is_yet = $this->request->data['TrnReceiving']['yet'];
        }

        //返品日From
        if($this->request->data['TrnReceiving']['date_select_start'] != '' ){
            if(isset($this->request->data['TrnReceiving']['yet'])){
                //未確定も含む
                $where .= " and ( z.work_date >= '" . $this->request->data['TrnReceiving']['date_select_start'] . "'";
                $where .= " or z.work_date is null ) ";
            }else{
                //未確定は除く
                $where .= " and z.work_date >= '" . $this->request->data['TrnReceiving']['date_select_start'] . "'";
            }
        }

        //返品日To
        if($this->request->data['TrnReceiving']['date_select_end'] != '' ){
            if(isset($this->request->data['TrnReceiving']['yet'])){
                //未確定も含む
                $where .= " and ( z.work_date <= '" . $this->request->data['TrnReceiving']['date_select_end'] . "'";
                $where .= " or z.work_date is null ) ";
            }else{
                //未確定は除く
                $where .= " and z.work_date <= '" . $this->request->data['TrnReceiving']['date_select_end'] . "'";
            }
        }

        $sql = $this->_getReturnCSV($where, $is_yet);
        $this->db_export_csv($sql , "返品履歴", '/returns/return_history');
    }

    private function _getReturnCSV($where, $mode = null) {

        $sql = '';
        $base_sql  = ' select ';
        $base_sql .= '       a.work_no                                 as 返品番号';
        $base_sql .= "     , to_char(a.work_date, 'YYYY/mm/dd')        as 返品日";
        $base_sql .= '     , h2.facility_name                          as 仕入先名';
        $base_sql .= '     , h1.facility_name                          as 施設名';
        $base_sql .= '     , c.internal_code                           as "商品ID"';
        $base_sql .= '     , c.item_name                               as 商品名';
        $base_sql .= '     , c.standard                                as 規格';
        $base_sql .= '     , c.item_code                               as 製品番号';
        $base_sql .= '     , c1.dealer_name                            as 販売元名';
        $base_sql .= '     , a.quantity                                as 数量';
        $base_sql .= '     , a.stocking_price                          as 仕入単価';
        $base_sql .= '     , d.lot_no                                  as ロット番号';
        $base_sql .= "     , to_char(d.validated_date, 'YYYY/mm/dd')   as 有効期限";
        $base_sql .= '     , d.hospital_sticker_no                     as 部署シール';
        $base_sql .= '     , d.facility_sticker_no                     as センターシール';
        $base_sql .= '     , e.name                                    as 作業区分名';
        $base_sql .= '     , f.user_name                               as 更新者名';
        $base_sql .= '     , a.recital                                 as 備考';
        $base_sql .= '   from ';
        $base_sql .= '     trn_receivings as a  ';

        $base_sql .= '     left join trn_return_headers as z  ';
        $base_sql .= '       on z.id = a.trn_return_header_id ';

        $base_sql .= '     left join mst_item_units as b  ';
        $base_sql .= '       on b.id = a.mst_item_unit_id  ';
        $base_sql .= '     left join mst_unit_names as b1  ';
        $base_sql .= '       on b1.id = b.mst_unit_name_id  ';
        $base_sql .= '     left join mst_unit_names as b2  ';
        $base_sql .= '       on b2.id = b.per_unit_name_id  ';
        $base_sql .= '     left join mst_facility_items as c  ';
        $base_sql .= '       on c.id = b.mst_facility_item_id  ';
        $base_sql .= '     left join mst_dealers as c1  ';
        $base_sql .= '       on c1.id = c.mst_dealer_id  ';
        $base_sql .= '     left join trn_stickers as d  ';
        $base_sql .= '       on d.id = a.trn_sticker_id  ';
        $base_sql .= '     left join mst_classes as e  ';
        $base_sql .= '       on e.id = a.work_type  ';
        $base_sql .= '     left join mst_users as f  ';
        $base_sql .= '       on f.id = a.modifier  ';
        $base_sql .= '     left join mst_departments as g1  ';
        $base_sql .= '       on g1.id = a.department_id_from  ';
        $base_sql .= '     left join mst_facilities as h1  ';
        $base_sql .= '       on h1.id = g1.mst_facility_id  ';
        $base_sql .= '     left join mst_departments as g2  ';
        $base_sql .= '       on g2.id = a.department_id_to  ';
        $base_sql .= '     left join mst_facilities as h2  ';
        $base_sql .= '       on h2.id = g2.mst_facility_id  ';
        $base_sql .= '   where ';
        $base_sql .= '     a.is_deleted = false  ';
        $base_sql .= '     and a.receiving_type in (' . Configure::read('ReceivingType.return'). ', ' . Configure::read('ReceivingType.returnReceiving') . ', ' . Configure::read('ReceivingType.returnStrage') . ', '. Configure::read('ReceivingType.returnDeposit') .')  ';
        $base_sql .= '     and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $base_sql .= $where;

        //確定済みの返品データ
        if(isset($this->request->data['Returns']['id'])){
            $sql = $base_sql;
            $sql .= '     and a.trn_return_header_id in (' . join(',',$this->request->data['Returns']['id']) . ')  ';
        } else {

            if ($mode != null && $mode==0) {
                $sql = $base_sql;
                $sql .= '     and a.trn_return_header_id is not null ';
            } else if ($mode==1) {
                $sql = $base_sql;
                $sql .= '     and a.trn_return_header_id is not null ';
                $sql .= ' union all  ';
                $sql .= $base_sql;
                $sql .= '    and a.trn_return_header_id is null ';
            }
        }

        //未確定の返品データ
        if(isset($this->request->data['Returns']['facility'])){
            foreach($this->request->data['Returns']['facility'] as $facility){
                list($from_id , $to_id) = explode('_' , $facility);
                if($sql != ''){
                    $sql .= ' union all  ';
                }

                $sql .= $base_sql;
                $sql .= '    and a.trn_return_header_id is null ';
                $sql .= '    and h1.id = ' . $from_id ;
                $sql .= '    and h2.id = ' . $to_id ;
            }
        }

        return $sql;
    }

    /**
     * 締めチェック
     */
    private function _isCloseType($date, $updatable){
        $is_close = false;

        $sql  = "";
        $sql .= ' select ';
        $sql .= '     close_type     as "TrnCloseHeaders__close_type" ';
        $sql .= '     ,is_provisional as "TrnCloseHeaders__is_provisional" ';
        $sql .= ' from ';
        $sql .= '     trn_close_headers ';
        $sql .= ' where ';
        $sql .= '     is_deleted = false ';
        $sql .= '     and mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= "     and start_date <= '" . $date . "'";
        $sql .= "     and end_date >= '" . $date . "'";
        $sql .= '      and close_type = '. Configure::read('CloseType.stock');
        $sql .= ' order by id desc ';
        $sql .= ' limit 1 ';

        $res = $this->TrnSticker->query($sql);

        foreach($res as $r){
            if($r['TrnCloseHeaders']['is_provisional'] == true){
                //仮締めの場合、更新権限を見る
                if ($updatable == false) {
                    //更新権限が無い場合は、締められている
                    $is_close = true;
                }
            } elseif ($r['TrnCloseHeaders']['is_provisional'] == false){
                $is_close = true;
            }

        }

        return $is_close;
    }

}
?>
