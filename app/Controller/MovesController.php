<?php
/**
 * MovesController
 *　移動
 * @version 1.0.0
 * @since 2010/05/18
 */
class MovesController extends AppController {

    /**
     * @var $name
     */
    var $name = 'Moves';

    /**
     * @var array $uses
     */
    var $uses = array(
        'MstClass',
        'MstUserBelonging',
        'MstFacility',
        'MstDepartment',
        'MstFacilityRelation',
        'MstFixedCount',
        'Hospital',
        'MstDepartment',
        'MstFacilityItem',
        'MstDealer',
        'TrnSticker',
        'TrnOrderHeader',
        'TrnMoveHeader',
        'MstTransactionConfig',
        'MstSalesConfig',
        'TrnReceiving',
        'TrnReceivingHeader',
        'TrnStock',
        'TrnStorage',
        'TrnShipping',
        'TrnStickerRecord',
        'MstItemUnit',
        'Center',
        'Outsider',
        'TrnCloseHeader'
        );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time',);

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Stickers','Common','CsvWriteUtils','CsvReadUtils');

    /**
     *
     * @var array errors
     */
    var $errors;

    /**
     * 定数：作業区分
     * Enter description here ...
     * @var unknown_type
     */
    const DEFAULT_WORK_CLASSES_MOVE = 14;

    const SESSION_NAME_TRN_MOVE_HEADER = 'SESSION_TRN_MOVE_HEADER';

    var $center_type   = 1;
    var $hospital_type = 2;
    var $work_type = self::DEFAULT_WORK_CLASSES_MOVE;

    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        //CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }
    }

    /**
     * 施設内移動
     */
    function move2inner() {
        $this->setRoleFunction(35); //施設内移動
        $this->__getWorkClasses();
        $this->request->data['TrnMoveHeader']['work_date'] = date('Y/m/d');
        $this->set('department_list' , array());
        $this->set('to_facilities',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected'),
                       array(Configure::read('FacilityType.hospital'),
                             Configure::read('FacilityType.center'),),
                       true
                       )
                   );

        $this->render('move2inner');
    }

    /**
     * 施設内移動
     * 商品検索＆選択
     */
    function move2inner_search() {
        //在庫部署リストを取得
        $this->set('from_departments',$this->getDepartmentsList( $this->request->data['TrnMoveHeader']['facility_code'] ));

        //検索結果格納変数
        $result = array();
        $cartResult = array();
        if(isset($this->request->data['TrnMoveHeader']['isSearch'])){
            /*  検索フラグが立っていれば検索処理実行  */
            // 条件文
            $where = '';
            // 商品ID
            if( !empty($this->request->data['MstFacilityItem']['internal_code'])) {
                $where .= "and c.internal_code like '%" . pg_escape_string($this->request->data['MstFacilityItem']['internal_code']) . "%'";
            }
            // 製品番号
            if( !empty($this->request->data['MstFacilityItem']['item_code'])) {
                $where .= " and c.item_code like '%" . pg_escape_string($this->request->data['MstFacilityItem']['item_code']) . "%'";
            }
            // 商品名
            if( !empty($this->request->data['MstFacilityItem']['item_name'])) {
                $where .= " and c.item_name like '%" . pg_escape_string($this->request->data['MstFacilityItem']['item_name']) . "%'";
            }
            // 規格
            if( !empty($this->request->data['MstFacilityItem']['standard'])) {
                $where .= " and c.standard like '%" . pg_escape_string($this->request->data['MstFacilityItem']['standard']) . "%'";
            }
            // 販売元
            if( !empty($this->request->data['MstDealer']['dealer_name'])) {
                $where .= " and c1.dealer_name like '%" . pg_escape_string($this->request->data['MstDealer']['dealer_name']) . "%'";
            }
            // シール番号
            if( !empty($this->request->data['TrnSticker']['facility_sticker_no'])) {
                $where .= " and ( a.facility_sticker_no ILIKE '%" . pg_escape_string($this->request->data['TrnSticker']['facility_sticker_no']) . "%'";
                $where .= " or a.hospital_sticker_no ILIKE '%" . pg_escape_string($this->request->data['TrnSticker']['facility_sticker_no']) . "%' )";
            }
            // ロット番号
            if( !empty($this->request->data['TrnSticker']['lot_no'])) {
                $where .= "and a.lot_no ILIKE '%" . pg_escape_string($this->request->data['TrnSticker']['lot_no']) . "%'";
            }
            //在庫部署
            if( !empty($this->request->data['TrnMoveHeader']['from_department'])){
                $where .= " and d.department_code = '". $this->request->data['TrnMoveHeader']['from_department'] . "'";
            }
            
            //カートに入っているものは表示しない
            if(!empty($this->request->data['TrnMoveHeader']['tempId'])){
                $where .= '  and a.id not IN ('. $this->request->data['TrnMoveHeader']['tempId'] .')';
            }

            $where .= " and e.facility_code = '" . $this->request->data['TrnMoveHeader']['facility_code']."'";
            $where .= " and d.department_code <> '" . $this->request->data['TrnMoveHeader']['department_code']."'";
            $result = $this->getMoveData($where , $this->request->data['limit']);
            
            //カート商品検索
            $cartResult = array();
            if(!empty($this->request->data['TrnMoveHeader']['tempId'])){
                $where2 = " and a.id IN (".$this->request->data['TrnMoveHeader']['tempId'].")";
                $cartResult = $this->getMoveData($where2);
            }
        }
        $this->set('result',$result);
        $this->set('cartResult',$cartResult);
    }


    function getMoveData($where = '' , $limit = null){
        $sql  =' select ';
        $sql .='       a.id                                      as "TrnMove__id" ';
        $sql .='     , a.facility_sticker_no                     as "TrnMove__facility_sticker_no" ';
        $sql .='     , a.hospital_sticker_no                     as "TrnMove__hospital_sticker_no" ';
        $sql .='     , a.lot_no                                  as "TrnMove__lot_no" ';
        $sql .='     , to_char(a.validated_date, \'YYYY/mm/dd\') as "TrnMove__validated_date" ';
        $sql .='     , a.has_reserved                            as "TrnMove__has_reserved"';
        $sql .='     , a.transaction_price                       as "TrnMove__transaction_price"';
        $sql .='     , 0                                         as "TrnMove__price"'; //評価単価
        $sql .='     , a.modified                                as "TrnMove__modified"';
        $sql .='     , c.internal_code                           as "TrnMove__internal_code" ';
        $sql .='     , c.item_name                               as "TrnMove__item_name" ';
        $sql .='     , c.item_code                               as "TrnMove__item_code" ';
        $sql .='     , c.standard                                as "TrnMove__standard" ';
        $sql .='     , c.jan_code                                as "TrnMove__jan_code" ';
        $sql .='     , c1.dealer_name                            as "TrnMove__dealer_name" ';
        $sql .='     , (  ';
        $sql .='       case  ';
        $sql .='         when b.per_unit = 1  ';
        $sql .='         then b1.unit_name  ';
        $sql .="         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .='         end ';
        $sql .='     )                                           as "TrnMove__unit_name" ';
        $sql .='     , e.facility_name                           as "TrnMove__facility_name" ';
        $sql .='     , d.department_name                         as "TrnMove__department_name"  ';
        $sql .='     , f.substitute_unit_id                      as "TrnMove__switch_id"'; // 定数切り替え
        $sql .='   from ';
        $sql .='     trn_stickers as a  ';
        $sql .='     left join mst_item_units as b  ';
        $sql .='       on b.id = a.mst_item_unit_id  ';
        $sql .='     left join mst_unit_names as b1  ';
        $sql .='       on b1.id = b.mst_unit_name_id  ';
        $sql .='     left join mst_unit_names as b2  ';
        $sql .='       on b2.id = b.per_unit_name_id  ';
        $sql .='     left join mst_facility_items as c  ';
        $sql .='       on c.id = b.mst_facility_item_id  ';
        $sql .='     left join mst_dealers as c1  ';
        $sql .='       on c1.id = c.mst_dealer_id  ';
        $sql .='     left join mst_departments as d  ';
        $sql .='       on d.id = a.mst_department_id  ';
        $sql .='     left join mst_facilities as e  ';
        $sql .='       on e.id = d.mst_facility_id  ';
        $sql .='     left join mst_fixed_counts as f ';
        $sql .='       on f.mst_item_unit_id = a.mst_item_unit_id';
        $sql .='      and f.mst_department_id = a.mst_department_id';
        $sql .='      and f.is_deleted = false ';
        $sql .='   where ';
        $sql .='     a.is_deleted = false  ';
        $sql .='     and a.quantity > 0  ';
        $sql .='     and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= $where;
        $sql .='   order by ';
        $sql .='     a.hospital_sticker_no ';
        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'TrnSticker'));
            $sql .= ' limit ' . $limit;
        }
        return $this->TrnSticker->query($sql);
    }

    /**
     * 施設内移動確認
     */
    function move2inner_confirm () {

        $this->__getWorkClasses();
        $result = $this->getMoveData(' and a.id in (' . $this->request->data['TrnMoveHeader']['cartId'] . ')');

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnMoveHeader.token' , $token);
        $this->request->data['TrnMoveHeader']['token'] = $token;

        $this->set('result',$result);
    }

    /**
     * 施設内移動結果
     */
    function move2inner_result () {
        if($this->request->data['TrnMoveHeader']['token'] === $this->Session->read('TrnMoveHeader.token')) {
            $this->Session->delete('TrnMoveHeader.token');

            $now = date('Y/m/d H:i:s.u');
            $this->__getWorkClasses();

            // トランザクション開始
            $this->TrnMoveHeader->begin();

            //行ロック
            $this->TrnSticker->query('select * from trn_stickers as a where a.id in (' . join(',',$this->request->data['Moves']['stickerId']) . ') for update');

            //更新チェック
            $ret = $this->TrnSticker->query('select count(*) from trn_stickers as a where a.id in (' . join(',',$this->request->data['Moves']['stickerId']) . ") and a.modified > '" . $this->request->data['TrnMoveHeader']['time'] . "'");
            if($ret[0][0]['count'] > 0){
                $this->TrnMoveHeader->rollback();
                $this->Session->setFlash('ほかユーザによって更新されています。やり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('move2inner');
            }


            $facility_id = $this->getFacilityId($this->request->data['TrnMoveHeader']['facility_code'] , array(Configure::read('FacilityType.hospital'),Configure::read('FacilityType.center')) );
            $to_department_id = $this->getDepartmentId($facility_id , array(Configure::read('DepartmentType.warehouse'),Configure::read('DepartmentType.opestock'),Configure::read('DepartmentType.hospital')) , $this->request->data['TrnMoveHeader']['department_code']);
            
            $sql  = ' select ';
            $sql .= '       a.mst_department_id ';
            $sql .= '     , count(*)   as count ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in ( ' . join(',',$this->request->data['Moves']['stickerId']) . ' )  ';
            $sql .= '   group by ';
            $sql .= '     a.mst_department_id  ';
            $sql .= '   order by ';
            $sql .= '     a.mst_department_id ';
            
            $header_data = $this->TrnSticker->query($sql);
            
            //本日日付
            $date = date("Y/m/d");

            //ヘッダID格納配列
            foreach($header_data as $h){
                //ワークNo取得
                $work_no = $this->setWorkNo4Header($date, 14);
                
                $this->TrnMoveHeader->create();
                $save_data['TrnMoveHeader'] = array();
                $save_data['TrnMoveHeader']['work_no']               = $work_no;
                $save_data['TrnMoveHeader']['work_type']             = $this->request->data['TrnMoveHeader']['work_class'];
                $save_data['TrnMoveHeader']['work_date']             = $this->request->data['TrnMoveHeader']['work_date'];
                $save_data['TrnMoveHeader']['recital']               = $this->request->data['TrnMoveHeader']['recital'];
                $save_data['TrnMoveHeader']['move_status']           = Configure::read('MoveStatus.complete');
                $save_data['TrnMoveHeader']['move_type']             = Configure::read("MoveType.move2inner");
                $save_data['TrnMoveHeader']['department_id_from']    = $h[0]['mst_department_id'];
                $save_data['TrnMoveHeader']['department_id_to']      = $to_department_id; // 移動先
                $save_data['TrnMoveHeader']['detail_count']          = $h[0]['count'];
                $save_data['TrnMoveHeader']['detail_total_quantity'] = $h[0]['count'];
                $save_data['TrnMoveHeader']['is_deleted']            = false;
                $save_data['TrnMoveHeader']['creater']               = $this->Session->read('Auth.MstUser.id');
                $save_data['TrnMoveHeader']['modifier']              = $this->Session->read('Auth.MstUser.id');
                $save_data['TrnMoveHeader']['created']               = $now;
                $save_data['TrnMoveHeader']['modified']              = $now;
                
                if (!$this->TrnMoveHeader->save($save_data)) {
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('移動ヘッダ登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('move2inner');
                }
                $trn_move_header_id = $this->TrnMoveHeader->getLastInsertId();
                $seq = 1;
                // 明細データ取得
                $sql  = ' select ';
                $sql .= '       a.id as trn_sticker_id ';
                $sql .= '     , a.facility_sticker_no  ';
                $sql .= '     , a.hospital_sticker_no ';
                $sql .= '     , a.mst_item_unit_id ';
                $sql .= '     , a.mst_department_id ';
                $sql .= '     , a.trn_order_id ';
                $sql .= '     , a.original_price ';
                $sql .= '     , a.transaction_price  ';
                $sql .= '     , a.trade_type ';
                $sql .= '     , b.department_type ';
                $sql .= '   from ';
                $sql .= '     trn_stickers as a  ';
                $sql .= '     left join mst_departments as b';
                $sql .= '     on b.id = a.mst_department_id ';
                $sql .= '   where ';
                $sql .= '     a.id in ( ' . join(',',$this->request->data['Moves']['stickerId']) . ' ) ';
                $sql .= '     and a.mst_department_id = ' . $h[0]['mst_department_id'];
                
                $data = $this->TrnSticker->query($sql);
                
                foreach($data as $d){
                    // 移動（出荷）
                    $shipping = array();
                    $shipping['TrnShipping']['work_no']                = $work_no;
                    $shipping['TrnShipping']['work_seq']               = $seq;
                    $shipping['TrnShipping']['work_date']              = $this->request->data['TrnMoveHeader']['work_date'];
                    $shipping['TrnShipping']['work_type']              = $this->request->data['Moves']['work_class'][$d[0]['trn_sticker_id']];
                    $shipping['TrnShipping']['recital']                = $this->request->data['Moves']['recital'][$d[0]['trn_sticker_id']];
                    $shipping['TrnShipping']['shipping_type']          = Configure::read('ShippingType.move');
                    $shipping['TrnShipping']['shipping_status']        = Configure::read('ShippingStatus.loaded');
                    $shipping['TrnShipping']['mst_item_unit_id']       = $d[0]['mst_item_unit_id'];
                    $shipping['TrnShipping']['department_id_from']     = $d[0]['mst_department_id'];
                    $shipping['TrnShipping']['department_id_to']       = $to_department_id;
                    $shipping['TrnShipping']['quantity']               = 1;
                    $shipping['TrnShipping']['trn_sticker_id']         = $d[0]['trn_sticker_id'];
                    $shipping['TrnShipping']['is_deleted']             = false;
                    $shipping['TrnShipping']['trn_move_header_id']     = $trn_move_header_id;
                    $shipping['TrnShipping']['creater']                = $this->Session->read('Auth.MstUser.id');
                    $shipping['TrnShipping']['modifier']               = $this->Session->read('Auth.MstUser.id');
                    $shipping['TrnShipping']['created']                = $now;
                    $shipping['TrnShipping']['modified']               = $now;
                    $shipping['TrnShipping']['move_status']            = Configure::read('MoveStatus.complete');
                    $shipping['TrnShipping']['facility_sticker_no']    = $d[0]['facility_sticker_no'];
                    $shipping['TrnShipping']['hospital_sticker_no']    = $d[0]['hospital_sticker_no'];
                    $this->TrnShipping->create();
                    if (!$this->TrnShipping->save($shipping)) {
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('移動明細（出荷）登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('move2inner');
                    }
                    $trn_shipping_id = $this->TrnShipping->getLastInsertId();  //出荷登録ID
                    
                    // 移動（入荷）
                    $receiving = array();
                    $receiving['TrnReceiving']['work_no']                 = $work_no;
                    $receiving['TrnReceiving']['work_seq']                = $seq;
                    $receiving['TrnReceiving']['work_date']               = $this->request->data['TrnMoveHeader']['work_date'];
                    $receiving['TrnReceiving']['work_type']               = $this->request->data['Moves']['work_class'][$d[0]['trn_sticker_id']];
                    $receiving['TrnReceiving']['recital']                 = $this->request->data['Moves']['recital'][$d[0]['trn_sticker_id']];
                    $receiving['TrnReceiving']['receiving_type']          = Configure::read('ReceivingType.move');
                    $receiving['TrnReceiving']['receiving_status']        = Configure::read('ReceivingStatus.stock');
                    $receiving['TrnReceiving']['mst_item_unit_id']        = $d[0]['mst_item_unit_id'];
                    $receiving['TrnReceiving']['department_id_from']      = $d[0]['mst_department_id'];
                    $receiving['TrnReceiving']['department_id_to']        = $to_department_id;
                    $receiving['TrnReceiving']['quantity']                = 1;
                    $receiving['TrnReceiving']['remain_count']            = 0;  //入庫済みなので0固定
                    $receiving['TrnReceiving']['stocking_price']          = $d[0]['original_price'];
                    $receiving['TrnReceiving']['sales_price']             = $d[0]['transaction_price'];
                    $receiving['TrnReceiving']['trn_sticker_id']          = $d[0]['trn_sticker_id'];
                    $receiving['TrnReceiving']['trn_shipping_id']         = $trn_shipping_id;
                    $receiving['TrnReceiving']['trn_order_id']            = $d[0]['trn_order_id'];
                    $receiving['TrnReceiving']['trn_move_header_id']      = $trn_move_header_id;
                    $receiving['TrnReceiving']['is_retroactable']         = true;
                    $receiving['TrnReceiving']['is_deleted']              = false;
                    $receiving['TrnReceiving']['creater']                 = $this->Session->read('Auth.MstUser.id');
                    $receiving['TrnReceiving']['modifier']                = $this->Session->read('Auth.MstUser.id');
                    $receiving['TrnReceiving']['created']                 = $now;
                    $receiving['TrnReceiving']['modified']                = $now;
                    $receiving['TrnReceiving']['facility_sticker_no']     = $d[0]['facility_sticker_no'];
                    $receiving['TrnReceiving']['hospital_sticker_no']     = $d[0]['hospital_sticker_no'];
                    $this->TrnReceiving->create();
                    if (!$this->TrnReceiving->save($receiving)) {
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('移動明細（入荷）登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('move2inner');
                    }
                    $trn_receiving_id = $this->TrnReceiving->getLastInsertId();  //入荷登録ID

                    // シール移動履歴
                    $sticker_records = array();
                    $sticker_records['TrnStickerRecord']['move_date']           = $this->request->data['TrnMoveHeader']['work_date'];
                    $sticker_records['TrnStickerRecord']['move_seq']            = $this->getNextRecord($d[0]['trn_sticker_id']);
                    $sticker_records['TrnStickerRecord']['record_type']         = Configure::read('RecordType.move');
                    $sticker_records['TrnStickerRecord']['record_id']           = $trn_shipping_id;
                    $sticker_records['TrnStickerRecord']['mst_department_id']   = $to_department_id;
                    $sticker_records['TrnStickerRecord']['trn_sticker_id']      = $d[0]['trn_sticker_id'];
                    $sticker_records['TrnStickerRecord']['facility_sticker_no'] = $d[0]['facility_sticker_no'];
                    $sticker_records['TrnStickerRecord']['hospital_sticker_no'] = $d[0]['hospital_sticker_no'];
                    $sticker_records['TrnStickerRecord']['is_deleted']          = false;
                    $sticker_records['TrnStickerRecord']['creater']             = $this->Session->read('Auth.MstUser.id');
                    $sticker_records['TrnStickerRecord']['modifier']            = $this->Session->read('Auth.MstUser.id');
                    $sticker_records['TrnStickerRecord']['created']             = $now;
                    $sticker_records['TrnStickerRecord']['modified']            = $now;
                    $this->TrnStickerRecord->create();
                    if (!$this->TrnStickerRecord->save($sticker_records)) {
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('シール移動履歴処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('move2inner');
                    }
                    
                    //センター倉庫 <=> オペ倉庫の場合区分は変えない。
                    if( $d[0]['department_type'] == Configure::read('DepartmentType.hospital')
                        && $this->Session->read('Auth.Config.InnerMoveTypeChange') == '1'){
                        $d[0]['trade_type'] = Configure::read('ClassesType.Temporary');
                    }
                    
                    // シール情報更新
                    $this->TrnSticker->create();
                    $ret = $this->TrnSticker->updateAll(
                        array(
                            'TrnSticker.mst_department_id'  => $to_department_id,
                            'TrnSticker.last_move_date'     => "'".$this->request->data['TrnMoveHeader']['work_date']."'",
                            'TrnSticker.trn_move_header_id' => $trn_move_header_id,
                            'TrnSticker.trade_type'         => $d[0]['trade_type'],
                            'TrnSticker.modifier'           => $this->Session->read('Auth.MstUser.id'),
                            'TrnSticker.modified'           => "'" . $now . "'"
                            ),
                        array(
                            'TrnSticker.id' => $d[0]['trn_sticker_id'],
                            ),
                        -1
                        );
                    if(!$ret){
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('シール情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('move2inner');
                    }
                    
                    // 移動元在庫減
                    $this->TrnStock->create();
                    $ret = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.stock_count' => 'stock_count - 1 ',
                            'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'    => "'" . $now . "'"
                            ),
                        array(
                            'TrnStock.id' => $this->getStockRecode($d[0]['mst_item_unit_id'] , $d[0]['mst_department_id']),
                            ),
                        -1
                        );
                    if(!$ret){
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('在庫情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('move2inner');
                    }

                    
                    // 移動先在庫増
                    // 移動元在庫減
                    $this->TrnStock->create();
                    $ret = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.stock_count' => 'stock_count + 1 ',
                            'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'    => "'" . $now . "'"
                            ),
                        array(
                            'TrnStock.id' => $this->getStockRecode($d[0]['mst_item_unit_id'] , $to_department_id),
                            ),
                        -1
                        );
                    if(!$ret){
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('在庫情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('move2inner');
                    }

                    $seq++;
                }
            }
            
            //更新チェック
            $ret = $this->TrnSticker->query('select count(*) from trn_stickers as a where a.id in (' . join(',',$this->request->data['Moves']['stickerId']) . ") and a.modified > '" . $this->request->data['TrnMoveHeader']['time'] . "' and a.modified <> '" .$now. "'" );
            if($ret[0][0]['count'] > 0){
                $this->TrnMoveHeader->rollback();
                $this->Session->setFlash('ほかユーザによって更新されています。やり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('move2inner');
            }

            $this->TrnMoveHeader->commit();  // コミット
            $this->Session->setFlash('施設内移動処理が完了しました', 'growl', array('type'=>'star') );

            $resultView = $this->getMoveData(' and a.id in (' . join(',',$this->request->data['Moves']['stickerId']) . ')');
            $this->Session->write('TrnMoveHeader.resultView' , $resultView);
            $this->set('resultView',$resultView);
        }else{
            //2度押し対策
            $this->__getWorkClasses();
            $this->set('resultView',$this->Session->read('TrnMoveHeader.resultView'));
        }
    }

    /**
     * 施設外移動
     */
    function move2outer() {
        $this->setRoleFunction(36); //施設外移動
        //作業区分
        $this->__getWorkClasses();
        $this->request->data['TrnMoveHeader']['work_date'] = date('Y/m/d');

        // 移動元は権限のある施設
        $this->set('from_facilities',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected'),
                       array(Configure::read('FacilityType.hospital')),
                       true
                       )
                   );

    }

    /**
     * 施設外移動
     * 商品検索＆選択
     */
    function move2outer_search() {
        //検索結果格納変数
        $result = array();
        $cartResult = array();

        if(isset($this->request->data['TrnMoveHeader']['isSearch'])){
            /*  検索フラグが立っていれば検索処理実行  */

            // 条件文
            $where = '';
            // 商品ID
            if( !empty($this->request->data['MstFacilityItem']['internal_code'])) {
                $where .= "and c.internal_code like '%" . pg_escape_string($this->request->data['MstFacilityItem']['internal_code']) . "%'";
            }
            // 製品番号
            if( !empty($this->request->data['MstFacilityItem']['item_code'])) {
                $where .= "and c.item_code like '%" . pg_escape_string($this->request->data['MstFacilityItem']['item_code']) . "%'";
            }
            // 商品名
            if( !empty($this->request->data['MstFacilityItem']['item_name'])) {
                $where .= "and c.item_name like '%" . pg_escape_string($this->request->data['MstFacilityItem']['item_name']) . "%'";
            }
            // 規格
            if( !empty($this->request->data['MstFacilityItem']['standard'])) {
                $where .= "and c.standard like '%" . pg_escape_string($this->request->data['MstFacilityItem']['standard']) . "%'";
            }
            // 販売元
            if( !empty($this->request->data['MstDealer']['dealer_name'])) {
                $where .= "and c1.dealer_name like '%" . pg_escape_string($this->request->data['MstDealer']['dealer_name']) . "%'";
            }
            // シール番号
            if( !empty($this->request->data['TrnSticker']['facility_sticker_no'])) {
                $where .= "and ( a.facility_sticker_no ILIKE '%" . pg_escape_string($this->request->data['TrnSticker']['facility_sticker_no']) . "%'";
                $where .= "or a.hospital_sticker_no ILIKE '%" . pg_escape_string($this->request->data['TrnSticker']['facility_sticker_no']) . "%' )";
            }
            // ロット番号
            if( !empty($this->request->data['TrnSticker']['lot_no'])) {
                $where .= "and a.lot_no like '%" . pg_escape_string($this->request->data['TrnSticker']['lot_no']) . "%'";
            }
            //在庫部署
            if( !empty($this->request->data['TrnMoveHeader']['from_department_code'])){
                $where .= " and d.department_code = '" . $this->request->data['TrnMoveHeader']['from_department_code'] . "'";
            }
            //カートに入っている商品は出さない
            if(!empty($this->request->data['TrnMoveHeader']['tempId'])){
                $where .= " and a.id not IN (". $this->request->data['TrnMoveHeader']['tempId'] .")";
            }

            // 売上請求がされているものは移動できない
            //$where .= " and TrnSticker.sale_claim_id is null ";
            
            $where .= " and a.trade_type <> ". Configure::read('TeisuType.Deposit');
            $where .= " and a.has_reserved = false "; 
            $where .= " and e.facility_code = '" . $this->request->data['TrnMoveHeader']['facility_code'] . "'";

            $result = $this->getMoveData($where , $this->request->data['limit']);

            //カート商品検索
            if(!empty($this->request->data['TrnMoveHeader']['tempId'])){
                $cartResult = $this->getMoveData(' and a.id in (' . $this->request->data['TrnMoveHeader']['tempId'] . ')  ');
            }

        }else{
            //初期処理
            //施設コードから施設IDを取得
            //$form_dept_id = $this->getFacilitiesId($this->request->data['TrnMoveHeader']['facility_from']);
            //$to_dept_id = $this->getFacilitiesId($this->request->data['TrnMoveHeader']['user_facility']);

            /*  月締めチェック  */
            //移動元締めチェック
            //if($this->checkClosed($this->request->data,$form_dept_id) != 0){
            //    $this->Session->setFlash('移動元に対して締めが行われています', 'growl', array('type'=>'error'));
            //    $this->move2outer();
            //    return;
            //}

            //移動先締めチェック
            //if($this->checkClosed($this->request->data,$to_dept_id) != 0){
            //    $this->Session->setFlash('移動先に対して締めが行われています', 'growl', array('type'=>'error'));
            //    $this->move2outer();
            //    return;
            //}
        }

        //在庫部署リストを取得
        $this->set('from_departments',$this->getDepartmentsList($this->request->data['TrnMoveHeader']['facility_code']));
        
        $this->set('cartResult',$cartResult);
        $this->set('result',$result);
    }


    /**
     * 月締め確認処理
     */
    function checkClosed($data,$id){
        $count = 0;
        $date = explode("/",$data['TrnMoveHeader']['work_date']);

        $params = array();
        $params['mst_facility_id'] = $id;
        $params['close_type'] = Configure::read("CloseType.stock");
        $params['start_date <= '] = $data['TrnMoveHeader']['work_date'];
        $params['end_date > '] = date("Y/m/d", mktime(0, 0, 0, $date[1], $date[2]+1, $date[0]));
        $params['is_deleted'] = false;
        $this->TrnCloseHeader->create();
        $this->TrnCloseHeader->unbindModel(array('belongsTo'=>array('MstFacility')));
        $count = $this->TrnCloseHeader->find('count', array('conditions' => $params));

        return $count;
    }

    /**
     * 施設外移動確認
     */
    function move2outer_confirm () {

        $result = $this->getMoveData(' and a.id in ( ' . $this->request->data['TrnMoveHeader']['cartId'] . ') ');
        $this->__getWorkClasses();

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnMoveHeader.token' , $token);
        $this->request->data['TrnMoveHeader']['token'] = $token;
        $this->set('result',$result);
    }

    /**
     * 施設外移動結果
     */
    function move2outer_result() {
        if($this->request->data['TrnMoveHeader']['token'] === $this->Session->read('TrnMoveHeader.token')) {
            $this->Session->delete('TrnMoveHeader.token');
            $now = date('Y/m/d H:i:s.u');
            // 移動処理を行う
            // トランザクション開始
            $this->TrnMoveHeader->begin();

            //行ロック
            $this->TrnSticker->query('select * from trn_stickers as a where a.id in (' . join(',',$this->request->data['Moves']['stickerId']) . ') for update');
            //更新チェック
            $ret = $this->TrnSticker->query('select count(*) from trn_stickers as a where a.id in (' . join(',',$this->request->data['Moves']['stickerId']) . ") and a.modified > '" . $this->request->data['TrnMoveHeader']['time'] . "'");
            if($ret[0][0]['count'] > 0){
                $this->TrnMoveHeader->rollback();
                $this->Session->setFlash('ほかユーザによって更新されています。やり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('move2outer');
            }

            $to_department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'), Configure::read('DepartmentType.warehouse'));
            
            $sql  = ' select ';
            $sql .= '       a.mst_department_id ';
            $sql .= '     , count(*)   as count ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in ( ' . join(',',$this->request->data['Moves']['stickerId']) . ' )  ';
            $sql .= '   group by ';
            $sql .= '     a.mst_department_id  ';
            $sql .= '   order by ';
            $sql .= '     a.mst_department_id ';
            
            $header_data = $this->TrnSticker->query($sql);
            
            //本日日付
            $date = date("Y/m/d");

            //ヘッダID格納配列
            foreach($header_data as $h){
                //ワークNo取得
                $work_no = $this->setWorkNo4Header($date, 14);
                
                $this->TrnMoveHeader->create();
                $save_data['TrnMoveHeader'] = array();
                $save_data['TrnMoveHeader']['work_no']               = $work_no;
                $save_data['TrnMoveHeader']['work_type']             = $this->request->data['TrnMoveHeader']['work_class'];
                $save_data['TrnMoveHeader']['work_date']             = $this->request->data['TrnMoveHeader']['work_date'];
                $save_data['TrnMoveHeader']['recital']               = $this->request->data['TrnMoveHeader']['recital'];
                $save_data['TrnMoveHeader']['move_status']           = Configure::read('MoveStatus.reserved');
                $save_data['TrnMoveHeader']['move_type']             = Configure::read("MoveType.move2outer");
                $save_data['TrnMoveHeader']['department_id_from']    = $h[0]['mst_department_id'];
                $save_data['TrnMoveHeader']['department_id_to']      = $to_department_id; // 移動先
                $save_data['TrnMoveHeader']['detail_count']          = $h[0]['count'];
                $save_data['TrnMoveHeader']['detail_total_quantity'] = $h[0]['count'];
                $save_data['TrnMoveHeader']['is_deleted']            = false;
                $save_data['TrnMoveHeader']['creater']               = $this->Session->read('Auth.MstUser.id');
                $save_data['TrnMoveHeader']['modifier']              = $this->Session->read('Auth.MstUser.id');
                $save_data['TrnMoveHeader']['created']               = $now;
                $save_data['TrnMoveHeader']['modified']              = $now;
                
                if (!$this->TrnMoveHeader->save($save_data)) {
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('移動ヘッダ登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('move2outer');
                }
                $trn_move_header_id = $this->TrnMoveHeader->getLastInsertId();
                $seq = 1;
                // 明細データ取得
                $sql  = ' select ';
                $sql .= '       a.id as trn_sticker_id ';
                $sql .= '     , a.facility_sticker_no  ';
                $sql .= '     , a.hospital_sticker_no ';
                $sql .= '     , a.mst_item_unit_id ';
                $sql .= '     , a.mst_department_id ';
                $sql .= '     , a.trn_order_id ';
                $sql .= '     , a.original_price ';
                $sql .= '     , a.transaction_price  ';
                $sql .= '   from ';
                $sql .= '     trn_stickers as a  ';
                $sql .= '   where ';
                $sql .= '     a.id in ( ' . join(',',$this->request->data['Moves']['stickerId']) . ' ) ';
                $sql .= '     and a.mst_department_id = ' . $h[0]['mst_department_id'];
                
                $data = $this->TrnSticker->query($sql);
                
                foreach($data as $d){
                    // 移動（出荷）
                    $shipping = array();
                    $shipping['TrnShipping']['work_no']                = $work_no;
                    $shipping['TrnShipping']['work_seq']               = $seq;
                    $shipping['TrnShipping']['work_date']              = $this->request->data['TrnMoveHeader']['work_date'];
                    $shipping['TrnShipping']['work_type']              = $this->request->data['Moves']['work_class'][$d[0]['trn_sticker_id']];
                    $shipping['TrnShipping']['recital']                = $this->request->data['Moves']['recital'][$d[0]['trn_sticker_id']];
                    $shipping['TrnShipping']['shipping_type']          = Configure::read('ShippingType.move');
                    $shipping['TrnShipping']['shipping_status']        = Configure::read('ShippingStatus.ordered');
                    $shipping['TrnShipping']['mst_item_unit_id']       = $d[0]['mst_item_unit_id'];
                    $shipping['TrnShipping']['department_id_from']     = $d[0]['mst_department_id'];
                    $shipping['TrnShipping']['department_id_to']       = $to_department_id;
                    $shipping['TrnShipping']['quantity']               = 1;
                    $shipping['TrnShipping']['trn_sticker_id']         = $d[0]['trn_sticker_id'];
                    $shipping['TrnShipping']['is_deleted']             = false;
                    $shipping['TrnShipping']['trn_move_header_id']     = $trn_move_header_id;
                    $shipping['TrnShipping']['creater']                = $this->Session->read('Auth.MstUser.id');
                    $shipping['TrnShipping']['modifier']               = $this->Session->read('Auth.MstUser.id');
                    $shipping['TrnShipping']['created']                = $now;
                    $shipping['TrnShipping']['modified']               = $now;
                    $shipping['TrnShipping']['move_status']            = Configure::read('MoveStatus.reserved');
                    $shipping['TrnShipping']['facility_sticker_no']    = $d[0]['facility_sticker_no'];
                    $shipping['TrnShipping']['hospital_sticker_no']    = $d[0]['hospital_sticker_no'];
                    $this->TrnShipping->create();
                    if (!$this->TrnShipping->save($shipping)) {
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('移動明細（出荷）登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('move2outer');
                    }
                    $trn_shipping_id = $this->TrnShipping->getLastInsertId();  //出荷登録ID
                    
                    
                    // シール情報更新
                    $this->TrnSticker->create();
                    $ret = $this->TrnSticker->updateAll(
                        array(
                            'TrnSticker.has_reserved'       => "'true'" ,
                            'TrnSticker.trn_move_header_id' => $trn_move_header_id,
                            'TrnSticker.modifier'           => $this->Session->read('Auth.MstUser.id'),
                            'TrnSticker.modified'           => "'" . $now . "'"
                            ),
                        array(
                            'TrnSticker.id' => $d[0]['trn_sticker_id'],
                            ),
                        -1
                        );
                    if(!$ret){
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('シール情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('move2outer');
                    }
                    
                    // 移動元予約増
                    $this->TrnStock->create();
                    $ret = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.reserve_count' => 'reserve_count + 1 ',
                            'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'    => "'" . $now . "'"
                            ),
                        array(
                            'TrnStock.id' => $this->getStockRecode($d[0]['mst_item_unit_id'] , $d[0]['mst_department_id']),
                            ),
                        -1
                        );
                    if(!$ret){
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('在庫情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('move2outer');
                    }
                    $seq++;
                }
            }

            //コミット直前に更新チェック
            $ret = $this->TrnSticker->query('select count(*) from trn_stickers as a where a.id in (' . join(',',$this->request->data['Moves']['stickerId']) . ") and a.modified > '" . $this->request->data['TrnMoveHeader']['time'] . "' and a.modified <> '" .$now. "'");
            if($ret[0][0]['count'] > 0){
                $this->TrnMoveHeader->rollback();
                $this->Session->setFlash('ほかユーザによって更新されています。やり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('move2outer');
            }

            $this->TrnMoveHeader->commit();  // コミット
            $this->Session->setFlash('施設外移動処理が完了しました', 'growl', array('type'=>'star') );

            $resultView = $this->getMoveData(' and a.id in ( ' . join(',',$this->request->data['Moves']['stickerId']) . ')');
            
            $this->Session->write('TrnMoveHeader.resultView' , $resultView);
            $this->set('resultView',$resultView);
        }else{
            //2度押し対策
            $this->set('resultView',$this->Session->read('TrnMoveHeader.resultView'));
        }
        $this->__getWorkClasses();
    }

    /**
     *  センター間移動
     */
    public function move2other() {
        $this->setRoleFunction(37); //センター間移動

        //施設 //操作センターと、サブコードの会社が同じセンターを取得
        $sql  = ' select ';
        $sql .= '       b.facility_code as "MstFacility__facility_code"';
        $sql .= '     , b.facility_name as "MstFacility__facility_name"';
        $sql .= '   from ';
        $sql .= '     mst_facilities as a  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on substr( a.subcode, 0 , strpos(a.subcode, \''.Configure::read('subcodeSplitter').'\')) = substr(b.subcode , 0, strpos(b.subcode, \'' .Configure::read('subcodeSplitter') .  '\') )  ';
        $sql .= '       and b.id <> ' . $this->Session->read('Auth.facility_id_selected') ;
        $sql .= '       and strpos(a.subcode, \''.Configure::read('subcodeSplitter').'\') > 1 ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and b.facility_type = ' . Configure::read('FacilityType.center');
        $sql .= '   order by b.facility_code ';

        $facilities =  Set::Combine($this->MstFacility->query($sql),'{n}.MstFacility.facility_code','{n}.MstFacility.facility_name');
        //施設
        $this->set('facilities', $facilities);

        //移動日は今日指定
        $this->request->data['Move2Other']['work_date'] = date('Y/m/d', time());
        //作業区分
        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'14'));

        $this->render('/moves/move2other');
    }

    /**
     *  センター間移動
     *  検索
     */
    public function move2other_search(){
        App::import('Sanitize');
        $SearchResult = array();
        $CartSearchResult = array();

        if(isset($this->request->data['add_search']['is_search']) ){
            $data = Sanitize::clean($this->request->data['Move2Other']);

            $where = '';
            $where2 = '';

            //検索条件
            if($data['internal_code'] != ''){
                $where .= " and MstFacilityItem.internal_code LIKE '%" . $data['internal_code'] ."%'";
            }
            if($data['item_code'] != ''){
                $where .= " and MstFacilityItem.item_code LIKE '%" . $data['item_code'] ."%'";
            }
            if($data['lot_no'] != ''){
                $where .= " and TrnSticker.lot_no LIKE '%" . $data['lot_no'] ."%'";
            }
            if($data['item_name'] != ''){
                $where .= " and MstFacilityItem.item_name LIKE '%" .$data['item_name'] . "%'";
            }
            if($data['dealer_name'] != ''){
                $where .= " and MstDealer.dealer_name LIKE '%" . $data['dealer_name'] . "%'";
            }
            if($data['standard'] != ''){
                $where .= " and MstFacilityItem.standard LIKE '%" . $data['standard'] . "%'";
            }
            if($data['sticker_no'] != ''){
                $where .= " and TrnSticker.facility_sticker_no ILIKE '%" . $data['sticker_no'] ."%'";
            }

            if($this->request->data['cart']['tmpid'] != '' ) {
                $where  .= ' and TrnSticker.id not in (' . $this->request->data['cart']['tmpid'] . ')';
                $where2  = ' and TrnSticker.id in (' . $this->request->data['cart']['tmpid'] . ')';
            }else{
                $where2 = ' and 1=0 ';
            }

            $SearchResult = $this->searchMove2Other($where , $data['limit']);
            $CartSearchResult = $this->searchMove2Other($where2);

        }

        $this->set('SearchResult', self::outputFilter($SearchResult));
        $this->set('CartSearchResult', self::outputFilter($CartSearchResult));
        $this->render('/moves/move2other_search');
    }


    /**
     * センター間移動確認
     */
    public function move2other_confirm(){

        $res = $this->searchMove2Other(' and TrnSticker.id in ( ' . $this->request->data['cart']['tmpid'] . ')');

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnMoveHeader.token' , $token);
        $this->request->data['TrnMoveHeader']['token'] = $token;

        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'14'));
        $this->set('SearchResult', self::outputFilter($res));
        $this->render('/moves/move2other_confirm');
    }

    /**
     * センター間移動＠登録
     */
    public function move2other_results(){
        if($this->request->data['TrnMoveHeader']['token'] === $this->Session->read('TrnMoveHeader.token')) {
            $this->Session->delete('TrnMoveHeader.token');

            $now = date('Y/m/d H:i:s.u');

            $params = $this->request->data['Move2Other'];

            //移動先センターの施設IDを取得
            $sql = " select id from mst_facilities as a where a.facility_code = '" . $this->request->data['Move2Other']['facility_input'] . "' and a.facility_type = " . Configure::read('FacilityType.center');
            $ret = $this->TrnMoveHeader->query($sql);
            $facility_id_to = $ret[0][0]['id'];
            $params['facility_id_to'] = $facility_id_to;

            $res = $this->searchMove2Other(' and TrnSticker.id in ( ' . join(',', $this->request->data['Move2Other']['id']) . ')');
            //登録処理
            $this->TrnMoveHeader->begin();
            //行ロック
            $this->TrnSticker->query('select * from trn_stickers as a where a.id in (' . join(',', $this->request->data['Move2Other']['id']) . ') for update');

            try {
                //更新時間チェック
                $ret = $this->TrnSticker->query('select count(*) from trn_stickers as a where a.id in (' . join(',', $this->request->data['Move2Other']['id']) . ") and a.modified > '" .$this->request->data['TrnMoveHeader']['time']. "'");
                if($ret[0][0]['count']){
                    throw new Exception('ほかユーザによって更新されています。最初からやり直してください。');
                }

                //移動ヘッダ作成
                $workNo = $this->setWorkNo4Header($params['work_date'], '14');
                $params['work_no'] = $workNo;
                $depTo = $this->getCenterDepartmentId($params['facility_id_to']);
                $this->TrnMoveHeader->create(false);
                $this->TrnMoveHeader->save(array(
                    'id'                    => false,
                    'work_no'               => $workNo,
                    'work_date'             => $params['work_date'],
                    'work_type'             => $params['work_type'],
                    'recital'               => $params['recital'],
                    'move_status'           => Configure::read('MoveStatus.reserved'),//予約中
                    'department_id_from'    => $this->getCenterDepartmentId($this->Session->read('Auth.facility_id_selected')),//自身の倉庫id
                    'department_id_to'      => $depTo,//相手のセンターの倉庫
                    'detail_count'          => count($res),
                    'is_deleted'            => false,
                    'creater'               => $this->Session->read('Auth.MstUser.id'),
                    'created'               => $now,
                    'modifier'              => $this->Session->read('Auth.MstUser.id'),
                    'modified'              => $now,
                    'detail_total_quantity' => count($res),
                    'move_type'             => Configure::read('MoveType.move2other'),
                    ));
                if(false !== $this->validateErrors($this->TrnMoveHeader)){
                    throw new Exception('移動ヘッダ作成失敗' . print_r($this->validateErrors($this->TrnMoveHeader),true));
                }
                $lastHeaderId = $this->TrnMoveHeader->getLastInsertID();
                $cnt = 0;
                foreach($res as $r){
                    $cnt++;
                    $row = $r[0];
                    //出荷明細
                    $this->TrnShipping->create(false);
                    $this->TrnShipping->save(array(
                        'id'                  => false,
                        'work_no'             => $workNo,
                        'work_seq'            => $cnt,
                        'work_date'           => $params['work_date'],
                        'work_type'           => $this->request->data['TrnShipping']['work_type'][$row['trn_sticker_id']],
                        'recital'             => $this->request->data['TrnShipping']['recital'][$row['trn_sticker_id']],
                        'shipping_type'       => Configure::read('ShippingType.move'),
                        'shipping_status'     => Configure::read('ShippingStatus.ordered'),
                        'mst_item_unit_id'    => $row['mst_item_unit_id'],
                        'department_id_from'  => $row['mst_department_id'],
                        'department_id_to'    => $depTo,
                        'quantity'            => $row['quantity'],
                        'trn_sticker_id'      => $row['trn_sticker_id'],
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        'trn_move_header_id'  => $lastHeaderId,
                        'move_status'         => Configure::read('MoveStatus.reserved'),
                        'facility_sticker_no' => $row['facility_sticker_no'],
                        'hospital_sticker_no' => $row['hospital_sticker_no'],
                        ));
                    if(false !== $this->validateErrors($this->TrnShipping)){
                        throw new Exception('出荷明細作成失敗' . print_r($this->validateErrors($this->TrnShipping),true));
                    }
                    $lastShippingId = $this->TrnShipping->getLastInsertID();


                    //シール更新
                    $ret = $this->TrnSticker->updateAll(array(
                        'TrnSticker.has_reserved'       => 'true',
                        'TrnSticker.transaction_price'  => (float)$this->request->data['TrnShipping']['unit_price'][$row['trn_sticker_id']],
                        'TrnSticker.trn_move_header_id' => $lastHeaderId,
                        'TrnSticker.modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'           => "'".$now."'",
                        ), array(
                        'TrnSticker.id' => $row['trn_sticker_id'],
                        ), -1);
                    if(false === $ret){
                        throw new Exception('シール情報更新失敗');
                    }

                    //在庫更新
                    $ret = $this->TrnStock->updateAll(array(
                        'TrnStock.reserve_count' => "TrnStock.reserve_count + {$row['quantity']}",
                        'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'      => "'".$now."'",
                        ), array(
                        'TrnStock.mst_item_unit_id'  => $row['mst_item_unit_id'],
                        'TrnStock.mst_department_id' => $row['mst_department_id'],
                        ), -1);
                    if(false === $ret){
                        throw new Exception('在庫情報更新失敗');
                    }
                }

                //コミット直前に更新チェック
                $ret = $this->TrnSticker->query('select count(*) from trn_stickers as a where a.id in (' . join(',', $this->request->data['Move2Other']['id']) . ") and a.modified > '" .$this->request->data['TrnMoveHeader']['time']. "' and a.modified <> '" . $now . "'");
                if($ret[0][0]['count']){
                    throw new Exception('ほかユーザによって更新されています。最初からやり直してください。');
                }
            } catch (Exception $ex) {
                $this->TrnMoveHeader->rollback();
                throw new Exception($ex->getMessage().print_r($res,true));
            }
            $this->TrnMoveHeader->commit();

            $this->set('params', $params);
            $this->set('results', $res);

            $classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'14');
            $classes[''] = '';
            $this->set('classes', $classes);

            $this->Session->write('TrnMoveHeader.params' , $params);
            $this->Session->write('TrnMoveHeader.result' , $res);

            $this->Session->write('TrnMoveHeader.classes' , $classes);

            $this->Session->setFlash('センター間移動処理が完了しました', 'growl', array('type'=>'star') );
        }else{
            $this->set('params', $this->Session->read('TrnMoveHeader.params'));
            $this->set('results', $this->Session->read('TrnMoveHeader.result'));
            $this->set('details', $this->Session->read('TrnMoveHeader.details'));
            $this->set('classes', $this->Session->read('TrnMoveHeader.classes'));
        }
        $this->render('move2other_result_view');
    }


    /**
     * 移動受領　商品検索
     */
    function search_item_moved () {
        App::import('Sanitize');
        $this->setRoleFunction(38); //移動受領登録
        $result = array();
        $move3_array = array(); //センター間の商品情報格納用

        //初回時は検索を行わない
        if (isset($this->request->data['isSearch']) && ($this->request->data['isSearch'] === '1')) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $where = '';
            /* 絞込み追加ココから */
            //商品名
            if($this->request->data['Search']['MstFacilityItem']['item_name'] != ""){
                $where .= ' and f.item_name LIKE ' ."'%".$this->request->data['Search']['MstFacilityItem']['item_name']."%' ";
            }

            //商品ID
            if($this->request->data['Search']['MstFacilityItem']['internal_code'] != ""){
                $where .= ' and f.internal_code = ' ."'".$this->request->data['Search']['MstFacilityItem']['internal_code']."' ";
            }
            //製品番号
            if($this->request->data['Search']['MstFacilityItem']['item_code'] != ""){
                $where .= ' and f.item_code LIKE ' ."'%".$this->request->data['Search']['MstFacilityItem']['item_code']."%' ";
            }
            //規格
            if($this->request->data['Search']['MstFacilityItem']['standard'] != ""){
                $where .= ' and f.standard LIKE ' ."'%".$this->request->data['Search']['MstFacilityItem']['standard']."%' ";
            }
            //販売元
            if($this->request->data['Search']['MstDealer']['dealer_name'] != ""){
                $where .= ' and f1.dealer_name LIKE ' ."'%".$this->request->data['Search']['MstDealer']['dealer_name']."%' ";
            }
            //JANコード(前方一致)
            if($this->request->data['Search']['MstFacilityItem']['jan_code'] != ""){
                $where .= ' and f.jan_code LIKE '."'" .$this->request->data['Search']['MstFacilityItem']['jan_code']."%' ";
            }
            //移動元
            if($this->request->data['Search']['from_facility'] != ""){
                $where .= ' and c2.facility_code = '."'" .$this->request->data['Search']['from_facility']."' ";
            }

            /* 絞込み追加ココまで */
            $sql  = ' select ';
            $sql .= '       b.id                       as "TrnMove__id" ';
            $sql .= '     , b.quantity                 as "TrnMove__quantity" ';
            $sql .= '     , b.modified                 as "TrnMove__modified" ';
            $sql .= '     , a.move_type                as "TrnMove__move_type" ';
            $sql .= '     , f.master_id                as "TrnMove__master_id" ';
            $sql .= '     , f.internal_code            as "TrnMove__internal_code" ';
            $sql .= '     , f.item_name                as "TrnMove__item_name" ';
            $sql .= '     , f.item_code                as "TrnMove__item_code" ';
            $sql .= '     , f.standard                 as "TrnMove__standard" ';
            $sql .= '     , f1.dealer_name             as "TrnMove__dealer_name" ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when e.per_unit = 1  ';
            $sql .= '         then e1.unit_name  ';
            $sql .= "         else e1.unit_name || '(' || e.per_unit || e2.unit_name || ')'  ";
            $sql .= '         end ';
            $sql .= '     )                            as "TrnMove__unit_name" ';
            $sql .= '     , e.per_unit                 as "TrnMove__per_unit" ';
            $sql .= '     , g.facility_sticker_no      as "TrnMove__facility_sticker_no" ';
            $sql .= '     , g.transaction_price        as "TrnMove__transaction_price" ';
            $sql .= '     , c1.department_name         as "TrnMove__department_name_from" ';
            $sql .= '     , c2.facility_name           as "TrnMove__facility_name_from" ';
            $sql .= '     , d1.department_name         as "TrnMove__department_name_to" ';
            $sql .= '     , d2.facility_name           as "TrnMove__facility_name_to"  ';
            $sql .= '   from ';
            $sql .= '     trn_move_headers as a  ';
            $sql .= '     left join trn_shippings as b  ';
            $sql .= '       on b.trn_move_header_id = a.id  ';
            $sql .= '     left join mst_departments as c1  ';
            $sql .= '       on c1.id = b.department_id_from  ';
            $sql .= '     left join mst_facilities as c2  ';
            $sql .= '       on c2.id = c1.mst_facility_id  ';
            $sql .= '     left join mst_departments as d1  ';
            $sql .= '       on d1.id = b.department_id_to  ';
            $sql .= '     left join mst_facilities as d2  ';
            $sql .= '       on d2.id = d1.mst_facility_id  ';
            $sql .= '     left join mst_item_units as e  ';
            $sql .= '       on e.id = b.mst_item_unit_id  ';
            $sql .= '     left join mst_unit_names as e1  ';
            $sql .= '       on e1.id = e.mst_unit_name_id  ';
            $sql .= '     left join mst_unit_names as e2  ';
            $sql .= '       on e2.id = e.per_unit_name_id  ';
            $sql .= '     left join mst_facility_items as f  ';
            $sql .= '       on f.id = e.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as f1  ';
            $sql .= '       on f1.id = f.mst_dealer_id  ';
            $sql .= '     left join trn_stickers as g  ';
            $sql .= '       on g.id = b.trn_sticker_id  ';
            $sql .= '     left join trn_receivings as h ';
            $sql .= '       on h.trn_shipping_id = b.id  ';
            $sql .= '   where ';
            $sql .= '     a.move_type in (' . Configure::read('MoveType.move2outer') . ', ' . Configure::read('MoveType.move2other' ) . ')  ';
            $sql .= '     and a.is_deleted = false  ';
            $sql .= '     and b.is_deleted = false  ';
            $sql .= '     and a.move_status in (1, 10)  ';
            $sql .= '     and d2.id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     and h.id is null ';
            $sql .= $where;
            $sql .= '   order by a.move_type , f.internal_code , g.facility_sticker_no';
            //全件数取得
            if(isset($this->request->data['Search']['center_move'])){
                $this->set('max' , $this->getMaxCount($sql , 'TrnMoveHeader' , false ));
            }else{
                $this->set('max' , $this->getMaxCount($sql , 'TrnMoveHeader'));
            }
            $sql .= ' limit ' . $this->request->data['limit'];
            if(isset($this->request->data['Search']['center_move'])){
                $result = $this->TrnMoveHeader->query($sql , false , false );
            }else{
                $result = $this->TrnMoveHeader->query($sql);
            }

            foreach($result as &$r){
                //センター間移動の場合は商品情報を取り直す。
                if($r['TrnMove']['move_type'] == Configure::read('MoveType.move2other')){
                    $sql  = ' select ';
                    $sql .= '      b.id                   as "TrnMove__mst_item_unit_id" ';
                    $sql .= '    , a.internal_code        as "TrnMove__internal_code" ';
                    $sql .= '    , a.item_name            as "TrnMove__item_name" ';
                    $sql .= '    , a.standard             as "TrnMove__standard" ';
                    $sql .= '    , a.item_code            as "TrnMove__item_code" ';
                    $sql .= "    , coalesce(a1.dealer_name,'')";
                    $sql .= '                             as "TrnMove__dealer_name" ';
                    $sql .= '    , (  ';
                    $sql .= '      case  ';
                    $sql .= '        when b.per_unit = 1  ';
                    $sql .= '        then b1.unit_name  ';
                    $sql .= "        else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
                    $sql .= '        end ';
                    $sql .= '    )                        as "TrnMove__unit_name"  ';
                    $sql .= '  from ';
                    $sql .= '    mst_facility_items as a  ';
                    $sql .= '    left join mst_dealers as a1  ';
                    $sql .= '      on a1.id = a.mst_dealer_id  ';
                    $sql .= '    left join mst_item_units as b  ';
                    $sql .= '      on b.mst_facility_item_id = a.id  ';
                    $sql .= '    left join mst_unit_names as b1  ';
                    $sql .= '      on b1.id = b.mst_unit_name_id  ';
                    $sql .= '    left join mst_unit_names as b2  ';
                    $sql .= '      on b2.id = b.per_unit_name_id  ';
                    $sql .= '  where ';
                    $sql .= '    a.mst_facility_id = ' .$this->Session->read('Auth.facility_id_selected');
                    $sql .= "    and a.master_id = '".$r['TrnMove']['master_id']."'  ";
                    $sql .= '    and b.per_unit =' . $r['TrnMove']['per_unit'];
                    $item_data = $this->TrnMoveHeader->query($sql);

                    $r['TrnMove']['internal_code'] = $item_data[0]['TrnMove']['internal_code'];
                    $r['TrnMove']['item_code']     = $item_data[0]['TrnMove']['item_code'];
                    $r['TrnMove']['item_name']     = $item_data[0]['TrnMove']['item_name'];
                    $r['TrnMove']['standard']      = $item_data[0]['TrnMove']['standard'];
                    $r['TrnMove']['unit_name']     = $item_data[0]['TrnMove']['unit_name'];
                    foreach($item_data as $i){
                        $move3_array[$r['TrnMove']['id']][$i['TrnMove']['mst_item_unit_id']] = $i;
                    }
                }
            }
            unset($r);
            $this->request->data = $data;
        }

        // 移動元プルダウンの取得
        $this->set('movefrom_facilities',$this->moveFacilityList());

        $this->request->data['TrnMoveHeader']['work_date'] = date('Y/m/d');
        $this->set( 'result' , $result );

        $this->set('move3_array' , $move3_array);
        $this->set('json_data' , json_encode($move3_array));
    }

    /**
     * 移動受領確認
     */
    function receive_confirm () {
        $this->Session->write('receive.readTime',date('Y-m-d H:i:s'));
        $this->request->data['MoveHeader']['work_date'] = date('Y/m/d');
        $sql = '';
        foreach($this->request->data['TrnMove']['id'] as $id){
            if($sql <> ''){
                $sql .= ' union all ';
            }
            $sql .= ' select ';
            $sql .= '       b.id                       as "TrnMove__id" ';
            $sql .= '     , b.quantity                 as "TrnMove__quantity" ';
            $sql .= '     , b.modified                 as "TrnMove__modified" ';
            $sql .= '     , a.move_type                as "TrnMove__move_type" ';
            $sql .= '     , f.master_id                as "TrnMove__master_id" ';
            $sql .= '     , f.internal_code            as "TrnMove__internal_code" ';
            $sql .= '     , f.item_name                as "TrnMove__item_name" ';
            $sql .= '     , f.item_code                as "TrnMove__item_code" ';
            $sql .= '     , f.standard                 as "TrnMove__standard" ';
            $sql .= '     , f1.dealer_name             as "TrnMove__dealer_name" ';
            if(isset($this->request->data['TrnMove']['mst_item_unit_id'][$id])){
                //センター間移動
                $sql .= '     , (  ';
                $sql .= '       case  ';
                $sql .= '         when f2.per_unit = 1  ';
                $sql .= '         then f4.unit_name  ';
                $sql .= "         else f4.unit_name || '(' || f2.per_unit || f4.unit_name || ')'  ";
                $sql .= '         end ';
                $sql .= '     )                            as "TrnMove__unit_name" ';
                $sql .= '     , f2.id                      as "TrnMove__mst_item_unit_id"';
            }else{
                //施設外移動
                $sql .= '     , (  ';
                $sql .= '       case  ';
                $sql .= '         when e.per_unit = 1  ';
                $sql .= '         then e1.unit_name  ';
                $sql .= "         else e1.unit_name || '(' || e.per_unit || e2.unit_name || ')'  ";
                $sql .= '         end ';
                $sql .= '     )                            as "TrnMove__unit_name" ';
                $sql .= '     , e.id                       as "TrnMove__mst_item_unit_id"';
            }
            $sql .= '     , e.per_unit                 as "TrnMove__per_unit" ';
            $sql .= '     , g.facility_sticker_no      as "TrnMove__facility_sticker_no" ';
            $sql .= '     , g.transaction_price        as "TrnMove__transaction_price" ';
            $sql .= '     , c1.department_name         as "TrnMove__department_name_from" ';
            $sql .= '     , c2.facility_name           as "TrnMove__facility_name_from" ';
            $sql .= '     , d1.department_name         as "TrnMove__department_name_to" ';
            $sql .= '     , d2.facility_name           as "TrnMove__facility_name_to"  ';
            $sql .= '   from ';
            $sql .= '     trn_move_headers as a  ';
            $sql .= '     left join trn_shippings as b  ';
            $sql .= '       on b.trn_move_header_id = a.id  ';
            $sql .= '     left join mst_departments as c1  ';
            $sql .= '       on c1.id = b.department_id_from  ';
            $sql .= '     left join mst_facilities as c2  ';
            $sql .= '       on c2.id = c1.mst_facility_id  ';
            $sql .= '     left join mst_departments as d1  ';
            $sql .= '       on d1.id = b.department_id_to  ';
            $sql .= '     left join mst_facilities as d2  ';
            $sql .= '       on d2.id = d1.mst_facility_id  ';
            $sql .= '     left join mst_item_units as e  ';
            $sql .= '       on e.id = b.mst_item_unit_id  ';
            $sql .= '     left join mst_unit_names as e1  ';
            $sql .= '       on e1.id = e.mst_unit_name_id  ';
            $sql .= '     left join mst_unit_names as e2  ';
            $sql .= '       on e2.id = e.per_unit_name_id  ';
            if(isset($this->request->data['TrnMove']['mst_item_unit_id'][$id])){
                //センター間移動
                $sql .= '     left join mst_facility_items as f3  ';
                $sql .= '       on f3.id = e.mst_facility_item_id  ';
                $sql .= '     left join mst_facility_items as f  ';
                $sql .= '       on f.master_id = f3.master_id ';
                $sql .= '      and f.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
                $sql .= '    inner join mst_item_units as f2 ';
                $sql .= '       on f.id = f2.mst_facility_item_id ';
                $sql .= '      and f2.id = ' . $this->request->data['TrnMove']['mst_item_unit_id'][$id];
                $sql .= '     left join mst_unit_names as f4  ';
                $sql .= '       on f4.id = f2.mst_unit_name_id  ';
                $sql .= '     left join mst_unit_names as f5  ';
                $sql .= '       on f5.id = f2.per_unit_name_id  ';
            }else{
                //施設外移動
                $sql .= '     left join mst_facility_items as f  ';
                $sql .= '       on f.id = e.mst_facility_item_id  ';
            }
            $sql .= '     left join mst_dealers as f1 ';
            $sql .= '       on f1.id = f.mst_dealer_id ';
            $sql .= '     left join trn_stickers as g  ';
            $sql .= '       on g.id = b.trn_sticker_id  ';
            $sql .= '   where ';
            $sql .= '     b.id = ' . $id;
        }
        if(isset($this->request->data['Search']['center_move'])){
            $result = $this->TrnMoveHeader->query($sql , false , false );
        }else{
            $result = $this->TrnMoveHeader->query($sql);
        }
        $this->set( 'result' , $result );

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnMoveHeader.token' , $token);
        $this->request->data['TrnMoveHeader']['token'] = $token;
    }

    /**
     * 移動受領結果
     */
    function receive_result () {
        if($this->request->data['TrnMoveHeader']['token'] === $this->Session->read('TrnMoveHeader.token')) {
            $this->Session->delete('TrnMoveHeader.token');
            $this->request->data['now'] = date('Y/m/d H:i:d.u');
            // トランザクション開始
            $this->TrnMoveHeader->begin();

            if ($this->__receiveLogic()) {
                $this->TrnMoveHeader->commit();
            } else {
                $this->TrnMoveHeader->rollback();
                $this->Session->setFlash('移動処理に失敗しました', 'growl', array('type'=>'error') );
                $this->redirect('/login/home');
            }

            //結果表示用
            $sql  = ' select ';
            $sql .= '       b.id                       as "TrnMove__id" ';
            $sql .= '     , b.quantity                 as "TrnMove__quantity" ';
            $sql .= '     , b.modified                 as "TrnMove__modified" ';
            $sql .= '     , a.move_type                as "TrnMove__move_type" ';
            $sql .= '     , f.master_id                as "TrnMove__master_id" ';
            $sql .= '     , f.internal_code            as "TrnMove__internal_code" ';
            $sql .= '     , f.item_name                as "TrnMove__item_name" ';
            $sql .= '     , f.item_code                as "TrnMove__item_code" ';
            $sql .= '     , f.standard                 as "TrnMove__standard" ';
            $sql .= '     , f1.dealer_name             as "TrnMove__dealer_name" ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when e.per_unit = 1  ';
            $sql .= '         then e1.unit_name  ';
            $sql .= "         else e1.unit_name || '(' || e.per_unit || e2.unit_name || ')'  ";
            $sql .= '         end ';
            $sql .= '     )                            as "TrnMove__unit_name" ';
            $sql .= '     , e.id                       as "TrnMove__mst_item_unit_id"';
            $sql .= '     , e.per_unit                 as "TrnMove__per_unit" ';
            $sql .= '     , g.facility_sticker_no      as "TrnMove__facility_sticker_no" ';
            $sql .= '     , g.transaction_price        as "TrnMove__transaction_price" ';
            $sql .= '     , c1.department_name         as "TrnMove__department_name_from" ';
            $sql .= '     , c2.facility_name           as "TrnMove__facility_name_from" ';
            $sql .= '     , d1.department_name         as "TrnMove__department_name_to" ';
            $sql .= '     , d2.facility_name           as "TrnMove__facility_name_to"  ';
            $sql .= '   from ';
            $sql .= '     trn_move_headers as a  ';
            $sql .= '     left join trn_shippings as b  ';
            $sql .= '       on b.trn_move_header_id = a.id  ';
            $sql .= '     left join mst_departments as c1  ';
            $sql .= '       on c1.id = b.department_id_from  ';
            $sql .= '     left join mst_facilities as c2  ';
            $sql .= '       on c2.id = c1.mst_facility_id  ';
            $sql .= '     left join mst_departments as d1  ';
            $sql .= '       on d1.id = b.department_id_to  ';
            $sql .= '     left join mst_facilities as d2  ';
            $sql .= '       on d2.id = d1.mst_facility_id  ';
            $sql .= '     left join trn_receivings as h ';
            $sql .= '       on h.trn_shipping_id = b.id  ';
            $sql .= '     left join mst_item_units as e  ';
            $sql .= '       on e.id = h.mst_item_unit_id  ';
            $sql .= '     left join mst_unit_names as e1  ';
            $sql .= '       on e1.id = e.mst_unit_name_id  ';
            $sql .= '     left join mst_unit_names as e2  ';
            $sql .= '       on e2.id = e.per_unit_name_id  ';
            $sql .= '     left join mst_facility_items as f ';
            $sql .= '       on f.id = e.mst_facility_item_id ';
            $sql .= '     left join mst_dealers as f1 ';
            $sql .= '       on f1.id = f.mst_dealer_id ';
            $sql .= '     left join trn_stickers as g  ';
            $sql .= '       on g.id = b.trn_sticker_id  ';
            $sql .= '   where ';
            $sql .= '     b.id in ( ' . join(',',$this->request->data['TrnMove']['id']) . ') ';
            if(isset($this->request->data['Search']['center_move']) && $this->request->data['Search']['center_move'] == 1){
                $result = $this->TrnMoveHeader->query($sql , false , false );
            }else{
                $result = $this->TrnMoveHeader->query($sql);
            }
            $this->set('result' , $result );
            $this->Session->write('TrnMoveHeader.result' , $result);
        }else{
            //2度押し対策
            $this->set('result' , $this->Session->read('TrnMoveHeader.result'));
        }
        $this->render('receive_result_view');
    }

    /**
     * 管理外移出
     * 初期画面
     */
    function move2outside() {
        $this->setRoleFunction(39); //管理外移出

        //作業区分を取得
        $this->__getWorkClasses();
        $this->request->data['TrnMoveHeader']['work_date'] = date('Y/m/d');
    }

    /**
     * 管理外移出
     * 検索画面
     */
    function move2outside_search(){
        //検索結果格納変数
        $result = array();
        $cartResult = array();
        
        if(isset($this->request->data['TrnMoveHeader']['isSearch']) && $this->request->data['TrnMoveHeader']['isSearch'] == 1){
            // 条件文
            $where = '';
            // 商品ID
            if( !empty($this->request->data['MstFacilityItem']['internal_code'])) {
                $where .= " and c.internal_code like '%" . pg_escape_string($this->request->data['MstFacilityItem']['internal_code']) . "%'";
            }
            // 製品番号
            if( !empty($this->request->data['MstFacilityItem']['item_code'])) {
                $where .= " and c.item_code like '%" . pg_escape_string($this->request->data['MstFacilityItem']['item_code']) . "%'";
            }
            // 商品名
            if( !empty($this->request->data['MstFacilityItem']['item_name'])) {
                $where .= " and c.item_name like '%" . pg_escape_string($this->request->data['MstFacilityItem']['item_name']) . "%'";
            }
            // 規格
            if( !empty($this->request->data['MstFacilityItem']['standard'])) {
                $where .= " and c.standard like '%" . pg_escape_string($this->request->data['MstFacilityItem']['standard']) . "%'";
            }
            // 販売元
            if( !empty($this->request->data['MstDealer']['dealer_name'])) {
                $where .= " and c1.dealer_name like '%" . pg_escape_string($this->request->data['MstDealer']['dealer_name']) . "%'";
            }
            // シール番号
            if( !empty($this->request->data['TrnSticker']['facility_sticker_no'])) {
                $where .= " and ( a.facility_sticker_no ILIKE '%" . pg_escape_string($this->request->data['TrnSticker']['facility_sticker_no']) . "%'";
                $where .= " or a.hospital_sticker_no ILIKE '%" . pg_escape_string($this->request->data['TrnSticker']['facility_sticker_no']) . "%' )";
            }
            // ロット番号
            if( !empty($this->request->data['TrnSticker']['lot_no'])) {
                $where .= " and a.lot_no like '%" . pg_escape_string($this->request->data['TrnSticker']['lot_no']) . "%'";
            }
            //カートに入っているものは除く
            if(!empty($this->request->data['TrnMoveHeader']['tempId'])){
                $where .= " and a.id not IN (" . $this->request->data['TrnMoveHeader']['tempId'] . ")";
            }
            $where .= " and c.item_type = " . Configure::read('Items.item_types.normalitem');// セット品は除外
            $where .= " and a.has_reserved = false ";                                        // 予約済みは除外
            $where .= " and a.lot_no <> ' '";                                                // 未入荷レコードは除外
            $where .= " and e.id = '".$this->Session->read('Auth.facility_id_selected')."'"; // 倉庫在庫のみ対象
            $where .= " and c.is_lowlevel = false ";                                         // 低レベル品は除外
            
            $result = $this->getMoveData($where , $this->request->data['limit']);
            
            //カート商品検索
            $cartResult = array();
            if(!empty($this->request->data['TrnMoveHeader']['tempId'])){
                $cartResult = $this->getMoveData(' and a.id in (' . $this->request->data['TrnMoveHeader']['tempId'] . ')');
            }
        }else{
            //在庫締めチェック
            if(false === $this->canUpdateAfterClose($this->request->data['TrnMoveHeader']['work_date'], $this->Session->read('Auth.facility_id_selected'))){
                $this->Session->setFlash('在庫締めが行われている為更新できません', 'growl', array('type'=>'error'));
                $this->redirect('move2outside');
                return;
            }
        }
        
        $this->set('cartResult',$cartResult);  //カート明細検索結果
        $this->set('result',$result);          //明細検索結果

    }

    /**
     * 管理外移出確認
     */
    function move2outside_confirm() {
        $this->__getWorkClasses();
        $result = $this->getMoveData(' and a.id in (' . $this->request->data['TrnMoveHeader']['cartId'] . ')');

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnMoveHeader.token' , $token);
        $this->request->data['TrnMoveHeader']['token'] = $token;
        $this->set('result',$result);
    }

    /**
     * 管理外移出結果
     */
    function move2outside_result() {
        if($this->request->data['TrnMoveHeader']['token'] === $this->Session->read('TrnMoveHeader.token')) {
            $this->Session->delete('TrnMoveHeader.token');

            $now = date('Y/m/d H:i:s.u');
            // トランザクション開始
            $this->TrnMoveHeader->begin();
            //行ロック
            $this->TrnSticker->query('select * from trn_stickers as a where a.id in (' .join(',' ,$this->request->data['stickerId']). ') for update ');

            //更新チェック
            $ret = $this->TrnSticker->query('select count(*) from trn_stickers as a where a.id in (' .join(',' ,$this->request->data['stickerId']). ") and a.modified > '" . $this->request->data['TrnMoveHeader']['time'] . "'");
            if($ret[0][0]['count'] > 0){
                $this->TrnMoveHeader->rollback();
                $this->Session->setFlash('ほかユーザによって更新されています。やり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('move2outside');
            }

            //作業番号を取得
            $work_no = $this->setWorkNo4Header(date("Y/m/d"), 14);
            
            //移動元(センター倉庫）
            $from_department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'), Configure::read('DepartmentType.warehouse'));

            $sql  = ' select ';
            $sql .= '       b.id  ';
            $sql .= '   from ';
            $sql .= '     mst_facilities as a  ';
            $sql .= '     left join mst_departments as b  ';
            $sql .= '       on b.mst_facility_id = a.id  ';
            $sql .= '   where ';
            $sql .= '     a.facility_type = ' . Configure::read('FacilityType.outside');
            $ret = $this->MstFacility->query($sql);
            // 移動先(管理外部署)
            $to_department_id =$ret[0][0]['id'];
            
            $save_data = array();
            $save_data['TrnMoveHeader'] = array();
            $save_data['TrnMoveHeader']['work_no']               = $work_no;
            $save_data['TrnMoveHeader']['work_type']             = $this->request->data['TrnMoveHeader']['work_class'];
            $save_data['TrnMoveHeader']['work_date']             = $this->request->data['TrnMoveHeader']['work_date'];
            $save_data['TrnMoveHeader']['recital']               = $this->request->data['TrnMoveHeader']['recital'];
            $save_data['TrnMoveHeader']['move_status']           = Configure::read('MoveStatus.complete');
            $save_data['TrnMoveHeader']['move_type']             = Configure::read("MoveType.move2outside_export");
            $save_data['TrnMoveHeader']['department_id_from']    = $from_department_id; //移動元(センター倉庫）
            $save_data['TrnMoveHeader']['department_id_to']      = $to_department_id; // 移動先(管理外部署)
            $save_data['TrnMoveHeader']['detail_count']          = count($this->request->data['stickerId']);
            $save_data['TrnMoveHeader']['detail_total_quantity'] = count($this->request->data['stickerId']);
            $save_data['TrnMoveHeader']['is_deleted']            = false;
            $save_data['TrnMoveHeader']['creater']               = $this->Session->read('Auth.MstUser.id');
            $save_data['TrnMoveHeader']['modifier']              = $this->Session->read('Auth.MstUser.id');
            $save_data['TrnMoveHeader']['created']               = $now;
            $save_data['TrnMoveHeader']['modified']              = $now;

            $this->TrnMoveHeader->create();
            if (!$this->TrnMoveHeader->save($save_data)) {
                $this->TrnMoveHeader->rollback();
                $this->Session->setFlash('移動ヘッダ登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('move2outside');
            }
            
            $trn_move_header_id = $this->TrnMoveHeader->getLastInsertId();
            
            //シール情報取得
            $seq = 1;
            // 明細データ取得
            $sql  = ' select ';
            $sql .= '       a.id as trn_sticker_id ';
            $sql .= '     , a.facility_sticker_no  ';
            $sql .= '     , a.hospital_sticker_no ';
            $sql .= '     , a.mst_item_unit_id ';
            $sql .= '     , a.mst_department_id ';
            $sql .= '     , a.trn_order_id ';
            $sql .= '     , a.original_price ';
            $sql .= '     , a.transaction_price  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in ( ' . join(',',$this->request->data['stickerId']) . ' ) ';
            
            $data = $this->TrnSticker->query($sql);
            
            foreach($data as $d){
                // 移動（出荷）
                $shipping = array();
                $shipping['TrnShipping']['work_no']                = $work_no;
                $shipping['TrnShipping']['work_seq']               = $seq;
                $shipping['TrnShipping']['work_date']              = $this->request->data['TrnMoveHeader']['work_date'];
                $shipping['TrnShipping']['work_type']              = $this->request->data['Moves']['work_class'][$d[0]['trn_sticker_id']];
                $shipping['TrnShipping']['recital']                = $this->request->data['Moves']['recital'][$d[0]['trn_sticker_id']];
                $shipping['TrnShipping']['shipping_type']          = Configure::read('ShippingType.move');
                $shipping['TrnShipping']['shipping_status']        = Configure::read('ShippingStatus.loaded');
                $shipping['TrnShipping']['mst_item_unit_id']       = $d[0]['mst_item_unit_id'];
                $shipping['TrnShipping']['department_id_from']     = $d[0]['mst_department_id'];
                $shipping['TrnShipping']['department_id_to']       = $to_department_id;
                $shipping['TrnShipping']['quantity']               = 1;
                $shipping['TrnShipping']['trn_sticker_id']         = $d[0]['trn_sticker_id'];
                $shipping['TrnShipping']['is_deleted']             = false;
                $shipping['TrnShipping']['trn_move_header_id']     = $trn_move_header_id;
                $shipping['TrnShipping']['creater']                = $this->Session->read('Auth.MstUser.id');
                $shipping['TrnShipping']['modifier']               = $this->Session->read('Auth.MstUser.id');
                $shipping['TrnShipping']['created']                = $now;
                $shipping['TrnShipping']['modified']               = $now;
                $shipping['TrnShipping']['move_status']            = Configure::read('MoveStatus.complete');
                $shipping['TrnShipping']['facility_sticker_no']    = $d[0]['facility_sticker_no'];
                $shipping['TrnShipping']['hospital_sticker_no']    = $d[0]['hospital_sticker_no'];
                $this->TrnShipping->create();
                if (!$this->TrnShipping->save($shipping)) {
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('移動明細（出荷）登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('move2outside');
                }
                $trn_shipping_id = $this->TrnShipping->getLastInsertId();  //出荷登録ID
                
                // 移動（入荷）
                $receiving = array();
                $receiving['TrnReceiving']['work_no']                 = $work_no;
                $receiving['TrnReceiving']['work_seq']                = $seq;
                $receiving['TrnReceiving']['work_date']               = $this->request->data['TrnMoveHeader']['work_date'];
                $receiving['TrnReceiving']['work_type']               = $this->request->data['Moves']['work_class'][$d[0]['trn_sticker_id']];
                $receiving['TrnReceiving']['recital']                 = $this->request->data['Moves']['recital'][$d[0]['trn_sticker_id']];
                $receiving['TrnReceiving']['receiving_type']          = Configure::read('ReceivingType.move');
                $receiving['TrnReceiving']['receiving_status']        = Configure::read('ReceivingStatus.stock');
                $receiving['TrnReceiving']['mst_item_unit_id']        = $d[0]['mst_item_unit_id'];
                $receiving['TrnReceiving']['department_id_from']      = $d[0]['mst_department_id'];
                $receiving['TrnReceiving']['department_id_to']        = $to_department_id;
                $receiving['TrnReceiving']['quantity']                = 1;
                $receiving['TrnReceiving']['remain_count']            = 0;  //入庫済みなので0固定
                $receiving['TrnReceiving']['stocking_price']          = $d[0]['original_price'];
                $receiving['TrnReceiving']['sales_price']             = $d[0]['transaction_price'];
                $receiving['TrnReceiving']['trn_sticker_id']          = $d[0]['trn_sticker_id'];
                $receiving['TrnReceiving']['trn_shipping_id']         = $trn_shipping_id;
                $receiving['TrnReceiving']['trn_order_id']            = $d[0]['trn_order_id'];
                $receiving['TrnReceiving']['trn_move_header_id']      = $trn_move_header_id;
                $receiving['TrnReceiving']['is_retroactable']         = true;
                $receiving['TrnReceiving']['is_deleted']              = false;
                $receiving['TrnReceiving']['creater']                 = $this->Session->read('Auth.MstUser.id');
                $receiving['TrnReceiving']['modifier']                = $this->Session->read('Auth.MstUser.id');
                $receiving['TrnReceiving']['created']                 = $now;
                $receiving['TrnReceiving']['modified']                = $now;
                $receiving['TrnReceiving']['facility_sticker_no']     = $d[0]['facility_sticker_no'];
                $receiving['TrnReceiving']['hospital_sticker_no']     = $d[0]['hospital_sticker_no'];
                $this->TrnReceiving->create();
                if (!$this->TrnReceiving->save($receiving)) {
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('移動明細（入荷）登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('move2outside');
                }
                $trn_receiving_id = $this->TrnReceiving->getLastInsertId();  //入荷登録ID
                
                // シール移動履歴
                $sticker_records = array();
                $sticker_records['TrnStickerRecord']['move_date']           = $this->request->data['TrnMoveHeader']['work_date'];
                $sticker_records['TrnStickerRecord']['move_seq']            = $this->getNextRecord($d[0]['trn_sticker_id']);
                $sticker_records['TrnStickerRecord']['record_type']         = Configure::read('RecordType.move');
                $sticker_records['TrnStickerRecord']['record_id']           = $trn_shipping_id;
                $sticker_records['TrnStickerRecord']['mst_department_id']   = $to_department_id;
                $sticker_records['TrnStickerRecord']['trn_sticker_id']      = $d[0]['trn_sticker_id'];
                $sticker_records['TrnStickerRecord']['facility_sticker_no'] = $d[0]['facility_sticker_no'];
                $sticker_records['TrnStickerRecord']['hospital_sticker_no'] = $d[0]['hospital_sticker_no'];
                $sticker_records['TrnStickerRecord']['is_deleted']          = false;
                $sticker_records['TrnStickerRecord']['creater']             = $this->Session->read('Auth.MstUser.id');
                $sticker_records['TrnStickerRecord']['modifier']            = $this->Session->read('Auth.MstUser.id');
                $sticker_records['TrnStickerRecord']['created']             = $now;
                $sticker_records['TrnStickerRecord']['modified']            = $now;
                $this->TrnStickerRecord->create();
                if (!$this->TrnStickerRecord->save($sticker_records)) {
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('シール移動履歴処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('move2outside');
                }
                
                // シール情報更新
                $this->TrnSticker->create();
                $ret = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.mst_department_id'  => $to_department_id,
                        'TrnSticker.last_move_date'     => "'".$this->request->data['TrnMoveHeader']['work_date']."'",
                        'TrnSticker.trn_move_header_id' => $trn_move_header_id,
                        //'TrnSticker.transaction_price'  => $this->request->data['Moves']['price'][$d[0]['trn_sticker_id']] ,
                        'TrnSticker.modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'           => "'" . $now . "'"
                        ),
                    array(
                        'TrnSticker.id' => $d[0]['trn_sticker_id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('シール情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('move2outside');
                }
                
                // 移動元在庫減
                $this->TrnStock->create();
                $ret = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.stock_count' => 'stock_count - 1 ',
                        'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'    => "'" . $now . "'"
                        ),
                    array(
                        'TrnStock.id' => $this->getStockRecode($d[0]['mst_item_unit_id'] , $d[0]['mst_department_id']),
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('在庫情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('move2outside');
                }
                
                $seq++;
            }
            
            //コミット直前に更新チェック
            $ret = $this->TrnSticker->query('select count(*) from trn_stickers as a where a.id in (' .join(',' ,$this->request->data['stickerId']). ") and a.modified > '" . $this->request->data['TrnMoveHeader']['time'] . "' and a.modified <> '" .$now. "'");
            if($ret[0][0]['count'] > 0){
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('move2outside');
            }
            
            
            $this->TrnMoveHeader->commit();

            $this->Session->setFlash('管理外移出処理が完了しました', 'growl', array('type'=>'star') );
            
            $resultView = $this->getMoveData(' and a.id in ( ' . join(',',$this->request->data['stickerId']) . ' ) ');
            $this->set('result',$resultView);
            $this->Session->write('TrnMoveHeader.result' , $resultView);
        }else{
            //2度押し対策
            $this->set('result' , $this->Session->read('TrnMoveHeader.result'));
        }
        $this->__getWorkClasses();
        $this->render('move2outside_result_view');
    }

    /**
     * 管理外移入
     * 初期画面表示＆検索
     */
    function receive_from_outside () {
        $this->setRoleFunction(40); //管理外移入
        $this->__getWorkClasses();

        $result = array();
        $cartResult = array();

        //サーチフラグが立っていれば検索実行
        if(isset($this->request->data['search']['is_search'])){
            
            $where  = ' and a.item_type = ' . Configure::read('Items.item_types.normalitem');
            // 商品ID
            if( isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] != ''){
                $where .= ' and a.internal_code ILIKE'."'%".pg_escape_string( $this->request->data['MstFacilityItem']['internal_code'])."%'";
            }
            // 製品番号
            if( isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] != ''){
                $where .= ' and a.item_code ILIKE'."'%".pg_escape_string( $this->request->data['MstFacilityItem']['item_code'])."%'";
            }
            // 商品名
            if( isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] != ''){
                $where .= ' and a.item_name ILIKE'."'%".pg_escape_string( $this->request->data['MstFacilityItem']['item_name'])."%'";
            }
            // 販売元
            if( isset($this->request->data['MstFacilityItem']['dealer_name']) && $this->request->data['MstFacilityItem']['dealer_name'] != '') {
                $where .= ' and c.dealer_name ILIKE'."'%".pg_escape_string( $this->request->data['MstFacilityItem']['dealer_name'])."%'";
            }
            // 規格
            if( isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] != ''){
                $where .= ' and a.standard ILIKE'."'%".pg_escape_string( $this->request->data['MstFacilityItem']['standard'])."%'";
            }
            // JANコード
            if( isset($this->request->data['MstFacilityItem']['jan_code']) && $this->request->data['MstFacilityItem']['jan_code'] != ''){
                $where .= ' and a.jan_code ILIKE'."'%".pg_escape_string( $this->request->data['MstFacilityItem']['jan_code'])."%'";
            }
            //カートに入っているものは除く
            if( isset($this->request->data['select']['tempId']) && $this->request->data['select']['tempId'] != '' ) {
                $where .= " and b.id not in (".$this->request->data['select']['tempId'].")";
            }
            $result = $this->getFacilityItem($where , $this->request->data['limit']);
            //カート検索
            if( isset($this->request->data['select']['tempId']) && $this->request->data['select']['tempId'] != '' ) {
                $cartResult = $this->getFacilityItem(" and b.id in (".$this->request->data['select']['tempId'].")");
            }
        }
        
        $this->set('result',$result);
        $this->set('cartResult',$cartResult);
    }

    /**
     * 管理外移入
     * 確認画面表示
     */
    function receive_from_outside_confirm () {
        //検索画面から渡された採用品ID取得
        $result = $this->getFacilityItem(" and b.id in (".$this->request->data['select']['Id'].")");
        $this->request->data['TrnMoveHeader']['work_date'] = date('Y/m/d');

        $this->set('work_classes',$this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_type));
        $this->set('result',$result);

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnMoveHeader.token' , $token);
        $this->request->data['TrnMoveHeader']['token'] = $token;
    }

    /**
     * 管理外移入結果
     */
    function receive_from_outside_result(){
        if($this->request->data['TrnMoveHeader']['token'] === $this->Session->read('TrnMoveHeader.token')) {
            $this->Session->delete('TrnMoveHeader.token');
            $now = date('Y/m/d H:i:s.u');
            /*  移動処理を行う  */
            // トランザクション開始
            $this->TrnMoveHeader->begin();

            //移動先(センター倉庫）
            $to_department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'), Configure::read('DepartmentType.warehouse'));

            $sql  = ' select ';
            $sql .= '       b.id  ';
            $sql .= '   from ';
            $sql .= '     mst_facilities as a  ';
            $sql .= '     left join mst_departments as b  ';
            $sql .= '       on b.mst_facility_id = a.id  ';
            $sql .= '   where ';
            $sql .= '     a.facility_type = ' . Configure::read('FacilityType.outside');
            $ret = $this->MstFacility->query($sql);
            // 移動先(管理外部署)
            $from_department_id =$ret[0][0]['id'];
            
            //行ロック
            $this->TrnStock->query('select * from trn_stocks as a where a.mst_item_unit_id in ( ' . join(',',$this->request->data['Receive']['ItemId']) . ') and a.mst_department_id = ' . $to_department_id .  ' for update ' );
            
            //更新チェック
            $ret = $this->TrnStock->query('select count(*) from trn_stocks as a where a.mst_item_unit_id in ( ' . join(',',$this->request->data['Receive']['ItemId']) . " ) and a.mst_department_id = " . $to_department_id .  " and a.modified > '" . $this->request->data['TrnMoveHeader']['time'] . "'");
            if($ret[0][0]['count'] > 0){
                $this->TrnMoveHeader->rollback();
                $this->Session->setFlash('ほかユーザによって更新されています。最初からやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('receive_from_outside');
            }
            
            //ワークNo取得
            $work_no = $this->setWorkNo4Header(date("Y/m/d"), 14);
            $this->request->data['TrnMoveHeader']['work_no'] = $work_no;
            
            $save_data = array();
            $save_data['TrnMoveHeader'] = array();
            $save_data['TrnMoveHeader']['work_no']               = $work_no;
            $save_data['TrnMoveHeader']['work_type']             = $this->request->data['TrnMoveHeader']['work_class'];
            $save_data['TrnMoveHeader']['work_date']             = $this->request->data['TrnMoveHeader']['work_date'];
            $save_data['TrnMoveHeader']['recital']               = $this->request->data['TrnMoveHeader']['recital'];
            $save_data['TrnMoveHeader']['move_status']           = Configure::read('MoveStatus.complete');
            $save_data['TrnMoveHeader']['move_type']             = Configure::read("MoveType.move2outside_import");
            $save_data['TrnMoveHeader']['department_id_from']    = $from_department_id;  //管理外部署
            $save_data['TrnMoveHeader']['department_id_to']      = $to_department_id;    //センター部署
            $save_data['TrnMoveHeader']['detail_count']          = count($this->request->data['Receive']['ItemId']);
            $save_data['TrnMoveHeader']['detail_total_quantity'] = count($this->request->data['Receive']['ItemId']);
            $save_data['TrnMoveHeader']['is_deleted']            = false;
            $save_data['TrnMoveHeader']['creater']               = $this->Session->read('Auth.MstUser.id');
            $save_data['TrnMoveHeader']['modifier']              = $this->Session->read('Auth.MstUser.id');
            $save_data['TrnMoveHeader']['created']               = $now;
            $save_data['TrnMoveHeader']['modified']              = $now;
            
            $this->TrnMoveHeader->create();
            if (!$this->TrnMoveHeader->save($save_data)) {
                $this->TrnMoveHeader->rollback();
                $this->Session->setFlash('移動ヘッダ登録処理中にエラーが発生しました。', 'growl', array('type'=>'error') );
                $this->redirect('receive_from_outside');
            }
            
            $trn_move_header_id = $this->TrnMoveHeader->getLastInsertId();  //移動ヘッダID

            $seq = 1;
            
            // 必要情報の取得
            $sql  = ' select ';
            $sql .= '       a.id ';
            $sql .= '     , b.id                 as trn_sticker_id  ';
            $sql .= '   from ';
            $sql .= '     mst_item_units as a  ';
            $sql .= '     left join trn_stickers as b  ';
            $sql .= '       on b.mst_item_unit_id = a.id  ';
            $sql .= '       and b.mst_department_id = ' . $to_department_id;
            $sql .= '       and b.is_deleted = false  ';
            $sql .= "       and b.lot_no = ' ' ";
            $sql .= "       and b.facility_sticker_no = '' "; 
            $sql .= "       and b.hospital_sticker_no = '' ";
            $sql .= '   where ';
            $sql .= '       a.id in ( ' . join(',',$this->request->data['Receive']['ItemId']) . ')';
            $result = $this->TrnSticker->query($sql);
            
            foreach($result as $r){
                if(is_null($r[0]['trn_sticker_id'])){
                    // 新規作成
                    $sticker = array();
                    $sticker['TrnSticker'] = array();
                    $sticker['TrnSticker']['facility_sticker_no']     = "";
                    $sticker['TrnSticker']['hospital_sticker_no']     = "";
                    $sticker['TrnSticker']['mst_item_unit_id']        = $r[0]['id'];
                    $sticker['TrnSticker']['mst_department_id']       = $to_department_id;
                    $sticker['TrnSticker']['trade_type']              = Configure::read('TeisuType.Constant');
                    $sticker['TrnSticker']['quantity']                = $this->request->data['Receive']['Quantity'][$r[0]['id']];
                    $sticker['TrnSticker']['lot_no']                  = " ";  //ロットは半角スペース
                    $sticker['TrnSticker']['original_price']          = $this->request->data['Receive']['Price'][$r[0]['id']];
                    $sticker['TrnSticker']['transaction_price']       = $this->request->data['Receive']['Price'][$r[0]['id']];
                    $sticker['TrnSticker']['last_move_date']          = $this->request->data['TrnMoveHeader']['work_date'];
                    $sticker['TrnSticker']['is_deleted']              = false;
                    $sticker['TrnSticker']['creater']                 = $this->Session->read('Auth.MstUser.id');
                    $sticker['TrnSticker']['modifier']                = $this->Session->read('Auth.MstUser.id');
                    $sticker['TrnSticker']['created']                 = $now;
                    $sticker['TrnSticker']['modified']                = $now;
                    $sticker['TrnSticker']['has_reserved']            = false;
                    $sticker['TrnSticker']['trn_move_header_id']      = $trn_move_header_id;
                    //登録
                    $this->TrnSticker->create();
                    if (!$this->TrnSticker->save($sticker)) {
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('シール情報登録処理中にエラーが発生しました。', 'growl', array('type'=>'error') );
                        $this->redirect('receive_from_outside');
                    }
                    
                    $r[0]['trn_sticker_id'] = $this->TrnSticker->getLastInsertId();  //シールID
                }else{
                    // 更新
                    $this->TrnSticker->create();
                    $ret = $this->TrnSticker->updateAll(
                        array(
                            'TrnSticker.quantity'           => 'quantity + ' . $this->request->data['Receive']['Quantity'][$r[0]['id']],
                            'TrnSticker.original_price'     => $this->request->data['Receive']['Price'][$r[0]['id']],
                            'TrnSticker.transaction_price'  => $this->request->data['Receive']['Price'][$r[0]['id']],
                            'TrnSticker.trn_move_header_id' => $trn_move_header_id,
                            'TrnSticker.last_move_date'     => "'" . $this->request->data['TrnMoveHeader']['work_date'] . "'",
                            'TrnSticker.modifier'           => $this->Session->read('Auth.MstUser.id'),
                            'TrnSticker.modified'           => "'" . $now . "'"
                            ),
                        array(
                            'TrnSticker.id' => $r[0]['trn_sticker_id']
                            ),
                        -1
                        );
                    if(!$ret) {
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('シール情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('receive_from_outside');
                    }
                }

                $shipping = array();
                $shipping['TrnShipping'] = array();
                $shipping['TrnShipping']['work_no']                = $work_no;
                $shipping['TrnShipping']['work_seq']               = $seq;
                $shipping['TrnShipping']['work_date']              = $this->request->data['TrnMoveHeader']['work_date'];
                $shipping['TrnShipping']['shipping_type']          = Configure::read('ShippingType.move');
                $shipping['TrnShipping']['shipping_status']        = Configure::read('ShippingStatus.ordered');
                $shipping['TrnShipping']['mst_item_unit_id']       = $r[0]['id'];
                $shipping['TrnShipping']['department_id_from']     = $from_department_id;
                $shipping['TrnShipping']['department_id_to']       = $to_department_id;
                $shipping['TrnShipping']['quantity']               = $this->request->data['Receive']['Quantity'][$r[0]['id']];
                $shipping['TrnShipping']['trn_sticker_id']         = $r[0]['trn_sticker_id'];
                $shipping['TrnShipping']['is_deleted']             = false;
                $shipping['TrnShipping']['trn_move_header_id']     = $trn_move_header_id;
                $shipping['TrnShipping']['creater']                = $this->Session->read('Auth.MstUser.id');
                $shipping['TrnShipping']['modifier']               = $this->Session->read('Auth.MstUser.id');
                $shipping['TrnShipping']['created']                = $now;
                $shipping['TrnShipping']['modified']               = $now;
                $shipping['TrnShipping']['move_status']            = Configure::read('MoveStatus.complete');
                $shipping['TrnShipping']['subcode']                = $this->request->data['TrnMoveHeader']['subcode'];
                //登録
                $this->TrnShipping->create();
                if (!$this->TrnShipping->save($shipping)) {
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('移動情報（出荷）登録処理中にエラーが発生しました。', 'growl', array('type'=>'error') );
                    $this->redirect('receive_from_outside');
                }
                $trn_shipping_id = $this->TrnShipping->getLastInsertId();  //出荷登録ID


                $receiving = array();
                $receiving['TrnReceiving'] = array();
                $receiving['TrnReceiving']['work_no']                 = $work_no;
                $receiving['TrnReceiving']['work_seq']                = $seq;
                $receiving['TrnReceiving']['work_date']               = $this->request->data['TrnMoveHeader']['work_date'];
                $receiving['TrnReceiving']['receiving_type']          = Configure::read('ReceivingType.arrival');
                $receiving['TrnReceiving']['receiving_status']        = Configure::read('ReceivingStatus.arrival');
                $receiving['TrnReceiving']['mst_item_unit_id']        = $r[0]['id'];
                $receiving['TrnReceiving']['department_id_from']      = $from_department_id;
                $receiving['TrnReceiving']['department_id_to']        = $to_department_id;
                $receiving['TrnReceiving']['quantity']                = $this->request->data['Receive']['Quantity'][$r[0]['id']];
                $receiving['TrnReceiving']['remain_count']            = $this->request->data['Receive']['Quantity'][$r[0]['id']];
                $receiving['TrnReceiving']['stocking_price']          = $this->request->data['Receive']['Price'][$r[0]['id']];
                $receiving['TrnReceiving']['trn_sticker_id']          = $r[0]['trn_sticker_id'];
                $receiving['TrnReceiving']['trn_shipping_id']         = $trn_shipping_id;
                $receiving['TrnReceiving']['stocking_close_type']     = Configure::read('StockingCloseType.none');
                $receiving['TrnReceiving']['sales_close_type']        = Configure::read('SalesCloseType.none');
                $receiving['TrnReceiving']['facility_close_type']     = Configure::read('FacilityCloseType.none');
                $receiving['TrnReceiving']['trn_move_header_id']      = $trn_move_header_id;
                $receiving['TrnReceiving']['hospital_sticker_no']     = "";
                $receiving['TrnReceiving']['is_retroactable']         = true;
                $receiving['TrnReceiving']['is_deleted']              = false;
                $receiving['TrnReceiving']['creater']                 = $this->Session->read('Auth.MstUser.id');
                $receiving['TrnReceiving']['modifier']                = $this->Session->read('Auth.MstUser.id');
                $receiving['TrnReceiving']['created']                 = $now;
                $receiving['TrnReceiving']['modified']                = $now;
                $receiving['TrnReceiving']['subcode']                 = $this->request->data['TrnMoveHeader']['subcode'];

                //登録
                $this->TrnReceiving->create();
                if (!$this->TrnReceiving->save($receiving)) {
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('移動情報（入荷）登録処理中にエラーが発生しました。', 'growl', array('type'=>'error') );
                    $this->redirect('receive_from_outside');
                }
                $trn_receiving_id = $this->TrnReceiving->getLastInsertId();  //入荷登録ID

                //移動先の在庫数プラス
                $this->TrnStock->create();
                $ret = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.receipted_count' => 'TrnStock.receipted_count + ' . $this->request->data['Receive']['Quantity'][$r[0]['id']],
                        'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'        => "'" . $now . "'"
                        ),
                    array(
                        'TrnStock.id' => $this->getStockRecode( $r[0]['id'],
                                                                $to_department_id)
                        ),
                    -1
                    );
                if(!$ret) {
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('シール情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('receive_from_outside');
                }
                
                $seq++;
            }

            //コミット前に更新チェック
            $ret = $this->TrnStock->query('select count(*) from trn_stocks as a where a.mst_item_unit_id in ( ' . join(',',$this->request->data['Receive']['ItemId']) . " ) and a.mst_department_id = " . $to_department_id .  " and a.modified > '" . $this->request->data['TrnMoveHeader']['time'] . "' and a.modified <> '" .$now. "'");
            if($ret[0][0]['count'] > 0){
                $this->TrnMoveHeader->rollback();
                $this->Session->setFlash('ほかユーザによって更新されています。最初からやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('receive_from_outside');
            }


            $this->TrnMoveHeader->commit();
            $this->Session->setFlash("施設外移入登録が完了しました", 'growl', array('type'=>'star') );
            //完了画面用表示項目取得
            $sql  = ' select ';
            $sql .= '       d.internal_code      as "TrnShipping__internal_code"';
            $sql .= '     , d.item_name          as "TrnShipping__item_name"';
            $sql .= '     , d.standard           as "TrnShipping__standard" ';
            $sql .= '     , d.item_code          as "TrnShipping__item_code"';
            $sql .= '     , e.dealer_name        as "TrnShipping__dealer_name"';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when c.per_unit = 1  ';
            $sql .= '         then f.unit_name  ';
            $sql .= "         else f.unit_name || '(' || c.per_unit || g.unit_name || ')'  ";
            $sql .= '         end ';
            $sql .= '     )                      as "TrnShipping__unit_name"';
            $sql .= '     , b.quantity           as "TrnShipping__quantity"';
            $sql .= '     , h.transaction_price  as "TrnShipping__price"';
            $sql .= '   from ';
            $sql .= '     trn_move_headers as a  ';
            $sql .= '     left join trn_shippings as b  ';
            $sql .= '       on b.trn_move_header_id = a.id  ';
            $sql .= '     left join mst_item_units as c  ';
            $sql .= '       on c.id = b.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as d  ';
            $sql .= '       on d.id = c.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as e  ';
            $sql .= '       on e.id = d.mst_dealer_id  ';
            $sql .= '     left join mst_unit_names as f  ';
            $sql .= '       on f.id = c.mst_unit_name_id  ';
            $sql .= '     left join mst_unit_names as g  ';
            $sql .= '       on g.id = c.per_unit_name_id  ';
            $sql .= '     left join trn_stickers as h  ';
            $sql .= '       on h.id = b.trn_sticker_id  ';
            $sql .= '   where ';
            $sql .= '     a.id = ' . $trn_move_header_id;
            $sql .= " order by d.id";
            $result = $this->MstFacilityItem->query($sql);
            
            $this->set("data",$this->request->data);
            $this->set('work_classes',$this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_type));
            $this->set('result',$result);
            $this->Session->write('TrnMoveHeader.data' , $this->request->data);
            $this->Session->write('TrnMoveHeader.work_classes' , $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_type));
            $this->Session->write('TrnMoveHeader.result' , $result);
            
        }else{
            //2度押し対策
            $this->request->data = $this->Session->read('TrnMoveHeader.data');
            $this->set('data' , $this->request->data);
            $this->set('work_classes', $this->Session->read('TrnMoveHeader.work_classes'));
            $this->set('result' , $this->Session->read('TrnMoveHeader.result'));
        }
        $this->render('receive_from_outside_result_view');
    }
    
    /**
     * 管理外移入
     * 表示項目取得用SQL
     */
    function getFacilityItem($where='' , $limit=null){
        $sql  = 'select ';
        $sql .= '       b.id            as "TrnMove__id"';
        $sql .= '      ,a.internal_code as "TrnMove__internal_code"';
        $sql .= '      ,a.item_name     as "TrnMove__item_name"';
        $sql .= '      ,a.standard      as "TrnMove__standard"';
        $sql .= '      ,a.item_code     as "TrnMove__item_code"';
        $sql .= '      ,c.dealer_name   as "TrnMove__dealer_name"';
        $sql .= '      ,(case ';
        $sql .= '        when b.per_unit = 1 then ';
        $sql .= '        b1.unit_name ';
        $sql .= '        else ';
        $sql .= "        b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .= '        end )          as "TrnMove__unit_name"';
        $sql .= '  from mst_facility_items as a';
        $sql .= '  left join mst_item_units as b';
        $sql .= '    on a.id = b.mst_facility_item_id';
        $sql .= '   and b.is_deleted = false';
        $sql .= '  left join mst_unit_names as b1';
        $sql .= '    on b.mst_unit_name_id = b1.id';
        $sql .= '  left join mst_unit_names as b2';
        $sql .= '    on b.per_unit_name_id = b2.id';
        $sql .= '  left join mst_dealers as c';
        $sql .= '    on a.mst_dealer_id = c.id';
        $sql .= ' where a.is_deleted = false';
        $sql .= ' and a.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
        $sql .= ' and a.is_lowlevel = false';
        $sql .= $where ;
        $sql .= ' order by a.internal_code , b.per_unit ';
        if(!is_null($limit)){
            // 全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));
            $sql .= ' limit '. $limit;
        }
        
        return $this->MstFacilityItem->query($sql);
    }

    /**
     * 移動履歴
     */
    function history () {
        App::import('Sanitize');
        $this->setRoleFunction(41); //移動履歴
        // コンボボックスに必要な値のセット
        $this->set('facility_list' , $this->moveFacilityList(true));

        $result = array();

        //検索ボタン押下
        if (isset($this->request->data['isSearch']) && ($this->request->data['isSearch'] === '1')) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            /* 検索条件追加 */
            $where = '';
            //作業番号
            if($this->request->data['Search']['work_no']!=''){
                $where .= " and a.work_no = '" . $this->request->data['Search']['work_no'] . "'";
            }
            //日付From
            if($this->request->data['Search']['work_date_from']!=''){
                $where .= " and a.work_date >= '" . $this->request->data['Search']['work_date_from'] . "'";
            }
            //日付To
            if($this->request->data['Search']['work_date_to']!=''){
                $where .= " and a.work_date <= '" . $this->request->data['Search']['work_date_to'] . "'";
            }
            //移動区分
            if($this->request->data['Search']['move_type']!=''){
                $where .= " and a.move_type = " . $this->request->data['Search']['move_type'] ;
            }
            //作業区分
            if($this->request->data['Search']['work_type']!=''){
                $where .= " and a.work_type = " . $this->request->data['Search']['work_type'] ;
            }
            //商品ID
            if($this->request->data['Search']['internal_code']!=''){
                $where .= " and f.internal_code = '" . $this->request->data['Search']['internal_code'] . "'";
            }
            //製品番号
            if($this->request->data['Search']['item_code']!=''){
                $where .= " and f.item_code like '%" . $this->request->data['Search']['item_code'] . "%'";
            }
            //商品名
            if($this->request->data['Search']['item_name']!=''){
                $where .= " and f.item_name like '%" . $this->request->data['Search']['item_name'] . "%'";
            }
            //規格
            if($this->request->data['Search']['standard']!=''){
                $where .= " and f.standard like '%" . $this->request->data['Search']['standard'] . "%'";
            }
            //販売元名
            if($this->request->data['Search']['dealer_name']!=''){
                $where .= " and f1.dealer_name like '%" . $this->request->data['Search']['dealer_name'] . "%'";
            }
            //ロット番号
            if($this->request->data['Search']['lot_no']!=''){
                $where .= " and g.lot_no like '%" . $this->request->data['Search']['lot_no'] . "%'";
            }
            //シール番号
            if($this->request->data['Search']['sticker_no']!=''){
                $where .= " and ( g.hospital_sticker_no like '%" . $this->request->data['Search']['sticker_no'] . "%'";
                $where .= " or g.facility_sticker_no like '%" . $this->request->data['Search']['sticker_no'] . "%' ) ";
            }
            //管理区分
            if($this->request->data['Search']['trade_type']!=''){
                $where .= " and g.trade_type = " . $this->request->data['Search']['trade_type'] ;
            }
            //移動元施設
            if($this->request->data['Search']['from_facility_code']!=''){
                $where .= " and b2.facility_code = '" . $this->request->data['Search']['from_facility_code'] . "'";
            }
            //移動先施設
            if($this->request->data['Search']['to_facility_code']!=''){
                $where .= " and c2.facility_code = '" . $this->request->data['Search']['to_facility_code'] . "'";
            }
            //センター間移動を表示する(チェック無しの場合、センター間を表示しない。)
            if(!isset($this->request->data['Search']['center_move'])){//暫定処理
                $where .= ' and a.move_type != ' . Configure::read('MoveType.move2other');
            }
            $sql  = ' select ';
            $sql .= '       a.id                               as "TrnMoveHeader__id" ';
            $sql .= '     , a.work_no                          as "TrnMoveHeader__work_no" ';
            $sql .= '     , to_char(a.work_date, \'YYYY/mm/dd\') as "TrnMoveHeader__work_date" ';
            $sql .= '     , a.move_type                        as "TrnMoveHeader__move_type" ';
            $sql .= '     , b2.facility_name                   as "TrnMoveHeader__facility_name_from" ';
            $sql .= '     , b1.department_name                 as "TrnMoveHeader__department_name_from" ';
            $sql .= '     , c2.facility_name                   as "TrnMoveHeader__facility_name_to" ';
            $sql .= '     , c1.department_name                 as "TrnMoveHeader__department_name_to" ';
            $sql .= '     , a1.user_name                       as "TrnMoveHeader__user_name" ';
            $sql .= '     , to_char(a.created, \'YYYY/mm/dd\')   as "TrnMoveHeader__created" ';
            $sql .= '     , a.recital                          as "TrnMoveHeader__recital" ';
            $sql .= '     , count(*)                           as "TrnMoveHeader__count" ';
            $sql .= '     , a.detail_count                     as "TrnMoveHeader__detail_count"  ';
            $sql .= '   from ';
            $sql .= '     trn_move_headers as a  ';
            $sql .= '     left join mst_users as a1  ';
            $sql .= '       on a1.id = a.creater  ';
            $sql .= '     left join mst_departments as b1  ';
            $sql .= '       on b1.id = a.department_id_from  ';
            $sql .= '     left join mst_facilities as b2 ';
            $sql .= '       on b2.id = b1.mst_facility_id ';
            $sql .= '     left join mst_departments as c1 ';
            $sql .= '       on c1.id = a.department_id_to ';
            $sql .= '     left join mst_facilities as c2 ';
            $sql .= '       on c2.id = c1.mst_facility_id ';
            $sql .= '     left join trn_shippings as d ';
            $sql .= '       on d.trn_move_header_id = a.id ';
            $sql .= '     left join trn_receivings as d1 ';
            $sql .= '       on d1.trn_shipping_id = d.id ';
            $sql .= '     left join mst_item_units as e ';
            $sql .= '       on e.id = d.mst_item_unit_id ';
            $sql .= '     left join mst_item_units as e1 ';
            $sql .= '       on e1.id = d1.mst_item_unit_id ';
            $sql .= '     left join mst_facility_items as f ';
            $sql .= '       on f.id = ( case when ' . $this->Session->read('Auth.facility_id_selected') . ' = e1.mst_facility_id then e1.mst_facility_item_id else e.mst_facility_item_id end ) ';
            $sql .= '     left join mst_dealers as f1 ';
            $sql .= '       on f1.id = f.mst_dealer_id ';
            $sql .= '     left join trn_stickers as g ';
            $sql .= '       on g.id = d.trn_sticker_id ';

            /* 病院絞込みココから */
            $sql .= '     left join mst_user_belongings as x ';
            $sql .= '       on x.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '       and x.mst_facility_id = b2.id ';
            $sql .= '       and x.is_deleted = false ';
            $sql .= '     left join mst_user_belongings as y ';
            $sql .= '       on y.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '       and y.mst_facility_id = c2.id ';
            $sql .= '       and y.is_deleted = false ';
            /* 病院絞込みココまで */

            $sql .= '   where ';
            $sql .= '     ( a.move_type in (' . Configure::read('MoveType.move2other') . ', ' . Configure::read('MoveType.move2outside_export') . ', ' . Configure::read('MoveType.move2outside_import') . ') '; //センター間、移入、移出は病院操作権限は見ない。
            $sql .= '     or (  ';
            $sql .= '       a.move_type in (' . Configure::read('MoveType.move2inner') . ', ' . Configure::read('MoveType.move2outer') . ')  '; //施設内、施設外移動は、病院操作権限をチェック
            $sql .= '       and x.id is not null ';
            $sql .= '       and y.id is not null ';
            $sql .= '     ) ) ';
            $sql .= '     and a.is_deleted = false ';
            $sql .= '     and d.is_deleted = false ';
            $sql .= $where;
            $sql .= '     and ( c2.id = ' . $this->Session->read('Auth.facility_id_selected') ;
            $sql .= '     or b2.id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     or f.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') . ') ';

            $sql .= '   group by ';
            $sql .= '     a.id ';
            $sql .= '     , a.work_no ';
            $sql .= '     , a.work_date ';
            $sql .= '     , a.move_type ';
            $sql .= '     , b2.facility_name ';
            $sql .= '     , b1.department_name ';
            $sql .= '     , c2.facility_name ';
            $sql .= '     , c1.department_name ';
            $sql .= '     , a1.user_name ';
            $sql .= '     , a.created ';
            $sql .= '     , a.recital ';
            $sql .= '     , a.detail_count  ';
            $sql .= '   order by ';
            $sql .= '     a.work_no ';

            //全件数取得
            if(isset($this->request->data['Search']['center_move'])){//暫定処理
                $this->set('max' , $this->getMaxCount($sql , 'TrnMoveHeader' , false ));
            }else{
                $this->set('max' , $this->getMaxCount($sql , 'TrnMoveHeader'));
            }
            $sql .= ' limit ' . $this->request->data['limit'];

            if(isset($this->request->data['Search']['center_move'])){//暫定処理
                $result = $this->TrnMoveHeader->query($sql , false , false );
            }else{
                $result = $this->TrnMoveHeader->query($sql);
            }
            $this->request->data = $data;
        }else{
            //日付の補完は初回のみ
            $this->request->data['Search']['work_date_from']   = date('Y/m/d', strtotime('-7 day'));
            $this->request->data['Search']['work_date_to']     = date('Y/m/d');
        }

        $this->set( 'result' , $result );

        $this->set('move_types',Configure::read('MoveTypes'));
        $this->set('teisu' , Configure::read('TeisuType.List'));

        $this->set('work_classes',$this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_type));
    }

    function search_history () {
        $this->redirect('history');
    }

    /**
     * 移動履歴明細
     */
    function view_history () {
        /*  移動明細取得  */
        $where = $this->_getHistoryDetailWhere(true);
        $sql = $this->_getHistoryDetailSQL($where);

        if(isset($this->request->data['Search']['center_move'])){ //暫定処理
            $result = $this->TrnShipping->query($sql , false , false);
        }else{
            $result = $this->TrnShipping->query($sql);
        }

        $this->set('move_types',Configure::read('MoveTypes'));
        $this->set('result',$result);

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnMoveHeader.token' , $token);
        $this->request->data['TrnMoveHeader']['token'] = $token;

    }

    /**
     * 移動履歴削除
     */
    function delete_history () {
        if($this->request->data['TrnMoveHeader']['token'] === $this->Session->read('TrnMoveHeader.token')) {
            $this->Session->delete('TrnMoveHeader.token');

            $now = date('Y/m/d H:i:s.u');

            //トランザクション開始
            $this->TrnShipping->begin();

            //行ロック
            $this->TrnSticker->query('select * from trn_shippings as a where a.id in (' . join(',',$this->request->data['TrnShipping']['id'] ) . ') for update ');

            //更新チェック
            $ret = $this->TrnSticker->query('select count(*) from trn_shippings as a where a.id in (' . join(',',$this->request->data['TrnShipping']['id'] ) . ") and a.modified > '" . $this->request->data['TrnMoveHeader']['time'] . "'");
            if($ret[0][0]['count'] > 0){
                $this->TrnShipping->rollback();
                $this->Session->setFlash('ほかユーザによって更新されています。最初からやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('history');
            }

            //結果表示用データ取得
            $sql  = 'SELECT TrnMoveHeader.id               AS "TrnMoveHeader__id"';
            $sql .= '      ,TrnMoveHeader.work_no          AS "TrnMoveHeader__work_no"';
            $sql .= '      ,TrnMoveHeader.work_type        AS "TrnMoveHeader__work_type"';
            $sql .= '      ,TrnMoveHeader.move_type        AS "TrnMoveHeader__move_type"';
            $sql .= '      ,TrnMoveHeader.recital          AS "TrnMoveHeader__recital"';
            $sql .= '      ,TrnShipping.id                 AS "TrnShipping__id"';
            $sql .= '      ,TrnShipping.work_no            AS "TrnShipping__work_no"';
            $sql .= '      ,TrnShipping.work_type          AS "TrnShipping__work_type"';
            $sql .= '      ,TrnShipping.quantity           AS "TrnShipping__quantity"';
            $sql .= '      ,TrnShipping.recital            AS "TrnShipping__recital"';
            $sql .= '      ,TrnShipping.move_status        AS "TrnShipping__move_status"';
            $sql .= '      ,TrnSticker.id                  AS "TrnSticker__id"';
            $sql .= '      ,TrnSticker.quantity            AS "TrnSticker__quantity"';
            $sql .= '      ,TrnSticker.lot_no              AS "TrnSticker__lot_no"';
            $sql .= '      ,TrnSticker.validated_date      AS "TrnSticker__validated_date"';
            $sql .= '      ,TrnSticker.facility_sticker_no AS "TrnSticker__facility_sticker_no"';
            $sql .= '      ,TrnSticker.transaction_price   AS "TrnSticker__transaction_price"';
            $sql .= '      ,FromFacility.facility_name     AS "FromFacility__facility_name"';
            $sql .= '      ,FromDepartment.department_name AS "FromDepartment__department_name"';
            $sql .= '      ,ToFacility.facility_name       AS "ToFacility__facility_name"';
            $sql .= '      ,ToDepartment.department_name   AS "ToDepartment__department_name"';
            $sql .= '      ,MstFacilityItem.internal_code  AS "MstFacilityItem__internal_code"';
            $sql .= '      ,MstFacilityItem.item_name      AS "MstFacilityItem__item_name"';
            $sql .= '      ,MstFacilityItem.standard       AS "MstFacilityItem__standard"';
            $sql .= '      ,MstFacilityItem.item_code      AS "MstFacilityItem__item_code"';
            $sql .= '      ,MstFacilityItem.unit_price     AS "MstFacilityItem__unit_price"';
            $sql .= '      ,MstDealer.dealer_name          AS "MstDealer__dealer_name"';
            $sql .= '      ,MstFacilityItem.per_unit       AS "MstFacilityItem__per_unit"';
            $sql .= '      ,( case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name ';
            $sql .= "         else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || PerUnitName.unit_name  || ')' ";
            $sql .= '         end ) as "MstFacilityItem__unit_name"';
            $sql .= '      ,MstItemUnit.per_unit           AS "MstItemUnit__per_unit"';
            $sql .= '      ,MstUnitName.unit_name          AS "MstUnitName__unit_name"';
            $sql .= '      ,PerUnitName.unit_name          AS "PerUnitName__unit_name"';
            $sql .= '      ,MstReceiving.id                AS "MstReceiving__id"';
            $sql .= '      ,MstReceiving.work_no           AS "MstReceiving__work_no"';
            $sql .= '      ,TrnStock.id                    as "TrnStock__id"';
            $sql .= ' FROM  trn_shippings AS TrnShipping';
            $sql .= ' INNER JOIN trn_move_headers AS TrnMoveHeader ON TrnShipping.trn_move_header_id = TrnMoveHeader.id';
            $sql .= ' INNER JOIN trn_stickers AS TrnSticker ON TrnShipping.trn_sticker_id = TrnSticker.id';
            $sql .= ' INNER JOIN mst_item_units AS MstItemUnit ON TrnShipping.mst_item_unit_id = MstItemUnit.id';
            $sql .= ' INNER JOIN mst_facility_items AS MstFacilityItem ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
            $sql .= ' LEFT  JOIN mst_unit_names MstUnitName ON MstItemUnit.mst_unit_name_id = MstUnitName.id';
            $sql .= ' LEFT  JOIN mst_unit_names PerUnitName ON MstItemUnit.per_unit_name_id = PerUnitName.id';
            $sql .= ' LEFT  JOIN mst_dealers MstDealer ON MstFacilityItem.mst_dealer_id = MstDealer.id';
            $sql .= ' INNER JOIN mst_departments AS FromDepartment ON TrnMoveHeader.department_id_from = FromDepartment.id';
            $sql .= ' INNER JOIN mst_facilities AS FromFacility ON FromDepartment.mst_facility_id = FromFacility.id';
            $sql .= ' INNER JOIN mst_departments AS ToDepartment ON TrnMoveHeader.department_id_to = ToDepartment.id';
            $sql .= ' INNER JOIN mst_facilities AS ToFacility ON ToDepartment.mst_facility_id = ToFacility.id';
            $sql .= ' LEFT  JOIN trn_receivings MstReceiving ON TrnSticker.id = MstReceiving.trn_sticker_id and MstReceiving.receiving_type = '.Configure::read('ReceivingType.arrival');
            $sql .= ' left join trn_stocks as TrnStock on TrnStock.mst_item_unit_id = TrnShipping.mst_item_unit_id and TrnStock.mst_department_id = TrnShipping.department_id_from ';
            $sql .= " WHERE TrnShipping.is_deleted = false ";
            $sql .= " AND TrnShipping.id IN (".join(',',$this->request->data['TrnShipping']['id']).")";
            $sql .= " AND TrnMoveHeader.move_type IN (".Configure::read('MoveType.move2outer').",".Configure::read('MoveType.move2other').")";
            $sql .= " AND TrnSticker.trn_move_header_id = TrnMoveHeader.id ";

            $this->TrnShipping->create();
            $result = $this->TrnShipping->query($sql);

            //取消処理
            foreach( $result as $r ){
                /*  取消処理  */
                $this->TrnShipping->create();
                $ret = $this->TrnShipping->updateAll(
                    array(
                        'TrnShipping.is_deleted' => "'true'",
                        'TrnShipping.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'TrnShipping.modified'   => "'" . $now . "'"
                        ),
                    array(
                        'TrnShipping.id' => $r['TrnShipping']['id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnShipping->rollback();
                    $this->Session->setFlash('移動取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('history');
                }

                //移動元の在庫予約数をマイナス
                $this->TrnStock->create();
                $ret = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.reserve_count' => 'TrnStock.reserve_count - ' . $r['TrnShipping']['quantity'] ,
                        'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'      => "'" . $now . "'"
                        ),
                    array(
                        'TrnStock.id' => $r['TrnStock']['id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnShipping->rollback();
                    $this->Session->setFlash('移動取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('history');
                }

                $this->TrnSticker->create();
                $ret = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.has_reserved'       => 'false' ,
                        'TrnSticker.trn_move_header_id' => null ,
                        'TrnSticker.modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'           => "'" . $now . "'"
                        ),
                    array(
                        'TrnSticker.id' => $r['TrnSticker']['id'],
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnShipping->rollback();
                    $this->Session->setFlash('移動取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('history');
                }
            }


            //コミット前に更新チェック
            $ret = $this->TrnSticker->query('select count(*) from trn_shippings as a where a.id in (' . join(',',$this->request->data['TrnShipping']['id'] ) . ") and a.modified > '" . $this->request->data['TrnMoveHeader']['time'] . "' and a.modified <> '" .$now. "'");
            if($ret[0][0]['count'] > 0){
                $this->TrnShipping->rollback();
                $this->Session->setFlash('ほかユーザによって更新されています。最初からやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('history');
            }
            
            $this->TrnShipping->commit();
            $this->Session->setFlash('移動明細を削除しました', 'growl', array('type'=>'star') );

            $this->set('move_types', Configure::read('MoveTypes'));
            $this->set('result',$result);

            $this->Session->write('TrnMoveHeader.result' , $result);
        }else{
            $this->set('result' , $this->Session->read('TrnMoveHeader.result'));
            $this->set('move_types', Configure::read('MoveTypes'));
        }
        $this->render('delete_history_view');
        $this->__getWorkClasses();
    }

    /**
     * 作業区分を取得する
     */
    private function __getWorkClasses() {
        $work_classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_type);
        $this->set('work_classes',$work_classes);
    }

    /**
     * 移動受領ロジック
     * Enter description here ...
     * @param $receive_data 移動データ（TrnMoveHeaderクラスと同じ構造)
     */
    protected function __receiveLogic() {
        //行ロック
        //分割親テーブルに対して行ロックが掛からないので処理を分岐
        if( Configure::read('SplitTable.flag')==0 || ( isset($this->request->data['Search']['center_move']) && $this->request->data['Search']['center_move'] == '' ) ){
            $this->TrnMoveHeader->query(' select * from trn_shippings as a where a.id in (' . join(',',$this->request->data['TrnMove']['id']) . ') for update ');
        }else{
            //施設一覧を取得
            $f_list = $this->TrnMoveHeader->query(' select a.center_facility_id as id from trn_shippings as a where a.id in (' . join(',',$this->request->data['TrnMove']['id']) . ') group by a.center_facility_id ' , false , false);
            foreach($f_list as $f ){
                $this->TrnMoveHeader->query(' select * from trn_shippings_' .$f[0]['id']. ' as a where a.id in (' . join(',',$this->request->data['TrnMove']['id']) . ') for update ' , false ,false );
            }
        }

        /* 更新時間チェック */
        foreach($this->request->data['TrnMove']['id'] as $id){
            $sql = 'select count(*) as count from trn_shippings as a where a.id = ' . $id . " and a.modified = '" . $this->request->data['TrnMove']['modified'][$id] . "'";
            if( Configure::read('SplitTable.flag')==0 || ( isset($this->request->data['Search']['center_move']) && $this->request->data['Search']['center_move'] == '') ){
                $check = $this->TrnMoveHeader->query($sql);
            }else{
                $check = $this->TrnMoveHeader->query($sql , false , false );
            }
            if($check[0][0]['count'] == 0){
                return false;
            }
        }

        $move_header_ids = array();

        $sticker_record_move_seq = 1;

        foreach( $this->request->data['TrnMove']['id'] as $id ) {
            $sql  =' select ';
            $sql .='       e.is_lowlevel               as "MstFacilityItem__is_lowlevel" ';
            $sql .='     , e.id                        as "MstFacilityItem__id" ';
            $sql .='     , b.id                        as "TrnMoveHeader__id" ';
            $sql .='     , b.move_type                 as "TrnMoveHeader__move_type" ';
            $sql .='     , a.id                        as "TrnShipping__id" ';
            $sql .='     , a.trn_move_header_id        as "TrnShipping__trn_move_header_id" ';
            $sql .='     , a.trn_sticker_id            as "TrnShipping__trn_sticker_id" ';
            $sql .='     , a.quantity                  as "TrnShipping__quantity" ';
            $sql .='     , a.department_id_from        as "TrnShipping__department_id_from" ';
            $sql .='     , f.id                        as "TrnShipping__facility_id_from" ';
            $sql .='     , a.department_id_to          as "TrnShipping__department_id_to" ';
            $sql .='     , a.work_no                   as "TrnShipping__work_no" ';
            $sql .='     , a.work_type                 as "TrnShipping__work_type" ';
            $sql .='     , c.id                        as "TrnSticker__department_id_to" ';
            $sql .='     , c.mst_item_unit_id          as "TrnSticker__mst_item_unit_id" ';
            $sql .='     , c.facility_sticker_no       as "TrnSticker__facility_sticker_no" ';
            $sql .='     , c.hospital_sticker_no       as "TrnSticker__hospital_sticker_no" ';
            $sql .='     , c.trn_order_id              as "TrnSticker__trn_order_id" ';
            $sql .='     , c.transaction_price         as "TrnSticker__transaction_price" ';
            $sql .='     , c.sales_price               as "TrnSticker__sales_price"  ';
            $sql .='     , g.id                        as "TrnStock__id_from" ';
            $sql .='     , h.id                        as "TrnStock__id_to" ';
            $sql .='     , i.id                        as "TrnSticker__stock_id"  ';

            $sql .='   from ';
            $sql .='     trn_shippings as a  ';
            $sql .='     inner join trn_move_headers as b  ';
            $sql .='       on a.trn_move_header_id = b.id  ';
            $sql .='     inner join trn_stickers as c  ';
            $sql .='       on a.trn_sticker_id = c.id  ';
            $sql .='     inner join mst_item_units as d  ';
            $sql .='       on c.mst_item_unit_id = d.id  ';
            $sql .='     inner join mst_facility_items as e  ';
            $sql .='       on d.mst_facility_item_id = e.id  ';
            $sql .='     left join mst_departments as f  ';
            $sql .='       on f.id = a.department_id_from  ';

            $sql .='     left join (  ';
            $sql .='       select ';
            $sql .='             z1.master_id ';
            $sql .='           , z2.per_unit ';
            $sql .='           , z2.id  ';
            $sql .='         from ';
            $sql .='           mst_facility_items as z1  ';
            $sql .='           left join mst_item_units as z2  ';
            $sql .='             on z2.mst_facility_item_id = z1.id  ';
            $sql .='         where ';
            $sql .='           z1.item_type = ' . Configure::read('Items.item_types.normalitem');
            $sql .='           and z1.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .="           and z1.master_id <> ''";
            $sql .='     ) as z  ';
            $sql .='       on z.master_id = e.master_id ';
            $sql .='       and d.per_unit = z.per_unit ';

            $sql .='     left join trn_stocks as g  ';
            $sql .='       on g.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .='       and g.mst_department_id = a.department_id_from  ';
            $sql .='     left join trn_stocks as h  ';
            $sql .='       on h.mst_department_id = a.department_id_to  ';
            $sql .='       and h.mst_item_unit_id = ' . $this->request->data['TrnMove']['mst_item_unit_id'][$id];
            $sql .='     left join trn_stickers as i  ';
            $sql .="       on i.lot_no = ' '  ";
            $sql .='       and i.mst_department_id = a.department_id_to  ';
            $sql .='       and i.mst_item_unit_id = ' . $this->request->data['TrnMove']['mst_item_unit_id'][$id];
            $sql .='       and i.is_deleted = false ';

            $sql .='   where ';
            $sql .='     a.id = ' . $id;
            if(isset($this->request->data['Search']['center_move']) && $this->request->data['Search']['center_move'] == 1){
                $ret = $this->TrnShipping->query($sql , false , false);
            }else{
                $ret = $this->TrnShipping->query($sql);
            }
            $r = $ret[0];
            $move_header_id = $r['TrnShipping']['trn_move_header_id'];
            $_move_type = $r['TrnMoveHeader']['move_type'];

            // 【STEP0】　締めチェック
            $_res = 0;
            //移動先に対しての締めチェック
            if(!$this->canUpdateAfterClose($this->request->data['MoveHeader']['work_date'],$this->Session->read('Auth.facility_id_selected'))){
                $_res++;
            }

            //移動元に対して締めチェックを行うかは要確認
            //移動元に対しての締めチェック
            //(出荷明細の移動元部署IDから施設IDを取得)
            if(!$this->canUpdateAfterClose($this->request->data['MoveHeader']['work_date'],$r['TrnShipping']['facility_id_from'])){
                $_res++;
            }

            if($_res != 0){
                $this->Session->setFlash('指定した受領日にすでに締めが行われています', 'growl', array('type'=>'error'));
                //締め済みなので、以降の処理は行わず戻る
                $this->TrnMoveHeader->rollback();
                $this->redirect('search_item_moved');
            }

            // 【STEP1】　入荷データ作成
            $work_seq = 1;

            $receiving_data = array(
                'TrnReceiving' => array(
                    'work_no'            => $r['TrnShipping']['work_no'],
                    'work_seq'           => $work_seq,
                    'work_date'          => $this->request->data['MoveHeader']['work_date'],
                    'work_type'          => $r['TrnShipping']['work_type'],
                    'recital'            => $this->request->data['MoveHeader']['recital'],
                    'mst_item_unit_id'   => $this->request->data['TrnMove']['mst_item_unit_id'][$id], //包装単位参照キー
                    'department_id_from' => $r['TrnShipping']['department_id_from'],
                    'department_id_to'   => $r['TrnShipping']['department_id_to'],
                    'quantity'           => $r['TrnShipping']['quantity'],                            //数量
                    'stocking_price'     => $r['TrnSticker']['transaction_price'],                    //仕入れ価格
                    'sales_price'        => $r['TrnSticker']['sales_price'],                          //売上価格
                    'trn_sticker_id'     => $r['TrnShipping']['trn_sticker_id'],                      //シール参照キー
                    'trn_shipping_id'    => $r['TrnShipping']['id'],                                  //出荷参照キー
                    'trn_order_id'       => $r['TrnSticker']['trn_order_id'],                         //発注参照キー
                    'remain_count'       => $r['TrnShipping']['quantity'],                            //残数
                    'receiving_type'     => Configure::read('ReceivingType.arrival'),                 //入荷済み
                    'trn_move_header_id' => $move_header_id,                                          //移動ヘッダ外部キー
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'created'            => $this->request->data['now'],
                    'modified'           => $this->request->data['now'],
                    'facility_sticker_no'=> $r['TrnSticker']['facility_sticker_no'],
                    'hospital_sticker_no'=> $r['TrnSticker']['hospital_sticker_no']
                    )
                );

            //施設外移動
            if($r['TrnMoveHeader']['move_type'] == Configure::read('MoveType.move2outer')){
                $receiving_data['TrnReceiving']['receiving_type']   = Configure::read('ReceivingType.move');    //入荷区分：移動
                $receiving_data['TrnReceiving']['receiving_status'] = Configure::read('ReceivingStatus.stock'); //入荷状態：入庫済
                //センター間移動
            } elseif($r['TrnMoveHeader']['move_type'] == Configure::read('MoveType.move2other')){
                $receiving_data['TrnReceiving']['receiving_type']   = Configure::read('ReceivingType.arrival');   //入荷区分：入荷
                $receiving_data['TrnReceiving']['receiving_status'] = Configure::read('ReceivingStatus.arrival'); //入荷状態：入荷済
            }

            //TrnReceivingオブジェクトをcreate
            $this->TrnReceiving->create();

            if (!$this->TrnReceiving->save($receiving_data)) {
                return false;
            }
            $last_receiving_id = $this->TrnReceiving->getLastInsertID();

            // 【STEP2-1】　移動元在庫更新：在庫数マイナス・予約数マイナス
            /* テーブル分割によって他センターのデータを更新するとエラーになるので、ベタ書きに変更 */
            $sql  = ' update trn_stocks ';
            $sql .= '    set stock_count = stock_count - ' . $r['TrnShipping']['quantity'];
            $sql .= '      , reserve_count = reserve_count - ' . $r['TrnShipping']['quantity'];
            $sql .= '      , modifier = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= "      , modified = '" . $this->request->data['now'] . "'";
            $sql .= '    where id = ' .  $r['TrnStock']['id_from'];

            if(isset($this->request->data['Search']['center_move']) && $this->request->data['Search']['center_move'] == 1){
                $this->TrnStock->query($sql , false , false );
            }else{
                $this->TrnStock->query($sql);
            }

            // 【STEP2-2】　移動先在庫更新：在庫数プラス
            //在庫が取得できなかった場合は作成する
            if(is_null($r['TrnStock']['id_to'])){
                //在庫レコードが無い場合はレコードを作成する
                $data = array('mst_item_unit_id'  => $this->request->data['TrnMove']['mst_item_unit_id'][$id],
                              'mst_department_id' => $r['TrnShipping']['department_id_to'],
                              'is_deleted'        => false,
                              'creater'           => $this->Session->read('Auth.MstUser.id'),
                              'modifier'          => $this->Session->read('Auth.MstUser.id'),
                              'created'           => $this->request->data['now'],
                              'modified'          => $this->request->data['now']
                              );

                //施設外移動
                if($_move_type == Configure::read('MoveType.move2outer')){
                    $data['TrnStock']['stock_count'] = $r['TrnShipping']['quantity'];//在庫数プラス
                    //センター間移動
                } elseif($_move_type == Configure::read('MoveType.move2other')){
                    $data['TrnStock']['receipted_count'] = $r['TrnShipping']['quantity'];//未入庫数プラス
                }
                //TrnStockオブジェクトをcreate
                $this->TrnStock->create();

                //保存
                if (false === $this->TrnStock->save($data)) {
                    return false;
                }
                //在庫が取得できた場合は更新する
            } else {
                $condtion = array( 'TrnStock.id ='=> $r['TrnStock']['id_to'] );
                $updatefield = array( 'TrnStock.modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                      'TrnStock.modified'        => "'".$this->request->data['now']."'",
                                      'TrnStock.stock_count'     => 'TrnStock.stock_count + ' . (($_move_type == Configure::read('MoveType.move2outer'))?$r['TrnShipping']['quantity']:0) ,
                                      'TrnStock.receipted_count' => 'TrnStock.receipted_count + ' . (($_move_type == Configure::read('MoveType.move2other'))?$r['TrnShipping']['quantity']:0),
                                      );
                $this->TrnStock->recursive = -1;
                if (!$this->TrnStock->updateAll($updatefield,$condtion)) {
                    return false;
                }
            }

            // 【STEP3】　シール移動履歴作成
            $sticker_record = array(
                'TrnStickerRecord'    => array(
                    'move_date'           => $this->request->data['MoveHeader']['work_date'], //移動日時
                    'move_seq'            => $this->getNextRecord($r['TrnShipping']['trn_sticker_id']), //移動連番
                    'record_type'         => Configure::read('RecordType.move'),              //シール移動履歴＠レコード区分「移動」
                    'record_id'           => $last_receiving_id,                              //レコードキー(入荷明細)
                    'trn_sticker_id'      => $r['TrnShipping']['trn_sticker_id'],             //シール参照キー
                    'facility_sticker_no' => $r['TrnSticker']['facility_sticker_no'],         //部署シール番号
                    'hospital_sticker_no' => $r['TrnSticker']['hospital_sticker_no'],         //センターシール番号
                    'mst_department_id'   => $r['TrnShipping']['department_id_to'],           //移動先
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $this->request->data['now'],
                    'modified'            => $this->request->data['now'],
                    'is_deleted'          => FALSE,
                    )
                );


            $this->TrnStickerRecord->create();

            if (!$this->TrnStickerRecord->save($sticker_record)) {
                return false;
            }

            // 【STEP4】　シール更新
            /* テーブル分割によって他センターのデータを更新するとエラーになるので、ベタ書きに変更 */
            $sql  = ' update trn_stickers ';
            $sql .= '    set mst_item_unit_id    = ' . $this->request->data['TrnMove']['mst_item_unit_id'][$id] ;//包装単位
            $sql .= '      , quantity            = ' . $r['TrnShipping']['quantity'];//数量
            $sql .= '      , last_move_date      = ' . "'".$this->request->data['MoveHeader']['work_date']."'";//最終移動日
            $sql .= '      , has_reserved        = false ' ;//予約フラグ
            $sql .= "      , hospital_sticker_no = '' ";//部署シールを空にする
            if($r['TrnSticker']['facility_sticker_no'] == ''){
                $facility_sticker_no = $this->getCenterStickerNo($this->request->data['MoveHeader']['work_date'],
                                                                 $this->getFacilityCode($this->Session->read('Auth.facility_id_selected')));
                $sql .= "      , facility_sticker_no = '" . $facility_sticker_no . "' ";//センターシールが空の場合、発版する
            }
            $sql .= '      , mst_department_id   = ' .$r['TrnShipping']['department_id_to'] ;//部署参照キー
            $sql .= '      , trade_type          = ' . Configure::read('ClassesType.Constant');
            $sql .= '      , is_deleted          = ' . (($_move_type == Configure::read('MoveType.move2other'))?'true':'false') ;//センター間の場合、削除フラグを立てる
            $sql .= '      , modifier            = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= "      , modified            = '" . $this->request->data['now'] . "'";
            $sql .= '    where id = ' .  $r['TrnShipping']['trn_sticker_id'];

            if(isset($this->request->data['Search']['center_move']) && $this->request->data['Search']['center_move'] == 1){
                $this->TrnSticker->query($sql , false , false );
            }else{
                $this->TrnSticker->query($sql);
            }

            // センター間移動のときは、入荷済み未入庫を増やす
            if($_move_type == Configure::read('MoveType.move2other')){
                if(is_null($r['TrnSticker']['stock_id'])){
                    //入荷済み未入庫レコードが無い場合
                    $sticker = array(
                        'TrnSticker'    => array(
                            'facility_sticker_no' => '',                                                       //部署シール番号
                            'hospital_sticker_no' => '',                                                       //センターシール番号
                            'lot_no'              => ' ',                                                      //ロット番号
                            'quantity'            => $r['TrnShipping']['quantity'],                            //数量
                            'mst_department_id'   => $r['TrnShipping']['department_id_to'],                    //移動先
                            'mst_item_unit_id'    => $this->request->data['TrnMove']['mst_item_unit_id'][$id], //包装単位
                            'trade_type'          => Configure::read('ClassesType.Constant'),
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $this->request->data['now'],
                            'modified'            => $this->request->data['now'],
                            'is_deleted'          => FALSE,
                            'transaction_price'   => 0,                                                        // 単価特定が出来ないので仮に0埋め
                            )
                        );
                    $this->TrnSticker->create();
                    if (!$this->TrnSticker->save($sticker)) {
                        return false;
                    }
                }else{
                    //入荷済み未入庫のレコードがある場合
                    $condtion = array( 'TrnSticker.id ='=> $r['TrnSticker']['stock_id'] );
                    $updatefield = array('TrnSticker.quantity' => 'TrnSticker.quantity + '.$r['TrnShipping']['quantity'] ,//数量
                                         'TrnSticker.modifier' => "'".$this->Session->read('Auth.MstUser.id')."'",
                                         'TrnSticker.modified' => "'".$this->request->data['now']."'",
                                         );
                    $this->TrnSticker->recursive = -1;
                    if (!$this->TrnSticker->updateAll($updatefield,$condtion)) {
                        return false;
                    }
                }
            }

            // 【STEP5】　出荷明細ステータス更新
            /* テーブル分割によって他センターのデータを更新するとエラーになるので、ベタ書きに変更 */
            $sql  = ' update trn_shippings ';
            $sql .= '    set shipping_status = ' . Configure::read('ShippingStatus.loaded');
            $sql .= '      , move_status     = ' . Configure::read('MoveStatus.complete');
            $sql .= '      , modifier        = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= "      , modified        = '" . $this->request->data['now'] . "'";
            $sql .= '    where id = ' .  $id;

            if(isset($this->request->data['Search']['center_move']) && $this->request->data['Search']['center_move'] == 1){
                $this->TrnShipping->query($sql , false , false );
            }else{
                $this->TrnShipping->query($sql);
            }
        }
        return true;
    }

    private function __getSelectSql() {
        $_sql = ' select ';
        $_sql .= '      TrnSticker.id                       as "TrnSticker__id", ';
        $_sql .= '      TrnSticker.facility_sticker_no      as "TrnSticker__facility_sticker_no", ';
        $_sql .= '      TrnSticker.hospital_sticker_no      as "TrnSticker__hospital_sticker_no", ';
        $_sql .= '      TrnSticker.mst_item_unit_id         as "TrnSticker__mst_item_unit_id", ';
        $_sql .= '      TrnSticker.mst_department_id        as "TrnSticker__mst_department_id", ';
        $_sql .= '      TrnSticker.trade_type               as "TrnSticker__trade_type", ';
        $_sql .= '      TrnSticker.quantity                 as "TrnSticker__quantity", ';
        $_sql .= '      TrnSticker.lot_no                   as "TrnSticker__lot_no", ';
        $_sql .= "      to_char(TrnSticker.validated_date ,'YYYY/mm/dd') ";
        $_sql .= '                                          as "TrnSticker__validated_date", ';
        $_sql .= '      TrnSticker.original_price           as "TrnSticker__original_price", ';
        $_sql .= '      TrnSticker.transaction_price        as "TrnSticker__transaction_price", ';
        $_sql .= '      TrnSticker.last_move_date           as "TrnSticker__last_move_date", ';
        $_sql .= '      TrnSticker.trn_set_stock_id         as "TrnSticker__trn_set_stock_id", ';
        $_sql .= '      TrnSticker.trn_consume_id           as "TrnSticker__trn_consume_id", ';
        $_sql .= '      TrnSticker.trn_promise_id           as "TrnSticker__trn_promise_id", ';
        $_sql .= '      TrnSticker.trn_order_id             as "TrnSticker__trn_order_id", ';
        $_sql .= '      TrnSticker.trn_receiving_id         as "TrnSticker__trn_receiving_id", ';
        $_sql .= '      TrnSticker.trn_storage_id           as "TrnSticker__trn_storage_id", ';
        $_sql .= '      TrnSticker.trn_shipping_id          as "TrnSticker__trn_shipping_id", ';
        $_sql .= '      TrnSticker.receipt_id               as "TrnSticker__receipt_id", ';
        $_sql .= '      TrnSticker.buy_claim_id             as "TrnSticker__buy_claim_id", ';
        $_sql .= '      TrnSticker.sale_claim_id            as "TrnSticker__sale_claim_id", ';
        $_sql .= '      TrnSticker.sales_price              as "TrnSticker__sales_price", ';
        $_sql .= '      TrnSticker.has_reserved             as "TrnSticker__has_reserved", ';
        $_sql .= '      TrnSticker.trn_return_header_id     as "TrnSticker__trn_return_header_id", ';
        $_sql .= '      TrnSticker.trn_move_header_id       as "TrnSticker__trn_move_header_id", ';
        $_sql .= '      TrnSticker.trn_fixed_promise_id     as "TrnSticker__trn_fixed_promise_id", ';
        $_sql .= '      TrnSticker.trn_return_receiving_id  as "TrnSticker__trn_return_receiving_id", ';
        $_sql .= '      TrnSticker.trn_adjust_id            as "TrnSticker__trn_adjust_id", ';
        $_sql .= '      TrnSticker.modified                 as "TrnSticker__modified", ';
        $_sql .= '      MstFacilityItem.id                  as "MstFacilityItem__id", ';
        $_sql .= '      MstFacilityItem.internal_code       as "MstFacilityItem__internal_code", ';
        $_sql .= '      MstFacilityItem.item_name           as "MstFacilityItem__item_name" , ';
        $_sql .= '      MstFacilityItem.item_code           as "MstFacilityItem__item_code" , ';
        $_sql .= '      MstFacilityItem.standard            as "MstFacilityItem__standard" , ';
        $_sql .= '      MstFacilityItem.is_lowlevel         as "MstFacilityItem__is_lowlevel", ';
        $_sql .= '      MstItemUnit.per_unit                as "MstItemUnit__per_unit", ';
        $_sql .= '      MstUnitName.unit_name               as "MstUnitName__unit_name", ';
        $_sql .= '      MstPerUnitName.unit_name            as "MstPerUnitName__unit_name", ';

        $_sql .= '      (  ';
        $_sql .= '       case  ';
        $_sql .= '         when MstItemUnit.per_unit = 1  ';
        $_sql .= '         then MstUnitName.unit_name  ';
        $_sql .= "         else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')'  ";
        $_sql .= '         end ';
        $_sql .= '      )                                             as "TrnSticker__unit_name" ,';

        $_sql .= '      MstDealer.dealer_name               as "MstDealer__dealer_name", ';
        $_sql .= '      MstDepartment.id                    as "MstDepartment__id", ';
        $_sql .= '      MstDepartment.department_name       as "MstDepartment__department_name", ';
        $_sql .= '      MstDepartment.department_code       as "MstDepartment__department_code", ';
        $_sql .= '      MstFacility.id                      as "MstFacility__id", ';
        $_sql .= '      MstFacility.facility_name           as "MstFacility__facility_name", ';
        $_sql .= '      MstFacility.facility_code           as "MstFacility__facility_code", ';
        $_sql .= '      COALESCE(TrnReceiving.stocking_price,';
        $_sql .= '               TrnSticker.original_price) as "TrnReceiving__stocking_price", ';
        $_sql .= '      TrnAppraisedValue.price             as "TrnAppraisedValue__price", ';
        $_sql .= '      MstFixedCount.substitute_unit_id    as "MstFixedCount__substitute_unit_id", ';
        $_sql .= '      MstDepartmentFrom.department_name   as "MstDepartmentFrom__department_name"';
        $_sql .= '      from ';
        $_sql .= '      trn_stickers as TrnSticker ';
        $_sql .= '      left join mst_item_units as MstItemUnit on TrnSticker.mst_item_unit_id = MstItemUnit.id ';
        $_sql .= '      left join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id ';
        $_sql .= '      left join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id ';
        $_sql .= '      left join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ';
        $_sql .= '      left join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id ';
        $_sql .= '      left join mst_departments as MstDepartment on TrnSticker.mst_department_id = MstDepartment.id ';
        $_sql .= '      left join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id ';
        $_sql .= '      left join trn_stocks as TrnStock ';
        $_sql .= '          on TrnSticker.mst_item_unit_id = TrnStock.mst_item_unit_id ';
        $_sql .= '          and TrnSticker.mst_department_id = TrnStock.mst_department_id ';
        $_sql .= '          and TrnStock.stock_count > 0 ';
        $_sql .= '      left join trn_receivings as TrnReceiving ';
        $_sql .= '          on TrnReceiving.id = TrnSticker.trn_receiving_id ';
        $_sql .= '          and TrnReceiving.is_deleted = false ';
        $_sql .= '      left join ( ';
        $_sql .= '      select ';
        $_sql .= '            mst_facility_item_id ';
        $_sql .= '          , price  ';
        $_sql .= '        from ';
        $_sql .= '          trn_appraised_values  ';
        $_sql .= '        where ';
        $_sql .= '          id in (  ';
        $_sql .= '            select ';
        $_sql .= '                  max(a.id)          as id  ';
        $_sql .= '              from ';
        $_sql .= '                trn_appraised_values as a  ';
        $_sql .= '              where ';
        $_sql .= '                a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $_sql .= '                and a.is_deleted = false  ';
        $_sql .= '              group by ';
        $_sql .= '                a.mst_facility_item_id ';
        $_sql .= '      )) as TrnAppraisedValue ';
        $_sql .= '       on TrnAppraisedValue.mst_facility_item_id = MstFacilityItem.id ';
        $_sql .= '      left join mst_fixed_counts as MstFixedCount on MstFixedCount.mst_item_unit_id = MstItemUnit.id and MstFixedCount.mst_department_id = MstDepartment.id and MstFixedCount.is_deleted = false ';
        $_sql .= '      left join mst_departments as MstDepartmentFrom on TrnReceiving.department_id_from = MstDepartmentFrom.id  ';

        return $_sql;

    }

    /**
     * センター間移動＠対象データ取得
     * @param array $cond
     * @access private
     * @return array
     */
    private function searchMove2Other($where , $limit=null){
        App::import('Sanitize');

        $sql  =' select ' ;
        $sql .=      ' MstFacilityItem.*';
        $sql .=      ',MstItemUnit.per_unit';
        $sql .=      ',MstItemUnit.id as mst_item_unit_id';
        $sql .=      ",(case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')' end) as packing_name" ;
        $sql .=      ',MstDealer.dealer_name';
        $sql .=      ',TrnSticker.id as trn_sticker_id';
        $sql .=      ',TrnSticker.quantity';
        $sql .=      ',TrnSticker.lot_no';
        $sql .=      ",to_char(TrnSticker.validated_date , 'YYYY/mm/dd') as validated_date";
        $sql .=      ',TrnSticker.modified as sticker_modified';
        $sql .=      ',TrnSticker.facility_sticker_no';
        $sql .=      ',TrnSticker.hospital_sticker_no';
        $sql .=      ',TrnSticker.transaction_price';
        $sql .=      ',TrnSticker.mst_department_id';
        $sql .=      ',TrnSticker.has_reserved' ;
        $sql .=      ',TrnAppraisedValue.price';
        $sql .=      ',(case when TrnSticker.has_reserved = true then false else true end ) as check';
        $sql .=  ' from ' ;
        $sql .=      ' trn_stickers as TrnSticker ';
        $sql .=      ' inner join trn_stocks as TrnStock on ' ;
        $sql .=              ' TrnSticker.mst_department_id = TrnStock.mst_department_id ' ;
        $sql .=          ' and TrnSticker.mst_item_unit_id = TrnStock.mst_item_unit_id';
        $sql .=      ' inner join mst_item_units as MstItemUnit on ' ;
        $sql .=          ' TrnSticker.mst_item_unit_id = MstItemUnit.id ' ;
        $sql .=      ' inner join mst_facility_items as MstFacilityItem on ';
        $sql .=          ' MstItemUnit.mst_facility_item_id = MstFacilityItem.id ' ;
        $sql .=      ' inner join mst_unit_names as MstUnitName on ' ;
        $sql .=          ' MstItemUnit.mst_unit_name_id = MstUnitName.id ' ;
        $sql .=      ' inner join mst_unit_names as MstPerUnitName on ';
        $sql .=          ' MstItemUnit.per_unit_name_id = MstPerUnitName.id ';
        $sql .=      ' left join mst_dealers as MstDealer on ' ;
        $sql .=          ' MstFacilityItem.mst_dealer_id = MstDealer.id ' ;
        $sql .=      ' inner join mst_departments as MstDepartment on ';
        $sql .=          ' TrnSticker.mst_department_id = MstDepartment.id ' ;
        $sql .=      ' left join (' ;
        $sql .=          ' select mst_facility_item_id,price' ;
        $sql .=          ' from trn_appraised_values' ;
        $sql .=          ' where trn_close_header_id in (' ;
        $sql .=          ' select max(id)' ;
        $sql .=          ' from trn_close_headers' ;
        $sql .=          ' where mst_facility_id='.$this->Session->read('Auth.facility_id_selected');
        $sql .=          ' and is_deleted=false' ;
        $sql .=          ' and work_date <= current_date' ;
        $sql .=          ' )' ;
        $sql .=      ' ) as TrnAppraisedValue' ;
        $sql .=      ' on TrnAppraisedValue.mst_facility_item_id = MstFacilityItem.id' ;

        //移動先センターで同じマスタID、包装単位入り数があるものだけ表示する
        $sql .=     ' inner join ( ';
        $sql .=       ' select ';
        $sql .=            '  a.master_id ';
        $sql .=            ', b.per_unit  ';
        $sql .=         ' from ';
        $sql .=           ' mst_facility_items as a  ';
        $sql .=           ' left join mst_item_units as b  ';
        $sql .=             ' on b.mst_facility_item_id = a.id  ';
        $sql .=           ' left join mst_facilities as c  ';
        $sql .=             ' on c.id = a.mst_facility_id  ';
        $sql .=         ' where ';
        $sql .=           " c.facility_code = '".$this->request->data['Move2Other']['facility_input']."' ";
        $sql .=         ' group by ';
        $sql .=            '  a.master_id ';
        $sql .=            ', b.per_unit  ';
        $sql .=     ' ) as x  ';
        $sql .=       ' on x.master_id = MstFacilityItem.master_id  ';
        $sql .=       ' and x.per_unit = MstItemUnit.per_unit  ';

        $sql .=  ' where ' ;
        $sql .=          ' 1 = 1' ;
        $sql .=      ' and MstDepartment.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .=      " and TrnSticker.lot_no <> ' ' ";
        $sql .=      ' and TrnSticker.is_deleted = false ' ;
        $sql .=      ' and TrnSticker.quantity > 0 ' ;
        $sql .=      ' and TrnSticker.trn_set_stock_id is null ';
        $sql .=      ' and TrnSticker.trade_type <> ' . Configure::read('ClassesType.Deposit') ;
        $sql .=      $where;
        $sql .=  ' order by ';
        $sql .=      ' MstFacilityItem.internal_code asc,TrnSticker.facility_sticker_no ' ;
        if(!is_null($limit)){
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnSticker' , false )); //暫定
            $sql .=           ' limit ' . $limit;
        }
        return $this->TrnSticker->query($sql , false , false ); //暫定
    }

    /**
     * センターの施設idを指定して倉庫の部署idを取得
     * @param string $fid
     * @return integer
     */
    private function getCenterDepartmentId($fid){
        $res = $this->MstDepartment->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'mst_facility_id' => $fid,
                'department_type' => Configure::read('DepartmentType.warehouse'),
            ),
        ));
        return $res['MstDepartment']['id'];
    }

    /* 移動履歴、移動受領用の施設一覧 */
    private function moveFacilityList($history = false){
        $sql  = ' select ';
        $sql .= '       b.facility_code              as "MstFacility__facility_code" ';
        $sql .= '     , b.facility_name              as "MstFacility__facility_name" ';
        $sql .= '   from ';
        $sql .= '     mst_facilities as a ';
        $sql .= '     left join mst_facilities as b ';
        $sql .= '       on substr(  ';
        $sql .= '         a.subcode ';
        $sql .= '         , 0 ';
        $sql .= "         , strpos(a.subcode, '".Configure::read('subcodeSplitter')."') ";
        $sql .= '       ) = substr( ';
        $sql .= '         b.subcode ';
        $sql .= '         , 0 ';
        $sql .= "         , strpos(b.subcode, '".Configure::read('subcodeSplitter')."') ";
        $sql .= '       ) ';
        $sql .= '       and b.id <> ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= "       and strpos(a.subcode, '".Configure::read('subcodeSplitter')."') > 1 ";
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and b.facility_type = ' . Configure::read('FacilityType.center');
        $sql .= ' union all ';
        $sql .= ' select ';
        $sql .= '       b.facility_code              as "MstFacility__facility_code" ';
        $sql .= '     , b.facility_name              as "MstFacility__facility_name" ';
        $sql .= '   from ';
        $sql .= '     mst_facility_relations as a ';
        $sql .= '     left join mst_facilities as b ';
        $sql .= '       on b.id = a.mst_facility_id ';
        $sql .= '       or b.id = a.partner_facility_id ';
        $sql .= '     inner join mst_user_belongings as c ';
        $sql .= '       on c.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '       and c.mst_facility_id = b.id ';
        $sql .= '       and c.is_deleted = false  ';
        $sql .= '   where ';
        $sql .= '     1 = 1 ';
        $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and b.facility_type in ('.Configure::read('FacilityType.center').', '.Configure::read('FacilityType.hospital').') ';
        if(!$history){
            $sql .= '     and b.id <> ' . $this->Session->read('Auth.facility_id_selected');
        }
        $sql .= '   group by ';
        $sql .= '     b.facility_code ';
        $sql .= '     , b.facility_name ';
        $sql .= '     , b.facility_code ';
        $sql .= '     , b.facility_type ';
        $sql .= '   order by ';
        $sql .= '     "MstFacility__facility_code" ';

        $facility_list = Set::Combine(
            $this->MstFacility->query($sql),
            "{n}.MstFacility.facility_code",
            "{n}.MstFacility.facility_name"
            );
        return $facility_list;
    }

    /**
     * 移動履歴明細where句作成
     */
    private function _getHistoryDetailWhere($isSort) {
        $where = '';
        $where .= " WHERE TrnShipping.is_deleted = false";

        //ヘッダIDが渡されなかった(CSV出力)の場合、メニューで選択されているセンターを条件とする。
        if (empty($this->request->data['TrnMoveHeader']['id'])) {
            $where .= " AND MstItemUnit.mst_facility_id =".$this->Session->read('Auth.facility_id_selected');

        } else {
            $where .= " AND TrnMoveHeader.id IN (".join(',',$this->request->data['TrnMoveHeader']['id']).")";
        }

        // 移動番号
        if(isset($this->request->data['Search']['work_no']) && $this->request->data['Search']['work_no'] != ''){
            $where .= " AND TrnMoveHeader.work_no ILIKE '%".pg_escape_string($this->request->data['Search']['work_no'])."%'";
        }
        // 移動日
        if(isset($this->request->data['Search']['work_date_from']) && $this->request->data['Search']['work_date_from'] != ''){
            $where .= " AND TrnMoveHeader.work_date >= '".pg_escape_string($this->request->data['Search']['work_date_from'])."'";
        }
        if(isset($this->request->data['Search']['work_date_to']) && $this->request->data['Search']['work_date_to'] != ''){
            $where .= " AND TrnMoveHeader.work_date <= '".pg_escape_string($this->request->data['Search']['work_date_to'])."'";
        }
        // 移動元施設
        if(isset($this->request->data['Search']['from_facility']) && $this->request->data['Search']['from_facility'] != ''){
            $where .= " AND FromFacility.facility_code = '".$this->request->data['Search']['from_facility']."'";
        }
        // 移動先施設
        if(isset($this->request->data['Search']['to_facility']) && $this->request->data['Search']['to_facility'] != ''){
            $where .= " AND ToFacility.facility_code = '".$this->request->data['Search']['to_facility']."'";
        }
        // 移動種別
        $_move_types = "";
        if(isset($this->request->data['Search']['move_type']) && $this->request->data['Search']['move_type'] != ''){
            $where .= ' AND TrnMoveHeader.move_type = '.$this->request->data['Search']['move_type'];
        }else{
            $where .= ' AND TrnMoveHeader.move_type in (';
            $where .= Configure::read('MoveType.move2inner') . ',';  // 施設内移動
            $where .= Configure::read('MoveType.move2outer') . ',';  // 施設外移動
            $where .= Configure::read('MoveType.move2other') . ',';  // センター間移動
            $where .= Configure::read('MoveType.move2outside_export') . ',';  // 管理外移出
            $where .= Configure::read('MoveType.move2outside_import') . ')';  // 施設外移入
            
        }
        // 商品ID
        if(isset($this->request->data['Search']['internal_code']) && $this->request->data['Search']['internal_code'] != ''){
            $where .= " AND MstFacilityItem.internal_code LIKE '%".$this->request->data['Search']['internal_code']."%'";
        }
        // 製品番号
        if(isset($this->request->data['Search']['item_code']) && $this->request->data['Search']['item_code'] != ''){
            $where .= " AND MstFacilityItem.item_code LIKE '%".$this->request->data['Search']['item_code']."%'";
        }
        // ロット番号
        if(isset($this->request->data['Search']['lot_no']) && $this->request->data['Search']['lot_no'] != ''){
            $where .= " AND TrnSticker.lot_no LIKE '%".$this->request->data['Search']['lot_no']."%'";
        }
        // 商品名
        if(isset($this->request->data['Search']['item_name']) && $this->request->data['Search']['item_name'] != ''){
            $where .= " AND MstFacilityItem.item_name LIKE '%".$this->request->data['Search']['item_name']."%'";
        }
        // 販売元
        if(isset($this->request->data['Search']['dealer_name']) && $this->request->data['Search']['dealer_name'] != ''){
            $where .= " AND MstDealer.dealer_name LIKE '%".$this->request->data['Search']['dealer_name']."%'";
        }
        // 管理区分
        if(isset($this->request->data['Search']['trade_type']) && $this->request->data['Search']['trade_type'] != ''){
            $where .= " AND TrnSticker.trade_type = ".$this->request->data['Search']['trade_type'];
        }
        // 規格
        if(isset($this->request->data['Search']['standard']) && $this->request->data['Search']['standard'] != ''){
            $where .= " AND MstFacilityItem.standard LIKE '%".$this->request->data['Search']['standard']."%'";
        }
        // シール番号
        if(isset($this->request->data['Search']['sticker_no']) && $this->request->data['Search']['sticker_no'] != ''){
            $where .= " AND (TrnSticker.facility_sticker_no LIKE '%".$this->request->data['Search']['sticker_no']."%'";
            $where .= "  OR  TrnSticker.hospital_sticker_no LIKE '%".$this->request->data['Search']['sticker_no']."%'";
            $where .= "  OR  TrnShipping.facility_sticker_no LIKE '%".$this->request->data['Search']['sticker_no']."%'";
            $where .= "  OR  TrnShipping.hospital_sticker_no LIKE '%".$this->request->data['Search']['sticker_no']."%')";
        }
        // 作業区分
        if(isset($this->request->data['Search']['work_type']) && $this->request->data['Search']['work_type'] != ''){
            $where .= ' AND TrnMoveHeader.work_type = '.$this->request->data['Search']['work_type'];
        }

        if ($isSort) {
            $where .= " ORDER BY TrnShipping.work_no,TrnShipping.work_seq";
        }

        return $where;
    }


    /**
     * 移動履歴明細SQL生成
     */
    private function _getHistoryDetailSQL($where) {
        $sql  = 'SELECT ';
        $sql .= '       TrnMoveHeader.work_no          AS "TrnMoveHeader__work_no"';
        $sql .= '      ,TrnMoveHeader.move_type        AS "TrnMoveHeader__move_type"';

        $sql .= '      ,TrnShipping.id                 AS "TrnShipping__id"';
        $sql .= '      ,TrnShipping.work_type          AS "TrnShipping__work_type"';
        $sql .= '      ,MstClasses.name                AS "TrnShipping__work_type_name"';
        
        $sql .= '      ,TrnShipping.quantity           AS "TrnShipping__quantity"';
        $sql .= '      ,TrnShipping.recital            AS "TrnShipping__recital"';
        $sql .= '      ,( case ';
        $sql .= '          when TrnMoveHeader.move_type = ' . Configure::read('MoveType.move2other') .' ' ;
        $sql .= '          and FromFacility.id <> ' . $this->Session->read('Auth.facility_id_selected') .' ' ;
        $sql .= '          then false ';
        $sql .= '          when TrnShipping.move_status = ' . Configure::read('MoveStatus.reserved') . ' then true ';
        $sql .= '          else false ';
        $sql .= '       end )                          as "TrnShipping__check" ';
        $sql .= '      ,TrnShipping.move_status        AS "TrnShipping__move_status"';
        $sql .= '      ,TrnSticker.lot_no              AS "TrnSticker__lot_no"';
        $sql .= "      ,to_char(TrnSticker.validated_date,'YYYY/mm/dd')";
        $sql .= '                                      AS "TrnSticker__validated_date"';
        $sql .= '      ,COALESCE(TrnShipping.facility_sticker_no, ';
        $sql .= '       TrnSticker.facility_sticker_no) AS "TrnSticker__facility_sticker_no"';
        $sql .= '      ,TrnSticker.transaction_price   AS "TrnSticker__transaction_price"';
        $sql .= '      ,FromFacility.facility_name     AS "FromFacility__facility_name"';
        $sql .= '      ,FromDepartment.department_name AS "FromDepartment__department_name"';
        $sql .= '      ,ToFacility.id                  AS "ToFacility__id"';
        $sql .= '      ,ToFacility.facility_name       AS "ToFacility__facility_name"';
        $sql .= '      ,ToDepartment.department_name   AS "ToDepartment__department_name"';
        $sql .= '      ,MstFacilityItem.internal_code  AS "MstFacilityItem__internal_code"';
        $sql .= '      ,MstFacilityItem.item_name      AS "MstFacilityItem__item_name"';
        $sql .= '      ,MstFacilityItem.standard       AS "MstFacilityItem__standard"';
        $sql .= '      ,MstFacilityItem.item_code      AS "MstFacilityItem__item_code"';

        $sql .= '      ,MstDealer.dealer_name          AS "MstDealer__dealer_name"';
        $sql .= '      ,( case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name ';
        $sql .= "       else MstUnitName.unit_name || '(' ||MstItemUnit.per_unit || PerUnitName.unit_name ||')'";
        $sql .= '       end )                          as "TrnShipping__unit_name"';
        $sql .= '      ,MstReceiving.work_no           AS "MstReceiving__work_no"';
        $sql .= ' FROM  trn_shippings AS TrnShipping';
        $sql .= '  LEFT JOIN mst_departments as a ';
        $sql .= '         ON a.id = TrnShipping.department_id_from';
        $sql .= '  LEFT JOIN trn_move_headers AS TrnMoveHeader';
        $sql .= '         ON TrnShipping.trn_move_header_id = TrnMoveHeader.id';
        $sql .= '  LEFT JOIN trn_receivings AS TrnReceiving';
        $sql .= '         ON TrnReceiving.trn_shipping_id = TrnShipping.id';
        $sql .= '  LEFT JOIN trn_stickers AS TrnSticker';
        $sql .= '         ON TrnShipping.trn_sticker_id = TrnSticker.id';
        $sql .= '  LEFT JOIN mst_item_units AS MstItemUnit';
        $sql .= '         ON (case when a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') . ' or TrnMoveHeader.move_type = '.Configure::read('MoveType.move2outer').' or TrnReceiving.mst_item_unit_id is NULL then TrnShipping.mst_item_unit_id else TrnReceiving.mst_item_unit_id  end ) = MstItemUnit.id';
        $sql .= '  LEFT JOIN mst_facility_items AS MstFacilityItem';
        $sql .= '         ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
        $sql .= '  LEFT JOIN mst_item_units AS FromMstItemUnit';
        $sql .= '         ON TrnSticker.mst_item_unit_id = FromMstItemUnit.id';
        $sql .= '  LEFT JOIN mst_facility_items AS FromMstFacilityItem';
        $sql .= '         ON FromMstItemUnit.mst_facility_item_id = FromMstFacilityItem.id';
        $sql .= '  LEFT JOIN mst_unit_names MstUnitName ON MstItemUnit.mst_unit_name_id = MstUnitName.id';
        $sql .= '  LEFT JOIN mst_unit_names PerUnitName ON MstItemUnit.per_unit_name_id = PerUnitName.id';
        $sql .= '  LEFT JOIN mst_dealers MstDealer ON MstFacilityItem.mst_dealer_id = MstDealer.id';
        $sql .= '  LEFT JOIN mst_departments AS FromDepartment ON TrnMoveHeader.department_id_from = FromDepartment.id';
        $sql .= '  LEFT JOIN mst_facilities AS FromFacility ON FromDepartment.mst_facility_id = FromFacility.id';
        $sql .= '  LEFT JOIN mst_departments AS ToDepartment ON TrnMoveHeader.department_id_to = ToDepartment.id';
        $sql .= '  LEFT JOIN mst_facilities AS ToFacility ON ToDepartment.mst_facility_id = ToFacility.id';
        $sql .= '  LEFT JOIN trn_receivings as MstReceiving ON TrnMoveHeader.id = MstReceiving.trn_move_header_id and MstReceiving.trn_shipping_id = TrnShipping.id';
        $sql .= '  LEFT JOIN mst_classes as MstClasses ON MstClasses.id = TrnShipping.work_type';
        $sql .= $where;

        return $sql;
    }


    /**
     * 移動履歴CSV出力
     */
    function export_csv() {
        $this->request->data['TrnMoveHeader']['id'] = null;

        $where = $this->_getHistoryDetailWhere(true);
        $sql = $this->_getMoveCSV($where);
        $this->db_export_csv($sql , "移動履歴", '/moves/history');
    }

    private function _getMoveCSV($where) {

        $sql  = 'SELECT ';
        $sql .= '       TrnMoveHeader.work_no          AS 移動番号';
        $sql .= '      ,MstReceiving.work_no           AS 受領番号';

        $sql .= "      ,FromFacility.facility_name||'／'||FromDepartment.department_name";
        $sql .= '                                      AS 移動元名';
        $sql .= "      ,ToFacility.facility_name||'／'||ToDepartment.department_name";
        $sql .= '                                      AS 移動先名';
        $sql .= '      ,MstFacilityItem.internal_code  AS "商品ID"';
        $sql .= ', ( case TrnMoveHeader.move_type ';
        foreach(Configure::read('MoveTypes') as $k=>$v){
            $sql .= " when $k then '$v' ";
        }
        $sql .= ' end )                                as 種別';

        $sql .= '      ,MstFacilityItem.item_name      AS 商品名';
        $sql .= '      ,MstFacilityItem.standard       AS 規格';
        $sql .= '      ,MstFacilityItem.item_code      AS 製品番号';
        $sql .= '      ,MstDealer.dealer_name          AS 販売元名';
        $sql .= '      ,( case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name ';
        $sql .= "       else MstUnitName.unit_name || '(' ||MstItemUnit.per_unit || PerUnitName.unit_name ||')'";
        $sql .= '       end )                          as 包装単位';
        $sql .= '      ,COALESCE(TrnShipping.facility_sticker_no, ';
        $sql .= '       TrnSticker.facility_sticker_no) AS センターシール';
        $sql .= '      ,TrnSticker.lot_no              AS ロット番号';
        $sql .= "      ,to_char(TrnSticker.validated_date,'YYYY/mm/dd')";
        $sql .= '                                      AS 有効期限';
        $sql .= '      ,TrnShipping.quantity           AS 数量';
        $sql .= '      ,TrnSticker.transaction_price   AS 単価';
        $sql .= '      ,MstClass.name                  AS 作業区分名';

        $sql .= '      ,TrnShipping.recital            AS 備考';

        $sql .= ' FROM  trn_shippings AS TrnShipping';
        $sql .= '  LEFT JOIN mst_departments as a ';
        $sql .= '         ON a.id = TrnShipping.department_id_from';
        $sql .= '  LEFT JOIN trn_move_headers AS TrnMoveHeader';
        $sql .= '         ON TrnShipping.trn_move_header_id = TrnMoveHeader.id';
        $sql .= '  LEFT JOIN trn_receivings AS TrnReceiving';
        $sql .= '         ON TrnReceiving.trn_shipping_id = TrnShipping.id';
        $sql .= '  LEFT JOIN trn_stickers AS TrnSticker';
        $sql .= '         ON TrnShipping.trn_sticker_id = TrnSticker.id';
        $sql .= '  LEFT JOIN mst_item_units AS MstItemUnit';
        $sql .= '         ON (case when a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') . ' or TrnMoveHeader.move_type = '.Configure::read('MoveType.move2outer').' or TrnReceiving.mst_item_unit_id is NULL then TrnShipping.mst_item_unit_id else TrnReceiving.mst_item_unit_id  end ) = MstItemUnit.id';
        $sql .= '  LEFT JOIN mst_facility_items AS MstFacilityItem';
        $sql .= '         ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
        $sql .= '  LEFT JOIN mst_item_units AS FromMstItemUnit';
        $sql .= '         ON TrnSticker.mst_item_unit_id = FromMstItemUnit.id';
        $sql .= '  LEFT JOIN mst_facility_items AS FromMstFacilityItem';
        $sql .= '         ON FromMstItemUnit.mst_facility_item_id = FromMstFacilityItem.id';
        $sql .= '  LEFT JOIN mst_unit_names MstUnitName ON MstItemUnit.mst_unit_name_id = MstUnitName.id';
        $sql .= '  LEFT JOIN mst_unit_names PerUnitName ON MstItemUnit.per_unit_name_id = PerUnitName.id';
        $sql .= '  LEFT JOIN mst_dealers MstDealer ON MstFacilityItem.mst_dealer_id = MstDealer.id';
        $sql .= '  LEFT JOIN mst_departments AS FromDepartment ON TrnMoveHeader.department_id_from = FromDepartment.id';
        $sql .= '  LEFT JOIN mst_facilities AS FromFacility ON FromDepartment.mst_facility_id = FromFacility.id';
        $sql .= '  LEFT JOIN mst_departments AS ToDepartment ON TrnMoveHeader.department_id_to = ToDepartment.id';
        $sql .= '  LEFT JOIN mst_facilities AS ToFacility ON ToDepartment.mst_facility_id = ToFacility.id';
        $sql .= '  LEFT JOIN trn_receivings as MstReceiving ON TrnMoveHeader.id = MstReceiving.trn_move_header_id and MstReceiving.trn_shipping_id = TrnShipping.id';
        $sql .= '  LEFT JOIN mst_classes as MstClass on MstClass.id = TrnShipping.work_type';

        $sql .= $where;

        return $sql;
    }

    /**
     * 部署シール印刷用メソッド
     */
    public function seal($type = null){
        if(!is_null($type)){
            $this->$type();
        }
    }

    /**
     * 部署シール印刷
     */
    public function print_hospital_sticker(){
        App::import('Sanitize');
        $_hno = array();
        $data = array();
        $sql  = ' select ';
        $sql .= '       c.hospital_sticker_no  ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as c  ';
        $sql .= '   where ';
        $sql .= '     c.id in ( ' . implode(",",$this->request->data['TrnMove']['id']).")";
        $sql .= " and c.hospital_sticker_no != ''";
        $ret = $this->TrnShipping->query($sql);

        foreach($ret as $r ){
            $_hno[] = $r[0]['hospital_sticker_no'];
        }
        if(!empty($_hno)){
            $order = 'MstDepartment.department_name asc, MstShelfNameTo.code asc';
            $data = $this->Stickers->getHospitalStickers($_hno, $order,2);
        }
        
        $this->set('sortkey' , '病院情報,センター棚番号,商品ID');
        $this->set('Sticker' , $data);
        $this->layout = 'xml/default';
        $this->render('/stickerissues/hospital_sticker');
    }
}
