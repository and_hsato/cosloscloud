<?php
/**
 * ExpirationsController
 *　期限切れ処理
 * @version 1.0.0
 * @since 2010/05/18
 */
class ExpirationsController extends AppController {

    /**
     * @var $name
     */
    var $name = 'Expirations';

    /**
     * @var array $uses
     */
    var $uses = array(
            'MstClass',
            'MstFacility',
            'TrnSticker',
            'MstItemUnit',
            'TrnCloseHeader',
            'TrnAdjustHeader',
            'TrnStickerRecord',
            'TrnStock',
            'TrnConsume',
            'MstDepartment',
            'MstUser',
            'MstFacilityRelation'
    );

    /**
     * @var bool $scaffold
    */
    //var $scaffold;

    /**
     * @var array $helpers
    */
    var $helpers = array('Form','Html','Time','DatePicker');

    /**
     * @var array $components
    */
    var $components = array('RequestHandler','CsvWriteUtils');

    function beforeFilter() {
        parent::beforeFilter();
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }


    /**
     *
    */
    function index (){
    }

    /**
     * 期限切れ 在庫調整 初期画面表示
     */
    function adjustment() {
        $this->setRoleFunction(46); //自家消費・雑損
        //施設リスト
        $this->set('facilities',$this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                       array(Configure::read('FacilityType.center') , Configure::read('FacilityType.hospital'))
                                                       )
                   );

        //作業区分
        $this->set('classes', $this->getClassesList());
        $this->request->data['setting']['date'] = date('Y/m/d');
    }

    /**
     * 期限切れ 在庫調整 バーコード入力画面
     */
    function adjustment_reader() {
        //在庫締めチェック
        if(false === $this->canUpdateAfterClose($this->request->data['setting']['date'], $this->Session->read('Auth.facility_id_selected'))){
            $this->Session->setFlash('すでに締めが行われています', 'growl', array('type'=>'error'));
            $this->adjustment();
            return;
        }

    }

    /**
     * 期限切れ 在庫調整 検索画面
     */
    function adjustment_search() {
        App::import('Sanitize');

        //在庫締めチェック
        if(false === $this->canUpdateAfterClose($this->request->data['setting']['date'], $this->Session->read('Auth.facility_id_selected'))){
            $this->Session->setFlash('すでに締めが行われています', 'growl', array('type'=>'error'));
            $this->adjustment();
            return;
        }

        //検索結果格納変数
        $SearchResult = array();
        $CartSearchResult = array();

        if( isset($this->request->data['search']['is_search']) ){
            //検索処理
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            /* 画面から入力された検索条件 */

            //病院部署を選択している場合、低レベルの調整はできない。
            $where  = " AND ( case ";
            $where .= ' when DPMT.department_type = ' . Configure::read('DepartmentType.warehouse') ;
            $where .= ' then ( STCK.trade_type in ( 1 , 2 , 7 ) ) '; // 倉庫は、定数品、準定数品、低レベル品
            $where .= ' else ( STCK.trade_type in ( 1 , 2 , 9 ) ) '; // 部署は、定数品、準定数品、非在庫品
            $where .= ' end ) ';
            //商品ID(前方一致)
            if( !empty($this->request->data['search']['item_id']) ){
                $where .= " AND FCIT.internal_code ILIKE '".$this->request->data['search']['item_id']."%'";
            }
            //製品番号(前方一致)
            if( !empty($this->request->data['search']['item_code']) ){
                $where .= " AND FCIT.item_code ILIKE '".$this->request->data['search']['item_code']."%'";
            }
            //商品名(部分一致)
            if( !empty($this->request->data['search']['item_name']) ){
                $where .= " AND FCIT.item_name ILIKE '%".$this->request->data['search']['item_name']."%'";
            }
            //規格(部分一致)
            if( !empty($this->request->data['search']['standard']) ){
                $where .= " AND FCIT.standard ILIKE '%".$this->request->data['search']['standard']."%'";
            }
            //販売元(部分一致)
            if( !empty($this->request->data['search']['dealer_name']) ){
                $where .= " AND DELR.dealer_name ILIKE '%".$this->request->data['search']['dealer_name']."%'";
            }
            //シール＃(前方一致)
            if( !empty($this->request->data['search']['stickers_id']) ){
                $where .= " AND (STCK.facility_sticker_no ILIKE '".$this->request->data['search']['stickers_id']."%'";
                $where .= "  OR STCK.hospital_sticker_no ILIKE '".$this->request->data['search']['stickers_id']."%')";
            }
            //ロット＃(前方一致)
            if( !empty($this->request->data['search']['lot_no']) ){
                $where .= " AND STCK.lot_no ILIKE '".$this->request->data['search']['lot_no']."%'";
            }

            //施設
            $where .= " AND FCLT.facility_code = '" . $this->request->data['setting']['facilities']. "'";
            //部署
            $where .= " AND DPMT.department_code = '" . $this->request->data['setting']['departments']. "'";

            //調整種別(在庫減の場合はシール数量ゼロを除外)
            switch ($this->request->data['setting']['adjustment_type']) {
                case Configure::read("ExpirationType.captiveConsumptionReduction"):
                    $where .= " AND STCK.quantity <> 0";
                    break;
                case Configure::read("ExpirationType.miscellaneousLossReduction"):
                    $where .= " AND STCK.quantity <> 0";
                    break;
                default:
                    $where .= " AND (CASE WHEN FCIT.is_lowlevel = false";
                    $where .= "     THEN STCK.quantity";
                    $where .= "     ELSE 0";
                    $where .= "     END) = 0";
                    break;
            }

            if($this->request->data['search']['tempId'] != "" ){
                $where .= " AND STCK.id not IN (".$this->request->data['search']['tempId'].")";
            }

            $SearchResult = $this->getDetail($this->request->data,$where,$this->request->data['Expiration']['limit']);

            //カートに入っている商品の検索
            if($this->request->data['search']['tempId'] != "" ){
                $where = " AND STCK.id IN (".$this->request->data['search']['tempId'].")";
                $CartSearchResult = $this->getDetail($this->request->data,$where);
            }
        }

        $this->set('SearchResult', $SearchResult);
        $this->set('CartSearchResult', $CartSearchResult);
    }

    /**
     * 期限切れ 在庫調整 確認画面表示
     */
    function adjustment_confirm() {
        //検索結果格納配列
        $SearchResult = array();

        //元画面のURLによって処理分岐
        if($this->request->data['Search']['Url'] == "adjustment_reader"){
            /* バーコード読取画面から遷移 */
            //読み込まれたバーコードを検索条件に設定
            $ReadCode = explode(',',substr($this->request->data['Expiration']['codez'],0,- 1));
            $i=0;
            foreach($ReadCode as $barcode){
                $where = " AND (STCK.facility_sticker_no = '".$barcode."' OR STCK.hospital_sticker_no = '".$barcode."' )";

                //検索
                $ret = $this->getDetail($this->request->data,$where);
                if(empty($ret)){
                    //該当のシールが無いのでダミー情報で埋める
                    $ret[0][0]['id']                  = '';
                    $ret[0][0]['facility_sticker_no'] = $barcode;
                    $ret[0][0]['hospital_sticker_no'] = $barcode;
                    $ret[0][0]['mst_item_unit_id']    = '';
                    $ret[0][0]['lot_no']              = '';
                    $ret[0][0]['validated_date']      = '';
                    $ret[0][0]['transaction_price']   = '';
                    $ret[0][0]['quantity']            = '';
                    $ret[0][0]['modified']            = '';
                    $ret[0][0]['unit_name']           = '';
                    $ret[0][0]['internal_code']       = '';
                    $ret[0][0]['item_name']           = '';
                    $ret[0][0]['standard']            = '';
                    $ret[0][0]['item_code']           = '';
                    $ret[0][0]['is_lowlevel']         = '';
                    $ret[0][0]['dealer_name']         = '';
                    $ret[0][0]['status']              = '無効シール';
                    $ret[0][0]['appraised_price']     = 0;
                    $ret[0][0]['substitute_unit_id']  = '';
                }else{
                    // 在庫減 で数量が0の場合登録できないようにする。
                    if( ( $this->request->data['setting']['adjustment_type'] == Configure::read('ExpirationType.captiveConsumptionReduction') ||
                          $this->request->data['setting']['adjustment_type'] == Configure::read('ExpirationType.miscellaneousLossReduction') ) &&
                          $ret[0][0]['quantity'] == 0
                        ){
                        $ret[0][0]['id']     ='';
                        $ret[0][0]['status'] = '数量なし';
                    }
                    // 在庫増 で数量が1以上の場合登録できないようにする。
                    if( ( $this->request->data['setting']['adjustment_type'] == Configure::read('ExpirationType.captiveConsumptionIncrease') ||
                          $this->request->data['setting']['adjustment_type'] == Configure::read('ExpirationType.miscellaneousLossIncrease') ) &&
                          $ret[0][0]['quantity'] > 0
                        ){
                        $ret[0][0]['id']     ='';
                        $ret[0][0]['status'] = '有効なシールです';
                    }
                }
                $SearchResult[$i] = $ret[0];
                $i++;
            }
            //パンくず
            $this->request->data['Search']['back'] = "シール読込";

        }else if($this->request->data['Search']['Url'] == "adjustment_search"){
            if(isset( $this->request->data['TrnInventoryHeader']['id'])){
                /* 棚卸から遷移 */
                foreach($this->request->data['SelectedId'] as $id){
                    if($id != 0){
                        $where = ' AND STCK.id = ' . $id;
                        //検索
                        $ret = $this->getDetail($this->request->data,$where);
                        if(empty($ret)){
                            //該当のシールが無いのでダミー情報で埋める
                            $ret[0][0]['id']                  = '';
                            $ret[0][0]['facility_sticker_no'] = $barcode;
                            $ret[0][0]['hospital_sticker_no'] = $barcode;
                            $ret[0][0]['mst_item_unit_id']    = '';
                            $ret[0][0]['lot_no']              = '';
                            $ret[0][0]['validated_date']      = '';
                            $ret[0][0]['transaction_price']   = '';
                            $ret[0][0]['quantity']            = '';
                            $ret[0][0]['modified']            = '';
                            $ret[0][0]['unit_name']           = '';
                            $ret[0][0]['internal_code']       = '';
                            $ret[0][0]['item_name']           = '';
                            $ret[0][0]['standard']            = '';
                            $ret[0][0]['item_code']           = '';
                            $ret[0][0]['is_lowlevel']         = '';
                            $ret[0][0]['dealer_name']         = '';
                            $ret[0][0]['status']              = '無効シール';
                            $ret[0][0]['appraised_price']     = 0;
                            $ret[0][0]['substitute_unit_id']  = '';
                        }
                        $SearchResult[] = $ret[0];
                    }
                }

            }else{
                /* 検索画面から遷移 */
                //選択されたID取得
                if( $this->request->data['search']['tempId'] != ''  ){
                    $where = " AND STCK.id IN (".$this->request->data['search']['tempId'].")";
                    $SearchResult = $this->getDetail($this->request->data,$where);
                }
            }
            //パンくず
            $this->request->data['Search']['back'] = "在庫選択";
        }

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnAdjustment.token' , $token);
        $this->request->data['TrnAdjustment']['token'] = $token;

        $this->set('classes', $this->getClassesList());
        $this->set("data",$this->request->data);
        $this->set('SearchResult',$SearchResult);

    }

    /**
     * 期限切れ 在庫調整登録処理
     */
    function adjustment_regist() {

        if($this->request->data['TrnAdjustment']['token'] === $this->Session->read('TrnAdjustment.token')) {
            $this->Session->delete('TrnAdjustment.token');

            $date = date("Y/m/d");
            $now = date('Y/m/d H:i:s.u');
            
            $seq = 1;
            //トランザクション開始
            $this->TrnAdjustHeader->begin();

            //在庫調整番号採番
            $work_no = $this->setWorkNo4Header($date, '16');
            $this->request->data['regist']['work_no'] = $work_no;

            //行ロック
            $this->TrnAdjustHeader->query( " select * from  trn_stickers as a where a.id in (" .join(',',$this->request->data['Regist']['Id']). ") for update ");

            //更新チェック
            $ret = $this->TrnAdjustHeader->query( " select count(*) from  trn_stickers as a where a.id in (" .join(',',$this->request->data['Regist']['Id']). ") and a.modified =  '" . $this->request->data['TrnAdjustment']['time'] . "'");
            if($ret[0][0]['count']>0){
                $this->TrnAdjustHeader->rollback();
                $this->Session->setFlash('ほかのユーザによって更新されています。最初からやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('adjustment');
            }

            //部署コードから部署IDを取得
            $mst_department_id = $this->getDepartmentId($this->getFacilityId($this->request->data['setting']['facilities'] , array(Configure::read('FacilityType.center'), Configure::read('FacilityType.hospital'))) ,
                                                      array(Configure::read('DepartmentType.warehouse'), Configure::read('DepartmentType.hospital'),Configure::read('DepartmentType.opestock')) ,
                                                      $this->request->data['setting']['departments']
                                                      );
            //調整ヘッダ作成
            $TrnAdjustHeader =  array(
                'work_no'           => $work_no,
                'work_date'         => $this->request->data['setting']['date'],
                'recital'           => $this->request->data['setting']['remarks'],
                'mst_department_id' => $mst_department_id,
                'detail_count'      => count($this->request->data['Regist']['Id']),
                'is_deleted'        => false,
                'creater'           => $this->Session->read('Auth.MstUser.id'),
                'modifier'          => $this->Session->read('Auth.MstUser.id'),
                'created'           => $now,
                'modified'          => $now
                );
            
            $this->TrnAdjustHeader->create();
            if (!$this->TrnAdjustHeader->save($TrnAdjustHeader)) {
                $this->TrnAdjustHeader->rollback();
                $this->Session->setFlash('調整ヘッダ登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('adjustment');
            }
            
            $trn_adjust_header_id = $this->TrnAdjustHeader->getLastInsertID();
            

            $sql  = ' select ';
            $sql .= '       a.id ';
            $sql .= '     , a.facility_sticker_no ';
            $sql .= '     , a.hospital_sticker_no ';
            $sql .= '     , a.mst_item_unit_id ';
            $sql .= '     , a.quantity  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a ';
            $sql .= '   where a.id in (' . join(',',$this->request->data['Regist']['Id']) . ')';

            $result = $this->TrnSticker->query($sql);
            $isSubtractionFlg = false;
            foreach($result as $r) { 
                //在庫の増or減処理設定
                if($this->request->data['setting']['adjustment_type'] == Configure::read('ExpirationType.captiveConsumptionReduction') ||
                   $this->request->data['setting']['adjustment_type'] == Configure::read('ExpirationType.miscellaneousLossReduction') ){
                    //調整区分が１or2の場合は減算
                    $isSubtractionFlg = true;
                }
                
                /**********************************
                 *  DB更新
                 **********************************/
                
                //在庫数量更新
                $this->TrnStock->create();
                if($isSubtractionFlg){
                    $ret = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.stock_count' => 'TrnStock.stock_count - ' . $this->request->data['Regist']['AdjustmentCount'][$r[0]['id']],
                            'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'    => "'" . $now . "'"
                            ),
                        array(
                            'TrnStock.id' => $this->getStockRecode( $r[0]['mst_item_unit_id'],
                                                                    $mst_department_id)
                            ),
                        -1
                        );
                }else{
                    $ret = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.stock_count' => 'TrnStock.stock_count + ' . $this->request->data['Regist']['AdjustmentCount'][$r[0]['id']],
                            'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'    => "'" . $now . "'"
                            ),
                        array(
                            'TrnStock.id' => $this->getStockRecode( $r[0]['mst_item_unit_id'],
                                                                    $mst_department_id)
                            ),
                        -1
                        );
                }
                if(!$ret) {
                    $this->TrnAdjustHeader->rollback();
                    $this->Session->setFlash('在庫情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('adjustment');
                }
                

                
                //在庫調整(消費)明細作成
                $TrnConsume = array(
                    'work_no'                     => $work_no,
                    'work_seq'                    => $seq,
                    'work_date'                   => $this->request->data['setting']['date'],
                    'work_type'                   => $this->request->data['Regist']['work_type'][$r[0]['id']],
                    'recital'                     => $this->request->data['Regist']['Remarks'][$r[0]['id']],
                    'use_type'                    => Configure::read("UseType.adjustment"),
                    'mst_item_unit_id'            => $r[0]['mst_item_unit_id'],
                    'mst_department_id'           => $mst_department_id,
                    'quantity'                    => $this->request->data['Regist']['AdjustmentCount'][$r[0]['id']],
                    'trn_sticker_id'              => $r[0]['id'],
                    'is_retroactable'             => true,
                    'is_deleted'                  => false,
                    'creater'                     => $this->Session->read('Auth.MstUser.id'),
                    'modifier'                    => $this->Session->read('Auth.MstUser.id'),
                    'created'                     => $now,
                    'modified'                    => $now,
                    'trn_adjust_header_id'        => $trn_adjust_header_id,
                    'adjust_type'                 => $this->request->data['setting']['adjustment_type'],
                    'facility_sticker_no'         => $r[0]['facility_sticker_no'],
                    'hospital_sticker_no'         => $r[0]['hospital_sticker_no'],
                    );
                
                $this->TrnConsume->create();
                if (!$this->TrnConsume->save($TrnConsume)) {
                    $this->TrnAdjustHeader->rollback();
                    $this->Session->setFlash('調整明細登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('adjustment');
                }
                
                //作成した消費レコードのID取得
                $trn_consume_id = $this->TrnConsume->getLastInsertID();
                
                //シール数量&消費参照キー更新
                $this->TrnSticker->create();
                if($isSubtractionFlg){
                    $ret = $this->TrnSticker->updateAll(
                        array(
                            'TrnSticker.quantity'       => 'TrnSticker.quantity - ' . $this->request->data['Regist']['AdjustmentCount'][$r[0]['id']],
                            'TrnSticker.last_move_date' => "'" . $this->request->data['setting']['date'] . "'",
                            'TrnSticker.trn_adjust_id'  => $trn_consume_id,
                            'TrnSticker.modifier'       => $this->Session->read('Auth.MstUser.id'),
                            'TrnSticker.modified'       => "'" . $now . "'"
                            ),
                        array(
                            'TrnSticker.id' => $r[0]['id']
                            ),
                        -1
                        );
                }else{
                    $ret = $this->TrnSticker->updateAll(
                        array(
                            'TrnSticker.quantity'       => 'TrnSticker.quantity + ' . $this->request->data['Regist']['AdjustmentCount'][$r[0]['id']],
                            'TrnSticker.last_move_date' => "'" . $this->request->data['setting']['date'] . "'",
                            'TrnSticker.trn_adjust_id'  => $trn_consume_id,
                            'TrnSticker.modifier'       => $this->Session->read('Auth.MstUser.id'),
                            'TrnSticker.modified'       => "'" . $now . "'"
                            ),
                        array(
                            'TrnSticker.id' => $r[0]['id']
                            ),
                        -1
                        );
                }
                if(!$ret) {
                    $this->TrnAdjustHeader->rollback();
                    $this->Session->setFlash('シール情報更新処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('adjustment');
                }
                
                //シール移動履歴作成
                $this->TrnStickerRecord->create();
                $TrnStickerRecord = array(
                    'move_date'           => "'".$this->request->data['setting']['date']."'",
                    'move_seq'            => $this->getNextRecord($r[0]['id']),
                    'record_type'         => Configure::read('RecordType.ajast'),
                    'record_id'           => $trn_consume_id,
                    'facility_sticker_no' => $r[0]['facility_sticker_no'],
                    'hospital_sticker_no' => $r[0]['hospital_sticker_no'],
                    'trn_sticker_id'      => $r[0]['id'],
                    'mst_department_id'   => $mst_department_id,
                    'is_deleted'          => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $now,
                    'modified'            => $now
                    );
                
                if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                    $this->TrnAdjustHeader->rollback();
                    $this->Session->setFlash('シール移動履歴登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('adjustment');
                }

                $seq++;
                
            }
            
            //コミット直前に更新チェック
            $ret = $this->TrnAdjustHeader->query( " select count(*) from  trn_stickers as a where a.id in (" .join(',',$this->request->data['Regist']['Id']). ") and a.modified =  '" . $this->request->data['TrnAdjustment']['time'] . "' and a.modified <> '" .$now. "'");
            if($ret[0][0]['count']>0){
                //更新チェックエラー
                $this->TrnAdjustHeader->rollback();
                $this->Session->setFlash('ほかのユーザによって更新されています。最初からやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('adjustment');
            }

            $this->TrnAdjustHeader->commit();

            // 結果表示用データを取得
            $sql  = ' select ';
            $sql .= '       a.id ';
            $sql .= '     , a.facility_sticker_no ';
            $sql .= '     , a.hospital_sticker_no ';
            $sql .= '     , a.lot_no';
            $sql .= "     , to_char(a.validated_date ,'YYYY/mm/dd') as validated_date ";
            $sql .= '     , a.quantity ';
            $sql .= '     , c.internal_code ';
            $sql .= '     , c.item_name ';
            $sql .= '     , c.item_code ';
            $sql .= '     , c.standard ';
            $sql .= '     , d.dealer_name ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when b.per_unit = 1  ';
            $sql .= '         then b1.unit_name  ';
            $sql .= "         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
            $sql .= '         end ';
            $sql .= '     )                            as unit_name  ';
            $sql .= '     , 0 as price'; //評価額
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '     left join mst_item_units as b  ';
            $sql .= '       on b.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_unit_names as b1  ';
            $sql .= '       on b1.id = b.mst_unit_name_id  ';
            $sql .= '     left join mst_unit_names as b2  ';
            $sql .= '       on b2.id = b.per_unit_name_id  ';
            $sql .= '     left join mst_facility_items as c  ';
            $sql .= '       on c.id = b.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as d  ';
            $sql .= '       on d.id = c.mst_dealer_id  ';
            $sql .= '   where a.id in (' . join(',',$this->request->data['Regist']['Id']) . ')';
            $sql .= '   order by ';
            $sql .= '     a.facility_sticker_no ';
            $sql .= ' , a.hospital_sticker_no ';
            
            $result = $this->TrnSticker->query($sql);
            
            $this->Session->write('TrnAdjustment.Result' , $result);
            $this->set('result',$result);
        }else{
            //2度押し対策
            $this->set('result',$this->Session->read('TrnAdjustment.Result'));
        }
    }

    /**
     * 期限切れ 調整履歴 初期画面表示
     */
    function history(){
        App::import('Sanitize');
        $this->setRoleFunction(47); //期限切れ調整履歴
        //施設リスト
        $this->set('facilities',$this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                       array(Configure::read('FacilityType.center') ,
                                                             Configure::read('FacilityType.hospital')),
                                                       false
                                                       )
        );
        //作業区分
        $this->set('classes', $this->getClassesList());
        //検索結果格納変数
        $SearchResult = array();

        $department = array();

        if( isset($this->request->data['search']['is_search']) ) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            /*****************************************************
             * 画面から入力された検索条件でWHERE句生成
            *****************************************************/
            $where = "";
            //調整番号(前方一致)
            if($this->request->data['search']['work_no'] != '' ){
                $where .= " and a.work_no ILIKE '".$this->request->data['search']['work_no']."%'";
            }
            //調整日(FROM)
            if($this->request->data['search']['work_date_from'] != '' ){
                $where .= " and a.work_date >= '". $this->request->data['search']['work_date_from']."'";
            }
            //調整日(TO)
            if($this->request->data['search']['work_date_to'] != ''){
                $where .= " and a.work_date <= '". $this->request->data['search']['work_date_to'] ."'";
            }
            //施設(完全一致)
            if($this->request->data['search']['facilities'] != '' ){
                $where .= " and e.facility_code = '".$this->request->data['search']['facilities']."'";
            }
            //部署(完全一致)
            if($this->request->data['search']['departments'] != '' ){
                $where .= " and d.department_code = '".$this->request->data['search']['departments']."'";
            }
            //商品ID(前方一致)
            if($this->request->data['search']['item_id'] != '' ){
                $where .= " and g.internal_code ILIKE '".$this->request->data['search']['item_id']."%'";
            }
            //製品番号(前方一致)
            if($this->request->data['search']['item_code'] != '' ){
                $where .= " and g.item_code ILIKE '".$this->request->data['search']['item_code']."%'";
            }
            //ロット＃(前方一致)
            if($this->request->data['search']['lot_no'] != ''){
                $where .= " and h.lot_no ILIKE '".$this->request->data['search']['lot_no']."%'";
            }
            //商品名(部分一致)
            if($this->request->data['search']['item_name'] != '' ){
                $where .= " and g.item_name ILIKE '%".$this->request->data['search']['item_name']."%'";
            }
            //販売元(部分一致)
            if($this->request->data['search']['dealer_name'] != ''){
                $where .= " and g1.dealer_name ILIKE '%".$this->request->data['search']['dealer_name']."%'";
            }
            //管理区分(完全一致)
            if($this->request->data['search']['management'] != ''){
                $where .= " and h.trade_type = ".$this->request->data['search']['management'];
            }
            //規格(部分一致)
            if($this->request->data['search']['standard'] != '' ){
                $where .= " and g.standard ILIKE '%".$this->request->data['search']['standard']."%'";
            }
            //シール＃(前方一致)
            if($this->request->data['search']['sticker_no'] != ''){
                $where .= " and (h.facility_sticker_no = '".$this->request->data['search']['sticker_no']."'";
                $where .= "  or h.hospital_sticker_no = '".$this->request->data['search']['sticker_no']."'";
                $where .= "  or b.facility_sticker_no = '".$this->request->data['search']['sticker_no']."'";
                $where .= "  or b.hospital_sticker_no = '".$this->request->data['search']['sticker_no']."')";
            }
            //作業区分(完全一致)
            if($this->request->data['search']['work_type'] != ''){
                $where .= " AND a.work_type = ".$this->request->data['search']['work_type'];
            }
            //取消も表示する
            if($this->request->data['search']['deleted'] == 0){
                $where .=  " AND b.is_deleted = false";
            }
            $sql  = ' select ';
            $sql .= '       a.id               as "TrnAdjustHeader__id"';
            $sql .= '     , a.work_no          as "TrnAdjustHeader__work_no"';
            $sql .= "     , to_char(a.work_date, 'YYYY/mm/dd') ";
            $sql .= '                          as "TrnAdjustHeader__work_date"';
            $sql .= '     , e.facility_name    as "TrnAdjustHeader__facility_name"' ;
            $sql .= '     , d.department_name  as "TrnAdjustHeader__department_name"';
            $sql .= "     , to_char(a.created, 'YYYY/mm/dd hh24:mi:ss') ";
            $sql .= '                          as "TrnAdjustHeader__created"';
            $sql .= '     , c.user_name        as "TrnAdjustHeader__user_name"';
            $sql .= '     , count(*)           as "TrnAdjustHeader__count"';
            $sql .= '     , a.detail_count     as "TrnAdjustHeader__detail_count"';
            $sql .= '     , a.recital          as "TrnAdjustHeader__recital"';
            $sql .= '   from ';
            $sql .= '     trn_adjust_headers as a  ';
            $sql .= '     left join trn_consumes as b  ';
            $sql .= '       on b.trn_adjust_header_id = a.id  ';
            $sql .= '     left join mst_users as c  ';
            $sql .= '       on c.id = a.creater  ';
            $sql .= '     left join mst_departments as d  ';
            $sql .= '       on d.id = b.mst_department_id  ';
            $sql .= '     left join mst_facilities as e  ';
            $sql .= '       on e.id = d.mst_facility_id  ';
            $sql .= '     left join mst_item_units as f  ';
            $sql .= '       on f.id = b.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as g  ';
            $sql .= '       on g.id = f.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as g1  ';
            $sql .= '       on g.mst_dealer_id = g1.id ';
            $sql .= '     left join trn_stickers as h  ';
            $sql .= '       on h.id = b.trn_sticker_id ';
            $sql .= '     inner join mst_user_belongings as z  ';
            $sql .= '       on z.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '       and z.mst_facility_id = e.id  ';
            $sql .= '   where ';
            $sql .= '     1 = 1  ';
            $sql .= '     and f.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= $where;
            $sql .= '   group by ';
            $sql .= '     a.id ';
            $sql .= '     , a.work_no ';
            $sql .= '     , a.work_date ';
            $sql .= '     , e.facility_name ';
            $sql .= '     , d.department_name ';
            $sql .= '     , c.user_name ';
            $sql .= '     , a.detail_count ';
            $sql .= '     , a.created ';
            $sql .= '     , a.recital ';
            $sql .= '   order by a.id desc ';
            //全件取得
            $countsql = 'select count(*) as count from ( ' . $sql . ' ) as x ' ;
            $ret = $this->TrnAdjustHeader->query($countsql);
            $this->set('max' , $ret[0][0]['count']);
            //表示データ取得
            $sql .= ' limit ' .$this->request->data['Expiration']['limit'];
            $SearchResult = $this->TrnAdjustHeader->query($sql);

            if($this->request->data['search']['facilities'] != '' ){
                $department = $this->getDepartmentsList($this->request->data['search']['facilities']);
            }
            $this->request->data = $data;
        }else{
            $this->request->data['search']['work_date_from'] = date('Y/m/d', strtotime('-7 day', time()));
            $this->request->data['search']['work_date_to']  = date('Y/m/d', time());
        }

        $this->set("management",Configure::read('Managements'));
        $this->set("SearchResult",$SearchResult);
        $this->set('department', $department);
        $this->render('/expirations/history');
    }

    /**
     * 期限切れ 調整履歴 明細画面表示
     */
    function history_confirm(){
        //検索結果格納変数
        $SearchResult = array();
        //etc
        $msg = "";

        $tempId = "";
        for($i=0; $i<count($this->request->data['SelectedId']); $i++){
            if(!empty($this->request->data['SelectedId'][$i])){
                $tempId .= "'".$this->request->data['SelectedId'][$i]."',";
            }
        }
        $tempId = substr($tempId,0,strlen($tempId)-1);
        $where = " AND CNSM.trn_adjust_header_id IN (".$tempId.")";

        $where .= $this->getWhere($this->request->data);
        $sql = $this->_getHistoryDetail($where);

        $SearchResult = $this->TrnConsume->query($sql);

        if(!$SearchResult){
            $msg .= "明細情報取得エラー";
            $searchFlg = false;
        }else{
            $msg .= "明細情報取得成功";
            $searchFlg = true;
        }

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnAdjustment.token' , $token);
        $this->request->data['TrnAdjustment']['token'] = $token;

        $this->set("data",$this->request->data);
        $this->set("SearchResult",$SearchResult);
    }

    /**
     * 期限切れ 調整履歴 結果画面表示
     */
    function history_result(){
        if($this->request->data['TrnAdjustment']['token'] === $this->Session->read('TrnAdjustment.token')) {
            $this->Session->delete('TrnAdjustment.token');
            $now = date('Y/m/d H:i:s.u');
            
            $targetDateAry = array();
            foreach($this->request->data['Regist']['Id'] as $id){
                $params = array();
                $params['conditions'] = array('TrnConsume.id'=>$id);
                $params['recursive']  = -1;
                $present_c = $this->TrnConsume->find('first',$params);
                $targetDateAry[] = $present_c['TrnConsume']['work_date'];
            }
            //仮締更新権限チェック
            //[選択した明細を全チェックする]
            $type = Configure::read('ClosePrivileges.update');
            if(false === $this->canUpdateAfterCloseByDateAry($type ,$targetDateAry, $this->Session->read('Auth.facility_id_selected'))){
                $this->redirect('history');
                return;
            }

            //結果画面表示用
            $Result = array();
            //結果確認フラグ
            $errorFlg = false;

            //トランザクション開始
            $this->TrnSticker->begin();

            //行ロック
            $this->TrnConsume->query('select * from trn_consumes as a where a.id in ('. join(',',$this->request->data['Regist']['Id']) .') for update ');

            //更新チェック
            $ret = $this->TrnConsume->query('select count(*) from trn_consumes as a where a.id in ('. join(',',$this->request->data['Regist']['Id']) .") and a.modified > '" . $this->request->data['TrnAdjustment']['time'] . "'");
            if($ret[0][0]['count']>0){
                $errorFlg = true;
                $msg = 'ほかユーザによって更新されています。最初からやり直してください。';
            }

            /************************************************
             * テーブル更新
            ************************************************/
            foreach($this->request->data['Regist']['Id'] as $id){
                //情報取得
                $this->TrnConsume->create();
                
                $this->TrnConsume->unbindModel(array(
                    'hasOne'=>array(
                        'TrnConsumeHeader',
                        'TrnStock',
                        'MstFixedCount'
                        ),
                    'belongsTo'=>array(
                        'TrnSticker',
                        'MstDepartment',
                        'MstItemUnit',
                        'TrnSalesRequestHeader'
                        )
                    ));
                
                $options = array();
                $options['fields'] = array(
                    'TrnConsume.modified',
                    'TrnConsume.quantity',
                    'TrnConsume.adjust_type',
                    'TrnConsume.stocking_close_type',
                    'TrnConsume.sales_close_type',
                    'TrnConsume.facility_close_type',
                    'STOC.id',
                    'STOC.stock_count',
                    'STCK.id',
                    'STCK.quantity',
                    'STCK.modified',
                    'STRC.id'
                    );
                //在庫
                $options['joins'][] = array(
                    'type'=>'INNER',
                    'table'=>'trn_stocks',
                    'alias'=>'STOC',
                    'conditions'=>array(
                        'STOC.mst_item_unit_id = TrnConsume.mst_item_unit_id',
                        'STOC.mst_department_id = TrnConsume.mst_department_id'
                        ));
                //シール
                $options['joins'][] = array(
                    'type'=>'INNER',
                    'table'=>'trn_stickers',
                    'alias'=>'STCK',
                    'conditions'=>array(
                        'STCK.id = TrnConsume.trn_sticker_id',
                        'STCK.is_deleted = false'
                        ));
                //シール移動履歴
                $options['joins'][] = array(
                    'type'=>'INNER',
                    'table'=>'trn_sticker_records',
                    'alias'=>'STRC',
                    'conditions'=>array(
                        'STCK.id = STRC.trn_sticker_id',
                        'STRC.is_deleted = false',
                        'STRC.record_type = '.Configure::read('RecordType.ajast')
                        ));
                
                $options['conditions'] = array('TrnConsume.id'=>$id);
                $result = $this->TrnConsume->find('all', $options);
                //更新時間チェック
                if($this->request->data['Regist']['Modified'][$id] != $result[0]['STCK']['modified']){
                    $errorFlg = true;
                    $msg = "別ユーザによってデータが更新されています";
                    break;
                }
                
                //消費テーブルに削除フラグ
                //在庫数量更新
                $this->TrnConsume->create();
                $updatecondtion = array();
                $updatecondtion = array('TrnConsume.id'=>$id);
                
                $updatefield = array();
                $updatefield = array('TrnConsume.is_deleted' => 'true',
                                     'TrnConsume.modifier'   => $this->Session->read('Auth.MstUser.id'),
                                     'TrnConsume.modified'   => "'" . $now . "'"
                                     );

                $res1 = $this->TrnConsume->updateAll( $updatefield, $updatecondtion );
                if(!$res1){
                    $errorFlg = true;
                    $msg .= "消費レコード削除エラー";
                }
                
                //在庫数を戻す
                $stock_quantity = 0;
                if($result[0]['TrnConsume']['adjust_type'] == Configure::read("ExpirationType.captiveConsumptionReduction") ||
                   $result[0]['TrnConsume']['adjust_type'] == Configure::read("ExpirationType.miscellaneousLossReduction") ){
                    //調整区分が「減」のものは在庫数をプラス
                    $stock_quantity = intval($result[0]['STOC']['stock_count']) + intval($result[0]['TrnConsume']['quantity']);
                }else{
                    //調整区分が「増」のものは在庫数をマイナスにする
                    $stock_quantity = intval($result[0]['STOC']['stock_count']) - intval($result[0]['TrnConsume']['quantity']);
                }
                
                $this->TrnStock->create();
                $updatecondtion = array();
                $updatecondtion = array('TrnStock.id'=>$result[0]['STOC']['id']);
                
                $updatefield = array();
                $updatefield = array('TrnStock.stock_count' => $stock_quantity,
                                     'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                     'TrnStock.modified'    => "'" . $now .  "'"
                                     );
                
                $res2 = $this->TrnStock->updateAll( $updatefield, $updatecondtion );
                if(!$res2){
                    $errorFlg = true;
                    $msg .= "在庫数調整エラー";
                }
                
                //シールの数量を戻す
                $sticker_quantity = 0;
                if($result[0]['TrnConsume']['adjust_type'] == Configure::read('ExpirationType.captiveConsumptionReduction') ||
                   $result[0]['TrnConsume']['adjust_type'] == Configure::read('ExpirationType.miscellaneousLossReduction') ){
                    //調整区分が「減」のものは在庫数をプラス
                    $sticker_quantity = intval($result[0]['STCK']['quantity']) + intval($result[0]['TrnConsume']['quantity']);
                }else{
                    //調整区分が「増」のものは在庫数をマイナスにする
                    $sticker_quantity = intval($result[0]['STCK']['quantity']) - intval($result[0]['TrnConsume']['quantity']);
                }
                
                $this->TrnSticker->create();
                $updatecondtion = array();
                $updatecondtion = array('TrnSticker.id'=>$result[0]['STCK']['id']);
                
                $updatefield = array();
                $updatefield = array('TrnSticker.quantity' => $sticker_quantity,
                                     'TrnSticker.modifier' => $this->Session->read('Auth.MstUser.id'),
                                     'TrnSticker.modified' => "'" . $now . "'"
                                     );
                
                $res3 = $this->TrnSticker->updateAll( $updatefield, $updatecondtion );
                if(!$res3){
                    $errorFlg = true;
                    $msg .= "シール数調整エラー";
                }
                
                //シール移動履歴に削除フラグ設定
                $this->TrnStickerRecord->create();
                $updatecondtion = array();
                $updatecondtion = array('TrnStickerRecord.id'=>$result[0]['STRC']['id']);
                
                $updatefield = array();
                $updatefield = array('TrnStickerRecord.is_deleted' => 'true',
                                     'TrnStickerRecord.modifier'   => $this->Session->read('Auth.MstUser.id'),
                                     'TrnStickerRecord.modified'   => "'" . $now . "'"
                                     );
                
                $res = $this->TrnStickerRecord->updateAll( $updatefield, $updatecondtion );
                if(!$res){
                    $errorFlg = true;
                    $msg .= "シール移動履歴削除エラー";
                }
            }//end of for
            
            //コミット直前に更新チェック
            //更新チェック
            $ret = $this->TrnConsume->query('select count(*) from trn_consumes as a where a.id in ('. join(',',$this->request->data['Regist']['Id']) .") and a.modified > '" . $this->request->data['TrnAdjustment']['time'] . "' and a.modified <> '" .$now. "'");
            if($ret[0][0]['count']>0){
                $errorFlg = true;
                $msg = 'ほかユーザによって更新されています。最初からやり直してください。';
            }

            if($errorFlg){
                //ロールバック
                $this->TrnSticker->rollback();
                //画面遷移
                $this->set('data', $this->request->data);
                $this->Session->setFlash($msg, 'growl', array('type'=>'error'));
                $this->adjustment();
                return;
            }else{
                $this->TrnSticker->commit();
            }

            $this->Session->write('TrnAdjustment.data' , $this->request->data);
            $this->Session->write('TrnAdjustment.Result' , $result);

            $this->set('data',$this->request->data);
            $this->set('Result',$result);
        }else{
            //2度押し対策
            $this->set('data', $this->Session->read('TrnAdjustment.data'));
            $this->set('Result',$this->Session->read('TrnAdjustment.Result'));
        }
    }

    /**
     * 作業区分リスト取得
     */
    function getClassesList(){
        $list = Set::Combine(
                $this->MstClass->find('all',
                        array('order' => 'MstClass.code',
                                'conditions'=>array('MstClass.mst_menu_id' => '16'
                                        ,"MstClass.mst_facility_id"=>$this->Session->read('Auth.facility_id_selected'))
                        )),'{n}.MstClass.id','{n}.MstClass.name');
        return $list;
    }

    /**
     * 月締め確認処理
     */
    function checkClosed($data){
        $count = 0;
        $date = explode("/",$data['setting']['date']);

        $params = array();
        $params['mst_facility_id'] = $this->getFacilityId($data['setting']['facilities'] , array(Configure::read('FacilityType.center') , Configure::read('FacilityType.hospital')));
        $params['close_type'] = Configure::read('CloseType.stock');
        $params['start_date <= '] = $data['setting']['date'];
        $params['end_date > '] = date("Y/m/d", mktime(0, 0, 0, $date[1], $date[2]+1, $date[0]));
        $params['is_deleted'] = false;
        $this->TrnCloseHeader->create();
        $this->TrnCloseHeader->unbindModel(array('belongsTo'=>array('MstFacility')));
        $count = $this->TrnCloseHeader->find('count', array('conditions' => $params));

        return $count;
    }

    /**
     * 在庫調整 登録
     * 明細情報を取得
     */
    function getDetail($data,$where,$limit=null){
        $SearchResult = array();

        $sql  = "SELECT STCK.id";
        $sql .= "      ,STCK.facility_sticker_no";
        $sql .= "      ,STCK.hospital_sticker_no";
        $sql .= "      ,STCK.mst_item_unit_id";
        $sql .= "      ,STCK.lot_no";
        $sql .= "      ,to_char(STCK.validated_date,'YYYY/mm/dd') as validated_date ";
        $sql .= "      ,STCK.transaction_price";
        $sql .= "      ,STCK.quantity";
        $sql .= "      ,STCK.modified";
        $sql .= "      ,(case when ITUT.per_unit = 1 then UTNM1.unit_name ";
        $sql .= "        else UTNM1.unit_name || '(' ||ITUT.per_unit || UTNM2.unit_name || ')'";
        $sql .= '        end )          AS "unit_name"';
        $sql .= "      ,ITUT.per_unit";
        $sql .= "      ,UTNM1.unit_name AS u_name";
        $sql .= "      ,UTNM2.unit_name AS p_name";
        $sql .= "      ,FCIT.internal_code";
        $sql .= "      ,FCIT.item_name";
        $sql .= "      ,FCIT.standard";
        $sql .= "      ,FCIT.item_code";
        $sql .= "      ,FCIT.is_lowlevel";
        $sql .= "      ,DELR.dealer_name";
        $sql .= "      ,( CASE";
        $sql .= "         WHEN FCLT.facility_code <> '".$data['setting']['facilities']."' THEN '別施設'";
        $sql .= "         WHEN DPMT.department_code <> '".$data['setting']['departments']."' THEN '別部署'";
        $sql .= "         WHEN STCK.trade_type = 3 THEN '預託品'";
        $sql .= "         WHEN SPNG.move_status = 1 THEN '移動予定'";
        $sql .= "         WHEN SPNG.move_status = 10 THEN '移動予定'";
        $sql .= "         ELSE";
        $sql .= "           CASE";
        $sql .= "             WHEN STCK.trn_return_receiving_id IS NOT NULL THEN";
        $sql .= "               CASE";
        $sql .= "                 WHEN STCK.trn_return_header_id IS NULL THEN '返品予定'";
        $sql .= "                 ELSE ''";
        $sql .= "               END";
        $sql .= "             ELSE";
        $sql .= "               CASE";
        $sql .= "                 WHEN STCK.has_reserved = true THEN '予約中'";
        $sql .= "                 ELSE ''";
        $sql .= "               END";
        $sql .= "           END";
        $sql .= "       END ) AS status";
        $sql .= "      ,(APVL.price * ITUT.per_unit) AS appraised_price";
        $sql .= '      ,MFXC.substitute_unit_id';
        $sql .= "  FROM trn_stickers STCK";
        $sql .= "        INNER JOIN mst_item_units ITUT";
        $sql .= "                ON STCK.mst_item_unit_id = ITUT.id";
        $sql .= "        INNER JOIN mst_unit_names UTNM1";
        $sql .= "                ON ITUT.mst_unit_name_id = UTNM1.id";
        $sql .= "               AND UTNM1.is_deleted = false";
        $sql .= "        INNER JOIN mst_unit_names UTNM2";
        $sql .= "                ON ITUT.per_unit_name_id = UTNM2.id";
        $sql .= "               AND UTNM2.is_deleted = false";
        $sql .= "        INNER JOIN mst_facility_items FCIT";
        $sql .= "                ON ITUT.mst_facility_item_id = FCIT.id";
        $sql .= "         LEFT JOIN mst_dealers DELR";
        $sql .= "                ON FCIT.mst_dealer_id = DELR.id";
        $sql .= "         LEFT JOIN trn_shippings SPNG";
        $sql .= "                ON STCK.trn_shipping_id = SPNG.id";
        $sql .= "               AND SPNG.is_deleted = false";
        $sql .= "        LEFT JOIN mst_departments DPMT";
        $sql .= "                ON STCK.mst_department_id = DPMT.id";
        $sql .= "               AND DPMT.is_deleted = false";
        $sql .= "        LEFT JOIN mst_facilities FCLT";
        $sql .= "                ON DPMT.mst_facility_id = FCLT.id";
        $sql .= "               AND FCLT.is_deleted = false";
        $sql .= '         left join mst_fixed_counts as MFXC';
        $sql .= '                on MFXC.mst_department_id = DPMT.id ';
        $sql .= '               and MFXC.mst_item_unit_id = ITUT.id ';
        $sql .= '               and MFXC.is_deleted = false ';
        //評価額
        $sql .= " LEFT JOIN (";
        $sql .= "      SELECT A.price";
        $sql .= "            ,A.mst_facility_id";
        $sql .= "            ,A.mst_facility_item_id";
        $sql .= "        FROM trn_appraised_values A";
        $sql .= "             INNER JOIN (";
        $sql .= "                   SELECT MAX(id) AS id";
        $sql .= "                         ,mst_facility_id";
        $sql .= "                         ,mst_facility_item_id";
        $sql .= "                     FROM trn_appraised_values";
        $sql .= "                    WHERE is_deleted = false";
        $sql .= "                      AND mst_facility_id = " . $this->Session->read('Auth.facility_id_selected');
        $sql .= "                    GROUP BY ";
        $sql .= "                          mst_facility_id,mst_facility_item_id";
        $sql .= "                   ) AS B";
        $sql .= "                ON A.id = B.id";
        $sql .= "       WHERE A.is_deleted = false";
        $sql .= "         AND A.mst_facility_id = " . $this->Session->read('Auth.facility_id_selected');
        $sql .= "      ) APVL";
        $sql .= "  ON FCIT.id = APVL.mst_facility_item_id";
        $sql .= " WHERE STCK.is_deleted = false";
        $sql .= "   AND STCK.trn_return_header_id IS NULL";
        $sql .= "   AND (CASE WHEN FCIT.is_lowlevel = false";
        $sql .= "        THEN STCK.lot_no ";
        $sql .= "        ELSE '0' ";
        $sql .= "        END) <> ' '";
        $sql .= "   AND FCIT.mst_facility_id = ".$this->Session->read('Auth.facility_id_selected')." ";

        $sql .= $where;
        $sql .= " ORDER BY STCK.facility_sticker_no,FCIT.internal_code";

        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'TrnSticker'));
            $sql .= " LIMIT ".$limit;
        }
        //検索実行
        $SearchResult = $this->TrnSticker->query($sql);

        return $SearchResult;
    }

    /**
     * 在庫調整履歴明細検索用WHERE句生成
     * USE：一覧画面の明細件数取得
     * USE：明細画面へ検索条件引継ぎ
     **/
    function getWhere($data){
        $where = "";
        //調整番号(前方一致)
        if(!empty($data['search']['work_no'])){
            $where .= " AND CNSM.work_no ILIKE '".$data['search']['work_no']."%'";
        }
        //調整日(FROM)
        if(!empty($data['search']['work_date_from'])){
            $where .= " AND CNSM.work_date >= '".date("Y-m-d",strtotime($data['search']['work_date_from']))."'";
        }
        //調整日(TO)
        if(!empty($data['search']['work_date_to'])){
            $where .= " AND CNSM.work_date < '".date('Y-m-d', strtotime('+1 day', strtotime($data['search']['work_date_to'])))."'";
        }
        //施設(完全一致)
        if(!empty($data['search']['facilities'])){
            $where .= " AND FCLT.facility_code = '".$data['search']['facilities']."'";
        }
        //部署(完全一致)
        if(!empty($data['search']['departments'])){
            $where .= " AND DPMT.department_code = '".$data['search']['departments']."'";
        }
        //商品ID(前方一致)
        if(!empty($data['search']['item_id'])){
            $where .= " AND FCIT.internal_code ILIKE '".$data['search']['item_id']."%'";
        }
        //製品番号(前方一致)
        if(!empty($data['search']['item_code'])){
            $where .= " AND FCIT.item_code ILIKE '".$data['search']['item_code']."%'";
        }
        //ロット＃(前方一致)
        if(!empty($data['search']['lot_no'])){
            $where .= " AND STCK.lot_no ILIKE '".$data['search']['lot_no']."%'";
        }
        //商品名(部分一致)
        if(!empty($data['search']['item_name'])){
            $where .= " AND FCIT.item_name ILIKE '%".$data['search']['item_name']."%'";
        }
        //販売元(部分一致)
        if(!empty($data['search']['dealer_name'])){
            $where .= " AND DLRS.dealer_name ILIKE '%".$data['search']['dealer_name']."%'";
        }
        //管理区分(完全一致)
        if(!empty($data['search']['management'])){
            $where .= " AND STCK.trade_type = ".$data['search']['management'];
        }
        //規格(部分一致)
        if(!empty($data['search']['standard'])){
            $where .= " AND FCIT.standard ILIKE '%".$data['search']['standard']."%'";
        }
        //シール＃(前方一致)
        if(!empty($data['search']['sticker_no'])){
            $where .= " AND (STCK.facility_sticker_no = '".$data['search']['sticker_no']."'";
            $where .= "  OR STCK.hospital_sticker_no = '".$data['search']['sticker_no']."'";
            $where .= "  OR CNSM.facility_sticker_no = '".$data['search']['sticker_no']."'";
            $where .= "  OR CNSM.hospital_sticker_no = '".$data['search']['sticker_no']."')";
        }
        //作業区分(完全一致)
        if(!empty($data['search']['work_type'])){
            $where .= " AND ADJH.work_type = ".$data['search']['work_type'];
        }
        return $where;
    }

    /**
     * 調整履歴明細取得
     */
    private function _getHistoryDetail($where){
        $inner_where = "";
        //調整日(FROM)
        if(!empty($this->request->data['search']['work_date_from'])){
            $inner_where .= " AND to_char(work_date, 'YYYYMM') >= '".date("Ym",strtotime($this->request->data['search']['work_date_from']))."'";
        }
        //調整日(TO)
        if(!empty($this->request->data['search']['work_date_to'])){
            $inner_where .= " AND to_char(work_date, 'YYYYMM') < '".date('Ym', strtotime('+1 day', strtotime($this->request->data['search']['work_date_to'])))."'";
        }

        $sql  = "";
        $sql .= "SELECT CNSM.id";
        $sql .= "      ,CNSM.work_no";
        $sql .= "      ,CNSM.is_deleted";
        $sql .= "      ,TO_CHAR(CNSM.work_date,'YYYY/MM/DD') AS work_date";
        $sql .= "      ,FCIT.internal_code";
        $sql .= "      ,FCIT.item_name";
        $sql .= "      ,FCIT.item_code";
        $sql .= "      ,(case when CNSM.quantity = 1 then ";
        $sql .= "          ( case when ITUN.per_unit = 1 then UTNM1.unit_name ";
        $sql .= "            else UTNM1.unit_name || '(' || ITUN.per_unit || UTNM2.unit_name || ')'";
        $sql .= "            end )";
        $sql .= "        else ";
        $sql .= "          ( case when ITUN.per_unit=1 then CNSM.quantity || UTNM1.unit_name ";
        $sql .= "            else CNSM.quantity || UTNM1.unit_name || '(' || ITUN.per_unit || UTNM2.unit_name || ')'";
        $sql .= "            end )";
        $sql .= "        end )                                   as unit_name  ";
        $sql .= "      ,STCK.lot_no";
        $sql .= "      ,COALESCE(CNSM.facility_sticker_no,STCK.facility_sticker_no) AS facility_sticker_no";
        $sql .= "      ,CLSS.name";
        $sql .= "      ,USRS.user_name";
        $sql .= "      ,FCLT.facility_name";
        $sql .= "      ,DPMT.department_name";
        $sql .= "      ,CASE CNSM.adjust_type";
        $sql .= "       WHEN '".Configure::read("ExpirationType.captiveConsumptionReduction")."' THEN '".Configure::read("ExpirationType1")."'";
        $sql .= "       WHEN '".Configure::read("ExpirationType.miscellaneousLossReduction")."' THEN '".Configure::read("ExpirationType2")."'";
        $sql .= "       WHEN '".Configure::read("ExpirationType.captiveConsumptionIncrease")."' THEN '".Configure::read("ExpirationType3")."'";
        $sql .= "       WHEN '".Configure::read("ExpirationType.miscellaneousLossIncrease")."' THEN '".Configure::read("ExpirationType4")."'";
        $sql .= "       END AS adjust_type";
        $sql .= "      ,FCIT.standard";
        $sql .= "      ,DLRS.dealer_name";
        $sql .= "      ,to_char(STCK.validated_date , 'YYYY/MM/DD') as validated_date";
        $sql .= "      ,COALESCE(CNSM.hospital_sticker_no,STCK.hospital_sticker_no) AS hospital_sticker_no";
        $sql .= "      ,STCK.id AS sticker_id";
        $sql .= "      ,STCK.modified";
        $sql .= "      ,(APVL.price * ITUN.per_unit * CNSM.quantity ) AS appraised_price";
        $sql .= "      ,CASE";
        $sql .= "       WHEN CNSM.is_deleted = true THEN '取消済みです'";
        $sql .= "       WHEN STCK.has_reserved = true THEN '予約済みです'";
        $sql .= "       WHEN STCK.has_reserved = true THEN '予約済みです'";
        $sql .= "       WHEN CNSM.facility_close_type = 2 then '締め済みです' ";
        $sql .= "       WHEN CNSM.facility_close_type = 1 and x.updatable = false then '締め済みです' ";
        $sql .= "       WHEN STCK.trade_type = '".Configure::read('ClassesType.LowLevel')."'";
        $sql .= "        THEN ( CASE WHEN STCK.quantity < CNSM.quantity AND ( CNSM.adjust_type = '" . Configure::read('ExpirationType.captiveConsumptionIncrease') ."'  OR CNSM.adjust_type = '" . Configure::read('ExpirationType.miscellaneousLossIncrease') . "' ) THEN '調整数量が在庫数を上回る為、取消しできません'  ELSE '' END ) ";
        $sql .= "       WHEN CNSM.adjust_type = '".Configure::read("ExpirationType.captiveConsumptionReduction")."'";
        $sql .= "        AND STCK.quantity > 0 THEN '数量が更新されています'";
        $sql .= "       WHEN CNSM.adjust_type = '".Configure::read("ExpirationType.miscellaneousLossReduction")."'";
        $sql .= "        AND STCK.quantity > 0 THEN '数量が更新されています'";
        $sql .= "       WHEN CNSM.adjust_type = '".Configure::read("ExpirationType.captiveConsumptionIncrease")."'";
        $sql .= "        AND STCK.quantity = 0 THEN '消費または調整処理されています'";
        $sql .= "       WHEN CNSM.adjust_type = '".Configure::read("ExpirationType.miscellaneousLossIncrease")."'";
        $sql .= "        AND STCK.quantity = 0 THEN '消費または調整処理されています'";
        $sql .= "       ELSE ''";
        $sql .= "        END AS regist_status";
        $sql .= " FROM trn_consumes CNSM";
        //シールテーブルJOIN
        $sql .= " INNER JOIN trn_stickers STCK";
        $sql .= " ON CNSM.trn_sticker_id = STCK.id";
        $sql .= " AND STCK.is_deleted = false";
        //部署テーブルJOIN
        $sql .= " INNER JOIN mst_departments DPMT";
        $sql .= " ON CNSM.mst_department_id = DPMT.id";
        //施設テーブルJOIN
        $sql .= " INNER JOIN mst_facilities FCLT";
        $sql .= " ON DPMT.mst_facility_id = FCLT.id";
        //包装単位テーブルJOIN
        $sql .= " INNER JOIN mst_item_units ITUN";
        $sql .= " ON CNSM.mst_item_unit_id = ITUN.id";
        //単位名テーブル1(包装単位名)JOIN
        $sql .= " INNER JOIN mst_unit_names UTNM1";
        $sql .= " ON ITUN.mst_unit_name_id = UTNM1.id";
        //単位名テーブル2JOIN(入数単位名)
        $sql .= " INNER JOIN mst_unit_names UTNM2";
        $sql .= " ON ITUN.per_unit_name_id = UTNM2.id";
        //施設採用テーブルJOIN
        $sql .= " INNER JOIN mst_facility_items FCIT";
        $sql .= " ON ITUN.mst_facility_item_id = FCIT.id";
        //販売元テーブルJOIN
        $sql .= " LEFT JOIN mst_dealers DLRS";
        $sql .= " ON FCIT.mst_dealer_id = DLRS.id";
        //ユーザテーブルJOIN
        $sql .= " LEFT JOIN mst_users USRS";
        $sql .= " ON CNSM.modifier = USRS.id";
        //調整ヘッダJOIN
        $sql .= " LEFT JOIN trn_adjust_headers ADJH";
        $sql .= " ON CNSM.trn_adjust_header_id=ADJH.id";
        //作業区分マスタJOIN
        $sql .= " LEFT JOIN mst_classes CLSS";
        $sql .= " ON CLSS.mst_facility_id = FCLT.id";
        $sql .= " AND CLSS.id::integer = CNSM.work_type";
        $sql .= " AND CLSS.mst_menu_id = 16";
        //評価額
        $sql .= " LEFT JOIN (";
        $sql .= "      SELECT A.price";
        $sql .= "            ,A.mst_facility_id";
        $sql .= "            ,A.mst_facility_item_id";
        $sql .= "            ,B.work_month";
        $sql .= "        FROM trn_appraised_values A";
        $sql .= "             INNER JOIN (";
        $sql .= "                   SELECT MAX(id) AS id";
        $sql .= "                         ,to_char(X.work_date,'YYYYMM') as work_month ";
        $sql .= "                         ,mst_facility_id";
        $sql .= "                         ,mst_facility_item_id";
        $sql .= "                     FROM trn_appraised_values X";
        $sql .= "                    WHERE is_deleted = false";
        $sql .= "                      AND mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= $inner_where;
        $sql .= "                    GROUP BY ";
        $sql .= "                          mst_facility_id,mst_facility_item_id,to_char(X.work_date,'YYYYMM')";
        $sql .= "                   ) AS B";
        $sql .= "                ON A.id = B.id";
        $sql .= "       WHERE A.is_deleted = false";
        $sql .= "      ) APVL";
        $sql .= " ON FCLT.id = APVL.mst_facility_id";
        $sql .= " AND FCIT.id = APVL.mst_facility_item_id";
        $sql .= " AND APVL.work_month = to_char(CNSM.work_date,'YYYYMM')";
        $sql .= ' cross join (  ';
        $sql .= '     select ';
        $sql .= '         c.updatable  ';
        $sql .= '     from ';
        $sql .= '         mst_users as a  ';
        $sql .= '         left join mst_roles as b  ';
        $sql .= '             on a.mst_role_id = b.id  ';
        $sql .= '         left join mst_privileges as c  ';
        $sql .= '             on c.mst_role_id = b.id  ';
        $sql .= '     where ';
        $sql .= '         a.id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '         and c.view_no = 50'; //
        $sql .= ' ) as x  ';

        //WHERE句
        if($this->request->data['search']['deleted'] !== "1"){
            $sql .= " WHERE CNSM.is_deleted = false";
        }else{
            $sql .= " WHERE 1 = 1";
        }
        $sql .= " AND CNSM.use_type = ".Configure::read("UseType.adjustment");
        $sql .= $where;
        $sql .= " ORDER BY CNSM.work_no,CNSM.work_seq";

        return $sql;

    }

    /**
     * 調整履歴CSV出力
     */
    function export_csv() {

        $where  = $this->getWhere($this->request->data);

        //取消も表示する
        if($this->request->data['search']['deleted'] == 0){
            $where .=  " AND CNSM.is_deleted = false";
        }

        $where .= ' and ITUN.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');

        $sql    = $this->_getExporationCSV($where);
        $this->db_export_csv($sql , "調整履歴", '/expirations/history');
    }
    /**
     * 調整履歴明細取得
     */
    private function _getExporationCSV($where){
        $inner_where = "";
        //調整日(FROM)
        if(!empty($this->request->data['search']['work_date_from'])){
            $inner_where .= " AND to_char(work_date, 'YYYYMM') >= '".date("Ym",strtotime($this->request->data['search']['work_date_from']))."'";
        }
        //調整日(TO)
        if(!empty($this->request->data['search']['work_date_to'])){
            $inner_where .= " AND to_char(work_date, 'YYYYMM') < '".date('Ym', strtotime('+1 day', strtotime($this->request->data['search']['work_date_to'])))."'";
        }

        $sql  = "";
        $sql .= "SELECT ";
        $sql .= "       CNSM.work_no                         as 調整番号";
        $sql .= "      ,TO_CHAR(CNSM.work_date,'YYYY/MM/DD') as 調整日";
        $sql .= "      ,FCLT.facility_name||'／'||DPMT.department_name";
        $sql .= '                                            as 施設／部署名';

        $sql .= '      ,FCIT.internal_code                   as "商品ID"';
        $sql .= "      ,( CASE CNSM.adjust_type";
        $sql .= "       WHEN '".Configure::read("ExpirationType.captiveConsumptionReduction")."' THEN '".Configure::read("ExpirationType1")."'";
        $sql .= "       WHEN '".Configure::read("ExpirationType.miscellaneousLossReduction")."' THEN '".Configure::read("ExpirationType2")."'";
        $sql .= "       WHEN '".Configure::read("ExpirationType.captiveConsumptionIncrease")."' THEN '".Configure::read("ExpirationType3")."'";
        $sql .= "       WHEN '".Configure::read("ExpirationType.miscellaneousLossIncrease")."' THEN '".Configure::read("ExpirationType4")."'";
        $sql .= "       END )                                as 種別";

        $sql .= "      ,FCIT.item_name                       as 商品名";
        $sql .= "      ,FCIT.standard                        as 規格";
        $sql .= "      ,FCIT.item_code                       as 製品番号";
        $sql .= "      ,DLRS.dealer_name                     as 販売元名";

        $sql .= "      ,(case when CNSM.quantity = 1 then ";
        $sql .= "          ( case when ITUN.per_unit = 1 then UTNM1.unit_name ";
        $sql .= "            else UTNM1.unit_name || '(' || ITUN.per_unit || UTNM2.unit_name || ')'";
        $sql .= "            end )";
        $sql .= "        else ";
        $sql .= "          ( case when ITUN.per_unit=1 then CNSM.quantity || UTNM1.unit_name ";
        $sql .= "            else CNSM.quantity || UTNM1.unit_name || '(' || ITUN.per_unit || UTNM2.unit_name || ')'";
        $sql .= "            end )";
        $sql .= "        end )                               as 包装単位";
        $sql .= "      ,(APVL.price * ITUN.per_unit * CNSM.quantity ) AS 調整単価";
        $sql .= "      ,STCK.lot_no as ロット番号";
        $sql .= "      ,to_char(STCK.validated_date , 'YYYY/MM/DD') as 有効期限";

        $sql .= "      ,COALESCE(CNSM.facility_sticker_no,STCK.facility_sticker_no) AS センターシール";
        $sql .= "      ,COALESCE(CNSM.hospital_sticker_no,STCK.hospital_sticker_no) AS 部署シール";

        $sql .= "      ,CLSS.name as 作業区分名";
        $sql .= "      ,USRS.user_name as 更新者名";

        $sql .= "      ,(CASE";
        $sql .= "       WHEN CNSM.is_deleted = true THEN '取消済みです'";
        $sql .= "       WHEN STCK.has_reserved = true THEN '予約済みです'";
        $sql .= "       WHEN STCK.trade_type = '".Configure::read('ClassesType.LowLevel')."'";
        $sql .= "        THEN ( CASE WHEN STCK.quantity < CNSM.quantity AND ( CNSM.adjust_type = '" . Configure::read('ExpirationType.captiveConsumptionIncrease') ."'  OR CNSM.adjust_type = '" . Configure::read('ExpirationType.miscellaneousLossIncrease') . "' ) THEN '調整数量が在庫数を上回る為、取消しできません'  ELSE '' END ) ";
        $sql .= "       WHEN CNSM.adjust_type = '".Configure::read("ExpirationType.captiveConsumptionReduction")."'";
        $sql .= "        AND STCK.quantity > 0 THEN '数量が更新されています'";
        $sql .= "       WHEN CNSM.adjust_type = '".Configure::read("ExpirationType.miscellaneousLossReduction")."'";
        $sql .= "        AND STCK.quantity > 0 THEN '数量が更新されています'";
        $sql .= "       WHEN CNSM.adjust_type = '".Configure::read("ExpirationType.captiveConsumptionIncrease")."'";
        $sql .= "        AND STCK.quantity = 0 THEN '消費または調整処理されています'";
        $sql .= "       WHEN CNSM.adjust_type = '".Configure::read("ExpirationType.miscellaneousLossIncrease")."'";
        $sql .= "        AND STCK.quantity = 0 THEN '消費または調整処理されています'";
        $sql .= "       ELSE ''";
        $sql .= "        END ) AS 備考";

        $sql .= " FROM trn_consumes CNSM";
        //シールテーブルJOIN
        $sql .= " INNER JOIN trn_stickers STCK";
        $sql .= " ON CNSM.trn_sticker_id = STCK.id";
        $sql .= " AND STCK.is_deleted = false";
        //部署テーブルJOIN
        $sql .= " INNER JOIN mst_departments DPMT";
        $sql .= " ON CNSM.mst_department_id = DPMT.id";
        //施設テーブルJOIN
        $sql .= " INNER JOIN mst_facilities FCLT";
        $sql .= " ON DPMT.mst_facility_id = FCLT.id";
        //包装単位テーブルJOIN
        $sql .= " INNER JOIN mst_item_units ITUN";
        $sql .= " ON CNSM.mst_item_unit_id = ITUN.id";
        //単位名テーブル1(包装単位名)JOIN
        $sql .= " INNER JOIN mst_unit_names UTNM1";
        $sql .= " ON ITUN.mst_unit_name_id = UTNM1.id";
        //単位名テーブル2JOIN(入数単位名)
        $sql .= " INNER JOIN mst_unit_names UTNM2";
        $sql .= " ON ITUN.per_unit_name_id = UTNM2.id";
        //施設採用テーブルJOIN
        $sql .= " INNER JOIN mst_facility_items FCIT";
        $sql .= " ON ITUN.mst_facility_item_id = FCIT.id";
        //販売元テーブルJOIN
        $sql .= " LEFT JOIN mst_dealers DLRS";
        $sql .= " ON FCIT.mst_dealer_id = DLRS.id";
        //ユーザテーブルJOIN
        $sql .= " LEFT JOIN mst_users USRS";
        $sql .= " ON CNSM.modifier = USRS.id";
        //調整ヘッダJOIN
        $sql .= " LEFT JOIN trn_adjust_headers ADJH";
        $sql .= " ON CNSM.trn_adjust_header_id=ADJH.id";
        //作業区分マスタJOIN
        $sql .= " LEFT JOIN mst_classes CLSS";
        $sql .= " ON CLSS.mst_facility_id = FCLT.id";
        $sql .= " AND CLSS.id::integer = CNSM.work_type";
        $sql .= " AND CLSS.mst_menu_id = 16";
        //評価額
        $sql .= " LEFT JOIN (";
        $sql .= "      SELECT A.price";
        $sql .= "            ,A.mst_facility_id";
        $sql .= "            ,A.mst_facility_item_id";
        $sql .= "            ,B.work_month";
        $sql .= "        FROM trn_appraised_values A";
        $sql .= "             INNER JOIN (";
        $sql .= "                   SELECT MAX(id) AS id";
        $sql .= "                         ,to_char(X.work_date,'YYYYMM') as work_month ";
        $sql .= "                         ,mst_facility_id";
        $sql .= "                         ,mst_facility_item_id";
        $sql .= "                     FROM trn_appraised_values X";
        $sql .= "                    WHERE is_deleted = false";
        $sql .= "                      AND mst_facility_id = ".$this->Session->read('Auth.facility_id_selected');
        $sql .= $inner_where;
        $sql .= "                    GROUP BY ";
        $sql .= "                          mst_facility_id,mst_facility_item_id,to_char(X.work_date,'YYYYMM')";
        $sql .= "                   ) AS B";
        $sql .= "                ON A.id = B.id";
        $sql .= "       WHERE A.is_deleted = false";
        $sql .= "      ) APVL";
        $sql .= " ON FCLT.id = APVL.mst_facility_id";
        $sql .= " AND FCIT.id = APVL.mst_facility_item_id";
        $sql .= " AND APVL.work_month = to_char(CNSM.work_date,'YYYYMM')";
        //WHERE句
        if($this->request->data['search']['deleted'] !== "1"){
            $sql .= " WHERE CNSM.is_deleted = false";
        }else{
            $sql .= " WHERE 1 = 1";
        }
        $sql .= " AND CNSM.use_type = ".Configure::read("UseType.adjustment");
        $sql .= $where;
        $sql .= " ORDER BY CNSM.work_no,CNSM.work_seq";

        return $sql;

    }


}
