<?php
class ClaimsOperationController extends AppController {

    /**
     * @var $name
     */
    var $name = 'ClaimsOperation';
    /**
     * @var array $uses
     */
    var $uses = array(
            'TrnClaim',
            'TrnClaimSpecifications',
            'MstFacility',
            'MstAccountlist',
            'MstCredit',
            'MstDepartment'
    );

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');
    /**
     * @var array $components
     */
    var $components = array('RequestHandler', 'DateUtil');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
    }

    public function index() {
        $this->setRoleFunction(61); //売上検索
        $this->redirect('search');
    }

    public function search() {

        if (   isset($this->request->data['inputPrice'])       && empty($this->request->data['inputPrice']) === false
            && isset($this->request->data['selectedHospital']) && empty($this->request->data['selectedHospital']) === false ){
          //POST　取得
          $inputPrice = $this->request->data['inputPrice'];
          $facility_id = $this->request->data['selectedHospital'];
          $this->regist_claim_specification($inputPrice, $facility_id);
          $this->set_displayItems();
        }else{
          $this->set_displayItems();
        }
    }

    function set_displayItems(){
        //POST　取得
        $facility_id = (isset($this->request->data['selectedHospital']) && empty($this->request->data['selectedHospital']) == false) ? $this->request->data['selectedHospital'] : 0;
        $purchaseDate = $this->get_purchaseDate();
        $paymentDueDate = date("Y-m-t", strtotime($purchaseDate." 1 Month"));
        $hospitalList = $this->search_hospitalList();
        $facilityInformation = $this->search_facilityInformation($facility_id);

        $selectedHospital = $facility_id;
        $maximumAmount = isset($facilityInformation) ? $facilityInformation['MstFacility']['zura_maximum_amount'] : 0;
        $commissionRate = isset($facilityInformation) ? $facilityInformation['MstFacility']['zura_commission_rate'] * 100 : 0;

        $this->set_6month_before_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_5month_before_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_4month_before_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_3month_before_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_2month_before_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_1month_before_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_present_month_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_1month_later_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_2month_later_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_3month_later_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_4month_later_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_5month_later_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->set_6month_later_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);

        $this->set('hospitalList', $hospitalList);
        $this->set('selectedHospital', $selectedHospital);
        $this->set('maximumAmount', $maximumAmount);
        $this->set('commissionRate', $commissionRate);
    }

    function regist_claim_specification($inputPrice, $facility_id){
        $purchaseDate = $this->get_purchaseDate();
        $closingDate = date("Y-m-t", strtotime($purchaseDate));
        $paymentDueDate = date("Y-m-t", strtotime($purchaseDate." 1 Month"));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);
        $carryOverInformation = $this->search_carry_overInformation($facility_id, $purchaseDate, $paymentDueDate);
        $tax_rate = $this->search_consumption_tax($paymentDueDate);
        $now = date('Y/m/d H:i:s');
        //データチェック
        if ($inputPrice === 0){ return; }
        if ($facility_id === 0){ return; }
        if (empty($purchaseDate)) { return; }
        if (empty($paymentDueDate)) { return; }
        if (isset($tax_rate) === false){ return; }

        //DB登録方法　判定
        $specification = $this->search_claim_specification($facility_id, $paymentDueDate);
        if (isset($claimsCalculation) === false || count($claimsCalculation) === 0){
          //当月購入商品なし、過去延伸商品有
          $extensionInformation = $this->search_extensionInformation($facility_id, $purchaseDate, $paymentDueDate);
          if (isset($specification) === false || count($specification) === 0){
            //挿入
            $this->insert_claims_specificaiton_($facility_id, $inputPrice, $tax_rate, $carryOverInformation, $extensionInformation, $closingDate, $paymentDueDate, $now);
          }else{
            //更新
            $this->update_claims_specificaiton_($specification, $inputPrice, $tax_rate, $carryOverInformation, $extensionInformation, $closingDate, $paymentDueDate, $now);
          }
        }else{
          //当月購入商品有
          if (isset($specification) === false || count($specification) === 0){
            //挿入
            $this->insert_claims_specificaiton($facility_id, $inputPrice, $tax_rate, $carryOverInformation, $claimsCalculation, $closingDate, $now);
          }else{
            //更新
            $this->update_claims_specificaiton($specification, $inputPrice, $tax_rate, $carryOverInformation, $claimsCalculation, $closingDate, $now);
          }
        }
    }

    /* 更新前　排他ロック */
    function rowLock_TrnClaimSpecification($id){
      $sql  = ' SELECT id';
      $sql .= '   FROM trn_claim_specifications';
      $sql .= '  WHERE id = '.$id;
      $sql .= '    FOR UPDATE';

      $this->TrnClaim->query($sql);
    }

    function insert_claims_specificaiton($facility_id, $inputPrice, $tax_rate, $carryOverInformation, $claimsCalculation, $closingDate, $now){
        //入力実績　取得
        $payment_track_record        = $inputPrice;
        $payment_track_record_in_tax = floor($inputPrice + ($inputPrice * $tax_rate));
        //先月繰越情報　取得
        $last_month_carry_over = 0;
        $last_month_carry_over_in_tax = 0;
        $this->get_carryOverData($carryOverInformation, &$last_month_carry_over, &$last_month_carry_over_in_tax);
        //今月繰越情報　取得
        $carryOver        = $claimsCalculation['ClaimCalculation']['payment_price'] + $claimsCalculation['ClaimCalculation']['present_month_extension_price'] - $payment_track_record;
        $carryOver_in_tax = $claimsCalculation['ClaimCalculation']['payment_total'] + $claimsCalculation['ClaimCalculation']['present_month_extension_price'] + $claimsCalculation['ClaimCalculation']['present_month_extension_tax'] - $payment_track_record_in_tax;
        //料金情報　取得
        $purchase_price    = $claimsCalculation['ClaimCalculation']['present_month_not_extension_price'] + $claimsCalculation['ClaimCalculation']['present_month_commission'];
        $purchase_in_tax   = $claimsCalculation['ClaimCalculation']['present_month_not_extension_price'] + $claimsCalculation['ClaimCalculation']['present_month_not_extension_tax']
                             + $claimsCalculation['ClaimCalculation']['present_month_commission'] + $claimsCalculation['ClaimCalculation']['present_month_commission_tax'];
        $commistion        = $claimsCalculation['ClaimCalculation']['present_month_commission'];
        $commission_in_tax = $claimsCalculation['ClaimCalculation']['present_month_commission'] + $claimsCalculation['ClaimCalculation']['present_month_commission_tax'];
        $extenstion_price  = $claimsCalculation['ClaimCalculation']['all_month_before_price'];
        $extension_in_tax  = $claimsCalculation['ClaimCalculation']['all_month_before_price'] + $claimsCalculation['ClaimCalculation']['all_month_before_tax'];
        $total_tax         = $claimsCalculation['ClaimCalculation']['present_month_not_extension_tax'] + $claimsCalculation['ClaimCalculation']['all_month_before_tax'] + $claimsCalculation['ClaimCalculation']['present_month_commission_tax'];


        $this->TrnClaimSpecifications->create();
        $this->TrnClaimSpecifications->begin();
        try{
          $result = $this->TrnClaimSpecifications->save(array(
            'mst_facility_id'              => $facility_id,
            'closing_date'                 => $closingDate,
            'deposit_plans_date'           => $claimsCalculation['ClaimCalculation']['billing_date'],
            'last_month_carry_over'        => $last_month_carry_over,
            'last_month_carry_over_in_tax' => $last_month_carry_over_in_tax,
            'carry_over'                   => $carryOver,
            'carry_over_in_tax'            => $carryOver_in_tax,
            'purchase_price'               => $purchase_price,
            'purchase_in_tax'              => $purchase_in_tax,
            'commission'                   => $commistion,
            'commission_in_tax'            => $commission_in_tax,
            'extension_price'              => $extenstion_price,
            'extension_in_tax'             => $extension_in_tax,
            'total_tax'                    => $total_tax,
            'payment_track_record'         => $payment_track_record,
            'payment_track_record_in_tax'  => $payment_track_record_in_tax,
            'is_deleted'                   => false,
            'created'                      => $now,
            'modified'                     => $now,
            'creater'                      => $this->Session->read('Auth.MstUser.id'),
            'modifier'                     => $this->Session->read('Auth.MstUser.id')
          ));

          $this->TrnClaimSpecifications->commit();
        }catch(Exception $e){
          $this->TrnClaimSpecifications->rollback();
        }
    }

    function insert_claims_specificaiton_($facility_id, $inputPrice, $tax_rate, $carryOverInformation, $extensionInformation, $closingDate, $paymentDueDate, $now){
        //入力実績　取得
        $payment_track_record        = $inputPrice;
        $payment_track_record_in_tax = floor($inputPrice + ($inputPrice * $tax_rate));
        //先月繰越情報　取得
        $last_month_carry_over        = 0;
        $last_month_carry_over_in_tax = 0;
        $this->get_carryOverData($carryOverInformation, &$last_month_carry_over, &$last_month_carry_over_in_tax);
        //今月繰越情報　取得
        $carryOver        = $last_month_carry_over - $payment_track_record;
        $carryOver_in_tax = $last_month_carry_over_in_tax - $last_month_carry_over_in_tax;
        //当月に対して延伸している商品　取得
        $extension_price  = 0;
        $extension_tax    = 0;
        $extension_in_tax = 0;
        $this->get_accountsReceivableData($extensionInformation, &$extension_price, &$extension_tax, &$extension_in_tax);
        //料金情報　取得
        $purchase_price    = 0;
        $purchase_in_tax   = 0;
        $commission        = 0;
        $commission_in_tax = 0;
        $total_tax         = $extension_tax;

        $this->TrnClaimSpecifications->create();
        $this->TrnClaimSpecifications->begin();
        try{
          $result = $this->TrnClaimSpecifications->save(array(
            'mst_facility_id'              => $facility_id,
            'closing_date'                 => $closingDate,
            'deposit_plans_date'           => $paymentDueDate,
            'last_month_carry_over'        => $last_month_carry_over,
            'last_month_carry_over_in_tax' => $last_month_carry_over_in_tax,
            'carry_over'                   => $carryOver,
            'carry_over_in_tax'            => $carryOver_in_tax,
            'purchase_price'               => $purchase_price,
            'purchase_in_tax'              => $purchase_in_tax,
            'commission'                   => $commission,
            'commission_in_tax'            => $commission_in_tax,
            'extension_price'              => $extension_price,
            'extension_in_tax'             => $extension_in_tax,
            'total_tax'                    => $total_tax,
            'payment_track_record'         => $payment_track_record,
            'payment_track_record_in_tax'  => $payment_track_record_in_tax,
            'is_deleted'                   => false,
            'created'                      => $now,
            'modified'                     => $now,
            'creater'                      => $this->Session->read('Auth.MstUser.id'),
            'modifier'                     => $this->Session->read('Auth.MstUser.id')
          ));

          $this->TrnClaimSpecifications->commit();
        }catch(Exception $e){
          $this->TrnClaimSpecifications->rollback();
        }
    }

    function update_claims_specificaiton($specification, $inputPrice, $tax_rate, $carryOverInformation, $claimsCalculation, $closingDate, $now){
        //入力実績　取得
        $payment_track_record        = $inputPrice;
        $payment_track_record_in_tax = floor($inputPrice + ($inputPrice * $tax_rate));
        //先月繰越情報　取得
        $last_month_carry_over = 0;
        $last_month_carry_over_in_tax = 0;
        $this->get_carryOverData($carryOverInformation, &$last_month_carry_over, &$last_month_carry_over_in_tax);
        //今月繰越情報　取得
        $carryOver        = $claimsCalculation['ClaimCalculation']['payment_price'] + $claimsCalculation['ClaimCalculation']['present_month_extension_price'] - $payment_track_record;
        $carryOver_in_tax = $claimsCalculation['ClaimCalculation']['payment_total'] + $claimsCalculation['ClaimCalculation']['present_month_extension_price'] + $claimsCalculation['ClaimCalculation']['present_month_extension_tax'] - $payment_track_record_in_tax;
        //料金情報　取得
        $purchase_price    = $claimsCalculation['ClaimCalculation']['present_month_not_extension_price'] + $claimsCalculation['ClaimCalculation']['present_month_commission'];
        $purchase_in_tax   = $claimsCalculation['ClaimCalculation']['present_month_not_extension_price'] + $claimsCalculation['ClaimCalculation']['present_month_not_extension_tax']
                             + $claimsCalculation['ClaimCalculation']['present_month_commission'] + $claimsCalculation['ClaimCalculation']['present_month_commission_tax'];
        $commistion        = $claimsCalculation['ClaimCalculation']['present_month_commission'];
        $commission_in_tax = $claimsCalculation['ClaimCalculation']['present_month_commission'] + $claimsCalculation['ClaimCalculation']['present_month_commission_tax'];
        $extenstion_price  = $claimsCalculation['ClaimCalculation']['all_month_before_price'];
        $extension_in_tax  = $claimsCalculation['ClaimCalculation']['all_month_before_price'] + $claimsCalculation['ClaimCalculation']['all_month_before_tax'];
        $total_tax         = $claimsCalculation['ClaimCalculation']['present_month_not_extension_tax'] + $claimsCalculation['ClaimCalculation']['all_month_before_tax'] + $claimsCalculation['ClaimCalculation']['present_month_commission_tax'];

        $this->TrnClaimSpecifications->create();
        $this->TrnClaimSpecifications->begin();
        try{
          $this->rowLock_TrnClaimSpecification($specification['TrnClaimSpecification']['id']);

          $result = $this->TrnClaimSpecifications->save(array(
            'id'                           => $specification['TrnClaimSpecification']['id'],
            'closing_date'                 => $closingDate,
            'deposit_plans_date'           => $claimsCalculation['ClaimCalculation']['billing_date'],
            'last_month_carry_over'        => $last_month_carry_over,
            'last_month_carry_over_in_tax' => $last_month_carry_over_in_tax,
            'carry_over'                   => $carryOver,
            'carry_over_in_tax'            => $carryOver_in_tax,
            'purchase_price'               => $purchase_price,
            'purchase_in_tax'              => $purchase_in_tax,
            'commission'                   => $commistion,
            'commission_in_tax'            => $commission_in_tax,
            'extension_price'              => $extenstion_price,
            'extension_in_tax'             => $extension_in_tax,
            'total_tax'                    => $total_tax,
            'payment_track_record'         => $payment_track_record,
            'payment_track_record_in_tax'  => $payment_track_record_in_tax,
            'is_deleted'                   => false,
            'modified'                     => $now,
            'modifier'                     => $this->Session->read('Auth.MstUser.id')
          ));
          if($result){
            $this->TrnClaimSpecifications->commit();
          }else{
            $this->TrnClaimSpecifications->rollback();
          }
        }catch(Exception $e){
          $this->TrnClaimSpecifications->rollback();
        }
    }

    function update_claims_specificaiton_($specification, $inputPrice, $tax_rate, $carryOverInformation, $extensionInformation, $closingDate, $paymentDueDate, $now){
        //入力実績　取得
        $payment_track_record        = $inputPrice;
        $payment_track_record_in_tax = floor($inputPrice + ($inputPrice * $tax_rate));
        //先月繰越情報　取得
        $last_month_carry_over        = 0;
        $last_month_carry_over_in_tax = 0;
        $this->get_carryOverData($carryOverInformation, &$last_month_carry_over, &$last_month_carry_over_in_tax);
        //今月繰越情報　取得
        $carryOver        = $last_month_carry_over - $payment_track_record;
        $carryOver_in_tax = $last_month_carry_over_in_tax - $payment_track_record_in_tax;
        //当月に対して延伸している商品　取得
        $extension_price      = 0;
        $extension_tax        = 0;
        $extension_in_tax     = 0;
        $this->get_accountsReceivableData($extensionInformation, &$extension_price, &$extension_tax, &$extension_in_tax);
        //料金情報　取得
        $purchase_price    = 0;
        $purchase_in_tax   = 0;
        $commission        = 0;
        $commission_in_tax = 0;
        $total_tax         = $extension_tax;

        $this->TrnClaimSpecifications->create();
        $this->TrnClaimSpecifications->begin();
        try{
          $this->rowLock_TrnClaimSpecification($specification['TrnClaimSpecification']['id']);

          $result = $this->TrnClaimSpecifications->save(array(
            'id'                           => $specification['TrnClaimSpecification']['id'],
            'closing_date'                 => $closingDate,
            'deposit_plans_date'           => $paymentDueDate,
            'last_month_carry_over'        => $last_month_carry_over,
            'last_month_carry_over_in_tax' => $last_month_carry_over_in_tax,
            'carry_over'                   => $carryOver,
            'carry_over_in_tax'            => $carryOver_in_tax,
            'purchase_price'               => $purchase_price,
            'purchase_in_tax'              => $purchase_in_tax,
            'commission'                   => $commission,
            'commission_in_tax'            => $commission_in_tax,
            'extension_price'              => $extension_price,
            'extension_in_tax'             => $extension_in_tax,
            'total_tax'                    => $total_tax,
            'payment_track_record'         => $payment_track_record,
            'payment_track_record_in_tax'  => $payment_track_record_in_tax,
            'is_deleted'                   => false,
            'modified'                     => $now,
            'modifier'                     => $this->Session->read('Auth.MstUser.id')
          ));
          if($result){
            $this->TrnClaimSpecifications->commit();
          }else{
            $this->TrnClaimSpecifications->rollback();
          }
        }catch(Exception $e){
          $this->TrnClaimSpecifications->rollback();
        }
    }

    /* 購入日　取得 */
    function get_purchaseDate(){
      $now = date('Y-m-d 00:00:00');
      if(date('d', strtotime($now."")) > 10){
        return date('Y-m-01 00:00:00', strtotime($now.""));
      }else{
        return date('Y-m-01 00:00:00', strtotime($now." -1 Month"));
      }
    }

    function get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate){
        //前月繰越情報　取得
        $carryOver = 0;
        $carryOver_in_tax = 0;
        $carryOverInformation = $this->search_carry_overInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->get_carryOverData($carryOverInformation, &$carryOver, &$carryOver_in_tax);
        //当月に対して延伸している商品　取得
        $extensionPrice = 0;
        $extensionPrice_tax = 0;
        $extensionPrice_in_tax = 0;
        $extensionInformation = $this->search_extensionInformation($facility_id, $purchaseDate, $paymentDueDate);
        $this->get_accountsReceivableData($extensionInformation, &$extensionPrice, &$extensionPrice_tax, &$extensionPrice_in_tax);
        //請求明細取得
        $claimsSpecification = $this->search_claim_specification($facility_id, $paymentDueDate);
        if (count($claimsSpecification) === 0){
          $payment_track_record = 0;
          $payment_track_record_in_tax = 0;
        }else{
          $payment_track_record = intval($claimsSpecification['TrnClaimSpecification']['payment_track_record']);
          $payment_track_record_in_tax = $claimsSpecification['TrnClaimSpecification']['payment_track_record_in_tax'];
        }

        $facilityInformation = $this->search_facilityInformation($facility_id);
        $maximumAmount =  isset($facilityInformation) ? $facilityInformation['MstFacility']['zura_maximum_amount'] - $carryOver : 0;

        return Array(
          "paymentDueDate"              => date('Y/m/t', strtotime($paymentDueDate)),
          "carryOver"                   => $carryOver,
          "carryOver_in_tax"            => $carryOver_in_tax,
          "billing_price"               => "0",
          "billing_price_in_tax"        => "0",
          "accounts_receivable"         => $carryOver,
          "accounts_receivable_in_tax"  => $carryOver_in_tax,
          "payment_price"               => $extensionPrice,
          "payment_tax"                 => $extensionPrice_tax,
          "payment_price_in_tax"        => $extensionPrice_in_tax,
          "payment_track_record"        => $payment_track_record,
          "payment_track_record_in_tax" => $payment_track_record_in_tax,
          "not_payment"                 => $carryOver - $payment_track_record,
          "not_payment_in_tax"          => $carryOver_in_tax - $payment_track_record_in_tax,
          "maximum_amount"              => $maximumAmount
        );
    }

    function get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation){
        $carryOver = 0;
        $carryOver_in_tax = 0;

        var_dump($purchaseDate);

        //施設情報取得
        $facilityInformation = $this->search_facilityInformation($facility_id);
        $maximumAmount = isset($facilityInformation) ? $facilityInformation['MstFacility']['zura_maximum_amount'] : 0;
        //繰越情報取得
        $carryOverInformation = $this->search_carry_overInformation($facility_id, $purchaseDate, $paymentDueDate);
        if (count($carryOverInformation) !== 0){
          $tmpPrice = 0;
          $tmpTax = 0;
          foreach($carryOverInformation AS $carryOver){
            $tmpPrice += $carryOver['TrnClaim']['carry_over_price'];
            $tmpTax += $carryOver['TrnClaim']['carry_over_tax'];
          }
          $carryOver = $tmpPrice;
          $carryOver_in_tax = $tmpPrice + $tmpTax;
        }
        //請求明細取得
        $claimsSpecification = $this->search_claim_specification($facility_id, $paymentDueDate);
        if (count($claimsSpecification) === 0){
          $payment_track_record = 0;
          $payment_track_record_in_tax = 0;
          $payment_extension_price = 0;
        }else{
          $payment_track_record = intval($claimsSpecification['TrnClaimSpecification']['payment_track_record']);
          $payment_track_record_in_tax = $claimsSpecification['TrnClaimSpecification']['payment_track_record_in_tax'];
          $payment_extension_price = $payment_track_record < $claimsCalculation['ClaimCalculation']['all_month_before_price'] ? 0 : $claimsCalculation['ClaimCalculation']['all_month_before_price'];
        }

        $present_month_in_tax = $claimsCalculation['ClaimCalculation']['present_month_price']
                                + $claimsCalculation['ClaimCalculation']['present_month_tax'];
        $accounts_receivable = $carryOver
                               + $claimsCalculation['ClaimCalculation']['present_month_price'];
        $accounts_receivable_in_tax = $carryOver_in_tax
                                      + $present_month_in_tax;
        $not_payment_price = $carryOver
                            + $claimsCalculation['ClaimCalculation']['payment_price']
                            + $claimsCalculation['ClaimCalculation']['present_month_extension_price']
                            - $payment_track_record
                            - $claimsCalculation['ClaimCalculation']['all_month_before_price'];
        $not_payment_in_tax = $carryOver_in_tax
                              + $claimsCalculation['ClaimCalculation']['payment_total']
                              + $claimsCalculation['ClaimCalculation']['present_month_extension_price']
                              + $claimsCalculation['ClaimCalculation']['present_month_extension_tax']
                              - $payment_track_record_in_tax
                              - $claimsCalculation['ClaimCalculation']['all_month_before_price']
                              - $claimsCalculation['ClaimCalculation']['all_month_before_tax'];
        $maximumAmount = $maximumAmount
                         + $payment_extension_price
                         - ($carryOver +  $claimsCalculation['ClaimCalculation']['present_month_extension_price']);

        return Array(
          "paymentDueDate"=>date('Y/m/t', strtotime($claimsCalculation['ClaimCalculation']['billing_date'])),
          "carryOver"=>$carryOver,
          "carryOver_in_tax"=>$carryOver_in_tax,
          "billing_price"=>$claimsCalculation['ClaimCalculation']['present_month_price'],
          "billing_price_in_tax"=>$present_month_in_tax,
          "accounts_receivable"=>$accounts_receivable,
          "accounts_receivable_in_tax"=>$accounts_receivable_in_tax,
          "payment_price"=>$claimsCalculation['ClaimCalculation']['payment_price'],
          "payment_tax"=>$claimsCalculation['ClaimCalculation']['payment_tax'],
          "payment_price_in_tax"=>$claimsCalculation['ClaimCalculation']['payment_total'],
          "payment_track_record"=>$payment_track_record,
          "payment_track_record_in_tax"=>$payment_track_record_in_tax,
          "not_payment"=>$not_payment_price,
          "not_payment_in_tax"=>$not_payment_in_tax,
          "maximum_amount"=>$maximumAmount
        );
    }

    function get_carryOverData($carryOverInformation, &$carryOver, &$carryOver_in_tax){
      if (count($carryOverInformation) !== 0){
        $tmpPrice = 0;
        $tmpTax = 0;
        foreach($carryOverInformation AS $carryOver){
          $tmpPrice += $carryOver['TrnClaim']['carry_over_price'];
          $tmpTax += $carryOver['TrnClaim']['carry_over_tax'];
        }
        $carryOver = $tmpPrice;
        $carryOver_in_tax = $tmpPrice + $tmpTax;
      }
    }

    function get_accountsReceivableData($extensionInformation, &$extensionPrice, &$extensionPrice_tax, &$extensionPrice_in_tax){
      if (count($extensionInformation) !== 0){
        $tmpPrice = 0;
        $tmpTax = 0;
        foreach($extensionInformation AS $extension){
          $tmpPrice += $extension['TrnClaim']['extension_price'];
          $tmpTax += $extension['TrnClaim']['extension_tax'];
        }
        $extensionPrice = $tmpPrice;
        $extensionPrice_tax = $tmpTax;
        $extensionPrice_in_tax = $tmpPrice + $tmpTax;
      }
    }

    function search_hospitalList(){
        $sql  = ' SELECT id AS "MstFacility__id" ';
        $sql .= '       ,facility_name AS "MstFacility__facility_name" ';
        $sql .= '   FROM mst_facilities';
        $sql .= '  WHERE is_deleted = false';
        $sql .= '    AND facility_type = 2';
        $result = $this->MstFacility->query($sql);

        $hospitalList = array(""=>"");

        foreach($result AS $facility){
          $key = $facility['MstFacility']['id'];
          $value = $facility['MstFacility']['facility_name'];
          $hospitalList[$key] = $value;
        }

        return $hospitalList;
    }

    function search_facilityInformation($facility_id){
        $sql  = ' SELECT zura_commission_rate AS "MstFacility__zura_commission_rate" ';
        $sql .= '       ,zura_maximum_amount AS "MstFacility__zura_maximum_amount" ';
        $sql .= '   FROM mst_facilities';
        $sql .= '  WHERE is_deleted = false';
        $sql .= '    AND id = '.$facility_id;

        $result = $this->MstFacility->query($sql);

        if(isset($result) && isset($result[0])){
          return $result[0];
        }
        else{
          return null;
        }
    }

    function search_claims_calculation_specification($facility_id, $paymentDueDate){
        $sql  = ' SELECT purchase_date AS "ClaimCalculation__purchase_date" ';
        $sql .= '       ,billing_date AS "ClaimCalculation__billing_date" ';
        $sql .= '       ,present_month_price AS "ClaimCalculation__present_month_price" ';
        $sql .= '       ,present_month_tax AS "ClaimCalculation__present_month_tax" ';
        $sql .= '       ,present_month_commission AS "ClaimCalculation__present_month_commission" ';
        $sql .= '       ,present_month_commission_tax AS "ClaimCalculation__present_month_commission_tax" ';
        $sql .= '       ,payment_total AS "ClaimCalculation__payment_total" ';
        $sql .= '       ,payment_price AS "ClaimCalculation__payment_price" ';
        $sql .= '       ,payment_tax AS "ClaimCalculation__payment_tax" ';
        $sql .= '       ,present_month_extension_price AS "ClaimCalculation__present_month_extension_price" ';
        $sql .= '       ,present_month_extension_tax AS "ClaimCalculation__present_month_extension_tax" ';
        $sql .= '       ,present_month_not_extension_price AS "ClaimCalculation__present_month_not_extension_price" ';
        $sql .= '       ,present_month_not_extension_tax AS "ClaimCalculation__present_month_not_extension_tax" ';
        $sql .= '       ,all_month_before_price AS "ClaimCalculation__all_month_before_price"';
        $sql .= '       ,all_month_before_tax AS "ClaimCalculation__all_month_before_tax"';
        $sql .= '   FROM v_claims_calculation_specification ';
        $sql .= '  WHERE facility_id = '.$facility_id;
        $sql .= "    AND billing_date = '".$paymentDueDate."'";

        $result = $this->TrnClaim->query($sql);

        if(isset($result) && isset($result[0])){
          return $result[0];
        }
        else{
          return null;
        }
    }

    function search_claim_specification($facility_id, $paymentDueDate){
        $sql  = ' SELECT id AS "TrnClaimSpecification__id" ';
        $sql .= '       ,mst_facility_id AS "TrnClaimSpecification__purchase_date" ';
        $sql .= '       ,deposit_plans_date AS "TrnClaimSpecification__deposit_plans_date" ';
        $sql .= '       ,last_month_carry_over AS "TrnClaimSpecification__last_month_carry_over" ';
        $sql .= '       ,last_month_carry_over_in_tax AS "TrnClaimSpecification__payment_last_month_carry_over_in_tax" ';
        $sql .= '       ,carry_over AS "TrnClaimSpecification__carry_over" ';
        $sql .= '       ,carry_over_in_tax AS "TrnClaimSpecification__carry_over_in_tax" ';
        $sql .= '       ,purchase_price AS "TrnClaimSpecification__purchase_price" ';
        $sql .= '       ,purchase_in_tax AS "TrnClaimSpecification__purchase_in_tax　" ';
        $sql .= '       ,commission AS "TrnClaimSpecification__commission" ';
        $sql .= '       ,commission_in_tax AS "TrnClaimSpecification__commission_in_tax" ';
        $sql .= '       ,extension_price AS "TrnClaimSpecification__extension_price" ';
        $sql .= '       ,extension_in_tax AS "TrnClaimSpecification__extension_in_tax" ';
        $sql .= '       ,total_tax AS "TrnClaimSpecification__payment_tax" ';
        $sql .= '       ,payment_track_record AS "TrnClaimSpecification__payment_track_record" ';
        $sql .= '       ,payment_track_record_in_tax AS "TrnClaimSpecification__payment_track_record_in_tax" ';
        $sql .= '   FROM trn_claim_specifications';
        $sql .= '  WHERE is_deleted = false';
        $sql .= '    AND mst_facility_id = '.$facility_id;
        $sql .= "    AND deposit_plans_date = '".$paymentDueDate."'";

        $result = $this->TrnClaim->query($sql);
        if(isset($result) && isset($result[0])){
          return $result[0];
        }
        else{
          return null;
        }
    }

    function search_carry_overInformation($facility_id, $purchaseDate, $paymentDueDate){
      $sql  = ' SELECT trunc(sum(claims.claim_price), 0) AS "TrnClaim__carry_over_price" ';
      $sql .= '       ,trunc(sum(claims.claim_price) * tax.tax_rate, 0) AS "TrnClaim__carry_over_tax" ';
      $sql .= '   FROM trn_claims AS claims';
      $sql .= '  INNER JOIN (SELECT tax_rate';
      $sql .= '                FROM mst_consumption_tax';
      $sql .= "               WHERE application_start_date < '".$paymentDueDate."'";
      $sql .= '               ORDER BY application_start_date DESC';
      $sql .= '               LIMIT 1) AS tax';
      $sql .= '     ON TRUE';
      $sql .= '  INNER JOIN mst_departments AS department';
      $sql .= '     ON claims.department_id_to = department.id';
      $sql .= '  INNER JOIN mst_facilities AS facility';
      $sql .= '     ON facility.id = department.mst_facility_id';
      $sql .= '  WHERE facility.id = '.$facility_id;
      $sql .= '    AND claims.is_deleted = false';
      $sql .= "    AND (    (claims.zura_number_of_month > 0 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-1 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month > 1 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-2 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-1 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month > 2 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-3 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-2 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month > 3 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-4 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-3 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month > 4 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-5 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-4 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month > 5 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-6 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-5 mons' + '-1 Day')";
      $sql .= "        )";
      $sql .= '  GROUP BY claims.claim_price';
      $sql .= '       ,tax.tax_rate';

      return $this->TrnClaim->query($sql);
    }

    function search_extensionInformation($facility_id, $purchaseDate, $paymentDueDate){
      $sql  = ' SELECT trunc(sum(claims.claim_price), 0) AS "TrnClaim__extension_price" ';
      $sql .= '       ,trunc(sum(claims.claim_price) * tax.tax_rate, 0) AS "TrnClaim__extension_tax" ';
      $sql .= '   FROM trn_claims AS claims';
      $sql .= '  INNER JOIN (SELECT tax_rate';
      $sql .= '                FROM mst_consumption_tax';
      $sql .= "               WHERE application_start_date < '".$paymentDueDate."'";
      $sql .= '               ORDER BY application_start_date DESC';
      $sql .= '               LIMIT 1) AS tax';
      $sql .= '     ON TRUE';
      $sql .= '  INNER JOIN mst_departments AS department';
      $sql .= '     ON claims.department_id_to = department.id';
      $sql .= '  INNER JOIN mst_facilities AS facility';
      $sql .= '     ON facility.id = department.mst_facility_id';
      $sql .= '  WHERE facility.id = '.$facility_id;
      $sql .= '    AND claims.is_deleted = false';
      $sql .= "    AND (    (claims.zura_number_of_month = 1 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-1 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month = 2 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-2 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-1 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month = 3 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-3 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-2 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month = 4 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-4 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-3 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month = 5 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-5 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-4 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month = 6 AND claims.claim_date BETWEEN date_trunc('month', '".$purchaseDate."' ::date) + '-6 mons' AND date_trunc('month', '".$purchaseDate."' ::date) + '-5 mons' + '-1 Day')";
      $sql .= "        )";
      $sql .= '  GROUP BY claims.claim_price';
      $sql .= '       ,tax.tax_rate';

      return $this->TrnClaim->query($sql);
    }

    function search_consumption_tax($paymentDueDate){
      $sql  = ' SELECT tax_rate';
      $sql .= '   FROM mst_consumption_tax';
      $sql .= "  WHERE application_start_date <= '".$paymentDueDate."'";
      $sql .= '  ORDER BY application_start_date DESC';
      $sql .= '  LIMIT 1';

      $result = $this->TrnClaim->query($sql);

      if(isset($result) && isset($result[0]) && isset($result[0][0])){
        return $result[0][0]['tax_rate'];
      }
      else{
        return null;
      }
    }

    function set_6month_before_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' -6 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' -6 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_6month_before_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_6month_before_carryOver', $claimsInformation['carryOver']);
        $this->set('_6month_before_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_6month_before_billing_price', $claimsInformation['billing_price']);
        $this->set('_6month_before_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_6month_before_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_6month_before_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_6month_before_payment_price', $claimsInformation['payment_price']);
        $this->set('_6month_before_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_6month_before_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_6month_before_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_6month_before_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_6month_before_not_payment', $claimsInformation['not_payment']);
        $this->set('_6month_before_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_6month_before_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_5month_before_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' -5 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' -5 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_5month_before_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_5month_before_carryOver', $claimsInformation['carryOver']);
        $this->set('_5month_before_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_5month_before_billing_price', $claimsInformation['billing_price']);
        $this->set('_5month_before_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_5month_before_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_5month_before_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_5month_before_payment_price', $claimsInformation['payment_price']);
        $this->set('_5month_before_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_5month_before_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_5month_before_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_5month_before_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_5month_before_not_payment', $claimsInformation['not_payment']);
        $this->set('_5month_before_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_5month_before_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_4month_before_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' -4 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' -4 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_4month_before_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_4month_before_carryOver', $claimsInformation['carryOver']);
        $this->set('_4month_before_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_4month_before_billing_price', $claimsInformation['billing_price']);
        $this->set('_4month_before_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_4month_before_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_4month_before_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_4month_before_payment_price', $claimsInformation['payment_price']);
        $this->set('_4month_before_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_4month_before_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_4month_before_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_4month_before_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_4month_before_not_payment', $claimsInformation['not_payment']);
        $this->set('_4month_before_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_4month_before_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_3month_before_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' -3 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' -3 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_3month_before_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_3month_before_carryOver', $claimsInformation['carryOver']);
        $this->set('_3month_before_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_3month_before_billing_price', $claimsInformation['billing_price']);
        $this->set('_3month_before_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_3month_before_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_3month_before_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_3month_before_payment_price', $claimsInformation['payment_price']);
        $this->set('_3month_before_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_3month_before_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_3month_before_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_3month_before_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_3month_before_not_payment', $claimsInformation['not_payment']);
        $this->set('_3month_before_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_3month_before_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_2month_before_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' -2 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' -2 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_2month_before_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_2month_before_carryOver', $claimsInformation['carryOver']);
        $this->set('_2month_before_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_2month_before_billing_price', $claimsInformation['billing_price']);
        $this->set('_2month_before_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_2month_before_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_2month_before_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_2month_before_payment_price', $claimsInformation['payment_price']);
        $this->set('_2month_before_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_2month_before_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_2month_before_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_2month_before_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_2month_before_not_payment', $claimsInformation['not_payment']);
        $this->set('_2month_before_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_2month_before_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_1month_before_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' -1 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' -1 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_1month_before_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_1month_before_carryOver', $claimsInformation['carryOver']);
        $this->set('_1month_before_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_1month_before_billing_price', $claimsInformation['billing_price']);
        $this->set('_1month_before_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_1month_before_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_1month_before_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_1month_before_payment_price', $claimsInformation['payment_price']);
        $this->set('_1month_before_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_1month_before_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_1month_before_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_1month_before_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_1month_before_not_payment', $claimsInformation['not_payment']);
        $this->set('_1month_before_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_1month_before_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_present_month_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime($referencePurchaseDate));
        $paymentDueDate = date('Y-m-t', strtotime($referencePaymentDueDate));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('present_month_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('present_month_carryOver', $claimsInformation['carryOver']);
        $this->set('present_month_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('present_month_billing_price', $claimsInformation['billing_price']);
        $this->set('present_month_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('present_month_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('present_month_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('present_month_payment_price', $claimsInformation['payment_price']);
        $this->set('present_month_payment_tax', $claimsInformation['payment_tax']);
        $this->set('present_month_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('present_month_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('present_month_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('present_month_not_payment', $claimsInformation['not_payment']);
        $this->set('present_month_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('present_month_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_1month_later_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' 1 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' 1 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_1month_later_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_1month_later_carryOver', $claimsInformation['carryOver']);
        $this->set('_1month_later_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_1month_later_billing_price', $claimsInformation['billing_price']);
        $this->set('_1month_later_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_1month_later_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_1month_later_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_1month_later_payment_price', $claimsInformation['payment_price']);
        $this->set('_1month_later_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_1month_later_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_1month_later_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_1month_later_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_1month_later_not_payment', $claimsInformation['not_payment']);
        $this->set('_1month_later_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_1month_later_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_2month_later_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' 2 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' 2 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_2month_later_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_2month_later_carryOver', $claimsInformation['carryOver']);
        $this->set('_2month_later_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_2month_later_billing_price', $claimsInformation['billing_price']);
        $this->set('_2month_later_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_2month_later_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_2month_later_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_2month_later_payment_price', $claimsInformation['payment_price']);
        $this->set('_2month_later_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_2month_later_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_2month_later_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_2month_later_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_2month_later_not_payment', $claimsInformation['not_payment']);
        $this->set('_2month_later_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_2month_later_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_3month_later_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' 3 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' 3 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_3month_later_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_3month_later_carryOver', $claimsInformation['carryOver']);
        $this->set('_3month_later_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_3month_later_billing_price', $claimsInformation['billing_price']);
        $this->set('_3month_later_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_3month_later_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_3month_later_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_3month_later_payment_price', $claimsInformation['payment_price']);
        $this->set('_3month_later_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_3month_later_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_3month_later_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_3month_later_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_3month_later_not_payment', $claimsInformation['not_payment']);
        $this->set('_3month_later_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_3month_later_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_4month_later_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' 4 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' 4 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_4month_later_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_4month_later_carryOver', $claimsInformation['carryOver']);
        $this->set('_4month_later_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_4month_later_billing_price', $claimsInformation['billing_price']);
        $this->set('_4month_later_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_4month_later_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_4month_later_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_4month_later_payment_price', $claimsInformation['payment_price']);
        $this->set('_4month_later_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_4month_later_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_4month_later_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_4month_later_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_4month_later_not_payment', $claimsInformation['not_payment']);
        $this->set('_4month_later_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_4month_later_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_5month_later_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' 5 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' 5 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_5month_later_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_5month_later_carryOver', $claimsInformation['carryOver']);
        $this->set('_5month_later_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_5month_later_billing_price', $claimsInformation['billing_price']);
        $this->set('_5month_later_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_5month_later_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_5month_later_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_5month_later_payment_price', $claimsInformation['payment_price']);
        $this->set('_5month_later_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_5month_later_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_5month_later_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_5month_later_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_5month_later_not_payment', $claimsInformation['not_payment']);
        $this->set('_5month_later_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_5month_later_maximum_amount', $claimsInformation['maximum_amount']);
    }

    function set_6month_later_claimsInformation($facility_id, $referencePurchaseDate, $referencePaymentDueDate){
        $purchaseDate = date('Y-m-01', strtotime(date('Y-m-01', strtotime($referencePurchaseDate)).' 6 month'));
        $paymentDueDate = date('Y-m-t', strtotime(date('Y-m-01', strtotime($referencePaymentDueDate)).' 6 month'));
        $claimsCalculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

        if(count($claimsCalculation) === 0){
          $claimsInformation = $this->get_default_claimsInformation($facility_id, $purchaseDate, $paymentDueDate);
        }else{
          $claimsInformation = $this->get_claimsInformation($facility_id, $purchaseDate, $paymentDueDate, $claimsCalculation);
        }

        $this->set('_6month_later_paymentDueDate', $claimsInformation['paymentDueDate']);
        $this->set('_6month_later_carryOver', $claimsInformation['carryOver']);
        $this->set('_6month_later_carryOver_in_tax', $claimsInformation['carryOver_in_tax']);
        $this->set('_6month_later_billing_price', $claimsInformation['billing_price']);
        $this->set('_6month_later_billing_price_in_tax', $claimsInformation['billing_price_in_tax']);
        $this->set('_6month_later_accounts_receivable', $claimsInformation['accounts_receivable']);
        $this->set('_6month_later_accounts_receivable_in_tax', $claimsInformation['accounts_receivable_in_tax']);
        $this->set('_6month_later_payment_price', $claimsInformation['payment_price']);
        $this->set('_6month_later_payment_tax', $claimsInformation['payment_tax']);
        $this->set('_6month_later_payment_price_in_tax', $claimsInformation['payment_price_in_tax']);
        $this->set('_6month_later_payment_track_record', $claimsInformation['payment_track_record']);
        $this->set('_6month_later_payment_track_record_in_tax', $claimsInformation['payment_track_record_in_tax']);
        $this->set('_6month_later_not_payment', $claimsInformation['not_payment']);
        $this->set('_6month_later_not_payment_in_tax', $claimsInformation['not_payment_in_tax']);
        $this->set('_6month_later_maximum_amount', $claimsInformation['maximum_amount']);
    }

}
