<?php
/**
 * MstFacilityItemsController
 *　マスタ管理系　採用品
 * @version 1.0.0
 * @since 2010/05/18
 */
class MstFacilityItemsController extends AppController {

    /**
     * @var $name
     */
    var $name = 'MstFacilityItems';

    /**
     * @var array $uses
     */
    var $uses = array(
        'MstFacilityItem',
        'MstFacilityRelation',
        'MstUnitName',
        'MstInsuranceClaim',
        'MstItemUnit',
        'MstGrandmaster',
        'MstGrandKyoMaterial',
        'MstGrandMatPrice',
        'MstGrandUnit',
        'MstGrandMktCo',
        'MstInsAppr',
        'MstMacmap',
        'MstRezCdRelation',
        'MstDepClCdRelation',
        'MstDepClCd',
        'MstDealer',
        'MstJmdnName',
        'MstItemCategory',
        'MstClassSeparation',
        'MstFacility',
        'MstConst',
        'Owner',
        'MstFixedCount',
        'MstShelfName',
        'MstTransactionConfig',
        'MstTransactionMain',
        'MstSalesConfig',
        'MstDepartment',
        'TrnStock',
        'TrnSticker',
        'MstAccountlist',
        'MstNumber',
        'MstSetDetail',
        'MstFavoriteGroup',
        'MstMedicalCode',
        'MstSameItem',
        'MstItemStandard',
        'TrnRfpHeader',
        'TrnRfp',
        'TrnRequestedRfp',
        'TransactionManager',
        );
    
    /**
     * @var bool $scaffold
     */
    // var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'Common');

    /**
     * @var array $components
     */
    var $components = array(
        'RequestHandler',
        'Common',
        'Facilityitem',
        'utils',
        'Stickers',
        'CsvWriteUtils',
        'CsvReadUtils',
        'Rfp',
        'Mail',
        );

    /**
     *
     * @var array $errors
     */
    var $errors;

    /**
     *
     * @var array $successes
     */
    var $successes;

    var $isDebug = true;
    var $linkString = "";

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';
        // CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        // CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }
    }

    /**
     *
     */
    function index (){
        $this->search();
    }

    /**
     * CSVダウンロード
     */
    function export_csv(){
        switch($this->request->data['csv']['type']){
          case 1:
            // 施設採用品
            $sql  = 'select ';
            $sql .= '      a.id                                    as "ＩＤ"'; // 半角だと、CSVを直に開く場合EXCELでエラーが出るので。。
            $sql .= '    , b.facility_code                         as 施設コード ';
            $sql .= '    , a.internal_code                         as "商品ID" ';
            $sql .= '    , a.item_name                             as 商品名 ';
            $sql .= '    , a.standard                              as 規格 ';
            $sql .= '    , a.item_code                             as 製品番号 ';
            $sql .= '    , c.dealer_code                           as 販売元コード ';
            $sql .= '    , c.dealer_name                           as 販売元名 ';
            $sql .= '    , a.jan_code                              as janコード ';
            $sql .= '    , a.old_jan_code                          as 旧janコード ';
            $sql .= '    , f.internal_code                         as 基本単位コード ';
            $sql .= '    , a.per_unit                              as 基本単位入数';
            $sql .= '    , a.refund_price                          as 償還価格 ';
            $sql .= '    , a.price                                 as 定価 ';
            $sql .= '    , a.unit_price                            as 定価単価 ';
            $sql .= '    , a.tax_type                              as 課税区分 ';
            $sql .= '    , h.insurance_claim_code                  as 保険請求コード ';
            $sql .= '    , g.const_nm                              as 保険請求区分 ';
            $sql .= '    , a.medical_code                          as 医事コード';
            $sql .= '    , a.jmdn_code                             as "JMDNコード" ';
            $sql .= '    , a.class_separation                      as クラス分類 ';
            $sql .= '    , i1.category_name                        as 大分類 ';
            $sql .= '    , i2.category_name                        as 中分類 ';
            $sql .= '    , i3.category_name                        as 小分類 ';
            $sql .= '    , a.parts_use                             as 使用部位 ';
            $sql .= '    , a.pharmaceutical_number                 as 薬事承認番号';
            $sql .= '    , a.biogenous_type                        as 生物由来区分 ';
            $sql .= "    , to_char(a.start_date, 'YYYY/mm/dd')     as 採用開始日 ";
            $sql .= "    , to_char(a.end_date, 'YYYY/mm/dd')       as 採用中止日 ";
            $sql .= '    , a.master_id                             as "マスタID" ';
            $sql .= '    , a.converted_num                         as 換算数 ';
            $sql .= '    , e.name                                  as 勘定科目 ';
            $sql .= '    , d.facility_code                         as 施主コード ';
            $sql .= '    , (  ';
            $sql .= '       case  ';
            $sql .= '         when a.is_auto_storage = false  ';
            $sql .= "         then ''  ";
            $sql .= "         else '○' ";
            $sql .= '         end ';
            $sql .= '     )                                        as 自動入庫フラグ  ';
            $sql .= '    , (  ';
            $sql .= '       case  ';
            $sql .= '         when a.is_lowlevel = false  ';
            $sql .= "         then ''  ";
            $sql .= "         else '○' ";
            $sql .= '         end ';
            $sql .= '     )                                        as 低レベル管理フラグ  ';
            $sql .= '    , (  ';
            $sql .= '       case  ';
            $sql .= '         when a.buy_flg = false  ';
            $sql .= "         then ''  ";
            $sql .= "         else '○' ";
            $sql .= '         end ';
            $sql .= '     )                                        as 薬品フラグ  ';
            $sql .= '    , a.expired_date                          as 期限切れ警告日数 ';
            $sql .= '    , a.enable_medicalcode                    as 医事コード発行フラグ  ';
            $sql .= '    , a.lot_ubd_alert                         as ロットubd警告 ';
            for($i=1;$i<=30;$i++){
                $sql .= '    , a.spare_key'.$i.'                            as 予備キー'.$i.' ';
            }
            $sql .= '    , a.recital                               as 備考 ';
            for($i=0;$i<5;$i++){
                $sql .= '    , (  ';
                $sql .= '      select ';
                $sql .= '            y.internal_code  ';
                $sql .= '        from ';
                $sql .= '          mst_item_units as x  ';
                $sql .= '          left join mst_unit_names as y  ';
                $sql .= '            on y.id = x.mst_unit_name_id  ';
                $sql .= '        where ';
                $sql .= '          x.mst_facility_item_id = a.id  ';
                $sql .= '        order by ';
                $sql .= '          x.is_standard desc , x.per_unit  ';
                $sql .= '        offset ' . $i;
                $sql .= '        limit 1 ';
                $sql .= '    )                                         as 包装単位' . ($i+1) . '単位コード';
                $sql .= '    , (  ';
                $sql .= '      select ';
                $sql .= '            x.per_unit  ';
                $sql .= '        from ';
                $sql .= '          mst_item_units as x  ';
                $sql .= '          left join mst_unit_names as y  ';
                $sql .= '            on y.id = x.mst_unit_name_id  ';
                $sql .= '        where ';
                $sql .= '          x.mst_facility_item_id = a.id  ';
                $sql .= '        order by ';
                $sql .= '          x.is_standard desc , x.per_unit  ';
                $sql .= '        offset ' . $i;
                $sql .= '        limit 1 ';
                $sql .= '    )                                         as 包装単位' . ($i+1) . '入数';
                $sql .= '    , (  ';
                $sql .= '      select ';
                $sql .= "            to_char(x.start_date,'YYYY/mm/dd')  ";
                $sql .= '        from ';
                $sql .= '          mst_item_units as x  ';
                $sql .= '          left join mst_unit_names as y  ';
                $sql .= '            on y.id = x.mst_unit_name_id  ';
                $sql .= '        where ';
                $sql .= '          x.mst_facility_item_id = a.id  ';
                $sql .= '        order by ';
                $sql .= '          x.is_standard desc , x.per_unit  ';
                $sql .= '        offset ' . $i;
                $sql .= '        limit 1 ';
                $sql .= '    )                                         as 包装単位' . ($i+1) . '開始日';
            }
            
            $sql .= '  from ';
            $sql .= '    mst_facility_items as a  ';
            $sql .= '    left join mst_facilities as b  ';
            $sql .= '      on b.id = a.mst_facility_id  ';
            $sql .= '    left join mst_dealers as c  ';
            $sql .= '      on c.id = a.mst_dealer_id  ';
            $sql .= '    left join mst_facilities as d  ';
            $sql .= '      on d.id = a.mst_owner_id  ';
            $sql .= '    left join mst_accountlists as e  ';
            $sql .= '      on e.id = a.mst_accountlist_id  ';
            $sql .= '    left join mst_unit_names as f  ';
            $sql .= '      on f.id = a.mst_unit_name_id  ';
            $sql .= '    left join mst_consts as g  ';
            $sql .= '      on g.const_cd = a.insurance_claim_type::varchar  ';
            $sql .= "      and g.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
            $sql .= '    left join mst_insurance_claims as h  ';
            $sql .= '      on h.insurance_claim_code = a.insurance_claim_code ';
            $sql .= '    left join mst_item_categories as i1 ';
            $sql .= '      on i1.id = a.item_category1 ';
            $sql .= '    left join mst_item_categories as i2 ';
            $sql .= '      on i2.id = a.item_category2 ';
            $sql .= '    left join mst_item_categories as i3 ';
            $sql .= '      on i3.id = a.item_category3 ';

            $sql .= '  where ';
            $sql .= '    a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '    and a.item_type = ' . Configure::read('Items.item_types.normalitem');

            /* 画面入力条件追加 ココから */
            // 商品ID(完全一致)
            if($this->request->data['download']['internal_code']!=''){
                $sql .= "  and a.internal_code = '" . $this->request->data['download']['internal_code']. "'";
            }
            // 商品名(部分一致)
            if($this->request->data['download']['item_name']!=''){
                $sql .= "  and a.item_name like '%" . $this->request->data['download']['item_name']. "%'";
            }
            // 製品番号(部分一致)
            if($this->request->data['download']['item_code']!=''){
                $sql .= "  and a.item_code ilike '%" . $this->request->data['download']['item_code']. "%'";
            }
            // 規格(部分一致)
            if($this->request->data['download']['standard']!=''){
                $sql .= "  and a.standard ilike '%" . $this->request->data['download']['item_code']. "%'";
            }
            // 販売元(部分一致)
            if($this->request->data['download']['dealer_name']!=''){
                $sql .= "  and c.dealer_name ilike '%" . $this->request->data['download']['dealer_name']. "%'";
            }
            // JANコード(完全一致)
            if($this->request->data['download']['jan_code']!=''){
                $sql .= "  and a.jan_code = '" . $this->request->data['download']['jan_code']. "'";
            }
            // 開始日From
            if($this->request->data['download']['start_date']!=''){
                $sql .= "  and a.start_date >=  '" . $this->request->data['download']['start_date']. "'";
            }
            // 開始日To
            if($this->request->data['download']['end_date']!=''){
                $sql .= "  and a.start_date <= '" . $this->request->data['download']['end_date']. "'";
            }
            // 有効なデータのみ
            if(isset($this->request->data['download']['flg'])){
                $sql .= '  and a.is_deleted = false ';
                $sql .= '  and a.start_date <= now()::date  ';
                $sql .= '  and ( a.end_date >= now()::date or a.end_date is null ) ';
            }

            /* 画面入力条件追加 ココまで */

            $sql .= '  order by ';
            $sql .= '    b.facility_code ';
            $sql .= '    , a.internal_code ';
            $name = '施設採用品';
            break;
          case 2:
            // 定数マスタ（センター）
            $sql  = ' select ';
            $sql .= '       a.id                                as "ＩＤ" '; // 半角だと、CSVを直に開く場合EXCELでエラーが出るので。。
            $sql .= '     , f.facility_code                     as 施設コード ';
            $sql .= '     , e.department_code                   as 部署コード ';
            $sql .= '     , c.internal_code                     as "商品ID" ';
            $sql .= '     , b.per_unit                          as 包装単位入数 ';
            $sql .= '     , a.order_point                       as 発注点 ';
            $sql .= '     , a.fixed_count                       as 定数 ';
            $sql .= '     , a.holiday_fixed_count               as 休日 ';
            $sql .= '     , a.spare_fixed_count                 as 予備 ';
            $sql .= '     , d.name                              as 棚番 ';
            $sql .= "     , to_char(a.start_date, 'YYYY/mm/dd') as 開始日 ";
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when a.is_deleted = true  ';
            $sql .= "         then ''  ";
            $sql .= "         else '○' ";
            $sql .= '         end ';
            $sql .= '     )                                     as 適用フラグ  ';
            $sql .= '   from ';
            $sql .= '     mst_fixed_counts as a  ';
            $sql .= '     left join mst_item_units as b  ';
            $sql .= '       on b.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as c  ';
            $sql .= '       on c.id = b.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as c1  ';
            $sql .= '       on c1.id = c.mst_dealer_id  ';
            $sql .= '     left join mst_shelf_names as d  ';
            $sql .= '       on d.id = a.mst_shelf_name_id  ';
            $sql .= '     left join mst_departments as e  ';
            $sql .= '       on e.id = a.mst_department_id  ';
            $sql .= '     left join mst_facilities as f  ';
            $sql .= '       on f.id = e.mst_facility_id  ';
            $sql .= '   where ';
            $sql .= '     f.id = ' . $this->Session->read('Auth.facility_id_selected');

            /* 画面入力条件追加 ココから */
            // 商品ID(完全一致)
            if($this->request->data['download']['internal_code']!=''){
                $sql .= "  and c.internal_code = '" . $this->request->data['download']['internal_code']. "'";
            }
            // 商品名(部分一致)
            if($this->request->data['download']['item_name']!=''){
                $sql .= "  and c.item_name like '%" . $this->request->data['download']['item_name']. "%'";
            }
            // 製品番号(部分一致)
            if($this->request->data['download']['item_code']!=''){
                $sql .= "  and c.item_code ilike '%" . $this->request->data['download']['item_code']. "%'";
            }
            // 規格(部分一致)
            if($this->request->data['download']['standard']!=''){
                $sql .= "  and c.standard ilike '%" . $this->request->data['download']['item_code']. "%'";
            }
            //販売元(部分一致)
            if($this->request->data['download']['dealer_name']!=''){
                $sql .= "  and c1.dealer_name ilike '%" . $this->request->data['download']['dealer_name']. "%'";
            }
            // JANコード(完全一致)
            if($this->request->data['download']['jan_code']!=''){
                $sql .= "  and c.jan_code = '" . $this->request->data['download']['jan_code']. "'";
            }
            // 開始日From
            if($this->request->data['download']['start_date']!=''){
                $sql .= "  and a.start_date >=  '" . $this->request->data['download']['start_date']. "'";
            }
            // 開始日To
            if($this->request->data['download']['end_date']!=''){
                $sql .= "  and a.start_date <= '" . $this->request->data['download']['end_date']. "'";
            }
            // 有効なデータのみ
            if(isset($this->request->data['download']['flg'])){
                $sql .= '  and a.is_deleted = false';
                $sql .= '  and a.start_date <= now()::date ';
            }

            /* 画面入力条件追加 ココまで */
            $sql .= '   order by ';
            $sql .= '     f.facility_code ';
            $sql .= '     , e.department_code ';
            $sql .= '     , c.internal_code ';
            $sql .= '     , b.per_unit ';

            $name = 'センター定数';
            break;

          case 3:
            // 定数マスタ（病院）
            $sql  = ' select ';
            $sql .= '       a.id                                as "ＩＤ" '; // 半角だと、CSVを直に開く場合EXCELでエラーが出るので。。
            $sql .= '     , f.facility_code                     as 施設コード ';
            $sql .= '     , e.department_code                   as 部署コード ';
            $sql .= '     , c.internal_code                     as "商品ID" ';
            $sql .= '     , b.per_unit                          as 包装単位入数 ';
            $sql .= '     , a.fixed_count                       as 定数 ';
            $sql .= '     , a.holiday_fixed_count               as 休日 ';
            $sql .= '     , a.spare_fixed_count                 as 予備 ';
            $sql .= '     , a.trade_type                        as 管理区分 ';
            $sql .= '     , a.print_type                        as 印刷区分 ';
            $sql .= '     , d.name                              as 棚番 ';
            $sql .= "     , to_char(a.start_date, 'YYYY/mm/dd') as 開始日 ";
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when a.is_deleted = true  ';
            $sql .= "         then ''  ";
            $sql .= "         else '○' ";
            $sql .= '         end ';
            $sql .= '     )                                     as 適用フラグ  ';
            $sql .= '   from ';
            $sql .= '     mst_fixed_counts as a  ';
            $sql .= '     left join mst_item_units as b  ';
            $sql .= '       on b.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as c  ';
            $sql .= '       on c.id = b.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as c1  ';
            $sql .= '       on c1.id = c.mst_dealer_id  ';
            $sql .= '     left join mst_shelf_names as d  ';
            $sql .= '       on d.id = a.mst_shelf_name_id  ';
            $sql .= '     left join mst_departments as e  ';
            $sql .= '       on e.id = a.mst_department_id  ';
            $sql .= '     left join mst_facilities as f  ';
            $sql .= '       on f.id = e.mst_facility_id  ';
            $sql .= '   where ';
            $sql .= '     c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     and e.department_type = ' . Configure::read('DepartmentType.hospital');
            /* 画面入力条件追加 ココから */
            // 得意先
            if($this->request->data['download']['id']!=''){
                $sql .= "  and f.id = '" . $this->request->data['download']['id']. "'";
            }
            // 商品ID(完全一致)
            if($this->request->data['download']['internal_code']!=''){
                $sql .= "  and c.internal_code = '" . $this->request->data['download']['internal_code']. "'";
            }
            // 商品名(部分一致)
            if($this->request->data['download']['item_name']!=''){
                $sql .= "  and c.item_name like '%" . $this->request->data['download']['item_name']. "%'";
            }
            // 製品番号(部分一致)
            if($this->request->data['download']['item_code']!=''){
                $sql .= "  and c.item_code ilike '%" . $this->request->data['download']['item_code']. "%'";
            }
            // 規格(部分一致)
            if($this->request->data['download']['standard']!=''){
                $sql .= "  and c.standard ilike '%" . $this->request->data['download']['item_code']. "%'";
            }
            // 販売元(部分一致)
            if($this->request->data['download']['dealer_name']!=''){
                $sql .= "  and c1.dealer_name ilike '%" . $this->request->data['download']['dealer_name']. "%'";
            }
            // JANコード(完全一致)
            if($this->request->data['download']['jan_code']!=''){
                $sql .= "  and c.jan_code = '" . $this->request->data['download']['jan_code']. "'";
            }
            // 開始日From
            if($this->request->data['download']['start_date']!=''){
                $sql .= "  and a.start_date >=  '" . $this->request->data['download']['start_date']. "'";
            }
            // 開始日To
            if($this->request->data['download']['end_date']!=''){
                $sql .= "  and a.start_date <= '" . $this->request->data['download']['end_date']. "'";
            }
            // 有効なデータのみ
            if(isset($this->request->data['download']['flg'])){
                $sql .= '  and a.is_deleted = false';
                $sql .= '  and a.start_date <= now()::date ';
            }
            /* 画面入力条件追加 ココまで */
            $sql .= '   order by ';
            $sql .= '     f.facility_code ';
            $sql .= '     , e.department_code ';
            $sql .= '     , c.internal_code ';
            $sql .= '     , b.per_unit ';

            $name = '病院定数';
            break;
          case 4:
            // 仕入マスタ
            $sql  = ' select ';
            $sql .= '       a.id                               as "ＩＤ" '; // 半角だと、CSVを直に開く場合EXCELでエラーが出るので。。
            $sql .= '     , b.facility_code                    as センター施設コード ';
            $sql .= "     , to_char(a.start_date,'YYYY/mm/dd') as 開始日 ";
            $sql .= '     , c.facility_code                    as 仕入先施設コード1 ';
            // $sql .= '     , c2.facility_code                   as 仕入先施設コード2 ';
            $sql .= '     , e.internal_code                    as "商品ID" ';
            $sql .= '     , d.per_unit                         as 包装単位入数 ';
            $sql .= '     , a.transaction_price                as 価格 ';
            if($this->Session->read('Auth.Config.MSCorporate') == '1' ){
                $sql .= '     , a.ms_transaction_price                as "MS価格" ';
            }
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when a.is_deleted = true  ';
            $sql .= "         then ''  ";
            $sql .= "         else '○'  ";
            $sql .= '         end ';
            $sql .= '     )                                    as 適用フラグ  ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when a2.id is not null ';
            $sql .= "         then '○'  ";
            $sql .= "         else ''  ";
            $sql .= '         end ';
            $sql .= '     )                                    as 代表フラグ  ';
            $sql .= '   from ';
            $sql .= '     mst_transaction_configs as a  ';
            $sql .= '     left join mst_transaction_mains as a2';
            $sql .= '       on a2.mst_facility_id = a.partner_facility_id ';
            $sql .= '      and a2.mst_item_unit_id = a.mst_item_unit_id ';
            $sql .= '     left join mst_facilities as b  ';
            $sql .= '       on b.id = a.mst_facility_id  ';
            $sql .= '     left join mst_facilities as c  ';
            $sql .= '       on c.id = a.partner_facility_id  ';
            $sql .= '     left join mst_facilities as c2  ';
            $sql .= '       on c2.id = a.partner_facility_id2  ';
            $sql .= '     left join mst_item_units as d  ';
            $sql .= '       on d.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as e  ';
            $sql .= '       on e.id = d.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as f  ';
            $sql .= '       on f.id = e.mst_dealer_id  ';
            $sql .= '   where ';
            $sql .= '     a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');

            /* 画面入力条件追加 ココから */
            // 商品ID(完全一致)
            if($this->request->data['download']['internal_code']!=''){
                $sql .= "  and e.internal_code = '" . $this->request->data['download']['internal_code']. "'";
            }
            // 商品名(部分一致)
            if($this->request->data['download']['item_name']!=''){
                $sql .= "  and e.item_name like '%" . $this->request->data['download']['item_name']. "%'";
            }
            // 製品番号(部分一致)
            if($this->request->data['download']['item_code']!=''){
                $sql .= "  and e.item_code ilike '%" . $this->request->data['download']['item_code']. "%'";
            }
            // 規格(部分一致)
            if($this->request->data['download']['standard']!=''){
                $sql .= "  and e.standard ilike '%" . $this->request->data['download']['item_code']. "%'";
            }
            // 販売元(部分一致)
            if($this->request->data['download']['dealer_name']!=''){
                $sql .= "  and f.dealer_name ilike '%" . $this->request->data['download']['dealer_name']. "%'";
            }
            // JANコード(完全一致)
            if($this->request->data['download']['jan_code']!=''){
                $sql .= "  and e.jan_code = '" . $this->request->data['download']['jan_code']. "'";
            }
            // 開始日From
            if($this->request->data['download']['start_date']!=''){
                $sql .= "  and a.start_date >=  '" . $this->request->data['download']['start_date']. "'";
            }
            // 開始日To
            if($this->request->data['download']['end_date']!=''){
                $sql .= "  and a.start_date <= '" . $this->request->data['download']['end_date']. "'";
            }
            // 有効なデータのみ
            if(isset($this->request->data['download']['flg'])){
                $sql .= '  and a.is_deleted = false';
                $sql .= '  and a.start_date <= now()::date ';
                $sql .= '  and a.end_date > now()::date ';
            }
            /* 画面入力条件追加 ココまで */
            $sql .= '   order by ';
            $sql .= '     b.facility_code ';
            $sql .= '     , e.internal_code ';
            $sql .= '     , d.per_unit ';
            $sql .= '     , a.start_date ';
            $name = '仕入設定';
            break;
          case 5:
            // 売上マスタ
            $sql  = ' select ';
            $sql .= '       a.id                                as "ＩＤ" '; // 半角だと、CSVを直に開く場合EXCELでエラーが出るので。。
            $sql .= '     , d.facility_code                     as センター施設コード ';
            $sql .= "     , to_char(a.start_date, 'YYYY/mm/dd') as 開始日 ";
            $sql .= '     , e.facility_code                     as 病院施設コード ';
            $sql .= '     , c.internal_code                     as "商品ID" ';
            $sql .= '     , b.per_unit                          as 包装単位入数 ';
            $sql .= '     , a.sales_price                       as 価格 ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when a.is_deleted = true  ';
            $sql .= "         then ''  ";
            $sql .= "         else '○'  ";
            $sql .= '         end ';
            $sql .= '     )                                     as 適用フラグ  ';
            $sql .= '   from ';
            $sql .= '     mst_sales_configs as a  ';
            $sql .= '     left join mst_item_units as b  ';
            $sql .= '       on b.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as c  ';
            $sql .= '       on c.id = b.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as c1  ';
            $sql .= '       on c1.id = c.mst_dealer_id  ';
            $sql .= '     left join mst_facilities as d  ';
            $sql .= '       on d.id = c.mst_facility_id  ';
            $sql .= '     left join mst_facilities as e  ';
            $sql .= '       on e.id = a.partner_facility_id  ';
            $sql .= '   where ';
            $sql .= '     a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');

            /* 画面入力条件追加 ココから */
            // 得意先
            if($this->request->data['download']['id']!=''){
                $sql .= "  and e.id = '" . $this->request->data['download']['id']. "'";
            }
            // 商品ID(完全一致)
            if($this->request->data['download']['internal_code']!=''){
                $sql .= "  and c.internal_code = '" . $this->request->data['download']['internal_code']. "'";
            }
            // 商品名(部分一致)
            if($this->request->data['download']['item_name']!=''){
                $sql .= "  and c.item_name like '%" . $this->request->data['download']['item_name']. "%'";
            }
            // 製品番号(部分一致)
            if($this->request->data['download']['item_code']!=''){
                $sql .= "  and c.item_code ilike '%" . $this->request->data['download']['item_code']. "%'";
            }
            // 規格(部分一致)
            if($this->request->data['download']['standard']!=''){
                $sql .= "  and c.standard ilike '%" . $this->request->data['download']['item_code']. "%'";
            }
            // 販売元(部分一致)
            if($this->request->data['download']['dealer_name']!=''){
                $sql .= "  and c1.dealer_name ilike '%" . $this->request->data['download']['dealer_name']. "%'";
            }
            // JANコード(完全一致)
            if($this->request->data['download']['jan_code']!=''){
                $sql .= "  and c.jan_code = '" . $this->request->data['download']['jan_code']. "'";
            }
            // 開始日From
            if($this->request->data['download']['start_date']!=''){
                $sql .= "  and a.start_date >=  '" . $this->request->data['download']['start_date']. "'";
            }
            // 開始日To
            if($this->request->data['download']['end_date']!=''){
                $sql .= "  and a.start_date <= '" . $this->request->data['download']['end_date']. "'";
            }
            // 有効なデータのみ
            if(isset($this->request->data['download']['flg'])){
                $sql .= '  and a.is_deleted = false';
                $sql .= '  and a.start_date <= now()::date ';
                $sql .= '  and a.end_date > now()::date ';
            }
            /* 画面入力条件追加 ココまで */
            $sql .= '   order by ';
            $sql .= '     d.facility_code ';
            $sql .= '     , e.facility_code ';
            $sql .= '     , c.internal_code ';
            $sql .= '     , b.per_unit ';
            $sql .= '     , a.start_date ';

            $name = '売上設定';
            break;
          default:
            $sql = 'select * from mst_users as a where 1 = 0 ';
            break;
        }

        $this->db_export_csv($sql , $name, 'import_csv');
    }


    /**
     * CSV取込画面
     */
    function import_csv() {
        $this->setRoleFunction(75); // 採用品CSV登録
        // 病院一覧取得
        $FacilityList = $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                               array(Configure::read('FacilityType.hospital')) ,
                                               false ,
                                               array('id' , 'facility_name') ,
                                               'facility_code');
        $this->set('FacilityList',$FacilityList);
    }

    /**
     * CSV取込メイン処理
     */
    function import_csv_result() {
        $this->request->data['now'] = date('Y/m/d H:i:s');
        // ファイルアップ
        $_upfile = $this->request->data['result']['tmp_name'];
        $_fileName = '../tmp/'.date('YmdHi').'_mst.csv';
        if (is_uploaded_file($_upfile)){
            move_uploaded_file($_upfile, mb_convert_encoding($_fileName, 'UTF-8', 'SJIS-win'));
            chmod($_fileName, 0644);
        }else{
            $this->Session->setFlash('ファイルが読み込めませんでした。CSVファイルを確認してください。', 'growl', array('type'=>'error') );
            $this->redirect('import_csv');
            exit(0);
        }

        // アップロードしたCSVデータ文字列として読み込む
        $_filedata = mb_convert_encoding(file_get_contents ($_fileName), 'UTF-8',"SJIS-win");
        // 配列変換
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $data_array = $this->utils->input_tsv($_filedata);
        }else{
            $data_array = $this->utils->input_csv($_filedata);
        }
        unlink($_fileName);

        switch($this->request->data['csv']['type']){
          case 1 :// 採用品
            $map['ＩＤ']                    ='id';
            $map['施設コード']              ='facility_code';
            $map['商品ID']                  ='internal_code';
            $map['商品名']                  ='item_name';
            $map['規格']                    ='standard';
            $map['製品番号']              ='item_code';
            $map['販売元コード']            ='dealer_code';
            $map['販売元名']                ='dealer_name';
            $map['janコード']               ='jan_code';
            $map['旧janコード']             ='old_jan_code';
            $map['基本単位コード']          ='unit_code';
            $map['基本単位入数']            ='per_unit';
            $map['償還価格']                ='refund_price';
            $map['定価']                    ='price';
            $map['定価単価']                ='unit_price';
            $map['課税区分']                ='tax_type';
            $map['保険請求区分']            ='insurance_claim_type';
            $map['保険請求コード']          ='insurance_claim_code';
            $map['医事コード']              ='medical_code';
            $map['JMDNコード']              ='jmdn_code';
            $map['クラス分類']              ='class_separation';
            $map['大分類']                  ='category_name1';
            $map['中分類']                  ='category_name2';
            $map['小分類']                  ='category_name3';
            $map['使用部位']                ='parts_use';
            $map['薬事承認番号']            ='pharmaceutical_number';
            $map['生物由来区分']            ='biogenous_type';
            $map['採用開始日']              ='start_date';
            $map['採用中止日']              ='end_date';
            $map['マスタID']                ='master_id';
            $map['換算数']                  ='converted_num';
            $map['勘定科目']                ='mst_accountlist';
            $map['施主コード']              ='owner_facility_code';
            $map['自動入庫フラグ']          ='is_auto_storage';
            $map['低レベル管理フラグ']      ='is_lowlevel';
            $map['薬品フラグ']              ='buy_flg';
            $map['期限切れ警告日数']        ='expired_date';
            $map['医事コード発行フラグ']    ='enable_medicalcode';
            $map['ロットubd警告']           ='lot_ubd_alert';
            for($i=1;$i<=30;$i++){
                $map["予備キー{$i}"]                  ="spare_key{$i}";
            }
            $map['備考']                       ='recital';
            
            for($i=1;$i<=5;$i++){
                $map["包装単位{$i}単位コード"]        ="unit_code{$i}";
                $map["包装単位{$i}入数"]              ="per_unit{$i}";
                $map["包装単位{$i}開始日"]            ="start_date{$i}";
            }
            
            $type_name = '施設採用品';
            break;
          case 2 :// センター定数設定
            $map = array(
                'ＩＤ'          =>'id',
                '施設コード'    =>'facility_code',
                '部署コード'    =>'department_code',
                '商品ID'        =>'internal_code',
                '包装単位入数'  =>'per_unit',
                '発注点'        =>'order_point',
                '定数'          =>'fixed_count',
                '休日'          =>'holiday_fixed_count',
                '予備'          =>'spare_fixed_count',
                '棚番'          =>'shelf_name',
                '開始日'        =>'start_date',
                '適用フラグ'    =>'is_deleted'
                );
            $type_name = 'センター定数';
            break;
          case 3 :// 病院定数設定
            $map = array(
                'ＩＤ'          =>'id',
                '施設コード'    =>'facility_code',
                '部署コード'    =>'department_code',
                '商品ID'        =>'internal_code',
                '包装単位入数'  =>'per_unit',
                '定数'          =>'fixed_count',
                '休日'          =>'holiday_fixed_count',
                '予備'          =>'spare_fixed_count',
                '管理区分'      =>'trade_type',
                '印刷区分'      =>'print_type',
                '棚番'          =>'shelf_name',
                '開始日'        =>'start_date',
                '適用フラグ'    =>'is_deleted'
                );
            $type_name = '病院定数';
            break;
          case 4 :// 仕入設定
            $map['ＩＤ']               = 'id';
            $map['センター施設コード'] = 'center_facility_code';
            $map['開始日']             = 'start_date';
            $map['仕入先施設コード1']  = 'facility_code1';
            // $map['仕入先施設コード2']  = 'facility_code2';
            $map['商品ID']             = 'internal_code';
            $map['包装単位入数']       = 'per_unit';
            $map['価格']               = 'transaction_price';
            if($this->Session->read('Auth.Config.MSCorporate') == '1' ){
                $map['MS価格']             = 'ms_transaction_price';
            }
            $map['適用フラグ']         = 'is_deleted';
            $map['代表フラグ']         = 'is_main';
            
            $type_name = '仕入設定';
            break;
          case 5 :// 売上設定
            $map = array(
                'ＩＤ'               => 'id',
                'センター施設コード' => 'center_facility_code',
                '開始日'             => 'start_date',
                '病院施設コード'     => 'hospital_facility_code',
                '商品ID'             => 'internal_code',
                '包装単位入数'       => 'per_unit',
                '価格'               => 'sales_price',
                '適用フラグ'         => 'is_deleted'
                );
            $type_name = '売上設定';
            break;
        }

        // UPファイルを配列に入れる
        $mapped_data = $this->_dataMapping($data_array, $map);
        if($mapped_data == false){
            // マッピングに失敗する場合、項目名が変更されているのでエラーにする
            $this->Session->setFlash('項目名を変更している場合取り込めません。確認してください。', 'growl', array('type'=>'error') );
            $this->redirect('import_csv');
            exit(0);
        }

        // 各登録処理に飛ぶ
        switch($this->request->data['csv']['type']){
          case 1 :// 採用品
            $this->saveFacilityItem($mapped_data);
            break;
          case 2 :// センター定数設定
            $this->saveCenterFixedCount($mapped_data);
            break;
          case 3 :// 病院定数設定
            $this->saveHospitalFixedCount($mapped_data);
            break;
          case 4 :// 仕入設定
            $this->saveTrnsactionConfig($mapped_data);
            break;
          case 5 :// 売上設定
            $this->saveSaleConfig($mapped_data);
            break;
        }

        // 表示用
        $this->set('type_name' , $type_name);
    }

    /**
     * 施設採用品登録検索画面
     * @param
     * @return void
     */
    function search() {
        App::import('Sanitize');
        $this->setRoleFunction(73); // 施設採用登録
        
        $ret = $item_category2 = $item_category3 = array();
        
        $item_category1 = $this->getItemCategory(1,null);
        if (isset($this->request->data['search']['is_search'])) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            
            $where = "";
            if (isset($this->request->data['MstGrandmaster']['master_id']) && $this->request->data['MstGrandmaster']['master_id'] != ''){
                $where .= " and a.matcd like '%" .$this->request->data['MstGrandmaster']['master_id']. "%'";
            }
            if (isset($this->request->data['MstGrandmaster']['item_name']) && $this->request->data['MstGrandmaster']['item_name'] != ''){
                $where .= " and a.matname like '%" .$this->request->data['MstGrandmaster']['item_name']. "%'";
            }
            if (isset($this->request->data['MstGrandmaster']['jan_code']) && $this->request->data['MstGrandmaster']['jan_code'] != ''){
                $where .= " and a.stdjancd like '" .$this->request->data['MstGrandmaster']['jan_code'] . "%'";
            }
            if (isset($this->request->data['MstGrandmaster']['standard']) && $this->request->data['MstGrandmaster']['standard'] != ''){
                $where .= " and a.sizdimnam like '%" .$this->request->data['MstGrandmaster']['standard'] . "%'";
            }
            if (isset($this->request->data['MstGrandmaster']['item_code']) && $this->request->data['MstGrandmaster']['item_code'] != ''){
                $where .= " and a.mkmatcd like '%" .$this->request->data['MstGrandmaster']['item_code'] . "%'";
            }
            if (isset($this->request->data['MstGrandmaster']['dealer_name']) && $this->request->data['MstGrandmaster']['dealer_name'] != '') {
                $where .= " and c.makcalnam like '%" .$this->request->data['MstGrandmaster']['dealer_name'] . "%'";
            }
            
            if (isset($this->request->data['MstGrandmaster']['isAccepted'])){
                $where .= " and b.master_id is null ";
            }

            $limit = " limit ". $this->_getLimitCount();

            $sql  ='select ';
            $sql .='      a.matcd                      as "MstGrandmaster__master_id" ';
            $sql .='    , b.master_id                  as "MstGrandmaster__accepted" ';
            $sql .='    , a.stdjancd                   as "MstGrandmaster__jan_code" ';
            $sql .='    , a.matname                    as "MstGrandmaster__item_name" ';
            $sql .='    , a.sizdimnam                  as "MstGrandmaster__standard" ';
            $sql .='    , a.mkmatcd                    as "MstGrandmaster__item_code" ';
            $sql .='    , c.makcalnam                  as "MstGrandmaster__dealer_name" ';
            $sql .='    , e.unitnam                    as "MstGrandmaster__unit_name" ';
            $sql .='    , ( ';
            $sql .='      case ';
            $sql .='        when b.master_id is not null ';
            $sql .="        then '済' ";
            $sql .="        else '未' ";
            $sql .='        end ';
            $sql .='    )                              as "MstGrandmaster__hasAccepted" ';
            $sql .='  from '; 
            $sql .='    mst_grand_kyo_materials        as a ';
            $sql .='    left join mst_facility_items as b  on a.matcd = b.master_id ';
            $sql .='      and b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .='      and b.is_deleted = false ';
            $sql .='    left join mst_grand_mkt_cos as c on c.makcocd = a.makcocd ';
            $sql .='      and c.is_deleted = false ';
            $sql .='    left join mst_grand_mat_prices as d on d.matcd = a.matcd ';
            $sql .='      and d.is_deleted = false ';
            $sql .='    left join mst_grand_units as e on e.unitcd = d.unit1 ';
            $sql .='      and e.is_deleted = false ';
            $sql .='  where ';
            $sql .='   (d.startdate <= now()) and  (d.enddate >= now()) ';
            $sql .='      and d.is_deleted = false ';
            $sql .=   $where ;
            $sql .='  order by a.matcd desc , a.stdjancd ';

            // 件数取得
            $this->set('max' , $this->getMaxCount($sql , 'MstGrandmaster'));

            $sql .= $limit;

            $ret = $this->MstGrandmaster->query($sql);
            $this->request->data = $data;
        } else {
            $this->request->data['MstGrandmaster']['isAccepted'] = 1;
        }
        $this->set('gm_items', $ret);
    }
    
    /**
     * 施設採用品入力
     * @return void
     */
    function add () {
        if ($this->request->data['MstGrandmaster']['opt'] != '') {
            // グランドマスタの情報を取得
            $matcds = $this->request->data['MstGrandmaster']['opt'];
            
            // グランドマスタの情報（配列）を分解
            foreach($matcds as $matcd){
                
                $this->request->data = $this->_MstGrandmaster($matcd);
                
                // 既定の商品IDのMAX + 1 を取得
                $this->request->data['MstFacilityItem']['internal_code'] = $this->getNextInternalCode(
                    Configure::read('ItemType.masterItem') ,
                    $this->Session->read('Auth.facility_id_selected')
                    );
                
                $this->request->data['MstFacilityItem']['start_date'] = date('Y/m/d');
                $this->request->data['MstFacilityItem']['converted_num'] = 1;
                // プルダウン作成
                $this->_createPullDown();
            }
        }
    }
    
    /**
     * 独自採用品入力
     */
    function add_original_item() {
        // 既定の商品IDのMAX + 1 を取得
        $this->request->data['MstFacilityItem']['internal_code'] = $this->getNextInternalCode(
            Configure::read('ItemType.originalItem') ,
            $this->Session->read('Auth.facility_id_selected')
            );

        $this->request->data['MstFacilityItem']['start_date'] = date('Y/m/d');
        // プルダウン作成
        $this->_createPullDown();
    }

    /**
     * 施設採用品テーブル（mst_facility_items）への登録確認
     */
    function add_confirm () {
        // プルダウン作成
        $this->_createPullDown();
        
        // 2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }

    /**
     * 施設採用品テーブル（mst_facility_items）への登録結果
     * @access public
     * @return void
     */
    function add_result () {

        // トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            $this->_createPullDown();
            $this->request->data = $this->Session->read('Facilityitems.saved');
            $this->Session->delete('Facilityitems.saved');
            $this->set('data',$this->request->data);
            $this->render('add_result');
            return;
        }
        $now = $this->request->data['now'] = date('Y/m/d H:i:s');
        // 施設採用品テーブル（mst_facility_items）登録用にデータ整形
        $this->request->data['MstFacilityItem']['is_auto_storage'] = true;

        $this->request->data['MstFacilityItem']['mst_dealer_id']   = $this->request->data['MstDealer']['id'];
        $this->request->data['MstFacilityItem']['per_unit']        = 1;
        $this->request->data['MstFacilityItem']['creater']         = $this->Session->read('Auth.MstUser.id');
        $this->request->data['MstFacilityItem']['modifier']        = $this->Session->read('Auth.MstUser.id');
        $this->request->data['MstFacilityItem']['mst_facility_id'] = $this->Session->read('Auth.facility_id_selected');
        
        // 重複チェック
        $result = $this->MstFacilityItem->chkDuplicateInternalCode($this->request->data['MstFacilityItem']['internal_code'],
                                                                   null,
                                                                   $this->Session->read('Auth.facility_id_selected'));


        if (!$result){
            $this->Session->setFlash('既に同じ商品IDが登録済みです。新しいIDに置き換えました', 'growl', array('type'=>'error') );
            $this->_createPullDown();
            if($this->request->data['MstFacilityItem']['isOriginal']==1){
                // 独自採用品の場合
                $this->request->data['MstFacilityItem']['internal_code'] = $this->getNextInternalCode(Configure::read('ItemType.originalItem') ,
                                                                                                      $this->Session->read('Auth.facility_id_selected'));
                $this->render('add_original_item');
            }else{
                // 通常品の場合
                $this->request->data['MstFacilityItem']['internal_code'] = $this->getNextInternalCode(Configure::read('ItemType.masterItem') ,
                                                                                                      $this->Session->read('Auth.facility_id_selected'));
                $this->render('add');
            }
            return;
        }

        if ($this->MstFacilityItem->validates($this->request->data['MstFacilityItem'])) {
            // トランザクション開始
            $this->MstFacilityItem->begin();
            
            // 施設採用品テーブル（mst_facility_items）への登録
            $this->MstFacilityItem->create();
            if(!$this->MstFacilityItem->save($this->request->data['MstFacilityItem'], array('validate' => false))){
                // エラー
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash('施設際用品レコードの作成中にエラーが発生しました', 'growl', array('type'=>'error') );
                // 2度押し対策用にトランザクショントークンを作る
                $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                $this->redirect('add_confirm');
            }
            
            // 包装単位を登録
            if(!$this->saveOneItemUnit($this->request->data['MstFacilityItem'],$this->MstFacilityItem->getLastInsertId())){
                // エラー
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash('包装単位レコードの作製中にエラーが発生しました', 'growl', array('type'=>'error') );
                // 2度押し対策用にトランザクショントークンを作る
                $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                $this->redirect('add_confirm');
            }
            // センター倉庫の在庫レコードを作成
            if(!$this->getStockRecode($this->MstItemUnit->getLastInsertId())){
                // エラー
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash('在庫レコードの作製中にエラーが発生しました', 'growl', array('type'=>'error') );
                // 2度押し対策用にトランザクショントークンを作る
                $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                $this->redirect('add_confirm');
            }
            // 仕入設定を登録
            $MstTransactionConfig = array(
                'MstTransactionConfig'=> array(
                    'mst_item_unit_id'         => $this->MstItemUnit->getLastInsertId(),
                    'start_date'               => $this->request->data['MstFacilityItem']['start_date'],
                    'end_date'                 => '3000/01/01',
                    'mst_facility_relation_id' => $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $this->request->data['MstTransactionConfig']['partner_facility_id']),
                    'mst_facility_item_id'     => $this->MstFacilityItem->getLastInsertId(),
                    'mst_facility_id'          => $this->Session->read('Auth.facility_id_selected'),
                    'partner_facility_id'      => $this->request->data['MstTransactionConfig']['partner_facility_id'],
                    'transaction_price'        => $this->request->data['MstTransactionConfig']['transaction_price'], 
                    'is_deleted'               => false,
                    'creater'                  => $this->Session->read('Auth.MstUser.id'),
                    'created'                  => $now,
                    'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                    'modified'                 => $now,
                    )
                );
            
            $this->MstTransactionConfig->create();
            // SQL実行
            if (!$this->MstTransactionConfig->save($MstTransactionConfig)) {
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash('仕入設定の作製中にエラーが発生しました', 'growl', array('type'=>'error') );
                // 2度押し対策用にトランザクショントークンを作る
                $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                $this->redirect('add_confirm');
            }
            
            $hospitalId = $this->getHospitalId($this->Session->read('Auth.facility_id_selected'));
            
            // 売上設定を登録
            $MstSalesConfig = array(
                'MstSalesConfig' => array(
                    'mst_item_unit_id'         => $this->MstItemUnit->getLastInsertId(),
                    'start_date'               => $this->request->data['MstFacilityItem']['start_date'],
                    'end_date'                 => '3000/01/01',
                    'mst_facility_relation_id' => $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $hospitalId),
                    'mst_facility_item_id'     => $this->MstFacilityItem->getLastInsertId(),
                    'mst_facility_id'          => $this->Session->read('Auth.facility_id_selected'),
                    'partner_facility_id'      => $hospitalId,
                    'sales_price'              => $this->request->data['MstTransactionConfig']['transaction_price'],
                    'is_deleted'               => false,
                    'creater'                  => $this->Session->read('Auth.MstUser.id'),
                    'created'                  => $now,
                    'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                    'modified'                 => $now,
                    )
                );
            
            $this->MstSalesConfig->create();
            if (!$this->MstSalesConfig->save($MstSalesConfig)) {
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash('売上設定の作製中にエラーが発生しました', 'growl', array('type'=>'error') );
                // 2度押し対策用にトランザクショントークンを作る
                $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                $this->redirect('add_confirm');
            }
            
            // エラーがなければコミット
            $this->MstFacilityItem->commit();
            
            $this->_createPullDown();
            
            $this->Session->write('Facilityitems.saved', $this->request->data);
            $this->Session->setFlash('Myマスタが正しく保存できました', 'growl', array('type'=>'star') );
            $this->render('add_result');
            
            // Myマスター登録件数などの情報を更新
            $this->AuthSession->updateMasterItemInfo();
            
        } else {
            // バリデートエラーのためロールバック
            $this->MstFacilityItem->rollback();
            $this->Session->setFlash('Myマスタの保存が完了できませんでした', 'growl', array('type'=>'error') );
            
            // 2度押し対策用にトランザクショントークンを作る
            $this->request->data[$this->name]['token'] = $this->createToken($this->name);
            $this->redirect('add_confirm');
        }
    }

    /**
     * 施設採用品テーブル（mst_facility_items）検索
     */
    function edit_search () {
        App::import('Sanitize');
        $this->setRoleFunction(74);
        
        $fitems = array();
        $ret = $item_category2 = $item_category3 = array();
        $item_category1 = $this->getItemCategory(1,null);
        
        if (isset($this->request->data['hide_action']) && $this->request->data['hide_action'] == 1) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            $where = '';
            if (isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] != ''){
                $where .= " and a.internal_code LIKE '" .$this->request->data['MstFacilityItem']['internal_code']."%'";
            }
            if (isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] != ''){
                $where .= " and a.item_name LIKE '%".$this->request->data['MstFacilityItem']['item_name']."%'";
            }
            if (isset($this->request->data['MstFacilityItem']['jan_code']) && $this->request->data['MstFacilityItem']['jan_code'] != ''){
                $where .= " and a.jan_code LIKE '" .$this->request->data['MstFacilityItem']['jan_code']."%'";
            }
            if (isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] != ''){
                $where .= " and a.standard LIKE '%" .$this->request->data['MstFacilityItem']['standard']."%'";
            }
            if (isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] != ''){
                $where .= " and a.item_code LIKE '%".$this->request->data['MstFacilityItem']['item_code']."%'";
            }
            if (isset($this->request->data['MstFacilityItem']['dealer_code']) && $this->request->data['MstFacilityItem']['dealer_code'] != '') {
                $where .= " and b.dealer_name LIKE '%".$this->request->data['MstFacilityItem']['dealer_code']."%'";
            }
            if (isset($this->request->data['MstFacilityItem']['item_category1']) && $this->request->data['MstFacilityItem']['item_category1'] != ''){
                $where .= ' and a.item_category1 = ' . $this->request->data['MstFacilityItem']['item_category1'];
                // 中分類を取得
                $item_category2 = $this->getItemCategory(2,$this->request->data['MstFacilityItem']['item_category1']);
            }
            if (isset($this->request->data['MstFacilityItem']['item_category2']) && $this->request->data['MstFacilityItem']['item_category2'] != ''){
                $where .= ' and a.item_category2 = ' . $this->request->data['MstFacilityItem']['item_category2'];
                // 小分類を取得
                $item_category3 = $this->getItemCategory(3,$this->request->data['MstFacilityItem']['item_category2']);
            }
            if (isset($this->request->data['MstFacilityItem']['item_category3']) && $this->request->data['MstFacilityItem']['item_category3'] != ''){
                $where .= ' and a.item_category3 = ' . $this->request->data['MstFacilityItem']['item_category3'];
            }
            if (isset($this->request->data['MstFacilityItem']['spare_key1']) && $this->request->data['MstFacilityItem']['spare_key1'] != ''){
                $where .= " and a.spare_key1 like '%" . $this->request->data['MstFacilityItem']['spare_key1'] . "%'";
            }
            if (isset($this->request->data['MstFacilityItem']['spare_key2']) && $this->request->data['MstFacilityItem']['spare_key2'] != ''){
                $where .= " and a.spare_key2 like '%" . $this->request->data['MstFacilityItem']['spare_key2'] . "%'";
            }
            if (isset($this->request->data['MstFacilityItem']['spare_key3']) && $this->request->data['MstFacilityItem']['spare_key3'] != ''){
                $where .= " and a.spare_key3 like '%" . $this->request->data['MstFacilityItem']['spare_key3'] . "%'";
            }
            if (isset($this->request->data['MstFacilityItem']['is_deleted'])) {
                $where .= ' and ( a.end_date is null or a.end_date > now()  )';
                $where .= ' and a.is_deleted = false ';
            }

            $sql  = ' select ';
            $sql .= '       a.id                   as "MstFacilityItem__id" ';
            $sql .= '     , a.master_id            as "MstFacilityItem__master_id" ';
            $sql .= '     , a.jan_code             as "MstFacilityItem__jan_code" ';
            $sql .= '     , a.internal_code        as "MstFacilityItem__internal_code" ';
            $sql .= '     , a.item_name            as "MstFacilityItem__item_name" ';
            $sql .= '     , a.standard             as "MstFacilityItem__standard" ';
            $sql .= '     , a.item_code            as "MstFacilityItem__item_code" ';
            $sql .= '     , b.id                   as "MstFacilityItem__dealer_code" ';
            $sql .= '     , a.per_unit             as "MstFacilityItem__per_unit" ';
            $sql .= '     , b.dealer_name          as "MstDealer__dealer_name" ';
            $sql .= '     , a.start_date           as "MstFacilityItem__start_date" ';
            $sql .= '     , a.end_date             as "MstFacilityItem__end_date"  ';
            $sql .= '     , ( case when a.end_date < now() then true';
            $sql .= '            when a.is_deleted = true then true ';
            $sql .= '       else false ';
            $sql .= '       end )                  as "MstFacilityItem__is_deleted"';
            $sql .= '   from ';
            $sql .= '     mst_facility_items as a ';
            $sql .= '     left join mst_dealers  as b ';
            $sql .= '       on a.mst_dealer_id = b.id ';
            $sql .= '       and b.is_deleted = false';
            $sql .= '   where ';
            $sql .= '     a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     and a.item_type   = ' . Configure::read('Items.item_types.normalitem');
            $sql .= $where;
            $sql .= '   order by ';
            $sql .= '     a.internal_code desc, a.jan_code ';
            // 全件数取得
            $this->set('max', $this->getMaxCount($sql , 'MstFacilityItem'));
            $sql .= '   limit ' . $this->_getLimitCount();

            $fitems =  $this->MstFacilityItem->query($sql);
            $this->request->data = $data;
        } else {
            $this->request->data['MstFacilityItem']['is_deleted'] = true ;
        }
        $this->set('fitems', $fitems);
        $this->set('item_category1' , $item_category1);
        $this->set('item_category2' , $item_category2);
        $this->set('item_category3' , $item_category3);
        $this->set('data',$this->request->data);
        
    }
    
    
    /**
     * 施設採用品テーブル(mst_facility_items)編集
     * @param integer $id
     */
    function edit ($matcd = NULL) {
        if(!is_null($matcd)){
            // マスタIDの付替
            $tmp = $this->_MstGrandmaster($matcd);
            if(!empty($tmp)){
                $gmItem = $tmp;
                $this->Session->setFlash('マスタIDに対応した商品情報を読み込みました', 'growl', array('type'=>'star') );
            } else {
                $gmItem = array();
                $this->Session->setFlash('マスタIDに対応した商品がありません。', 'growl', array('type'=>'error') );
            }
        }
        // 更新時間チェック用に画面を開いた時間を保存 
        $this->Session->write('MstFacilityItem.readTime',date('Y-m-d H:i:s'));
        
        $_fitem = array();
        // 施設採用品テーブル(mst_facility_items)から値を取得
        $_fitem = $this->_getFacilityItem($this->request->data['MstFacilityItem']['id']);
        
        if(!is_null($matcd)){
            if(count($gmItem)>0){
                $_fitem= $this->changeMasterId($_fitem ,$gmItem);
                if ($gmItem){
                    $_fitem['MstFacilityItem']['master_id'] = $matcd;
                    $_fitem['MstFacilityItem']['item_category1'] = $this->request->data['MstFacilityItem']['item_category1'];
                    $_fitem['MstFacilityItem']['item_category2'] = $this->request->data['MstFacilityItem']['item_category2'];
                    $_fitem['MstFacilityItem']['item_category3'] = $this->request->data['MstFacilityItem']['item_category3'];
                    $_fitem['MstFacilityItem']['start_date']     = $this->request->data['MstFacilityItem']['start_date'];
                    $_fitem['MstFacilityItem']['end_date']       = $this->request->data['MstFacilityItem']['end_date'];
                }
            }
        }
        $this->request->data = $_fitem;
        
        $menuGroups = $this->AuthSession->getMenuGroups();
        if (isset($menuGroups['見積機能']['views']['見積依頼'])) {
            // 同種同効品数
            $sameItemCount = $this->getSameItemCount(
                $_fitem['MstFacilityItem']['master_id']);
            $this->set('sameItemCount', $sameItemCount);
        }
        
        // プルダウンの作成
        $this->_createPullDown();
        
        // 2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
        
        $this->set('data',$this->request->data);
    }
    
    
    /**
     * 施設採用品編集結果
     * @access public
     * @return void
     */
    function edit_result () {
        
        // トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            // プルダウンの作成
            $this->_createPullDown();
            $this->request->data = $this->Session->read('data');
            $this->Session->delete('data');
            $this->set('data',$this->request->data);
            $this->render('edit_result');
            return;
        }
        $now = date('Y/m/d H:i:s.u');

        // 重複チェック
        $result = $this->MstFacilityItem->chkDuplicateInternalCode($this->request->data['MstFacilityItem']['internal_code'],
                                                                   $this->request->data['MstFacilityItem']['id'],
                                                                   $this->Session->read('Auth.facility_id_selected'));
        if (!$result){
            $this->Session->setFlash('既に同じ商品IDが登録済みです。新しいIDに置き換えました', 'growl', array('type'=>'error') );

            // 商品ID採番しなおし
            $this->request->data['MstFacilityItem']['internal_code'] = $this->getNextInternalCode(substr($this->request->data['MstFacilityItem']['internal_code'] , 0 , 1) , $this->Session->read('Auth.facility_id_selected'));

            // 各種プルダウン
            $this->_createPullDown();
            // 2度押し対応トークン作成
            $this->request->data[$this->name]['token'] = $this->createToken($this->name);
            $this->render('edit');
            return;
        }

        // 施設採用品登録用にデータ整形
        $this->request->data['MstFacilityItem']['mst_dealer_id']         = $this->request->data['MstDealer']['id'];
        $this->request->data['MstFacilityItem']['dealer_code']           = $this->request->data['MstDealer']['dealer_code'];
        $this->request->data['MstFacilityItem']['modifier']              = $this->Session->read('Auth.MstUser.id');
        $this->request->data['MstFacilityItem']['modified']              = "'".$now."'";
        $this->request->data['MstFacilityItem']['is_deleted']            = false;
        $this->request->data['MstFacilityItem']['enable_medicalcode']    = (isset($this->request->data['MstFacilityItem']['enable_medicalcode'])?$this->request->data['MstFacilityItem']['enable_medicalcode']:null);
        $this->request->data['MstFacilityItem']['is_auto_storage']       = (isset($this->request->data['MstFacilityItem']['is_auto_storage'])?true:false);
        $this->request->data['MstFacilityItem']['buy_flg']               = ($this->request->data['MstFacilityItem']['buy_flg']==1?true:false);
        
        // トランザクション開始
        $this->MstFacilityItem->begin();
        // 行ロック
        $this->MstFacilityItem->query(' select * from mst_facility_items as a where a.id = ' . $this->request->data['MstFacilityItem']['id'] . '  for update ');
        // 更新チェック
        $ret = $this->MstFacilityItem->query(" select count(*) from mst_facility_items as a where a.id = " . $this->request->data['MstFacilityItem']['id'] . " and a.modified > '" . $this->request->data['MstFacilityItem']['time'] . "'");
        if( $ret[0][0]['count'] > 0 ){
            $this->MstFacilityItem->rollback();
            $this->Session->setFlash("別ユーザによって更新されています", 'growl', array('icon'=>'error'));
            $this->redirect('edit_search');
            return;
        }
        
        
        // 施設採用品更新
        if ($this->MstFacilityItem->validates($this->request->data['MstFacilityItem'])) {
            if ($this->MstFacilityItem->save($this->request->data['MstFacilityItem'],array('validate'=>false))) {
                
                // MstItemUnitがあるか？
                $tmp = $this->MstItemUnit->query(' select id from mst_item_units as a where a.mst_facility_item_id = ' . $this->request->data['MstFacilityItem']['id']);
                
                if(empty($tmp[0][0]['id'])){
                    // 新規単位 MstItemUnit作る
                    
                    $MstItemUnit = array(
                        'MstItemUnit'=> array(
                            'mst_facility_item_id' => $this->request->data['MstFacilityItem']['id'],
                            'mst_facility_id'      => $this->Session->read('Auth.facility_id_selected'),
                            'per_unit'             => 1,
                            'mst_unit_name_id'     => $this->request->data['MstTransactionConfig']['mst_unit_name_id'],
                            'start_date'           => $this->request->data['MstFacilityItem']['start_date'],
                            'end_date'             => '3000/01/01',
                            'is_standard'          => true,
                            'is_deleted'           => false,
                            'creater'              => $this->Session->read('Auth.MstUser.id'),
                            'created'              => $now,
                            'modifier'             => $this->Session->read('Auth.MstUser.id'),
                            'modified'             => $now,
                            'per_unit_name_id'     => $this->request->data['MstTransactionConfig']['mst_unit_name_id']
                            )
                        );
                    
                    $this->MstItemUnit->create();
                    if (!$this->MstItemUnit->save($MstItemUnit)) {
                        $this->MstFacilityItem->rollback();
                        $this->Session->setFlash('単位の作製中にエラーが発生しました', 'growl', array('type'=>'error') );
                        // 2度押し対策用にトランザクショントークンを作る
                        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                        $this->redirect('edit_search');
                    }
                    $this->request->data['MstTransactionConfig']['mst_item_unit_id'] = $this->MstItemUnit->getLastInsertID();
                    
                    // 仕入設定を新規登録
                    $MstTransactionConfig = array(
                        'MstTransactionConfig'=> array(
                            'mst_item_unit_id'         => $this->request->data['MstTransactionConfig']['mst_item_unit_id'],
                            'start_date'               => $this->request->data['MstFacilityItem']['start_date'],
                            'end_date'                 => '3000/01/01',
                            'mst_facility_relation_id' => $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $this->request->data['MstTransactionConfig']['partner_facility_id']),
                            'mst_facility_item_id'     => $this->request->data['MstFacilityItem']['id'],
                            'mst_facility_id'          => $this->Session->read('Auth.facility_id_selected'),
                            'partner_facility_id'      => $this->request->data['MstTransactionConfig']['partner_facility_id'],
                            'transaction_price'        => $this->request->data['MstTransactionConfig']['transaction_price'],
                            'is_deleted'               => false,
                            'creater'                  => $this->Session->read('Auth.MstUser.id'),
                            'created'                  => $now,
                            'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                            'modified'                 => $now,
                            )
                        );
                    
                    $this->MstTransactionConfig->create();
                    // SQL実行
                    if (!$this->MstTransactionConfig->save($MstTransactionConfig)) {
                        $this->MstFacilityItem->rollback();
                        $this->Session->setFlash('仕入設定の作製中にエラーが発生しました', 'growl', array('type'=>'error') );
                        // 2度押し対策用にトランザクショントークンを作る
                        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                        $this->redirect('edit_search');
                    }
                    
                    // 売上を追加
                    
                    // 新規登録
                    $hospitalId = $this->getHospitalId($this->Session->read('Auth.facility_id_selected'));
                    
                    // 売上設定を登録
                    $MstSalesConfig = array(
                        'MstSalesConfig' => array(
                            'mst_item_unit_id'         => $this->request->data['MstTransactionConfig']['mst_item_unit_id'],
                            'start_date'               => $this->request->data['MstFacilityItem']['start_date'],
                            'end_date'                 => '3000/01/01',
                            'mst_facility_relation_id' => $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $hospitalId),
                            'mst_facility_item_id'     => $this->request->data['MstFacilityItem']['id'],
                            'mst_facility_id'          => $this->Session->read('Auth.facility_id_selected'),
                            'partner_facility_id'      => $hospitalId,
                            'sales_price'              => $this->request->data['MstTransactionConfig']['transaction_price'],
                            'is_deleted'               => false,
                            'creater'                  => $this->Session->read('Auth.MstUser.id'),
                            'created'                  => $now,
                            'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                            'modified'                 => $now,
                            )
                        );
                    
                    $this->MstSalesConfig->create();
                    if (!$this->MstSalesConfig->save($MstSalesConfig)) {
                        $this->MstFacilityItem->rollback();
                        $this->Session->setFlash('売上設定の作製中にエラーが発生しました', 'growl', array('type'=>'error') );
                        // 2度押し対策用にトランザクショントークンを作る
                        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                        $this->redirect('edit_search');
                    }
                    
                }else{
                    // 更新
                    // 単位名
                    $UnitId = $tmp[0][0]['id'];
                    $this->MstItemUnit->create();
                    $res = $this->MstItemUnit->updateAll(
                        array(
                            'MstItemUnit.mst_unit_name_id' => $this->request->data['MstTransactionConfig']['mst_unit_name_id'] ,
                            'MstItemUnit.per_unit_name_id' => $this->request->data['MstTransactionConfig']['mst_unit_name_id'] ,
                            ),
                        array(
                            'MstItemUnit.id' => $UnitId,
                            ),
                        -1);
                    
                    if(false === $res){
                        $this->MstFacilityItem->rollback();
                        $this->Session->setFlash('単位の更新中にエラーが発生しました', 'growl', array('type'=>'error') );
                        // 2度押し対策用にトランザクショントークンを作る
                        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                        $this->redirect('edit_search');
                    }                    
                    // 仕入設定
                    $this->MstTransactionConfig->create();
                    $res = $this->MstTransactionConfig->updateAll(
                        array(
                            'MstTransactionConfig.start_date'               => "'" . $this->request->data['MstFacilityItem']['start_date'] . "'",
                            'MstTransactionConfig.mst_facility_relation_id' => $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $this->request->data['MstTransactionConfig']['partner_facility_id']),
                            'MstTransactionConfig.partner_facility_id'      => $this->request->data['MstTransactionConfig']['partner_facility_id'],
                            'MstTransactionConfig.transaction_price'        => $this->request->data['MstTransactionConfig']['transaction_price'],
                            ),
                        array(
                            'MstTransactionConfig.mst_item_unit_id' => $UnitId,
                            ),
                        -1);
                    
                    if(false === $res){
                        $this->MstFacilityItem->rollback();
                        $this->Session->setFlash('仕入設定の更新中にエラーが発生しました', 'growl', array('type'=>'error') );
                        // 2度押し対策用にトランザクショントークンを作る
                        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                        $this->redirect('edit_search');
                    }
                    
                    
                    // 売上を更新
                    $hospitalId = $this->getHospitalId($this->Session->read('Auth.facility_id_selected'));

                    $this->MstSalesConfig->create();
                    $res = $this->MstSalesConfig->updateAll(
                        array(
                            'MstSalesConfig.start_date'  => "'" . $this->request->data['MstFacilityItem']['start_date'] . "'",
                            'MstSalesConfig.sales_price' => $this->request->data['MstTransactionConfig']['transaction_price'],
                            ),
                        array(
                            'MstSalesConfig.mst_item_unit_id' => $UnitId,
                            ),
                        -1);
                    
                    if(false === $res){
                        $this->MstFacilityItem->rollback();
                        $this->Session->setFlash('売上設定の更新中にエラーが発生しました', 'growl', array('type'=>'error') );
                        // 2度押し対策用にトランザクショントークンを作る
                        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
                        $this->redirect('edit_search');
                    }
                }
                
                // コミット直前に更新チェック
                $ret = $this->MstFacilityItem->query(" select count(*) from mst_facility_items as a where a.id = " . $this->request->data['MstFacilityItem']['id'] . " and a.modified > '" . $this->request->data['MstFacilityItem']['time'] . "' and a.modified <> '" . $now . "'");
                if( $ret[0][0]['count'] > 0 ){
                    $this->MstFacilityItem->rollback();
                    $this->Session->setFlash("別ユーザによって更新されています", 'growl', array('icon'=>'error'));
                    $this->redirect('edit_search');
                    return;
                }

                // コミット
                $this->MstFacilityItem->commit();
                $this->Session->setFlash('Myマスタが正しく保存できました','growl',array('icon'=>'star'));
                // プルダウンの作成
                $this->_createPullDown();
                
                $this->Session->write('data', $this->request->data);

                $this->render('edit_result');
            } else {
                // 施設採用品更新失敗
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash('Myマスタの編集が完了できませんでした', 'growl', array('icon'=>'error'));
                $this->redirect('edit_search');
                return;
            }
        } else {
            // バリデートエラーのためロールバック
            $this->MstFacilityItem->rollback();
            $this->Session->setFlash('Myマスタの編集が完了できませんでした', 'growl', array('icon'=>'error'));
            $this->redirect('edit_search');
            return;
        }
    }

    /**
     * 医事コードメンテ検索
     */
    function mc_search() {
        App::import('Sanitize');
        $this->setRoleFunction(76); // 医事コードメンテ
        $this->_deleteLimitCount();
        $_fitems = array();
        if (isset($this->request->data['search']['is_search'])) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            if (false === $this->isSortCall) {
                $this->deleteSortInfo();
            }
            $_params = array();

            $where =  ' and a.start_date <= now() ' ;
            $where .= ' and ( a.end_date >= now() ' ;
            $where .= ' or a.end_date is null ) ' ;

            // 商品ID
            if (isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] != ''){
                $where .=" and a.internal_code LIKE '%" . $this->request->data['MstFacilityItem']['internal_code']. "%'";
            }
            // 商品名
            if (isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] != ''){
                $where .=" and a.item_name LIKE '%" . $this->request->data['MstFacilityItem']['item_name']. "%'";
            }
            // janコード
            if (isset($this->request->data['MstFacilityItem']['jan_code']) && $this->request->data['MstFacilityItem']['jan_code'] != ''){
                $where .=" and a.jan_code LIKE '" . $this->request->data['MstFacilityItem']['jan_code']. "%'";
            }
            // 規格
            if (isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] != ''){
                $where .=" and a.standard LIKE '" . $this->request->data['MstFacilityItem']['standard']. "%'";
            }
            // 製品番号
            if (isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] != ''){
                $where .=" and a.item_code LIKE '%" . $this->request->data['MstFacilityItem']['item_code']. "%'";
            }
            // 販売元
            if (isset($this->request->data['MstFacilityItem']['dealer_code']) && $this->request->data['MstFacilityItem']['dealer_code'] != ''){
                $where .=" and e.dealer_name LIKE '%" . $this->request->data['MstFacilityItem']['dealer_code']. "%'";
            }
            // 予備キー1
            if (isset($this->request->data['MstFacilityItem']['spare_key1']) && $this->request->data['MstFacilityItem']['spare_key1'] != ''){
                $where .=" and a.spare_key1 LIKE '%" . $this->request->data['MstFacilityItem']['spare_key1']. "%'";
            }
            // 予備キー2
            if (isset ($this->request->data['MstFacilityItem']['spare_key2']) && $this->request->data['MstFacilityItem']['spare_key2'] != ''){
                $where .=" and a.spare_key2 LIKE '%" . $this->request->data['MstFacilityItem']['spare_key2']. "%'";
            }
            // 予備キー3
            if (isset ($this->request->data['MstFacilityItem']['spare_key3'] ) && $this->request->data['MstFacilityItem']['spare_key3'] != ''){
                $where .=" and a.spare_key3 LIKE '%" . $this->request->data['MstFacilityItem']['spare_key3']. "%'";
            }
            // 得意先
            if (isset($this->request->data['MstFacilityItem']['customer']) && $this->request->data['MstFacilityItem']['customer'] != '') {
                $where .=" and MstFacilities.facility_code = '" . $this->request->data['MstFacilityItem']['customer']."'";
            }

            $sql  =' select ';
            $sql .= '  a.internal_code  as "MstFacilityItem__internal_code"';
            $sql .= ', a.item_name      as "MstFacilityItem__item_name" ';
            $sql .= ', a.item_code      as "MstFacilityItem__item_code"';
            $sql .= ', a.standard       as "MstFacilityItem__standard" ';
            $sql .= ', b.facility_name  as "MstFacilityItem__partner_facility_name"';
            $sql .= ', b.id             as "MstFacility__id"';
            $sql .= ', c.medical_code   as "MstFacilityItemMc__medical_code"';
            $sql .= ', d.dealer_name    as "MstDealer__dealer_name" ';
            $sql .= ', c.id             as "MstFacilityItemMc__id"';
            $sql .= ', a.id             as "MstFacilityItem__id"';
            $sql .=    'from';
            $sql .= ' mst_facility_items as  a ';
            $sql .= '  left join mst_facilities as b ';
            $sql .=  ' on b.id in ( ';
            $sql .= ' select ';
            $sql .=   ' x.partner_facility_id ';
            $sql .=      ' from ';
            $sql .=    'mst_facility_relations as x ';
            $sql .=       ' where ';
            $sql .=       ' x.mst_facility_id =  ' . $this->Session->read('Auth.facility_id_selected');
            $sql .=       ' and x.is_deleted = false ';
            $sql .=    ' ) ';
            $sql .=  ' and b.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .=  ' and b.is_deleted = false ';
            $sql .=  ' left join mst_medical_codes as c ';
            $sql .=  '  on a.id = c.mst_facility_item_id ';
            $sql .=  ' and c.mst_facility_id = b.id';
            $sql .=  ' left join mst_dealers as d ';
            $sql .=  '  on a.mst_dealer_id = d.id ';
            $sql .=  ' inner join mst_user_belongings as e ';
            $sql .=  '  on e.mst_facility_id = b.id ';
            $sql .=  '  and e.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .=  '  and e.is_deleted = false';
            $sql .=  ' where';
            $sql .=  ' a.item_type = '. Configure::read('Items.item_types.normalitem');
            $sql .=  ' and a.is_deleted = false ';
            $sql .=  ' and a.enable_medicalcode > 1 ';
            $sql .=  ' and a.mst_facility_id = '. $this->Session->read('Auth.facility_id_selected');
            $sql .=    $where ;
            $sql .=        ' order by a.modified DESC, a.mst_facility_id ASC ' ;

            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));

            $sql .= " limit " . $this->request->data['limit'];

            $_fitems = $this->MstFacilityItem->query($sql);
            $this->request->data = $data;
        }

        // 検索条件用得意先プルダウン作成
        $_customers = $this->getFacilityList($this->Session->read('Auth.facility_id_selected')
                                             , array(Configure::read('FacilityType.hospital'))
                                             , true
                                             );

        $this->set('customers', $_customers);
        $this->set('fitems', $_fitems);
        $this->set('data', $this->request->data);
        $this->Session->write('FacilityItems.customers', $_customers);
    }

    /**
     * 医事コード編集
     */
    function mc_edit () {

        $result = array();
        // 更新時間チェック用に画面を開いた時間を保存 
        $this->Session->write('MedicalCode.readTime',date('Y-m-d H:i:s'));
        $facility_item_id = null ;
        $facility_id = null;
        list($facility_item_id , $facility_id) = explode('_' , $this->request->data['MstFacilityItem']['opt'] );
        
        $sql  ='select';
        $sql .='      c.id                              as "MstFacilityItem__facility_id" ';
        $sql .='    , c.facility_name                   as "MstFacilityItem__customer_name" ';
        $sql .='    , a.id                              as "MstFacilityItem__id" ';
        $sql .='    , a.internal_code                   as "MstFacilityItem__internal_code" ';
        $sql .='    , a.item_name                       as "MstFacilityItem__item_name" ';
        $sql .='    , a.item_code                       as "MstFacilityItem__item_code"';
        $sql .='    , d.dealer_name                     as "MstDealer__dealer_name" ';
        $sql .='    , a.standard                        as "MstFacilityItem__standard" ';
        $sql .='    , a.jan_code                        as "MstFacilityItem__jan_code" ';
        $sql .='    , coalesce(b.medical_code , a.medical_code) as "MstFacilityItem__medical_code" ';
        $sql .='    , b.id                              as "MstFacilityItem__medical_id" ';
        $sql .='    , a.refund_price                    as "MstFacilityItem__refund_price"';
        $sql .='    , e.insurance_claim_name_s          as "MstFacilityItem__insurance_claim_name_s"';
        $sql .='    , a.biogenous_type                  as "MstFacilityItem__biogenous_type" ';
        $sql .='    , f.const_nm                        as "MstFacilityItem__insurance_claim_department_name"';
        $sql .='    , g.const_nm                        as "MstFacilityItem__biogenous_type_name"';
        
        $sql .='  from ';
        $sql .='    mst_facility_items as a ';
        $sql .='    left join mst_facilities as c ';
        $sql .='      on c.id = '. $facility_id ;
        $sql .='    left join mst_medical_codes as b ';
        $sql .='      on a.id = b.mst_facility_item_id ';
        $sql .='      and b.mst_facility_id = c.id ';
        $sql .='    left join mst_dealers as d ';
        $sql .='      on d.id = a.mst_dealer_id ';
        $sql .='    left join mst_insurance_claims as e ';
        $sql .='      on e.insurance_claim_code = a.insurance_claim_code ';
        $sql .='    left join mst_consts as f ';
        $sql .='      on f.const_cd = a.insurance_claim_type::varchar ';
        $sql .="      and f.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
        $sql .='    left join mst_consts as g ';
        $sql .='      on g.const_cd = a.biogenous_type ';
        $sql .="      and g.const_group_cd = '" . Configure::read('ConstGroupType.biogenousTypes') . "'";
        $sql .='  where ';
        $sql .='    a.item_type = ' . Configure::read('Items.item_types.normalitem') ;
        $sql .='    and a.id = ' . $facility_item_id ;
        $result = $this->MstFacilityItem->query($sql);
        
        // 2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
        $this->set('data',$result[0]);
    }

    /**
     * 医事コードメンテ編集結果
     */
    function mc_edit_result () {
        
        // トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            $this->set('data',$this->request->data );
            $this->render('mc_edit_result');
            return;
        }
        // トランザクション開始
        $this->MstFacilityItem->begin();
        // 行ロック
        $this->MstFacilityItem->query( ' select * from mst_facility_items as a where a.id = ' . $this->request->data['MstFacilityItem']['id'] . ' for update ');

        $read_time = $this->Session->read('MedicalCode.readTime');

        $this->request->data['now'] = date('Y/m/d H:i:s.u');
        $_res = array();

        if(empty($this->request->data['MstFacilityItem']['medical_id'])){
            // 新規
            $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
            $save_data['created'] = $this->request->data['now'] = date('Y/m/d H:i:s.u');;
            
        }else {
            // 更新
            $save_data['id'] = $this->request->data['MstFacilityItem']['medical_id']; // ID
        }
        $save_data['medical_code']         = $this->request->data['MstFacilityItem']['medical_code'];
        $save_data['mst_facility_item_id'] = $this->request->data['MstFacilityItem']['id'];;
        $save_data['mst_facility_id']      = $this->request->data['MstFacilityItem']['facility_id'];
        $save_data['is_deleted']           = false ;
        $save_data['modifier']             = $this->Session->read('Auth.MstUser.id');
        $save_data['modified']             = $this->request->data['now'] = date('Y/m/d H:i:s.u');;
        
        $this->MstMedicalCode->create();
        
        if ($this->MstMedicalCode->save($save_data)) {
            $this->MstFacilityItem->commit();

            $this->set('data',$this->request->data );
        } else {
            $this->MstFacilityItem->rollback();
            $this->Session->setFlash('データが保存できませんでした。', 'growl', array('type'=>'error') );
            
            // 2度押し対策用にトランザクショントークンを作る
            $this->request->data[$this->name]['token'] = $this->createToken($this->name);
            $this->redirect('mc_edit');
        }
    }

    /**
     * セット品初期画面
     */
    function set_items(){
        App::import('Sanitize');
        $this->setRoleFunction(77); // セット品
        $result = array();
        if(isset($this->request->data['search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $where = "";
            if(isset($this->request->data['MstFacilityItems']['internal_code']) && $this->request->data['MstFacilityItems']['internal_code'] !== ''){
                $where .=" and a.internal_code ilike '%" .$this->request->data['MstFacilityItems']['internal_code']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['standard']) && $this->request->data['MstFacilityItems']['standard'] !== '' ){
                $where .=" and a.standard ilike '%" .$this->request->data['MstFacilityItems']['standard']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['item_name']) && $this->request->data['MstFacilityItems']['item_name'] !== ''){
                $where .=" and a.item_name ilike '%" .$this->request->data['MstFacilityItems']['item_name']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['item_code']) && $this->request->data['MstFacilityItems']['item_code'] !== ''){
                $where .=" and a.item_code ilike '%" .$this->request->data['MstFacilityItems']['item_code']."%'";
            }
            $sql  = ' select ';
            $sql .= '     a.id            as "MstFacilityItem__id", ';
            $sql .= '     a.internal_code as "MstFacilityItem__internal_code", ';
            $sql .= '     a.item_name     as "MstFacilityItem__item_name", ';
            $sql .= '     a.standard      as "MstFacilityItem__standard", ';
            $sql .= '     a.item_code     as "MstFacilityItem__item_code" ';
            $sql .= ' from  ';
            $sql .= '    mst_facility_items as a ';
            $sql .= ' where  ';
            $sql .= '     a.item_type = ' . Configure::read('Items.item_types.setitem');
            $sql .= '     and a.is_deleted = false ';
            $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= $where;
            $sql .= ' order by a.item_name, a.internal_code, a.id ';

            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));

            $sql .= ' limit ' .$this->request->data['limit'];
            $result = $this->MstFacilityItem->query($sql);
            $this->request->data = $data;
        }
        $this->set('result' , $result);
        $this->set('data',$this->request->data );
    }

    /**
     * セット品新規追加
     */
    function add_set_items(){

        $SearchResult = array();
        $CartSearchResult = array();

        if(isset($this->request->data['add_search']['is_search'])){
            $limit = '' ;
            if(isset($this->request->data['SetItems']['limit'])){
                $limit .= 'limit ' . $this->request->data['SetItems']['limit'];
            }

            $order =' order by a.internal_code ,b.per_unit ';

            $where = '';
            $where .=' and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
            $where .=' and a.item_type = ' . Configure::read('Items.item_types.normalitem'); // 通常品のみ
            $where .=' and a.is_deleted = false ';
            $where .=' and b.is_deleted = false ';
            $where .=' and a.is_lowlevel = false '; // 低レベル品は除外

            $where .=" and a.start_date <= now()"; // 施設採用品開始日
            $where .=" and ( a.end_date >= now()"; // 施設採用品終了日
            $where .=' or a.end_date is null ) ';

            $where .=" and b.start_date <= now()"; // 包装単位開始日
            $where .=" and ( b.end_date >= now()"; // 包装単位終了日
            $where .=' or b.end_date is null ) ';

            $where2 = $where;

            if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and b.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 .= " and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 .= " and 0 = 1 ";
            }


            if(isset($this->request->data['MstFacilityItems']['internal_code']) && $this->request->data['MstFacilityItems']['internal_code'] !== ''){
                $where .=" and a.internal_code ilike '%" .$this->request->data['MstFacilityItems']['internal_code']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['standard']) && $this->request->data['MstFacilityItems']['standard'] !== '' ){
                $where .=" and a.standard ilike '%" .$this->request->data['MstFacilityItems']['standard']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['item_name']) && $this->request->data['MstFacilityItems']['item_name'] !== ''){
                $where .=" and a.item_name ilike '%" .$this->request->data['MstFacilityItems']['item_name']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['item_code']) && $this->request->data['MstFacilityItems']['item_code'] !== ''){
                $where .=" and a.item_code ilike '%" .$this->request->data['MstFacilityItems']['item_code']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['dealer_name']) && $this->request->data['MstFacilityItems']['dealer_name'] !== ''){
                $where .=" and c.dealer_name ilike '%" .$this->request->data['MstFacilityItems']['dealer_name']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['jan_code']) && $this->request->data['MstFacilityItems']['jan_code'] !== ''){
                $where .=" and a.jan_code ilike '" .$this->request->data['MstFacilityItems']['jan_code']."%'";
            }

            // 検索（画面上）用一覧取得
            $SearchResult = $this->MstFacilityItem->query($this->searchSetItems($where , $order , $limit));

            // カート（画面下）用一覧取得
            $CartSearchResult = $this->MstFacilityItem->query($this->searchSetItems($where2 , $order ));

        }

        $this->set('SearchResult' , $SearchResult);
        $this->set('CartSearchResult' , $CartSearchResult);

        $this->set('data',$this->request->data );
    }

    /**
     * セット品新規追加確認
     */
    function add_set_items_confirm(){
        $where  ='     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
        $where .='     and a.item_type = ' . Configure::read('Items.item_types.normalitem');
        $where .='     and a.is_deleted = false ';
        $where .='     and b.is_deleted = false ';
        $where .='     and b.id in (' . $this->request->data['cart']['tmpid'] . ' ) ';

        $order ='   order by a.internal_code ,b.per_unit ';

        $result = $this->MstFacilityItem->query($this->searchSetItems($where , $order));

        $this->request->data['MstSetItems']['set_id'] =  $this->getNextInternalCode(Configure::read('ItemType.setItem') ,
                                                                                    $this->Session->read('Auth.facility_id_selected'));

        
        // 2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);

        $this->set('result' , $result);
        $this->set('data',$this->request->data );
    }

    /**
     * セット品新規追加結果
     */
    function add_set_items_result(){
        
        // トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            $result = $this->Session->read('result');
            $this->Session->delete('result');
            $this->set('result' , $result);
            $this->request->data = $this->Session->read('data');
            $this->Session->delete('data');
            $this->set('data',$this->request->data );
            $this->render('add_set_items_result');
            return;
        }

        $where = '';
        if(!$this->add_setItem_regist($this->request->data)){
            // 登録失敗
            $this->Session->setFlash('登録が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );

            $where  =' and 1 = 0 ' ;
        }

        $where .=' and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
        $where .=' and a.item_type = ' . Configure::read('Items.item_types.normalitem');
        $where .=' and a.is_deleted = false ';
        $where .=' and b.is_deleted = false ';
        $where .=' and b.id in (' . implode("," ,  $this->request->data['MstSetItems']['id']) . ' ) ';

        $order ='   order by a.internal_code ,b.per_unit ';

        $result = $this->MstFacilityItem->query($this->searchSetItems($where , $order));

        $this->set('result' , $result);
        $this->Session->write('result', $result);

        $this->set('data',$this->request->data );
        $this->Session->write('data', $this->request->data );
    }


    /**
     * セット品新規登録
     */
    function add_setItem_regist($data){

        // トランザクション開始
        $this->MstFacilityItem->begin();

        // ヘッダ登録
        // 施設採用品 Insert
        $header_data = array(
            // 施設採用品
            'MstFacilityItem' => array(
                'mst_facility_id'  => $this->Session->read('Auth.facility_id_selected'),
                'internal_code'    => $data['MstSetItems']['set_id'],
                'item_name'        => $data['MstSetItems']['set_name'],
                'standard'         => $data['MstSetItems']['standard'],
                'item_code'        => $data['MstSetItems']['item_code'],
                'jan_code'         => '',
                'mst_unit_name_id' => 9, // ｾｯﾄ を入れておく
                'item_type'        => Configure::read('Items.item_types.setitem'),
                'start_date'       => '1970/01/01',
                'converted_num'    => 1,
                'is_deleted'       => false,
                'creater'          => $this->Session->read('Auth.MstUser.id'),
                'modifier'         => $this->Session->read('Auth.MstUser.id'),
                )
            );

        // ヘッダオブジェクトをcreate
        $this->MstFacilityItem->create();

        // SQL実行
        if (!$this->MstFacilityItem->save($header_data)) {
            // RollBack
            $this->MstFacilityItem->rollback();
            return false;
        }

        // 最後に挿入したIDを取得
        $item_id = $this->MstFacilityItem->getLastInsertID();

        // 明細登録
        $i = 0;
        foreach($data['MstSetItems']['id'] as $id ) {
            $setitem_data[] = array(
                'MstSetDetail' => array(
                    'set_item_unit_id'         => $item_id,
                    'mst_item_unit_id'         => $id,
                    'quantity'                 => $data['MstSetItems']['quantity'][$i],
                    'is_deleted'               => FALSE,
                    'creater'                  => $this->Session->read('Auth.MstUser.id'),
                    'modifier'                 => $this->Session->read('Auth.MstUser.id')
                    )
                );
            $i++;
        }
        $this->MstSetDetail->create();

        // SQL実行
        if (!$this->MstSetDetail->saveAll($setitem_data , array('validates' => true,'atomic' => false))) {
            // RollBack
            $this->MstFacilityItem->rollback();
            return false;
        }

        $itemunit_data = array(
            // 包装単位
            'MstItemUnit' => array(
                'mst_facility_item_id' => $item_id ,
                'mst_facility_id'      => $this->Session->read('Auth.facility_id_selected'),
                'per_unit'             => 1,
                'mst_unit_name_id'     => 9,
                'start_date'           => '1970/01/01',
                'is_standard'          => true ,
                'is_deleted'           => false,
                'creater'              => $this->Session->read('Auth.MstUser.id'),
                'modifier'             => $this->Session->read('Auth.MstUser.id'),
                )
            );

        $itemunit_data['MstItemUnit']['per_unit_name_id'] = 9;
        // 包装単位

        $this->MstItemUnit->create();
        // SQL実行
        $ret = $this->MstItemUnit->save($itemunit_data , array('validates' => true,'atomic' => false));
        if (!$ret) {
            // 包装単位の更新失敗
            $this->MstFacilityItem->rollback();
            return false;
        }
        $itemunit_id = $this->MstFacilityItem->getLastInsertID();

        // 在庫レコード作成
        if (!$this->getStockRecode($itemunit_id)) {
            // 在庫の更新失敗
            $this->MstFacilityItem->rollback();
            return false;
        }

        // コミット
        $this->MstFacilityItem->commit();

        return true;
    }


    /**
     * セット品編集
     */
    function edit_set_items(){
        $sql  = 'select ';
        $sql .= '      a.internal_code as "MstSetItemHeader__internal_code" ';
        $sql .= '    , a.item_name     as "MstSetItemHeader__item_name" ';
        $sql .= '    , a.standard      as "MstSetItemHeader__standard" ';
        $sql .= '    , a.item_code     as "MstSetItemHeader__item_code" ';
        $sql .= '    , d.internal_code as "MstSetItems__internal_code" ';
        $sql .= '    , d.item_name     as "MstSetItems__item_name"';
        $sql .= '    , d.standard      as "MstSetItems__standard"';
        $sql .= '    , d.item_code     as "MstSetItems__item_code" ';
        $sql .= '    , e.dealer_name   as "MstSetItems__dealer_name" ';
        $sql .= '    , b.quantity      as "MstSetItems__quantity" ';
        $sql .= '    , ( case ';
        $sql .= '        when c.per_unit = 1 then f.unit_name ';
        $sql .= "        else f.unit_name || '(' || c.per_unit || g.unit_name || ')' ";
        $sql .= '        end )         as "MstSetItems__unit_name"';
        $sql .= '  from ';
        $sql .= '    mst_facility_items as a  ';
        $sql .= '    left join mst_set_details as b  ';
        $sql .= '      on a.id = b.set_item_unit_id  ';
        $sql .= '    left join mst_item_units as c  ';
        $sql .= '      on c.id = b.mst_item_unit_id  ';
        $sql .= '    left join mst_facility_items as d  ';
        $sql .= '      on d.id = c.mst_facility_item_id  ';
        $sql .= '    left join mst_dealers as e  ';
        $sql .= '      on e.id = d.mst_dealer_id  ';
        $sql .= '    left join mst_unit_names as f  ';
        $sql .= '      on f.id = c.mst_unit_name_id  ';
        $sql .= '    left join mst_unit_names as g  ';
        $sql .= '      on g.id = c.per_unit_name_id  ';
        $sql .= '  where ';
        $sql .= '    a.item_type = ' . Configure::read('Items.item_types.setitem');
        $sql .= '    and a.id = ' . $this->request->data['MstFacilityItem']['id'];

        $result = $this->MstFacilityItem->query($sql);
        $this->set('result' , $result);
        $this->set('data',$this->request->data );
    }


    /**
     * セット品編集結果
     */
    function edit_set_items_result(){
        if(!$this->edit_setItem_regist($this->request->data)){
            // 登録失敗
            $this->Session->setFlash('登録が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
        }

        $sql  = 'select ';
        $sql .= '      a.internal_code as "MstSetItemHeader__internal_code" ';
        $sql .= '    , a.item_name     as "MstSetItemHeader__item_name" ';
        $sql .= '    , a.standard      as "MstSetItemHeader__standard" ';
        $sql .= '    , a.item_code     as "MstSetItemHeader__item_code" ';
        $sql .= '    , d.internal_code as "MstSetItems__internal_code" ';
        $sql .= '    , d.item_name     as "MstSetItems__item_name"';
        $sql .= '    , d.standard      as "MstSetItems__standard"';
        $sql .= '    , d.item_code     as "MstSetItems__item_code" ';
        $sql .= '    , e.dealer_name   as "MstSetItems__dealer_name" ';
        $sql .= '    , b.quantity      as "MstSetItems__quantity" ';
        $sql .= '    , ( case ';
        $sql .= '        when c.per_unit = 1 then f.unit_name ';
        $sql .= "        else f.unit_name || '(' || c.per_unit || g.unit_name || ')' ";
        $sql .= '        end )         as "MstSetItems__unit_name"';
        $sql .= '  from ';
        $sql .= '    mst_facility_items as a  ';
        $sql .= '    left join mst_set_details as b  ';
        $sql .= '      on a.id = b.set_item_unit_id  ';
        $sql .= '    left join mst_item_units as c  ';
        $sql .= '      on c.id = b.mst_item_unit_id  ';
        $sql .= '    left join mst_facility_items as d  ';
        $sql .= '      on d.id = c.mst_facility_item_id  ';
        $sql .= '    left join mst_dealers as e  ';
        $sql .= '      on e.id = d.mst_dealer_id  ';
        $sql .= '    left join mst_unit_names as f  ';
        $sql .= '      on f.id = c.mst_unit_name_id  ';
        $sql .= '    left join mst_unit_names as g  ';
        $sql .= '      on g.id = c.per_unit_name_id  ';
        $sql .= '  where ';
        $sql .= '    a.item_type = ' . Configure::read('Items.item_types.setitem');
        $sql .= '    and a.id = ' . $this->request->data['MstFacilityItem']['id'];

        $result = $this->MstFacilityItem->query($sql);
        $this->set('result' , $result);
        $this->set('data',$this->request->data );
    }
    /**
     * セット品編集登録
     */
    function edit_setItem_regist($data){
        // Begin
        $this->MstFacilityItem->begin();
        // 行ロック
        $this->MstFacilityItem->query('select * from mst_facility_items as a where a.id = ' . $data['MstFacilityItem']['id'] . ' for update ');

        // ヘッダ登録
        // 施設採用品 update
        $header_data = array(
            // 施設採用品
            'MstFacilityItem' => array(
                'id'               => $data['MstFacilityItem']['id'],
                'mst_facility_id'  => $this->Session->read('Auth.facility_id_selected'),
                'internal_code'    => $data['MstSetItems']['set_id'],
                'item_name'        => $data['MstSetItems']['set_name'],
                'standard'         => $data['MstSetItems']['standard'],
                'item_code'        => $data['MstSetItems']['item_code'],
                'jan_code'         => '',
                'mst_unit_name_id' => 9, // ｾｯﾄ を入れておく
                'item_type'        => Configure::read('Items.item_types.setitem'),
                'start_date'       => '1900/01/01',
                'converted_num'    => 1,
                'is_deleted'       => false,
                'modifier'         => $this->Session->read('Auth.MstUser.id'),
                )
            );

        // ヘッダオブジェクトをcreate
        $this->MstFacilityItem->create();

        // SQL実行
        if (!$this->MstFacilityItem->save($header_data)) {
            // RollBack
            $this->MstFacilityItem->rollback();
            return false;
        }

        $this->MstFacilityItem->commit();
        return true;

    }

    public function seal(){
        // コストシールデータ取得
        $records = $this->Stickers->getCostStickers(array($this->request->data['MstFacilityItem']['id']) , 3);
        // 空なら初期化
        if(!$records){ $records = array();}

        $this->layout = 'xml/default';
        $this->set('records', $records);
        $this->render('/stickerissues/cost_sticker');
    }

    private function changeMasterId($item ,$gmItem ){
        if(!is_null($gmItem)){
            // 商品IDが独自採用品のもの「9」から始まり、続く文字列が数字の並びなら先頭の文字を「1」に変更
            $pattern = '/^('.Configure::read('ItemType.originalItem').')([0-9]+)/i';
            $replacement = Configure::read('ItemType.masterItem') . '$2';
            $item['MstFacilityItem']['internal_code']         =  preg_replace($pattern, $replacement, $item['MstFacilityItem']['internal_code']);

            $item['MstFacilityItem']['standard']              = $gmItem['MstFacilityItem']['standard'];
            $item['MstFacilityItem']['insurance_claim_type']  = $gmItem['MstFacilityItem']['insurance_claim_type'];
            $item['MstFacilityItem']['item_code']             = $gmItem['MstFacilityItem']['item_code'];
            $item['MstFacilityItem']['class_separation']      = $gmItem['MstFacilityItem']['class_separation'];

            $item['MstFacilityItem']['item_name']             = $gmItem['MstFacilityItem']['item_name'];
            $item['MstFacilityItem']['insurance_claim_code']  = $gmItem['MstFacilityItem']['insurance_claim_code'];
            $item['MstFacilityItem']['jan_code']              = trim($gmItem['MstFacilityItem']['jan_code']);
            $item['MstFacilityItem']['unit_price']            = $gmItem['MstFacilityItem']['unit_price'];
            $item['MstFacilityItem']['refund_price']          = $gmItem['MstFacilityItem']['refund_price'];
            $item['MstFacilityItem']['biogenous_type']        = $gmItem['MstFacilityItem']['biogenous_type'];
            $item['MstFacilityItem']['tax_type']              = $gmItem['MstFacilityItem']['tax_type'];

            $item['MstFacilityItem']['unit_name'] = $gmItem['MstFacilityItem']['unit_name'];

            $mstDealer = $this->MstDealer->find('first' ,
                                                array(
                                                    'conditions' => array(
                                                        'MstDealer.dealer_code' => trim($gmItem['dealer_code']),
                                                        'MstDealer.start_date <= now()',
                                                        '(MstDealer.end_date > now() or MstDealer.end_date is null)',
                                                        'MstDealer.is_deleted = false'

                                                        )
                                                    )
                                                );
            if(!empty($mstDealer)){
                $item['MstDealer']['id']          = $mstDealer['MstDealer']['id'];
                $item['MstDealer']['dealer_name'] = $mstDealer['MstDealer']['dealer_name'];
                $item['MstDealer']['dealer_code'] = $mstDealer['MstDealer']['dealer_code'];
            }else{
                $item['MstDealer']['id']          = '';
                $item['MstDealer']['dealer_name'] = '';
                $item['MstDealer']['dealer_code'] = '';
            }
        }
        return $item;
    }
    private function searchSetItems($where = null , $order = null , $limit = null ){

        $sql = '';
        $sql .=' select ';
        $sql .='       b.id            as "MstSetItems__id"';
        $sql .='     , a.internal_code as "MstSetItems__internal_code"';
        $sql .='     , a.item_name     as "MstSetItems__item_name"';
        $sql .='     , a.standard      as "MstSetItems__standard"';
        $sql .='     , a.item_code     as "MstSetItems__item_code"';
        $sql .='     , c.dealer_name   as "MstSetItems__dealer_name"';
        $sql .='     , b.per_unit      as "MstSetItems__per_unit"';
        $sql .='     , e.unit_name     as "MstSetItems__per_unit_name"  ';
        $sql .='     , ( case ';
        $sql .='         when b.per_unit = 1 then d.unit_name ';
        $sql .="         else d.unit_name || '(' || b.per_unit || e.unit_name || ')' ";
        $sql .='         end )         as "MstSetItems__unit_name"';
        $sql .='   from ';
        $sql .='     mst_facility_items as a  ';
        $sql .='     left join mst_item_units as b  ';
        $sql .='       on a.id = b.mst_facility_item_id  ';
        $sql .='     left join mst_dealers as c  ';
        $sql .='       on c.id = a.mst_dealer_id  ';
        $sql .='     left join mst_unit_names as d  ';
        $sql .='       on d.id = b.mst_unit_name_id  ';
        $sql .='     left join mst_unit_names as e  ';
        $sql .='       on e.id = b.per_unit_name_id  ';
        if(!is_null($where)){
            $sql .=' where 1 = 1 ';
            $sql .=' ' . $where;
        }
        if(!is_null($order)){
            $sql .=' ' . $order;
        }
        if(!is_null($limit)){
            $sql .=' ' .$limit;
        }
        return $sql;
    }


    /**
     * 次の施設採用品の商品IDを返す
     */
    private function getNextInternalCode($prefix = '' , $facility_id = null){
        $suffix = '';
        // $sql="select max(substr(internal_code , 2)) from mst_facility_items where internal_code SIMILAR TO '[A-Z][0-9]+' and mst_facility_id = " . $facility_id;
        $sql="select max(substr(internal_code , 2)) from mst_facility_items where mst_facility_id = " . $facility_id;
        // 製品番号のMAXを取得
        $_max_internal = $this->MstFacilityItem->query($sql);
        $_max = (int)(ltrim($_max_internal[0][0]['max'],'0'));
        $_max++;
        return  $prefix . str_pad($_max,($this->Session->read('Auth.Config.CodeLength') -1),'0',STR_PAD_LEFT) . $suffix;
    }


    // 施設採用品（mst_facility_items）から値を取得
    private function _getFacilityItem($id = null){
        if(is_null($id)){
            return array();
        }
        $sql  =' select ';
        $sql .='       a.id                                   as "MstFacilityItem__id" ';
        $sql .='     , a.mst_facility_id                      as "MstFacilityItem__mst_facility_id" ';
        $sql .='     , a.internal_code                        as "MstFacilityItem__internal_code" ';
        $sql .='     , a.item_name                            as "MstFacilityItem__item_name" ';
        $sql .='     , a.standard                             as "MstFacilityItem__standard" ';
        $sql .='     , a.item_code                            as "MstFacilityItem__item_code" ';
        $sql .='     , a.jan_code                             as "MstFacilityItem__jan_code" ';
        $sql .='     , a.old_jan_code                         as "MstFacilityItem__old_jan_code" ';
        $sql .='     , a.refund_price                         as "MstFacilityItem__refund_price" ';
        $sql .='     , a.unit_price                           as "MstFacilityItem__unit_price" ';
        $sql .='     , to_char(a.start_date , \'YYYY/mm/dd\') as "MstFacilityItem__start_date" ';
        $sql .='     , to_char(a.end_date , \'YYYY/mm/dd\')   as "MstFacilityItem__end_date" ';
        $sql .='     , a.converted_num                        as "MstFacilityItem__converted_num" ';
        $sql .='     , a.biogenous_type                       as "MstFacilityItem__biogenous_type" ';
        $sql .='     , a.class_separation                     as "MstFacilityItem__class_separation" ';
        $sql .='     , a.tax_type                             as "MstFacilityItem__tax_type" ';
        $sql .='     , a.master_id                            as "MstFacilityItem__master_id" ';
        $sql .='     , a.mst_unit_name_id                     as "MstFacilityItem__basic_unit_name_id" ';
        $sql .='     , a.expired_date                         as "MstFacilityItem__expired_date" ';
        $sql .='     , round(a.refund_converted_num)          as "MstFacilityItem__refund_converted_num"';
        $sql .='     , a.pharmaceutical_number                as "MstFacilityItem__pharmaceutical_number"';
        $sql .='     , a.parts_use                            as "MstFacilityItem__parts_use"';
        $sql .='     , a.keep_type                            as "MstFacilityItem__keep_type"';
        $sql .='     , a.recital                              as "MstFacilityItem__recital" ';
        for($i=1;$i<=30;$i++){
            $sql .= '    , a.spare_key'.$i.'                            as "MstFacilityItem__spare_key'.$i.'" ';
        }
        $sql .='     , c.id                                   as "MstDealer__id"';
        $sql .='     , c.dealer_name                          as "MstDealer__dealer_name"';
        $sql .='     , c.dealer_code                          as "MstDealer__dealer_code"';
        $sql .='     , a.insurance_claim_type                 as "MstFacilityItem__insurance_claim_type"';
        $sql .='     , a.item_category1                       as "MstFacilityItem__item_category1"';
        $sql .='     , a.item_category2                       as "MstFacilityItem__item_category2"';
        $sql .='     , a.item_category3                       as "MstFacilityItem__item_category3"';
        $sql .='     , f.transaction_price                    as "MstTransactionConfig__transaction_price"';
        $sql .='     , f.partner_facility_id                  as "MstTransactionConfig__partner_facility_id"';
        $sql .='     , j.unit_name                            as "MstTransactionConfig__unit_name"';
        $sql .='     , f.mst_item_unit_id                     as "MstTransactionConfig__mst_item_unit_id"';
        $sql .='     , j.id                                   as "MstTransactionConfig__mst_unit_name_id"';
        $sql .='     , a.mst_unit_name_id                     as "MstFacilityItem__mst_unit_name_id"';
        $sql .='     , h.unitnam                              as "MstFacilityItem__unit_name"';
        $sql .='     , k.jmdn_code                            as "MstFacilityItem__jmdn_code" ';
        $sql .='     , k.jmdn_name                            as "MstFacilityItem__jmdn_name" ';
        $sql .='     , a.medical_code                         as "MstFacilityItem__medical_code"';
        $sql .='     , a.insurance_claim_code                 as "MstFacilityItem__insurance_claim_code_hidden"';
        $sql .='     , a.insurance_claim_code                 as "MstFacilityItem__insurance_claim_code"';
        $sql .='     , e.depclcalname                         as "MstFacilityItem__insurance_claim_name"';
        $sql .='   from ';
        $sql .='     mst_facility_items as a  ';
        $sql .='     left join mst_dealers as c ';
        $sql .='       on c.id = a.mst_dealer_id ';
        $sql .='     left join mst_grand_dep_cl_cds          as e on e.rezcd = a.insurance_claim_code';
        $sql .='     left join mst_transaction_configs as f ';
        $sql .='       on f.mst_facility_item_id = a.id ';
        $sql .='      and f.is_deleted = false ';
        $sql .='     left join mst_grand_mat_prices          as g on g.matcd                = a.master_id ';
        $sql .='     left join mst_grand_units               as h on h.unitcd               = g.unit1 ';
        $sql .='     left join mst_item_units                as i on i.id                   = f.mst_item_unit_id ';
        $sql .='     left join mst_unit_names as j ';
        $sql .='       on j.id = i.mst_unit_name_id ';
        
        $sql .='     left join mst_jmdn_names                as k on k.jmdn_code            = a.jmdn_code ';
        
        $sql .='   where ';
        $sql .='     a.id = ' . $id ;

        $tmp = $this->MstFacilityItem->query($sql);
        return $tmp[0]; 
    }


    private function _MstGrandmaster($matcd = null){
        if(is_null($matcd)){
            return array();
        }

        $sql  =' select ';
        $sql .='       a.matname                 as "MstFacilityItem__item_name" ';
        // 規格
        $sql .='     , a.sizdimnam               as "MstFacilityItem__standard" ';
        // 製造番号
        $sql .='     , a.mkmatcd                 as "MstFacilityItem__item_code" ';
        // JANコード
        $sql .='     , trim(a.stdjancd)          as "MstFacilityItem__jan_code" ';
        // 償還価格
        $sql .='     , c.retprc                  as "MstFacilityItem__refund_price" ';
        // 償還係数
        $sql .='     , round(b.depclcoef)        as "MstFacilityItem__refund_converted_num" ';
        // 定価
        $sql .='     , d.prc1                    as "MstFacilityItem__unit_price" ';
        // 生物由来区分
        $sql .='     , a.biomat                  as "MstFacilityItem__biogenous_type"';
        // クラス分類
        $sql .='     , a.hemiclas                as "MstFacilityItem__class_separation"';
        // 課税区分
        $sql .='     , a.taxclas                 as "MstFacilityItem__tax_type" ';
        // 保管区分
        $sql .='     , a.storclas                as "MstFacilityItem__keep_type" ';
        // コスロスID
        $sql .='     , a.matcd                   as "MstFacilityItem__master_id" ';
        // 単位名称
        $sql .='     , e.unitnam                 as "MstFacilityItem__unit_name" ';
        // 備品単位名称ID
        $sql .='     , i.id                      as "MstFacilityItem__mst_unit_name_id"';
        // 販売元ID
        $sql .='     , f.id                      as "MstDealer__id"';
        // 販売元
        $sql .='     , f.makcalnam               as "MstDealer__dealer_name"';
        // 販売元コード
        $sql .='     , f.makcocd                 as "MstDealer__dealer_code"';
        // 保険請求区分
        $sql .='     , g.inschrgclas             as "MstFacilityItem__insurance_claim_type"';
        // 保険請求コード
        $sql .='     , c.rezcd                   as "MstFacilityItem__insurance_claim_code"';
        // 保険請求名（短）
        // 保険請求名
        $sql .='     , c.depclcalname            as "MstFacilityItem__insurance_claim_name"'; 
        // 単位名称（MSC商品マスタ）
        $sql .='     , e.unitnam                 as "MstGrandmaster__unit_name"';
        // 単位名称（備品マスタ）
        $sql .='     , e.unitnam                 as "MstItem__unit_name"';
        $sql .='     , e.unitnam                 as "MstFacilityItems__unit_name" ';
        // 薬事承認番号
        $sql .='     , a.medapno                 as "MstFacilityItem__pharmaceutical_number"';
        $sql .='   from ';
        $sql .='     mst_grand_kyo_materials as a ';
        $sql .='     left join mst_grand_dep_cl_cd_relations as b ';
        $sql .='       on b.matcd = a.matcd ';
        $sql .='       and b.startdate <= now() ';
        $sql .='       and b.enddate >= now()';
        $sql .='     left join mst_grand_dep_cl_cds as c ';
        $sql .='       on c.depclcd = b.depclcd ';
        $sql .='       and c.startdate <= now() ';
        $sql .='       and c.enddate >= now()';
        $sql .='     left join mst_grand_mat_prices as d ';
        $sql .= '      on d.matcd = a.matcd ';
        $sql .='       and d.startdate <= now() ';
        $sql .='       and d.enddate >= now()';
        $sql .='     left join mst_grand_units as e ';
        $sql .='       on e.unitcd = d.unit1 ';
        $sql .='     left join mst_grand_mkt_cos as f';
        $sql .='       on f.makcocd = a.makcocd ';
        $sql .='     left join mst_grand_ins_apprs as g ';
        $sql .='       on g.matcd = a.matcd ';
        $sql .='     left join mst_facility_items as h ';
        $sql .='       on h.master_id = a.matcd ';
        $sql .='     left join mst_unit_names as i ';
        $sql .='       on i.id = h.mst_unit_name_id ';
        $sql .='   where ';
        $sql .='     a.matcd = ' ."'".$matcd."'";

        $tmp = $this->MstGrandmaster->query($sql);
        return $tmp[0]; 
    }


    /**
     *
     * データマッピング
     */
    private function _dataMapping($array, $map){
        $title_row = $array[0];
        $mapped_title = array();
        $mapped_data = array();
        foreach($title_row as $key => $title){
            if(array_key_exists ($title,$map)){
                $mapped_title[$key] = $map[$title];
            }
        }
        // ヘッダ項目名称が変更されている場合NG
        if(count($mapped_title) != count($map)){
            return false;
        }
        foreach($array as $row_key => $row_data){
            // 空項目のみの行は無視
            if( count(array_merge(array(), array_diff($row_data, array("")))) <> 0 ){
                // ヘッダ項目数 < データ項目数だった場合の対応
                $row_data2 = array_slice( $row_data , 0 , count($mapped_title));
                if($row_key != 0 && is_array($row_data2) && count($row_data2) == count($mapped_title)){
                    $mapped_data[$row_key] = array_combine($mapped_title,$row_data2);
                }
            }
        }
        return $mapped_data;
    }

    /**
     * CSV取込登録処理
     * 採用品
     */
    private function saveFacilityItem($data){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        $ids = array();
        // トランザクション開始
        $this->MstFacilityItem->begin();
        foreach($data as $d){
            $count++;
            $save_data = array();
            // データチェック
            if($this->preCheckFacilityItem($d , $count)){
                if($d['id']==''){
                    // 新規登録の場合
                    $save_data['creater']                  = $this->Session->read('Auth.MstUser.id');
                    $save_data['created']                  = $this->request->data['now'];
                    $save_data['is_lowlevel']              = ($d['is_lowlevel']=='')?false:true;
                }else{
                    // 更新の場合
                    $save_data['id']                       = $d['id'];
                    $ids[] = $d['id'];
                }

                $save_data['mst_facility_id']       = $this->Session->read('Auth.facility_id_selected');
                $save_data['internal_code']         = trim($d['internal_code']);
                $save_data['item_name']             = trim(mb_convert_kana($d['item_name'],"ksnr"));
                $save_data['standard']              = trim(mb_convert_kana($d['standard'],"ksnr"));
                $save_data['item_code']             = trim(mb_convert_kana($d['item_code'],"ksnr"));
                $save_data['jan_code']              = trim($d['jan_code']);
                $save_data['per_unit']              = $d['per_unit'];
                $save_data['mst_unit_name_id']      = $this->_getUnitNameId($d['unit_code']);
                $save_data['price']                 = ($d['price']=='')?null:str_replace(',','',$d['price']);
                $save_data['unit_price']            = ($d['unit_price']=='')?null:str_replace(',','',$d['unit_price']);
                $save_data['refund_price']          = ($d['refund_price']=='')?null:str_replace(',','',$d['refund_price']);
                $save_data['insurance_claim_type']  = $this->_getClaimTypeCode($d['insurance_claim_type']);
                $save_data['insurance_claim_code']  = $d['insurance_claim_code'];

                // 後で使うので詰め直しておく
                $d['insurance_claim_type']          = $save_data['insurance_claim_type'];

                $save_data['item_category1']        = $this->_getItemCategoryCode($d['category_name1'],1);
                $save_data['item_category2']        = $this->_getItemCategoryCode($d['category_name2'],2);
                $save_data['item_category3']        = $this->_getItemCategoryCode($d['category_name3'],3);
                $save_data['biogenous_type']        = ($d['biogenous_type']=='')?null:$d['biogenous_type'];
                // $save_data['jmdn_code']             = $d['jmdn_code'];
                $save_data['class_separation']      = $d['class_separation'];
                $save_data['tax_type']              = ($d['tax_type']=='')?null:$d['tax_type'];
                $save_data['start_date']            = $d['start_date'];
                $save_data['end_date']              = ($d['end_date']=='')?null:$d['end_date'];
                $save_data['recital']               = $d['recital'];

                $save_data['converted_num']         = $d['converted_num'];
                $save_data['item_type']             = Configure::read('Items.item_types.normalitem');
                $save_data['mst_owner_id']          = $this->getFacilityId($d['owner_facility_code'] ,
                                                                           array(Configure::read('FacilityType.donor')) ,
                                                                           $this->Session->read('Auth.facility_id_selected')
                                                                           );
                // 後で使うので詰め直しておく
                $d['mst_owner_id']                  = $save_data['mst_owner_id'];
                $save_data['medical_code']          = $d['medical_code'];

                $save_data['mst_dealer_id']         = $this->_getDealerId($d['dealer_code']);
                // 後で使うので詰め直しておく
                $d['mst_dealer_id']                 = $save_data['mst_dealer_id'];
                $save_data['dealer_code']           = $d['dealer_code'];

                $save_data['master_id']             = $d['master_id'];

                $save_data['expired_date']          = ($d['expired_date']=='')?null:$d['expired_date'];
                $save_data['is_auto_storage']       = ($d['is_auto_storage']=='')?false:true;
                $save_data['buy_flg']               = ($d['buy_flg']=='')?false:true;
                
                $save_data['mst_accountlist_id']    = $this->_getAccountListId($d['mst_accountlist']);

                $save_data['enable_medicalcode']    = $d['enable_medicalcode'];
                $save_data['lot_ubd_alert']         = $d['lot_ubd_alert'];

                for($i=1 ; $i <= 30 ; $i++){
                    $save_data["spare_key{$i}"] = $d["spare_key{$i}"];
                }
                $save_data['modifier']              = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']              = $this->request->data['now'];
                $this->MstFacilityItem->create();

                if(!$this->MstFacilityItem->save($save_data)){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '商品情報の登録に失敗しました。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }elseif(!isset($save_data['id'])){
                    $ids[] = $this->MstFacilityItem->getLastInsertId();
                    $mst_facility_item_id = $this->MstFacilityItem->getLastInsertId();
                }else{
                    $mst_facility_item_id = $save_data['id'];
                }

                // 包装単位更新1
                if(!$this->saveMstItemUnit(
                    $mst_facility_item_id ,
                    $save_data['mst_unit_name_id'] ,
                    $d['per_unit1'] ,
                    $d['unit_code1'],
                    $d['start_date1'],
                    true
                    )){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 包装単位1の登録に失敗しました。');
                    if($err_cnt > 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }

                for($i=2;$i<=5;$i++){
                    // 包装単位更新2-5
                    if($d['unit_code'.$i] != '' ){
                        // 空でなければ登録処理
                        if(!$this->saveMstItemUnit(
                            $mst_facility_item_id ,
                            $save_data['mst_unit_name_id'] ,
                            $d["per_unit{$i}"] ,
                            $d["unit_code{$i}"],
                            $d["start_date{$i}"],
                            false
                            )){
                            $err_cnt++;
                            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 包装単位'.$i.'の登録に失敗しました。');
                            if($err_cnt > 100){
                                // 100件以上になったらそれ以上処理しない
                                break;
                            }
                        }
                    }
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if(count($ids) > 0){
            // 重複チェック（商品IDの重複がないこと）
            $check_array = $this->duplicateCheckFacilityItem($ids);
            if($check_array != array()){
                foreach($check_array as $c){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '商品ID:'.$c[0]['internal_code'].'が重複しています。');
                }
            }
        }

        if($err_cnt>0){
            // ロールバック
            $this->MstFacilityItem->rollback();
        }else{
            // コミット
            $this->MstFacilityItem->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }

    /**
     * CSV取込登録処理
     * センター定数設定
     */
    private function saveCenterFixedCount($data){
        $now = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        $ids = array();
        // トランザクション開始
        $this->MstFixedCount->begin();
        foreach($data as $d){
            $count++;
            $save_data = array();
            // データチェック
            if($this->preCheckFixedCount($d , $count , Configure::read('FacilityType.center'))){
                if($d['id']==''){
                    // 新規登録の場合
                    $save_data['creater']                  = $this->Session->read('Auth.MstUser.id');
                    $save_data['created']                  = $now;
                }else{
                    // 更新の場合
                    $save_data['id']                       = $d['id'];
                    $ids[] = $d['id'];
                }

                $save_data['mst_item_unit_id']         = $this->_getMstItemUnitId($d['internal_code'] , $d['per_unit']);
                $save_data['mst_facility_item_id']     = $this->_getFacilityItemId($save_data['mst_item_unit_id']);
                $save_data['mst_facilities_id']        = $this->Session->read('Auth.facility_id_selected');
                $save_data['mst_department_id']        = $this->getDepartmentId($save_data['mst_facilities_id'] , array(Configure::read('DepartmentType.warehouse'),Configure::read('DepartmentType.opestock')) , $d['department_code']);

                $save_data['start_date']               = $d['start_date'];
                $save_data['is_deleted']               = ($d['is_deleted']=='')?true:false;
                $save_data['modifier']                 = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']                 = $now;

                $save_data['trn_stock_id']             = $this->_getStockId($save_data['mst_item_unit_id'] , $save_data['mst_department_id'] , $now);
                $save_data['order_point']              = $d['order_point'];
                $save_data['fixed_count']              = $d['fixed_count'];
                $save_data['holiday_fixed_count']      = $d['holiday_fixed_count'];
                $save_data['spare_fixed_count']        = $d['spare_fixed_count'];

                $save_data['mst_shelf_name_id']        = $this->_getShelfNameId($d['shelf_name'] ,$save_data['mst_department_id']);
                $save_data['trade_type']               = Configure::read('TeisuType.Constant'); // センターは定数品固定

                $this->MstFixedCount->create();

                if(!$this->MstFixedCount->save($save_data)){
                    $err_cnt++;
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }elseif(!isset($save_data['id'])){
                    $ids[] = $this->MstFixedCount->getLastInsertId();
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if(count($ids) > 0){
            // 重複チェック（包装単位で重複がないこと）
            $check_array = $this->duplicateCheckFixedCount($ids , Configure::read('FacilityType.center'));
            if($check_array != array()){
                foreach($check_array as $c){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '商品ID:'.$c[0]['internal_code'].'、包装単位入り数:'.$c[0]['per_unit'].'で設定が重複しています。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }
        }

        if($err_cnt>0){
            // ロールバック
            $this->MstFixedCount->rollback();
        }else{
            // コミット
            $this->MstFixedCount->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }

    /**
     * CSV取込登録処理
     * 病院定数設定
     */
    private function saveHospitalFixedCount($data){
        $now = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        $ids = array();
        // トランザクション開始
        $this->MstFixedCount->begin();
        foreach($data as $d){
            $count++;
            $save_data = array();
            // データチェック
            if($this->preCheckFixedCount($d , $count , Configure::read('FacilityType.hospital'))){
                if($d['id']==''){
                    // 新規登録の場合
                    $save_data['creater']                  = $this->Session->read('Auth.MstUser.id');
                    $save_data['created']                  = $now;
                }else{
                    // 更新の場合
                    $save_data['id']                       = $d['id'];
                    $ids[] = $d['id'];
                }


                $save_data['mst_item_unit_id']         = $this->_getMstItemUnitId($d['internal_code'] , $d['per_unit']);
                $save_data['mst_facility_item_id']     = $this->_getFacilityItemId($save_data['mst_item_unit_id']);
                $save_data['mst_facilities_id']        = $this->getFacilityId($d['facility_code'] , array(Configure::read('FacilityType.hospital')) , $this->Session->read('Auth.facility_id_selected'));
                $save_data['mst_department_id']        = $this->getDepartmentId($save_data['mst_facilities_id'] , array(Configure::read('DepartmentType.hospital')) , $d['department_code']);

                $save_data['start_date']               = $d['start_date'];
                $save_data['is_deleted']               = ($d['is_deleted']=='')?true:false;
                $save_data['modifier']                 = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']                 = $now;
                $save_data['trn_stock_id']             = $this->_getStockId($save_data['mst_item_unit_id'] , $save_data['mst_department_id'] , $now);
                $save_data['fixed_count']              = $d['fixed_count'];
                $save_data['holiday_fixed_count']      = $d['holiday_fixed_count'];
                $save_data['spare_fixed_count']        = $d['spare_fixed_count'];
                $save_data['mst_shelf_name_id']        = $this->_getShelfNameId($d['shelf_name'] ,$save_data['mst_department_id']);
                $save_data['trade_type']               = $d['trade_type'];
                $save_data['print_type']               = $d['print_type'];

                // 預託品、非在庫は発注点に、定数と同じ値を入れる
                if($d['trade_type'] == Configure::read('TeisuType.Deposit') ||
                   $d['trade_type'] == Configure::read('TeisuType.NonStock')
                   ){
                    $save_data['order_point']=$d['fixed_count'];
                }else{
                    $save_data['order_point']=0;
                }

                $this->MstFixedCount->create();

                if(!$this->MstFixedCount->save($save_data)){
                    $err_cnt++;
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }elseif(!isset($save_data['id'])){
                    $ids[] = $this->MstFixedCount->getLastInsertId();
                }

            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if(count($ids) > 0){
            // 重複チェック（包装単位で重複がないこと）
            $check_array = $this->duplicateCheckFixedCount($ids , Configure::read('FacilityType.hospital'));
            if($check_array != array()){
                foreach($check_array as $c){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '病院：'.$c[0]['facility_name'].'、部署:'.$c[0]['department_name'].'、商品ID:'.$c[0]['internal_code'].' 定数設定で重複しているデータが存在します。病院部署に対して設定できる包装単位は1つのみです。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }
        }

        if($err_cnt>0){
            // ロールバック
            $this->MstFixedCount->rollback();
        }else{
            // コミット
            $this->MstFixedCount->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }

    /**
     * CSV取込登録処理
     * 仕入設定
     */
    private function saveTrnsactionConfig($data){
        $now = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        $ids = array();
        // トランザクション開始
        $this->MstTransactionConfig->begin();
        foreach($data as $d){
            $count++;
            $save_data = array();
            // データチェック
            if($this->preCheckTransactionConfig($d , $count)){
                if($d['id']==''){
                    // 新規登録の場合
                    $save_data['creater']              = $this->Session->read('Auth.MstUser.id');
                    $save_data['created']              = $now;
                    $save_data['end_date']             = '3000/01/01';
                }else{
                    // 更新の場合
                    $save_data['id']                   = $d['id'];
                    $ids[] = $d['id'];
                }

                $save_data['mst_item_unit_id']         = $this->_getMstItemUnitId($d['internal_code'] , $d['per_unit']);
                $save_data['mst_facility_item_id']     = $this->_getFacilityItemId($save_data['mst_item_unit_id']);
                $save_data['mst_facility_id']          = $this->Session->read('Auth.facility_id_selected');
                $save_data['partner_facility_id']      = $this->getFacilityId($d['facility_code1'] , array(Configure::read('FacilityType.supplier')) , $this->Session->read('Auth.facility_id_selected'));
                $save_data['partner_facility_id2']     = $this->getFacilityId($d['facility_code2'] , array(Configure::read('FacilityType.supplier')) , $this->Session->read('Auth.facility_id_selected'));
                $save_data['mst_facility_relation_id'] = $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $save_data['partner_facility_id']);
                $save_data['transaction_price']        = str_replace(',','',$d['transaction_price']);
                if($this->Session->read('Auth.Config.MSCorporate') == '1' ) {
                    $save_data['ms_transaction_price']        = str_replace(',','',$d['ms_transaction_price']);
                }else{
                    $save_data['ms_transaction_price']        = str_replace(',','',$d['transaction_price']);
                }
                $save_data['start_date']               = $d['start_date'];
                $save_data['is_deleted']               = ($d['is_deleted']=='')?true:false;
                $save_data['modifier']                 = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']                 = $now;

                $this->MstTransactionConfig->create();
                if(!$this->MstTransactionConfig->save($save_data)){
                    $err_cnt++;
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }elseif(!isset($save_data['id'])){
                    $ids[] = $this->MstTransactionConfig->getLastInsertId();
                }
                
                /* 代表フラグ処理 */
                if($d['is_main']!=''){
                    // 代表フラグを入れる
                    if(!$this->saveTransactionMain($save_data['mst_facility_item_id'] , $save_data['mst_item_unit_id'] , $save_data['partner_facility_id'])){
                        $err_cnt++;
                        if($err_cnt >= 100){
                            // 100件以上になったらそれ以上処理しない
                            break;
                        }
                    }
                }

                if(Configure::read('SalesCreate.flag') == 1){
                    if(!$this->createSalesConfig($save_data)){
                        $err_cnt++;
                        if($err_cnt >= 100){
                            // 100件以上になったらそれ以上処理しない
                            break;
                        }
                    }
                }
                
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if(count($ids)>0){
            // 重複チェック（仕入先、開始日、包装単位で重複がないこと）
            $check_array = $this->duplicateCheckTransactionConfig($ids);
            if($check_array != array()){
                foreach($check_array as $c){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '仕入先:'.$c[0]['facility_name'].'、商品ID:'.$c[0]['internal_code'].'、開始日:'.$c[0]['start_date'].'、包装単位入り数:'.$c[0]['per_unit'].'で重複しているデータが存在します。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }
        }
        if($err_cnt>0){
            // ロールバック
            $this->MstTransactionConfig->rollback();
        }else{
            // ココでend_dateの帳尻を合わせる
            $this->setTransactionEndDate($ids);
            // コミット
            $this->MstTransactionConfig->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }

    /**
     * 代表フラグ
     **/
    private function saveTransactionMain($mst_facility_item_id , $mst_item_unit_id , $mst_facility_id){
        $sql = ' select id from mst_transaction_mains where mst_facility_item_id = ' . $mst_facility_item_id ;
        $ret = $this->MstTransactionMain->query($sql);
        
        $now = date('Y/m/d H:i:s');
        if(isset($ret[0][0]['id'])){
            // 更新
            $save_data['id'] = $ret[0][0]['id'];
            
        }else{
            // 新規
            $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
            $save_data['created'] = $now;
        }
        $save_data['mst_facility_item_id'] = $mst_facility_item_id  ;
        $save_data['mst_item_unit_id']     = $mst_item_unit_id ;
        $save_data['mst_facility_id']      = $mst_facility_id;
        $save_data['modifier']             = $this->Session->read('Auth.MstUser.id');
        $save_data['modified']             = $now;

        $this->MstTransactionMain->create();
        if(!$this->MstTransactionMain->save($save_data)){
            return false ;
        }
        return true;
    }

    private function createSalesConfig($data){
        // 裏で売上設定を作る。
        $sql  = ' select ';
        $sql .= '       c.id                           as mst_item_unit_id ';
        $sql .= '     , a.per_unit                     as tran_pre_unit ';
        $sql .= '     , c.per_unit                     as sale_per_unit ';
        $sql .= '     , a.mst_facility_item_id ';
        $sql .= '     , x.partner_facility_id  ';
        $sql .= '   from ';
        $sql .= '     mst_item_units as a  ';
        $sql .= '     left join mst_facility_items as b  ';
        $sql .= '       on b.id = a.mst_facility_item_id  ';
        $sql .= '       and b.item_type = ' . Configure::read('Items.item_types.normalitem');
        $sql .= '     left join mst_facility_relations as x  ';
        $sql .= '       on x.mst_facility_id = b.mst_facility_id  ';
        $sql .= '     inner join mst_facilities as y  ';
        $sql .= '       on y.id = x.partner_facility_id  ';
        $sql .= '       and y.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on c.mst_facility_item_id = b.id  ';
        $sql .= '     left join mst_sales_configs as d  ';
        $sql .= '       on d.mst_item_unit_id = c.id  ';
        $sql .= '      and d.partner_facility_id = y.id';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $data['mst_item_unit_id'];
        $sql .= '     and d.id is null ';
        
        $ret = $this->MstSalesConfig->query($sql);
        // 売上げ設定がない場合作る
        foreach($ret as $r ){
            $sales = array(
                'MstSalesConfig' => array(
                    'mst_item_unit_id'         => $r[0]['mst_item_unit_id'],
                    'start_date'               => '1900/01/01',
                    'end_date'                 => '3000/01/01',
                    'mst_facility_relation_id' => $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $r[0]['partner_facility_id']),
                    'mst_facility_item_id'     => $r[0]['mst_facility_item_id'],
                    'mst_facility_id'          => $this->Session->read('Auth.facility_id_selected'),
                    'partner_facility_id'      => $r[0]['partner_facility_id'],
                    'sales_price'              => round((( $data['transaction_price'] / $r[0]['tran_pre_unit'] ) * $r[0]['sale_per_unit']) , 2),
                    'is_deleted'               => false,
                    'creater'                  => $this->Session->read('Auth.MstUser.id'),
                    'created'                  => 'now()',
                    'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                    'modified'                 => 'now()',
                    )
                );
            
            $this->MstSalesConfig->create();
            if (!$this->MstSalesConfig->save($sales)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * CSV取込登録処理
     * 売上設定
     */
    private function saveSaleConfig($data){
        $now = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        $ids = array();
        // トランザクション開始
        $this->MstSalesConfig->begin();
        foreach($data as $d){
            $count++;
            $save_data = array();
            // データチェック
            if($this->preCheckSalesConfig($d , $count)){
                if($d['id']==''){
                    // 新規登録の場合
                    $save_data['creater']                  = $this->Session->read('Auth.MstUser.id');
                    $save_data['created']                  = $now;
                    $save_data['end_date']                 = '3000/01/01';
                }else{
                    // 更新の場合
                    $save_data['id']                       = $d['id'];
                    $ids[] = $d['id'];
                }

                $save_data['mst_item_unit_id']         = $this->_getMstItemUnitId($d['internal_code'] , $d['per_unit']);
                $save_data['mst_facility_item_id']     = $this->_getFacilityItemId($save_data['mst_item_unit_id']);
                $save_data['mst_facility_id']          = $this->Session->read('Auth.facility_id_selected');
                $save_data['partner_facility_id']      = $this->getFacilityId($d['hospital_facility_code'] , array(Configure::read('FacilityType.hospital')) , $this->Session->read('Auth.facility_id_selected'));
                $save_data['mst_facility_relation_id'] = $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $save_data['partner_facility_id']);
                $save_data['sales_price']              = str_replace(',','',$d['sales_price']);
                $save_data['start_date']               = $d['start_date'];
                $save_data['is_deleted']               = ($d['is_deleted']=='')?true:false;
                $save_data['modifier']                 = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']                 = $now;
                $save_data['is_main']                  = false;
                $this->MstSalesConfig->create();

                if(!$this->MstSalesConfig->save($save_data)){
                    $err_cnt++;
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }elseif(!isset($save_data['id'])){
                    $ids[] = $this->MstSalesConfig->getLastInsertId();
                }

            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if(count($ids) > 0){
            // 重複チェック（得意先、開始日、包装単位で重複がないこと）
            $check_array = $this->duplicateCheckSalesConfig($ids);
            if($check_array != array()){
                foreach($check_array as $c){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '得意先:'.$c[0]['facility_name'].'、商品ID:'.$c[0]['internal_code'].'、開始日:'.$c[0]['start_date'].'、包装単位入り数:'.$c[0]['per_unit'].'で重複しているデータが存在します。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }
        }

        if($err_cnt>0){
            // ロールバック
            $this->MstSalesConfig->rollback();
        }else{
            // ココでend_dateの帳尻を合わせる
            $this->setSalesEndDate($ids);
            // コミット
            $this->MstSalesConfig->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }

    /**
     * 採用品CSV取込事前チェック
     */
    private function preCheckFacilityItem($d , $count){
        $return = true;
        if($d['id'] <> '' ){
            // IDがあった場合操作可能な行をさしているか？
            $sql = ' select count(*) as count from mst_facility_items as a where a.id = ' . $d['id'] .  ' and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $r = $this->MstFacilityItem->query($sql);
            if($r[0][0]['count'] == 0){
                $this->request->data['err_result'][] = array($count => 'IDの指定に誤りがあります。');
                $return = false;
            }
        }
        // 施設コードは正しいか？
        if(!$this->checkFacilityCode($d['facility_code'] , array(Configure::read('FacilityType.center')) ,$this->Session->read('Auth.facility_id_selected'))){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 施設コード['.$d['facility_code'].']に誤りがあります。');
            $return = false;
        }
        // 商品IDの必須チェック
        if($d['internal_code'] == ''){
            $this->request->data['err_result'][] = array($count => '商品IDは必須項目です。');
            $return = false;
        }
        // 商品名の必須チェック
        if($d['item_name'] == ''){
            $this->request->data['err_result'][] = array($count => '商品ID : '.$d['internal_code'].' 商品名は必須項目です。');
            $return = false;
        }
        // 販売元コードと販売元名は正しいか？
        if(!$this->checkDealer($d['dealer_code'] , $d['dealer_name']) && $d['dealer_code']!='' && $d['dealer_name'] !=''){
            $this->request->data['err_result'][] = array($count => '商品ID : '. $d['internal_code']. ' 販売元に誤りがあります。');
            $return = false;
        }
        // JANコードは13文字まで
        if(strlen($d['jan_code']) > 13){
            $this->request->data['err_result'][] = array($count => '商品ID : '. $d['internal_code'] .' JANコード[' . $d['jan_code'] . ']が長すぎます。');
            $return = false;
        }
        // JANコードは数字のみ
        if($d['jan_code'] != '' && preg_match("/^[0-9]{1,13}$/", $d['jan_code']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' JANコード['.$d['jan_code'].']は数字のみ入力可能です。');
            $return = false;
        }
        
        // 基本単位コード必須チェック
        if($d['unit_code'] == ''){
            $this->request->data['err_result'][] = array($count => '商品ID : '.$d['internal_code'].' 基本単位コードは必須項目です。');
            $return = false;
        }
        // 基本単位コード存在チェック
        if(!$this->_getUnitNameId($d['unit_code'])){
            $this->request->data['err_result'][] = array($count => '商品ID : '.$d['internal_code'].' 基本単位コード[' . $d['unit_code'] . ']に誤りがあります。');
            $return = false;
        }
        
        // 基本単位入数チェック
        if(preg_match("/^[1-9][0-9]{0,9}$/", $d['per_unit']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code']. ' 基本単位入数['.$d['per_unit'].']に誤りがあります。');
            $return = false;
        }
        // 償還価格のフォーマットは正しいか？
        $d['refund_price'] = str_replace(',','',$d['refund_price']);
        if($d['refund_price'] != '' && preg_match("/^[0-9]{1,9}(\.[0-9]{1,2})?$/", $d['refund_price']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 償還価格['.$d['refund_price'].']に誤りがあります。');
            $return = false;
        }
        
        // 定価のフォーマットは正しいか？
        $d['price'] = str_replace(',','',$d['price']);
        if($d['price'] != ''  && preg_match("/^[0-9]{1,9}(\.[0-9]{1,2})?$/", $d['price']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 定価['.$d['price'].']に誤りがあります。');
            $return = false;
        }
        // 定価単価のフォーマットは正しいか？
        $d['unit_price'] = str_replace(',','',$d['unit_price']);
        if($d['unit_price'] != ''  && preg_match("/^[0-9]{1,9}(\.[0-9]{1,2})?$/", $d['unit_price']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 定価単価['.$d['unit_price'].']に誤りがあります。');
            $return = false;
        }
        // 課税区分は正しいか？
        if($d['tax_type'] != '' && preg_match("/^[0-1]$/", $d['tax_type']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 課税区分['.$d['tax_type'].']に誤りがあります。');
            $return = false;
        }
        // 保険請求コードは正しいか？
        if(!$this->checkClaimCode($d['insurance_claim_code']) && $d['insurance_claim_code']!=''){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 保険請求コード['.$d['insurance_claim_code'].']に誤りがあります。');
            $return = false;
        }
        // 保険請求区分は正しいか？
        if(!$this->_getClaimTypeCode($d['insurance_claim_type']) && $d['insurance_claim_type']!=''){
            $this->request->data['err_result'][] = array($count => '商品ID : '. $d['internal_code']. ' 保険請求区分['.$d['insurance_claim_type'].']に誤りがあります。');
            $return = false;
        }
        // 医事コードは20文字まで
        if(strlen($d['medical_code']) > 20){
            $this->request->data['err_result'][] = array($count => '商品ID : '. $d['internal_code']. ' 医事コード['.$d['medical_code'].']が長すぎます。');
            $return = false;
        }
        // JMDNコードは正しいか？
        if(!$this->checkJMDNCode($d['jmdn_code']) && $d['jmdn_code']!=''){
            $this->request->data['err_result'][] = array($count => '商品ID : '. $d['internal_code']. ' JMDNコード['.$d['jmdn_code'].']に誤りがあります。');
            $return = false;
        }
        // クラス分類は正しいか？
        if(!$this->checkClassSeparation($d['class_separation']) && trim($d['class_separation']) != ''){
            $this->request->data['err_result'][] = array($count => '商品ID : '. $d['internal_code']. ' クラス分類['.$d['class_separation'].']に誤りがあります。');
            $return = false;
        }
        // 大分類は正しいか？
        if(!$this->_getItemCategoryCode($d['category_name1'] , 1 ) && $d['category_name1']!=''){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 大分類[' .$d['category_name1']. ']に誤りがあります。');
            $return = false;
        }
        // 中分類は正しいか？
        if(!$this->_getItemCategoryCode($d['category_name2'] , 2 ) && $d['category_name2']!=''){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 中分類[' .$d['category_name2']. ']に誤りがあります。');
            $return = false;
        }
        // 小分類は正しいか？
        if(!$this->_getItemCategoryCode($d['category_name3'] , 3 ) && $d['category_name3']!=''){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 小分類[' .$d['category_name3']. ']に誤りがあります。');
            $return = false;
        }
        // 生物由来区分は正しいか？
        if(trim($d['biogenous_type']) != '' && preg_match("/^[1-4]$/", $d['biogenous_type']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code']. ' 生物由来区分['.$d['biogenous_type'].']に誤りがあります。');
            $return = false;
        }
        // 開始日のフォーマットは正しいか？
        if(!$this->checkDateFormat($d['start_date'])){
            $this->request->data['err_result'][] = array($count => '商品ID : '.$d['internal_code'].' 採用開始日['.$d['start_date'].']の日付形式に誤りがあります。');
            $return = false;
        }
        // 終了日のフォーマットは正しいか？
        if($d['end_date'] != '' && !$this->checkDateFormat($d['end_date'])){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code']. ' 採用中止日['.$d['end_date'].']の日付形式に誤りがあります。');
            $return = false;
        }
        // MasterIDは13文字まで
        if(strlen($d['master_id']) > 13){
            $this->request->data['err_result'][] = array($count => '商品ID : '. $d['internal_code']. ' マスタID['.$d['master_id'].']が長すぎます。');
            $return = false;
        }
        // 換算数のフォーマットは正しいか？
        if(preg_match("/^[0-9]{1,8}(\.[0-9]{1,3})?$/", $d['converted_num']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code']. ' 換算数['.$d['converted_num'].']に誤りがあります。');
            $return = false;
        }
        // 勘定科目は正しいか?
        if(!$this->_getAccountListId($d['mst_accountlist']) && $d['mst_accountlist']!=''){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 勘定科目['.$d['mst_accountlist'].']に誤りがあります。');
            $return = false;
        }
        // 施主コードは正しいか？
        if(!$this->checkFacilityCode($d['owner_facility_code'] , array(Configure::read('FacilityType.donor')) ,$this->Session->read('Auth.facility_id_selected'))){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 施主コード['.$d['owner_facility_code'].']に誤りがあります。');
            $return = false;
        }
        // 期限切れ警告日数のフォーマットは正しいか？
        if($d['expired_date'] !='' && preg_match("/^[0-9]{1,9}$/", $d['expired_date']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 期限切れ警告日数['.$d['expried_date'].']に誤りがあります。');
            $return = false;
        }
        // LOT/UBDの必須チェック
        if(preg_match("/^[0-3]$/", $d['lot_ubd_alert']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : '. $d['internal_code']. ' ロットUBD警告['.$d['lot_ubd_alert'].']に誤りがあります。');
            $return = false;
        }
        // 薬事承認番号は30文字以内
        if(mb_strlen($d['pharmaceutical_number']) > 30){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 薬事承認番号['.$d['pharmaceutical_number'].']が長すぎます。');
            $return = false;
        }
        // 予備キー1～30は50文字以内
        for ($i = 1; $i <= 30; $i++) {
            if(mb_strlen($d["spare_key" . $i]) > 50){
                $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 予備キー'.$i.'['.$d["spare_key" . $i].']が長すぎます。');
                $return = false;
            }
        }
        // 備考は200文字以内
        if(mb_strlen($d['recital']) > 200){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 備考['.$d['recial'].']が長すぎます。');
            $return = false;
        }
        // 包装単位1単位コードチェック
        if(!$this->_getUnitNameId($d['unit_code1'])){
            $this->request->data['err_result'][] = array($count => '商品ID : '. $d['internal_code']. ' 包装単位1単位コード['.$d['unit_code1'].']に誤りがあります。');
            $return = false;
        }
        // 包装単位1入数の必須チェック
        if(preg_match("/^[1-9][0-9]{0,9}$/", $d['per_unit1']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code']. ' 包装単位1入数['.$d['per_unit1'].']に誤りがあります。');
            $return = false;
        }
        // 包装単位1の開始日のフォーマットは正しいか？
        if(!$this->checkDateFormat($d['start_date1'])){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code']. ' 包装単位1開始日['.$d['start_date1'].']の日付形式に誤りがあります。');
            $return = false;
        }

        for ($i = 2; $i <= 5; $i++) {
            // 包装単位2-5は全部空 or 全部埋まっているになっているか
            if(!(($d['start_date'.$i] != '' && $d['unit_code'.$i] != '' && $d['per_unit'.$i] != '' ) ||
                 ($d['start_date'.$i] == '' && $d['unit_code'.$i] == '' && $d['per_unit'.$i] == '' ) ) ){
                $this->request->data['err_result'][] = array($count => '商品ID : '. $d['internal_code']. ' 包装単位'.$i.'に誤りがあります。');
                $return = false;
            }
            // 包装単位2-5単位コードチェック
            if(!$this->_getUnitNameId($d['unit_code'.$i]) && $d['unit_code'.$i]!=''){
                $this->request->data['err_result'][] = array($count => '商品ID : '. $d['internal_code']. ' 包装単位'.$i.'単位コード['.$d['unit_code'.$i].']に誤りがあります。');
                $return = false;
            }
            // 包装単位2-5入数のフォーマット
            if(preg_match("/^[1-9][0-9]{0,9}$/", $d['per_unit'.$i]) != 1 && $d['per_unit'.$i]!=''){
                $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code']. ' 包装単位'.$i.'入数['.$d['per_unit'.$i].']に誤りがあります。');
                $return = false;
            }
            // 包装単位2-5の開始日のフォーマットは正しいか？
            if($d['start_date'.$i] != '' && !$this->checkDateFormat($d['start_date'.$i])){
                $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code']. ' 包装単位'.$i.'開始日['.$d['start_date'.$i].']の日付形式に誤りがあります。');
                $return = false;
            }
        }
        
        return $return;
    }

    /**
     * 定数設定CSV取込事前チェック
     */
    private function preCheckFixedCount($d , $count , $type){
        $return = true;
        if($d['id'] <> '' ){
            // IDがあった場合操作可能な行をさしているか？
            $sql  = ' select ';
            $sql .= '       count(*) as count  ';
            $sql .= '   from ';
            $sql .= '     mst_fixed_counts as a  ';
            $sql .= '     inner join mst_user_belongings as b  ';
            $sql .= '       on b.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '       and b.is_deleted = false  ';
            $sql .= '       and b.mst_facility_id = a.mst_facilities_id  ';
            $sql .= '     left join mst_facility_items as c  ';
            $sql .= '       on c.id = a.mst_facility_item_id  ';
            $sql .= '   where ';
            $sql .= '     a.id = ' . $d['id'];
            $sql .= '     and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');

            $r = $this->MstTransactionConfig->query($sql);
            if($r[0][0]['count'] == 0){
                $this->request->data['err_result'][] = array($count => 'IDの指定に誤りがあります。');
                $return = false ;
            }
        }

        // 施設コードは正しいか？
        if( $type == Configure::read('FacilityType.center')){
            // センターの場合
            if(!$this->checkFacilityCode($d['facility_code'] , array(Configure::read('FacilityType.center')) ,$this->Session->read('Auth.facility_id_selected'))){
                $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 施設コード['.$d['facility_code'].']に誤りがあります。');
                $return = false;
            }
        }else{
            // 病院の場合
            if(!$this->checkFacilityCode($d['facility_code'] , array( Configure::read('FacilityType.hospital')) ,$this->Session->read('Auth.facility_id_selected'))){
                $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 施設コード['.$d['facility_code'].']に誤りがあります。');
                $return = false;
            }
        }
        // 部署コードは正しいか？
        if($type == Configure::read('FacilityType.center')){
            // センターの場合
            if(!$this->checkDepartmentCode($d['facility_code'] , $d['department_code'] , array(Configure::read('DepartmentType.warehouse'),Configure::read('DepartmentType.opestock')) ,$this->Session->read('Auth.facility_id_selected'))){
                $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 部署コード['.$d['department_code'].']に誤りがあります。');
                $return = false;
            }
        }else{
            // 病院の場合
            if(!$this->checkDepartmentCode($d['facility_code'] , $d['department_code'] , array(Configure::read('DepartmentType.hospital')) ,$this->Session->read('Auth.facility_id_selected'))){
                $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 部署コード['.$d['department_code'].']に誤りがあります。');
                $return = false;
            }
        }

        // 包装単位入り数は、商品に設定済みの数値か？
        if(!$this->checkPerUnit($d['internal_code'] , $d['per_unit'],$this->Session->read('Auth.facility_id_selected'))){
            $this->request->data['err_result'][] = array($count =>  '商品ID:' . $d['internal_code'] . ' 包装単位入り数['.$d['per_unit'].']に誤りがあります。');
            $return = false;
        }
        // センターの場合
        if($type == Configure::read('FacilityType.center')){
            // 発注点のフォーマットは正しいか？
            if(preg_match("/^[0-9]{1,9}$/", $d['order_point']) != 1){
                $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 発注点['.$d['order_point'].']に誤りがあります。');
                $return = false;
            }
        }
        // 定数のフォーマットは正しいか？
        if(preg_match("/^[0-9]{1,9}$/", $d['fixed_count']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 定数['.$d['fixed_count'].']に誤りがあります。');
            $return = false;
        }
        // 休日定数のフォーマットは正しいか？
        if(preg_match("/^[0-9]{1,9}$/", $d['holiday_fixed_count']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 休日定数['.$d['holiday_fixed_count'].']に誤りがあります。');
            $return = false;
        }
        // 予備定数のフォーマットは正しいか？
        if(preg_match("/^[0-9]{1,9}$/", $d['spare_fixed_count']) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 予備定数['.$d['spare_fixed_count'].']に誤りがあります。');
            $return = false;
        }
        // 病院の場合
        if($type == Configure::read('FacilityType.hospital')){
            // 管理区分のフォーマットは正しいか？
            if(preg_match("/^[1|2|3|5]{1}$/", $d['trade_type']) != 1){
                $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 管理区分['.$d['trade_type'].']に誤りがあります。');
                $return = false;
            }
            // 印刷区分のフォーマットは正しいか？
            if(preg_match("/^[0|1|2|3]{1}$/", $d['print_type']) != 1){
                $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 印刷区分['.$d['print_type'].']に誤りがあります。');
                $return = false;
            }
        }
        
        // 棚番名称は正しいか？
        if(!$this->checkShelfName($d['shelf_name'] , $d['facility_code'] ,$d['department_code']) && $d['shelf_name']!=''){
            $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 棚番['.$d['shelf_name'].']に誤りがあります。');
            $return = false;
        }
        // 開始日のフォーマットは正しいか？
        if(!$this->checkDateFormat($d['start_date'])){
            $this->request->data['err_result'][] = array($count => '商品ID:' . $d['internal_code'] . ' 日付形式['.$d['start_date'].']に誤りがあります。');
            $return = false;
        }
        // 低レベル品
        $msg = $this->checklowlevelFixedCount($d , $type);
        if(!empty($msg)){
            $this->request->data['err_result'][] = array($count => $msg);
            $return = false;
        }
        
        return $return;
    }

    /**
     * 仕入設定CSV取込事前チェック
     */
    private function preCheckTransactionConfig($d , $count){
        $return = true;
        if($d['id'] <> '' ){
            // IDがあった場合操作可能な行をさしているか？
            $sql = ' select count(*) as count from mst_transaction_configs as a where a.id = ' . $d['id'] .  ' and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $r = $this->MstTransactionConfig->query($sql);
            if($r[0][0]['count'] == 0){
                $this->request->data['err_result'][] = array($count => 'ID[' . $d['id'] . ']の指定に誤りがあります。');
                $return = false;
            }
        }
        // センターの施設コードは正しいか？
        if(!$this->checkFacilityCode($d['center_facility_code'] , array(Configure::read('FacilityType.center')) ,$this->Session->read('Auth.facility_id_selected'))){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' センター施設コード[' . $d['center_facility_code'] . ']に誤りがあります。');
            $return = false;
        }
        // 開始日のフォーマットは正しいか？
        if(!$this->checkDateFormat($d['start_date'])){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 日付形式[' . $d['start_date'] . ']に誤りがあります。');
            $return = false;
        }
        // 仕入先の施設1コードは正しいか？
        if(!$this->checkFacilityCode($d['facility_code1'] , array(Configure::read('FacilityType.supplier')) ,$this->Session->read('Auth.facility_id_selected'))){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 仕入先施設コード1[' . $d['facility_code1'] . ']に誤りがあります。');
            $return = false;
        }
        // 仕入先の施設2コードは正しいか？
        /*if(!$this->checkFacilityCode($d['facility_code2'] , array(Configure::read('FacilityType.supplier')) ,$this->Session->read('Auth.facility_id_selected'))){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 仕入先施設コード2[' . $d['facility_code2'] . ']に誤りがあります。');
            $return = false;
        }*/
        // 包装単位入り数は、商品に設定済みの数値か？
        if(!$this->checkPerUnit($d['internal_code'] , $d['per_unit'],$this->Session->read('Auth.facility_id_selected'))){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 包装単位入り数[' . $d['per_unit'] . ']に誤りがあります。');
            $return = false;
        }
        // 価格のフォーマットは正しいか？
        if(preg_match("/^[0-9]{1,9}(\.[0-9]{1,2})?$/", str_replace(',','',$d['transaction_price'])) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code']  . ' 価格[' . $d['transaction_price'] . ']に誤りがあります。');
            $return = false;
        }
        
        if($this->Session->read('Auth.Config.MSCorporate') == '1' ) { 
            // MS価格のフォーマットは正しいか？
            if(preg_match("/^[0-9]{1,9}(\.[0-9]{1,2})?$/", str_replace(',','',$d['ms_transaction_price'])) != 1){
                $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' MS価格[' . $d['ms_transaction_price'] . ']に誤りがあります。');
                $return = false;
            }
        }
        return $return;
    }

    /**
     * 売上設定CSV取込事前チェック
     */
    private function preCheckSalesConfig($d , $count){
        $return = true;
        if($d['id'] <> '' ){
            // IDがあった場合操作可能な行をさしているか？
            $sql = ' select count(*) as count from mst_sales_configs as a where a.id = ' . $d['id'] .  ' and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $r = $this->MstSalesConfig->query($sql);
            if($r[0][0]['count'] == 0){
                $this->request->data['err_result'][] = array($count => 'ID[' . $d['id'] . ']の指定に誤りがあります。');
                $return = false;
            }
        }
        // 病院の施設コードは正しいか？
        if(!$this->checkFacilityCode($d['hospital_facility_code'] , array(Configure::read('FacilityType.hospital')) ,$this->Session->read('Auth.facility_id_selected'))){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 病院施設コード[' . $d['hospital_facility_code'] . ']に誤りがあります。');
            $return = false;
        }
        // 開始日のフォーマットは正しいか？
        if(!$this->checkDateFormat($d['start_date'])){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 日付形式[' . $d['start_date'] . ']に誤りがあります。');
            $return = false;
        }
        // センターの施設コードは正しいか？
        if(!$this->checkFacilityCode($d['center_facility_code'] , array(Configure::read('FacilityType.center')) ,$this->Session->read('Auth.facility_id_selected'))){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' センター施設コード[' . $d['center_facility_code'] . ']に誤りがあります。');
            $return = false;
        }
        // 包装単位入り数は、商品に設定済みの数値か？
        if(!$this->checkPerUnit($d['internal_code'] , $d['per_unit'],$this->Session->read('Auth.facility_id_selected'))){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 包装単位入数[' . $d['per_unit'] . ']に誤りがあります。');
            $return = false;
        }
        // 価格のフォーマットは正しいか？
        if(preg_match("/^[0-9]{1,9}(\.[0-9]{1,2})?$/", str_replace(',','',$d['sales_price'])) != 1){
            $this->request->data['err_result'][] = array($count => '商品ID : ' . $d['internal_code'] . ' 価格[' . $d['sales_price'] . ']に誤りがあります。');
            $return = false;
        }
        return $return;
    }

    // 日付フォーマットチェック
    private function checkDateFormat($date){
        if(preg_match('/^([0-9]{4})[\/]+([0-9]+)[\/]+([0-9]+)$/', $date, $parts)){
            if(checkdate($parts[2], $parts[3], $parts[1])){
                return true;
            }
        }
        return false;
    }

    // 施設コードチェック
    private function checkFacilityCode($fcode , $ftype , $center_id ){
        $sql  = ' select';
        $sql .= '       count(*)                       as count ';
        $sql .= '   from';
        $sql .= '     mst_facilities as a ';
        $sql .= '     left join mst_facility_relations as b ';
        $sql .= '       on b.mst_facility_id = a.id ';
        $sql .= '     left join mst_facilities as c ';
        $sql .= '       on c.id = b.partner_facility_id ';
        $sql .= '   where';
        $sql .= '     a.id = ' . $center_id;
        if (in_array(Configure::read('FacilityType.center'), $ftype)) {
            $sql .= "     and a.facility_code = '" . $fcode . "' ";
            $sql .= '     and a.facility_type in ( ' . join(',',$ftype) . ' ) ';
        }else{
            $sql .= "     and c.facility_code = '" . $fcode . "' ";
            $sql .= '     and c.facility_type in ( ' . join(',',$ftype) . ')';
        }

        $ret = $this->MstFacility->query($sql);
        if($ret[0][0]['count']==0){
            return false;
        }else{
            return true;
        }
    }

    // 部署施設コードチェック
    private function checkDepartmentCode($fcode , $dcode , $ftype , $center_id ){
        $sql  = ' select';
        $sql .= '       count(*)                       as count ';
        $sql .= '   from';
        $sql .= '     mst_facilities as a ';
        $sql .= '     left join mst_departments as a1 ';
        $sql .= '       on a1.mst_facility_id = a.id ';
        $sql .= '     left join mst_facility_relations as b ';
        $sql .= '       on b.mst_facility_id = a.id ';
        $sql .= '     left join mst_facilities as c ';
        $sql .= '       on c.id = b.partner_facility_id ';
        $sql .= '     left join mst_departments as c1  ';
        $sql .= '       on c1.mst_facility_id = c.id ';
        $sql .= '   where';
        $sql .= '     a.id = ' . $center_id;
        if (in_array(Configure::read('DepartmentType.warehouse'), $ftype)) {
            $sql .= "     and a.facility_code = '" . $fcode . "' ";
            $sql .= '     and a1.department_type in ( ' . join(',',$ftype) . ')';
            $sql .= "     and a1.department_code = '" . $dcode . "'";
        }else{
            $sql .= "     and c.facility_code = '" . $fcode . "' ";
            $sql .= '     and c1.department_type in ( ' . join(',',$ftype) .')';
            $sql .= "     and c1.department_code = '" . $dcode . "' ";
        }

        $ret = $this->MstFacility->query($sql);
        if($ret[0][0]['count']==0){
            return false;
        }else{
            return true;
        }
    }

    // 入り数チェック
    private function checkPerUnit($internal_code , $per_unit , $center_id){
        $sql  = ' select ';
        $sql .= '       count(*)               as count  ';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.mst_facility_item_id = a.id  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_id = ' . $center_id;
        $sql .= "     and a.internal_code = '". $internal_code ."'  ";
        $sql .= '     and b.per_unit = ' . $per_unit;
        $ret = $this->MstFacilityItem->query($sql);

        if($ret[0][0]['count'] == 0){
            return false;
        }else{
            return true;
        }
    }

    // 在庫が合ったら、管理区分、包装単位の変更ができない。
    private function checkHospitalStock($id , $per_unit , $trade_type){
        $sql  = ' select ';
        $sql .= '       count(*) as count';
        $sql .= '   from ';
        $sql .= '     mst_fixed_counts as a  ';
        $sql .= '     left join trn_stocks as b  ';
        $sql .= '       on a.trn_stock_id = b.id  ';
        $sql .= '     left join mst_item_units as c ';
        $sql .= '       on c.id = a.mst_item_unit_id ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $id;
        $sql .= '     and (a.trade_type <> ' .$trade_type. ' or c.per_unit <> '.$per_unit.')  ';
        $sql .= '     and (  ';
        $sql .= '       b.stock_count != 0  ';
        $sql .= '       or b.reserve_count != 0  ';
        $sql .= '       or b.promise_count != 0  ';
        $sql .= '       or b.requested_count != 0  ';
        $sql .= '       or b.receipted_count != 0 ';
        $sql .= '     )  ';
        $sql .= '     and b.is_deleted = false ';
        $ret = $this->MstFixedCount->query($sql);
        if($ret[0][0]['count'] == 0){
            return true ;
        }else{
            return false;
        }
    }

    // 棚番名称チェック
    private function checkShelfName($shelf_name , $facility_code , $department_code){
        $sql  = ' select ';
        $sql .= '       count(*)                as count  ';
        $sql .= '   from ';
        $sql .= '     mst_shelf_names as a  ';
        $sql .= '     left join mst_departments as b  ';
        $sql .= '       on b.id = a.mst_department_id  ';
        $sql .= '     left join mst_facilities as c  ';
        $sql .= '       on c.id = a.mst_facility_id  ';
        $sql .= '   where ';
        $sql .= "     a.name = '".$shelf_name."'  ";
        $sql .= "     and b.department_code = '" . $department_code . "'  ";
        $sql .= "     and c.facility_code = '" . $facility_code . "' ";
        $ret = $this->MstShelfName->query($sql);
        if($ret[0][0]['count'] == 0 ){
            return false;
        }else{
            return true;
        }
    }

    private function checklowlevelFixedCount($d , $type){
        $sql = " select is_lowlevel from mst_facility_items where internal_code = '" . $d['internal_code'] . "'";
        $tmp = $this->MstFacilityItem->query($sql);
        $msg = '';
        
        if(isset($tmp[0][0]['is_lowlevel']) && $tmp[0][0]['is_lowlevel']){
            // 低レベル品
            if($type == 1){
                if($d['per_unit'] <> 1 ){
                    $msg = '商品ID:' . $d['internal_code'] . ' は低レベル品のため、入り数' . $d['per_unit']. ' で定数設定はできません。';
                }
                
            }else{
                $msg = '商品ID:' . $d['internal_code'] . ' は低レベル品のため、部署定数の設定はできません。';
            }
        }
        return $msg;
    }

    // JMDNコードチェック
    private function checkJMDNCode($code){
        $sql  = ' select ';
        $sql .= '       count(*)                as count  ';
        $sql .= '   from ';
        $sql .= '     mst_jmdn_names as a  ';
        $sql .= '   where ';
        $sql .= "     a.jmdn_code = '".$code."'  ";
        $ret = $this->MstShelfName->query($sql);
        if($ret[0][0]['count'] == 0 ){
            return false;
        }else{
            return true;
        }
    }
    // 保険請求コード
    private function checkClaimCode($code){
        $sql  = ' select ';
        $sql .= '       count(*)                as count  ';
        $sql .= '   from ';
        $sql .= '     mst_insurance_claims as a  ';
        $sql .= '   where ';
        $sql .= "     a.insurance_claim_code = '".$code."'  ";
        $ret = $this->MstInsuranceClaim->query($sql);
        if($ret[0][0]['count'] == 0 ){
            return false;
        }else{
            return true;
        }
    }
    // 販売元
    private function checkDealer($code, $name){
        $sql  = ' select ';
        $sql .= '       count(*)                as count  ';
        $sql .= '   from ';
        $sql .= '     mst_dealers as a  ';
        $sql .= '   where ';
        $sql .= "     a.dealer_code = '".$code."'  ";
        $sql .= "     and a.dealer_name = '".$name."'  ";
        $ret = $this->MstDealer->query($sql);
        if($ret[0][0]['count'] == 0 ){
            return false;
        }else{
            return true;
        }
    }
    // クラス分類
    private function checkClassSeparation($code){
        $sql  = ' select ';
        $sql .= '       count(*)            as count  ';
        $sql .= '   from ';
        $sql .= '     mst_class_separations as a  ';
        $sql .= '   where ';
        $sql .= "     a.class_separation_code = '".$code."' ";
        $ret = $this->MstClassSeparation->query($sql);
        if($ret[0][0]['count'] == 0 ){
            return false;
        }else{
            return true;
        }

    }

    // 施設IDを2つ受け取って、施設関連IDを取得する
    private function _getRelationID( $c_id , $h_id ){
        $sql  = ' select ';
        $sql .= '       id ';
        $sql .= '   from ';
        $sql .= '     mst_facility_relations as a  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_id = ' . $c_id;
        $sql .= '     and a.partner_facility_id = ' . $h_id;
        $ret = $this->MstFacilityRelation->query($sql);
        return $ret[0][0]['id'];
    }

    // 包装単位IDを受け取って、採用品IDを取得する。
    private function _getFacilityItemId($id){
        $sql = ' select mst_facility_item_id from mst_item_units as a where a.id = ' . $id;
        $ret = $this->MstItemUnit->query($sql);
        return $ret[0][0]['mst_facility_item_id'];
    }

    /**
     * 商品IDと包装単位入り数を受けて、包装単位IDを返す
     */
    private function _getMstItemUnitId($internal_code , $per_unit){
        $sql  = ' select ';
        $sql .= '       b.id  ';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on a.id = b.mst_facility_item_id  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= "     and a.internal_code = '".$internal_code."'  ";
        $sql .= '     and b.per_unit = ' . $per_unit;

        $ret = $this->MstFacilityItem->query($sql);

        return $ret[0][0]['id'];
    }

    /**
     * 包装単位IDと部署IDを受けて、在庫IDを返す
     * 在庫レコードが存在しない場合は作成してIDを返す
     */
    private function _getStockId($mst_item_unit_id , $mst_department_id , $now){
        $sql  = ' select ';
        $sql .= '       a.id  ';
        $sql .= '   from ';
        $sql .= '     trn_stocks as a  ';
        $sql .= '   where ';
        $sql .= '     a.mst_item_unit_id = ' . $mst_item_unit_id;
        $sql .= '     and a.mst_department_id = ' . $mst_department_id;

        $ret = $this->TrnStock->query($sql);
        if(isset($ret[0][0]['id'])){
            return $ret[0][0]['id'];
        }else{
            // 在庫レコードがない場合作成する
            $stock = array(
                'TrnStock'=> array(
                    'mst_item_unit_id'  => $mst_item_unit_id,
                    'mst_department_id' => $mst_department_id,
                    'stock_count'       => 0,
                    'reserve_count'     => 0,
                    'promise_count'     => 0,
                    'requested_count'   => 0,
                    'receipted_count'   => 0,
                    'is_deleted'        => false,
                    'creater'           => $this->Session->read('Auth.MstUser.id'),
                    'created'           => $now,
                    'modifier'          => $this->Session->read('Auth.MstUser.id'),
                    'modified'          => $now,
                    )
                );
            $this->TrnStock->create();
            $this->TrnStock->save($stock);

            return $this->TrnStock->getLastInsertID();
        }
    }

    /**
     * 棚名と部署IDを受けて、棚名IDを返す
     */
    private function _getShelfNameId($shelf_name , $mst_department_id ){
        if($shelf_name == '' ){
            return null;
        }
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '   from ';
        $sql .= '     mst_shelf_names as a  ';
        $sql .= '   where ';
        $sql .= "     a.name = '" . $shelf_name. "'";
        $sql .= '     and a.mst_department_id = ' . $mst_department_id;
        $ret = $this->MstShelfName->query($sql);

        return $ret[0][0]['id'];
    }

    /**
     * 勘定科目名称を受けてIDを返す
     */
    private function _getAccountListId($account_name){
        if($account_name == ''){
            return null;
        }
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '   from ';
        $sql .= '     mst_accountlists as a  ';
        $sql .= '   where ';
        $sql .= "     a.name = '" . $account_name . "'";
        $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $ret = $this->MstAccountlist->query($sql);
        if(isset($ret[0][0]['id'])){
            return $ret[0][0]['id'];
        }else{
            return null;
        }
    }
    /**
     * 商品カテゴリ名称を受けて商品カテゴリコードを返す
     */
    private function _getItemCategoryCode($name,$rank){
        if($name == ''){
            return null;
        }
        $sql  =' select ';
        $sql .= '       a.id  ';
        $sql .= '   from ';
        $sql .= '     mst_item_categories as a  ';
        $sql .= '   where ';
        $sql .= "     a.category_name = '".$name."'";
        $sql .= "     and a.category_rank = " . $rank ;
        $ret = $this->MstItemCategory->query($sql);
        if(isset($ret[0][0]['id'])){
            return $ret[0][0]['id'];
        }else{
            return null;
        }
    }

    /**
     * 保険請求区分名を受けて保険請求区分コードを返す
     */
    private function _getClaimTypeCode($name){
        if($name == ''){
            return null;
        }
        $sql  = ' select ';
        $sql .= '       a.const_cd  ';
        $sql .= '   from ';
        $sql .= '     mst_consts as a  ';
        $sql .= '   where ';
        $sql .= "     a.const_nm = '".$name."' ";
        $sql .= "     and a.const_group_cd='".Configure::read('ConstGroupType.insuranceClaimType')."'";

        $ret = $this->MstConst->query($sql);
        if(isset($ret[0][0]['const_cd'])){
            return $ret[0][0]['const_cd'];
        }else{
            return null;
        }
    }

    /**
     * 包装単位コードを受けて、IDを返す
     */
    private function _getUnitNameId($code){
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '   from ';
        $sql .= '     mst_unit_names as a ';
        $sql .= '   where ';
        $sql .= "     a.internal_code = '".$code."'";

        $ret = $this->MstUnitName->query($sql);
        if(isset($ret[0][0]['id'])){
            return $ret[0][0]['id'];
        }else{
            return false;
        }
    }

    /**
     * 販売元コードを受けて、IDを返す
     */
    private function _getDealerId($code){
        if($code == ''){
            return null;
        }

        $sql  = ' select ';
        $sql .= '     id ';
        $sql .= ' from ';
        $sql .= '     mst_dealers as a ';
        $sql .= ' where ';
        $sql .= "     a.dealer_code = '" .$code. "'";

        $ret = $this->MstDealer->query($sql);
        if(isset($ret[0][0]['id'])){
            return $ret[0][0]['id'];
        }else{
            return false;
        }
    }

    /**
     * 登録データ作成後重複チェック
     * 仕入設定
     */
    private function duplicateCheckTransactionConfig($ids){
        $sql  = ' select ';
        $sql .= '       a.mst_item_unit_id ';
        $sql .= '     , a.mst_facility_id ';
        $sql .= '     , a.partner_facility_id ';
        $sql .= '     , a.start_date  ';
        $sql .= '   from ';
        $sql .= '     mst_transaction_configs as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',' , $ids) . ')  ';
        $sql .= '   group by ';
        $sql .= '     a.mst_item_unit_id ';
        $sql .= '     , a.mst_facility_id ';
        $sql .= '     , a.partner_facility_id ';
        $sql .= '     , a.start_date ';

        $ret = $this->MstTransactionConfig->query($sql);
        $return = array();
        foreach($ret as $r){
            $sql  = ' select ';
            $sql .= '       count(*)              as count  ';
            $sql .= '     , d.internal_code ';
            $sql .= '     , c.per_unit ';
            $sql .= '     , b.facility_name ';
            $sql .= "     , to_char(a.start_date,'YYYY/mm/dd') as start_date ";
            $sql .= '   from ';
            $sql .= '     mst_transaction_configs as a  ';
            $sql .= '     left join mst_facilities as b ';
            $sql .= '       on b.id = a.partner_facility_id ';
            $sql .= '     left join mst_item_units as c ';
            $sql .= '       on c.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as d ';
            $sql .= '       on d.id = c.mst_facility_item_id ';
            $sql .= '   where ';
            $sql .= '     a.mst_item_unit_id = ' . $r[0]['mst_item_unit_id'];
            $sql .= '     and a.mst_facility_id = ' . $r[0]['mst_facility_id'];
            $sql .= '     and a.partner_facility_id = ' . $r[0]['partner_facility_id'];
            $sql .= "     and a.start_date = '" . $r[0]['start_date'] . "' ";
            $sql .= '   group by ';
            $sql .= '     d.internal_code ';
            $sql .= '     , c.per_unit ';
            $sql .= '     , b.facility_name ';
            $sql .= '     , a.start_date ';

            $check = $this->MstTransactionConfig->query($sql);
            if($check[0][0]['count'] > 1 ){
                $return[] = $check[0];
            }
        }
        return $return;
    }
    /**
     * 代表フラグ重複チェック
     * 仕入設定
     */
    private function duplicateCheckTransactionIsMain($ids){
        $sql  = ' select ';
        $sql .= '       a.mst_facility_item_id ';
        $sql .= '     , a.mst_facility_id ';
        $sql .= '     , a.partner_facility_id ';
        $sql .= '   from ';
        $sql .= '     mst_transaction_configs as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',' , $ids) . ')  ';
        $sql .= '   group by ';
        $sql .= '     a.mst_facility_item_id ';
        $sql .= '     , a.mst_facility_id ';
        $sql .= '     , a.partner_facility_id ';

        $ret = $this->MstTransactionConfig->query($sql);
        $return = array();
        foreach($ret as $r){
            $sql  = ' select ';
            $sql .= '       count(case when a.is_main = true then 1 end) as count  ';
            $sql .= '     , d.internal_code';
            $sql .= '   from ';
            $sql .= '     mst_transaction_configs as a  ';
            $sql .= '     left join mst_facilities as b ';
            $sql .= '       on b.id = a.partner_facility_id ';
            $sql .= '     left join mst_item_units as c ';
            $sql .= '       on c.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as d ';
            $sql .= '       on d.id = c.mst_facility_item_id ';
            $sql .= '   where ';
            $sql .= '     a.mst_facility_item_id = ' . $r[0]['mst_facility_item_id'];
            $sql .= '     and a.mst_facility_id = ' . $r[0]['mst_facility_id'];
            $sql .= '     and a.partner_facility_id = ' . $r[0]['partner_facility_id'];
            $sql .= '   group by ';
            $sql .= '     d.internal_code';
            $sql .= '     , a.mst_facility_id';
            $sql .= '     , a.partner_facility_id';

            $check = $this->MstTransactionConfig->query($sql);
            if($check[0][0]['count'] > 1 ){
                $return[] = $check[0];
            }
        }
        return $return;
    }

    /**
     * 仕入マスタ：終了日を設定する。
     */
    private function setTransactionEndDate($ids){
        $sql  = ' select ';
        $sql .= '       a.mst_item_unit_id ';
        $sql .= '     , a.mst_facility_id ';
        $sql .= '     , a.partner_facility_id ';
        $sql .= '   from ';
        $sql .= '     mst_transaction_configs as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',' , $ids) . ')  ';
        $sql .= '   group by ';
        $sql .= '     a.mst_item_unit_id ';
        $sql .= '     , a.mst_facility_id ';
        $sql .= '     , a.partner_facility_id ';
        $ret = $this->MstTransactionConfig->query($sql);
        foreach($ret as $r){
            $sql  = ' select ';
            $sql .= '       a.id  ';
            $sql .= '       , a.start_date  ';
            $sql .= '   from ';
            $sql .= '     mst_transaction_configs as a  ';
            $sql .= '   where ';
            $sql .= '     a.mst_item_unit_id = ' . $r[0]['mst_item_unit_id'];
            $sql .= '     and a.mst_facility_id = ' . $r[0]['mst_facility_id'];
            $sql .= '     and a.partner_facility_id = ' . $r[0]['partner_facility_id'];
            $sql .= '   order by a.start_date desc ';

            $ret2 = $this->MstTransactionConfig->query($sql);
            $end_date = '3000/01/01';
            foreach($ret2 as $r2){
                $this->MstTransactionConfig->save(array('id'=>$r2[0]['id'] , 'end_date'=>$end_date));
                $end_date = $r2[0]['start_date'];
            }
        }
    }

    /**
     * 登録データ作成後重複チェック
     * 売上設定
     */
    private function duplicateCheckSalesConfig($ids){
        $sql  = ' select ';
        $sql .= '       a.mst_item_unit_id ';
        $sql .= '     , a.mst_facility_id ';
        $sql .= '     , a.partner_facility_id ';
        $sql .= '     , a.start_date  ';
        $sql .= '   from ';
        $sql .= '     mst_sales_configs as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',' , $ids) . ')  ';
        $sql .= '   group by ';
        $sql .= '     a.mst_item_unit_id ';
        $sql .= '     , a.mst_facility_id ';
        $sql .= '     , a.partner_facility_id ';
        $sql .= '     , a.start_date ';

        $ret = $this->MstSalesConfig->query($sql);
        $return = array();
        foreach($ret as $r){
            $sql  = ' select ';
            $sql .= '     d.internal_code ';
            $sql .= '     , c.per_unit ';
            $sql .= '     , b.facility_name ';
            $sql .= "     , to_char(a.start_date,'YYYY/mm/dd') as start_date";
            $sql .= '     , count(*)              as count  ';
            $sql .= '   from ';
            $sql .= '     mst_sales_configs as a  ';
            $sql .= '     left join mst_facilities as b ';
            $sql .= '       on b.id = a.partner_facility_id ';
            $sql .= '     left join mst_item_units as c ';
            $sql .= '       on c.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as d ';
            $sql .= '       on d.id = c.mst_facility_item_id ';
            $sql .= '   where ';
            $sql .= '     a.mst_item_unit_id = ' . $r[0]['mst_item_unit_id'];
            $sql .= '     and a.mst_facility_id = ' . $r[0]['mst_facility_id'];
            $sql .= '     and a.partner_facility_id = ' . $r[0]['partner_facility_id'];
            $sql .= "     and a.start_date = '" . $r[0]['start_date'] . "' ";
            $sql .= '   group by ';
            $sql .= '     d.internal_code ';
            $sql .= '     , c.per_unit ';
            $sql .= '     , b.facility_name ';
            $sql .= '     , a.start_date ';

            $check = $this->MstSalesConfig->query($sql);
            if($check[0][0]['count'] > 1 ){
                $return[] = $check[0];
            }
        }
        return $return;
    }
    /**
     * 売上マスタ：終了日を設定する。
     */
    private function setSalesEndDate($ids){
        $sql  = ' select ';
        $sql .= '       a.mst_item_unit_id ';
        $sql .= '     , a.mst_facility_id ';
        $sql .= '     , a.partner_facility_id ';
        $sql .= '   from ';
        $sql .= '     mst_sales_configs as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',' , $ids) . ')  ';
        $sql .= '   group by ';
        $sql .= '     a.mst_item_unit_id ';
        $sql .= '     , a.mst_facility_id ';
        $sql .= '     , a.partner_facility_id ';
        $ret = $this->MstSalesConfig->query($sql);
        foreach($ret as $r){
            $sql  = ' select ';
            $sql .= '       a.id  ';
            $sql .= '       , a.start_date  ';
            $sql .= '   from ';
            $sql .= '     mst_sales_configs as a  ';
            $sql .= '   where ';
            $sql .= '     a.mst_item_unit_id = ' . $r[0]['mst_item_unit_id'];
            $sql .= '     and a.mst_facility_id = ' . $r[0]['mst_facility_id'];
            $sql .= '     and a.partner_facility_id = ' . $r[0]['partner_facility_id'];
            $sql .= '   order by a.start_date desc ';

            $ret2 = $this->MstSalesConfig->query($sql);
            $end_date = '3000/01/01';
            foreach($ret2 as $r2){
                $this->MstSalesConfig->save(array('id'=>$r2[0]['id'] , 'end_date'=>$end_date));
                $end_date = $r2[0]['start_date'];
            }
        }
    }

    /**
     * 登録データ作成後重複チェック
     * 定数設定
     */
    private function duplicateCheckFixedCount($ids , $type){
        $sql  = ' select ';
        if($type == Configure::read('FacilityType.center')){
            $sql .= '       a.mst_item_unit_id ';
        }else{
            $sql .= '       a.mst_facility_item_id ';
        }
        $sql .= '     , a.mst_facilities_id ';
        $sql .= '     , a.mst_department_id ';
        $sql .= '   from ';
        $sql .= '     mst_fixed_counts as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',' , $ids) . ')  ';
        $sql .= '   group by ';
        if($type == Configure::read('FacilityType.center')){
            $sql .= '       a.mst_item_unit_id ';
        }else{
            $sql .= '       a.mst_facility_item_id ';
        }
        $sql .= '     , a.mst_facilities_id ';
        $sql .= '     , a.mst_department_id ';

        $ret = $this->MstFixedCount->query($sql);
        $return = array();
        foreach($ret as $r){
            $sql  = ' select ';
            $sql .= '       count(*)              as count  ';
            if($type == Configure::read('FacilityType.center')){
                $sql .= '   , d.per_unit';
            }
            $sql .= '     , e.internal_code';
            $sql .= '     , c.facility_name';
            $sql .= '     , b.department_name';
            $sql .= '   from ';
            $sql .= '     mst_fixed_counts as a  ';
            $sql .= '     left join mst_departments as b';
            $sql .= '       on b.id = a.mst_department_id';
            $sql .= '     left join mst_facilities as c';
            $sql .= '       on c.id = b.mst_facility_id';
            $sql .= '     left join mst_item_units as d';
            $sql .= '       on d.id = a.mst_item_unit_id';
            $sql .= '     left join mst_facility_items as e';
            $sql .= '       on e.id = d.mst_facility_item_id';
            $sql .= '   where ';
            if($type == Configure::read('FacilityType.center')){
                $sql .= '     a.mst_item_unit_id = ' . $r[0]['mst_item_unit_id'];
            }else{
                $sql .= '     a.mst_facility_item_id = ' . $r[0]['mst_facility_item_id'];
            }
            $sql .= '     and a.mst_facilities_id = ' . $r[0]['mst_facilities_id'];
            $sql .= '     and a.mst_department_id = ' . $r[0]['mst_department_id'];
            $sql .= '   group by ';
            $sql .= '       e.internal_code';
            $sql .= '     , c.facility_name';
            $sql .= '     , b.department_name';

            if($type == Configure::read('FacilityType.center')){
                $sql .= '   , d.per_unit';
            }

            $check = $this->MstFixedCount->query($sql);
            if($check[0][0]['count'] > 1 ){
                $return[] = $check[0];
            }
        }
        return $return;
    }

    /**
     * 登録データ作成後重複チェック
     * 採用品
     */
    private function duplicateCheckFacilityItem($ids){
        $sql  = ' select ';
        $sql .= '       a.internal_code ';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',' , $ids) . ')  ';
        $sql .= '   group by ';
        $sql .= '       a.internal_code ';

        $ret = $this->MstFacilityItem->query($sql);
        $return = array();
        foreach($ret as $r){
            $sql  = ' select ';
            $sql .= '       count(*)              as count  ';
            $sql .= '     , a.internal_code ';
            $sql .= '   from ';
            $sql .= '     mst_facility_items as a  ';
            $sql .= '   where ';
            $sql .= "     a.internal_code = '" . $r[0]['internal_code'] . "'";
            $sql .= '     and a.item_type = ' . Configure::read('Items.item_types.normalitem');
            $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '   group by a.internal_code ';
            $check = $this->MstFacilityItem->query($sql);
            if($check[0][0]['count'] > 1 ){
                $return[] = $check[0];
            }
        }
        return $return;
    }

    /**
     * 包装単位を登録
     * 既存データがあったら更新
     * 既存データがなかったら追加
     */
    private function saveMstItemUnit($mst_facility_item_id , $mst_unit_name_id , $pre_unit , $per_unit_code , $start_date , $is_standard){
        if($is_standard){
            // 代表フラグがONだったら、一度すべてOFFにする
            if (!$this->MstItemUnit->updateAll(array('MstItemUnit.is_standard' => "'false'")
                                            , array('MstItemUnit.mst_facility_item_id'  => $mst_facility_item_id)
                                            , -1
                                            )
                ) {
                return false;
            }
        }

        // 既存データがあるか確認
        $sql  = ' select ';
        $sql .= '       a.id  ';
        $sql .= '   from ';
        $sql .= '     mst_item_units as a  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_item_id = ' . $mst_facility_item_id;
        $sql .= '     and a.per_unit = ' . $pre_unit;
        $ret = $this->MstItemUnit->query($sql);
        $save_data = array();
        if(isset($ret[0][0]['id'])){
            // 既存
            $save_data['MstItemUnit']['id'] = $ret[0][0]['id'];
        }else{
            // 新規
            $save_data['MstItemUnit']['creater']    = $this->Session->read('Auth.MstUser.id');
            $save_data['MstItemUnit']['created']    = $this->request->data['now'];
            $save_data['MstItemUnit']['end_date']   = null;
            $save_data['MstItemUnit']['is_deleted'] = false;
        }

        $save_data['MstItemUnit']['mst_facility_item_id'] = $mst_facility_item_id;
        $save_data['MstItemUnit']['mst_facility_id']      = $this->Session->read('Auth.facility_id_selected');
        $save_data['MstItemUnit']['per_unit']             = $pre_unit;
        $save_data['MstItemUnit']['mst_unit_name_id']     = $this->_getUnitNameId($per_unit_code);
        $save_data['MstItemUnit']['start_date']           = $start_date;
        $save_data['MstItemUnit']['is_standard']          = $is_standard;
        $save_data['MstItemUnit']['modifier']             = $this->Session->read('Auth.MstUser.id');
        $save_data['MstItemUnit']['modified']             = $this->request->data['now'];
        $save_data['MstItemUnit']['per_unit_name_id']     = $mst_unit_name_id;
        $this->MstItemUnit->create();
        if($this->MstItemUnit->save($save_data)){
            if(!isset($ret[0][0]['id'])){
                // センター在庫レコード作成
                return $this->getStockRecode($this->MstItemUnit->getLastInsertId());
            }
            return true;
        }else{
            return false;
        }
    }

    function getItemUnit($mst_facility_item_id , $mst_facility_id){
        $sql  = ' select ';
        $sql .= '     id                     as "MstItemUnit__id" ';
        $sql .= '   , mst_facility_item_id   as "MstItemUnit__mst_facility_item_id" ';
        $sql .= '   , mst_facility_id        as "MstItemUnit__mst_facility_id" ';
        $sql .= '   , per_unit               as "MstItemUnit__per_unit" ';
        $sql .= '   , mst_unit_name_id       as "MstItemUnit__mst_unit_name_id" ';
        $sql .= "   , to_char(start_date, 'YYYY/mm/dd') ";
        $sql .= '                            as "MstItemUnit__start_date" ';
        $sql .= "   , to_char(end_date, 'YYYY/mm/dd')  ";
        $sql .= '                            as "MstItemUnit__end_date" ';
        $sql .= '   , is_standard            as "MstItemUnit__is_standard" ';
        $sql .= '   , is_deleted             as "MstItemUnit__is_deleted" ';
        $sql .= '   , ( case when is_deleted = true then false else true end ) ';
        $sql .= '                            as "MstItemUnit__checked" ';
        $sql .= '   , creater                as "MstItemUnit__creater" ';
        $sql .= '   , created                as "MstItemUnit__created" ';
        $sql .= '   , modifier               as "MstItemUnit__modifier" ';
        $sql .= '   , modified               as "MstItemUnit__modified" ';
        $sql .= '   , per_unit_name_id       as "MstItemUnit__per_unit_name_id" ';
        $sql .= '   , center_facility_id     as "MstItemUnit__center_facility_id"  ';
        $sql .= ' from ';
        $sql .= '   mst_item_units  ';
        $sql .= ' where ';
        $sql .= '   mst_facility_item_id = ' . $mst_facility_item_id;
        $sql .= '   and mst_facility_id = ' . $mst_facility_id;
        $sql .= ' order by ';
        $sql .= '   per_unit asc ';
        
        $ret = $this->MstItemUnit->query($sql);
        return $ret;
    }

    public function saveOneItemUnit ($_fi = null, $_facility_item_id = null) {
        $ItemUnit = null;

        $ItemUnit['mst_facility_item_id'] = (integer)$_facility_item_id;
        $ItemUnit['mst_facility_id']      = $this->Session->read('Auth.facility_id_selected');
        $ItemUnit['per_unit']             = 1;
        $ItemUnit['mst_unit_name_id']     = (integer)$_fi['mst_unit_name_id'];
        $ItemUnit['start_date']           = $_fi['start_date'];
        $ItemUnit['end_date']             = '3000/01/01';
        $ItemUnit['is_standard']          = true;
        $ItemUnit['is_deleted']           = false;
        $ItemUnit['creater']              = $this->Session->read('Auth.MstUser.id');
        $ItemUnit['created']              = $this->request->data['now'];
        $ItemUnit['modifier']             = $this->Session->read('Auth.MstUser.id');
        $ItemUnit['modified']             = $this->request->data['now'];
        $ItemUnit['per_unit_name_id']     = $_fi['mst_unit_name_id'];
       
        $this->MstItemUnit->create();
        return $this->MstItemUnit->save($ItemUnit, array('validate'=>true));
    }

    private function getHospitalId($c_id){
        $sql  = ' select ';
        $sql .= '       a.partner_facility_id  ';
        $sql .= '   from ';
        $sql .= '     mst_facility_relations as a  ';
        $sql .= '   left join mst_facilities as b ';
        $sql .= '     on b.id = a.partner_facility_id ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_id = ' . $c_id;
        $sql .= '     and b.facility_type = ' . Configure::read('FacilityType.hospital');
        $ret = $this->MstFacilityRelation->query($sql);

        return $ret[0][0]['partner_facility_id'];
    }

    
    // 以下同種同効品検索/見積もり依頼関連
    public function same_index(){
        $items = $this->findSameItemData($this->request->data['MstFacilityItem']['master_id'], true);
        $this->set('items', $items);
        $this->set('baseItemName', $this->request->data['MstFacilityItem']['item_name']);
    }

    public function same_order(){
        $items = $this->findMasterItems($this->request->data['grandmaster_ids']);
        $this->set('items', $items);
        
        $edis = $this->Rfp->findEdiFacilities($this->Session->read('Auth.facility_id_selected'));
        $this->set('edis', $edis);
    }
    
    public function same_result(){
        $this->request->data['work_no'] = $this->setWorkNo4Header(date('Ymd'), "30");
        $this->request->data['login_user_id'] = $this->Session->read('Auth.MstUser.id');
        $this->request->data['with_send_mail'] = true;
        
        $result = $this->Rfp->doRfpTransaction($this->request->data);
        if (empty($result)) {
            $this->Session->setFlash('見積依頼処理中にエラーが発生したため処理を中止しました。', 'growl', array('type' => 'error'));
            $this->redirect('edit_search');
        }
    }
    
    
    private function getSameItemCount($masterId) {
        return $this->findSameItemData($masterId, true, true);
    }
    
    private function findSameItemData($masterId, $isOwnExclude = false, $isCountOnly = false) {
        // ベースとなるItemの同種同効の規格番号を取得
        $options = array(
            // 再帰検索しない
            'recursive' => -1,
                'fields' => array(
                    'master_id', // 同種同効規格番号
                    'same_standard_nums', // 同種同効規格番号
                ),
                // 検索条件
                'conditions' => array(
                        'master_id'  => $masterId,
                        'is_deleted' => false,
                ),
        );
        // 検索実行
        $data = $this->MstSameItem->find('first', $options);
        if (empty($data)) return false;
        
        
        $baseMstSameItem = $data['MstSameItem'];
        $sameStandardNums = explode(',', $baseMstSameItem['same_standard_nums']); // 配列へ変換
        $maxStandardNo = max($sameStandardNums); // このItemの同種同効規格番号の最大値を取得
        
        
        // ベースとなるItemの規格情報を取得(同種同効品検索のキーとして必要な情報)
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                'fields' => array(
                        '"MstItemStandard"."master_id"     AS "master_id"'      // マスターID(コスロスCD)
                ),
                // 検索条件
                'conditions' => array(
                        'MstItemStandard.master_id' => $masterId,
                        'MstItemStandard.is_deleted' => false,
                ),
                // 連結するモデル
                'joins' => array(
                        array(
                                'table' => 'mst_grand_kyo_materials',
                                'alias' => 'MstGrandKyoMaterial',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstGrandKyoMaterial.matcd' => $baseMstSameItem['master_id'],
                                ),
                        ),
                ),
                'group' => array(
                        'MstItemStandard.master_id'
                ),
        );
        // Item毎に存在する1～Nまでの複数の規格情報を横展開した形式で取得
        foreach ($sameStandardNums as $standardNo) {
            $options['fields'][] = 
                "MAX(CASE standard_no WHEN $standardNo THEN standard_name ELSE '' END) AS \"standard_name_$standardNo\"";
        }
        $data = $this->MstItemStandard->find('first', $options);
        $baseMstItemStandard = $data[0];
        
        
        // 全Itemの規格情報を横展開した形式で取得するためのサブクエリ―を生成(前回のoptionsを流用)
        // 同種同効品を検索するため動的なビューとして使用する
        $options['conditions'] = array(); // 全件取得を行うため条件はクリア
        $options['table'] = 'mst_item_standards';
        $options['alias'] = 'MstItemStandard';
        $db = $this->MstItemStandard->getDataSource();
        $subQuery = $db->buildStatement($options, $this->MstItemStandard);
        
        
        // 同種同効品情報を取得
        $conditionsForStdNames = array();
        if ($isOwnExclude) {
            $conditionsForStdNames[] = 'ItemStandard.master_id != \'' . $baseMstItemStandard['master_id'] . '\'';
        }
        // $conditionsForStdNames['ItemStandard.item_category2'] = $baseMstItemStandard['item_category2'];
        foreach ($baseMstItemStandard as $field => $value) {
            if (preg_match('/^standard_name_[0-9]+$/', $field) === 1) {
                $conditionsForStdNames['ItemStandard.' . $field] = $value;
            }
        }
        $conditionsForStdNames[] = 'ItemStandard.master_id = MstGrandKyoMaterial.matcd';
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                'fields' => array(
                        '"MstGrandKyoMaterial"."id"',
                ),
                // 検索条件
                'conditions' => array(
                        'MstGrandKyoMaterial.is_deleted' => false,
                ),
                // 連結するモデル
                'joins' => array(
                        array(
                                'table' =>  "($subQuery)",
                                'alias' => 'ItemStandard',
                                'type' => 'INNER',
                                'conditions' => $conditionsForStdNames,
                        ),
                ),
        );
        
        // 検索実行
        $data = null;
        if ($isCountOnly) {
            $options['fields'] = array();
            $data = $this->MstGrandKyoMaterial->find('count', $options);
        }else{
            $data = $this->MstGrandKyoMaterial->find('list', $options);
            $ids = array_keys($data);
            $data = $this->findMasterItems($ids);
        }
        
        return $data;
    }
    
    private function findMasterItems($ids) {
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                'fields' => array(
                        '"MstGrandKyoMaterial"."id"        AS "MstGrandmaster__id"',          // プライマリーID
                        '"MstGrandKyoMaterial"."matcd"     AS "MstGrandmaster__master_id"',   // コスロスID
                        '"MstGrandKyoMaterial"."mkmatcd"   AS "MstGrandmaster__item_code"',   // 製品番号
                        '"MstGrandKyoMaterial"."stdjancd"  AS "MstGrandmaster__jan_code"',    // JANコード
                        '"MstGrandKyoMaterial"."makcocd"   AS "MstGrandmaster__dealer_name"', // 販売元
                        '"MstGrandKyoMaterial"."matname"   AS "MstGrandmaster__item_name"',   // 商品名
                        '"MstGrandKyoMaterial"."sizdimnam" AS "MstGrandmaster__standard"',    // 規格
                        '"MstFacilityItem"."id"',
                ),
                // 検索条件
                'conditions' => array(
                    'MstGrandKyoMaterial.id' => $ids,
                    'MstGrandKyoMaterial.is_deleted' => false,
                ),
                // 連結するモデル
                'joins' => array(
                        array(
                                'table' => 'mst_facility_items',
                                'alias' => 'MstFacilityItem',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstFacilityItem.master_id = MstGrandKyoMaterial.matcd',
                                ),
                        ),
                ),
                // 並び順
                'order' => array(
                        'MstGrandKyoMaterial.matcd' => 'DESC',
                ),
        );

        // 検索実行
        $data = $this->MstGrandKyoMaterial->find('all', $options);
        
        return $data;
    }
    
    function adds_result () {
        $now = date('Y/m/d H:i:s');
        // トランザクション開始
        $this->MstFacilityItem->begin();
        
        foreach($this->request->data['MstGrandmaster']['opt'] as $matcd){
            $tmp[] = $item =  $this->_MstGrandmaster($matcd);
            
            // 既定の商品IDのMAX + 1 を取得
            $internal_code = $this->getNextInternalCode(
                Configure::read('ItemType.masterItem') ,
                $this->Session->read('Auth.facility_id_selected')
                );
            
            // 採用品
            $MstFacilityItem = array(
                'MstFacilityItem'=> array(
                    'master_id'             => $matcd,
                    'internal_code'         => $internal_code ,
                    'item_name'             => $item['MstFacilityItem']['item_name'],
                    'standard'              => $item['MstFacilityItem']['standard'],
                    'item_code'             => $item['MstFacilityItem']['item_code'],
                    'jan_code'              => $item['MstFacilityItem']['jan_code'],
                    'unit_price'            => $item['MstFacilityItem']['unit_price'],
                    'refund_price'          => $item['MstFacilityItem']['refund_price'],
                    'pharmaceutical_number' => $item['MstFacilityItem']['pharmaceutical_number'],
                    'per_unit'              => 1,
                    'mst_unit_name_id'      => (is_null($item['MstFacilityItem']['mst_unit_name_id'])?0:$item['MstFacilityItem']['mst_unit_name_id']),
                    'class_separation'      => $item['MstFacilityItem']['class_separation'],
                    'biogenous_type'        => $item['MstFacilityItem']['biogenous_type'],
                    'insurance_claim_code'  => $item['MstFacilityItem']['insurance_claim_code'],
                    'insurance_claim_type'  => $item['MstFacilityItem']['insurance_claim_type'],
                    'is_auto_storage'       => true,
                    'buy_flg'               => false ,
                    'converted_num'         => 1,
                    'mst_dealer_id'         => $item['MstDealer']['id'],
                    'tax_type'              => $item['MstFacilityItem']['tax_type'],
                    'lot_ubd_alert'         => 0,
                    'item_type'             => Configure::read('Items.item_types.normalitem'),
                    'start_date'            => date('Y/m/d'),
                    'mst_facility_id'       => $this->Session->read('Auth.facility_id_selected'),
                    'is_lowlevel'           => false ,
                    'is_ms_corporate'       => false ,
                    'is_deleted'            => false ,
                    'creater'               => $this->Session->read('Auth.MstUser.id'),
                    'created'               => $now,
                    'modifier'              => $this->Session->read('Auth.MstUser.id'),
                    'modified'              => $now
                    )
                );
            
            $this->MstFacilityItem->create();
            // SQL実行
            if (!$this->MstFacilityItem->save($MstFacilityItem)) {
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash('採用品登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('search');
            }
        }
        
        // コミット
        $this->MstFacilityItem->commit();
        $this->set('tmp',$tmp);
    }
}
