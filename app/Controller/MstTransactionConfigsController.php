<?php
/**
 * MstTransactionConfigsController
 * @version 1.0.0
 * @since 2010/07/22
 */

class MstTransactionConfigsController extends AppController {
    var $name = 'MstTransactionConfigs';

    /**
     *
     * @var array $uses
     */
    var $uses = array(
        'MstTransactionConfig',
        'MstTransactionMain',
        'MstItemUnit',
        'MstUnitName',
        'MstFacility',
        'MstFacilityItem',
        'MstUserBelonging',
        'MstDealer',
        'MstFacilityRelation',
        'MstUser',
        'MstSalesConfig',
        'MstInsuranceClaim',
        'MstInsuranceClaimDepartment'
        );

    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;

    /**
     * @var MstTransactionConfigs
     */
    var $MstTransactionConfigs;


    //ログインなしでの印刷を許可
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
    }


    public function index(){
        $this->redirect('search');
    }
    
    /**
     * search
     * 仕入設定一覧
     */
    function search() {
        $this->setRoleFunction(81); //仕入設定
        $TransactionConfig_Info = array();

        $this->set('suppliers_enabled',
                   $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                          array(Configure::read('FacilityType.supplier')),
                                          true,
                                          array('facility_code' , 'facility_name'),
                                          'facility_name, facility_code'
                                          )
                   );
        
        //検索ボタン押下
        if(isset($this->request->data['MstTransactionConfig']['is_search'])){
            App::import('Sanitize');
            $where = "";
            //ユーザ入力値による検索条件の作成------------------------------------------
            //仕入先(完全一致)
            if((isset($this->request->data['MstFacilityItem']['supplierCode'])) && ($this->request->data['MstFacilityItem']['supplierCode'] != "")){
                $where .= " and d.facility_code = '" . $this->request->data['MstFacilityItem']['supplierCode'] . "'";
            }
            
            //商品ID(前方一致)
            if((isset($this->request->data['MstFacilityItem']['search_internal_code'])) && ($this->request->data['MstFacilityItem']['search_internal_code'] != "")){
                $where .= " and a.internal_code  LIKE '" . $this->request->data['MstFacilityItem']['search_internal_code'] . "%'";
            }
            
            //商品名(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_item_name'])) && ($this->request->data['MstFacilityItem']['search_item_name'] != "")){
                $where .= " and a.item_name LIKE '%". $this->request->data['MstFacilityItem']['search_item_name']."%'";
            }

            //規格(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_standard'])) && ($this->request->data['MstFacilityItem']['search_standard'] != "")){
                $where .= " and a.standard LIKE '%" . $this->request->data['MstFacilityItem']['search_standard'] . "%'";
            }
            
            //製品番号(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_item_code'])) && ($this->request->data['MstFacilityItem']['search_item_code'] != "")){
                $where .= " and a.item_code  LIKE '%" . $this->request->data['MstFacilityItem']['search_item_code'] ."%'";
            }
            //販売元(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_dealer_name'])) && ($this->request->data['MstFacilityItem']['search_dealer_name'] != "")){
                $where .=  " and b.dealer_name LIKE '%" . $this->request->data['MstFacilityItem']['search_dealer_name'] . "%'";
            }

            //JANコード(前方一致)
            if((isset($this->request->data['MstFacilityItem']['search_jan_code'])) && ($this->request->data['MstFacilityItem']['search_jan_code'] != "")){
                $where .= " and a.jan_code LIKE '" . $this->request->data['MstFacilityItem']['search_jan_code'] ."%'";
            }

            //仕入設定なし
            if(isset($this->request->data['MstTransactionConfig']['is_setting'])){
                $where .= ' and c.id is null ';
            }
            //検索----------------------------------------------------------------------
            $TransactionConfig_Info = $this->getItems($where);
        }

        $TransactionConfig_Info = AppController::outputFilter($TransactionConfig_Info);
        //************************************************************************
        //Viewへ遷移（検索の場合）
        $this->set('TransactionConfig_Info',$TransactionConfig_Info);
    }

    /**
     * Mod 仕入設定情報編集
     */
    function mod() {
        $this->Session->write('TransactionConfig.readTime',date('Y-m-d H:i:s'));
        //選択した商品情報を取得
        $sql  = ' select ';
        $sql .= '       a.id                                    as "MstFacilityItem__id" ';
        $sql .= '     , a.internal_code                         as "MstFacilityItem__internal_code" ';
        $sql .= '     , a.item_name                             as "MstFacilityItem__item_name" ';
        $sql .= '     , a.item_code                             as "MstFacilityItem__item_code" ';
        $sql .= '     , a.standard                              as "MstFacilityItem__standard" ';
        $sql .= '     , a.jan_code                              as "MstFacilityItem__jan_code" ';
        $sql .= '     , b.dealer_name                           as "MstFacilityItem__dealer_name" ';
        $sql .= '     , a.refund_price                          as "MstFacilityItem__refund_price" ';
        $sql .= '     , d.insurance_claim_name_s                as "MstFacilityItem__insurance_claim_name_s"  ';
        $sql .= '     , e.const_nm                              as "MstFacilityItem__insurance_claim_department_name"';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_dealers as b  ';
        $sql .= '       on b.id = a.mst_dealer_id  ';
        $sql .= '     left join mst_insurance_claims as d  ';
        $sql .= '       on d.insurance_claim_code = a.insurance_claim_code  ';
        $sql .= '     left join mst_consts as e ';
        $sql .= "       on e.const_cd = a.insurance_claim_type ::varchar ";
        $sql .= "      and e.const_group_cd = '" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->request->data['MstFacilityItem']['id'];

        $tmp = $this->MstFacilityItem->query($sql);
        $this->request->data = $tmp[0];
        
        //仕入設定の取得
        $TransactionConfig = $this->getTransactionConfig($this->request->data['MstFacilityItem']['id']);
        
        //代表仕入設定を取得
        $sql = ' select id as "MstTransactionMain__id",mst_facility_id as "MstTransactionMain__mst_facility_id", mst_item_unit_id as "MstTransactionMain__mst_item_unit_id" from mst_transaction_mains where mst_facility_item_id =  ' . $this->request->data['MstFacilityItem']['id'];
        $TrnMain = $this->MstTransactionMain->query($sql);
        if($TrnMain == array()){
            $TrnMain[0]['MstTransactionMain']['id']               = null;
            $TrnMain[0]['MstTransactionMain']['mst_facility_id']  = null;
            $TrnMain[0]['MstTransactionMain']['mst_item_unit_id'] = null;
        }
        $this->set('TrnMain' ,$TrnMain);
        
        //包装単位プルダウン用
        $this->set('UnitName_List',$this->setItemUnits());
        //仕入先プルダウン用
        $this->set('Suppliers_List',
                   $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                          array(Configure::read('FacilityType.supplier')),
                                          true,
                                          array('id' , 'facility_name'),
                                          'facility_name, facility_code'
                                          )
                   );

        //売上設定の取得
        $SalesConfig = $this->getSalesConfig($this->request->data['MstFacilityItem']['id']);
        
        $this->set('SalesConfig',$SalesConfig);
        $this->set('TransactionConfig',$TransactionConfig);
    }

    //
    //仕入設定印刷
    //
    function report(){
        $params = $this->getSearchParamater();
        $TransactionConfig_Info = $this->getReports($this->request->data['selected_facility_id'], $params);
        $this->print_config($TransactionConfig_Info,$this->request->data);
    }
    
    private function getSearchParamater(){
        App::import('Sanitize');
        $params = array('conditions' => array());
        //仕入先(完全一致)
        if((isset($this->request->data['MstFacilityItem']['supplierCode'])) && ($this->request->data['MstFacilityItem']['supplierCode'] != "")){
            $id = $this->MstFacility->find('first',array('conditions'=>array('MstFacility.facility_code'=>Sanitize::escape($this->request->data['MstFacilityItem']['supplierCode']),
                                                                             'MstFacility.facility_type'=> array(Configure::read('FacilityType.supplier'))
                                                                             )));
            $where = array('MstPartnerFacility.id' => Sanitize::escape($id['MstFacility']['id']));
            $params['conditions'] = array_merge($params['conditions'],$where);
        }
        //商品ID(前方一致)
        if((isset($this->request->data['MstFacilityItem']['search_internal_code'])) && ($this->request->data['MstFacilityItem']['search_internal_code'] != "")){
            $where = array('MstFacilityItem.internal_code LIKE' => Sanitize::escape($this->request->data['MstFacilityItem']['search_internal_code'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }

        //商品名(LIKE検索)
        if((isset($this->request->data['MstFacilityItem']['search_item_name'])) && ($this->request->data['MstFacilityItem']['search_item_name'] != "")){
            $where = array('MstFacilityItem.item_name LIKE' => "%". Sanitize::escape($this->request->data['MstFacilityItem']['search_item_name'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }

        //規格(LIKE検索)
        if((isset($this->request->data['MstFacilityItem']['search_standard'])) && ($this->request->data['MstFacilityItem']['search_standard'] != "")){
            $where = array('MstFacilityItem.standard LIKE' => "%".Sanitize::escape($this->request->data['MstFacilityItem']['search_standard'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }
        //製品番号(LIKE検索)
        if((isset($this->request->data['MstFacilityItem']['search_item_code'])) && ($this->request->data['MstFacilityItem']['search_item_code'] != "")){
            $where = array('MstFacilityItem.item_code LIKE' => "%".Sanitize::escape($this->request->data['MstFacilityItem']['search_item_code'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }

        //販売元(LIKE検索)
        if((isset($this->request->data['MstFacilityItem']['search_dealer_name'])) && ($this->request->data['MstFacilityItem']['search_dealer_name'] != "")){

            $where = array('MstDealer.dealer_name LIKE' => "%".Sanitize::escape($this->request->data['MstFacilityItem']['search_dealer_name'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }


        //JANコード(前方一致)
        if((isset($this->request->data['MstFacilityItem']['search_jan_code'])) && ($this->request->data['MstFacilityItem']['search_jan_code'] != "")){
            $where = array('MstFacilityItem.jan_code LIKE' => Sanitize::escape($this->request->data['MstFacilityItem']['search_jan_code'])."%");
            $params['conditions'] = array_merge($params['conditions'],$where);
        }

        return $params;
    }
    private function getReports($fid, $params){
        $where = ' 1 = 1 ';
        foreach($params['conditions'] as $_key => $_val){
            if(0 === preg_match('/LIKE|<|>/i', $_key)){
                $where .= " and {$_key} = '{$_val}' ";
            }else{
                $where .= " and {$_key} '{$_val}' ";
            }
        }
        $sql  = ' select ';
        $sql .=               ' MstFacilityItem.id                      as "MstFacilityItem__id"';
        $sql .=               ',MstFacilityItem.item_code               as "MstFacilityItem__item_code"';
        $sql .=               ',MstFacilityItem.internal_code           as "MstFacilityItem__internal_code"';
        $sql .=               ',MstFacilityItem.item_name               as "MstFacilityItem__item_name"';
        $sql .=               ',MstFacilityItem.standard                as "MstFacilityItem__standard"';
        $sql .=               ',MstFacilityItem.mst_facility_id         as "MstFacilityItem__mst_facility_id"';
        $sql .=               ',MstFacilityItem.tax_type                as "MstFacilityItem__tax_type"';
        $sql .=               ',MstDealer.dealer_name                   as "MstDealer__dealer_name"';
        $sql .=               ',(case ';
        $sql .=                 ' when MstItemUnit.per_unit = 1 then MstUnitName.unit_name ';
        $sql .=                 " else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')' ";
        $sql .=                 'end)                                   as "MstFacilityItem__unit_name"';
        $sql .=               ',MstTransactionConfig.id                 as "MstTransactionConfig__id"' ;
        $sql .=               ",to_char(MstTransactionConfig.start_date,'yyyy/mm/dd')";
        $sql .=                                                         'as "MstTransactionConfig__start_date"' ;
        $sql .=               ',MstTransactionConfig.transaction_price  as "MstTransactionConfig__transaction_price"' ;
        $sql .=               ',MstFacility.facility_formal_name        as "MstFacility__facility_formal_name"';
        $sql .=               ',MstPartnerFacility.facility_formal_name as "MstPartnerFacility__facility_formal_name"';
        $sql .=           ' from ' ;
        $sql .=               ' mst_facility_items as MstFacilityItem ' ;
        $sql .=               ' left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id ' ;
        $sql .=               ' inner join mst_item_units as MstItemUnit on MstFacilityItem.id = MstItemUnit.mst_facility_item_id';
        $sql .=               ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id';
        $sql .=               ' inner join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id';
        $sql .=               ' inner join mst_transaction_configs as MstTransactionConfig on MstTransactionConfig.mst_facility_item_id = MstFacilityItem.id and MstTransactionConfig.mst_item_unit_id = MstItemUnit.id';
        $sql .=               ' inner join mst_facilities as MstFacility on MstTransactionConfig.mst_facility_id = MstFacility.id and MstFacility.id = ' . $fid ;
        $sql .=               ' inner join mst_facilities as MstPartnerFacility on MstTransactionConfig.partner_facility_id =MstPartnerFacility.id';
        $sql .=               ' inner join mst_facility_relations as MstFacilityRelation on MstFacilityRelation.mst_facility_id = ' . $fid . ' and MstFacilityRelation.partner_facility_id = MstPartnerFacility.id ';
        $sql .=           ' where ' ;
        $sql .=               $where ;
        $sql .=               ' and MstFacilityItem.is_deleted = false ' ;
        $sql .=               ' and MstTransactionConfig.is_deleted = false ' ;
        $sql .=           ' order by ' ;
        $sql .=               ' MstFacility.id asc, MstFacilityItem.id asc, MstTransactionConfig.start_date asc' ;
        return $this->MstFacilityItem->query($sql);
    }
    /**
     * result
     *
     * 仕入情報更新（新規登録・更新）
     */
    function result() {
        //トランザクションの開始
        $this->MstTransactionConfig->begin();

        //包装単位プルダウン用
        $this->set('UnitName_List',$this->setItemUnits());
        //仕入先プルダウン用
        $this->set('Suppliers_List',
                   $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                          array(Configure::read('FacilityType.supplier')),
                                          true,
                                          array('id' , 'facility_name')
                                          )
                   );

        $now = date('Y/m/d H:i:s');
        $save_date = array();
        $ids = array();

        //チェック & 適用終了日の調整
        foreach ($this->request->data['MstTransactionConfig']['id'] as $line => $id){
            $a[$line] =  $this->request->data['MstTransactionConfig']['partner_facility_id'][$line] ."_". $this->request->data['MstTransactionConfig']['mst_item_unit_id'][$line] ."_". $this->request->data['MstTransactionConfig']['start_date'][$line];

            if($id){
                //更新のIDを取得
                $ids[] = $id;
            }
        }

        if($ids){
            //行ロック
            $this->MstTransactionConfig->query(' select * from mst_transaction_configs as a where a.id in (' . join(',',$ids) . ') for update ');
        }
        asort($a);

        $pre_v = null;
        $pre_k = null;

        foreach($a as $k => $v ){
            if(!is_null($pre_v)){
                if($pre_v === $v){
                    //同じ包装単位で同じ開始日なのでエラーにする
                    $this->Session->setFlash('適用開始日・包装単位・仕入先が同一の仕入設定があります。', 'growl', array('type'=>'error') );
                    //POSTデータを表示用に整形する
                    foreach($this->request->data['MstTransactionConfig']['id'] as $line => $id ){
                        $result[$line]['MstTransactionConfig']['id']                   = $this->request->data['MstTransactionConfig']['id'][$line];
                        $result[$line]['MstTransactionConfig']['start_date']           = $this->request->data['MstTransactionConfig']['start_date'][$line];
                        $result[$line]['MstTransactionConfig']['end_date']             = $this->request->data['MstTransactionConfig']['end_date'][$line];
                        $result[$line]['MstTransactionConfig']['mst_item_unit_id']     = $this->request->data['MstTransactionConfig']['mst_item_unit_id'][$line];
                        $result[$line]['MstTransactionConfig']['partner_facility_id']  = $this->request->data['MstTransactionConfig']['partner_facility_id'][$line];
                        $result[$line]['MstTransactionConfig']['partner_facility_id2'] = $this->request->data['MstTransactionConfig']['partner_facility_id2'][$line];
                        $result[$line]['MstTransactionConfig']['is_deleted']           = (isset($this->request->data['MstTransactionConfig']['is_deleted'][$line])?false:true);
                        $result[$line]['MstTransactionConfig']['status']               = $this->request->data['MstTransactionConfig']['status'][$line];
                        $result[$line]['MstTransactionConfig']['transaction_price']    = $this->request->data['MstTransactionConfig']['transaction_price'][$line];
                        $result[$line]['MstTransactionConfig']['ms_transaction_price'] = $this->request->data['MstTransactionConfig']['ms_transaction_price'][$line];
                    }

                    //売上設定の取得
                    $SalesConfig = $this->getSalesConfig($this->request->data['MstFacilityItem']['id']);

                    $this->set('SalesConfig',$SalesConfig);
                    $this->set('TransactionConfig',$result);

                    $this->render('mod');
                    return false;
                }
                list($per_facility_id , $per_item_unit_id , $pre_start_date) =explode('_' , $pre_v);
                list($facility_id , $item_unit_id , $start_date) = explode('_' , $v);
                if($per_item_unit_id == $item_unit_id
                   && $per_facility_id == $facility_id ){
                    //同じ包装単位・仕入先で開始日が違うものがあったら終了日を調整する
                    $this->request->data['MstTransactionConfig']['end_date'][$pre_k] = $start_date;
                }
            }

            $pre_v = $v;
            $pre_k = $k;
        }


        //データの詰め込み
        foreach($this->request->data['MstTransactionConfig']['id'] as $line => $id ){
            $data = array();
            if($id){
                //更新
                $data['MstTransactionConfig']['id']      = $id;
            }else{
                $data['MstTransactionConfig']['creater'] = $this->Session->read('Auth.MstUser.id');
                $data['MstTransactionConfig']['created'] = $now;
            }
            $data['MstTransactionConfig']['mst_item_unit_id']         = $this->request->data['MstTransactionConfig']['mst_item_unit_id'][$line];
            $data['MstTransactionConfig']['start_date']               = $this->request->data['MstTransactionConfig']['start_date'][$line];
            $data['MstTransactionConfig']['end_date']                 = $this->request->data['MstTransactionConfig']['end_date'][$line];
            $data['MstTransactionConfig']['mst_facility_relation_id'] = $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $this->request->data['MstTransactionConfig']['partner_facility_id'][$line]);
            $data['MstTransactionConfig']['mst_facility_item_id']     = $this->request->data['MstFacilityItem']['id'];
            $data['MstTransactionConfig']['mst_facility_id']          = $this->Session->read('Auth.facility_id_selected');
            $data['MstTransactionConfig']['partner_facility_id']      = $this->request->data['MstTransactionConfig']['partner_facility_id'][$line];
//            $data['MstTransactionConfig']['partner_facility_id2']     = $this->request->data['MstTransactionConfig']['partner_facility_id2'][$line];
            $data['MstTransactionConfig']['transaction_price']        = $this->request->data['MstTransactionConfig']['transaction_price'][$line];
            if($this->Session->read('Auth.Config.MSCorporate') == '1'){
                $data['MstTransactionConfig']['ms_transaction_price']     = $this->request->data['MstTransactionConfig']['ms_transaction_price'][$line];
            } else {
                $data['MstTransactionConfig']['ms_transaction_price']     = $this->request->data['MstTransactionConfig']['transaction_price'][$line];
            }
            $data['MstTransactionConfig']['is_deleted']               = (isset($this->request->data['MstTransactionConfig']['is_deleted'][$line])?false:true);
            $data['MstTransactionConfig']['modifier']                 = $this->Session->read('Auth.MstUser.id');
            $data['MstTransactionConfig']['modified']                 = $now;
            $save_data[] = $data;
        }
        
        if (!$this->MstTransactionConfig->saveAll($save_data , array('validates' => true,'atomic' => false))) {
            //更新失敗のためロールバック
            $this->MstTransactionConfig->rollback();
            //エラーメッセージ
            $this->Session->setFlash('登録が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('mod');
        }


        if(Configure::read('SalesCreate.flag') == 1){
            if(!$this->createSalesConfig($save_data)){
                $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                $this->MstTransactionConfig->rollback();//ロールバック
                $this->redirect('transaction_configs_list');
            }
        }
        /*
        //代表仕入
        $data = array();
        if(!empty($this->request->data['MstTransactionMain']['id'])){
            //更新
            $data['MstTransactionMain']['id']      = $this->request->data['MstTransactionMain']['id'];
        }else{
            $data['MstTransactionMain']['creater'] = $this->Session->read('Auth.MstUser.id');
            $data['MstTransactionMain']['created'] = $now;
        }
        $data['MstTransactionMain']['mst_item_unit_id']         = $this->request->data['MstTransactionMain']['mst_item_unit_id'];
        $data['MstTransactionMain']['mst_facility_item_id']     = $this->request->data['MstFacilityItem']['id'];
        $data['MstTransactionMain']['mst_facility_id']          = $this->request->data['MstTransactionMain']['mst_facility_id'];
        $data['MstTransactionMain']['modifier']                 = $this->Session->read('Auth.MstUser.id');
        $data['MstTransactionMain']['modified']                 = $now;
        
        $this->MstTransactionMain->create();
        // SQL実行
        if (!$this->MstTransactionMain->save($data)) {
            $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
            $this->MstTransactionConfig->rollback();//ロールバック
            $this->redirect('transaction_configs_list');
        }
        $this->set('main_result' , $data);
        */
        //コミット
        $this->MstTransactionConfig->commit();
        //結果画面表示用データ
        $this->set('result' , $save_data);
    }

    //編集画面用包装単位プルダウン作成用データ
    function setItemUnits () {
        $sql = ' select ';
        $sql.= '      a.id                   as "MstItemUnit__id"';
        $sql.= '    , (  ';
        $sql.= '      case  ';
        $sql.= '        when a.per_unit = 1  ';
        $sql.= '        then b.unit_name  ';
        $sql.= "        else b.unit_name || '(' || a.per_unit || c.unit_name || ')'  ";
        $sql.= '        end ';
        $sql.= '    )                        as "MstItemUnit__unit_name"  ';
        $sql.= '  from ';
        $sql.= '    mst_item_units as a  ';
        $sql.= '    left join mst_unit_names as b  ';
        $sql.= '      on b.id = a.mst_unit_name_id  ';
        $sql.= '    left join mst_unit_names as c  ';
        $sql.= '      on c.id = a.per_unit_name_id ';
        $sql.= '      where ';
        $sql.= '      a.mst_facility_item_id = ' . $this->request->data['MstFacilityItem']['id'];
        $sql.= ' order by a.per_unit ';

        $list = Set::Combine(
             $this->MstItemUnit->query($sql),
            "{n}.MstItemUnit.id",
            "{n}.MstItemUnit.unit_name"
            );
        return $list;
    }

    /**
     * 仕入設定印刷
     * @param
     * @todo
     */
    function print_config($print_data,$search_params=""){

        $columns = array("センター名",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "仕入先",
                         "包装単位",
                         "適用開始日",
                         "仕入課税区分",
                         "仕入単価",
                         "代表印",
                         "検索条件",
                         "印刷年月日"
                         );
        $data = array();
        $today = date("Y/m/d");//印刷年月日
        $tax_type = Configure::read('PrintFacilityItems.taxes');
        $output = array();

        //検索条件生成
        if($search_params != ""){
            //施設コードから仕入先施設名を取得
            $supplier_name = $this->MstFacility->find('first',array('fields'=>'facility_formal_name','conditions'=>array('MstFacility.facility_code'=>$search_params['MstFacilityItem']['supplierCode'] , 'MstFacility.facility_type'=> array(Configure::read('FacilityType.supplier'))),'recursive'  => -1));
            $Search_condition = "商品ID = ".$search_params['MstFacilityItem']['search_internal_code']
              ."・製品番号 = ".$search_params['MstFacilityItem']['search_item_code']
              ."・商品名 = ".$search_params['MstFacilityItem']['search_item_name']
              ."・販売元 = ".$search_params['MstFacilityItem']['search_dealer_name']
              ."・規格 = ".str_replace(" ","",$search_params['MstFacilityItem']['search_standard'])
              ."・JANコード = ".$search_params['MstFacilityItem']['search_jan_code']
              ."・仕入先 = ".$search_params['MstFacilityItem']['supplierName'];
        } else {
            $Search_condition = "商品ID = "
              ."・製品番号 = "
              ."・商品名 = "
              ."・販売元 = "
              ."・規格 = "
              ."・JANコード = "
              ."・仕入先 = ";
        }


        foreach($print_data as $data){
            $taxType = "";
            if(isset($data['MstFacilityItem']['tax_type']) && $data['MstFacilityItem']['tax_type'] != null){
                $taxType = $tax_type[$data['MstFacilityItem']['tax_type']];
            }
            $output[] = array( $data['MstFacility']['facility_formal_name']         //センター名
                              ,$data['MstFacilityItem']['item_name']                //商品名
                              ,$data['MstFacilityItem']['standard']                 //規格
                              ,$data['MstFacilityItem']['item_code']                //製品番号
                              ,$data['MstDealer']['dealer_name']                    //販売元
                              ,$data['MstFacilityItem']['internal_code']            //商品ID
                              ,$data['MstPartnerFacility']['facility_formal_name']  //仕入先
                              ,$data['MstFacilityItem']['unit_name']                //包装単位
                              ,$data['MstTransactionConfig']['start_date']          //適用開始日
                              ,$taxType                                             //仕入課税区分
                              ,$data['MstTransactionConfig']['transaction_price']   //仕入単価
                              ,''                                                   //代表印
                              ,$today);                                             //印刷年月日
        }

        $layout_name = Configure::read('layoutnameModels');
        $layout_file = "xml_output";
        $fix_value = array('タイトル'=>'仕入設定印刷','検索条件'=>$Search_condition);
        $this->xml_output($output,join("\t",$columns),$layout_name['20'],$fix_value,$layout_file);
    }

    //
    //検索
    //
    private function getItems($where){
        $limit = $this->_getLimitCount();

        $sql  = ' select ';
        $sql .= '       a.id                          as "MstFacilityItem__id" ';
        $sql .= '     , a.item_code                   as "MstFacilityItem__item_code" ';
        $sql .= '     , a.internal_code               as "MstFacilityItem__internal_code" ';
        $sql .= '     , a.item_name                   as "MstFacilityItem__item_name" ';
        $sql .= '     , a.standard                    as "MstFacilityItem__standard" ';
        $sql .= '     , b.dealer_name                 as "MstFacilityItem__dealer_name" ';
        $sql .= '     , d.facility_name               as "MstFacilityItem__facility_name"  ';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_dealers as b  ';
        $sql .= '       on a.mst_dealer_id = b.id  ';
        $sql .= '     left join mst_transaction_mains as c  ';
        $sql .= '       on c.mst_facility_item_id = a.id  ';
        $sql .= '     left join mst_facilities as d  ';
        $sql .= '       on c.mst_facility_id = d.id  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.item_type = ' . Configure::read('Items.item_types.normalitem');
        $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= $where;
        $sql .= '   group by ';
        $sql .= '     a.id ';
        $sql .= '     , a.item_code ';
        $sql .= '     , a.internal_code ';
        $sql .= '     , a.item_name ';
        $sql .= '     , a.standard ';
        $sql .= '     , a.mst_facility_id ';
        $sql .= '     , a.tax_type ';
        $sql .= '     , b.dealer_name ';
        $sql .= '     , d.facility_name  ';
        $sql .= '   order by ';
        $sql .= '     a.internal_code asc  ';
        
        $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));
        $sql .= " limit {$limit}";
        
        return $this->MstFacilityItem->query($sql);
    }

    /*
     * 施設採用品IDから仕入設定を取得する
     */
    private function getTransactionConfig($id){
        $sql  = ' select ';
        $sql .= '       a.id                                  as "MstTransactionConfig__id" ';
        $sql .= '     , a.mst_item_unit_id                    as "MstTransactionConfig__mst_item_unit_id" ';
        $sql .= '     , to_char(a.start_date, \'YYYY/mm/dd\') as "MstTransactionConfig__start_date" ';
        $sql .= '     , to_char(a.end_date, \'YYYY/mm/dd\')   as "MstTransactionConfig__end_date" ';
        $sql .= '     , a.partner_facility_id                 as "MstTransactionConfig__partner_facility_id" ';
        $sql .= '     , a.partner_facility_id2                as "MstTransactionConfig__partner_facility_id2" ';
        $sql .= '     , a.transaction_price                   as "MstTransactionConfig__transaction_price" ';
        $sql .= '     , a.ms_transaction_price                as "MstTransactionConfig__ms_transaction_price" ';
        $sql .= '     , a.is_deleted                          as "MstTransactionConfig__is_deleted" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.end_date > now()  ';
        $sql .= '         then true  ';
        $sql .= '         else false  ';
        $sql .= '         end ';
        $sql .= '     )                                       as "MstTransactionConfig__status"  ';
        $sql .= '   from ';
        $sql .= '     mst_transaction_configs as a  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_item_id = ' . $id;

        return $this->MstTransactionConfig->query($sql);
    }

    /*
     * 施設採用品IDから売上設定を取得する
     */
    private function getSalesConfig($id){
        $sql  = ' select ';
        $sql .= '       e.facility_name                       as "MstSalesConfig__facility_name" ';
        $sql .= '     , to_char(a.start_date, \'YYYY/mm/dd\') as "MstSalesConfig__start_date" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                       as "MstSalesConfig__unit_name" ';
        $sql .= '     , a.sales_price                         as "MstSalesConfig__sales_price"  ';
        $sql .= '   from ';
        $sql .= '     mst_sales_configs as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on a.mst_item_unit_id = b.id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on b.mst_unit_name_id = c.id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on b.per_unit_name_id = d.id  ';
        $sql .= '     left join mst_facilities as e  ';
        $sql .= '       on e.id = a.partner_facility_id  ';
        $sql .= '     inner join mst_user_belongings as f ';
        $sql .= '       on f.mst_user_id = ' . $this->Session->read('Auth.MstUser.id') ;
        $sql .= '      and f.mst_facility_id = e.id ' ;
        $sql .= '      and f.is_deleted = false' ;
        $sql .= '   where ';
        $sql .= '     a.mst_facility_item_id = ' . $id ;
        $sql .= '     and a.is_deleted = false ';
        $sql .= '     and a.start_date < now()  ';
        $sql .= '     and a.end_date > now() ';

        return $this->MstSalesConfig->query($sql);
    }


    // 施設関連IDを取得する
    private function _getRelationID( $c_id , $h_id ){
        $sql  = ' select ';
        $sql .= '       id  ';
        $sql .= '   from ';
        $sql .= '     mst_facility_relations as a  ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_id = ' . $c_id;
        $sql .= '     and a.partner_facility_id = ' . $h_id;

        $ret = $this->MstFacilityRelation->query($sql);

        return $ret[0][0]['id'];
    }

    private function createSalesConfig($data){
        //裏で売上設定を作る。
        foreach($data as $item){
            $sql  = ' select ';
            $sql .= '       c.id                           as mst_item_unit_id ';
            $sql .= '     , a.per_unit                     as tran_pre_unit ';
            $sql .= '     , c.per_unit                     as sale_per_unit ';
            $sql .= '     , a.mst_facility_item_id ';
            $sql .= '     , x.partner_facility_id  ';
            $sql .= '   from ';
            $sql .= '     mst_item_units as a  ';
            $sql .= '     left join mst_facility_items as b  ';
            $sql .= '       on b.id = a.mst_facility_item_id  ';
            $sql .= '       and b.item_type = ' . Configure::read('Items.item_types.normalitem');
            $sql .= '     left join mst_facility_relations as x  ';
            $sql .= '       on x.mst_facility_id = b.mst_facility_id  ';
            $sql .= '     inner join mst_facilities as y  ';
            $sql .= '       on y.id = x.partner_facility_id  ';
            $sql .= '       and y.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .= '     left join mst_item_units as c  ';
            $sql .= '       on c.mst_facility_item_id = b.id  ';
            $sql .= '     left join mst_sales_configs as d  ';
            $sql .= '       on d.mst_item_unit_id = c.id  ';
            $sql .= '      and d.partner_facility_id = y.id';
            $sql .= '   where ';
            $sql .= '     a.id = ' . $item['MstTransactionConfig']['mst_item_unit_id'];
            $sql .= '     and d.id is null ';

            $ret = $this->MstSalesConfig->query($sql);
            //売上げ設定がない場合作る
            foreach($ret as $r ){
                $sales = array(
                    'MstSalesConfig' => array(
                        'mst_item_unit_id'         => $r[0]['mst_item_unit_id'],
                        'start_date'               => '1900/01/01',
                        'end_date'                 => '3000/01/01',
                        'mst_facility_relation_id' => $this->_getRelationID($this->Session->read('Auth.facility_id_selected') , $r[0]['partner_facility_id']),
                        'mst_facility_item_id'     => $r[0]['mst_facility_item_id'],
                        'mst_facility_id'          => $this->Session->read('Auth.facility_id_selected'),
                        'partner_facility_id'      => $r[0]['partner_facility_id'],
                        'sales_price'              => round((( $item['MstTransactionConfig']['transaction_price'] / $r[0]['tran_pre_unit'] ) * $r[0]['sale_per_unit']) , 2),
                        'is_deleted'               => false,
                        'creater'                  => $this->Session->read('Auth.MstUser.id'),
                        'created'                  => 'now()',
                        'modifier'                 => $this->Session->read('Auth.MstUser.id'),
                        'modified'                 => 'now()',
                        )
                    );

                $this->MstSalesConfig->create();
                if (!$this->MstSalesConfig->save($sales)) {
                    return false;
                }
            }
        }
        return true;
    }
}
