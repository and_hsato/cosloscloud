<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('Sanitize', 'Utility');
class AppController extends Controller {

    /**
     * Post Model
     *
     * @var Post
     */
    var $Post;

    /**
     * User Model
     *
     * @var User
     */
    var $User;

    /**
     * Group Model
     *
     * @var Group
     */
    var $Group;

    /**
     * AuthComponent
     *
     * @var AuthComponent
     */
    var $Auth;

    /**
     * SessionComponent
     *
     * @var SessionComponent
     */
    var $Session;

    /**
     * RequestHandlerComponent
     *
     * @var RequestHandlerComponent
     */
    var $RequestHandler;

    //  var $uses = array('User',);

    /**
     *
     * @var array $helpers
     */
    var $helpers = array('Form',
                         'Html',
                         //'Ajax',
                         //'Javascript',
                         'Time',
                         'DatePicker',
                         'Js' => array('Jquery'),
                         'Session',
                         'Common');

    /**
     *
     * @var array $components
     */
    var $components = array(
        'RequestHandler',
        'Auth' => array(
            'loginAction' => array(
                'controller' => 'login',
                'action'     => 'login'
             ),
            'userModel'     => 'MstUser',
            'authorize'     => 'controller',
            'authError'     => 'ログインしてください。',
            'loginRedirect' => array('controller' => 'login', 'action' => 'home'),
            'logoutRedirect' => array('controller' => 'login', 'action' => 'login'),
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'login_id' , 'password'=>'password')
                    )
                )
            ),
        'Session',
        'Common',
        'AuthSession',
        );
    
    const DEFAULT_LIMIT_COUNT = 100;

    /**
     * Experimental Code.
     * HACK:　Comment out here at release time!
     * Caching for model objects.
     * @var bool $persistModel
     */
    //var $persistModel = true;

    /**
     * isAuthorized
     *
     * AuthComponentで認証が成功した後に実行される処理
     * TODO 追加の認証処理が必要になった場合実装する。（ブラウザのチェック等）
     *
     * @return boolean
     */
    function isAuthorized() {
        return true;
    }

    /**
     * 権限のための機能のセット
     * @param integer $id
     */
    protected function setRoleFunction($id){
        $this->Session->write('Role.function_id', $id);
    }
    
    /**
     *
     */
    /*
    public function  beforeFilter() {
        $this->response->disableCache();
    }
    */


    /**
     * jQuery用：施設区分を指定して施設を取得する
     * @access public
     * @param string $_field
     * @param string $_value
     * @return void|string json data.
     */
    public function get_facilities ($_field = null, $_value = null) {
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($_field) && isset($_value)) {
            $_value = (isset($_POST['value']))? $_POST['value']:$_value;
            $_field = (isset($_POST['field']))? $_POST['field']:$_field;

            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;

                $_params = array();
                if($_value==1){
                    $_params['fields'] = array('MstFacility.id',
                                               'MstFacility.facility_code',
                                               'MstFacility.facility_name');
                    
                    $_params['conditions'] =  array('MstFacility.is_deleted' => false,
                                                    'MstFacility.id'         =>$this->Session->read('Auth.facility_id_selected')
                                                );
                    $_params['recursive'] = -1;
                    $_params['order'] = array('MstFacility.facility_code');
                    $_facility = $this->MstFacility->find('all', $_params);
                }else{
                    
                    $sql  = ' select ';
                    $sql .= '     a.id            as "MstFacility__id"';
                    $sql .= '   , a.facility_code as "MstFacility__facility_code" ';
                    $sql .= '   , a.facility_name as "MstFacility__facility_name"';
                    $sql .= ' from ';
                    $sql .= '   mst_facilities as a  ';
                    $sql .= '   left join mst_facility_relations as b  ';
                    $sql .= '     on b.partner_facility_id = a.id  ';
                    $sql .= ' where ';
                    $sql .= '   b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
                    $sql .= '   and a.is_deleted = false  ';
                    $sql .= '   and b.is_deleted = false  ';
                    $sql .= '   and a.facility_type =' . $_value;

                    $_facility = $this->MstFacility->query($sql);
                }
                $_fas = array();

                for ($_i = 0;$_i < count($_facility);$_i++) {
                    $_fas[$_i]  = $_facility[$_i]['MstFacility'];
                }

                return (json_encode($_fas));
            } else {
                $this->cakeError('error404');
            }
        } else {
            $this->cakeError('error404');
        }
    }
    
    /**
     * jQuery用：施設を指定して部署を取得する
     * @access public
     * @param string $_field
     * @param string $_value
     * @return void|string json data.
     */
    public function get_departments ($_field = null, $_value = null , $_type = array(1,2)) {
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($_field) && isset($_value)) {
            $_value = (isset($_POST['value']))? $_POST['value']:$_value;
            $_field = (isset($_POST['field']))? $_POST['field']:$_field;
            $_type = (isset($_POST['type']))? $_POST['type']:$_type;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                if($_field !== 'id'){
                    $_field = 'facility_code';
                }

                $_params = array();
                $_params['fields'] = array('MstDepartment.id',
                                           'MstDepartment.department_code',
                                           'MstDepartment.department_name');

                $_params['conditions'] =  array('MstDepartment.is_deleted' => false,
                                                "MstFacility.{$_field}" => $_value ,
                                                'MstFacility.facility_type'=>$_type
                                                );
                $_params['joins'][] = array('type'       => 'inner' ,
                                            'table'      => 'mst_facilities',
                                            'alias'      => 'MstFacility',
                                            'conditions' => array('MstFacility.id=MstDepartment.mst_facility_id')
                                            );
                $_params['recursive'] = -1;
                $_params['order'] = array('MstDepartment.department_code');

                $_departments = $this->MstDepartment->find('all', $_params);
                $_depts = array();

                for ($_i = 0;$_i < count($_departments);$_i++) {
                    $_depts[$_i]  = $_departments[$_i]['MstDepartment'];
                }

                return (json_encode($_depts));
            } else {
                $this->cakeError('error404');
            }
        } else {
            $this->cakeError('error404');
        }
    }

    /**
     * jQuery用：カテゴリを取得する
     * @access public
     * @param string $_rank
     * @param string $_id
     * @return void|string json data.
     */
    public function get_category ($_rank = null, $_id = null ) {
        Configure::write('debug', 0);
        if (isset($_POST['rank']) && isset($_POST['id']) || isset($_rank) && isset($_id)) {
            $_rank = (isset($_POST['rank']))? $_POST['rank']:$_rank;
            $_id = (isset($_POST['id']))? $_POST['id']:$_id;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $_params = array();
                $_params['fields'] = array('MstItemCategory.id',
                                           'MstItemCategory.category_name');

                $_params['conditions'] =  array('MstItemCategory.is_deleted'    => false,
                                                'MstItemCategory.category_rank' => $_rank,
                                                'MstItemCategory.parent_id'     => $_id
                                                );
                $_params['recursive'] = -1;
                $_params['order'] = array('MstItemCategory.category_code');
                $_category = $this->MstItemCategory->find('all', $_params);
                $_cat = array();

                for ($_i = 0;$_i < count($_category);$_i++) {
                    $_cat[$_i]  = $_category[$_i]['MstItemCategory'];
                }

                return (json_encode($_cat));
            } else {
                $this->cakeError('error404');
            }
        } else {
            $this->cakeError('error404');
        }
    }

    
    /**
     * 仮締め後更新できるかどうか
     * @param string $targetDate yyyy/mm/dd
     * @param string $facility 施設id
     * @return boolean
     */
    protected function canUpdateAfterClose($targetDate, $facility){
        $mp = ClassRegistry::init('MstPrivilege');
        $privilege = $mp->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'mst_role_id' => $this->Session->read('Auth.MstUser.mst_role_id'),
                'view_no' => $this->Session->read('Role.function_id'),
                ),
            ));

        $tc = ClassRegistry::init('TrnCloseHeader');
        $res = $tc->find('all', array(
            'recursive' => -1,
            'conditions' => array(
                'start_date <=' => $targetDate,
                'end_date >=' => $targetDate,
                'mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
                'is_deleted' => false,
                'partner_facility_id' => $facility
                ),
            ));
        switch(count($res)){
          case 0: //締めデータ無し
            return true;
            break;
          case 1: //仮締め
            if($privilege['MstPrivilege']['updatable'] == true){
                return true;
            }else{
                return false;
            }
            break;
          case 2: //本締め
            return false;
            break;
        }
    }

    /**
     * 仮締め後更新できる明細が含まれるかどうか
     * @param string   $mode     DB更新モード(1:作成・2:更新[削除])
     * @param string[] $targetDate yyyy/mm/dd #配列
     * @param string   $facility 施設id
     * @return boolean
     */
    protected function canUpdateAfterCloseByDateAry($mode, $targetDateAry, $facility){
        $mp = ClassRegistry::init('MstPrivilege');
        $privilege = $mp->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'mst_role_id' => $this->Session->read('Auth.MstRole.id'),
                'view_no' => $this->Session->read('Role.function_id'),
                ),
            ));
        $canDo = false;
        switch($mode){
          case 1: //作成権限
            $canDo = $privilege['MstPrivilege']['creatable'];
            break;
          case 2: //更新権限(論理削除もここ）
            $canDo = $privilege['MstPrivilege']['updatable'];
            break;
        }
        $tc = ClassRegistry::init('TrnCloseHeader');
        $mst_facility_id = $this->Session->read('Auth.MstFacility.id');
        //日付数分ループ
        for ($i = 0;$i < count($targetDateAry);$i++) {
            $res = $tc->find('all', array(
                'recursive' => -1,
                'conditions' => array(
                    'start_date <=' =>  $targetDateAry[$i],
                    'end_date >=' =>  $targetDateAry[$i],
                    'mst_facility_id' => $mst_facility_id,
                    'is_deleted' => false,
                    'partner_facility_id' => $facility
                    ),
                ));

            switch(count($res)){
              case 0:
                break;
              case 1:
                if($canDo == false){
                    $this->Session->setFlash('ユーザに仮締データに対する操作権限がありません。','growl',array('type'=>'error'));
                    return false;
                    break;
                }
                break;
              case 2:
                $this->Session->setFlash('明細に締実施データが含まれています。','growl',array('type'=>'error'));
                return false;
                break;
            }
        }
        return true;
    }

    /**
     * 仮締め後更新できるかどうか(期間指定)
     * @param array $targetDates start_date=>yyyy/mm/dd, end_date=>yyyy/mm/dd
     * @param string $facility 施設id
     * @return boolean
     */
    protected function canUpdateAfterCloseBySpan($targetDates, $facility){
        if(!is_array($targetDates) || count($targetDates) !== 2){
            return false;
        }

        if(isset($targetDates['start_date']) && isset($targetDates['end_date'])){
            $start_date = $targetDates['start_date'];
            $end_date = $targetDates['end_date'];
        } else {
            $start_date = $targetDates[0];
            $end_date = $targetDates[1];
        }
        $mp = ClassRegistry::init('MstPrivilege');
        $privilege = $mp->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'mst_role_id' => $this->Session->read('Auth.MstRole.id'),
                'view_no' => $this->Session->read('Role.function_id'),
                ),
            ));

        $tc = ClassRegistry::init('TrnCloseHeader');
        $res = $tc->find('all', array(
            'recursive' => -1,
            'conditions' => array(
                'start_date <=' => $end_date,
                'end_date >=' => $start_date,
                'mst_facility_id' => $this->Session->read('Auth.MstFacility.id'),
                'is_deleted' => false,
                'partner_facility_id' => $facility
                ),
            ));

        switch(count($res)){
          case 0:
            return true;
            break;
          case 1:
            if($privilege['MstPrivilege']['creatable'] == true){
                return true;
            }else{
                return false;
            }
            break;
          case 2:
            return false;
            break;
        }
    }

    /**
     * Generating work number.
     * 各機能毎のWORKNOを生成します。Header情報のworknoは、それに紐づく明細データに引き継がれます。
     * Usage: controllerにて
     *        $this->setWorkNo4Header(<入力された日付YYYYMMDD>, <機能のコード番号2ケタ数値>);
     *        ※入力された日付は「/」がついている場合でも「/」を取り除きます。
     * @param string $date
     * @param string $category_code
     * @return string
     */
    function setWorkNo4Header ($date = null, $numberType = null , $facility_id = null) {
        /**************************************************************************************
         * 採番重複対策  ここから
         **************************************************************************************/

        //採番テーブルインポート
        App::import('Model','MstNumber');
        //モデルのオブジェクト生成
        $MstNumber = new MstNumber;
        if(is_null($facility_id)){
            $facility_id = $this->Session->read('Auth.facility_id_selected');
        }
        //レコードロック取得
        $sql = "";
        $sql .= "SELECT * ";
        $sql .= "  FROM mst_numbers";
        $sql .= " WHERE number_type = ".$numberType;
        $sql .= "   AND mst_facility_id = "  . $facility_id;
        $sql .= " FOR UPDATE";
        //行ロック用SQL実行
        $res = $MstNumber->query($sql);
        //対象行がない場合、行を追加した上で、再度ロック処理を行う
        if($res == array()){
            $now = date('Y/m/d H:i:s');
            $data = array(
                'MstNumber'=> array(
                    'number_type'     => $numberType ,
                    'mst_facility_id' => $facility_id,
                    'last_date'       => date('Y/m/d'),
                    'counter'         => 0,
                    'is_deleted'      => false,
                    'creater'         => $this->Session->read('Auth.MstUser.id'),
                    'created'         => $now,
                    'modifier'        => $this->Session->read('Auth.MstUser.id'),
                    'modified'        => $now
                    )
                );
            
            $MstNumber->save($data ,  false);
            $res = $MstNumber->query($sql);
        }
        /**************************************************************************************
         * 採番重複対策  ここまで
         **************************************************************************************/
        $no = $res[0][0]['counter'] ;
        $work_date = date('Ymd' ,strtotime($res[0][0]['last_date']));
        if($work_date != date('Ymd')){
            $no = 0;
            $work_date = date('Ymd');
        }
        $sql = ' update mst_numbers set counter =  ' . ($no + 1) . " , last_date = '" .$work_date. "' where number_type = ".$numberType . ' AND mst_facility_id = '  . $facility_id ;
        //更新
        $res = $MstNumber->query($sql);

        return $work_date.$numberType.sprintf('%06d', (string)($no + 1));
    }

    /**
     * センターシール番号を発番
     * @param string $date 日付文字列
     * @param string $facility_code センターの施設コード
     * @return string
     */
    public function getCenterStickerNo($date, $facility_code, $center_id=null){
        /**************************************************************************************
         * 採番重複対策  ここから
         **************************************************************************************/
        //採番テーブルインポート
        App::import('Model','MstNumber');

        //施設ID確認 空の場合はセッションから取得
        if($this->isNNs($center_id)){
            $center_id = $this->Session->read('Auth.facility_id_selected');
        }

        //モデルのオブジェクト生成
        $MstNumber = new MstNumber;
        //レコードロック取得
        $sql  = "SELECT * FROM mst_numbers WHERE number_type = ".Configure::read('NumberType.Numbering_center') . " AND mst_facility_id = " . $center_id . " FOR UPDATE";
        //行ロック用SQL実行
        $res = $MstNumber->query($sql);
        //対象行がない場合、行を追加した上で、再度ロック処理を行う
        if($res == array()){
            $now = date('Y/m/d H:i:s');
            $data = array(
                'MstNumber'=> array(
                    'number_type'     => Configure::read('NumberType.Numbering_center') ,
                    'mst_facility_id' => $center_id,
                    'last_date'       => date('Y/m/d'),
                    'counter'         => 0,
                    'is_deleted'      => false,
                    'creater'         => $this->Session->read('Auth.MstUser.id'),
                    'created'         => $now,
                    'modifier'        => $this->Session->read('Auth.MstUser.id'),
                    'modified'        => $now
                    )
                );
            
            $MstNumber->save($data ,  false);
            $res = $MstNumber->query($sql);
        }

        $no = $res[0][0]['counter'] ;
        $work_date = date('ymd' ,strtotime($res[0][0]['last_date']));
        if($work_date != date('ymd')){
            $no = 0;
            $work_date = date('ymd');
        }
        $sql = ' update mst_numbers set counter =  ' . ($no + 1) . " , last_date = '" .$work_date. "' where number_type = ".Configure::read('NumberType.Numbering_center') . ' AND mst_facility_id = ' . $center_id;
        //更新
        $res = $MstNumber->query($sql);

        return $work_date.$facility_code.sprintf('%06d', (string)($no + 1));
    }

    /**
     * 部署シール番号を発番(TrnStickerとTrnStickerIssuesの最新部署シール番号を比べ、ユニークな値を返す)
     * @param string $date 日付文字列
     * @param string $facility_code 病院の施設コード
     * @param string $center_id 病院の施設ID
     * @return string
     */
    protected function getHospitalStickerNo($date, $facility_code, $center_id=null){
        /**************************************************************************************
         * 採番重複対策  ここから
         **************************************************************************************/
        //採番テーブルインポート
        App::import('Model','MstNumber');

        //施設ID確認 空の場合はセッションから取得
        if($this->isNNs($center_id)){
            $center_id = $this->Session->read('Auth.facility_id_selected');
        }

        //モデルのオブジェクト生成
        $MstNumber = new MstNumber;
        //レコードロック取得
        $sql  = "SELECT * FROM mst_numbers WHERE number_type = ".Configure::read('NumberType.Numbering_hospital') . " AND mst_facility_id = " . $center_id . " FOR UPDATE";
        //行ロック用SQL実行
        $res = $MstNumber->query($sql);
        //対象行がない場合、行を追加した上で、再度ロック処理を行う
        if($res == array()){
            $now = date('Y/m/d H:i:s');
            $data = array(
                'MstNumber'=> array(
                    'number_type'     => Configure::read('NumberType.Numbering_hospital') ,
                    'mst_facility_id' => $center_id,
                    'last_date'       => date('Y/m/d'),
                    'counter'         => 0,
                    'is_deleted'      => false,
                    'creater'         => $this->Session->read('Auth.MstUser.id'),
                    'created'         => $now,
                    'modifier'        => $this->Session->read('Auth.MstUser.id'),
                    'modified'        => $now
                    )
                );
            
            $MstNumber->save($data ,  false);
            $res = $MstNumber->query($sql);
        }

        $no = $res[0][0]['counter'] ;
        $work_date = date('ymd' ,strtotime($res[0][0]['last_date']));
        if($work_date != date('ymd')){
            $no = 0;
            $work_date = date('ymd');
        }
        $sql = ' update mst_numbers set counter =  ' . ($no + 1) . " , last_date = '" .$work_date. "' where number_type = ".Configure::read('NumberType.Numbering_hospital') . ' AND mst_facility_id = ' . $center_id;
        //更新
        $res = $MstNumber->query($sql);

        return $work_date.$facility_code.sprintf('%06d', (string)($no + 1));

    }

    /**
     * 未定義、null、空の場合の判定
     * @param mixed $val
     * @return boolean
     * @access protected
     */
    public function isNNs($val){
        if((false === isset($val)) || (null === $val)){
            return true;
        }elseif(is_array($val) && empty($val)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * ソート順を取得
     * 未設定の場合はnullを返します
     * 設定済みの場合は、配列にして返します
     *
     * @access protected
     * @return mixed
     */
    protected function getSortOrder(){
        if(false === $this->Session->check($this->params['controller'].'.__sortName__')){
            return null;
        }else{
            return array(sprintf('%s %s', $this->Session->read($this->params['controller'].'.__sortName__'), $this->Session->read($this->params['controller'].'.__sortDirection__')));
        }
    }

    /**
     *  ソート情報の削除
     * @access protected
     * @return void
     */
    protected function deleteSortInfo(){
        $this->Session->delete($this->params['controller'].'.__sortName__');
        $this->Session->delete($this->params['controller'].'.__sortDirection__');
    }

    /**
     * LIMITを取得する
     */
    protected function _getLimitCount() {
        $_limit = min(array_keys(Configure::read('displaycounts_combobox')));

        if( isset($this->request->data['limit'])) {
            $_limit =  $this->request->data['limit'];
        }
        return $_limit;
    }

    protected function _deleteLimitCount() {
        $this->Session->delete($this->name . '__limit');
    }

    /**
     *　ソート情報の登録
     * @param string $column
     * @param string $direction
     * @access protected
     * @return void
     */
    protected function setSortInfo($column,$direction){
        $this->Session->write($this->params['controller'].'.__sortName__',$column);
        $this->Session->write($this->params['controller'].'.__sortDirection__',$direction);
    }

    protected function getSortInfo(){
        $_val = $this->getSortOrder();
        if($_val === null){
            return null;
        }
        $_order = explode(' ', $_val[0]);
        return array('column' => $_order[0], 'direction' => $_order[1]);
    }

    protected $isSortCall = false;
    public function sort(){
        $_methodName = substr($this->request->data['__action__'], 1);
        $_pos = strpos($this->request->data['__sortColumn__'], '_');
        $_columnName = sprintf('%s.%s', substr($this->request->data['__sortColumn__'],0,$_pos),substr($this->request->data['__sortColumn__'],$_pos+1));
        if(true === method_exists($this, $_methodName)){
            if(false === $this->Session->check($this->params['controller'].'.__sortName__')){
                $this->setSortInfo($_columnName,'ASC');
            }else{
                $_col = $this->Session->read($this->params['controller'].'.__sortName__');
                $_dir = $this->Session->read($this->params['controller'].'.__sortDirection__');

                if($_col == $_columnName){
                    $this->setSortInfo($_columnName, ($_dir === 'ASC' ? 'DESC' : 'ASC'));
                }else{
                    $this->setSortInfo($_columnName, 'ASC');
                }
            }

            $ref = new ReflectionMethod($this, $_methodName);
            $this->isSortCall = true;
            $ref->invoke($this);
        }else{
            throw new Exception('ソートを実行する関数名定義エラー');
        }
    }

    static public function outputFilter($params){
        if(is_array($params)){
            return array_map(array('AppController', 'outputFilter'), $params);
        }else{
            return htmlspecialchars($params, ENT_QUOTES);
        }
    }

    static public function sanitizeFilter($params){
        App::import('Sanitize');
        if(is_array($params)){
            return array_map(array('AppController', 'sanitizeFilter'), $params);
        }else{
            return Sanitize::escape($params);
        }
    }

    /**
     * 有効期限の書式をつけて返す
     * 00が末尾の場合は月末日指定
     * @param string $val yyMMdd形式の日付文字列
     * @return string
     */
    public function getFormatedValidateDate($val){
        if(strlen($val) === 0){
            return '';
        }
        if(strlen($val) === 10 && false !== strstr($val, '/')){
            return $val;
        }


        $isEnd = (substr($val,4) === '00');
        if(true === $isEnd){
            $d = sprintf('%s-%s-01', substr($val,0,2),substr($val,2,2));
        }else{
            $d = sprintf('%s-%s-%s', substr($val,0,2),substr($val,2,2), substr($val,4));
        }
        $t = date('Y/m/d', strtotime($d));
        return ($isEnd ? date('Y/m/d', strtotime('+1 month -1 day', strtotime($t))) : date('Y/m/d', strtotime($t)));
    }


    /**
     * 施設リスト取得
     */
    function getFacilityList($facility_id = null ,
                             $type = array() ,
                             $deleted = true ,
                             $field = array('facility_code' , 'facility_name') ,
                             $order = null
                             ){

        list($a , $b) = $field;
        $sql  = '';
        $sql .= 'select ';
        $sql .= "      b.{$a} as \"MstFacility__{$a}\" ";
        $sql .= "    , b.{$b} as \"MstFacility__{$b}\" ";
        $sql .= '  from';
        $sql .= '    mst_facility_relations as a ';
        $sql .= '    left join mst_facilities as b ';
        $sql .= '      on b.id = a.mst_facility_id ';
        $sql .= '      or b.id = a.partner_facility_id ';
        if($facility_id){
            if(in_array(Configure::read('FacilityType.hospital') , $type)){
                $sql .= '    inner join mst_user_belongings as c ';
            }else{
                $sql .= '    left join mst_user_belongings as c ';
            }
            $sql .= '      on c.mst_user_id = ' .  $this->Session->read('Auth.MstUser.id');
            $sql .= '      and c.mst_facility_id = b.id ';
            $sql .= '      and c.is_deleted = false ';
        }
        $sql .= '  where';
        $sql .= ' 1 = 1 ';
        if($facility_id){
            $sql .= '    and a.mst_facility_id = '.$facility_id;
        }

        if($deleted){
            $sql .= '    and a.is_deleted = false ';
            $sql .= '    and b.is_deleted = false ';
        }
        if(!empty($type)){
            $sql .= '    and b.facility_type in ('.implode("," , $type).') ';
        }
        $sql .= '  group by';
        $sql .= "    b.{$a}";
        $sql .= "    , b.{$b}";
        $sql .= "    , b.facility_code";
        $sql .= "    , b.facility_type";
        if(is_null($order)){
            $sql .= ' order by b.facility_type desc ,  b.facility_code ';
        }else{
            $sql .= ' order by ' . $order;
        }
        $list = Set::Combine(
            $this->MstFacility->query($sql),
            "{n}.MstFacility.{$a}",
            "{n}.MstFacility.{$b}"
            );
        return $list;
    }

    /**
     * 部署リスト取得
     */
    function getDepartmentsList( $select_facility = null ,
                                 $deleted = true  ,
                                 $params = 'facility_code' ,
                                 $field = array('department_code' , 'department_name') ,
                                 $type = array( 1 , 2 )
                                 ){
        
        list($a , $b) = $field;

        $sql  ='select ';
        $sql .="      b.{$a} as \"MstDepartment__{$a}\"";
        $sql .="    , b.{$b} as \"MstDepartment__{$b}\"";
        $sql .='  from ';
        $sql .='    mst_facilities as a  ';
        $sql .='    left join mst_departments as b  ';
        $sql .='      on a.id = b.mst_facility_id  ';
        $sql .='  where ';
        if(is_null($select_facility)){
            $sql .='    1 = 0';
        }else{
            $sql .="    a.{$params} = '" .$select_facility. "'";
        }
        if(!empty($type) || is_array($type)){
            $sql .= '   and a.facility_type in (' . join(',',$type). ')';
        }
        if($deleted){
            $sql .='    and b.is_deleted = false ';
        }

        $sql .= ' order by b.department_code';

        $list = Set::Combine(
            $this->MstDepartment->query($sql),
            "{n}.MstDepartment.{$a}",
            "{n}.MstDepartment.{$b}"
            );
        return $list;
    }

    /**
     * 作業区分リスト取得
     */
    function getClassesList($facility_id = null  , $menu_id = null){
        $list = Set::Combine(
            $this->MstClass->find('all',
                                  array('order' => 'MstClass.code',
                                        'conditions'=>array('MstClass.mst_menu_id'     => $menu_id,
                                                            'MstClass.mst_facility_id' => $facility_id)
                                        )),'{n}.MstClass.id','{n}.MstClass.name');
        return $list;
    }


    
    /**
     * 施設コードを取得
     * @param <type> $fid
     * @return <type>
     */
    public function getFacilityCode($fid){
        $res = $this->MstFacility->find('first', array(
            'conditions' => array(
                'id' => $fid,
                ),
            'recursive' => -1,
            ));
        return $res['MstFacility']['facility_code'];
    }

    /**
     * 施設IDを取得
     * @param <type> $fid
     * @return <type>
     */
    public function getFacilityId($fcode , $ftype , $center_id = null ){
        if(is_null($center_id)){
            $res = $this->MstFacility->find('first', array(
                'conditions' => array(
                    'facility_code' => $fcode,
                    'facility_type' => $ftype,
                    ),
                'recursive' => -1,
                ));
        }else{
            $res = $this->MstFacility->find('first', array(
                'conditions' => array(
                    'facility_code' => $fcode,
                    'facility_type' => $ftype,
                    ),
                'joins' => array(
                    array('type'=>'inner'
                          ,'table'=>'mst_facility_relations'
                          ,'alias'=>'MstFacilityRelation'
                          ,'conditions'=>array('MstFacilityRelation.partner_facility_id = MstFacility.id' ,
                                               'MstFacilityRelation.mst_facility_id' => $center_id,
                                               'MstFacilityRelation.is_deleted = false'
                                               )
                          )
                    ),
                'recursive' => -1,
                ));
        }
        return $res['MstFacility']['id'];
    }


    /**
     * 施設名を取得
     * @param <type> $fid
     * @return <type>
     */
    public function getFacilityName($fid){
        $res = $this->MstFacility->find('first', array(
            'conditions' => array(
                'id' => $fid,
                ),
            'recursive' => -1,
            ));
        return $res['MstFacility']['facility_name'];
    }

    /**
     * 部署IDを取得
     * @param string $facilityId
     * @param string $type
     * @return string
     */
    public function getDepartmentId($facilityId, $type , $code = null){
        if($code==null){
            $_res = $this->MstDepartment->find('first', array(
                'conditions' => array(
                    'mst_facility_id' => $facilityId,
                    'department_type' => $type,
                    ),
                'recursive' => -1,
                ));
        }else{
            $_res = $this->MstDepartment->find('first', array(
                'conditions' => array(
                    'mst_facility_id' => $facilityId,
                    'department_type' => $type,
                    'department_code' => $code,
                    ),
                'recursive' => -1,
                ));
        }
        if(empty($_res)){
            return false;
        }
        return $_res['MstDepartment']['id'];
    }

    /**
     * 部署名を取得
     * @param <type> $did
     * @return <type>
     */
    public function getDepartmentName($did){
        $res = $this->MstDepartment->find('first', array(
            'conditions' => array(
                'id' => $did,
                ),
            'recursive' => -1,
            ));
        return $res['MstDepartment']['department_name'];
    }

    /**
     * 作業区分名を取得
     * @param <type> $cid
     * @return <type>
     */
    public function getWorkTypeName($cid){
        if($cid==""){
            return "";
        }
        $res = $this->MstClass->find('first', array(
            'conditions' => array(
                'id' => $cid,
                ),
            'recursive' => -1,
            ));
        return $res['MstClass']['name'];
    }

    /*
     * sql_dump をコントローラから
     */
    function sql_dump()
      {
          if (!class_exists('ConnectionManager') || Configure::read('debug') < 2) {
              return false;
          }
          $noLogs = !isset($logs);
          if ($noLogs):
          $sources = ConnectionManager::sourceList();

          $logs = array();
          foreach ($sources as $source):
          $db =& ConnectionManager::getDataSource($source);
          if (!$db->isInterfaceSupported('getLog')):
          continue;
          endif;
          $logs[$source] = $db->getLog();
          endforeach;
          endif;

          $tmp = '';
          if ($noLogs || isset($_forced_from_dbo_)){
              foreach ($logs as $source => $logInfo){
                  $text = $logInfo['count'] > 1 ? 'queries' : 'query';
                  $tmp .= sprintf("cakeSqlLog_%s ",preg_replace('/[^A-Za-z0-9_]/', '_', uniqid(time(), true)));
                  $tmp .= sprintf("(%s) %s %s took %s ms\n", $source, $logInfo['count'], $text, $logInfo['time']);
                  $tmp .= "Nr\tQuery\tError\tAffected\tNum. rows\tTook (ms)\n";
                  foreach ($logInfo['log'] as $k => $i){
                      $tmp .= sprintf("%s\t%s\t%s\t%s\t%s\t%s\t\n",($k + 1), $i['query'],$i['error'],$i['affected'],$i['numRows'],$i['took']);
                  }
              }
          }else{
              $tmp .= 'Encountered unexpected $logs cannot generate SQL log';
          }
          return $tmp;
      }

    /**
     * Lot/有効期限切れチェック対象か判定
     */
    public function isTypeLotAndValidatedDate($_lot_ubd_alert, &$_lotchk = null, &$_datechk = null){
        //全てチェックなし
        if (!isset($_lot_ubd_alert) || $_lot_ubd_alert == 0){
            $_lotchk = false;
            $_datechk = false;
            return  false;
        }
        switch ($_lot_ubd_alert){
          case 1: //両方チェック
            $_lotchk = true;
            $_datechk = true;
            break;
          case 2: //Lotのみチェック
            $_lotchk = true;
            $_datechk = false;
            break;
          case 3: //有効期限のみチェック
            $_lotchk = false;
            $_datechk = true;
            break;
          default:
            $_lotchk = true;
            $_datechk = true;
            break;
        }
        return true;
    }
    /**
     * トランザクショントークン作成（コントロール名 + token）
     */
    public function createToken($modelName){

        $ses_token = sprintf("%s.%s",$modelName,"token");

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write($ses_token , $token);

        return $token;
    }
    /**
     *トランザクショントークンを検証
     */
    public function validateToken($formToken, $modelName){

        $ses_token = sprintf("%s.%s",$modelName,"token");

        if($formToken == $this->Session->read($ses_token)) {
            $this->Session->delete($ses_token);
            return true;
        }else{
            return false;
        }
    }

    /*
     * Ajax 部署コード
     * Submit時重複チェック
     */
    public function submitCheckDepartmentCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない

        $code           = $_POST['data']['MstDepartment']['department_code'];
        $facility_id    = $_POST['data']['MstDepartment']['mst_facility_id'];
        $department_id  = (isset($_POST['data']['MstDepartment']['id'])?$_POST['data']['MstDepartment']['id']:'');

        $arrayToJs    = array();
        $arrayToJs[0]    = array();
        $arrayToJs[0][0] = 'MstDepartmentDepartmentCode';

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_departments as a  ';
        $sql .= '   where ';
        $sql .= "     a.department_code = '".Sanitize::clean($code)."'  ";
        $sql .= '     and a.mst_facility_id = ' . $facility_id;
        if($department_id !== "" ){
            $sql .= '     and a.id <> ' . $department_id;
        }

        $ret = $this->MstDepartment->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[0][1] = true;

        }else{
            $arrayToJs[0][1] = false;
            $arrayToJs[0][2] = "* 部署コードは既に使用されています。";

        }

        return (json_encode($arrayToJs));
    }


    /*
     * Ajax 部署コード重複チェック
     */
    public function duplicateCheckDepartmentCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない
        $code           = $_POST['fieldValue'];
        $facility_id    = $_POST['facility_id'];
        $department_id  = (isset($_POST['department_id'])?$_POST['department_id']:'');

        $arrayToJs    = array();
        $arrayToJs[0] = 'MstDepartmentDepartmentCode';

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_departments as a  ';
        $sql .= '   where ';
        $sql .= "     a.department_code = '".Sanitize::clean($code)."'  ";
        $sql .= '     and a.mst_facility_id = ' . $facility_id;
        if($department_id !== "" ){
            $sql .= '     and a.id <> ' . $department_id;
        }

        $ret = $this->MstDepartment->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[1] = true;

        }else{
            $arrayToJs[1] = false;

        }

        return (json_encode($arrayToJs));
    }

    /*
     * Ajax 施設コード
     * Submit時重複チェック
     */
    public function submitCheckFacilityCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない
        $type   = $_POST['data']['MstFacility']['facility_type'];
        $code   = $_POST['data']['MstFacility']['facility_code'];

        $subcode1 = $_POST['data']['MstFacility']['subcode_1'];
        $subcode2 = $_POST['data']['MstFacility']['subcode_2'];
        $subcode3 = $_POST['data']['MstFacility']['subcode_3'];
        $subcode  = $subcode1 . $subcode2 . $subcode3;
        $err = 0;
        $facility_id    = (isset($_POST['data']['MstFacility']['id'])?$_POST['data']['MstFacility']['id']:'');

        $arrayToJs    = array();
        $arrayToJs[0]    = array();

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_facilities as a  ';
        $sql .= '   where ';
        $sql .= "     a.facility_code = '".Sanitize::clean($code)."'  ";
        if($type < 3 ){
            //病院、センターの場合 シールの番号が重複してしまうので、施設コードの重複不可
            $sql .= "     and a.facility_type in ( ". Configure::read('FacilityType.center')." , ".Configure::read('FacilityType.hospital')." )";
        }else{
            //施主、仕入先の場合 種別内でユニークであればOK
            $sql .= "     and a.facility_type = '" . $type . "' ";
        }
        if($facility_id !== "" ){
            $sql .= '     and a.id <> ' . $facility_id;
        }

        $ret = $this->MstFacility->query($sql);
        if($ret[0][0]['count'] != 0 ){
            $arrayToJs[$err][0] = 'MstFacilityFacilityCode';
            $arrayToJs[$err][1] = false;
            $arrayToJs[$err][2] = "* 仕入先コードは既に使用されています。";
            $err++;
        }

        //サブコードチェック
        if($subcode != "" ){
            if($subcode1==""){
                $arrayToJs[$err][0] = "MstFacilitySubCode1";
                $arrayToJs[$err][1] = false;
                $arrayToJs[$err][2] = "* サブコード1が入力されていません";
                $err++;
            }
            if($subcode2==""){
                $arrayToJs[$err][0] = "MstFacilitySubCode2";
                $arrayToJs[$err][1] = false;
                $arrayToJs[$err][2] = "* サブコード2が入力されていません";
                $err++;
            }
            if($subcode3==""){
                $arrayToJs[$err][0] = "MstFacilitySubCode3";
                $arrayToJs[$err][1] = false;
                $arrayToJs[$err][2] = "* サブコード3が入力されていません";
                $err++;
            }
            if( !strstr($subcode1, Configure::read('subcodeSplitter')) === false ){
                $arrayToJs[$err][0] = "MstFacilitySubCode1";
                $arrayToJs[$err][1] = false;
                $arrayToJs[$err][2] = '* サブコードに使用できない文字「' . Configure::read('subcodeSplitter') .'」が含まれています';
                $err++;
            }
            if( !strstr($subcode2, Configure::read('subcodeSplitter')) === false ){
                $arrayToJs[$err][0] = "MstFacilitySubCode2";
                $arrayToJs[$err][1] = false;
                $arrayToJs[$err][2] = '* サブコードに使用できない文字「' . Configure::read('subcodeSplitter') .'」が含まれています';
                $err++;
            }
            if( !strstr($subcode3, Configure::read('subcodeSplitter')) === false ){
                $arrayToJs[$err][0] = "MstFacilitySubCode3";
                $arrayToJs[$err][1] = false;
                $arrayToJs[$err][2] = '* サブコードに使用できない文字「' . Configure::read('subcodeSplitter') .'」が含まれています';
                $err++;
            }
        }
        if($err == 0 ){
            $arrayToJs[0][1] = true;
        }
        return (json_encode($arrayToJs));
    }

    /*
     * Ajax 施設コード重複チェック
     */
    public function duplicateCheckFacilityCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない
        $code           = $_POST['fieldValue'];
        $type           = $_POST['facility_type'];
        $facility_id    = (isset($_POST['facility_id'])?$_POST['facility_id']:'');

        $arrayToJs    = array();
        $arrayToJs[0] = 'MstFacilityFacilityCode';

        if($type!=""){
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     mst_facilities as a  ';
            $sql .= '   where ';
            $sql .= "     a.facility_code = '".Sanitize::clean($code)."'  ";
            if($type < 3 ){
                //病院、センターの場合 シール番号が重複してしまうので、施設コードの重複不可
                $sql .= "     and a.facility_type in ( ".Configure::read('FacilityType.center')." , ".Configure::read('FacilityType.hospital')." ) ";
            }else{
                //施主、仕入先の場合、種別内でユニークであればOK
                $sql .= "     and a.facility_type = '" . $type . "' ";
            }
            if($facility_id !== "" ){
                $sql .= '     and a.id <> ' . $facility_id;
            }

            $ret = $this->MstFacility->query($sql);

            if($ret[0][0]['count'] == 0 ){
                $arrayToJs[1] = true;
            }else{
                $arrayToJs[1] = false;
            }
        }else{
            $arrayToJs[1] = false;
            $arrayToJs[2] = '種別を選択してください';
        }
        return (json_encode($arrayToJs));
    }


    /*
     * Ajax ユーザID
     * Submit時重複チェック
     */
    public function submitCheckUserId(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない
        $login_id = $_POST['data']['MstUser']['login_id'];
        $user_id  = (isset($_POST['data']['MstUser']['id'])?$_POST['data']['MstUser']['id']:0);

        $arrayToJs       = array();
        $arrayToJs[0]    = array();
        $arrayToJs[0][0] = 'MstUserLoginId';

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_users as a  ';
        $sql .= '   where ';
        $sql .= "     a.login_id = '".Sanitize::clean($login_id)."'  ";
        if($user_id !== "" ){
            $sql .= '     and a.id <> ' . $user_id;
        }

        $ret = $this->MstUser->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[0][1] = true;
        }else{
            $arrayToJs[0][1] = false;
            $arrayToJs[0][2] = "* 利用者IDは既に使用されています。";
       }

        return (json_encode($arrayToJs));
    }

    /*
     * Ajax ユーザID重複チェック
     */
    public function duplicateCheckUserId(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない
        $login_id = $_POST['fieldValue'];
        $user_id  = (isset($_POST['user_id'])?$_POST['user_id']:'');

        $arrayToJs    = array();
        $arrayToJs[0] = $_POST['fieldId'];

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_users as a  ';
        $sql .= '   where ';
        $sql .= "     a.login_id = '".Sanitize::clean($login_id)."'  ";
        if($user_id !== "" ){
            $sql .= '     and a.id <> ' . $user_id;
        }

        $ret = $this->MstUser->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[1] = true;
        }else{
            $arrayToJs[1] = false;
        }

        return (json_encode($arrayToJs));
    }

    /*
     * Ajax 販売元コード
     * Submit時重複チェック
     */
    public function submitCheckDealerCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない
        $dealer_code = $_POST['data']['MstDealer']['dealer_code'];
        $dealer_id   = (isset($_POST['data']['MstDealer']['id'])?$_POST['data']['MstDealer']['id']:0);

        $arrayToJs       = array();
        $arrayToJs[0]    = array();
        $arrayToJs[0][0] = 'MstDealerDealerCode';

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_dealers as a  ';
        $sql .= '   where ';
        $sql .= "     a.dealer_code = '".Sanitize::clean($dealer_code)."'  ";
        if($dealer_id !== "" ){
            $sql .= '     and a.id <> ' . $dealer_id;
        }

        $ret = $this->MstDealer->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[0][1] = true;
        }else{
            $arrayToJs[0][1] = false;
            $arrayToJs[0][2] = "* 販売元コードは既に使用されています。";
       }

        return (json_encode($arrayToJs));
    }

    /*
     * Ajax 販売元コード重複チェック
     */
    public function duplicateCheckDealerCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない
        $dealer_code = $_POST['fieldValue'];
        $dealer_id   = (isset($_POST['dealer_id'])?$_POST['dealer_id']:'');

        $arrayToJs    = array();
        $arrayToJs[0] = $_POST['fieldId'];

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_dealers as a  ';
        $sql .= '   where ';
        $sql .= "     a.dealer_code = '".Sanitize::clean($dealer_code)."'  ";
        if($dealer_id !== "" ){
            $sql .= '     and a.id <> ' . $dealer_id;
        }

        $ret = $this->MstDealer->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[1] = true;
        }else{
            $arrayToJs[1] = false;
        }

        return (json_encode($arrayToJs));
    }


    /*
     * Ajax 単位コード
     * Submit時重複チェック
     */
    public function submitCheckUnitNameCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない
        $unitname_code = $_POST['data']['MstUnitName']['internal_code'];
        $unitname_id   = (isset($_POST['data']['MstUnitName']['id'])?$_POST['data']['MstUnitName']['id']:0);

        $arrayToJs       = array();
        $arrayToJs[0]    = array();
        $arrayToJs[0][0] = 'MstUnitNameInternalCode';

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_unit_names as a  ';
        $sql .= '   where ';
        $sql .= "     a.internal_code = '".Sanitize::clean($unitname_code)."'  ";
        if($unitname_id !== "" ){
            $sql .= '     and a.id <> ' . $unitname_id;
        }

        $ret = $this->MstUnitName->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[0][1] = true;
        }else{
            $arrayToJs[0][1] = false;
            $arrayToJs[0][2] = "* 単位コードは既に使用されています。";
       }

        return (json_encode($arrayToJs));
    }

    /*
     * Ajax 単位コード重複チェック
     */
    public function duplicateCheckUnitNameCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない
        $unitname_code = $_POST['fieldValue'];
        $unitname_id   = (isset($_POST['unitname_id'])?$_POST['unitname_id']:'');

        $arrayToJs    = array();
        $arrayToJs[0] = $_POST['fieldId'];

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_unit_names as a  ';
        $sql .= '   where ';
        $sql .= "     a.internal_code = '".Sanitize::clean($unitname_code)."'  ";
        if($unitname_id !== "" ){
            $sql .= '     and a.id <> ' . $unitname_id;
        }

        $ret = $this->MstUnitName->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[1] = true;
        }else{
            $arrayToJs[1] = false;
        }

        return (json_encode($arrayToJs));
    }


    /*
     * Ajax 術式コード
     * Submit時重複チェック
     */
    public function submitCheckMethodCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない

        $code           = $_POST['data']['MstOpeMethod']['code'];
        $facility_id    = $_POST['data']['MstOpeMethod']['mst_facility_id'];
        $method_id      = (isset($_POST['data']['MstOpeMethod']['id'])?$_POST['data']['MstOpeMethod']['id']:'');

        $arrayToJs    = array();
        $arrayToJs[0]    = array();
        $arrayToJs[0][0] = 'code';

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_ope_method_names as a  ';
        $sql .= '   where ';
        $sql .= "     a.ope_method_set_code = '".Sanitize::clean($code)."'  ";
        $sql .= '     and a.mst_facility_id = ' . $facility_id;
        $sql .= '     and a.is_deleted = false ';
        if($method_id !== "" ){
            $sql .= '     and a.id <> ' . $method_id;
        }

        $ret = $this->MstOpeMethodName->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[0][1] = true;
        }else{
            $arrayToJs[0][1] = false;
            $arrayToJs[0][2] = "* 術式コードは既に使用されています。";

        }

        return (json_encode($arrayToJs));
    }


    /*
     * Ajax 術式コード重複チェック
     */
    public function duplicateCheckMethodCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない
        $code           = $_POST['fieldValue'];
        $facility_id    = $_POST['facility_id'];
        $method_id      = (isset($_POST['method_id'])?$_POST['method_id']:'');

        $arrayToJs    = array();
        $arrayToJs[0] = 'code';

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_ope_method_names as a  ';
        $sql .= '   where ';
        $sql .= "     a.ope_method_set_code = '".Sanitize::clean($code)."'  ";
        $sql .= '     and a.mst_facility_id = ' . $facility_id;
        $sql .= '     and a.is_deleted = false ';
        if($method_id !== "" ){
            $sql .= '     and a.id <> ' . $method_id;
        }

        $ret = $this->MstOpeMethodName->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[1] = true;
        }else{
            $arrayToJs[1] = false;
        }

        return (json_encode($arrayToJs));
    }

    /*
     * Ajax 材料セットコード
     * Submit時重複チェック
     */
    public function submitCheckItemSetCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない

        $code           = $_POST['data']['MstOpeItemSetHeader']['ope_item_set_code'];
        $facility_id    = $_POST['data']['MstOpeItemSetHeader']['mst_facility_id'];
        $itemset_id     = (isset($_POST['data']['MstOpeItemSetHeader']['id'])?$_POST['data']['MstOpeItemSetHeader']['id']:'');

        $arrayToJs    = array();
        $arrayToJs[0]    = array();
        $arrayToJs[0][0] = 'code';

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_ope_item_set_headers as a  ';
        $sql .= '   where ';
        $sql .= "     a.ope_item_set_code = '".Sanitize::clean($code)."'  ";
        $sql .= '     and a.mst_facility_id = ' . $facility_id;
        $sql .= '     and a.is_deleted = false ';
        if($itemset_id !== "" ){
            $sql .= '     and a.id <> ' . $itemset_id;
        }

        $ret = $this->MstOpeMethodName->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[0][1] = true;
        }else{
            $arrayToJs[0][1] = false;
            $arrayToJs[0][2] = "* 材料セットコードは既に使用されています。";
        }
        return (json_encode($arrayToJs));
    }


    /*
     * Ajax 材料セットコード重複チェック
     */
    public function duplicateCheckItemSetCode(){
        App::import('Sanitize');
        Configure::write('debug', 0); //デバック抑止
        $this->autoRender = false; //Viewを使わない
        $code           = $_POST['fieldValue'];
        $facility_id    = $_POST['facility_id'];
        $itemset_id      = (isset($_POST['itemset_id'])?$_POST['itemset_id']:'');

        $arrayToJs    = array();
        $arrayToJs[0] = 'code';

        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     mst_ope_item_set_headers as a  ';
        $sql .= '   where ';
        $sql .= "     a.ope_item_set_code = '".Sanitize::clean($code)."'  ";
        $sql .= '     and a.mst_facility_id = ' . $facility_id;
        $sql .= '     and a.is_deleted = false ';
        if($itemset_id !== "" ){
            $sql .= '     and a.id <> ' . $itemset_id;
        }

        $ret = $this->MstOpeMethodName->query($sql);

        if($ret[0][0]['count'] == 0 ){
            $arrayToJs[1] = true;
        }else{
            $arrayToJs[1] = false;
        }

        return (json_encode($arrayToJs));
    }

    /*
     * 配列内から特定要素を削除
     */
    function array_unset_keys( $array, $keys) {
        $unset_keys = is_array( $keys) == TRUE? array_flip( $keys): array( $keys=>TRUE);
        $result = array_diff_key( $array, $unset_keys);
        return $result;
    }

    /**
     * 有効期限を取得する。
     */
    public function getNextEffectiveDate($day , $next = false ){
        $months = Configure::read('UserPassword.updateMonth');
        sort($months); //念のため、昇順に並べ直しておく

        $now_month = date('m' , strtotime($day));
        if($next){
            $now_month++;
        }

        $year = date('Y' , strtotime($day));
        $month = 0;

        for($i=0;$i<count($months);$i++){
            if($now_month <= $months[$i]){
                $month = $months[$i];
                break;
            }
        }

        if($month == 0 ){
            $month = $months[0]; //最小値を返す
            $year++; //翌年にする。
        }

        return date("Y/m/d", mktime(0, 0, 0, $month+1, 0, $year));
    }

    /**
     * 平文パスワードをMD5で暗号化して返す
     */
    public function getHashPassword($password){
        if(Configure::read('Password.Security') == 1 ){
            return md5(Configure::read('Security.salt') . $password);
        }else{
            return $password;
        }
    }

    /**
     * 日付を受けて月末日を返す。
     */
    public function getLastDay($date){
        return date('Y/m/t', strtotime($date));
    }

    /**
     * SQLのカウント件数を返す
     */
    public function getMaxCount($sql , $model , $split_table_flg = true ){
        if($split_table_flg){
            $ret = $this->$model->query('select count(*) as count from ( ' . $sql . ' ) as x ' );
        }else{
            $ret = $this->$model->query('select count(*) as count from ( ' . $sql . ' ) as x ' , false , false);
        }
        return $ret[0][0]['count'];
    }

    /**
     * 分割テーブル用
     */
    public function split_table($sql) {
        if(Configure::read('SplitTable.flag') == 1){
            $table_names = Configure::read('split_tables');

            if(isset($_SESSION['Auth']['facility_id_selected'])){
                $center_id = $_SESSION['Auth']['facility_id_selected'];
                foreach($table_names as $t){
                    $tbl_name = $t . '_' . $center_id;
                    $sql = str_replace ($t, $tbl_name, $sql);
                }
            }
        }
        return $sql;
    }

    /**
     * 大量データ対策済みのCSV出力
     */
    public function db_export_csv($sql , $csvname , $redirect, $split_table_flg=1 , $title=true){
        $db = new DATABASE_CONFIG();
        $dsn = sprintf("dbname=%s port=%s user=%s password=%s options='--client_encoding=%s'"
                       , $db->default['database']
                       , $db->default['port']
                       , $db->default['login']
                       , $db->default['password']
                       , $db->default['encoding']);
        if ($db->default['host']) {
            $dsn .= sprintf(" host=%s", $db->default['host']);
        }
        //大量データ取得の場合Cakeのメモリ超過でエラーになるので、
        //Cakeのクエリメソッドは使用しない
        $con = pg_connect($dsn);
        if (!$con){
            $this->Session->setFlash('DBに接続できませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
            $this->redirect($redirect);
        }
        set_time_limit(0);
        if ($split_table_flg == 1) {
            $sql = $this->split_table($sql);
        }

        $result = pg_query($con, $sql);
        if (!$result){
            $this->Session->setFlash('DBクエリエラー。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
            pg_close($con);
            $this->redirect($redirect);
        }

        $rec_cnt = pg_num_rows($result);
        if ($rec_cnt == 0){
            $this->Session->setFlash('対象データがありませんでした。', 'growl',  array('type'=>'important'));
            $this->redirect($redirect);
        }

        // CSVファイル名設定
        $filename = $csvname .'_'. date('YmdHis') .  '.csv';
        $this->CsvWriteUtils->setFileName( $filename );

        // CSVヘッダオープン
        $this->CsvWriteUtils->open();
        $fields = pg_num_fields($result);
        $fieldAry = array();
        for($i=0; $i<$fields; $i++){
            $fieldAry[] = pg_field_name($result, $i);
        }

        // ヘッダ情報記述(配列のkey情報取得)
        $this->CsvWriteUtils->fieldClear();
        if($title){
            $this->CsvWriteUtils->writeLineArray( array_values($fieldAry) );
        }
        while($rows = pg_fetch_row($result)){
            $values = array_values($rows);
            $this->CsvWriteUtils->fieldClear();
            $this->CsvWriteUtils->writeLineArray( $values );
        }

        pg_close($con);
        exit(0);
    }

    /**
     * 販売元検索 Ajax用
     * @access public
     * @param string $field
     * @param string $value
     * @return string json formatted.
     */
    public function search_dealer($field = null, $value = null) {
        App::import('Sanitize');
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($field) && isset($value)) {
            $_name = ($_POST['value']!='')? $_POST['value']:$value;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $_dls = array();

                //検索条件に該当する件数の取得
                $sql = "select count(*) from mst_dealers as MstDealer ";
                $sql .= ' where 1=1';
                if (isset($_name)) {
                    $sql .= " and MstDealer.dealer_name LIKE '%". Sanitize::clean($_name) . "%'";
                }

                //$sql .= ' and MstDealer.start_date <= now() ';
                //$sql .= ' and (MstDealer.end_date > now() or MstDealer.end_date is null) ';
                $sql .= ' and MstDealer.is_deleted = false ';

                $ret = $this->MstDealer->query($sql , false);
                $_counts = $ret[0][0]['count'];
                if ($_counts > 50) {
                    $_dls[0]['msg'] = '検索結果が50件以上存在します['.$_counts.'件'.']';
                }

                $sql  = 'select ';
                $sql .= ' id          AS "MstDealer__id",';
                $sql .= ' dealer_code AS "MstDealer__dealer_code",';
                $sql .= ' dealer_name AS "MstDealer__dealer_name"';
                $sql .= ' from mst_dealers as MstDealer';
                $sql .= ' where 1=1';
                $sql .= " and MstDealer.dealer_name LIKE '%". Sanitize::clean($_name) . "%'";
                //$sql .= ' and MstDealer.start_date <= now() ';
                //$sql .= ' and (MstDealer.end_date > now() or MstDealer.end_date is null) ';
                $sql .= ' and MstDealer.is_deleted = false ';
                $sql .= ' order by MstDealer.dealer_type,MstDealer.dealer_name, MstDealer.dealer_code';
                $sql .= ' limit 50 ';

                $_dealers = $this->MstDealer->query($sql);
                $_counts = count($_dealers);
                for ($_i = 0;$_i < $_counts;$_i++) {
                    $_dls[$_i]['id']          = $_dealers[$_i]['MstDealer']['id'];
                    $_dls[$_i]['dealer_name'] = $_dealers[$_i]['MstDealer']['dealer_name'];
                    $_dls[$_i]['dealer_code'] = $_dealers[$_i]['MstDealer']['dealer_code'];
                }
                unset($_params, $_dealers, $_counts);
                return (json_encode($_dls));
            } else {
                throw new Exception('error');
            }
        }
    }

    /**
     * JMDN検索 ajax用
     * @param $field
     * @param $value
     */
    public function search_jmdn_name ($field = null, $value = null) {
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($field) && isset($value)) {
            $_name = ($_POST['value']!='')? $_POST['value']:$value;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $_params = $_pre_jmdns = $_jmdns = array();
                $_params['fields'] = array('MstJmdnName.jmdn_code','MstJmdnName.jmdn_name');
                $_params['conditions'] =  array('MstJmdnName.jmdn_name LIKE ' => '%'. $_name . '%');

                $_params['conditions'] =  array('or' =>
                                                array( 'MstJmdnName.jmdn_name LIKE ' => '%'. $_name . '%' ,
                                                       'MstJmdnName.jmdn_code LIKE ' => '%'. $_name . '%' )
                                                );

                
                
                $_params['recursive'] = -1;
                $_params['order'] = array('MstJmdnName.jmdn_code');

                //検索条件に該当する件数の取得
                $sql = "select count(*) from mst_jmdn_names ";
                if (isset($_name)) {
                    $sql .= " where jmdn_name LIKE '%". $_name ."%'";
                    $sql .= "    or jmdn_code LIKE '%". $_name ."%'";
                }

                $ret = $this->MstJmdnName->query($sql , false);
                $_counts = $ret[0][0]['count'];
                if ($_counts > 50) {
                    $_jmdns[0]['msg'] = '検索結果が50件以上存在します['.$_counts.'件'.']';
                }

                $_params['limit'] = 50;
                $_pre_jmdns = $this->MstJmdnName->find('all', $_params);
                $_counts = count($_pre_jmdns);
                for ($_i = 0;$_i < $_counts;$_i++) {
                    $_jmdns[$_i]['jmdn_name'] = $_pre_jmdns[$_i]['MstJmdnName']['jmdn_name'];
                    $_jmdns[$_i]['jmdn_code'] = $_pre_jmdns[$_i]['MstJmdnName']['jmdn_code'];
                }
                return (json_encode($_jmdns));
                unset($_params, $_pre_jmdns, $_jmdns, $_counts);
            } else {
                throw new Exception('error');
            }
        }
    }

    /**
     * 保険請求区分検索 Ajax用
     * @param $field
     * @param $value
     */
    public function search_icc_name ($field = null, $value = null) {
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($field) && isset($value)) {
            $_name = ($_POST['value']!='')? $_POST['value']:$value;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $_params = $_pre_iccs = $_iccs = array();
                $_params['fields'] = array('MstInsuranceClaim.insurance_claim_code','MstInsuranceClaim.insurance_claim_name','MstInsuranceClaim.insurance_claim_name_s');
                $_params['conditions'] =  array('or' => array( 'MstInsuranceClaim.insurance_claim_name LIKE ' => '%'. $_name . '%' ,
                                                'MstInsuranceClaim.insurance_claim_code LIKE ' => '%'. $_name . '%' )
                                                );
                $_params['recursive'] = -1;
                $_params['order'] = array('MstInsuranceClaim.insurance_claim_code');

                //検索条件に該当する件数の取得
                $sql  = " select count(*) from mst_insurance_claims ";
                if (isset($_name)) {
                    $sql .= " where insurance_claim_name LIKE '%". $_name . "%'";
                }

                $ret = $this->MstInsuranceClaim->query($sql , false);
                $_counts = $ret[0][0]['count'];
                if ($_counts > 50) {
                    $_iccs[0]['msg'] = '検索結果が50件以上存在します['.$_counts.'件'.']';
                }

                $_params['limit'] = 50;
                $_pre_iccs = $this->MstInsuranceClaim->find('all', $_params);
                $_counts = count($_pre_iccs);
                for ($_i = 0;$_i < $_counts;$_i++) {
                    $_iccs[$_i]['insurance_claim_code']   = $_pre_iccs[$_i]['MstInsuranceClaim']['insurance_claim_code'];
                    $_iccs[$_i]['insurance_claim_name_s'] = $_pre_iccs[$_i]['MstInsuranceClaim']['insurance_claim_name_s'];
                    $_iccs[$_i]['insurance_claim_name']   = $_pre_iccs[$_i]['MstInsuranceClaim']['insurance_claim_name'];
                }
                return (json_encode($_iccs));
                unset($_params, $_pre_iccs, $_iccs, $_counts);
            } else {
                throw new Exception('error');
            }
        }
    }

    /**
     * 商品カテゴリ検索 Ajax用
     *
     * @param $field
     * @param $value
     */
    public function search_item_category_code ($field = null, $value = null) {
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($field) && isset($value)) {
            $_name = ($_POST['value']!='')? $_POST['value']:$value;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $_params = $_pre_itemCategories = $_itemCategories = array();
                $_params['fields'] = array('MstItemCategory.category_code','MstItemCategory.category_name');
                $_params['conditions'] =  array('MstItemCategory.category_name LIKE ' => '%'. $_name . '%');
                $_params['recursive'] = -1;
                $_params['order'] = array('MstItemCategory.category_code');

                //検索条件に該当する件数の取得
                $sql = "select count(*) from mst_item_categories ";
                if (isset($_name)) {
                    $sql .= " where category_name LIKE '%". $_name . "%'";
                }

                $ret = $this->MstItemCategory->query($sql , false);
                $_counts = $ret[0][0]['count'];
                if ($_counts > 50) {
                    $_itemCategories[0]['msg'] = '検索結果が50件以上存在します['.$_counts.'件'.']';
                }

                $_params['limit'] = 50;
                $_pre_itemCategories = $this->MstItemCategory->find('all', $_params);
                $_counts = count($_pre_itemCategories);
                for ($_i = 0;$_i < $_counts;$_i++) {
                    $_itemCategories[$_i]['category_code'] = $_pre_itemCategories[$_i]['MstItemCategory']['category_code'];
                    $_itemCategories[$_i]['category_name'] = $_pre_itemCategories[$_i]['MstItemCategory']['category_name'];
                }
                return (json_encode($_itemCategories));
                unset($_params, $_pre_itemCategories, $_itemCategories, $_counts);
            } else {
                throw new Exception('error');
            }
        }
    }

    public function _createPullDown(){
        /* 必要なプルダウンを作成 */
        // 保険請求区分
        $this->set('insuranceClaimTypes', $this->MstConst->find('list' ,
                                                          array(
                                                              'conditions'=> array('const_group_cd'=>Configure::read('ConstGroupType.insuranceClaimType')) ,
                                                              'fields'=> array('const_cd',
                                                                               'const_nm'),
                                                              'order' => array('const_cd')
                                                              )
                                                      )
                   );

        // クラス分類マスタ
        $this->set('classseparations', $this->MstConst->find('list' ,
                                                          array(
                                                              'conditions'=> array('const_group_cd'=>Configure::read('ConstGroupType.classSeparations')) ,
                                                              'fields'=> array('const_cd',
                                                                               'const_nm'),
                                                              'order' => array('const_cd')
                                                              )
                                                      )
                   );
        // 生物由来区分
        $this->set('biogenous_types', $this->MstConst->find('list' ,
                                                            array(
                                                                'conditions'=> array('const_group_cd'=>Configure::read('ConstGroupType.biogenousTypes')) ,
                                                                'fields'=> array('const_cd',
                                                                                 'const_nm'),
                                                                'order' => array('const_cd')
                                                                )
                                                            )
                   );
        
        // 課税区分
        $this->set('taxes', $this->MstConst->find('list' ,
                                                          array(
                                                              'conditions'=> array('const_group_cd'=>Configure::read('ConstGroupType.taxes')) ,
                                                              'fields'=> array('const_cd',
                                                                               'const_nm'),
                                                              'order' => array('const_cd')
                                                              )
                                                      )
                   );
        // 医事シール区分
        $this->set('cost_sticker', $this->MstConst->find('list' ,
                                                          array(
                                                              'conditions'=> array('const_group_cd'=>Configure::read('ConstGroupType.costSticker')) ,
                                                              'fields'=> array('const_cd',
                                                                               'const_nm'),
                                                              'order' => array('const_cd')
                                                              )
                                                      )
                   );
        // LOT/UBD警告
        $this->set('lotubd', $this->MstConst->find('list' ,
                                                          array(
                                                              'conditions'=> array('const_group_cd'=>Configure::read('ConstGroupType.lotubd')) ,
                                                              'fields'=> array('const_cd',
                                                                               'const_nm'),
                                                              'order' => array('const_cd')
                                                              )
                                                      )
                   );

        // 施主
        $this->set('owners', $this->getFacilityList(
            $this->Session->read('Auth.facility_id_selected'),
            array(Configure::read('FacilityType.donor')),
            true
            )
        );

        // 仕入先
        $this->set('suppliers',$this->getFacilityList(
            $this->Session->read('Auth.facility_id_selected') ,
            array(Configure::read('FacilityType.supplier')),
            true,
            array('id' , 'facility_name')
            )
        );
        
        // 基本単位マスタ
        $this->set('item_units', $this->MstUnitName->find('list' ,
                                               array(
                                                   'conditions'=> array('MstUnitName.is_deleted'=>false) ,
                                                   'fields'=> array('MstUnitName.id',
                                                                    'MstUnitName.unit_name'),
                                                   'order' => array('MstUnitName.order_num')
                                                   )
                                               )
                   );


        // 勘定科目取得
        $this->set('accountlists' , $this->MstAccountlist->find('list' ,
                                                     array(
                                                         'conditions' => array(
                                                             'MstAccountlist.mst_facility_id' => $this->Session->read('Auth.facility_id_selected')
                                                             ),
                                                         'order' => array('MstAccountlist.id')
                                                         )
                                                     )
                   );
        
        // お気に入りグループ取得
        $this->set('favoritelists', $this->MstFavoriteGroup->find('list' ,
                                                     array(
                                                         'fields'=> array('MstFavoriteGroup.id',
                                                                          'MstFavoriteGroup.group_name'),
                                                         'conditions' => array(
                                                             'MstFavoriteGroup.mst_facility_id' => $this->Session->read('Auth.facility_id_selected')
                                                             ),
                                                         'order' => array('MstFavoriteGroup.group_code')
                                                         )
                                                     )
                   );

        // 大分類
        $_item_category1 = $this->MstItemCategory->find('list' ,
                                                        array(
                                                            'fields'=> array('id' ,
                                                                             'category_name'),
                                                            'conditions' => array(
                                                                'category_rank' => 1 ,
                                                                'is_deleted = false '
                                                                ),
                                                            'order' => array('category_code')
                                                            )
                                                        );
        $this->set('item_category1',$_item_category1);

        if(isset($this->request->data['MstFacilityItem']['item_category1'])){
            $item_category2 = $this->getItemCategory(2,$this->request->data['MstFacilityItem']['item_category1']);
        }else{
            $item_category2 = array();
        }
        $this->set('item_category2' , $item_category2);

        if(isset($this->request->data['MstFacilityItem']['item_category2'])){
            $item_category3 = $this->getItemCategory(3,$this->request->data['MstFacilityItem']['item_category2']);
        }else{
            $item_category3 = array();
        }
        $this->set('item_category3' , $item_category3);

        // 保管区分
        $this->set('keep_type', $this->MstConst->find('list' ,
                                                          array(
                                                              'conditions'=> array('const_group_cd'=>Configure::read('ConstGroupType.keep')) ,
                                                              'fields'=> array('const_cd',
                                                                               'const_nm'),
                                                              'order' => array('const_cd')
                                                              )
                                                      )
                   );
        
        /* 必要なプルダウン作成 おわり */
    }

    function getItemCategory($rank , $id=null){
        return $this->MstItemCategory->find('list' ,
                                            array(
                                                'fields'=> array('id' ,'category_name'),
                                                'conditions' => array(
                                                    'category_rank' => $rank ,
                                                    'is_deleted = false ',
                                                    'parent_id' =>  $id
                                                    ),
                                                'order' => array('category_code')
                                                )
                                            );
    }
    


    
    public function getStockRecode($item_unit_id , $department_id=null){
        if(is_null($department_id)){
            //部署指定がない場合センターの部署を使う
            $department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') , array(Configure::read('DepartmentType.warehouse')));
        }
        $params = array(
            'recursive' => -1,
            'conditions' => array(
                'TrnStock.is_deleted' => FALSE,
                'TrnStock.mst_item_unit_id'  => $item_unit_id,
                'TrnStock.mst_department_id' => $department_id,
                ),
            );
        
        $Stock = $this->TrnStock->find('first', $params);
        if(empty( $Stock )) {
            //在庫レコードが無い場合はレコードを作成する
            $stock_data = array(
                'mst_item_unit_id'  => $item_unit_id,
                'mst_department_id' => $department_id,
                'stock_count'       => '0',
                'reserve_count'     => '0',
                'promise_count'     => '0',
                'requested_count'   => '0',
                'receipted_count'   => '0',
                'is_deleted'        => false,
                'creater'           => $this->Session->read('Auth.MstUser.id'),
                'modifier'          => $this->Session->read('Auth.MstUser.id'),
                );
            $this->TrnStock->create();
            if (!$this->TrnStock->save($stock_data)) {
                //在庫の更新失敗
                return false;
            }
            $stock_id = $this->TrnStock->getLastInsertID();
        }else{
            $stock_id = $Stock['TrnStock']['id'];
        }
        return $stock_id;
    }


    public function getStockId($trn_sticker_id){
        $sql  = ' select ';
        $sql .= '       b.id  ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join trn_stocks as b  ';
        $sql .= '       on b.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '       and b.mst_department_id = a.mst_department_id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $trn_sticker_id;
        
        $res = $this->TrnSticker->query($sql);
        
        return $res[0][0]['id'];
    }
    

    public function getReceivingStockRecode($item_unit_id , $department_id){
        $params = array(
            'recursive' => -1,
            'conditions' => array(
                'TrnSticker.is_deleted'        => FALSE,
                'TrnSticker.mst_item_unit_id'  => $item_unit_id,
                'TrnSticker.mst_department_id' => $department_id,
                'TrnSticker.lot_no'            => ' ',
                ),
            );
        
        $TrnSticker = $this->TrnSticker->find('first', $params);
        return $TrnSticker['TrnSticker']['id'];
    }
    

    /**
     * 指定した丸め区分で単価を計算する
     * @param string $roundType
     * @param float $unitPrice
     * @return mixed
     */
    public function getFacilityUnitPrice($roundType, $unitPrice , $quantity = 1 ){
        switch($roundType){
            case Configure::read('RoundType.Round'):
                return round($unitPrice * $quantity);
                break;
            case Configure::read('RoundType.Ceil'):
                return ceil($unitPrice * $quantity );
                break;
            case Configure::read('RoundType.Floor'):
                return floor($unitPrice * $quantity);
                break;
            default:
                return $unitPrice * $quantity;
        }
    }
    
    function createMsClaimRecode($id){
        //テーブルインポート
        App::import('Model','TrnClaim');
        //モデルのオブジェクト生成
        $TrnClaim = new TrnClaim;

        // MS法人対応が有効な場合
        if( $this->Session->read('Auth.Config.MSCorporate') == '1' ){
            $sql  = ' select ';
            $sql .= '     a.department_id_from ';
            $sql .= '   , a.department_id_to ';
            $sql .= '   , a.mst_item_unit_id ';
            $sql .= '   , a.claim_date ';
            $sql .= '   , a.claim_price ';
            $sql .= '   , a.trn_consume_id ';
            $sql .= '   , a.trn_receiving_id ';
            $sql .= '   , a.trn_shipping_id ';
            $sql .= '   , a.trn_retroact_record_id ';
            $sql .= '   , a.stocking_close_type ';
            $sql .= '   , a.sales_close_type ';
            $sql .= '   , a.facility_close_type ';
            $sql .= '   , a.is_deleted ';
            $sql .= '   , a.created ';
            $sql .= '   , a.modified ';
            $sql .= '   , a.is_stock_or_sale ';
            $sql .= '   , a.count ';
            $sql .= '   , a.unit_price ';
            $sql .= '   , a.round ';
            $sql .= '   , a.gross ';
            $sql .= '   , a.is_not_retroactive ';
            $sql .= '   , a.creater';
            $sql .= '   , a.modifier';
            $sql .= '   , a.facility_sticker_no';
            $sql .= '   , a.hospital_sticker_no';
            $sql .= '   , a.retroact_execution_flag ';
            $sql .= '   , a.center_facility_id ';
            $sql .= '   , a.trade_type ';
            $sql .= '   , c.is_ms_corporate ';
            $sql .= '   , e.ms_transaction_price  ';
            $sql .= ' from ';
            $sql .= '   trn_claims as a  ';
            $sql .= '   left join mst_item_units as b  ';
            $sql .= '     on b.id = a.mst_item_unit_id  ';
            $sql .= '   left join mst_facility_items as c  ';
            $sql .= '     on c.id = b.mst_facility_item_id  ';
            $sql .= '   left join mst_departments as d  ';
            $sql .= '     on d.id = a.department_id_from  ';
            $sql .= '   left join mst_transaction_configs as e  ';
            $sql .= '     on e.partner_facility_id = d.mst_facility_id  ';
            $sql .= '     and e.is_deleted = false  ';
            $sql .= '     and e.start_date <= a.claim_date  ';
            $sql .= '     and e.end_date > a.claim_date  ';
            $sql .= '     and e.mst_item_unit_id = a.mst_item_unit_id ';
            $sql .= '   left join mst_departments as f ';
            $sql .= '     on f.id = a.department_id_to  ';

            $sql .= ' where  ';
            $sql .= '   a.id = ' .$id;
            
            $ret = $TrnClaim->query($sql);
            $data = $ret[0][0];

            if(isset($data['is_ms_corporate']) && $data['is_ms_corporate'] == true ){
                $claim_price = $this->getFacilityUnitPrice( $data['round'], $data['ms_transaction_price'] , $data['count']);
                if( $data['claim_price'] < 0 ){
                    // 返品で作成される場合、単価がプラスで、請求金額がマイナスなので合わせる
                    $claim_price = 0 - $claim_price;
                    $claim_type = Configure::read('ClaimType.return');
                }else{
                    $claim_type = Configure::read('ClaimType.stock');
                }
                $TrnClaim = array(
                    'TrnClaim'=> array(
                        'department_id_from'      => $data['department_id_from'],
                        'department_id_to'        => $data['department_id_to'],
                        'mst_item_unit_id'        => $data['mst_item_unit_id'],
                        'claim_date'              => $data['claim_date'],
                        'claim_price'             => $claim_price,
                        'trn_consume_id'          => $data['trn_consume_id'],
                        'trn_receiving_id'        => $data['trn_receiving_id'],
                        'trn_shipping_id'         => $data['trn_shipping_id'],
                        'trn_retroact_record_id'  => $data['trn_retroact_record_id'],
                        'stocking_close_type'     => $data['stocking_close_type'],
                        'sales_close_type'        => $data['sales_close_type'],
                        'facility_close_type'     => $data['facility_close_type'],
                        'is_deleted'              => $data['is_deleted'],
                        'created'                 => $data['created'],
                        'modified'                => $data['modified'],
                        'is_stock_or_sale'        => '3',
                        'count'                   => $data['count'],
                        'unit_price'              => $data['ms_transaction_price'],
                        'round'                   => $data['round'],
                        'gross'                   => $data['gross'],
                        'is_not_retroactive'      => $data['is_not_retroactive'],
                        'creater'                 => $data['creater'],
                        'modifier'                => $data['modifier'],
                        'facility_sticker_no'     => $data['facility_sticker_no'],
                        'hospital_sticker_no'     => $data['hospital_sticker_no'],
                        'retroact_execution_flag' => $data['retroact_execution_flag'],
                        'center_facility_id'      => $data['center_facility_id'],
                        'trade_type'              => $data['trade_type'],
                        'claim_type'              => $claim_type
                    )
                );
                $this->TrnClaim->create();
                // SQL実行
                if (!$this->TrnClaim->save($TrnClaim)) {
                    return false;
                }
            }
        }
        return true;
    }


    function createSaleClaimRecode($id){
        //テーブルインポート
        App::import('Model','TrnClaim');
        //モデルのオブジェクト生成
        $TrnClaim = new TrnClaim;
        
        $sql  = ' select ';
        $sql .= '     a.department_id_from ';
        $sql .= '   , a.department_id_to ';
        $sql .= '   , a.mst_item_unit_id ';
        $sql .= '   , a.claim_date ';
        $sql .= '   , a.claim_price ';
        $sql .= '   , a.trn_consume_id ';
        $sql .= '   , a.trn_receiving_id ';
        $sql .= '   , a.trn_shipping_id ';
        $sql .= '   , a.trn_retroact_record_id ';
        $sql .= '   , a.stocking_close_type ';
        $sql .= '   , a.sales_close_type ';
        $sql .= '   , a.facility_close_type ';
        $sql .= '   , a.is_deleted ';
        $sql .= '   , a.created ';
        $sql .= '   , a.modified ';
        $sql .= '   , a.is_stock_or_sale ';
        $sql .= '   , a.count ';
        $sql .= '   , a.unit_price ';
        $sql .= '   , a.round ';
        $sql .= '   , a.gross ';
        $sql .= '   , a.is_not_retroactive ';
        $sql .= '   , a.creater';
        $sql .= '   , a.modifier';
        $sql .= '   , a.facility_sticker_no';
        $sql .= '   , a.hospital_sticker_no';
        $sql .= '   , a.retroact_execution_flag ';
        $sql .= '   , a.center_facility_id ';
        $sql .= '   , a.trade_type ';
        $sql .= '   , c.buy_flg ';
        $sql .= '   , c.is_lowlevel';
        $sql .= '   , e.sales_price  ';
        $sql .= ' from ';
        $sql .= '   trn_claims as a  ';
        $sql .= '   left join mst_item_units as b  ';
        $sql .= '     on b.id = a.mst_item_unit_id  ';
        $sql .= '   left join mst_facility_items as c  ';
        $sql .= '     on c.id = b.mst_facility_item_id  ';
        $sql .= '   left join mst_departments as d  ';
        $sql .= '     on d.id = a.department_id_from  ';
        $sql .= '   left join mst_sales_configs as e  ';
        $sql .= '     on e.is_deleted = false  ';
        $sql .= '     and e.start_date <= a.claim_date  ';
        $sql .= '     and e.end_date > a.claim_date  ';
        $sql .= '     and e.mst_item_unit_id = a.mst_item_unit_id ';
        $sql .= '   left join mst_departments as f ';
        $sql .= '     on f.id = a.department_id_to  ';
        
        $sql .= ' where  ';
        $sql .= '   a.id = ' .$id;
        
        $ret = $TrnClaim->query($sql);
        $data = $ret[0][0];
        
        if((isset($data['buy_flg']) && $data['buy_flg'] == true )
           || ( $this->Session->read('Auth.Config.LowlevelSalesType') == '1' && $data['is_lowlevel'] == true ))
          {
            $claim_price = $this->getFacilityUnitPrice( $data['round'], $data['sales_price'] , $data['count']);
            $TrnClaim = array(
                'TrnClaim'=> array(
                    'department_id_from'      => $data['department_id_from'],
                    'department_id_to'        => $data['department_id_to'],
                    'mst_item_unit_id'        => $data['mst_item_unit_id'],
                    'claim_date'              => $data['claim_date'],
                    'claim_price'             => $claim_price,
                    'trn_consume_id'          => $data['trn_consume_id'],
                    'trn_receiving_id'        => $data['trn_receiving_id'],
                    'trn_shipping_id'         => $data['trn_shipping_id'],
                    'trn_retroact_record_id'  => $data['trn_retroact_record_id'],
                    'stocking_close_type'     => $data['stocking_close_type'],
                    'sales_close_type'        => $data['sales_close_type'],
                    'facility_close_type'     => $data['facility_close_type'],
                    'is_deleted'              => $data['is_deleted'],
                    'created'                 => $data['created'],
                    'modified'                => $data['modified'],
                    'is_stock_or_sale'        => '2',
                    'count'                   => $data['count'],
                    'unit_price'              => $data['sales_price'],
                    'round'                   => $data['round'],
                    'gross'                   => $data['gross'],
                    'is_not_retroactive'      => $data['is_not_retroactive'],
                    'creater'                 => $data['creater'],
                    'modifier'                => $data['modifier'],
                    'facility_sticker_no'     => $data['facility_sticker_no'],
                    'hospital_sticker_no'     => $data['hospital_sticker_no'],
                    'retroact_execution_flag' => $data['retroact_execution_flag'],
                    'center_facility_id'      => $data['center_facility_id'],
                    'trade_type'              => $data['trade_type'],
                    'claim_type'              => Configure::read('ClaimType.sale')
                    )
                );
            $this->TrnClaim->create();
            // SQL実行
            if (!$this->TrnClaim->save($TrnClaim)) {
                return false;
            }
        }
        return true;
    }


    /**
     * xml出力テスト用
     * @param
     * @todo 各コントローラの共通関数化
     */
    function xml_output($data,$columns,$layout_name,$fix_value){
        $this->layout = 'xml/default';
        $this->set('columns',$columns);
        $this->set('posted',$data);
        $this->set('layout_name',$layout_name);
        $this->set('fix_value',$fix_value); //連想配列のキーにname属性を指定
        $this->render('/Elements/xml_output');
    }

    /**
     * 売り上げデータ作成
     */
    function createSalesData($sticker_id,$department_id,$work_date,$sales_price=null){
        // シール情報を取得
        $sql  = ' select ';
        $sql .= '       f.item_type            as "TrnSticker__item_type"';
        $sql .= '     , c.sales_price          as "TrnSticker__sales_price"';
        $sql .= '     , d.gross                as "TrnSticker__gross"';
        $sql .= '     , d.round                as "TrnSticker__round"';
        $sql .= '     , a.facility_sticker_no  as "TrnSticker__facility_sticker_no"';
        $sql .= '     , a.hospital_sticker_no  as "TrnSticker__hospital_sticker_no"';
        $sql .= '     , a.mst_item_unit_id     as "TrnSticker__mst_item_unit_id"';
        $sql .= '     , a.receipt_id           as "TrnSticker__receipt_id"';
        $sql .= '     , a.trn_consume_id       as "TrnSticker__trn_consume_id"';
        $sql .= '     , h.id                   as "TrnSticker__mst_department_id"';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as f  ';
        $sql .= '       on f.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_sales_configs as c  ';
        $sql .= '       on c.mst_item_unit_id = b.id  ';
        $sql .= "       and c.start_date <= '" . $work_date .  "'  ";
        $sql .= "       and c.end_date > '" . $work_date . "' ";
        $sql .= '     left join mst_facilities as d  ';
        $sql .= '       on d.id = c.partner_facility_id  ';
        $sql .= '     left join mst_departments as e  ';
        $sql .= '       on e.mst_facility_id = d.id  ';
        $sql .= '     left join mst_facilities as g';
        $sql .= '       on g.id = f.mst_facility_id ';
        $sql .= '     left join mst_departments as h ';
        $sql .= '       on h.mst_facility_id = g.id';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $sticker_id;
        $sql .= '     and e.id = ' . $department_id;
        
        $ret = $this->TrnSticker->query($sql);
        $s = $ret[0];
        
        if($s['TrnSticker']['item_type'] == Configure::read('Items.item_types.normalitem') ){
            if(is_null($sales_price)){
                $sales_price = $s['TrnSticker']['sales_price'];
            }
            // 売り上げデータを作成する
            $TrnClaim = array(
                'TrnClaim' => array(
                    'department_id_from'  => $s['TrnSticker']['mst_department_id'], // 倉庫部署ID
                    'department_id_to'    => $department_id,
                    'claim_date'          => $work_date,
                    'trn_consume_id'      => $s['TrnSticker']['trn_consume_id'],
                    'stocking_close_type' => 0,
                    'sales_close_type'    => 0,
                    'facility_close_type' => 0,
                    'trn_receiving_id'    => $s['TrnSticker']['receipt_id'],
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => date('Y/m/d H:i:s'),
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => date('Y/m/d H:i:s'),
                    'is_stock_or_sale'    => '2',  //売り上げ
                    'count'               => 1,
                    'is_not_retroactive'  => false,
                    'is_deleted'          => false,
                    'facility_sticker_no' => $s['TrnSticker']['facility_sticker_no'],
                    'hospital_sticker_no' => $s['TrnSticker']['hospital_sticker_no'],
                    'mst_item_unit_id'    => $s['TrnSticker']['mst_item_unit_id'],
                    'unit_price'          => $sales_price,
                    'round'               => $s['TrnSticker']['round'],
                    'gross'               => $s['TrnSticker']['gross'],
                    'claim_price'         => $this->getFacilityUnitPrice($s['TrnSticker']['round'] , $sales_price),
                    'claim_type'          => Configure::read('ClaimType.sale')
                    )
                );
            $this->TrnClaim->create();
            // SQL実行
            if (!$this->TrnClaim->save($TrnClaim)) {
                return false;
            }
            
            $TrnClaimId = $this->TrnClaim->getLastInsertID();
            
            // シールの売上IDを更新する。
            $this->TrnSticker->create();
            $res = $this->TrnSticker->updateAll(
                array(
                    'TrnSticker.sale_claim_id'   => $TrnClaimId,
                    ),
                array(
                    'TrnSticker.id' => $sticker_id,
                    ),
                -1);
            
            if(false === $res){
                return false;
            }
        }else{
            // セット品だったら構成要素で売上を作成する。
            
            $TrnClaimId = null;
        }
        
        
        return $TrnClaimId;
    }


    /**
     * Excelオブジェクト作成
     */
    function createExcelObj($filepath){
        // Excel出力用ライブラリ
        App::import('Vendor', 'PHPExcel', array('file'=>'phpexcel' . DS . 'PHPExcel.php'));
        App::import('Vendor', 'PHPExcel_IOFactory', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'IOFactory.php'));
        App::import('Vendor', 'PHPExcel_Cell_AdvancedValueBinder', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'Cell' . DS . 'AdvancedValueBinder.php'));
        
        // Excel95用ライブラリ
        App::import('Vendor', 'PHPExcel_Writer_Excel5', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'Writer' . DS . 'Excel5.php'));
        App::import('Vendor', 'PHPExcel_Reader_Excel5', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'Reader' . DS . 'Excel5.php'));
        try{ 
            $objReader = PHPExcel_IOFactory::createReader("Excel5");
            $PHPExcel = $objReader->load($filepath);
        } catch (Exception $ex) {
            return false;
        }
        return $PHPExcel;
    }
    
    /*
     * シール移動履歴のシーケンスを返す
     */
    function getNextRecord($sticker_id, $split=true){
        $sql  = ' select ';
        $sql .= '       coalesce(max(move_seq),0) + 1 as next_val ';
        $sql .= '   from ';
        $sql .= '     trn_sticker_records as a  ';
        $sql .= '   where ';
        $sql .= '     a.trn_sticker_id = ' . $sticker_id;
        $sql .= '     and a.is_deleted = false ';
        $ret = $this->TrnStickerRecord->query($sql, false, $split);
        return $ret[0][0]['next_val'];
    }

    /**
     * 丸め区分を取得
     * @param string $fid
     * @return string
     */
    public function getRoundType($fid){
        $res = $this->MstFacility->find('first', array(
            'conditions' => array(
                'id' => $fid,
            ),
            'recursive' => -1,
        ));
        return $res['MstFacility']['round'];
    }

    /**
     * まとめ区分を取得
     * @param string $fid
     * @return string
     */
    public function getGrossType($fid){
        $res = $this->MstFacility->find('first', array(
            'conditions' => array(
                'id' => $fid,
            ),
            'recursive' => -1,
        ));
        return $res['MstFacility']['gross'];
    }


    public function getHospitalDepartmentList($center_id){
        $sql  = ' select ';
        $sql .= '       c.department_code as "MstDepartment__department_code"';
        $sql .= '     , c.department_name as "MstDepartment__department_name"';
        $sql .= '   from ';
        $sql .= '     mst_facility_relations as a  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.partner_facility_id  ';
        $sql .= '       and b.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '     left join mst_departments as c  ';
        $sql .= '       on c.mst_facility_id = b.id  ';
        $sql .= '   where ';
        $sql .= '     c.is_deleted = false  ';
        $sql .= '     and a.mst_facility_id = ' . $center_id;
        $sql .= '   order by ';
        $sql .= '     c.department_code ';

        $list = Set::Combine(
            $this->MstDepartment->query($sql),
            "{n}.MstDepartment.department_code",
            "{n}.MstDepartment.department_name"
            );
        return $list;
    }


    public function getHospitalDepartmentId($department_code , $center_id){
        $sql  = ' select ';
        $sql .= '       c.id';
        $sql .= '   from ';
        $sql .= '     mst_facility_relations as a  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.partner_facility_id  ';
        $sql .= '       and b.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '     left join mst_departments as c  ';
        $sql .= '       on c.mst_facility_id = b.id  ';
        $sql .= '   where ';
        $sql .= '     c.is_deleted = false  ';
        $sql .= '     and a.mst_facility_id = ' . $center_id;
        $sql .= "     and c.department_code = '" . $department_code . "'";
        
        $ret = $this->MstDepartment->query($sql);
        return $ret[0][0]['id'];
    }
    
}
?>
