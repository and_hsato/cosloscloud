<?php
/**
 * MstSwitchController
 *
 * @version 1.0.0
 * @since 2012/08/17
 */

class MstSwitchController extends AppController {
    var $name = 'MstSwitch';

    /**
     *
     * @var array $uses
     */
    var $uses = array(
        'MstFixedCount',
        'MstItemUnit',
        'TrnStock',
        'MstDepartment',
        'MstFacility',
        'MstUserBelonging',
        'MstFacilityRelation',
        'TrnSwitchHistory',
        'TrnSwitchHistoryHeader'
        );

    /**
     * Components using.
     * @var array $components
     */
    var $components = array('RequestHandler','CsvWriteUtils','CsvReadUtils');

    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;

    /**
     * @var Departments
     */
    var $Departments;

    /**
     *
     */
    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        //CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }
    }


    function index(){
        $this->selectFromItem();
    }

    /*
     * 切替元商品選択
     */
    function selectFromItem(){
        App::import('Sanitize');
        //権限ロール設定
        $this->setRoleFunction(86);

        $SearchResult = array();
        //検索ボタン押下
        if(isset($this->request->data['MstSwitch']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            //画面から入力された検索条件
            $where = '';
            if($this->request->data['MstSwitch']['internal_code'] != ''){
                $where .= " and c.internal_code like '" . $this->request->data['MstSwitch']['internal_code'] . "%'";
            }
            if($this->request->data['MstSwitch']['item_code'] != ''){
                $where .= " and c.item_code like '%" . $this->request->data['MstSwitch']['item_code'] . "%'";
            }
            if($this->request->data['MstSwitch']['item_name'] != ''){
                $where .= " and c.item_name like '%" . $this->request->data['MstSwitch']['item_name'] . "%'";
            }
            if($this->request->data['MstSwitch']['jan_code'] != ''){
                $where .= " and c.jan_code like '" . $this->request->data['MstSwitch']['jan_code'] . "%'";
            }
            if($this->request->data['MstSwitch']['dealer_name'] != ''){
                $where .= " and d.dealer_name like'%" . $this->request->data['MstSwitch']['dealer_name'] . "%'";
            }
            if($this->request->data['MstSwitch']['standard'] != ''){
                $where .= " and c.standard like '%" . $this->request->data['MstSwitch']['standard'] . "%'";
            }

            $sql  = ' select ';
            $sql .= '       b.id                       as "MstSwitch__id" ';
            $sql .= '     , c.internal_code            as "MstSwitch__internal_code" ';
            $sql .= '     , c.item_name                as "MstSwitch__item_name" ';
            $sql .= '     , c.standard                 as "MstSwitch__standard" ';
            $sql .= '     , c.item_code                as "MstSwitch__item_code" ';
            $sql .= '     , d.dealer_name              as "MstSwitch__dealer_name" ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when b.per_unit = 1  ';
            $sql .= '         then e.unit_name  ';
            $sql .= "         else e.unit_name || '(' || b.per_unit || f.unit_name || ')'  ";
            $sql .= '         end ';
            $sql .= '     )                            as "MstSwitch__unit_name"  ';
            $sql .= '   from ';
            $sql .= '     mst_fixed_counts as a  ';
            $sql .= '     left join mst_item_units as b  ';
            $sql .= '       on b.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as c  ';
            $sql .= '       on c.id = b.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as d  ';
            $sql .= '       on d.id = c.mst_dealer_id  ';
            $sql .= '     left join mst_unit_names as e  ';
            $sql .= '       on e.id = b.mst_unit_name_id  ';
            $sql .= '     left join mst_unit_names as f  ';
            $sql .= '       on f.id = b.per_unit_name_id  ';
            $sql .= '     left join mst_fixed_counts as g  ';
            $sql .= '       on a.id = g.substitute_unit_id  ';
            $sql .= '     inner join mst_user_belongings as h';
            $sql .= '       on h.mst_facility_id = a.mst_facilities_id';
            $sql .= '      and h.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '      and h.is_deleted = false';
            $sql .= '   where ';
            $sql .= '     c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     and a.is_deleted = false  ';
            $sql .= '     and a.substitute_unit_id is null';
            $sql .= '     and g.id is null ';
            $sql .= $where;
            $sql .= '   group by ';
            $sql .= '     b.id ';
            $sql .= '     , c.internal_code ';
            $sql .= '     , c.item_name ';
            $sql .= '     , c.standard ';
            $sql .= '     , c.item_code ';
            $sql .= '     , d.dealer_name ';
            $sql .= '     , e.unit_name ';
            $sql .= '     , b.per_unit ';
            $sql .= '     , f.unit_name  ';
            $sql .= '   order by ';
            $sql .= '     c.internal_code ';
            $sql .= '     , b.per_unit ';
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFixedCount'));
            if(isset($this->request->data['MstSwitch']['limit'])){
                $sql .= ' limit ' . $this->request->data['MstSwitch']['limit'];
            }
            $SearchResult = $this->MstFixedCount->query($sql);
            $this->request->data = $data;
        }

        $this->set('SearchResult' , $SearchResult);
        $this->render('/switch/select_from_item');
    }

    /*
     * 部署選択
     */
    function selectDepartment(){
        //権限ロール設定
        $this->setRoleFunction(86);

        $SearchResult = array();
        $this->set('Facility_List' ,
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected'),
                       array(Configure::read('FacilityType.hospital')),
                       true ,
                       array('id' , 'facility_name')
                       )
                   );

        //検索ボタン押下
        if(isset($this->request->data['MstSwitch']['is_department_search'])){
            $sql  = ' select ';
            $sql .= '       b.id                    as "MstSwitch__department_id" ';
            $sql .= '     , c.facility_name         as "MstSwitch__facility_name" ';
            $sql .= '     , b.department_name       as "MstSwitch__department_name"  ';
            $sql .= '   from ';
            $sql .= '     mst_fixed_counts as a  ';
            $sql .= '     left join mst_departments as b  ';
            $sql .= '       on b.id = a.mst_department_id  ';
            $sql .= '     left join mst_facilities as c  ';
            $sql .= '       on c.id = b.mst_facility_id  ';
            $sql .= '     inner join mst_user_belongings as d';
            $sql .= '       on d.mst_facility_id = c.id';
            $sql .= '      and d.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '      and d.is_deleted = false';
            $sql .= '   where ';
            $sql .= '     a.mst_item_unit_id = ' . $this->request->data['MstSwitch']['fromItemId'];
            $sql .= '     and c.facility_type = ' .  Configure::read('FacilityType.hospital');
            $sql .= '     and a.is_deleted = false ';
            $sql .= '     and a.substitute_unit_id is null '; //既に切替元として設定されている場合は無視
            if($this->request->data['MstSwitch']['facility_id'] != ""){
                $sql .= ' and c.id = ' . $this->request->data['MstSwitch']['facility_id'];
            }

            $sql .= ' order by c.facility_code , b.department_code';

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFixedCount'));
            if(isset($this->request->data['MstSwitch']['limit'])){
                $sql .= ' limit ' . $this->request->data['MstSwitch']['limit'];
            }else{
                $sql .= ' limit ' . min(array_keys(Configure::read('displaycounts_combobox')));
            }

            $SearchResult = $this->MstFixedCount->query($sql);
        }

        $this->set('SearchResult' , $SearchResult);
        $this->render('/switch/select_department');
        
    }

    /*
     * 切替先商品選択
     */
    function selectToItem(){
        App::import('Sanitize');
        //権限ロール設定
        $this->setRoleFunction(86);

        $SearchResult = array();
        //検索ボタン押下
        if(isset($this->request->data['MstSwitch']['is_to_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            //画面から入力された検索条件
            $where = '';
            if($this->request->data['MstSwitch']['internal_code'] != ''){
                $where .= " and a.internal_code like '" . $this->request->data['MstSwitch']['internal_code'] . "%'";
            }
            if($this->request->data['MstSwitch']['item_code'] != ''){
                $where .= " and a.item_code like '%" . $this->request->data['MstSwitch']['item_code'] . "%'";
            }
            if($this->request->data['MstSwitch']['item_name'] != ''){
                $where .= " and a.item_name like '%" . $this->request->data['MstSwitch']['item_name'] . "%'";
            }
            if($this->request->data['MstSwitch']['jan_code'] != ''){
                $where .= " and a.jan_code like '" . $this->request->data['MstSwitch']['jan_code'] . "%'";
            }
            if($this->request->data['MstSwitch']['dealer_name'] != ''){
                $where .= " and f.dealer_name like'%" . $this->request->data['MstSwitch']['dealer_name'] . "%'";
            }
            if($this->request->data['MstSwitch']['standard'] != ''){
                $where .= " and a.standard like '%" . $this->request->data['MstSwitch']['standard'] . "%'";
            }

            $sql  = ' select ';
            $sql .= '       b.id                     as "MstSwitch__toItemId" ';
            $sql .= '     , a.internal_code          as "MstSwitch__internal_code" ';
            $sql .= '     , a.item_name              as "MstSwitch__item_name" ';
            $sql .= '     , a.item_code              as "MstSwitch__item_code" ';
            $sql .= '     , a.standard               as "MstSwitch__standard" ';
            $sql .= '     , f.dealer_name            as "MstSwitch__dealer_name" ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when b.per_unit = 1  ';
            $sql .= '         then d.unit_name  ';
            $sql .= "         else d.unit_name || '(' || b.per_unit || e.unit_name || ')'  ";
            $sql .= '         end ';
            $sql .= '     )                          as "MstSwitch__unit_name"  ';
            $sql .= '   from ';
            $sql .= '     mst_facility_items as a  ';
            $sql .= '     left join mst_item_units as b  ';
            $sql .= '       on b.mst_facility_item_id = a.id  ';
            $sql .= '     left join mst_fixed_counts as c  ';
            $sql .= '       on c.mst_item_unit_id = b.id  ';
            $sql .= '     left join mst_unit_names as d  ';
            $sql .= '       on d.id = b.mst_unit_name_id  ';
            $sql .= '     left join mst_unit_names as e  ';
            $sql .= '       on e.id = b.per_unit_name_id  ';
            $sql .= '     left join mst_dealers as f  ';
            $sql .= '       on f.id = a.mst_dealer_id  ';
            $sql .= '   where ';
            $sql .= '     a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     and a.id <> (  ';
            $sql .= '       select ';
            $sql .= '             mst_facility_item_id  ';
            $sql .= '         from ';
            $sql .= '           mst_item_units  ';
            $sql .= '         where ';
            $sql .= '           id = ' . $this->request->data['MstSwitch']['fromItemId'];
            $sql .= '     )  ';
            $sql .= $where;
            $sql .= '   group by ';
            $sql .= '     b.id ';
            $sql .= '     , a.internal_code ';
            $sql .= '     , a.item_name ';
            $sql .= '     , a.item_code ';
            $sql .= '     , a.standard ';
            $sql .= '     , d.unit_name ';
            $sql .= '     , b.per_unit ';
            $sql .= '     , e.unit_name ';
            $sql .= '     , f.dealer_name ';
            $sql .= '   order by ';
            $sql .= '     a.internal_code ';
            $sql .= '     , b.per_unit ';

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFixedCount'));
            if(isset($this->request->data['MstSwitch']['limit'])){
                $sql .= ' limit ' . $this->request->data['MstSwitch']['limit'];
            }
            $SearchResult = $this->MstFixedCount->query($sql);
            $this->request->data = $data;
        }

        $this->set('SearchResult' , $SearchResult);
        $this->render('/switch/select_to_item');
        
    }

    /*
     * 確認画面
     */
    function confirm(){
        //権限ロール設定
        $this->setRoleFunction(86);

        //切替先商品情報を取得
        $result = $this->getConfirmData($this->request->data['MstSwitch']['department_id'] , $this->request->data['MstSwitch']['fromItemId'] , $this->request->data['MstSwitch']['toItemId']);
        $this->set('result' , $result);

        //切替元商品情報を取得
        $sql  = ' select ';
        $sql .= '       b.internal_code            as "MstSwitch__internal_code" ';
        $sql .= '     , b.item_name                as "MstSwitch__item_name" ';
        $sql .= '     , b.item_code                as "MstSwitch__item_code" ';
        $sql .= '     , b.standard                 as "MstSwitch__standard" ';
        $sql .= '     , e.dealer_name              as "MstSwitch__dealer_name" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || a.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                            as "MstSwitch__unit_name"  ';
        $sql .= '   from ';
        $sql .= '     mst_item_units as a  ';
        $sql .= '     left join mst_facility_items as b  ';
        $sql .= '       on b.id = a.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on c.id = a.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on d.id = a.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on e.id = b.mst_dealer_id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->request->data['MstSwitch']['fromItemId'];
        $tmp = $this->MstItemUnit->query($sql);

        $this->request->data['MstSwitch']['internal_code'] = $tmp[0]['MstSwitch']['internal_code'];
        $this->request->data['MstSwitch']['item_name']     = $tmp[0]['MstSwitch']['item_name'];
        $this->request->data['MstSwitch']['item_code']     = $tmp[0]['MstSwitch']['item_code'];
        $this->request->data['MstSwitch']['standard']      = $tmp[0]['MstSwitch']['standard'];
        $this->request->data['MstSwitch']['dealer_name']   = $tmp[0]['MstSwitch']['dealer_name'];
        $this->request->data['MstSwitch']['unit_name']     = $tmp[0]['MstSwitch']['unit_name'];

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('MstSwitch.token' , $token);
        $this->request->data['MstSwitch']['token'] = $token;
        $this->render('/switch/confirm');

    }

    /*
     * 完了画面
     */
    function result(){

        if($this->request->data['MstSwitch']['token'] === $this->Session->read('MstSwitch.token')) {
            $this->Session->delete('MstSwitch.token');

            $this->request->data['now'] = date('Y/m/d H:i:s.u');
            $switch_header_ids = array();
            //トランザクション開始
            $this->MstFixedCount->begin();
            $item_unit_id = $this->request->data['MstSwitch']['toItemId'];
            //登録処理
            foreach($this->request->data['MstSwitch']['id'] as $key => $department_id){
                //切替元定数設定を取得
                $fromFixedCount = $this->MstFixedCount->find('first', array(
                    'recursive' => -1,
                    'conditions'=> array(
                        'mst_item_unit_id'  => $this->request->data['MstSwitch']['fromItemId'] ,
                        'mst_department_id' => $department_id
                        )
                    )
                                                             );

                $from_fixed_count_id = $fromFixedCount['MstFixedCount']['id'];
                //切替先定数設定を確認
                $fixed_count_id = $this->checkFixedCount($department_id , $item_unit_id , true );
                $fixed_data = array();
                if($fixed_count_id) {
                    //定数設定レコードあり => 更新
                    $fixed_data['MstFixedCount']['id'] = $fixed_count_id;
                } else {
                    //定数設定レコードなし => 新規追加
                    //包装単位IDから、施設採用品ID、採用品IDを取得する。
                    $itemUnit = $this->MstItemUnit->find('first', array(
                        'recursive' => -1,
                        'conditions'=> array(
                            'id'  => $item_unit_id,
                            )
                        )
                     );

                    $fixed_data['MstFixedCount']['mst_facility_item_id'] = $itemUnit['MstItemUnit']['mst_facility_item_id'];
                    //部署IDから施設IDを取得する。
                    $fixed_data['MstFixedCount']['mst_facilities_id'] = $this->getFacilityId2DepartmentId($department_id);
                    $fixed_data['MstFixedCount']['mst_department_id'] = $department_id;

                    $fixed_data['MstFixedCount']['creater'] = $this->Session->read('Auth.MstUser.id');
                    $fixed_data['MstFixedCount']['created'] = $this->request->data['now'];
                }

                $fixed_data['MstFixedCount']['mst_item_unit_id']    = $item_unit_id;
                $fixed_data['MstFixedCount']['trn_stock_id']        = $this->getStockRecode($item_unit_id , $department_id );  //切替先定数の在庫IDを取得（なければ作る）
                $fixed_data['MstFixedCount']['order_point']         = $fromFixedCount['MstFixedCount']['order_point'];
                $fixed_data['MstFixedCount']['fixed_count']         = 0;  //切替先定数の初期は、0固定
                $fixed_data['MstFixedCount']['holiday_fixed_count'] = $fromFixedCount['MstFixedCount']['holiday_fixed_count'] - $fromFixedCount['MstFixedCount']['fixed_count'];
                $fixed_data['MstFixedCount']['spare_fixed_count']   = $fromFixedCount['MstFixedCount']['spare_fixed_count'] - $fromFixedCount['MstFixedCount']['fixed_count'];
                $fixed_data['MstFixedCount']['start_date']          = $fromFixedCount['MstFixedCount']['start_date'];
                $fixed_data['MstFixedCount']['trade_type']          = $fromFixedCount['MstFixedCount']['trade_type'];
                $fixed_data['MstFixedCount']['mst_shelf_name_id']   = $fromFixedCount['MstFixedCount']['mst_shelf_name_id'];
                $fixed_data['MstFixedCount']['is_deleted']          = false;
                $fixed_data['MstFixedCount']['modifier']            = $this->Session->read('Auth.MstUser.id');
                $fixed_data['MstFixedCount']['modified']            = $this->request->data['now'];

                $this->MstFixedCount->create();
                if(!$this->MstFixedCount->save($fixed_data)){
                    //定数設定更新失敗
                    $this->MstFixedCount->rollback();
                    //エラーメッセージ
                    $this->Session->setFlash('切替先定数の更新に失敗しました。', 'growl', array('type'=>'error') );
                    //リダイレクト
                    $this->redirect('/mst_switch/selectFromItem');
                    return false;
                }

                //切替元包装単位に切替先包装単位のIDを追加する
                $to_fixed_count_id = (isset($fixed_data['MstFixedCount']['id'])?$fixed_data['MstFixedCount']['id']:$this->MstFixedCount->getLastInsertId());
                $fromFixedCount['MstFixedCount']['substitute_unit_id'] = $to_fixed_count_id;
                $fromFixedCount['MstFixedCount']['holiday_fixed_count'] = $fromFixedCount['MstFixedCount']['fixed_count'];
                $fromFixedCount['MstFixedCount']['spare_fixed_count'] = $fromFixedCount['MstFixedCount']['fixed_count'];

                $this->MstFixedCount->create();
                if(!$this->MstFixedCount->save($fromFixedCount)){
                    //定数設定更新失敗
                    $this->MstFixedCount->rollback();
                    //エラーメッセージ
                    $this->Session->setFlash('切替元定数の更新に失敗しました。', 'growl', array('type'=>'error') );
                    //リダイレクト
                    $this->redirect('/mst_switch/selectFromItem');
                    return false;
                }

                //切替履歴ヘッダ
                $history_header = array();
                $history_header['TrnSwitchHistoryHeader']['fixed_count_id_from'] = $from_fixed_count_id;
                $history_header['TrnSwitchHistoryHeader']['fixed_count_id_to']   = $to_fixed_count_id;
                $history_header['TrnSwitchHistoryHeader']['item_unit_id_from']   = $this->request->data['MstSwitch']['fromItemId'];
                $history_header['TrnSwitchHistoryHeader']['item_unit_id_to']     = $item_unit_id;
                $history_header['TrnSwitchHistoryHeader']['start_date']          = $this->request->data['MstSwitch']['start_date'][$key];
                $history_header['TrnSwitchHistoryHeader']['center_stock_flg']    = (($this->request->data['MstSwitch']['center_stock'][$key]==1)?true:false);
                $history_header['TrnSwitchHistoryHeader']['is_deleted']          = false;
                $history_header['TrnSwitchHistoryHeader']['creater']             = $this->Session->read('Auth.MstUser.id');
                $history_header['TrnSwitchHistoryHeader']['created']             = $this->request->data['now'];
                $history_header['TrnSwitchHistoryHeader']['modifier']            = $this->Session->read('Auth.MstUser.id');
                $history_header['TrnSwitchHistoryHeader']['modified']            = $this->request->data['now'];

                $this->TrnSwitchHistoryHeader->create();
                if(!$this->TrnSwitchHistoryHeader->save($history_header)){
                    //定数設定更新失敗
                    $this->MstFixedCount->rollback();
                    //エラーメッセージ
                    $this->Session->setFlash('切替履歴ヘッダの登録に失敗しました。', 'growl', array('type'=>'error') );
                    //リダイレクト
                    $this->redirect('/mst_switch/selectFromItem');
                    return false;
                }

                $switch_header_ids[] = $this->TrnSwitchHistoryHeader->getLastInsertID();

                //切替履歴明細
                $history = array();

                $history['TrnSwitchHistory']['trn_switch_history_header_id'] = $this->TrnSwitchHistoryHeader->getLastInsertID();
                $history['TrnSwitchHistory']['fixed_count_from']             = $fromFixedCount['MstFixedCount']['fixed_count'];
                $history['TrnSwitchHistory']['fixed_count_to']               = 0;
                $history['TrnSwitchHistory']['type']                         = Configure::read('SwitchType.register'); //登録
                $history['TrnSwitchHistory']['is_deleted']                   = false;
                $history['TrnSwitchHistory']['creater']                      = $this->Session->read('Auth.MstUser.id');
                $history['TrnSwitchHistory']['created']                      = $this->request->data['now'];
                $history['TrnSwitchHistory']['modifier']                     = $this->Session->read('Auth.MstUser.id');
                $history['TrnSwitchHistory']['modified']                     = $this->request->data['now'];

                $this->TrnSwitchHistory->create();
                if(!$this->TrnSwitchHistory->save($history)){
                    //定数設定更新失敗
                    $this->MstFixedCount->rollback();
                    //エラーメッセージ
                    $this->Session->setFlash('切替履歴の登録に失敗しました。', 'growl', array('type'=>'error') );
                    //リダイレクト
                    $this->redirect('/mst_switch/selectFromItem');
                    return false;
                }
            }

            //コミット
            $this->MstFixedCount->commit();
            $result = $this->getDetailData($switch_header_ids);
            $this->Session->write('MstSwitch.result' , $result);
            //画面表示用データ取得
            $this->set('result' , $result);
        }else{
            $this->set('result' , $this->Session->read('MstSwitch.result'));
        }
        $this->render('/switch/result');
        
    }


    /*
     * 履歴画面
     */
    function history(){
        App::import('Sanitize');
        //権限ロール設定
        $this->setRoleFunction(3);

        //施設リスト
        $this->set('Facility_List' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                             array(Configure::read('FacilityType.hospital'))
                                                             ));

        $Department_List = array();
        if(isset($this->request->data['search']['facilities']) && $this->request->data['search']['facilities']!=""){
            //施設が選択されている場合、ひもづく部署を取得する。
            $Department_List = $this->getDepartmentsList($this->request->data['search']['facilities'],
                                                        false);
        }

        $SearchResult = array();

        //検索ボタン押下
        if(isset($this->request->data['MstSwitch']['is_search'])){
            $where = '';

            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            //施設
            if($this->request->data['search']['facilities'] !=''){
                $where .=' and h.facility_code like ' . "'%" . $this->request->data['search']['facilities'] ."%'";
            }
            //部署
            if($this->request->data['search']['departments'] !=''){
                $where .=' and g.department_code like ' . "'%" . $this->request->data['search']['departments'] ."%'";
            }
            //商品ID
            if($this->request->data['search']['internal_code'] !=''){
                $where .=' and ( d.internal_code like ' . "'%" . $this->request->data['search']['internal_code'] ."%'";
                $where .=' or d2.internal_code like ' . "'%" . $this->request->data['search']['internal_code'] ."%')";
            }

            //商品名
            if($this->request->data['search']['item_name'] !=''){
                $where .=' and ( d.item_name like ' . "'%" . $this->request->data['search']['item_name'] ."%'";
                $where .=' or d2.item_name like ' . "'%" . $this->request->data['search']['item_name'] ."%')";
            }

            //製品番号
            if($this->request->data['search']['item_code'] !=''){
                $where .=' and ( d.item_code like ' . "'%" . $this->request->data['search']['item_code'] ."%'";
                $where .=' or d2.item_code like ' . "'%" . $this->request->data['search']['item_code'] ."%')";
            }

            //規格
            if($this->request->data['search']['standard'] !=''){
                $where .=' and ( d.standard like ' . "'%" . $this->request->data['search']['standard'] ."%'";
                $where .=' or d2.standard like ' . "'%" . $this->request->data['search']['standard'] ."%')";
            }

            //販売元
            if($this->request->data['search']['dealer_name'] !=''){
                $where .=' and ( j.dealer_name like ' . "'%" . $this->request->data['search']['dealer_name'] ."%'";
                $where .=' or j2.dealer_name like ' . "'%" . $this->request->data['search']['dealer_name'] ."%')";
            }

            //取消は表示しない
            if(isset($this->request->data['search']['is_deleted'])){
                $where .= ' and a.is_deleted = false  ';
            }

            //完了は表示しない
            if(isset($this->request->data['search']['is_end'])){
                $where .= ' and a.end_date is null ';
            }
            $SearchResult = $this->getHistoryData($where , $this->request->data['Switch']['limit']);
            $this->request->data = $data;
        }else{
            //初回チェックボックスの初期チェック
            $this->request->data['search']['is_deleted'] = true;
            $this->request->data['search']['is_end'] = true;
        }
        $this->set('SearchResult' , $SearchResult);
        $this->set('Department_List' , $Department_List);

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('MstSwitch.token' , $token);
        $this->request->data['MstSwitch']['token'] = $token;

        $this->render('/switch/history');
    }

    /*
     * 履歴明細画面
     */
    function detail(){
        //権限ロール設定
        $this->setRoleFunction(3);

        $this->set('SearchResult' , $this->getDetailData($this->request->data['MstSwitch']['id']));
        $this->render('/switch/detail');
    }

    /*
     * 取消完了画面
     */
    function cancel(){
        if($this->request->data['MstSwitch']['token'] === $this->Session->read('MstSwitch.token')) {
            $this->Session->delete('MstSwitch.token');

            $SearchResult = array();

            //トランザクション開始
            $this->MstFixedCount->begin();
            if(!$this->cancel_register()){
                //取消し失敗

                //ロールバック
                $this->MstFixedCount->rollback();

                //エラーメッセージ
                $this->Session->setFlash('定数切替の取消しに失敗しました。', 'growl', array('type'=>'error') );

                //リダイレクト
                $this->redirect('/mst_switch/history');
            }

            //コミット
            $this->MstFixedCount->commit();

            //結果表示用データを取得
            $where = ' and a.id in ( ' . join(',',$this->request->data['MstSwitch']['id']) . ')';
            $SearchResult = $this->getHistoryData($where);
            $this->set('SearchResult' , $SearchResult);
            $this->Session->write('MstSwitch.SearchResult' , $SearchResult);
        }else{
            $this->set('SearchResult' , $this->Session->read('MstSwitch.SearchResult'));
        }
        
        $this->render('/switch/cancel');
        
    }

    /*
     * 取消処理メイン
     */

    private function cancel_register(){
        $now = date('Y/m/d H:i:s.u');

        foreach($this->request->data['MstSwitch']['id'] as $id ){
            //履歴ヘッダ
            if (!$this->TrnSwitchHistoryHeader->updateAll(array(  'TrnSwitchHistoryHeader.is_deleted'  => 'true',
                                                                  'TrnSwitchHistoryHeader.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                                  'TrnSwitchHistoryHeader.modified'    => "'" . $now . "'"
                                                                  )
                                                          ,
                                                          array(
                                                              'TrnSwitchHistoryHeader.id'  => $id,
                                                              )
                                                          ,
                                                          -1
                                                          )
                ) {
                return false;
            }


            //履歴明細
            if (!$this->TrnSwitchHistory->updateAll(array(  'TrnSwitchHistory.is_deleted'  => 'true',
                                                            'TrnSwitchHistory.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                            'TrnSwitchHistory.modified'    => "'" . $now . "'"
                                                            )
                                                    ,
                                                    array(
                                                        'TrnSwitchHistory.trn_switch_history_header_id'  => $id,
                                                        )
                                                    ,
                                                    -1
                                                    )
                ) {
                return false;
            }

            $switch = $this->TrnSwitchHistoryHeader->find('first', array(
                'recursive' => -1,
                'conditions'=> array(
                    'id' => $id
                    )
                )
            );


            //定数設定(切替元)
            if (!$this->MstFixedCount->updateAll(array(  'MstFixedCount.substitute_unit_id' => null ,
                                                         'MstFixedCount.modifier'           => $this->Session->read('Auth.MstUser.id'),
                                                         'MstFixedCount.modified'           => "'" . $now . "'"
                                                         )
                                                 ,
                                                 array(
                                                     'MstFixedCount.id'  => $switch['TrnSwitchHistoryHeader']['fixed_count_id_from'],
                                                     )
                                                 ,
                                                 -1
                                                 )
                ) {
                return false;
            }
            //定数設定(切替先)
            if (!$this->MstFixedCount->updateAll(array(  'MstFixedCount.is_deleted'         => 'true',
                                                         'MstFixedCount.modifier'           => $this->Session->read('Auth.MstUser.id'),
                                                         'MstFixedCount.modified'           => "'" . $now . "'"
                                                         )
                                                 ,
                                                 array(
                                                     'MstFixedCount.id'  => $switch['TrnSwitchHistoryHeader']['fixed_count_id_to'],
                                                     )
                                                 ,
                                                 -1
                                                 )
                ) {
                return false;
            }


        }

        return true;
    }

    /*
     * 部署、包装単位を受けて定数設定のIDを返す
     * param $department_id : 部署ID
     * param $item_unit_id  : 包装単位ID
     * param $flg           : 商品が同じで、包装単位違いの場合もIDを返すか？
     */
    private function checkFixedCount($department_id , $item_unit_id , $flg = false ){
        $sql  = ' select ';
        $sql .= '       a.id  ';
        $sql .= '   from ';
        $sql .= '     mst_fixed_counts as a  ';
        $sql .= '     left join mst_item_units as b  ';
        if($flg){
            $sql .= '       on b.mst_facility_item_id = a.mst_facility_item_id  ';
        }else{
            $this->MstFixedCount->rollback();            $sql .= '       on b.id = a.mst_item_unit_id ';
        }
        $sql .= '   where ';
        $sql .= '     a.mst_department_id = ' . $department_id;
        $sql .= '     and b.id = ' . $item_unit_id;
        $ret = $this->MstFixedCount->query($sql);
        return (isset($ret[0][0]['id'])?$ret[0][0]['id']:false);
    }

    /*
     * 部署IDから施設IDを返す
     */
    private function getFacilityId2DepartmentId($department_id){
        $res = $this->MstDepartment->find('first', array(
            'conditions' => array(
                'id' => $department_id,
                ),
            'recursive' => -1,
            ));
        return $res['MstDepartment']['mst_facility_id'];
    }

    /*
     * 確認画面表示データ取得
     */
    function getConfirmData( $department_ids , $fromitem_id , $toitem_id ){
        $sql  = 'select ';
        $sql .= '      a.id                       as "MstSwitch__id" ';
        $sql .= '    , b.facility_name            as "MstSwitch__facility_name" ';
        $sql .= '    , a.department_name          as "MstSwitch__department_name" ';
        $sql .= '    , d.internal_code            as "MstSwitch__internal_code" ';
        $sql .= '    , d.item_name                as "MstSwitch__item_name" ';
        $sql .= '    , d.item_code                as "MstSwitch__item_code" ';
        $sql .= '    , d.standard                 as "MstSwitch__standard" ';
        $sql .= '    , e.dealer_name              as "MstSwitch__dealer_name" ';
        $sql .= '    , (  ';
        $sql .= '      case  ';
        $sql .= '        when c.per_unit = 1  ';
        $sql .= '        then f.unit_name  ';
        $sql .= "        else f.unit_name || '(' || c.per_unit || g.unit_name || ')'  ";
        $sql .= '        end ';
        $sql .= '    )                            as "MstSwitch__unit_name" ';
        $sql .= '    , ( ';
        $sql .= '      case ';
        $sql .= "        when h.id is not null then '定数設定済みのため登録できません' ";
        $sql .= "        when i.id is not null then '他包装単位にて定数設定済みのため登録できません' ";
        $sql .= "        else '' ";
        $sql .= '        end ';
        $sql .= '    )                            as "MstSwitch__err"  ';
        $sql .= '  from ';
        $sql .= '    mst_departments as a  ';
        $sql .= '    left join mst_facilities as b  ';
        $sql .= '      on b.id = a.mst_facility_id  ';
        $sql .= '    cross join mst_item_units as c  ';
        $sql .= '    left join mst_facility_items as d  ';
        $sql .= '      on d.id = c.mst_facility_item_id  ';
        $sql .= '    left join mst_dealers as e  ';
        $sql .= '      on e.id = d.mst_dealer_id  ';
        $sql .= '    left join mst_unit_names as f  ';
        $sql .= '      on f.id = c.mst_unit_name_id  ';
        $sql .= '    left join mst_unit_names as g  ';
        $sql .= '      on g.id = c.per_unit_name_id  ';

        $sql .= '    left join mst_fixed_counts as h  ';
        $sql .= '      on h.mst_item_unit_id = ' . $toitem_id;
        $sql .= '      and h.mst_department_id = a.id  ';
        $sql .= '      and h.is_deleted = false  ';

        $sql .= '    left join (  ';
        $sql .= '      select ';
        $sql .= '            a.mst_department_id    as id  ';
        $sql .= '        from ';
        $sql .= '          mst_fixed_counts as a  ';
        $sql .= '          left join mst_item_units as b  ';
        $sql .= '            on b.mst_facility_item_id = a.mst_facility_item_id  ';
        $sql .= '        where ';
        $sql .= '          b.id = ' . $toitem_id;
        $sql .= '          and a.is_deleted = false ';
        $sql .= '    ) as i  ';
        $sql .= '      on i.id = a.id  ';

        $sql .= '  where ';
        $sql .= '    a.id in (' .join(',' ,$department_ids ). ')  ';
        $sql .= '    and c.id =' . $toitem_id;
        $sql .= '  order by b.facility_code , a.department_code ';

        return $this->MstItemUnit->query($sql);
    }


    /*
     * 履歴データ取得
     */
    function getHistoryData($where = "" , $limit = null ){
        $sql  = ' select ';
        $sql .= '       a.id                                  as "MstSwitch__id" ';
        $sql .= '     , h.facility_name                       as "MstSwitch__facility_name" ';
        $sql .= '     , g.department_name                     as "MstSwitch__department_name" ';
        
        $sql .= '     , d.internal_code                       as "MstSwitch__from_internal_code" ';
        $sql .= '     , d.item_name                           as "MstSwitch__from_item_name" ';
        $sql .= '     , d.standard                            as "MstSwitch__from_standard" ';
        $sql .= '     , d.item_code                           as "MstSwitch__from_item_code" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when c.per_unit = 1  ';
        $sql .= '         then e.unit_name  ';
        $sql .= "         else e.unit_name || '(' || c.per_unit || f.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                       as "MstSwitch__from_unit_name" ';
        $sql .= '     , d2.internal_code                      as "MstSwitch__to_internal_code" ';
        $sql .= '     , d2.item_name                          as "MstSwitch__to_item_name" ';
        $sql .= '     , d2.standard                           as "MstSwitch__to_standard" ';
        $sql .= '     , d2.item_code                          as "MstSwitch__to_item_code" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when c2.per_unit = 1  ';
        $sql .= '         then e2.unit_name  ';
        $sql .= "         else e2.unit_name || '(' || c2.per_unit || f2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                       as "MstSwitch__to_unit_name" ';
        $sql .= '     , i.user_name                           as "MstSwitch__user_name" ';
        $sql .= "     , ( case when a.center_stock_flg = true then '計算に含める' else '計算に含めない'  end ) ";
        $sql .= '                                             as "MstSwitch__center_stock" ';
        $sql .= '     , to_char(a.start_date ,\'YYYY/mm/dd\') as "MstSwitch__start_date" ';
        $sql .= '     , to_char(a.end_date , \'YYYY/mm/dd\')  as "MstSwitch__end_date"  ';
        $sql .= '     , ( case ';
        $sql .= '         when a.is_deleted = true then 0';
        $sql .= '         when a.end_date is not null then 0';
        $sql .= '         else 1 ';
        $sql .= '         end )                               as "MstSwitch__status"';
        $sql .= '   from ';
        $sql .= '     trn_switch_history_headers as a  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on a.item_unit_id_from = c.id  ';
        $sql .= '     left join mst_facility_items as d  ';
        $sql .= '       on d.id = c.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as e  ';
        $sql .= '       on e.id = c.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as f  ';
        $sql .= '       on f.id = c.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as j  ';
        $sql .= '       on j.id = d.mst_dealer_id  ';
        $sql .= '     left join mst_item_units as c2  ';
        $sql .= '       on a.item_unit_id_to = c2.id  ';
        $sql .= '     left join mst_facility_items as d2  ';
        $sql .= '       on d2.id = c2.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as e2  ';
        $sql .= '       on e2.id = c2.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as f2  ';
        $sql .= '       on f2.id = c2.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as j2  ';
        $sql .= '       on j2.id = d2.mst_dealer_id  ';
        $sql .= '     left join mst_fixed_counts as b  ';
        $sql .= '       on b.id = a.fixed_count_id_from  ';
        $sql .= '     left join mst_departments as g  ';
        $sql .= '       on g.id = b.mst_department_id  ';
        $sql .= '     left join mst_facilities as h  ';
        $sql .= '       on h.id = g.mst_facility_id  ';
        $sql .= '     left join mst_users as i  ';
        $sql .= '       on i.id = a.creater ';
        $sql .= '     inner join mst_user_belongings as k ';
        $sql .= '       on k.mst_facility_id = h.id ';
        $sql .= '      and k.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '      and k.is_deleted = false ';
        $sql .= '    inner join mst_facility_relations as x ';
        $sql .= '      on x.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and x.partner_facility_id = h.id ';

        $sql .= ' where 1=1 ';
        $sql .= $where;

        //全件取得
        $this->set('max' , $this->getMaxCount($sql , 'TrnSwitchHistoryHeader'));
        if(!is_null($limit)){
            $sql .= ' limit ' . $limit;
        }

        return $this->TrnSwitchHistoryHeader->query($sql);
    }


    /*
     * 明細表示/登録結果データ
     */
    function getDetailData( $MstSwitchHeaderId = array()){
        $sql  = ' select ';
        $sql .= '       i.facility_name                    as "MstSwitch__facility_name" ';
        $sql .= '     , h.department_name                  as "MstSwitch__department_name" ';
        $sql .= '     , ( case a.type ';
        foreach(Configure::read('SwitchType.list') as $k => $v ){
            $sql .= " when  ${k} then  '${v}' ";
        }
        $sql .= '       end )                              as "MstSwitch__type"';
        $sql .= '     , e1.internal_code                   as "MstSwitch__from_internal_code" ';
        $sql .= '     , e1.item_name                       as "MstSwitch__from_item_name" ';
        $sql .= '     , e1.standard                        as "MstSwitch__from_standard" ';
        $sql .= '     , e1.item_code                       as "MstSwitch__from_item_code" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when d1.per_unit = 1  ';
        $sql .= '         then f1.unit_name  ';
        $sql .= "         else f1.unit_name || '(' || d1.per_unit || g1.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                    as "MstSwitch__from_unit_name" ';
        $sql .= '     , e2.internal_code                   as "MstSwitch__to_internal_code" ';
        $sql .= '     , e2.item_name                       as "MstSwitch__to_item_name" ';
        $sql .= '     , e2.item_code                       as "MstSwitch__to_item_code" ';
        $sql .= '     , e2.standard                        as "MstSwitch__to_standard" ';
        $sql .= '     , h2.dealer_name                     as "MstSwitch__to_dealer_name" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when d2.per_unit = 1  ';
        $sql .= '         then f2.unit_name  ';
        $sql .= "         else f2.unit_name || '(' || d2.per_unit || g2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                    as "MstSwitch__to_unit_name" ';
        $sql .= '     , a.fixed_count_from                 as "MstSwitch__fixed_count_from" ';
        $sql .= '     , a.fixed_count_to                   as "MstSwitch__fixed_count_to" ';
        $sql .= '     , j.user_name                        as "MstSwitch__user_name" ';
        $sql .= "     , ( case when b.center_stock_flg = true then '計算に含める' else '計算に含めない' end ) ";
        $sql .= '                                          as "MstSwitch__center_stock"';
        $sql .= "     , to_char(a.created , 'YYYY/mm/dd hh24:mi:ss') ";
        $sql .= '                                          as "MstSwitch__created"  ';
        $sql .= "     , to_char(l.start_date ,'YYYY/mm/dd') ";
        $sql .= '                                          as "MstSwitch__start_date"  ';
        $sql .= '     , k.work_no                          as "MstSwitch__work_no"  ';
        $sql .= '   from ';
        $sql .= '     trn_switch_histories as a  ';
        $sql .= '     left join trn_switch_history_headers as b  ';
        $sql .= '       on b.id = a.trn_switch_history_header_id  ';
        $sql .= '     left join mst_fixed_counts as c1  ';
        $sql .= '       on c1.id = b.fixed_count_id_from  ';
        $sql .= '     left join mst_item_units as d1  ';
        $sql .= '       on d1.id = c1.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as e1  ';
        $sql .= '       on e1.id = d1.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as f1  ';
        $sql .= '       on f1.id = d1.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as g1  ';
        $sql .= '       on g1.id = d1.per_unit_name_id  ';
        $sql .= '     left join mst_fixed_counts as c2  ';
        $sql .= '       on c2.id = b.fixed_count_id_to  ';
        $sql .= '     left join mst_item_units as d2  ';
        $sql .= '       on d2.id = c2.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as e2  ';
        $sql .= '       on e2.id = d2.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as f2  ';
        $sql .= '       on f2.id = d2.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as g2  ';
        $sql .= '       on g2.id = d2.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as h2  ';
        $sql .= '       on h2.id = e2.mst_dealer_id  ';
        $sql .= '     left join mst_departments as h  ';
        $sql .= '       on h.id = c1.mst_department_id  ';
        $sql .= '     left join mst_facilities as i  ';
        $sql .= '       on i.id = h.mst_facility_id  ';
        $sql .= '     left join mst_users as j  ';
        $sql .= '       on j.id = a.creater  ';
        $sql .= '     left join trn_consumes as k ';
        $sql .= '       on k.id = a.trn_consume_id ';
        $sql .= '     left join trn_switch_history_headers as l ';
        $sql .= '       on l.id = a.trn_switch_history_header_id ';

        $sql .= '   where ';
        $sql .= '     a.trn_switch_history_header_id in ( ' . join(',', $MstSwitchHeaderId ) . ')';
        $sql .= '   order by b.id ,a.id , a.created ';
        return $this->TrnSwitchHistory->query($sql);

    }

    /**
     * 定数切替履歴CSV出力
     */
    function export_csv() {
        $where = '';

        $data = $this->request->data;
        $this->request->data = Sanitize::clean($this->request->data);

        //施設
        if($this->request->data['search']['facility_name'] !=''){
            $where .=' and i.facility_name like ' . "'%" . $this->request->data['search']['facility_name'] ."%'";
        }
        //部署
        if($this->request->data['search']['department_name'] !=''){
            $where .=' and h.department_name like ' . "'%" . $this->request->data['search']['department_name'] ."%'";
        }
        //商品ID
        if($this->request->data['search']['internal_code'] !=''){
            $where .=' and ( e1.internal_code like ' . "'%" . $this->request->data['search']['internal_code'] ."%'";
            $where .=' or e2.internal_code like ' . "'%" . $this->request->data['search']['internal_code'] ."%')";
        }

        //商品名
        if($this->request->data['search']['item_name'] !=''){
            $where .=' and ( e1.item_name like ' . "'%" . $this->request->data['search']['item_name'] ."%'";
            $where .=' or e2.item_name like ' . "'%" . $this->request->data['search']['item_name'] ."%')";
        }

        //製品番号
        if($this->request->data['search']['item_code'] !=''){
            $where .=' and ( e1.item_code like ' . "'%" . $this->request->data['search']['item_code'] ."%'";
            $where .=' or e2.item_code like ' . "'%" . $this->request->data['search']['item_code'] ."%')";
        }

        //規格
        if($this->request->data['search']['standard'] !=''){
            $where .=' and ( e1.standard like ' . "'%" . $this->request->data['search']['standard'] ."%'";
            $where .=' or e2.standard like ' . "'%" . $this->request->data['search']['standard'] ."%')";
        }

        //販売元
        if($this->request->data['search']['dealer_name'] !=''){
            $where .=' and ( h1.dealer_name like ' . "'%" . $this->request->data['search']['dealer_name'] ."%'";
            $where .=' or h2.dealer_name like ' . "'%" . $this->request->data['search']['dealer_name'] ."%')";
        }

        //取消は表示しない
        if(isset($this->request->data['search']['is_deleted'])){
            $where .= ' and b.is_deleted = false  ';
        }

        //完了は表示しない
        if(isset($this->request->data['search']['is_end'])){
            $where .= ' and b.end_date is null ';
        }

        $sql = $this->getCSVData($where);
        $this->db_export_csv($sql , "定数切替履歴", 'history');
    }


    /**
     * CSVデータ取得SQL
     */
    function getCSVData( $where){
        $sql  = ' select ';
        $sql .= '       i.facility_name                       as 施設名 ';
        $sql .= '     , h.department_name                     as 部署名 ';
        $sql .= '     , k.work_no                             as 消費番号  ';
        $sql .= '     , ( case a.type ';
        foreach(Configure::read('SwitchType.list') as $k => $v ){
            $sql .= " when  ${k} then  '${v}' ";
        }
        $sql .= '       end )                                 as 区分';
        $sql .= "     , ( case when b.center_stock_flg = true then '計算に含める' else '計算に含めない' end ) ";
        $sql .= '                                             as センター在庫';
        $sql .= '     , e1.internal_code                      as "切替元商品ID" ';
        $sql .= '     , e1.item_name                          as 切替元商品名 ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when d1.per_unit = 1  ';
        $sql .= '         then f1.unit_name  ';
        $sql .= "         else f1.unit_name || '(' || d1.per_unit || g1.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                       as 切替元包装単位名 ';
        $sql .= '     , a.fixed_count_from                    as 切替元商品定数 ';
        $sql .= '     , e2.internal_code                      as "切替先商品ID" ';
        $sql .= '     , e2.item_name                          as 切替先商品名 ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when d2.per_unit = 1  ';
        $sql .= '         then f2.unit_name  ';
        $sql .= "         else f2.unit_name || '(' || d2.per_unit || g2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                       as 切替先包装単位名 ';
        $sql .= '     , a.fixed_count_to                      as 切替先商品定数 ';
        $sql .= '     , to_char(b.start_date ,\'YYYY/mm/dd\') as 開始日 ';
        $sql .= '     , to_char(b.end_date , \'YYYY/mm/dd\')  as 終了日  ';
        $sql .= '     , j.user_name                           as 登録者名 ';
        $sql .= "     , to_char(b.created , 'YYYY/mm/dd hh24:mi:ss') ";
        $sql .= '                                             as 登録日時  ';
        $sql .= '   from ';
        $sql .= '     trn_switch_histories as a  ';
        $sql .= '     left join trn_switch_history_headers as b  ';
        $sql .= '       on b.id = a.trn_switch_history_header_id  ';
        $sql .= '     left join mst_fixed_counts as c1  ';
        $sql .= '       on c1.id = b.fixed_count_id_from  ';
        $sql .= '     left join mst_item_units as d1  ';
        $sql .= '       on d1.id = c1.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as e1  ';
        $sql .= '       on e1.id = d1.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as h1  ';
        $sql .= '       on h1.id = e1.mst_dealer_id  ';
        $sql .= '     left join mst_unit_names as f1  ';
        $sql .= '       on f1.id = d1.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as g1  ';
        $sql .= '       on g1.id = d1.per_unit_name_id  ';
        $sql .= '     left join mst_fixed_counts as c2  ';
        $sql .= '       on c2.id = b.fixed_count_id_to  ';
        $sql .= '     left join mst_item_units as d2  ';
        $sql .= '       on d2.id = c2.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as e2  ';
        $sql .= '       on e2.id = d2.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as f2  ';
        $sql .= '       on f2.id = d2.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as g2  ';
        $sql .= '       on g2.id = d2.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as h2  ';
        $sql .= '       on h2.id = e2.mst_dealer_id  ';
        $sql .= '     left join mst_departments as h  ';
        $sql .= '       on h.id = c1.mst_department_id  ';
        $sql .= '     left join mst_facilities as i  ';
        $sql .= '       on i.id = h.mst_facility_id  ';
        $sql .= '     left join mst_users as j  ';
        $sql .= '       on j.id = a.creater  ';
        $sql .= '     left join trn_consumes as k ';
        $sql .= '       on k.id = a.trn_consume_id ';
        $sql .= '     left join trn_switch_history_headers as l ';
        $sql .= '       on l.id = a.trn_switch_history_header_id ';

        $sql .= ' where 1=1 ';
        $sql .= $where;

        $sql .= '   order by b.id ,a.id , a.created ';
        return $sql;

    }
}