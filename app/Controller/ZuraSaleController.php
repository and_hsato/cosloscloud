<?php
/**
 * ZuraSearchController
 * サイトずらし用売上検索
 * @version 1.0.0
 * @since 2010/05/18
 */
class ZuraSaleController extends AppController {

    /**
     * @var $name
     */
    var $name = 'ZuraSale';
    /**
     * @var array $uses
     */
    var $uses = array(
            'TrnClaim',
            'TrnClaimSpecifications',
            'MstFacility',
            'MstAccountlist',
            'MstCredit',
            'MstDepartment'
    );

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');
    /**
     * @var array $components
     */
    var $components = array('RequestHandler', 'DateUtil');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
    }

    public function index() {
        $this->setRoleFunction(61); //売上検索
        $this->redirect('search');
    }

    /* 支払いサイト延伸［ZuraSale］ */
    public function search() {
      //共通情報　取得
      $facility_id = $this->search_selectedFacility();
      $purchaseDate = $this->get_purchaseDate();
      $paymentDueDate = date("Y-m-t", strtotime($purchaseDate." 1 Month"));

      $extensionHopingMonth = Array(
        "1"=>"1",
        "2"=>"2",
        "3"=>"3",
        "4"=>"4",
        "5"=>"5",
        "6"=>"6"
      );
      //共通料金情報　取得
      $commission_rate = $this->search_zura_commission_rate($facility_id);
      $purchase_items = $this->search_purchase_items($facility_id, $purchaseDate);
      $upperInformation = $this->search_extensionUpperInformation($facility_id, $purchaseDate);

      $extensionUpperLimit = 0;
      $extension_price = 0;
      if(isset($upperInformation)){
        $maximum_amount = $upperInformation['MstFacility']['zura_maximum_amount'];
        $extension_price = $upperInformation['TrnClaim']['extension_price'];
        $extensionUpperLimit = $maximum_amount <= $extension_price ? $maximum_amount : $maximum_amount - $extension_price;
      }

      $this->set_search_PriceList($facility_id, $purchaseDate, $paymentDueDate, $extensionUpperLimit);
      $this->set('purchase_year', date("Y", strtotime($purchaseDate."")));
      $this->set('purchase_month', date("m", strtotime($purchaseDate."")));
      $this->set('purchase_day', date("t", strtotime($purchaseDate."")));
      $this->set('commission_rate', $commission_rate);
      $this->set('extensionUpperLimit', $extensionUpperLimit);
      $this->set('purchase_items', $purchase_items);
      $this->set('extension_price', $extension_price);
      $this->set('extensionHopingMonth', $extensionHopingMonth);
    }

    /* 支払いサイト延伸内容詳細 */
    public function confirm() {
      //POST　取得
      $extensionHopingPrice = isset($this->request->data['extensionHopingPrice']) ? $this->request->data['extensionHopingPrice'] : null;
      $selectedExtensionHopingMonth = isset($this->request->data['selectedExtensionHopingMonth']) ? $this->request->data['selectedExtensionHopingMonth'] : null;
      //共通情報　取得
      $facility_id = $this->search_selectedFacility();
      $purchaseDate = $this->get_purchaseDate();
      $paymentDueDate = date("Y-m-t", strtotime($purchaseDate." 1 Month"));
      //共通料金情報　取得
      $commission_rate = $this->search_zura_commission_rate($facility_id);
      $extensionUpperLimit = $this->search_extensionUpperLimit($facility_id, $purchaseDate);
      $paymentAmout = $this->search_paymentAmout($facility_id, $purchaseDate);

      $default_extensionUpperLimit = $extensionUpperLimit;

      if (   isset($extensionHopingPrice) && empty($extensionHopingPrice) == false
          && isset($selectedExtensionHopingMonth) && empty($selectedExtensionHopingMonth) == false){
        //前画面から延伸希望手数料、延伸希望期間を受け取っている場合は、延伸対象の商品を計算する
        $this->set_confirm_DisplayItems_($selectedExtensionHopingMonth);
      }else{
        //前画面から延伸希望手数料、延伸希望期間を受け取っていない
        $this->set_confirm_DisplayItems($facility_id, $purchaseDate, $paymentDueDate);
      }

      $this->set('commission_rate', $commission_rate);
      $this->set('extensionUpperLimit', $extensionUpperLimit);
      $this->set('paymentAmout', $paymentAmout);
      $this->set('default_extensionUpperLimit', $default_extensionUpperLimit);
    }

    /* 支払いサイト延伸完了 */
    public function result(){
      //延伸情報　DB更新
      $now = date('Y/m/d H:i:s');
      $ids = $this->request->data['id'];
      $months = $this->request->data['month'];

      $this->update_trnClaim($ids, $months, $now);

      //更新後、画面表示用データ取得
      $facility_id = $this->search_selectedFacility();
      $purchaseDate = $this->get_purchaseDate();
      $paymentDueDate = date("Y-m-t", strtotime($purchaseDate." 1 Month"));

      $claims_calculation = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

      $this->set('totalExtensionPrice', $claims_calculation['ClaimCalculation']['present_month_extension_price']);
      $this->set('totalCommission', $claims_calculation['ClaimCalculation']['present_month_commission']);
      $this->set('totalPrice', $claims_calculation['ClaimCalculation']['present_month_not_extension_price'] + $claims_calculation['ClaimCalculation']['present_month_commission']);
      $this->set('present_year', date("Y", strtotime($purchaseDate."")));
      $this->set('present_month', date("m", strtotime($purchaseDate."")));
      $this->set('present_day', date("t", strtotime($purchaseDate."")));
    }

    /* 請求書発行 */
    public function report(){
      $facility_id = $this->search_selectedFacility();

      $selectedDateList = $this->search_report_DateList($facility_id);

      $this->set('selectedDateList', $selectedDateList);
    }

    /* 支払いサイト延伸［ZuraSale］　料金項目　セット */
    function set_search_PriceList($facility_id, $purchaseDate, $paymentDueDate, $extensionUpperLimit){
      $this->set_search_present_month_PriceList($facility_id, date('Y-m-t', strtotime($paymentDueDate)), $extensionUpperLimit);
      $this->set_search_1month_later_PriceList($facility_id, date('Y-m-d', strtotime($purchaseDate." 1 month")), date('Y-m-t', strtotime($purchaseDate." 2 month")));
      $this->set_search_2month_later_PriceList($facility_id, date('Y-m-d', strtotime($purchaseDate." 2 month")), date('Y-m-t', strtotime($purchaseDate." 3 month")));
      $this->set_search_3month_later_PriceList($facility_id, date('Y-m-d', strtotime($purchaseDate." 3 month")), date('Y-m-t', strtotime($purchaseDate." 4 month")));
      $this->set_search_4month_later_PriceList($facility_id, date('Y-m-d', strtotime($purchaseDate." 4 month")), date('Y-m-t', strtotime($purchaseDate." 5 month")));
      $this->set_search_5month_later_PriceList($facility_id, date('Y-m-d', strtotime($purchaseDate." 5 month")), date('Y-m-t', strtotime($purchaseDate." 6 month")));
      $this->set_search_6month_later_PriceList($facility_id, date('Y-m-d', strtotime($purchaseDate." 6 month")), date('Y-m-t', strtotime($purchaseDate." 7 month")));
    }

    /* 支払いサイト延伸［ZuraSale］　当月　料金項目　セット */
    function set_search_present_month_PriceList($facility_id, $paymentDueDate, $extensionUpperLimit){
      $result = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);

      $price            = isset($result) === false || count($result) === 0 ? 0 : $result['ClaimCalculation']['present_month_not_extension_price'] + $result['ClaimCalculation']['all_month_before_price'];
      $commission       = isset($result) === false || count($result) === 0 ? 0 : $result['ClaimCalculation']['present_month_commission'];
      $sum_price        = $price + $commission;
      $totalPrice       = isset($result) === false || count($result) === 0 ? 0 : $result['ClaimCalculation']['present_month_price'];
      $amount_available = $extensionUpperLimit < $result['ClaimCalculation']['present_month_not_extension_price'] ? $extensionUpperLimit : $result['ClaimCalculation']['present_month_not_extension_price'];

      $this->set('present_year', date("Y", strtotime($paymentDueDate."")));
      $this->set('present_month', date("m", strtotime($paymentDueDate."")));
      $this->set('present_day', date("t", strtotime($paymentDueDate."")));
      $this->set('present_month_price', $price);
      $this->set('present_month_commission', $commission);
      $this->set('present_month_sum_price', $sum_price);
      $this->set('present_month_totalPrice', $totalPrice);
      $this->set('amount_available', $amount_available);
    }

    /* 支払いサイト延伸［ZuraSale］　１ヶ月後　料金項目　セット */
    function set_search_1month_later_PriceList($facility_id, $purchaseDate, $paymentDueDate){
      $result = $this->search_extensionInformation($facility_id, $purchaseDate);

      $price      = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];
      $commission = 0;
      $sum_price  = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];

      var_dump($purchaseDate);
      var_dump($paymentDueDate);

      $this->set('_1month_later_year', date("Y", strtotime($paymentDueDate)));
      $this->set('_1month_later_month', date("m", strtotime($paymentDueDate)));
      $this->set('_1month_later_price', $price);
      $this->set('_1month_later_commission', $commission);
      $this->set('_1month_later_sum_price', $sum_price);
    }

    /* 支払いサイト延伸［ZuraSale］　２ヶ月後　料金項目　セット */
    function set_search_2month_later_PriceList($facility_id, $purchaseDate, $paymentDueDate){
      $result = $this->search_extensionInformation($facility_id, $purchaseDate);

      $price      = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];
      $commission = 0;
      $sum_price  = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];

      $this->set('_2month_later_year', date("Y", strtotime($paymentDueDate)));
      $this->set('_2month_later_month', date("m", strtotime($paymentDueDate)));
      $this->set('_2month_later_price', $price);
      $this->set('_2month_later_commission', $commission);
      $this->set('_2month_later_sum_price', $sum_price);
    }

    /* 支払いサイト延伸［ZuraSale］　３ヶ月後　料金項目　セット */
    function set_search_3month_later_PriceList($facility_id, $purchaseDate, $paymentDueDate){
      $result = $this->search_extensionInformation($facility_id, $purchaseDate);

      $price      = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];
      $commission = 0;
      $sum_price  = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];

      $this->set('_3month_later_year', date("Y", strtotime($paymentDueDate)));
      $this->set('_3month_later_month', date("m", strtotime($paymentDueDate)));
      $this->set('_3month_later_price', $price);
      $this->set('_3month_later_commission', $commission);
      $this->set('_3month_later_sum_price', $sum_price);
    }

    /* 支払いサイト延伸［ZuraSale］　４ヶ月後　料金項目　セット */
    function set_search_4month_later_PriceList($facility_id, $purchaseDate, $paymentDueDate){
      $result = $this->search_extensionInformation($facility_id, $purchaseDate);

      $price      = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];
      $commission = 0;
      $sum_price  = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];

      $this->set('_4month_later_year', date("Y", strtotime($paymentDueDate)));
      $this->set('_4month_later_month', date("m", strtotime($paymentDueDate)));
      $this->set('_4month_later_price', $price);
      $this->set('_4month_later_commission', $commission);
      $this->set('_4month_later_sum_price', $sum_price);
    }

    /* 支払いサイト延伸［ZuraSale］　５ヶ月後　料金項目　セット */
    function set_search_5month_later_PriceList($facility_id, $purchaseDate, $paymentDueDate){
      $result = $this->search_extensionInformation($facility_id, $purchaseDate);

      $price      = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];
      $commission = 0;
      $sum_price  = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];

      $this->set('_5month_later_year', date("Y", strtotime($paymentDueDate)));
      $this->set('_5month_later_month', date("m", strtotime($paymentDueDate)));
      $this->set('_5month_later_price', $price);
      $this->set('_5month_later_commission', $commission);
      $this->set('_5month_later_sum_price', $sum_price);
    }

    /* 支払いサイト延伸［ZuraSale］　６ヶ月後　料金項目　セット */
    function set_search_6month_later_PriceList($facility_id, $purchaseDate, $paymentDueDate){
      $result = $this->search_extensionInformation($facility_id, $purchaseDate);

      $price      = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];
      $commission = 0;
      $sum_price  = isset($result) === false || count($result) === 0 ? 0 : $result['TrnClaim']['extension_price'];

      $this->set('_6month_later_year', date("Y", strtotime($paymentDueDate)));
      $this->set('_6month_later_month', date("m", strtotime($paymentDueDate)));
      $this->set('_6month_later_price', $price);
      $this->set('_6month_later_commission', $commission);
      $this->set('_6month_later_sum_price', $sum_price);
    }

    /* 支払いサイト延伸内容詳細　画面項目　セット */
    function set_confirm_DisplayItems($facility_id, $purchaseDate, $paymentDueDate){
      $result = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);
      $purchase_items = $this->search_purchase_items($facility_id, $purchaseDate);

      $extensionPrice = isset($result) === false || count($result) === 0 ? 0 : $result['ClaimCalculation']['present_month_extension_price'];;
      $commission     = isset($result) === false || count($result) === 0 ? 0 : $result['ClaimCalculation']['present_month_commission'];
      $price          = isset($result) === false || count($result) === 0 ? 0 : $result['ClaimCalculation']['present_month_not_extension_price'];
      $totalPrice     = $price + $commission;

      $default_extensionPrice = $extensionPrice;
      $default_commission     = $commission;
      $default_totalPrice     = $totalPrice;

      $this->set('purchase_items', $purchase_items);
      $this->set('totalExtensionPrice', $extensionPrice);
      $this->set('totalCommission', $commission);
      $this->set('totalPrice', $totalPrice);
      $this->set('default_totalExtensionPrice', $default_extensionPrice);
      $this->set('default_totalCommission', $default_commission);
      $this->set('default_totalPrice', $default_totalPrice);
      $this->set('present_year', date("Y", strtotime($paymentDueDate."")));
      $this->set('present_month', date("m", strtotime($paymentDueDate."")));
      $this->set('present_day', date("t", strtotime($paymentDueDate."")));
    }

    /* 支払いサイト延伸内容詳細　画面項目　セット
     * MEMO:前画面から延伸希望金額、延伸希望期間を渡された場合
     */
    function set_confirm_DisplayItems_($facility_id, $purchaseDate, $paymentDueDate, $selectedExtensionHopingMonth){
      $result = $this->search_claims_calculation_specification($facility_id, $paymentDueDate);
      $purchase_items = $this->search_purchase_items($facility_id, $purchaseDate);

      $extensionPrice = isset($result) === false || count($result) === 0 ? 0 : $result['ClaimCalculation']['present_month_extension_price'];;
      $commission     = isset($result) === false || count($result) === 0 ? 0 : $result['ClaimCalculation']['present_month_commission'];
      $price          = isset($result) === false || count($result) === 0 ? 0 : $result['ClaimCalculation']['present_month_not_extension_price'];
      $totalPrice     = $price + $commission;
      //延伸設定
      $apply_filter_items = $this->set_confirm_filter_items($facility_id, $extensionHopingPrice, $selectedExtensionHopingMonth, $purchase_items);
      //延伸設定後の購入商品リストを基に各種料金を再計算
      $defference_extensionPrice  = $this->get_defference_extensionPrice($apply_filter_items);
      $defference_commission      = $this->get_defference_commission($facility_id, $apply_filter_items, $selectedExtensionHopingMonth);
      $extensionPrice            += $defference_extensionPrice;
      $commission                += $defference_commission;
      $totalPrice                -= $defference_extensionPrice;

      $default_extensionPrice = $extensionPrice;
      $default_commission     = $commission;
      $default_totalPrice     = $totalPrice;

      $this->set('purchase_items', $apply_filter_items);
      $this->set('totalExtensionPrice', $extensionPrice);
      $this->set('totalCommission', $commission);
      $this->set('totalPrice', $totalPrice);
      $this->set('default_totalExtensionPrice', $default_extensionPrice);
      $this->set('default_totalCommission', $default_commission);
      $this->set('default_totalPrice', $default_totalPrice);
      $this->set('present_year', date("Y", strtotime($paymentDueDate."")));
      $this->set('present_month', date("m", strtotime($paymentDueDate."")));
      $this->set('present_day', date("t", strtotime($paymentDueDate."")));
    }

    /* 支払いサイト延伸内容詳細 購入商品　セット */
    function set_confirm_filter_items($facility_id, $extensionHopingPrice, $selectedExtensionHopingMonth, $purchase_items){
      $commission_rate = $this->search_zura_commission_rate($facility_id);
      $sumExtensionPrice = 0;
      $sumThisMonthlyPaymentPrice = 0;
      $magnification = $selectedExtensionHopingMonth * $commission_rate;
      $result = Array();
      foreach($purchase_items as $item){
        $sumItemPrice = ($item['TrnClaim']['unit_price'] * $item['TrnClaim']['count']) + (($item['TrnClaim']['unit_price'] * $magnification) * $item['TrnClaim']['count']);
        if (($sumItemPrice + $sumExtensionPrice)  > $extensionHopingPrice)
        {
          $sumThisMonthlyPaymentPrice += $item['TrnClaim']['unit_price'] * $item['TrnClaim']['count'];
          $result[] = $item;
          continue;
        }

        $this->change_ExtensionFlg($item, $selectedExtensionHopingMonth);
        $sumExtensionPrice += $item['TrnClaim']['unit_price'] * $item['TrnClaim']['count'];

        $result[] = $item;
      }
      return $result;
    }

    /* 料金情報　検索 */
    function search_claims_calculation_specification($facility_id, $paymentDueDate){
      $sql  = ' SELECT present_month_price AS "ClaimCalculation__present_month_price" ';
      $sql .= '       ,present_month_commission AS "ClaimCalculation__present_month_commission" ';
      $sql .= '       ,present_month_not_extension_price AS "ClaimCalculation__present_month_not_extension_price" ';
      $sql .= '       ,present_month_extension_price AS "ClaimCalculation__present_month_extension_price" ';
      $sql .= '       ,all_month_before_price AS "ClaimCalculation__all_month_before_price" ';
      $sql .= '       ,purchase_date AS "ClaimCalculation__purchase_date" ';
      $sql .= '       ,billing_date AS "ClaimCalculation__billing_date" ';
      $sql .= '   FROM v_claims_calculation_specification ';
      $sql .= '  WHERE facility_id = '.$facility_id;
      $sql .= "    AND billing_date = '".$paymentDueDate."'";

      $result = $this->TrnClaim->query($sql);

      if(isset($result) && isset($result[0])){
        return $result[0];
      }
      else{
        return null;
      }
    }

    /**　過去延伸料金取得
     * @var int $facility_id　対象の施設Id
     * @var date $paymentDueDate　対象の購入日
     **/
    function search_extensionInformation($facility_id, $paymentDueDate){
      $sql  = ' SELECT trunc(sum(claims.claim_price), 0) AS "TrnClaim__extension_price" ';
      $sql .= '       ,trunc(sum(claims.claim_price) * tax.tax_rate, 0) AS "TrnClaim__extension_tax" ';
      $sql .= '   FROM trn_claims AS claims';
      $sql .= '  INNER JOIN (SELECT tax_rate';
      $sql .= '                FROM mst_consumption_tax';
      $sql .= "               WHERE application_start_date < '".$paymentDueDate."'";
      $sql .= '               ORDER BY application_start_date DESC';
      $sql .= '               LIMIT 1) AS tax';
      $sql .= '     ON TRUE';
      $sql .= '  INNER JOIN mst_departments AS department';
      $sql .= '     ON claims.department_id_to = department.id';
      $sql .= '  INNER JOIN mst_facilities AS facility';
      $sql .= '     ON facility.id = department.mst_facility_id';
      $sql .= '  WHERE facility.id = '.$facility_id;
      $sql .= '    AND claims.is_deleted = false';
      $sql .= "    AND (    (claims.zura_number_of_month = 1 AND claims.claim_date BETWEEN date_trunc('month', '".$paymentDueDate."' ::date) + '-1 mons' AND date_trunc('month', '".$paymentDueDate."' ::date) + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month = 2 AND claims.claim_date BETWEEN date_trunc('month', '".$paymentDueDate."' ::date) + '-2 mons' AND date_trunc('month', '".$paymentDueDate."' ::date) + '-1 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month = 3 AND claims.claim_date BETWEEN date_trunc('month', '".$paymentDueDate."' ::date) + '-3 mons' AND date_trunc('month', '".$paymentDueDate."' ::date) + '-2 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month = 4 AND claims.claim_date BETWEEN date_trunc('month', '".$paymentDueDate."' ::date) + '-4 mons' AND date_trunc('month', '".$paymentDueDate."' ::date) + '-3 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month = 5 AND claims.claim_date BETWEEN date_trunc('month', '".$paymentDueDate."' ::date) + '-5 mons' AND date_trunc('month', '".$paymentDueDate."' ::date) + '-4 mons' + '-1 Day')";
      $sql .= "          OR (claims.zura_number_of_month = 6 AND claims.claim_date BETWEEN date_trunc('month', '".$paymentDueDate."' ::date) + '-6 mons' AND date_trunc('month', '".$paymentDueDate."' ::date) + '-5 mons' + '-1 Day')";
      $sql .= "        )";
      $sql .= '  GROUP BY claims.center_facility_id';
      $sql .= '       ,tax.tax_rate';

      $result = $this->TrnClaim->query($sql);

      if(isset($result) && isset($result[0])){
        return $result[0];
      }
      else{
        return null;
      }
    }

    /* 購入商品　検索
     * MEMO:$paymentDueDateに購入した商品、かつ$paymentDueDateに対して延伸された商品を返す
     */
    function search_purchase_items($facility_id, $paymentDueDate){
      $presentmonth_minDate = date("Y-m-01", strtotime($paymentDueDate.""));
      $presentmonth_maxDate = date("Y-m-t", strtotime($paymentDueDate.""));
      $_1monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -1 Month"));
      $_1monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -1 Month"));
      $_2monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -2 Month"));
      $_2monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -2 Month"));
      $_3monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -3 Month"));
      $_3monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -3 Month"));
      $_4monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -4 Month"));
      $_4monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -4 Month"));
      $_5monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -5 Month"));
      $_5monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -5 Month"));
      $_6monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -6 Month"));
      $_6monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -6 Month"));

      $sql  = ' SELECT TrnClaim.id AS "TrnClaim__id" ';
      $sql .= '       ,TrnClaim.claim_price AS "TrnClaim__claim_price" ';
      $sql .= '       ,TrnClaim.claim_date AS "TrnClaim__claim_date" ';
      $sql .= '       ,TRUNC(TrnClaim.unit_price, 0) AS "TrnClaim__unit_price" ';
      $sql .= '       ,TrnClaim.count AS "TrnClaim__count" ';
      $sql .= '       ,TrnClaim.zura_number_of_month AS "TrnClaim__zura_number_of_month"';
      $sql .= '       ,MstFacilityItem.id AS "MstFacilityItem__id"';
      $sql .= '       ,MstFacilityItem.item_name AS "MstFacilityItem__item_name"';
      $sql .= '       ,MstFacilityItem.standard AS "MstFacilityItem__standard"';
      $sql .= '       ,MstFacilityItem.item_code AS "MstFacilityItem__item_code"';
      $sql .= '       ,MstUnitName.unit_name AS "MstUnitName__unit_name" ';
      $sql .= '       ,CASE WHEN TrnClaim.zura_number_of_month = 0 THEN TRUE';
      $sql .= '             ELSE FALSE END AS "TrnClaim__extension_possible" ';
      $sql .= '       ,CASE WHEN TrnClaim.zura_number_of_month = 0 THEN FALSE';
      $sql .= '             ELSE TRUE END AS "TrnClaim__extension_set"';
      $sql .= '       ,CASE WHEN TrnClaim.zura_number_of_month = 0 THEN TRUE';
      $sql .= '             ELSE FALSE END AS "TrnClaim__present_month"';
      $sql .= '       ,FALSE AS "TrnClaim__1_month_later"';
      $sql .= '       ,FALSE AS "TrnClaim__2_month_later"';
      $sql .= '       ,FALSE AS "TrnClaim__3_month_later"';
      $sql .= '       ,FALSE AS "TrnClaim__4_month_later"';
      $sql .= '       ,FALSE AS "TrnClaim__5_month_later"';
      $sql .= '       ,FALSE AS "TrnClaim__6_month_later"';
      $sql .= '   FROM trn_claims AS TrnClaim';
      $sql .= '  INNER JOIN mst_departments AS MstDepartment';
      $sql .= '     ON TrnClaim.department_id_to = MstDepartment.id';
      $sql .= '  INNER JOIN mst_facilities AS MstFacility';
      $sql .= '     ON MstDepartment.mst_facility_id = MstFacility.id';
      $sql .= '  INNER JOIN mst_item_units AS MstItemUnit';
      $sql .= '     ON TrnClaim.mst_item_unit_id = MstItemUnit.id';
      $sql .= '  INNER JOIN mst_facility_items AS MstFacilityItem';
      $sql .= '     ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
      $sql .= '  INNER JOIN mst_unit_names AS MstUnitName';
      $sql .= '     ON MstItemUnit.mst_unit_name_id = MstUnitName.id';
      $sql .= "  WHERE TrnClaim.is_stock_or_sale = '2'";
      //当月購入分　（延伸なし）
      $sql .= "    AND ((TrnClaim.claim_date BETWEEN '".$presentmonth_minDate."' AND '".$presentmonth_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month = 0)';
      //過去購入分　（当月延伸）
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_1monthLater_minDate."' AND '".$_1monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month = 1)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_2monthLater_minDate."' AND '".$_2monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month = 2)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_3monthLater_minDate."' AND '".$_3monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month = 3)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_4monthLater_minDate."' AND '".$_4monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month = 4)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_5monthLater_minDate."' AND '".$_5monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month = 5)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_6monthLater_minDate."' AND '".$_6monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month = 6))';
      $sql .= '    AND MstFacility.id = '.$facility_id;
      $sql .= '    AND TrnClaim.is_deleted = false';
      $sql .= '    AND MstDepartment.is_deleted = false';
      $sql .= '    AND MstFacility.is_deleted = false';
      $sql .= '    AND MstItemUnit.is_deleted = false';
      $sql .= '    AND MstFacilityItem.is_deleted = false';
      $sql .= '    AND MstUnitName.is_deleted = false';
      $sql .= '  ORDER BY TrnClaim.claim_date';

      return $this->TrnClaim->query($sql);
    }

    /* 延伸商品　検索 */
    function search_extensionItems($facility_id, $paymentDueDate){
      $presentmonth_minDate = date("Y-m-01", strtotime($paymentDueDate.""));
      $presentmonth_maxDate = date("Y-m-t", strtotime($paymentDueDate.""));

      $sql  = ' SELECT TrnClaim.id AS "TrnClaim__id" ';
      $sql .= '       ,TrnClaim.claim_price AS "TrnClaim__claim_price" ';
      $sql .= '       ,TrnClaim.claim_date AS "TrnClaim__claim_date" ';
      $sql .= '       ,TRUNC(TrnClaim.unit_price, 0) AS "TrnClaim__unit_price" ';
      $sql .= '       ,TrnClaim.count AS "TrnClaim__count" ';
      $sql .= '       ,TrnClaim.zura_number_of_month AS "TrnClaim__zura_number_of_month"';
      $sql .= '       ,MstFacilityItem.id AS "MstFacilityItem__id"';
      $sql .= '       ,MstFacilityItem.item_name AS "MstFacilityItem__item_name"';
      $sql .= '       ,MstFacilityItem.standard AS "MstFacilityItem__standard"';
      $sql .= '       ,MstFacilityItem.item_code AS "MstFacilityItem__item_code"';
      $sql .= '       ,MstUnitName.unit_name AS "MstUnitName__unit_name" ';
      $sql .= '       ,CASE WHEN TrnClaim.zura_number_of_month = 0 THEN TRUE';
      $sql .= '             ELSE FALSE END AS "TrnClaim__extension_possible" ';
      $sql .= '       ,FALSE AS "TrnClaim__extension_set"';
      $sql .= '       ,CASE WHEN TrnClaim.zura_number_of_month = 0 THEN TRUE';
      $sql .= '             ELSE FALSE END AS "TrnClaim__present_month"';
      $sql .= '       ,FALSE AS "TrnClaim__1_month_later"';
      $sql .= '       ,FALSE AS "TrnClaim__2_month_later"';
      $sql .= '       ,FALSE AS "TrnClaim__3_month_later"';
      $sql .= '       ,FALSE AS "TrnClaim__4_month_later"';
      $sql .= '       ,FALSE AS "TrnClaim__5_month_later"';
      $sql .= '       ,FALSE AS "TrnClaim__6_month_later"';
      $sql .= '   FROM trn_claims AS TrnClaim';
      $sql .= '  INNER JOIN mst_departments AS MstDepartment';
      $sql .= '     ON TrnClaim.department_id_to = MstDepartment.id';
      $sql .= '  INNER JOIN mst_facilities AS MstFacility';
      $sql .= '     ON MstDepartment.mst_facility_id = MstFacility.id';
      $sql .= '  INNER JOIN mst_item_units AS MstItemUnit';
      $sql .= '     ON TrnClaim.mst_item_unit_id = MstItemUnit.id';
      $sql .= '  INNER JOIN mst_facility_items AS MstFacilityItem';
      $sql .= '     ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
      $sql .= '  INNER JOIN mst_unit_names AS MstUnitName';
      $sql .= '     ON MstItemUnit.mst_unit_name_id = MstUnitName.id';
      $sql .= "  WHERE TrnClaim.is_stock_or_sale = '2'";
      //当月購入分　（延伸あり）
      $sql .= "    AND TrnClaim.claim_date BETWEEN '".$presentmonth_minDate."' AND '".$presentmonth_maxDate."'";
      $sql .= '    AND TrnClaim.zura_number_of_month > 0';
      $sql .= '    AND MstFacility.id = '.$facility_id;
      $sql .= '    AND TrnClaim.is_deleted = false';
      $sql .= '    AND MstDepartment.is_deleted = false';
      $sql .= '    AND MstFacility.is_deleted = false';
      $sql .= '    AND MstItemUnit.is_deleted = false';
      $sql .= '    AND MstFacilityItem.is_deleted = false';
      $sql .= '    AND MstUnitName.is_deleted = false';
      $sql .= '  ORDER BY TrnClaim.claim_date';

      return $this->TrnClaim->query($sql);
    }

    /* 延伸上限金額　検索 */
    function search_extensionUpperLimit($facility_id, $paymentDueDate){
      $presentmonth_minDate = date("Y-m-01", strtotime($paymentDueDate.""));
      $presentmonth_maxDate = date("Y-m-t", strtotime($paymentDueDate.""));
      $_1monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -1 Month"));
      $_1monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -1 Month"));
      $_2monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -2 Month"));
      $_2monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -2 Month"));
      $_3monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -3 Month"));
      $_3monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -3 Month"));
      $_4monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -4 Month"));
      $_4monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -4 Month"));
      $_5monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -5 Month"));
      $_5monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -5 Month"));
      $_6monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -6 Month"));
      $_6monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -6 Month"));

      $sql  = ' SELECT SUM(TrnClaim.claim_price) AS "TrnClaim__extension_price" ';
      $sql .= '       ,MstFacility.zura_maximum_amount AS "MstFacility__zura_maximum_amount" ';
      $sql .= '   FROM trn_claims AS TrnClaim';
      $sql .= '  INNER JOIN mst_departments AS MstDepartment';
      $sql .= '     ON TrnClaim.department_id_to = MstDepartment.id';
      $sql .= '  INNER JOIN mst_facilities AS MstFacility';
      $sql .= '     ON MstDepartment.mst_facility_id = MstFacility.id';
      $sql .= '  INNER JOIN mst_item_units AS MstItemUnit';
      $sql .= '     ON TrnClaim.mst_item_unit_id = MstItemUnit.id';
      $sql .= '  INNER JOIN mst_facility_items AS MstFacilityItem';
      $sql .= '     ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
      $sql .= '  INNER JOIN mst_unit_names AS MstUnitName';
      $sql .= '     ON MstUnitName.id = MstItemUnit.mst_unit_name_id ';
      $sql .= '   LEFT JOIN trn_claim_specifications AS TrnSpecifications';
      $sql .= '     ON TrnSpecifications.mst_facility_id = '.$facility_id;
      $sql .= '    ANd TrnSpecifications.is_deleted = false';
      $sql .= "    AND TrnSpecifications.closing_date BETWEEN '".$presentmonth_minDate."' AND '".$presentmonth_maxDate."'";
      //当月購入分　（延伸あり）
      $sql .= "  WHERE ((TrnClaim.claim_date BETWEEN '".$presentmonth_minDate."' AND '".$presentmonth_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month > 0)';
      //過去購入分　（当月延伸）
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_1monthLater_minDate."' AND '".$_1monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 1)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_2monthLater_minDate."' AND '".$_2monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 2)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_3monthLater_minDate."' AND '".$_3monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 3)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_4monthLater_minDate."' AND '".$_4monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 4)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_5monthLater_minDate."' AND '".$_5monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 5)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_6monthLater_minDate."' AND '".$_6monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 6))';
      $sql .= '    AND MstFacility.id = '.$facility_id;
      $sql .= "    AND TrnClaim.is_stock_or_sale = '2'";
      $sql .= '    AND TrnClaim.is_deleted = false';
      $sql .= '    AND MstDepartment.is_deleted = false';
      $sql .= '    AND MstFacility.is_deleted = false';
      $sql .= '    AND MstItemUnit.is_deleted = false';
      $sql .= '    AND MstFacilityItem.is_deleted = false';
      $sql .= '    AND MstUnitName.is_deleted = false';
      $sql .= '  GROUP BY MstFacility.id';

      $result = $this->TrnClaim->query($sql, false, false);
      $maximum_amount = 0;
      $payment_track_record = 0;

      if (isset($result[0]) == false || isset($result[0]['MstFacility']) == false || isset($result[0]['TrnClaim']) == false){
        return 0;
      }
      else {
        $maximum_amount = $result[0]['MstFacility']['zura_maximum_amount'];
        $payment_track_record = $result[0]['TrnClaim']['extension_price'];
        if ($maximum_amount <= $payment_track_record)
        {
          return 0;
        }
        return $maximum_amount - $payment_track_record;
      }
    }

    /*　延伸設定情報　検索 */
    function search_extensionUpperInformation($facility_id, $paymentDueDate){
      $presentmonth_minDate = date("Y-m-01", strtotime($paymentDueDate.""));
      $presentmonth_maxDate = date("Y-m-t", strtotime($paymentDueDate.""));
      $_1monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -1 Month"));
      $_1monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -1 Month"));
      $_2monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -2 Month"));
      $_2monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -2 Month"));
      $_3monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -3 Month"));
      $_3monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -3 Month"));
      $_4monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -4 Month"));
      $_4monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -4 Month"));
      $_5monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -5 Month"));
      $_5monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -5 Month"));
      $_6monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -6 Month"));
      $_6monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -6 Month"));

      $sql  = ' SELECT SUM(TrnClaim.claim_price) AS "TrnClaim__extension_price" ';
      $sql .= '       ,MstFacility.zura_maximum_amount AS "MstFacility__zura_maximum_amount" ';
      $sql .= '   FROM trn_claims AS TrnClaim';
      $sql .= '  INNER JOIN mst_departments AS MstDepartment';
      $sql .= '     ON TrnClaim.department_id_to = MstDepartment.id';
      $sql .= '  INNER JOIN mst_facilities AS MstFacility';
      $sql .= '     ON MstDepartment.mst_facility_id = MstFacility.id';
      $sql .= '  INNER JOIN mst_item_units AS MstItemUnit';
      $sql .= '     ON TrnClaim.mst_item_unit_id = MstItemUnit.id';
      $sql .= '  INNER JOIN mst_facility_items AS MstFacilityItem';
      $sql .= '     ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
      $sql .= '  INNER JOIN mst_unit_names AS MstUnitName';
      $sql .= '     ON MstUnitName.id = MstItemUnit.mst_unit_name_id ';
      $sql .= '   LEFT JOIN trn_claim_specifications AS TrnSpecifications';
      $sql .= '     ON TrnSpecifications.mst_facility_id = '.$facility_id;
      $sql .= '    ANd TrnSpecifications.is_deleted = false';
      $sql .= "    AND TrnSpecifications.closing_date BETWEEN '".$presentmonth_minDate."' AND '".$presentmonth_maxDate."'";
      //当月購入分　（延伸あり）
      $sql .= "  WHERE ((TrnClaim.claim_date BETWEEN '".$presentmonth_minDate."' AND '".$presentmonth_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month > 0)';
      //過去購入分　（当月延伸）
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_1monthLater_minDate."' AND '".$_1monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 1)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_2monthLater_minDate."' AND '".$_2monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 2)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_3monthLater_minDate."' AND '".$_3monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 3)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_4monthLater_minDate."' AND '".$_4monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 4)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_5monthLater_minDate."' AND '".$_5monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 5)';
      $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_6monthLater_minDate."' AND '".$_6monthLater_maxDate."'";
      $sql .= '              AND TrnClaim.zura_number_of_month >= 6))';
      $sql .= '    AND MstFacility.id = '.$facility_id;
      $sql .= "    AND TrnClaim.is_stock_or_sale = '2'";
      $sql .= '    AND TrnClaim.is_deleted = false';
      $sql .= '    AND MstDepartment.is_deleted = false';
      $sql .= '    AND MstFacility.is_deleted = false';
      $sql .= '    AND MstItemUnit.is_deleted = false';
      $sql .= '    AND MstFacilityItem.is_deleted = false';
      $sql .= '    AND MstUnitName.is_deleted = false';
      $sql .= '  GROUP BY MstFacility.id';

      $result = $this->TrnClaimSpecifications->query($sql, false, false);

      if (isset($result[0]) == false || isset($result[0]['MstFacility']) == false || isset($result[0]['TrnClaim']) == false){
        return null;
      }
      else {
        return $result[0];
      }
    }

    /*　手数料利率　検索 */
    function search_zura_commission_rate($facility_id){
      $sql  = ' SELECT zura_commission_rate AS "MstFacility__zura_commission_rate" ';
      $sql .= '   FROM mst_facilities';
      $sql .= '  WHERE is_deleted = false';
      $sql .= '    AND id = '.$facility_id;

      $result = $this->MstFacility->query($sql);
      if(isset($result) && isset($result[0])){
        return $result[0]['MstFacility']['zura_commission_rate'];
      }
      else{
        return null;
      }
    }

    /* 入金予定額　検索 */
    function search_paymentAmout($facility_id, $paymentDueDate){
      $minDate = date('Y-m-01', strtotime($paymentDueDate.""));
      $maxDate = date('Y-m-t', strtotime($paymentDueDate.""));

      $sql  = ' SELECT present_month_price AS "ClaimCalculation__present_month_price" ';
      $sql .= '   FROM v_claims_calculation_specification';
      $sql .= '  WHERE facility_id = '.$facility_id;
      $sql .= "    AND purchase_date BETWEEN '".$minDate."' AND '".$maxDate."'";

      $result = $this->TrnClaim->query($sql);

      if (isset($result[0]) == false || isset($result[0]['ClaimCalculation']) == false){
        return 0;
      }
      else {
        return $result[0]['ClaimCalculation']['present_month_price'];
      }
    }

    /* 選択施設ID　検索 */
    function search_selectedFacility(){
      $facility_id = $this->Session->read('Auth.facility_id_selected');
      $selectedFacility_id = $this->search_partner_facility($facility_id);

      return $selectedFacility_id;
    }

    /* 選択パートナー施設ID　検索 */
    function search_partner_facility($facility_id){
      $sql  = ' SELECT rel.partner_facility_id AS "MstFacility__partner_facility_id" ';
      $sql .= '   FROM mst_facility_relations AS rel';
      $sql .= '  INNER JOIN mst_facilities AS fac';
      $sql .= '     ON rel.partner_facility_id = fac.id';
      $sql .= '  WHERE rel.mst_facility_id = '.$facility_id;
      $sql .= "    AND fac.facility_type = '2'";
      $sql .= '    AND fac.is_deleted = false';

      $result = $this->MstFacility->query($sql);
      if(isset($result) && isset($result[0])){
        return $result[0]['MstFacility']['partner_facility_id'];
      }
      else{
        return null;
      }
    }

    /* 請求書発行　対象月リスト　検索 */
    function search_report_DateList($facility_id){
      $sql  = ' SELECT id AS "TrnClaimSpecification__id" ';
      $sql .= '       ,closing_date AS "TrnClaimSpecification__closing_date" ';
      $sql .= '   FROM trn_claim_specifications ';
      $sql .= '  WHERE mst_facility_id = '.$facility_id;
      $sql .= '    AND is_deleted = false';

      $result = $this->TrnClaim->query($sql);

      $dateList = array(""=>"");

      foreach($result AS $specification){
        $key = $specification['TrnClaimSpecification']['id'];
        $value = date('Y/m', strtotime($specification['TrnClaimSpecification']['closing_date']));
        $dateList[$key] = $value;
      }

      return $dateList;
    }

    /* 購入日　取得 */
    function get_purchaseDate(){
      $now = date('Y-m-d 00:00:00');
      if(date('d', strtotime($now."")) > 10){
        return date('Y-m-01 00:00:00', strtotime($now.""));
      }else{
        return date('Y-m-01 00:00:00', strtotime($now." -1 Month"));
      }
    }

    /* 延伸設定後、延伸金額　取得 */
    function get_defference_extensionPrice($apply_filter_items){
      $extensionPrice = 0;
      foreach($apply_filter_items AS $item){

        if ($item['TrnClaim']['extension_set'] == false || $item['TrnClaim']['zura_number_of_month'] > 0) { continue; }
        $extensionPrice += $item['TrnClaim']['claim_price'];
      }
      return $extensionPrice;
    }

    /* 延伸設定後、延伸手数料　取得 */
    function get_defference_commission($facility_id, $apply_filter_items, $selectedExtensionHopingMonth){
      $zura_commission_rate = $this->search_zura_commission_rate($facility_id);
      $commission = 0;

      foreach($apply_filter_items AS $item){
        if ($item['TrnClaim']['extension_set'] == false || $item['TrnClaim']['zura_number_of_month'] > 0) { continue; }
        $price = $item['TrnClaim']['claim_price'];
        $commission += floor($price * $zura_commission_rate * $selectedExtensionHopingMonth);
      }

      return $commission;
    }

    /* 延伸情報設定
     * MEMO:$selectedExtensionHopingMonthに対応した商品に対して延伸設定を行う
     */
    function change_ExtensionFlg(&$item, $selectedExtensionHopingMonth){
      switch($selectedExtensionHopingMonth){
        case "0":
            $item['TrnClaim']['extension_set'] = false;
            $item['TrnClaim']['present_month'] = true;
            $item['TrnClaim']['1_month_later'] = false;
            $item['TrnClaim']['2_month_later'] = false;
            $item['TrnClaim']['3_month_later'] = false;
            $item['TrnClaim']['4_month_later'] = false;
            $item['TrnClaim']['5_month_later'] = false;
            $item['TrnClaim']['6_month_later'] = false;
            return;
        case "1":
            $item['TrnClaim']['extension_set'] = true;
            $item['TrnClaim']['present_month'] = false;
            $item['TrnClaim']['1_month_later'] = true;
            $item['TrnClaim']['2_month_later'] = false;
            $item['TrnClaim']['3_month_later'] = false;
            $item['TrnClaim']['4_month_later'] = false;
            $item['TrnClaim']['5_month_later'] = false;
            $item['TrnClaim']['6_month_later'] = false;
            return;
        case "2":
            $item['TrnClaim']['extension_set'] = true;
            $item['TrnClaim']['present_month'] = false;
            $item['TrnClaim']['1_month_later'] = false;
            $item['TrnClaim']['2_month_later'] = true;
            $item['TrnClaim']['3_month_later'] = false;
            $item['TrnClaim']['4_month_later'] = false;
            $item['TrnClaim']['5_month_later'] = false;
            $item['TrnClaim']['6_month_later'] = false;
            return;
        case "3":
            $item['TrnClaim']['extension_set'] = true;
            $item['TrnClaim']['present_month'] = false;
            $item['TrnClaim']['1_month_later'] = false;
            $item['TrnClaim']['2_month_later'] = false;
            $item['TrnClaim']['3_month_later'] = true;
            $item['TrnClaim']['4_month_later'] = false;
            $item['TrnClaim']['5_month_later'] = false;
            $item['TrnClaim']['6_month_later'] = false;
            return;
        case "4":
            $item['TrnClaim']['extension_set'] = true;
            $item['TrnClaim']['present_month'] = false;
            $item['TrnClaim']['1_month_later'] = false;
            $item['TrnClaim']['2_month_later'] = false;
            $item['TrnClaim']['3_month_later'] = false;
            $item['TrnClaim']['4_month_later'] = true;
            $item['TrnClaim']['5_month_later'] = false;
            $item['TrnClaim']['6_month_later'] = false;
            return;
        case "5":
            $item['TrnClaim']['extension_set'] = true;
            $item['TrnClaim']['present_month'] = false;
            $item['TrnClaim']['1_month_later'] = false;
            $item['TrnClaim']['2_month_later'] = false;
            $item['TrnClaim']['3_month_later'] = false;
            $item['TrnClaim']['4_month_later'] = false;
            $item['TrnClaim']['5_month_later'] = true;
            $item['TrnClaim']['6_month_later'] = false;
            return;
        case "6":
            $item['TrnClaim']['extension_set'] = true;
            $item['TrnClaim']['present_month'] = false;
            $item['TrnClaim']['1_month_later'] = false;
            $item['TrnClaim']['2_month_later'] = false;
            $item['TrnClaim']['3_month_later'] = false;
            $item['TrnClaim']['4_month_later'] = false;
            $item['TrnClaim']['5_month_later'] = false;
            $item['TrnClaim']['6_month_later'] = true;
            return;
      }
    }

    /* 更新前　排他ロック */
    function rowLock_trnClaim($id){
      $sql  = ' SELECT id';
      $sql .= '   FROM trn_claims';
      $sql .= '  WHERE id = '.$id;
      $sql .= '    FOR UPDATE';

      $this->TrnClaim->query($sql);
    }

    /* 延伸月数　更新 */
    function update_trnClaim($ids, $months, $now){
      //更新処理
      $this->TrnClaim->create();
      $this->TrnClaim->begin();
      foreach($ids as $id){
        $month = $months[$id];
        $this->rowLock_trnClaim($id);
        try{
          $result = $this->TrnClaim->save(array(
            'id'                  => $id,
            'zura_number_of_month'=> $month,
            'modifier'            => $this->Session->read('Auth.MstUser.id'),
            'modified'            => $now,
          ));
        }catch(Exception $e){
          $this->TrnClaim->rollback();
          return;
        }
        if($result === false){
          $this->TrnClaim->rollback();
          return;
        }
      }
      $this->TrnClaim->commit();
    }
}
