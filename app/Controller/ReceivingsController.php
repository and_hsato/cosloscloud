<?php
/**
 * ReceivingsController
 *  入荷
 * @version 1.0.0
 * @since 2010/08/17
 */
class ReceivingsController extends AppController {
    var $name = 'Receivings';
    var $uses = array(
        'TrnOrderHeader',
        'TrnOrder',
        'MstFacility',
        'MstClass',
        'MstTransactionConfig',
        'MstDepartment',
        'MstItemUnit',
        'TrnCloseHeader',
        'TrnReceivingHeader',
        'TrnReceiving',
        'TrnClaim',
        'TrnSticker',
        'TrnStock',
        'TrnStorageHeader',
        'TrnStorage',
        'TrnStickerRecord',
    );

    /**
     *
     * @var array  $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    public function beforeFilter(){
      parent::beforeFilter();
      $this->Auth->allowedActions[] = 'report';
      $this->Auth->allowedActions[] = 'seal';
    }

    var $components = array('RequestHandler','Stickers','Barcode');

    public function index(){
        $this->setRoleFunction(21); //入荷登録
        $this->search();
    }

    /**
     *  Search
     */
    public function search(){
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }

        $this->set('facilities',
                   $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                          array(Configure::read('FacilityType.supplier')),
                                          true,
                                          array('facility_code' , 'facility_name'),
                                          'facility_name, facility_code'
                                          )
                   );

        
        $_OrderType = Configure::read('OrderTypes.orderTypeName');
        $this->set('orderTypes', $_OrderType);

        $_classes = $this->getClassesList( $this->Session->read('Auth.facility_id_selected'),'8');
        $this->set('classes', $_classes);

        $_SearchResult = array();

        if(isset($this->request->data['IsSearch'])){
            App::import('Sanitize');
            $where = '';
            if($this->request->data['TrnOrderHeader']['work_no'] != ''){
                $where .= " and a.work_no LIKE '%" . Sanitize::escape($this->request->data['TrnOrderHeader']['work_no']) . "%'";
            }
            if($this->request->data['TrnOrderHeader']['order_date_from'] != ''){
                $where .= " and a.work_date >= '" . $this->request->data['TrnOrderHeader']['order_date_from'] . "'";
            }
            if($this->request->data['TrnOrderHeader']['order_date_to'] != ''){
                $where .= " and a.work_date <= '" . $this->request->data['TrnOrderHeader']['order_date_to'] . "'";
            }

            if($this->request->data['TrnOrderHeader']['order_type'] != ''){
                $where .= " and a.order_type = " . $this->request->data['TrnOrderHeader']['order_type'];
            }

            if($this->request->data['MstFacility']['facility_code'] != ''){
                $where .= " and j.facility_code = '" . $this->request->data['MstFacility']['facility_code'] . "'";
            }

            if($this->request->data['MstFacilityItem']['internal_code'] != ''){
                $where .= " and d.internal_code = '" . $this->request->data['MstFacilityItem']['internal_code'] . "'";
            }

            if($this->request->data['MstFacilityItem']['item_code'] != ''){
                $where .= " and d.item_code LIKE '%" . $this->request->data['MstFacilityItem']['item_code'] . "%'";
            }

            if($this->request->data['MstFacilityItem']['item_name'] != ''){
                $where .= " and d.item_name LIKE '%" . $this->request->data['MstFacilityItem']['item_name'] . "%'";
            }

            if($this->request->data['MstFacilityItem']['standard'] != ''){
                $where .= " and d.standard LIKE '%" . $this->request->data['MstFacilityItem']['standard'] . "%'";
            }
            
            if($this->request->data['MstFacilityItem']['aptage_code'] != ''){
                $where .= " and d.spare_key2 LIKE '%" . $this->request->data['MstFacilityItem']['aptage_code'] . "%'";
            }

            if($this->request->data['MstDealer']['dealer_name'] != '') {
                $where .= " and e.dealer_name LIKE '%" . $this->request->data['MstDealer']['dealer_name'] . "%'";
            }

            if($this->request->data['MstClass']['id'] != ''){
                $where .= ' and b.work_type = ' . $this->request->data['MstClass']['id'];
            }

            if(isset($this->request->data['TrnEdiShipping']['isEdiShipping'])){
                $where .= ' and x.trn_edi_receiving_id is not null ';
            }
            
            $_limit = $this->_getLimitCount();
            
            $_res = $this->getOrderHeaders($where, $_limit , true);
            for($i = 0; $i< count($_res); $i++){
                $_r = array();
                array_walk_recursive($_res[$i], array('ReceivingsController','BuildSearchResult'), &$_r);
                $_SearchResult[] = $_r;
            }
        }
        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set('SearchResult', $_SearchResult);
        $this->render('search');
    }

    public function confirm($mode=1){
        if($mode == 1 ){
            //通常処理の場合
            $where = '';
            if($this->request->data['MstFacility']['facility_code'] != ''){
                $where .= " and i.facility_code ='" . $this->request->data['MstFacility']['facility_code'] . "'";
            }
            
            if($this->request->data['MstFacilityItem']['internal_code'] != ''){
                $where .= " and d.internal_code ='" . $this->request->data['MstFacilityItem']['internal_code'] . "'";
            }
            
            if($this->request->data['MstFacilityItem']['aptage_code'] != ''){
                $where .= " and d.spare_key2 like'%" . $this->request->data['MstFacilityItem']['aptage_code'] . "%'";
            }

            
            if($this->request->data['MstFacilityItem']['item_code'] != ''){
                $where .= " and d.item_code like '%" . $this->request->data['MstFacilityItem']['item_code'] . "%'";
            }
            
            if($this->request->data['MstFacilityItem']['item_name'] != ''){
                $where .= " and d.item_name like '%" . $this->request->data['MstFacilityItem']['item_name'] . "%'";
            }
            
            if($this->request->data['MstFacilityItem']['standard'] != ''){
                $where .= " and d.standard like '%" . $this->request->data['MstFacilityItem']['standard'] . "%'";
            }
            
            if($this->request->data['MstDealer']['dealer_name'] != '') {
                $where .= " and e.dealer_name like '%" . $this->request->data['MstDealer']['dealer_name'] . "%'";
            }
            
            if($this->request->data['MstClass']['id'] != ''){
                $where .= " and b.work_type = " . $this->request->data['MstClass']['id'];
            }
            
            $_headerCondition = array();
            foreach($this->request->data['TrnOrderHeader']['Rows'] as $_key=>$_value){
                if($_value == '1'){
                    $_headerCondition[] = $_key;
                }
            }
            $where .= ' and a.id in (' . join(',',$_headerCondition) . ')';
            $where .= ' and b.remain_count > 0 ';
            
            $_searchResult = $this->getOrders($where);
        }else{
            // 出荷伝票読み込みの場合
            $sql  = ' select ';
            $sql .= '       d.id            as "TrnOrder__id"';
            $sql .= '     , d.trn_order_header_id as "TrnOrder__trn_order_header_id"';
            $sql .= '     , d.mst_item_unit_id as "TrnOrder__mst_item_unit_id"';
            $sql .= '     , d.work_no       as "TrnOrder__work_no"';
            $sql .= '     , d.order_type    as "TrnOrder__order_type"';
            $sql .= '     , f.is_lowlevel   as "TrnOrder__is_lowlevel"';
            $sql .= "     , to_char(d.work_date,'YYYY/mm/dd')";
            $sql .= '                       as "TrnOrder__work_date"';
            $sql .= '     , ( case ';
            foreach(Configure::read('OrderTypes.orderTypeName') as $k => $v ) {
                $sql .= "         when d.order_type = '" .  $k . "' then '" . $v . "'";
            }
            $sql .= "       else '' ";
            $sql .= '       end )                       as "TrnOrder__order_type_name"';
            
            $sql .= '     , f.internal_code as "TrnOrder__internal_code" ';
            $sql .= '     , f.item_name     as "TrnOrder__item_name"';
            $sql .= '     , f.item_code     as "TrnOrder__item_code"';
            $sql .= '     , f.standard      as "TrnOrder__standard"';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when e.per_unit = 1  ';
            $sql .= '         then e1.unit_name  ';
            $sql .= "         else e1.unit_name || '(' || e.per_unit || e2.unit_name || ')' ";
            $sql .= '         end ';
            $sql .= '     )                 as "TrnOrder__unit_name" ';
            $sql .= '     , g.dealer_name   as "TrnOrder__dealer_name"';
            $sql .= '     , i.facility_name as "TrnOrder__facility_name"';
            $sql .= '     , i.id            as "TrnOrder__facility_id"';
            $sql .= '     , j.transaction_price as "TrnOrder__transaction_price"';
            $sql .= '     , ( case when d.remain_count > b.quantity then b.quantity';
            $sql .= '         else d.remain_count';
            $sql .= '        end )          as "TrnOrder__remain_count"';
            
            $sql .= '   from ';
            $sql .= '     trn_edi_shipping_headers as a  ';
            $sql .= '     left join trn_edi_shippings as b  ';
            $sql .= '       on b.trn_edi_shipping_header_id = a.id  ';
            $sql .= '     left join trn_edi_receivings as c  ';
            $sql .= '       on c.id = b.trn_edi_receiving_id  ';
            $sql .= '     left join trn_orders as d  ';
            $sql .= '       on d.id = c.trn_order_id  ';
            $sql .= '     left join mst_item_units as e  ';
            $sql .= '       on e.id = d.mst_item_unit_id  ';
            $sql .= '     left join mst_unit_names as e1  ';
            $sql .= '       on e1.id = e.mst_unit_name_id  ';
            $sql .= '     left join mst_unit_names as e2  ';
            $sql .= '       on e2.id = e.per_unit_name_id  ';
            $sql .= '     left join mst_facility_items as f  ';
            $sql .= '       on f.id = e.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as g  ';
            $sql .= '       on g.id = f.mst_dealer_id  ';
            $sql .= '     left join mst_departments as h ';
            $sql .= '       on h.id = d.department_id_to ';
            $sql .= '     left join mst_facilities as i ';
            $sql .= '       on i.id = h.mst_facility_id ';
            $sql .= '     left join mst_transaction_configs as j ';
            $sql .= '       on j.mst_item_unit_id = e.id ';
            $sql .= '      and j.partner_facility_id = i.id ';
            $sql .= '      and j.start_date <= b.work_date ';
            $sql .= '      and j.end_date > b.work_date';
            $sql .= '     left join mst_departments as k ';
            $sql .= '       on k.id = d.department_id_from ';
            
            $sql .= '   where ';
            $sql .= "     a.work_no = '" . $this->request->data['TrnEdiShipping']['work_no'] . "'";
            $sql .= '     and a.is_deleted = false  ';
            $sql .= '     and b.is_deleted = false  ';
            $sql .= '     and c.is_deleted = false  ';
            $sql .= '     and d.is_deleted = false  ';
            $sql .= '     and d.remain_count > 0 ';
            $sql .= '     and k.department_type = ' . Configure::read('DepartmentType.warehouse');
            
            $_searchResult = $this->TrnOrder->query($sql);
        }
        
        $_classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'09');
        $this->set('classes', $_classes);
        
        $this->request->data['TrnReceivingHeader']['work_date'] = date('Y/m/d');
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnReceiving.token' , $token);
        $this->request->data['TrnReceiving']['token'] = $token;

        $_searchResult = AppController::outputFilter($_searchResult);
        $this->set('SearchResult', $_searchResult);
        $this->Session->write('TrnOrder.SearchResult', $_searchResult);
        
        $this->set('IsShowFinal', true);
        $this->render('/receivings/confirm');
    }

    public function results(){
        $now = date('Y/m/d H:i:s');
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['TrnReceiving']['token'] === $this->Session->read('TrnReceiving.token')) {
            $this->Session->delete('TrnReceiving.token');
            $_classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'09');

            $orderIds = array();
            $receivingHeaderId = array();
            
            $this->TrnReceivingHeader->begin();
            //行ロック
            $this->TrnOrder->query('select * from trn_orders as a where a.id in ( ' . join(',' , $this->request->data['TrnReceiving']['id'] ) . ' ) for update ');
            
            //更新時間チェックをする
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_orders as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $this->request->data['TrnReceiving']['id']  ). ')';
            $sql .= "     and a.modified > '" . $this->request->data['TrnReceiving']['time'] ."'";

            $ret = $this->TrnSticker->query($sql);
            
            if($ret[0][0]['count'] > 0){
                $this->rollbackTables();
                $this->Session->setFlash('ほかのユーザによって更新されています。最初からやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('search');
            }
            
            //ヘッダ情報を取得
            $sql  = ' select ';
            $sql .= '       a.trn_order_header_id ';
            $sql .= '     , a.department_id_from ';
            $sql .= '     , a.department_id_to ';
            $sql .= '     , a.order_type ';
            $sql .= '     , c.round ';
            $sql .= '     , count(*) as count ';
            $sql .= '   from ';
            $sql .= '     trn_orders as a  ';
            $sql .= '   left join mst_departments as b';
            $sql .= '     on b.id = a.department_id_from ';
            $sql .= '   left join mst_facilities as c ';
            $sql .= '     on c.id = b.mst_facility_id';
            $sql .= '   where a.id in (' . join(',',$this->request->data['TrnReceiving']['id']) . ')';
            $sql .= '   group by ';
            $sql .= '     a.trn_order_header_id ';
            $sql .= '     , a.department_id_from ';
            $sql .= '     , a.department_id_to ';
            $sql .= '     , c.round';
            $sql .= '     , a.order_type ';
            
            $header_data = $this->TrnOrder->query($sql);
            
            //登録処理
            foreach($header_data as $h){
                $work_no  = $this->setWorkNo4Header($this->request->data['TrnReceivingHeader']['work_date'], '09');
                $round               = $h[0]['round'];              //仕入先の丸め区分をとること
                $dep_from            = $h[0]['department_id_from']; //発注元（センター or 病院）
                $dep_to              = $h[0]['department_id_to'];   //仕入れ先部署
                $count               = $h[0]['count'];
                $trn_order_header_id = $h[0]['trn_order_header_id'];
                $order_type          = $h[0]['order_type'];
                
                $this->TrnReceivingHeader->create();
                $_resHeader = $this->TrnReceivingHeader->save(array(
                    'id'                 => false,
                    'work_no'            => $work_no,
                    'work_date'          => $this->request->data['TrnReceivingHeader']['work_date'],
                    'recital'            => $this->request->data['TrnReceivingHeader']['recital'],
                    'receiving_type'     => Configure::read('ReceivingType.arrival'),
                    'receiving_status'   => Configure::read('ReceivingStatus.arrival'),
                    'department_id_from' => $dep_to,
                    'department_id_to'   => $dep_from,
                    'detail_count'       => $count,
                    'is_deleted'         => false,
                    'created'            => $now,
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'modified'           => $now,
                    'trn_order_header'   => $trn_order_header_id,
                    ));
                if(false === $_resHeader) {
                    $this->rollbackTables();
                    throw new Exception('TrnReceivingHeader登録失敗' . print_r($this->request->data,true));
                    exit;
                }
                $_lastId = $this->TrnReceivingHeader->getLastInsertID();
                if($order_type == Configure::read('OrderTypes.opestock') ){
                    $receivingHeaderId[] = $_lastId;
                }
                $seq = 0; //明細シーケンス
                
                // 入荷ヘッダ作成
                foreach($this->request->data['TrnReceiving']['id'] as $id){
                    if( $this->request->data['TrnReceiving']['trn_order_header_id'][$id] == $trn_order_header_id){
                        $quantity         = $this->request->data['TrnReceiving']['quantity'][$id];
                        $mst_item_unit_id = $this->request->data['TrnReceiving']['mst_item_unit_id'][$id];
                        $work_type        = $this->request->data['TrnReceiving']['work_type'][$id];
                        $recital          = $this->request->data['TrnReceiving']['recital'][$id];
                        $IsExclude        = $this->request->data['TrnReceiving']['IsExclude'][$id];
                        $stocking_price   = $this->request->data['TrnReceiving']['stocking_price'][$id];
                        $order_type       = $this->request->data['TrnReceiving']['order_type'][$id];
                        $is_lowlevel      = $this->request->data['TrnReceiving']['is_lowlevel'][$id];
                        
                        $seq++;
                        // 入荷明細作成
                        $this->TrnReceiving->create();
                        $_retDetail = $this->TrnReceiving->save(array(
                            'work_no'                 => $work_no,
                            'work_seq'                => $seq,
                            'work_date'               => $this->request->data['TrnReceivingHeader']['work_date'],
                            'work_type'               => $work_type,
                            'recital'                 => $recital,
                            'receiving_type'          => Configure::read('ReceivingType.arrival'),
                            'receiving_status'        => Configure::read('ReceivingStatus.arrival'),
                            'mst_item_unit_id'        => $mst_item_unit_id,
                            'department_id_from'      => $dep_to,
                            'department_id_to'        => $dep_from,
                            'quantity'                => $quantity,
                            'stocking_price'          => $stocking_price,
                            'trn_order_id'            => $id,
                            'stocking_close_type'     => Configure::read('StockingCloseType.none'),
                            'sales_close_type'        => Configure::read('SalesCloseType.none'),
                            'facility_close_type'     => Configure::read('FacilityCloseType.none'),
                            'is_retroactable'         => ($IsExclude == '1' ? false : true),
                            'is_deleted'              => false,
                            'creater'                 => $this->Session->read('Auth.MstUser.id'),
                            'created'                 => $now,
                            'modifier'                => $this->Session->read('Auth.MstUser.id'),
                            'modified'                => $now,
                            'trn_receiving_header_id' => $_lastId,
                            'remain_count'            => $quantity,
                            ));
                        if(false === $_retDetail) {
                            $this->rollbackTables();
                            throw new Exception('TrnReceiving登録失敗' . print_r($this->request->data,true));
                            exit;
                        }
                        
                        $_lastReceivingId = $this->TrnReceiving->getLastInsertID();
                        
                        // 発注残数更新
                        $this->TrnOrder->updateAll(array(
                            'TrnOrder.remain_count' => 'remain_count - ' . (integer)$quantity,
                            'TrnOrder.modifier'     => $this->Session->read('Auth.MstUser.id'),
                            'TrnOrder.modified'     => "'" . $now . "'",
                            ),array(
                            'TrnOrder.id' => $id,
                        ), -1);
                        
                        //発注IDを確保しておく
                        $orderIds[] = $id;

                        // 仕入れ作成
                        $this->TrnClaim->create();
                        $this->TrnClaim->save(array(
                            'department_id_from'  => $dep_to,
                            'department_id_to'    => $dep_from,
                            'mst_item_unit_id'    => $mst_item_unit_id,
                            'claim_date'          => $this->request->data['TrnReceivingHeader']['work_date'],
                            'claim_price'         => $this->getFacilityUnitPrice($round,  $stocking_price , $quantity),
                            'trn_receiving_id'    => $_lastReceivingId,
                            'stocking_close_type' => Configure::read('StockingCloseType.none'),
                            'sales_close_type'    => Configure::read('SalesCloseType.none'),
                            'facility_close_type' => Configure::read('FacilityCloseType.none'),
                            'is_deleted'          => false,
                            'is_stock_or_sale'    => '1',
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now,
                            'count'               => $quantity,
                            'unit_price'          => $stocking_price,
                            'round'               => $round,
                            'gross'               => Configure::read('GrossType.Seal'),
                            'is_not_retroactive'  => ($IsExclude == '1' ? true : false),
                            'claim_type'          => Configure::read('ClaimType.stock')
                            ));
                        
                        $last_id = $this->TrnClaim->getLastInsertID();
                        $this->createMsClaimRecode($last_id);
                        $this->createSaleClaimRecode($last_id);
                        
                        //低レベルの場合、基本包装単位の在庫を検索
                        if($is_lowlevel){
                            // いり数1の単位情報を取得する。
                            $sql = " select b.id ,a.per_unit,c.id as stock_id from mst_item_units as a inner join mst_item_units as b on a.mst_facility_item_id=b.mst_facility_item_id and b.per_unit=1 left join trn_stocks as c on b.id=c.mst_item_unit_id and c.mst_department_id=" . $dep_from . " where a.id=" . $mst_item_unit_id . " limit 1";
                            $_baseUnit = $this->MstItemUnit->query($sql);
                            
                            $_target_id = $_baseUnit[0][0]['id']; //基本単位のIDが対象シールのIDとなる
                            $_target_cnt = $_baseUnit[0][0]['per_unit']; //元の入数が入庫数の係数となる
                        } else {
                            // 通常品の場合
                            $_target_id = $mst_item_unit_id;
                            $_target_cnt = 1;
                        }
                        
                        // 在庫レコード更新
                        if($order_type == Configure::read('OrderTypes.temporary') ||
                           $order_type == Configure::read('OrderTypes.temporary_calc_notautocalc') ){
                            $req_count = 0;
                        }else{
                            $req_count = $quantity;
                        }
                        $this->TrnStock->updateAll(array(
                            'TrnStock.requested_count' => "TrnStock.requested_count - {$req_count}", // 未入荷数減
                            'TrnStock.receipted_count' => "TrnStock.receipted_count + {$quantity}",
                            'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'        => "'" . $now . "'",
                            ),array(
                                'TrnStock.id'  => $this->getStockRecode($mst_item_unit_id,$dep_from)
                            ), -1);
                        
                        if($is_lowlevel){
                            //低レベル品の場合は、未入庫をマイナス
                            $this->TrnStock->updateAll(array(
                                'TrnStock.receipted_count' => "TrnStock.receipted_count - {$quantity}",
                                //入荷の在庫数はここでは変動させない 'TrnStock.stock_count' => "TrnStock.stock_count + {$quantity}",
                                'TrnStock.modifier' => $this->Session->read('Auth.MstUser.id'),
                                'TrnStock.modified' => "'" . $now . "'",
                                ),array(
                                    'TrnStock.id'  => $this->getStockRecode($mst_item_unit_id,$dep_from)
                                ), -1);
                            
                            //基本単位の在庫を入荷入数分プラス
                            $this->TrnStock->updateAll(array(
                                'TrnStock.stock_count'          => "TrnStock.stock_count + " . $quantity * $_target_cnt,
                                'TrnStock.lowlevel_stock_count' => "TrnStock.lowlevel_stock_count + " . $quantity * $_target_cnt,
                                'TrnStock.modifier'             => $this->Session->read('Auth.MstUser.id'),
                                'TrnStock.modified'             => "'" . $now . "'",
                                ),array(
                                    'TrnStock.id'  => $this->getStockRecode($_target_id,$dep_from)
                                ), -1);
                        }
                        
                        // オペ用発注の場合
                        if($order_type == Configure::read('OrderTypes.opestock')){
                            // いり数1の単位を取得
                            $sql = " select a.per_unit , b.id , c.spare_key7 , c.spare_key8 from mst_item_units as a left join mst_item_units as b on a.mst_facility_item_id = b.mst_facility_item_id and b.per_unit = 1 left join mst_facility_items as c on c.id = a.mst_facility_item_id where a.id = " . $mst_item_unit_id ; 
                            $tmp = $this->MstItemUnit->query($sql);
                            $one_unit_id   = $tmp[0][0]['id'];
                            $base_per_unit = $tmp[0][0]['per_unit'];
                            $type_name     = $tmp[0][0]['spare_key7'];
                            $class_name    = $tmp[0][0]['spare_key8'];
                            
                            // センター倉庫:在庫レコード 未入庫数▽
                            $this->TrnStock->updateAll(array(
                                'TrnStock.receipted_count' => "TrnStock.receipted_count - {$quantity}",
                                'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                'TrnStock.modified'        => "'" . $now . "'",
                             ),array(
                                'TrnStock.id'  => $this->getStockRecode($mst_item_unit_id,$dep_from)
                             ), -1);
                            
                            $cnt = ( $quantity * $base_per_unit);
                            // オペ倉庫:在庫レコード 在庫数△
                            $this->TrnStock->updateAll(array(
                                'TrnStock.stock_count'     => "TrnStock.stock_count + {$cnt}" ,
                                'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                'TrnStock.modified'        => "'" . $now . "'",
                             ),array(
                                'TrnStock.id'  => $this->getStockRecode($one_unit_id,$dep_from)
                             ), -1);
                            
                            // 入荷レコード残数▽
                            $this->TrnReceiving->updateAll(array(
                                'TrnReceiving.remain_count'     => 0,
                                'TrnReceiving.receiving_status' => Configure::read('ReceivingStatus.stock'),
                                'TrnReceiving.modifier'         => $this->Session->read('Auth.MstUser.id'),
                                'TrnReceiving.modified'         => "'" . $now . "'",
                            ), array(
                                'TrnReceiving.id' => $_lastReceivingId
                            ), -1);

                            //入庫ヘッダ作成
                            $this->TrnStorageHeader->create();
                            $this->TrnStorageHeader->save(array(
                                'work_no'                 => $work_no,
                                'work_date'               => $this->request->data['TrnReceivingHeader']['work_date'],
                                'storage_type'            => Configure::read('StorageType.direct'),
                                'storage_status'          => Configure::read('StorageStatus.arrival'),
                                'mst_department_id'       => $dep_from,
                                'detail_count'            => $quantity,
                                'is_deleted'              => false,
                                'creater'                 => $this->Session->read('Auth.MstUser.id'),
                                'created'                 => "'" . $now . "'",
                                'modifier'                => $this->Session->read('Auth.MstUser.id'),
                                'modified'                => "'" . $now . "'",
                                'trn_receiving_header_id' => $_lastId,
                                ));
                            
                            $trnStorageHeaderid = $this->TrnStorageHeader->getLastInsertID();
                            for($i = 1 ; $i <= $cnt ; $i++){
                                $StickerNo = $this->getCenterStickerNo($this->request->data['TrnReceivingHeader']['work_date'], $this->getFacilityCode($this->Session->read('Auth.facility_id_selected')));
                                
                                // シールレコード作成
                                $this->TrnSticker->create();
                                $this->TrnSticker->save(array(
                                    'facility_sticker_no' => $StickerNo,
                                    'hospital_sticker_no' => '',
                                    'mst_item_unit_id'    => $one_unit_id,
                                    'mst_department_id'   => $dep_from,
                                    'trade_type'          => Configure::read('ClassesType.Constant'),
                                    'quantity'            => 1,
                                    'lot_no'              => '',
                                    'validated_date'      => null,
                                    'original_price'      => $stocking_price,
                                    'transaction_price'   => round($stocking_price / $base_per_unit , 2 ),
                                    'trn_order_id'        => $id,
                                    'trn_receiving_id'    => $_lastReceivingId,
                                    'buy_claim_id'        => $last_id,
                                    'is_deleted'          => false,
                                    'has_reserved'        => false,
                                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                                    'created'             => "'" . $now . "'",
                                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                                    'modified'            => "'" . $now . "'",
                                    'type_name'           => $type_name,
                                    'class_name'          => $class_name,
                                    ));
                                $trnStickerId = $this->TrnSticker->getLastInsertID();
                                
                                // 入庫明細
                                $this->TrnStorage->create();
                                $this->TrnStorage->save(array(
                                    'work_no'             => $work_no,
                                    'work_seq'            => $i,
                                    'work_date'           => $this->request->data['TrnReceivingHeader']['work_date'],
                                    'storage_type'        => Configure::read('StorageType.normal'),
                                    'storage_status'      => Configure::read('StorageStatus.arrival'),
                                    'mst_item_unit_id'    => $one_unit_id,
                                    'mst_department_id'   => $dep_from,
                                    'quantity'            => 1,
                                    'trn_sticker_id'      => $trnStickerId,
                                    'trn_receiving_id'    => $_lastReceivingId,
                                    'is_deleted'          => false,
                                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                                    'created'             => "'" . $now . "'",
                                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                                    'modified'            => "'" . $now . "'",
                                    'facility_sticker_no' => $StickerNo,
                                    'hospital_sticker_no' => '',
                                    ));
                                
                                $trnStorageId = $this->TrnStorage->getLastInsertID();
                                
                                // シールに入庫IDを刺す
                                $this->TrnSticker->updateAll(array(
                                    'TrnSticker.trn_storage_id' => $trnStorageId,
                                    ), array(
                                        'TrnSticker.id' => $trnStickerId
                                ), -1);
                                
                                // シール移動履歴
                                $this->TrnStickerRecord->create();
                                $this->TrnStickerRecord->save(array(
                                    'move_date'           => $this->request->data['TrnReceivingHeader']['work_date'],
                                    'move_seq'            => $this->getNextRecord($trnStickerId),
                                    'record_type'         => Configure::read('RecordType.storage'),
                                    'record_id'           => $trnStorageId,
                                    'trn_sticker_id'      => $trnStickerId,
                                    'mst_department_id'   => $dep_from,
                                    'facility_sticker_no' => $StickerNo,
                                    'hospital_sticker_no' => '',
                                    'is_deleted'          => false,
                                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                                    'created'             => "'" . $now . "'",
                                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                                    'modified'            => "'" . $now . "'",
                                    ));
                            }
                        }
                    }
                }
            }
            
            $errors = array();
            $errors = array_merge(
                $errors,
                is_array($this->validateErrors($this->TrnReceiving)) ? $this->validateErrors($this->TrnReceiving) : array(),
                is_array($this->validateErrors($this->TrnReceivingHeader)) ? $this->validateErrors($this->TrnReceivingHeader) : array(),
                is_array($this->validateErrors($this->TrnOrder)) ? $this->validateErrors($this->TrnOrder) : array(),
                is_array($this->validateErrors($this->TrnOrderHeader)) ? $this->validateErrors($this->TrnOrderHeader) : array(),
                is_array($this->validateErrors($this->TrnClaim)) ? $this->validateErrors($this->TrnClaim) : array(),
                is_array($this->validateErrors($this->TrnStock)) ? $this->validateErrors($this->TrnStock) : array(),
                is_array($this->validateErrors($this->TrnSticker)) ? $this->validateErrors($this->TrnSticker) : array()
                );

            if(false === empty($errors)){
                $this->TrnReceiving->rollback();
                throw new Exception(print_r($errors, true));
                exit;
            }

            //コミット直前に更新時間チェックをする
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_orders as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $orderIds). ')';
            $sql .= "     and a.modified > '" . $this->request->data['TrnReceiving']['time'] ."'";
            $sql .= "     and a.modified <> '" . $now ."'";

            $ret = $this->TrnSticker->query($sql);

            if($ret[0][0]['count']> 0 ){
                $this->rollbackTables();
                $this->Session->setFlash('ほかのユーザによって更新されています。最初からやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('search');
            }

            //コミット
            $this->commitTables();
            // 結果表示用にデータ取得
            $where = ' and b.id in (' . join(',',$this->request->data['TrnReceiving']['id']) . ') ';
            $_results = $this->getOrders($where);
            $this->set('TrnReceivingIds' , $receivingHeaderId);
            $this->Session->write('TrnReceivingIds',$receivingHeaderId);
            $this->request->data['results']  = $_results;
            $this->request->data['workDate'] = $this->request->data['TrnReceivingHeader']['work_date'];
            $this->request->data['recital']  = $this->request->data['TrnReceivingHeader']['recital'];
            $this->Session->write('data', $this->request->data);
            $this->Session->setFlash("入荷を行いました。", 'growl', array('type' => 'star'));
            $this->Session->delete('TrnOrder');

        }else{
            // トランザクショントークン不正
            // リロード or ２度押し
            $this->request->data = $this->Session->read('data');
            $this->set('TrnReceivingIds' ,  $this->Session->read('TrnReceivingIds'));
            
        }
    }


    private function rollbackTables(){
        $this->TrnReceivingHeader->rollback();
    }

    private function commitTables(){
        $this->TrnReceivingHeader->commit();
    }

    private function getOrderHeaders($where, $limit , $count=false){
        $sql  = ' select ';
        $sql .= '       a.id                     as "TrnOrderHeder__id" ';
        $sql .= '     , a.work_no                as "TrnOrderHeder__work_no" ';
        $sql .= "     , to_char(a.work_date, 'YYYY/mm/dd')";
        $sql .= '                                as "TrnOrderHeder__work_date" ';
        $sql .= '     , a.recital                as "TrnOrderHeder__recital" ';
        $sql .= '     , a.order_type             as "TrnOrderHeder__order_type" ';
        $sql .= '     , a.order_status           as "TrnOrderHeder__order_status" ';
        $sql .= '     , a.department_id_from     as "TrnOrderHeder__department_id_from" ';
        $sql .= '     , a.department_id_to       as "TrnOrderHeder__department_id_to" ';
        $sql .= '     , a.detail_count           as "TrnOrderHeder__detail_count" ';
        $sql .= '     , a.is_deleted             as "TrnOrderHeder__is_deleted" ';
        $sql .= '     , a.creater                as "TrnOrderHeder__creater" ';
        $sql .= "     , to_char(a.created, 'YYYY/mm/dd hh24:mi:ss') ";
        $sql .= '                                as "TrnOrderHeder__created" ';
        $sql .= '     , a.modifier               as "TrnOrderHeder__modifier" ';
        $sql .= '     , a.modified               as "TrnOrderHeder__modified" ';
        $sql .= '     , b.trn_order_header_id    as "TrnOrder__trn_order_header_id" ';
        $sql .= '     , j.facility_name          as "MstFacility__facility_name" ';
        $sql .= '     , k.user_name              as "MstUser__user_name" ';
        $sql .= '     , count(b.id)              as "receivable_detail_count" ';
        $sql .= '     , sum(b.remain_count)      as "remain_count"  ';
        $sql .= '   from ';
        $sql .= '     trn_order_headers as a  ';
        $sql .= '     inner join trn_orders as b  ';
        $sql .= '       on a.id = b.trn_order_header_id  ';
        $sql .= '       and b.is_deleted = false  ';
        $sql .= '       and b.remain_count > 0  ';
        $sql .= '     inner join mst_item_units as c  ';
        $sql .= '       on c.id = b.mst_item_unit_id  ';
        $sql .= '     inner join mst_facility_items as d  ';
        $sql .= '       on c.mst_facility_item_id = d.id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on d.mst_dealer_id = e.id  ';
        $sql .= '     inner join mst_unit_names as f  ';
        $sql .= '       on c.mst_unit_name_id = f.id  ';
        $sql .= '     inner join mst_unit_names as g  ';
        $sql .= '       on c.per_unit_name_id = g.id  ';
        $sql .= '     inner join mst_departments as h  ';
        $sql .= '       on a.department_id_to = h.id  ';
        $sql .= '     inner join mst_departments as i  ';
        $sql .= '       on a.department_id_from = i.id  ';
        $sql .= '     inner join mst_facilities as j  ';
        $sql .= '       on h.mst_facility_id = j.id  ';
        $sql .= '     inner join mst_users as k  ';
        $sql .= '       on a.creater = k.id  ';
        $sql .= '     left join trn_edi_receivings as l  ';
        $sql .= '       on l.trn_order_id = b.id  ';
        $sql .= '       and l.is_deleted = false  ';
        $sql .= '     left join (  ';
        $sql .= '       select ';
        $sql .= '             x.trn_edi_receiving_id ';
        $sql .= '           , sum(x.quantity)  ';
        $sql .= '         from ';
        $sql .= '           trn_edi_shippings as x  ';
        $sql .= '         where ';
        $sql .= '           x.is_deleted = false  ';
        $sql .= '         group by ';
        $sql .= '           x.trn_edi_receiving_id ';
        $sql .= '     ) as x  ';
        $sql .= '       on x.trn_edi_receiving_id = l.id  ';
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .= '     and a.order_status in (' . Configure::read('OrderStatus.ordered') . ',  ' . Configure::read('OrderStatus.partLoaded') . ')  ';
        $sql .= '     and b.remain_count > 0 ';
        $sql .= '     and b.order_status in (' . Configure::read('OrderStatus.ordered') . ',  ' . Configure::read('OrderStatus.partLoaded') . ')  ';
        $sql .= '     and j.facility_type = ' . Configure::read('FacilityType.supplier');
        $sql .= '     and i.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and a.order_type not in (' . Configure::read('OrderTypes.replenish') . ', ' . Configure::read('OrderTypes.direct') . ')  ';
        $sql .= '     and a.is_deleted = false  ';
        $sql .= $where;
        $sql .= '   group by ';
        $sql .= '     a.id ';
        $sql .= '     , a.work_no ';
        $sql .= '     , a.work_date ';
        $sql .= '     , a.recital ';
        $sql .= '     , a.order_type ';
        $sql .= '     , a.order_status ';
        $sql .= '     , a.department_id_from ';
        $sql .= '     , a.department_id_to ';
        $sql .= '     , a.detail_count ';
        $sql .= '     , a.is_deleted ';
        $sql .= '     , a.creater ';
        $sql .= '     , a.created ';
        $sql .= '     , a.modifier ';
        $sql .= '     , a.modified ';
        $sql .= '     , b.trn_order_header_id ';
        $sql .= '     , j.facility_name ';
        $sql .= '     , k.user_name  ';
        $sql .= '   having ';
        $sql .= '     sum(b.remain_count) > 0  ';
        $sql .= '   order by ';
        $sql .= '     a.id asc  ';
        
        if($count){
            $this->set('max' , $this->getMaxCount($sql , 'TrnOrderHeader'));
        }
        $sql .= "limit {$limit}";
        return $this->TrnOrderHeader->query($sql);
    }


    /**
     * Use this with php function: array_walk_recursive
     * @param mixed $item
     * @param string $key
     * @param array $res
     * @access public
     * @static
     */
    public static function BuildSearchResult($item, $key, $res){
        //HACK:  最上位の配列値を優先するようにしてます・・
        if(false === isset($res[$key])){
            $res[$key] = $item;
        }
    }



    /**
     * 発注明細検索
     * @param array $params
     * @return array
     * @access private
     */
    private function getOrders($where){

        $sql  = ' select ';
        $sql .= '       b.id                        as "TrnOrder__id" ';
        $sql .= '     , b.trn_order_header_id       as "TrnOrder__trn_order_header_id"';
        $sql .= '     , b.mst_item_unit_id          as "TrnOrder__mst_item_unit_id"';
        $sql .= '     , b.order_type                as "TrnOrder__order_type"';
        $sql .= '     , d.is_lowlevel               as "TrnOrder__is_lowlevel"';
        $sql .= "     , to_char(b.work_date,'YYYY/mm/dd') ";
        $sql .= '                                   as "TrnOrder__work_date" ';
        $sql .= '     , b.work_no                   as "TrnOrder__work_no" ';
        $sql .= '     , b.remain_count              as "TrnOrder__remain_count"';
        $sql .= '     , ( case ';
        foreach(Configure::read('OrderTypes.orderTypeName') as $k => $v ) {
            $sql .= "         when b.order_type = '" .  $k . "' then '" . $v . "'";
        }
        $sql .= "       else '' ";
        $sql .= '       end )                       as "TrnOrder__order_type_name"';
        $sql .= '     , c.mst_facility_id ';
        $sql .= '     , d.mst_facility_id ';
        $sql .= '     , d.internal_code             as "TrnOrder__internal_code"';
        $sql .= '     , d.item_name                 as "TrnOrder__item_name"';
        $sql .= '     , d.standard                  as "TrnOrder__standard"';
        $sql .= '     , d.item_code                 as "TrnOrder__item_code"';
        $sql .= '     , d.jan_code                  as "TrnOrder__jan_code"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when c.per_unit = 1  ';
        $sql .= '         then f.unit_name  ';
        $sql .= "         else f.unit_name || '(' || c.per_unit || g.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                             as "TrnOrder__unit_name" ';
        $sql .= '     , e.dealer_name               as "TrnOrder__dealer_name"'; 
        $sql .= '     , i.id                        as "TrnOrder__facility_id" ';
        $sql .= '     , i.facility_name             as "TrnOrder__facility_name" ';
        $sql .= '     , k.transaction_price         as "TrnOrder__transaction_price"';
        $sql .= '   from ';
        $sql .= '     trn_order_headers as a  ';
        $sql .= '     inner join trn_orders as b  ';
        $sql .= '       on a.id = b.trn_order_header_id  ';
        $sql .= '     inner join mst_item_units as c  ';
        $sql .= '       on c.id = b.mst_item_unit_id  ';
        $sql .= '     inner join mst_facility_items as d  ';
        $sql .= '       on c.mst_facility_item_id = d.id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on d.mst_dealer_id = e.id  ';
        $sql .= '     inner join mst_unit_names as f  ';
        $sql .= '       on c.mst_unit_name_id = f.id  ';
        $sql .= '     inner join mst_unit_names as g  ';
        $sql .= '       on c.per_unit_name_id = g.id  ';
        $sql .= '     inner join mst_departments as h  ';
        $sql .= '       on b.department_id_to = h.id  ';
        $sql .= '     inner join mst_facilities as i  ';
        $sql .= '       on h.mst_facility_id = i.id  ';
        $sql .= '     inner join mst_users as j  ';
        $sql .= '       on b.modifier = j.id  ';
        $sql .= '     left join mst_transaction_configs as k ';
        $sql .= '       on k.mst_item_unit_id = c.id ';
        $sql .= '      and k.partner_facility_id = i.id ';
        $sql .= '      and k.start_date <= b.work_date ';
        $sql .= '      and k.end_date > b.work_date';
        
        $sql .= '     left join trn_edi_receivings as y  ';
        $sql .= '       on y.trn_order_id = b.id  ';
        $sql .= '       and y.is_deleted = false  ';
        $sql .= '     left join (  ';
        $sql .= '       select ';
        $sql .= '             x.trn_edi_receiving_id ';
        $sql .= '           , sum(x.quantity)  ';
        $sql .= '         from ';
        $sql .= '           trn_edi_shippings as x  ';
        $sql .= '         where ';
        $sql .= '           x.is_deleted = false  ';
        $sql .= '         group by ';
        $sql .= '           x.trn_edi_receiving_id ';
        $sql .= '     ) as x  ';
        $sql .= '       on x.trn_edi_receiving_id = y.id  ';
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .= $where;
        $sql .= '     and a.order_status in (' . Configure::read('OrderStatus.ordered') . ' , ' . Configure::read('OrderStatus.partLoaded') .' )  ';
        $sql .= '     and b.order_status in (' . Configure::read('OrderStatus.ordered') . ' , ' . Configure::read('OrderStatus.partLoaded') .' )  ';
        $sql .= '     and i.facility_type = ' . Configure::read('FacilityType.supplier');
        
        $sql .= '     and b.is_deleted = false  ';
        $sql .= '   order by ';
        $sql .= '     b.trn_order_header_id ';
        $sql .= '     , d.mst_owner_id ';
        $sql .= '     , e.dealer_name ';
        $sql .= '     , d.item_name ';
        $sql .= '     , d.item_code ';
        $result =  $this->TrnOrder->query($sql);
        return $result;
    }

    public function seal(){
        App::import('Sanitize');
        $_sid = array();
        
        $sql  = ' select ';
        $sql .= '   d.id  ';
        $sql .= ' from ';
        $sql .= '   trn_receiving_headers as a  ';
        $sql .= '   left join trn_receivings as b  ';
        $sql .= '     on b.trn_receiving_header_id = a.id  ';
        $sql .= '   left join trn_storages as c  ';
        $sql .= '     on c.trn_receiving_id = b.id  ';
        $sql .= '   left join trn_stickers as d  ';
        $sql .= '     on d.id = c.trn_sticker_id  ';
        $sql .= ' where ';
        $sql .= '   a.id in ( ' . join(',',$this->request->data['TrnReceivingHeader']['id']) . ')' ;

        $ret = $this->TrnReceiving->query($sql);
        foreach($ret as $r ){
            $_sid[] = $r[0]['id'];
        }
        
        // コストシールデータ取得
        $records = $this->Stickers->getCostStickers($_sid);
        // 空なら初期化
        if(!$records){ $records = array();}
        
        $this->layout = 'xml/default';
        $this->set('records', $records);
        $this->render('/stickerissues/cost_sticker');
    }
}
?>
