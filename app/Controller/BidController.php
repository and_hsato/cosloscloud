<?php
/**
 * BidController
 *　入札
 */
class BidController extends AppController {

  /**
   * @var $name
   */
  var $name = 'Bid';

  /**
   * @var array $uses
   */
    var $uses = array(
            'MstClass',
            'MstDepartment',
            'MstFacility',
            'MstFacilityItem',
            'MstUser',
            'MstTransactionConfig',
            'TrnInformation',
            'TrnSticker',
            'TrnStock',
            'TrnSetStock',
            'TrnSetStockHeader',
            'TrnProposalHeader',
            'TrnProposal',
            'TrnRfpHeader',
            'TrnRfp',
            'TrnRequestedRfp',
            'TransactionManager',
    );

    /**
     * @var bool $scaffold
     */
    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'seal';
        $this->Auth->allowedActions[] = 'report';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        //CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }

    }


    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker' );

    /**
     * @var array $components
     */
    var $components = array(
            'xml',
            'Common',
            'Stickers',
            'RequestHandler',
            'CsvWriteUtils',
            'CsvReadUtils',
            'Rfp',
            'Mail',
    );

    /**
     *
     */
    function index (){
        $this->redirect('/');
    }
    
    /**
     * 見積依頼 商品選択
     */
    function rfp_item_select(){
        $this->setRoleFunction(109); //見積依頼

        // カート式で商品を選択
        App::import('Sanitize');
        
        $SearchResult = array();
        $CartSearchResult = array();

        //初回時は検索を行わない
        if (isset($this->request->data['add_search']['is_search'])) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $limit = '' ;
            if(isset($this->request->data['FacilityItems']['limit'])){
                $limit .= 'limit ' . $this->request->data['FacilityItems']['limit'];
            }
            $order =' order by a.internal_code ,b.per_unit ';
            $where = '';
            $where .=' and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
            $where .=' and a.item_type = ' . Configure::read('Items.item_types.normalitem'); // 通常品のみ
            $where .=' and a.is_deleted = false '; //施設採用品で削除されているものは除外
            $where .=' and b.is_deleted = false '; //包装単位で適用が外れているものは除外

            $where2 = $where;

            if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and b.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 .= " and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 .= " and 0 = 1 ";
            }

            if(isset($this->request->data['MstFacilityItems']['internal_code']) && $this->request->data['MstFacilityItems']['internal_code'] !== ''){
                $where .=" and a.internal_code ilike '%" .$this->request->data['MstFacilityItems']['internal_code']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['standard']) && $this->request->data['MstFacilityItems']['standard'] !== '' ){
                $where .=" and a.standard ilike '%" .$this->request->data['MstFacilityItems']['standard']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['item_name']) && $this->request->data['MstFacilityItems']['item_name'] !== ''){
                $where .=" and a.item_name ilike '%" .$this->request->data['MstFacilityItems']['item_name']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['item_code']) && $this->request->data['MstFacilityItems']['item_code'] !== ''){
                $where .=" and a.item_code ilike '%" .$this->request->data['MstFacilityItems']['item_code']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['dealer_name']) && $this->request->data['MstFacilityItems']['dealer_name'] !== ''){
                $where .=" and c.dealer_name ilike '%" .$this->request->data['MstFacilityItems']['dealer_name']."%'";
            }
            //JAN(前方一致)
            if(isset($this->request->data['MstFacilityItems']['jan_code']) && $this->request->data['MstFacilityItems']['jan_code'] !== ''){
                $where .=" and a.jan_code ilike '" .$this->request->data['MstFacilityItems']['jan_code']."%'";
            }
            
            // 代表仕入のみ
            if(isset($this->request->data['MstFacilityItems']['is_main']) && $this->request->data['MstFacilityItems']['is_main'] == '1'){
                $where .=" and f.id is not null ";
            }
            
            // 見積依頼可能商品のみ
            $isBidOnly = $this->request->data['MstFacilityItems']['is_bid_only'];
            
            // 検索（画面上）用一覧取得
            $SearchResult = $this->MstFacilityItem->query($this->searchFacilityItems($where , $order , $limit, $isBidOnly));

            // カート（画面下）用一覧取得
            $CartSearchResultTmp = $this->MstFacilityItem->query($this->searchFacilityItems($where2 , $order));

            if(!empty($this->request->data['cart']['tmpid'])){
                //カートに追加した順に並べなおす。
                foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
                    foreach($CartSearchResultTmp as $tmp){
                        if($tmp['MstFacilityItems']['id'] == $id){
                            $CartSearchResult[] = $tmp;
                            break;
                        }
                    }
                }
            }
            $this->request->data = $data;
            $this->set('data' , $this->request->data);
        }
        if (!isset($this->request->data['MstFacilityItems']['is_bid_only'])) {
            // 初期値はON (常にONにする場合はView上のチェックボックスをhiddenにしてください)
            $this->request->data['MstFacilityItems']['is_bid_only'] = 1;
        }
        $this->set('SearchResult' , $SearchResult);
        $this->set('CartSearchResult' , $CartSearchResult);
    }
    
    /**
     * 見積依頼 確認
     */
    function rfp_item_select_confirm(){
        // 選択商品情報を表示
        $where = " and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
        $SearchResultTmp = $this->MstFacilityItem->query($this->searchFacilityItems($where));
        $SearchResult = array();
        
        if(!empty($this->request->data['cart']['tmpid'])){
            //カートに追加した順に並べなおす。
            foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
                foreach($SearchResultTmp as $tmp){
                    if($tmp['MstFacilityItems']['id'] == $id){
                        $SearchResult[] = $tmp;
                        break;
                    }
                }
            }
        }
        
        $this->set('SearchResult' , $SearchResult);
        
        $edis = $this->Rfp->findEdiFacilities($this->Session->read('Auth.facility_id_selected'));
        $this->set('edis', $edis);
    }
    
    /**
     * 見積依頼 結果
     */
    function rfp_item_select_result() {
        $this->request->data['work_no'] = $this->setWorkNo4Header(date('Ymd'), "30");
        $this->request->data['login_user_id'] = $this->Session->read('Auth.MstUser.id');
        $this->request->data['with_send_mail'] = true;
        $result = $this->Rfp->doRfpTransaction($this->request->data);
        if (empty($result)) {
            $this->Session->setFlash('見積依頼処理中にエラーが発生したため処理を中止しました。', 'growl', array('type' => 'error'));
            $this->redirect('rfp_item_select');
        }
        
        $result = $this->Rfp->findRfpItems($result['TrnRfpHeader']);
        $this->set('result' , $result);
    }

    /**
     * 見積取込 初期
     */
    function proposal(){
        $this->setRoleFunction(110); //見積取込
    }
      
    /**
     * ファイルアップロード
     */
    public function upload(){
        $err_flg = false ;
        // Excel 取込
        $_upfile = $this->request->data['result']['tmp_name'];
        $_fileName = realpath( TMP ).DS.date('YmdHi').'.xls';
        if (is_uploaded_file($_upfile)){
            move_uploaded_file($_upfile, mb_convert_encoding($_fileName, 'UTF-8', 'SJIS-win'));
            chmod($_fileName, 0644);
        }else{
            $this->Session->setFlash('ファイルが読み込めませんでした。ファイルを確認してください。', 'growl', array('type'=>'error') );
            $this->redirect('proposal');
            exit(0);
        }
        
        //テンプレートファイルフルパス
        $PHPExcel = $this->createExcelObj($_fileName);
        if($PHPExcel === false){
            // Excelファイル読み込み失敗
            $this->Session->setFlash('ファイルが読み込めませんでした。ファイル形式を確認してください。', 'growl', array('type'=>'error') );
            $this->redirect('/bid/proposal');
        }
        $PHPExcel->setActiveSheetIndex(0);  //一番左のシート
        $sheet = $PHPExcel->getActiveSheet();

        // ID取得
        $id = $sheet->getCell('B1')->getValue();
        $dataArray = array();
        $num = 0;
        // ループ
        while(true){
            $cell =  (string)($num+3);
            if($sheet->getCell('A' . $cell )->getValue() != "" ){
                if($sheet->getCell('A' . $cell )->getValue() != ""){
                    $price = $sheet->getCell('H' . $cell )->getValue(); // 金額
                    $seq = $sheet->getCell('A' . $cell )->getValue(); // seq

                    /* 金額フォーマットチェック */
                    // 半角化 , カンマ削除
                    $price = str_replace(",", "", mb_convert_kana($price,"a","utf-8"));
                    
                    if(preg_match("/^[0-9]{1,9}(\.[0-9]{1,2})?$/", $price) == 1){
                        // チェックOKの場合
                        $dataArray[$seq] = $price; 
                    }
                }
            }else{
                // Noが空になったのでループを抜ける
                break;
            }
            
            $num++;
        }
        
        //アップロード一時ファイルを削除
        unlink($_fileName);
        
        // 情報取得
        $sql = "select a.work_no , to_char(a.limit_date , 'yyyy/mm/dd') as limit_date from trn_rfp_headers as a where a.id = '" . $id . "' and a.is_deleted = false ";
        $ret = $this->TrnRfpHeader->query($sql);
        
        if(count($ret) == 0){
            // 有効な見積依頼が存在しない場合、エラー
            $this->Session->setFlash('有効な見積依頼がありません。', 'growl', array('type'=>'error') );
            $err_flg = true;
        }else{
            // 作業番号
            $work_no = $ret[0][0]['work_no'];
            // 締切日
            $limit_date = $ret['0']['0']['limit_date'];
            
            // 締切日チェック
            if($limit_date < date('y/m/d')){
                // 締切日を過ぎている場合、取込エラー
                $this->Session->setFlash('締切日が過ぎています。', 'growl', array('type'=>'error') );
                $err_flg = true;
            }
            
            // 入力件数チェック
            if(count($dataArray) == 0 ){
                // 金額入力 0 件の場合
                $this->Session->setFlash('金額が入力されている商品がありません。', 'growl', array('type'=>'error') );
                $err_flg = true;
            }
        }
        if($err_flg){
            // 異常終了の場合、エラーを表示して初期画面に遷移
            $this->redirect('/bid/proposal');
        }else{
            // 必要情報をセッションに格納
            $this->Session->write('bid.id', $id);
            $this->Session->write('bid.data', $dataArray);

            // 正常終了の場合、確認画面に遷移
            $this->redirect('/bid/proposal_confirm');
        }
    }
    
    /**
     * 見積取込 確認
     */
    function proposal_confirm(){
        // 必要情報をセッションから取得
        $id = $this->Session->read('bid.id');
        $array =  $this->Session->read('bid.data');
        $i = 0;
        
        foreach($array as $seq => $price){
            $sql  = ' select ';
            $sql .= '     a.id            as "MstFacilityItems__id"  ';
            $sql .= '   , c.internal_code as "MstFacilityItems__internal_code"';
            $sql .= '   , c.item_name     as "MstFacilityItems__item_name"'; 
            $sql .= '   , c.item_code     as "MstFacilityItems__item_code"';
            $sql .= '   , c.standard      as "MstFacilityItems__standard"';
            $sql .= '   , c.jan_code      as "MstFacilityItems__jan_code"';
            $sql .= '   , (  ';
            $sql .= '     case  ';
            $sql .= '       when b.per_unit = 1  ';
            $sql .= '       then b1.unit_name  ';
            $sql .= "       else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
            $sql .= '       end ';
            $sql .= '   )                 as "MstFacilityItems__unit_name"  ';
            $sql .= '   ,  0              as "MstFacilityItems__price"';
            $sql .= ' from ';
            $sql .= '   trn_rfps as a  ';
            $sql .= '   left join mst_item_units as b  ';
            $sql .= '     on b.id = a.mst_item_unit_id  ';
            $sql .= '   left join mst_unit_names as b1  ';
            $sql .= '     on b1.id = b.mst_unit_name_id  ';
            $sql .= '   left join mst_unit_names as b2  ';
            $sql .= '     on b2.id = b.per_unit_name_id  ';
            $sql .= '   left join mst_facility_items as c  ';
            $sql .= '     on c.id = b.mst_facility_item_id  ';
            $sql .= ' where ';
            $sql .= '   a.trn_rfp_header_id = ' . $id;
            $sql .= '   and a.work_seq = ' .$seq;
            
            $r = $this->TrnRfp->query($sql , false , false);
            
            $ret[$i] = $r[0];
            $ret[$i]['MstFacilityItems']['price'] = $price;
            $i++;
        }
        $this->set('result' , $ret);

    }
    
    
    /**
     * 見積取込 結果
     */
    function proposal_result(){
        $now = date('Y/m/d H:i:s');
        // トランザクション開始
        $this->TrnProposalHeader->begin();
        $work_no = $this->setWorkNo4Header( date('Ymd') , "31");
        
        
        //見積ヘッダ
        $TrnProposalHeader = array(
            'TrnProposalHeader'=> array(
                'work_no'      => $work_no,
                'work_date'    => date('Y/m/d'),
                'recital'      => null,
                'supplier_id'  => $this->Session->read('Auth.facility_id_selected'), // 業者ID
                'detail_count' => count($this->request->data['rfp']['id']),
                'is_deleted'   => false,
                'creater'      => $this->Session->read('Auth.MstUser.id'),
                'created'      => $now,
                'modifier'     => $this->Session->read('Auth.MstUser.id'),
                'modified'     => $now
                )
            );
        
        $this->TrnProposalHeader->create();
        // SQL実行
        if (!$this->TrnProposalHeader->save($TrnProposalHeader)) {
            $this->TrnProposalHeader->rollback();
            $this->Session->setFlash('見積登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        $h_id = $this->TrnProposalHeader->getLastInsertID();
        $i=1;
        foreach($this->request->data['rfp']['id'] as $id=>$price){
            // 見積明細
            $TrnProposal = array(
                'TrnProposal'=> array(
                    'work_no'                 => $work_no,
                    'work_seq'                => $i,
                    'work_date'               => date('Y/m/d'),
                    'work_type'               => null,
                    'recital'                 => $this->request->data['rfp'][$id]['recital'],
                    'trn_proposal_headers_id' => $h_id, 
                    'trn_rfp_id'              => $id,
                    'price'                   => $price,
                    'is_deleted'              => false,
                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                    'created'                 => $now,
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'modified'                => $now
                    )
                );
            
            $this->TrnProposal->create();
            // SQL実行
            if (!$this->TrnProposal->save($TrnProposal)) {
                $this->TrnProposal->rollback();
                $this->Session->setFlash('見積登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
            if( $i == 1 ){
                $sql = ' select trn_rfp_header_id from trn_rfps where id = ' . $id ;
                $ret = $this->TrnRfp->query($sql);
            }
            $i++;
        }
        
        // 見積依頼と紐づくお知らせを削除

        $this->TrnInformation->create();
        $ret = $this->TrnInformation->updateAll(
            array(
                'TrnInformation.is_deleted' => "'true'",
                'TrnInformation.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnInformation.modified'   => "'" . $now . "'"
                ),
            array(
                'TrnInformation.trn_rfp_header_id'  => $ret[0][0]['trn_rfp_header_id'] , // 見積依頼情報ID
                ),
            -1
            );
        if(!$ret){
            $this->TrnRfp->rollback();
            $this->Session->setFlash('見積登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        
        $this->TrnProposalHeader->commit();
        
        // 結果画面表示
        $sql  = ' select ';
        $sql .= '     e.internal_code as "MstFacilityItems__internal_code"';
        $sql .= '   , e.item_name     as "MstFacilityItems__item_name"';
        $sql .= '   , e.item_code     as "MstFacilityItems__item_code"';
        $sql .= '   , e.standard      as "MstFacilityItems__standard"';
        $sql .= '   , e.jan_code      as "MstFacilityItems__jan_code"';
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when d.per_unit = 1  ';
        $sql .= '       then d1.unit_name  ';
        $sql .= "       else d1.unit_name || '(' || d.per_unit || d2.unit_name || ')'  ";
        $sql .= '       end ';
        $sql .= '   )                 as "MstFacilityItems__unit_name"';
        $sql .= '   , b.price         as "MstFacilityItems__price"';
        $sql .= '   , b.recital       as "MstFacilityItems__recital"';
        
        $sql .= ' from ';
        $sql .= '   trn_proposal_headers as a  ';
        $sql .= '   left join trn_proposals as b  ';
        $sql .= '     on b.trn_proposal_headers_id = a.id  ';
        $sql .= '   left join trn_rfps as c  ';
        $sql .= '     on c.id = b.trn_rfp_id  ';
        $sql .= '   left join mst_item_units as d  ';
        $sql .= '     on d.id = c.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as d1  ';
        $sql .= '     on d1.id = d.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as d2  ';
        $sql .= '     on d2.id = d.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as e  ';
        $sql .= '     on e.id = d.mst_facility_item_id  ';
        $sql .= ' where ';
        $sql .= '   a.id = ' . $h_id;
        $result = $this->TrnProposalHeader->query($sql , false , false );
        $this->set('result' , $result);
    }
    
    /**
     * 見積確定 一覧
     */
    function decision_search(){
        $this->setRoleFunction(111); //見積確定
        $result = array();
        
        // 取込済、見積データ検索
        if(isset($this->request->data['Bid']['is_search'])){
            $sql  = '  select ';
            $sql .= '     a.id              as "Bid__id"';
            $sql .= '   , a.work_no         as "Bid__work_no"';
            $sql .= "   , to_char(a.limit_date , 'YYYY/mm/dd')";
            $sql .= '                       as "Bid__limit_date"';
            $sql .= '   , d.user_name       as "Bid__user_name"';
            $sql .= "   , to_char(a.created , 'YYYY/mm/dd HH24:MI') ";
            $sql .= '                       as "Bid__created"';
            $sql .= '   , a.recital         as "Bid__recital"';
            $sql .= '   , a.detail_count    as "Bid__detail_count"';
            $sql .= '   , x.count           as "Bid__count"';
            $sql .= ' from ';
            $sql .= '   trn_rfp_headers as a  ';
            $sql .= '   left join trn_rfps as b  ';
            $sql .= '     on b.trn_rfp_header_id = a.id  ';
            $sql .= '   left join mst_item_units as c  ';
            $sql .= '     on c.id = b.mst_item_unit_id  ';
            $sql .= '   left join mst_users as d  ';
            $sql .= '     on d.id = a.creater  ';

            $sql .= '   left join (  ';
            $sql .= '     select ';
            $sql .= '         b.trn_rfp_header_id ';
            $sql .= '       , count(*)         as count  ';
            $sql .= '     from ';
            $sql .= '       trn_proposals as a  ';
            $sql .= '       left join trn_rfps as b  ';
            $sql .= '         on a.trn_rfp_id = b.id  ';
            $sql .= '     group by ';
            $sql .= '       b.trn_rfp_header_id ';
            $sql .= '   ) as x  ';
            $sql .= '     on x.trn_rfp_header_id = a.id  ';
            
            $sql .= ' where ';
            $sql .= '   c.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
            // 見積番号
            if($this->request->data['Bid']['work_no'] != "" ){
                $sql .= " and a.work_no = '" . $this->request->data['Bid']['work_no'] . "'";
            }
            // 締切日From
            if($this->request->data['Bid']['limit_date_from'] != "" ){
                $sql .= " and a.limit_date >= '" . $this->request->data['Bid']['limit_date_from'] . "'";
            }
            
            // 締切日To
            if($this->request->data['Bid']['limit_date_to'] != "" ){
                $sql .= " and a.limit_date <= '" . $this->request->data['Bid']['limit_date_to'] . "'";
            }
            
            // 締切済みのみ表示
            if($this->request->data['Bid']['limit_flg'] == 1){
                $sql .= " and a.limit_date <= '" . date('Y/m/d') . "'";
            }
            $sql .= ' and a.is_deleted = false ';
            $sql .= ' and b.is_deleted = false ';
            
            $sql .= ' group by ';
            $sql .= '   a.id ';
            $sql .= '   , a.work_no ';
            $sql .= '   , a.limit_date ';
            $sql .= '   , d.user_name ';
            $sql .= '   , a.created ';
            $sql .= '   , a.detail_count ';
            $sql .= '   , a.recital';
            $sql .= '   , x.count';
            $sql .= ' order by ';
            $sql .= '   a.work_no ';
            
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnRfpHeader'));
            if(!is_null($this->request->data['limit'])){
                $sql .= ' limit '.  $this->request->data['limit'];
            }
            
            $result = $this->TrnRfpHeader->query($sql);
        }else{
            $this->request->data['Bid']['limit_flg'] = true;
        }
        $this->set('result' , $result);
    }
    
    /**
     * 見積確定 確認
     */
    function decision_confirm(){
        // 選択した見積の明細情報を表示

        $sql  = ' select ';
        $sql .= '     b.id                     as "Bid__id"';
        $sql .= '   , d.internal_code          as "Bid__internal_code"';
        $sql .= '   , d.item_name              as "Bid__item_name"';
        $sql .= '   , d.item_code              as "Bid__item_code"';
        $sql .= '   , d.standard               as "Bid__standard"';
        $sql .= '   , d.jan_code               as "Bid__jan_code"';
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when c.per_unit = 1  ';
        $sql .= '       then c1.unit_name  ';
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '       end ';
        $sql .= '   )                          as "Bid__unit_name" ';
        $sql .= '   , (  ';
        $sql .= '     select ';
        $sql .= '         x1.id  ';
        $sql .= '     from ';
        $sql .= '       trn_proposals as x1  ';
        $sql .= '       left join trn_proposal_headers as x2  ';
        $sql .= '         on x2.id = x1.trn_proposal_headers_id  ';
        $sql .= '       left join mst_facilities as x3  ';
        $sql .= '         on x3.id = x2.supplier_id  ';
        $sql .= '     where ';
        $sql .= '       x1.trn_rfp_id = b.id  ';
        $sql .= '       and x1.is_deleted = false  ';
        $sql .= '     order by ';
        $sql .= '       x1.price ';
        $sql .= '       , x1.id  ';
        $sql .= '     limit ';
        $sql .= '       1 ';
        $sql .= '   )                          as "Bid__proposal_id1" ';
        $sql .= '   , (  ';
        $sql .= '     select ';
        $sql .= '         x1.price  ';
        $sql .= '     from ';
        $sql .= '       trn_proposals as x1  ';
        $sql .= '       left join trn_proposal_headers as x2  ';
        $sql .= '         on x2.id = x1.trn_proposal_headers_id  ';
        $sql .= '       left join mst_facilities as x3  ';
        $sql .= '         on x3.id = x2.supplier_id  ';
        $sql .= '     where ';
        $sql .= '       x1.trn_rfp_id = b.id  ';
        $sql .= '       and x1.is_deleted = false  ';
        $sql .= '     order by ';
        $sql .= '       x1.price ';
        $sql .= '       , x1.id  ';
        $sql .= '     limit ';
        $sql .= '       1 ';
        $sql .= '   )                          as "Bid__proposal_price1" ';
        $sql .= '   , (  ';
        $sql .= '     select ';
        $sql .= '         x3.facility_name  ';
        $sql .= '     from ';
        $sql .= '       trn_proposals as x1  ';
        $sql .= '       left join trn_proposal_headers as x2  ';
        $sql .= '         on x2.id = x1.trn_proposal_headers_id  ';
        $sql .= '       left join mst_facilities as x3  ';
        $sql .= '         on x3.id = x2.supplier_id  ';
        $sql .= '     where ';
        $sql .= '       x1.trn_rfp_id = b.id  ';
        $sql .= '       and x1.is_deleted = false  ';
        $sql .= '     order by ';
        $sql .= '       x1.price ';
        $sql .= '       , x1.id  ';
        $sql .= '     limit ';
        $sql .= '       1 ';
        $sql .= '   )                          as "Bid__proposal_supplier1" ';
        $sql .= '   , (  ';
        $sql .= '     select ';
        $sql .= '         x1.id  ';
        $sql .= '     from ';
        $sql .= '       trn_proposals as x1  ';
        $sql .= '       left join trn_proposal_headers as x2  ';
        $sql .= '         on x2.id = x1.trn_proposal_headers_id  ';
        $sql .= '       left join mst_facilities as x3  ';
        $sql .= '         on x3.id = x2.supplier_id  ';
        $sql .= '     where ';
        $sql .= '       x1.trn_rfp_id = b.id  ';
        $sql .= '       and x1.is_deleted = false  ';
        $sql .= '     order by ';
        $sql .= '       x1.price ';
        $sql .= '       , x1.id offset 1  ';
        $sql .= '     limit ';
        $sql .= '       1 ';
        $sql .= '   )                          as "Bid__proposal_id2" ';
        $sql .= '   , (  ';
        $sql .= '     select ';
        $sql .= '         x1.price  ';
        $sql .= '     from ';
        $sql .= '       trn_proposals as x1  ';
        $sql .= '       left join trn_proposal_headers as x2  ';
        $sql .= '         on x2.id = x1.trn_proposal_headers_id  ';
        $sql .= '       left join mst_facilities as x3  ';
        $sql .= '         on x3.id = x2.supplier_id  ';
        $sql .= '     where ';
        $sql .= '       x1.trn_rfp_id = b.id  ';
        $sql .= '       and x1.is_deleted = false  ';
        $sql .= '     order by ';
        $sql .= '       x1.price ';
        $sql .= '       , x1.id offset 1  ';
        $sql .= '     limit ';
        $sql .= '       1 ';
        $sql .= '   )                          as "Bid__proposal_price2" ';
        $sql .= '   , (  ';
        $sql .= '     select ';
        $sql .= '         x3.facility_name  ';
        $sql .= '     from ';
        $sql .= '       trn_proposals as x1  ';
        $sql .= '       left join trn_proposal_headers as x2  ';
        $sql .= '         on x2.id = x1.trn_proposal_headers_id  ';
        $sql .= '       left join mst_facilities as x3  ';
        $sql .= '         on x3.id = x2.supplier_id  ';
        $sql .= '     where ';
        $sql .= '       x1.trn_rfp_id = b.id  ';
        $sql .= '       and x1.is_deleted = false  ';
        $sql .= '     order by ';
        $sql .= '       x1.price ';
        $sql .= '       , x1.id offset 1  ';
        $sql .= '     limit ';
        $sql .= '       1 ';
        $sql .= '   )                          as "Bid__proposal_supplier2" ';
        $sql .= '   , (  ';
        $sql .= '     select ';
        $sql .= '         x1.id  ';
        $sql .= '     from ';
        $sql .= '       trn_proposals as x1  ';
        $sql .= '       left join trn_proposal_headers as x2  ';
        $sql .= '         on x2.id = x1.trn_proposal_headers_id  ';
        $sql .= '       left join mst_facilities as x3  ';
        $sql .= '         on x3.id = x2.supplier_id  ';
        $sql .= '     where ';
        $sql .= '       x1.trn_rfp_id = b.id  ';
        $sql .= '       and x1.is_deleted = false  ';
        $sql .= '     order by ';
        $sql .= '       x1.price ';
        $sql .= '       , x1.id offset 2  ';
        $sql .= '     limit ';
        $sql .= '       1 ';
        $sql .= '   )                          as "Bid__proposal_id3" ';
        $sql .= '   , (  ';
        $sql .= '     select ';
        $sql .= '         x1.price  ';
        $sql .= '     from ';
        $sql .= '       trn_proposals as x1  ';
        $sql .= '       left join trn_proposal_headers as x2  ';
        $sql .= '         on x2.id = x1.trn_proposal_headers_id  ';
        $sql .= '       left join mst_facilities as x3  ';
        $sql .= '         on x3.id = x2.supplier_id  ';
        $sql .= '     where ';
        $sql .= '       x1.trn_rfp_id = b.id  ';
        $sql .= '       and x1.is_deleted = false  ';
        $sql .= '     order by ';
        $sql .= '       x1.price ';
        $sql .= '       , x1.id offset 2  ';
        $sql .= '     limit ';
        $sql .= '       1 ';
        $sql .= '   )                          as "Bid__proposal_price3" ';
        $sql .= '   , (  ';
        $sql .= '     select ';
        $sql .= '         x3.facility_name  ';
        $sql .= '     from ';
        $sql .= '       trn_proposals as x1  ';
        $sql .= '       left join trn_proposal_headers as x2  ';
        $sql .= '         on x2.id = x1.trn_proposal_headers_id  ';
        $sql .= '       left join mst_facilities as x3  ';
        $sql .= '         on x3.id = x2.supplier_id  ';
        $sql .= '     where ';
        $sql .= '       x1.trn_rfp_id = b.id  ';
        $sql .= '       and x1.is_deleted = false  ';
        $sql .= '     order by ';
        $sql .= '       x1.price ';
        $sql .= '       , x1.id offset 2  ';
        $sql .= '     limit ';
        $sql .= '       1 ';
        $sql .= '   )                          as "Bid__proposal_supplier3"  ';
        $sql .= ' from ';
        $sql .= '   trn_rfp_headers as a  ';
        $sql .= '   left join trn_rfps as b  ';
        $sql .= '     on b.trn_rfp_header_id = a.id  ';
        $sql .= '   left join mst_item_units as c  ';
        $sql .= '     on c.id = b.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as c1  ';
        $sql .= '     on c1.id = c.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as c2  ';
        $sql .= '     on c2.id = c.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as d  ';
        $sql .= '     on d.id = c.mst_facility_item_id  ';
        $sql .= ' where ';
        $sql .= '   a.id = ' . $this->request->data['Bid']['id'];
        $sql .= ' order by ';
        $sql .= '   b.work_seq ';
        $result = $this->TrnRfpHeader->query($sql);
        $this->set('result' , $result);
    }
    
    /**
     * 見積確定 結果
     */
    function decision_result(){
        $now = date('Y/m/d H:i:s');
        $start_date  = $this->request->data['Bid']['start_date'];
        $this->TrnRfp->begin();
        
        // 登録処理
        foreach($this->request->data['Bid']['id'] as $id){
            // 見積依頼に見積IDを追加する
            $this->TrnRfp->create();
            $ret = $this->TrnRfp->updateAll(
                array(
                    'TrnRfp.trn_proposal_id' => $this->request->data['Bid']['proposal_id'][$id] ,
                    'TrnRfp.modifier'        => $this->Session->read('Auth.MstUser.id'),
                    'TrnRfp.modified'        => "'" . $now . "'"
                    ),
                array(
                    'TrnRfp.id'  => $id ,
                    ),
                -1
                );
            if(!$ret){
                $this->TrnRfp->rollback();
                $this->Session->setFlash('見積確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
            
            // 仕入設定を更新する。
            
            $sql  = ' select ';
            $sql .= '     d.id ';
            $sql .= '   , d.start_date ';
            $sql .= '   , d.end_date ';
            $sql .= '   , d.transaction_price ';
            $sql .= '   , d.is_main ';
            $sql .= ' from ';
            $sql .= '   trn_proposals as a ';
            $sql .= '   left join trn_proposal_headers as a1 ';
            $sql .= '     on a1.id = a.trn_proposal_headers_id ';
            $sql .= '   left join trn_rfps as b ';
            $sql .= '     on b.id = a.trn_rfp_id ';
            $sql .= '   left join mst_item_units as c ';
            $sql .= '     on c.id = b.mst_item_unit_id ';
            $sql .= '   left join mst_transaction_configs as d ';
            $sql .= '     on d.mst_item_unit_id = c.id ';
            $sql .= '     and d.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     and d.partner_facility_id = a1.supplier_id  ';
            $sql .= ' where ';
            $sql .= '   a.id = ' . $this->request->data['Bid']['proposal_id'][$id];
            $sql .= "   and d.start_date <= '" . $start_date . "'";
            $sql .= "   and d.end_date > '" . $start_date . "'";
            // 既存データの確認
            $trnConf = $this->MstTransactionConfig->query($sql);
            if(count($trnConf) > 0){
                // 既存があったら終了日付を入れる
                $this->MstTransactionConfig->create();
                $ret = $this->MstTransactionConfig->updateAll(
                    array(
                        'MstTransactionConfig.end_date' => "'" . $start_date . "'",
                        'MstTransactionConfig.modifier' => $this->Session->read('Auth.MstUser.id'),
                        'MstTransactionConfig.modified' => "'" . $now . "'"
                        ),
                    array(
                        'MstTransactionConfig.id'  => $trnConf[0][0]['id'] ,
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnRfp->rollback();
                    $this->Session->setFlash('見積確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
            }
            
            // 新規追加行の代表フラグ判定
            $flg = $this->isMain($this->request->data['Bid']['proposal_id'][$id]);
            if(!is_null($trnConf[0][0]['start_date']) && $trnConf[0][0]['start_date'] == $start_date){
                // 更新
                $this->MstTransactionConfig->create();
                $ret = $this->MstTransactionConfig->updateAll(
                    array(
                        'MstTransactionConfig.is_main'    => "'" . $flg . "'",
                        'MstTransactionConfig.start_date' => "'" . $start_date . "'",
                        'MstTransactionConfig.end_date'   => "'3000/01/01'",
                        'MstTransactionConfig.modifier'   => $this->Session->read('Auth.MstUser.id'),
                        'MstTransactionConfig.modified'   => "'" . $now . "'"
                        ),
                    array(
                        'MstTransactionConfig.id'  => $trnConf[0][0]['id'] ,
                        ),
                    -1
                    );
                if(!$ret){
                    $this->TrnRfp->rollback();
                    $this->Session->setFlash('見積確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
                
            }else{
                // 新規行を追加
                $sql  = ' select ';
                $sql .= '     d.id                           as "MstTransactionConfig__mst_item_unit_id" ';
                $sql .= "   , '" . $start_date . "' ";
                $sql .= '                                    as "MstTransactionConfig__start_date" ';
                $sql .= "   , '3000/01/01'";
                $sql .= '                                    as "MstTransactionConfig__end_date" ';
                $sql .= '   , f.id                           as "MstTransactionConfig__mst_facility_relation_id" ';
                $sql .= '   , e.id                           as "MstTransactionConfig__mst_facility_item_id" ';
                $sql .= "   , '" .$this->Session->read('Auth.facility_id_selected') . "'";
                $sql .= '                                    as "MstTransactionConfig__mst_facility_id" ';
                $sql .= '   , b.supplier_id                  as "MstTransactionConfig__partner_facility_id" ';
                $sql .= '   , a.price                        as "MstTransactionConfig__transaction_price" ';
                $sql .= "   , '" . $flg . "'";
                $sql .= '                                    as "MstTransactionConfig__is_main" ';
                $sql .= '   , false                          as "MstTransactionConfig__is_deleted" ';
                $sql .= "   , '" . $this->Session->read('Auth.MstUser.id') . "'";
                $sql .= '                                    as "MstTransactionConfig__creater" ';
                $sql .= "   , '" . $now . "'";
                $sql .= '                                    as "MstTransactionConfig__created" ';
                $sql .= "   , '" . $this->Session->read('Auth.MstUser.id') ."'";
                $sql .= '                                    as "MstTransactionConfig__modifier" ';
                $sql .= "   , '" . $now . "'";
                $sql .= '                                    as "MstTransactionConfig__modified"  ';
                $sql .= ' from ';
                $sql .= '   trn_proposals as a  ';
                $sql .= '   left join trn_proposal_headers as b  ';
                $sql .= '     on b.id = a.trn_proposal_headers_id  ';
                $sql .= '   left join trn_rfps as c  ';
                $sql .= '     on c.id = a.trn_rfp_id  ';
                $sql .= '   left join mst_item_units as d  ';
                $sql .= '     on d.id = c.mst_item_unit_id  ';
                $sql .= '   left join mst_facility_items as e  ';
                $sql .= '     on e.id = d.mst_facility_item_id  ';
                $sql .= '   left join mst_facility_relations as f  ';
                $sql .= '     on f.mst_facility_id = e.mst_facility_id  ';
                $sql .= '     and f.partner_facility_id = b.supplier_id  ';
                $sql .= ' where ';
                $sql .= '   a.id ='. $this->request->data['Bid']['proposal_id'][$id];

                $MstTransactionConfig =  $this->MstTransactionConfig->query($sql);

                $this->MstTransactionConfig->create();
                // SQL実行
                if (!$this->MstTransactionConfig->save($MstTransactionConfig[0])) {
                    $this->TrnRfp->rollback();
                    $this->Session->setFlash('見積登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
            }
        }
        // コミット
        $this->TrnRfp->commit();
        
        // 結果画面を表示
        $where = ' and b.id in (' . join(',',$this->request->data['Bid']['id']) . ') ';
        $result = $this->searchRfpHistory($where);
        $this->set('result' , $result);
    }
    
    /**
     * 見積履歴 検索
     */
    function proposal_history(){
        $this->setRoleFunction(112); //見積履歴
        $result = array();
        
        // 検索
        if(isset($this->request->data['Bid']['is_search'])){
            $sql  = '  select ';
            $sql .= '     a.id              as "Bid__id"';
            $sql .= '   , a.work_no         as "Bid__work_no"';
            $sql .= "   , to_char(a.limit_date , 'YYYY/mm/dd')";
            $sql .= '                       as "Bid__limit_date"';
            $sql .= '   , d.user_name       as "Bid__user_name"';
            $sql .= "   , to_char(a.created , 'YYYY/mm/dd HH24:MI') ";
            $sql .= '                       as "Bid__created"';
            $sql .= '   , a.recital         as "Bid__recital"';
            $sql .= '   , a.detail_count    as "Bid__detail_count"';
            $sql .= '   , x.count           as "Bid__count"';
            $sql .= ' from ';
            $sql .= '   trn_rfp_headers as a  ';
            $sql .= '   left join trn_rfps as b  ';
            $sql .= '     on b.trn_rfp_header_id = a.id  ';
            $sql .= '   left join mst_item_units as c  ';
            $sql .= '     on c.id = b.mst_item_unit_id  ';
            $sql .= '   left join mst_users as d  ';
            $sql .= '     on d.id = a.creater  ';

            $sql .= '   left join (  ';
            $sql .= '     select ';
            $sql .= '         b.trn_rfp_header_id ';
            $sql .= '       , count(*)         as count  ';
            $sql .= '     from ';
            $sql .= '       trn_proposals as a  ';
            $sql .= '       left join trn_rfps as b  ';
            $sql .= '         on a.trn_rfp_id = b.id  ';
            $sql .= '     group by ';
            $sql .= '       b.trn_rfp_header_id ';
            $sql .= '   ) as x  ';
            $sql .= '     on x.trn_rfp_header_id = a.id  ';
            
            $sql .= ' where ';
            $sql .= '   c.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
            // 見積番号
            if($this->request->data['Bid']['work_no'] != "" ){
                $sql .= " and a.work_no = '" . $this->request->data['Bid']['work_no'] . "'";
            }
            // 締切日From
            if($this->request->data['Bid']['limit_date_from'] != "" ){
                $sql .= " and a.limit_date >= '" . $this->request->data['Bid']['limit_date_from'] . "'";
            }
            
            // 締切日To
            if($this->request->data['Bid']['limit_date_to'] != "" ){
                $sql .= " and a.limit_date <= '" . $this->request->data['Bid']['limit_date_to'] . "'";
            }
            

            $sql .= ' and a.is_deleted = false ';
            $sql .= ' and b.is_deleted = false ';
            
            $sql .= ' group by ';
            $sql .= '   a.id ';
            $sql .= '   , a.work_no ';
            $sql .= '   , a.limit_date ';
            $sql .= '   , d.user_name ';
            $sql .= '   , a.created ';
            $sql .= '   , a.detail_count ';
            $sql .= '   , a.recital';
            $sql .= '   , x.count';
            $sql .= ' order by ';
            $sql .= '   a.work_no ';
            
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnRfpHeader'));
            if(!is_null($this->request->data['limit'])){
                $sql .= ' limit '.  $this->request->data['limit'];
            }
            
            $result = $this->TrnRfpHeader->query($sql);
        }
        
        $this->set('result' , $result);
    }
    
    /**
     * 見積履歴 確認
     */
    function proposal_history_confirm(){
        // 選択データを表示
        $where = ' and a.id = ' . $this->request->data['Bid']['id'];
        $result = $this->searchRfpHistory($where);
        $this->set('result' , $result);
    }
    
    /**
     * 見積履歴 結果
     */
    function proposal_history_result(){
        $now = date('Y/m/d H:i:s');
        $this->TrnRfp->begin();
        // 取り消し処理
        // 一部取り消しはできない。全件取り消した後再度登録する！
        
        // ヘッダ削除
        $this->TrnRfpHeader->create();
        $ret = $this->TrnRfpHeader->updateAll(
            array(
                'TrnRfpHeader.is_deleted' => "'true'" ,
                'TrnRfpHeader.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnRfpHeader.modified'   => "'" . $now . "'"
                ),
            array(
                'TrnRfpHeader.id'  => $this->request->data['Bid']['id'] ,
                ),
            -1
            );
        if(!$ret){
            $this->TrnRfp->rollback();
            $this->Session->setFlash('見積取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        
        // 明細削除
        $this->TrnRfp->create();
        $ret = $this->TrnRfp->updateAll(
            array(
                'TrnRfp.is_deleted' => "'true'",
                'TrnRfp.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnRfp.modified'   => "'" . $now . "'"
                ),
            array(
                'TrnRfp.trn_rfp_header_id'  => $this->request->data['Bid']['id'] ,
                ),
            -1
            );
        if(!$ret){
            $this->TrnRfp->rollback();
            $this->Session->setFlash('見積取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        
        $this->TrnRfp->commit();

        // お知らせ情報削除
        $this->TrnInformation->create();
        $ret = $this->TrnInformation->updateAll(
            array(
                'TrnInformation.is_deleted' => "'true'",
                'TrnInformation.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnInformation.modified'   => "'" . $now . "'"
                ),
            array(
                'TrnInformation.trn_rfp_header_id'  =>  $this->request->data['Bid']['id'], // 見積依頼情報ID
                ),
            -1
            );
        if(!$ret){
            $this->TrnRfp->rollback();
            $this->Session->setFlash('見積取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        
        // 結果データを表示
        $where = ' and a.id = ' . $this->request->data['Bid']['id'];
        $result = $this->searchRfpHistory($where);
        $this->set('result' , $result);
    }

    /**
     * 商品検索 SQL 
     */
    private function searchFacilityItems($where = null , $order = null , $limit = null, $isBidOnly = false){
        $sql  =' select ';
        $sql .='       b.id            as "MstFacilityItems__id"';
        $sql .='     , a.internal_code as "MstFacilityItems__internal_code"';
        $sql .='     , a.item_name     as "MstFacilityItems__item_name"';
        $sql .='     , a.standard      as "MstFacilityItems__standard"';
        $sql .='     , a.item_code     as "MstFacilityItems__item_code"';
        $sql .='     , c.dealer_name   as "MstFacilityItems__dealer_name"';
        $sql .='     , ( case ';
        $sql .='         when b.per_unit = 1 then d.unit_name  ';
        $sql .="         else d.unit_name || '(' || b.per_unit || e.unit_name || ')'";
        $sql .='         end )         as "MstFacilityItems__unit_name" ';
        $sql .='     , g.id            as "MstFacilityItems__mst_grandmaster_id"';
        $sql .='   from ';
        $sql .='     mst_facility_items as a  ';
        $sql .='     left join mst_item_units as b  ';
        $sql .='       on a.id = b.mst_facility_item_id  ';
        $sql .='     left join mst_dealers as c  ';
        $sql .='       on c.id = a.mst_dealer_id  ';
        $sql .='     left join mst_unit_names as d  ';
        $sql .='       on d.id = b.mst_unit_name_id  ';
        $sql .='     left join mst_unit_names as e  ';
        $sql .='       on e.id = b.per_unit_name_id  ';
        $sql .='     left join mst_transaction_mains as f ';
        $sql .='       on f.mst_item_unit_id = b.id  ';
        $joinOpt = 'left';
        if ($isBidOnly) $joinOpt = 'inner';
        $sql .="     $joinOpt join mst_grand_kyo_materials as g ";
        $sql .='       on g.matcd = a.master_id  ';
        
        if(!is_null($where)){
            $sql .=' where 1 = 1 ';
            $sql .=' ' . $where;
        }
        if(!is_null($order)){
            $sql .=' ' . $order;
        }
        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));
            $sql .=' ' .$limit;
        }
        return $sql;
    }

    /**
     * Excel作成
     */
    function createExcel($id){
        //テンプレートファイルフルパス
        $template = realpath(TMP);
        $template .= DS . 'excel' . DS;
        $template_path = $template . "template.xls";
        
        // Excelオブジェクト作成
        $PHPExcel = $this->createExcelObj($template_path);

        //表紙への入力
        //シートの設定
        $PHPExcel->setActiveSheetIndex(0);  //一番左のシート
        $sheet = $PHPExcel->getActiveSheet();
        
        $sql  = ' select ';
        $sql .= '     b.work_seq ';
        $sql .= '   , d.internal_code ';
        $sql .= '   , d.item_name ';
        $sql .= '   , d.item_code ';
        $sql .= '   , d.standard ';
        $sql .= '   , d.jan_code ';
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when c.per_unit = 1  ';
        $sql .= '       then c1.unit_name  ';
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '       end ';
        $sql .= '   )                            as unit_name  ';
        $sql .= "   , to_char(a.limit_date, 'yyyy/mm/dd')";
        $sql .= '                                as limit_date';
        $sql .= '   , a.work_no' ;
        $sql .= ' from ';
        $sql .= '   trn_rfp_headers as a  ';
        $sql .= '   left join trn_rfps as b  ';
        $sql .= '     on b.trn_rfp_header_id = a.id  ';
        $sql .= '   left join mst_item_units as c  ';
        $sql .= '     on c.id = b.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as c1  ';
        $sql .= '     on c1.id = c.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as c2  ';
        $sql .= '     on c2.id = c.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as d  ';
        $sql .= '     on d.id = c.mst_facility_item_id  ';
        $sql .= ' where ';
        $sql .= '   a.id = ' . $id;
        $sql .= ' order by ';
        $sql .= '   b.work_seq ';
        // データ取得
        $ret = $this->TrnRfpHeader->query($sql);

        // Excel書込処理
        foreach($ret as $num => $r){
            $cell =  (string)($num+4);
            
            $sheet->setCellValue("A" . $cell, $r[0]['work_seq']);      // No
            $sheet->setCellValueExplicit("B" . $cell, $r[0]['internal_code'] , PHPExcel_Cell_DataType::TYPE_STRING);  // 商品ID
            $sheet->setCellValue("C" . $cell, $r[0]['item_name']);     // 商品名
            $sheet->setCellValue("D" . $cell, $r[0]['item_code']);     // 製品番号
            $sheet->setCellValue("E" . $cell, $r[0]['standard']);      // 規格
            $sheet->setCellValueExplicit("F" . $cell, $r[0]['jan_code'] , PHPExcel_Cell_DataType::TYPE_STRING);  // JANコード
            $sheet->setCellValue("G" . $cell, $r[0]['unit_name']);     // 包装単位
        }
        $sheet->setCellValue("B2" , $id);    // ID
        $sheet->setCellValue("H2" , $ret[0][0]['limit_date']); // 締切日
        
        //保存ファイル名
        $filename = "見積依頼_" . $ret[0][0]['work_no'] . "_" . date('YmdHis')  . ".xls";
        $message = '<a href="<@WEBROOT>rfp/' . $filename . '">見積依頼</a>が登録されました。<br>締切日までに見積を登録してください。 ';

        $filename = mb_convert_encoding($filename, 'sjis', 'utf-8');
        
        // 保存ファイルパス
        $upload = realpath( TMP );
        $upload .= DS . '..' . DS . 'webroot' . DS . 'rfp' . DS;
        $path = $upload . $filename;
        
        $objWriter = new PHPExcel_Writer_Excel5( $PHPExcel );   //2003形式で保存
        $objWriter->save( $path );
        
        // お知らせを追加
        $sql  = ' insert  ';
        $sql .= ' into trn_informations(  ';
        $sql .= '   mst_facility_id ';
        $sql .= '   , mst_department_id ';
        $sql .= '   , start_date ';
        $sql .= '   , end_date ';
        $sql .= '   , title ';
        $sql .= '   , message ';
        $sql .= '   , is_deleted ';
        $sql .= '   , creater ';
        $sql .= '   , created ';
        $sql .= '   , modifier ';
        $sql .= '   , modified ';
        $sql .= '   , trn_rfp_header_id';
        $sql .= ' )  ';
        $sql .= ' select ';
        $sql .= '     x.id ';
        $sql .= '   , null ';
        $sql .= '   , now() ::date ';
        $sql .= "   , '" . $ret[0][0]['limit_date'] . "'";
        $sql .= "   , '見積依頼_" . $ret[0][0]['work_no'] . "'";
        $sql .= "   , '" . $message . "'";
        $sql .= '   , false ';
        $sql .= '   , ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '   , now() ';
        $sql .= '   , ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '   , now()  ';
        $sql .= '   , ' . $id;
        $sql .= ' from ';
        $sql .= '   (  ';
        $sql .= '     select ';
        $sql .= '         c.id  ';
        $sql .= '     from ';
        $sql .= '       mst_facilities as a  ';
        $sql .= '       inner join mst_facility_relations as b  ';
        $sql .= '         on b.mst_facility_id = a.id  ';
        $sql .= '         and b.is_deleted = false  ';
        $sql .= '       inner join mst_facilities as c  ';
        $sql .= '         on c.id = b.partner_facility_id  ';
        $sql .= '         and c.is_deleted = false  ';
        $sql .= '         and c.facility_type = ' . Configure::read('FacilityType.supplier');
        $sql .= '     where ';
        $sql .= '       a.id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '   ) as x ';
        
        $this->TrnInformation->query($sql);
    }

    
    function searchRfpHistory($where){
        $sql  = ' select ';
        $sql .= '     b.id                         as "Bid__id" ';
        $sql .= '   , d.internal_code              as "Bid__internal_code" ';
        $sql .= '   , d.item_name                  as "Bid__item_name" ';
        $sql .= '   , d.item_code                  as "Bid__item_code" ';
        $sql .= '   , d.standard                   as "Bid__standard" ';
        $sql .= '   , d.jan_code                   as "Bid__jan_code" ';
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when c.per_unit = 1  ';
        $sql .= '       then c1.unit_name  ';
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '       end ';
        $sql .= '   )                              as "Bid__unit_name" ';
        $sql .= '   , e.price                      as "Bid__price" ';
        $sql .= '   , g.facility_name              as "Bid__facility_name"  ';
        $sql .= ' from ';
        $sql .= '   trn_rfp_headers as a  ';
        $sql .= '   left join trn_rfps as b  ';
        $sql .= '     on b.trn_rfp_header_id = a.id  ';
        $sql .= '   left join mst_item_units as c  ';
        $sql .= '     on c.id = b.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as c1  ';
        $sql .= '     on c1.id = c.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as c2  ';
        $sql .= '     on c2.id = c.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as d  ';
        $sql .= '     on d.id = c.mst_facility_item_id  ';
        $sql .= '   left join trn_proposals as e  ';
        $sql .= '     on e.id = b.trn_proposal_id  ';
        $sql .= '   left join trn_proposal_headers as f  ';
        $sql .= '     on f.id = e.trn_proposal_headers_id  ';
        $sql .= '   left join mst_facilities as g  ';
        $sql .= '     on g.id = f.supplier_id  ';
        $sql .= ' where 1 = 1';
        $sql .= $where;
        $sql .= ' order by ';
        $sql .= '   b.work_seq ';
        $result = $this->TrnRfpHeader->query($sql);
        
        return $result;
    }
    /**
     * 見積確定代表フラグ判定
     */
    function isMain($proposal_id){
        $return = false;
        
        $sql  = " select ";
        $sql .= "     b.mst_item_unit_id  ";
        $sql .= " from ";
        $sql .= "   trn_proposals as a  ";
        $sql .= "   left join trn_rfps as b  ";
        $sql .= "     on b.id = a.trn_rfp_id  ";
        $sql .= "  inner join mst_transaction_configs as c  ";
        $sql .= "     on c.mst_item_unit_id = b.mst_item_unit_id  ";
        $sql .= "     and c.is_main = true  ";
        $sql .= " where ";
        $sql .= "   a.id = " . $proposal_id;
        
        $res = $this->MstTransactionConfig->query($sql);

        if(is_null($res)){
            $return = false;
        }else{
            // 包装単位IDで、代表フラグをOFF
            /*
            $this->MstTransactionConfig->create();
            $ret = $this->MstTransactionConfig->updateAll(
                array(
                    'MstTransactionConfig.is_main'  => "'false'"
                    ),
                array(
                    'MstTransactionConfig.mst_item_unit_id'  => $res[0][0]['mst_item_unit_id'] , // 包装単位ID
                    ),
                -1
                );
            if(!$ret){
                $this->TrnRfp->rollback();
                $this->Session->setFlash('見積確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
              */
            $return = true;
        }
        
        return $return;
    }
    
}
?>
