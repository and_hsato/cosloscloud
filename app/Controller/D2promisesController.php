<?php
/**
 * D2PromisesController
 * 直納品要求
 * @version 1.0.0
 * @since 2013/12/11
 */
class D2PromisesController extends AppController {
    /**
     * @var $name
     */
    var $name = 'd2promises';
    /**
     * @var array $uses
     */
    var $uses = array(
        'MstUser',              //ユーザ
        'MstFacilityRelation',  //施設関係
        'MstFacility',          //施設
        'MstFacilityItem',      //施設採用品
        'MstDepartment',        //部署
        'MstClass',             //作業区分
        'MstItemUnit',          //包装単位
        'MstDealer',            //販売元
        'TrnSticker',           //シール
        'MstTransactionConfig', //仕入設定
        'MstSalesConfig',       //売上設定
        'TrnStock',             //在庫テーブル
        'TrnClaim',             //請求テーブル（売上データ）
        'MstUnitName',          //単位名
        'TrnCloseHeader',       //締め
        'TrnStickerRecord',     //シール移動履歴
        'TrnStickerIssue',
        'TrnOrder',
        'TrnOrderHeader',
    );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;
    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');
    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Stickers','CsvWriteUtils');

    public function beforeFilter(){
      parent::beforeFilter();
      $this->Auth->allowedActions[] = 'report';
      $this->Auth->allowedActions[] = 'seal';
    }

    /**
     * 直納品条件入力画面
     */
    public function index() {
        $this->setRoleFunction(50); //直納品要求登録
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected') ,'18')); //作業区分プルダウン用データ取得

        if($this->Auth->user('user_type') == Configure::read('UserType.edi')) {
            // 業者の場合
            $sql  = ' select ';
            $sql .= '     c.facility_code as "MstFacility__facility_code"';
            $sql .= '   , c.facility_name as "MstFacility__facility_name" ';
            $sql .= ' from ';
            $sql .= '   mst_facility_relations as a  ';
            $sql .= '   left join mst_facility_relations as b  ';
            $sql .= '     on b.mst_facility_id = a.mst_facility_id  ';
            $sql .= '   inner join mst_facilities as c  ';
            $sql .= '     on c.id = b.partner_facility_id  ';
            $sql .= '     and c.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .= ' where ';
            $sql .= '   a.partner_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '   and a.is_deleted = false '; 
            $sql .= '   and b.is_deleted = false ';
            
            $list = Set::Combine(
                $this->MstFacility->query($sql),
                "{n}.MstFacility.facility_code",
                "{n}.MstFacility.facility_name"
                );

            //得意先
            $this->set('hospital_list',$list);
            //仕入先
            $sql  = ' select ';
            $sql .= '     a.facility_code as "MstFacility__facility_code"';
            $sql .= '   , a.facility_name as "MstFacility__facility_name" ';
            $sql .= ' from ';
            $sql .= '   mst_facilities as a  ';
            $sql .= ' where ';
            $sql .= '   a.id = ' . $this->Session->read('Auth.facility_id_selected');
            
            $list = Set::Combine(
                $this->MstFacility->query($sql),
                "{n}.MstFacility.facility_code",
                "{n}.MstFacility.facility_name"
                );
            
            $this->set('supplier_list',$list);
            
        }else{
            // センターの場合
            //得意先
            $this->set('hospital_list',
                       $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                              array(Configure::read('FacilityType.hospital')),
                                              true
                                              )
                       );
            //仕入先
            $this->set('supplier_list',
                       $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                              array(Configure::read('FacilityType.supplier')),
                                              true,
                                              array('facility_code' , 'facility_name'),
                                              'facility_name, facility_code'
                                              )
                       );
        }
        
        $this->request->data['d2promises']['work_date'] = date('Y/m/d');
        $this->render('index');
    }

    /**
     * 施設採用品選択
     * @param
     * @return
     */
    public function add() {
        App::import('Sanitize');

        $SearchResult = array();
        $CartSearchResult = array();

        //初回時は検索を行わない
        if (isset($this->request->data['add_search']['is_search'])) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $limit = '' ;
            if(isset($this->request->data['limit'])){
                $limit .= 'limit ' . $this->request->data['limit'];
            }
            $order =' order by a.internal_code ,b.per_unit ';
            $where = '';
            $where .=' and a.mst_facility_id = ' . $this->request->data['d2promises']['centerId'] ;
            $where .=' and a.item_type = ' . Configure::read('Items.item_types.normalitem'); // 通常品のみ
            $where .=' and a.is_deleted = false '; //施設採用品で削除されているものは除外
            $where .=' and b.is_deleted = false '; //包装単位で適用が外れているものは除外
            $where .=' and a.is_lowlevel = false '; // 低レベル品は除外

            $where .=" and a.start_date <= '" . $this->request->data['d2promises']['work_date'] . "'"; //施設採用品開始日
            $where .=" and ( a.end_date >= '" . $this->request->data['d2promises']['work_date'] . "'"; //施設採用品終了日
            $where .=' or a.end_date is null ) ';

            $where .=" and b.start_date <= '" . $this->request->data['d2promises']['work_date'] . "'"; //包装単位開始日
            $where .=" and ( b.end_date >= '" . $this->request->data['d2promises']['work_date'] . "'"; //包装単位終了日
            $where .=' or b.end_date is null ) ';

            $where2 = $where;

            if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and b.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 .= " and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 .= " and 0 = 1 ";
            }


            if(isset($this->request->data['MstFacilityItems']['internal_code']) && $this->request->data['MstFacilityItems']['internal_code'] !== ''){
                $where .=" and a.internal_code ilike '%" .$this->request->data['MstFacilityItems']['internal_code']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['standard']) && $this->request->data['MstFacilityItems']['standard'] !== '' ){
                $where .=" and a.standard ilike '%" .$this->request->data['MstFacilityItems']['standard']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['item_name']) && $this->request->data['MstFacilityItems']['item_name'] !== ''){
                $where .=" and a.item_name ilike '%" .$this->request->data['MstFacilityItems']['item_name']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['item_code']) && $this->request->data['MstFacilityItems']['item_code'] !== ''){
                $where .=" and a.item_code ilike '%" .$this->request->data['MstFacilityItems']['item_code']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['dealer_name']) && $this->request->data['MstFacilityItems']['dealer_name'] !== ''){
                $where .=" and c.dealer_name ilike '%" .$this->request->data['MstFacilityItems']['dealer_name']."%'";
            }
            //JAN(前方一致)
            if(isset($this->request->data['MstFacilityItems']['jan_code']) && $this->request->data['MstFacilityItems']['jan_code'] !== ''){
                $where .=" and a.jan_code ilike '" .$this->request->data['MstFacilityItems']['jan_code']."%'";
            }

            //チェック可能なもののみ
            if(isset($this->request->data['MstFacilityItems']['canChecked']) && $this->request->data['MstFacilityItems']['canChecked']==1){
                $where .=' and f.id is not null ';
                $where .=' and g.id is not null ';
            }

            // 操作者が業者の場合分割テーブルが使えないので判定を入れる
            if($this->Auth->user('user_type') == Configure::read('UserType.edi') ) {
                $split_flg = false;
            }else{
                $split_flg = true;
            }
            
            // 検索（画面上）用一覧取得
            $SearchResult = $this->MstFacilityItem->query($this->searchFacilityItems($where , $order , $limit , $split_flg) , false , $split_flg);

            // カート（画面下）用一覧取得
            $CartSearchResultTmp = $this->MstFacilityItem->query($this->searchFacilityItems($where2 , $order , null , $split_flg) , false , $split_flg);

            if(!empty($this->request->data['cart']['tmpid'])){
                //カートに追加した順に並べなおす。
                foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
                    foreach($CartSearchResultTmp as $tmp){
                        if($tmp['MstFacilityItems']['id'] == $id){
                            $CartSearchResult[] = $tmp;
                            break;
                        }
                    }
                }
            }
            $this->request->data = $data;
            $this->set('SearchResult' , $SearchResult);
            $this->set('CartSearchResult' , $CartSearchResult);
            $this->set('data' , $this->request->data);

            $this->render('add');
            //初回遷移時のみ締めチェック

        } else {
            //直納品条件入力の入力データをセッションに保持
            $this->Session->write('input_D2promises', $this->request->data['d2promises']);

            //得意先コードから施設IDを取得
            $tmp = $this->MstFacility->find('first', array('fields' => 'id',
                                                           'conditions' => array('MstFacility.facility_code' => $this->request->data['d2promises']['hospitalCode'] ,
                                                                                 'MstFacility.facility_type'=> array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')) ),
                                                           'recursive' => -1));
            $this->request->data['d2promises']['hospitalId'] = $tmp['MstFacility']['id'];
            unset($tmp);

            //得意先部署コードから部署IDを取得
            $tmp = $this->MstDepartment->find('first',
                    array(
                          'fields' => 'id',
                          'conditions' => array('MstDepartment.department_code' => $this->request->data['d2promises']['departmentCode'],
                                                'MstDepartment.mst_facility_id' => $this->request->data['d2promises']['hospitalId']),
                          'recursive' => -1
                         )
                    );
            $this->request->data['d2promises']['departmentId'] = $tmp['MstDepartment']['id'];
            unset($tmp);

            if($this->Auth->user('user_type') == Configure::read('UserType.edi') ) {
                $this->request->data['d2promises']['supplierId'] = $this->Session->read('Auth.facility_id_selected');
            }else{
                //仕入先コードから施設IDを取得
                $tmp = $this->MstFacility->find('first', array('fields' => 'id',
                                                               'conditions' => array('MstFacility.facility_code' => $this->request->data['d2promises']['supplierCode'] ,
                                                                                     'MstFacility.facility_type'=> array(Configure::read('FacilityType.supplier'))),
                                                               
                                                               'joins' => array(
                                                                   array('type'=>'inner'
                                                                         ,'table'=>'mst_facility_relations'
                                                                         ,'alias'=>'MstFacilityRelation'
                                                                         ,'conditions'=> array('MstFacilityRelation.partner_facility_id = MstFacility.id'
                                                                                               ,'MstFacilityRelation.mst_facility_id'    => $this->Session->read('Auth.facility_id_selected')
                                                                                               ,'MstFacilityRelation.is_deleted = false'
                                                                                               )
                                                                         )
                                                                   ),
                                                               'recursive' => -1));
                $this->request->data['d2promises']['supplierId'] = $tmp['MstFacility']['id'];
                unset($tmp);
            }
            // 病院部署IDから、センターIDを取得
            $sql  = ' select ';
            $sql .= '     b.mst_facility_id  ';
            $sql .= ' from ';
            $sql .= '   mst_departments as a  ';
            $sql .= '   left join mst_facility_relations as b  ';
            $sql .= '     on b.partner_facility_id = a.mst_facility_id  ';
            $sql .= ' where ';
            $sql .= '   a.id = ' . $this->request->data['d2promises']['departmentId'];
            $ret = $this->MstDepartment->query($sql);
            $this->request->data['d2promises']['centerId'] = $ret[0][0]['mst_facility_id'];
            
            $this->set('data' , $this->request->data);
            $this->render('add');
        }
    }

    /**
     * 直納品登録確認画面
     */
    public function add_confirm() {
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'18'));
        // 操作者が業者の場合分割テーブルが使えないので判定を入れる
        if($this->Auth->user('user_type') == Configure::read('UserType.edi') ) {
            $split_flg = false;
        }else{
            $split_flg = true;
        }

        $where = ' and b.id in ( ' . $this->request->data['cart']['tmpid'] . ')';
        $FacilityItemListTmp = $this->MstFacilityItem->query($this->searchFacilityItems($where , null , null , $split_flg),false,$split_flg);

        //カートに追加した順に並べなおす。
        foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
            foreach($FacilityItemListTmp as $tmp){
                if($tmp['MstFacilityItems']['id'] == $id){
                    $FacilityItemList[] = $tmp;
                    break;
                }
            }
        }

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('d2promises.token' , $token);
        $this->request->data['d2promises']['token'] = $token;

        $this->set('FacilityItemList', $FacilityItemList);
        $this->render('add_confirm');
    }

    /**
     * 直納品登録
     */
    public function add_result() {
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'18'));
        if($this->request->data['d2promises']['token'] === $this->Session->read('d2promises.token')) {
            $this->Session->delete('d2promises.token');

            $now = date('Y/m/d H:i:s');
            $this->TrnOrderHeader->begin();

            $workno = $this->setWorkNo4Header( date('Ymd') , "28");
            $toId = $this->getDepartmentId($this->request->data['d2promises']['supplierId'] , Configure::read('FacilityType.supplier') );
            // 直納要求ヘッダ
            $TrnOrderHeader = array(
                'TrnOrderHeader'=> array(
                    'work_no'            => $workno,
                    'work_date'          => $this->request->data['d2promises']['work_date'],
                    'work_type'          => $this->request->data['d2promises']['classId'],
                    'recital'            => $this->request->data['d2promises']['recital1'],
                    'order_type'         => Configure::read('OrderTypes.direct'),
                    'order_status'       => Configure::read('OrderStatus.ordered'),
                    'department_id_from' => $this->request->data['d2promises']['departmentId'],
                    'department_id_to'   => $toId,
                    'detail_count'       => count($this->request->data['d2promises']['id']),
                    'is_deleted'         => false,
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'created'            => $now,
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'modified'           => $now,
                    'center_facility_id' => $this->request->data['d2promises']['centerId']
                    )
                );

            $this->TrnOrderHeader->create();
            // SQL実行
            if (!$this->TrnOrderHeader->save($TrnOrderHeader)) {
                $this->TrnOrderHeader->rollback();
                $this->Session->setFlash('直納要求処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
            $h_id = $this->TrnOrderHeader->getLastInsertID();
            
            foreach ( $this->request->data['d2promises']['id'] as $cnt=>$id ) {
                $TrnOrder = array(
                    'TrnOrder'=> array(
                        'work_no'             => $workno,
                        'work_seq'            => $cnt+1,
                        'work_date'           => $this->request->data['d2promises']['work_date'],
                        'work_type'           => $this->request->data['d2promises']['work_type'][$id],
                        'recital'             => $this->request->data['d2promises']['recital'][$id],
                        'order_type'          => Configure::read('OrderTypes.direct'),
                        'order_status'        => Configure::read('OrderStatus.loaded'),
                        'mst_item_unit_id'    => $id,
                        'department_id_from'  => $this->request->data['d2promises']['departmentId'],
                        'department_id_to'    => $toId,
                        'quantity'            => $this->request->data['d2promises']['quantity'][$id],
                        'before_quantity'     => $this->request->data['d2promises']['quantity'][$id],
                        'remain_count'        => $this->request->data['d2promises']['quantity'][$id],
                        'stocking_price'      => $this->getStokingPrice($id, $this->request->data['d2promises']['supplierId'] , $this->request->data['d2promises']['work_date']) ,
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        'fixed_num_used'      => 0,
                        'trn_order_header_id' => $h_id,
                        'center_facility_id'  => $this->request->data['d2promises']['centerId']
                        )
                    );

                $this->TrnOrder->create();
                // SQL実行
                if (!$this->TrnOrder->save($TrnOrder)) {
                    $this->TrnOrderHeader->rollback();
                    $this->Session->setFlash('直納要求処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
            }

            $this->TrnOrderHeader->commit();
            
            // 操作者が業者の場合分割テーブルが使えないので判定を入れる
            if($this->Auth->user('user_type') == Configure::read('UserType.edi')) {
                $split_flg = false;
            }else{
                $split_flg = true;
            }

            $where = ' and b.id in ( ' . join(',',$this->request->data['d2promises']['id']) . ')';
            $FacilityItemListTmp = $this->MstFacilityItem->query($this->searchFacilityItems($where , null , null , $split_flg),false,$split_flg);

            //カートに追加した順に並べなおす。
            foreach( $this->request->data['d2promises']['id'] as $id){
                foreach($FacilityItemListTmp as $tmp){
                    if($tmp['MstFacilityItems']['id'] == $id){
                        $FacilityItemList[] = $tmp;
                        break;
                    }
                }
            }
            
            $this->request->data['d2promises']['id'][0] = $h_id;
            $this->set('FacilityItemList', $FacilityItemList);
            $this->Session->write('d2customers.FacilityItemList' , $FacilityItemList);
            $this->render('add_result');
        }else{
            // トランザクショントークン不正
            // リロード or ２度押し
            $this->set('FacilityItemList' , $this->Session->read('d2customers.FacilityItemList'));
        }
    }
    /**
     * 直納要求履歴
     */
    public function history(){
        $this->setRoleFunction(51); //直納品要求履歴
        $result = array();
        $department_list = array();
        $this->set('facility_list' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                            array(Configure::read('FacilityType.hospital'))
                                                            ));
        
        if(isset($this->request->data['d2promises']['facilityCode']) && $this->request->data['d2promises']['facilityCode']!=""){
            //施設が選択済みだったら部署一覧を取得
            $department_list = $this->getDepartmentsList($this->request->data['d2promises']['facilityCode'],
                                                         false
                                                         );
        }
        $this->set('department_list' , $department_list);
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),18));
        
        if(isset($this->request->data['d2promises']['is_search'])){
            $where = "";
            // 絞り込み条件追加

            //要求番号
            if(isset($this->request->data['d2promises']['work_no']) && $this->request->data['d2promises']['work_no'] != "" ){
                $where .= " and a.work_no = '" . $this->request->data['d2promises']['work_no'] . "'";
            }
            //要求日From
            if(isset($this->request->data['d2promises']['work_date_from']) && $this->request->data['d2promises']['work_date_from'] != "" ){
                $where .= " and a.work_date >= '" . $this->request->data['d2promises']['work_date_from'] . "'";
            }
            //要求日To
            if(isset($this->request->data['d2promises']['work_date_to']) && $this->request->data['d2promises']['work_date_to'] != "" ){
                $where .= " and a.work_date <= '" . $this->request->data['d2promises']['work_date_to'] . "'";
            }

            //取消も表示する
            if(isset($this->request->data['d2promises']['is_deleted']) && $this->request->data['d2promises']['is_deleted'] != "1" ){
                $where .= " and a.is_deleted = false ";
                $where .= " and b.is_deleted = false ";
            }
            
            //要求施設
            if(isset($this->request->data['d2promises']['facilityCode']) && $this->request->data['d2promises']['facilityCode'] != "" ){
                $where .= " and g.facility_code = '" . $this->request->data['d2promises']['facilityCode'] . "'";
            }
            
            //要求部署
            if(isset($this->request->data['d2promises']['departmentCode']) && $this->request->data['d2promises']['departmentCode'] != "" ){
                $where .= " and f.department_code = '" . $this->request->data['d2promises']['departmentCode'] . "'";
            }
            
            //商品ID
            if(isset($this->request->data['d2promises']['internal_code']) && $this->request->data['d2promises']['internal_code'] != "" ){
                $where .= " and d.internal_code = '" . $this->request->data['d2promises']['internal_code'] . "'";
            }
            
            //製品番号
            if(isset($this->request->data['d2promises']['item_code']) && $this->request->data['d2promises']['item_code'] != "" ){
                $where .= " and d.item_code = '%" . $this->request->data['d2promises']['item_code'] . "%'";
            }
            
            //商品名
            if(isset($this->request->data['d2promises']['item_name']) && $this->request->data['d2promises']['item_name'] != "" ){
                $where .= " and d.item_name = '" . $this->request->data['d2promises']['item_name'] . "'";
            }
            
            //販売元
            if(isset($this->request->data['d2promises']['dealer_name']) && $this->request->data['d2promises']['dealer_name'] != "" ){
                $where .= " and h.dealer_name = '%" . $this->request->data['d2promises']['dealer_name'] . "%'";
            }
            
            //作業区分
            if(isset($this->request->data['d2promises']['work_type']) && $this->request->data['d2promises']['work_type'] != "" ){
                $where .= " and b.work_type = '" . $this->request->data['d2promises']['work_type'] . "'";
            }
            
            //規格
            if(isset($this->request->data['d2promises']['standard']) && $this->request->data['d2promises']['standard'] != "" ){
                $where .= " and d.standard like '%" . $this->request->data['d2promises']['standard'] . "%'";
            }
            
            //JANコード
            if(isset($this->request->data['d2promises']['jan_code']) && $this->request->data['d2promises']['jan_code'] != "" ){
                $where .= " and d.jan_code = '" . $this->request->data['d2promises']['jan_code'] . "'";
            }
            
            $result = $this->getHistory($where , $this->request->data['limit']);
        }else{
            // 初回のみ
            $this->request->data['d2promises']['work_date_from'] = date('Y/m/d' , strtotime("-7 day"));
            $this->request->data['d2promises']['work_date_to'] = date('Y/m/d');
        }
     
        $this->set('result' , $result);
    }
    
    /**
     * 直納要求履歴編集
     */
    public function history_confirm(){
        $where = "";

        // 選択ID
        $where .= " and a.id in ( " . join(',',$this->request->data['d2promises']['id']) . ")" ;

        //要求番号
        if(isset($this->request->data['d2promises']['work_no']) && $this->request->data['d2promises']['work_no'] != "" ){
            $where .= " and a.work_no = '" . $this->request->data['d2promises']['work_no'] . "'";
        }
        //要求日From
        if(isset($this->request->data['d2promises']['work_date_from']) && $this->request->data['d2promises']['work_date_from'] != "" ){
            $where .= " and a.work_date >= '" . $this->request->data['d2promises']['work_date_from'] . "'";
        }
        //要求日To
        if(isset($this->request->data['d2promises']['work_date_to']) && $this->request->data['d2promises']['work_date_to'] != "" ){
            $where .= " and a.work_date <= '" . $this->request->data['d2promises']['work_date_to'] . "'";
        }
        
        //取消も表示する
        if(isset($this->request->data['d2promises']['is_deleted']) && $this->request->data['d2promises']['is_deleted'] != "1" ){
            $where .= " and a.is_deleted = false ";
            $where .= " and b.is_deleted = false ";
        }
        
        //要求施設
        if(isset($this->request->data['d2promises']['facilityCode']) && $this->request->data['d2promises']['facilityCode'] != "" ){
            $where .= " and g.facility_code = '" . $this->request->data['d2promises']['facilityCode'] . "'";
        }
        
        //要求部署
        if(isset($this->request->data['d2promises']['departmentCode']) && $this->request->data['d2promises']['departmentCode'] != "" ){
            $where .= " and f.department_code = '" . $this->request->data['d2promises']['departmentCode'] . "'";
        }
        
        //商品ID
        if(isset($this->request->data['d2promises']['internal_code']) && $this->request->data['d2promises']['internal_code'] != "" ){
            $where .= " and d.internal_code = '" . $this->request->data['d2promises']['internal_code'] . "'";
        }
        
        //製品番号
        if(isset($this->request->data['d2promises']['item_code']) && $this->request->data['d2promises']['item_code'] != "" ){
            $where .= " and d.item_code = '%" . $this->request->data['d2promises']['item_code'] . "%'";
        }
        
        //商品名
        if(isset($this->request->data['d2promises']['item_name']) && $this->request->data['d2promises']['item_name'] != "" ){
            $where .= " and d.item_name = '" . $this->request->data['d2promises']['item_name'] . "'";
        }
        
        //販売元
        if(isset($this->request->data['d2promises']['dealer_name']) && $this->request->data['d2promises']['dealer_name'] != "" ){
            $where .= " and h.dealer_name = '%" . $this->request->data['d2promises']['dealer_name'] . "%'";
        }
        
        //作業区分
        if(isset($this->request->data['d2promises']['work_type']) && $this->request->data['d2promises']['work_type'] != "" ){
            $where .= " and b.work_type = '" . $this->request->data['d2promises']['work_type'] . "'";
        }
        
        //規格
        if(isset($this->request->data['d2promises']['standard']) && $this->request->data['d2promises']['standard'] != "" ){
            $where .= " and d.standard like '%" . $this->request->data['d2promises']['standard'] . "%'";
        }
        
        //JANコード
        if(isset($this->request->data['d2promises']['jan_code']) && $this->request->data['d2promises']['jan_code'] != "" ){
            $where .= " and d.jan_code = '" . $this->request->data['d2promises']['jan_code'] . "'";
        }
        
        $result = $this->getHistoryDetail($where);
        $this->set('result' , $result);
    }
    
    /**
     * 直納要求履歴結果
     */
    public function history_result(){
        
        $now = date('Y/m/d H:i:s');
        //トランザクション開始
        $this->TrnOrder->begin();

        // 行ロック
        $sql = " select * from trn_orders as a where a.id in ( " . join(',', $this->request->data['d2promises']['id']) . ") for update ";
        $this->TrnOrder->query($sql);
        
        $res = $this->TrnOrder->updateAll(
            array(
                'TrnOrder.is_deleted' => 'true',
                'TrnOrder.modifier' => $this->Session->read('Auth.MstUser.id'),
                'TrnOrder.modified' => "'".$now."'",
                ),
            array(
                'TrnOrder.id' => $this->request->data['d2promises']['id'],
                ),
            -1);
        
        if(false === $res){
            $this->TrnOrder->rollback();
            $this->Session->setFlash('直納要求取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('history');
        }

        //コミット
        $this->TrnOrder->commit();
        
        $where = " and b.id in ( " . join(',',$this->request->data['d2promises']['id']) . ")";
        $result = $this->getHistoryDetail($where);
        $this->set('result' , $result);
    }
    
    /**
     * 商品情報検索
     */
    private function searchFacilityItems($where = null , $order = null , $limit = null , $split_flg=true){
        $sql  =' select ';
        $sql .='       b.id            as "MstFacilityItems__id"';
        $sql .='     , a.internal_code as "MstFacilityItems__internal_code"';
        $sql .='     , a.item_name     as "MstFacilityItems__item_name"';
        $sql .='     , a.standard      as "MstFacilityItems__standard"';
        $sql .='     , a.item_code     as "MstFacilityItems__item_code"';
        $sql .='     , c.dealer_name   as "MstFacilityItems__dealer_name"';
        $sql .='     , ( case ';
        $sql .='         when b.per_unit = 1 then d.unit_name ';
        $sql .="         else d.unit_name || '(' || b.per_unit || e.unit_name || ')' ";
        $sql .='         end )         as "MstFacilityItems__unit_name"';
        $sql .='     , ( case ';
        $sql .='         when f.id is null then false ';
        $sql .='         when g.id is null then false ';
        $sql .='         else true ';
        $sql .='         end )         as "MstFacilityItems__check" ';
        $sql .='   from ';
        $sql .='     mst_facility_items as a  ';
        $sql .='     left join mst_item_units as b  ';
        $sql .='       on a.id = b.mst_facility_item_id  ';
        $sql .='     left join mst_dealers as c  ';
        $sql .='       on c.id = a.mst_dealer_id  ';
        $sql .='     left join mst_unit_names as d  ';
        $sql .='       on d.id = b.mst_unit_name_id  ';
        $sql .='     left join mst_unit_names as e  ';
        $sql .='       on e.id = b.per_unit_name_id  ';
        $sql .='    left join mst_sales_configs as f  ';
        $sql .='      on f.mst_item_unit_id = b.id  ';
        $sql .='      and f.is_deleted = false  ';
        $sql .='      and f.partner_facility_id = ' . $this->request->data['d2promises']['hospitalId'];
        $sql .="      and f.start_date <= '" . $this->request->data['d2promises']['work_date'] . "'"; //売上設定開始日
        $sql .="      and ( f.end_date >  '" . $this->request->data['d2promises']['work_date'] . "'"; //売上設定終了日
        $sql .='      or f.end_date is null ) ';

        $sql .='    left join mst_transaction_configs as g  ';
        $sql .='      on g.mst_item_unit_id = b.id  ';
        $sql .='      and g.is_deleted = false  ';
        $sql .='      and g.partner_facility_id = ' . $this->request->data['d2promises']['supplierId'];
        $sql .="      and g.start_date <= '" . $this->request->data['d2promises']['work_date'] . "'"; //仕入設定開始日
        $sql .="      and ( g.end_date >  '" . $this->request->data['d2promises']['work_date'] . "'"; //仕入設定終了日
        $sql .='      or g.end_date is null ) ';

        if(!is_null($where)){
            $sql .=' where 1 = 1 ';
            $sql .=' ' . $where;
        }
        if(!is_null($order)){
            $sql .=' ' . $order;
        }
        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem' ,$split_flg));
            $sql .=' ' .$limit;
        }
        return $sql;
    }
    
    private function getStokingPrice($mst_item_unit_id , $supplier_id , $work_date){
        $sql  = ' select ';
        $sql .= '     a.transaction_price  ';
        $sql .= ' from ';
        $sql .= '   mst_transaction_configs as a  ';
        $sql .= ' where ';
        $sql .= '   a.is_deleted = false  ';
        $sql .= '   and a.mst_item_unit_id = ' . $mst_item_unit_id;
        $sql .= '   and a.partner_facility_id = ' . $supplier_id;
        $sql .= "   and a.start_date <= '" . $work_date . "'";
        $sql .= "   and a.end_date > '" . $work_date . "'";
        $ret = $this->MstTransactionConfig->query($sql , false , false );
        
        return $ret[0][0]['transaction_price'];
    }

    /**
     * 履歴検索
     */
    private function getHistory($where, $limit=null, $split_flg=true){
        $sql  = ' select ';
        $sql .= '     a.id              as "d2promises__id"';
        $sql .= '   , a.work_no         as "d2promises__work_no"';
        $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') ";
        $sql .= '                       as "d2promises__work_date"';
        $sql .= '   , g.facility_name   as "d2promises__facility_name"';
        $sql .= '   , f.department_name as "d2promises__department_name" ';
        $sql .= '   , e.user_name       as "d2promises__user_name"';
        $sql .= "   , to_char(b.created, 'YYYY/mm/dd HH24:MI:SS') ";
        $sql .= '                       as "d2promises__created"';
        $sql .= '   , a.recital         as "d2promises__recital"';
        $sql .= '   , sum(b.quantity)   as "d2promises__quantity"';
        $sql .= '   , count(*)          as "d2promises__count"';
        $sql .= '   , a.detail_count    as "d2promises__detail_count" ';
        $sql .= ' from ';
        $sql .= '   trn_order_headers as a  ';
        $sql .= '   left join trn_orders as b  ';
        $sql .= '     on b.trn_order_header_id = a.id  ';
        $sql .= '   left join mst_item_units as c  ';
        $sql .= '     on c.id = b.mst_item_unit_id  ';
        $sql .= '   left join mst_facility_items as d  ';
        $sql .= '     on d.id = c.mst_facility_item_id  ';
        $sql .= '   left join mst_users as e  ';
        $sql .= '     on e.id = b.creater  ';
        $sql .= '   left join mst_departments as f  ';
        $sql .= '     on f.id = a.department_id_from  ';
        $sql .= '   left join mst_facilities as g  ';
        $sql .= '     on g.id = f.mst_facility_id  ';
        $sql .= '   left join mst_dealers as h ';
        $sql .= '     on h.id = d.mst_dealer_id';
        $sql .= ' where ';
        $sql .= '   a.order_type = ' . Configure::read('OrderTypes.direct');
        $sql .= '   and g.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= $where;
        $sql .= ' group by ';
        $sql .= '   a.id ';
        $sql .= '   , a.work_no ';
        $sql .= '   , a.work_date ';
        $sql .= '   , b.created ';
        $sql .= '   , e.user_name ';
        $sql .= '   , g.facility_name ';
        $sql .= '   , f.department_name ';
        $sql .= '   , a.detail_count ';
        $sql .= '   , a.recital  ';
        $sql .= ' order by ';
        $sql .= '   a.work_no ';
        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'TrnOrderHeader' ,$split_flg));
            $sql .=' limit ' .$limit;
        }
        
        $result = $this->TrnOrderHeader->query($sql , false , $split_flg);
        return $result;
    }

    /**
     * 履歴明細検索
     */
    private function getHistoryDetail($where, $split_flg=true){
        $sql  = ' select ';
        $sql .= '     b.id              as "d2promises__id"';
        $sql .= '   , a.work_no         as "d2promises__work_no"';
        $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') ";
        $sql .= '                       as "d2promises__work_date"';
        $sql .= '   , g.facility_name   as "d2promises__facility_name"';
        $sql .= '   , f.department_name as "d2promises__department_name" ';
        $sql .= '   , e.user_name       as "d2promises__user_name"';
        $sql .= "   , to_char(b.created, 'YYYY/mm/dd HH24:MI:SS') ";
        $sql .= '                       as "d2promises__created"';
        $sql .= '   , b.recital         as "d2promises__recital"';
        $sql .= '   , d.internal_code   as "d2promises__internal_code"';
        $sql .= '   , d.item_name       as "d2promises__item_name"';
        $sql .= '   , d.item_code       as "d2promises__item_code"';
        $sql .= '   , d.jan_code        as "d2promises__jan_code"';
        $sql .= '   , d.standard        as "d2promises__standard"';
        $sql .= '   , h.dealer_name     as "d2promises__dealer_name"';
        $sql .= '   , ( case ';
        $sql .= '       when c.per_unit = 1 then c1.unit_name ';
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')' ";
        $sql .= '       end )           as "d2promises__unit_name"';
        $sql .= '   , ( case ';
        $sql .= '       when b.is_deleted = true then false ';
        $sql .= '       when b.remain_count = 0 then false ';
        $sql .= '       else true ';
        $sql .= '       end )           as "d2promises__canCancel"';
        $sql .= '   , i.name            as "d2promises__class_name"';
        $sql .= '   , b.quantity        as "d2promises__quantity"';
        $sql .= ' from ';
        $sql .= '   trn_order_headers as a  ';
        $sql .= '   left join trn_orders as b  ';
        $sql .= '     on b.trn_order_header_id = a.id  ';
        $sql .= '   left join mst_item_units as c  ';
        $sql .= '     on c.id = b.mst_item_unit_id  ';
        
        $sql .= '   left join mst_unit_names as c1  ';
        $sql .= '     on c1.id = c.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as c2  ';
        $sql .= '     on c2.id = c.per_unit_name_id  ';
        
        $sql .= '   left join mst_facility_items as d  ';
        $sql .= '     on d.id = c.mst_facility_item_id  ';
        $sql .= '   left join mst_users as e  ';
        $sql .= '     on e.id = b.creater  ';
        $sql .= '   left join mst_departments as f  ';
        $sql .= '     on f.id = a.department_id_from  ';
        $sql .= '   left join mst_facilities as g  ';
        $sql .= '     on g.id = f.mst_facility_id  ';
        $sql .= '   left join mst_dealers as h ';
        $sql .= '     on h.id = d.mst_dealer_id';
        $sql .= '   left join mst_classes as i ';
        $sql .= '     on i.id = b.work_type ';
        $sql .= ' where ';
        $sql .= '   a.order_type = ' . Configure::read('OrderTypes.direct');
        $sql .= '   and g.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= $where;
        $sql .= ' order by ';
        $sql .= '   b.work_no ';
        $sql .= ' , b.work_seq ';
        
        $result = $this->TrnOrder->query($sql , false , $split_flg);
        return $result;
    }

    public function export_csv(){
        $where = '';
        //要求番号
        if(isset($this->request->data['d2promises']['work_no']) && $this->request->data['d2promises']['work_no'] != "" ){
            $where .= " and a.work_no = '" . $this->request->data['d2promises']['work_no'] . "'";
        }
        //要求日From
        if(isset($this->request->data['d2promises']['work_date_from']) && $this->request->data['d2promises']['work_date_from'] != "" ){
            $where .= " and a.work_date >= '" . $this->request->data['d2promises']['work_date_from'] . "'";
        }
        //要求日To
        if(isset($this->request->data['d2promises']['work_date_to']) && $this->request->data['d2promises']['work_date_to'] != "" ){
            $where .= " and a.work_date <= '" . $this->request->data['d2promises']['work_date_to'] . "'";
        }
        
        //取消も表示する
        if(isset($this->request->data['d2promises']['is_deleted']) && $this->request->data['d2promises']['is_deleted'] != "1" ){
            $where .= " and a.is_deleted = false ";
            $where .= " and b.is_deleted = false ";
        }
        
        //要求施設
        if(isset($this->request->data['d2promises']['facilityCode']) && $this->request->data['d2promises']['facilityCode'] != "" ){
            $where .= " and g.facility_code = '" . $this->request->data['d2promises']['facilityCode'] . "'";
        }
        
        //要求部署
        if(isset($this->request->data['d2promises']['departmentCode']) && $this->request->data['d2promises']['departmentCode'] != "" ){
            $where .= " and f.department_code = '" . $this->request->data['d2promises']['departmentCode'] . "'";
        }
        
        //商品ID
        if(isset($this->request->data['d2promises']['internal_code']) && $this->request->data['d2promises']['internal_code'] != "" ){
            $where .= " and d.internal_code = '" . $this->request->data['d2promises']['internal_code'] . "'";
        }
        
        //製品番号
        if(isset($this->request->data['d2promises']['item_code']) && $this->request->data['d2promises']['item_code'] != "" ){
            $where .= " and d.item_code = '%" . $this->request->data['d2promises']['item_code'] . "%'";
        }
        
        //商品名
        if(isset($this->request->data['d2promises']['item_name']) && $this->request->data['d2promises']['item_name'] != "" ){
            $where .= " and d.item_name = '" . $this->request->data['d2promises']['item_name'] . "'";
        }
        
        //販売元
        if(isset($this->request->data['d2promises']['dealer_name']) && $this->request->data['d2promises']['dealer_name'] != "" ){
            $where .= " and h.dealer_name = '%" . $this->request->data['d2promises']['dealer_name'] . "%'";
        }
        
        //作業区分
        if(isset($this->request->data['d2promises']['work_type']) && $this->request->data['d2promises']['work_type'] != "" ){
            $where .= " and b.work_type = '" . $this->request->data['d2promises']['work_type'] . "'";
        }
        
        //規格
        if(isset($this->request->data['d2promises']['standard']) && $this->request->data['d2promises']['standard'] != "" ){
            $where .= " and d.standard like '%" . $this->request->data['d2promises']['standard'] . "%'";
        }
        
        //JANコード
        if(isset($this->request->data['d2promises']['jan_code']) && $this->request->data['d2promises']['jan_code'] != "" ){
            $where .= " and d.jan_code = '" . $this->request->data['d2promises']['jan_code'] . "'";
        }
        $sql  = ' select ';
        $sql .= '     a.work_no         as "要求番号"';
        $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') ";
        $sql .= '                       as "要求日"';
        $sql .= '   , g.facility_name   as "要求施設"';
        $sql .= '   , f.department_name as "要求部署" ';
        $sql .= '   , d.internal_code   as "商品ID"';
        $sql .= '   , d.item_name       as "商品名"';
        $sql .= '   , d.item_code       as "製品番号"';
        $sql .= '   , d.jan_code        as "JANコード"';
        $sql .= '   , d.standard        as "規格"';
        $sql .= '   , h.dealer_name     as "販売元"';
        $sql .= '   , ( case ';
        $sql .= '       when c.per_unit = 1 then c1.unit_name ';
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')' ";
        $sql .= '       end )           as "包装単位"';
        $sql .= '   , b.quantity        as "要求数量"';
        $sql .= '   , i.name            as "作業区分"';
        $sql .= '   , b.recital         as "備考"';
        $sql .= '   , e.user_name       as "作成者"';
        $sql .= "   , to_char(b.created, 'YYYY/mm/dd HH24:MI:SS') ";
        $sql .= '                       as "作成日時"';
        $sql .= ' from ';
        $sql .= '   trn_order_headers as a  ';
        $sql .= '   left join trn_orders as b  ';
        $sql .= '     on b.trn_order_header_id = a.id  ';
        $sql .= '   left join mst_item_units as c  ';
        $sql .= '     on c.id = b.mst_item_unit_id  ';
        
        $sql .= '   left join mst_unit_names as c1  ';
        $sql .= '     on c1.id = c.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as c2  ';
        $sql .= '     on c2.id = c.per_unit_name_id  ';
        
        $sql .= '   left join mst_facility_items as d  ';
        $sql .= '     on d.id = c.mst_facility_item_id  ';
        $sql .= '   left join mst_users as e  ';
        $sql .= '     on e.id = b.creater  ';
        $sql .= '   left join mst_departments as f  ';
        $sql .= '     on f.id = a.department_id_from  ';
        $sql .= '   left join mst_facilities as g  ';
        $sql .= '     on g.id = f.mst_facility_id  ';
        $sql .= '   left join mst_dealers as h ';
        $sql .= '     on h.id = d.mst_dealer_id';
        $sql .= '   left join mst_classes as i ';
        $sql .= '     on i.id = b.work_type ';
        $sql .= ' where ';
        $sql .= '   a.order_type = ' . Configure::read('OrderTypes.direct');
        $sql .= '   and g.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= $where;
        $sql .= ' order by ';
        $sql .= '   b.work_no ';
        $sql .= ' , b.work_seq ';

        $this->db_export_csv($sql , "直納要求履歴", 'history');
    }

    /**
     * 帳票印刷
     */
    function report($mode = '' ){
        if($mode == 'order'){
            $this->order_report();
        }elseif($mode == 'remain'){
            $this->remain_report();
        }
    }

    function order_report(){
        $now = date("Y/m/d H:i:s");
        $header_id = $this->request->data['d2promises']['id'];
        $uid = $this->request->data['User']['user_id'];
        
        if(is_array($header_id)){
            $where = ' and a.trn_order_header_id in (' . join(',',$header_id) . ')';
        }else{
            $where = ' and a.trn_order_header_id = ' . $header_id;
        }

        
        $order = $this->_getPrintData($where);

        $columns = array("発注番号",
                         "仕入先名",
                         "納品先名",
                         "施設名",
                         "発注日",
                         "納品先住所",
                         "施設住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "金額",
                         "単価",
                         "明細備考",
                         "包装単位",
                         "課税区分",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $tmpHeader = "";
        $i = 1;
        $tmp_page_no = '';

        foreach($order as $detail){
            $page_no = $detail['TrnOrder']['work_no'].$detail['MstOwner']['id'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }

            if($tmpHeader !== $detail['TrnOrderHeader']['id']){
                $tmpHeader = $detail['TrnOrderHeader']['id'];
                $this->setPrintedDate($detail['TrnOrderHeader']['id'],$now,$uid);
            }

            $price = $detail['TrnOrder']['stocking_price'] * $detail['TrnOrder']['quantity'];
            if($detail['FacilityTo']['round'] == Configure::read("RoundType.Round") ){
                $price = round($price);
            }else if( $detail['FacilityTo']['round'] == Configure::read("RoundType.Ceil") ){
                $price = ceil($price);
            }else if( $detail['FacilityTo']['round'] == Configure::read("RoundType.Floor") ){
                $price = floor($price);
            }else{
                $price = (int)$price;
            }

           $facility_item_code = $detail['MstFacilityItem']['internal_code'];
            if(!empty($detail['MstFacilityItem']['aptage_code'])){
                $facility_item_code .= "／" . $detail['MstFacilityItem']['aptage_code'];
            }

            $data[] = array($detail['TrnOrder']['work_no']                                              //発注番号
                            ,$detail['FacilityTo']['facility_formal_name']."様"                         //仕入先名
                            ,$detail['FacilityFrom']['name']                                            //納品先名
                            ,$detail['MstOwner']['facility_formal_name']                                //施主名
                            ,$detail['TrnOrder']['work_date']                                           //発注日
                            ,$detail['FacilityFrom']['address']                                         //納品先住所
                            ,$detail['MstOwner']['address']                                             //施設住所
                            ,$i                                                                         //行番号
                            ,$detail['MstFacilityItem']['item_name']                                    //商品名
                            ,$detail['MstFacilityItem']['standard']                                     //規格
                            ,$detail['MstFacilityItem']['item_code']                                    //製品番号
                            ,$detail['MstDealer']['dealer_name']                                        //販売元
                            ,$facility_item_code                                                        //商品ID
                            ,number_format($price)                                                      //金額
                            ,$detail['TrnOrder']['stocking_price']                                      //単価
                            ,$detail['TrnOrder']['recital']                                             //明細備考
                            ,$detail['TrnOrder']['quantity'] . $detail['MstFacilityItem']['unit_name']  //包装単位
                            ,$detail['MstFacilityItem']['tax_type_name']                                //課税区分
                            ,$detail['TrnOrder']['quantity']                                            //数量
                            ,$detail['TrnOrderHeader']['recital']                                       //ヘッダ備考
                            ,$detail['TrnOrderHeader']['printed_date']                                  //印刷年月日
                            ,$detail['FacilityTo']['round']                                             //丸め区分
                            ,$detail['TrnOrder']['is_deleted']                                          //削除フラグ
                            ,$detail['TrnOrder']['change']                                              //変更フラグ
                            ,$page_no                                                                   //改ページ番号
                            );
            $i++;
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'発注書');
        $this->xml_output($data,join("\t",$columns),$layout_name['10'],$fix_value);

        $errors = array();
        $errors = array_merge($errors
                              ,is_array($this->validateErrors($this->TrnOrderHeader)) ? $this->validateErrors($this->TrnOrderHeader) : array());
        if(count($errors) > 0){
            $this->TrnOrderHeader->rollback();
        }else{
            $this->TrnOrderHeader->commit();
        }
    }

    /* 未入荷一覧 */
    function remain_report(){
        /* 検索条件 */
        $where = '';

        $where .= ' and a.remain_count > 0 ';
        $where .= ' and a.is_deleted = false ';
        
        $order = $this->_getPrintData($where);
 
        $columns = array("発注番号",
                         "仕入先名",
                         "納品先名",
                         "施設名",
                         "発注日",
                         "納品先住所",
                         "施設住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "金額",
                         "単価",
                         "明細備考",
                         "包装単位",
                         "課税区分",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $tmpHeader = "";
        $i = 1;
        $tmp_page_no = '';

        foreach($order as $detail){
            $page_no = $detail['TrnOrder']['work_no'].$detail['MstOwner']['id'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }

            if($tmpHeader !== $detail['TrnOrderHeader']['id']){
                $tmpHeader = $detail['TrnOrderHeader']['id'];
            }

            $price = $detail['TrnOrder']['stocking_price'] * $detail['TrnOrder']['quantity'];
            if($detail['FacilityTo']['round'] == Configure::read("RoundType.Round") ){
                $price = round($price);
            }else if( $detail['FacilityTo']['round'] == Configure::read("RoundType.Ceil") ){
                $price = ceil($price);
            }else if( $detail['FacilityTo']['round'] == Configure::read("RoundType.Floor") ){
                $price = floor($price);
            }else{
                $price = (int)$price;
            }

            $data[] = array($detail['TrnOrder']['work_no']                                              //発注番号
                            ,$detail['FacilityTo']['facility_formal_name']."様"                         //仕入先名
                            ,$detail['FacilityFrom']['name']                                            //納品先名
                            ,$detail['MstOwner']['facility_formal_name']                                //施主名
                            ,$detail['TrnOrder']['work_date']                                           //発注日
                            ,$detail['FacilityFrom']['address']                                         //納品先住所
                            ,$detail['MstOwner']['address']                                             //施設住所
                            ,$i                                                                         //行番号
                            ,$detail['MstFacilityItem']['item_name']                                    //商品名
                            ,$detail['MstFacilityItem']['standard']                                     //規格
                            ,$detail['MstFacilityItem']['item_code']                                    //製品番号
                            ,$detail['MstDealer']['dealer_name']                                        //販売元
                            ,$detail['MstFacilityItem']['internal_code']                                //商品ID
                            ,number_format($price)                                                      //金額
                            ,$detail['TrnOrder']['stocking_price']                                      //単価
                            ,$detail['TrnOrder']['recital']                                             //明細備考
                            ,$detail['TrnOrder']['quantity'] . $detail['MstFacilityItem']['unit_name']  //包装単位
                            ,$detail['MstFacilityItem']['tax_type_name']                                //課税区分
                            ,$detail['TrnOrder']['quantity']                                            //数量
                            ,$detail['TrnOrderHeader']['recital']                                       //ヘッダ備考
                            ,$detail['TrnOrderHeader']['printed_date']                                  //印刷年月日
                            ,$detail['FacilityTo']['round']                                             //丸め区分
                            ,$detail['TrnOrder']['is_deleted']                                          //削除フラグ
                            ,$detail['TrnOrder']['change']                                              //変更フラグ
                            ,$page_no                                                                   //改ページ番号
                            );
            $i++;
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'未受領一覧');
        $this->xml_output($data,join("\t",$columns),$layout_name['10'],$fix_value);
    }
    
    
    //発注帳票データ取得
    function _getPrintData ($where = '') {
        $sql  = ' select ';
        $sql .= '     a.id                     as "TrnOrder__id"';
        $sql .= '   , a.work_no                as "TrnOrder__work_no"';
        $sql .= '   , a.work_seq               as "TrnOrder__work_seq"';
        $sql .= "   , to_char(a.work_date,'YYYY年MM月dd日')";
        $sql .= '                              as "TrnOrder__work_date"';
        $sql .= '   , a.work_type              as "TrnOrder__work_type"';
        $sql .= '   , a.recital                as "TrnOrder__recital"';
        $sql .= '   , a.order_type             as "TrnOrder__order_type"';
        $sql .= '   , a.order_status           as "TrnOrder__order_status"';
        $sql .= '   , a.quantity               as "TrnOrder__quantity"';
        $sql .= '   , ( case ';
        $sql .= '       when a.before_quantity = a.quantity then 0 ';
        $sql .= '       else 1 ';
        $sql .= '       end )                  as "TrnOrder__change"';
        $sql .= '   , a.remain_count           as "TrnOrder__remain_count"';
        $sql .= '   , a.stocking_price         as "TrnOrder__stocking_price"';
        $sql .= '   , ( case ';
        $sql .= '       when a.is_deleted = true then 1 ';
        $sql .= '       else 0 ';
        $sql .= '       end )                  as "TrnOrder__is_deleted"';
        $sql .= '   , a1.id                    as "TrnOrderHeader__id"';
        $sql .= '   , a1.recital               as "TrnOrderHeader__recital"';
        $sql .= '   , a1.printed_date          as "TrnOrderHeader__printed_date"';
        $sql .= '   , ( case when b.per_unit = 1 then b1.unit_name ';
        $sql .= "       else b1.unit_name || '(' ||  b.per_unit || b2.unit_name || ')' ";
        $sql .= '       end )                  as "MstFacilityItem__unit_name"';
        $sql .= '   , c.internal_code          as "MstFacilityItem__internal_code"';
        $sql .= '   , c.item_name              as "MstFacilityItem__item_name"';
        $sql .= '   , c.standard               as "MstFacilityItem__standard"';
        $sql .= '   , c.item_code              as "MstFacilityItem__item_code"';
        $sql .= '   , c.spare_key2             as "MstFacilityItem__aptage_code"';
        $sql .= '   , ( case ';
        foreach(Configure::read('PrintFacilityItems.taxes') as $k=>$v){
            $sql .= '       when c.tax_type =' . $k . " then '" . $v . "'";
        }
        $sql .= "       else '' ";
        $sql .= '      end )                   as "MstFacilityItem__tax_type_name"';
        $sql .= '   , d.dealer_name            as "MstDealer__dealer_name"';
        $sql .= '   , e.id                     as "MstOwner__id"';
        $sql .= '   , e.facility_formal_name   as "MstOwner__facility_formal_name"';
        $sql .= "   , '〒' || coalesce(e.zip,'') || ' ' || coalesce(e.address,'') || '\nTel:' || coalesce(e.tel,'') || '／Fax:' || coalesce(e.fax,'')";
        $sql .= '                              as "MstOwner__address"';
        $sql .= '   , f.id                     as "DepartmentFrom__id"';
        $sql .= '   , f.department_formal_name as "DepartmentFrom__department_formal_name"';
        $sql .= '   , ( case ';
        $sql .= '       when g.facility_type = ' . Configure::read('FacilityType.hospital') ;
        $sql .= "       then g.facility_formal_name || ' ' || f.department_formal_name ";
        $sql .= '       else g.facility_formal_name ';
        $sql .= '       end )                 as "FacilityFrom__name"';
        
        $sql .= '   , g.id                     as "FacilityFrom__id"';
        $sql .= '   , g.facility_formal_name   as "FacilityFrom__facility_formal_name"';
        $sql .= "   , '〒' || coalesce(g.zip,'') || ' ' || coalesce(g.address,'') || '\nTel:' || coalesce(g.tel,'') || '／Fax:' || coalesce(g.fax,'')";
        $sql .= '                              as "FacilityFrom__address"';
        $sql .= '   , h.id                     as "DepartmentTo__id"';
        $sql .= '   , h.department_formal_name as "DepartmentTo__department_formal_name"';
        $sql .= '   , i.id                     as "FacilityTo__id"';
        $sql .= '   , i.facility_formal_name   as "FacilityTo__facility_formal_name"';
        $sql .= '   , i.facility_type          as "FacilityTo__facility_type"';
        $sql .= '   , i.zip                    as "FacilityTo__zip"';
        $sql .= '   , i.address                as "FacilityTo__address"';
        $sql .= '   , i.tel                    as "FacilityTo__tel"';
        $sql .= '   , i.fax                    as "FacilityTo__fax"';
        $sql .= '   , i.gross                  as "FacilityTo__gross"';
        $sql .= '   , i.round                  as "FacilityTo__round"';
        $sql .= ' from ';
        $sql .= '   trn_orders as a  ';
        $sql .= '   left join trn_order_headers as a1  ';
        $sql .= '     on a1.id = a.trn_order_header_id  ';
        $sql .= '   left join mst_item_units as b  ';
        $sql .= '     on b.id = a.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as b1  ';
        $sql .= '     on b1.id = b.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as b2  ';
        $sql .= '     on b2.id = b.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as c  ';
        $sql .= '     on c.id = b.mst_facility_item_id  ';
        $sql .= '   left join mst_dealers as d  ';
        $sql .= '     on d.id = c.mst_dealer_id  ';
        $sql .= '   left join mst_facilities as e  ';
        $sql .= '     on e.id = c.mst_owner_id  ';
        $sql .= '     and e.facility_type = ' . Configure::read('FacilityType.donor');
        $sql .= '   left join mst_departments as f  ';
        $sql .= '     on f.id = a.department_id_from  ';
        $sql .= '   left join mst_facilities as g  ';
        $sql .= '     on g.id = f.mst_facility_id  ';
        $sql .= '   left join mst_departments as h  ';
        $sql .= '     on h.id = a.department_id_to  ';
        $sql .= '   left join mst_facilities as i  ';
        $sql .= '     on i.id = h.mst_facility_id  ';
        $sql .= ' where ';
        $sql .= ' a.order_type = ' . Configure::read('OrderTypes.direct');
        $sql .= $where;
        $sql .= ' order by ';
        $sql .= '   a.trn_order_header_id ';
        $sql .= '   , e.id ';
        $sql .= '   , d.dealer_name ';
        $sql .= '   , c.item_name ';
        $sql .= '   , c.item_code ';
        
        $order = $this->TrnOrder->query($sql);
        
        return $order;
    }
    
    /**
     * 発注明細の印刷日時更新
     */
    private function setPrintedDate($id,$date = null,$uid = null){
        if(isset($uid)){
            $userId = $uid;
        }else{
            $userId = $this->Session->read('Auth.MstUser.id');
        }
        if(isset($date) && $date!==""){
            $date = date('Y-m-d H:i:s');
        }

        //在庫情報を更新
        $printed_date = array('modified'       => "'" . $date . "'"
                              ,'printed_date'  => "'" . $date . "'"
                              ,'modifier'      => $userId
                              );
        $this->TrnOrderHeader->updateAll($printed_date,array(
            'TrnOrderHeader.id' => $id
        ),-1);

    }
}

