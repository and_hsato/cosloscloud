<?php
/**
 * ClosesController
 *　締め
 * @version 1.0.0
 * @since 2010/09/10
 */
class ClosesController extends AppController {

  /**
   * @var $name
   */
  var $name = 'Closes';

  /**
   * @var array $uses
   */
  var $uses = array(
      'MstClass',
      'TrnReceiving',
      'TrnClaim',
      'TrnCloseHeader',
      'TrnCloseRecord',
      'TrnAppraisedValue',
      'TrnConsume',
      'MstFacilityRelation',
      'MstFacility'
  );

  /**
   * @var bool $scaffold
   */
  //var $scaffold;

  /**
   * @var array $helpers
   */
  var $helpers = array('Form', 'Html','Time');

  /**
   * @var array $components
   */
  var $components = array('RequestHandler','Common');

  /**
   *
   */
  public function index (){
      //作業区分取得
      $classes = $this->getClassesList( $this->Session->read('Auth.facility_id_selected') ,'20');
      //締め年月
      $this->request->data['TrnClose']['close_month'] = $this->getCloseMonth();

      //得意先一覧を取得
      $partners = $this->getClosedInfomation(Configure::read('CloseType.sales'));
      $this->set('partners', $partners);

      //仮締めが行われているかどうか
      if(count($partners) === 0 || $partners[0]['TrnCloseHeader']['is_provisional'] == ''){
          $isTemporary = true;
      }else{
          $isTemporary = false;
      }

      $this->set('isTemporary', $isTemporary);

      //仕入一覧を取得
      $supplier = $this->getClosedInfomation(Configure::read('CloseType.supply'));
      $this->set('suppliers', $supplier);
      $this->set('classes', $classes);
      $this->render('/closes/index');
  }

  /**
   * 仮締めを実行
   * @access public action method
   * @return void
   */
  public function temporary(){
      //締め年月の月末日を取得
      $endDate = date('Y/m/d', strtotime('1 month -1 day',strtotime($this->getCloseMonth() . '/01')));

      //すでに締められているかどうか
      if(true === $this->isAlreadyClosed(true, $endDate)){
          $this->Session->setFlash('すでに仮締めが行われています', 'growl', array('type'=>'error'));
          $this->index();
          return;
      }

      //仮締めを実行
      $startDate = substr($endDate, 0, 7) . '/01';

      //トランザクション開始
      $this->TrnClaim->begin();

      //行ロック
      $this->TrnClaim->query( " select * from trn_claims as a where (a.department_id_from in (select c.id from mst_facility_relations as a left join mst_facilities as b on b.id = a.mst_facility_id or b.id = a.partner_facility_id left join mst_departments as c on c.mst_facility_id = b.id where c.department_type in (" . Configure::read('DepartmentType.warehouse') . ", " . Configure::read('DepartmentType.hospital') . ") and b.id = ".$this->Session->read('Auth.facility_id_selected')." group by c.id) or a.department_id_to in (select c.id from mst_facility_relations as a left join mst_facilities as b on b.id = a.mst_facility_id or b.id = a.partner_facility_id left join mst_departments as c on c.mst_facility_id = b.id where c.department_type in (". Configure::read('DepartmentType.warehouse') .", " . Configure::read('DepartmentType.hospital') . ") and b.id = ".$this->Session->read('Auth.facility_id_selected')." group by c.id)) and a.claim_date between '${startDate}' and '${endDate}' for update ");
      $this->TrnReceiving->query( " select * from trn_receivings as a where (a.department_id_from in (select c.id from mst_facility_relations as a left join mst_facilities as b on b.id = a.mst_facility_id or b.id = a.partner_facility_id left join mst_departments as c on c.mst_facility_id = b.id where c.department_type in (" . Configure::read('DepartmentType.warehouse') . ", " . Configure::read('DepartmentType.hospital') . ") and b.id = ".$this->Session->read('Auth.facility_id_selected')." group by c.id) or a.department_id_to in (select c.id from mst_facility_relations as a left join mst_facilities as b on b.id = a.mst_facility_id or b.id = a.partner_facility_id left join mst_departments as c on c.mst_facility_id = b.id where c.department_type in (" . Configure::read('DepartmentType.warehouse') . "," . Configure::read('DepartmentType.hospital') . ") and b.id = ".$this->Session->read('Auth.facility_id_selected')." group by c.id)) and a.work_date between '${startDate}' and '${endDate}' for update " );
      $this->TrnConsume->query(" select * from trn_consumes as a where a.mst_department_id in (select c.id from mst_facility_relations as a left join mst_facilities as b on b.id = a.mst_facility_id or b.id = a.partner_facility_id left join mst_departments as c on c.mst_facility_id = b.id where c.department_type in (" . Configure::read('DepartmentType.warehouse') . ", " . Configure::read('DepartmentType.hospital') . ") and b.id = ".$this->Session->read('Auth.facility_id_selected')." group by c.id) and a.work_date between '${startDate}' and '${endDate}' for update ");

      try {
          //仕入締めデータ作成
          $this->makeFacilityData($startDate, $endDate, true);

          //売上締めデータ作成
          $this->makeSalesData($startDate, $endDate, true);

          //在庫締めデータ作成
          $this->makeStocksData($startDate, $endDate, true);

      } catch (Exception $ex) {
          $this->TrnClaim->rollback();
          throw new Exception($ex->getMessage());
      }


      //2重締めになって無いかチェック
      $sql  = ' select ';
      $sql .= '       count(*)        as count  ';
      $sql .= '   from ';
      $sql .= '     trn_close_headers as a  ';
      $sql .= '   where ';
      $sql .= '     a.is_deleted = false  ';
      $sql .= '     and a.close_type = ' . Configure::read('CloseType.stock');
      $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
      $sql .= '     and a.is_provisional = true  ';
      $sql .= "     and a.start_date = '".$startDate."' ";
      $sql .= "     and a.end_date = '".$endDate."' ";

      $ret = $this->TrnClaim->query($sql);

      if($ret[0][0]['count'] > 1){
          $this->TrnClaim->rollback();
          $this->Session->setFlash('既に仮締めが行われています。', 'growl', array('type'=>'error'));
          $this->index();
      }

      $this->TrnClaim->commit();

      $this->render('/closes/results');
    }

    public function complete(){
        //締め年月の月末日を取得
        $endDate = $this->getLastClosedDay();

        //すでに実行されているかどうか
        if(true === $this->isAlreadyClosed(false, $endDate)){
            $this->Session->setFlash('すでに本締めが行われています', 'growl', array('type'=>'error'));
            $this->index();
            return;
        }

        //本締めを実行
        $startDate = substr($endDate, 0, 7) . '/01';

        //トランザクション開始
        $this->TrnClaim->begin();

        //行ロック
        $this->TrnClaim->query( " select * from trn_claims as a where (a.department_id_from in (select c.id from mst_facility_relations as a left join mst_facilities as b on b.id = a.mst_facility_id or b.id = a.partner_facility_id left join mst_departments as c on c.mst_facility_id = b.id where c.department_type in (" . Configure::read('DepartmentType.warehouse') . ", " . Configure::read('DepartmentType.hospital') . ") and b.id = ".$this->Session->read('Auth.facility_id_selected')." group by c.id) or a.department_id_to in (select c.id from mst_facility_relations as a left join mst_facilities as b on b.id = a.mst_facility_id or b.id = a.partner_facility_id left join mst_departments as c on c.mst_facility_id = b.id where c.department_type in (1, 2) and b.id = ".$this->Session->read('Auth.facility_id_selected')." group by c.id)) and a.claim_date between '${startDate}' and '${endDate}' for update ");
        $this->TrnReceiving->query( " select * from trn_receivings as a where (a.department_id_from in (select c.id from mst_facility_relations as a left join mst_facilities as b on b.id = a.mst_facility_id or b.id = a.partner_facility_id left join mst_departments as c on c.mst_facility_id = b.id where c.department_type in (" . Configure::read('DepartmentType.warehouse') . ", " . Configure::read('DepartmentType.hospital') . ") and b.id = ".$this->Session->read('Auth.facility_id_selected')." group by c.id) or a.department_id_to in (select c.id from mst_facility_relations as a left join mst_facilities as b on b.id = a.mst_facility_id or b.id = a.partner_facility_id left join mst_departments as c on c.mst_facility_id = b.id where c.department_type in (1, 2) and b.id = ".$this->Session->read('Auth.facility_id_selected')." group by c.id)) and a.work_date between '${startDate}' and '${endDate}' for update " );
        $this->TrnConsume->query(" select * from trn_consumes as a where a.mst_department_id in (select c.id from mst_facility_relations as a left join mst_facilities as b on b.id = a.mst_facility_id or b.id = a.partner_facility_id left join mst_departments as c on c.mst_facility_id = b.id where c.department_type in (" . Configure::read('DepartmentType.warehouse') . ", " . Configure::read('DepartmentType.hospital') . ") and b.id = ".$this->Session->read('Auth.facility_id_selected')." group by c.id) and a.work_date between '${startDate}' and '${endDate}' for update ");

        try {
            //仕入締めデータ作成
            $this->makeFacilityData($startDate, $endDate, false);

            //売上締めデータ作成
            $this->makeSalesData($startDate, $endDate, false);

            //在庫締めデータ作成
            $this->makeStocksData($startDate, $endDate, false);

        } catch (Exception $ex) {
            $this->TrnClaim->rollback();
            throw new Exception($ex->getMessage());
        }

        //2重締めになって無いかチェック
        $sql  = ' select ';
        $sql .= '       count(*)        as count  ';
        $sql .= '   from ';
        $sql .= '     trn_close_headers as a  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.close_type = ' . Configure::read('CloseType.stock');
        $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and a.is_provisional = false  ';
        $sql .= "     and a.start_date = '".$startDate."' ";
        $sql .= "     and a.end_date = '".$endDate."' ";

        $ret = $this->TrnClaim->query($sql);

        if($ret[0][0]['count'] > 1){
            $this->TrnClaim->rollback();
            $this->Session->setFlash('既に本締めが行われています。', 'growl', array('type'=>'error'));
            $this->index();
        }

        $this->TrnClaim->commit();

        $this->render('/closes/results');
    }

    private function getLastClosedDay(){
        $res = $this->TrnCloseHeader->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'is_provisional' => true,
                'is_deleted' => false,
                'mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
                ),
            'order' => array(
                'end_date desc',
                ),
            ));
        return date('Y/m/d', strtotime($res['TrnCloseHeader']['end_date']));
    }

    /**
     * 仕入締めデータ作成
     * @param string $startDate
     * @param string $endDate
     * @param string $is_provisional
     * @return void
     */
    private function makeFacilityData($startDate, $endDate, $is_provisional){
      $now = date('Y/m/d H:i:s');

      //入荷テーブル更新
      $sql  = "";
      $sql .= ' update trn_receivings set ';
      $sql .= "     stocking_close_type = '" . ($is_provisional ? '1' : '2') . "' ";
      $sql .= "    ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "' ";
      $sql .= "    ,modified = '" . $now ."'";
      $sql .= ' from ';
      $sql .= '     trn_claims as TrnClaim ';
      $sql .= '     inner join mst_item_units as MstItemUnit ';
      $sql .= '         on TrnClaim.mst_item_unit_id=MstItemUnit.id ';
      $sql .= '             and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
      $sql .= ' where ';
      $sql .= '     TrnClaim.trn_receiving_id = trn_receivings.id ';
      $sql .= "     and TrnClaim.is_stock_or_sale =  '1' ";
      $sql .= "     and TrnClaim.claim_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '     and TrnClaim.is_deleted = false ';
      $sql .= '     and trn_receivings.work_date is not null'; //返品予約状態があるため
      $res = $this->TrnClaim->query($sql);
      if(false === $res){
          throw new Exception("仕入締め＠入荷テーブル更新失敗\n{$startDate}\n{$endDate}\n{$sql}");
      }

      //消費テーブル更新
      $sql  = "";
      $sql .= ' update trn_consumes set ';
      $sql .= "     stocking_close_type = '" . ($is_provisional ? '1' : '2') . '" ';
      $sql .= "    ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "' ";
      $sql .= "    ,modified = '" . $now . "'";
      $sql .= ' from ';
      $sql .= '     trn_claims as TrnClaim ';
      $sql .= '     inner join mst_item_units as MstItemUnit ';
      $sql .= '         on TrnClaim.mst_item_unit_id=MstItemUnit.id ';
      $sql .= '             and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
      $sql .= ' where ';
      $sql .= '     TrnClaim.trn_consume_id = trn_consumes.id ';
      $sql .= "     and TrnClaim.is_stock_or_sale =  '1' ";
      $sql .= "     and TrnClaim.claim_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '     and TrnClaim.is_deleted = false ';
      if(false === $res){
          throw new Exception("仕入締め＠消費テーブル更新失敗\n{$startDate}\n{$endDate}\n{$sql}");
      }
      //請求テーブル更新
      $sql  = "";
      $sql .= ' update trn_claims set ';
      $sql .= "     stocking_close_type = '" . ($is_provisional ? '1' : '2') . "' ";
      $sql .= "    ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "' ";
      $sql .= "    ,modified = '" . $now . "'";
      $sql .= ' from ';
      $sql .= '     mst_item_units as MstItemUnit ';
      $sql .= ' where ';
      $sql .= '     trn_claims.mst_item_unit_id=MstItemUnit.id ';
      $sql .= '     and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
      $sql .= "     and trn_claims.is_stock_or_sale in ( '1' , '3' )";
      $sql .= "     and trn_claims.claim_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '     and trn_claims.is_deleted = false ';
      $res = $this->TrnClaim->query($sql);
      if(false === $res){
          throw new Exception("仕入締め＠請求テーブル更新失敗\n{$startDate}\n{$endDate}\n{$sql}");
      }

      //締めテーブル作成
      $this->TrnClaim->bindModel(array(
          'belongsTo' => array(
              'MstDepartment' => array(
                'className'  => 'MstDepartment',
                'foreignKey' => 'department_id_from',
              ),
          ),
      ));

      $facilities = $this->MstFacilityRelation->find('all', array(
          'recursive' => -1,
          'fields' => array(
              'MstFacility.id',
          ),
          'joins' => array(
              array(
                  'type'       => 'inner',
                  'table'      => 'mst_facilities',
                  'alias'      => 'MstFacility',
                  'conditions' => 'MstFacilityRelation.partner_facility_id = MstFacility.id',
              ),
          ),
          'conditions' => array(
              'MstFacilityRelation.is_deleted' => false,
              'MstFacility.facility_type' => Configure::read('FacilityType.supplier'),
              'MstFacilityRelation.mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
          ),
          'order' => array(
              'MstFacility.id asc',
          ),
      ));

      //締めレコード作成
      foreach($facilities as $row){
          $this->TrnCloseHeader->create(false);
          $workNo = $this->setWorkNo4Header($endDate, '20');
          //締めヘッダ作成
          $res = $this->TrnCloseHeader->save(array(
              'id'                  => false,
              'work_no'             => $workNo,
              'work_date'           => $endDate,
              'work_type'           => $this->request->data['MstClass']['id'],
              'recital'             => $this->request->data['TrnCloseHeader']['recital'],
              'close_type'          => Configure::read('CloseType.supply'), //仕入れ締め
              'mst_facility_id'     => $this->Session->read('Auth.facility_id_selected'),
              'partner_facility_id' => $row['MstFacility']['id'],
              'start_date'          => $startDate,
              'end_date'            => $endDate,
              'taxable_amount'      => 0, //あとで更新
              'nontaxable_amount'   => 0, //あとで更新
              'is_deleted'          => false,
              'is_provisional'      => $is_provisional,
              'creater'             => $this->Session->read('Auth.MstUser.id'),
              'created'             => $now,
              'modifier'            => $this->Session->read('Auth.MstUser.id'),
              'modified'            => $now,
              ));
          if(false === $res){
              throw new Exception("締めヘッダ作成エラー\n" . print_r($this->validateErrors($this->TrnCloseHeader),true));
          }
          $lastHeaderId = $this->TrnCloseHeader->getLastInsertID();

          //締め明細作成
          $sql  = "";
          $sql .= ' insert into trn_close_records(';
          $sql .= '     work_no';
          $sql .= '    ,close_type';
          $sql .= '    ,target_date';
          $sql .= '    ,mst_facility_id';
          $sql .= '    ,mst_facility_item_id';
          $sql .= '    ,mst_item_unit_id';
          $sql .= '    ,mst_facility_id_to';
          $sql .= '    ,mst_department_id_to';
          $sql .= '    ,quantity';
          $sql .= '    ,taxable_amount';
          $sql .= '    ,nontaxable_amount';
          $sql .= '    ,trn_close_header_id';
          $sql .= '    ,is_provisional';
          $sql .= '    ,creater';
          $sql .= '    ,created';
          $sql .= '    ,modifier';
          $sql .= '    ,modified';

          if(Configure::read('SplitTable.flag') == 1){
              $sql .= ',center_facility_id' ; //分割テーブル対応
          }

          $sql .= ' )';
          $sql .= ' select';
          $sql .= "     '{$workNo}' as work_no";
          $sql .= "    ,'" . Configure::read('CloseType.supply') . "' as close_type";
          $sql .= '    ,target_date';
          $sql .= '    ,mst_facility_id';
          $sql .= '    ,mst_facility_item_id';
          $sql .= '    ,mst_item_unit_id';
          $sql .= '    ,mst_facility_id_to';
          $sql .= '    ,mst_department_id_to';
          $sql .= '    ,sum(quantity) as quantity';
          $sql .= '    ,sum(taxable_amount) as taxable_amount';
          $sql .= '    ,sum(nontaxable_amount) as nontaxable_amount';
          $sql .= "    ,{$lastHeaderId} as trn_close_header_id";
          $sql .= '    ,' . ($is_provisional ? 'true' : 'false') . ' as is_procisional';
          $sql .= '    ,' . $this->Session->read('Auth.MstUser.id');
          $sql .= "    ,'".$now."'";
          $sql .= '    ,' . $this->Session->read('Auth.MstUser.id');
          $sql .= "    ,'".$now."'" ;

          if(Configure::read('SplitTable.flag') == 1){
               $sql .= ',' . $this->Session->read('Auth.facility_id_selected') ; //分割テーブル対応
          }

          $sql .= ' from ';
          $sql .= '     (select';
          $sql .= '          a.claim_date as target_date';
          $sql .= '         ,b.mst_facility_item_id';
          $sql .= '         ,b.id as mst_item_unit_id';
          $sql .= '         ,c.mst_facility_id';
          $sql .= '         ,d.mst_facility_id as mst_facility_id_to';
          $sql .= '         ,a.department_id_from as mst_department_id_to';
          $sql .= '         ,(case when a.claim_price < 0 then -1 * a.count else a.count end) as quantity';
          $sql .= '         ,(case when a.claim_price < 0 then 2 else 1 end) as sum_type';
          $sql .= '         ,(case c.tax_type when 1 then 0 else a.claim_price end) as taxable_amount';
          $sql .= '         ,(case c.tax_type when 1 then a.claim_price else 0 end) as nontaxable_amount';
          $sql .= '      from ';
          $sql .= '          trn_claims as a';
          $sql .= '         ,mst_item_units as b';
          $sql .= '         ,mst_facility_items as c';
          $sql .= '         ,mst_departments as d';
          $sql .= '         ,mst_facilities as e';
          $sql .= '      where ';
          $sql .= '          a.mst_item_unit_id = b.id';
          $sql .= '          and b.mst_facility_item_id = c.id';
          $sql .= '          and a.department_id_from = d.id';
          $sql .= '          and c.mst_facility_id = e.id ';
          $sql .= "          and a.is_stock_or_sale = '1'";
          $sql .= '          and a.is_deleted = false';
          $sql .= "          and a.claim_date between '{$startDate}' and '{$endDate}' ";
          $sql .= "          and d.mst_facility_id = '{$row['MstFacility']['id']}' ";
          $sql .= '          and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
          $sql .= '          and d.department_type = ' . Configure::read('DepartmentType.supplier');    //仕入先固定
          $sql .= '      union all ';
          //預託品返却
          $sql .= '      select ';
          $sql .= '          c.claim_date ';
          $sql .= '        , d.mst_facility_item_id ';
          $sql .= '        , b.mst_item_unit_id ';
          $sql .= '        , d.mst_facility_id ';
          $sql .= '        , i.mst_facility_id as mst_facility_id_to ';
          $sql .= '        , h.department_id_to ';
          $sql .= '        , ( case when c.claim_price < 0 then - 1 * c.count else c.count end ) as quantity ';
          $sql .= '        , ( case when c.claim_price < 0 then 2 else 1 end) as sum_type ';
          $sql .= '        , ( case e.tax_type when 1 then 0 else c.claim_price end ) as taxable_amount ';
          $sql .= '        , ( case e.tax_type when 1 then c.claim_price else 0 end ) as nontaxable_amount ';
          $sql .= '      from ';
          $sql .= '          trn_repay_headers as a ';
          $sql .= '          left join trn_receivings as b ';
          $sql .= '              on b.trn_repay_header_id = a.id ';
          $sql .= '          left join trn_claims as c ';
          $sql .= '              on c.trn_receiving_id = b.id ';
          $sql .= '          left join mst_item_units as d ';
          $sql .= '              on d.id = b.mst_item_unit_id ';
          $sql .= '          left join mst_facility_items as e ';
          $sql .= '              on e.id = d.mst_facility_item_id ';
          $sql .= '          left join mst_departments as f ';
          $sql .= '              on f.id = a.department_id_to ';
          $sql .= '          left join trn_stickers as g ';
          $sql .= '              on g.id = b.trn_sticker_id ';
          $sql .= '          left join trn_orders as h ';
          $sql .= '              on h.id = g.trn_order_id ';
          $sql .= '          left join mst_departments as i ';
          $sql .= '              on i.id = h.department_id_to ';
          $sql .= '      where ';
          $sql .= "          a.work_date between '{$startDate}' and '{$endDate}' ";
          $sql .= '          and a.repay_type = ' . Configure::read('RepayType.replenish');
          $sql .= '          and a.is_deleted = false ';
          $sql .= "          and c.is_stock_or_sale = '1' ";
          $sql .= "          and i.mst_facility_id = '{$row['MstFacility']['id']}' ";
          $sql .= '     ) as a';
          $sql .= ' group by ';
          $sql .= '     target_date';
          $sql .= '    ,mst_facility_item_id';
          $sql .= '    ,mst_item_unit_id';
          $sql .= '    ,mst_facility_id';
          $sql .= '    ,mst_facility_id_to';
          $sql .= '    ,mst_department_id_to';
          $sql .= '    ,sum_type';
          $sql .= ' order by';
          $sql .= '     target_date';
          $sql .= '    ,mst_facility_item_id';
          $sql .= '    ,mst_item_unit_id';
          $sql .= '    ,mst_facility_id';
          $sql .= '    ,mst_facility_id_to';
          $sql .= '    ,mst_department_id_to';
          $sql .= '    ,sum_type';

          $res = $this->TrnClaim->query($sql);

          if(false === $res){
             throw new Exception("締め明細作成エラー\n" . $sql);
          }

          //締めヘッダ再び更新(金額)
          $sql  = "";
          $sql .= ' update trn_close_headers set ';
          $sql .= '     taxable_amount = coalesce(TrnClose.taxable_amount,0) ';
          $sql .= '    ,nontaxable_amount = coalesce(TrnClose.nontaxable_amount,0) ';
          $sql .= ' from ';
          $sql .= '     (select sum(taxable_amount) as taxable_amount, sum(nontaxable_amount) as nontaxable_amount from trn_close_records ';
          $sql .= "     where trn_close_header_id = {$lastHeaderId} ) as TrnClose ";
          $sql .= ' where ';
          $sql .= '     id = ' . $lastHeaderId;
          $res = $this->TrnCloseHeader->query($sql);
          if(false === $res){
              throw new Exception("締めヘッダ更新失敗\n" . $sql);
          }
      }
  }

  /**
   * 売上締めデータ作成
   * @param string $startDate
   * @param string $endDate
   * @param string $is_provisional
   */
  private function makeSalesData($startDate, $endDate, $is_provisional){
      $now = date('Y/m/d H:i:s');
      //入荷テーブル更新
      $sql = "";
      $sql .= ' update trn_receivings set ';
      $sql .= "     sales_close_type = '" . ($is_provisional ? '1' : '2') . "' ";
      $sql .= "    ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "' ";
      $sql .= "    ,modified = '" . $now . "'";
      $sql .= ' from ';
      $sql .= '     trn_claims as TrnClaim ';
      $sql .= '     inner join mst_item_units as MstItemUnit ';
      $sql .= '         on TrnClaim.mst_item_unit_id=MstItemUnit.id ';
      $sql .= '         and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
      $sql .= ' where ';
      $sql .= '     TrnClaim.trn_receiving_id = trn_receivings.id ';
      $sql .= "     and TrnClaim.is_stock_or_sale =  '2' ";
      $sql .= "     and TrnClaim.claim_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '     and TrnClaim.is_deleted = false ';
      $sql .= '     and trn_receivings.work_date is not null '; //返品の予約に対応
      $res = $this->TrnClaim->query($sql);
      if(false === $res){
          throw new Exception("売上締め＠入荷テーブル更新失敗\n{$startDate}\n{$endDate}\n{$sql}");
      }

      //消費テーブル更新
      $sql  = "";
      $sql .= ' update trn_consumes set ';
      $sql .= "     sales_close_type = '" . ($is_provisional ? '1' : '2') . '" ';
      $sql .= "    ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "' ";
      $sql .= "    ,modified = '" . $now . "'";
      $sql .= ' from ';
      $sql .= '     trn_claims as TrnClaim ';
      $sql .= '     inner join mst_item_units as MstItemUnit ';
      $sql .= '         on TrnClaim.mst_item_unit_id=MstItemUnit.id ';
      $sql .= '         and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
      $sql .= ' where ';
      $sql .= '     TrnClaim.trn_consume_id = trn_consumes.id ';
      $sql .= "     and TrnClaim.is_stock_or_sale =  '2' ";
      $sql .= "     and TrnClaim.claim_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '     and TrnClaim.is_deleted = false ';
      if(false === $res){
          throw new Exception("売上締め＠消費テーブル更新失敗\n{$startDate}\n{$endDate}\n{$sql}");
      }

      //請求テーブル更新
      $sql = "";
      $sql .= ' update trn_claims set ';
      $sql .= "     sales_close_type = '" . ($is_provisional ? '1' : '2') . "' ";
      $sql .= "    ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "' ";
      $sql .= "    ,modified = '" . $now . "'";
      $sql .= ' from ';
      $sql .= '     mst_item_units as MstItemUnit ';
      $sql .= ' where ';
      $sql .= '     trn_claims.mst_item_unit_id=MstItemUnit.id ';
      $sql .= '     and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
      $sql .= "     and trn_claims.is_stock_or_sale =  '2' ";
      $sql .= "     and trn_claims.claim_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '     and trn_claims.is_deleted = false ';
      $res = $this->TrnClaim->query($sql);
      if(false === $res){
          throw new Exception("売上締め＠請求テーブル更新失敗\n{$startDate}\n{$endDate}\n{$sql}");
      }

      //締めテーブル作成
      $this->TrnClaim->bindModel(array(
          'belongsTo' => array(
              'MstDepartment' => array(
                'className' => 'MstDepartment',
                'foreignKey' => 'department_id_to',
              ),
          ),
      ));

      $facilities = $this->MstFacilityRelation->find('all', array(
          'recursive' => -1,
          'fields' => array(
              'MstFacility.id',
          ),
          'joins' => array(
              array(
                  'type' => 'inner',
                  'table' => 'mst_facilities',
                  'alias' => 'MstFacility',
                  'conditions' => 'MstFacilityRelation.partner_facility_id = MstFacility.id',
              ),
          ),
          'conditions' => array(
              'MstFacilityRelation.is_deleted' => false,
              'MstFacility.facility_type' => Configure::read('FacilityType.hospital'),
              'MstFacilityRelation.mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
          ),
          'order' => array(
              'MstFacility.id asc',
          ),
      ));

      //締めレコード作成
      foreach($facilities as $row){
          $this->TrnCloseHeader->create(false);
          $workNo = $this->setWorkNo4Header($endDate, '20');
          //締めヘッダ作成
          $res = $this->TrnCloseHeader->save(array(
              'id'                  => false,
              'work_no'             => $workNo,
              'work_date'           => $endDate,
              'work_type'           => $this->request->data['MstClass']['id'],
              'recital'             => $this->request->data['TrnCloseHeader']['recital'],
              'close_type'          => Configure::read('CloseType.sales'),
              'mst_facility_id'     => $this->Session->read('Auth.facility_id_selected'),
              'partner_facility_id' => $row['MstFacility']['id'],
              'start_date'          => $startDate,
              'end_date'            => $endDate,
              'taxable_amount'      => 0, //あとで更新
              'nontaxable_amount'   => 0, //あとで更新
              'is_deleted'          => false,
              'is_provisional'      => $is_provisional,
              'creater'             => $this->Session->read('Auth.MstUser.id'),
              'created'             => $now,
              'modifier'            => $this->Session->read('Auth.MstUser.id'),
              'modified'            => $now,
              ));
          if(false === $res){
              throw new Exception("売上締め＠締めヘッダ作成エラー\n" . print_r($this->validateErrors($this->TrnCloseHeader),true));
          }
          $lastHeaderId = $this->TrnCloseHeader->getLastInsertID();

          //締め明細作成
          $sql  = "";
          $sql .= ' insert into trn_close_records(';
          $sql .= '     work_no';
          $sql .= '    ,close_type';
          $sql .= '    ,target_date';
          $sql .= '    ,mst_facility_id';
          $sql .= '    ,mst_facility_item_id';
          $sql .= '    ,mst_item_unit_id';
          $sql .= '    ,mst_facility_id_to';
          $sql .= '    ,mst_department_id_to';
          $sql .= '    ,quantity';
          $sql .= '    ,taxable_amount';
          $sql .= '    ,nontaxable_amount';
          $sql .= '    ,trn_close_header_id';
          $sql .= '    ,is_provisional';
          $sql .= '    ,creater';
          $sql .= '    ,created';
          $sql .= '    ,modifier';
          $sql .= '    ,modified' ;
          if(Configure::read('SplitTable.flag') == 1){
              $sql .= ',center_facility_id'; //分割テーブル対応
          }
          $sql .= ' ) ';
          $sql .= ' select';
          $sql .= "     '{$workNo}' as work_no";
          $sql .= "    ,'" . Configure::read('CloseType.sales') . "' as close_type";
          $sql .= '    ,target_date';
          $sql .= '    ,mst_facility_id';
          $sql .= '    ,mst_facility_item_id';
          $sql .= '    ,mst_item_unit_id';
          $sql .= '    ,mst_facility_id_to';
          $sql .= '    ,mst_department_id_to';
          $sql .= '    ,sum(quantity) as quantity';
          $sql .= '    ,sum(taxable_amount) as taxable_amount';
          $sql .= '    ,sum(nontaxable_amount) as nontaxable_amount';
          $sql .= "    ,{$lastHeaderId} as trn_close_header_id";
          $sql .= '    ,' . ($is_provisional ? 'true' : 'false') . ' as is_procisional';
          $sql .= '    ,' . $this->Session->read('Auth.MstUser.id');
          $sql .= "    ,'". $now ."'";
          $sql .= '    ,' . $this->Session->read('Auth.MstUser.id');
          $sql .= "    ,'". $now ."'";
          if(Configure::read('SplitTable.flag') == 1){
              $sql .= ',' . $this->Session->read('Auth.facility_id_selected') ; //分割テーブル対応
          }
          $sql .= ' from ';
          $sql .= '     (select';
          $sql .= '          a.claim_date as target_date';
          $sql .= '         ,b.mst_facility_item_id';
          $sql .= '         ,a.mst_item_unit_id';
          $sql .= '         ,c.mst_facility_id as mst_facility_id';
          $sql .= '         ,d.mst_facility_id as mst_facility_id_to';
          $sql .= '         ,a.department_id_to as mst_department_id_to';
          $sql .= '         ,(case when a2.id is not null then -1 * a.count else a.count end) as quantity';
          $sql .= '         ,(case when a2.id is not null then 2 else 1 end) as sum_type';
          $sql .= '         ,(case when a2.id is not null and a.claim_price > 0 then -1 else 1 end)';
          $sql .= '             * (case c.tax_type when 1 then 0 else a.claim_price end) as taxable_amount';
          $sql .= '         ,(case when a2.id is not null and a.claim_price > 0 then -1 else 1 end)';
          $sql .= '             * (case c.tax_type when 1 then a.claim_price else 0 end) as nontaxable_amount';
          $sql .= '      from ';
          $sql .= '          trn_claims as a';
          $sql .= '          left join trn_receivings as a1 on a.trn_receiving_id = a1.id';
          $sql .= '          left join trn_repay_headers as a2 on a1.trn_repay_header_id = a2.id';
          $sql .= '          left join trn_retroact_records as a3 on a.trn_retroact_record_id = a3.id';
          $sql .= '         ,mst_item_units as b';
          $sql .= '         ,mst_facility_items as c';
          $sql .= '         ,mst_departments as d';
          $sql .= '         ,mst_facilities as e';
          $sql .= '      where ';
          $sql .= '          a.mst_item_unit_id = b.id';
          $sql .= '          and b.mst_facility_item_id = c.id';
          $sql .= '          and a.department_id_to = d.id';
          $sql .= '          and c.mst_facility_id = e.id';
          $sql .= "          and a.is_stock_or_sale = '2'";
          $sql .= '          and a.is_deleted = false';
          $sql .= "          and a.claim_date between '{$startDate}' and '{$endDate}' ";
          $sql .= "          and d.mst_facility_id = {$row['MstFacility']['id']} ";
          $sql .= '          and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
          $sql .= '          and d.department_type = ' . Configure::read('DepartmentType.hospital');
          $sql .= '          and ( a3.retroact_type not in ( ' . Configure::read('Retroact.sales_retroact_minus') .' , ' . Configure::read('Retroact.sales_retroact_plus')  .') ';
          $sql .= '              or a3.retroact_type is null ) ';
          $sql .= '     ) as a';
          $sql .= ' group by ';
          $sql .= '     target_date';
          $sql .= '    ,mst_facility_item_id';
          $sql .= '    ,mst_item_unit_id';
          $sql .= '    ,mst_facility_id';
          $sql .= '    ,mst_facility_id_to';
          $sql .= '    ,mst_department_id_to';
          $sql .= '    ,sum_type';
          $sql .= ' order by';
          $sql .= '     target_date';
          $sql .= '    ,mst_facility_item_id';
          $sql .= '    ,mst_item_unit_id';
          $sql .= '    ,mst_facility_id';
          $sql .= '    ,mst_facility_id_to';
          $sql .= '    ,mst_department_id_to';
          $sql .= '    ,sum_type';
          $res = $this->TrnClaim->query($sql);
          if(false === $res){
             throw new Exception("売上締め＠締め明細作成エラー\n" . $sql);
          }

          //締めヘッダ再び更新(金額)
          $sql  = "";
          $sql .= ' update trn_close_headers set ';
          $sql .= '     taxable_amount = coalesce(TrnClose.taxable_amount,0) ';
          $sql .= '    ,nontaxable_amount = coalesce(TrnClose.nontaxable_amount,0) ';
          $sql .= ' from ';
          $sql .= '     (select sum(taxable_amount) as taxable_amount, sum(nontaxable_amount) as nontaxable_amount from trn_close_records ';
          $sql .= "      where trn_close_header_id = {$lastHeaderId} ) as TrnClose ";
          $sql .= ' where ';
          $sql .= '     id = ' . $lastHeaderId;
          $res = $this->TrnCloseHeader->query($sql);
          if(false === $res){
              throw new Exception("締めヘッダ更新失敗\n" . $sql);
          }
      }
  }


  /**
   * 在庫締めデータ作成
   * @param string $startDate
   * @param string $endDate
   * @param string $is_provisional
   */
  private function makeStocksData($startDate, $endDate, $is_provisional){
      $now = date('Y/m/d H:i:s');
      //受領更新
      $sql  = "";
      $sql .= ' update trn_receivings set ';
      $sql .= "     facility_close_type = '" . ($is_provisional ? '1' : '2') . "' ";
      $sql .= "    ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "' ";
      $sql .= "    ,modified = '" . $now . "'";
      $sql .= ' from ';
      $sql .= '     mst_item_units as MstItemUnit ';
      $sql .= ' where ';
      $sql .= '     trn_receivings.mst_item_unit_id=MstItemUnit.id ';
      $sql .= '     and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
      $sql .= '     and trn_receivings.is_deleted = false ';
      $sql .= "     and trn_receivings.work_date between '{$startDate}' and '{$endDate}' ";

      $res = $this->TrnClaim->query($sql);
      if(false === $res){
          throw new Exception('在庫締め＠受領＠更新失敗' . $sql);
      }

      $sql  = "";
      $sql .= ' update trn_consumes set ';
      $sql .= "     facility_close_type = '" . ($is_provisional ? '1' : '2') . "' ";
      $sql .= "    ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "' ";
      $sql .= "    ,modified = '" . $now . "'";
      $sql .= ' from ';
      $sql .= '     mst_item_units as MstItemUnit ';
      $sql .= ' where ';
      $sql .= '     trn_consumes.mst_item_unit_id=MstItemUnit.id ';
      $sql .= '     and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
      $sql .= '     and trn_consumes.is_deleted = false ';
      $sql .= "     and trn_consumes.work_date between '{$startDate}' and '{$endDate}' ";
      $res = $this->TrnClaim->query($sql);
      if(false === $res){
          throw new Exception('在庫締め＠払出＠消費更新失敗' . $sql);
      }

      //請求テーブル更新
      $sql  = "";
      $sql .= ' update trn_claims set ';
      $sql .= "     facility_close_type = '" . ($is_provisional ? '1' : '2') . "' ";
      $sql .= "    ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "' ";
      $sql .= "    ,modified = '" . $now . "'";
      $sql .= ' from ';
      $sql .= '     mst_item_units as MstItemUnit ';
      $sql .= ' where ';
      $sql .= '     trn_claims.mst_item_unit_id=MstItemUnit.id ';
      $sql .= '     and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
      $sql .= "     and trn_claims.is_stock_or_sale =  '2' ";
      $sql .= "     and trn_claims.claim_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '     and trn_claims.is_deleted = false ';

      $res = $this->TrnClaim->query($sql);
      if(false === $res){
          throw new Exception("売上締め＠請求テーブル更新失敗\n{$startDate}\n{$endDate}\n{$sql}");
      }

      //締めヘッダ作成
      $workNo = $this->setWorkNo4Header($endDate, '20');
      $this->TrnCloseHeader->create(false);
      $this->TrnCloseHeader->save(array(
          'id'                  => false,
          'work_no'             => $workNo,
          'work_date'           => $endDate,
          'work_type'           => $this->request->data['MstClass']['id'],
          'recital'             => $this->request->data['TrnCloseHeader']['recital'],
          'close_type'          => Configure::read('CloseType.stock'),
          'mst_facility_id'     => $this->Session->read('Auth.facility_id_selected'),
          'partner_facility_id' => $this->Session->read('Auth.facility_id_selected'),
          'start_date'          => $startDate,
          'end_date'            => $endDate,
          'taxable_amount'      => 0, //あとで更新
          'nontaxable_amount'   => 0, //あとで更新
          'is_deleted'          => false,
          'is_provisional'      => $is_provisional,
          'creater'             => $this->Session->read('Auth.MstUser.id'),
          'created'             => $now,
          'modifier'            => $this->Session->read('Auth.MstUser.id'),
          'modified'            => $now,
          ));
      $lastHeaderId = $this->TrnCloseHeader->getLastInsertId();

      //締め明細作成
      $sql  = "";
      $sql .= ' insert into trn_close_records(';
      $sql .= '     work_no';
      $sql .= '    ,close_type';
      $sql .= '    ,target_date';
      $sql .= '    ,mst_facility_id';
      $sql .= '    ,mst_facility_item_id';
      $sql .= '    ,mst_item_unit_id';
      $sql .= '    ,mst_facility_id_to';
      $sql .= '    ,mst_department_id_to';
      $sql .= '    ,quantity';
      $sql .= '    ,taxable_amount';
      $sql .= '    ,nontaxable_amount';
      $sql .= '    ,sum_type';
      $sql .= '    ,trn_close_header_id';
      $sql .= '    ,is_provisional';
      $sql .= '    ,creater';
      $sql .= '    ,created';
      $sql .= '    ,modifier';
      $sql .= '    ,modified' ;
      if(Configure::read('SplitTable.flag') == 1){
          $sql .= ',center_facility_id' ; //分割テーブル対応
      }
      $sql .= ' )';
      $sql .= ' select';
      $sql .= "     '{$workNo}' as work_no";
      $sql .= "     , (case move_type when " . Configure::read('MoveType.move2outside_import') . " then " . Configure::read('CloseType.supply') . " when " . Configure::read('MoveType.move2outside_export') . " then " . Configure::read('CloseType.stock') . " when " .Configure::read('MoveType.move2other') ." then  ". Configure::read('CloseType.stock') . " when 10 then  ". Configure::read('CloseType.supply') . " else " . Configure::read('CloseType.stock') . " end)  as close_type";
      $sql .= '     ,target_date';
      $sql .= '     ,mst_facility_id';
      $sql .= '     ,mst_facility_item_id';
      $sql .= '     ,mst_item_unit_id';
      $sql .= '     ,mst_facility_id_to';
      $sql .= '     ,mst_department_id_to';
      $sql .= '     ,sum(quantity) as quantity';
      $sql .= '     ,sum(taxable_amount) as taxable_amount';
      $sql .= '     ,sum(nontaxable_amount) as nontaxable_amount';
      $sql .= '     ,sum_type';
      $sql .= "     ,'{$lastHeaderId}' as trn_close_header_id";
      $sql .= '     ,' . ($is_provisional ? 'true' : 'false') . ' as is_procisional';
      $sql .= '     ,' . $this->Session->read('Auth.MstUser.id');
      $sql .= "     ,'".$now."'";
      $sql .= '     ,' . $this->Session->read('Auth.MstUser.id');
      $sql .= "     ,'".$now."'" ;
      if(Configure::read('SplitTable.flag') == 1){
          $sql .= ',' . $this->Session->read('Auth.facility_id_selected') ; //分割テーブル対応
      }
      $sql .= ' from ';
      $sql .= '     (select';
      $sql .= '          a.work_date as target_date';
      $sql .= '         ,a.move_type';
      $sql .= '         ,d.mst_facility_item_id';
      $sql .= '         ,x.mst_item_unit_id';
      $sql .= '         ,e.mst_facility_id';
      $sql .= '         ,f.mst_facility_id as mst_facility_id_to';
      $sql .= '         ,a.department_id_to as mst_department_id_to';
      $sql .= '         ,x.quantity';
      $sql .= '         ,(case when a.move_type = ' . Configure::read('MoveType.move2outside_import') . ' then 2 else 1 end ) as sum_type';
      $sql .= '         ,coalesce(xx.stocking_price,0) as taxable_amount';
      $sql .= '         ,0 as nontaxable_amount';
      $sql .= '      from trn_move_headers as a';
      $sql .= '          inner join trn_shippings as x on a.id=x.trn_move_header_id';
      $sql .= '          left  join trn_receivings as xx on xx.trn_shipping_id=x.id';
      $sql .= '          inner join trn_stickers as b on b.id=x.trn_sticker_id';
      $sql .= '          inner join trn_receivings as c on b.trn_receiving_id=c.id'; //移動元の商品情報を取得する為の苦肉の策
      $sql .= '          inner join mst_item_units as d on c.mst_item_unit_id=d.id';
      $sql .= '          inner join mst_facility_items as e on d.mst_facility_item_id=e.id';
      $sql .= '          inner join mst_departments as f on a.department_id_to=f.id';
      $sql .= "      where a.work_date between '{$startDate}' and '{$endDate}'";
      $sql .= "          and ( a.move_type in (" . Configure::read('MoveType.move2outside_export') . ", " . Configure::read('MoveType.move2outside_import') . " )" ;
      if(Configure::read('SplitTable.flag') == 1){ //センター間で、移動が完了しているもの
            $sql .= '    or ( a.move_type = ' . Configure::read('MoveType.move2other') . ' and x.move_status = ' . Configure::read('MoveStatus.complete') . ' ) ) ';
      }else{
            $sql .= '    or ( a.move_type = ' . Configure::read('MoveType.move2other') . ' and xx.id is not null ) ) ';
      }
      $sql .= '          and a.is_deleted = false';
      $sql .= "          and e.mst_facility_id = " . $this->Session->read('Auth.facility_id_selected');
      $sql .= '      union all ';
      $sql .= '      select ';
      $sql .= '          a.work_date as target_date ';
      $sql .= '        , 0 as move_type ';
      $sql .= '        , c.mst_facility_item_id ';
      $sql .= '        , b.mst_item_unit_id ';
      $sql .= '        , d.mst_facility_id ';
      $sql .= '        , e.mst_facility_id          as mst_facility_id_to ';
      $sql .= '        , b.mst_department_id        as mst_department_id_to ';
      $sql .= '        , b.quantity                 as quantity ';
      $sql .= '        , ( case when b.adjust_type= ' . Configure::read('ExpirationType.captiveConsumptionReduction') . ' or b.adjust_type = ' . Configure::read('ExpirationType.miscellaneousLossReduction'). ' then 3 when b.adjust_type=' . Configure::read('ExpirationType.captiveConsumptionIncrease') . ' or b.adjust_type= ' . Configure::read('ExpirationType.miscellaneousLossIncrease') . ' then 4 end ) as sum_type ';
      $sql .= '        , 0                          as taxable_amount ';
      $sql .= '        , 0                          as nontaxable_amount  ';
      $sql .= '      from ';
      $sql .= '          trn_adjust_headers as a  ';
      $sql .= '          left join trn_consumes as b  ';
      $sql .= '              on b.trn_adjust_header_id = a.id  ';
      $sql .= '          left join mst_item_units as c  ';
      $sql .= '              on c.id = b.mst_item_unit_id  ';
      $sql .= '          left join mst_facility_items as d  ';
      $sql .= '              on d.id = c.mst_facility_item_id  ';
      $sql .= '          left join mst_departments as e  ';
      $sql .= '              on e.id = b.mst_department_id  ';
      $sql .= '      where ';
      $sql .= "          a.work_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '          and a.is_deleted = false ';
      $sql .= '          and b.is_deleted = false ';
      $sql .= '          and d.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
      $sql .= '          and b.adjust_type in (' . Configure::read('ExpirationType.captiveConsumptionReduction') . ', ' . Configure::read('ExpirationType.miscellaneousLossReduction') .')';
      $sql .= '      union all ';
      $sql .= '      select ';
      $sql .= '          a.work_date                as target_date';
      $sql .= '        , 10 as move_type ';  //センター間受け側
      $sql .= '        , b.mst_facility_item_id ';
      $sql .= '        , a.mst_item_unit_id ';
      $sql .= '        , c.mst_facility_id          as mst_facility_id ';
      $sql .= '        , d.mst_facility_id          as mst_facility_id_to ';
      $sql .= '        , a.department_id_from       as mst_department_id_to ';
      $sql .= '        , a.quantity ';
      $sql .= '        , 2                          as sum_type ';
      $sql .= '        , ( case c.tax_type when 1 then 0 else a.quantity * a.stocking_price end ) as taxable_amount';
      $sql .= '        , ( case c.tax_type when 1 then a.quantity * a.stocking_price else 0 end ) as nontaxable_amount ';
      $sql .= '      from';
      $sql .= '          trn_receivings as a ';
      $sql .= '          left join mst_item_units as b ';
      $sql .= '              on b.id = a.mst_item_unit_id ';
      $sql .= '          left join mst_facility_items as c ';
      $sql .= '              on c.id = b.mst_facility_item_id ';
      $sql .= '          left join mst_departments as d ';
      $sql .= '              on d.id = a.department_id_to ';
      $sql .= '          left join trn_shippings as e ';
      $sql .= '              on e.id = a.trn_shipping_id ';
      $sql .= '          left join trn_move_headers as f ';
      $sql .= '              on f.id = e.trn_move_header_id ';
      $sql .= '      where ';
      $sql .= '          a.is_deleted = false ';
      if(Configure::read('SplitTable.flag') == 1){ //移動ヘッダIDがあって、つながらないもの
        $sql .=   '      and f.id is null  and a.trn_move_header_id is not null  ';
        $sql .=   '      and a.trn_repay_header_id is null ' ;
        $sql .=   '      and a.receiving_type = ' . Configure::read('ReceivingType.arrival');
      }else{
        $sql .=   '      and f.move_type = ' . Configure::read('MoveType.move2other');
      }
      $sql .= "          and a.work_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '          and d.mst_facility_id = '. $this->Session->read('Auth.facility_id_selected');

      $sql .= '     ) as a';
      $sql .= ' group by';
      $sql .= '     target_date';
      $sql .= '    ,mst_facility_item_id';
      $sql .= '    ,mst_item_unit_id';
      $sql .= '    ,mst_facility_id';
      $sql .= '    ,mst_facility_id_to';
      $sql .= '    ,mst_department_id_to';
      $sql .= '    ,sum_type';
      $sql .= '    ,move_type';
      $sql .= ' order by';
      $sql .= '     target_date';
      $sql .= '    ,mst_facility_item_id';
      $sql .= '    ,mst_item_unit_id';
      $sql .= '    ,mst_facility_id';
      $sql .= '    ,mst_facility_id_to';
      $sql .= '    ,mst_department_id_to';
      $sql .= '    ,sum_type';

      $res = $this->TrnClaim->query($sql);
      if(false === $res){
          throw new Exception('在庫締め＠払出＠締め明細作成失敗' . $sql);
      }

      //受入
      $sql  = "";
      $sql .= ' insert into trn_close_records(';
      $sql .= '    work_no';
      $sql .= '   ,close_type';
      $sql .= '   ,target_date';
      $sql .= '   ,mst_facility_id';
      $sql .= '   ,mst_facility_item_id';
      $sql .= '   ,mst_item_unit_id';
      $sql .= '   ,mst_facility_id_to';
      $sql .= '   ,mst_department_id_to';
      $sql .= '   ,quantity';
      $sql .= '   ,taxable_amount';
      $sql .= '   ,nontaxable_amount';
      $sql .= '   ,sum_type';
      $sql .= '   ,trn_close_header_id';
      $sql .= '   ,is_provisional';
      $sql .= '   ,creater';
      $sql .= '   ,created';
      $sql .= '   ,modifier';
      $sql .= '   ,modified' ;
      if(Configure::read('SplitTable.flag') == 1){
          $sql .= ',center_facility_id'; //分割テーブル対応
      }
      $sql .= ' )';
      $sql .= ' select';
      $sql .= "     '{$workNo}' as work_no";
      $sql .= "    ," . Configure::read('CloseType.stock') . " as close_type";
      $sql .= '    ,target_date';
      $sql .= '    ,mst_facility_id';
      $sql .= '    ,mst_facility_item_id';
      $sql .= '    ,mst_item_unit_id';
      $sql .= '    ,mst_facility_id_to';
      $sql .= '    ,mst_department_id_to';
      $sql .= '    ,sum(quantity) as quantity';
      $sql .= '    ,sum(taxable_amount) as taxable_amount';
      $sql .= '    ,sum(nontaxable_amount) as nontaxable_amount';
      $sql .= '    ,sum_type';
      $sql .= "    ,{$lastHeaderId} as trn_close_header_id";
      $sql .= '    ,' . ($is_provisional ? 'true' : 'false') . ' as is_procisional';
      $sql .= '    ,' . $this->Session->read('Auth.MstUser.id');
      $sql .= "    ,'" . $now . "'";
      $sql .= '    ,' . $this->Session->read('Auth.MstUser.id');
      $sql .= "    ,'" . $now . "'";
      if(Configure::read('SplitTable.flag') == 1){
          $sql .=  ',' . $this->Session->read('Auth.facility_id_selected') ; //分割テーブル対応
      }
      $sql .= ' from ';
      $sql .= '     (select';
      $sql .= '          a.work_date as target_date';
      $sql .= '         ,b.mst_facility_item_id';
      $sql .= '         ,a.mst_item_unit_id';
      $sql .= '         ,c.mst_facility_id as mst_facility_id';
      $sql .= '         ,d.mst_facility_id as mst_facility_id_to';
      $sql .= '         ,a.department_id_from as mst_department_id_to';
      $sql .= '         ,a.quantity';
      $sql .= '         ,2 as sum_type';
      $sql .= '         ,(case c.tax_type when 1 then 0 else a.quantity * a.stocking_price end) as taxable_amount';
      $sql .= '         ,(case c.tax_type when 1 then a.quantity * a.stocking_price else 0 end) as nontaxable_amount';
      $sql .= '      from ';
      $sql .= '          trn_receivings as a';
      $sql .= '         ,mst_item_units as b';
      $sql .= '         ,mst_facility_items as c';
      $sql .= '         ,mst_departments as d';
      $sql .= '         ,trn_move_headers as e';
      $sql .= '      where ';
      $sql .= '          a.mst_item_unit_id = b.id';
      $sql .= '          and b.mst_facility_item_id = c.id';
      $sql .= '          and a.department_id_from = d.id';
      $sql .= '          and a.trn_move_header_id = e.id';
      $sql .= '          and a.is_deleted = false';
      $sql .= "          and a.work_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '          and d.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
      $sql .= '      union all';
      $sql .= '      select';
      $sql .= '          cast(a.work_date as date) as target_date';
      $sql .= '         ,b.mst_facility_item_id';
      $sql .= '         ,a.mst_item_unit_id';
      $sql .= '         ,c.mst_facility_id as mst_facility_id';
      $sql .= '         ,d.mst_facility_id as mst_facility_id_to';
      $sql .= '         ,a.mst_department_id as mst_department_id_to';
      $sql .= '         ,a.quantity';
      $sql .= '         ,4 as sum_type';
      $sql .= '         ,(case c.tax_type when 1 then 0 else a.quantity * f.transaction_price end) as taxable_amount';
      $sql .= '         ,(case c.tax_type when 1 then a.quantity * f.transaction_price else 0 end) as nontaxable_amount';
      $sql .= '      from';
      $sql .= '          trn_consumes as a';
      $sql .= '         ,mst_item_units as b';
      $sql .= '         ,mst_facility_items as c';
      $sql .= '         ,mst_departments as d';
      $sql .= '         ,trn_adjust_headers as e';
      $sql .= '         ,trn_stickers as f';
      $sql .= '      where ';
      $sql .= '          a.mst_item_unit_id = b.id';
      $sql .= '          and b.mst_facility_item_id = c.id';
      $sql .= '          and a.mst_department_id = d.id';
      $sql .= '          and a.trn_adjust_header_id = e.id';
      $sql .= '          and a.trn_sticker_id = f.id';
      $sql .= '          and a.adjust_type in (' . Configure::read('ExpirationType.captiveConsumptionIncrease') . ',' . Configure::read('ExpirationType.miscellaneousLossIncrease') . ')';
      $sql .= '          and a.is_deleted = false';
      $sql .= "          and a.work_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '          and d.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
      $sql .= '     ) as a';
      $sql .= ' group by';
      $sql .= '     target_date';
      $sql .= '    ,mst_facility_item_id';
      $sql .= '    ,mst_item_unit_id';
      $sql .= '    ,mst_facility_id';
      $sql .= '    ,mst_facility_id_to';
      $sql .= '    ,mst_department_id_to';
      $sql .= '    ,sum_type';
      $sql .= ' order by';
      $sql .= '     target_date';
      $sql .= '    ,mst_facility_item_id';
      $sql .= '    ,mst_item_unit_id';
      $sql .= '    ,mst_facility_id';
      $sql .= '    ,mst_facility_id_to';
      $sql .= '    ,mst_department_id_to';
      $sql .= '    ,sum_type';

      $res = $this->TrnClaim->query($sql);
      if(false === $res){
          throw new Exception('在庫締め＠受入＠作成失敗' . $sql);
      }

      //在庫評価額作成
      $sql  = "";
      $sql .= ' insert into trn_appraised_values(';
      $sql .= '     work_no';
      $sql .= '    ,work_date';
      $sql .= '    ,mst_facility_id';
      $sql .= '    ,mst_facility_item_id';
      $sql .= '    ,total_count';
      $sql .= '    ,total_price';
      $sql .= '    ,remain_count';
      $sql .= '    ,hospital_remain_count';
      $sql .= '    ,price';
      $sql .= '    ,remain_price';
      $sql .= '    ,trn_close_header_id';
      $sql .= '    ,creater';
      $sql .= '    ,created';
      $sql .= '    ,modifier';
      $sql .= '    ,modified';
      if(Configure::read('SplitTable.flag') == 1){
          $sql .=' ,center_facility_id' ; //分割テーブル対応
      }

      $sql .= ' )';
      $sql .= ' select';
      $sql .= "     '{$workNo}' as work_no";
      $sql .= "    ,'{$endDate}' as work_date";
      $sql .= '    ,a.mst_facility_id';
      $sql .= '    ,a.mst_facility_item_id';
      $sql .= '    ,a.total_count';
      $sql .= '    ,a.total_price';
      $sql .= '    ,a.remain_count';
      $sql .= '    ,a.hospital_remain_count'; //病院在庫
      $sql .= '    ,round(case a.total_abs_count when 0 then a.total_abs_price else a.total_abs_price / a.total_abs_count end,2) as price';
      $sql .= '    ,round(case a.total_abs_count when 0 then a.total_abs_price else a.total_abs_price / a.total_abs_count end,2) * a.remain_count as remain_price';
      $sql .= "    ,{$lastHeaderId} as trn_close_header_id";
      $sql .= '    ,' . $this->Session->read('Auth.MstUser.id');
      $sql .= "    ,'" . $now . "'";
      $sql .= '    ,' . $this->Session->read('Auth.MstUser.id');
      $sql .= "    ,'" . $now . "'" ;
      if(Configure::read('SplitTable.flag') == 1){
          $sql .= ',' . $this->Session->read('Auth.facility_id_selected'); //分割テーブル対応
      }
      $sql .= ' from ';
      $sql .= '     (select';
      $sql .= '          a.mst_facility_id';
      $sql .= '         ,a.id as mst_facility_item_id';
      $sql .= '         ,(coalesce(b.remain_count,0) + coalesce(c.total_count,0)) as total_count';
      $sql .= '         ,(coalesce(b.remain_count,0) + coalesce(c.total_abs_count,0)) as total_abs_count';
      $sql .= '         ,(case when (coalesce(b.remain_price,0) + coalesce(c.total_price,0)) = 0 then (coalesce(b.price,0)) else (coalesce(b.remain_price,0) + coalesce(c.total_price,0)) end ) as total_price';
      $sql .= '         ,(case when (coalesce(b.remain_price,0) + coalesce(c.total_abs_price,0)) = 0 then (coalesce(b.price,0)) else (coalesce(b.remain_price,0) + coalesce(c.total_abs_price,0)) end ) as total_abs_price';
      $sql .= '         ,(coalesce(b.remain_count,0) + coalesce(c.total_count,0) - coalesce(d.total_count,0)) as remain_count';
      $sql .= '         ,(coalesce(b.hospital_remain_count , 0) + coalesce(e.hospital_remain_count , 0 )) as hospital_remain_count';
      $sql .= '         ,b.mst_facility_id as mst_facility_id_old';
      $sql .= '         ,c.mst_facility_id as mst_facility_id_in';
      $sql .= '         ,d.mst_facility_id as mst_facility_id_out';
      $sql .= '      from ';
      $sql .= '          mst_facility_items as a';
      $sql .= '          left join ('; //前月残
      $sql .= '              select';
      $sql .= '                  b.mst_facility_id';
      $sql .= '                 ,b.mst_facility_item_id';
      $sql .= '                 ,b.remain_count';
      $sql .= '                 ,b.hospital_remain_count';
      $sql .= '                 ,b.remain_price';
      $sql .= '                 ,b.price';
      $sql .= '              from trn_appraised_values as b';
      $sql .= '                  inner join trn_close_headers as b1 on';
      $sql .= '                      b.trn_close_header_id=b1.id ';
      $sql .= '                      and b1.is_provisional=false ';
      $sql .= '                      and b1.is_deleted=false';
      $sql .= '              where b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
      $sql .= "                  and b.work_date = '" . date('Y/m/d', strtotime('-1 day', strtotime($startDate))) . "' ";
      $sql .= '                  and b.is_deleted = false';
      $sql .= '          ) as b on';
      $sql .= '              a.mst_facility_id = b.mst_facility_id';
      $sql .= '              and a.id = b.mst_facility_item_id';
      $sql .= '          left join ('; //当月受入
      $sql .= '              select';
      $sql .= '                  a.mst_facility_id';
      $sql .= '                 ,b.mst_facility_item_id';
      $sql .= '                 ,sum(a.quantity * b.per_unit) as total_count';
      $sql .= '                 ,sum(abs(a.quantity) * b.per_unit) as total_abs_count';
      $sql .= '                 ,sum(a.taxable_amount + a.nontaxable_amount) as total_price';
      $sql .= '                 ,sum(abs(a.taxable_amount) + abs(a.nontaxable_amount)) as total_abs_price';
      $sql .= '              from';
      $sql .= '                  trn_close_records as a';
      $sql .= '                 ,mst_item_units as b';
      $sql .= '              where ';
      $sql .= '                  a.mst_item_unit_id = b.id';
      $sql .= '                  and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
      $sql .= "                  and a.target_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '                  and a.close_type in (' . Configure::read('CloseType.supply') . ')';
      $sql .= '                  and a.is_deleted = false ';
      $sql .= '                  and a.is_provisional = ' . ($is_provisional ? 'true' : 'false');
      $sql .= '              group by';
      $sql .= '                  a.mst_facility_id';
      $sql .= '                 ,b.mst_facility_item_id';
      $sql .= '          ) as c on';
      $sql .= '              a.mst_facility_id = c.mst_facility_id';
      $sql .= '              and a.id = c.mst_facility_item_id';
      $sql .= '          left join (';
      $sql .= '              select';
      $sql .= '                  a.mst_facility_id';
      $sql .= '                 ,b.mst_facility_item_id';
      $sql .= '                 ,sum((case when a.sum_type = 4 then -1 * a.quantity else a.quantity end) * b.per_unit) as total_count';
      $sql .= '              from ';
      $sql .= '                  trn_close_records as a';
      $sql .= '                 ,mst_item_units as b';
      $sql .= '              where ';
      $sql .= '                  a.mst_item_unit_id = b.id';
      $sql .= '                  and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
      $sql .= "                  and a.target_date between '{$startDate}' and '{$endDate}' ";
      $sql .= '                  and a.close_type in (' . Configure::read('CloseType.sales') . ',' . Configure::read('CloseType.stock') . ')';
      $sql .= '                  and a.sum_type in (0,1,3,4)';
      $sql .= '                  and a.is_deleted = false ';
      $sql .= '                  and a.is_provisional = ' . ($is_provisional ? 'true' : 'false');
      $sql .= '              group by';
      $sql .= '                  a.mst_facility_id';
      $sql .= '                 ,b.mst_facility_item_id';
      $sql .= '          ) as d on ';
      $sql .= '              a.mst_facility_id = d.mst_facility_id';
      $sql .= '              and a.id = d.mst_facility_item_id';
      $sql .= '          left join ( ';
      $sql .= "              select ";
      $sql .= "                    xxx.id as mst_facility_item_id ";
      $sql .= "                   ,( coalesce(x1.quantity, 0) "; /* 受領 */
      $sql .= "                    - coalesce(x2.quantity, 0) "; /* 消費 */
      $sql .= "                    - coalesce(x3.quantity, 0) "; /* 移動 */
      $sql .= "                    + coalesce(x4.quantity, 0) "; /* 調整 */
      $sql .= "                   ) as hospital_remain_count ";
      $sql .= "              from ";
      $sql .= "                  mst_facility_items as xxx ";
      $sql .= "                  left join ( ";
      $sql .= "                      select ";
      $sql .= "                          b.mst_facility_item_id ";
      $sql .= "                        , sum(b.per_unit * a.quantity) as quantity ";
      $sql .= "                      from ";
      $sql .= "                          trn_receivings as a ";
      $sql .= "                          left join mst_item_units as b ";
      $sql .= "                              on b.id = a.mst_item_unit_id ";
      $sql .= "                          left join mst_departments as c ";
      $sql .= "                              on c.id = a.department_id_to ";
      $sql .= "                          left join trn_shippings as d ";
      $sql .= "                              on d.id = a.trn_shipping_id ";
      $sql .= "                      where ";
      $sql .= "                          a.is_deleted = false ";
      $sql .= "                          and a.receiving_type = " . Configure::read('ReceivingType.receipt');
      $sql .= "                          and d.shipping_type = " . Configure::read('ShippingType.fixed');
      $sql .= "                          and a.work_date between  '{$startDate}' and '{$endDate}' ";
      $sql .= "                          and c.department_type = " . Configure::read('DepartmentType.warehouse');
      $sql .= "                          and b.mst_facility_id = " . $this->Session->read('Auth.facility_id_selected');
      $sql .= "                      group by ";
      $sql .= "                          b.mst_facility_item_id ";
      $sql .= "                  ) as x1 ";
      $sql .= "                      on xxx.id = x1.mst_facility_item_id ";
      $sql .= "                  left join ( ";
      $sql .= "                      select ";
      $sql .= "                          b.mst_facility_item_id ";
      $sql .= "                        , sum(b.per_unit * a.quantity) as quantity ";
      $sql .= "                      from ";
      $sql .= "                          trn_consumes as a ";
      $sql .= "                          left join mst_item_units as b ";
      $sql .= "                              on b.id = a.mst_item_unit_id ";
      $sql .= "                          left join mst_departments as c ";
      $sql .= "                              on c.id = a.mst_department_id ";
      $sql .= "                          left join trn_stickers as d ";
      $sql .= "                              on d.id = a.trn_sticker_id ";
      $sql .= "                      where ";
      $sql .= "                          a.is_deleted = false ";
      $sql .= "                          and d.trade_type = 1 ";
      $sql .= "                          and a.use_type in (" . Configure::read('UseType.use'). ", " . Configure::read('UseType.moveuse'). ") ";
      $sql .= "                          and a.work_date between  '{$startDate}' and '{$endDate}' ";
      $sql .= "                          and b.mst_facility_id = " . $this->Session->read('Auth.facility_id_selected');
      $sql .= "                      group by ";
      $sql .= "                          b.mst_facility_item_id ";
      $sql .= "                  ) as x2 ";
      $sql .= "                      on xxx.id = x2.mst_facility_item_id ";
      $sql .= "                  left join ( ";
      $sql .= "                      select ";
      $sql .= "                          c.mst_facility_item_id ";
      $sql .= "                        , sum(b.quantity * c.per_unit) as quantity ";
      $sql .= "                      from ";
      $sql .= "                          trn_move_headers as a ";
      $sql .= "                          left join trn_receivings as b ";
      $sql .= "                              on b.trn_move_header_id = a.id ";
      $sql .= "                          left join mst_item_units as c ";
      $sql .= "                              on c.id = b.mst_item_unit_id ";
      $sql .= "                          left join mst_departments as d ";
      $sql .= "                              on d.id = a.department_id_from ";
      $sql .= "                          left join trn_stickers as e ";
      $sql .= "                              on e.id = b.trn_sticker_id ";
      $sql .= "                      where ";
      $sql .= "                          a.is_deleted = false ";
      $sql .= "                          and b.is_deleted = false ";
      $sql .= "                          and d.department_type = " . Configure::read('DepartmentType.hospital');
      $sql .= "                          and e.trade_type = 1 ";
      $sql .= "                          and a.move_type = " . Configure::read('MoveType.move2outer');
      $sql .= "                          and a.move_status in ('10', '100') ";
      $sql .= "                          and a.work_date between  '{$startDate}' and '{$endDate}'";
      $sql .= "                          and c.mst_facility_id = " . $this->Session->read('Auth.facility_id_selected');
      $sql .= "                      group by ";
      $sql .= "                          c.mst_facility_item_id ";
      $sql .= "                  ) as x3 ";
      $sql .= "                      on xxx.id = x3.mst_facility_item_id ";
      $sql .= "                  left join ( ";
      $sql .= "                      select ";
      $sql .= "                          c.mst_facility_item_id ";
      $sql .= "                        , sum( ";
      $sql .= "                              case ";
      $sql .= "                              when b.adjust_type = " . Configure::read('ExpirationType.captiveConsumptionReduction');
      $sql .= "                                  then b.quantity * c.per_unit * - 1 ";
      $sql .= "                              when b.adjust_type = " . Configure::read('ExpirationType.miscellaneousLossReduction');
      $sql .= "                                  then b.quantity * c.per_unit * - 1 ";
      $sql .= "                              when b.adjust_type = " . Configure::read('ExpirationType.captiveConsumptionIncrease');
      $sql .= "                                  then b.quantity * c.per_unit ";
      $sql .= "                              when b.adjust_type = " . Configure::read('ExpirationType.miscellaneousLossIncrease');
      $sql .= "                                  then b.quantity * c.per_unit ";
      $sql .= "                              end ";
      $sql .= "                          ) as quantity ";
      $sql .= "                      from ";
      $sql .= "                          trn_adjust_headers as a ";
      $sql .= "                          left join trn_consumes as b ";
      $sql .= "                              on b.trn_adjust_header_id = a.id ";
      $sql .= "                          left join mst_item_units as c ";
      $sql .= "                              on c.id = b.mst_item_unit_id ";
      $sql .= "                          left join mst_departments as d ";
      $sql .= "                              on d.id = b.mst_department_id ";
      $sql .= "                          left join trn_stickers as e ";
      $sql .= "                              on e.id = b.trn_sticker_id ";
      $sql .= "                      where ";
      $sql .= "                          a.is_deleted = false ";
      $sql .= "                          and b.is_deleted = false ";
      $sql .= "                          and e.trade_type = 1 ";
      $sql .= "                          and d.department_type = " . Configure::read('DepartmentType.hospital');
      $sql .= "                          and b.work_date between  '{$startDate}' and '{$endDate}' ";
      $sql .= "                          and c.mst_facility_id = " . $this->Session->read('Auth.facility_id_selected');
      $sql .= "                      group by ";
      $sql .= "                          c.mst_facility_item_id ";
      $sql .= "                  ) as x4 ";
      $sql .= "                      on x4.mst_facility_item_id = xxx.id ";
      $sql .= '          ) as e ';
      $sql .= '              on a.id = e.mst_facility_item_id';
      $sql .= '      where ';
      $sql .= '          a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
      $sql .= ' ) as a';
      $sql .= ' where ';
      $sql .= '     a.remain_count > 0';
      $sql .= '     or a.mst_facility_id_old is not null';
      $sql .= '     or a.mst_facility_id_in is not null';
      $sql .= '     or a.mst_facility_id_out is not null';
      $sql .= ' order by';
      $sql .= '     a.mst_facility_id';
      $sql .= '    ,a.mst_facility_item_id';
        $res = $this->TrnClaim->query($sql);

        if(false === $res){
            throw new Exception('在庫評価額作成失敗' . $sql);
        }

      //締めヘッダ再び更新(金額)
      $sql  = "";
      $sql .= ' update trn_close_headers set ';
      $sql .= '     taxable_amount = coalesce(TrnClose.taxable_amount,0) ';
      $sql .= '    ,nontaxable_amount = coalesce(TrnClose.nontaxable_amount,0) ';
      $sql .= ' from ';
      $sql .= '     (select sum(taxable_amount) as taxable_amount, sum(nontaxable_amount) as nontaxable_amount from trn_close_records ';
      $sql .= "     where trn_close_header_id = {$lastHeaderId} ) as TrnClose ";
      $sql .= ' where ';
      $sql .= '     id = ' . $lastHeaderId;
      $res = $this->TrnCloseHeader->query($sql);
      if(false === $res){
          throw new Exception("締めヘッダ更新失敗\n" . $sql);
      }
  }

  /**
   *　すでに締められているかどうか
   * @param string $closeType
   * @param string $endDate
   * @return boolean
   */
  private function isAlreadyClosed($isProvisional, $endDate){
      $res = $this->TrnCloseHeader->find('count', array(
            'conditions' => array(
                'end_date'        => $endDate,
                'is_deleted'      => false,
                'is_provisional'  => $isProvisional,
                'mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
            ),
           'recursive' => -1,
      ));
      return ($res > 0);
  }


  /**
   * 締め年月を取得
   * @access private
   * @return string 締め年月
   */
  private function getCloseMonth(){
      $sql  = ' select ';
      $sql .= '       TrnCloseHeader.end_date       as "TrnCloseHeader__end_date" ';
      $sql .= '     , TrnCloseHeader.is_provisional as "TrnCloseHeader__is_provisional"  ';
      $sql .= '   from ';
      $sql .= '     trn_close_headers as TrnCloseHeader  ';
      $sql .= '   where ';
      $sql .= '     TrnCloseHeader.is_deleted = false  ';
      $sql .= '     and TrnCloseHeader.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
      $sql .= '     and end_date in (  ';
      $sql .= '       select ';
      $sql .= '             max(end_date)  ';
      $sql .= '         from ';
      $sql .= '           trn_close_headers  ';
      $sql .= '         where ';
      $sql .= '           is_deleted = false  ';
      $sql .= '           and mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
      $sql .= '     )  ';
      $sql .= '   group by ';
      $sql .= '     TrnCloseHeader.is_provisional ';
      $sql .= '     , TrnCloseHeader.end_date ';

      $res = $this->TrnCloseHeader->query($sql);


      if(0 === count($res)){
          //締めデータに該当がないので当月指定
          return date('Y/m');
      }else{
          if(1 === count($res)){
              return date('Y/m', strtotime($res[0]['TrnCloseHeader']['end_date']));
          }else{
              return date('Y/m', strtotime('1 day', strtotime($res[0]['TrnCloseHeader']['end_date'])));
          }

      }
  }

  /**
   * 得意先一覧を取得
   * @access private
   * @return array
   */
  private function getClosedInfomation($close_type){
      //仮締めがあればそれを出力
      //なければ前回の本締めを出力
      $res = $this->TrnCloseHeader->find('all', array(
          'recursive' => -1,
          'fields' => array(
              'end_date',
              'is_provisional',
          ),
          'conditions' => array(
              'is_deleted' => false,
              'mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
              'end_date in (select max(end_date) from trn_close_headers ' .
              'where is_deleted=false and mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') . ')',
          ),
          'group' => array(
              'end_date',
              'is_provisional',
          ),
      ));

      if(0 === count($res)){
          return array();
      }elseif(1 === count($res)){

          //仮締めのみ
          return $this->TrnCloseHeader->find('all', array(
              'fields' => array(
                  'TrnCloseHeader.end_date',
                  'TrnCloseHeader.close_type',
                  'TrnCloseHeader.is_provisional' ,
                  'MstFacility.facility_name',
              ),
              'joins' => array(
                  array(
                      'type'       => 'inner',
                      'table'      => 'mst_facilities',
                      'alias'      => 'MstFacility',
                      'conditions' => 'TrnCloseHeader.partner_facility_id = MstFacility.id',
                  ),
              ),
              'conditions' => array(
                  'TrnCloseHeader.is_deleted'      => false,
                  'TrnCloseHeader.is_provisional'  => true,
                  'TrnCloseHeader.close_type'      => $close_type,
                  'TrnCloseHeader.end_date'        => $res[0]['TrnCloseHeader']['end_date'],
                  'TrnCloseHeader.mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
              ),
              'group' => array(
                  'TrnCloseHeader.end_date',
                  'TrnCloseHeader.close_type',
                  'TrnCloseHeader.is_provisional' ,
                  'MstFacility.facility_name',
              ),
              'recursive' => -1,
          ));
      }else{

        //本締め
        return $this->TrnCloseHeader->find('all', array(
              'fields' => array(
                  'TrnCloseHeader.end_date',
                  'TrnCloseHeader.close_type',
                  'TrnCloseHeader.is_provisional' ,
                  'MstFacility.facility_name',
              ),
              'joins' => array(
                  array(
                      'type'       => 'inner',
                      'table'      => 'mst_facilities',
                      'alias'      => 'MstFacility',
                      'conditions' => 'TrnCloseHeader.partner_facility_id = MstFacility.id',
                  ),
              ),
              'conditions' => array(
                  'TrnCloseHeader.is_deleted'      => false,
                  'TrnCloseHeader.is_provisional'  => false,
                  'TrnCloseHeader.close_type'      => $close_type,
                  'TrnCloseHeader.end_date'        => $res[0]['TrnCloseHeader']['end_date'],
                  'TrnCloseHeader.mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
              ),
              'group' => array(
                  'TrnCloseHeader.end_date',
                  'TrnCloseHeader.close_type',
                  'TrnCloseHeader.is_provisional' ,
                  'MstFacility.facility_name',
              ),
              'recursive' => -1,
          ));
      }
  }
}
