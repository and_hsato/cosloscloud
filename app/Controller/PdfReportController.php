<?php
/**
 * PdfReportController
 *　PDF帳票
 * @version 1.0.0
 * @since 2015/08/24
 */
class PdfReportController extends AppController {

    /**
     * @var $name
     */
    var $name = 'PdfReport';

    /**
     * @var array $uses
     */
    var $uses = array(
            'TrnOrderHeader',
            'TrnReceivingHeader',
            'TrnEdiShipping',
            'TrnSticker',
            'TrnClaim',
            'TrnClaimSpecifications',
            'MstFacility',
            'TrnEdiShippingHeader',
    );

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler', 'DateUtil', 'PdfReport');


    public function  beforeFilter() {
        parent::beforeFilter();
        $this->autoLayout = false;
        $this->autoRender = false;
    }

    public function order() {
        $trnOrderHeaderIds = $this->getTrnOrderHeaderIds($this->request->data);

        $data = $this->PdfReport->getPdfDataOfOrder($trnOrderHeaderIds);
        $this->set('data', $data);
        $this->render('_pdf_data');
    }

    public function shipping() {
        $trnShippingHeaderIds = $this->getTrnShippingHeaderIds($this->request->data);

        $data = $this->PdfReport->getPdfDataOfShipping($trnShippingHeaderIds);
        $this->set('data', $data);
        $this->render('_pdf_data');
    }

    public function sticker() {
        $trnStickerIds = $this->getTrnStickerIds($this->request->data);

        $data = $this->PdfReport->getPdfDataOfSticker($trnStickerIds);
        $this->set('data', $data);
        $this->render('_pdf_data');
    }

    public function claim() {
        $facilityId = $this->Session->read('Auth.facility_id_selected');
        $facilities = $this->Session->read('Auth.facilities');
        $facilityName = $facilities[$facilityId];

        $loginUserName = $this->Session->read('Auth.MstUser.user_name');

        //請求明細id
        $trnClaimSpecificationId = $this->request->data['selected_id'];
        $data = $this->PdfReport->getPdfDataOfClaim($facilityName, $loginUserName, $trnClaimSpecificationId);

        $this->set('data', $data);
        $this->render('_pdf_data');
    }


    /*
     * リクエストデータの情報から処理対象となるTrnOrderのIDリストを生成
     * 呼び出し元からのリクエストデータに違いがあるため、ケース毎に処理を切り分け
     */
    private function getTrnOrderHeaderIds($requestData) {
        $trnOrderHeaderIds = array();
        if (isset($requestData['TrnOrderHeader']['id'])) {
            // 発注履歴から呼ばれるケース(発注確定完了も?)
            $trnOrderHeaderIds = $requestData['TrnOrderHeader']['id'];
        } else if (isset($requestData['Order']['id'])) {
            // EDIによる受注登録画面から呼ばれるケース
            $trnOrderHeaderIds = $requestData['Order']['id'];
        }
        return $trnOrderHeaderIds;
    }

    /*
     * リクエストデータの情報から処理対象となるTrnStickerのIDリストを生成
     * 呼び出し元からのリクエストデータに違いがあるため、ケース毎に処理を切り分け
     */
    private function getTrnStickerIds($requestData) {
        $trnStickerIds = array();
        if (isset($requestData['MstReceipts']['sticker_id'])) {
            // 入荷処理完了から呼ばれるケース
            $trnStickerIds = $requestData['MstReceipts']['sticker_id'];
        } else if (isset($requestData['MstReceipts']['id'])) {
            // 入荷履歴から呼ばれるケース (MstReceipts.idにTrnReceivingHeader.idがセットされている)
            $options = array(
                'recursive' => -1,
                'fields' => array('TrnSticker.id'),
                'joins' => array(
                    array(
                        'table' => 'trn_receivings',
                        'alias' => 'TrnReceiving',
                        'type' => 'INNER',
                        'conditions' => array(
                            'TrnReceiving.trn_receiving_header_id = TrnReceivingHeader.id',
                        ),
                    ),
                    array(
                        'table' => 'trn_stickers',
                        'alias' => 'TrnSticker',
                        'type' => 'INNER',
                        'conditions' => array(
                            'TrnSticker.id = TrnReceiving.trn_sticker_id',
                        ),
                    ),
                ),
                'conditions' => array('TrnReceivingHeader.id' => $requestData['MstReceipts']['id']),
                'order' => array('TrnSticker.id'),
            );

            $result = $this->TrnReceivingHeader->find('all', $options, !$this->AuthSession->isEdiUserRole());
            foreach ($result as $data) {
                $trnStickerIds[] = $data['TrnSticker']['id'];
            }
        } else if (isset($requestData['Edi']['id'])) {
            // EDIによる出荷登録完了から呼ばれるケース
            $options = array(
                    'recursive' => -1,
                    'fields' => array('TrnSticker.id'),
                    'joins' => array(
                            array(
                                    'table' => 'trn_stickers',
                                    'alias' => 'TrnSticker',
                                    'type' => 'INNER',
                                    'conditions' => array(
                                            'TrnSticker.trn_edi_shipping_id = TrnEdiShipping.id',
                                    ),
                            ),
                    ),
                    'conditions' => array(
                            'TrnEdiShipping.trn_edi_shipping_header_id' => $requestData['Edi']['id'],
                            'NOT' => array(
                                    'TrnSticker.hospital_sticker_no' => '',
                            ),
                    ),
            );

            $result = $this->TrnEdiShipping->find('all', $options, !$this->AuthSession->isEdiUserRole());
            foreach ($result as $data) {
                $trnStickerIds[] = $data['TrnSticker']['id'];
            }
        }
        return $trnStickerIds;
    }

    /*
     * リクエストデータの情報から処理対象となるTrnEdiShippingのIDリストを生成
     * 呼び出し元からのリクエストデータに違いがあるため、ケース毎に処理を切り分け
     */
    private function getTrnShippingHeaderIds($requestData) {
        $trnShippingHeaderIds = array();
        if (isset($requestData['Edi']['id'])) {
            // 出荷登録完了、出荷履歴から呼ばれるケース
            $trnShippingHeaderIds = $requestData['Edi']['id'];
        }
        return $trnShippingHeaderIds;
    }

}
