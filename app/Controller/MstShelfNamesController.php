<?php
class MstShelfNamesController extends AppController {

    var $name = 'MstShelfNames';

    var $uses = array(
        'MstFacility',
        'MstFacilityRelation',
        'MstUserBelonging',
        'MstShelfName',
        'TrnSticker',
        'TrnStock',
        'MstFixedCount',
        'MstDepartment'
    );

    /**
     *
     * @var array  $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker','common');

    /**
     * @var array $components
     */
    var $components = array('xml','Common','RequestHandler','utils','Barcode','CsvWriteUtils');
    

    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }
    
    /**
     * 棚番号マスタ検索
     *
     */
    public function search() {
        $this->setRoleFunction(92); //棚マスタ

        App::import('Sanitize');  //サニタイズ関数
        $result = array();  //棚名一覧用配列

        //施設のリスト取得
        
        $facility_list = $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                array(1,2)
                                                );

        $department_list = array();
        //検索か初期表示か
        if(isset($this->request->data['searchFlg'])){
            //施設プルダウンが選択されている場合、部署リストを取得
            if(isset($this->request->data['MstShelfName']['facilityCode']) && $this->request->data['MstShelfName']['facilityCode'] != ""){
                $department_list = $this->getDepartmentsList($this->request->data['MstShelfName']['facilityCode'] ,
                                                             false 
                                                             );

            }

            //棚一覧を取得
            $sql  = ' select ';
            $sql .= '     a.id                           as "MstShelfName__id" ';
            $sql .= '   , b.facility_name                as "MstFacility__facility_name" ';
            $sql .= '   , c.department_name              as "MstDepartment__department_name" ';
            $sql .= '   , a.code                         as "MstShelfName__code" ';
            $sql .= '   , a.name                         as "MstShelfName__name"  ';
            $sql .= ' from ';
            $sql .= '   mst_shelf_names as a  ';
            $sql .= '   left join mst_facilities as b  ';
            $sql .= '     on b.id = a.mst_facility_id  ';
            $sql .= '   left join mst_departments as c  ';
            $sql .= '     on c.id = a.mst_department_id  ';
            $sql .= '   left join mst_facility_relations as d  ';
            $sql .= '     on d.partner_facility_id = b.id  ';
            $sql .= '     or d.mst_facility_id = b.id  ';
            $sql .= ' where ';
            $sql .= '   a.is_deleted = false  ';
            $sql .= '   and (  ';
            $sql .= '     d.partner_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     or d.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '   )  ';
            //画面入力値でWHERE句生成
            if(isset($this->request->data['MstShelfName']['facilityCode']) && $this->request->data['MstShelfName']['facilityCode'] != ""){
                $sql .= " AND b.facility_code = '".$this->request->data['MstShelfName']['facilityCode'] . "'";
            }
            if(isset($this->request->data['MstShelfName']['departmentCode']) && $this->request->data['MstShelfName']['departmentCode'] != ""){
                $sql .= " AND c.department_code = '".$this->request->data['MstShelfName']['departmentCode'] . "'";
            }
            if(!empty($this->request->data['MstShelfName']['code'])){
                $sql .= ' AND a.code like '."'%".Sanitize::escape($this->request->data['MstShelfName']['code'])."%'";
            }
            if(!empty($this->request->data['MstShelfName']['name'])){
                $sql .= ' AND a.name like '."'%".Sanitize::escape($this->request->data['MstShelfName']['name'])."%'";
            }

            
            $sql .= ' group by ';
            $sql .= '   a.id ';
            $sql .= '   , b.facility_name ';
            $sql .= '   , c.department_name ';
            $sql .= '   , b.facility_code ';
            $sql .= '   , c.department_code ';
            $sql .= '   , a.code ';
            $sql .= '   , a.name  ';
            $sql .= ' order by ';
            $sql .= '   b.facility_code ';
            $sql .= '   , c.department_code ';
            $sql .= '   , a.code  ';

            

            $this->set('max' , $this->getMaxCount($sql , 'MstShelfName'));
            $sql .= ' LIMIT '.$this->request->data["limit"];

            $res = array();
            $res = $this->MstShelfName->query($sql);
            $result = AppController::outputFilter($res);
        }
        
        $this->set('department_list',$department_list);
        $this->set('selected_facility_id',$this->Session->read('Auth.facility_id_selected'));
        
        $this->set('result',$result);
        $this->set('facility_list',$facility_list);
    }

    /**
     * 棚番号マスタ新規登録
     *
     */
    function add(){
        //施設のリスト取得
        $facility_list = $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                array(1,2)
                                                );

        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
        
        $this->set("facility_list",$facility_list);
    }

    /**
     * 棚番号マスタ更新
     *
     */
    function regist(){
        //棚番号マスタ検索
        $sql  = 'SELECT MstShelfName.id               AS "MstShelfName__id"';
        $sql .= '      ,MstShelfName.code             AS "MstShelfName__code"';
        $sql .= '      ,MstShelfName.name             AS "MstShelfName__name"';
        $sql .= '      ,MstFacility.id                AS "MstShelfName__facilityId"';
        $sql .= '      ,MstFacility.facility_name     AS "MstShelfName__facilityName"';
        $sql .= '      ,MstFacility.facility_code     AS "MstShelfName__facilityCode"';
        $sql .= '      ,MstDepartment.id              AS "MstShelfName__departmentId"';
        $sql .= '      ,MstDepartment.department_name AS "MstShelfName__departmentName"';
        $sql .= '      ,MstDepartment.department_code AS "MstShelfName__departmentCode"';
        $sql .= '  FROM mst_shelf_names AS MstShelfName';
        $sql .= ' INNER JOIN mst_facilities AS MstFacility';
        $sql .= '    ON MstShelfName.mst_facility_id = MstFacility.id';
        $sql .= ' INNER JOIN mst_departments AS MstDepartment';
        $sql .= '    ON MstShelfName.mst_department_id = MstDepartment.id';
        $sql .= ' WHERE MstShelfName.is_deleted = false';
        $sql .= '   AND MstShelfName.id = '.$this->request->data['MstShelfName']['id'];

        $res = $this->MstShelfName->query($sql);
        $this->request->data['MstShelfName'] = $res[0]['MstShelfName'];
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }

    /**
     * 棚番号マスタ完了
     *
     */
    function result(){
        App::import('Sanitize');  //サニタイズ関数
        $now = date('Y/m/d H:i:s');
        
        //トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            $this->request->data = $this->Session->read("data");
        }else{
        
            $this->MstShelfName->begin();

            //行ロック
            if(!empty($this->request->data['MstShelfName']['id'])){
                $this->MstShelfName->query('select * from mst_shelf_names as a where a.id = ' .$this->request->data['MstShelfName']['id']. ' for update ');
            }

            // 施設コードから施設IDを取得
            $this->request->data['MstShelfName']['facilityId'] = $this->getFacilityId($this->request->data['MstShelfName']['facilityCode'] ,
                                                                             array(Configure::read('FacilityType.center'),
                                                                                   Configure::read('FacilityType.hospital'))
                                                                             );
            // 部署コードから部署IDを取得
            if($this->request->data['MstShelfName']['departmentCode']!=''){
                $this->request->data['MstShelfName']['departmentId'] = $this->getDepartmentId($this->request->data['MstShelfName']['facilityId'],
                                                                                              array(Configure::read('DepartmentType.warehouse'),
                                                                                                    Configure::read('DepartmentType.hospital')
                                                                                                    ),
                                                                                              $this->request->data['MstShelfName']['departmentCode']
                                                                                              );
            }

            //棚番号(code)の重複チェック
            $sql = '';
            $sql .= 'SELECT MstShelfName.id AS "MstShelfName__id"';
            $sql .= '  FROM mst_shelf_names AS MstShelfName';
            $sql .= ' WHERE MstShelfName.code = '."'".Sanitize::escape($this->request->data['MstShelfName']['code'])."'";
            $sql .= '   AND MstShelfName.mst_facility_id = '.$this->request->data['MstShelfName']['facilityId'];
            $sql .= '   AND MstShelfName.mst_department_id = '.$this->request->data['MstShelfName']['departmentId'];
            if(!empty($this->request->data['MstShelfName']['id'])){
                $sql .= ' AND MstShelfName.id <> '.$this->request->data['MstShelfName']['id'];
            }
            $res = $this->MstShelfName->query($sql);

            if(count($res) !== 0){
                //重複あり
                $this->MstShelfName->rollback();
                $this->Session->setFlash("棚番号が重複しているため登録できませんでした", 'growl', array('type'=>'error') );
                $this->redirect('search');
            }

            //棚番号マスタIDの有無で新規登録か更新かの処理分け
            if(empty($this->request->data['MstShelfName']['id'])){
                //新規登録
                $this->MstShelfName->create();
                $res1 = $this->MstShelfName->save(array(
                    'mst_facility_id'   => $this->request->data['MstShelfName']['facilityId'],
                    'mst_department_id' => $this->request->data['MstShelfName']['departmentId'],
                    'name'              => Sanitize::escape($this->request->data['MstShelfName']['code']),
                    'is_deleted'        => false,
                    'creater'           => $this->Session->read('Auth.MstUser.id'),
                    'modifier'          => $this->Session->read('Auth.MstUser.id'),
                    'created'           => $now,
                    'modified'          => $now,
                    'code'              => Sanitize::escape($this->request->data['MstShelfName']['code'])
                    ));

                if(false === $res1){
                    //新規登録エラー
                    $this->Session->setFlash("棚番号データ新規登録エラー", 'growl', array('type'=>'error') );
                    $this->MstShelfName->rollback();
                    $this->redirect('search');
                }
                $this->request->data['url'] = "棚番号登録";
            }else{
                //更新
                $updatecondtion = array("MstShelfName.id" => $this->request->data['MstShelfName']['id']);
                $updatefield = array(
                    'MstShelfName.name'     => "'".Sanitize::escape($this->request->data['MstShelfName']['code'])."'",
                    'MstShelfName.code'     => "'".Sanitize::escape($this->request->data['MstShelfName']['code'])."'",
                    'MstShelfName.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'MstShelfName.modified' => "'" . $now . "'"
                    );
                $res1 = $this->MstShelfName->updateAll( $updatefield, $updatecondtion );
                if(false === $res1){
                    //更新エラー
                    $this->Session->setFlash("棚番号データ新規登録エラー", 'growl', array('type'=>'error') );
                    $this->MstShelfName->rollback();
                    $this->redirect('search');
                }
                $this->request->data['url'] = "棚番号編集";
            }

            $this->MstShelfName->commit();
            $this->Session->write('data' , $this->request->data);
        }
    }


    /**
     * 棚番バーコード帳票印字
     */
    function report(){
        //棚一覧を取得
        $sql  = 'SELECT '; 
        $sql .= '    MstFacilities.facility_name          AS "MstFacilities__center_name" ';
        $sql .= '   ,MstShelfName.mst_facility_id         AS "MstFacilities__mst_facility_id" ';
        $sql .= '   ,MstFacilities.facility_code          AS "MstFacilities__facility_code" ';
        $sql .= '   ,MstFacilities.facility_name          AS "MstFacilities__facility_name" ';
        $sql .= '   ,MstShelfName.mst_department_id       AS "MstFacilities__mst_department_id" ';
        $sql .= '   ,MstDepartments.department_code       AS "MstFacilities__department_code" ';
        $sql .= '   ,MstDepartments.department_name       AS "MstFacilities__department_name" ';
        $sql .= '   ,MstShelfName.id                      AS "MstShelfName__id" ';
        $sql .= '   ,MstShelfName.code                    AS "MstShelfName__code" ';
        $sql .= '   ,MstShelfName.name                    AS "MstShelfName__name" ';
        $sql .= 'FROM mst_shelf_names AS MstShelfName ';
        $sql .= 'LEFT JOIN mst_facilities AS MstFacilities ';
        $sql .= '   ON MstShelfName.mst_facility_id = MstFacilities.id ';
        $sql .= 'LEFT JOIN mst_departments AS MstDepartments ';
        $sql .= '   ON MstShelfName.mst_department_id = MstDepartments.id ';
        $sql .= 'WHERE '; 
        $sql .= '   MstShelfName.is_deleted = false ';
        $sql .= '   AND MstFacilities.id = ' . $this->request->data['MstFacility']['id'];
        //画面入力値でWHERE句生成
        if(isset($this->request->data['MstShelfName']['facilityCode']) && $this->request->data['MstShelfName']['facilityCode'] != ""){
            $sql .= ' AND MstShelfName.mst_facility_id = '.$this->request->data['MstShelfName']['facilityCode'];
        }
        if(isset($this->request->data['MstShelfName']['departmentCode']) && $this->request->data['MstShelfName']['departmentCode'] != ""){
            $sql .= ' AND MstShelfName.mst_department_id = '.$this->request->data['MstShelfName']["department"];
        }
        if(!empty($this->request->data['MstShelfName']['code'])){
            $sql .= ' AND MstShelfName.code = '."'".Sanitize::escape($this->request->data['MstShelfName']['code'])."'";
        }
        if(!empty($this->request->data['MstShelfName']['name'])){
            $sql .= ' AND MstShelfName.name = '."'".Sanitize::escape($this->request->data['MstShelfName']['name'])."'";
        }
        $sql .= ' ORDER BY MstFacilities.facility_code';
        $sql .= '         ,MstDepartments.department_code';
        $sql .= '         ,MstShelfName.code';
        
        $result = array(); 
        $result = $this->MstShelfName->query($sql);

        $head = array(
            'センター名',
            '施設ID',
            '施設コード',
            '施設名',
            '部署ID',
            '部署コード',
            '部署名',
            '棚ID',
            '棚コード',
            '棚名',
            '印刷年月日',
            );
        $output = array();
        foreach($result as $row){
            $output[] = array(
                $row['MstFacilities']['center_name'],
                $row['MstFacilities']['mst_facility_id'],
                $row['MstFacilities']['facility_code'],
                $row['MstFacilities']['facility_name'],
                $row['MstFacilities']['mst_department_id'],
                $row['MstFacilities']['department_code'],
                $row['MstFacilities']['department_name'],
                $row['MstShelfName']['id'],
                $row['MstShelfName']['code'],
                $row['MstShelfName']['name'],
                date('Y/m/d H:i:s')
                );
        }
        
        $this->layout = 'xml/default';
        $this->set('head', $head);
        $this->set('result', $output);
        $this->render('report');
    }
    /**
     * CSV取込画面遷移
     */
    function csvimport(){
        //センターのみ
        //施設のリスト取得
        $facility_list =  parent::getFacilityList(
            $this->Session->read('Auth.facility_id_selected') , // 操作中のセンター
            array(Configure::read('FacilityType.center')) ,       // センター
            true,                                               // 削除フラグをみる
            array('id' , 'facility_name')
            );
        $this->set('facility_list',$facility_list);
        $this->render('csv_confirm');
    }
    
    /**
     * CSV取込処理
     */
    function execution_import_result() {
        //結果画面初期化
        $this->set('csvresult' , false);
        //センターのみ
        //施設のリスト取得
        $facility_list =  parent::getFacilityList(
            $this->Session->read('Auth.facility_id_selected') , // 操作中のセンター
            array(Configure::read('FacilityType.center')) ,      // センター
            true,// 削除フラグをみる
            array('id' , 'facility_name') 
            );
        $this->set("facility_list",$facility_list);
        //CSV登録
        if(!isset($this->request->data['selected']['from_csv'])) {
            $this->render('csv_confirm');
            return;
        }
        //ファイルアップ
        $_upfile = $this->request->data['selected']['csv_file']['tmp_name'];
        $_fileName = '../tmp/'.date('YmdHi').'_shelf.csv';
        if (is_uploaded_file($_upfile)){
            move_uploaded_file($_upfile, mb_convert_encoding($_fileName, 'UTF-8', "SJIS-win"));
            chmod($_fileName, 0644);
        }else{
            //ファイル無しエラー画面に遷移
            $this->Session->setFlash('ファイルが読み込めませんでした。CSVファイルを確認してください。', 'growl', array('type'=>'error') );
            $this->render('csv_confirm');
             return;
        }
        //アップロードしたCSVデータ文字列として読み込む
        $_csvdata = mb_convert_encoding(file_get_contents ($_fileName), 'UTF-8', "SJIS-win");
        $csv_array = $this->utils->input_csv($_csvdata);
        $mapped_csv = $this->_csvMapping($csv_array);
        
        $read_count = 0;
        $err_count  = 0;
        $target = array();
        foreach($mapped_csv as $value) {
            $_barcode = $this->Barcode->readEan128($value['sticker_no']);
            $item = array();
            //商品情報の取得
            if($_barcode != false ){
                //川鉄シールの場合
                $item = $this->kawatetu($_barcode);
            }else{
                //通常シールの場合
                $item = $this->getItemInfoBySealno($value['sticker_no']);
            }
            if (!$item){
                $err_count++;
                continue;
            }
            //マスタに存在する棚IDかの判定
            if ($this->isExistMstShelfName($value['shelf_id'], $this->request->data['facility'])==False){
                $err_count++;
                continue;
            }
            $department_id = $this->getDepartmentId($this->request->data['facility'] , Configure::read('DepartmentType.warehouse'));
            $item_unit_id = $item[0]['MstItemUnits']['mst_item_unit_id'];
            $target[] = array(
                'MstItemUnits'    => $item[0]['MstItemUnits'],
                'MstFacilityId'   => $this->request->data['facility'],
                'MstShelfNameId'  => $value['shelf_id'],
                'MstDepartmentId' => $department_id
                );
            $read_count++;
        }
        //トランザクション開始
        $this->MstFixedCount->begin();
        //棚番登録処理
        $ret = $this->updateFixedCount($target);
        if ($ret == false){
            //ロールバック
            $this->MstFixedCount->rollback();
            $this->render('csv_confirm');
            return; 
        }
        //コミット
        $this->MstFixedCount->commit();   

        $this->set('csvresult' , true);
        $this->set('read_count' , $read_count);
        $this->set('err_count' , $err_count);
        $this->render('csv_confirm'); 
    }
    /**
     * CSVマッピング
     */
    function _csvMapping($csv_array){
        $csvdata = array();
        foreach($csv_array as $title => $row){
            if (isset($row[0]) && isset($row[1])){
                $csvdata[] = array(
                    "sticker_no" => trim($row[0]), 
                    "shelf_id"   => trim($row[1]),
                    );
            }
        }
        return $csvdata;
    }
    /**
     * 川鉄在庫の場合の商品情報取得
     */
    function kawatetu($barcode){
        
        $sql  = ' select ';
        $sql .= '    TrnStickers.mst_item_unit_id       AS "MstItemUnits__mst_item_unit_id" ';
        $sql .= '   ,MstItemUnits.mst_facility_item_id  AS "MstItemUnits__mst_facility_item_id" ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as TrnStickers ';
        $sql .= '     left join mst_item_units as MstItemUnits  ';
        $sql .= '           on MstItemUnits.id = TrnStickers.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as MstFacilityItems  ';
        $sql .= '           on MstFacilityItems.id = MstItemUnits.mst_facility_item_id  ';
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .= '     and MstFacilityItems.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        if($barcode['quantity'] == 1 || $barcode['quantity'] == null ){
            $sql .= '     and MstItemUnits.per_unit = 1  ';
        }else{
            $sql .= '     and MstItemUnits.per_unit = ' . $barcode['quantity'];
        }
        $sql .= "     and MstFacilityItems.jan_code like '" . $barcode['pure_jan_code'] ."%'";
        $sql .= "     and TrnStickers.facility_sticker_no = ''  ";
        $sql .= "     and TrnStickers.hospital_sticker_no = ''  ";
        return  $this->TrnSticker->query($sql);
    }
    /**
     * センターシール（部署シール）NOから商品情報取得
     */
    private function getItemInfoBySealno($sealno){
        $sql  = 'select ';
        $sql .= '    TrnStickers.mst_item_unit_id      AS "MstItemUnits__mst_item_unit_id" ';
        $sql .= '   ,MstItemUnits.mst_facility_item_id AS "MstItemUnits__mst_facility_item_id" ';
        $sql .= 'from trn_stickers AS TrnStickers ';
        $sql .= 'left join mst_item_units AS MstItemUnits ';
        $sql .= ' on '; 
        $sql .= '  TrnStickers.mst_item_unit_id = MstItemUnits.id ';
        $sql .= 'where ';
        $sql .= '   ( ';
        $sql .= "    TrnStickers.facility_sticker_no = "."'".$sealno."'";
        $sql .= '   or ';
        $sql .= "   TrnStickers.hospital_sticker_no = "."'".$sealno."'";
        $sql .= '   ) ';
        return  $this->TrnSticker->query($sql);
    }
    /**
     * 棚マスタ存在チェックと
     * 取込対象の棚IDの施設種別と選択された施設種別が一致するか
     * （PDA入力と施設選択の誤りチェック用）
     */  
    private function isExistMstShelfName($shelf_id, $facility_id){
        $sql  = 'select ';
        $sql .= '   count(MstShelfNames.id) AS "MstShelfNames__cnt" ';
        $sql .= 'from mst_shelf_names AS MstShelfNames ';
        $sql .= 'inner join mst_facilities AS MstFacilities ';
        $sql .= '    on MstShelfNames.mst_facility_id = MstFacilities.id ';
        $sql .= 'inner join ';
        $sql .= '( ';
        $sql .= '    select  '; 
        $sql .= '        id, ';
        $sql .= '        facility_type ';
        $sql .= '    from mst_facilities ';
        $sql .= '    where ';
        $sql .= '    id = ' .$facility_id;
        $sql .= ') SelectedFacility ';
        $sql .= '     on MstFacilities.id = SelectedFacility.id ';
        $sql .= '    and ';
        $sql .= '    MstFacilities.facility_type = SelectedFacility.facility_type ';
        $sql .= 'where ';
        $sql .= '    MstShelfNames.id = ' .$shelf_id;
        $ret = $this->MstShelfName->query($sql);
        return  ($ret[0]['MstShelfNames']['cnt'] > 0 ? true : false);
    } 
    /**
     * 定数マスタ登録
     */
    private function updateFixedCount($result){
        $now = date('Y/m/d H:i:s');
        
        for ($i = 0; $i < count($result);$i++) {
            $item_unit_id         = $result[$i]['MstItemUnits']['mst_item_unit_id'];
            $mst_item_id          = $result[$i]['MstItemUnits']['mst_item_id'];
            $mst_facility_item_id = $result[$i]['MstItemUnits']['mst_facility_item_id'];
            $department_id        = $result[$i]['MstDepartmentId'];
            $facility_id          = $result[$i]['MstFacilityId'];
            $shelf_id             = $result[$i]['MstShelfNameId'];
            //定数マスタ存在チェック
            $fixedId = $this->isExistMstFixedCount($result[$i]['MstItemUnits'], $facility_id, $department_id);
            if (!isset($fixedId)){
                //在庫レコードの取得
                $stockId = $this->getStockRecode($item_unit_id, $department_id);
                $now_startdate = date('Y-m-d');
                //新規(削除フラグ=trueで）
                $fixedCount = array(
                    'MstFixedCount'=> array(
                        'trn_stock_id'          => $stockId, 
                        'mst_facility_item_id'  => $mst_facility_item_id, 
                        'mst_item_unit_id'      => $item_unit_id,
                        'mst_facilities_id'     => $facility_id, 
                        'mst_department_id'     => $department_id, 
                        'order_point'           => 0,
                        'fixed_count'           => 0,
                        'holiday_fixed_count'   => 0,
                        'spare_fixed_count'     => 0,
                        'mst_shelf_name_id'     => $shelf_id, 
                        'trade_type'            => 1,
                        'substitute_unit_id'    => null,
                        'start_date'            => $now_startdate,
                        'is_deleted'            => true,
                        'creater'               => $this->Session->read('Auth.MstUser.id'),
                        'modifier'              => $this->Session->read('Auth.MstUser.id'),
                        'created'               => $now,
                        'modified'              => $now,
                        )
                    );
                $this->MstFixedCount->create();
                $fRes = $this->MstFixedCount->save($fixedCount);
                if($fRes === FALSE){
                    $this->Session->setFlash('定数作成処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    return false;
                }
            }else{
                $condtion = array( "MstFixedCount.id" => $fixedId );
                $field = array( "MstFixedCount.mst_shelf_name_id" => $shelf_id
                                ,"MstFixedCount.modifier" => $this->Session->read('Auth.MstUser.id')
                                ,"MstFixedCount.modified" => "'" . $now . "'"
                                );
                $fRes = $this->MstFixedCount->updateAll( $field, $condtion);
                if(false === $fRes){
                    $this->Session->setFlash('定数作成処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    return false;
                }
            }
        }
        return  true;
    }
    /**
     * 定数IDの取得
     */
    private function isExistMstFixedCount($item_unit, $facility_id, $department_id){
        $mst_facility_item_id = $item_unit['mst_facility_item_id'];
        $item_unit_id         = $item_unit['mst_item_unit_id'];
        $sql  = 'select ';
        $sql .= '   id  AS "MstFixedCounts__id" ';
        $sql .= 'from mst_fixed_counts ';
        $sql .= 'where ';
        $sql .= '   mst_facility_item_id = '.$mst_facility_item_id;
        $sql .= '   and ';
        $sql .= '   mst_item_unit_id = '.$item_unit_id; 
        $sql .= '   and ';
        $sql .= '   mst_facilities_id = '.$facility_id;
        $sql .= '   and ';
        $sql .= '   mst_department_id = '.$department_id;
        $ret = array();
        $ret =  $this->MstFixedCount->query($sql);
        return (($ret) ? $ret[0]['MstFixedCounts']['id'] : null);
    }
    
    public function export_csv(){
        App::import('Sanitize');
        $sql  = ' select ';
        $sql .= '       b.facility_name   as 施設名';
        $sql .= '     , c.department_name as 部署名';
        $sql .= '     , a.code            as 棚番号';
        $sql .= '     , a.name            as 棚名';
        $sql .= '   from ';
        $sql .= '     mst_shelf_names as a  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.mst_facility_id  ';
        $sql .= '     left join mst_departments as c  ';
        $sql .= '       on c.id = a.mst_department_id  ';
        $sql .= '     left join mst_facility_relations as d  ';
        $sql .= '       on (  ';
        $sql .= '         d.partner_facility_id = a.mst_facility_id  ';
        $sql .= '         or d.mst_facility_id = a.mst_facility_id ';
        $sql .= '       )  ';
        $sql .= '     inner join mst_user_belongings as e  ';
        $sql .= '       on e.mst_user_id = ' . $this->request->data['MstUser']['id'];
        $sql .= '       and e.mst_facility_id = a.mst_facility_id  ';
        $sql .= '       and e.is_deleted = false  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and (a.mst_facility_id = ' . $this->request->data['MstFacility']['id'] . ' or d.mst_facility_id = ' .$this->request->data['MstFacility']['id'] . ' )  ';


        //画面入力値でWHERE句生成
        if(isset($this->request->data['MstShelfName']['facility']) && $this->request->data['MstShelfName']['facility'] != ""){
            $sql .= ' and a.mst_facility_id = '.$this->request->data['MstShelfName']['facility'];
        }
        if(isset($this->request->data['MstShelfName']['department']) && $this->request->data['MstShelfName']['department'] != ""){
            $sql .= ' and a.mst_department_id = '.$this->request->data['MstShelfName']['department'];
        }
        if(!empty($this->request->data['MstShelfName']['code'])){
            $sql .= ' and a.code like '."'%".Sanitize::escape($this->request->data['MstShelfName']['code'])."%'";
        }
        if(!empty($this->request->data['MstShelfName']['name'])){
            $sql .= ' and a.name like '."'%".Sanitize::escape($this->request->data['MstShelfName']['name'])."%'";
        }
        
        $sql .= '   group by ';
        $sql .= '     b.facility_name ';
        $sql .= '     , b.facility_code ';
        $sql .= '     , c.department_name ';
        $sql .= '     , c.department_code ';
        $sql .= '     , a.name ';
        $sql .= '     , a.code  ';
        $sql .= '   order by ';
        $sql .= '     b.facility_code ';
        $sql .= '     , c.department_code ';
        $sql .= '     , a.code ';

        $this->db_export_csv($sql , "棚番号一覧", 'search');
        
    }
}
?>
