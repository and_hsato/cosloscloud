<?php
/**
 * MstAnalyzeOrdersController
 * 分析並び順マスタメンテ
 * @since 2011/02/01
 */
class MstAnalyzeOrdersController extends AppController {
    var $name = 'MstAnalyzeOrders';

    var $uses = array(
        'MstUser'
       ,'MstAnalyze'
       ,'MstAnalyzeRole'
       ,'MstAnalyzeOrder'
       ,'MstFacility'
    );

    var $Auth;
    var $Session;
    var $MstRoles;

    var $components = array('RequestHandler');

    public function  beforeFilter() {
        parent::beforeFilter();
    }

    public function index(){
        $this->analyzes_list();
    }

    public function analyzes_list() {
        $this->setRoleFunction(99); //分析並べ替えマスタ
        $Analyzes_List = array();
        App::import('Sanitize');
        //施設プルダウン用データ取得
        $this->set('facility_enabled',
                   $this->getFacilityList(
                       null ,
                       array(Configure::read('FacilityType.center')),
                       true,
                       array('id' , 'facility_name')
                       )
                   );
        
        // 初回時でない場合のみデータを検索する
        if(isset($this->request->data['MstAnalyze']['is_search'])){
            $sql  = 'SELECT A.id            As "MstAnalyze__id" ';
            $sql .= '      ,A.title         As "MstAnalyze__title" ';
            $sql .= "      ,coalesce(B.facility_name,'すべて') ";
            $sql .= '                       As "MstAnalyze__center_name"';
            $sql .= '      ,A.is_deleted    As "MstAnalyze__is_deleted"';
            $sql .= '     , C.is_hidden     As "MstAnalyze__is_hidden"';
            $sql .= '     , C.order_num     As "MstAnalyze__order_num"';
            $sql .= '     , C.title         As "MstAnalyze__other_title"';
            $sql .= '     , C.id            As "MstAnalyze__order_id"' ;
            
            $sql .= '  FROM mst_analyzes A ';
            $sql .= '  LEFT JOIN mst_facilities B ';
            $sql .= '  ON B.id = A.mst_facility_id ';
            $sql .= '     left join mst_analyze_orders as C ';
            $sql .= '       on C.mst_analyze_id = A.id ';
            $sql .= '       and C.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
            $sql .= ' WHERE 1 = 1 ';

            // ユーザ入力値による検索条件の作成--------------------------------------------
            // タイトル(部分一致)
            if ( (isset($this->request->data['MstAnalyze']['title'])) && ($this->request->data['MstAnalyze']['title'] != "") ) {
                $sql .= " AND A.title ILIKE '%" . Sanitize::escape( $this->request->data['MstAnalyze']['title'] ) . "%' ";
            }

            // 非表示フラグ
            if( (isset($this->request->data['MstAnalyze']['hidden'])) && ($this->request->data['MstAnalyze']['hidden'] == 1) ){
            }else{
                $sql .= " AND ( C.is_hidden = false or C.is_hidden is null )";
            }
            
            $sql .= ' AND ( B.id = ' . $this->Session->read('Auth.facility_id_selected') . ' or B.id is null ) ';
            $sql .= ' AND A.is_deleted = false ';
            
            $sql .= " ORDER BY ";
            $sql .= "       C.order_num , A.id ";
            
            $this->set('max' , $this->getMaxCount($sql , 'MstAnalyze'));
            $sql .= " LIMIT " . $this->_getLimitCount();

            $Analyzes_List = $this->MstAnalyze->query( $sql );
        }
        $this->set('Analyzes_List',$Analyzes_List);
    }
    
    public function result(){
        $now = date('Y/m/d H:i:s.u');
        $this->MstAnalyzeOrder->begin();
        foreach($this->request->data['MstAnalyze']['id'] as $id){
            if($this->request->data['MstAnalyze']['order_id'][$id] == ''){
                //新規
                $save_data = array(
                    'mst_facility_id' => $this->Session->read('Auth.facility_id_selected') , 
                    'mst_analyze_id'  => $id,
                    'is_hidden'       => (isset($this->request->data['MstAnalyze']['is_hidden'][$id])?true:false),
                    'order_num'       => (($this->request->data['MstAnalyze']['order_num'][$id]=='')?null:$this->request->data['MstAnalyze']['order_num'][$id]),
                    'title'           => (($this->request->data['MstAnalyze']['other_title'][$id]=='')?null:$this->request->data['MstAnalyze']['other_title'][$id]),
                    'is_deleted'      => false ,
                    'creater'         => $this->Session->read('Auth.MstUser.id'),
                    'created'         => $now,
                    'modifier'        => $this->Session->read('Auth.MstUser.id'),
                    'modified'        => $now,
                    );
            }else{
                //更新
                $save_data = array(
                    'id'              => $this->request->data['MstAnalyze']['order_id'][$id],
                    'mst_facility_id' => $this->Session->read('Auth.facility_id_selected') , 
                    'mst_analyze_id'  => $id,
                    'is_hidden'       => (isset($this->request->data['MstAnalyze']['is_hidden'][$id])?true:false),
                    'order_num'       => (($this->request->data['MstAnalyze']['order_num'][$id]=='')?null:$this->request->data['MstAnalyze']['order_num'][$id]),
                    'title'           => (($this->request->data['MstAnalyze']['other_title'][$id]=='')?null:$this->request->data['MstAnalyze']['other_title'][$id]),
                    'is_deleted'      => false ,
                    'creater'         => $this->Session->read('Auth.MstUser.id'),
                    'created'         => $now,
                    'modifier'        => $this->Session->read('Auth.MstUser.id'),
                    'modified'        => $now,
                    );
            }
            $this->MstAnalyzeOrder->create();
            if(!$this->MstAnalyzeOrder->save($save_data)){
                //保存失敗
                $this->MstAnalyzeOrder->rollback();
                $this->Session->setFlash('登録に失敗しました', 'growl', array('type'=>'error') );
                $this->redirect('analyzes_list');
            }
        }
        
        $this->MstAnalyzeOrder->commit();
        
        //結果画面用にデータを取り直す
        $sql  = 'SELECT A.id            As "MstAnalyze__id" ';
        $sql .= '      ,A.title         As "MstAnalyze__title" ';
        $sql .= "      ,coalesce(B.facility_name,'すべて') ";
        $sql .= '                       As "MstAnalyze__center_name"';
        $sql .= '      ,A.is_deleted    As "MstAnalyze__is_deleted"';
        $sql .= "     , ( case when C.is_hidden = true then '○' else '' end ) ";
        $sql .= '                       As "MstAnalyze__is_hidden"';
        $sql .= '     , C.order_num     As "MstAnalyze__order_num"';
        $sql .= '     , C.title         As "MstAnalyze__other_title"';
        $sql .= '     , C.id            As "MstAnalyze__order_id"' ;
            
        $sql .= '  FROM mst_analyzes A ';
        $sql .= '  LEFT JOIN mst_facilities B ';
        $sql .= '  ON B.id = A.mst_facility_id ';
        $sql .= '     left join mst_analyze_orders as C ';
        $sql .= '       on C.mst_analyze_id = A.id ';
        $sql .= '       and C.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
        $sql .= ' WHERE 1 = 1 ';
        $sql .= ' AND ( B.id = ' . $this->Session->read('Auth.facility_id_selected') . ' or B.id is null ) ';
        $sql .= ' AND a.id in ('.join(',',$this->request->data['MstAnalyze']['id']).')';
        $sql .= ' AND A.is_deleted = false ';
        $sql .= " ORDER BY ";
        $sql .= "       C.order_num , A.id ";
        
        $result = $this->MstAnalyze->query( $sql );
        $this->set('result' , $result);
    }

}

?>
