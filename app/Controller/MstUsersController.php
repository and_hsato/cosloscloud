<?php
/**
 * MstUsersController
 *
 * @version 1.0.0
 * @since 2010/07/08
 */

class MstUsersController extends AppController {
    var $name = 'MstUsers';
    
    /**
     *
     * @var array $uses
     */
    var $uses = array('MstUser','MstDepartment','MstFacility','MstRole','MstUserBelonging');

    /**
     * @var array $components
     */
    var $components = array('CsvWriteUtils');
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;
    
    /**
     * @var MstUser
     */
    //var $MstUser;

    function beforeFilter() {
        parent::beforeFilter();
        //暗号化種別をMD5にする。
        if(Configure::read('Password.Security') == 1){
            Security::setHash('md5');
        }
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    
    /**
     * userlist
     *
     * ユーザ一覧
     */
    function userlist() {
        App::import('Sanitize');
        $this->setRoleFunction(88); //ユーザー
        $User_List = array();
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        //初期表示以外の場合のみデータを取得する
        if(isset($this->request->data['MstUser']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            
            //ユーザ入力値による検索条件の作成------------------------------------
            $where = '';
            if(Configure::read('Password.Security') == 1 ){
                //暗号化しない場合、全ユーザが無制限ユーザなので
                $where .= ' and  a.is_indefinite = false ';
            }
            
            //ログインID(LIKE検索)
            if(isset($this->request->data['MstUser']['search_login_id']) && $this->request->data['MstUser']['search_login_id'] != ''){
                $where .= " and a.login_id LIKE '%".$this->request->data['MstUser']['search_login_id']."%'";
            }
            
            //ユーザ名(LIKE検索)
            if(isset($this->request->data['MstUser']['search_user_name']) && $this->request->data['MstUser']['search_user_name'] != ''){
                $where .= " and a.user_name LIKE '%" .$this->request->data['MstUser']['search_user_name'] ."%'";
            }
            
            //削除済み表示
            if(!isset($this->request->data['MstUser']['search_is_deleted'])){
                $where .= ' and a.is_deleted = false ';
            }

            //検索条件の作成終了-------------------------------------------------

            if(!isset($this->request->data['limit'])){
                $this->request->data['limit'] = 100;
            }
            //ユーザリスト取得
            $User_List = $this->getUserList($where , $this->request->data['limit'] );
            $this->request->data = $data;
        }
        $this->set('User_List',$User_List);
    }
    
    
    /**
     * Add ユーザ新規登録
     */
    function add() {
        // 部署リスト
        $department_list = $this->getHospitalDepartmentList($this->Session->read('Auth.facility_id_selected'));
        $this->set('department_list' , $department_list);
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }
    
    /**
     * Mod ユーザ情報編集
     */
    function mod() {
        //更新チェック用にmod画面に入った瞬間の時間を保持
        $this->Session->write('Usermod.readTime',date('Y-m-d H:i:s'));
        
        $department_list = array();
        
        //userlistから引き継いできたidでユーザ情報を取得
        $sql  = ' select ';
        $sql .= '       a.login_id              as "MstUser__login_id" ';
        $sql .= '     , a.id                    as "MstUser__id" ';
        $sql .= '     , a.user_name             as "MstUser__user_name" ';
        $sql .= '     , a.password              as "MstUser__password" ';
        $sql .= '     , a.mst_facility_id       as "MstUser__mst_facility_id" ';
        $sql .= '     , a.mst_role_id           as "MstUser__mst_role_id" ';
        $sql .= '     , a.parent_id             as "MstUser__parent_id" ';
        $sql .= '     , a.is_deleted            as "MstUser__is_deleted" ';
        $sql .= '     , a.is_temp_issue         as "MstUser__is_temp_issue" ';
        $sql .= '     , a.is_indefinite         as "MstUser__is_indefinite" ';
        $sql .= '     , b.role_name             as "MstRole__role_name" ';
        $sql .= '     , b.id                    as "MstRole__id" ';
        $sql .= '     , c.facility_code         as "MstFacility__centerText" ';
        $sql .= '     , c.facility_code         as "MstFacility__centerCode" ';
        $sql .= '     , c.facility_name         as "MstFacility__centerName" ';
        $sql .= '     , e.facility_code         as "MstFacility__facilityCode" ';
        $sql .= '     , e.facility_code         as "MstFacility__facilityText" ';
        $sql .= '     , e.facility_name         as "MstFacility__facilityName" ';
        $sql .= '     , d.department_code       as "MstDepartment__departmentCode" ';
        $sql .= '     , d.department_code       as "MstDepartment__departmentText" ';
        $sql .= '     , d.department_name       as "MstDepartment__departmentName"  ';
        $sql .= '   from ';
        $sql .= '     mst_users as a  ';
        $sql .= '     left join mst_roles as b  ';
        $sql .= '       on b.id = a.mst_role_id  ';
        $sql .= '     left join mst_facilities as c  ';
        $sql .= '       on c.id = a.mst_facility_id  ';
        $sql .= '     left join mst_departments as d  ';
        $sql .= '       on d.id = a.mst_department_id  ';
        $sql .= '     left join mst_facilities as e  ';
        $sql .= '       on e.id = d.mst_facility_id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->request->data['MstUser']['id'];
        $ret = $this->MstUser->query($sql);
        $this->request->data = $ret[0];
        
        $department_list = $this->getHospitalDepartmentList($this->Session->read('Auth.facility_id_selected'));
        $this->set('department_list' , $department_list);

        $this->request->data['MstFacility']['centerCode'] = $ret[0]['MstFacility']['facilityCode'];
        
    }
    
    /**
     * result
     *
     * ユーザ情報更新（新規登録・更新）
     */
    function result() {
        $loginUser = $this->Session->read('Auth.MstUser');
        $loginUserRole = $this->Session->read('Auth.MstRole');
        
        //施設コードから、施設IDを取得
        $this->request->data['MstUser']['mst_facility_id'] = $this->Session->read('Auth.facility_id_selected');
        // カタログ用部署
        
        // 部署コードから部署IDに変更する。
        $mst_department_id = $this->getHospitalDepartmentId(
            $this->request->data['MstDepartment']['departmentCode'],
            $this->Session->read('Auth.facility_id_selected')
            );
        
        $now = date('Y/m/d H:i:s.u');
        //トランザクションを開始
        $this->MstUser->begin();
        //行ロック（更新時のみ）
        if(isset( $this->request->data['MstUser']['id']) && !empty( $this->request->data['MstUser']['id'])){
            $this->MstUser->query( ' select * from mst_users as a where a.id = ' . $this->request->data['MstUser']['id'] . ' for update ');
        }
        
        $user_data = array();
        //データ整形
        if(isset($this->request->data['MstUser']['id'])){
            //更新の場合
            $user_data['MstUser']['id'] = $this->request->data['MstUser']['id'];
        }else{
            //新規の場合
            $user_data['MstUser']['creater']        = $loginUser['id'];
            $user_data['MstUser']['created']        = $now;
            $user_data['MstUser']['effective_date'] = $this->getNextEffectiveDate(date('Y/m/d'));
            $user_data['MstUser']['user_type']      = Configure::read('UserType.hospital');
            $user_data['MstUser']['parent_id']      = $loginUser['id'];
        }
        //仮パスワード発行の場合
        if(isset($this->request->data['MstUser']['is_temp_issue']) && $this->request->data['MstUser']['is_update']==1){
            if(Configure::read('Password.Security') == 1 ){
                $user_data['MstUser']['password']          = $this->request->data['MstUser']['password'];
            }else{
                $user_data['MstUser']['password']          = $_POST['data']['MstUser']['password'];
            }
            $user_data['MstUser']['is_temp_issue']     = true;
            $user_data['MstUser']['password_modified'] = $now;
        }
        
        if(Configure::read('Password.Security') == 0 ){
            //暗号化しない場合、全ユーザが無制限ユーザ
            $user_data['MstUser']['is_indefinite']     = true;
            $user_data['MstUser']['password']          = $_POST['data']['MstUser']['password'];
            $user_data['MstUser']['is_temp_issue']     = false;
            $user_data['MstUser']['password_modified'] = $now;
        }
        
        $user_data['MstUser']['login_id']          = $this->request->data['MstUser']['login_id'];
        $user_data['MstUser']['user_name']         = $this->request->data['MstUser']['user_name'];
        $user_data['MstUser']['mst_facility_id']   = $this->request->data['MstUser']['mst_facility_id'];
        $user_data['MstUser']['mst_department_id'] = $mst_department_id;
        $user_data['MstUser']['is_deleted']        = (isset($this->request->data['MstUser']['is_deleted'])?true:false);
        $user_data['MstUser']['modifier']          = $loginUser['id'];
        $user_data['MstUser']['modified']          = $now;
        
        // カタログユーザのロールはログイン中のユーザのエディションに対応するロールをセット
        $roleTypeId = Configure::read('role.type.catalog');
        $userRoleId = $this->getRoleBy($roleTypeId, $loginUserRole['mst_edition_id']);
        $user_data['MstUser']['mst_role_id']       = $userRoleId;
        
        if(!$this->MstUser->save($user_data)){
            //ロールバック
            $this->MstUser->rollback();
            //エラーメッセージ
            $this->Session->setFlash('ユーザ情報の登録に失敗しました。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('userlist');
        }
        
        //ユーザ操作管理
        if(!isset($this->request->data['MstUser']['id'])){
            //新規の場合
            $this->request->data['MstUser']['id'] = $this->MstUser->getLastInsertId();
            
            // 親と同じ権限で、ユーザIDだけ差し替え分をINSERT
            $sql  = ' insert  ';
            $sql .= '   into mst_user_belongings(  ';
            $sql .= '     mst_user_id ';
            $sql .= '     , mst_facility_id ';
            $sql .= '     , is_deleted ';
            $sql .= '     , creater ';
            $sql .= '     , created ';
            $sql .= '     , modifier ';
            $sql .= '     , modified ';
            $sql .= '   )  ';
            $sql .= ' select ';
            $sql .= '      ' . $this->request->data['MstUser']['id'];
            $sql .= '     , mst_facility_id ';
            $sql .= '     , is_deleted ';
            $sql .= '     , ' . $loginUser['id'];
            $sql .= '     , now()';
            $sql .= '     , ' . $loginUser['id'];
            $sql .= '     , now()  ';
            $sql .= '   from ';
            $sql .= '     mst_user_belongings as a  ';
            $sql .= '   where ';
            $sql .= '     a.mst_user_id = ' . $loginUser['id'];
            
            $this->MstUser->query($sql);
        }
        //コミット
        $this->MstUser->commit();
    }
    
    
    /**
     * ユーザリストを返す。
     */
    private function getUserList($where , $limit = null){
        $sql  = ' select ';
        $sql .= '       a.id              as "MstUser__id"';
        $sql .= '     , a.login_id        as "MstUser__login_id"';
        $sql .= '     , a.user_name       as "MstUser__user_name"';
        $sql .= '     , b.facility_name   as "MstFacility__facility_name"';
        $sql .= '     , d.department_name as "MstUser__department_name"';
        $sql .= '     , c.role_name       as "MstRole__role_name"';
        $sql .= "     , ( case when a.is_temp_issue = true then '○' else '' end ) ";
        $sql .= '                         as "MstUser__is_temp_issue"';
        $sql .= "     , ( case when a.is_deleted = true then '○' else '' end ) ";
        $sql .= '                         as "MstUser__is_deleted"';
        $sql .= "     , to_char(a.password_modified , 'YYYY/mm/dd') ";
        $sql .= '                         as "MstUser__password_modified"';
        $sql .= "     , ( case when a.effective_date < now()::date then '' else '○' end ) " ;
        $sql .= '                         as "MstUser__update_check"';
        
        $sql .= '   from ';
        $sql .= '     mst_users as xa  ';
        $sql .= '     left join mst_facility_relations as xb  ';
        $sql .= '       on xa.mst_facility_id = xb.mst_facility_id  ';
        $sql .= '     left join mst_facilities as xc  ';
        $sql .= '       on xc.id = xb.partner_facility_id  ';
        $sql .= '       and xc.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '     left join mst_departments as xd  ';
        $sql .= '       on xd.mst_facility_id = xc.id  ';
        $sql .= '     inner join mst_users as a  ';
        $sql .= '       on a.mst_department_id = xd.id  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.mst_facility_id  ';
        $sql .= '     left join mst_roles as c  ';
        $sql .= '       on c.id = a.mst_role_id  ';
        $sql .= '     left join mst_departments as d ';
        $sql .= '       on d.id = a.mst_department_id ';
        $sql .= '   where 1=1 ';
        $sql .= '     and a.user_type = ' . Configure::read('UserType.hospital');
        $sql .= '     and c.mst_role_type_id = ' . Configure::read('role.type.catalog');
        $sql .= '     and xa.id = ' . $this->Session->read('Auth.MstUser.id');
        
        $sql .= $where ;
        $sql .= '   order by ';
        $sql .= '     b.id asc ';
        $sql .= '     , c.id asc ';
        $sql .= '     , a.login_id asc ';
        
        $this->set('max' , $this->getMaxCount($sql , 'MstUser'));
        if(!is_null($limit)){
            $sql .= '   limit ' . $limit;
        }
        return $this->MstUser->query($sql);
    }

    /**
     * CSV出力
     */
    public function export_csv(){
        App::import('Sanitize');
        
        //現在有効な病院一覧を取得
        $sql  = ' select ';
        $sql .= '       a.id             as "MstFacility__id"';
        $sql .= '     , a.facility_name  as "MstFacility__facility_name"';
        $sql .= '   from ';
        $sql .= '     mst_facilities as a  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '   order by ';
        $sql .= '     a.id ';
        $hospital_list = $this->MstFacility->query($sql);
        
        //ユーザ入力値による検索条件の作成------------------------------------
        $where = ' and  a.is_indefinite = false ';
        
        //ログインID(LIKE検索)
        if(($this->request->data['MstUser']['search_login_id'] != "")){
            $where .= " and a.login_id LIKE '%".Sanitize::clean($this->request->data['MstUser']['search_login_id'])."%'";
        }
        
        //ユーザ名(LIKE検索)
        if(($this->request->data['MstUser']['search_user_name'] != "")){
            $where .= " and a.user_name LIKE '%" .Sanitize::clean($this->request->data['MstUser']['search_user_name']) ."%'";
        }
        
        //施設(完全一致・施設コード)
        if($this->request->data['MstUser']['search_facility_cd'] != ""){
            $where .= " and b.facility_code = '" . $this->request->data['MstUser']['search_facility_cd'] . "'";
        }
        //ロール(完全一致)
        if($this->request->data['MstUser']['search_role_id'] != ""){
            $where .= ' and a.mst_role_id = ' . $this->request->data['MstUser']['search_role_id'];
        }
        
        //削除済み表示
        if(!isset($this->request->data['MstUser']['search_is_deleted'])){
            $where .= ' and a.is_deleted = false ';
        }
        //更新済み表示
        if(!isset($this->request->data['MstUser']['search_is_update'])){
            $where .= ' and a.effective_date < now() ';
        }
        //仮パスワード表示
        if(!isset($this->request->data['MstUser']['search_is_temp'])){
            $where .= ' and a.is_temp_issue = false ';
        }
        
        //ユーザ一覧を取得
        $sql  = ' select ';
        $sql .= '       b.facility_name        as 事業会社 ';
        $sql .= '     , a.login_id             as "ユーザID" ';
        $sql .= '     , a.user_name            as ユーザ名 ';
        foreach($hospital_list as $h){
            $sql .= '     , ( case when c' .$h['MstFacility']['id'].  '.mst_user_id is not null then ' ;
            $sql .= '    a1.role_name_s end )  as "'. $h['MstFacility']['facility_name'] . '"';
        }
        $sql .= "     , '' as \"" . date('Y/m/d') . '"';
        $sql .= '   from ';
        $sql .= '     mst_users as a  ';
        $sql .= '     left join mst_roles as a1 ';
        $sql .= '       on a.mst_role_id = a1.id';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.mst_facility_id  ';
        foreach($hospital_list as $h){
            $sql .= '     left join (  ';
            $sql .= '       select ';
            $sql .= '             a.mst_user_id  ';
            $sql .= '         from ';
            $sql .= '           mst_user_belongings as a  ';
            $sql .= '         where ';
            $sql .= '           a.is_deleted = false  ';
            $sql .= '           and a.mst_facility_id = ' . $h['MstFacility']['id'];
            $sql .= '     ) as c'. $h['MstFacility']['id'];
            $sql .= '       on c'. $h['MstFacility']['id'] .'.mst_user_id = a.id  ';
        }
        $sql .= '   where 1=1 ';
        $sql .= '     and a.user_type = ' . Configure::read('UserType.hospital');
        $sql .= $where ;
        $sql .= '   order by ';
        $sql .= '     b.facility_name asc ';
        $sql .= '     , a.mst_role_id asc ';
        
        $this->db_export_csv($sql , "ユーザ一覧", 'passwordlist');
    }
    

    private function getRoleBy($roleTypeId, $editionTypeId) {
        $result = $this->MstRole->find('first', array(
                'conditions' => array(
                        'MstRole.mst_role_type_id' => $roleTypeId,
                        'MstRole.mst_edition_id' => $editionTypeId,
                ),
                'recursive' => -1,
        ));
        return $result['MstRole']['id'];
    }
    
}
