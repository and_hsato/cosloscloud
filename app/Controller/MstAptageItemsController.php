<?php
/**
 * MstAptageItemsController
 * アプテージ側商品
 * @version 1.0.0
 * @since 2015/01/22
 */
class MstAptageItemsController extends AppController {

    /**
     * @var $name
     */
    var $name = 'MstAptageItems';

    /**
     * @var array $uses
     */
    var $uses = array('MstAptageItem');

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'Common');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Common','utils','CsvWriteUtils','CsvReadUtils','AptageDataChecker');

    public function beforeFilter() {
        parent::beforeFilter();
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        //CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }
    }

    /**
     *
     */
    function index (){
        $this->redirect('import_csv');
        exit(0);
    }

    /**
     * CSV取込画面
     */
    function import_csv() {
        $this->setRoleFunction(121); //アプテージデータ取込
    }

    /**
     * CSV取込メイン処理
     */
    function import_csv_result() {
        $this->request->data['now'] = date('Y/m/d H:i:s');

        //更新有無
        $is_update = false;
        if(!empty($this->request->data['is_update'])){
            $is_update = $this->request->data['is_update'];
        }

        //ファイルアップ
        $_upfile = $this->request->data['result']['tmp_name'];
        $_fileName = '../tmp/'.date('YmdHi').'_mst.csv';
        if (is_uploaded_file($_upfile)){
            move_uploaded_file($_upfile, mb_convert_encoding($_fileName, 'UTF-8', 'SJIS-win'));
            chmod($_fileName, 0644);
        }else{
            $this->Session->setFlash('ファイルが読み込めませんでした。CSVファイルを確認してください。', 'growl', array('type'=>'error') );
            $this->redirect('import_csv');
            exit(0);
        }

        //アップロードしたCSVデータ文字列として読み込む
        $_tsvdata = mb_convert_encoding(file_get_contents ($_fileName), 'UTF-8',"SJIS-win");
        unlink($_fileName);

        // 配列変換
        $tsv_array = $this->utils->input_tsv($_tsvdata);

        //CSVカラムとテーブルカラムのマッピング
        $map = array(
            '得意先コード'                => 'facility_code',
            '得意先名'                    => 'facility_name',
            '製品番号'                  => 'item_code',
            '商品名'                      => 'item_name',
            '規格'                        => 'standard',
            'メーカコード'                => 'maker_code',
            'メーカ名'                    => 'maker_name',
            'メーカ品番'                  => 'maker_item_number',
            '入数ランク'                  => 'per_unit_rank',
            '単位コード'                  => 'unit_code',
            '単位名'                      => 'unit_name',
            '入数名'                      => 'per_unit_name',
            '売上単価'                    => 'sales_price',
            '単価契約区分'                => 'contract_type',
            '単価契約区分名'              => 'contract_name',
            '単価契約番号'                => 'contract_number',
            '仕入先コード'                => 'supplier_code',
            '仕入先名'                    => 'supplier_name',
            '売上_実績報告用仕入先コード' => 'sales_supplier_code',
            '売上_実績報告用仕入先名'     => 'sales_supplier_name',
            '短縮製品番号'              => 'short_item_code',
            '相手先製品番号'            => 'dest_unit_code',
            '相手先商品名'                => 'dest_unit_name',
            '相手先規格'                  => 'dest_standard',
            '相手先メーカ名'              => 'dest_maker_name',
            '相手先入数名'                => 'dest_per_unit_name',
            '相手先単位コード'            => 'dest_unit_code',
            '相手先単位名'                => 'dest_unit_name',
            '実績担当者コード'            => 'result_person_code',
            '実績担当者名'                => 'result_person_name',
            '売上_備考'                   => 'sales_note',
            '仕入_実績報告用仕入先コード' => 'purchase_supplier_code',
            '仕入_実績報告用仕入先名'     => 'purchase_supplier_name',
            '仕入単価'                    => 'purchase_price',
            '補償単価'                    => 'compensation_price',
            '補償区分'                    => 'compensation_type',
            '補償区分名'                  => 'compensation_name',
            '仕入_備考'                   => 'purchase_note',
            '遡及処理区分'                => 'retroactive_type',
            '遡及処理区分名'              => 'retroactive_name',
            '開始遡及処理日付'            => 'retroactive_date'
        );

        //UPファイルを配列に入れる
        $mapped_tsv = $this->_tsvMapping($tsv_array, $map);
        if($mapped_tsv == false){
            //マッピングに失敗する場合、項目名が変更されているのでエラーにする
            $this->Session->setFlash('項目名を変更している場合取り込めません。確認してください。', 'growl', array('type'=>'error') );
            $this->redirect('import_csv');
            exit(0);
        }

        $this->_importAptageItemCSV($mapped_tsv, $is_update);
    }

    /**
     *
     * TSVマッピング
     */
    private function _tsvMapping($tsv_array, $map){
        $title_row = $tsv_array[0];
        $mapped_title = array();
        $mapped_tsv = array();
        foreach($title_row as $key => $title){
            if(array_key_exists($title,$map)){
                $mapped_title[$key] = $map[$title];
            }
        }

        //ヘッダ項目名称が変更されている場合NG
        if(count($mapped_title) != count($map)){
            return false;
        }
        foreach($tsv_array as $row_key => $row_data){
            //空項目のみの行は無視
            if( count(array_merge(array(), array_diff($row_data, array("")))) <> 0 ){
                //ヘッダ項目数 < データ項目数だった場合の対応
                $row_data2 = array_slice( $row_data , 0 , count($mapped_title));
                if($row_key != 0 && is_array($row_data2) && count($row_data2) == count($mapped_title)){
                    $mapped_tsv[$row_key] = array_combine($mapped_title,$row_data2);
                }
            }
        }
        return $mapped_tsv;
    }

    /**
     * CSV取込登録処理
     * アプテージ商品情報
     */
    private function _importAptageItemCSV($data, $is_update=false) {
        $now = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        $display_error = 50; //暫定で50件まで
        $ids = array();
        $used_item_codes = array();

        //トランザクション開始
        $this->MstAptageItem->begin();

        try {
            set_time_limit(0);

            if($is_update == false) {
                //更新じゃない場合は、全データを論理削除
                //$this->MstAptageItem->del(array('is_deleted'      => 'false',
                //                                'mst_facility_id' => $this->Session->read('Auth.facility_id_selected') ));
                // 物理削除
                $sql = ' delete from mst_aptage_items where mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
                $this->MstAptageItem->query($sql);
            } else {
                $used_item_codes = $this->MstAptageItem->getItemCodes($this->Session->read('Auth.facility_id_selected'));
            }

            foreach($data as $d) {
                //エラーが多すぎたら抜ける
                if($err_cnt > $display_error) break;
                $count++;
                $is_duplicate = false;
                $save_data = array();

                //得意先コード
                $check = $this->AptageDataChecker->isFacilityCode($d['facility_code']);
                if($check['RESULT'] == false) {
                    $this->request->data['err_result'][] = array($count =>'得意先コード:' . $d['facility_code'] . ', ' . $check['MESSAGE']);
                    $err_cnt++;
                    continue;
                }

                //製品番号
                $check = $this->AptageDataChecker->isItemCode($d['item_code']);
                if($check['RESULT'] == false) {
                    $this->request->data['err_result'][] = array($count =>'製品番号:' . $d['item_code'] . ', ' . $check['MESSAGE']);
                    $err_cnt++;
                    continue;
                }

                //重複データチェック 暫定で、得意先コードとアプテージ製品番号をチェック
                if(isset($used_item_codes[$d['facility_code']])) {
                    if(in_array($d['item_code'], array_keys($used_item_codes[$d['facility_code']]))) {
                        if($is_update == false){
                            $this->request->data['err_result'][] = array($count =>'すでに登録されています。 得意先コード:' . $d['facility_code'] . ', 製品番号:' . $d['item_code']);
                            $err_cnt++;
                            continue;
                        }
                        $is_duplicate = true;
                    }
                }

                //得意先名
                //$check = $this->AptageDataChecker->isFacilityName($d['item_code']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'製品番号:' . $d['item_code'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //仕入先コード
                //$check = $this->AptageDataChecker->isSupplierCode($d['supplier_code']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'仕入先コード:' . $d['supplier_code'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //仕入先名
                //$check = $this->AptageDataChecker->isSupplierName($d['supplier_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'仕入先名:' . $d['supplier_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //商品名
                //$check = $this->AptageDataChecker->isItemName($d['item_code']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'商品名:' . $d['item_code'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //規格
                //$check = $this->AptageDataChecker->isStandard($d['standard']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'規格:' . $d['standard'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //メーカコード
                //$check = $this->AptageDataChecker->isMakerCode($d['maker_code']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'メーカコード:' . $d['maker_code'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //メーカ名
                //$check = $this->AptageDataChecker->isMakerName($d['maker_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'メーカ名:' . $d['maker_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //メーカ品番
                //$check = $this->AptageDataChecker->isMakerItemNumber($d['maker_item_number']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'メーカ品番:' . $d['maker_item_number'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //入数ランク
                //$check = $this->AptageDataChecker->isPerUnitRank($d['per_unit_rank']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'入数ランク:' . $d['per_unit_rank'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //入数名
                //$check = $this->AptageDataChecker->isPerUnitName($d['per_unit_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'入数名:' . $d['per_unit_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //単位コード
                //$check = $this->AptageDataChecker->isUnitCode($d['unit_code']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'単位コード:' . $d['unit_code'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //単位名
                $check = $this->AptageDataChecker->isUnitName($d['unit_name']);
                if($check['RESULT'] == false) {
                    $this->request->data['err_result'][] = array($count =>'単位名:' . $d['unit_name'] . ', ' . $check['MESSAGE']);
                    $err_cnt++;
                    continue;
                }

                //売上価格
                //$check = $this->AptageDataChecker->isSalesPrice($d['sales_price']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'売上価格:' . $d['sales_price'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //仕入価格
                //$check = $this->AptageDataChecker->isPurchasePrice($d['purchase_price']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'仕入価格:' . $d['purchase_price'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //補償価格
                //$check = $this->AptageDataChecker->isCompensationPrice($d['compensation_price']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'補償価格:' . $d['compensation_price'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //補償区分
                //$check = $this->AptageDataChecker->isCompensationType($d['compensation_type']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'補償区分:' . $d['compensation_type'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //補償区分名
                //$check = $this->AptageDataChecker->isCompensationName($d['compensation_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'補償区分名:' . $d['compensation_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //単価契約区分
                //$check = $this->AptageDataChecker->isContractType($d['contract_type']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'単価契約区分:' . $d['contract_type'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //単価契約区分名
                //$check = $this->AptageDataChecker->isContractName($d['contract_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'単価契約区分名:' . $d['contract_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //単価契約番号
                //$check = $this->AptageDataChecker->isContractNumber($d['contract_number']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'単価契約番号:' . $d['contract_number'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //短縮製品番号
                //$check = $this->AptageDataChecker->isShortItemCode($d['short_item_code']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'短縮製品番号:' . $d['short_item_code'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //相手先製品番号
                //$check = $this->AptageDataChecker->isDestItemCode($d['dest_unit_code']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'相手先製品番号:' . $d['dest_unit_code'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //相手先商品名
                //$check = $this->AptageDataChecker->isDestItemName($d['dest_unit_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'相手先商品名:' . $d['dest_unit_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //相手先規格
                //$check = $this->AptageDataChecker->isDestStandard($d['dest_standard']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'相手先規格:' . $d['dest_standard'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //相手先メーカ名
                //$check = $this->AptageDataChecker->isDestMakerName($d['dest_maker_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'相手先メーカ名:' . $d['dest_maker_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //相手先入数名
                //$check = $this->AptageDataChecker->isDestPerUnitName($d['dest_per_unit_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'相手先単位名:' . $d['dest_per_unit_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //相手先単位コード
                //$check = $this->AptageDataChecker->isDestUnitCode($d['dest_unit_code']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'相手先単位コード:' . $d['dest_unit_code'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //相手先単位名
                //$check = $this->AptageDataChecker->isDestUnitName($d['dest_unit_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'相手先単位名:' . $d['dest_unit_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //実績担当者コード
                //$check = $this->AptageDataChecker->isResultPersonCode($d['result_person_code']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'実績担当者コード:' . $d['result_person_code'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //実績担当者名
                //$check = $this->AptageDataChecker->isResultPersonName($d['result_person_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'実績担当者名:' . $d['result_person_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //売上_実績報告用仕入先コード
                //$check = $this->AptageDataChecker->isSalesSupplierCode($d['sales_supplier_code']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'売上_実績報告用仕入先コード:' . $d['sales_supplier_code'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //売上_実績報告用仕入先名
                //$check = $this->AptageDataChecker->isSalesSupplierName($d['sales_supplier_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'売上_実績報告用仕入先名:' . $d['sales_supplier_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //売上_備考
                //$check = $this->AptageDataChecker->isSalesNote($d['sales_note']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'売上_備考:' . $d['sales_note'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //仕入_実績報告用仕入先コード
                //$check = $this->AptageDataChecker->isPurchaseSupplierCode($d['purchase_supplier_code']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'仕入_実績報告用仕入先コード:' . $d['purchase_supplier_code'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //仕入_実績報告用仕入先名
                //$check = $this->AptageDataChecker->isPurchaseSupplierName($d['purchase_supplier_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'仕入_実績報告用仕入先名:' . $d['purchase_supplier_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //仕入_備考
                //$check = $this->AptageDataChecker->isPurchaseNote($d['purchase_note']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'仕入_備考:' . $d['purchase_note'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //遡及処理区分
                //$check = $this->AptageDataChecker->isRetroactiveType($d['retroactive_type']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'遡及処理区分:' . $d['retroactive_type'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //遡及処理区分名
                //$check = $this->AptageDataChecker->isRetroactiveName($d['retroactive_name']);
                //if($check['RESULT'] == false) {
                //    $this->request->data['err_result'][] = array($count =>'遡及処理区分名:' . $d['retroactive_name'] . ', ' . $check['MESSAGE']);
                //    $err_cnt++;
                //    continue;
                //}

                //開始遡及処理日付
                // $check = $this->AptageDataChecker->isRetroactiveName($d['retroactive_date']);
                // if($check['RESULT'] == false) {
                //     $this->request->data['err_result'][] = array($count =>'開始遡及処理日付:' . $d['retroactive_date'] . ', ' . $check['MESSAGE']);
                //     $err_cnt++;
                //     continue;
                // }

                // 問題なければ登録用配列へ
                $save_data = $d;
                $save_data['modifier']        = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']        = $now;
                $save_data['mst_facility_id'] = $this->Session->read('Auth.facility_id_selected');
                
                // 更新？
                if($is_duplicate == true) {
                    $update_facility_code = $save_data['facility_code'];
                    $update_item_code     = $save_data['item_code'];
                    foreach ($save_data as $key => $value) {
                        //シングルクォテーションは更新時に構文エラーとなるのでエスケープする
                        if(preg_match('/\'/', $value)) $value = str_replace("'", "''", $value);

                        $save_data[$key] = "'".$value."'";

                        //NUMERIC型、TIMESTAMP型はNULLで来た時に構文エラーになるのでCASTする
                        if(preg_match("/.+_price$/", $key)) $save_data[$key] = "cast(nullif('".$value."','') AS numeric)";
                        if(preg_match("/.+_date$/", $key))  $save_data[$key] = "cast(nullif('".$value."','') AS timestamp)";
                    }
                    $ret = $this->MstAptageItem->updateAll(
                            $save_data,
                            array('facility_code' => $update_facility_code,
                                  'item_code'     => $update_item_code,
                                  'is_deleted'    => 'false'
                            )
                        );
                    if(!$ret){
                        $err_cnt++;
                    }
                } else {
                    $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
                    $save_data['created'] = $now;
                    $this->MstAptageItem->create();
                    $ret = $this->MstAptageItem->save($save_data);
                    if(!$ret){
                        $err_cnt++;
                    }
                }
            }

            if($err_cnt>0){
                //ロールバック
                $this->MstAptageItem->rollback();
            }else{
                //コミット
                $this->MstAptageItem->commit();
            }
        }catch(Exception $e){
            $this->MstAptageItem->rollback();
            $this->request->data['err_result'][] = array('System' =>'システムエラーが発生しました。もう一度、最初から登録してください。');
            Debugger::log($e);
        }

        $this->set('count' , $count);
        $this->set('display_error', $display_error);
        $this->set('err_result' , $this->request->data['err_result']);
    }

    //日付フォーマットチェック
    private function checkDateFormat($date){
        if(preg_match('/^([0-9]{4})[\/]+([0-9]+)[\/]+([0-9]+)$/', $date, $parts)){
            if(checkdate($parts[2], $parts[3], $parts[1])){
                return true;
            }
        }
        return false;
    }

}

