<?php
/**
 * MstUnitNamesController
 *
 * @version 1.0.0
 * @since 2010/07/13
 */

class MstUnitNamesController extends AppController {
    var $name = 'MstUnitNames';
    
    /**
     *
     * @var array $uses
     */
    var $uses = array('MstUnitName');
    
    /**
     * @var array $components
     */
    var $components = array('CsvWriteUtils');
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;
    
    /**
     * @var MstFacilities
     */
    var $MstRoles;


    public function beforeFilter(){
        parent::beforeFilter();
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }
    
    //初期画面
    function index(){
        $this->redirect('unit_names_list');
    }

    /**
     * unit_names_list
     *
     * 単位名一覧
     */
    function unit_names_list() {
        $this->setRoleFunction(91); //単位名マスタ
        $UnitName_List = array();
        if(isset($this->request->data['search']['is_search'])){
            $limit = $this->_getLimitCount();
            //ユーザ入力値による検索条件の作成--------------------------------------------
            $where = '';
            //単位名コード(完全一致)
            if($this->request->data['search']['internal_code'] != '' ){
                $where .= " and a.internal_code = '" . $this->request->data['search']['internal_code'] . "'";
            }
            //単位名(LIKE検索)
            if($this->request->data['search']['unit_name'] != '' ){
                $where .= " and a.unit_name like '%" . $this->request->data['search']['unit_name'] . "%'";
            }
            //削除も表示
            if(!isset($this->request->data['search']['is_deleted'])){
                $where .= ' and a.is_deleted = false ';
            }
            
            $sql  = ' select ';
            $sql .= '       a.id            as "MstUnitName__id" ';
            $sql .= '     , a.internal_code as "MstUnitName__internal_code" ';
            $sql .= '     , a.unit_name     as "MstUnitName__unit_name" ';
            $sql .= '     , a.order_num     as "MstUnitName__order_num" ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when a.is_deleted = true  ';
            $sql .= "         then '○'  ";
            $sql .= "         else ''  ";
            $sql .= '         end ';
            $sql .= '     )                 as "MstUnitName__is_deleted"  ';
            $sql .= '   from ';
            $sql .= '     mst_unit_names as a  ';
            $sql .= '   where 1=1';
            $sql .= $where;
            $sql .= '   order by ';
            $sql .= '     a.order_num , a.id ';
            
            //検索条件の作成終了---------------------------------------------------------

            $this->set('max' , $this->getMaxCount($sql , 'MstUnitName'));
            $sql .= ' limit ' . $limit;
            
            //SQL実行
            $UnitName_List = $this->MstUnitName->query($sql);
        }
        $this->set('UnitName_List',$UnitName_List);
    }
    
    
    /**
     * 新規登録
     */
    function add() {
        $this->setRoleFunction(91); //単位名マスタ
    }
    
    /**
     * 編集
     */
    function mod() {
        $this->setRoleFunction(91); //単位名マスタ
        $this->Session->write('MstUnitName.readTime',date('Y-m-d H:i:s'));
        //roles_listから引き継いできたidで単位名情報を取得
        $params = array (
            'conditions' => array('MstUnitName.id' => $this->request->data['MstUnitName']['id']),
            'fields'     => array('MstUnitName.id',
                                  'MstUnitName.internal_code',
                                  'MstUnitName.unit_name',
                                  'MstUnitName.order_num',
                                  'MstUnitName.is_deleted'
                                  ),
            'order'      => array('MstUnitName.id'),
            'recursive'  => -1
            );
        
        $UnitName = $this->MstUnitName->find('first', $params);
        $this->request->data = $UnitName;
    }
    
    /**
     * 結果画面
     */
    function result() {
        $unitname_data = array();
        $now = date('Y/m/d H:i:s.u');
        
        //トランザクション開始
        $this->MstUnitName->begin();
        //行ロック（更新時のみ）
        if(isset($this->request->data['MstUnitName']['id'])){
            $this->MstUnitName->query('select * from mst_unit_names as a where a.id = ' .$this->request->data['MstUnitName']['id']. ' for update ');
        }
        
        //保存データの整形
        if(isset($this->request->data['MstUnitName']['id'])){
            //更新の場合
            $unitname_data['MstUnitName']['id']            = $this->request->data['MstUnitName']['id'];
        }else{
            //新規の場合
            $unitname_data['MstUnitName']['creater']   = $this->Session->read('Auth.MstUser.id');
            $unitname_data['MstUnitName']['created']   = $now;
        }
        
        $unitname_data['MstUnitName']['internal_code'] = $this->request->data['MstUnitName']['internal_code'];
        $unitname_data['MstUnitName']['unit_name']     = $this->request->data['MstUnitName']['unit_name'];
        $unitname_data['MstUnitName']['order_num']     = $this->request->data['MstUnitName']['order_num'];
        $unitname_data['MstUnitName']['is_deleted']    = (isset($this->request->data['MstUnitName']['is_deleted'])?true:false);
        $unitname_data['MstUnitName']['modifier']      = $this->Session->read('Auth.MstUser.id');
        $unitname_data['MstUnitName']['modified']      = $now;
        
        //SQL実行
        if(!$this->MstUnitName->save($unitname_data)){
            //ロールバック
            $this->MstUnitName->rollback();
            //エラーメッセージ
            $this->Session->setFlash('単位名情報の登録に失敗しました。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('unit_names_list');
        }
        $this->MstUnitName->commit();
    }


    public function export_csv(){
        App::import('Sanitize');

        //ユーザ入力値による検索条件の作成--------------------------------------------
        $where = '';
        //単位名コード(完全一致)
        if($this->request->data['search']['internal_code'] != '' ){
            $where .= " and a.internal_code = '" . $this->request->data['search']['internal_code'] . "'";
        }
        //単位名(LIKE検索)
        if($this->request->data['search']['unit_name'] != '' ){
            $where .= " and a.unit_name like '%" . $this->request->data['search']['unit_name'] . "%'";
        }
        //削除も表示
        if(!isset($this->request->data['search']['is_deleted'])){
            $where .= ' and a.is_deleted = false ';
        }
        $sql  = ' select ';
        $sql .= '       a.internal_code as "単位コード" ';
        $sql .= '     , a.unit_name     as "単位名" ';
        $sql .= '     , a.order_num     as "表示順" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.is_deleted = true  ';
        $sql .= "         then '○'  ";
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                 as "削除"  ';
        $sql .= '   from ';
        $sql .= '     mst_unit_names as a  ';
        $sql .= '   where 1=1';
        $sql .= $where;
        $sql .= '   order by ';
        $sql .= '     a.order_num , a.id ';
        
        $this->db_export_csv($sql , "単位名一覧", 'unit_names_list');
    }
}
