<?php
class MastersController extends AppController {

    var $name = 'Masters';

    var $uses = array(
        'MstGrandmaster',
        'MstNumber',
        'MstUser'
    );

    var $isDebug = true;

    function index() {
        //権限に応じたメニューを出す
        $sql  = ' select ';
        $sql .= '       d.name ';
        $sql .= '     , d.url ';
        $sql .= '   from ';
        $sql .= '     mst_users as a  ';
        $sql .= '     inner join mst_roles as b  ';
        $sql .= '       on b.id = a.mst_role_id  ';
        $sql .= '     inner join mst_privileges as c  ';
        $sql .= '       on c.mst_role_id = b.id  ';
        $sql .= '     inner join mst_views as d  ';
        $sql .= '       on d.view_no = c.view_no  ';
        $sql .= '     inner join mst_view_headers as e  ';
        $sql .= '       on d.view_header_id = e.id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= "     and e.class = 'masters'";
        $sql .= '     and (  ';
        $sql .= '       (c.readable = true and d.regist_flg = false)  ';
        $sql .= '       or (c.creatable = true and d.regist_flg = true) ';
        $sql .= '     )  ';
        $sql .= '   order by ';
        $sql .= '     d.order_num ';
        
        $result = $this->MstUser->query($sql);
        $this->set('result' ,$result);

    }

    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = "replaceName";
        $this->Auth->allowedActions[] = "updateGrand";
    }
}
?>
