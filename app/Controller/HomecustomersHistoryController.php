<?php
/**
 *
 * @since 2010/10/09
 */
class HomeCustomersHistoryController extends AppController {
    /**
     * @var $name
     */
    var $name = 'HomeCustomersHistory';
    var $uses = array('MstUser',              //ユーザ
                      'MstFacilityRelation',  //施設関係
                      'MstFacility',          //施設
                      'MstFacilityItem',      //施設採用品
                      'MstDepartment',        //部署
                      'MstClass',             //作業区分
                      'TrnShippingHeader',    //出荷ヘッダ
                      'TrnShipping',          //出荷明細
                      'MstItemUnit',          //包装単位
                      'MstDealer',            //販売元
                      'TrnSticker',           //シール
                      'TrnPromiseHeader',     //引当ヘッダ
                      'TrnPromise',           //引当明細
                      'MstTransactionConfig', //仕入設定
                      'MstSalesConfig',       //売上設定
                      'TrnReceivingHeader',   //入荷ヘッダ
                      'TrnReceiving',         //入荷明細
                      'TrnStorageHeader',     //入庫ヘッダ
                      'TrnStorage',           //入庫明細
                      'TrnStock',             //在庫テーブル
                      'TrnClaim',             //請求テーブル（売上データ）
                      'MstUnitName',          //単位名
                      'TrnConsumeHeader',     //消費ヘッダ
                      'TrnConsume',           //消費明細
                      'TrnCloseHeader',       //締め
                      'TrnStickerRecord',     //シール移動履歴
                      'TrnOrderHeader',       //発注ヘッダ
                      'TrnOrder',             //発注明細
                      'MstPrivilege',         //権限
                      'TrnStickerIssue',
                      );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;
    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');
    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Stickers' ,'CsvWriteUtils');

    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';

        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    public function index(){
        $this->setRoleFunction(128); //在宅品履歴
        $this->redirect('search');
    }

    public function search(){
        //得意先を取得
        $this->set('classes', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'18'));
        $this->set('customers', $this->getFacilityList(
                                     $this->Session->read('Auth.facility_id_selected') ,
                                     array(Configure::read('FacilityType.hospital')),
                                     false
                                    )
                                 );
        $searchResult = array();

        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        if(isset($this->request->data['isSearch'])){
            if($this->request->data['isSearch'] == '1'){
                $params = array();
                $params = $this->getParams($this->request->data);

                //limit
                if($this->_getLimitCount() > 0){
                    $params['limit'] = ' limit ' . $this->_getLimitCount();
                }else{
                    $params['limit'] = '';
                }

                //order
                if(null === $this->getSortOrder()){
                    $params['orderby'] = ' order by x.work_no asc ';
                }else{
                    $o = $this->getSortOrder();
                    $params['orderby'] = ' order by ' . $o[0];
                }

                //全件取得
                $ret = $this->getDirectList($params , true);
                $this->set('max' , $ret[0][0]['count']);
                //表示データ取得
                $searchResult = $this->outputFilter($this->getDirectList($params));
            }

        }else{
            $this->request->data['Direct']['work_date_from'] = date('Y/m/d', strtotime('-7 day', time()));
            $this->request->data['Direct']['work_date_to'] = date('Y/m/d', time());
        }


        $this->set('data' , $this->request->data);
        $this->set('searchResult', $searchResult);
        $this->render('search');
    }

    public function confirm(){
        $cond = array();
        foreach($this->request->data['TrnReceivingHeader']['Rows'] as $key=>$val){
            if($val['isSelected'] == '1'){
                $cond['header'][] = "'{$key}'";
            }
        }

        $params = array();
        $params = $this->getParams($this->request->data);

        $res = $this->outputFilter($this->getDirects($cond,$params));

        $reserved = array();//予約済みフラグ
        //予約済みフラグの確認
        $j = 0;
        foreach($res as $row){
            $i = 0;//初期化
            while(isset($row['reserved'][$i])){
                if($row['reserved'][$i] == 1){//予約済みフラグがたっていたら
                    $reserved[$j] = true;
                    break;
                }
                $i++;
            }
            if(!isset($reserved[$j])){
                $reserved[$j] = false;//予約済みフラグがたってなかったら
            }
            $j++;
        }
        $this->set('reserved', $reserved);
        $this->set('searchResult', $res);
        $this->render('confirm');
    }

    public function results(){
        $this->request->data['now'] = date('Y/m/d H:i:s.u');
        $cond = array();
        foreach($this->request->data['TrnReceivingCenter']['Rows'] as $key =>$val){
            if($val['isSelected'] == '1'){
                $cond['detail'][] = $key;
            }
        }

        //取消の更新チェック
        if(true === $this->isUpdated($cond['detail'])){
            $this->Session->setFlash('在宅品情報が更新されています。再度実行してください', 'growl', array('type'=>'error'));
            $this->index();
            return;
        }
        $params = array();
        $params = $this->getDirects($cond,$params);

        //シールの更新チェック
        foreach($params as $row){
            $i = 0;//初期化
            while(isset($row['modified_time'][$i])){
                if($row['modified_time'][$i] > $this->request->data['pre_modified']){//確認画面を開いた時点でシールが更新されたら
                    $this->Session->setFlash('シール情報が更新されています。再度実行してください', 'growl', array('type'=>'error'));
                    $this->index();
                    return;
                }
                $i++;
            }
        }

        //トランザクション開始
        $this->TrnSticker->begin();
        // 行ロック
        $this->TrnReceiving->query(' select * from trn_receivings as a where a.id in (' . join(',',$cond['detail']) . ') for update ');
        
        try {
            $ret = $this->TrnReceiving->query(" select count(*) from trn_receivings as a where a.id in (" . join(',',$cond['detail']) . ") and a.modified > '" . $this->request->data['pre_modified'] . "'");
            if($ret[0][0]['count'] > 0) {
                throw new Exception('更新時間チェックエラー');
            }

            //order
            $this->cancelOrder($params);

            //receiving
            $this->cancelReceiving($params);

            //storage
            $this->cancelStorage($params);

            //shipping
            $this->cancelShipping($params);

            //receipt
            $this->cancelReceipt($params);

            //update
            $this->updateSticker($params);

            //checkUpdate
            $ret = $this->TrnReceiving->query(" select count(*) from trn_receivings as a where a.id in (" . join(',',$cond['detail']) . ") and a.modified <> '" .$this->request->data['now'] . "' and a.modified > '" . $this->request->data['pre_modified'] . "'");
            if($ret[0][0]['count'] > 0) {
                throw new Exception('更新時間チェックエラー');
            }
        } catch (Exception $ex) {
            $this->TrnSticker->rollback();
            throw new Exception($ex->getMessage(). "\n" . print_r($params, true));
        }

        $this->TrnSticker->commit();
        $this->set('results', $params);
        $this->render('results');
    }

    /**
     * 発注取消
     * @param array $params
     */
    private function cancelOrder($params){
        foreach($params as $key => $row){
            $res = $this->TrnOrder->updateAll(array(
                'TrnOrder.is_deleted' => 'true',
                'TrnOrder.modifier' => $this->Session->read('Auth.MstUser.id'),
                'TrnOrder.modified' => "'".$this->request->data['now']."'",
            ),array(
                'TrnOrder.id' => $row['trn_order_id'],
            ),-1);
            if(false === $res){
                throw new Exception('発注明細更新失敗');
            }

            $cnt = $this->TrnOrder->find('count', array(
                'recursive' => -1,
                'conditions' => array(
                    'trn_order_header_id' => $row['trn_order_header_id'],
                    'is_deleted' => false,
                ),
            ));

            if($cnt === 0){
                $res = $this->TrnOrderHeader->updateAll(array(
                    'TrnOrderHeader.is_deleted' => 'true',
                    'TrnOrderHeader.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnOrderHeader.modified'=> "'".$this->request->data['now']."'",
                ),array(
                    'TrnOrderHeader.id' => $row['trn_order_header_id'],
                ),-1);
                if(false === $res){
                    throw new Exception('発注ヘッダ更新失敗');
                }
            }
        }
    }

    /**
     * 入荷取消
     * @param array $res
     */
    private function cancelReceiving($params){
        foreach($params as $key => $row){
            $res = $this->TrnReceiving->updateAll(array(
                'TrnReceiving.is_deleted' => 'true',
                'TrnReceiving.modifier' => $this->Session->read('Auth.MstUser.id'),
                'TrnReceiving.modified' => "'".$this->request->data['now']."'",
            ),array(
                'TrnReceiving.id' => $row['id'],
            ),-1);
            if(false === $res){
                throw new Exception('入荷明細更新失敗');
            }

            $cnt = $this->TrnReceiving->find('count', array(
                'conditions' => array(
                    'is_deleted' => false,
                    'trn_receiving_header_id' => $row['trn_receiving_header_id'],
                ),
                'recursive' => -1,
            ));
            if(0 === $cnt){
                $res = $this->TrnReceivingHeader->updateAll(array(
                    'TrnReceivingHeader.is_deleted' => 'true',
                    'TrnReceivingHeader.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnReceivingHeader.modified' => "'".$this->request->data['now']."'",
                ), array(
                    'TrnReceivingHeader.id' => $row['trn_receiving_header_id'],
                ), -1);
                if(false === $res){
                    throw new Exception('入荷ヘッダ更新失敗');
                }
            }

        }
    }

    /**
     * 入庫取消
     * @param array $res
     */
    private function cancelStorage($params){
        foreach($params as $key => $row){
            $res = $this->TrnStorage->updateAll(array(
                'TrnStorage.is_deleted' => 'true',
                'TrnStorage.modifier' => $this->Session->read('Auth.MstUser.id'),
                'TrnStorage.modified' => "'".$this->request->data['now']."'",
            ),array(
                'TrnStorage.trn_receiving_id' => $row['id'],
            ),-1);
            if(false === $res){
                throw new Exception('入庫明細更新失敗');
            }

            $cnt = $this->TrnStorage->find('count', array(
                'recursive' => -1,
                'conditions' => array(
                    'is_deleted' => false,
                    'trn_receiving_id' => $row['id'],
                ),
            ));
            if(0 === $cnt){
                $res = $this->TrnStorageHeader->updateAll(array(
                    'TrnStorageHeader.is_deleted' => 'true',
                    'TrnStorageHeader.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnStorageHeader.modified' => "'".$this->request->data['now']."'",
                ),array(
                    'TrnStorageHeader.trn_receiving_header_id' => $row['trn_receiving_header_id'],
                ),-1);
                if(false === $res){
                    throw new Exception('入庫ヘッダ更新失敗');
                }
            }
        }
    }

    /**
     * 出荷取消
     * @param array $res
     */
    private function cancelShipping($params){
        foreach($params as $key => $row){
            foreach($row['sticker_id'] as $stickerId){
                $res = $this->TrnShipping->updateAll(array(
                    'TrnShipping.is_deleted' => 'true',
                    'TrnShipping.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnShipping.modified' => "'".$this->request->data['now']."'",
                ), array(
                    'TrnShipping.trn_sticker_id' => $stickerId,
                ), -1);
                if(false === $res){
                    throw new Exception('出荷明細更新失敗');
                }
            }

            $cnt = $this->TrnShipping->find('count', array(
                'recursive' => -1,
                'conditions' => array(
                    'trn_shipping_header_id' => $row['trn_shipping_header_id'],
                    'is_deleted' => false,
                ),
            ));
            if(0 === $cnt){
                $res = $this->TrnShippingHeader->updateAll(array(
                    'TrnShippingHeader.is_deleted' => 'true',
                    'TrnShippingHeader.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnShippingHeader.modified' => "'".$this->request->data['now']."'",
                ), array(
                    'TrnShippingHeader.id' => $row['trn_shipping_header_id'],
                ), -1);
                if(false === $res){
                    throw new Exception('出荷ヘッダ更新失敗');
                }
            }
        }
    }

    /**
     * 受領取消
     * @param array $res
     */
    private function cancelReceipt($params){
        foreach($params as $key => $row){
            foreach($row['sticker_id'] as $stickerId){
                //入荷
                $res = $this->TrnReceiving->updateAll(array(
                    'TrnReceiving.is_deleted' => 'true',
                    'TrnReceiving.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnReceiving.modified' => "'".$this->request->data['now']."'",
                ), array(
                    'TrnReceiving.trn_sticker_id' => $stickerId,
                ), -1);
                if(false === $res){
                    throw new Exception('受領＠入荷明細更新失敗');
                }

                //入庫
                $res = $this->TrnStorage->updateAll(array(
                    'TrnStorage.is_deleted' => 'true',
                    'TrnStorage.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnStorage.modified' => "'".$this->request->data['now']."'",
                ), array(
                    'TrnStorage.trn_sticker_id' => $stickerId,
                ), -1);
                if(false === $res){
                    throw new Exception('受領＠入庫明細更新失敗');
                }

                if($this->Session->read('Auth.Config.D2Consumes') == '0' ){
                    //消費
                    $res = $this->TrnConsume->updateAll(array(
                        'TrnConsume.is_deleted' => 'true',
                        'TrnConsume.modifier' => $this->Session->read('Auth.MstUser.id'),
                        'TrnConsume.modified' => "'".$this->request->data['now']."'",
                        ), array(
                            'TrnConsume.trn_sticker_id' => $stickerId,
                            ), -1);
                    if(false === $res){
                        throw new Exception('受領＠消費更新失敗');
                    }
                }
            }

            //入荷ヘッダ
            $cnt = $this->TrnReceiving->find('count', array(
                'conditions' => array(
                    'is_deleted' => false,
                    'trn_receiving_header_id' => $row['customer_receiving_header_id'],
                ),
                'recursive' => -1,
            ));
            if(0 === $cnt){
                $res = $this->TrnReceivingHeader->updateAll(array(
                    'TrnReceivingHeader.is_deleted' => 'true',
                    'TrnReceivingHeader.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnReceivingHeader.modified' => "'".$this->request->data['now']."'",
                ), array(
                    'TrnReceivingHeader.id' => $row['customer_receiving_header_id'],
                ), -1);
                if(false === $res){
                    throw new Exception('受領＠入荷ヘッダ更新失敗');
                }
            }

            //入庫ヘッダ
            $cnt = $this->TrnStorage->find('count', array(
                'recursive' => -1,
                'conditions' => array(
                    'trn_storage_header_id' => $row['trn_storage_header_id'],
                    'is_deleted' => false,
                ),
            ));
            if(0 === $cnt){
                $res = $this->TrnStorageHeader->updateAll(array(
                    'TrnStorageHeader.is_deleted' => 'true',
                    'TrnStorageHeader.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnStorageHeader.modified' => "'".$this->request->data['now']."'",
                ), array(
                    'TrnStorageHeader.id' => $row['trn_storage_header_id'],
                ), -1);
                if(false === $res){
                    throw new Exception('受領＠入庫ヘッダ更新失敗');
                }
            }

            if($this->Session->read('Auth.Config.D2Consumes') == '0') {
                //消費ヘッダ
                $cnt = $this->TrnConsume->find('count', array(
                    'recursive' => -1,
                    'conditions' => array(
                        'is_deleted' => false,
                        'trn_consume_header_id' => $row['trn_consume_header_id'],
                        ),
                    ));
                if(0 === $cnt){
                    $res = $this->TrnConsumeHeader->updateAll(array(
                        'TrnConsumeHeader.is_deleted' => 'true',
                        'TrnConsumeHeader.modifier' => $this->Session->read('Auth.MstUser.id'),
                        'TrnConsumeHeader.modified' => "'".$this->request->data['now']."'",
                        ), array(
                            'TrnConsumeHeader.id' => $row['trn_consume_header_id'],
                            ), -1);
                    if(false === $res){
                        throw new Exception('受領＠消費ヘッダ更新失敗');
                    }
                }
            }
        }
    }

    /**
     * シール情報更新
     * @param array $res
     */
    private function updateSticker($params){
        //シール
        foreach($params as $key => $row){
            foreach($row['sticker_id'] as $stickerId){
                //シール
                $res = $this->TrnSticker->updateAll(array(
                    'TrnSticker.is_deleted' => 'true',
                    'TrnSticker.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnSticker.modified' => "'".$this->request->data['now']."'",
                ), array(
                    'TrnSticker.id' => $stickerId,
                ), -1);
                if(false === $res){
                    throw new Exception('シール更新失敗');
                }
                //移動履歴
                $res = $this->TrnStickerRecord->updateAll(array(
                    'TrnStickerRecord.is_deleted' => 'true',
                    'TrnStickerRecord.modifier' => $this->Session->read('Auth.MstUser.id'),
                    'TrnStickerRecord.modified' => "'".$this->request->data['now']."'",
                ), array(
                    'TrnStickerRecord.trn_sticker_id' => $stickerId,
                ), -1);
                if(false === $res){
                    throw new Exception('シール移動履歴更新失敗');
                }
            }

            //センター仕入取消
            $res = $this->TrnClaim->updateAll(array(
                'TrnClaim.is_deleted' => 'true',
                'TrnClaim.modifier' => $this->Session->read('Auth.MstUser.id'),
                'TrnClaim.modified' => "'".$this->request->data['now']."'",
            ), array(
                'TrnClaim.trn_receiving_id' => $row['id'],
            ), -1);
            if(false === $res){
                throw new Exception('センター仕入情報更新失敗');
            }

            if($this->Session->read('Auth.Config.D2Consumes') == '0' ){
                //売上更新
                foreach($row['sales_claim_id'] as $claimId){
                    $res = $this->TrnClaim->updateAll(array(
                        'TrnClaim.is_deleted' => 'true',
                        'TrnClaim.modifier' => $this->Session->read('Auth.MstUser.id'),
                        'TrnClaim.modified' => "'".$this->request->data['now']."'",
                        ), array(
                            'TrnClaim.id' => $claimId,
                            ), -1);
                    if(false === $res){
                        throw new Exception('売上情報更新失敗');
                    }
                }
            }
        }
    }

    public function report($type){
        switch($type){
            case 'order':
                break;
            case 'invoice':
                break;
        }
    }


    /**
     * 更新されているかどうか
     * @param array $cond
     * @return boolean
     */
    private function isUpdated($cond){
        $cnt = $this->TrnReceiving->find('count', array(
            'recursive' => -1,
            'conditions' => array(
                'TrnReceiving.id' => $cond,
                'TrnReceiving.is_deleted' => true,
//                'TrnReceiving.modified > ' => $this->request->data['pre_modified'],
            ),
        ));
        return $cnt > 0;
    }
    
    /**
     * 在宅品明細情報取得
     * @param $cond
     * @param $params
     */
    private function getDirects($cond,$params){
        $where = '';
        if(isset($cond['header'])){
            $where = ' AND TrnReceivingHeader.id in (' . join(',', $cond['header']) . ')';
        }
        if(isset($cond['detail'])){
            $where = ' AND TrnReceivingCenter.id in (' . join(',', $cond['detail']) . ')';
        }

        App::import('Sanitize');

        if(isset($params['where'])){
            foreach($params['where'] as $key => $val){
                if(!preg_match('/LIKE|<|>|( in)/i', $key)){
                    $where .= sprintf(" AND %s = '%s' ", $key, Sanitize::escape($val));
                }elseif(preg_match('/( in)/i', $key) > 0){
                    $where .= sprintf(" AND %s %s ", $key, Sanitize::escape($val));
                }else{
                    $where .= sprintf(" AND %s '%s' ", $key, Sanitize::escape($val));
                }
            }
        }
        if(isset($params['where_sticker'])){
            $val = Sanitize::escape($params['where_sticker']);
            $where .= " AND (TrnSticker.facility_sticker_no ILIKE '{$val}' OR TrnSticker.hospital_sticker_no ILIKE '{$val}' ";
            $where .= "  OR  TrnReceivingCustomer.facility_sticker_no ILIKE '{$val}' OR TrnReceivingCustomer.hospital_sticker_no ILIKE '{$val}') ";
        }

        $sql  = ' select ';
        $sql .= '       trnreceivingcenter.id ';
        $sql .= '     , trnreceivingcenter.trn_receiving_header_id ';
        $sql .= '     , trnreceivingcustomer.work_no ';
        $sql .= "     , to_char(trnreceivingcustomer.work_date , 'YYYY/mm/dd')  as work_date";
        $sql .= '     , mstfacilityitem.item_name ';
        $sql .= '     , mstfacilityitem.item_code ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when mstitemunit.per_unit = 1  ';
        $sql .= '         then mstunitname.unit_name  ';
        $sql .= "         else mstunitname.unit_name || '(' || mstitemunit.per_unit || mstperunitname.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                                       as packing_name ';
        $sql .= '     , trnsticker.transaction_price ';
        $sql .= '     , trnsticker.id                                         as trn_sticker_id ';
        $sql .= '     , trnsticker.lot_no ';
        $sql .= '     , trnsticker.sale_claim_id';
        $sql .= '     , coalesce(  ';
        $sql .= '       trnshipping.hospital_sticker_no ';
        $sql .= '       , trnsticker.hospital_sticker_no ';
        $sql .= '     )                                                       as hospital_sticker_no ';
        $sql .= '     , trnsticker.has_reserved ';
        $sql .= "     , to_char(trnsticker.modified, 'YYYY/MM/DD HH24:MI:SS') as modified ";
        $sql .= '     , trnreceivingcustomer.work_type ';
        $sql .= '     , mstuser.user_name ';
        $sql .= '     , customer.facility_name                                as customer_name ';
        $sql .= '     , mstdepartmentcustomer.department_name ';
        $sql .= '     , mstfacilityitem.standard ';
        $sql .= '     , mstdealer.dealer_name ';
        $sql .= '     , mstfacilityitem.internal_code ';
        $sql .= '     , trnclaimsales.claim_price                             as sales_price ';
        $sql .= "     , to_char(trnsticker.validated_date,'YYYY/mm/dd')       as validated_date ";
        $sql .= '     , trnreceivingcustomer.recital ';
        $sql .= '     , trnsticker.mst_item_unit_id ';
        $sql .= '     , mstclass.name                                         as work_type_name ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when y.repay_check > 0 then false  ';
        $sql .= '         when (  ';
        $sql .= '           trnclaimcenter.is_deleted = true  ';
        $sql .= '           or trnclaimcustomer.is_deleted = true  ';
        $sql .= '           or trnclaimcenter.stocking_close_type = 2  ';
        $sql .= '           or trnclaimcenter.sales_close_type = 2  ';
        $sql .= '           or trnclaimcenter.facility_close_type = 2  ';
        $sql .= '           or trnclaimcustomer.stocking_close_type = 2  ';
        $sql .= '           or trnclaimcustomer.sales_close_type = 2  ';
        $sql .= '           or trnclaimcustomer.facility_close_type = 2 ';
        $sql .= '         )  ';
        $sql .= '         then false  ';
        $sql .= '         when (  ';
        $sql .= '           x.updatable = true  ';
        $sql .= '           and trnclaimcenter.stocking_close_type < 2  ';
        $sql .= '           and trnclaimcenter.sales_close_type < 2  ';
        $sql .= '           and trnclaimcenter.facility_close_type < 2  ';
        $sql .= '           and trnclaimcustomer.stocking_close_type < 2  ';
        $sql .= '           and trnclaimcustomer.sales_close_type < 2  ';
        $sql .= '           and trnclaimcustomer.facility_close_type < 2 ';
        $sql .= '         )  ';
        $sql .= '         then true  ';
        $sql .= '         when (  ';
        $sql .= '           x.updatable = false  ';
        $sql .= '           and (  ';
        $sql .= '             trnclaimcenter.stocking_close_type > 0  ';
        $sql .= '             or trnclaimcenter.sales_close_type > 0  ';
        $sql .= '             or trnclaimcenter.facility_close_type > 0  ';
        $sql .= '             or trnclaimcustomer.stocking_close_type > 0  ';
        $sql .= '             or trnclaimcustomer.sales_close_type > 0  ';
        $sql .= '             or trnclaimcustomer.facility_close_type > 0 ';
        $sql .= '           ) ';
        $sql .= '         )  ';
        $sql .= '         then false  ';
        $sql .= '         when (  ';
        $sql .= '           trnclaimcenter.stocking_close_type = 0  ';
        $sql .= '           and trnclaimcenter.sales_close_type = 0  ';
        $sql .= '           and trnclaimcenter.facility_close_type = 0  ';
        $sql .= '           and trnclaimcustomer.stocking_close_type = 0  ';
        $sql .= '           and trnclaimcustomer.sales_close_type = 0  ';
        $sql .= '           and trnclaimcustomer.facility_close_type = 0 ';
        $sql .= '         )  ';
        $sql .= '         then true  ';
        $sql .= '         end ';
        $sql .= '     )                                                       as cancancel ';
        $sql .= '     , trnreceivingcenter.trn_order_id ';
        $sql .= '     , trnorder.trn_order_header_id ';
        $sql .= '     , trnreceivingcenter.department_id_to                   as center_department_id ';
        $sql .= '     , trnclaimcustomer.id                                   as trn_claim_customer_id ';
        $sql .= '     , trnshipping.trn_shipping_header_id ';
        $sql .= '     , trnconsume.trn_consume_header_id ';
        $sql .= '     , trnreceivingheader.id                                 as customer_receiving_header_id ';
        $sql .= '     , trnstorage.trn_storage_header_id  ';
        $sql .= '   from ';
        $sql .= '     trn_shippings as trnshipping ';
        $sql .= '     left join trn_stickers as trnsticker   ';
        $sql .= '       on trnsticker.id = trnshipping.trn_sticker_id  ';
        $sql .= '     left join trn_receivings as trnreceivingcustomer  ';
        $sql .= '       on trnreceivingcustomer.id = trnsticker.receipt_id  ';
        $sql .= '     left join trn_receiving_headers as trnreceivingheader  ';
        $sql .= '       on trnreceivingheader.id = trnreceivingcustomer.trn_receiving_header_id  ';
        $sql .= '     left join mst_departments as mstdepartmentcustomer  ';
        $sql .= '       on trnshipping.department_id_to = mstdepartmentcustomer.id  ';
        $sql .= '     left join mst_facilities as customer  ';
        $sql .= '       on mstdepartmentcustomer.mst_facility_id = customer.id  ';
        $sql .= '     left join mst_users as mstuser  ';
        $sql .= '       on trnsticker.modifier = mstuser.id  ';
        $sql .= '     left join mst_item_units as mstitemunit  ';
        $sql .= '       on trnsticker.mst_item_unit_id = mstitemunit.id  ';
        $sql .= '     left join mst_unit_names as mstunitname  ';
        $sql .= '       on mstitemunit.mst_unit_name_id = mstunitname.id  ';
        $sql .= '     left join mst_unit_names as mstperunitname  ';
        $sql .= '       on mstitemunit.per_unit_name_id = mstperunitname.id  ';
        $sql .= '     left join mst_facility_items as mstfacilityitem  ';
        $sql .= '       on mstitemunit.mst_facility_item_id = mstfacilityitem.id  ';
        $sql .= '     left join mst_dealers as mstdealer  ';
        $sql .= '       on mstfacilityitem.mst_dealer_id = mstdealer.id  ';
        $sql .= '     left join mst_classes as mstclass  ';
        $sql .= '       on trnreceivingcustomer.work_type = mstclass.id  ';
        $sql .= '     left join trn_receivings as trnreceivingcenter  ';
        $sql .= '       on trnsticker.trn_receiving_id = trnreceivingcenter.id  ';
        $sql .= '     left join trn_claims as trnclaimcenter  ';
        $sql .= '       on trnclaimcenter.trn_receiving_id = trnreceivingcenter.id  ';
        $sql .= "       and trnclaimcenter.is_stock_or_sale = '1' ";
        $sql .= '     left join trn_claims as trnclaimcustomer  ';
        $sql .= '       on trnclaimcustomer.trn_receiving_id = trnreceivingcustomer.id  ';
        $sql .= "       and trnclaimcustomer.is_stock_or_sale = '1' ";
        $sql .= '     left join trn_orders as trnorder  ';
        $sql .= '       on trnreceivingcenter.trn_order_id = trnorder.id  ';
        $sql .= '     left join trn_consumes as trnconsume  ';
        $sql .= '       on trnsticker.id = trnconsume.trn_sticker_id  ';
        $sql .= '     left join trn_storages as trnstorage  ';
        $sql .= '       on trnstorage.trn_receiving_id = trnreceivingcustomer.id  ';
        $sql .= '     left join trn_claims as trnclaimsales';
        $sql .= '       on trnclaimsales.trn_consume_id = trnconsume.id';
        
        $sql .= '    left join ( ';
        $sql .= '      select';
        $sql .= '            a.trn_shipping_header_id';
        $sql .= '          , count(b.trn_repay_header_id) as repay_check ';
        $sql .= '        from';
        $sql .= '          trn_shippings as a ';
        $sql .= '          left join trn_receivings as b ';
        $sql .= '            on b.trn_shipping_id = a.id ';
        $sql .= '        where';
        $sql .= '          a.shipping_type = ' . Configure::read('ClassesType.PayDirectly');
        $sql .= '          and a.is_deleted = false ';
        $sql .= '          and b.is_deleted = false ';
        $sql .= '          and b.trn_repay_header_id is not null ';
        $sql .= '        group by';
        $sql .= '          a.trn_shipping_header_id';
        $sql .= '    ) as y';
        $sql .= '    on';
        $sql .= '    y.trn_shipping_header_id = trnshipping.trn_shipping_header_id ';
        
        $sql .= '     cross join (  ';
        $sql .= '       select ';
        $sql .= '             c.updatable  ';
        $sql .= '         from ';
        $sql .= '           mst_users as a  ';
        $sql .= '           left join mst_roles as b  ';
        $sql .= '             on a.mst_role_id = b.id  ';
        $sql .= '           left join mst_privileges as c  ';
        $sql .= '             on c.mst_role_id = b.id  ';
        $sql .= '         where ';
        $sql .= '           a.id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '           and c.view_no = 53 ';
        $sql .= '     ) as x  ';
        $sql .= '   where ';
        $sql .= '     trnsticker.trade_type = ' .Configure::read('ClassesType.PayDirectly');
        $sql .= '   and trnshipping.trn_shipping_header_id is not null ';
        $sql .=$where ;
        $sql .= '   order by ';
        $sql .= '     trnsticker.id asc ';

        $res = $this->TrnSticker->query($sql);
       
        $result = array();
        foreach($res as $row){
            if(false === isset($result[$row[0]['id']])){
                $result[$row[0]['id']]                   = $row[0];
                $result[$row[0]['id']]['sticker_no']     = array($row[0]['hospital_sticker_no']);
                $result[$row[0]['id']]['reserved']       = array($row[0]['has_reserved']);
                $result[$row[0]['id']]['modified_time']  = array($row[0]['modified']);
                $result[$row[0]['id']]['sticker_id']     = array($row[0]['trn_sticker_id']);
                $result[$row[0]['id']]['sales_claim_id'] = array($row[0]['sale_claim_id']);
            }else{
                $result[$row[0]['id']]['sticker_no'][]     = $row[0]['hospital_sticker_no'];
                $result[$row[0]['id']]['reserved'][]       = $row[0]['has_reserved'];
                $result[$row[0]['id']]['modified_time'][]  = $row[0]['modified'];
                $result[$row[0]['id']]['sticker_id'][]     = $row[0]['trn_sticker_id'];
                $result[$row[0]['id']]['sales_claim_id'][] = $row[0]['sale_claim_id'];
            }
        }
        return $result;
    }


    /**
     * 在宅品受領検索(一覧)
     * @param array $params
     * @access private
     * @return array
     */
    private function getDirectList($params , $count=false){
        App::import('Sanitize');
        $where = '';
        if(isset($params['where'])){
            foreach($params['where'] as $key => $val){
                if(!preg_match('/LIKE|<|>|( in)/i', $key)){
                    $where .= sprintf(" AND %s = '%s' ", $key, Sanitize::escape($val));
                }elseif(preg_match('/( in)/i', $key) > 0){
                    $where .= sprintf(" AND %s %s ", $key, Sanitize::escape($val));
                }else{
                    $where .= sprintf(" AND %s '%s' ", $key, Sanitize::escape($val));
                }
            }
        }
        if(isset($params['where_sticker'])){
            $val = Sanitize::escape($params['where_sticker']);
            $where .= " AND (TrnSticker.facility_sticker_no ILIKE '{$val}' OR TrnSticker.hospital_sticker_no ILIKE '{$val}' ";
            $where .= "  OR  TrnReceivingCustomer.facility_sticker_no ILIKE '{$val}' OR TrnReceivingCustomer.hospital_sticker_no ILIKE '{$val}') ";
        }
        


        $sql  = ' select ';
        $sql .= '       id ';
        $sql .= '     , work_no ';
        $sql .= "     , to_char(work_date,'YYYY/mm/dd') as work_date";
        $sql .= '     , supplier_name ';
        $sql .= '     , customer_name ';
        $sql .= '     , department_name ';
        $sql .= '     , user_name ';
        $sql .= "     , to_char(created,'YYYY/mm/dd HH24:MI:SS') as created ";
        $sql .= '     , recital ';
        $sql .= '     , detail_count ';
        $sql .= '     , trn_shipping_header_id ';
        $sql .= '     , trn_order_header_id ';
        $sql .= '     , count(*) as total_count  ';
        $sql .= '   from ';
        $sql .= '     (  ';
        
        $sql .= 'SELECT TrnReceivingHeader.id';
        $sql .= '      ,TrnReceivingCustomer.work_no';
        $sql .= '      ,TrnReceivingCustomer.work_date';
        $sql .= '      ,Supplier.facility_name AS supplier_name';
        $sql .= '      ,Customer.facility_name AS customer_name';
        $sql .= '      ,MstDepartmentCustomer.department_name';
        $sql .= '      ,MstUser.user_name';
        $sql .= '      ,TrnReceivingHeader.created';
        $sql .= '      ,TrnReceivingHeader.recital';
        $sql .= '      ,TrnOrderHeader.detail_count';
        $sql .= '      ,TrnShipping.trn_shipping_header_id';
        $sql .= '      ,TrnOrder.trn_order_header_id';
        $sql .= '  FROM trn_shippings AS TrnShipping';
        $sql .= '  LEFT JOIN trn_receivings AS TrnReceivingCenter ON TrnShipping.id = TrnReceivingCenter.trn_shipping_id';
        $sql .= '  LEFT JOIN trn_stickers as TrnSticker ON TrnSticker.id = TrnShipping.trn_sticker_id ';
        $sql .= '  LEFT JOIN trn_consumes AS TrnConsume ON TrnConsume.id = TrnSticker.trn_consume_id ';
        $sql .= '  LEFT JOIN trn_orders AS TrnOrder ON TrnSticker.trn_order_id = TrnOrder.id';
        $sql .= '  LEFT JOIN trn_order_headers AS TrnOrderHeader ON TrnOrderHeader.id = TrnOrder.trn_order_header_id';
        $sql .= '  LEFT JOIN trn_receivings AS TrnReceivingCustomer ON TrnReceivingCustomer.id = TrnSticker.receipt_id';
        $sql .= '  LEFT JOIN trn_receiving_headers AS TrnReceivingHeader ON TrnReceivingHeader.id = TrnReceivingCustomer.trn_receiving_header_id';
        $sql .= '  LEFT JOIN mst_departments AS MstDepartmentSupplier ON TrnOrder.department_id_to = MstDepartmentSupplier.id';
        $sql .= '  LEFT JOIN mst_facilities AS Supplier ON MstDepartmentSupplier.mst_facility_id = Supplier.id';
        $sql .= '  LEFT JOIN mst_departments AS MstDepartmentCustomer ON TrnShipping.department_id_to = MstDepartmentCustomer.id';
        $sql .= '  LEFT JOIN mst_facilities AS Customer ON MstDepartmentCustomer.mst_facility_id = Customer.id';
        $sql .= '  LEFT JOIN mst_users AS MstUser ON TrnReceivingCenter.creater = MstUser.id';
        $sql .= '  LEFT JOIN mst_item_units AS MstItemUnit ON TrnSticker.mst_item_unit_id = MstItemUnit.id ';
        $sql .= '  LEFT JOIN mst_facility_items AS MstFacilityItem ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id ';
        $sql .= '  LEFT JOIN mst_dealers AS MstDealer ON MstFacilityItem.mst_dealer_id = MstDealer.id ';
        $sql .= ' INNER JOIN mst_facility_relations AS MstFacilityRelation ON MstFacilityRelation.partner_facility_id = Customer.id and MstFacilityRelation.mst_facility_id = ' .$this->Session->read('Auth.facility_id_selected') ;
        $sql .= ' INNER JOIN mst_user_belongings AS MstUserBelonging ON MstUserBelonging.mst_facility_id = Customer.id and MstUserBelonging.mst_user_id = ' .$this->Session->read('Auth.MstUser.id') . ' and MstUserBelonging.is_deleted = false ';
        $sql .= ' WHERE TrnShipping.shipping_type = ' . Configure::read('ClassesType.PayDirectly');
        $sql .= ' AND TrnShipping.trn_shipping_header_id is not null ';
        $sql .= ' AND TrnReceivingCenter.trn_repay_header_id is null ';
        $sql .= ' AND TrnReceivingHeader.id is not null ';
        $sql .= ' AND TrnConsume.use_type = ' . Configure::read('UseType.home'); // 消費区分：在宅のみ
        $sql .= $where;
        $sql .= ' GROUP BY ';
        $sql .= '       TrnReceivingHeader.id';
        $sql .= '      ,TrnReceivingCustomer.work_no';
        $sql .= '      ,TrnReceivingCustomer.work_date';
        $sql .= '      ,TrnReceivingCustomer.recital';
        $sql .= '      ,TrnOrderHeader.detail_count';
        $sql .= '      ,Supplier.facility_name';
        $sql .= '      ,Customer.facility_name';
        $sql .= '      ,MstDepartmentCustomer.department_name';
        $sql .= '      ,MstUser.user_name';
        $sql .= '      ,TrnReceivingHeader.created';
        $sql .= '      ,TrnReceivingHeader.recital';
        $sql .= '      ,TrnShipping.trn_shipping_header_id';
        $sql .= '      ,TrnOrder.trn_order_header_id';
        $sql .= '      ,TrnOrder.id';

        $sql .= ' ) as x ';
        $sql .= '  group by ';
        $sql .= '    id ';
        $sql .= '    , work_no ';
        $sql .= '    , work_date ';
        $sql .= '    , supplier_name ';
        $sql .= '    , customer_name ';
        $sql .= '    , department_name ';
        $sql .= '    , user_name ';
        $sql .= '    , created ';
        $sql .= '    , recital ';
        $sql .= '    , detail_count ';
        $sql .= '    , trn_shipping_header_id  ';
        $sql .= '    , trn_order_header_id';
        $sql .= $params['orderby'];
        if($count){
            $sql = 'select count(*) as count  from ( ' . $sql . ' ) as x ' ;
            return $this->TrnOrderHeader->query($sql);
        }
        
        $sql .= $params['limit'];

        return $this->TrnOrderHeader->query($sql);
    }

    private function getParams($data = array()){
        $params = array();
        if($this->request->data['Direct']['work_no'] != ''){
            $params['where']['TrnReceivingCenter.work_no LIKE '] = "%{$this->request->data['Direct']['work_no']}%";
        }
        if($this->request->data['Direct']['work_date_from'] != ''){
            $params['where']['TrnReceivingCenter.work_date >= '] = date('Y/m/d', strtotime($this->request->data['Direct']['work_date_from']));
        }
        if($this->request->data['Direct']['work_date_to'] != ''){
            $params['where']['TrnReceivingCenter.work_date < '] = date('Y/m/d', strtotime('1 day', strtotime($this->request->data['Direct']['work_date_to'])));
        }
        if(isset($this->request->data['Direct']['customer_id']) && ($this->request->data['Direct']['customer_id'] != '')){
            $params['where']['Customer.facility_code'] = $this->request->data['Direct']['customer_id'];
        }
        if($this->request->data['Direct']['department_input'] != ''){
            $params['where']['MstDepartmentCustomer.department_code'] = $this->request->data['Direct']['department_input'];
        }
        if($this->request->data['Direct']['internal_code'] != ''){
            $params['where']['MstFacilityItem.internal_code LIKE '] = "%{$this->request->data['Direct']['internal_code']}%";
        }
        if($this->request->data['Direct']['item_code'] != ''){
            $params['where']['MstFacilityItem.item_code LIKE '] = "%{$this->request->data['Direct']['item_code']}%";
        }
        if($this->request->data['Direct']['lot_no'] != ''){
            $params['where']['TrnSticker.lot_no LIKE '] = "%{$this->request->data['Direct']['lot_no']}%";
        }
        if($this->request->data['Direct']['item_name'] != ''){
            $params['where']['MstFacilityItem.item_name LIKE '] = "%{$this->request->data['Direct']['item_name']}%";
        }
        if($this->request->data['Direct']['dealer_name'] != ''){
            $params['where']['MstDealer.dealer_name LIKE '] = "%{$this->request->data['Direct']['dealer_name']}%";
        }
        if($this->request->data['Direct']['work_type'] != ''){
            $params['where']['TrnReceivingCenter.work_type'] = $this->request->data['Direct']['work_type'];
        }
        if($this->request->data['Direct']['standard'] != ''){
            $params['where']['MstFacilityItem.standard LIKE '] = "%{$this->request->data['Direct']['standard']}%";
        }
        if($this->request->data['Direct']['sticker_no'] != ''){
            $params['where_sticker'] = $this->request->data['Direct']['sticker_no'];
        }
        if($this->request->data['Direct']['showDeleted'] != '1'){
            $params['where']['TrnSticker.is_deleted'] = 'false';
        }else{
            $params['where']['TrnSticker.is_deleted in '] = "(true, false)";
        }
        return $params;
    }
    
    public function seal($type){
        App::import('Sanitize');
        $ids = array();
        foreach($this->request->data['TrnReceivingCenter']['Rows'] as $key =>$val){
            if($val['isSelected'] == '1'){
                $ids[] = $key;
            }
        }
        $_hno = array();
        $sql  = ' select ';
        $sql .= '       b.id  ';
        $sql .= '     , b.hospital_sticker_no  ';
        $sql .= '   from ';
        $sql .= '     trn_receivings as a  ';
        $sql .= '     left join trn_stickers as b  ';
        $sql .= '       on  a.id = b.trn_receiving_id  ';
        $sql .= '   where ';
        $sql .= '     a.id in ( ' . join(',',$ids) . ') ';
        $ret = $this->TrnShipping->query($sql);
        foreach($ret as $r ){
            $_hno[] = $r[0]['hospital_sticker_no'];
            $_sid[] = $r[0]['id'];
        }
        
        if($type==1){
            //部署シール
            $order = 'MstDepartment.department_name asc, MstShelfNameTo.code asc';
            $data = $this->Stickers->getHospitalStickers($_hno, $order,2);
            $this->set('sortkey' , '病院情報,センター棚番号,商品ID');
            $this->set('Sticker' , $data);
            $this->layout = 'xml/default';
            $this->render('/stickerissues/hospital_sticker');
            
        }elseif($type==2){
            // コストシールデータ取得
            $records = $this->Stickers->getCostStickers($_sid);
            // 空なら初期化
            if(!$records){ $records = array();}
            
            $this->layout = 'xml/default';
            $this->set('records', $records);
            $this->render('/stickerissues/cost_sticker');
        }
    }
    
    
    /*
     * 在宅品履歴CSV
     */
    function export_csv(){
        $params = array();
        $params = $this->getParams($this->request->data);
        $sql = $this->_getDirectCSV(array(),$params);
        $this->db_export_csv($sql , "在宅品受領履歴", 'search');
    }
    
    private function _getDirectCSV($cond,$params){
        App::import('Sanitize');
        $where = '';
        if(isset($params['where'])){
            foreach($params['where'] as $key => $val){
                if(!preg_match('/LIKE|<|>|( in)/i', $key)){
                    $where .= sprintf(" AND %s = '%s' ", $key, Sanitize::escape($val));
                }elseif(preg_match('/( in)/i', $key) > 0){
                    $where .= sprintf(" AND %s %s ", $key, Sanitize::escape($val));
                }else{
                    $where .= sprintf(" AND %s '%s' ", $key, Sanitize::escape($val));
                }
            }
        }
        if(isset($params['where_sticker'])){
            $val = Sanitize::escape($params['where_sticker']);
            $where .= " AND (TrnSticker.facility_sticker_no ILIKE '{$val}' OR TrnSticker.hospital_sticker_no ILIKE '{$val}' ";
            $where .= "  OR  TrnReceivingCustomer.facility_sticker_no ILIKE '{$val}' OR TrnReceivingCustomer.hospital_sticker_no ILIKE '{$val}') ";
        }
        
        $sql  = ' select ';
        $sql .= '       trnreceivingcustomer.work_no                            as 消費番号';
        $sql .= "     , to_char(trnreceivingcustomer.work_date , 'YYYY/mm/dd')  as 消費日";
        $sql .= "     , customer.facility_name||' / '||mstdepartmentcustomer.department_name ";
        $sql .= '                                                               as "施設　／　部署" ';
        $sql .= '     , mstfacilityitem.internal_code                           as "商品ID"';
        $sql .= '     , mstfacilityitem.item_name                               as 商品名';
        $sql .= '     , mstfacilityitem.item_code                               as 製品番号';
        $sql .= '     , mstfacilityitem.standard                                as 規格';
        $sql .= '     , mstdealer.dealer_name                                   as 販売元名';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when mstitemunit.per_unit = 1  ';
        $sql .= '         then mstunitname.unit_name  ';
        $sql .= "         else mstunitname.unit_name || '(' || mstitemunit.per_unit || mstperunitname.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                                         as 包装単位 ';
        $sql .= '     , trnsticker.transaction_price                            as 仕入単価';
        $sql .= '     , trnsticker.sales_price                                  as 売上単価';
        $sql .= '     , trnsticker.hospital_sticker_no                          as 部署シール';
        $sql .= '     , trnsticker.lot_no                                       as ロット番号';
        $sql .= "     , to_char(trnsticker.validated_date,'YYYY/mm/dd')         as 有効期限";
        $sql .= '     , mstclass.name                                           as 作業区分 ';
        $sql .= '     , mstuser.user_name                                       as 更新者';
        $sql .= '     , trnreceivingcustomer.recital                            as 備考';
        $sql .= '   from ';
        $sql .= '     trn_shippings as trnshipping ';
        $sql .= '     left join trn_stickers as trnsticker   ';
        $sql .= '       on trnsticker.id = trnshipping.trn_sticker_id  ';
        $sql .= '     left join trn_consumes as trnconsume  ';
        $sql .= '       on trnconsume.id = trnsticker.trn_consume_id  ';
        $sql .= '     left join trn_receivings as trnreceivingcustomer  ';
        $sql .= '       on trnreceivingcustomer.id = trnsticker.receipt_id  ';
        $sql .= '     left join trn_receiving_headers as trnreceivingheader  ';
        $sql .= '       on trnreceivingheader.id = trnreceivingcustomer.trn_receiving_header_id  ';
        $sql .= '     left join mst_departments as mstdepartmentcustomer  ';
        $sql .= '       on trnshipping.department_id_to = mstdepartmentcustomer.id  ';
        $sql .= '     left join mst_facilities as customer  ';
        $sql .= '       on mstdepartmentcustomer.mst_facility_id = customer.id  ';
        $sql .= '     left join mst_users as mstuser  ';
        $sql .= '       on trnsticker.modifier = mstuser.id  ';
        $sql .= '     left join mst_item_units as mstitemunit  ';
        $sql .= '       on trnsticker.mst_item_unit_id = mstitemunit.id  ';
        $sql .= '     left join mst_unit_names as mstunitname  ';
        $sql .= '       on mstitemunit.mst_unit_name_id = mstunitname.id  ';
        $sql .= '     left join mst_unit_names as mstperunitname  ';
        $sql .= '       on mstitemunit.per_unit_name_id = mstperunitname.id  ';
        $sql .= '     left join mst_facility_items as mstfacilityitem  ';
        $sql .= '       on mstitemunit.mst_facility_item_id = mstfacilityitem.id  ';
        $sql .= '     left join mst_dealers as mstdealer  ';
        $sql .= '       on mstfacilityitem.mst_dealer_id = mstdealer.id  ';
        $sql .= '     left join mst_classes as mstclass  ';
        $sql .= '       on trnreceivingcustomer.work_type = mstclass.id  ';
        $sql .= '     left join trn_receivings as trnreceivingcenter  ';
        $sql .= '       on trnsticker.trn_receiving_id = trnreceivingcenter.id  ';

        $sql .= '   where ';
        $sql .= '     trnsticker.trade_type = ' .Configure::read('ClassesType.PayDirectly');
        $sql .= '   and trnshipping.trn_shipping_header_id is not null ';
        $sql .= '   and trnconsume.use_type = ' . Configure::read('UseType.home');
        $sql .=$where ;
        $sql .= '   order by ';
        $sql .= '     trnsticker.id asc ';

        return $sql;
    }
}
?>
