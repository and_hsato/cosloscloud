<?php
/**
 * ClosesHistoryController
 *　締め履歴　単位
 * @version 1.0.0
 * @since 2010/10/15
 */
class ClosesHistoryController extends AppController {

  /**
   * @var $name
   */
  var $name = 'ClosesHistory';

    /**
    * @var array $uses
    */
    //var $uses    = array('MstItemUnit');
    var $uses = array(
        'TrnCloseRecord',
        'TrnCloseHeader',
        'TrnClaim',
        'TrnConsume',
        'TrnReceiving',
        'TrnOrder',
        'MstDepartment',
        'MstTransactionConfig',
        'MstFacility',
        'MstItemUnit',
        'MstFacilityItem',
        'TrnAppraisedValue'
    );

    /**
    * @var array $helpers
    */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    /**
    * @var array $components
    */
    var $components = array('RequestHandler');

    /**
    * @var $paginate
    */
    //var $paginate ;

    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
    }

    public function index(){
        $this->setRoleFunction(57); //締め履歴
        $this->redirect('search');
    }

    public function search(){
        $closeTypes = array(
            '1' => '仕入締め',
            '2' => '売上締め',
            '3' => '在庫締め',
        );
        $provisional = array(
            '1' => '仮締め',
            '0' => '本締め',
        );
        $SearchResult = array();
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }

        if(isset($this->request->data['isSearch'])){
            $cond = array();
            if($this->request->data['TrnCloseHeader']['close_type'] != ''){
                $cond['TrnCloseHeader.close_type'] = $this->request->data['TrnCloseHeader']['close_type'];
            }
            if($this->request->data['TrnCloseHeader']['work_date_from'] != ''){
                $cond['TrnCloseHeader.work_date >= '] = date('Y/m/d', strtotime($this->request->data['TrnCloseHeader']['work_date_from'] . '/01'));
            }
            if($this->request->data['TrnCloseHeader']['work_date_to'] != ''){
                $cond['TrnCloseHeader.work_date < '] = date('Y/m/d', strtotime('1 month', strtotime($this->request->data['TrnCloseHeader']['work_date_to'] . '/01')));
            }
            if($this->request->data['MstFacility']['facility_name'] != ''){
                $cond['MstFacility.facility_name LIKE '] = "%{$this->request->data['MstFacility']['facility_name']}%";
            }
            if($this->request->data['TrnCloseHeader']['is_provisional'] != ''){
                $cond['TrnCloseHeader.is_provisional'] = $this->request->data['TrnCloseHeader']['is_provisional'] == '1' ? 'true' : 'false';
            }
            $limit = ($this->_getLimitCount() > 0 ? ' limit ' . $this->_getLimitCount() : '');
            $SearchResult = $this->getCloseRecords($cond , $limit);
        }else{
            $this->request->data['TrnCloseHeader']['work_date_from'] = date('Y/m', strtotime('-1 month', strtotime(date('Y/m/d', time()))));
            $this->request->data['TrnCloseHeader']['work_date_to'] = date('Y/m', time());
        }
        $this->set('SearchResult', $SearchResult);
        $this->set('provisional', $provisional);
        $this->set('closeTypes', $closeTypes);
    }

    public function cancel(){
        $closeTypes = array(
            '1' => '仕入締め',
            '2' => '売上締め',
            '3' => '在庫締め',
        );
        $provisional = array(
            '1' => '仮締め',
            '0' => '本締め',
        );

        $cond = array();
        foreach($this->request->data['TrnCloseHeader']['Rows'] as $key => $val){
            if($val['isSelected'] == '1'){
                $cond[] = $key;
            }
        }
        $header = $this->getMoveHeader($cond);
        $target = $this->getCloseRecords(array(
            'TrnCloseHeader.end_date' => $header['TrnCloseHeader']['end_date'],
            'TrnCloseHeader.is_provisional' => $header['TrnCloseHeader']['is_provisional'] ? 'true' : 'false',
        ));

        //トランザクション開始
        $this->TrnCloseHeader->begin();

        $ids = array();
        //行ロック
        foreach($this->request->data['TrnCloseHeader']['Rows'] as $id => $val){
            $ids[] = $id;
        }
        $this->TrnCloseHeader->query('select * from trn_close_headers as a where a.id in (' .join(',',$ids). ') for update ');

        try {
            //締め各種取消
            $this->doCloseCancel($target);
        } catch (Exception $ex) {
            //ロールバック
            $this->TrnCloseHeader->rollback();
            throw new Exception($ex->getMessage() . "\n" . print_r($target, true));
        }

        //コミット
        $this->TrnCloseHeader->commit();

        $this->set('results', $target);
        $this->set('provisional', $provisional);
        $this->set('closeTypes', $closeTypes);
        $this->render('results');
    }

    private function getMoveHeader($cond){
        return $this->TrnCloseHeader->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'id' => $cond
            ),
        ));
    }

    /**
     * 締め取消処理
     * @access private
     * @param array $target
     */
    private function doCloseCancel($target){
        $now = date('Y/m/d H:i:s');
        foreach($target as $row){
            //移動ヘッダ削除
            $res = $this->TrnCloseHeader->updateAll(array(
                'TrnCloseHeader.is_deleted' => 'true',
                'TrnCloseHeader.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnCloseHeader.modified'   => "'". $now ."'",
            ), array(
                'TrnCloseHeader.id' => $row['id'],
            ), -1);

            if(false === $res){
                throw new Exception('移動ヘッダ更新失敗' . $row['id']);
            }

            //移動明細削除失敗
            $res = $this->TrnCloseRecord->updateAll(array(
                'TrnCloseRecord.is_deleted' => 'true',
                'TrnCloseRecord.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnCloseRecord.modified'   => "'". $now ."'" ,
            ), array(
                'TrnCloseRecord.trn_close_header_id' => $row['id'],
            ), -1);
            if(false === $res){
                throw new Exception('移動明細更新失敗' . $row['id']);
            }

            //在庫評価テーブル削除
            if($row['close_type'] == Configure::read('CloseType.stock')){
                $res = $this->TrnAppraisedValue->updateAll(array(
                    'TrnAppraisedValue.is_deleted' => 'true',
                    'TrnAppraisedValue.modifier'   => $this->Session->read('Auth.MstUser.id'),
                    'TrnAppraisedValue.modified'   => "'". $now ."'",
                ), array(
                    'TrnAppraisedValue.trn_close_header_id' => $row['id'],
                ), -1);
                if(false === $res){
                    throw new Exception('在庫評価テーブル更新失敗');
                }
            }
        }

        $startDate = date('Y/m/d', strtotime($target[0]['start_date']));
        $endDate = date('Y/m/d', strtotime($target[0]['end_date']));

        //入荷更新
        $sql  = "";
        $sql .= ' update trn_receivings set ';
        $sql .= '      stocking_close_type = (case when stocking_close_type = 0 then 0 else stocking_close_type - 1 end) ';
        $sql .= '     ,facility_close_type = (case when facility_close_type = 0 then 0 else facility_close_type - 1 end) ';
        $sql .= '     ,sales_close_type = (case when sales_close_type = 0 then 0 else sales_close_type - 1 end) ';
        $sql .= "     ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "'";
        $sql .= '     ,modified = ' ."'". $now . "'";
        $sql .= ' from ';
        $sql .= '     mst_item_units as MstItemUnit ';
        $sql .= ' where ';
        $sql .= '     trn_receivings.mst_item_unit_id=MstItemUnit.id ';
        $sql .= '     and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and trn_receivings.is_deleted = false ';
        $sql .= "     and trn_receivings.work_date between '{$startDate}' and '{$endDate}' ";

        $res = $this->TrnClaim->query($sql);

        if(false === $res){
            throw new Exception('入荷明細更新失敗' . $sql);
        }

        //消費更新
        $sql  = "";
        $sql .= ' update trn_consumes set ';
        $sql .= '      stocking_close_type = (case when stocking_close_type = 0 then 0 else stocking_close_type - 1 end) ';
        $sql .= '     ,facility_close_type = (case when facility_close_type = 0 then 0 else facility_close_type - 1 end) ';
        $sql .= '     ,sales_close_type = (case when sales_close_type = 0 then 0 else sales_close_type - 1 end) ';
        $sql .= "     ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "'";
        $sql .= '     ,modified = ' . "'" . $now . "'";
        $sql .= ' from ';
        $sql .= '     mst_item_units as MstItemUnit ';
        $sql .= ' where ';
        $sql .= '     trn_consumes.mst_item_unit_id=MstItemUnit.id ';
        $sql .= '     and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and trn_consumes.is_deleted = false ';
        $sql .= " and trn_consumes.work_date between '{$startDate}' and '{$endDate}' ";

        $res = $this->TrnClaim->query($sql);
        if(false === $res){
            throw new Exception('消費明細更新失敗' . $sql);
        }

        //請求更新
        $sql  = "";
        $sql .= ' update trn_claims set ';
        $sql .= '      stocking_close_type = (case when stocking_close_type = 0 then 0 else stocking_close_type - 1 end) ';
        $sql .= '     ,facility_close_type = (case when facility_close_type = 0 then 0 else facility_close_type - 1 end) ';
        $sql .= '     ,sales_close_type = (case when sales_close_type = 0 then 0 else sales_close_type - 1 end) ';
        $sql .= "     ,modifier = '" . $this->Session->read('Auth.MstUser.id') . "'";
        $sql .= '     ,modified = ' . "'" . $now . "'";
        $sql .= ' from ';
        $sql .= '     mst_item_units as MstItemUnit ';
        $sql .= ' where ';
        $sql .= '     trn_claims.mst_item_unit_id=MstItemUnit.id ';
        $sql .= '     and MstItemUnit.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');
        $sql .= "     and trn_claims.claim_date between '{$startDate}' and '{$endDate}' ";
        $sql .= '     and trn_claims.is_deleted = false ';

         $res = $this->TrnClaim->query($sql);
         if(false === $res){
             throw new Exception('請求データ更新失敗' . $sql);
         }
    }

    private function getCloseRecords($cond , $limit = null){
        $where = '';
        App::import('Sanitize');
        foreach($cond as $key => $val){

            if(0 === preg_match('/<|>|LIKE/i', $key)){
                $where .= sprintf(" and %s = '%s' ", $key, Sanitize::escape($val));
            }else{
                $where .= sprintf(" and %s '%s' ", $key, Sanitize::escape($val));
            }
        }
        $order = '';
        if(null === $this->getSortOrder()){
            $order = ' TrnCloseHeader.end_date desc , TrnCloseHeader.close_type desc';
        }else{
            $o = $this->getSortOrder();
            $order = $o[0];
        }

        $sql  = "";
        $sql .= ' select ';
        $sql .= '      TrnCloseHeader.id ';
        $sql .= '     ,TrnCloseHeader.start_date ';
        $sql .= "     ,to_char(TrnCloseHeader.end_date, 'YYYY/MM') as close_month";
        $sql .= '     ,MstFacility.facility_name ';
        $sql .= '     ,MstUser.user_name ';
        $sql .= "     ,to_char(TrnCloseHeader.created, 'YYYY/MM/DD hh24:mi:ss') as work_date ";
        $sql .= '     ,TrnCloseHeader.end_date ';
        $sql .= '     ,TrnCloseHeader.is_provisional ';
        $sql .= '     ,TrnCloseHeader.close_type ';
        $sql .= '     ,TrnCloseHeader.recital ';
        $sql .= ' from ';
        $sql .= '     trn_close_headers as TrnCloseHeader inner join mst_users as MstUser ';
        $sql .= '         on TrnCloseHeader.creater = MstUser.id ';
        $sql .= '     inner join mst_facilities as MstFacility ';
        $sql .= '         on TrnCloseHeader.partner_facility_id = MstFacility.id ';
        $sql .= ' where ';
        $sql .= '     1 = 1 ';
        $sql .= '     and TrnCloseHeader.is_deleted = false ';
        $sql .= '     and TrnCloseHeader.mst_facility_id=' . $this->Session->read('Auth.facility_id_selected');

        $sql .= $where;

        $sql .= ' order by ';
        $sql .= $order;

        if($limit){
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnCloseHeader'));
            $sql .= $limit;
        }
        $res =  $this->TrnCloseHeader->query($sql);
        $result = array();

        $lastClose = $this->TrnCloseHeader->find('first', array(
            'recursive' => -1,
            'fields' => array(
                'is_provisional',
                'end_date',
            ),
            'conditions' => array(
                'close_type' => Configure::read('CloseType.stock'),
                'is_deleted' => false,
                'mst_facility_id' => $this->Session->read('Auth.facility_id_selected'),
            ),
            'group' => array(
                'is_provisional',
                'end_date',
            ),
            'order' => array(
                'TrnCloseHeader.end_date desc ',
                'TrnCloseHeader.is_provisional'
                )
        ));

        foreach($res as $row){
            $r = $row[0];
            //在庫締めの場合
            if($row[0]['close_type'] == Configure::read('CloseType.stock')){
                if($row[0]['end_date'] != $lastClose['TrnCloseHeader']['end_date']){
                    $r['canCancel'] = false;
                }else{
                    if(count($lastClose) === 2 || $lastClose['TrnCloseHeader']['is_provisional'] == false){
                        //本締めまで終わってる場合
                        $r['canCancel'] = (false === $row[0]['is_provisional']);
                    }else{
                        $r['canCancel'] = (true === $row[0]['is_provisional']);
                    }
                }
            }else{
                $r['canCancel'] = false;
            }
            $result[] = $r;
        }

        return $result;
    }


}
