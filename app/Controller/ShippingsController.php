<?php
/**
 * ShippingsController
 * 出荷
 * @version 1.0.0
 */
class ShippingsController extends AppController {

    /**
     * @var $name
     */
    var $name = 'Shippings';

    /**
     * @var array $uses
     */
    var $uses = array('TrnSticker',
                      'TrnStickerIssue',
                      'TrnStickerIssueHeader',
                      'TrnStickerRecord',
                      'MstFacility',
                      'MstFacilityItem',
                      'MstClass',
                      'MstUserBelonging',
                      'MstDepartment',
                      'MstUser',
                      'MstShelfName',
                      'MstFixedCount',
                      'MstInsuranceClaimDepartment',
                      'MstPrivilege',
                      'TrnShipping',
                      'TrnShippingHeader',
                      'TrnCloseHeader',
                      'TrnPromiseHeader',
                      'TrnPromise',
                      'TrnFixedPromise',
                      'MstTransactionConfig',
                      'MstSalesConfig',
                      'TrnReceivingHeader',
                      'TrnReceiving',
                      'TrnStorageHeader',
                      'TrnStorage',
                      'TrnStock',
                      'TrnClaim',
                      'MstUnitName',
                      'TrnConsumeHeader',
                      'TrnConsume',
                      'TrnCloseHeader',
                      'TrnOrderHeader',
                      'TrnOrder',
                      );


    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker','common');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Common','Barcode','Stickers','CsvWriteUtils','CsvReadUtils', 'TemporaryItem');

    /**
     * 機能別の2桁の番号
     * @var int $work_no_model
     */
    var $work_no_model = '06';

    /**
     * 納品書に出力しない出荷区分名(管理区分名)
     */
    var $invoice_not_print_type = array('定数品');

    /**
     * エラーメッセージ(商品採用日時切れ)
     */
    var $item_limit_error = '商品採用日時を過ぎています。';

    /**
     * エラーメッセージ(有効期限切れ)
     */
    var $validated_limit_error = ' 有効期限切れ ';

    /**
     * エラーメッセージ(有効期限警告)
     */
    var $validated_alert_error = ' 有効期限警告 ';

    /**
     *
     */
    function index (){
    }

    /**
     * 出荷検索
     * @param
     * @return
     */
    function add() {
        $this->setRoleFunction(11); //出荷照合
        $this->request->data['TrnShippingHeader']['work_date'] = date('Y/m/d');
        $this->set('facility_list', $this->getFacilityList( $this->Session->read('Auth.facility_id_selected') ,
                                                                 array(Configure::read('FacilityType.hospital'))
                                                                 ));

        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));
    }

    function add_check2(){
        // 施設コードから施設IDを取得
        $this->request->data['TrnShippingHeader']['facilityId'] = $this->getFacilityId($this->request->data['TrnShippingHeader']['facilityCode'] ,
                                                                             Configure::read('FacilityType.hospital'));
        // 部署コードから部署IDを取得
        $this->request->data['TrnShippingHeader']['departmentId'] = $this->getDepartmentId($this->request->data['TrnShippingHeader']['facilityId'],
                                                                                  Configure::read('DepartmentType.hospital'),
                                                                                  $this->request->data['TrnShippingHeader']['departmentCode']
                                                                                  );
    }
    
    function add_confirm2 () {
        // 読み込んだシール一覧
        $sql  = ' select ';
        $sql .= '     a.id ';
        $sql .= '   , a.facility_sticker_no ';
        $sql .= '   , a.hospital_sticker_no ';
        $sql .= '   , a.lot_no ';
        $sql .= "   , to_char(a.validated_date,'YYYY/mm/dd') as validated_date";
        $sql .= '   , a.mst_department_id ';
        $sql .= '   , a.mst_item_unit_id ';
        $sql .= '   , a.has_reserved ';
        $sql .= '   , a.is_deleted ';
        $sql .= '   , a.quantity ';
        $sql .= '   , c.internal_code ';
        $sql .= '   , c.item_name ';
        $sql .= '   , c.item_code ';
        $sql .= '   , c.standard ';
        $sql .= '   , d.dealer_name';
        $sql .= '   , c.jan_code ';
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when b.per_unit = 1  ';
        $sql .= '       then b1.unit_name  ';
        $sql .= "       else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '       end ';
        $sql .= '   )                            as unit_name  ';
        $sql .= '    , coalesce(a.sales_price,e.sales_price) as sales_price ';
        $sql .= "    , ''                        as status";
        $sql .= "    , ''                        as fixed_promise_id";
        $sql .= ' from ';
        $sql .= '   trn_stickers as a  ';
        $sql .= '   left join mst_item_units as b  ';
        $sql .= '     on b.id = a.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as b1  ';
        $sql .= '     on b1.id = b.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as b2  ';
        $sql .= '     on b2.id = b.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as c  ';
        $sql .= '     on c.id = b.mst_facility_item_id  ';
        $sql .= '   left join mst_dealers as d  ';
        $sql .= '     on d.id = c.mst_dealer_id  ';
        $sql .= '   left join mst_sales_configs as e ';
        $sql .= '     on e.mst_item_unit_id = a.mst_item_unit_id';
        $sql .= '    and e.partner_facility_id = ' . $this->request->data['TrnShippingHeader']['facilityId'];
        $sql .= '    and e.is_deleted = false ';
        $sql .= "    and e.start_date <= '" . $this->request->data['TrnShippingHeader']['work_date'] . "'";
        $sql .= "    and e.end_date > '" . $this->request->data['TrnShippingHeader']['work_date'] . "'";
        $sql .= ' where ';
        $sql .= "   a.facility_sticker_no in ('" . join("','",explode(',',$this->request->data['codez'])) .  "') ";
        
        $center_sticker = $this->TrnSticker->query($sql);
        
        // 引当データ取得
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , a.remain_count ';
        $sql .= '     , a.mst_item_unit_id  ';
        $sql .= '   from ';
        $sql .= '     trn_fixed_promises as a  ';
        $sql .= '   where ';
        $sql .= '     a.department_id_from = ' . $this->request->data['TrnShippingHeader']['departmentId'];
        $sql .= '     and a.is_deleted = false  ';
        $sql .= '     and a.remain_count > 0 ';
        
        $fixed_promise = $this->TrnStickerIssue->query($sql);
        
        // センター倉庫部署ID取得
        $center_department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') , array(Configure::read('DepartmentType.warehouse')));
        
        foreach($center_sticker as &$c){
            if($c[0]['quantity'] == 0){
                // 数量が０
                $c[0]['status'] = '数量がありません。';
            }else if($c[0]['is_deleted'] == true ){
                // 削除済み
                $c[0]['status'] = '削除済みのシールです。';
            }else if($c[0]['has_reserved']){
                // 予約済み
                $c[0]['status'] = '予約中のシールです。';
            }else if($c[0]['mst_department_id'] != $center_department_id ){
                // 部署が違う
                $c[0]['status'] = '施設が異なります。';
            }
            
            // 引当データと、比較
            foreach($fixed_promise as &$f){
                if($c[0]['mst_item_unit_id'] == $f[0]['mst_item_unit_id']
                   && $f[0]['remain_count'] > 0 ){
                    $c[0]['fixed_promise_id'] = $f[0]['id'];
                    $f[0]['remain_count']--;
                    break;
                }
            }
            unset($h);
            // 引当なし
            if($c[0]['fixed_promise_id'] == ""){
                $c[0]['status'] = '引当なし';
            }
        }
        unset($f);

        // 読み込み順に並べなおす
        $result = array();

        foreach(explode(',',$this->request->data['codez']) as $r ){
            $ret = array();
            foreach($center_sticker as $c){
                if($c[0]['facility_sticker_no'] === $r){
                    $ret['id']                  = $c[0]['id'];
                    $ret['facility_sticker_no'] = $c[0]['facility_sticker_no'];
                    $ret['hospital_sticker_no'] = $c[0]['hospital_sticker_no'];
                    $ret['lot_no']              = $c[0]['lot_no'];
                    $ret['validated_date']      = $c[0]['validated_date'];
                    $ret['internal_code']       = $c[0]['internal_code'];
                    $ret['item_name']           = $c[0]['item_name'];
                    $ret['item_code']           = $c[0]['item_code'];
                    $ret['standard']            = $c[0]['standard'];
                    $ret['jan_code']            = $c[0]['jan_code'];
                    $ret['dealer_name']         = $c[0]['dealer_name'];
                    $ret['unit_name']           = $c[0]['unit_name'];
                    $ret['sales_price']         = $c[0]['sales_price'];
                    $ret['fixed_promise_id']    = $c[0]['fixed_promise_id'];
                    $ret['status']              = $c[0]['status'];
                    break;
                }
            }
            
            //該当シールなし
            if($ret == array()){
                $ret['id']                  = '';
                $ret['facility_sticker_no'] = $r;
                $ret['hospital_sticker_no'] = '';
                $ret['lot_no']              = '';
                $ret['validated_date']      = '';
                $ret['internal_code']       = '';
                $ret['item_name']           = '';
                $ret['item_code']           = '';
                $ret['standard']            = '';
                $ret['jan_code']            = '';
                $ret['dealer_name']         = '';
                $ret['unit_name']           = '';
                $ret['sales_price']         = '';
                $ret['fixed_promise_id']    = '';
                $ret['status']              = '無効なシール';
            }
            $result[] = $ret;
        }
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Shipping.token' , $token);
        $this->request->data['Shipping']['token'] = $token;
        
        $this->set('classes_enabled', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));
        $this->set('result' , $result);
    }
    
    /**
     * 臨時出荷登録
     */
    function ex_add () {
        $this->setRoleFunction(12); //臨時出荷
        $this->request->data['TrnShippingHeader']['work_date'] = date('Y/m/d');
        $this->set('facility_list', $this->getFacilityList( $this->Session->read('Auth.facility_id_selected') ,
                                                                 array(Configure::read('FacilityType.hospital'))
                                                                 ));

        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));
    }

    /**
     * 臨時出荷 シール読み込み
     */
    function ex_add_input_sticker () {
        // セッション処理
        $this->Session->write('ExShipping.input_type','INPUT_STICKER');

        // 施設コードから施設IDを取得
        $this->request->data['TrnShippingHeader']['facilityId'] = $this->getFacilityId($this->request->data['TrnShippingHeader']['facilityCode'] ,
                                                                             Configure::read('FacilityType.hospital'));
        // 部署コードから部署IDを取得
        $this->request->data['TrnShippingHeader']['departmentId'] = $this->getDepartmentId($this->request->data['TrnShippingHeader']['facilityId'],
                                                                                  Configure::read('DepartmentType.hospital'),
                                                                                  $this->request->data['TrnShippingHeader']['departmentCode']
                                                                                  );
        
        // 締めチェック
        $this->closedCheck('ex_add', $this->request->data['TrnShippingHeader']['work_date'], null, $this->request->data['TrnShippingHeader']['facilityCode']);
    }

    /**
     * 臨時出荷確認
     */
    function ex_add_confirm () {
        // セッション処理
        $this->set('input_type' ,$this->Session->read('ExShipping.input_type'));
        $this->Session->write('ExShipping.readTime',date('Y-m-d H:i:s'));
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));

        /* CSV取り込みの場合 */
        /* シール読み込みの場合 */
        // TODO 川鉄シール対応はとりあえず無視
        // TODO CSV取り込みはとりえず無視 そもそも必要性があるか？
        // TODO セット品無視
        
        // ループ開始
        // シール番号から、商品情報を取得
        $result = array();
        $i = 0;
        foreach(explode(',',substr($this->request->data['codez'],0,-1)) as $barcbode){
            $r = $this->getExShippingData($barcbode);
            if(empty($r)){
                //シールが存在しない場合ダミーで埋める
                $dummy = array(
                    'TrnShipping' => array(
                        'id'                  => null,
                        'internal_code'       => '',
                        'item_name'           => '該当なし',
                        'item_code'           => '' ,
                        'standard'            => '' ,
                        'unit_name'           => '',
                        'dealer_name'         => '',
                        'facility_sticker_no' => $barcbode,
                        'hospital_sticker_no' => '',
                        'is_check'            => false,
                        'alert'               => '無効なシールです',
                        'validated_date'      => '',
                        'lot_no'              => '',
                        'sales_price'         => '',
                        )
                    );
                $result[$i] = $dummy;
            }else{
                //シール情報を一覧に移す
                $result[$i] = $r[0];
            }
            $i++;
        }

        $this->set('result' , $result);
        $this->set('input_type' ,$this->Session->read('ExShipping.input_type'));

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('ExShipping.token' , $token);
        $this->request->data['ExShipping']['token'] = $token;
    }

    /**
     * 臨時出荷結果
     */
    function ex_add_result () {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['ExShipping']['token'] === $this->Session->read('ExShipping.token')) {
            $now = date('Y/m/d H:i:s');
            $this->Session->delete('ExShipping.token');
            $result = array();
            // トランザクション開始
            $this->TrnSticker->begin();
            // 行ロック
            $this->TrnSticker->query(' select * from trn_stickers as a where a.id in ( ' . join(',',$this->request->data['TrnShipping']['id']) .')  for update ' );

            //更新チェックを行う
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $this->request->data['TrnShipping']['id']). ')';
            $sql .= "     and a.modified > '" . $this->request->data['ExShipping']['time'] ."'";

            $ret = $this->TrnSticker->query($sql);

            if($ret[0][0]['count']> 0 ){
                $this->TrnSticker->rollback();
                $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }
            
            $work_no = $this->setWorkNo4Header( date('Ymd') , "28");
            // 倉庫部署ID取得
            $center_department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') , array(Configure::read('DepartmentType.warehouse')));
            // 出荷先部署ID取得
            $hospital_department_id = $this->request->data['TrnShippingHeader']['departmentId'];
            // 病院施設コード
            $hospital_facility_code = $this->request->data['TrnShippingHeader']['facilityCode'];
            // 出荷ヘッダ
            $TrnShippingHeader = array(
                'TrnShippingHeader'=> array(
                    'work_no'            => $work_no,
                    'work_date'          => $this->request->data['TrnShippingHeader']['work_date'],
                    'work_type'          => $this->request->data['TrnShippingHeader']['classId'],
                    'recital'            => $this->request->data['TrnShippingHeader']['recital'],
                    'shipping_type'      => Configure::read('ShippingType.exshipping'),
                    'shipping_status'    => Configure::read('ShippingStatus.loaded'), //受領済
                    'department_id_from' => $center_department_id,
                    'department_id_to'   => $hospital_department_id,
                    'detail_count'       => count($this->request->data['TrnShipping']['id']),
                    'is_deleted'         => false,
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'created'            => $now,
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'modified'           => $now,
                    )
                );
            
            $this->TrnShippingHeader->create();
            // SQL実行
            if (!$this->TrnShippingHeader->save($TrnShippingHeader)) {
                $this->TrnSticker->rollback();
                $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }
            
            $TrnShippingHeaderId = $this->TrnShippingHeader->getLastInsertID();
            $this->request->data['TrnShipping']['trn_shipping_header_id'][0] = $TrnShippingHeaderId;

            // 受領ヘッダ作成
            $TrnReceivingHeader = array(
                'TrnReceivingHeader'=> array(
                    'work_no'                => $work_no,
                    'work_date'              => $this->request->data['TrnShippingHeader']['work_date'],
                    'recital'                => $this->request->data['TrnShippingHeader']['recital'],
                    'receiving_type'         => Configure::read('ReceivingType.receipt'),
                    'receiving_status'       => Configure::read('ReceivingStatus.stock'),
                    'department_id_from'     => $center_department_id,
                    'department_id_to'       => $hospital_department_id,
                    'detail_count'           => count($this->request->data['TrnShipping']['id']),
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'trn_shipping_header_id' => $TrnShippingHeaderId,//出荷ヘッダ参照キー
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'modified'               => $now,
                    )
                );
            
            $this->TrnReceivingHeader->create();
            // SQL実行
            if (!$this->TrnReceivingHeader->save($TrnReceivingHeader)) {
                $this->TrnSticker->rollback();
                $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }
            
            $TrnReceivingHeaderId = $this->TrnReceivingHeader->getLastInsertID();
            if($this->Session->read('Auth.Config.ExShippingConsumes') == '0' ){
                // 消費ヘッダ作成
                $TrnConsumeHeader = array(
                    'TrnConsumeHeader'=> array(
                        'work_no'            => $work_no,
                        'work_date'          => $this->request->data['TrnShippingHeader']['work_date'],
                        'recital'            => $this->request->data['TrnShippingHeader']['recital'],
                        'use_type'           => Configure::read('UseType.temporary'),
                        'mst_department_id'  => $hospital_department_id,
                        'detail_count'       => count($this->request->data['TrnShipping']['id']),
                        'is_deleted'         => false,
                        'creater'            => $this->Session->read('Auth.MstUser.id'),
                        'created'            => $now,
                        'modifier'           => $this->Session->read('Auth.MstUser.id'),
                        'modified'           => $now,
                        )
                    );
                
                $this->TrnConsumeHeader->create();
                // SQL実行
                if (!$this->TrnConsumeHeader->save($TrnConsumeHeader)) {
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                $TrnConsumeHeaderId = $this->TrnConsumeHeader->getLastInsertID();
            }
            
            // 必要情報取得
            $sql  = ' select ';
            $sql .= '       a.id                   as "TrnSticker__id"';
            $sql .= '     , a.mst_item_unit_id     as "TrnSticker__mst_item_unit_id"';
            $sql .= '     , a.facility_sticker_no  as "TrnSticker__facility_sticker_no"';
            $sql .= '     , a.hospital_sticker_no  as "TrnSticker__hospital_sticker_no"';
            $sql .= '     , a.quantity             as "TrnSticker__quantity"';
            $sql .= '     , a.trn_order_id         as "TrnSticker__trn_order_id"';
            $sql .= '     , a.transaction_price    as "TrnSticker__transaction_price"';
            $sql .= '     , a.sales_price          as "TrnSticker__sales_price"';
            $sql .= '     , a.trn_storage_id       as "TrnSticker__trn_storage_id"';
            $sql .= '     , b.id                   as "TrnSticker__center_stock_id" ';
            $sql .= '     , a.sale_claim_id        as "TrnSticker__sale_claim_id"';
            $sql .= '     , d.buy_flg              as "TrnSticker__buy_flg"';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '     left join trn_stocks as b  ';
            $sql .= '       on b.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '       and b.mst_department_id = a.mst_department_id  ';
            $sql .= '     left join mst_item_units as c ';
            $sql .= '       on c.id = a.mst_item_unit_id ';
            $sql .= '     left join mst_facility_items as d ';
            $sql .= '       on d.id = c.mst_facility_item_id ';
            $sql .= '   where ';
            $sql .= '     a.id in ( ' . join(',',$this->request->data['TrnShipping']['id']) . ' )  ';

            $sticker_data = $this->TrnSticker->query($sql);
            $seq = 1;
            foreach($sticker_data as $s){
                // 部署シール番号採番
                $hospital_sticker_no = $this->getHospitalStickerNo(date('ymd'),$hospital_facility_code);
                // 出荷明細作成
                $TrnShipping = array(
                    'TrnShipping'=> array(
                        'work_no'                => $work_no,
                        'work_seq'               => $seq,
                        'work_date'              => $this->request->data['TrnShippingHeader']['work_date'],
                        'work_type'              => $this->request->data['TrnShipping']['classId'][$s['TrnSticker']['id']], //作業区分
                        'recital'                => $this->request->data['TrnShipping']['recital'][$s['TrnSticker']['id']], //
                        'shipping_type'          => Configure::read('ShippingType.exshipping'), //臨時出荷
                        'shipping_status'        => Configure::read('ShippingStatus.loaded'), //受領済
                        'mst_item_unit_id'       => $s['TrnSticker']['mst_item_unit_id'],
                        'department_id_from'     => $center_department_id,
                        'department_id_to'       => $hospital_department_id,
                        'quantity'               => $s['TrnSticker']['quantity'],
                        'trn_sticker_id'         => $s['TrnSticker']['id'],
                        'is_deleted'             => false,
                        'creater'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                => $now,
                        'modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'modified'               => $now,
                        'trn_shipping_header_id' => $TrnShippingHeaderId,
                        'facility_sticker_no'    => $s['TrnSticker']['facility_sticker_no'],
                        'hospital_sticker_no'    => $hospital_sticker_no,
                        )
                    );
                
                $this->TrnShipping->create();
                // SQL実行
                if (!$this->TrnShipping->save($TrnShipping)) {
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                $TrnShippingId = $this->TrnShipping->getLastInsertID();
                
                // シール移動履歴作成（臨時出荷）
                $TrnStickerRecord = array(
                    'TrnStickerRecord'=> array(
                        'move_date'           => $this->request->data['TrnShippingHeader']['work_date'],
                        'move_seq'            => $this->getNextRecord($s['TrnSticker']['id']),
                        'record_type'         => Configure::read('RecordType.ExShipping'),
                        'record_id'           => $TrnShippingId,
                        'trn_sticker_id'      => $s['TrnSticker']['id'],
                        'mst_department_id'   => $hospital_department_id,
                        'facility_sticker_no' => $s['TrnSticker']['facility_sticker_no'],
                        'hospital_sticker_no' => $hospital_sticker_no,
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        )
                    );
                $this->TrnStickerRecord->create();
                // SQL実行
                if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                
                // 受領明細作成
                $TrnReceiving = array(
                    'TrnReceiving' => array(
                        'work_no'                 => $work_no,
                        'work_seq'                => $seq,
                        'work_date'               => $this->request->data['TrnShippingHeader']['work_date'],
                        'recital'                 => $this->request->data['TrnShipping']['recital'][$s['TrnSticker']['id']],
                        'receiving_type'          => Configure::read('ReceivingType.receipt'),    //入荷区分:受領による入荷
                        'receiving_status'        => Configure::read('ShippingStatus.loaded'),    //入荷状態（受領済み）
                        'mst_item_unit_id'        => $s['TrnSticker']['mst_item_unit_id'],        //包装単位参照キー
                        'department_id_from'      => $center_department_id,
                        'department_id_to'        => $hospital_department_id,
                        'quantity'                => $s['TrnSticker']['quantity'],                //数量
                        'trade_type'              => Configure::read('ClassesType.Temporary') ,   //臨時品固定
                        'stocking_price'          => $s['TrnSticker']['transaction_price'],       //仕入れ価格
                        'sales_price'             => $s['TrnSticker']['sales_price'],             //売上価格
                        'trn_sticker_id'          => $s['TrnSticker']['id'],                      //シール参照キー
                        'trn_shipping_id'         => $TrnShippingId,                              //出荷参照キー
                        'trn_order_id'            => $s['TrnSticker']['trn_order_id'],            //発注参照キー
                        'trn_receiving_header_id' => $TrnReceivingHeaderId,                       //入荷ヘッダ外部キー
                        'facility_sticker_no'     => $s['TrnSticker']['facility_sticker_no'],
                        'hospital_sticker_no'     => $hospital_sticker_no,
                        'creater'                 => $this->Session->read('Auth.MstUser.id'),
                        'modifier'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                 => $now,
                        'modified'                => $now,
                        )
                    );

                $this->TrnReceiving->create();
                // SQL実行
                if (!$this->TrnReceiving->save($TrnReceiving)) {
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }

                $TrnReceivingId = $this->TrnReceiving->getLastInsertID();
                
                if($this->Session->read('Auth.Config.ExShippingConsumes') == '0' ){
                    // 消費明細
                    $TrnConsume = array(
                        'TrnConsume' => array(
                            'work_no'               => $work_no,
                            'work_seq'              => $seq,
                            'work_date'             => $this->request->data['TrnShippingHeader']['work_date'],
                            'recital'               => $this->request->data['TrnShipping']['recital'][$s['TrnSticker']['id']],
                            'use_type'              => Configure::read('UseType.temporary'), //消費区分：直納品
                            'mst_item_unit_id'      => $s['TrnSticker']['mst_item_unit_id'], //包装単位参照キー
                            'mst_department_id'     => $hospital_department_id,              //部署参照キー
                            'quantity'              => $s['TrnSticker']['quantity'],         //数量
                            'trn_sticker_id'        => $s['TrnSticker']['id'],               //シール参照キー
                            'trn_storage_id'        => $s['TrnSticker']['trn_storage_id'],   //入庫参照キー
                            'trn_consume_header_id' => $TrnConsumeHeaderId,                  //消費ヘッダ外部キー
                            'is_retroactable'       => false,
                            'creater'               => $this->Session->read('Auth.MstUser.id'),
                            'created'               => $now,
                            'modifier'              => $this->Session->read('Auth.MstUser.id'),
                            'modified'              => $now,
                            'is_deleted'            => false,
                            'facility_sticker_no'   => $s['TrnSticker']['facility_sticker_no'],
                            'hospital_sticker_no'   => $hospital_sticker_no,
                            )
                        );
                    
                    $this->TrnConsume->create();
                    // SQL実行
                    if (!$this->TrnConsume->save($TrnConsume)) {
                        $this->TrnSticker->rollback();
                        $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('ex_add');
                    }
                    
                    $TrnConsumeId = $this->TrnConsume->getLastInsertID();

                    // シール移動履歴作成（消費）
                    $TrnStickerRecord = array(
                        'TrnStickerRecord'=> array(
                            'move_date'           => $this->request->data['TrnShippingHeader']['work_date'],
                            'move_seq'            => $this->getNextRecord($s['TrnSticker']['id']),
                            'record_type'         => Configure::read('RecordType.consume'),
                            'record_id'           => $TrnConsumeId,
                            'trn_sticker_id'      => $s['TrnSticker']['id'],
                            'mst_department_id'   => $hospital_department_id,
                            'facility_sticker_no' => $s['TrnSticker']['facility_sticker_no'],
                            'hospital_sticker_no' => $hospital_sticker_no,
                            'is_deleted'          => false,
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now,
                            )
                        );
                    $this->TrnStickerRecord->create();
                    // SQL実行
                    if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                        $this->TrnSticker->rollback();
                        $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('ex_add');
                    }
                }else{
                    // 病院部署在庫増
                    $this->TrnStock->create();
                    $c = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.stock_count' => 'TrnStock.stock_count + ' . $s['TrnSticker']['quantity'],
                            'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'    => "'" . $now . "'"
                            ),
                        array(
                            'TrnStock.id' => $this->getStockRecode($s['TrnSticker']['mst_item_unit_id'] ,
                                                                   $hospital_department_id),
                            ),
                        -1
                        );
                    if(!$c){
                        $this->TrnSticker->rollback();
                        $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('ex_add');
                    }
                }
                
                // 倉庫在庫減
                $this->TrnStock->create();
                $c = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.stock_count' => 'TrnStock.stock_count - ' . $s['TrnSticker']['quantity'],
                        'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'    => "'" . $now . "'"
                        ),
                    array(
                        'TrnStock.id' => $s['TrnSticker']['center_stock_id'],
                        ),
                    -1
                    );
                if(!$c){
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                $TrnClaimId = $s['TrnSticker']['sale_claim_id'];
                // 薬品ではない、かつ、臨時出荷消費が「消費あり」の場合、売上データを作成する
                if($s['TrnSticker']['buy_flg'] == false && $this->Session->read('Auth.Config.ExShippingConsumes') == '0'){
                    // 売上明細
                    $TrnClaimId = $this->createSalesData($s['TrnSticker']['id'],
                                                         $hospital_department_id,
                                                         $this->request->data['TrnShippingHeader']['work_date']
                                                         );
                }
                // シール情報更新（部署ID、出荷ID、受領ID、最終移動日付更新）
                $this->TrnSticker->create();
                $c = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.mst_department_id'   => $hospital_department_id,
                        'TrnSticker.trn_shipping_id'     => $TrnShippingId,
                        'TrnSticker.receipt_id'          => $TrnReceivingId,
                        'TrnSticker.sale_claim_id'       => $TrnClaimId,
                        'TrnSticker.last_move_date'      => "'" . $this->request->data['TrnShippingHeader']['work_date'] . "'",
                        'TrnSticker.hospital_sticker_no' => $hospital_sticker_no,
                        'TrnSticker.trade_type'          => "'" . Configure::read('ClassesType.ExShipping') . "'" , 
                        'TrnSticker.modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'            => "'" . $now . "'"
                        ),
                    array(
                        'TrnSticker.id' => $s['TrnSticker']['id'],
                        ),
                    -1
                    );
                if(!$c){
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                
                
                if($this->Session->read('Auth.Config.ExShippingConsumes') == '0' ){
                    // 消費が有効なら（消費ID、売り上げID、数量減
                    $this->TrnSticker->create();
                    $c = $this->TrnSticker->updateAll(
                        array(
                            'TrnSticker.trn_consume_id' => $TrnConsumeId,
                            'TrnSticker.quantity'       => 0,
                            ),
                        array(
                            'TrnSticker.id' => $s['TrnSticker']['id'],
                            ),
                        -1
                        );
                    if(!$c){
                        $this->TrnSticker->rollback();
                        $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('ex_add');
                    }
                }
                
                $seq++;
                $r = $this->getExShippingData($s['TrnSticker']['facility_sticker_no']);
                $result[] = $r[0];
            }
            
            //コミット直前に更新チェックを行う
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $this->request->data['TrnShipping']['id']). ')';
            $sql .= "     and a.modified > '" . $this->request->data['ExShipping']['time'] ."'";
            $sql .= "     and a.modified <> '" . $now ."'";

            $ret = $this->TrnSticker->query($sql);

            if($ret[0][0]['count']> 0 ){
                $this->TrnSticker->rollback();
                $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }

            // コミット
            $this->TrnSticker->commit();
            $this->Session->write('Shipping.result',$result);
            // 結果表示用データ取得
            $this->set('result' , $result);
            $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));
            $this->set('input_type' ,$this->Session->read('ExShipping.input_type'));
            $this->set('facility_from_id',$this->Session->read('Auth.facility_id_selected'));
            $this->set('user_id',$this->Session->read('Auth.MstUser.id'));
        }else{
            // トランザクショントークン不正
            // リロード or ２度押し
            $this->set('result',$this->Session->read('Shipping.result'));
            $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));
            $this->set('input_type' ,$this->Session->read('ExShipping.input_type'));
            $this->set('facility_from_id',$this->Session->read('Auth.facility_id_selected'));
            $this->set('user_id',$this->Session->read('Auth.MstUser.id'));
        }
    }

    /**
     * 出荷履歴CSV出力
     */
    function export_csv() {
        $this->request->data['TrnShipping']['trn_shipping_header_id']=null;
        //検索
        $where = $this->_getShippingDetailWhere();
        $sql = $this->_getShippingCSV($where);

        $this->db_export_csv($sql , "出荷履歴", '/shippings/history');
    }

    private function _getShippingCSV($where){
        $sql  = ' select ';
        $sql .= '       a.work_no                            as 出荷番号 ';
        $sql .= "     , to_char(a.work_date, 'YYYY/mm/dd')   as 出荷日 ";
        $sql .= "     , i.facility_name||'／'||h.department_name ";
        $sql .= '                                            as "施設／部署名" ';
        $sql .= '     , e.internal_code                      as "商品ID" ';
        $sql .= '     , ( case a.shipping_type ';
        foreach(Configure::read('ShippingTypes') as $key => $val){
            $sql .= "           when " . $key . " then '" . $val ."' ";
        }

        $sql .= '     end )                                       as "管理区分名" ';
        $sql .= '     , e.item_name                               as "商品名" ';
        $sql .= '     , e.standard                                as "規格" ';
        $sql .= '     , e.item_code                               as "製品番号" ';
        $sql .= '     , f.dealer_name                             as "販売元名" ';
        $sql .= "     , ( case when a.quantity = 1 then '' else a.quantity::varchar end ) " ;
        $sql .= '     || (  ';
        $sql .= '       case  ';
        $sql .= '         when d.per_unit = 1  ';
        $sql .= '         then j1.unit_name  ';
        $sql .= "         else j1.unit_name || '(' || d.per_unit || j2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                           as "包装単位名" ';
        $sql .= '     , coalesce( l.unit_price , m.sales_price )  as "販売単価" ';
        $sql .= "     , to_char(c.validated_date, 'YYYY/mm/dd')   as 有効期限";
        $sql .= '     , c.facility_sticker_no                     as "センターシール" ';
        $sql .= '     , c.hospital_sticker_no                     as "部署シール" ';
        $sql .= '     , k.name                                    as "作業区分名" ';
        $sql .= '     , g.user_name                               as "更新者名" ';
        $sql .= '     , a.recital                                 as "備考" ';

        $sql .= '   from ';
        $sql .= '     trn_shippings as a ';
        $sql .= '     left join trn_shipping_headers as b ';
        $sql .= '       on a.trn_shipping_header_id = b.id ';
        $sql .= '     left join trn_stickers as c ';
        $sql .= '       on a.trn_sticker_id = c.id ';
        $sql .= '     left join mst_item_units as d ';
        $sql .= '       on a.mst_item_unit_id = d.id ';
        $sql .= '     left join mst_facility_items as e ';
        $sql .= '       on d.mst_facility_item_id = e.id ';
        $sql .= '       and e.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
        $sql .= '     left join mst_dealers as f ';
        $sql .= '       on f.id = e.mst_dealer_id ';
        $sql .= '     left join mst_users as g ';
        $sql .= '       on g.id = a.modifier ';
        $sql .= '     left join mst_departments as h ';
        $sql .= '       on b.department_id_to = h.id ';
        $sql .= '     left join mst_facilities as i ';
        $sql .= '       on h.mst_facility_id = i.id ';
        $sql .= '     left join mst_unit_names as j1 ';
        $sql .= '       on j1.id = d.mst_unit_name_id ';
        $sql .= '     left join mst_unit_names as j2 ';
        $sql .= '       on j2.id = d.per_unit_name_id ';
        $sql .= '     left join mst_classes as k ';
        $sql .= '       on k.id = a.work_type ';
        $sql .= '     left join trn_claims as l ';
        $sql .= '       on l.id = c.sale_claim_id ';
        $sql .= '      and l.is_deleted = false  ';
        $sql .= '     left join mst_sales_configs as m';
        $sql .= '       on m.mst_item_unit_id = d.id ';
        $sql .= '      and m.start_date <= a.work_date ';
        $sql .= '      and m.end_date > a.work_date ';
        $sql .= '      and m.is_deleted = false  ';
        $sql .= '      and m.partner_facility_id = i.id  ';
        $sql .= '     inner join mst_user_belongings as z  ';
        $sql .= '       on z.mst_facility_id = i.id  ';
        $sql .= '       and z.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '       and z.is_deleted = false  ';
        $sql .= '   where ';
        $sql .= '     a.shipping_type in ('. Configure::read('ShippingType.fixed') .', '. Configure::read('ShippingType.semifixed') .', '. Configure::read('ShippingType.extraordinary') .', '. Configure::read('ShippingType.exconsume') .', '. Configure::read('ShippingType.exshipping') .') ';
        $sql .= '     and e.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
        $sql .= $where;
        $sql .= '   order by ';
        $sql .= '     b.work_no desc , a.id ';

        return $sql;
    }

    /**
     * 出荷履歴検索
     */
    function history() {
        App::import('Sanitize');
        $this->setRoleFunction(13); //出荷履歴
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }

        $result = array();
        $departments = array();//部署セレクト
        //検索ボタン押下
        if(isset($this->request->data['search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            //施設が選択されていたら、部署一覧を取得する
            if($this->request->data['search']['facilityCode'] !== ""){
                $departments = $this->getDepartmentsList($this->request->data['search']['facilityCode']);
            }

            //検索条件追加
            $where = '';
            //出荷番号
            if($this->request->data['search']['work_no'] != '' ){
                $where .= " and a.work_no like '" . $this->request->data['search']['work_no'] . "%'";
            }
            //出荷日FROM
            if($this->request->data['search']['start_date'] != '' ){
                $where .= " and a.work_date >= '" . $this->request->data['search']['start_date'] . "'";
            }
            //出荷日TO
            if($this->request->data['search']['end_date'] != '' ){
                $where .= " and a.work_date <= '" . $this->request->data['search']['end_date'] . "'";
            }
            //取消も表示する
            if( !isset($this->request->data['search']['is_deleted']) ){
                $where .= " and a.is_deleted = false ";
            }
            //出荷先施設
            if($this->request->data['search']['facilityCode'] != '' ){
                $where .= " and i.facility_code = '" . $this->request->data['search']['facilityCode'] . "'";
            }
            //出荷先部署
            if($this->request->data['search']['departmentCode'] != '' ){
                $where .= " and h.department_code = '" . $this->request->data['search']['departmentCode'] . "'";
            }
            //商品ID
            if($this->request->data['search']['internal_code'] != '' ){
                $where .= " and e.internal_code = '" . $this->request->data['search']['internal_code'] . "'";
            }
            //商品名
            if($this->request->data['search']['item_name'] != '' ){
                $where .= " and e.item_name like '%" . $this->request->data['search']['item_name'] . "%'";
            }
            //製品番号
            if($this->request->data['search']['item_code'] != '' ){
                $where .= " and e.item_code like '" . $this->request->data['search']['item_code'] . "%'";
            }
            //規格
            if($this->request->data['search']['standard'] != '' ){
                $where .= " and e.standard like '%" . $this->request->data['search']['standard'] . "%'";
            }
            //販売元
            if($this->request->data['search']['dealer_name'] != '' ){
                $where .= " and f.dealer_name like '%" . $this->request->data['search']['dealer_name'] . "%'";
            }
            //ロット番号
            if($this->request->data['search']['lot_no'] != '' ){
                $where .= " and c.lot_no like '" . $this->request->data['search']['lot_no'] . "%'";
            }
            //シール番号
            if($this->request->data['search']['sticker_no'] != '' ){
                $where .= " and ( c.facility_sticker_no = '" . $this->request->data['search']['sticker_no'] . "'";
                $where .= "    or c.hospital_sticker_no = '" . $this->request->data['search']['sticker_no'] . "'";
                $where .= "    or a.facility_sticker_no = '" . $this->request->data['search']['sticker_no'] . "'";
                $where .= "    or a.hospital_sticker_no = '" . $this->request->data['search']['sticker_no'] . "' ) ";
            }
            //管理区分
            if($this->request->data['search']['shipping_type'] != '' ){
                if($this->request->data['search']['shipping_type'] == Configure::read('ShippingType.extraordinary') ){
                    $where .= " and a.shipping_type in ( " . Configure::read('ShippingType.extraordinary') . " , " . Configure::read('ShippingType.exshipping') . " ) ";
                }else{
                    $where .= " and a.shipping_type = " . $this->request->data['search']['shipping_type'];
                }
            }
            //作業区分
            if($this->request->data['search']['work_type'] != '' ){
                $where .= " and a.work_type = " . $this->request->data['search']['work_type'] ;
            }

            $sql  = ' select ';
            $sql .= '       b.id                                          as "TrnShipping__id" ';
            $sql .= '     , b.work_no                                     as "TrnShipping__work_no" ';
            $sql .= '     , to_char(b.work_date, \'yyyy/mm/dd\')          as "TrnShipping__work_date" ';
            $sql .= '     , to_char(b.created, \'yyyy/mm/dd hh24:mi:ss\') as "TrnShipping__created" ';
            $sql .= '     , b.recital                                     as "TrnShipping__recital" ';
            $sql .= '     , b.detail_count                                as "TrnShipping__detail_count" ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when b.shipping_type = ' . Configure::read('ShippingType.exshipping');
            $sql .= '         then ' . Configure::read('ShippingType.extraordinary') ;
            $sql .= '         else b.shipping_type  ';
            $sql .= '         end ';
            $sql .= '     )                                               as "TrnShipping__shipping_type" ';
            $sql .= '     , sum(  ';
            $sql .= '       case  ';
            $sql .= '         when a.shipping_type != ' . Configure::read('ShippingType.fixed');
            $sql .= '         then 1  ';
            $sql .= '         else 0  ';
            $sql .= '         end ';
            $sql .= '     )                                               as "TrnShipping__type_sum" ';
            $sql .= '     , count(a.id)                                   as "TrnShipping__count" ';
            $sql .= '     , g.user_name                                   as "TrnShipping__user_name" ';
            $sql .= '     , i.facility_name                               as "TrnShipping__facility_name" ';
            $sql .= '     , h.department_name                             as "TrnShipping__department_name"  ';
            $sql .= '   from ';
            $sql .= '     trn_shippings as a ';
            $sql .= '     left join trn_shipping_headers as b ';
            $sql .= '       on a.trn_shipping_header_id = b.id ';
            $sql .= '     left join trn_stickers as c ';
            $sql .= '       on a.trn_sticker_id = c.id ';
            $sql .= '     left join mst_item_units as d ';
            $sql .= '       on a.mst_item_unit_id = d.id ';
            $sql .= '     left join mst_facility_items as e ';
            $sql .= '       on d.mst_facility_item_id = e.id ';
            $sql .= '       and e.mst_facility_id =' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     left join mst_dealers as f ';
            $sql .= '       on f.id = e.mst_dealer_id ';
            $sql .= '     left join mst_users as g ';
            $sql .= '       on g.id = b.creater ';
            $sql .= '     left join mst_departments as h ';
            $sql .= '       on b.department_id_to = h.id ';
            $sql .= '     left join mst_facilities as i ';
            $sql .= '       on h.mst_facility_id = i.id ';
            $sql .= '     inner join mst_user_belongings as z ';
            $sql .= '       on z.mst_facility_id = i.id ';
            $sql .= '       and z.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '       and z.is_deleted = false ';
            $sql .= '   where ';
            $sql .= '     a.shipping_type in ( ';
            $sql .= '                         ' . Configure::read('ShippingType.fixed') . ',';
            $sql .= '                         ' . Configure::read('ShippingType.semifixed') . ',';
            $sql .= '                         ' . Configure::read('ShippingType.extraordinary') . ',';
            $sql .= '                         ' . Configure::read('ShippingType.exconsume') . ',';
            $sql .= '                         ' . Configure::read('ShippingType.exshipping') ;
            $sql .= '     ) ';
            $sql .= '     and e.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= $where;
            $sql .= '   group by ';
            $sql .= '     b.id ';
            $sql .= '     , b.work_no ';
            $sql .= '     , b.work_date ';
            $sql .= '     , b.created ';
            $sql .= '     , b.recital ';
            $sql .= '     , b.detail_count ';
            $sql .= '     , b.shipping_type ';
            $sql .= '     , g.user_name ';
            $sql .= '     , i.facility_name ';
            $sql .= '     , h.department_name  ';
            $sql .= '   order by ';
            if(is_null($this->getSortOrder())){
                $sql .= '     b.work_no desc ';
            }else{
                $sql .= join(',',$this->getSortOrder());
            }
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnShipping'));
            $sql .= '   limit ' . $this->request->data['limit'];

            //表示データ取得
            $result = $this->TrnShipping->query($sql);
            $this->request->data = $data;
        }else{
            //初期検索条件
            $this->request->data['search']['start_date'] = date('Y/m/d', strtotime("-6 day"));
            $this->request->data['search']['end_date']   = date('Y/m/d');
        }

        $this->set('facilities_enabled', $this->getFacilityList( $this->Session->read('Auth.facility_id_selected') ,
                                                                 array(Configure::read('FacilityType.hospital')),
                                                                 false
                                                                 ));
        $this->set('departments', $departments);
        $this->set('classes_enabled', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));
        $this->set('shipping_type_list',Configure::read('ShippingTypes'));

        $this->set('result' , $result);
        $this->render('history');
    }

    /**
     * 詳細表示
     */
    function history_edit () {
        $this->Session->write('ShippingHistory.readTime',date('Y-m-d H:i:s'));
        $this->deleteSortInfo();
        $this->set('shipping_type_list',Configure::read('ShippingTypes'));

        $where = $this->_getShippingDetailWhere();

        $result = $this->getShippingDetail($where);
        $this->set('result', $result);
        
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Shipping.token' , $token);
        $this->request->data['Shipping']['token'] = $token;
    }

    /**
     * 出荷取消結果
     */
    function history_edit_result () {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['Shipping']['token'] === $this->Session->read('Shipping.token')) {
            $this->Session->delete('Shipping.token');
            
            $this->TrnSticker->begin();
            $this->Session->write('ShippingHistory.history_edit_result',$this->request->data);
            $readTime = $this->Session->read('ShippingHistory.readTime');  //画面開いた時間
            $this->request->data['now'] = $now = date('Y/m/d H:i:s.u');


            // 必要情報を取得
            $sql  = 'select ';
            $sql .= '    a.id ';
            $sql .= '  , a.shipping_type ';
            $sql .= '  , b.id                       as trn_sticker_id ';
            $sql .= '  , b.hospital_sticker_no ';
            $sql .= '  , b.facility_sticker_no ';
            $sql .= '  , b.trn_receiving_id ';
            $sql .= '  , b.trn_storage_id ';
            $sql .= '  , coalesce(b.trn_consume_id, j.id) as trn_consume_id';
            $sql .= '  , b.trn_promise_id ';
            $sql .= '  , b.trn_fixed_promise_id';
            $sql .= '  , b.receipt_id ';
            $sql .= '  , coalesce(b.sale_claim_id, i.id) as sale_claim_id ';
            $sql .= '  , d.is_lowlevel ';
            $sql .= '  , e.id                       as hospital_stock_id ';
            $sql .= '  , f.id                       as center_stock_id ';
            $sql .= '  , f.mst_department_id        as center_department_id';
            $sql .= '  , a.quantity ';
            $sql .= '  , d.buy_flg ';
            $sql .= 'from ';
            $sql .= '  trn_shippings as a  ';
            $sql .= '  left join trn_stickers as b  ';
            $sql .= '    on b.id = a.trn_sticker_id  ';
            $sql .= '  left join mst_item_units as c  ';
            $sql .= '    on c.id = a.mst_item_unit_id  ';
            $sql .= '  left join mst_facility_items as d  ';
            $sql .= '    on d.id = c.mst_facility_item_id  ';
            $sql .= '  left join trn_stocks as e  ';
            $sql .= '    on e.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '    and e.mst_department_id = a.department_id_to  ';
            $sql .= '  left join trn_stocks as f  ';
            $sql .= '    on f.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '    and f.mst_department_id = a.department_id_from  ';
            $sql .= '  left join trn_receivings as h  ';
            $sql .= '    on h.id = b.receipt_id  ';
            $sql .= '    and h.is_deleted = false  ';
            $sql .= '  left join trn_claims as i ';
            $sql .= '    on i.trn_shipping_id = a.id ';
            $sql .= "    and i.is_stock_or_sale = '2'";
            $sql .= '  left join trn_consumes as j ';
            $sql .= '    on i.trn_consume_id = j.id ';
            $sql .= '  where a.id in ( ' . implode(",",$this->request->data['TrnShipping']['id']).")";

            $shipping_data = $this->TrnSticker->query($sql);

            //更新時間チェック
            $ret = $this->TrnShipping->query( "select count(*) from trn_shippings as a where a.id in ( " .join(',', $this->request->data['TrnShipping']['id'] ). ") and a.modified > '" . $this->request->data['Shipping']['time'] . "'" );
            if($ret[0][0]['count'] > 0){
                $this->rollbackAndRedirect('history');
            }

            foreach($shipping_data as $d){
                /* 通常品 */
                if($d[0]['shipping_type'] <> Configure::read('ShippingType.exshipping')
                   && $d[0]['shipping_type'] <> Configure::read('ShippingType.exconsume')) {
                    // シール更新
                    $this->TrnSticker->create();
                    if($this->Session->read('Auth.Config.Shipping') == '0'){
                        $hospital_sticker_no = $d[0]['hospital_sticker_no'];
                    }else{
                        $hospital_sticker_no = '';
                    }
                    //SQL実行
                    if (!$this->TrnSticker->updateAll(array(  'TrnSticker.trn_shipping_id'      => null ,
                                                              'TrnSticker.trn_promise_id'       => null ,
                                                              'TrnSticker.sale_claim_id'        => null ,
                                                              'TrnSticker.trn_fixed_promise_id' => null ,
                                                              'TrnSticker.receipt_id'           => null ,
                                                              'TrnSticker.quantity'             => '1' ,
                                                              'TrnSticker.hospital_sticker_no'  => "'" . $hospital_sticker_no . "'",
                                                              'TrnSticker.mst_department_id'    => $d[0]['center_department_id'],
                                                              'TrnSticker.has_reserved'         => 'false',
                                                              'TrnSticker.trade_type'           => Configure::read('ClassesType.Constant'),// 倉庫にある物品は常に定数品
                                                              'TrnSticker.modifier'             => $this->Session->read('Auth.MstUser.id'),
                                                              'TrnSticker.modified'             => "'" . $now . "'"
                                                              )
                                                      ,
                                                      array(
                                                          'TrnSticker.id'  => $d[0]['trn_sticker_id'],
                                                          )
                                                      ,
                                                      -1
                                                      )
                        ) {
                        $this->rollbackAndRedirect('history');
                    }

                    if($this->Session->read('Auth.Config.Shipping') == '1' ){
                        // 部署シール削除
                        $this->TrnStickerIssue->create();
                        //SQL実行
                        if (!$this->TrnStickerIssue->updateAll(array(  'TrnStickerIssue.is_deleted' => 'true',
                                                                       'TrnStickerIssue.modifier'   => $this->Session->read('Auth.MstUser.id'),
                                                                       'TrnStickerIssue.modified'   => "'" . $now . "'"
                                                                       )
                                                               ,
                                                               array(
                                                                   'TrnStickerIssue.hospital_sticker_no'      => $d[0]['hospital_sticker_no'],
                                                                   )
                                                               ,
                                                               -1
                                                               )
                            ) {
                            $this->rollbackAndRedirect('history');
                        }
                        // 引当数量増
                        $this->TrnFixedPromise->create();
                        //SQL実行
                        if (!$this->TrnFixedPromise->updateAll(array(  'TrnFixedPromise.remain_count'         => 'TrnFixedPromise.remain_count + 1 ',
                                                                       'TrnFixedPromise.fixed_promise_status' => Configure::read('PromiseStatus.Matching'),
                                                                       'TrnFixedPromise.modifier'             => $this->Session->read('Auth.MstUser.id'),
                                                                       'TrnFixedPromise.modified'             => "'" . $now . "'"
                                                                       )
                                                               ,
                                                               array(
                                                                   'TrnFixedPromise.id'      => $d[0]['trn_fixed_promise_id'],
                                                                   )
                                                               ,
                                                               -1
                                                               )
                            ) {
                            $this->rollbackAndRedirect('history');
                        }
                    }
                    
                    if($this->Session->read('Auth.Config.Shipping') == '0'){
                        // センター在庫の予約数を変更
                        $this->TrnStock->create();
                        //SQL実行
                        if (!$this->TrnStock->updateAll(array(  'TrnStock.reserve_count' => 'TrnStock.reserve_count -1',
                                                                'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                                                                'TrnStock.modified'      => "'" . $now . "'"
                                                                )
                                                        ,
                                                        array(
                                                            'TrnStock.id'      => $d[0]['center_stock_id'],
                                                            )
                                                        ,
                                                        -1
                                                        )
                            ) {
                            $this->rollbackAndRedirect('history');
                        }
                    }else{
                        if( $this->TemporaryItem->isInclude($this->name, $d[0]['shipping_type']) ){ // 臨時品の場合
                            if( $this->Session->read('Auth.Config.ShippingConsumes') == '0' ) { //臨時品で、消費アリの場合
                                // 消費削除
                                $this->TrnConsume->create();
                                //SQL実行
                                if (!$this->TrnConsume->del(array('TrnConsume.id'=> $d[0]['trn_consume_id']))) {
                                    $this->rollbackAndRedirect('history');
                                }

                                // シール移動履歴(消費)削除
                                $this->TrnStickerRecord->create();
                                //SQL実行
                                if (!$this->TrnStickerRecord->del(array('TrnStickerRecord.record_id'      => $d[0]['trn_consume_id'],
                                                                        'TrnStickerRecord.trn_sticker_id' => $d[0]['trn_sticker_id']
                                                                        ))
                                    ) {
                                    $this->rollbackAndRedirect('history');
                                }
                            }
                            if($d[0]['buy_flg'] == false){
                                // 売上削除
                                $this->TrnClaim->create();
                                //SQL実行
                                if (!$this->TrnClaim->del(array('TrnClaim.id' => $d[0]['sale_claim_id']))) {
                                    $this->rollbackAndRedirect('history');
                                }
                            }
                        }
                        
                        // センター在庫の予約数を変更
                        $this->TrnStock->create();
                        //SQL実行
                        if (!$this->TrnStock->updateAll(array(  'TrnStock.stock_count'   => 'TrnStock.stock_count +1',
                                                                'TrnStock.reserve_count' => 'TrnStock.reserve_count + 1',
                                                                'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                                                                'TrnStock.modified'      => "'" . $now . "'"
                                                                )
                                                        ,
                                                        array(
                                                            'TrnStock.id'      => $d[0]['center_stock_id'],
                                                            )
                                                        ,
                                                        -1
                                                        )
                            ) {
                            $this->rollbackAndRedirect('history');
                        }
                    }
                    if($d[0]['shipping_type'] == Configure::read('ShippingType.semifixed') ){ //準定数品の場合
                        if($d[0]['buy_flg'] == false){
                            //売上削除
                            $this->TrnClaim->create();
                            //SQL実行
                            if (!$this->TrnClaim->del(array('TrnClaim.id' => $d[0]['sale_claim_id']))) {
                                $this->rollbackAndRedirect('history');
                            }
                        }
                    }

                    /* 受領省略状態で、受領IDがある場合処理する */
                    if($this->Session->read('Auth.Config.Shipping') == '1' && !is_null($d[0]['receipt_id'])){
                        //受領削除
                        $this->TrnReceiving->create();
                        //SQL実行
                        if (!$this->TrnReceiving->del(array('TrnReceiving.id' => $d[0]['receipt_id']))) {
                            $this->rollbackAndRedirect('history');
                        }
                        
                        //病院在庫 在庫数▽ 未入荷数△
                        $this->TrnStock->create();
                        $requested_count = $d[0]['quantity'];
                        if($d[0]['shipping_type'] == Configure::read('ShippingType.extraordinary')){
                            // 臨時品の場合、未入荷数は増やさない
                            $requested_count = 0 ;
                        }
                        //SQL実行
                        if (!$this->TrnStock->updateAll(array(  'TrnStock.stock_count'     => 'TrnStock.stock_count -' . $d[0]['quantity'],
                                                                'TrnStock.requested_count' => 'TrnStock.requested_count +' . $requested_count,
                                                                'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                                                'TrnStock.modified'        => "'" . $now . "'"
                                                                )
                                                        ,
                                                        array(
                                                            'TrnStock.id' => $d[0]['hospital_stock_id'],
                                                            )
                                                        ,
                                                        -1
                                                        )
                            ) {
                            $this->rollbackAndRedirect('history');
                        }
                        
                        //要求の受領数▽ 未受領数△
                        $this->TrnPromise->create();
                        //SQL実行
                        if (!$this->TrnPromise->updateAll(array(  'TrnPromise.receipt_count'   => 'TrnPromise.receipt_count -' . $d[0]['quantity'],
                                                                  'TrnPromise.unreceipt_count' => 'TrnPromise.unreceipt_count +' . $d[0]['quantity'],
                                                                  'TrnPromise.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                                                  'TrnPromise.modified'        => "'" . $now . "'"
                                                                )
                                                        ,
                                                        array(
                                                            'TrnPromise.id' => $d[0]['trn_promise_id'],
                                                            )
                                                        ,
                                                        -1
                                                        )
                            ) {
                            $this->rollbackAndRedirect('history');
                        }
                        
                        
                        //シール移動履歴（受領）削除
                        $this->TrnStickerRecord->create();
                        //SQL実行
                        if (!$this->TrnStickerRecord->del(array('TrnStickerRecord.record_id'      => $d[0]['receipt_id'],
                                                                'TrnStickerRecord.trn_sticker_id' => $d[0]['trn_sticker_id']
                                                                ))
                            ) {
                            $this->rollbackAndRedirect('history');
                        }
                    }


                }else{ /* 臨時出荷 */
                    if($d[0]['is_lowlevel'] == true ){ /* 低レベル臨時出荷 */
                        // 低レベル在庫増
                        $this->TrnStock->create();
                        //SQL実行
                        if (!$this->TrnStock->updateAll(array(  'TrnStock.lowlevel_stock_count' => 'TrnStock.lowlevel_stock_count +' . $d[0]['quantity'],
                                                                'TrnStock.modifier'             => $this->Session->read('Auth.MstUser.id'),
                                                                'TrnStock.modified'             => "'" . $now . "'"
                                                                  )
                                                          ,
                                                          array(
                                                              'TrnStock.id' => $d[0]['center_stock_id'],
                                                              )
                                                          ,
                                                          -1
                                                          )
                            ) {
                            $this->rollbackAndRedirect('history');
                        }
                        
                        // 消費削除
                        $this->TrnConsume->create();
                        //SQL実行
                        if (!$this->TrnConsume->del(array('TrnConsume.id' => $d[0]['trn_consume_id']))) {
                            $this->rollbackAndRedirect('history');
                        }

                    }else{ /* 通常品臨時出荷 */
                        // シール移動
                        $this->TrnSticker->create();
                        //SQL実行
                        if (!$this->TrnSticker->updateAll(array(  'TrnSticker.trn_shipping_id'      => null ,
                                                                  'TrnSticker.trn_promise_id'       => null ,
                                                                  'TrnSticker.trn_fixed_promise_id' => null,
                                                                  'TrnSticker.receipt_id'           => null ,
                                                                  'TrnSticker.hospital_sticker_no'  => "''",
                                                                  'TrnSticker.mst_department_id'    => $d[0]['center_department_id'],
                                                                  'TrnSticker.trade_type'           => Configure::read('ClassesType.Constant'),
                                                                  'TrnSticker.quantity'             => '1',
                                                                  'TrnSticker.has_reserved'         => 'false',
                                                                  'TrnSticker.modifier'             => $this->Session->read('Auth.MstUser.id'),
                                                                  'TrnSticker.modified'             => "'" . $now . "'"
                                                                  )
                                                          ,
                                                          array(
                                                              'TrnSticker.id' => $d[0]['trn_sticker_id'],
                                                              )
                                                          ,
                                                          -1
                                                          )
                            ) {
                            $this->rollbackAndRedirect('history');
                        }
                    }

                    // センター在庫増
                    $this->TrnStock->create();
                    //SQL実行
                    if (!$this->TrnStock->updateAll(array(  'TrnStock.stock_count'      => 'TrnStock.stock_count +'.$d[0]['quantity'],
                                                            'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                                            'TrnStock.modified'        => "'" . $now . "'"
                                                            )
                                                    ,
                                                    array(
                                                        'TrnStock.id' => $d[0]['center_stock_id'],
                                                        )
                                                    ,
                                                    -1
                                                    )
                        ) {
                        $this->rollbackAndRedirect('history');
                    }
                    // 臨時品で、同時に消費を行う場合
                    if( ( $this->Session->read('Auth.Config.ShippingConsumes') == '0' && $d[0]['shipping_type'] == Configure::read('ShippingType.extraordinary') ) 
                         || 
                        ( $this->Session->read('Auth.Config.ExShippingConsumes') == '0' && $d[0]['shipping_type'] == Configure::read('ShippingType.exshipping') )
                         ||
                        ( $d[0]['shipping_type'] == Configure::read('ShippingType.exconsume') )
                        ) { 
                        // 消費削除
                        $this->TrnConsume->create();
                        //SQL実行
                        if (!$this->TrnConsume->del(array('TrnConsume.id' => $d[0]['trn_consume_id']))) {
                            $this->rollbackAndRedirect('history');
                        }

                        // シール移動履歴(消費)削除
                        $this->TrnStickerRecord->create();
                        //SQL実行
                        if (!$this->TrnStickerRecord->del(array('TrnStickerRecord.record_id'      => $d[0]['trn_consume_id'],
                                                                'TrnStickerRecord.trn_sticker_id' => $d[0]['trn_sticker_id']
                                                                ))) {
                            $this->rollbackAndRedirect('history');
                        }
                    } else {
                        // 病院在庫▽
                        $this->TrnStock->create();
                        //SQL実行
                        if (!$this->TrnStock->updateAll(array(  'TrnStock.stock_count'     => 'TrnStock.stock_count -' . $d[0]['quantity'],
                                                                'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                                                'TrnStock.modified'        => "'" . $now . "'"
                                                                )
                                                        ,
                                                        array(
                                                            'TrnStock.id' => $d[0]['hospital_stock_id'],
                                                            )
                                                        ,
                                                        -1
                                                        )
                            ) {
                            $this->rollbackAndRedirect('history');
                        }
                        
                    }
                    if($d[0]['buy_flg'] == false){
                        // 売上削除
                        $this->TrnClaim->create();
                        //SQL実行
                        if (!$this->TrnClaim->del(array('TrnClaim.id'=> $d[0]['sale_claim_id']))) {
                            $this->rollbackAndRedirect('history');
                        }
                    }
                    // 受領削除
                    $this->TrnReceiving->create();
                    //SQL実行
                    if (!$this->TrnReceiving->del(array('TrnReceiving.id'=> $d[0]['receipt_id']))) {
                        $this->rollbackAndRedirect('history');
                    }

                    // シール移動履歴(受領)削除
                    $this->TrnStickerRecord->create();
                    //SQL実行
                    if (!$this->TrnStickerRecord->del(array('TrnStickerRecord.record_id'      => $d[0]['receipt_id'],
                                                            'TrnStickerRecord.trn_sticker_id' => $d[0]['trn_sticker_id']))) {
                        $this->rollbackAndRedirect('history');
                    }
                }

                // 出荷明細削除
                $this->TrnShipping->create();
                //SQL実行
                if (!$this->TrnShipping->del(array('TrnShipping.id'=> $d[0]['id']))) {
                    $this->rollbackAndRedirect('history');
                }

                // シール移動履歴(出荷)削除
                $this->TrnStickerRecord->create();
                //SQL実行
                if (!$this->TrnStickerRecord->del(array('TrnStickerRecord.record_id'      => $d[0]['id'],
                                                        'TrnStickerRecord.trn_sticker_id' => $d[0]['trn_sticker_id']
                                                        ))) {
                    $this->rollbackAndRedirect('history');
                }
            }

            // 出荷ヘッダ更新
            //コミット直前に更新時間チェック
            $ret = $this->TrnShipping->query( "select count(*) from trn_shippings as a where a.id in ( " .join(',', $this->request->data['TrnShipping']['id'] ). ") and a.modified <> '" . $this->request->data['now'] ."' and a.modified > '" . $this->request->data['Shipping']['time'] . "'" );
            if($ret[0][0]['count'] > 0){
                $this->rollbackAndRedirect('history');
            }

            // コミット
            $this->TrnSticker->commit();
            $where = '';
            $where .= ' and a.id in ( ' . join(',',$this->request->data['TrnShipping']['id']) . ')';
            $result = $this->getShippingDetail($where);
            $this->Session->write('Shipping.result', $result);
        }else{
            $result = $this->Session->read('Shipping.result');
        }
        $this->set('shipping_type_list',Configure::read('ShippingTypes'));
        $this->set('result', $result);
    }

    /**
     * 更新時間チェック&エラー処理
     * @param string $updateTime
     * @param string $nowTime
     * @return val
     */
    private function checkUpdateTime($updateTime, $nowTime, $url = null){
        if($nowTime < $updateTime){
            $this->Session->setFlash('処理中のデータに更新がかけられました。', 'growl', array('type'=>'error') );
            if(!$url){
                return false;
            } else {
                $this->redirect($url);
                return false;
            }
        }
        return true;
    }

    /**
     * データ保存中のロールバックとリダイレクトを行う
     */
    private function rollbackAndRedirect($url,$message = ''){
        // ロールバック&リダイレクト
        $this->TrnSticker->rollback();
        if($message === ''){
            $this->Session->setFlash('データ保存中にエラーが起こりました', 'growl', array('type'=>'error') );
        } else {
            $this->Session->setFlash($message, 'growl', array('type'=>'error') );
        }
        $this->redirect($url);
    }

    /**
     * 締めチェック
     */
    private function closedCheck($url, $work_date, $facility_id = null, $facility_code = null){
        if($facility_id === null && $facility_code === null){
            $this->Session->setflash('出荷先施設情報が取得できませんでした', 'growl', array('type'=>'error','view_no') );
            $this->redirect($url);
        }
        if($facility_id === null){
            $facility_data = $this->MstFacility->find('first',array('conditions'=>array('MstFacility.facility_code' => $facility_code,
                                                                                        'MstFacility.facility_type' => array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital'))
                                                                                        )));
            $facility_id = $facility_data['MstFacility']['id'];
        }
        if($this->canUpdateAfterClose($work_date,$facility_id) === false){
            $this->Session->setflash('出荷先施設は締められています', 'growl', array('type'=>'error') );
            $this->redirect($url);
        }
        return true;
    }


    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        //CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }
    }

    /**
     * 部署シール印刷用メソッド
     */
    public function seal($type = null){
        if(!is_null($type)){
            $this->$type();
        }
    }

    /**
     * 印刷メソッド
     * @param stfings $reportType
     */
    public function report($reportType = null){
        $printType = $reportType;
        $isLowLevel = false;
        $listType = "";

        // 出荷履歴からデータを作成する場合
        switch($reportType){
          case 'makeShipping':
            $this->print_shipping_list();
            break;
          case 'makeInvoice':
            $this->print_invoice();
            break;
          default:
            break;
        }
    }

    /**
     * xml出力
     * @param
     */
    function xml_output($data,$columns,$layout_name,$fix_value,$layout_file){
        $this->layout = 'xml/default';
        $this->set('columns',$columns);
        $this->set('posted',$data);
        $this->set('layout_name',$layout_name);
        $this->set('fix_value',$fix_value); //連想配列のキーにname属性を指定
        $this->render($layout_file);
    }

    /**
     * 出荷リスト印刷
     */
    function print_shipping_list(){
        $xml_data = array();
        $xml_data['columns'] = array("納品先名",
                                     "出荷番号",
                                     "出荷先名",
                                     "施主名",
                                     "施設名",
                                     "出荷日",
                                     "出荷先住所",
                                     "施主住所",
                                     "施設住所",
                                     "行番号",
                                     "商品名",
                                     "規格",
                                     "製品番号",
                                     "販売元",
                                     "部署シール",
                                     "ロット番号",
                                     "明細備考",
                                     "数量",
                                     "包装単位",
                                     "管理区分",
                                     "商品ID",
                                     "印刷年月日",
                                     "ヘッダ備考",
                                     "削除フラグ",
                                     "改ページ番号"
                                     );

        $sql  = ' select ';
        $sql .= "       i1.facility_name || '様'                                                      as 納品先名 ";
        $sql .= '     , a.work_no                                                                     as 出荷番号 ';
        $sql .= "     , '出荷先: ' || h1.department_name                                              as 出荷先名 ";
        $sql .= '     , e.facility_name                                                               as 施主名 ';
        $sql .= '     , i2.facility_name                                                              as 施設名 ';
        $sql .= "     , to_char(a.work_date, 'YYYY年mm月dd日')                                        as 出荷日 ";
        $sql .= "     , '〒' ||  coalesce(i1.zip,'') || '  ' ||  coalesce(i1.address,'') || '\nTel:' ||  coalesce(i1.tel,'') || '／Fax:' ||  coalesce(i1.fax,'') as 出荷先住所 ";
        $sql .= "     , '〒' ||  coalesce(e.zip,'') || '  ' ||  coalesce(e.address,'') || '\nTel:' ||  coalesce(e.tel,'') || '／Fax:' ||  coalesce(e.fax,'')     as 施主住所 ";
        $sql .= "     , '〒' ||  coalesce(i2.zip,'') || '  ' ||  coalesce(i2.address,'') || '\nTel:' ||  coalesce(i2.tel,'') || '／Fax:' ||  coalesce(i2.fax,'') as 施設住所 ";
        $sql .= '     , d.item_name                                                                   as 商品名 ';
        $sql .= '     , d.standard                                                                    as 規格 ';
        $sql .= '     , d.item_code                                                                   as 製品番号 ';
        $sql .= '     , f.dealer_name                                                                 as 販売元 ';
        $sql .= '     , coalesce(b.hospital_sticker_no, g.hospital_sticker_no)                        as 部署シール ';
        $sql .= '     , g.lot_no                                                                      as ロット番号 ';
        $sql .= '     , b.recital                                                                     as 明細備考 ';
        $sql .= '     , b.quantity                                                                    as 数量 ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when c.per_unit = 1  ';
        $sql .= '         then c1.unit_name  ';
        $sql .= "         else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                                                               as 包装単位 ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.shipping_type = ' . Configure::read('ShippingType.exshipping');
        $sql .= '         then ' . Configure::read('ShippingType.extraordinary');
        $sql .= '         else b.shipping_type  ';
        $sql .= '         end ';
        $sql .= '     )                                                                               as 管理区分 ';
        $sql .= '     , d.internal_code                                                               as "商品ID" ';
        $sql .= '     , a.printed_date1                                                               as 印刷年月日 ';
        $sql .= '     , a.recital                                                                     as ヘッダ備考 ';
        $sql .= '     , b.is_deleted                                                                  as 削除フラグ ';
        $sql .= '     , a.work_no || e.id                                                             as 改ページ番号  ';
        $sql .= '   from ';
        $sql .= '     trn_shipping_headers as a  ';
        $sql .= '     left join trn_shippings as b  ';
        $sql .= '       on b.trn_shipping_header_id = a.id  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on c.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as c1  ';
        $sql .= '       on c1.id = c.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as c2  ';
        $sql .= '       on c2.id = c.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as d  ';
        $sql .= '       on d.id = c.mst_facility_item_id  ';
        $sql .= '     left join mst_facilities as e  ';
        $sql .= '       on e.id = d.mst_owner_id  ';
        $sql .= '     left join mst_dealers as f  ';
        $sql .= '       on f.id = d.mst_dealer_id  ';
        $sql .= '     left join trn_stickers as g  ';
        $sql .= '       on g.trn_shipping_id = b.id  ';
        $sql .= '     left join mst_departments as h1  ';
        $sql .= '       on h1.id = b.department_id_to  ';
        $sql .= '     left join mst_facilities as i1  ';
        $sql .= '       on i1.id = h1.mst_facility_id  ';
        $sql .= '     left join mst_departments as h2  ';
        $sql .= '       on h2.id = b.department_id_from  ';
        $sql .= '     left join mst_facilities as i2  ';
        $sql .= '       on i2.id = h2.mst_facility_id  ';
        $sql .= '   where ';
        $sql .= '     a.id in ( ' . join(',',$this->request->data['TrnShipping']['trn_shipping_header_id']) . ')';
        $sql .= '   order by a.work_no desc ';
        $sql .= '           ,e.id ';
        $sql .= '           ,case  ';
        $sql .= '              when b.shipping_type = ' . Configure::read('ShippingType.exshipping') . ' then ' . Configure::read('ShippingType.extraordinary');
        $sql .= '              else b.shipping_type  ';
        $sql .= '            end ';
        $sql .= '           ,f.dealer_name ';
        $sql .= '           ,d.item_name ';
        $sql .= '           ,d.standard ';
        $sql .= '           ,d.item_code ';
        $sql .= '           ,coalesce(b.hospital_sticker_no, g.hospital_sticker_no) ';
        $sql .= '           ,g.lot_no ';
        $ret = $this->TrnShipping->query($sql);

        $classes = Configure::read('ShippingTypes');

        $before = "0";
        foreach($ret as $r){
            // 明細番号
            $now = $r[0]['改ページ番号'];
            if($before !== $now){
                $i = 1;
                $before = $now;
            }

            // 出力データ
            $xml_data['data'][] = array(
                $r[0]['納品先名'],            //納品先名
                $r[0]['出荷番号'],            //出荷番号
                $r[0]['出荷先名'],            //出荷先名
                $r[0]['施設名'],              //施設名
                $r[0]['施主名'],              //施主名
                $r[0]['出荷日'],              //出荷日
                $r[0]['出荷先住所'],          //出荷先住所
                $r[0]['施設住所'],            //施設住所
                $r[0]['施主住所'],            //施主住所
                $i,                           //行番号
                $r[0]['商品名'],              //商品名
                $r[0]['規格'],                //規格
                $r[0]['製品番号'],          //製品番号
                $r[0]['販売元'],              //販売元
                $r[0]['部署シール'],          //部署シール
                $r[0]['ロット番号'],          //ロット番号
                $r[0]['明細備考'],            //明細備考
                $r[0]['数量'],                //数量
                $r[0]['包装単位'],            //包装単位
                $classes[$r[0]['管理区分']],  //管理区分
                $r[0]['商品ID'],              //商品ID
                $r[0]['印刷年月日'],          //印刷日時
                $r[0]['ヘッダ備考'],          //ヘッダ備考
                $r[0]['削除フラグ'],          //削除フラグ
                $r[0]['改ページ番号']         //改ページ番号
                );
            $i++;
        }

        $layout_name = Configure::read('layoutnameModels');
        $xml_data['layout_name'] = $layout_name['08'];
        $xml_data['fix_value'] = array('タイトル'=>'出荷伝票','sortkey' =>'出荷番号,改ページ番号');
        $xml_data['layout_file'] = '/shippings/xml_output';

        //xml出力関数呼び出し
        $this->xml_output(
            $xml_data["data"],
            join("\t",$xml_data['columns']),
            $xml_data['layout_name'],
            $xml_data['fix_value'],
            $xml_data['layout_file']
            );
    }


    /**
     * 納品書印刷
     */
    function print_invoice(){
        $xml_data = array();
        $xml_data['columns'] = array("納品先名",
                                     "納品番号",
                                     "納品先部署名",
                                     "施主名",
                                     "施設名",
                                     "納品日",
                                     "納品先住所",
                                     "施主住所",
                                     "施設住所",
                                     "行番号",
                                     "商品名",
                                     "規格",
                                     "製品番号",
                                     "販売元",
                                     "部署シール",
                                     "ロット番号",
                                     "明細備考",
                                     "数量",
                                     "包装単位",
                                     "管理区分",
                                     "商品ID",
                                     "単価",
                                     "金額",
                                     "印刷年月日",
                                     "ヘッダ備考",
                                     "丸め区分",
                                     "削除フラグ",
                                     "改ページ番号"
                                     );

        $sql  = ' select ';
        $sql .= "       i1.facility_name || '様'                               as 納品先名 ";
        $sql .= '     , a.work_no                                              as 納品番号 ';
        $sql .= "     , '納品先名: ' || h1.department_name                     as 納品先部署名 ";
        $sql .= '     , e.facility_name                                        as 施主名 ';
        $sql .= '     , i2.facility_name                                       as 施設名 ';
        $sql .= "     , to_char(a.work_date, 'YYYY年mm月dd日')                 as 納品日 ";
        $sql .= "     , '〒' || coalesce(i1.zip, '') || ' ' || coalesce(i1.address, '') || '\nTel:' || coalesce(i1.tel, '')  ";
        $sql .= "     || '／Fax:' || coalesce(i1.fax, '')                      as 納品先住所 ";
        $sql .= "     , '〒' || coalesce(e.zip, '') || ' ' || coalesce(e.address, '') || '\nTel:' || coalesce(e.tel, '') || '／Fax:'  ";
        $sql .= "     || coalesce(e.fax, '')                                   as 施主住所 ";
        $sql .= "     , '〒' || coalesce(i2.zip, '') || ' ' || coalesce(i2.address, '') || '\nTel:' || coalesce(i2.tel, '')  ";
        $sql .= "     || '／Fax:' || coalesce(i2.fax, '')                      as 施設住所 ";
        $sql .= '     , d.item_name                                            as 商品名 ';
        $sql .= '     , d.standard                                             as 規格 ';
        $sql .= '     , d.item_code                                            as 製品番号 ';
        $sql .= '     , f.dealer_name                                          as 販売元 ';
        $sql .= '     , coalesce(b.hospital_sticker_no, g.hospital_sticker_no) as 部署シール ';
        $sql .= '     , g.lot_no                                               as ロット番号 ';
        $sql .= '     , b.recital                                              as 明細備考 ';
        $sql .= '     , b.quantity                                             as 数量 ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when c.per_unit = 1  ';
        $sql .= '         then c1.unit_name  ';
        $sql .= "         else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                                        as 包装単位 ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.shipping_type = ' . Configure::read('ShippingType.exshipping');
        $sql .= '         then ' . Configure::read('ShippingType.extraordinary');
        $sql .= '         else b.shipping_type  ';
        $sql .= '         end ';
        $sql .= '     )                                                        as 管理区分 ';
        $sql .= '     , d.internal_code                                        as "商品ID" ';
        $sql .= '     , a.printed_date2                                        as 印刷年月日 ';
        $sql .= '     , j.sales_price                                          as 単価 ';
        $sql .= '     , j.sales_price * b.quantity                             as 金額 ';
        $sql .= '     , a.recital                                              as ヘッダ備考 ';
        $sql .= '     , i1.round                                               as 丸め区分 ';
        $sql .= '     , b.is_deleted                                           as 削除フラグ ';
        $sql .= '     , a.work_no || e.id                                      as 改ページ番号  ';
        $sql .= '   from ';
        $sql .= '     trn_shipping_headers as a  ';
        $sql .= '     left join trn_shippings as b  ';
        $sql .= '       on b.trn_shipping_header_id = a.id  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on c.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as c1  ';
        $sql .= '       on c1.id = c.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as c2  ';
        $sql .= '       on c2.id = c.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as d  ';
        $sql .= '       on d.id = c.mst_facility_item_id  ';
        $sql .= '     left join mst_facilities as e  ';
        $sql .= '       on e.id = d.mst_owner_id  ';
        $sql .= '     left join mst_dealers as f  ';
        $sql .= '       on f.id = d.mst_dealer_id  ';
        $sql .= '     left join trn_stickers as g  ';
        $sql .= '       on g.trn_shipping_id = b.id  ';
        $sql .= '     left join mst_departments as h1  ';
        $sql .= '       on h1.id = b.department_id_to  ';
        $sql .= '     left join mst_facilities as i1  ';
        $sql .= '       on i1.id = h1.mst_facility_id  ';
        $sql .= '     left join mst_departments as h2  ';
        $sql .= '       on h2.id = b.department_id_from  ';
        $sql .= '     left join mst_facilities as i2  ';
        $sql .= '       on i2.id = h2.mst_facility_id  ';
        $sql .= '     left join mst_sales_configs as j  ';
        $sql .= '       on j.mst_item_unit_id = c.id  ';
        $sql .= '       and j.partner_facility_id = i1.id  ';
        $sql .= '       and j.start_date <= a.work_date  ';
        $sql .= '       and j.end_date > a.work_date  ';
        $sql .= '       and j.is_deleted = false  ';
        $sql .= '   where ';
        $sql .= '     a.id in ('.join(',',$this->request->data['TrnShipping']['trn_shipping_header_id']).')  ';
        $sql .= '     and b.shipping_type <> ' . Configure::read('ShippingType.fixed');
        if($this->request->data['TemporaryClaimType'] == '1'){
            // 臨時品売上が消費時の場合、臨時も除く。
            $sql .= '     and b.shipping_type <> ' . Configure::read('ShippingType.extraordinary');
            $sql .= '     and b.shipping_type <> ' . Configure::read('ShippingType.exshipping');
        }
        $sql .= '   order by a.work_no desc ';
        $sql .= '           ,e.id ';
        $sql .= '           ,case  ';
        $sql .= '              when b.shipping_type = ' . Configure::read('ShippingType.extraordinary') . ' then ' . Configure::read('ShippingType.extraordinary');
        $sql .= '              else b.shipping_type  ';
        $sql .= '            end ';
        $sql .= '           ,f.dealer_name ';
        $sql .= '           ,d.item_name ';
        $sql .= '           ,d.standard ';
        $sql .= '           ,d.item_code ';
        $sql .= '           ,coalesce(b.hospital_sticker_no, g.hospital_sticker_no) ';
        $sql .= '           ,g.lot_no ';

        $ret = $this->TrnShipping->query($sql);

        // 管理区分定数
        $classes = Configure::read('ShippingTypes');

        $before = "0";
        foreach($ret as $r){
            // 明細番号
            $now = $r[0]['改ページ番号'];
            if($before !== $now){
                $i = 1;
                $before = $now;
            }
            // 出力データ
            $xml_data['data'][] = array(
                $r[0]['納品先名'],           //納品先名
                $r[0]['納品番号'],           //納品番号
                $r[0]['納品先部署名'],       //納品先部署名
                $r[0]['施主名'],             //施主名
                $r[0]['施設名'],             //施設名
                $r[0]['納品日'],             //納品日
                $r[0]['納品先住所'],         //納品先住所
                $r[0]['施主住所'],           //施主住所
                $r[0]['施設住所'],           //施設住所
                $i,                          //行番号
                $r[0]['商品名'],             //商品名
                $r[0]['規格'],               //規格
                $r[0]['製品番号'],         //製品番号
                $r[0]['販売元'],             //販売元
                $r[0]['部署シール'],         //部署シール
                $r[0]['ロット番号'],         //ロット番号
                $r[0]['明細備考'],           //明細備考
                $r[0]['数量'],               //数量
                $r[0]['包装単位'],           //包装単位
                $classes[$r[0]['管理区分']], //管理区分
                $r[0]['商品ID'],             //商品ID
                $r[0]['単価'],               //単価
                $r[0]['金額'],               //金額
                $r[0]['印刷年月日'],         //印刷日時
                $r[0]['ヘッダ備考'],         //ヘッダ備考
                $r[0]['丸め区分'],           //丸め区分
                $r[0]['削除フラグ'],         //削除フラグ
                $r[0]['改ページ番号']        //改ページ番号
                );
            $i++;
        }

        $layout_name = Configure::read('layoutnameModels');
        $xml_data['layout_name'] = $layout_name['09'];
        $xml_data['fix_value'] = array('タイトル'=>'納品書','sortkey' =>'納品番号,改ページ番号');
        $xml_data['layout_file'] = '/shippings/xml_output';
        //xml出力関数呼び出し
        $this->xml_output(
            $xml_data["data"],
            join("\t",$xml_data['columns']),
            $xml_data['layout_name'],
            $xml_data['fix_value'],
            $xml_data['layout_file']
            );
    }

    /**
     * 部署シール印刷
     */
    public function print_hospital_sticker(){
        App::import('Sanitize');
        $_hno = array();
        $data = array();
        $sql  = ' select ';
        $sql .= '       c.hospital_sticker_no  ';
        $sql .= '   from ';
        $sql .= '     trn_shipping_headers as a  ';
        $sql .= '     left join trn_shippings as b  ';
        $sql .= '       on b.trn_shipping_header_id = a.id  ';
        $sql .= '     left join trn_stickers as c  ';
        $sql .= '       on c.id = b.trn_sticker_id  ';
        $sql .= '   where ';
        if(isset($this->request->data['TrnShipping']['trn_shipping_header_id'][0])){
            $sql .= '     a.id = ' . $this->request->data['TrnShipping']['trn_shipping_header_id'][0];
        }else{
            $sql .= '     b.id in ( ' . implode(",",$this->request->data['TrnShipping']['id']).")";
        }
        $sql .= " and c.hospital_sticker_no != ''";
        $ret = $this->TrnShipping->query($sql);

        foreach($ret as $r ){
            $_hno[] = $r[0]['hospital_sticker_no'];
        }
        if(!empty($_hno)){
            $order = 'MstDepartment.department_name asc, MstShelfNameTo.code asc';
            $data = $this->Stickers->getHospitalStickers($_hno, $order,2);
        }
        
        $this->set('sortkey' , '病院情報,センター棚番号,商品ID');
        $this->set('Sticker' , $data);
        $this->layout = 'xml/default';
        $this->render('/stickerissues/hospital_sticker');
    }

    /**
     * コストシール印刷
     */
    function print_cost_sticker(){
        $ids = array();
        $sql  = ' select ';
        $sql .= '       b.id  ';
        $sql .= '   from ';
        $sql .= '     trn_shippings as a  ';
        $sql .= '     left join trn_stickers as b  ';
        $sql .= '       on b.trn_shipping_id = a.id  ';
        $sql .= '   where ';
        $sql .= '     a.trn_shipping_header_id = ' . $this->request->data['TrnShipping']['trn_shipping_header_id'][0];
        $ret = $this->TrnShipping->query($sql);

        foreach($ret as $r){
            $ids[] = $r[0]['id'];
        }
        // コストシールデータ取得
        $records = $this->Stickers->getCostStickers($ids);
        // 空なら初期化
        if(!$records){ $records = array();}

        $this->layout = 'xml/default';
        $this->set('records', $records);
        $this->render('/stickerissues/cost_sticker');
    }

    /**
     * 臨時出荷品検索画面
     *
     */
    function lowlevel_search(){
        
        App::import('Sanitize');
        $result = $cart_result = array();
        
        //検索フラグが立っている場合、検索処理実行
        if(isset($this->request->data['TrnShippingHeader']['searchFlg'])){
            
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            $where = '';

            $where = ' and d.partner_facility_id = ' . $this->request->data['TrnShippingHeader']['facilityId'];
            $where = " and d.start_date <= '" . $this->request->data['TrnShippingHeader']['work_date'] . "'";
            $where = " and d.end_date > '" . $this->request->data['TrnShippingHeader']['work_date'] . "'";
            $where = ' and f.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $where = ' and c.lowlevel_stock_count > 0 ';
            
            //商品ID
            if(!empty($this->request->data['search']['internal_code'])){
                $where .= ' and a.internal_code ILIKE '."'".trim($this->request->data['search']['internal_code'])."%'";
            }
            //商品名
            if(!empty($this->request->data['search']['item_name'])){
                $where .= ' and a.item_name ILIKE '."'%".trim($this->request->data['search']['item_name'])."%'";
            }
            //規格
            if(!empty($this->request->data['search']['standard'])){
                $where .= ' and a.standard ILIKE '."'%".trim($this->request->data['search']['standard'])."%'";
            }
            //製品番号
            if(!empty($this->request->data['search']['item_code'])){
                $where .= ' and a.item_code ILIKE '."'".trim($this->request->data['search']['item_code'])."%'";
            }
            //販売元
            if(!empty($this->request->data['search']['dealer_name'])){
                $where .= ' and a1.dealer_name ILIKE '."'".trim($this->request->data['search']['dealer_name'])."'";
            }
            //JANコード
            if(!empty($this->request->data['search']['jan_code'])){
                $where .= ' and a.jan_code ILIKE '."'".trim($this->request->data['search']['jan_code'])."%'";
            }
            //引当済み限定
            if(isset($this->request->data['search']['fixed_promise']) && $this->request->data['search']['fixed_promise'] === "1"){
               $where .= ' and x.id is not null ';
            }
            
            //カートに入っている商品は出さない。
            if(!empty($this->request->data['search']['tempId'])){
                $where .= ' and c.id not in ( ' . $this->request->data['search']['tempId'] . ')';
            }
            $result = $this->getLowLevelSearch($where , $this->request->data['search']['limit']);
            
            /* 選択商品検索 */
            $tempArray = array();
            if(!empty($this->request->data['search']['tempId'])){
                
                $temp = $this->getLowLevelSearch(' and c.id in ( ' . $this->request->data['search']['tempId'] . ')');

                //カートに追加した順に並べなおす。
                foreach( explode( ',', $this->request->data['search']['tempId']) as $id){
                    foreach($temp as $t){
                        if($t['TrnShipping']['id'] == $id){
                            $tempArray[] = $t;
                            break;
                        }
                    }
                }
                
                $cart_result = AppController::outputFilter($tempArray);
                
            }
            $this->request->data = $data;
        }else{
            // 施設コードから施設IDを取得
            $this->request->data['TrnShippingHeader']['facilityId'] = $this->getFacilityId($this->request->data['TrnShippingHeader']['facilityCode'] ,
                                                                                  Configure::read('FacilityType.hospital'));
            // 部署コードから部署IDを取得
            $this->request->data['TrnShippingHeader']['departmentId'] = $this->getDepartmentId($this->request->data['TrnShippingHeader']['facilityId'],
                                                                                      Configure::read('DepartmentType.hospital'),
                                                                                      $this->request->data['TrnShippingHeader']['departmentCode']
                                                                                      );
        }
        $this->set('result', $result);
        $this->set('cart_result', $cart_result);
    }

    /**
     * 低レベル品検索用SQL生成
     */
    private function getLowLevelSearch($where , $limit = null){
        $sql  = ' select ';
        $sql .= '       c.id                      as "TrnShipping__id" ';
        $sql .= '     , a.internal_code           as "TrnShipping__internal_code" ';
        $sql .= '     , a.item_name               as "TrnShipping__item_name" ';
        $sql .= '     , a.item_code               as "TrnShipping__item_code" ';
        $sql .= '     , a.standard                as "TrnShipping__standard" ';
        $sql .= '     , a1.dealer_name            as "TrnShipping__dealer_name" ';
        $sql .= '     , a.jan_code                as "TrnShipping__jan_code" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then b1.unit_name  ';
        $sql .= "         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                           as "TrnShipping__unit_name" ';
        $sql .= '     , c.lowlevel_stock_count    as "TrnShipping__quantity"';
        $sql .= '     , c.lowlevel_stock_count    as "TrnShipping__remain_count"';
        $sql .= '     , d.sales_price             as "TrnShipping__sales_price" ';
        $sql .= '     , d.modified                as "TrnShipping__modified" ';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_dealers as a1  ';
        $sql .= '       on a1.id = a.mst_dealer_id  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.mst_facility_item_id = a.id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     inner join trn_stocks as c  ';
        $sql .= '       on c.mst_item_unit_id = b.id  ';
        $sql .= '     left join mst_sales_configs as d  ';
        $sql .= '       on d.mst_item_unit_id = b.id  ';
        $sql .= '       and b.is_deleted = false ';
        $sql .= '       and d.is_deleted = false ';
        $sql .= "       and d.start_date <= '" . $this->request->data['TrnShippingHeader']['work_date'] . "'";
        $sql .= "       and d.end_date > '" . $this->request->data['TrnShippingHeader']['work_date'] . "'";
        $sql .= '     left join mst_facilities as e ';
        $sql .= '       on e.id = d.partner_facility_id ';
        $sql .= '     left join mst_departments as f ';
        $sql .= '       on f.id = c.mst_department_id ';


        $sql .= '    left join (  ';
        $sql .= '       select ';
        $sql .= '             b.id ';
        $sql .= '           , sum(a.remain_count) as remain_count' ;
        $sql .= '         from ';
        $sql .= '           trn_fixed_promises as a  ';
        $sql .= '           left join mst_item_units as b';
        $sql .= '             on b.id = a.mst_item_unit_id';
        $sql .= '         where ';
        $sql .= '           a.is_deleted = false  ';
        $sql .= '           and a.remain_count > 0  ';
        $sql .= '           and a.department_id_from = ' . $this->request->data['TrnShippingHeader']['departmentId'];
        $sql .= '         group by ';
        $sql .= '           b.id ';
        $sql .= '     ) as x  ';
        $sql .= '       on x.id = b.id ';

        $sql .= '   where ';
        $sql .= '     a.is_lowlevel = true  ';
        $sql .= '     and b.per_unit = 1 ';
        $sql .= $where;
        $sql .='    order by a.internal_code ';
        if(!is_null($limit)){
            $this->getMaxCount($sql, 'TrnStock');
            $sql .= ' limit ' . $limit ;
        }

        return $this->TrnStock->query($sql);
    }

    /**
     * 低レベル品出荷確認画面
     *
     */
    function lowlevel_confirm(){
        $result = $this->getLowLevelSearch(' and c.id in ( ' . $this->request->data['select']['selectId'] . ')');
        
        $tempArray = array();
        //カートに追加した順に並べなおす。
        foreach( explode( ',', $this->request->data['select']['selectId']) as $id){
            foreach($result as $tmp){
                if($tmp['TrnShipping']['id'] == $id){
                    $tempArray[] = $tmp;
                    break;
                }
            }
        } 
        
        $result = AppController::outputFilter($tempArray);
        $this->set('result', $result);
        
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('ExShipping.token' , $token);
        $this->request->data['ExShipping']['token'] = $token;
    }

    /**
     * 低レベル品出荷完了画面
     *
     */
    function lowlevel_result(){
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['ExShipping']['token'] === $this->Session->read('ExShipping.token')) {
            $this->Session->delete('ExShipping.token');
            $now = date('Y/m/d H:i:s.u');
            $date = date("Ymd");

            // 出荷先病院施設
            $facilityIdTo = $this->request->data['TrnShippingHeader']['facilityId'];
            // 出荷先病院部署
            $departmentIdTo = $this->request->data['TrnShippingHeader']['departmentId'];
            // 倉庫部署
            $departmentIdFrom = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'), array(Configure::read('DepartmentType.warehouse')));

            //出荷先施設の丸め区分、まとめ区分を取得
            $sql = " select gross , round from mst_facilities where id = " . $this->request->data['TrnShippingHeader']['facilityId'];
            $res = $this->MstFacility->query($sql);
            $gross = $res[0][0]['gross']; //まとめ区分
            $round = $res[0][0]['round']; //丸め区分

            $shippingStickerNo = "";
            
            //トランザクション開始
            $this->TrnStock->begin();

            //行ロック
            $this->TrnStock->query('select * from trn_stocks as a where a.id in (' .join(',',$this->request->data['SelectItemid']). ') for update ');
            
            $detailCount = count($this->request->data['SelectItemid']);
            /*****************************
             * 出荷ヘッダ作成
             *****************************/
            $shippingWorkNo = $this->setWorkNo4Header($date,"06");

            $TrnShippingHeader = array(
                'TrnShippingHeader' => array(
                    'work_no'            => $shippingWorkNo,
                    'work_date'          => $this->request->data['TrnShippingHeader']['work_date'],
                    'recital'            => $this->request->data['TrnShippingHeader']['recital'],
                    'shipping_type'      => Configure::read('ShippingType.exshipping'), //臨時出荷
                    'shipping_status'    => Configure::read('ShippingStatus.loaded'), //受領済
                    'department_id_from' => $departmentIdFrom,
                    'department_id_to'   => $departmentIdTo,
                    'detail_count'       => $detailCount,
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'created'            => $now,
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'modified'           => $now,
                    )
                );
            
            $this->TrnShippingHeader->create();
            if(!$this->TrnShippingHeader->save($TrnShippingHeader)){
                $this->TrnStock->rollback();
                $this->Session->setFlash("出荷ヘッダ作成エラー", 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }
            $TrnShippingHeaderId = $this->TrnShippingHeader->getLastInsertID(); //作成レコードID
            $this->request->data['TrnShipping']['trn_shipping_header_id']['0'] = $TrnShippingHeaderId; // 結果画面で帳票印刷用に確保
            
            /*****************************
             * 消費ヘッダ作成
             *****************************/
            $consumeWorkNo = $this->setWorkNo4Header($date,'01');
            $this->TrnConsumeHeader->create();
            $TrnConsumeHeader = array(
                'TrnConsumeHeader' =>  array(
                    'work_no'               => $consumeWorkNo,
                    'work_date'             => $this->request->data['TrnShippingHeader']['work_date'],
                    'recital'               => $this->request->data['TrnShippingHeader']['recital'],
                    'is_deleted'            => false,
                    'use_type'              => Configure::read('UseType.temporary'),//消費区分：臨時
                    'mst_department_id'     => $departmentIdTo,//部署参照キー
                    'detail_count'          => $detailCount,
                    'creater'               => $this->Session->read('Auth.MstUser.id'),
                    'modifier'              => $this->Session->read('Auth.MstUser.id'),
                    'created'               => $now,
                    'modified'              => $now,
                    )
                );

            if(!$this->TrnConsumeHeader->save($TrnConsumeHeader)){
                $this->TrnStock->rollback();
                $this->Session->setFlash("消費ヘッダ作成エラー", 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }
            $TrnConsumeHeaderId = $this->TrnConsumeHeader->getLastInsertID(); //作成レコードID
            
            /*****************************
             * 各明細更新
             *****************************/
            
            // 明細用のデータを取得
            $sql  = ' select ';
            $sql .= '       a.id               as "TrnShipping__trn_stock_id"';     // 倉庫在庫ID
            $sql .= '     , a.mst_item_unit_id as "TrnShipping__mst_item_unit_id"'; // 包装単位
            $sql .= '     , b.sales_price      as "TrnShipping__sales_price"';      // 売上単価
            $sql .= '   from ';
            $sql .= '     trn_stocks as a  ';
            $sql .= '     left join mst_sales_configs as b  ';
            $sql .= '       on b.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '       and b.partner_facility_id = ' . $facilityIdTo;
            $sql .= '       and b.is_deleted = false  ';
            $sql .= "       and b.start_date <= '" .  $this->request->data['TrnShippingHeader']['work_date'] . "'";
            $sql .= "       and b.end_date > '" .  $this->request->data['TrnShippingHeader']['work_date'] . "'";
            $sql .= '   where ';
            $sql .= '     a.id in ( ' . join(',',$this->request->data['SelectItemid']).  ' ) ';

            $result = $this->TrnStock->query($sql);
            
            $seq = 1; //明細用シーケンス番号
            foreach ($result as $r){

                $quantity = $this->request->data['input']['quantity'][$r['TrnShipping']['trn_stock_id']];
                $recital  = $this->request->data['input']['remarks'][$r['TrnShipping']['trn_stock_id']];
                
                /**** 出荷明細作成 ****/
                $this->TrnShipping->create();
                $TrnShipping = array(
                    'TrnShipping' => array(
                        'work_no'                => $shippingWorkNo,
                        'work_seq'               => $seq,
                        'work_date'              => $this->request->data['TrnShippingHeader']['work_date'],
                        'work_type'              => $this->request->data['TrnShippingHeader']['classId'],
                        'recital'                => $recital,
                        'shipping_type'          => Configure::read('ShippingType.exshipping'),
                        'shipping_status'        => Configure::read('ShippingStatus.loaded'),
                        'mst_item_unit_id'       => $r['TrnShipping']['mst_item_unit_id'],
                        'department_id_from'     => $departmentIdFrom,
                        'department_id_to'       => $departmentIdTo,
                        'trn_sticker_id'         => 0,
                        'quantity'               => $quantity,
                        'creater'                => $this->Session->read('Auth.MstUser.id'),
                        'modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'created'                => $now,
                        'modified'               => $now,
                        'trn_shipping_header_id' => $TrnShippingHeaderId
                        )
                    );
                if(!$this->TrnShipping->save($TrnShipping)){
                    $this->TrnStock->rollback();
                    $this->Session->setFlash("出荷明細作成エラー", 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                $TrnShippingId = $this->TrnShipping->getLastInsertID();

                /**** 消費明細作成 ****/
                $this->TrnConsume->create();
                $TrnConsume = array(
                    'TrnConsume' => array(
                        'work_no'               => $consumeWorkNo,
                        'work_seq'              => $seq,
                        'work_date'             => $this->request->data['TrnShippingHeader']['work_date'],
                        'work_type'             => $this->request->data['TrnShippingHeader']['classId'],
                        'recital'               => $recital,
                        'use_type'              => Configure::read('UseType.temporary'),
                        'mst_item_unit_id'      => $r['TrnShipping']['mst_item_unit_id'],
                        'mst_department_id'     => $departmentIdTo,
                        'quantity'              => $quantity,
                        'trade_type'            => Configure::read('ClassesType.LowLevel'), //低レベル品固定
                        'creater'               => $this->Session->read('Auth.MstUser.id'),
                        'modifier'              => $this->Session->read('Auth.MstUser.id'),
                        'created'               => $now,
                        'modified'              => $now,
                        'trn_consume_header_id' => $TrnConsumeHeaderId,
                        'before_quantity'       => $quantity
                        )
                    );
                if(!$this->TrnConsume->save($TrnConsume)){
                    $this->TrnStock->rollback();
                    $this->Session->setFlash("消費明細作成エラー", 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                $TrnConsumeId = $this->TrnConsume->getLastInsertID();
                if($this->Session->read('Auth.Config.LowlevelSalesType') == '0'){
                    /**** 請求(売上)作成 ****/
                    $claimPrice = $this->getFacilityUnitPrice($round , $r['TrnShipping']['sales_price'] , $quantity);
                    
                    $this->TrnClaim->create();
                    $TrnClaim = array(
                        'TrnClaim' => array(
                            'department_id_from' => $departmentIdFrom,
                            'department_id_to'   => $departmentIdTo,
                            'mst_item_unit_id'   => $r['TrnShipping']['mst_item_unit_id'],
                            'claim_date'         => $this->request->data['TrnShippingHeader']['work_date'],
                            'claim_price'        => $claimPrice,
                            'trn_consume_id'     => $TrnConsumeId,
                            'trn_shipping_id'    => $TrnShippingId,
                            'trade_type'         => Configure::read('ClassesType.LowLevel'),
                            'is_stock_or_sale'   => "2",//仕入売上判定フラグ 1:仕入;2:売上
                            'count'              => $quantity,//請求数
                            'unit_price'         => $r['TrnShipping']['sales_price'],//請求単価
                            'round'              => $round,//丸め区分
                            'gross'              => $gross,//まとめ区分
                            'is_not_retroactive' => false,//遡及除外フラグ
                            'creater'            => $this->Session->read('Auth.MstUser.id'),
                            'modifier'           => $this->Session->read('Auth.MstUser.id'),
                            'created'            => $now,
                            'modified'           => $now
                            )
                        );
                    if(!$this->TrnClaim->save($TrnClaim)){
                        $this->TrnStock->rollback();
                        $this->Session->setFlash("請求明細作成エラー", 'growl', array('type'=>'error') );
                        $this->redirect('ex_add');
                    }
                    $TrnClaimId = $this->TrnClaim->getLastInsertID();
                }
                $this->TrnStock->create();
                $ck = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.stock_count'          => 'stock_count - ' .  $quantity,
                        'TrnStock.lowlevel_stock_count' => 'lowlevel_stock_count - ' .  $quantity,
                        'TrnStock.modifier'             => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'             => "'" . $now . "'"
                        ),
                    array(
                        'TrnStock.id' => $r['TrnShipping']['trn_stock_id'],
                        ),
                    -1
                    );
                
                if(!$ck){
                    $this->TrnStock->rollback();
                    $this->Session->setFlash("センター在庫作成エラー", 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }

                $seq++; //明細用シーケンス番号加算
                // 引当済みのレコードがあった場合
                $sql  = ' select ';
                $sql .= '       a.id                    as fixed_promise_id ';
                $sql .= '     , a.remain_count          as remain_count';
                $sql .= '     , b.id                    as promise_id ';
                $sql .= '     , b.unreceipt_count       as unreceipt_count';
                $sql .= '     , c.id                    as center_stock_id  ';
                $sql .= '     , c.reserve_count         as reserve_count';
                $sql .= '   from ';
                $sql .= '     trn_fixed_promises as a  ';
                $sql .= '     left join trn_promises as b  ';
                $sql .= '       on a.trn_promise_id = b.id  ';
                $sql .= '     left join trn_stocks as c  ';
                $sql .= '       on c.mst_item_unit_id = a.mst_item_unit_id  ';
                $sql .= '     left join mst_departments as d  ';
                $sql .= '       on d.id = c.mst_department_id  ';
                $sql .= '   where ';
                $sql .= '     a.is_deleted = false  ';
                $sql .= '     and a.remain_count > 0  ';
                $sql .= '     and a.department_id_from = ' . $this->request->data['TrnShippingHeader']['departmentId'];
                $sql .= '     and a.mst_item_unit_id = ' . $r['TrnShipping']['mst_item_unit_id'];
                $sql .= '     and d.department_type = ' . Configure::read('DepartmentType.warehouse');
                $sql .= '   order by ';
                $sql .= '     a.id ';

                $ret = $this->TrnStock->query($sql);
                if($ret){
                    $count = 0;
                    // センター部署在庫から被要求数を減らす。
                    if( $ret[0][0]['reserve_count'] < $quantity){
                        $count = $ret[0][0]['reserve_count'];
                    } else {
                        $count = $quantity;
                    }
                    $this->TrnStock->create();
                    $ck = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.reserve_count' => 'reserve_count - ' .  $count,
                            'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'      => "'" . $now . "'"
                            ),
                        array(
                            'TrnStock.id' => $ret[0][0]['center_stock_id'],
                            ),
                        -1
                        );
                    if(!$ck){
                        $this->TrnStock->rollback();
                        $this->Session->setFlash("出荷処理中にエラーが発生しました", 'growl', array('type'=>'error') );
                        $this->redirect('ex_add');
                    }
                    
                    $r_quantity = $quantity;
                    $u_quantity = $quantity;
                    
                    foreach($ret as $r){
                        // 引当レコードから残数を減らす
                        if( $r_quantity > 0 ){
                            if($r[0]['remain_count'] < $r_quantity ){
                                $remain_count = 0;
                                $r_quantity = $r_quantity - $r[0]['remain_count'];
                            }else{
                                $remain_count = $r[0]['remain_count'] - $r_quantity;
                                $r_quantity = 0;
                            }
                            $this->TrnFixedPromise->create();
                            $ck = $this->TrnFixedPromise->updateAll(
                                array(
                                    'TrnFixedPromise.remain_count' => $remain_count,
                                    'TrnFixedPromise.modifier'      => $this->Session->read('Auth.MstUser.id'),
                                    'TrnFixedPromise.modified'      => "'" . $now . "'"
                                    ),
                                array(
                                    'TrnFixedPromise.id' => $r[0]['fixed_promise_id'],
                                    ),
                                -1
                                );
                            if(!$ck){
                                $this->TrnStock->rollback();
                                $this->Session->setFlash("出荷処理中にエラーが発生しました", 'growl', array('type'=>'error') );
                                $this->redirect('ex_add');
                            }
                        }
                        
                        // 要求レコードから未受領数を減らす。
                        if( $u_quantity > 0 ){
                            if($r[0]['unreceipt_count'] < $u_quantity ){
                                $unreceipt_count = 0;
                                $u_quantity = $u_quantity - $r[0]['unreceipt_count'];
                            }else{
                                $unreceipt_count = $r[0]['unreceipt_count'] - $u_quantity;
                                $u_quantity = 0;
                            }
                            $this->TrnPromise->create();
                            $ck = $this->TrnPromise->updateAll(
                                array(
                                    'TrnPromise.unreceipt_count' => $unreceipt_count,
                                    'TrnPromise.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                    'TrnPromise.modified'        => "'" . $now . "'"
                                    ),
                                array(
                                    'TrnPromise.id' => $r[0]['promise_id'],
                                    ),
                                -1
                                );
                            if(!$ck){
                                $this->TrnStock->rollback();
                                $this->Session->setFlash("出荷処理中にエラーが発生しました", 'growl', array('type'=>'error') );
                                $this->redirect('ex_add');
                            }
                        }
                    }
                }
            } //end of foreach

            //コミット直前に更新チェックを行う
            $ret = $this->TrnStock->query('select count(*) as count from trn_stocks as a where a.id in (' .join(',',$this->request->data['SelectItemid']). ") and a.modified <> '" .$now . "' and a.modified > '" . $this->request->data['ExShipping']['time'] ."'");
            if($ret[0][0]['count'] > 0){
                $this->TrnStock->rollback();
                $this->Session->setFlash("ほかユーザによって更新されています。最初からやり直してください。", 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }


            //コミット
            $this->TrnSticker->commit();
            $this->Session->setFlash("対象の商品を出荷しました", 'growl', array('type'=>'star') );
            $this->request->data['TrnShippingHeader']['shippingStickerNo'] = substr($shippingStickerNo,0,-1);
            
            //完了画面表示用データ取得
            $result = $this->getLowLevelSearch(' and c.id in ( ' . join(',',$this->request->data['SelectItemid']) . ')');
            
            $this->request->data['TrnShippingHeader']['ResultList'] = $result;
            $this->request->data['TrnShippingHeader']['work_no'] = $shippingWorkNo;
            $this->Session->write('ExShipping.data', $this->request->data );
        }else{
            // トランザクショントークン不正
            // リロード or ２度押し
            $this->request->data = $this->Session->read('ExShipping.data');
        }
    }

    /*
     * 未出荷一覧を取得
     */
    private function _getUnShippingList($mst_department_id){

        $sql  = ' select ';
        $sql .= '       (  ';
        $sql .= '       case a.promise_type  ';
        foreach(Configure::read('PromiseType.list') as $k => $v){
            $sql .= "         when " . $k . " then '" . $v . "'  ";
        }
        $sql .= '         end ';
        $sql .= '     )                            as "UnShipping__promise_type" ';
        $sql .= '     , c.internal_code            as "UnShipping__internal_code" ';
        $sql .= '     , c.item_name                as "UnShipping__item_name" ';
        $sql .= '     , c.item_code                as "UnShipping__item_code" ';
        $sql .= '     , c.standard                 as "UnShipping__standard" ';
        $sql .= '     , d.dealer_name              as "UnShipping__dealer_name" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then e.unit_name  ';
        $sql .= "         else e.unit_name || '(' || b.per_unit || f.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                            as "UnShipping__unit_name" ';
        $sql .= '     , \'\'                       as "UnShipping__hospital_sticker_no"  ';
        $sql .= '     , i.name                     as "UnShipping__shelf_name"';
        if($this->Session->read('Auth.Config.Shipping') == '0'){
            $sql .= '     , g.quantity                 as "UnShipping__count"';
        } else { 
            $sql .= '     , sum(a.remain_count)        as "UnShipping__count"';
        }
        $sql .= '   from ';
        $sql .= '     trn_fixed_promises as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as d  ';
        $sql .= '       on d.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_unit_names as e  ';
        $sql .= '       on e.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as f  ';
        $sql .= '       on f.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_fixed_counts as h';
        $sql .= '       on h.mst_item_unit_id = a.mst_item_unit_id ';
        $sql .= '      and h.mst_department_id = a.department_id_to ';
        $sql .= '     left join mst_shelf_names as i ';
        $sql .= '       on i.id = h.mst_shelf_name_id ';
        // 部署シールを利用する場合
        if($this->Session->read('Auth.Config.Shipping') == '0'){
            $sql .= '     left join trn_sticker_issues as g  ';
            $sql .= '       on a.id = g.trn_fixed_promise_id  ';
        }
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .= '     and a.department_id_from = ' . $mst_department_id;
        $sql .= '     and a.is_deleted = false  ';
        // 部署シールを利用する場合
        if($this->Session->read('Auth.Config.Shipping') == '0'){
            $sql .= '     and g.quantity > 0  ';
            $sql .= '     and g.is_deleted = false ';
        }else{
            $sql .= '     and a.remain_count > 0  ';
            $sql .= '   group by ';
            $sql .= '       a.promise_type  ';
            $sql .= '     , c.internal_code ';
            $sql .= '     , c.item_name ';
            $sql .= '     , c.item_code ';
            $sql .= '     , c.standard ';
            $sql .= '     , d.dealer_name ';
            $sql .= '     , e.unit_name ';
            $sql .= '     , b.per_unit ';
            $sql .= '     , f.unit_name ';
            $sql .= '     , i.name ';
        }
        $sql .= '   order by ';
        $sql .= '     a.promise_type ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , b.per_unit ';
        
        return $this->TrnStickerIssue->query($sql);
    }

    /**
     * 出荷履歴明細出力データ
     */
    private function getShippingDetail($where){
        $sql  = ' select ';
        $sql .= '       a.id                                      as "TrnShipping__id" ';
        $sql .= '     , e.internal_code                           as "TrnShipping__internal_code" ';
        $sql .= '     , e.item_name                               as "TrnShipping__item_name" ';
        $sql .= '     , e.item_code                               as "TrnShipping__item_code" ';
        $sql .= '     , e.standard                                as "TrnShipping__standard" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when d.per_unit = 1  ';
        $sql .= '         then j1.unit_name  ';
        $sql .= "         else j1.unit_name || '(' || d.per_unit || j2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                           as "TrnShipping__unit_name" ';
        $sql .= '     , a.work_no                                 as "TrnShipping__work_no" ';
        $sql .= '     , to_char(a.work_date, \'YYYY/mm/dd\')      as "TrnShipping__work_date" ';
        $sql .= '     , a.recital                                 as "TrnShipping__recital" ';
        $sql .= '     , a.quantity                                as "TrnShipping__quantity" ';
        $sql .= '     , c.facility_sticker_no                     as "TrnShipping__facility_sticker_no" ';
        $sql .= '     , coalesce(a.hospital_sticker_no, c.hospital_sticker_no)    as "TrnShipping__hospital_sticker_no" ';
        $sql .= '     , c.lot_no                                  as "TrnShipping__lot_no" ';
        $sql .= '     , to_char(c.validated_date, \'YYYY/mm/dd\') as "TrnShipping__validate_date" ';
        $sql .= '     , g.user_name                               as "TrnShipping__user_name" ';
        $sql .= '     , a.shipping_type                           as "TrnShipping__shipping_type" ';
        $sql .= '     , k.name                                    as "TrnShipping__work_type" ';
        $sql .= '     , f.dealer_name                             as "TrnShipping__dealer_name" ';
        $sql .= '     , i.facility_name                           as "TrnShipping__facility_name" ';
        $sql .= '     , h.department_name                         as "TrnShipping__department_name" ';
        $sql .= '     , coalesce( l.unit_price , m.sales_price )  as "TrnShipping__price" ';
        $sql .= '    , (  ';
        $sql .= '      case  ';
        $sql .= '        when a.is_deleted = true  ';
        $sql .= "            then '削除済みの為取消しできません'  ";
        $sql .= '        when a.shipping_type = ' . Configure::read('ShippingType.deposit');
        $sql .= "            then '預託品の為取消しできません'  ";
        $sql .= '        when a.shipping_type = ' . Configure::read('ShippingType.direct');
        $sql .= "            then '直納品の為取消しできません'  ";
        $sql .= '        when a.shipping_type = ' . Configure::read('ShippingType.nostock');
        $sql .= "            then '非在庫品の為取消しできません'  ";
        $sql .= '        when a.shipping_type in ( ' . Configure::read('ShippingType.fixed')  . ' , ' . Configure::read('ShippingType.semifixed') ;
        // 臨時品消費なしの場合
        if($this->Session->read('Auth.Config.ShippingConsumes') == 1 ){
            $sql .= ' , ' . Configure::read('ShippingType.extraordinary');
        }
        // 臨時出荷消費なしの場合
        if($this->Session->read('Auth.Config.ExShippingConsumes') == 1 ){
            $sql .= ' , ' . Configure::read('ShippingType.exshipping');
        }
        $sql .= ' ) and e.is_lowlevel = false and n.id is not null ';
        $sql .= "            then '消費済みの為取消しできません'";
        $sql .= "        when a.shipping_type = " . Configure::read('ShippingType.exshipping') . " and a.department_id_to <> c.mst_department_id ";
        $sql .= "            then '返却済みの為取消しできません' ";
        $sql .= '        when l.sales_close_type = 2 ';
        $sql .= "            then '締め済みの為取消しできません'  ";
        $sql .= '        when l.sales_close_type = 1 and x.updatable = false ';
        $sql .= "            then '締め済みの為取消しできません'  ";
        if($this->Session->read('Auth.Config.Shipping') == '0' ){
        $sql .= '        when e.is_lowlevel = false ';
        $sql .= '             and a.shipping_type != ' . Configure::read('ShippingType.exshipping') ;
        $sql .= '             and a.shipping_type != ' . Configure::read('ShippingType.exconsume') ;
        $sql .= '             and  a.shipping_status = ' . Configure::read('ShippingStatus.loaded');
        $sql .= "            then '受領済みの為取消しできません'  ";
        }
        $sql .= '        end ';
        $sql .= '    )                                         as "TrnShipping__alert_message"  ';
        $sql .= '     , e.item_type                            as "TrnShipping__item_type"  ';
        $sql .= '     , x.updatable                            as "TrnShipping__updatable"  ';
        $sql .= '   from ';
        $sql .= '     trn_shippings as a ';
        $sql .= '     left join trn_shipping_headers as b ';
        $sql .= '       on a.trn_shipping_header_id = b.id ';
        $sql .= '     left join trn_stickers as c ';
        $sql .= '       on a.trn_sticker_id = c.id ';
        $sql .= '     left join mst_item_units as d ';
        $sql .= '       on a.mst_item_unit_id = d.id ';
        $sql .= '     left join mst_facility_items as e ';
        $sql .= '       on d.mst_facility_item_id = e.id ';
        $sql .= '       and e.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
        $sql .= '     left join mst_dealers as f ';
        $sql .= '       on f.id = e.mst_dealer_id ';
        $sql .= '     left join mst_users as g ';
        $sql .= '       on g.id = a.modifier ';
        $sql .= '     left join mst_departments as h ';
        $sql .= '       on b.department_id_to = h.id ';
        $sql .= '     left join mst_facilities as i ';
        $sql .= '       on h.mst_facility_id = i.id ';
        $sql .= '     left join mst_unit_names as j1 ';
        $sql .= '       on j1.id = d.mst_unit_name_id ';
        $sql .= '     left join mst_unit_names as j2 ';
        $sql .= '       on j2.id = d.per_unit_name_id ';
        $sql .= '     left join mst_classes as k ';
        $sql .= '       on k.id = a.work_type ';
        $sql .= '     left join trn_claims as l ';
        $sql .= '       on l.id = c.sale_claim_id ';
        $sql .= '      and l.is_deleted = false  ';
        $sql .= '     left join mst_sales_configs as m';
        $sql .= '       on m.mst_item_unit_id = d.id ';
        $sql .= '      and m.start_date <= a.work_date ';
        $sql .= '      and m.end_date > a.work_date ';
        $sql .= '      and m.is_deleted = false  ';
        $sql .= '      and m.partner_facility_id = i.id ';
        $sql .= '     left join trn_consumes as n';
        $sql .= '       on n.id = c.trn_consume_id ';
        $sql .= '     left join trn_receivings as o ';
        $sql .= '       on o.trn_shipping_id = a.id ';
        $sql .= '      and o.is_deleted = false ';
        $sql .= '     inner join mst_user_belongings as z ';
        $sql .= '       on z.mst_facility_id = i.id ';
        $sql .= '       and z.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '       and z.is_deleted = false  ';
        $sql .= '     cross join (  ';
        $sql .= '       select ';
        $sql .= '             c.updatable  ';
        $sql .= '         from ';
        $sql .= '           mst_users as a  ';
        $sql .= '           left join mst_roles as b  ';
        $sql .= '             on a.mst_role_id = b.id  ';
        $sql .= '           left join mst_privileges as c  ';
        $sql .= '             on c.mst_role_id = b.id  ';
        $sql .= '         where ';
        $sql .= '           a.id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '           and c.view_no = 13'; //出荷履歴
        $sql .= '     ) as x  ';
        $sql .= '   where ';
        $sql .= '     a.shipping_type <> ' . Configure::read('ShippingType.move');
        $sql .= '     and e.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
        $sql .= $where;
        $sql .= '   order by ';
        $sql .= '     b.work_no desc , a.id ';

        //return $this->TrnShipping->query($sql);
        $ShippingList = $this->TrnShipping->query($sql);

        for($j=0;$j<count($ShippingList);$j++){
            if($ShippingList[$j]['TrnShipping']['item_type'] == Configure::read('Items.item_types.setitem')) {
                if ($this->_isCloseType($ShippingList[$j]['TrnShipping']['work_date'], $ShippingList[$j]['TrnShipping']['updatable']) == true) {
                    $ShippingList[$j]['TrnShipping']['alert_message'] = "締め済みの為取消しできません";
                }
            }
        }

        return $ShippingList;
    }


    /**
     * 出荷履歴明細where句作成
     */
    private function _getShippingDetailWhere(){

        //検索条件追加
        $where = '';
        //出荷番号
        if($this->request->data['search']['work_no'] != '' ){
            $where .= " and a.work_no like '" . $this->request->data['search']['work_no'] . "%'";
        }
        //出荷日FROM
        if($this->request->data['search']['start_date'] != '' ){
            $where .= " and a.work_date >= '" . $this->request->data['search']['start_date'] . "'";
        }
        //出荷日TO
        if($this->request->data['search']['end_date'] != '' ){
            $where .= " and a.work_date <= '" . $this->request->data['search']['end_date'] . "'";
        }
        //取消も表示する
        if( !isset($this->request->data['search']['is_deleted']) ){
            $where .= " and a.is_deleted = false ";
        }
        //出荷先施設
        if($this->request->data['search']['facilityCode'] != '' ){
            $where .= " and i.facility_code = '" . $this->request->data['search']['facilityCode'] . "'";
        }
        //出荷先部署
        if($this->request->data['search']['departmentCode'] != '' ){
            $where .= " and h.department_code = '" . $this->request->data['search']['departmentCode'] . "'";
        }
        //商品ID
        if($this->request->data['search']['internal_code'] != '' ){
            $where .= " and e.internal_code = '" . $this->request->data['search']['internal_code'] . "'";
        }
        //商品名
        if($this->request->data['search']['item_name'] != '' ){
            $where .= " and e.item_name like '%" . $this->request->data['search']['item_name'] . "%'";
        }
        //製品番号
        if($this->request->data['search']['item_code'] != '' ){
            $where .= " and e.item_code like '" . $this->request->data['search']['item_code'] . "%'";
        }
        //規格
        if($this->request->data['search']['standard'] != '' ){
            $where .= " and e.standard like '%" . $this->request->data['search']['standard'] . "%'";
        }
        //販売元
        if($this->request->data['search']['dealer_name'] != '' ){
            $where .= " and f.dealer_name like '%" . $this->request->data['search']['dealer_name'] . "%'";
        }
        //ロット番号
        if($this->request->data['search']['lot_no'] != '' ){
            $where .= " and c.lot_no like '" . $this->request->data['search']['lot_no'] . "%'";
        }
        //シール番号
        if($this->request->data['search']['sticker_no'] != '' ){
            $where .= " and ( c.facility_sticker_no = '" . $this->request->data['search']['sticker_no'] . "'";
            $where .= "    or c.hospital_sticker_no = '" . $this->request->data['search']['sticker_no'] . "'";
            $where .= "    or a.facility_sticker_no = '" . $this->request->data['search']['sticker_no'] . "'";
            $where .= "    or a.hospital_sticker_no = '" . $this->request->data['search']['sticker_no'] . "' ) ";
        }
        //管理区分
        if($this->request->data['search']['shipping_type'] != '' ){
            if($this->request->data['search']['shipping_type'] == Configure::read('ShippingType.extraordinary') ){
                $where .= " and a.shipping_type in ( " . Configure::read('ShippingType.extraordinary') . " , " . Configure::read('ShippingType.exshipping') . " ) ";
            }else{
                $where .= " and a.shipping_type = " . $this->request->data['search']['shipping_type'];
            }
        }
        //作業区分
        if($this->request->data['search']['work_type'] != '' ){
            $where .= " and a.work_type = " . $this->request->data['search']['work_type'] ;
        }

        //ヘッダID
        if(!empty($this->request->data['TrnShipping']['trn_shipping_header_id'])){
            $where .= ' and a.trn_shipping_header_id in ( ' . join(',',$this->request->data['TrnShipping']['trn_shipping_header_id']) . ')';
        }

        return $where;
    }


    /**
     * 締めチェック
     */
    private function _isCloseType($date, $updatable){
        $is_close = false;

        $sql  = ' select ';
        $sql .= '     close_type     as "TrnCloseHeaders__close_type" ';
        $sql .= '     ,is_provisional as "TrnCloseHeaders__is_provisional" ';
        $sql .= ' from ';
        $sql .= '     trn_close_headers ';
        $sql .= ' where ';
        $sql .= '     is_deleted = false ';
        $sql .= '     and mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= "     and start_date <= '" . $date . "'";
        $sql .= "     and end_date >= '" . $date . "'";
        $sql .= '     and close_type = '. Configure::read('CloseType.stock');
        $sql .= ' order by id desc ';
        $sql .= ' limit 1 ';

        $res = $this->TrnShipping->query($sql);

        foreach($res as $r){
            if($r['TrnCloseHeaders']['is_provisional'] == true){
                //仮締めの場合、更新権限を見る
                if ($updatable == false) {
                    //更新権限が無い場合は、締められている
                    $is_close = true;
                }
            } elseif ($r['TrnCloseHeaders']['is_provisional'] == false){
                $is_close = true;
            }

        }

        return $is_close;
    }


    function getSalesPrice($facility_id , $mst_item_unit_id , $work_date){
        $sql  = ' select ';
        $sql .= '     a.sales_price ';
        $sql .= ' from ';
        $sql .= '   mst_sales_configs as a  ';
        $sql .= ' where ';
        $sql .= '   a.partner_facility_id = ' . $facility_id;
        $sql .= '   and a.mst_item_unit_id = ' . $mst_item_unit_id;
        $sql .= "   and a.start_date <= '" . $work_date . "'  ";
        $sql .= "   and a.end_date > '" . $work_date . "'  ";
        $sql .= '   and a.is_deleted = false  ';
        
        $ret = $this->MstSalesConfig->query($sql);
        
        return $ret[0][0]['sales_price'];
    }
    
    function getExShippingData($sticker_no){
        $sql  = ' select ';
        $sql .= '       a.id                       as "TrnShipping__id"';
        $sql .= '     , c.internal_code            as "TrnShipping__internal_code"';
        $sql .= '     , c.item_name                as "TrnShipping__item_name"';
        $sql .= '     , c.item_code                as "TrnShipping__item_code"';
        $sql .= '     , c.standard                 as "TrnShipping__standard"';
        $sql .= '     , d.dealer_name              as "TrnShipping__dealer_name"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then b1.unit_name  ';
        $sql .= "         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .= '         end ';
        $sql .= '     )                            as "TrnShipping__unit_name" ';
        $sql .= '     , a.facility_sticker_no      as "TrnShipping__facility_sticker_no"';
        $sql .= '     , a.hospital_sticker_no      as "TrnShipping__hospital_sticker_no"';
        $sql .= '     , a.lot_no                   as "TrnShipping__lot_no"';
        $sql .= "     , to_char(a.validated_date, 'YYYY/mm/dd') " ;
        $sql .= '                                  as "TrnShipping__validated_date" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.id is null  ';
        $sql .= '         then false  ';
        $sql .= '         when e.id <> f.id  ';
        $sql .= '         then false  ';
        $sql .= '         when a.quantity = 0  ';
        $sql .= '         then false  ';
        $sql .= '         when a.is_deleted = true  ';
        $sql .= '         then false  ';
        $sql .= '         when a.has_reserved = true  ';
        $sql .= '         then false  ';
        $sql .= '         when g.id is null  ';
        $sql .= '         then false  ';
        $sql .= '         else true  ';
        $sql .= '         end ';
        $sql .= '     )                            as "TrnShipping__is_check" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.id is null  ';
        $sql .= "         then '無効なシール番号です' ";
        $sql .= '         when e.id <> f.id  ';
        $sql .= "         then '別部署に存在するシールです。' ";
        $sql .= '         when a.quantity = 0  ';
        $sql .= "         then '使用済みシールです'  ";
        $sql .= '         when a.is_deleted = true  ';
        $sql .= "         then '使用済みシールです'  ";
        $sql .= '         when a.has_reserved = true  ';
        $sql .= "         then '予約状態です'  ";
        $sql .= '         when g.id is null ';
        $sql .= "         then '売上設定がありません'";
        $sql .= "         else '' ";
        $sql .= '         end ';
        $sql .= '     )                            as "TrnShipping__alert" ';
        $sql .= '     , g.sales_price              as "TrnShipping__sales_price"';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as d  ';
        $sql .= '       on d.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_departments as e  ';
        $sql .= '       on e.id = a.mst_department_id  ';
        $sql .= '     left join mst_departments as f  ';
        $sql .= '       on f.mst_facility_id = c.mst_facility_id  ';
        $sql .= '      and f.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '     left join mst_sales_configs as g ';
        $sql .= '       on g.mst_item_unit_id = b.id ';
        $sql .= '      and g.partner_facility_id = ' . $this->request->data['TrnShippingHeader']['facilityId'];
        $sql .= "      and g.start_date <= '" . $this->request->data['TrnShippingHeader']['work_date']. "'";
        $sql .= "      and g.end_date > '" . $this->request->data['TrnShippingHeader']['work_date']. "'";
        $sql .= "      and g.is_deleted = false ";
        $sql .= '   where ';
        $sql .= "     a.facility_sticker_no = '" . $sticker_no . "'";
        $sql .= "     and a.lot_no != ' ' ";
        return $this->TrnSticker->query($sql);
    }

    /**
     * 出荷照合
     */
    function add_check () {
        // セッション処理
        $this->Session->write('Shipping.check_readTime', date('Y-m-d H:i:s'));
        // 施設コードから施設IDを取得
        $this->request->data['TrnShippingHeader']['facilityId'] = $this->getFacilityId($this->request->data['TrnShippingHeader']['facilityCode'] ,
                                                                             Configure::read('FacilityType.hospital'));
        // 部署コードから部署IDを取得
        $this->request->data['TrnShippingHeader']['departmentId'] = $this->getDepartmentId($this->request->data['TrnShippingHeader']['facilityId'],
                                                                                  Configure::read('DepartmentType.hospital'),
                                                                                  $this->request->data['TrnShippingHeader']['departmentCode']
                                                                                  );

        // 締めチェック
        $this->closedCheck('add', $this->request->data['TrnShippingHeader']['work_date'], null, $this->request->data['TrnShippingHeader']['facilityCode']);
        // シール情報取得
        $shipping_stecker_list = $this->TrnSticker->findEnableDataByCenterId($this->Session->read('Auth.facility_id_selected'),
                                                                             $this->request->data['TrnShippingHeader']['departmentCode']);

        // 指定部署で、未使用の部署シールと同じ単位のセンターシール一覧を取得する。
        $sql  = ' select ';
        $sql .= '       b.facility_sticker_no ';
        $sql .= '     , b.lot_no ';
        $sql .= '     , b.has_reserved ';
        $sql .= '     , c.per_unit ';
        $sql .= '     , c.is_standard ';
        $sql .= '     , d.internal_code ';
        $sql .= '     , c.id ';
        $sql .= '     , substr(d.jan_code, 0, 12)  as jan_code  ';
        $sql .= '   from ';
        $sql .= '     trn_sticker_issues as a  ';
        $sql .= '     left join trn_stickers as b  ';
        $sql .= '       on b.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '       and b.has_reserved = false  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on c.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as d  ';
        $sql .= '       on d.id = c.mst_facility_item_id  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.quantity > 0  ';
        $sql .= '     and b.is_deleted = false  ';
        $sql .= '     and b.quantity > 0  ';
        $sql .= '     and a.department_id_to = ' . $this->request->data['TrnShippingHeader']['departmentId'];
        
        $shipping_stecker_list = $this->TrnSticker->query($sql);
        
        // 部署シールコード情報取得
        $sql  = ' select ';
        $sql .= '       a.hospital_sticker_no ';
        $sql .= '     , c.per_unit ';
        $sql .= '     , c.is_standard ';
        $sql .= '     , d.internal_code ';
        $sql .= '     , c.id ';
        $sql .= '     , substr(d.jan_code, 0, 12)  as jan_code  ';
        $sql .= '   from ';
        $sql .= '     trn_sticker_issues as a  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on c.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as d  ';
        $sql .= '       on d.id = c.mst_facility_item_id  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.quantity > 0  ';
        $sql .= '     and a.department_id_to = ' . $this->request->data['TrnShippingHeader']['departmentId'];

        $sticker_issues_data = $this->TrnSticker->query($sql);

        // チェックのため、部署コードだけ配列化
        $hospital_code_list = array();
        foreach($sticker_issues_data as $key => $issue_data){
            $hospital_code_list[$key] = $issue_data[0]['hospital_sticker_no'];
        }
        $for_json = array();
        foreach($shipping_stecker_list as $table_data){
            $for_json[] = array(
                'facility_sticker_no' => $table_data[0]['facility_sticker_no'],
                'hospital_sticker_no' => '',
                'jan_code'            => $table_data[0]['jan_code'],
                'lot_no'              => $table_data[0]['lot_no'],
                'item_code'           => $table_data[0]['internal_code'],
                'item_unit_id'        => $table_data[0]['id'],
                'reserved_flag'       => $table_data[0]['has_reserved'],
                'standard_flag'       => $table_data[0]['is_standard'],
                'per_unit'            => $table_data[0]['per_unit'],
                );
        }

        // 病院コードを追加
        foreach($hospital_code_list as $code_key => $code){
            $for_json[] = array(
                'facility_sticker_no' => '',
                'hospital_sticker_no' => $code,
                'jan_code'            => '',
                'lot_no'              => '',
                'item_code'           => $sticker_issues_data[$code_key][0]['internal_code'],
                'item_unit_id'        => $sticker_issues_data[$code_key][0]['id'],
                'reserved_flag'       => '',
                'standard_flag'       => '',
                'per_unit'            => $sticker_issues_data[$code_key][0]['per_unit'],
                );
        }

        $error = '';
        if(count($shipping_stecker_list) == 0 ){
            $error .= "センターシール番号が存在しません。";
        }
        if(count($hospital_code_list) == 0 ){
            $error .= "部署シール番号が存在しません。";
        }

        $this->set('error',$error);
        $this->set('to_codes', json_encode($for_json));

    }

    /**
     * 出荷確認
     */
    function add_confirm () {
        // セッション処理
        $this->Session->write('Shipping.readTime',date('Y-m-d H:i:s'));
        
        $facility_sticker_no_list = $this->request->data['facility_sticker_no'];
        $hospital_sticker_no_list = $this->request->data['hospital_sticker_no'];

        // センター部署ID
        $centerDepartmentId = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') ,
                                                     Configure::read('DepartmentType.warehouse') );
        
        foreach($facility_sticker_no_list as $cnt => $val){

            $err_flg = false;
            $alert_message = '';
            $trn_sticker = $trn_sticker_issue = array();
            
            // trn_sticker 取得
            $sql  =' select ';
            $sql .='       a.id ';
            $sql .='     , a.facility_sticker_no';
            $sql .='     , a.quantity ';
            $sql .='     , a.is_deleted ';
            $sql .='     , a.has_reserved ';
            $sql .='     , a.mst_department_id ';
            $sql .='     , a.lot_no ';
            $sql .="     , to_char(a.validated_date,'YYYY/mm/dd') as validated_date";
            $sql .='     , a.mst_item_unit_id ';
            $sql .='     , c.id as mst_facility_item_id';
            $sql .='     , c.internal_code ';
            $sql .='     , c.item_name ';
            $sql .='     , c.item_code ';
            $sql .='     , c.standard ';
            $sql .='     , d.dealer_name ';
            $sql .='     , c.mst_owner_id  ';
            $sql .='     , ( case when b.per_unit = 1 then b1.unit_name ';
            $sql .="         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
            $sql .='         end ) as unit_name';
            $sql .='     , coalesce(a.sales_price,e.sales_price) as sales_price ';
            $sql .='   from ';
            $sql .='     trn_stickers as a ';
            $sql .='     left join mst_item_units as b  ';
            $sql .='       on b.id = a.mst_item_unit_id  ';
            $sql .='     left join mst_unit_names as b1';
            $sql .='       on b.mst_unit_name_id = b1.id';
            $sql .='     left join mst_unit_names as b2';
            $sql .='       on b.per_unit_name_id = b2.id';
            $sql .='     left join mst_facility_items as c  ';
            $sql .='       on c.id = b.mst_facility_item_id  ';
            $sql .='     left join mst_dealers as d  ';
            $sql .='       on d.id = c.mst_dealer_id  ';
            $sql .='    left join mst_sales_configs as e ';
            $sql .='       on e.mst_item_unit_id = a.mst_item_unit_id';
            $sql .='      and e.partner_facility_id = ' . $this->request->data['TrnShippingHeader']['facilityId'];
            $sql .='      and e.is_deleted = false ';
            $sql .="      and e.start_date <= '" . $this->request->data['TrnShippingHeader']['work_date'] . "'";
            $sql .="      and e.end_date > '" . $this->request->data['TrnShippingHeader']['work_date'] . "'";
            
            $sql .='   where ';
            $sql .="     a.facility_sticker_no = '" . $val . "'";
            $trn_sticker = $this->TrnSticker->query($sql);
            
            // trn_sticker_issue 取得
            $sql  =' select ';
            $sql .='       a.id ';
            $sql .='     , a.hospital_sticker_no ';
            $sql .='     , a.quantity ';
            $sql .='     , a.is_deleted ';
            $sql .='     , a.department_id_to ';
            $sql .='     , a.mst_item_unit_id ';
            $sql .='     , c.id as mst_facility_item_id';
            $sql .='     , c.internal_code ';
            $sql .='     , c.item_name ';
            $sql .='     , c.item_code ';
            $sql .='     , c.standard ';
            $sql .='     , d.dealer_name ';
            $sql .='     , c.mst_owner_id  ';
            $sql .='   from ';
            $sql .='     trn_sticker_issues as a  ';
            $sql .='     left join mst_item_units as b  ';
            $sql .='       on b.id = a.mst_item_unit_id  ';
            $sql .='     left join mst_facility_items as c  ';
            $sql .='       on c.id = b.mst_facility_item_id  ';
            $sql .='     left join mst_dealers as d  ';
            $sql .='       on d.id = c.mst_dealer_id  ';
            $sql .='   where ';
            $sql .="     a.hospital_sticker_no = '" . $hospital_sticker_no_list[$cnt] . "'";
            $trn_sticker_issue = $this->TrnStickerIssue->query($sql);
            
            if(count($trn_sticker) == 0){
                // センターシール無効
                $alert_message = 'センターシール番号が無効です。';
                $err_flg = true;
                // ダミーで埋める。
                $trn_sticker[0][0]['id']                   = '';
                $trn_sticker[0][0]['internal_code']        = '';
                $trn_sticker[0][0]['item_name']            = '';
                $trn_sticker[0][0]['item_code']            = '';
                $trn_sticker[0][0]['unit_name']            = '';
                $trn_sticker[0][0]['lot_no']               = '';
                $trn_sticker[0][0]['standard']             = '';
                $trn_sticker[0][0]['dealer_name']          = '';
                $trn_sticker[0][0]['validated_date']       = '';
                $trn_sticker[0][0]['facility_sticker_no']  = '';
                $trn_sticker[0][0]['mst_facility_item_id'] = '';
                $trn_sticker[0][0]['mst_owner_id']         = '';
            }
            
            if( $alert_message == '' ){
                if(count($trn_sticker_issue) == 0){
                    // 部署シール無効
                    $alert_message = '部署シール番号が無効です。';
                    $err_flg = true;
                    // ダミーで埋める。
                    $trn_sticker_issue[0][0]['id']                  = '';
                    $trn_sticker_issue[0][0]['hospital_sticker_no'] = '';
                }
            }
            // 同じ商品?
            if( $alert_message == '' ){
                if($trn_sticker[0][0]['mst_facility_item_id'] != $trn_sticker_issue[0][0]['mst_facility_item_id']){
                    $alert_message = '商品が異なります。';
                    $err_flg = true;
                }
            }
            // 同じ単位?
            if( $alert_message == '' ){
                if($trn_sticker[0][0]['mst_item_unit_id'] != $trn_sticker_issue[0][0]['mst_item_unit_id']){
                    $alert_message = '包装単位が異なります。';
                    $err_flg = true;
                }
            }

            // シールは有効？
            if( $alert_message == '' ){
                if($trn_sticker[0][0]['is_deleted']){
                    $alert_message = '削除済みのセンターシールです。';
                    $err_flg = true;
                }
            }
            if( $alert_message == '' ){
                if($trn_sticker_issue[0][0]['is_deleted']){
                    $alert_message = '削除済みの部署シールです。';
                    $err_flg = true;
                }
            }
            
            // シールに数量はある？
            if( $alert_message == '' ){
                if($trn_sticker[0][0]['quantity'] == 0){
                    $alert_message = 'センターシールに数量がありません。';
                    $err_flg = true;
                }
            }
            if( $alert_message == '' ){
                if($trn_sticker_issue[0][0]['quantity'] == 0){
                    $alert_message = '部署シールに数量がありません。';
                    $err_flg = true;
                }
            }
            
            // シールは予約済みではない？
            if( $alert_message == '' ){
                if($trn_sticker[0][0]['has_reserved']){
                    $alert_message = '予約済みのセンターシールです。';
                    $err_flg = true;
                }
            }
            
            // TODO：有効期限切れ？
            
            // TODO：有効期限警告？
            
            // センターにあるシール番号？
            if( $alert_message == '' ){
                if($trn_sticker[0][0]['mst_department_id'] != $centerDepartmentId){
                    $alert_message = '出荷済みのセンターシールです。';
                    $err_flg = true;
                }
            }
            
            // 選択部署に払い出すシール？
            if( $alert_message == '' ){
                if($trn_sticker_issue[0][0]['department_id_to'] !=  $this->request->data['TrnShippingHeader']['departmentId']){
                    $alert_message = '別部署の部署シールです。';
                    $err_flg = true;
                }
            }
            
            // 表示リスト
            $result[] = array(
                'trn_sticker_id'       => $trn_sticker[0][0]['id'],
                'trn_sticker_issue_id' => $trn_sticker_issue[0][0]['id'],
                'item_id'              => $trn_sticker[0][0]['internal_code'],
                'item_name'            => $trn_sticker[0][0]['item_name'],
                'item_code'            => $trn_sticker[0][0]['item_code'],
                'unit_name'            => $trn_sticker[0][0]['unit_name'],
                'lot_number'           => $trn_sticker[0][0]['lot_no'],
                'standard'             => $trn_sticker[0][0]['standard'],
                'dealer'               => $trn_sticker[0][0]['dealer_name'],
                'sales_price'          => $trn_sticker[0][0]['sales_price'], 
                'validated_date'       => $trn_sticker[0][0]['validated_date'],
                'hospital_sticker_no'  => $trn_sticker_issue[0][0]['hospital_sticker_no'],
                'facility_sticker_no'  => $trn_sticker[0][0]['facility_sticker_no'],
                'facility_item_id'     => $trn_sticker[0][0]['mst_facility_item_id'],
                'owner_id'             => $trn_sticker[0][0]['mst_owner_id'],
                'alert_message'        => $alert_message ,
                'err_flg'              => $err_flg
                );
        }

        // シールを読んだ順に並べ替える
        $tmp_result = $result;
        $result = array();
        
        foreach( $facility_sticker_no_list as $sticker){
            foreach($tmp_result as $tmp){
                if($tmp['facility_sticker_no'] == $sticker){
                    $result[] = $tmp;
                    break;
                }
            }
        }
        
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Shipping.token' , $token);
        $this->request->data['Shipping']['token'] = $token;

        $this->set('result', $result);
        $this->set('classes_enabled', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));
    }

    function add_result($mode="1"){
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['Shipping']['token'] === $this->Session->read('Shipping.token')) {
            $this->Session->delete('Shipping.token');
            $now = date('Y/m/d H:i:s');
            $read_time = $this->Session->read('Shipping.readTime');
            $invoice_print_flg = false ;
            $ids = array();
            $trn_consume_ids = array();
            
            foreach($this->request->data['TrnShipping']['id'] as $id){
                //TrnStickerId _ TrnStickerIssueId
                $tmp = explode('_' , $id );
                
                $ids[$tmp[0]] = $tmp[1];
                $chkStickerIds[] = $tmp[0];
            }
            $now = date('Y/m/d H:i:s');
            // トランザクション開始
            $this->TrnShippingHeader->begin();
            //行ロック
            $this->TrnShippingHeader->query(" select * from trn_stickers where id in (" . join(',',$chkStickerIds) . ") for update ");
            
            //更新チェックを行う
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $chkStickerIds). ')';
            $sql .= "     and a.modified > '" . $this->request->data['Shipping']['time'] ."'";

            $ret = $this->TrnSticker->query($sql);

            if($ret[0][0]['count']> 0 ){
                $this->TrnShippingHeader->rollback();
                $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }
            
            // センター倉庫部署ID取得
            $center_department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') ,
                                                           array(Configure::read('DepartmentType.warehouse')));
            
            if($mode=="1"){
                $shipping_status = Configure::read('ShippingStatus.ordered');
            } else {
                $shipping_status = Configure::read('ShippingStatus.loaded');
            }
            
            $workno = $this->setWorkNo4Header( date('Ymd') , "28");
            //出荷ヘッダ
            $TrnShippingHeader = array(
                'TrnShippingHeader'=> array(
                    'work_no'            => $workno,
                    'work_date'          => $this->request->data['TrnShippingHeader']['work_date'],
                    'recital'            => $this->request->data['TrnShippingHeader']['recital'],
                    'shipping_status'    => $shipping_status,
                    'shipping_type'      => Configure::read('ShippingType.fixed'),
                    'department_id_from' => $center_department_id,
                    'department_id_to'   => $this->request->data['TrnShippingHeader']['departmentId'],
                    'detail_count'       => count($ids),
                    'is_deleted'         => false,
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'created'            => $now,
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'modified'           => $now
                    )
                );
            $this->TrnShippingHeader->create();
            // SQL実行
            if (!$this->TrnShippingHeader->save($TrnShippingHeader)) {
                $this->TrnShippingHeader->rollback();
                $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('add');
            }
            $trn_shipping_header_id = $this->TrnShippingHeader->getLastInsertID();
            if($mode=="2"){
                $this->request->data['TrnShipping']['trn_shipping_header_id'][0] = $trn_shipping_header_id;
                //受領ヘッダ作成
                $TrnReceivingHeader = array(
                    'TrnReceivingHeader' => array(
                        'work_no'                => $workno,
                        'work_date'              => $this->request->data['TrnShippingHeader']['work_date'],
                        'recital'                => $this->request->data['TrnShippingHeader']['recital'], //最初の画面で入力した備考
                        'receiving_type'         => Configure::read('ReceivingType.receipt'), //入荷区分:受領による入荷
                        'receiving_status'       => Configure::read('ReceivingStatus.stock'), //入荷状態（受領済）
                        'department_id_from'     => $center_department_id,
                        'department_id_to'       => $this->request->data['TrnShippingHeader']['departmentId'],
                        'detail_count'           => count($ids),
                        'trn_shipping_header_id' => $trn_shipping_header_id,
                        'creater'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                => $now,
                        'modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'modified'               => $now,
                        )
                    );
                $this->TrnReceivingHeader->create();
                // SQL実行
                if (!$this->TrnReceivingHeader->save($TrnReceivingHeader)) {
                    $this->TrnShippingHeader->rollback();
                    $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }
                $trn_receiving_header_id = $this->TrnReceivingHeader->getLastInsertID();
                
                // 部署シール発行ヘッダ作成
                $TrnStickerIssueHeader = array(
                    'TrnStickerIssueHeader' => array(
                        'work_no'           => $workno,
                        'work_date'         => $this->request->data['TrnShippingHeader']['work_date'],
                        'recital'           => $this->request->data['TrnShippingHeader']['recital'],
                        'mst_department_id' => $this->request->data['TrnShippingHeader']['departmentId'],
                        'detail_count'      => count($ids),
                        'is_deleted'        => false,
                        'creater'           => $this->Session->read('Auth.MstUser.id'),
                        'modifier'          => $this->Session->read('Auth.MstUser.id'),
                        'created'           => $now,
                        'modified'          => $now,
                        )
                    );
                $this->TrnStickerIssueHeader->create();
                // SQL実行
                if (!$this->TrnStickerIssueHeader->save($TrnStickerIssueHeader)) {
                    $this->TrnShippingHeader->rollback();
                    $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }
                $trn_sticker_issue_header_id = $this->TrnStickerIssueHeader->getLastInsertID();
            }
            $seq = 1;
            foreach($ids as $cid => $hid){
                
                //TrnStickerに紐づく情報を取得
                $sql  = ' select ';
                $sql .= '     a.id ';
                $sql .= '   , a.facility_sticker_no';
                $sql .= '   , a.quantity ';
                $sql .= '   , a.mst_item_unit_id ';
                $sql .= '   , d.id as center_stock_id';
                $sql .= '   , a.trn_order_id';
                $sql .= '   , a.sales_price';
                $sql .= '   , a.transaction_price';
                $sql .= '   , a.trn_storage_id';
                $sql .= '   , c.buy_flg';
                $sql .= ' from ';
                $sql .= '   trn_stickers as a  ';
                $sql .= '   left join mst_item_units as b  ';
                $sql .= '     on b.id = a.mst_item_unit_id  ';
                $sql .= '   left join mst_facility_items as c  ';
                $sql .= '     on c.id = b.mst_facility_item_id ';
                $sql .= '   left join trn_stocks as d';
                $sql .= '     on d.mst_item_unit_id = a.mst_item_unit_id ';
                $sql .= '    and d.mst_department_id = a.mst_department_id';
                $sql .= ' where ';
                $sql .= '   a.id = ' . $cid;
                $sticker =  $this->TrnSticker->query($sql);
                
                //TrnStickerIssuに紐づく情報を取得
                $sql  = ' select ';
                if($mode=="1"){
                    $sql .= '     a.id ';
                    $sql .= '   , a.hospital_sticker_no ';
                    $sql .= '   , b.id                       as trn_fixed_promise_id ';
                }else{
                    $sql .= '     b.id                       as trn_fixed_promise_id ';
                }
                $sql .= '   , b.remain_count';
                $sql .= '   , c.id                       as trn_promise_id ';
                $sql .= '   , c.promise_type';
                $sql .= '   , (  ';
                $sql .= '     case  ';
                $sql .= '       when c.promise_type = ' . Configure::read('PromiseType.Temporary');
                $sql .= '       then ' . Configure::read('ShippingType.extraordinary');
                $sql .= '       else c.trade_type  ';
                $sql .= '       end ';
                $sql .= '   )                            as trade_type  ';
                $sql .= '   , g.gross';
                $sql .= '   , g.round';
                $sql .= '   , g.facility_code';
                $sql .= ' from ';
                if($mode=="1"){
                    $sql .= '   trn_sticker_issues as a  ';
                    $sql .= '   left join trn_fixed_promises as b  ';
                    $sql .= '     on b.id = a.trn_fixed_promise_id  ';
                }else{
                    $sql .= '   trn_fixed_promises as b  ';
                }
                $sql .= '   left join trn_promises as c  ';
                $sql .= '     on c.id = b.trn_promise_id  ';
                $sql .= '   left join mst_departments as f';
                $sql .= '     on f.id = b.department_id_from';
                $sql .= '   left join mst_facilities as g';
                $sql .= '     on g.id = f.mst_facility_id';
                $sql .= ' where ';
                if($mode=="1"){
                    $sql .= '   a.id = ' . $hid;
                }else{
                    $sql .= '   b.id = ' . $hid;
                }
                
                $stickerissue = $this->TrnStickerIssue->query($sql);
                if($mode=="2"){
                    // 部署シール番号発版
                    $hospital_sticker_no = $this->getHospitalStickerNo(date('ymd'),$stickerissue[0][0]['facility_code']);
                    // 部署シール明細
                    $TrnStickerIssue = array(
                        'TrnStickerIssue'=> array(
                            'work_no'                     => $workno,
                            'work_seq'                    => $seq,
                            'work_date'                   => $this->request->data['TrnShippingHeader']['work_date'],
                            'fixed_promise_status'        => Configure::read('PromiseStatus.SealIssue'),
                            'mst_item_unit_id'            => $sticker[0][0]['mst_item_unit_id'],
                            'department_id_from'          => $center_department_id,// センター部署ID
                            'department_id_to'            => $this->request->data['TrnShippingHeader']['departmentId'], // 病院部署ID
                            'quantity'                    => 0,
                            'trn_fixed_promise_id'        => $stickerissue[0][0]['trn_fixed_promise_id'],
                            'hospital_sticker_no'         => $hospital_sticker_no,
                            'is_deleted'                  => false,
                            'creater'                     => $this->Session->read('Auth.MstUser.id'),
                            'modifier'                    => $this->Session->read('Auth.MstUser.id'),
                            'created'                     => $now,
                            'modified'                    => $now,
                            'promise_type'                => $stickerissue[0][0]['promise_type'],
                            'trn_sticker_issue_header_id' => $trn_sticker_issue_header_id,
                            )
                        );
                    
                    $this->TrnStickerIssue->create();
                    // SQL実行
                    if (!$this->TrnStickerIssue->save($TrnStickerIssue)) {
                        $this->TrnShippingHeader->rollback();
                        $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }
                    $trn_sticker_issue_id = $this->TrnStickerIssue->getLastInsertID();

                    // 引当残数減
                    $this->TrnFixedPromise->create();
                    $res = $this->TrnFixedPromise->updateAll(
                        array(
                            'TrnFixedPromise.remain_count'         => 'TrnFixedPromise.remain_count -1 ',
                            'TrnFixedPromise.fixed_promise_status' => Configure::read('PromiseStatus.SealIssue'),
                            'TrnFixedPromise.modifier'             => $this->Session->read('Auth.MstUser.id'),
                            'TrnFixedPromise.modified'             => "'".$now."'",
                            ),
                        array(
                            'TrnFixedPromise.id' => $stickerissue[0][0]['trn_fixed_promise_id'],
                            ),
                        -1);
                    
                    if(false === $res){
                        $this->TrnShippingHeader->rollback();
                        $this->Session->setFlash('出荷処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }
                }else{
                    $hospital_sticker_no = $stickerissue[0][0]['hospital_sticker_no'];
                }
                
                //出荷明細作成
                $TrnShipping = array(
                    'TrnShipping'=> array(
                        'work_no'                => $workno,
                        'work_seq'               => $seq,
                        'work_date'              => $this->request->data['TrnShippingHeader']['work_date'],
                        'work_type'              => $this->request->data['TrnShipping']['work_type'][$cid],
                        'recital'                => $this->request->data['TrnShipping']['recital'][$cid],
                        'shipping_status'        => $shipping_status,
                        'shipping_type'          => $stickerissue[0][0]['trade_type'],
                        'mst_item_unit_id'       => $sticker[0][0]['mst_item_unit_id'],
                        'department_id_from'     => $center_department_id,
                        'department_id_to'       => $this->request->data['TrnShippingHeader']['departmentId'],
                        'quantity'               => $sticker[0][0]['quantity'],
                        'trn_sticker_id'         => $cid,
                        'trn_fixed_promise_id'   => $stickerissue[0][0]['trn_fixed_promise_id'],
                        'is_deleted'             => false,
                        'creater'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                => $now,
                        'modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'modified'               => $now,
                        'trn_shipping_header_id' => $trn_shipping_header_id
                        )
                    );

                $this->TrnShipping->create();
                // SQL実行
                if (!$this->TrnShipping->save($TrnShipping)) {
                    $this->TrnShippingHeader->rollback();
                    $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }
                $trn_shipping_id = $this->TrnShipping->getLastInsertID();
                $trn_shipping_ids[] = $trn_shipping_id;

                //シール移動履歴作成（出荷）
                $TrnStickerRecord = array(
                    'TrnStickerRecord'=> array(
                        'move_date'           => $this->request->data['TrnShippingHeader']['work_date'],
                        'move_seq'            => $this->getNextRecord($cid),
                        'record_type'         => Configure::read('RecordType.shipping'),
                        'record_id'           => $trn_shipping_id,
                        'mst_facility_id'     => $this->Session->read('Auth.facility_id_selected'),
                        'mst_department_id'   => $center_department_id,
                        'trn_sticker_id'      => $cid,
                        'facility_sticker_no' => $sticker[0][0]['facility_sticker_no'],
                        'hospital_sticker_no' => $hospital_sticker_no,
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now
                        )
                    );

                $this->TrnStickerRecord->create();
                // SQL実行
                if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                    $this->TrnShippingHeader->rollback();
                    $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }

                //部署シール数量減
                $this->TrnStickerIssue->create();
                $res = $this->TrnStickerIssue->updateAll(
                    array(
                        'TrnStickerIssue.quantity' => 'TrnStickerIssue.quantity - 1 ',
                        'TrnStickerIssue.modifier' => $this->Session->read('Auth.MstUser.id'),
                        'TrnStickerIssue.modified' => "'".$now."'",
                        ),
                    array(
                        'TrnStickerIssue.id' => $hid,
                        ),
                    -1);

                if(false === $res){
                    $this->TrnShippingHeader->rollback();
                    $this->Session->setFlash('出荷処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }

                if($mode=="2"){
                    //センター在庫:在庫数減、予約数減
                    $this->TrnStock->create();
                    $res = $this->TrnStock->updateAll(
                        array(
                            'TrnStock.stock_count'   => 'TrnStock.stock_count - 1 ',
                            'TrnStock.reserve_count' => 'TrnStock.reserve_count - 1 ',
                            'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                            'TrnStock.modified'      => "'".$now."'",
                            ),
                        array(
                            'TrnStock.id' => $sticker[0][0]['center_stock_id'],
                            ),
                        -1);
                    if(false === $res){
                        $this->TrnShippingHeader->rollback();
                        $this->Session->setFlash('出荷処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }
                }
                if( $stickerissue[0][0]['trade_type'] ==  Configure::read('ShippingType.extraordinary')){
                    //臨時品の場合
                    if($this->Session->read('Auth.Config.ShippingConsumes') == '1' ) {
                        $this->TrnStock->create();
                        $res = $this->TrnStock->updateAll(
                            array(
                                'TrnStock.stock_count'     => 'TrnStock.stock_count + 1 ',
                                'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                'TrnStock.modified'        => "'".$now."'",
                                ),
                            array(
                                'TrnStock.id' => $this->getStockRecode($sticker[0][0]['mst_item_unit_id'],
                                                                       $this->request->data['TrnShippingHeader']['departmentId']
                                                                       )
                                ),
                            -1);
                    }
                } else {
                    if($mode=="2"){
                        //病院在庫:在庫数増、入荷予定減
                        $this->TrnStock->create();
                        $res = $this->TrnStock->updateAll(
                            array(
                                'TrnStock.stock_count'     => 'TrnStock.stock_count + 1 ',
                                'TrnStock.requested_count' => 'TrnStock.requested_count - 1 ',
                                'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                'TrnStock.modified'        => "'".$now."'",
                                ),
                            array(
                                'TrnStock.id' => $this->getStockRecode($sticker[0][0]['mst_item_unit_id'],
                                                                       $this->request->data['TrnShippingHeader']['departmentId']
                                                                       )
                                ),
                            -1);
                    }
                }
                if(false === $res){
                    $this->TrnShippingHeader->rollback();
                    $this->Session->setFlash('出荷処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }

                $claim_id = null;
                if($mode=="2") {
                    //受領明細作成
                    $TrnReceiving = array(
                        'TrnReceiving'=> array(
                            'work_no'                 => $workno,
                            'work_seq'                => $seq,
                            'work_date'               => $this->request->data['TrnShippingHeader']['work_date'],
                            'receiving_type'          => Configure::read('ReceivingType.receipt'),
                            'receiving_status'        => Configure::read('ShippingStatus.loaded'),
                            'mst_item_unit_id'        => $sticker[0][0]['mst_item_unit_id'],
                            'department_id_from'      => $center_department_id,
                            'department_id_to'        => $this->request->data['TrnShippingHeader']['departmentId'],
                            'quantity'                => $sticker[0][0]['quantity'],
                            'trade_type'              => $stickerissue[0][0]['trade_type'],
                            'stocking_price'          => $sticker[0][0]['transaction_price'],
                            'sales_price'             => $sticker[0][0]['sales_price'],
                            'trn_sticker_id'          => $cid,
                            'trn_shipping_id'         => $trn_shipping_id,
                            'trn_order_id'            => $sticker[0][0]['trn_order_id'],
                            'trn_receiving_header_id' => $trn_receiving_header_id,
                            'facility_sticker_no'     => $sticker[0][0]['facility_sticker_no'],
                            'hospital_sticker_no'     => $hospital_sticker_no,
                            'creater'                 => $this->Session->read('Auth.MstUser.id'),
                            'created'                 => $now,
                            'modifier'                => $this->Session->read('Auth.MstUser.id'),
                            'modified'                => $now,
                            )
                        );
                    
                    $this->TrnReceiving->create();
                    // SQL実行
                    if (!$this->TrnReceiving->save($TrnReceiving)) {
                        $this->TrnShippingHeader->rollback();
                        $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }
                    $trn_receiving_id = $this->TrnReceiving->getLastInsertID();
                    
                    //シール移動履歴（受領）
                    $TrnStickerRecord = array(
                        'TrnStickerRecord'=> array(
                            'move_date'           => $this->request->data['TrnShippingHeader']['work_date'],
                            'move_seq'            => $this->getNextRecord($cid),
                            'record_type'         => Configure::read('RecordType.receipts'),
                            'record_id'           => $trn_receiving_id,
                            'mst_facility_id'     => $this->request->data['TrnShippingHeader']['facilityId'],
                            'mst_department_id'   => $this->request->data['TrnShippingHeader']['departmentId'],
                            'trn_sticker_id'      => $cid,
                            'facility_sticker_no' => $sticker[0][0]['facility_sticker_no'],
                            'hospital_sticker_no' => $hospital_sticker_no,
                            'is_deleted'          => false,
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now
                            )
                        );
                    
                    $this->TrnStickerRecord->create();
                    // SQL実行
                    if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                        $this->TrnShippingHeader->rollback();
                        $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }
                    
                    //引当明細更新
                    $this->TrnPromise->create();
                    $res = $this->TrnPromise->updateAll(
                        array(
                            'TrnPromise.receipt_count'   => 'TrnPromise.receipt_count + 1 ',
                            'TrnPromise.unreceipt_count' => 'TrnPromise.unreceipt_count - 1 ',
                            'TrnPromise.modifier'        => $this->Session->read('Auth.MstUser.id'),
                            'TrnPromise.modified'        => "'".$now."'",
                            ),
                        array(
                            'TrnPromise.id' => $stickerissue[0][0]['trn_promise_id'],
                            ),
                        -1);
                    
                    if(false === $res){
                        $this->TrnShippingHeader->rollback();
                        $this->Session->setFlash('出荷処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }
                }else{
                    $trn_receiving_id = null;
                }

                //売上作成(部署シール未使用かつ準定数 もしくは、臨時品かつ臨時品売上が「払出」の場合)
                if($sticker[0][0]['buy_flg'] == false ){
                    if( ( $stickerissue[0][0]['trade_type'] == Configure::read('ShippingType.semifixed') && $mode == '2' ) ||
                        $this->TemporaryItem->isClaim($this->name, $stickerissue[0][0]['trade_type'], $this->Session->read('Auth.Config.TemporaryClaimType'))
                    ){
                        if($stickerissue[0][0]['trade_type'] == Configure::read('ShippingType.semifixed')){
                            $trade_type = Configure::read('ClassesType.SemiConstant');
                        } else {
                            $trade_type = Configure::read('ClassesType.Temporary');
                        }
                        $price = $this->getSalesPrice($this->request->data['TrnShippingHeader']['facilityId'],
                                                      $sticker[0][0]['mst_item_unit_id'],
                                                      $this->request->data['TrnShippingHeader']['work_date']
                                                      );
                        $TrnClaim = array(
                            'TrnClaim' => array(
                                'department_id_from' => $center_department_id,
                                'department_id_to'   => $this->request->data['TrnShippingHeader']['departmentId'],
                                'mst_item_unit_id'   => $sticker[0][0]['mst_item_unit_id'],
                                'claim_date'         => $this->request->data['TrnShippingHeader']['work_date'],
                                'count'              => $sticker[0][0]['quantity'],
                                'unit_price'         => $price,
                                'round'              => $stickerissue[0][0]['round'],
                                'gross'              => $stickerissue[0][0]['gross'],
                                'is_not_retroactive' => false,
                                'claim_price'        => $price * $sticker[0][0]['quantity'],
                                'trn_receiving_id'   => $trn_receiving_id,
                                'trn_shipping_id'    => $trn_shipping_id,
                                'trade_type'         => $trade_type,
                                'creater'            => $this->Session->read('Auth.MstUser.id'),
                                'created'            => $now,
                                'modifier'           => $this->Session->read('Auth.MstUser.id'),
                                'modified'           => $now,
                                'is_stock_or_sale'   => '2',                         //仕入売上判定フラグ 1:仕入;2:売上
                                'is_deleted'         => false,
                                'stocking_close_type'=> 0,
                                'sales_close_type'   => 0,
                                'facility_close_type'=> 0,
                                'facility_sticker_no'=> $sticker[0][0]['facility_sticker_no'],
                                'hospital_sticker_no'=> $hospital_sticker_no,
                                'claim_type'         => Configure::read('ClaimType.sale'),  //売上
                                )
                            );
                        $this->TrnClaim->create();
                        // SQL実行
                        if (!$this->TrnClaim->save($TrnClaim)) {
                            $this->TrnShippingHeader->rollback();
                            $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('add');
                        }
                        $claim_id = $this->TrnClaim->getLastInsertID();
                    }
                }

                $trn_consume_id = null;

                if($stickerissue[0][0]['trade_type'] == Configure::read('ShippingType.extraordinary') &&
                   $this->Session->read('Auth.Config.ShippingConsumes') == '0'){
                    //消費作成(臨時の場合)
                    $TrnConsume = array(
                        'TrnConsume' => array(
                            'work_no'               => $workno,
                            'work_seq'              => $seq,
                            'work_date'             => $this->request->data['TrnShippingHeader']['work_date'],
                            'use_type'              => Configure::read('UseType.temporary'),        //消費区分：臨時
                            'mst_item_unit_id'      => $sticker[0][0]['mst_item_unit_id'],
                            'mst_department_id'     => $this->request->data['TrnShippingHeader']['departmentId'],
                            'quantity'              => $sticker[0][0]['quantity'],
                            'trn_sticker_id'        => $cid,
                            'trn_storage_id'        => $sticker[0][0]['trn_storage_id'],
                            'trn_consume_header_id' => '',              //後で更新
                            'is_retroactable'       => 0,
                            'creater'               => $this->Session->read('Auth.MstUser.id'),
                            'created'               => $now,
                            'modifier'              => $this->Session->read('Auth.MstUser.id'),
                            'modified'              => $now,
                            'is_deleted'            => false,
                            'facility_sticker_no'   => $sticker[0][0]['facility_sticker_no'],
                            'hospital_sticker_no'   => $hospital_sticker_no
                            )
                        );
                    $this->TrnConsume->create();
                    // SQL実行
                    if (!$this->TrnConsume->save($TrnConsume)) {
                        $this->TrnShippingHeader->rollback();
                        $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }

                    $trn_consume_id = $this->TrnConsume->getLastInsertID();
                    $trn_consume_ids[] = $trn_consume_id;
                    //シール移動履歴（消費）
                    $TrnStickerRecord = array(
                        'TrnStickerRecord'=> array(
                            'move_date'           => $this->request->data['TrnShippingHeader']['work_date'],
                            'move_seq'            => $this->getNextRecord($cid),
                            'record_type'         => Configure::read('RecordType.consume'),
                            'record_id'           => $trn_consume_id,
                            'mst_facility_id'     => $this->request->data['TrnShippingHeader']['facilityId'],
                            'mst_department_id'   => $this->request->data['TrnShippingHeader']['departmentId'],
                            'trn_sticker_id'      => $cid,
                            'facility_sticker_no' => $sticker[0][0]['facility_sticker_no'],
                            'hospital_sticker_no' => $hospital_sticker_no,
                            'is_deleted'          => false,
                            'creater'             => $this->Session->read('Auth.MstUser.id'),
                            'created'             => $now,
                            'modifier'            => $this->Session->read('Auth.MstUser.id'),
                            'modified'            => $now
                            )
                        );

                    $this->TrnStickerRecord->create();
                    // SQL実行
                    if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                        $this->TrnShippingHeader->rollback();
                        $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }

                    //シール数量0に更新
                    $this->TrnSticker->create();
                    $res = $this->TrnSticker->updateAll(
                        array('TrnSticker.quantity' => 0),
                        array('TrnSticker.id' => $cid),
                        -1);

                    if(false === $res){
                        $this->TrnShippingHeader->rollback();
                        $this->Session->setFlash('出荷処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }
                }

                if($mode=="1"){
                    $mst_department_id = $center_department_id;
                    $has_reserved = "true";
                }else{
                    $mst_department_id = $this->request->data['TrnShippingHeader']['departmentId'];
                    $has_reserved = "false";
                }

                //シールを更新
                $this->TrnSticker->create();
                $res = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.mst_department_id'    => $mst_department_id,
                        'TrnSticker.trn_shipping_id'      => $trn_shipping_id,
                        'TrnSticker.trn_promise_id'       => $stickerissue[0][0]['trn_promise_id'],
                        'TrnSticker.trn_fixed_promise_id' => $stickerissue[0][0]['trn_fixed_promise_id'],
                        'TrnSticker.receipt_id'           => $trn_receiving_id,
                        'TrnSticker.trn_consume_id'       => $trn_consume_id,
                        'TrnSticker.sale_claim_id'        => $claim_id,
                        'TrnSticker.last_move_date'       => "'".$this->request->data['TrnShippingHeader']['work_date']."'",
                        'TrnSticker.trade_type'           => $stickerissue[0][0]['trade_type'],
                        'TrnSticker.hospital_sticker_no'  => "'" . $hospital_sticker_no . "'",
                        'TrnSticker.has_reserved'         => "'" . $has_reserved . "'",
                        'TrnSticker.is_deleted'           => 'false',
                        'TrnSticker.modifier'             => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'             => "'".$now."'",
                        ),
                    array(
                        'TrnSticker.id' => $cid,
                        ),
                    -1);

                if(false === $res){
                    $this->TrnShippingHeader->rollback();
                    $this->Session->setFlash('出荷処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }

                //売上データ作成済み、かつ、消費データ作成済みの場合、売上データを更新
                if($claim_id != null && $trn_consume_id != null){
                    $this->TrnClaim->create();
                    $res = $this->TrnClaim->updateAll(
                        array(
                            'TrnClaim.trn_consume_id' => $trn_consume_id,
                          ),
                        array(
                          'TrnClaim.id' => $claim_id,
                          ),
                        -1);
                    if(false === $res){
                        $this->TrnShippingHeader->rollback();
                        $this->Session->setFlash('出荷処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('add');
                    }
                }

                // 定数品以外の場合納品書を出力する
                if($stickerissue[0][0]['trade_type'] != Configure::read('ClassesType.Constant')){
                    $invoice_print_flg = true;
                }

                $seq++;
            }
            if(count($trn_consume_ids) > 0){
                //消費ヘッダ作成
                $TrnConsumeHeader = array(
                    'TrnConsumeHeader' => array(
                        'work_no'               => $workno,
                        'work_date'             => $this->request->data['TrnShippingHeader']['work_date'],
                        'use_type'              => Configure::read('UseType.temporary'),           //消費区分：臨時
                        'mst_department_id'     => $this->request->data['TrnShippingHeader']['departmentId'], //部署参照キー
                        'detail_count'          => count($trn_consume_ids) ,
                        'creater'               => $this->Session->read('Auth.MstUser.id'),
                        'created'               => $now,
                        'modifier'              => $this->Session->read('Auth.MstUser.id'),
                        'modified'              => $now,
                        'is_deleted'            => false
                        )
                    );

                $this->TrnConsumeHeader->create();
                // SQL実行
                if (!$this->TrnConsumeHeader->save($TrnConsumeHeader)) {
                    $this->TrnShippingHeader->rollback();
                    $this->Session->setFlash('出荷中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }
                $trn_consume_hedaer_id = $this->TrnConsumeHeader->getLastInsertID();

                //消費明細に消費ヘッダIDを指す
                $this->TrnConsume->create();
                $res = $this->TrnConsume->updateAll(
                    array(
                        'TrnConsume.trn_consume_header_id' => $trn_consume_hedaer_id,
                        'TrnConsume.modifier'              => $this->Session->read('Auth.MstUser.id'),
                        'TrnConsume.modified'              => "'".$now."'",
                        ),
                    array(
                        'TrnConsume.id' => $trn_consume_ids,
                        ),
                    -1);

                if(false === $res){
                    $this->TrnShippingHeader->rollback();
                    $this->Session->setFlash('出荷処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('add');
                }
            }


            //コミット
            $this->TrnShippingHeader->commit();

            //結果表示用データ取得
            $sql  = ' select ';
            $sql .= '     c.internal_code            as "TrnShipping__internal_code"';
            $sql .= '   , c.item_name                as "TrnShipping__item_name"';
            $sql .= '   , c.item_code                as "TrnShipping__item_code" ';
            $sql .= '   , (  ';
            $sql .= '     case  ';
            $sql .= '       when b.per_unit = 1  ';
            $sql .= '       then b1.unit_name  ';
            $sql .= "       else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
            $sql .= '       end ';
            $sql .= '   )                            as "TrnShipping__unit_name" ';
            $sql .= '   , d.lot_no                   as "TrnShipping__lot_no" ';
            $sql .= '   , d.hospital_sticker_no      as "TrnShipping__hospital_sticker_no" ';
            $sql .= '   , f.name                     as "TrnShipping__work_type_name" ';
            $sql .= '   , ( case ';
            foreach(Configure::read('ShippingTypes') as $k=>$v){
                $sql .= ' when a.shipping_type = '. $k . " then '" . $v . "'";
            }
            $sql .= '    end )                       as "TrnShipping__shipping_type_name" ';
            $sql .= '   , c.standard                 as "TrnShipping__standard"';
            $sql .= '   , e.dealer_name              as "TrnShipping__dealer_name"';
            $sql .= '   , g.sales_price              as "TrnShipping__sales_price"';
            $sql .= "   , to_char(d.validated_date,'YYYY/mm/dd')";
            $sql .= '                                as "TrnShipping__validated_date"';
            $sql .= '   , d.facility_sticker_no      as "TrnShipping__facility_sticker_no"';
            $sql .= '   , a.recital                  as "TrnShipping__recital"';
            $sql .= ' from ';
            $sql .= '   trn_shippings as a  ';
            $sql .= '   left join mst_item_units as b  ';
            $sql .= '     on b.id = a.mst_item_unit_id  ';
            $sql .= '   left join mst_unit_names as b1  ';
            $sql .= '     on b1.id = b.mst_unit_name_id  ';
            $sql .= '   left join mst_unit_names as b2  ';
            $sql .= '     on b2.id = b.per_unit_name_id  ';
            $sql .= '   left join mst_facility_items as c  ';
            $sql .= '     on c.id = b.mst_facility_item_id  ';
            $sql .= '   left join trn_stickers as d  ';
            $sql .= '     on d.id = a.trn_sticker_id  ';
            $sql .= '   left join mst_dealers as e  ';
            $sql .= '     on e.id = c.mst_dealer_id  ';
            $sql .= '   left join mst_classes as f  ';
            $sql .= '     on f.id = a.work_type  ';
            $sql .= '   left join mst_sales_configs as g ';
            $sql .= '     on g.mst_item_unit_id = a.mst_item_unit_id';
            $sql .= '    and g.partner_facility_id = ' . $this->request->data['TrnShippingHeader']['facilityId'];
            $sql .= '    and g.is_deleted = false ';
            $sql .= "    and g.start_date <= '" . $this->request->data['TrnShippingHeader']['work_date'] . "'";
            $sql .= "    and g.end_date > '" . $this->request->data['TrnShippingHeader']['work_date'] . "'";
            
            $sql .= ' where ';
            $sql .= '    a.id in (' . join(',',$trn_shipping_ids) . ') ';
            $result = $this->TrnShipping->query($sql);

            $this->Session->write('Shipping.result',$result);
            $this->set('result' , $result);
            $this->Session->write('Shipping.invoice_print_flg',$invoice_print_flg);
            $this->set('invoice_print_flg', $invoice_print_flg);
            $this->request->data['Shipping']['StickerCheck'] = 1 ;

        } else {
            if(isset($this->request->data['Shipping']['UnShippingList'])){
                $this->set('unShippingList' , $this->_getUnShippingList($this->request->data['TrnShippingHeader']['departmentId']));
            }
            $this->set('result' , $this->Session->read('Shipping.result'));
            $this->set('invoice_print_flg' , $this->Session->read('Shipping.invoice_print_flg'));
        }
    }

}