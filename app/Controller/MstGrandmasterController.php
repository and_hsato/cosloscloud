<?php
/**
 * MstGrandmasterController
 * グランドマスタ
 */

class MstGrandmasterController extends AppController {

    /**
     * @var $name
     */
    var $name = 'MstGrandmaster';

    /**
     * @var array $uses
     */
    
    
    //使用Model名
    var $uses = array(
                      'MstGrandMktCo', 
                      'MstGrandInsAppr',
                      'MstGrandKyoMaterial',
                      'MstGrandDepClCdRelation',
                      'MstGrandUnit',
                      'MstGrandMatPrice',
                      'MstGrandMacmapClass',
                      'MstGrandDepClCd',
                      'MstDealer',
                      'MstInsuranceClaim'
    );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'Common');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Common','utils','Stickers','CsvWriteUtils','CsvReadUtils');

    /**
     *
     * @var array $errors
     */
    var $errors;

    /**
     *
     * @var array $successes
     */
    var $successes;

    var $isDebug = true;
    var $linkString = "";

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';
                
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        //CSV入力方式の切替
        // コスロスマスタは、カンマ区切り固定？
    }
    
    /**
     *
     */
    function index(){
        $this->import_csv();
    }

    /**
     * CSV取込画面
     */
    function import_csv() {
        $memo1 = $this->MstGrandKyoMaterial->find('all',
        array(
            'fields' => array('MstGrandKyoMaterial.modified'),
            'order' => array('MstGrandKyoMaterial.modified desc'),
            'limit' => 1,
            'recursive'=>-1
          )
        );    
        $this->set('Memo1',$memo1);
        
        $memo2 = $this->MstGrandInsAppr->find('all',
        array(
            'fields' => array('MstGrandInsAppr.modified'),
            'order' => array('MstGrandInsAppr.modified desc'),
            'limit' => 1,
            'recursive'=>-1
          )
        );    
        $this->set('Memo2',$memo2);
        
        $memo3 = $this->MstGrandMktCo->find('all',
        array(
            'fields' => array('MstGrandMktCo.modified'),
            'order' => array('MstGrandMktCo.modified desc'),
            'limit' => 1,
            'recursive'=>-1
          )
        );    
        $this->set('Memo3',$memo3);
        
        $memo5 = $this->MstGrandDepClCdRelation->find('all',
        array(
            'fields' => array('MstGrandDepClCdRelation.modified'),
            'order' => array('MstGrandDepClCdRelation.modified desc'),
            'limit' => 1,
            'recursive'=>-1
          )
        );    
        $this->set('Memo5',$memo5);
        
        $memo6 = $this->MstGrandUnit->find('all',
        array(
            'fields' => array('MstGrandUnit.modified'),
            'order' => array('MstGrandUnit.modified desc'),
            'limit' => 1,
            'recursive'=>-1
          )
        );    
        $this->set('Memo6',$memo6);
        
        $memo7 = $this->MstGrandMatPrice->find('all',
        array(
            'fields' => array('MstGrandMatPrice.modified'),
            'order' => array('MstGrandMatPrice.modified desc'),
            'limit' => 1,
            'recursive'=>-1
          )
        );    
        $this->set('Memo7',$memo7);

        $memo8 = $this->MstGrandMacmapClass->find('all',
        array(
            'fields' => array('MstGrandMacmapClass.modified'),
            'order' => array('MstGrandMacmapClass.modified desc'),
            'limit' => 1,
            'recursive'=>-1
          )
        );    
        $this->set('Memo8',$memo8);
        
        $memo9 = $this->MstGrandDepClCd->find('all',
        array(
            'fields' => array('MstGrandDepClCd.modified'),
            'order' => array('MstGrandDepClCd.modified desc'),
            'limit' => 1,
            'recursive'=>-1
          )
        ); 
        $this->set('Memo9',$memo9);
        
        $this->render('import_csv');
    }

    /**
     * CSV取込メイン処理
     */
    function import_csv_result() {
        $this->request->data['now'] = date('Y/m/d H:i:s');
        //ファイルアップ
        $_upfile = $this->request->data['result']['tmp_name'];
        $_fileName = '../tmp/'.date('YmdHi').'_mst.csv';
        if (is_uploaded_file($_upfile)){
            move_uploaded_file($_upfile, mb_convert_encoding($_fileName, 'UTF-8', 'SJIS-win'));
            chmod($_fileName, 0644);
        }else{
            $this->Session->setFlash('ファイルが読み込めませんでした。CSVファイルを確認してください。', 'growl', array('type'=>'error') );
            $this->redirect('import_csv');
            exit(0);
        }

        //アップロードしたCSVデータ文字列として読み込む
        $_csvdata = mb_convert_encoding(file_get_contents ($_fileName), 'UTF-8',"SJIS-win");
        // 配列変換
        $csv_array = $this->utils->input_csv($_csvdata);

        unlink($_fileName);
        
        switch($this->request->data['csv']['type']){
          case 1 ://MSC商品
            $map = array(
                '更新区分'           => 'uptype',
                'MSC商品ｺｰﾄﾞ'        => 'matcd',
                'ﾒｰｶｰｺｰﾄﾞ'           => 'makcocd',
                '商品名(全角)'       => 'matname',
                '商品名(半角)'       => 'matnamsgl',
                '規格(全角)'         => 'sizdimnam',
                '規格(半角)'         => 'sizdimnamsgl',
                'ﾒｰｶｰ商品ｺｰﾄﾞ'       => 'mkmatcd',
                'ＪＡＮｺｰﾄﾞ'         => 'stdjancd',
                '毒劇区分'           => 'poisclas',
                '保管区分'           => 'storclas',
                'ｸﾗｽ分類'            => 'hemiclas',
                '生物由来製品区分'   => 'biomat',
                '税区分'             => 'taxclas',
                '備考'               => 'remarks',
                '最終確認日'         => 'datchkdate',
                '登録実施者ID'       => 'datentstaff',
                '登録実施日'         => 'datentdate',
                '修正実施者ID'       => 'datmodstaff',
                '修正実施日'         => 'datmoddate',
                '中止日'             => 'stpmfgdat',
                '情報更新日'         => 'datupddate',
                '中止理由区分'       => 'stpreasonclas',
                '薬事法承認番号'     => 'medapno',
                '商品グループ区分'   => 'matgrpclas',
                '移行先MSC商品コード'=> 'tranmatcd'
                );                
            $type_name = 'MSC商品'; 
            break;
          case 2 ://保険適用
            $map = array(
                '更新区分'           => 'uptype',
                'MSC商品ｺｰﾄﾞ'        => 'matcd',
                '有効期間開始日'     => 'startdate',
                '有効期間終了日'     => 'enddate',
                '保険適用可否'       => 'insappr',
                '保険請求区分'       => 'inschrgclas',
                '備考'               => 'remarks',
                '登録実施者ID'       => 'datentstaff',
                '登録実施日'         => 'datentdate',
                '修正実施者ID'       => 'datmodstaff',
                '修正実施日'         => 'datmoddate'
                );
            $type_name = '保険適用'; 
            break;
          case 3 ://メーカー
            $map = array(
                '更新区分'           => 'uptype',
                'ﾒｰｶｰｺｰﾄﾞ'           => 'makcocd',
                'ﾒｰｶｰ連番'           => 'makseqn',
                'ﾒｰｶｰ名称（正式）'   => 'maknamfl',
                'ﾒｰｶｰ名称（略式）'   => 'makcalnam',
                'ﾒｰｶｰ名称（カナ）'   => 'makkannam',
                '事業部名称'         => 'depfulnam',
                '事業部略称'         => 'depcalnam',
                '事業部名称（カナ）' => 'depnamkana',
                '使用禁止日'         => 'mktcostpusedate',
                '使用禁止区分'       => 'mktcostpuseclas',
                '備考'               => 'remarks',
                '登録実施者ID'       => 'datentstaff',
                '登録実施日'         => 'datentdate',
                '修正実施者ID'       => 'datmodstaff',
                '修正実施日'         => 'datmoddate',
                'ﾒｼﾞｬｰ区分'          => 'majorclas',
                '商品有無区分'       => 'matclas'
                );
            $type_name = 'メーカー'; 
            break;
          case 4 ://レセプト電算連携
            $map = array(
                '更新区分'           => 'uptype',
                'MSC商品ｺｰﾄﾞ'        => 'matcd',
                '有効期間開始日'     => 'startdate',
                '有効期間終了日'     => 'enddate',
                'ﾚｾﾌﾟﾄ電算ｺｰﾄﾞ'      => 'rezcd',
                '備考'               => 'remarks',
                '登録実施者ID'       => 'datentstaff',
                '登録実施日'         => 'datentdate',
                '修正実施者ID'       => 'datmodstaff',
                '修正実施日'         => 'datmoddate'
                );
            $type_name = 'レセプト電算連携'; 
            break;
          case 5 ://償還連携
            $map = array(
                '更新区分'           => 'uptype',
                'MSC商品ｺｰﾄﾞ'        => 'matcd',
                '有効期間開始日'     => 'startdate',
                '有効期間終了日'     => 'enddate',
                '償還ｺｰﾄﾞ'           => 'depclcd',
                '係数'               => 'depclcoef',
                '入数'               => 'depclqty',
                '備考'               => 'remarks',
                '登録実施者ID'       => 'datentstaff',
                '登録実施日'         => 'datentdate',
                '修正実施者ID'       => 'datmodstaff',
                '修正実施日'         => 'datmoddate'
                );
            $type_name = '償還連携'; 
            break;
          case 6 ://単位
            $map = array(
                '更新区分'           => 'uptype',
                '単位ｺｰﾄﾞ'           => 'unitcd',
                '単位名称'           => 'unitnam',
                '備考'               => 'remarks',
                '登録実施者ID'       => 'datentstaff',
                '登録実施日'         => 'datentdate',
                '修正実施者ID'       => 'datmodstaff',
                '修正実施日'         => 'datmoddate'
                );
            $type_name = '単位'; 
            break;
          case 7 ://定価
            $map = array(
                '更新区分'             => 'uptype',
                'MSC商品ｺｰﾄﾞ'          => 'matcd',
                '有効期間開始日'       => 'startdate',
                '有効期間終了日'       => 'enddate',
                '最小販売単位識別ｺｰﾄﾞ' => 'minselqtycd',
                '入数１'               => 'qty1',
                '単位ｺｰﾄﾞ１'           => 'unit1',
                '定価１'               => 'prc1',
                '入数２'               => 'qty2',
                '単位ｺｰﾄﾞ２'           => 'unit2',
                '定価２'               => 'prc2',
                '入数３'               => 'qty3',
                '単位ｺｰﾄﾞ３'           => 'unit3',
                '定価３'               => 'prc3',
                '入数４'               => 'qty4',
                '単位ｺｰﾄﾞ４'           => 'unit4',
                '定価４'               => 'prc4',
                '入数５'               => 'qty5',
                '単位ｺｰﾄﾞ５'           => 'unit5',
                '定価５'               => 'prc5',
                '入数６'               => 'qty6',
                '単位ｺｰﾄﾞ６'           => 'unit6',
                '定価６'               => 'prc6',
                '備考'                 => 'remarks',
                '登録実施者ID'         => 'datentstaff',
                '登録実施日'           => 'datentdate',
                '修正実施者ID'         => 'datmodstaff',
                '修正実施日'           => 'datmoddate',
                'オープン価格区分'     => 'openprcclas'
                );
            $type_name = '定価'; 
            break;
          case 8 ://MSC分類
            $map = array(
                '更新区分'              => 'uptype',
                'MSC商品ｺｰﾄﾞ'           => 'matcd',
                'MSC分類ｺｰﾄﾞ'           => 'mapclcd',
                'MSC分類ｺｰﾄﾞ名（正式）' => 'mscnam',
                'MSC分類ｺｰﾄﾞ名（略式）' => 'msccalnam',
                'MSC分類階層ｺｰﾄﾞ1'      => 'mscclasscd1',
                'MSC分類階層ｺｰﾄﾞ1名'    => 'mscclass1',
                'MSC分類階層ｺｰﾄﾞ2'      => 'mscclasscd2',
                'MSC分類階層ｺｰﾄﾞ2名'    => 'mscclass2',
                'MSC分類階層ｺｰﾄﾞ3'      => 'mscclasscd3',
                'MSC分類階層ｺｰﾄﾞ3名'    => 'mscclass3',
                'MSC分類階層ｺｰﾄﾞ4'      => 'mscclasscd4',
                'MSC分類階層ｺｰﾄﾞ4名'    => 'mscclass4',
                'MSC分類階層ｺｰﾄﾞ5'      => 'mscclasscd5',
                'MSC分類階層ｺｰﾄﾞ5名'    => 'mscclass5',
                'MSC分類階層ｺｰﾄﾞ6'      => 'mscclasscd6',
                'MSC分類階層ｺｰﾄﾞ6名'    => 'mscclass6',
                'MSC分類階層ｺｰﾄﾞ7'      => 'mscclasscd7',
                'MSC分類階層ｺｰﾄﾞ7名'    => 'mscclass7',
                'MSC分類階層ｺｰﾄﾞ8'      => 'mscclasscd8',
                'MSC分類階層ｺｰﾄﾞ8名'    => 'mscclass8',
                'MSC分類階層ｺｰﾄﾞ9'      => 'mscclasscd9',
                'MSC分類階層ｺｰﾄﾞ9名'    => 'mscclass9',
                'MSC分類階層ｺｰﾄﾞ10'     => 'mscclasscd10',
                'MSC分類階層ｺｰﾄﾞ10名'   => 'mscclass10',
                'MSC分析用入数'         => 'mapclqty',
                '備考'                  => 'remarks',
                '登録実施者ID'          => 'datentstaff',
                '登録実施日'            => 'datentdate',
                '修正実施者ID'          => 'datmodstaff',
                '修正実施日'            => 'datmoddate'
                );
            $type_name = 'MSC分類'; 
            break;
          case 9 ://償還
            $map = array(
                '更新区分'           => 'uptype',
                '償還ｺｰﾄﾞ'           => 'depclcd',
                '有効期間開始日'     => 'startdate',
                '有効期間終了日'     => 'enddate',
                '償還名'             => 'depclcalname',
                '償還価格'           => 'retprc',
                '償還記号'           => 'depclmrk',
                '償還階層ｺｰﾄﾞ1'      => 'depcllevcd1',
                '償還階層名1'        => 'depcllevnam1',
                '償還階層ｺｰﾄﾞ2'      => 'depcllevcd2',
                '償還階層名2'        => 'depcllevnam2',
                '償還階層ｺｰﾄﾞ3'      => 'depcllevcd3',
                '償還階層名3'        => 'depcllevnam3',
                '償還階層ｺｰﾄﾞ4'      => 'depcllevcd4',
                '償還階層名4'        => 'depcllevnam4',
                '償還階層ｺｰﾄﾞ5'      => 'depcllevcd5',
                '償還階層名5'        => 'depcllevnam5',
                '償還階層ｺｰﾄﾞ6'      => 'depcllevcd6',
                '償還階層名6'        => 'depcllevnam6',
                '償還階層ｺｰﾄﾞ7'      => 'depcllevcd7',
                '償還階層名7'        => 'depcllevnam7',
                '償還階層ｺｰﾄﾞ8'      => 'depcllevcd8',
                '償還階層名8'        => 'depcllevnam8',
                '償還階層ｺｰﾄﾞ9'      => 'depcllevcd9',
                '償還階層名9'        => 'depcllevnam9',
                '償還階層ｺｰﾄﾞ10'     => 'depcllevcd10',
                '償還階層名10'       => 'depcllevnam10',
                '備考'               => 'remarks',
                '登録実施者ID'       => 'datentstaff',
                '登録実施日'         => 'datentdate',
                '修正実施者ID'       => 'datmdstaff',
                '修正実施日'         => 'datmddate',
                '償還単位'           => 'depclunit'
                );
            $type_name = '償還'; 
            break;
        }
        
        //UPファイルを配列に入れる
        $mapped_csv = $this->_csvMapping($csv_array, $map);
        if($mapped_csv == false){
            //マッピングに失敗する場合、項目名が変更されているのでエラーにする
            $this->Session->setFlash('項目名を変更している場合取り込めません。確認してください。', 'growl', array('type'=>'error') );
            $this->redirect('import_csv');
            exit(0);
        }

        //各登録処理に飛ぶ
        switch($this->request->data['csv']['type']){
          case 1 ://MSC商品
            $this->saveKyoMaterial($mapped_csv);
            break;
          case 2 ://保険適用
            $this->saveInsAppr($mapped_csv);
            break;
          case 3 ://メーカー
            $this->saveMktCo($mapped_csv);
            //$this->saveMstDealer($mapped_csv);
            break;
          case 4 ://レセプト電算連携
            $this->saveRezCdRelation($mapped_csv);
            break;
          case 5 ://償還連携
            $this->saveDepClCdRelation($mapped_csv);
            break;
          case 6 ://単位
            $this->saveUnit($mapped_csv);
            break;
          case 7 ://定価
            $this->saveMatPrice($mapped_csv);
            break;
          case 8 ://MSC分類
            $this->saveMacmapClass($mapped_csv);
            break;
          case 9 ://償還
            $this->saveDepClCd($mapped_csv);
            $this->saveMstInsuranceClaim($mapped_csv);
            break;
        }
        
        //表示用
        $this->set('type_name' , $type_name);
    }
    

    private function _csvMapping($csv_array, $map){
        $title_row = $csv_array[0];
        $mapped_title = array();
        $mapped_csv = array();
        foreach($title_row as $key => $title){
            if(array_key_exists ($title,$map)){
                $mapped_title[$key] = $map[$title];
            }
        }

        //ヘッダ項目名称が変更されている場合NG
        if(count($mapped_title) != count($map)){
            return false;
        }
        foreach($csv_array as $row_key => $row_data){
            //空項目のみの行は無視
            if( count(array_merge(array(), array_diff($row_data, array("")))) <> 0 ){
                //ヘッダ項目数 < データ項目数だった場合の対応
                $row_data2 = array_slice( $row_data , 0 , count($mapped_title));
                if($row_key != 0 && is_array($row_data2) && count($row_data2) == count($mapped_title)){
                    $mapped_csv[$row_key] = array_combine($mapped_title,$row_data2);
                }
            }
        }
        return $mapped_csv;
    }
    
    /**
     * CSV取込登録処理
     * グランドマスタ
     */
    
    private function saveMktCo($data){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        //トランザクション開始
        $this->MstGrandMktCo->begin();
        
        foreach($data as $d){
            $count++;
            $save_data = array();
            $new_flg = false;
            //データチェック
            if($this->preCheckMktCo($d , $count)){
                //ﾒｰｶｰｺｰﾄﾞは新規登録か？
                $id = $this->checkMakCoCd($d['makcocd']);
                
                if(empty($id)){
                    //新規登録の場合
                    $new_flg = true ;
                    $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
                    $save_data['created'] = $this->request->data['now'];
                }else{
                    //更新の場合
                    $save_data['id'] = $id;
                }
                $save_data['makcocd']           = $d['makcocd'];
                if(!is_null($d['makseqn'])){
                    $save_data['makseqn']       = $d['makseqn'];
                }
                $save_data['maknamfl']          = $d['maknamfl'];
                $save_data['makcalnam']         = mb_convert_kana($d['makcalnam'], "ka");
                
                $save_data['makkannam']         = $d['makkannam'];
                $save_data['depfulnam']         = $d['depfulnam'];
                $save_data['depcalnam']         = $d['depcalnam'];
                $save_data['depnamkana']        = $d['depnamkana'];
                $save_data['mktcostpusedate']   = $d['mktcostpusedate'];
                $save_data['mktcostpuseclas']   = $d['mktcostpuseclas'];
                $save_data['remarks']           = $d['remarks'];
                
                if(!is_null($d['datentstaff'])){
                    $save_data['datentstaff']   = $d['datentstaff'];
                }else{
                    $save_data['datentstaff']   = '';
                }
                
                if(!is_null($d['datentdate'])){
                    $save_data['datentdate']    = $d['datentdate'];
                }else{
                    $save_data['datentdate']   = '';
                }
                
                if(!is_null($d['datmodstaff'])){
                    $save_data['datmodstaff']   = $d['datmodstaff'];
                }else{
                    $save_data['datmodstaff']   = '';
                }
                
                if(!is_null($d['datmoddate'])){
                    $save_data['datmoddate']    = $d['datmoddate'];
                }else{
                    $save_data['datmoddate']   = '';
                }
                
                if(!is_null($d['majorclas'])){
                    $save_data['majorclas']     = $d['majorclas'];
                }else{
                    $save_data['majorclas']   = '';
                }
                
                if(!is_null($d['matclas'])){
                    $save_data['matclas']       = $d['matclas'];
                }else{
                    $save_data['matclas']   = '';
                }
                
                if($d['uptype']=="2"){
                   $save_data['is_deleted']     = true;
                }
                
                $save_data['modifier']          = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']          = $this->request->data['now'];
                
                $this->MstGrandMktCo->create();

                if(!$this->MstGrandMktCo->save($save_data)){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => 'メーカーマスタへのメーカー情報の登録に失敗しました。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if($err_cnt>0){
            //ロールバック
            $this->MstGrandMktCo->rollback();
        }else{
            //コミット
            $this->MstGrandMktCo->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }
    
    private function saveInsAppr($data){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        //トランザクション開始
        $this->MstGrandInsAppr->begin();
        
        foreach($data as $d){
            $count++;
            $save_data = array();
            $new_flg = false;
            //データチェック
            if($this->preCheckInsAppr($d , $count)){
                //MSC商品ｺｰﾄﾞは新規登録か？
                $id = $this->checkInsApprMatCd($d['matcd']);
                
                if(empty($id)){
                    //新規登録の場合
                    $new_flg = true ;
                    $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
                    $save_data['created'] = $this->request->data['now'];
                }else{
                    //更新の場合
                    $save_data['id'] = $id;
                }
                $save_data['matcd']             = $d['matcd'];
                $save_data['startdate']         = $d['startdate'];
                $save_data['enddate']           = $d['enddate'];
                
                if(!is_null($d['insappr'])){
                    $save_data['insappr']       = $d['insappr'];
                }else{
                    $save_data['insappr']   = '';
                }
                
                if(!is_null($d['inschrgclas'])){
                    $save_data['inschrgclas']   = $d['inschrgclas'];
                }else{
                    $save_data['inschrgclas']   = '';
                }
                
                $save_data['remarks']           = $d['remarks'];
                
                if(!is_null($d['datentstaff'])){
                    $save_data['datentstaff']   = $d['datentstaff'];
                }else{
                    $save_data['datentstaff']   = '';
                }
                
                if(!is_null($d['datentdate'])){
                    $save_data['datentdate']    = $d['datentdate'];
                }else{
                    $save_data['datentdate']   = '';
                }
                
                if(!is_null($d['datmodstaff'])){
                    $save_data['datmodstaff']   = $d['datmodstaff'];
                }else{
                    $save_data['datmodstaff']   = '';
                }
                
                if(!is_null($d['datmoddate'])){
                    $save_data['datmoddate']    = $d['datmoddate'];
                }else{
                    $save_data['datmoddate']   = '';
                }
                
                if($d['uptype']=="2"){
                $save_data['is_deleted']        = true;
                }
                
                $save_data['modifier']          = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']          = $this->request->data['now'];
                
                $this->MstGrandInsAppr->create();

                if(!$this->MstGrandInsAppr->save($save_data)){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '保険適用情報の登録に失敗しました。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if($err_cnt>0){
            //ロールバック
            $this->MstGrandInsAppr->rollback();
        }else{
            //コミット
            $this->MstGrandInsAppr->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }
     
    private function saveKyoMaterial($data){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        //トランザクション開始
        $this->MstGrandKyoMaterial->begin();
        
        foreach($data as $d){
            $count++;
            $save_data = array();
            $new_flg = false;
            //データチェック
            if($this->preCheckKyoMaterial($d , $count)){
                //MSC商品ｺｰﾄﾞは新規登録か？
                $id = $this->checkKyoMaterialMatCd($d['matcd']);
                
                if(empty($id)){
                    //新規登録の場合
                    $new_flg = true ;
                    $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
                    $save_data['created'] = $this->request->data['now'];
                }else{
                    //更新の場合
                    $save_data['id'] = $id;
                }
                $save_data['matcd']             = $d['matcd'];

                if(!is_null($d['makcocd'])){
                    $save_data['makcocd']       = $d['makcocd'];
                }else{
                    $save_data['makcocd']   = '';
                }
                
                if(!is_null($d['matname'])){
                    $save_data['matname']       = $d['matname'];
                }else{
                    $save_data['matname']   = '';
                }
                
                $save_data['matnamsgl']         = $d['matnamsgl'];
                $save_data['sizdimnam']         = $d['sizdimnam'];
                $save_data['sizdimnamsgl']      = $d['sizdimnamsgl'];
                $save_data['mkmatcd']           = $d['mkmatcd'];
                $save_data['stdjancd']          = $d['stdjancd'];
                
                if(!is_null($d['poisclas'])){
                    $save_data['poisclas']      = $d['poisclas'];
                }else{
                    $save_data['poisclas']   = '';
                }
                
                if(!is_null($d['storclas'])){
                    $save_data['storclas']      = $d['storclas'];
                }else{
                    $save_data['storclas']   = '';
                }
                
                if(!is_null($d['hemiclas'])){
                    $save_data['hemiclas']      = $d['hemiclas'];
                }else{
                    $save_data['hemiclas']   = '';
                }
                
                if(!is_null($d['biomat'])){
                    $save_data['biomat']        = $d['biomat'];
                }else{
                    $save_data['biomat']   = '';
                }
                
                if(!is_null($d['taxclas'])){
                   $save_data['taxclas']        = $d['taxclas'];
                }else{
                    $save_data['taxclas']   = '';
                }
                
                   $save_data['remarks']        = $d['remarks'];
                
                if(!is_null($d['datchkdate'])){
                   $save_data['datchkdate']     = $d['datchkdate'];
                }else{
                    $save_data['datchkdate']   = '';
                }
                
                if(!is_null($d['datentstaff'])){
                    $save_data['datentstaff'] = $d['datentstaff'];
                }else{
                    $save_data['datentstaff'] = '';
                }
                
                if(!is_null($d['datentdate'])){
                    $save_data['datentdate'] = $d['datentdate'];
                }else{
                    $save_data['datentdate'] = '';
                }
                
                if(!is_null($d['datmodstaff'])){
                    $save_data['datmodstaff'] = $d['datmodstaff'];
                }else{
                    $save_data['datmodstaff'] = '';
                }
                
                if(!is_null($d['datmoddate'])){
                    $save_data['datmoddate'] = $d['datmoddate'];
                }else{
                    $save_data['datmoddate'] = '';
                }
                
                $save_data['stpmfgdat']         = $d['stpmfgdat'];
                
                if(!is_null($d['datupddate'])){
                    $save_data['datupddate'] = $d['datupddate'];
                }else{
                    $save_data['datupddate'] = '';
                }
                
                $save_data['stpreasonclas']  = $d['stpreasonclas'];
                $save_data['medapno']        = $d['medapno'];
                $save_data['matgrpclas']     = $d['matgrpclas'];
                $save_data['tranmatcd']      = $d['tranmatcd'];
                
                //if($d['uptype']=="2"){
                //   $save_data['is_deleted']  = true;
                //}
                
                $save_data['modifier']          = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']          = $this->request->data['now'];
                
                $this->MstGrandKyoMaterial->create();
                
                if(!$this->MstGrandKyoMaterial->save($save_data)){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '商品情報の登録に失敗しました。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if($err_cnt>0){
            //ロールバック
            $this->MstGrandKyoMaterial->rollback();
        }else{
            //コミット
            $this->MstGrandKyoMaterial->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }
     
    private function saveRezCdRelation($data){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        //トランザクション開始
        $this->MstGrandRezCdRelation->begin();
        
        
        foreach($data as $d){
            $count++;
            $save_data = array();
            $new_flg = false;
            //データチェック
            if($this->preCheckRezCdRelation($d , $count)){
                //ﾒｰｶｰｺｰﾄﾞは新規登録か？
                $id = $this->checkRezCdRelationMatCd($d['matcd']);
                
                if(empty($id)){
                    //新規登録の場合
                    $new_flg = true ;
                    $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
                    $save_data['created'] = $this->request->data['now'];
                }else{
                    //更新の場合
                    $save_data['id'] = $id;
                }
                
                $save_data['matcd']            = $d['matcd'];
                $save_data['startdate']        = $d['startdate'];
                $save_data['enddate']          = $d['enddate'];
                                
                if(!is_null($d['rezcd'])){
                    $save_data['rezcd']        = $d['rezcd'];
                }else{
                    $save_data['rezcd']   = '';
                }
                
                $save_data['remarks']          = $d['remarks'];
                
                if(!is_null($d['datentstaff'])){
                    $save_data['datentstaff']  = $d['datentstaff'];
                }else{
                    $save_data['datentstaff']   = '';
                }
                
                if(!is_null($d['datentdate'])){
                    $save_data['datentdate']   = $d['datentdate'];
                }else{
                    $save_data['datentdate']   = '';
                }
                
                if(!is_null($d['datmodstaff'])){
                    $save_data['datmodstaff']  = $d['datmodstaff'];
                }else{
                    $save_data['datmodstaff']   = '';
                }
                
                if(!is_null($d['datmoddate'])){
                    $save_data['datmoddate']   = $d['datmoddate'];
                }else{
                    $save_data['datmoddate']   = '';
                }
                
                if($d['uptype']=="2"){
                   $save_data['is_deleted']     = true;
                }
                
                $save_data['modifier']         = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']         = $this->request->data['now'];
                
                $this->MstGrandRezCdRelation->create();

                if(!$this->MstGrandRezCdRelation->save($save_data)){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => 'レセプト電算連携情報の登録に失敗しました。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if($err_cnt>0){
            //ロールバック
            $this->MstGrandRezCdRelation->rollback();
        }else{
            //コミット
            $this->MstGrandRezCdRelation->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }
     
    private function saveDepClCdRelation($data){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        //トランザクション開始
        $this->MstGrandDepClCdRelation->begin();
        
        foreach($data as $d){
            $count++;
            $save_data = array();
            $new_flg = false;
            //データチェック
            if($this->preCheckDepClCdRelation($d , $count)){
                //ﾒｰｶｰｺｰﾄﾞは新規登録か？
                $id = $this->checkDepClCdRelationMatCd($d['depclcd']);
                
                if(empty($id)){
                    //新規登録の場合
                    $new_flg = true ;
                    $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
                    $save_data['created'] = $this->request->data['now'];
                }else{
                    //更新の場合
                    $save_data['id'] = $id;
                }
                $save_data['matcd']            = $d['matcd'];
                $save_data['startdate']        = $d['startdate'];
                $save_data['enddate']          = $d['enddate'];
                
                if(!is_null($d['depclcd'])){
                    $save_data['depclcd']      = $d['depclcd'];
                }else{
                    $save_data['depclcd']   = '';
                }
                
                if(!is_null($d['depclcoef'])){
                    $save_data['depclcoef']    = $d['depclcoef'];
                }else{
                    $save_data['depclcoef']   = '';
                }
                
                if(!is_null($d['depclqty'])){
                    $save_data['depclqty']     = $d['depclqty'];
                }else{
                    $save_data['depclqty']   = '';
                }
                
                $save_data['remarks']          = $d['remarks'];
                
                if(!is_null($d['datentstaff'])){
                    $save_data['datentstaff']  = $d['datentstaff'];
                }else{
                    $save_data['datentstaff']   = '';
                }
                
                if(!is_null($d['datentdate'])){
                    $save_data['datentdate']   = $d['datentdate'];
                }else{
                    $save_data['datentdate']   = '';
                }
                
                if(!is_null($d['datmodstaff'])){
                    $save_data['datmodstaff']  = $d['datmodstaff'];
                }else{
                    $save_data['datmodstaff']   = '';
                }
                
                if(!is_null($d['datmoddate'])){
                    $save_data['datmoddate']   = $d['datmoddate'];
                }else{
                    $save_data['datmoddate']   = '';
                }
                
                if($d['uptype']=="2"){
                $save_data['is_deleted']     = true;
                }
                
                $save_data['modifier']         = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']         = $this->request->data['now'];
                
                $this->MstGrandDepClCdRelation->create();

                if(!$this->MstGrandDepClCdRelation->save($save_data)){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '償還連携情報の登録に失敗しました。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if($err_cnt>0){
            //ロールバック
            $this->MstGrandDepClCdRelation->rollback();
        }else{
            //コミット
            $this->MstGrandDepClCdRelation->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }

    private function saveMstInsuranceClaim($data){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        //トランザクション開始
        $this->MstInsuranceClaim->begin();
        
        foreach($data as $d){
            $count++;
            $save_data = array();
            $new_flg = false;
            //データチェック
            if($this->preCheckDepClCdRelation($d , $count)){
                //ﾒｰｶｰｺｰﾄﾞは新規登録か？
                $id = $this->checkMstInsuranceClaim($d['depclcd']);
                
                if(empty($id)){
                    //新規登録の場合
                    $new_flg = true ;
                    $save_data['created'] = $this->request->data['now'];
                }
                /*else{
                    //更新の場合
                    $save_data['id'] = $id;
                }*/
                
                $save_data['insurance_claim_code']   = $d['depclcd'];
                $save_data['insurance_claim_name_s'] = $d['depclcalname'];
                $save_data['insurance_claim_name']   = $d['depclcalname'];
                
                $save_data['before_gazette_code']    = '';
                $save_data['gazette_code']           = '';
                $save_data['before_price']           = '';
                $save_data['price']                  = '';
                $save_data['converted_unit']         = '';
                $save_data['is_adopt_division']      = '';
                $save_data['enforce_date']           = '';
                $save_data['standard_code']          = '';
                $save_data['gazette_code_info']      = '';
                $save_data['government_code']        = '';
                $save_data['recital']                = '';
                
                if($d['uptype']=="2"){
                $save_data['is_deleted']     = true;
                }
                
                $save_data['modified']         = $this->request->data['now'];
                
                $this->MstInsuranceClaim->create();

                if(!$this->MstInsuranceClaim->save($save_data)){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '保険請求マスタへの償還情報の登録に失敗しました。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }
        if($err_cnt>0){
            //ロールバック
            $this->MstInsuranceClaim->rollback();
        }else{
            //コミット
            $this->MstInsuranceClaim->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }

    private function saveUnit($data){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        //トランザクション開始
        $this->MstGrandUnit->begin();
        
        
        foreach($data as $d){
            $count++;
            $save_data = array();
            $new_flg = false;
            //データチェック
            if($this->preCheckUnit($d , $count)){
                //単位ｺｰﾄﾞは新規登録か？
                $id = $this->checkUnitCd($d['unitcd']);
                
                if(empty($id)){
                    //新規登録の場合
                    $new_flg = true ;
                    $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
                    $save_data['created'] = $this->request->data['now'];
                }else{
                    //更新の場合
                    $save_data['id'] = $id;
                }
                
                $save_data['unitcd']           = $d['unitcd'];                                

                if(!is_null($d['unitnam'])){
                    $save_data['unitnam']      = $d['unitnam'];
                }else{
                    $save_data['unitnam']   = '';
                }
                
                $save_data['remarks']          = $d['remarks'];
                $save_data['datentstaff']      = $d['datentstaff'];
                $save_data['datentdate']       = $d['datentdate'];
                $save_data['datmodstaff']      = $d['datmodstaff'];
                $save_data['datmoddate']       = $d['datmoddate'];
                
                if($d['uptype']=="2"){
                   $save_data['is_deleted']    = true;
                }
                
                $save_data['modifier']         = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']         = $this->request->data['now'];
                
                $this->MstGrandUnit->create();

                if(!$this->MstGrandUnit->save($save_data)){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '単位情報の登録に失敗しました。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if($err_cnt>0){
            //ロールバック
            $this->MstGrandUnit->rollback();
        }else{
            //コミット
            $this->MstGrandUnit->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }
     
    private function saveMatPrice($data){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        //トランザクション開始
        $this->MstGrandMatPrice->begin();
        
        
        foreach($data as $d){
            $count++;
            $save_data = array();
            $new_flg = false;
            //データチェック
            if($this->preCheckMatPrice($d , $count)){
                //定価は新規登録か？
                $id = $this->checkMatPriceMatCd($d['unit1']);
                
                if(empty($id)){
                    //新規登録の場合
                    $new_flg = true ;
                    $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
                    $save_data['created'] = $this->request->data['now'];
                }else{
                    //更新の場合
                    $save_data['id'] = $id;
                }
                $save_data['matcd']            = $d['matcd'];   
                $save_data['startdate']        = $d['startdate'];
                $save_data['enddate']          = $d['enddate'];                             
                
                $save_data['minselqtycd']  = $d['minselqtycd'];
                
                if(!is_null($d['qty1'])){
                    $save_data['qty1']         = $d['qty1'];
                }else{
                    $save_data['qty1']   = '';
                }
                
                if(!is_null($d['unit1'])){
                    $save_data['unit1']        = $d['unit1'];
                }else{
                    $save_data['unit1']   = '';
                }
                
                if(!is_null($d['prc1'])){
                    $save_data['prc1']         = $d['prc1'];
                }else{
                    $save_data['prc1']   = '';
                }
                
                $save_data['qty2']             = $d['qty2'];
                $save_data['unit2']            = $d['unit2'];
                $save_data['prc2']             = $d['prc2'];
                $save_data['qty3']             = $d['qty3'];
                $save_data['unit3']            = $d['unit3'];
                $save_data['prc3']             = $d['prc3'];
                $save_data['qty4']             = $d['qty4'];
                $save_data['unit4']            = $d['unit4'];
                $save_data['prc4']             = $d['prc4'];
                $save_data['qty5']             = $d['qty5'];
                $save_data['unit5']            = $d['unit5'];
                $save_data['prc5']             = $d['prc5'];
                $save_data['qty6']             = $d['qty6'];
                $save_data['unit6']            = $d['unit6'];
                $save_data['prc6']             = $d['prc6'];
                $save_data['remarks']          = $d['remarks'];
                
                if(!is_null($d['datentstaff'])){
                    $save_data['datentstaff']  = $d['datentstaff'];
                }else{
                    $save_data['datentstaff']   = '';
                }
                
                if(!is_null($d['datentdate'])){
                    $save_data['datentdate']   = $d['datentdate'];
                }else{
                    $save_data['datentdate']   = '';
                }
                
                if(!is_null($d['datmodstaff'])){
                    $save_data['datmodstaff']  = $d['datmodstaff'];
                }else{
                    $save_data['datmodstaff']   = '';
                }
                
                if(!is_null($d['datmoddate'])){
                    $save_data['datmoddate']   = $d['datmoddate'];
                }else{
                    $save_data['datmoddate']   = '';
                }
                
                if(!is_null($d['openprcclas'])){
                    $save_data['openprcclas']  = $d['openprcclas'];
                }else{
                    $save_data['openprcclas']   = '';
                }
                
                if($d['uptype']=="2"){
                   $save_data['is_deleted']     = true;
                }else{
                    $save_data['unitnam']   = '';
                }
                
                $save_data['modifier']         = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']         = $this->request->data['now'];
                
                $this->MstGrandMatPrice->create();

                if(!$this->MstGrandMatPrice->save($save_data)){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '定価情報の登録に失敗しました。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if($err_cnt>0){
            //ロールバック
            $this->MstGrandMatPrice->rollback();
        }else{
            //コミット
            $this->MstGrandMatPrice->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }

    private function saveMacmapClass($data){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        //トランザクション開始
        $this->MstGrandMacmapClass->begin();
        
        foreach($data as $d){
            $count++;
            $save_data = array();
            $new_flg = false;
            //データチェック
            if($this->preCheckMacmapClass($d , $count)){
                //商品ｺｰﾄﾞは新規登録か？
                $id = $this->checkMacmapClassMatCd($d['matcd']);
                
                if(empty($id)){
                    //新規登録の場合
                    $new_flg = true ;
                    $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
                    $save_data['created'] = $this->request->data['now'];
                }else{
                    //更新の場合
                    $save_data['id'] = $id;
                }
                $save_data['matcd']            = $d['matcd'];                                

                if(!is_null($d['mapclcd'])){
                    $save_data['mapclcd']      = $d['mapclcd'];
                }else{
                    $save_data['mapclcd']   = '';
                }
                
                $save_data['mscnam']           = $d['mscnam'];
                $save_data['msccalnam']        = $d['msccalnam'];
                $save_data['mscclasscd1']      = $d['mscclasscd1'];
                $save_data['mscclass1']        = $d['mscclass1'];
                $save_data['mscclasscd2']      = $d['mscclasscd2'];
                $save_data['mscclass2']        = $d['mscclass2'];
                $save_data['mscclasscd3']      = $d['mscclasscd3'];
                $save_data['mscclass3']        = $d['mscclass3'];
                $save_data['mscclasscd4']      = $d['mscclasscd4'];
                $save_data['mscclass4']        = $d['mscclass4'];
                $save_data['mscclasscd5']      = $d['mscclasscd5'];
                $save_data['mscclass5']        = $d['mscclass5'];
                $save_data['mscclasscd6']      = $d['mscclasscd6'];
                $save_data['mscclass6']        = $d['mscclass6'];
                $save_data['mscclasscd7']      = $d['mscclasscd7'];
                $save_data['mscclass7']        = $d['mscclass7'];
                $save_data['mscclasscd8']      = $d['mscclasscd8'];
                $save_data['mscclass8']        = $d['mscclass8'];
                $save_data['mscclasscd9']      = $d['mscclasscd9'];
                $save_data['mscclass9']        = $d['mscclass9'];
                $save_data['mscclasscd10']     = $d['mscclasscd10'];
                $save_data['mscclass10']       = $d['mscclass10'];
                $save_data['mapclqty']         = $d['mapclqty'];
                $save_data['remarks']          = $d['remarks'];
                 
                if(!is_null($d['datentstaff'])){
                    $save_data['datentstaff']  = $d['datentstaff'];
                }else{
                    $save_data['datentstaff']   = '';
                }
                
                if(!is_null($d['datentdate'])){
                    $save_data['datentdate']   = $d['datentdate'];
                }else{
                    $save_data['datentdate']   = '';
                }
                
                if(!is_null($d['datmodstaff'])){
                    $save_data['datmodstaff']  = $d['datmodstaff'];
                }else{
                    $save_data['datmodstaff']   = '';
                }
                
                if(!is_null($d['datmoddate'])){
                    $save_data['datmoddate']   = $d['datmoddate'];
                }else{
                    $save_data['datmoddate']   = '';
                }
                
                if($d['uptype']=="2"){
                   $save_data['is_deleted']    = true;
                }
                
                $save_data['modifier']         = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']         = $this->request->data['now'];
                
                $this->MstGrandMacmapClass->create();

                if(!$this->MstGrandMacmapClass->save($save_data)){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => 'MSC分類情報の登録に失敗しました。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if($err_cnt>0){
            //ロールバック
            $this->MstGrandMacmapClass->rollback();
        }else{
            //コミット
            $this->MstGrandMacmapClass->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }

    private function saveDepClCd($data){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        $this->request->data['err_result'] = array();
        $count = 0;
        $err_cnt = 0;
        //トランザクション開始
        $this->MstGrandDepClCd->begin();
        
        foreach($data as $d){
            $count++;
            $save_data = array();
            $new_flg = false;
            //データチェック
            if($this->preCheckDepClCd($d , $count)){
                //商品ｺｰﾄﾞは新規登録か？
                $id = $this->checkDepClCd($d['depclcd']);
                
                if(empty($id)){
                    //新規登録の場合
                    $new_flg = true ;
                    $save_data['creater'] = $this->Session->read('Auth.MstUser.id');
                    $save_data['created'] = $this->request->data['now'];
                }else{
                    //更新の場合
                    $save_data['id'] = $id;
                }
                $save_data['depclcd']          = $d['depclcd'];

                $save_data['startdate']          = $d['startdate'];
                $save_data['enddate']          = $d['enddate'];
                $save_data['depclcalname']     = $d['depclcalname'];
                $save_data['retprc']           = $d['retprc'];
                $save_data['depclmrk']         = $d['depclmrk'];                
                $save_data['depcllevcd1']      = $d['depcllevcd1'];
                $save_data['depcllevnam1']     = $d['depcllevnam1'];
                $save_data['depcllevcd2']      = $d['depcllevcd2'];
                $save_data['depcllevnam2']     = $d['depcllevnam2'];
                $save_data['depcllevcd3']      = $d['depcllevcd3'];
                $save_data['depcllevnam3']     = $d['depcllevnam3'];
                $save_data['depcllevcd4']      = $d['depcllevcd4'];
                $save_data['depcllevnam4']     = $d['depcllevnam4'];
                $save_data['depcllevcd5']      = $d['depcllevcd5'];
                $save_data['depcllevnam5']     = $d['depcllevnam5'];
                $save_data['depcllevcd6']      = $d['depcllevcd6'];
                $save_data['depcllevnam6']     = $d['depcllevnam6'];
                $save_data['depcllevcd7']      = $d['depcllevcd7'];
                $save_data['depcllevnam7']     = $d['depcllevnam7'];
                $save_data['depcllevcd8']      = $d['depcllevcd8'];
                $save_data['depcllevnam8']     = $d['depcllevnam8'];
                $save_data['depcllevcd9']      = $d['depcllevcd9'];
                $save_data['depcllevnam9']     = $d['depcllevnam9'];
                $save_data['depcllevcd10']     = $d['depcllevcd10'];
                $save_data['depcllevnam10']    = $d['depcllevnam10'];
                $save_data['remarks']          = $d['remarks'];
                                 
                if(!is_null($d['datentstaff'])){
                    $save_data['datentstaff']  = $d['datentstaff'];
                }else{
                    $save_data['datentstaff']   = '';
                }
                
                if(!is_null($d['datentdate'])){
                    $save_data['datentdate']   = $d['datentdate'];
                }else{
                    $save_data['datentdate']   = '';
                }
                
                if(!is_null($d['datmdstaff'])){
                    $save_data['datmdstaff']   = $d['datmdstaff'];
                }else{
                    $save_data['datmdstaff']   = '';
                }
                
                if(!is_null($d['datmddate'])){
                    $save_data['datmddate']    = $d['datmddate'];
                }else{
                    $save_data['datmddate']   = '';
                }
                
                if($d['uptype']=="2"){
                   $save_data['is_deleted']    = true;
                }
                
                $save_data['modifier']         = $this->Session->read('Auth.MstUser.id');
                $save_data['modified']         = $this->request->data['now'];
                
                $this->MstGrandDepClCd->create();

                if(!$this->MstGrandDepClCd->save($save_data)){
                    $err_cnt++;
                    $this->request->data['err_result'][] = array($count => '償還マスタへの償還情報の登録に失敗しました。');
                    if($err_cnt >= 100){
                        // 100件以上になったらそれ以上処理しない
                        break;
                    }
                }
            }else{
                $err_cnt++;
                if($err_cnt >= 100){
                    // 100件以上になったらそれ以上処理しない
                    break;
                }
            }
        }

        if($err_cnt>0){
            //ロールバック
            $this->MstGrandDepClCd->rollback();
        }else{
            //コミット
            $this->MstGrandDepClCd->commit();
        }
        $this->set('count' , $count);
        $this->set('err_result' , $this->request->data['err_result']);
    }


    //日付フォーマットチェック
    private function checkDateFormat($date){
           if(preg_match('/^([0-9]{4})[\/]+([0-9]+)[\/]+([0-9]+)$/', $date, $parts)){
               if(checkdate($parts[2], $parts[3], $parts[1])){
                   return true;
               }
           }
           return false;
       }
    
    private function checkMakCoCd($makcocd){
        
        $sql = 'select "id" from mst_grand_mkt_cos where "makcocd" = \''.$makcocd.'\'';
        $ret = $this->MstGrandMktCo->query($sql);
        if(count($ret) != 1 ){
            return false;
        }else{
            return $ret[0][0]['id'];
        }
    }
    
    private function checkMstDealer($makcocd){
        $sql = 'select "id" from mst_dealers where "dealer_code" = \''.$makcocd.'\'';
        $ret = $this->MstDealer->query($sql);
        if(count($ret) != 1 ){
            return false;
        }else{
            return $ret[0][0]['id'];
        }
    }
    
    private function preCheckMktCo($d , $count){
        $return = true;
        return $return;
    }

    private function checkInsApprMatCd($matcd){
        $sql = 'select "id" from mst_grand_ins_apprs where "matcd" = \''.$matcd.'\'';
        $ret = $this->MstGrandInsAppr->query($sql);
        if(count($ret) != 1 ){
            return false;
        }else{
            return $ret[0][0]['id'];
        }
    }
    
    private function preCheckInsAppr($d , $count){
        $return = true;
        return $return;
    }

    private function checkKyoMaterialMatCd($matcd){
        $sql = 'select "id" from mst_grand_kyo_materials where "matcd" = \''.$matcd.'\'';
        $ret = $this->MstGrandKyoMaterial->query($sql);
        if(count($ret) != 1 ){
            return false;
        }else{
            return $ret[0][0]['id'];
        }
    }

    private function preCheckKyoMaterial($d , $count){
        $return = true;
        return $return;
    }

    private function checkRezCdRelationMatCd($matcd){
        $sql = 'select "id" from mst_grand_rez_cd_relations where "matcd" = \''.$matcd.'\'';
        $ret = $this->MstGrandRezCdRelation->query($sql);
        if(count($ret) != 1 ){
            return false;
        }else{
            return $ret[0][0]['id'];
        }
    }

    private function preCheckRezCdRelation($d , $count){
        $return = true;
        return $return;
    }

    private function checkDepClCdRelationMatCd($depclcd){
        $sql = 'select "id" from mst_grand_dep_cl_cd_relations where "depclcd" = \''.$depclcd.'\'';
        $ret = $this->MstGrandDepClCdRelation->query($sql);
        if(count($ret) != 1 ){
            return false;
        }else{
            return $ret[0][0]['id'];
        }
    }
    
    private function checkMstInsuranceClaim($depclcd){
        $sql = 'select "insurance_claim_code" from mst_insurance_claims where "insurance_claim_code" = \''.$depclcd.'\'';
        $ret = $this->MstInsuranceClaim->query($sql);
        if(count($ret) != 1 ){
            return false;
        }else{
            return $ret[0][0]['insurance_claim_code'];
        }
    }

    private function preCheckDepClCdRelation($d , $count){
        $return = true;
        return $return;
    }

    private function checkUnitCd($unitcd){
        $sql = 'select "id" from mst_grand_units where "unitcd" = \''.$unitcd.'\'';
        $ret = $this->MstGrandUnit->query($sql);
        if(count($ret) != 1 ){
            return false;
        }else{
            return $ret[0][0]['id'];
        }
    }

    private function preCheckUnit($d , $count){
        $return = true;
        return $return;
    }

    private function checkMatPriceMatCd($unit1){
        $sql = 'select "id" from mst_grand_mat_prices where "prc1" = \''.$unit1.'\'';
        $ret = $this->MstGrandMatPrice->query($sql);
        if(count($ret) != 1 ){
            return false;
        }else{
            return $ret[0][0]['id'];
        }
    }

    private function preCheckMatPrice($d , $count){
        $return = true;
        return $return;
    }

    private function checkMacmapClassMatCd($matcd){
        $sql = 'select "id" from mst_grand_macmap_classes where "matcd" = \''.$matcd.'\'';
        $ret = $this->MstGrandMacmapClass->query($sql);
        if(count($ret) != 1 ){
            return false;
        }else{
            return $ret[0][0]['id'];
        }
    }

    private function preCheckMacmapClass($d , $count){
        $return = true;
        return $return;
    }

    private function checkDepClCd($depclcd){
        $sql = 'select "id" from mst_grand_dep_cl_cds where "depclcd" = \''.$depclcd.'\'';
        $ret = $this->MstGrandDepClCd->query($sql);
        if(count($ret) != 1 ){
            return false;
        }else{
            return $ret[0][0]['id'];
        }
    }

    private function preCheckDepClCd($d , $count){
        $return = true;
        return $return;
    }
}