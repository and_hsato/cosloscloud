<?php
/**
 * MstFavoriteMenuController
 *
 * @version 1.0.0
 * @since 2013/11/28
 */

class MstFavoriteMenuController extends AppController {
    var $name = 'FavoriteMenu';
    
    /**
     *
     * @var array $uses
     */
    var $uses = array('MstFavoriteView' , 'MstView','MstUser');
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;
    
    /**
     * @var Departments
     */
    var $Departments;
    
    function index(){
        $this->menu_list();
    }


    /**
     * メニュー一覧
     */
    function menu_list($flg=null) {
        if($flg=='update'){
            // 更新処理
            $this->MstFavoriteView->begin();
            $now = date('Y/m/d H:i:s');
            $view_nos = explode(',', $this->request->data['Menu']['tmpid']);

            // 既存はすべて削除フラグを立てる
            // 受注ヘッダ
            $this->MstFavoriteView->create();
            $ret = $this->MstFavoriteView->updateAll(
                array(
                    'MstFavoriteView.is_deleted' => "'true'",
                    'MstFavoriteView.modifier'   => $this->Session->read('Auth.MstUser.id'),
                    'MstFavoriteView.modified'   => "'" . $now . "'"
                    ),
                array(
                    'MstFavoriteView.mst_user_id' => $this->Session->read('Auth.MstUser.id'),
                    ),
                -1
                );
            if(!$ret){
                $this->MstFavoriteView->rollback();
                $this->Session->setFlash('お気に入り編集中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('menu_list');
            }
            
            $i = 1;
            // 新規追加
            if(!empty($this->request->data['Menu']['tmpid'])){
                foreach($view_nos as $v){
                    // 出荷ヘッダ
                    $MstFavoriteView = array(
                        'MstFavoriteView'=> array(
                            'mst_view_id'            => $v,
                            'mst_user_id'            => $this->Session->read('Auth.MstUser.id'),
                            'order_num'              => $i,
                            'is_deleted'             => false,
                            'creater'                => $this->Session->read('Auth.MstUser.id'),
                            'created'                => $now,
                            'modifier'               => $this->Session->read('Auth.MstUser.id'),
                            'modified'               => $now 
                            )
                        );
                    
                    $this->MstFavoriteView->create();
                    // SQL実行
                    if (!$this->MstFavoriteView->save($MstFavoriteView ,  array('validates' => true,'atomic' => false))) {
                        $this->MstFavoriteView->rollback();
                        $this->Session->setFlash('お気に入り編集中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('menu_list');
                    }
                    $i++;
                }
            }
            $this->MstFavoriteView->commit();
            
            // 更新後データをメニュセッションに格納
            $this->AuthSession->updateFavoriteMenus();

            $this->redirect('menu_list');
        }
        // 左:表示可能メニュー一覧
        $sql  ="select";
        $sql .="      e.class";
        $sql .="    , d.name ";
        $sql .="    , d.url ";
        $sql .="    , d.id ";
        $sql .="    , ( case when f.id is null then false else true end ) as disable";
        $sql .="  from";
        $sql .="    mst_users as a ";
        $sql .="    inner join mst_roles as b ";
        $sql .="      on a.mst_role_id = b.id ";
        $sql .="     and b.is_deleted = false ";
        $sql .="    inner join mst_privileges as c ";
        $sql .="      on b.id = c.mst_role_id ";
        $sql .="     and c.is_deleted = false ";
        $sql .="    inner join mst_views as d ";
        $sql .="      on c.view_no = d.view_no ";
        $sql .="     and d.is_deleted = false ";
        $sql .="    inner join mst_view_headers as e ";
        $sql .="      on d.view_header_id = e.id ";
        $sql .="     and e.is_deleted = false ";
        $sql .="     left join mst_favorite_views as f";
        $sql .="      on f.mst_view_id = d.id ";
        $sql .= "    and f.mst_user_id = "  . $this->Session->read('Auth.MstUser.id');
        $sql .= "    and f.is_deleted = false " ;
        $sql .="  where";
        $sql .="    a.id = " . $this->Session->read('Auth.MstUser.id');
        $sql .="    and (( c.readable = true and d.regist_flg = false ) ";
        $sql .="     or ( c.creatable = true and d.regist_flg = true )) ";
        $sql .="  order by e.order_num , d.order_num ";
        $menu = $this->MstView->query($sql);
        $this->set('Menu' , $menu);
        $this->set('MenuCount' , count($menu)); // 位置調整用
        
        // 右:選択済みお気に入りメニュー
        $sql  = " select ";
        $sql .= "      b.name ";
        $sql .= "    , b.url ";
        $sql .= "    , b.id ";
        $sql .= " from ";
        $sql .= "   mst_favorite_views as a  ";
        $sql .= "   left join   ";
        $sql .= "   mst_views as b  ";
        $sql .= "   on ";
        $sql .= "    a.mst_view_id = b.id ";
        $sql .= " where ";
        $sql .= "   a.mst_user_id = "  . $this->Session->read('Auth.MstUser.id');
        $sql .= "   and a.is_deleted = false " ;
        $sql .= " order by ";
        $sql .= "   a.order_num ";
        $favorite = $this->MstFavoriteView->query($sql);
        
        $this->set('Favorite' , $favorite);
        
        $this->render('menu_list');
    }
}
