<?php
/**
 * AccountsController
 *　締め
 * @version 1.0.0
 * @since 2010/05/18
 */
class AccountsController extends AppController {
    
  /**
   * @var $name
   */
  var $name = 'Accounts';
    
  /**
   * @var array $uses
   */
  var $uses = array('',);

  /**
   * @var bool $scaffold
   */
  //var $scaffold;

  /**
   * @var array $helpers
   */
  var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

  /**
   * @var array $components
   */
  var $components = array('RequestHandler');

  /**
   *
   */
  function index (){
  }

  /**
   * 締め情報作成
   * 日締め、月締めの違いがある
   * @param
   * @return 
   */
  function add() {
  }

  /**
   * 
   */
  function add_confirm () {
      $this->render('add_confirm');
  }
  
  /**
   * 
   */
  function add_result () {
      $this->render('add_result');
  }
  
  /**
   * 締めデータの履歴表示、検索も行える
   * とりあえずは月・日締め別、日時指定で検索できればよい。
   *　@param string $keyword
   */
  function history ($keyword = null) {
  }  
  
  /**
   * 
   * @param integer $id
   */
  function edit ($id = null) {
  }    

  /**
   * 
   */
  function edit_confirm (){
      $this->render('edit_confirm');
  }
  
  /**
   * 
   */
  function edit_result () {
      $this->render('edit_result');
  }
  
}
    