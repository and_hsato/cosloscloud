<?php
/**
 * BenchMarks_Controller
 * ベンチマーク
 */
class BenchMarksController extends AppController {

    /**
     * @var $name
     */
    var $name = 'BenchMarks';

    /**
     * @var array $uses
     */

    var $uses = array(
        'MstItemUnit',
        'MstFacilityItem',
        'MstUserBelonging'
    );

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Mail');

    /**
     * @var $paginate
     */

    public function index(){
        
    }
    
    public function send(){
        $mailParams = array(
                MailComponent::PARAM_CONFIG       => 'and',
                MailComponent::PARAM_TO           => array($this->Session->read('Auth.MstUser.login_id'),),
                MailComponent::PARAM_BCC          => Configure::read('admin.inquiry.emails'),
                MailComponent::PARAM_VIEW_VARS    => array(
                    'user_name' => $this->Session->read('Auth.MstUser.user_name'),
                ),
        );
        
        if ($this->Mail->sendBenchMarksMail($mailParams)) {
            // 成功時の処理
            $this->Session->setFlash('申し込みを行いました','growl',array('type'=>'star'));
        } else {
            // 失敗時の処理
            $this->Session->setFlash('メール送信に失敗しました','growl',array('type'=>'error'));
        }
        
        $this->redirect('thank'); 
    }
    
    public function thank(){
    }
}
