<?php
/**
 * RecombinationController
 *  組換入庫
 * @since 2010/09/23
 */
class RecombinationController extends AppController {
    var $name = 'Recombination';
    var $uses = array(
        'MstClass',
        'TrnSticker',
        'TrnStorage',
        'TrnStorageHeader',
        'TrnReceiving',
        'TrnMoveHeader',
        'TrnShipping',
        'TrnStock',
        'TrnReceivingHeader',
        'TrnReceiving',
        'TrnStickerRecord',
        'TrnStickerIssue',
        'MstSalesConfig',
        'MstTransactionConfig',
        'MstFacility',
        'MstDepartment',
        'MstFacilityItem',
        );

    var $components = array('RequestHandler', 'Storage','Barcode','Stickers');
    
    public function index(){
        $this->setRoleFunction(26); //組み換え登録
        $classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'11');
        $this->set('classes', $classes);
        $this->render('/recombination/select');
    }
    
    public function confirm(){
        //在庫締め
        if(false === $this->canUpdateAfterClose(date('Y/m/d', time()), $this->Session->read('Auth.facility_id_selected'))){
            $this->Session->setFlash('already_closed', 'growl', array('type'=>'error'));
            $this->index();
            return;
        }
        $kawatetu_quantity = array();
        $master = array();
        $stickers = array();
        foreach($this->request->data['Recombination']['seals'] as $_no){
            //川鉄シール対応
            $row = array();
            if(preg_match("/^01[0-9][0-9]{13}/", $_no)){
                $row = $this->getStickerInfo($_no);
                if(isset($kawatetu_quantity[$row[0]['id']])){
                    if($kawatetu_quantity[$row[0]['id']] > 0){
                        $row[0]['quantity'] = 1;
                        $kawatetu_quantity[$row[0]['id']]--;
                    }else{
                        $row[0]['quantity'] = 0;
                    }
                }else{
                    if($row[0]['quantity'] > 0){
                        $kawatetu_quantity[$row[0]['id']] = $row[0]['quantity'];
                        $row[0]['quantity'] = 1;
                        $kawatetu_quantity[$row[0]['id']]--;
                    }else{
                        $kawatetu_quantity[$row[0]['id']] = 0;
                        $row[0]['quantity'] = 0;
                    }
                }
            }else{
                $row = $this->getSealInfo($_no);
            }
            if(0 === count($row)){
                //該当なし
                $stickers[] = array();
            }else{
                $stickers[] = $row[0];
                if(empty($master)){
                    $master = $row[0];
                }
            }
        }
        
        $hasError = false;
        $maxUnits = 0;
        
        $reserveCheck = array();  //引当予約チェック用
        
        for($i = 0; $i < count($stickers); $i++){
            if(empty($master)){
                $stickers[$i]['error'][] = '該当なし';
            }else{
                if($this->isNNs($stickers[$i])){
                    $stickers[$i]['error'][] = '該当なし';
                    $hasError = true;
                    continue;
                }
                if($stickers[$i]['mst_facility_item_id'] != $master['mst_facility_item_id']){
                    $stickers[$i]['error'][] = '商品違い';
                }
                
                // 県総すぺしゃる 開始
                // 区分名
                if($stickers[$i]['type_name'] != $master['type_name']){
                    $stickers[$i]['error'][] = '区分名違い';
                }
                // 種類名
                if($stickers[$i]['class_name'] != $master['class_name']){
                    $stickers[$i]['error'][] = '種類名違い';
                }
                // 売り済みフラグ
                if($stickers[$i]['sales_flg'] ==  true ){
                    $stickers[$i]['error'][] = '病院資産';
                }
                // 病院資産
                if($stickers[$i]['class_name'] ==  '病院資産' ){
                    $stickers[$i]['error'][] = '病院資産';
                }
                // 県総すぺしゃる 終了
                
                if($stickers[$i]['lot_no'] != $master['lot_no']){
                    $stickers[$i]['error'][] = 'ロット番号違い';
                }

                if($stickers[$i]['validated_date'] != $master['validated_date']){
                    $stickers[$i]['error'][] = '有効期限違い';
                }

                if((bool)$stickers[$i]['is_deleted'] === true
                   || trim($stickers[$i]['hospital_sticker_no']) != ''
                   || (integer)$stickers[$i]['quantity'] === 0){
                    $stickers[$i]['error'][] = '無効なシール';
                }

                if((bool)$stickers[$i]['has_reserved'] === true ){
                    $stickers[$i]['error'][] = '予約済みシール';
                }
                
                //引当予約済みシール
                //予約数チェック
                if(isset($stickers[$i]['id']) && !empty($stickers[$i]['id'])){
                    if(!isset($reserveCheck[$stickers[$i]['mst_item_unit_id']])) {
                        $sql  = ' select ';
                        $sql .= '       x.mst_item_unit_id ';
                        $sql .= '     , x.stock_count - (coalesce(x.count, 0) - coalesce(y.count, 0)) as stock_count ';
                        $sql .= '   from ';
                        $sql .= '     (  ';
                        $sql .= '       select ';
                        $sql .= '             a.mst_item_unit_id ';
                        $sql .= '           , a.mst_department_id ';
                        $sql .= '           , b.stock_count ';
                        $sql .= '           , (b.reserve_count) as count  ';
                        $sql .= '         from ';
                        $sql .= '           trn_stickers as a  ';
                        $sql .= '           left join trn_stocks as b  ';
                        $sql .= '             on a.mst_item_unit_id = b.mst_item_unit_id  ';
                        $sql .= '             and a.mst_department_id = b.mst_department_id  ';
                        $sql .= '         where ';
                        $sql .= '           a.id = ' . $stickers[$i]['id'];
                        $sql .= '     ) as x  ';
                        $sql .= '     left join (  ';
                        $sql .= '       select ';
                        $sql .= '             count(*)   as count ';
                        $sql .= '           , a.mst_item_unit_id ';
                        $sql .= '           , a.mst_department_id  ';
                        $sql .= '         from ';
                        $sql .= '           trn_stickers as a  ';
                        $sql .= '         where ';
                        $sql .= '           a.is_deleted = false  ';
                        $sql .= '           and a.has_reserved = true  ';
                        $sql .= '         group by ';
                        $sql .= '           a.mst_item_unit_id ';
                        $sql .= '           , a.mst_department_id ';
                        $sql .= '     ) as y  ';
                        $sql .= '       on x.mst_item_unit_id = y.mst_item_unit_id  ';
                        $sql .= '       and x.mst_department_id = y.mst_department_id ';
                        $ret = $this->TrnSticker->query($sql);
                        $reserveCheck[$ret[0][0]['mst_item_unit_id']] = $ret[0][0]['stock_count'];
                    }
                    
                    if($reserveCheck[$stickers[$i]['mst_item_unit_id']] > 0){
                        //使える在庫がある。
                        $reserveCheck[$stickers[$i]['mst_item_unit_id']]--;
                    }else{
                        //使える在庫がない。
                        $stickers[$i]['error'][] = '引当予約済み';
                    }
                }
                
                if($stickers[$i]['item_type'] ==  Configure::read('Items.item_types.setitem')){
                    $stickers[$i]['error'][] = 'セット品';
                }
            }
            if(isset($stickers[$i]['error'])){
                $hasError = true;
            }else{
                $maxUnits += $stickers[$i]['quantity'] * $stickers[$i]['p_unit'];
            }
        }

        $classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'11');
        $work_type = $this->request->data['MstClass']['id'];
        $this->set('workType', array(
            'id' => $work_type,
            'name' => ($work_type == '' ? '' : $classes[$work_type]),
        ));

        $this->Session->write('Recombination.stickers', $stickers);
        $this->Session->write('Recombination.seals', $this->request->data['Recombination']['seals']);
        $this->set('classes', $classes);
        $this->set('master', $this->outputFilter($master));
        $this->set('hasError', $hasError);
        $this->set('stickers', $stickers);
        $this->set('recital', $this->request->data['Recombination']['recital']);
        $this->set('maxUnits', $maxUnits);

        if(false === $hasError){
            $stocks = $this->Storage->getStocks($master['mst_facility_item_id'], $maxUnits);
            $this->set('stocks', $stocks);
            $this->Session->write('Recombination.stocks', $stocks);
        }else{
            $this->Session->setFlash('recombination_item_error', 'growl', array('type'=>'error'));
        }
        
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
        
        $this->render('/recombination/confirm');
    }

    /**
     * シール情報取得
     * @param mixed $sid
     * @access private
     * @return array
     */
    private function getSealInfo($sid){
        App::import('Sanitize');
        $sql =
            ' select ' .
                ' a.id'.
                ',a.facility_sticker_no'.
                ',a.hospital_sticker_no'.
                ',a.lot_no'.
                ",to_char(a.validated_date,'YYYY/MM/DD') as validated_date".
                ',a.mst_item_unit_id'.
                ',a.is_deleted'.
                ',a.quantity'.
                ',a.has_reserved'.
                ',a.sales_flg '.
                ',a.trn_order_id'.
                ',a.transaction_price'.
                ',a.original_price'.
                ',coalesce(a.sales_price,0) as sales_price'.
                ',a.class_name'.
                ',a.type_name'.
                ',a.modified'.
                ',b.mst_facility_item_id'.
                ',c.internal_code'.
                ',c.item_code'.
                ',c.item_name'.
                ',c.standard' .
                ',round((c.unit_price * b.per_unit * c.converted_num),2) as price'.
                ',c.item_type'.
                ',c.mst_owner_id'.
                ',c.biogenous_type'.
                ',f.dealer_name'.
                ',d.unit_name as unitName' .
                ',e.unit_name as p_unit_name'.
                ',b.per_unit as p_unit' .
                ",(case when b.per_unit = 1 then d.unit_name else d.unit_name || '(' || b.per_unit || e.unit_name || ')'  end) as packing_name ".
                ',h.name as shelf_name '.
                ',i.const_nm as insurance_claim_department_name'.
            ' from ' .
                ' trn_stickers a ' .
                ' inner join mst_item_units b on a.mst_item_unit_id = b.id ' .
                ' inner join mst_facility_items c on b.mst_facility_item_id = c.id ' .
                ' inner join mst_unit_names d on b.mst_unit_name_id = d.id ' .
                ' inner join mst_unit_names e on b.per_unit_name_id = e.id ' .
                ' left join mst_dealers f on c.mst_dealer_id = f.id' .
                ' left join mst_fixed_counts g on a.mst_department_id = g.mst_department_id and a.mst_item_unit_id = g.mst_item_unit_id ' .
                ' left join mst_shelf_names h on g.mst_shelf_name_id = h.id ' .
                " left join mst_consts i on i.const_cd = c.insurance_claim_type::varchar and i.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'" .
                  
            ' where ';
                if(false === is_array($sid)){
                    $sql .= " a.facility_sticker_no = '" . Sanitize::escape($sid) . "' ";
                }else{
                    $c = array();
                    foreach($sid as $_no){
                        $c[] = "'" . Sanitize::escape(trim($_no)) . "'";
                    }
                    $sql .= ' a.facility_sticker_no in (' . join(',', $c) . ')';
                }
        $sql .= " and a.quantity > 0 and a.hospital_sticker_no = '' and a.is_deleted = false ";
        $sql .= ' order by a.id asc';
        $res = $this->TrnSticker->query($sql);
        $result = array();
        if(empty($res)){
            return false;
        }else{
            foreach($res as $_row){
                $r = array();
                array_walk_recursive($_row, array('RecombinationController','BuildSearchResult'), &$r);
                $result[] = $r;
            }
        }
        return $result;
    }

    /**
     * 組替確定
     *
     */
    public function commit(){
        
        //トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)){
            $master = $this->Session->read('master');
            $newResult = $this->Session->read('newResult');
            $oldResult = $this->Session->read('oldResult');
            $stocks = $this->Session->read('stocks');
            $facility_name = $this->Session->read('facility_name');
            $this->set('master', $master);
            $this->set('newResult', $newResult);
            $this->set('oldResult', $oldResult);
            $this->set('stocks', $stocks);
            $this->set('facility_name', $facility_name);
            $this->render('/recombination/results');
            return;
        }
        
        $now = date('Y/m/d H:i:s.u');
        
        $stickers = $this->Session->read('Recombination.stickers');
        $maxUnits = 0;
        $total_transaction_price = 0;
        
        //登録処理
        $this->TrnSticker->begin();
        
        //行ロック
        $ids = array();
        foreach($stickers as $_row){
            $ids[] = $_row['id'];
        }
        $this->TrnSticker->query(' select * from trn_stickers as a where a.id in (' . join( ',' , $ids ) . ') for update ');

        //更新チェックを行う
        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',' , $ids). ')';
        $sql .= "     and a.modified > '" . $this->request->data['Recombination']['time'] ."'";
        
        $ret = $this->TrnSticker->query($sql);

        if($ret[0][0]['count']> 0 ){
            $this->TrnSticker->rollback();
            $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。', 'growl', array('type'=>'error') );
            $this->redirect('index');
        }
        
        foreach($stickers as $_row){
            //バラの個数を取得
            $maxUnits += (integer)$_row['quantity'] * (integer)$_row['p_unit'];
            //読み込んだシールの仕入価格の合計を取得
            $total_transaction_price  += $_row['transaction_price'];
        }
        // センターの情報を取得
        $this->MstFacility->recursive = -1;
        $facility = $this->MstFacility->find('first',array('conditions'=>array('MstFacility.id'=>$this->Session->read('Auth.facility_id_selected'))));

        $_stocks = $this->Storage->getStocks($stickers[0]['mst_facility_item_id'], $maxUnits);

        //シールの更新チェック
        if(true === $this->hasModifyDifference($this->request->data['showTime'], $stickers)){
            $this->Session->setFlash('シール情報が更新されています。再度実行してください', 'growl', array('type'=>'error'));
            $this->index();
            return;
        }

        //在庫の更新チェック
        if(true === $this->hasModifyDifference($_stocks, $this->Session->read('Recombination.stocks'))){
            $this->Session->setFlash('在庫情報が更新されています。再度実行してください', 'growl', array('type'=>'error'));
            $this->index();
            return;
        }

        //更新対象の在庫を取得
        $stocks = array();
        foreach($this->request->data['Recombination']['Rows'] as $_key => $_val){
            if($_val['selected'] == '1'){
                $r = $this->Storage->getStockById($_key, $_stocks);
                $r['input_quantity']  = (integer)$this->request->data['Recombination']['Rows'][$_key]['quantity'];
                $r['input_recital']   = $this->request->data['Recombination']['Rows'][$_key]['recital'];
                $r['input_work_type'] = $this->request->data['Recombination']['Rows'][$_key]['work_type'];
                $stocks[] = $r;
            }
        }

        $classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'11');

        $workDate = date('Y/m/d');
        $workNo =  $this->setWorkNo4Header($workDate, '14');
        $depId = $this->Storage->getDepartmentId($this->Session->read('Auth.facility_id_selected'), Configure::read('DepartmentType.warehouse'));

        //移動ヘッダ作成
        $this->TrnMoveHeader->create(false);
        $this->TrnMoveHeader->recursive = -1;
        $this->TrnMoveHeader->save(array(
            'id'                    => false,
            'work_no'               => $workNo,
            'work_date'             => $workDate,
            'work_type'             => $this->request->data['Recombination']['work_type'] == '' ? null : $this->request->data['Recombination']['work_type'],
            'recital'               => $this->request->data['Recombination']['recital'],
            'move_status'           => Configure::read('MoveStatus.complete'),
            'department_id_from'    => $depId,
            'department_id_to'      => $depId,
            'detail_count'          => 0,
            'detail_total_quantity' => 0,
            'is_deleted'            => false,
            'creater'               => $this->Session->read('Auth.MstUser.id'),
            'created'               => $now,
            'modifier'              => $this->Session->read('Auth.MstUser.id'),
            'modified'              => $now,
            'move_type'             => Configure::read('MoveType.recombination'),
        ));

        $lastHeaderId = $this->TrnMoveHeader->getLastInsertID();
        $oldCount = 0;
        $newCount = 0;
        $newResult = array();
        $oldResult = array();

        //旧レコード
        foreach($stickers as $_row){
            $oldCount += 1;

            //シールの数量を０に更新
            $this->TrnSticker->create();
            $this->TrnSticker->recursive = -1;
            $this->TrnSticker->updateAll(array(
                'TrnSticker.quantity' => 'TrnSticker.quantity -1 ' ,
                'TrnSticker.modifier' => $this->Session->read('Auth.MstUser.id'),
                'TrnSticker.modified' => "'" . $now ."'",
            ),array(
                'TrnSticker.id' => $_row['id'],
            ),-1);

            //在庫数を減らす
            $this->TrnStock->create();
            $this->TrnStock->recursive = -1;
            $this->TrnStock->updateAll(array(
                'TrnStock.stock_count' => 'TrnStock.stock_count - '.(int)$_row["quantity"],
                'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                'TrnStock.modified'    => "'" . $now . "'",
            ),array(
                'TrnStock.mst_department_id' => $depId,
                'TrnStock.mst_item_unit_id'  => $_row['mst_item_unit_id'],
            ),-1);

            //出荷レコード作成
            $this->TrnShipping->create(false);
            $this->TrnShipping->recursive = -1;
            $this->TrnShipping->save(array(
                'id'                 => false,
                'work_no'            => $workNo,
                'work_seq'           => $oldCount,
                'work_date'          => $workDate,
                'work_type'          => $this->request->data['Recombination']['work_type'] == '' ? null : $this->request->data['Recombination']['work_type'],
                'recital'            => $this->request->data['Recombination']['recital'],
                'shipping_type'      => Configure::read('ShippingType.fixed'),
                'shipping_status'    => Configure::read('ShippingStatus.loaded'),
                'mst_item_unit_id'   => $_row['mst_item_unit_id'],
                'department_id_from' => $depId,
                'department_id_to'   => $depId,
                'quantity'           => 1,
                'trn_sticker_id'     => $_row['id'],
                'is_deleted'         => false,
                'creater'            => $this->Session->read('Auth.MstUser.id'),
                'created'            => $now,
                'modifier'           => $this->Session->read('Auth.MstUser.id'),
                'modified'           => $now,
                'trn_move_header_id' => $lastHeaderId,
                'move_status'        => Configure::read('MoveStatus.complete'),
            ));

            $oldResult['facility_sticker_no'][] = $_row['facility_sticker_no'];
            $oldResult['packing_name'][] = $_row['packing_name'];
        }

        //センターコード
        $facility_code = $this->getFacilityCode($this->Session->read('Auth.facility_id_selected') , array(1,2));
        //新レコード
        foreach($stocks as $_row){
            for($i = 0; $i < $_row['input_quantity']; $i++){
                $newCount += 1;

                //在庫数を増やす
                $this->TrnStock->create(false);
                $this->TrnStock->recursive = -1;
                $this->TrnStock->updateAll(array(
                    'TrnStock.stock_count' => 'TrnStock.stock_count + 1',
                    'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                    'TrnStock.modified'    => "'" . $now . "'",
                ),array(
                    'TrnStock.mst_department_id' => $depId,
                    'TrnStock.mst_item_unit_id'  => $_row['mst_item_unit_id'],
                ),-1);


                $price = $this->Storage->getTransactionPrice($maxUnits ,
                                                             $total_transaction_price ,
                                                             $_row['per_unit'] ,
                                                             $facility['MstFacility']['round']
                                                             );

                
                //シール作成
                $sNO = $this->getCenterStickerNo($workDate, $facility_code);
                $this->TrnSticker->create(false);
                $this->TrnSticker->recursive = -1;
                $this->TrnSticker->save(array(
                    'id'                  => false,
                    'facility_sticker_no' => $sNO,
                    'mst_item_unit_id'    => $_row['mst_item_unit_id'],
                    'mst_department_id'   => $depId,
                    'trade_type'          => Configure::read('ClassesType.Constant'),
                    'quantity'            => 1,
                    'lot_no'              => $stickers[0]['lot_no'],
                    'validated_date'      => $stickers[0]['validated_date'],
                    'original_price'      => $stickers[0]['original_price'],
                    'transaction_price'   => $price,
                    'last_move_date'      => $workDate,
                    'trn_order_id'        => $stickers[0]['trn_order_id'],
                    'is_deleted'          => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $now,
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $now,
                    'has_reserved'        => false,
                    'trn_move_header_id'  => $lastHeaderId,
                    'class_name'          => $stickers[0]['class_name'],
                    'type_name'           => $stickers[0]['type_name'],
                ));
                $lastStickerId = $this->TrnSticker->getLastInsertID();

                //入荷レコード
                $this->TrnReceiving->create(false);
                $this->TrnReceiving->recursive = -1;
                $this->TrnReceiving->save(array(
                    'id'                  => false,
                    'work_no'             => $workNo,
                    'work_seq'            => $newCount,
                    'work_date'           => $workDate,
                    'work_type'           => $_row['input_work_type'] == '' ? null : $_row['input_work_type'],
                    'recital'             => $_row['input_recital'],
                    'receiving_type'      => Configure::read('ReceivingType.recombination'),
                    'receiving_status'    => Configure::read('ReceivingStatus.arrival'),
                    'mst_item_unit_id'    => $_row['mst_item_unit_id'],
                    'department_id_from'  => $depId,
                    'department_id_to'    => $depId,
                    'quantity'            => 1,
                    'remain_count'        => 1,
                    'stocking_price'      => $price,
                    'sales_price'         => $price,
                    'trn_sticker_id'      => $lastStickerId,
                    'stocking_close_type' => Configure::read('StockingCloseType.none'),
                    'sales_close_type'    => Configure::read('SalesCloseType.none'),
                    'facility_close_type' => Configure::read('FacilityCloseType.none'),
                    'trn_move_header_id'  => $lastHeaderId,
                    'is_deleted'          => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $now,
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $now,
                ));

                $lastReceivingId = $this->TrnReceiving->getLastInsertID();

                //シールのキーを更新
                $this->TrnSticker->create(false);
                $this->TrnSticker->recursive = -1;
                $this->TrnSticker->updateAll(array(
                    'TrnSticker.trn_receiving_id' => $lastReceivingId,
                ),array(
                    'TrnSticker.id' => $lastStickerId,
                ),-1);

                //結果表示用配列
                $newResult[] = array(
                    'facility_sticker_no' => $sNO,
                    'packing_name'        => $_row['packing_name'],
                    'work_type_name'      => $_row['input_work_type'] == '' ? '' : $classes[$_row['input_work_type']],
                    'recital'             => $_row['input_recital'],
                );
            }
        }

        //移動ヘッダ更新
        $this->TrnMoveHeader->create(false);
        $this->TrnMoveHeader->recursive = -1;
        $this->TrnMoveHeader->updateAll(array(
            'TrnMoveHeader.detail_count' => $newCount,
            'TrnMoveHeader.detail_total_quantity' => $oldCount + $newCount,
        ),array(
            'TrnMoveHeader.id' => $lastHeaderId,
        ),-1);

        $errors = array();
        $errors = array_merge(
            $errors,
            is_array($this->validateErrors($this->TrnMoveHeader)) ? $this->validateErrors($this->TrnMoveHeader) : array(),
            is_array($this->validateErrors($this->TrnSticker)) ? $this->validateErrors($this->TrnSticker) : array(),
            is_array($this->validateErrors($this->TrnShipping)) ? $this->validateErrors($this->TrnShipping) : array(),
            is_array($this->validateErrors($this->TrnReceiving)) ? $this->validateErrors($this->TrnReceiving) : array(),
            is_array($this->validateErrors($this->TrnStock)) ? $this->validateErrors($this->TrnStock) : array()
        );
        
        $ret = $this->TrnSticker->query(' select count(*) from trn_stickers as a where a.id in (' . join(',',$ids) . ") and a.modified <> '". $now . "' and a.modified > '" . $this->request->data['Recombination']['time'] . "'");
        
        //コミット直前に更新チェック
        if( $ret[0][0]['count'] > 0 ){
            throw new Exception('ほかのユーザによって更新されています。最初からやり直してください。');
            return;
        }
        
        if(count($errors) > 0){
            throw new Exception(print_r($errors,true));
            return;
        }else{
            $this->TrnSticker->commit();
        }

        $master = $stickers[0];
        $master['work_type_name'] = $this->request->data['Recombination']['work_type'] == '' ? '' : $classes[$this->request->data['Recombination']['work_type']];
        $master['recital'] = $this->request->data['Recombination']['recital'];
        $this->set('master', $master);
        $this->set('newResult', $newResult);
        $this->set('oldResult', $oldResult);
        $this->set('stocks', $this->Storage->getStocks($stickers[0]['mst_facility_item_id'], $maxUnits));
        $fid = $this->Session->read('Auth.facility_id_selected');
        $selects = $this->Session->read('Auth.facilities');
        $this->set('facility_name', $selects[$fid]);
        
        $this->Session->write('master', $master);
        $this->Session->write('newResult', $newResult);
        $this->Session->write('oldResult', $oldResult);
        $this->Session->write('stocks', $this->Storage->getStocks($stickers[0]['mst_facility_item_id'], $maxUnits));
        $this->Session->write('facility_name', $selects[$fid]);
        
        $this->render('/recombination/results');
    }

    /**
     *
     * Enter description here ...
     */
    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'seal';
    }

    /**
     *
     * Enter description here ...
     * @param $dummy
     */
    public function seal($dummy = null){
        $_fno = explode(',' , $this->request->data['Recombination']['facility_sticker_no'][0] );
        $fid = $this->request->data['MstFacility']['id'];
        $fname = $this->getFacilityName($fid);
        $stickers = $this->Stickers->getFacilityStickers($_fno, $fname, $fid);
        $this->layout = 'xml/default';
        $this->set('stickers', $stickers);
        $this->render('/storages/seal');
    }

    /**
     * 明細に差異があるかどーか
     * @param int $showTime
     * @param array $preResult
     * @access private
     * @return boolean
     */
    private function hasModifyDifference($showTime, array $preResult){
        foreach($preResult as $row){
            if($showTime < strtotime($row['modified'])){
                return true;
            }
        }
        return false;
    }

    /**
     * Use this with php function: array_walk_recursive
     * @param mixed $item
     * @param string $key
     * @param array $res
     * @access public
     * @static
     */
    public static function BuildSearchResult($item, $key, $res){
        //HACK:  最上位の配列値を優先するようにしてます・・
        if(false === isset($res[$key])){
            $res[$key] = $item;
        }
    }

    /**
     * 包装単位名を取得
     * @param string $unitName
     * @param string $parUnit
     * @param string $parUnitName
     * @access private
     * @return string
     */
    private function getPackingName($unitName,$parUnit,$parUnitName){
        if((integer)$parUnit === 1){
            return $unitName;
        }else{
            return sprintf('%s（%s%s）',$unitName,$parUnit,$parUnitName);
        }
    }

    /**
     * 川鉄シールからシール情報を取得
     * @param string $barcode 画面で入力されたバーコード
     * @access private
     * @return trn_sticker シールレコード
     */
    private function getStickerInfo($barcode){
        //
        $kawatetu = $this->Barcode->readEAN128($barcode);
        $quantity = 1;
        $lot_no = "";
        //数量(入数)
        if(!empty($kawatetu['quantity'])){
            $quantity = $kawatetu['quantity'];
        }
        //ロット
        if(!empty($kawatetu['lot_no'])){
            $lot_no = $kawatetu['lot_no'];
        }

        //JAN、ロット、数量(入数)からシールを検索
        $sql  = "SELECT a.* ";
        $sql .= "      ,b.mst_facility_item_id";
        $sql .= "      ,c.internal_code";
        $sql .= "      ,c.item_code";
        $sql .= "      ,c.item_name";
        $sql .= "      ,c.standard";
        $sql .= "      ,c.price";
        $sql .= "      ,c.item_type";
        $sql .= "      ,c.mst_owner_id";
        $sql .= "      ,c.biogenous_type";
        $sql .= "      ,f.dealer_name";
        $sql .= "      ,d.unit_name as unitName";
        $sql .= "      ,e.unit_name as p_unit_name";
        $sql .= "      ,b.per_unit as p_unit";
        $sql .= "      ,h.name as shelf_name";
        $sql .= "      ,i.const_nm as insurance_claim_department_name";
        $sql .= "  FROM trn_stickers a";
        $sql .= " INNER JOIN mst_item_units b";
        $sql .= "    ON a.mst_item_unit_id = b.id";
        $sql .= "   AND b.per_unit = ".$quantity;
        $sql .= " INNER JOIN mst_facility_items c";
        $sql .= "    ON b.mst_facility_item_id = c.id";
        $sql .= "   AND c.jan_code like '".$kawatetu["pure_jan_code"]."%'";
        $sql .= " INNER JOIN mst_unit_names d";
        $sql .= "    ON b.mst_unit_name_id = d.id";
        $sql .= " INNER JOIN mst_unit_names e";
        $sql .= "    ON b.per_unit_name_id = e.id";
        $sql .= "  LEFT JOIN mst_dealers f";
        $sql .= "    ON c.mst_dealer_id = f.id";
        $sql .= "  LEFT JOIN mst_fixed_counts g";
        $sql .= "    ON a.mst_department_id = g.mst_department_id";
        $sql .= "   AND a.mst_item_unit_id = g.mst_item_unit_id";
        $sql .= "  LEFT JOIN mst_shelf_names h";
        $sql .= "    ON g.mst_shelf_name_id = h.id";
        $sql .= "  LEFT JOIN mst_consts i";
        $sql .= "    ON i.const_cd = c.insurance_claim_type::varchar";
        $sql .= "    AND i.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
        $sql .= " WHERE a.lot_no = '".$lot_no. "'";
        $sql .= "   AND a.facility_sticker_no = ''";
        $sql .= "   AND a.hospital_sticker_no = ''";
        $sql .= "   AND a.quantity >= 1";
        $sql .= "   AND a.is_deleted = false";
        $sql .= "   AND a.has_reserved = false";


        //検索実行
        $res = $this->TrnSticker->query($sql);
        $result = array();
        if(empty($res)){
            return false;
        }else{
            foreach($res as $_row){
                $r = array();
                array_walk_recursive($_row, array('RecombinationController','BuildSearchResult'), &$r);
                $r['packing_name'] = $this->getPackingName($r['unitname'], $r['p_unit'], $r['p_unit_name']);
                $result[] = $r;
            }
        }
        return $result;
    }
}
?>
