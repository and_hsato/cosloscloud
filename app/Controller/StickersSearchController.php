<?php
/**
 * StickersSearchController
 *　シール管理
 * @version 1.0.0
 * @since 2010/09/10
 */
class StickersSearchController extends AppController {
    
    /**
     * @var $name
     */
    var $name = 'StickersSearch';
    
    /**
     * @var array $uses
     */
    var $uses = array(
        'TrnSticker',
        'TrnStickerRecord',
        'MstDepartment',
        'MstFacility',
        'MstFacilityItem',
        'MstUserBelonging',
        );
    
    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time',);
    
    /**
     * @var array $components
     */
    var $components = array('RequestHandler', 'Stickers' , 'Barcode','CsvWriteUtils');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }
    
    public function index(){
        $this->redirect('search');
    }
    /**
     *
     */
    function search(){
        $this->setRoleFunction(54); //シール検索
        
        // センター情報取得
        $fid = $this->Session->read('Auth.facility_id_selected');
        $selects = $this->Session->read('Auth.facilities');
        $this->set('facility_name', $selects[$fid]);
        $this->set('facility_id', $fid);
        
        $_stickers = array();
        if( isset($this->request->data['isSearch']) ) {
            //検索ボタン押下
            $_params = array();
            // ソート順の指定
            $_search_conditions = $this->request->data;
            $_params['order'] = 'TrnSticker.facility_sticker_no,TrnSticker.hospital_sticker_no,TrnSticker.modified desc';
            $_params['limit'] = $this->_getLimitCount();
            
            // 施設が指定されていない場合は、ログインしている施設を出す
            if( empty($_search_conditions['Sticker']['facilityCode']) ) {
                $_search_conditions['facility_user_belonging'] = $this->Session->read('Auth.MstUser.id');
            }
            
            $ret = $this->TrnSticker->search( $_search_conditions , $_params ,$this->Session->read('Auth.facility_id_selected') , $this->request->data['TrnSticker']['mst_user_id'] , true );
            $this->set('max' , $ret[0][0]['count']);
            
            $_stickers = $this->TrnSticker->search( $_search_conditions , $_params ,$this->Session->read('Auth.facility_id_selected') , $this->request->data['TrnSticker']['mst_user_id'] );
        }else{
            //初期設定
            $this->request->data['TrnSticker']['mst_user_id'] = $this->Session->read('Auth.MstUser.id');
            $this->request->data['TrnSticker']['is_active'] = 1;
        }
        $this->set( 'stickers' , $_stickers);
        
        $this->set( 'facilities' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                                          array(Configure::read('FacilityType.hospital') , Configure::read('FacilityType.center'))
                                                          ));
        $_department_list = array();
        
        if( isset($this->request->data['Sticker']['facilityCode']) && $this->request->data['Sticker']['facilityCode'] != "") {
            $_department_list = $this->getDepartmentsList($this->request->data['Sticker']['facilityCode']);
        }
        
        $this->set('department_list' , $_department_list);
    }
    
    function edit(){
        $this->set('view_type' , 'edit');
        $_id = $this->request->data['trnsticker_id_selected'];
        $this->__setSticker( $_id );
        
        $this->render('edit_result');
    }
    
    function edit_result(){
        // センター情報取得
        $_id = $this->request->data['trnsticker_id_selected'];
        //トランザクション開始
        $this->TrnSticker->begin();
        
        //行ロック
        $this->TrnSticker->query('select * from trn_stickers as a where a.id = ' . $_id . ' for update ');
        
        $_success = false;
        
        //更新チェック
        $sql = "";
        $sql .= "SELECT modified FROM trn_stickers WHERE trn_stickers.id = ".$_id;
        $tempModified = $this->TrnSticker->query($sql);
        
        if($tempModified[0][0]["modified"] !== $this->request->data["TrnSticker"]["modified"]){
            //更新エラー
            $this->TrnSticker->rollback();
            $this->Session->setFlash("別ユーザによってデータが更新されています", 'growl', array('type'=>'error') );
            $this->edit();
            return;
        }
        
        $this->TrnSticker->id = $_id;
        $save_data['TrnSticker']['lot_no']         = $this->request->data['TrnSticker']['lot_no'];
        $save_data['TrnSticker']['validated_date'] = $this->request->data['TrnSticker']['validated_date'];
        $_result = $this->TrnSticker->save($save_data);
        
        if( $_result !== false ) {
            $_success = true;
        }
        
        if( $_success ) {
            $this->set('view_type' , 'result');
            $this->Session->setFlash('シールの更新処理が終了しました', 'growl', array('type'=>'star') );
            $this->TrnSticker->commit();
        }else{
            $this->set('view_type' , 'edit');
            $this->Session->setFlash('シールの更新に失敗しました', 'growl', array('type'=>'error') );
            $this->TrnSticker->rollback();
        }
        $this->__setSticker( $_id );
        $this->render('edit_result');
    }
    
    private function __setSticker( $id ) {

        $sql  =' select ';
        $sql .='       a.id ';
        $sql .='     , c.internal_code ';
        $sql .='     , c.item_name ';
        $sql .='     , c.standard ';
        $sql .='     , c.item_code ';
        $sql .='     , c.jan_code ';
        $sql .='     , c.converted_num ';
        $sql .='     , d.dealer_name ';
        $sql .='     , b.per_unit ';
        $sql .='     , (  ';
        $sql .='       case  ';
        $sql .='         when b.per_unit = 1  ';
        $sql .='         then b1.unit_name  ';
        $sql .="         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .='         end ';
        $sql .='     )                            as unit_name ';
        $sql .='     , e.department_name ';
        $sql .='     , f.facility_name  ';
        $sql .='     , a.trade_type';
        $sql .='     , c.class_separation';
        $sql .='     , a.quantity';
        $sql .='     , a.is_deleted ';
        $sql .='     , a.original_price';
        $sql .='     , a.transaction_price';
        $sql .='     , a.modified';
        $sql .='     , a.facility_sticker_no';
        $sql .='     , a.hospital_sticker_no';
        $sql .='     , a.lot_no';
        $sql .="     , to_char(a.validated_date , 'YYYY/mm/dd') as validated_date";
        $sql .='     , coalesce(h.department_name , k.department_name ) as asset_department_name ';
        $sql .='     , coalesce(i.facility_name , j.facility_name ) as asset_facility_name ';
        $sql .='   from ';
        $sql .='     trn_stickers as a  ';
        $sql .='     left join mst_item_units as b  ';
        $sql .='       on b.id = a.mst_item_unit_id  ';
        $sql .='     left join mst_unit_names as b1  ';
        $sql .='       on b1.id = b.mst_unit_name_id  ';
        $sql .='     left join mst_unit_names as b2  ';
        $sql .='       on b2.id = b.per_unit_name_id  ';
        $sql .='     left join mst_facility_items as c '; 
        $sql .='       on c.id = b.mst_facility_item_id  ';
        $sql .='     left join mst_dealers as d  ';
        $sql .='       on d.id = c.mst_dealer_id  ';
        $sql .='     left join mst_departments as e  ';
        $sql .='       on e.id = a.mst_department_id  ';
        $sql .='     left join mst_facilities as f  ';
        $sql .='       on f.id = e.mst_facility_id  ';
        $sql .='     left join trn_claims as g ';
        $sql .='       on g.id = a.sale_claim_id';
        $sql .='     left join mst_departments as h ';
        $sql .='       on h.id = g.department_id_to';
        $sql .='     left join mst_facilities as i ';
        $sql .='       on i.id = h.mst_facility_id ';
        $sql .='     left join mst_facilities as j ';
        $sql .='       on j.id = c.mst_facility_id ';
        $sql .='     left join mst_departments as k ';
        $sql .='       on k.mst_facility_id = j.id ';
        $sql .='   where ';
        $sql .='     a.id = ' .  $id;
        $sticker = $this->TrnSticker->query($sql);
        
        // 移動履歴
        $sql  = ' select ';
        $sql .= "       to_char(a.move_date,'YYYY/mm/dd') as move_date";
        $sql .= '     , ( case a.record_type ';
        foreach(Configure::read('RecordType.list') as $key => $val){
            $sql .= "       when ${key} then '${val}' ";
        }
        $sql .= '       end ) as status';
        $sql .= '     , a.facility_sticker_no ';
        $sql .= '     , a.hospital_sticker_no ';
        $sql .= '     , b.department_name ';
        $sql .= '     , c.facility_name  ';
        $sql .= '   from ';
        $sql .= '     trn_sticker_records as a  ';
        $sql .= '     left join mst_departments as b  ';
        $sql .= '       on b.id = a.mst_department_id  ';
        $sql .= '     left join mst_facilities as c  ';
        $sql .= '       on c.id = b.mst_facility_id  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.trn_sticker_id = ' . $id;
        $sql .= '   order by ';
        $sql .= '     a.move_seq ';
        $sticker_records = $this->TrnStickerRecord->query($sql);
        
        $this->set( 'sticker' , $sticker );
        $this->set( 'sticker_records' , $sticker_records );
        $this->request->data['TrnSticker']['lot_no'] = $sticker[0][0]['lot_no'];
        $this->request->data['TrnSticker']['validated_date'] = $sticker[0][0]['validated_date'];
    }
    
    function change_trade_type(){
        $_id = $this->request->data['trnsticker_id_selected'];
        //トランザクション開始
        $this->TrnSticker->begin();
        
        //行ロック
        $this->TrnSticker->query('select * from trn_stickers as a where a.id = ' . $_id . ' for update ');
        
        $this->TrnSticker->create();
        // 臨時品売り上げタイミングによって切り替えるシール区分を変える。
        if($this->Session->read('Auth.Config.TemporaryClaimType') == '1'){
            $trade_type = Configure::read('ClassesType.Constant');
        }else{
            $trade_type = Configure::read('ClassesType.SemiConstant');
        }
        $this->TrnSticker->create();
        $ret = $this->TrnSticker->updateAll(
            array(
                'TrnSticker.trade_type' => $trade_type
                ),
            array(
                'TrnSticker.id' => $_id,
                ),
            -1
            );
        if(!$ret){
            $this->TrnSticker->rollback();
            $this->Session->setFlash('シール区分変更処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('edit');
        }
        
        //コミット
        $this->TrnSticker->commit();
        $this->Session->setFlash('シール区分を変更しました', 'growl', array('type'=>'star') );
        
        $this->set('view_type' , 'edit');
        $this->__setSticker( $_id );
        $this->render('edit_result');
    }
    
    
    public function seal($type){
        switch($type){
          case '1':
            //center
            $this->printCenterSticker();
            break;
          case '2':
            //hospital
            $this->printHospitalSticker();
            break;
          case '3':
            //cost
            $this->printCostSticker();
            break;
        }
    }
    
    private function printCenterSticker(){
        $_fno = array();
        foreach ($this->request->data['StickerNo'] as $data) {
            if (isset($data['checked'])){
                $_fno[] = $data['sticker_no'];
            }
        }
        $fname = $this->request->data['facility_name'];
        $fid = $this->request->data['facility_id'];
        $stickers = $this->Stickers->getFacilityStickers($_fno, $fname, $fid);
        
        $this->layout = 'xml/default';
        $this->set('stickers', $stickers);
        $this->render('/storages/seal');
    }
    
    private function printCostSticker(){
        $ids = array();
        foreach ($this->request->data['StickerNo'] as $data) {
            if (isset($data['checked'])){
                $ids[] = $data['id'];
            }
        }
        // コストシールデータ取得
        $records = $this->Stickers->getCostStickers($ids);
        // 空なら初期化
        if(!$records){ $records = array();}
        
        $this->layout = 'xml/default';
        $this->set('records', $records);
        $this->render('/stickerissues/cost_sticker');
    }
    
    private function printHospitalSticker(){
        $_hno = array();
        
        foreach ($this->request->data['StickerNo'] as $data) {
            if (isset($data['checked'])){
                $_hno[] = $data['hospital_sticker_no'];
            }
        }
        
        $order = 'MstDepartment.department_name asc, MstShelfNameTo.code asc';
        
        $data = $this->Stickers->getHospitalStickers($_hno, $order,2);
        $this->set('sortkey' , '病院情報,センター棚番号,商品ID');
        $this->set('Sticker' , $data);
        $this->layout = 'xml/default';
        $this->render('/stickerissues/hospital_sticker');

        
    }
    
    public function read_barcode(){
        // センター情報取得
        $this->set('view_type' , 'edit');
        $_id = $this->request->data['trnsticker_id_selected'];
        $this->__setSticker( $_id );
        
        if($this->request->data['IsRead'] == '1'){
            $_barcode = $this->Barcode->readEan128($this->request->data['barcode']);
            if(false === $_barcode){
                $this->Session->setFlash('バーコードが正しくありません', 'growl', array('type'=>'error'));
                $this->render('edit_result');
                return;
            }else if($this->isNNs($_barcode['validated_date']) && $this->isNNs($_barcode['lot_no'])){
                $this->Session->setFlash('ロット情報が取得できませんでした', 'growl', array('type'=>'error'));
                $this->render('edit_result');
                return;
            }
            
            $this->set('BarcodeInfo', $_barcode);
            $this->request->data['TrnSticker']['lot_no'] = $_barcode['lot_no'];
            $this->request->data['TrnSticker']['validated_date'] = $this->getFormatedValidateDate($_barcode['validated_date']);
            $this->render('edit_result');
        }else{
            $this->render('edit_result');
        }
    }
    
    public function report(){
        //検索条件指定
        App::import("sanitize");
        $_conditions = "";
        $_displaycond = "";
        if(isset($this->request->data['Sticker']['facilityCode']) && $this->request->data['Sticker']['facilityCode'] != ""){
            $_conditions .= " and c24.facility_code = '".Sanitize::escape($this->request->data['Sticker']['facilityCode'])."'";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "配置施設 = ".Sanitize::escape($this->request->data['Sticker']['facilityName'])." ";
        }
        if(isset($this->request->data['Sticker']['departmentCode']) && $this->request->data['Sticker']['departmentCode'] != ""){
            $_conditions .= " and c11.department_code = '".Sanitize::escape($this->request->data['Sticker']['departmentCode'])."'";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "配置部署 = ".Sanitize::escape($this->request->data['Sticker']['departmentCode'])." ";
        }
        if($this->request->data['MstFacilityItem']['internal_code'] != ""){
            $_conditions .= " and a.internal_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."%'";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "商品ID = ".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])." ";
        }
        if($this->request->data['MstFacilityItem']['item_code'] != ""){
            $_conditions .= " and a.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "製品番号 = ".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])." ";
        }
        if($this->request->data['TrnSticker']['lot_no'] != ""){
            $_conditions .= " and c.lot_no LIKE '%".Sanitize::escape($this->request->data['TrnSticker']['lot_no'])."%'";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "ロット番号 = ".Sanitize::escape($this->request->data['TrnSticker']['lot_no'])." ";
        }
        if($this->request->data['MstFacilityItem']['item_name'] != ""){
            $_conditions .= " and a.item_name LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "商品名 = ".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])." ";
        }
        if($this->request->data['MstDealer']['dealer_name'] != ""){
            $_conditions .= " and a1.dealer_name LIKE '%".Sanitize::escape($this->request->data['MstDealer']['dealer_name'])."%'";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "販売元 = ".Sanitize::escape($this->request->data['MstDealer']['dealer_name'])." ";
        }
        if($this->request->data['MstFacilityItem']['standard'] != ""){
            $_conditions .= " and a.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "規格 = ".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])." ";
        }
        if($this->request->data['TrnSticker']['facility_sticker_no'] != ""){
            $_conditions .= " and ( c.facility_sticker_no LIKE '%".Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."%'";
            $_conditions .= " or c.hospital_sticker_no LIKE '%".Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."%' )";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "シール番号 = ".Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])." ";
        }
        if($this->request->data['TrnSticker']['is_active'] != '0'){
            $_conditions .= " and a.is_deleted = false";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "有効なシールのみ";
        }

        if($this->request->data['MstFacilityItem']['spare_key1'] != ""){
            $_conditions .= " and a.spare_key1 LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['spare_key1'])."%'";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "予備キー1 = ".Sanitize::escape($this->request->data['MstFacilityItem']['spare_key1'])." ";
        }
        
        if($this->request->data['MstFacilityItem']['spare_key2'] != ""){
            $_conditions .= " and a.spare_key2 LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['spare_key2'])."%'";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "予備キー2 = ".Sanitize::escape($this->request->data['MstFacilityItem']['spare_key2'])." ";
        }
        if($this->request->data['MstFacilityItem']['spare_key3'] != ""){
            $_conditions .= " and a.spare_key3 LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['spare_key3'])."%'";
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "予備キー3 = ".Sanitize::escape($this->request->data['MstFacilityItem']['spare_key3'])." ";
        }
        // フリーキーワード
        if( isset($this->request->data['Free']['text']) &&  $this->request->data['Free']['text'] != '') {
            $_conditions .= " and ";
            $_conditions .= "coalesce(a.internal_code,'')||";
            $_conditions .= "coalesce(a.item_code, '')||";
            $_conditions .= "coalesce(a.item_name, '')||";
            $_conditions .= "coalesce(a.standard, '')||";
            $_conditions .= "coalesce(a.spare_key1, '')||";
            $_conditions .= "coalesce(a.spare_key2, '')||";
            $_conditions .= "coalesce(a.spare_key3, '')||";
            $_conditions .= "coalesce(a1.dealer_name, '')||";
            $_conditions .= "coalesce(c.facility_sticker_no, '')||";
            $_conditions .= "coalesce(c.hospital_sticker_no, '')||";
            $_conditions .= "coalesce(c.lot_no, '')";
            $_conditions .= " ilike '%" . $this->request->data['Free']['text'] . "%'";
            
            if($_displaycond != ""){
                $_displaycond .= ",";
            }
            $_displaycond .= "フリー検索 = ".Sanitize::escape($this->request->data['Free']['text'])." ";
            
            
        }
        
        
        //施主コードでのソート・自センターの施設採用品のみという条件追加
        //改ページごとに行番号を振り直す処理追加
        $sql =
          "select
            a.internal_code as 商品ID,
            a.item_name as 商品名,
            a.standard as 規格,
            a.item_code as 製品番号,
            a1.dealer_name as 販売元,
            (case b.per_unit
             when 1 then b1.unit_name
             else b1.unit_name || '(' || cast(b.per_unit as varchar) || b2.unit_name || ')' end
            ) as 包装単位,
            c.hospital_sticker_no as 部署シール,
            c1.claim_date as 仕入日,
            trunc( ( c1.unit_price / cc1.per_unit ) * cc2.per_unit  , 2) as 仕入単価,
            c12.facility_name as 仕入先,
            c13.transaction_price as 仕入設定,
            c2.claim_date as 売上日,
            c2.unit_price as 売上単価,
            c21.department_name as 配置部署,
            c22.facility_name as 売上先,
            c23.transaction_price as 売上設定,
            (case
             when c1.unit_price = 0 then '仕入ゼロ'
             when c2.unit_price = 0 then '売上ゼロ'
             when c1.unit_price != c13.transaction_price then '仕入違い'
             when c2.unit_price != c23.transaction_price then '売上違い'
             when ( c1.unit_price / cc1.per_unit ) * cc2.per_unit > c2.unit_price then '逆鞘'
             else null end
            ) as 差異内容,
            c24.facility_name as 施主名,
            c24.facility_code as 施主コード
    from
            mst_facility_items as a
            left join mst_facilities as c24
                    on a.mst_owner_id = c24.id
                    and c24.facility_type=".Configure::read('FacilityType.donor')."
            left join mst_dealers as a1
                    on a.mst_dealer_id = a1.id
            left join mst_item_units as b
                    on b.mst_facility_item_id = a.id 
            left join mst_unit_names as b1
                    on b.mst_unit_name_id = b1.id
            left join mst_unit_names as b2
                    on b.per_unit_name_id = b2.id
            left join trn_stickers as c
                    on c.mst_item_unit_id = b.id 
            left join trn_claims as c1
                    on c.buy_claim_id = c1.id
            left join mst_item_units as cc1
                    on cc1.id = c1.mst_item_unit_id
            left join mst_departments as c11
                    on c1.department_id_from = c11.id
            left join mst_facilities as c12
                    on c11.mst_facility_id = c12.id
            left join mst_transaction_configs as c13
                    on c1.mst_item_unit_id = c13.mst_item_unit_id
                    and c12.id = c13.partner_facility_id
                    and c13.start_date <= c1.claim_date
                    and c13.end_date > c1.claim_date
            left join trn_claims as c2
                    on c.sale_claim_id = c2.id
            left join mst_item_units as cc2
                    on cc2.id = c2.mst_item_unit_id
            left join mst_departments as c21
                    on c2.department_id_to = c21.id
            left join mst_facilities as c22
                    on c21.mst_facility_id = c22.id
            left join mst_transaction_configs as c23
                    on c2.mst_item_unit_id = c23.mst_item_unit_id
                    and c22.id = c23.partner_facility_id
                    and c23.start_date <= c2.claim_date
                    and c23.end_date > c2.claim_date

    where a.id = b.mst_facility_item_id ".
            " and a.mst_facility_id = ".$this->request->data['facility_id'].
              " and b.id = c.mst_item_unit_id
    and (c1.id is not null or c2.id is not null)
    and (c1.unit_price = 0
    or c2.unit_price = 0
    or (c1.unit_price != c13.transaction_price)
    or (c2.unit_price != c23.transaction_price)
    or (c1.unit_price > c2.unit_price))".
                $_conditions;
        $sql .= " order by  c24.facility_code ";

        $head = array(
            '施設情報',
            '差異内容',
            '行番号',
            '商品名',
            '規格',
            '製品番号',
            '販売元',
            '商品ID',
            '仕入先',
            '仕入設定',
            '仕入日',
            '包装単位',
            '部署シール',
            '仕入単価',
            '売上先',
            '売上日',
            '売上設定',
            '売上単価',
            '配置部署',
            '検索条件',
            '印刷年月日',
            );
        
        $res = $this->MstFacilityItem->query($sql);
        
        $SearchResult = array();
        $cnt = 1;
        $i = 0;
        $before_facility_info = "";
        
        foreach($res as $row){
            if($row[0]['施主名'] == ""){
                $SearchResult[$i]['施設情報'] = "なし";
            }else{
                $SearchResult[$i]['施設情報'] = $row[0]['施主名']."[".$row[0]['施主コード']."]";
            }
            
            //改ページ番号が変わるたびに行番号初期化
            if($before_facility_info !== $SearchResult[$i]['施設情報']){
                $cnt = 1;
            } else {
                $cnt++;
            }
            $before_facility_info = $SearchResult[$i]['施設情報'];
            
            $SearchResult[$i]['差異内容']   = $row[0]['差異内容'];
            $SearchResult[$i]['行番号']     = $cnt;
            $SearchResult[$i]['商品名']     = $row[0]['商品名'];
            $SearchResult[$i]['規格']       = $row[0]['規格'];
            $SearchResult[$i]['製品番号'] = $row[0]['製品番号'];
            $SearchResult[$i]['販売元']     = $row[0]['販売元'];
            $SearchResult[$i]['商品ID']     = $row[0]['商品id'];
            $SearchResult[$i]['仕入先']     = $row[0]['仕入先'];
            $SearchResult[$i]['仕入設定']   = $row[0]['仕入設定'];
            $SearchResult[$i]['仕入日']     = date('Y/m/d', strtotime($row[0]['仕入日']));
            $SearchResult[$i]['包装単位']   = $row[0]['包装単位'];
            $SearchResult[$i]['部署シール'] = $row[0]['部署シール'];
            $SearchResult[$i]['仕入単価']   = $row[0]['仕入単価'];
            $SearchResult[$i]['売上先']     = $row[0]['売上先'];
            $SearchResult[$i]['売上日']     = date('Y/m/d', strtotime($row[0]['売上日']));
            $SearchResult[$i]['売上設定']   = $row[0]['売上設定'];
            $SearchResult[$i]['売上単価']   = $row[0]['売上単価'];
            $SearchResult[$i]['配置部署']   = $row[0]['配置部署'];
            if($_displaycond == ""){
                $SearchResult[$i]['検索条件'] = "なし";
            }else{
                $SearchResult[$i]['検索条件'] = $_displaycond;
            }
            $SearchResult[$i]['印刷年月日'] = date("Y/m/d");
            $i++;
        }
        
        $this->layout = 'xml/default';
        $this->set('head', $head);
        $this->set('result', $SearchResult);
        $this->render('print');
    }

    // CSV出力
    public function export_csv(){
        //センターID
        $where = '     and c.mst_facility_id = ' . $this->request->data['facility_id'];
        //ロット番号(部分一致)
        if($this->request->data['TrnSticker']['lot_no'] != '' ){
            $where .= "     and a.lot_no like '%" . $this->request->data['TrnSticker']['lot_no'] . "%'";
        }
        //シール番号(部分一致)
        if($this->request->data['TrnSticker']['facility_sticker_no'] != '' ){
            $where .= "     and ( a.facility_sticker_no  like '%" . $this->request->data['TrnSticker']['facility_sticker_no'] . "%' ";
            $where .= "     or a.hospital_sticker_no like '%" . $this->request->data['TrnSticker']['facility_sticker_no'] . "%' ) ";
        }
        //商品ID(完全一致)
        if($this->request->data['MstFacilityItem']['internal_code'] != '' ){
            $where .= "     and c.internal_code = '" . $this->request->data['MstFacilityItem']['internal_code'] . "'";
        }
        //製品番号(前方一致)
        if($this->request->data['MstFacilityItem']['item_code'] != '' ){
            $where .= "     and c.item_code like '" . $this->request->data['MstFacilityItem']['item_code'] . "%'";
        }
        //商品名(部分一致)
        if($this->request->data['MstFacilityItem']['item_name'] != '' ){
            $where .= "     and c.item_name like '%" . $this->request->data['MstFacilityItem']['item_name'] . "%'";
        }
        //規格(部分一致)
        if($this->request->data['MstFacilityItem']['standard'] != '' ){
            $where .= "     and c.standard like '%" . $this->request->data['MstFacilityItem']['standard'] . "%'";
        }
        //販売元(部分一致)
        if($this->request->data['MstDealer']['dealer_name'] != '' ){
            $where .= "     and c1.dealer_name like '%" . $this->request->data['MstDealer']['dealer_name'] . "%'";
        }
        //施設
        if(isset($this->request->data['Sticker']['facilityCode'] ) && $this->request->data['Sticker']['facilityCode'] != '' ){
            $where .= "     and e.facility_code = '" . $this->request->data['Sticker']['facilityCode'] . "'";
        }
        //部署
        if(isset($this->request->data['Sticker']['departmentCode']) && $this->request->data['Sticker']['departmentCode'] != '' ){
            $where .= "     and d.department_code = '" . $this->request->data['Sticker']['departmentCode'] . "'";
        }
        //シール区分
        if(isset($this->request->data['TrnSticker']['trade_type']) && $this->request->data['TrnSticker']['trade_type'] != ''){
            $where .= '     and a.trade_type = ' . $this->request->data['TrnSticker']['trade_type'];
        }
        
        //有効なシールのみ
        if(isset($this->request->data['TrnSticker']['is_active']) && $this->request->data['TrnSticker']['is_active'] == 1){
            $where .= "     and a.is_deleted = false ";
            $where .= "     and a.quantity > 0 ";
        }
        
        // 予備キー1
        if( isset($this->request->data['MstFacilityItem']['spare_key1']) &&  $this->request->data['MstFacilityItem']['spare_key1'] != '') {
            $where .= "and c.spare_key1 like '%" . pg_escape_string($this->request->data['MstFacilityItem']['spare_key1'])."%'";
        }
        
        // 予備キー2
        if( isset($this->request->data['MstFacilityItem']['spare_key2']) &&  $this->request->data['MstFacilityItem']['spare_key2'] != '') {
            $where .= "and c.spare_key2 like '%" . pg_escape_string($this->request->data['MstFacilityItem']['spare_key2'])."%'";
        }
        
        // 予備キー3
        if( isset($this->request->data['MstFacilityItem']['spare_key3']) &&  $this->request->data['MstFacilityItem']['spare_key3'] != '') {
            $where .= "and c.spare_key3 like '%" . pg_escape_string($this->request->data['MstFacilityItem']['spare_key3'])."%'";
        }
        
        // フリーキーワード
        if( isset($this->request->data['Free']['text']) &&  $this->request->data['Free']['text'] != '') {
            $where .= " and ";
            $where .= "coalesce(c.internal_code,'')||";
            $where .= "coalesce(c.item_code, '')||";
            $where .= "coalesce(c.item_name, '')||";
            $where .= "coalesce(c.standard, '')||";
            $where .= "coalesce(c.spare_key1, '')||";
            $where .= "coalesce(c.spare_key2, '')||";
            $where .= "coalesce(c.spare_key3, '')||";
            $where .= "coalesce(c1.dealer_name, '')||";
            $where .= "coalesce(a.facility_sticker_no, '')||";
            $where .= "coalesce(a.hospital_sticker_no, '')||";
            $where .= "coalesce(a.lot_no, '')";
            $where .= " ilike '%" . $this->request->data['Free']['text'] . "%'";
        }
        
        
        $sql  = ' select ';
        $sql .= '       c.internal_code                                 as "商品ID" ';
        $sql .= '     , c.item_name                                     as 商品名 ';
        $sql .= '     , c.item_code                                     as 製品番号 ';
        $sql .= '     , c.standard                                      as 規格 ';
        $sql .= '     , c1.dealer_name                                  as 販売元 ';
        $sql .= '     , a.quantity                                      as 数量 ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then b1.unit_name  ';
        $sql .= "         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                                 as 包装単位 ';
        $sql .= '     , a.lot_no                                        as ロット番号 ';
        $sql .= "     , to_char(a.validated_date, 'YYYY/MM/DD')         as 有効期限 ";
        $sql .= '     , a.facility_sticker_no                           as センターシール番号 ';
        $sql .= '     , a.hospital_sticker_no                           as 部署シール番号 ';
        $sql .= '     , e.facility_name                                 as 配置施設 ';
        $sql .= '     , d.department_name                               as 配置部署 ';
        $sql .= '     , a.transaction_price                             as 仕入単価 ';
        $sql .= '     , c.medical_code                                  as 医事コード ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when c.is_lowlevel = true  ';
        $sql .= "         then '低レベル品'  ";
        foreach(Configure::read('Classes') as $k => $v){
            $sql .= '         when a.trade_type = ' . $k;
            $sql .= "         then '".$v."'  ";
        }
        $sql .= '         end ';
        $sql .= '     )                                                 as 取引区分 ';
        $sql .= "     , to_char(f.work_date, 'YYYY/MM/DD')              as 発注日 ";
        $sql .= '     , f.work_no                                       as 発注番号 ';
        $sql .= '     , f2.facility_name                                as 発注先 ';
        $sql .= "     , to_char(g.work_date, 'YYYY/MM/DD')              as 入荷日 ";
        $sql .= "     , g.work_no                                       as 入荷番号 ";
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when g1.per_unit = 1  ';
        $sql .= '         then g2.unit_name  ';
        $sql .= "         else g2.unit_name || '(' || g1.per_unit || g3.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                                 as 入荷包装単位 ';
        $sql .= "     , to_char(h.work_date, 'YYYY/MM/DD')              as 入庫日 ";
        $sql .= "     , h.work_no                                       as 入庫番号 ";
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when h1.per_unit = 1  ';
        $sql .= '         then h2.unit_name  ';
        $sql .= "         else h2.unit_name || '(' || h1.per_unit || h3.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                                 as 入庫包装単位 ';
        $sql .= "     , to_char(i.work_date, 'YYYY/MM/DD')              as 出荷日 ";
        $sql .= "     , i.work_no                                       as 出荷番号 ";
        $sql .= "     , i2.facility_name || ' / ' || i1.department_name as 出荷施設 ";
        $sql .= "     , to_char(j.work_date, 'YYYY/MM/DD')              as 受領日 ";
        $sql .= "     , j.work_no                                       as 受領番号 ";
        $sql .= "     , j2.facility_name || ' / ' || j1.department_name as 受領施設 ";
        $sql .= "     , to_char(k.work_date, 'YYYY/MM/DD')              as 消費日 ";
        $sql .= "     , k.work_no                                       as 消費番号 ";
        $sql .= "     , k2.facility_name || ' / ' || k1.department_name as 消費施設  ";
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as c1  ';
        $sql .= '       on c1.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_departments as d  ';
        $sql .= '       on d.id = a.mst_department_id  ';
        $sql .= '     left join mst_facilities as e  ';
        $sql .= '       on e.id = d.mst_facility_id  ';
        $sql .= '     left join trn_orders as f  ';
        $sql .= '       on f.id = a.trn_order_id  ';
        $sql .= '     left join mst_departments as f1  ';
        $sql .= '       on f1.id = f.department_id_to  ';
        $sql .= '     left join mst_facilities as f2  ';
        $sql .= '       on f2.id = f1.mst_facility_id  ';
        $sql .= '     left join trn_receivings as g  ';
        $sql .= '       on g.id = a.trn_receiving_id  ';
        $sql .= '     left join mst_item_units as g1  ';
        $sql .= '       on g1.id = g.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as g2  ';
        $sql .= '       on g2.id = g1.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as g3  ';
        $sql .= '       on g3.id = g1.per_unit_name_id  ';
        $sql .= '     left join trn_storages as h  ';
        $sql .= '       on h.id = a.trn_storage_id  ';
        $sql .= '     left join mst_item_units as h1  ';
        $sql .= '       on h1.id = h.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as h2  ';
        $sql .= '       on h2.id = h1.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as h3  ';
        $sql .= '       on h3.id = h1.per_unit_name_id  ';
        $sql .= '     left join trn_shippings as i  ';
        $sql .= '       on i.id = a.trn_shipping_id  ';
        $sql .= '     left join mst_departments as i1  ';
        $sql .= '       on i1.id = i.department_id_to  ';
        $sql .= '     left join mst_facilities as i2  ';
        $sql .= '       on i2.id = i1.mst_facility_id  ';
        $sql .= '     left join trn_receivings as j  ';
        $sql .= '       on j.id = a.receipt_id  ';
        $sql .= '     left join mst_departments as j1  ';
        $sql .= '       on j1.id = j.department_id_to  ';
        $sql .= '     left join mst_facilities as j2  ';
        $sql .= '       on j2.id = j1.mst_facility_id  ';
        $sql .= '     left join trn_consumes as k  ';
        $sql .= '       on k.id = a.trn_consume_id  ';
        $sql .= '     left join mst_departments as k1  ';
        $sql .= '       on k1.id = k.mst_department_id  ';
        $sql .= '     left join mst_facilities as k2  ';
        $sql .= '       on k2.id = k1.mst_facility_id  ';
        $sql .= '     inner join mst_user_belongings as l ';
        $sql .= '       on l.mst_facility_id = e.id';
        $sql .= '       and l.mst_user_id = ' .$this->request->data['TrnSticker']['mst_user_id']; //ユーザが操作可能な施設にあるシールのみ表示する
        $sql .= '       and l.is_deleted = false ';
        $sql .= '   where ';
        $sql .= "     a.lot_no <> ' '  "; //入荷済み未入庫のレコードは除外
        $sql .= $where;
        $sql .= '   order by a.facility_sticker_no,a.hospital_sticker_no,a.modified desc';
        $this->db_export_csv($sql , "シール一覧", 'index');
    }
}
