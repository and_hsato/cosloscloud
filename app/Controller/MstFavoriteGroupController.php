<?php
/**
 * MstFavoriteGroupsController
 * @version 1.0.0
 */

class MstFavoriteGroupController extends AppController {
    var $name = 'MstFavoriteGroup';

    /**
     *
     * @var array $uses
     */
    var $uses = array('MstFavoriteGroup');

    /**
     * @var array $components
     */
    var $components = array('CsvWriteUtils');
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;


    function beforeFilter() {
        parent::beforeFilter();
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }
    
    
    function index(){
        $this->redirect('favorite_group_list');
    }

    /**
     * お気に入りグループ一覧
     */
    function favorite_group_list() {
        // $this->setRoleFunction(96);

        $FavoriteGroup_List = array();
        App::import('Sanitize');

        //初回時でない場合のみデータを検索する
        if(isset($this->request->data['MstFavoriteGroup']['is_search'])){
            $where  ='';
            //ユーザ入力値による検索条件の作成--------------------------------------------
            //コード(LIKE検索)
            if($this->request->data['MstFavoriteGroup']['search_code'] != ""){
                $where .= " and a.group_code LIKE '%".Sanitize::escape($this->request->data['MstFavoriteGroup']['search_code'])."%'";
            }
            //名称(LIKE検索)
            if($this->request->data['MstFavoriteGroup']['search_name'] != ""){
                $where .= " and a.group_name LIKE '%".Sanitize::escape($this->request->data['MstFavoriteGroup']['search_name'])."%'";
            }
            //検索条件の作成終了---------------------------------------------------------

            $order = ' order by ' ;
            $order .= 'a.id';

            $sql  = 'select ';
            $sql .= '      a.id         as "MstFavoriteGroup__id"';
            $sql .= '    , a.group_code as "MstFavoriteGroup__group_code"';
            $sql .= '    , a.group_name as "MstFavoriteGroup__group_name"';
            $sql .= '  from ';
            $sql .= '    mst_favorite_groups as a  ';
            $sql .= '  where 1=1 ';
            $sql .= $where;
            $sql .= '  and  a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .=$order;
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'MstFavoriteGroup'));
            $sql .= ' limit ' . $this->_getLimitCount();

            //SQL実行
            $FavoriteGroup_List = $this->MstFavoriteGroup->query($sql);
        }
        $this->set('FavoriteGroup_List',$FavoriteGroup_List);
    }


    /**
     * 新規
     */
    function add() {
        //$this->setRoleFunction(96);
        //プルダウン作成用データ取得
        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
    }

    /**
     * 編集
     */
    function mod() {
        //$this->setRoleFunction(96);
        $result = $this->getData($this->request->data['MstFavoriteGroup']['id']);
        $this->request->data = $result[0];
    }

    /**
     * 結果
     */
    function result() {
        $data = array();
        $now = date('Y/m/d H:i:s.u');

        //トランザクション開始
        $this->MstFavoriteGroup->begin();
        //行ロック（更新時のみ）
        if(isset($this->request->data['MstFavoriteGroup']['id'])){
            $this->MstFavoriteGroup->query('select * from mst_favorite_groups as a where a.id = ' .$this->request->data['MstFavoriteGroup']['id']. ' for update ');
        }

        //保存データの整形
        if(isset($this->request->data['MstFavoriteGroup']['id'])){
            //更新の場合
            $data['MstFavoriteGroup']['id']      = $this->request->data['MstFavoriteGroup']['id'];
        }else{
            //新規の場合
            $data['MstFavoriteGroup']['created'] = $now;
            $data['MstFavoriteGroup']['creater'] = $this->Session->read('Auth.MstUser.id');
        }

        $data['MstFavoriteGroup']['group_code']      = $this->request->data['MstFavoriteGroup']['group_code'];
        $data['MstFavoriteGroup']['group_name']      = $this->request->data['MstFavoriteGroup']['group_name'];
        $data['MstFavoriteGroup']['mst_facility_id'] = $this->Session->read('Auth.facility_id_selected');
        $data['MstFavoriteGroup']['modified']        = $now;
        $data['MstFavoriteGroup']['modifier']        = $this->Session->read('Auth.MstUser.id');

        //SQL実行
        if(!$this->MstFavoriteGroup->save($data)){
            //ロールバック
            $this->MstFavoriteGroup->rollback();
            //エラーメッセージ
            $this->Session->setFlash('お気に入りグループ情報の登録に失敗しました。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('accounte_list');
        }
        $this->MstFavoriteGroup->commit();
    }

    /*
     * データ取得
     */
    private function getData($id){
        $sql  = '  select ';
        $sql .= '      a.id          as "MstFavoriteGroup__id" ';
        $sql .= '    , a.group_name  as "MstFavoriteGroup__group_name" ';
        $sql .= '    , a.group_code  as "MstFavoriteGroup__group_code" ';
        $sql .= '  from ';
        $sql .= '    mst_favorite_groups as a  ';
        $sql .= '  where ';
        $sql .= '    a.id = ' . $id;

        return $this->MstFavoriteGroup->query($sql);
    }

    public function export_csv(){
        App::import('Sanitize');
        $where  ='';
        //ユーザ入力値による検索条件の作成--------------------------------------------
        //コード(LIKE検索)
        if($this->request->data['MstFavoriteGroup']['search_code'] != ""){
            $where .= " and a.group_code LIKE '%".Sanitize::escape($this->request->data['MstFavoriteGroup']['search_code'])."%'";
        }
        //名称(LIKE検索)
        if($this->request->data['MstFavoriteGroup']['search_name'] != ""){
            $where .= " and a.group_name LIKE '%".Sanitize::escape($this->request->data['MstFavoriteGroup']['search_name'])."%'";
        }
        //検索条件の作成終了---------------------------------------------------------

        $order = ' order by ' ;
        $order .= 'a.id';

        $sql  = 'select ';
        $sql .= '      a.group_code as "お気に入りグループコード"';
        $sql .= '    , a.group_name as "お気に入りグループ名称"';
        $sql .= '  from ';
        $sql .= '    mst_favorite_groups as a  ';
        $sql .= '  where 1=1 ';
        $sql .= $where;
        $sql .= '  and  a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .=$order;
        
        $this->db_export_csv($sql , "お気に入りグループ一覧", 'favorite_group_list');
    }
}
