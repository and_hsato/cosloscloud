<?php
/**
 * 日付処理関連
 * @since 2016/02/17
 */
class DateUtilComponent extends Component {

    private static $gengoList = array(
        array('name' => '平成', 'timestamp' =>  600188400),  // 1989-01-08
        array('name' => '昭和', 'timestamp' => -1357635600), // 1926-12-25
        array('name' => '大正', 'timestamp' => -1812186000), // 1912-07-30
        array('name' => '明治', 'timestamp' => -3216790800), // 1868-01-25
    );

    /*
     * 指定された年月から$months後の末日(23:59:59)を返す
     * (1/31から単純に1ヶ月後などとした場合3/3とかになるので1日をベースに処理している)
     */
    public static function addMonthOfEndDay($date, $months) {
        $baseFirstDay = date('Y-m-01', strtotime($date));
        $returnEndDay = date('Y-m-t 23:59:59',
                strtotime($months . ' month',
                        strtotime($baseFirstDay)
                        )
                );
        return $returnEndDay;
    }

    /*
     * 指定された2つの日付の月の差($to - $from)を返す
     * (月の絶対値で処理している)
     */
    public static function diffMonth($from, $to) {
        $time1 = strtotime($from);
        $time2 = strtotime($to);
        $month1 = date("Y", $time1) * 12 + date("m", $time1);
        $month2 = date("Y", $time2) * 12 + date("m", $time2);
        $diff = $month2 - $month1;
        return $diff;
    }

    /*
     * 指定された2つの日付の月が同じかどうかを返す
     * (月の絶対値で処理している)
     */
    public static function monthEquals($date1, $date2) {
        $ret = self::diffMonth($date1, $date2);
        return $ret == 0;
    }

    /*
     * 1月から12月までの月のリストを返す
     */
    public static function getMonthList($format = '%02d') {
        $months = array();
        for($i = 1; $i <= 12; $i++) {
            $month = sprintf($format, $i);
            $months[$month] = $month;
        }
        return $months;
    }

    /*
     * 元号と和暦年(1年を元年と表記)、月（m月)、日（d日）に変換した文字列を返す
     * $timestamp　変換するタイムスタンプ
     * $isAddGetngo 元号を追加するかどうか
     */
    public static function getJpDate($timestamp, $isAddGengo = true){
        foreach (self::$gengoList as $g) {
          if ($g['timestamp'] <= $timestamp) {
              $gengo = $g;
              break;
          }
        }
        // 元号が取得できない場合はException
        if (empty($gengo)) {
            throw new Exception('Can not be converted to a timestamp : '.$timestamp);
        }
        $year = date('Y', $timestamp) - date('Y', $gengo['timestamp']) + 1;
        $year = $year == 1 ? '元' : $year;
        $month = date('m', $timestamp);
        $day = date('d', $timestamp);

        $JpDate  = $isAddGengo ? $gengo['name'].$year.'年' : $year.'年';
        $JpDate .= $month.'月';
        $JpDate .= $day.'日';

        return $JpDate;
    }
}
?>
