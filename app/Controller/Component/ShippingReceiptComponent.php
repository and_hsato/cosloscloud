<?php
/**
 * ShippingReceiptComponent
 * 臨時出荷用受領処理
 * @version 1.0.0
 * @since 2010/10/10
 */
class ShippingReceiptComponent extends Component {

    /**
     *
     * @var array $components
     */
    var $components = array('Session');

    private $TrnShippingHeader;
    private $TrnShipping;
    private $TrnPromiseHeader;
    private $TrnPromise;
    private $MstTransactionConfig;
    private $MstSalesConfig;
    private $TrnReceivingHeader;
    private $TrnReceiving;
    private $TrnStorageHeader;
    private $TrnStorage;
    private $TrnStock;
    private $TrnClaim;
    private $MstUnitName;
    private $TrnConsumeHeader;
    private $TrnConsume;
    private $TrnCloseHeader;
    private $TrnStickerRecord;
    private $TrnOrderHeader;
    private $TrnOrder;
    private $TrnSticker;
    private $MstDepartment;
    private $MstFacility;
    private $MstItemUnit;
    private $_controller;

    /**
     * 作業日時
     * @var strings
     */
    private $work_date = '';

    public function startup(&$controller){
        $this->work_date = '';
        $this->now = date('Y/m/d H:i:s');
        $this->_controller = $controller;
    }

    /***************************************************
     * 受領処理
     ***************************************************/

    /**
     * Receipts
     *
     * 受領時に必要な処理
     */
    public function Receipts($shipping_header_id,$shipping_id,$work_date){
        $this->Session->write('ExShipping.receibt_read_time',date('Y-m-d H:i:s'));

        $this->work_date = $work_date;
        //受領時に必要な処理1：引当明細テーブルの受領数の更新
        if(!$this->UpdatePromise_receipt_count($shipping_header_id,$shipping_id)){
            $this->rollbackTables();
            throw new Exception('引当明細テーブル更新でエラー');
        }
        
        if($this->Session->read('Auth.Config.ShippingConsumes') == '0'){
            //受領時に必要な処理2：臨時品の場合は、先に売上データを作成する（区分は明細を見ること）
            if(!$this->createClaims($shipping_header_id,$shipping_id , $work_date)){
                $this->rollbackTables();
                throw new Exception('売上データ作成でエラー');
            }
        }


        //受領時に必要な処理3：病院の入荷ヘッダ・明細の作成
        $key = $this->createHospitalReceiving($shipping_header_id,$shipping_id);

        if( (empty($key)) or (!$key) ){
            $this->rollbackTables();
            throw new Exception('病院の入荷ヘッダ・明細の作成でエラー');
        }
        //作成した入荷ヘッダと入荷明細のIDを取得
        while(list ($_key, $_val) = each($key[0])) {
            $shipping_header_key[$_key]=$_val;
        }
        while(list ($_key, $_val) = each($key[1])) {
            $shipping_key[$_key]=$_val;
        }
        //受領時に必要な処理4：病院の入庫ヘッダ・明細の作成
        $key2 = $this->createHospitalStorage($shipping_header_id,$shipping_id,$shipping_header_key,$shipping_key);
        if((empty($key2)) or (!$key2)){
            $this->rollbackTables();
            throw new Exception('病院の入庫ヘッダ・明細の作成でエラー');
        }
        //作成した入庫ヘッダと入庫明細のIDを取得
        while(list ($_key, $_val) = each($key2[0])) {
            $storage_header_key[$_key]=$_val;
        }
        while(list ($_key, $_val) = each($key2[1])) {
            $storage_key[$_key]=$_val;
        }

        //受領時に必要な処理5：在庫更新
        //(1)移動先病院在庫の在庫数を増やし、(臨時品の場合のみ)未入荷数を減らす
        //(2)移動元センター在庫の在庫数を減らし、予約数を減らす
        if(!$this->updateHospitalStock($shipping_header_id,$shipping_id)){
            $this->rollbackTables();
            throw new Exception('在庫更新でエラー');
        }
        //受領時に必要な処理6：出荷データの出荷状態を変更
        if(!$this->updateShipping($shipping_header_id,$shipping_id)){
            $this->rollbackTables();
            throw new Exception('出荷データのステータス更新でエラー');
        }
        if($this->Session->read('Auth.Config.ShippingConsumes') == '0'){
            //受領時に必要な処理7：臨時品の場合、消費データ作成＋病院在庫の在庫数を減らす
            if(!$this->createConsume($shipping_header_id,$shipping_id,$storage_header_key,$storage_key)){
                $this->rollbackTables();
                throw new Exception('臨時出荷の場合の消費データ作成＋在庫数更新でエラー');
            }
        }
        //受領時に必要な処理8：シールの移動履歴を作成する
        if(!$this->createStickerRecords($shipping_header_id,$shipping_id,$shipping_header_key,$shipping_key)){
            $this->rollbackTables();
            throw new Exception('シールの移動履歴作成でエラー');
        }
        return true;
    }

    /**
     * UpdatePromise_receipt_count
     *
     * 受領時に必要な処理その1：引当明細の受領数・未受領数を更新（預託品の場合を除く）
     */
    function UpdatePromise_receipt_count($header_id,$id){

        //出荷明細ごとにループして処理
        foreach($header_id as $header){
            foreach($id[$header] as $meisai){

                //出荷明細IDから出荷情報（出荷区分）を取得
                $shipping_type = $this->_controller->TrnShipping->find('first',array('fields'=>'shipping_type','recursive' => -1,'conditions'=>array('id'=>$meisai)));

                //預託品の場合は以降の処理を行わない
                if($shipping_type['TrnShipping']['shipping_type'] != Configure::read('ClassesType.Deposit')){
                    //出荷明細IDからシールIDを取得
                    $sticker_id = $this->_controller->TrnShipping->find('first',array('fields'=>'trn_sticker_id','recursive' => -1,'conditions'=>array('id'=>$meisai)));

                    //シールテーブルから引当明細IDと数量を取得
                    $promise_id = $this->_controller->TrnSticker->find('first',array('fields'=>'trn_promise_id,quantity','recursive' => -1,'conditions'=>array('id'=>$sticker_id['TrnShipping']['trn_sticker_id'])));

                    if($promise_id['TrnSticker']['trn_promise_id'] != ''){
                        //現在の受領数・未受領数を取得
                        $now_receipt_count = $this->_controller->TrnPromise->find('first',array('fields'=>array('receipt_count','unreceipt_count'),'recursive' => -1,'conditions'=>array('id'=>$promise_id['TrnSticker']['trn_promise_id'])));

                        //受領数を増やす計算
                        $receipt_count   = $now_receipt_count['TrnPromise']['receipt_count'] + $promise_id['TrnSticker']['quantity'];

                        //未領数を減らす計算
                        $unreceipt_count = $now_receipt_count['TrnPromise']['unreceipt_count'] - $promise_id['TrnSticker']['quantity'];

                        $condtion = array( 'TrnPromise.id ='    => $promise_id['TrnSticker']['trn_promise_id'] );
                        $updatefield = array( 'receipt_count'   => "'".$receipt_count."'",  //受領数
                                              'unreceipt_count' => "'".$unreceipt_count."'",//未受領数
                                              'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                              'modified'        => "'".$this->now."'",
                                              );

                        //TrnPromiseオブジェクトをcreate
                        $this->_controller->TrnPromise->create();

                        //SQL実行
                        if (!$this->_controller->TrnPromise->updateAll( $updatefield, $condtion )) {
                            $this->rollbackTables();
                            throw new Exception('引当明細更新エラー'
                                                . print_r($updatefield,true)
                                                . print_r($this->_controller->validateErrors($this->_controller->TrnPromise), true)
                                                );
                            exit;
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }


    /**
     * createClaims
     *
     * 受領時に必要な処理その2：売上が必要な場合は先に売上データを作成する
     */
    function createClaims($header_id ,$id , $work_date) {
        $ids = array();
        foreach($header_id as $header_id_data){
            foreach($id[$header_id_data] as $data){
                $ids[] = $data;
            }
        }


        $from = '';
        $from .="   from ";
        $from .="     trn_shippings as a  ";
        $from .="     left join trn_stickers as b  ";
        $from .="       on a.trn_sticker_id = b.id  ";
        $from .="     left join mst_item_units as c  ";
        $from .="       on c.id = b.mst_item_unit_id  ";
        $from .="     left join mst_facility_items as d  ";
        $from .="       on d.id = c.mst_facility_item_id  ";
        $from .="     left join mst_departments as f  ";
        $from .="       on f.id = a.department_id_to  ";
        $from .="     left join mst_facilities as g  ";
        $from .="       on g.id = f.mst_facility_id  ";
        $from .="     left join mst_sales_configs as e  ";
        $from .="       on e.mst_facility_item_id = d.id  ";
        $from .="       and e.mst_item_unit_id = c.id  ";
        $from .="       and e.partner_facility_id = g.id";
        $from .="       and e.start_date <= '" .$work_date."'";
        $from .="       and e.end_date > '".$work_date."'";
        $from .="     left join trn_set_stock_headers as h  ";
        $from .="       on b.trn_set_stock_id = h.id  ";
        $from .="     left join trn_set_stocks as i  ";
        $from .="       on i.trn_set_stock_header_id = h.id  ";
        $from .="     left join trn_stickers as j  ";
        $from .="       on j.id = i.trn_sticker_id  ";
        $from .="     left join mst_item_units as k  ";
        $from .="       on k.id = j.mst_item_unit_id  ";
        $from .="     left join mst_sales_configs as l  ";
        $from .="       on l.mst_facility_item_id = k.mst_facility_item_id  ";
        $from .="       and l.mst_item_unit_id = k.id  ";
        $from .="       and l.partner_facility_id = g.id";
        $from .="       and l.start_date <= '".$work_date."'";
        $from .="       and l.end_date > '" .$work_date."'";

        $where = '';
        $where .="   where ";
        $where .="     a.id in (" .join(',' , $ids ).")  ";
        $where .="     and ( e.partner_facility_id = g.id  or ( e.partner_facility_id  is null and l.partner_facility_id = g.id ))";
        $where .="     and a.shipping_type not in (" . Configure::read('ShippingType.fixed') . ",". Configure::read('ShippingType.deposit') .") ";

        $sql ='';
        $sql .=" select ";
        $sql .="       a.department_id_from ";
        $sql .="     , a.department_id_to ";
        $sql .="     , coalesce(k.id , a.mst_item_unit_id) as mst_item_unit_id ";
        $sql .="     , a.quantity ";
        $sql .="     , d.item_type ";
        $sql .="     , g.gross ";
        $sql .="     , g.round ";
        $sql .="     , coalesce(l.sales_price, e.sales_price) as sales_price ";
        $sql .="     , x.total_sales_price " ;
        $sql .="     , (  ";
        $sql .="       case  ";
        $sql .="         when g.gross = '1'  ";
        $sql .="         then (  ";
        $sql .="           case  ";
        $sql .="             when g.round = '1'  ";
        $sql .="             then round(coalesce(l.sales_price, e.sales_price))  ";
        $sql .="             when g.round = '2'  ";
        $sql .="             then trunc(coalesce(l.sales_price, e.sales_price) + .99)  ";
        $sql .="             when g.round = '3'  ";
        $sql .="             then trunc(coalesce(l.sales_price, e.sales_price))  ";
        $sql .="             when g.round = '4'  ";
        $sql .="             then coalesce(l.sales_price, e.sales_price)  ";
        $sql .="             else coalesce(l.sales_price, e.sales_price)  ";
        $sql .="             end ";
        $sql .="         )  ";
        $sql .="         else (  ";
        $sql .="           case  ";
        $sql .="             when g.round = '1'  ";
        $sql .="             then round(  ";
        $sql .="               a.quantity * coalesce(l.sales_price, e.sales_price) ";
        $sql .="             )  ";
        $sql .="             when g.round = '2'  ";
        $sql .="             then trunc(  ";
        $sql .="               a.quantity * coalesce(l.sales_price, e.sales_price) +.99 ";
        $sql .="             )  ";
        $sql .="             when g.round = '3'  ";
        $sql .="             then trunc(  ";
        $sql .="               a.quantity * coalesce(l.sales_price, e.sales_price) ";
        $sql .="             )  ";
        $sql .="             when g.round = '4'  ";
        $sql .="             then a.quantity * coalesce(l.sales_price, e.sales_price)  ";
        $sql .="             else a.quantity * coalesce(l.sales_price, e.sales_price)  ";
        $sql .="             end ";
        $sql .="         )  ";
        $sql .="         end ";
        $sql .="     )                                        as clime_price ";
        $sql .="     , a.id ";
        $sql .="     , b.facility_sticker_no ";
        $sql .="     , b.hospital_sticker_no  ";
        $sql .="     , b.id  as sticker_id";
        $sql .=$from;
        $sql .="     left join ( ";
        $sql .="     select sum(coalesce(l.sales_price, e.sales_price)) as total_sales_price , b.id as sticker_id ";
        $sql .=$from;
        $sql .=$where;
        $sql .="   group by  b.id ) as x";
        $sql .=" on x.sticker_id = b.id ";
        $sql .=$where;

        $result = $this->_controller->TrnShipping->query($sql);

        foreach($result as $r ){

            $claims_data = array(
                'TrnClaim' => array(
                    'department_id_from' => $r[0]['department_id_from'],//請求元参照キー
                    'department_id_to'   => $r[0]['department_id_to'],//請求先参照キー
                    'mst_item_unit_id'   => $r[0]['mst_item_unit_id'],//包装単位参照キー
                    'claim_date'         => $work_date,//請求年月
                    'count'              => $r[0]['quantity'],//請求数
                    'unit_price'         => $r[0]['sales_price'],//請求単価
                    'round'              => $r[0]['round'],//丸め区分
                    'gross'              => $r[0]['gross'],//まとめ区分
                    'is_not_retroactive' => FALSE,//遡及除外フラグ
                    'claim_price'        => $r[0]['clime_price'],//請求金額
                    'trn_receiving_id'   => "",//入荷参照キー
                    'trn_shipping_id'    => $r[0]['id'],//出荷参照キー
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'is_stock_or_sale'   => "2",//仕入売上判定フラグ 1:仕入;2:売上
                    'is_deleted'         => FALSE,
                    'stocking_close_type'=> 0,//仕入締め区分
                    'sales_close_type'   => 0,//売上締め区分
                    'facility_close_type'=> 0,//在庫締め区分
                    'trade_type'         => Configure::read('ClassesType.ExShipping'),
                    "facility_sticker_no"=> $r[0]['facility_sticker_no'],
                    "hospital_sticker_no"=> $r[0]['hospital_sticker_no'],
                    )
                );


            //TrnClaimrオブジェクトをcreate
            $this->_controller->TrnClaim->create();

            if (!$this->_controller->TrnClaim->save($claims_data)) {
                $this->rollbackTables();
                throw new Exception('売上データ作成でエラー'
                                    . print_r($claims_data,true)
                                    . print_r($this->validateErrors($this->_controller->TrnClaim), true)
                                    );
                exit;
                return false;

            } else {
                $last_id = $this->_controller->TrnClaim->getLastInsertID();//シールテーブル更新用　売上請求参照キー
            }
            $condtion = array( 'TrnSticker.id ='      => $r[0]['sticker_id'] );
            $updatefield = array( 'sales_price'       => "'".$r[0]['total_sales_price']."'",//売上価格
                                  'sale_claim_id'     => (($r[0]['item_type'] != Configure::read('Items.item_types.setitem') )?("'".$last_id."'"):"null")   ,//売上請求参照キー
                                  );


            //TrnStickerオブジェクトをcreate
            $this->_controller->TrnSticker->create();

            //SQL実行
            $this->_controller->TrnSticker->recursive = -1;
            if (!$this->_controller->TrnSticker->updateAll( $updatefield, $condtion )) {
                $this->rollbackTables();
                throw new Exception('売上データ作成に伴うシール更新エラー'
                                    . print_r($updatefield,true)
                                    . print_r($condtion,true)
                                    . print_r($this->validateErrors($this->_controller->TrnSticker), true)
                                    );
                exit;
                return false;
            }
        }
        return true;
    }
    //--------------------------------------------------------------------------------------
    /**
     * createHospitalReceiving
     *
     * 受領時に必要な処理その3：病院入荷ヘッダ・明細データの作成
     */
    function createHospitalReceiving($header_id,$id){
        //ヘッダIDから出荷ヘッダテーブルを検索し、必要な情報を取得--------------------
        $params = array (
            'fields'     => array(),
            'order'      => array('TrnShippingHeader.id'),
            'recursive'  => -1,
            'conditions' => array('is_deleted' => FALSE,
                                  'id' => $header_id,
                                  ),
            );

        //仕入れ価格・売上価格取得用にシール情報もアソシエーションを設定
        $this->_controller->TrnShipping->bindModel(array('belongsTo' => array('TrnSticker' => array('conditions' => 'TrnSticker.is_deleted = FALSE'))));
        //不要なアソシエーションを一時解除
        $this->_controller->TrnShippingHeader->unbindModel(array('belongsTo' => array('MstUser')));
        $ShippingHeader_Info = $this->_controller->TrnShippingHeader->find('all',$params);
        //------------------------------------------------------------------------

        foreach($ShippingHeader_Info as $Info){
            $work_seq = 1;
            $work_no = $this->setWorkNo4Header($this->work_date,'09');

            //明細数算出
            $detail_count = count($id[$Info['TrnShippingHeader']['id']]);
            //***************
            //入荷ヘッダの登録
            //***************
            $receiving_header_data = array(
                //ユーザ情報
                'TrnReceivingHeader' => array(
                    'work_no'                => $work_no,
                    'work_date'              => $this->work_date,
                    'recital'                => '',
                    'receiving_type'         => Configure::read('ReceivingType.receipt'),//入荷区分:受領による入荷
                    'receiving_status'       => Configure::read('ReceivingStatus.stock'),//入荷状態（受領済）
                    'department_id_from'     => $Info['TrnShippingHeader']['department_id_from'],
                    'department_id_to'       => $Info['TrnShippingHeader']['department_id_to'],
                    'detail_count'           => $detail_count,//明細数
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'trn_shipping_header_id' => $Info['TrnShippingHeader']['id'],//出荷ヘッダ参照キー
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    )
                );

            //TrnReceivingHeaderオブジェクトをcreate
            $this->_controller->TrnReceivingHeader->create();
            //SQL実行

            $this->_controller->TrnReceivingHeader->recursive = -1;
            if (!$this->_controller->TrnReceivingHeader->save($receiving_header_data , false )) {
                $this->rollbackTables();
                throw new Exception('入荷ヘッダ作成でエラー'
                                    . print_r($receiving_header_data,true)
                                    . print_r($this->_controller->validateErrors($this->_controller->TrnReceivingHeader), true)
                                    );
                exit;
                return false;
            }

            //ヘッダの登録に成功した場合、明細の更新処理へ
            $last_id = $this->_controller->TrnReceivingHeader->getLastInsertID();//入荷ヘッダID

            //明細IDから出荷明細テーブルを検索し、必要な情報を取得----------------
            $params = array (
                'fields'     => array(
                    'TrnShipping.id',
                    'TrnShipping.mst_item_unit_id',
                    'TrnShipping.department_id_from',
                    'TrnShipping.department_id_to',
                    'TrnShipping.quantity',
                    'TrnShipping.shipping_type',
                    'TrnShipping.trn_sticker_id',
                    'TrnSticker.transaction_price',
                    'TrnSticker.sales_price',
                    'TrnSticker.trn_order_id',
                    'TrnSticker.facility_sticker_no',
                    'TrnSticker.hospital_sticker_no',
                    ),
                'order'      => array('TrnShipping.id'),
                'conditions' => array('TrnShipping.is_deleted' => FALSE,
                                      'TrnShipping.id' => $id[$Info['TrnShippingHeader']['id']],
                                      ),
                'joins'       => array(
                    array( 'type'       => 'left'
                           ,'table'      => 'trn_stickers'
                           ,'alias'      => 'TrnSticker'
                           ,'conditions' => array('TrnSticker.id = TrnShipping.trn_sticker_id')
                           )
                    )

                );

            //仕入れ価格・売上価格取得用にシール情報もアソシエーションを設定
            $this->_controller->TrnShipping->recursive = -1;
            $Shipping_Info = $this->_controller->TrnShipping->find('all',$params);

            //------------------------------------------------------------------

            //現在処理中のヘッダ配下の明細ID回ループ
            foreach($id[$Info['TrnShippingHeader']['id']] as $data_id){
                foreach($Shipping_Info as $info2){
                    if($data_id == $info2['TrnShipping']['id']){

                        //*************
                        //入荷明細の登録
                        //*************
                        $receiving_data = array(
                            //ユーザ情報
                            'TrnReceiving' => array(
                                'work_no'                 => $work_no,
                                'work_seq'                => $work_seq,
                                'work_date'               => $this->work_date,
                                'work_type'               => '',
                                'recital'                 => '',
                                'receiving_type'          => Configure::read('ReceivingType.receipt'),//入荷区分:受領による入荷
                                'receiving_status'        => Configure::read('ShippingStatus.loaded'),//入荷状態（受領済み）
                                'mst_item_unit_id'        => $info2['TrnShipping']['mst_item_unit_id'],//包装単位参照キー
                                'department_id_from'      => $info2['TrnShipping']['department_id_from'],
                                'department_id_to'        => $info2['TrnShipping']['department_id_to'],
                                'quantity'                => $info2['TrnShipping']['quantity'],//数量
                                'stocking_price'          => $info2['TrnSticker']['transaction_price'],//仕入れ価格
                                'sales_price'             => $info2['TrnSticker']['sales_price'],//売上価格
                                'trn_sticker_id'          => $info2['TrnShipping']['trn_sticker_id'],//シール参照キー
                                'trn_shipping_id'         => $info2['TrnShipping']['id'],//出荷参照キー
                                'trn_order_id'            => $info2['TrnSticker']['trn_order_id'],//発注参照キー
                                'trn_receiving_header_id' => $last_id,//入荷ヘッダ外部キー
                                'creater'                 => $this->Session->read('Auth.MstUser.id'),
                                'modifier'                => $this->Session->read('Auth.MstUser.id'),
                                'facility_sticker_no'     => $info2['TrnSticker']['facility_sticker_no'],
                                'hospital_sticker_no'     => $info2['TrnSticker']['hospital_sticker_no']
                                )
                            );


                        //TrnReceivingオブジェクトをcreate
                        $this->_controller->TrnReceiving->create();

                        if (!$this->_controller->TrnReceiving->save($receiving_data)) {
                            $this->rollbackTables();
                            throw new Exception('入荷明細作成エラー'
                                                . print_r($receiving_data,true)
                                                . print_r($this->_controller->validateErrors($this->_controller->TrnReceiving), true)
                                                );
                            exit;
                            return false;

                        }
                        $work_seq++;

                        $last_id2 = $this->_controller->TrnReceiving->getLastInsertID();
                        $key_header[$Info['TrnShippingHeader']['id']] = $last_id;
                        $key_data[$Info['TrnShippingHeader']['id']][$info2['TrnShipping']['id']] = $last_id2;


                        if($this->Session->read('Auth.Config.ShippingConsumes') == '0'){
                            //----------------------------------------------------------
                            //売上データ(TrnClaim)へ入荷参照キーを更新しに行く
                            //----------------------------------------------------------
                            if($info2['TrnShipping']['shipping_type'] != Configure::read('ClassesType.Constant')){
                                $condtion = array( 'TrnClaim.trn_shipping_id ='=> $info2['TrnShipping']['id'] );
                                $updatefield = array( 'trn_receiving_id'       => "'".$last_id2."'",
                                                      'is_deleted'             => "'false'",
                                                      'modifier'               => "'".$this->Session->read('Auth.MstUser.id')."'",
                                                      'modified'               => "'".$this->now."'",
                                                      );
                                

                                //MstUserオブジェクトをcreate
                                $this->_controller->TrnClaim->create();
                                
                                //SQL実行
                                $this->_controller->TrnClaim->recursive = -1;
                                if (!$this->_controller->TrnClaim->updateAll( $updatefield, $condtion )) {
                                    $this->rollbackTables();
                                    throw new Exception('入荷明細作成に伴う請求データ更新エラー'
                                                        . print_r($updatefield,true)
                                                        . print_r($condtion,true)
                                                        . print_r($this->_controller->validateErrors($this->_controller->TrnClaim), true)
                                                        );
                                    exit;
                                    //エラー発生時の飛び先
                                    return false;
                                    
                                }
                            }
                        }
                        //----------------------------------------------------------
                        //シールテーブルの受領参照キー・持ち主を更新しに行く(全区分)
                        //----------------------------------------------------------
                        $condtion = array( 'TrnSticker.id ='=> $info2['TrnShipping']['trn_sticker_id'] );
                        $updatefield = array( 'receipt_id'        => "'".$last_id2."'",//受領参照キー（入荷明細キー）
                                              'mst_department_id' => "'".$info2['TrnShipping']['department_id_to']."'",//出荷明細.移動先部署
                                              );


                        //TrnStickerオブジェクトをcreate
                        $this->_controller->TrnSticker->create();

                        //SQL実行
                        $this->_controller->TrnSticker->recursive = -1;
                        if (!$this->_controller->TrnSticker->updateAll( $updatefield, $condtion )) {
                            $this->rollbackTables();
                            throw new Exception('入荷明細作成に伴うシール更新エラー'
                                                . print_r($updatefield,true)
                                                . print_r($condtion,true)
                                                . print_r($this->_controller->validateErrors($this->_controller->TrnSticker), true)
                                                );
                            exit;
                            return false;
                        }
                    }
                }
            }
        }
        return array($key_header,$key_data);
    }

    //--------------------------------------------------------------------------------------
    /**
     * createHospitalStorage
     *
     * 受領時に必要な処理その4：病院入庫ヘッダ・明細データの作成
     */
    function createHospitalStorage($header_id,$id,$h_key,$m_key){

        //ヘッダIDから出荷ヘッダテーブルを検索し、必要な情報を取得--------------------
        $params = array (
            'fields'     => array(),
            'order'      => array('TrnShippingHeader.id'),
            'recursive'  => 1,
            'conditions' => array('is_deleted' => FALSE,
                                  'id' => $header_id,
                                  ),
            );

        //不要なアソシエーションを一時解除
        $this->_controller->TrnShippingHeader->unbindModel(array('belongsTo' => array('MstUser')));
        //仕入れ価格・売上価格取得用にシール情報もアソシエーションを設定
        $this->_controller->TrnShipping->bindModel(array('belongsTo' => array('TrnSticker' => array('conditions' => 'TrnSticker.is_deleted = FALSE'))));

        $ShippingHeader_Info = $this->_controller->TrnShippingHeader->find('all',$params);
        //------------------------------------------------------------------------

        $err = 0;
        foreach($ShippingHeader_Info as $Info){
            $work_seq = 1;
            $work_no = $this->setWorkNo4Header($this->work_date,'10');

            //明細数算出
            $detail_count = count($id[$Info['TrnShippingHeader']['id']]);
            //***************
            //入庫ヘッダの登録
            //***************
            $storage_header_data = array(
                //ユーザ情報
                'TrnStorageHeader' => array(
                    'work_no'               => $work_no,
                    'work_date'             => $this->work_date,
                    'recital'               => '',
                    'storage_type'          => $Info['TrnShippingHeader']['shipping_type'],//入庫区分
                    'storage_status'        => Configure::read('StorageStatus.arrival'),//入庫状態←は入庫済（仮）
                    'mst_department_id'     => $Info['TrnShippingHeader']['department_id_to'],//部署参照キー

                    'detail_count'          => $detail_count,
                    'creater'               => $this->Session->read('Auth.MstUser.id'),
                    'trn_receiving_header_id'=> $h_key[$Info['TrnShippingHeader']['id']],//入荷ヘッダ参照キー
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    )
                );

            //TrnStorageHeaderオブジェクトをcreate
            $this->_controller->TrnStorageHeader->create();

            //SQL実行
            if (!$this->_controller->TrnStorageHeader->save($storage_header_data)) {
                $this->rollbackTables();
                throw new Exception('入庫ヘッダ作成エラー'
                                    . print_r($storage_header_data,true)
                                    . print_r($this->_controller->validateErrors($this->_controller->TrnStorageHeader), true)
                                    );
                exit();

                return false;
                $err++;
            }
            //ヘッダの登録に成功した場合、明細の更新処理へ
            $last_id = $this->_controller->TrnStorageHeader->getLastInsertID();


            //明細IDから出荷テーブルを検索し、必要な情報を取得---------------------
            $params = array (
                'fields'     => array('TrnShipping.id',
                                      'TrnShipping.shipping_type' ,
                                      'TrnShipping.work_type',
                                      'TrnShipping.mst_item_unit_id',
                                      'TrnShipping.department_id_to',
                                      'TrnShipping.quantity',
                                      'TrnSticker.trn_set_stock_id',
                                      'TrnShipping.trn_sticker_id',
                                      'TrnSticker.facility_sticker_no',
                                      'TrnSticker.hospital_sticker_no',
                                      'TrnSticker.sale_claim_id'
                                      ),
                'order'      => array('TrnShipping.id'),
                'conditions' => array('TrnShipping.is_deleted' => FALSE,
                                      'TrnShipping.id' => $id[$Info['TrnShippingHeader']['id']],
                                      ),
                'joins'       => array(
                    array( 'type'       => 'left'
                           ,'table'      => 'trn_stickers'
                           ,'alias'      => 'TrnSticker'
                           ,'conditions' => array('TrnSticker.id = TrnShipping.trn_sticker_id')
                           )
                    )
                );
            $this->_controller->TrnShipping->recursive = -1;
            $Shipping_Info = $this->_controller->TrnShipping->find('all',$params);

            //------------------------------------------------------------------

            //現在処理中のヘッダ配下の明細ID回ループ
            foreach($id[$Info['TrnShippingHeader']['id']] as $data_id){
                foreach($Shipping_Info as $info2){
                    if($data_id == $info2['TrnShipping']['id']){

                        //*************
                        //入庫明細の登録
                        //*************
                        $storage_data = array(
                            //ユーザ情報
                            'TrnStorage' => array(
                                'work_no'               => $work_no,
                                'work_seq'              => $work_seq,
                                'work_date'             =>$this->work_date,
                                'work_type'             => $info2['TrnShipping']['work_type'],
                                'recital'               => '',
                                'storage_type'          => $info2['TrnShipping']['shipping_type'],//入庫区分
                                'storage_status'        => Configure::read('ReceivingStatus.arrival'),//入庫状態
                                'mst_item_unit_id'      => $info2['TrnShipping']['mst_item_unit_id'],//包装単位参照キー
                                'mst_department_id'     => $info2['TrnShipping']['department_id_to'],//部署参照キー
                                'quantity'              => $info2['TrnShipping']['quantity'],//数量
                                'trn_sticker_id'        => $info2['TrnShipping']['trn_sticker_id'],//シール参照キー
                                'trn_receiving_id'      => $m_key[$Info['TrnShippingHeader']['id']][$info2['TrnShipping']['id']],//入荷参照キー
                                'trn_storage_header_id' => $last_id,//入庫ヘッダ外部キー
                                'creater'               => $this->Session->read('Auth.MstUser.id'),
                                'modifier'              => $this->Session->read('Auth.MstUser.id'),
                                'facility_sticker_no'   => $info2['TrnSticker']['facility_sticker_no'],
                                'hospital_sticker_no'   => $info2['TrnSticker']['hospital_sticker_no']
                                )
                            );

                        //TrnStorageオブジェクトをcreate
                        $this->_controller->TrnStorage->create();

                        if (!$this->_controller->TrnStorage->save($storage_data)) {
                            $this->rollbackTables();
                            throw new Exception('入庫明細作成エラー'
                                                . print_r($storage_data,true)
                                                . print_r($this->_controller->validateErrors($this->_controller->TrnStorage), true)
                                                );
                            exit();

                            return false;
                            $err++;
                        }
                        $work_seq++;
                        $last_id2 = $this->_controller->TrnStorage->getLastInsertID();

                        $key_header[$Info['TrnShippingHeader']['id']] = $last_id;
                        $key_data[$Info['TrnShippingHeader']['id']][$info2['TrnShipping']['id']] = $last_id2;
                    }
                }
            }
        }

        return array($key_header,$key_data);
    }

    /**
     * updateHospitalStock
     *
     * 受領時に必要な処理その5：(1)移動先病院在庫の在庫数を増やし、(臨時品以外の場合のみ)未入荷数を減らす
     *                        (2)移動元センター在庫の在庫を減らし、予約数を減らす
     */
    function updateHospitalStock($header_id,$id){

        $chk = array();

        foreach($id as $header){
            foreach($header as $meisai){

                //出荷明細データの取得
                $params = array (
                    'fields'     => array(),
                    'order'      => array('TrnShipping.id'),
                    'recursive'  => -1,
                    'conditions' => array('TrnShipping.is_deleted' => FALSE,
                                          'TrnShipping.id' => $meisai,
                                          ),
                    );

                $Shipping_Info = $this->_controller->TrnShipping->find('all',$params);
                //******************************************************************
                //(1)移動先病院在庫の在庫数を増やし、(臨時品以外の場合のみ)未入荷数を減らす
                //※在庫が無い場合はデータを作成する
                //******************************************************************

                //取得した出荷明細データから、在庫テーブルの更新対象レコードを探す
                $params = array (
                    'fields'     => array(),
                    'order'      => array('TrnStock.id'),
                    'recursive'  => -1,
                    'conditions' => array('TrnStock.is_deleted' => FALSE,
                                          'TrnStock.mst_item_unit_id'  => $Shipping_Info[0]['TrnShipping']['mst_item_unit_id'],   //包装単位
                                          'TrnStock.mst_department_id' => $Shipping_Info[0]['TrnShipping']['department_id_to'],  //部署参照キー
                                          ),
                    );

                $chk[$Shipping_Info[0]['TrnShipping']['department_id_to']][$Shipping_Info[0]['TrnShipping']['mst_item_unit_id']] = "1";

                $Stock = $this->_controller->TrnStock->find('all',$params);

                if(empty($Stock)){
                    //病院在庫レコードが無い場合はレコードを作成する
                    $data = array('mst_item_unit_id'  => $Shipping_Info[0]['TrnShipping']['mst_item_unit_id'],
                                  'mst_department_id' => $Shipping_Info[0]['TrnShipping']['department_id_to'],
                                  'stock_count'       => $Shipping_Info[0]['TrnShipping']['quantity'],
                                  'reserve_count'     => '0',
                                  'promise_count'     => '0',
                                  'requested_count'   => '0',
                                  'receipted_count'   => '0',
                                  'is_deleted'        => false,
                                  'creater'           => $this->Session->read('Auth.MstUser.id'),
                                  'modifier'          => $this->Session->read('Auth.MstUser.id')
                                  );


                    //TrnStockオブジェクトをcreate
                    $this->_controller->TrnStock->create();

                    //SQL実行
                    if (false === $this->_controller->TrnStock->save($data)) {
                        $this->rollbackTables();
                        throw new Exception('病院在庫レコード作成エラー'
                                            . print_r($data,true)
                                            . print_r($this->_controller->validateErrors($this->_controller->TrnStock), true)
                                            );
                        exit();
                        return false;
                    }
                    $this->Session->write('Receipt.createstock_flg','1');
                } else {
                    //病院在庫レコードが有る場合は更新
                    //******************************************************************

                    $condtion = array( 'TrnStock.id' => $Stock[0]['TrnStock']['id']);
                    $updatefield = array( //在庫数増やす
                                          'stock_count'     => "'".($Stock[0]['TrnStock']['stock_count']+$Shipping_Info[0]['TrnShipping']['quantity'])."'",
                                          'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                          'modified'        => "'".$this->now."'",
                                          );


                    //TrnStockオブジェクトをcreate
                    $this->_controller->TrnStock->create();

                    //SQL実行
                    if (!$this->_controller->TrnStock->updateAll( $updatefield, $condtion )) {
                        $this->rollbackTables();
                        throw new Exception('病院在庫レコード更新エラー'
                                            . print_r($updatefield,true)
                                            . print_r($condtion,true)
                                            . print_r($this->_controller->validateErrors($this->_controller->TrnStock), true)
                                            );
                        exit();
                        return false;
                    }
                }
                //********************************************************************
                //(2)移動元在庫の在庫を減らし、予約数を減らす
                //******************************************************************

                //取得した出荷明細IDから、在庫テーブルの更新対象レコードを探す
                $params2 = array (
                    'fields'     => array(),
                    'order'      => array('TrnStock.id'),
                    'recursive'  => -1,
                    'conditions' => array('TrnStock.is_deleted' => FALSE,
                                          'TrnStock.mst_item_unit_id'  => $Shipping_Info[0]['TrnShipping']['mst_item_unit_id'],   //包装単位mst_item_unit_id
                                          'TrnStock.mst_department_id' => $Shipping_Info[0]['TrnShipping']['department_id_from'],  //部署参照キー
                                          ),
                    );

                $Stock2 = $this->_controller->TrnStock->find('all',$params2);
                //******************************************************************
                $condtion2 = array( 'TrnStock.id' => $Stock2[0]['TrnStock']['id'],
                                    );
                $updatefield2 = array( //在庫数減らす
                                       'stock_count'     => "'".($Stock2[0]['TrnStock']['stock_count']-$Shipping_Info[0]['TrnShipping']['quantity'])."'",
                                       //予約数減らす
                                       'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                       'modified'        => "'".$this->now."'",
                                       );

                //TrnStockオブジェクトをcreate
                $this->_controller->TrnStock->create();

                //SQL実行
                if (!$this->_controller->TrnStock->updateAll( $updatefield2, $condtion2 )) {
                    $this->rollbackTables();
                    throw new Exception('センター在庫レコード更新エラー'
                                        . print_r($updatefield2,true)
                                        . print_r($condtion2,true)
                                        . print_r($this->_controller->validateErrors($this->_controller->TrnStock), true)
                                        );
                    exit();
                    return false;
                }
            }
        }

        return true;
    }

    //--------------------------------------------------------------------------
    /**
     * updateShipping
     *
     * 受領時に必要な処理その6：出荷データの出荷状態を変更（ヘッダと明細両方）
     */
    function updateShipping($header_id,$id){

        foreach($header_id as $header){

            $All_shipping = array();
            $cnt=0;

            //更新対象出荷ヘッダに紐付く明細の取得(未受領or一部受領のみ)
            $params = array (
                'fields'     => array(),
                'order'      => array('TrnShipping.id'),
                'recursive'  => 1,
                'conditions' => array('TrnShipping.is_deleted' => FALSE,
                                      'TrnShipping.trn_shipping_header_id' => $header,
                                      'TrnShipping.shipping_status < ' => Configure::read('ShippingStatus.loaded')),//ステータスが完了以下
                );
            $Shipping_Info = $this->_controller->TrnShipping->find('all',$params);

            //出荷明細IDだけを格納
            foreach($Shipping_Info as $Info){
                $All_shipping[] = $Info['TrnShipping']['id'];
            }

            //チェック&ステータス格納用の値を設定
            $status = Configure::read('ShippingStatus.loaded');
            foreach($All_shipping as $meisai_id){
                if (in_array($meisai_id, $id[$header])) {
                } else {
                    $status = Configure::read('ShippingStatus.partLoaded');//一部受領
                    break;
                }
            }

            //出荷ヘッダ更新-----------------------------------------------------
            //更新時間チェック
            if(!$this->check_ShippingHeaderModified($header,$this->Session->read('ExShipping.receibt_read_time'))){
                throw new Exception('出荷ヘッダ更新日時チェック' . "header: {$header} time: {$this->Session->read('ExShipping.receibt_read_time')}");
                $this->Session->setFlash('処理中のデータに更新がかけられました。最初からやり直してください。', 'growl', array('type'=>'error') );
                return false;
            }

            $condtion = array( 'TrnShippingHeader.id' => $header,);
            $updatefield = array( 'shipping_status' => "'".$status."'",
                                  'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                  'modified'        => "'".$this->now."'",
                                  );

            //TrnShippingHeaderオブジェクトをcreate
            $this->_controller->TrnShippingHeader->create();

            //SQL実行
            if (!$this->_controller->TrnShippingHeader->updateAll( $updatefield, $condtion )) {
                $this->rollbackTables();
                throw new Exception('出荷ヘッダ更新エラー'
                                    . print_r($updatefield,true)
                                    . print_r($condtion,true)
                                    . print_r($this->_controller->validateErrors($this->_controller->TrnShippingHeader), true)
                                    );
                exit();
                return false;
            }

            //出荷明細の出荷状態の更新
            foreach($id[$header] as $meisaiID){

                //出荷明細更新---------------------------------------------------
                //更新時間チェック
                if(!$this->check_ShippingModified($meisaiID,$this->Session->read('ExShipping.receibt_read_time'))){
                    throw new Exception('出荷明細更新日時チェック' . "header: {$header} time: {$this->Session->read('ExShipping.receibt_read_time')}");
                    $this->Session->setFlash('処理中のデータに更新がかけられました。最初からやり直してください。', 'growl', array('type'=>'error') );
                    return false;
                }

                $condtion2 = array( 'TrnShipping.id' => $meisaiID,);
                    $updatefield2 = array('TrnShipping.shipping_status' => "'".Configure::read('ShippingStatus.loaded')."'",
                                          'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                          'modified'        => "'".$this->now."'",
                                          );

                //TrnShippingオブジェクトをcreate
                $this->_controller->TrnShipping->create();

                //SQL実行
                if (!$this->_controller->TrnShipping->updateAll( $updatefield2, $condtion2 )) {
                    $this->rollbackTables();
                    throw new Exception('出荷明細更新エラー'
                                        . print_r($updatefield2,true)
                                        . print_r($condtion2,true)
                                        . print_r($this->_controller->validateErrors($this->_controller->TrnShipping), true)
                                        );
                        exit();
                    return false;
                }
            }

        }

        return true;

    }



    /**
     * createConsume
     *
     * 受領時に必要な処理その7：消費データの作成（臨時品の場合）＋病院在庫の在庫数を減らす
     */
    function createConsume($header_id,$id,$h_key,$m_key){

        //ヘッダIDから出荷ヘッダテーブルを検索し、必要な情報を取得--------------------
        $params = array (
            'fields'     => array(),
            'order'      => array('TrnShippingHeader.id'),
            'recursive'  => 1,
            'conditions' => array('is_deleted' => FALSE,
                                  'id' => $header_id,
                                  ),
            );

        //不要なアソシエーションを一時解除
        $this->_controller->TrnShippingHeader->unbindModel(array('belongsTo' => array('MstUser')));
        //仕入れ価格・売上価格取得用にシール情報もアソシエーションを設定
        $this->_controller->TrnShipping->bindModel(array('belongsTo' => array('TrnSticker' => array('conditions' => 'TrnSticker.is_deleted = FALSE'))));

        //出荷テーブルから出荷明細テーブルへのアソシエーションを一時設定
        $this->_controller->TrnShippingHeader->bindModel(array('hasMany' => array('TrnShipping' => array('foreignKey' => 'trn_shipping_header_id',
                                                                                            'conditions' => array('TrnShipping.is_deleted = FALSE',
                                                                                                                  '(TrnShipping.shipping_type ='. Configure::read('ClassesType.Temporary')
                                                                                                                  . ' OR TrnShipping.shipping_type ='. Configure::read('ShippingType.exshipping').')'
                                                                                                                  )))));//臨時品のみ

        $ShippingHeader_Info = $this->_controller->TrnShippingHeader->find('all',$params);
        //------------------------------------------------------------------------

        $err = 0;
        foreach($ShippingHeader_Info as $Info){
            $cnt=0;$tmp = array();
            foreach($Info['TrnShipping'] as $tmp){
                if(in_array($tmp['id'],$id[$Info['TrnShippingHeader']['id']])){
                    $cnt++;
                }
            }
            //区分取得
            $key = array_keys(Configure::read('workNoModels'),"TrnConsumeHeader");
            if($cnt!=0){
                $work_seq = 1;
                $work_no = $this->setWorkNo4Header($this->work_date,$key[0]);
                //明細数算出
                $detail_count = count($Info['TrnShipping']);
                //明細に臨時品がある場合のみ消費ヘッダ・明細を作成する
                if(count($Info['TrnShipping']) > 0){
                    //***************
                    //消費ヘッダの登録
                    //***************
                    $storage_header_data = array(
                        //ユーザ情報
                        'TrnConsumeHeader' => array(
                            'work_no'               => $work_no,
                            'work_date'             => $this->work_date,
                            'recital'               => $Info['TrnShippingHeader']['recital'],
                            'use_type'              => Configure::read('UseType.temporary'),//消費区分：臨時
                            'mst_department_id'     => $Info['TrnShippingHeader']['department_id_to'],//部署参照キー
                            'detail_count'          => $detail_count,
                            'creater'               => $this->Session->read('Auth.MstUser.id'),
                            'modifier'              => $this->Session->read('Auth.MstUser.id'),
                            'is_deleted'            => false
                            )
                        );

                    //TrnConsumeHeaderオブジェクトをcreate
                    $this->_controller->TrnConsumeHeader->create();
                    //SQL実行
                    if (!$this->_controller->TrnConsumeHeader->save($storage_header_data)) {
                        $this->rollbackTables();
                        throw new Exception('消費ヘッダ作成エラー'
                                            . print_r($storage_header_data,true)
                                            . print_r($this->_controller->validateErrors($this->_controller->TrnConsumeHeader), true)
                                            );
                        exit();
                        return false;
                    }
                    //明細の更新処理へ
                    $last_id = $this->_controller->TrnConsumeHeader->getLastInsertID();
                    //明細IDから出荷テーブルを検索し、必要な情報を取得---------------------
                    $params = array (
                        'fields'     => array('TrnShipping.id',
                                              'TrnShipping.shipping_type' ,
                                              'TrnShipping.work_type',
                                              'TrnShipping.mst_item_unit_id',
                                              'TrnShipping.department_id_to',
                                              'TrnShipping.quantity',
                                              'TrnSticker.trn_set_stock_id',
                                              'TrnShipping.trn_sticker_id',
                                              'TrnSticker.facility_sticker_no',
                                              'TrnSticker.hospital_sticker_no',
                                              'TrnSticker.sale_claim_id'

                                              ),
                        'order'      => array('TrnShipping.id'),
                        'conditions' => array('TrnShipping.is_deleted' => FALSE,
                                              'TrnShipping.id' => $id[$Info['TrnShippingHeader']['id']],
                                              ),
                        'joins'       => array(
                                         array( 'type'       => 'left'
                                               ,'table'      => 'trn_stickers'
                                               ,'alias'      => 'TrnSticker'
                                               ,'conditions' => array('TrnSticker.id = TrnShipping.trn_sticker_id')
                                               )
                            )
                        );
                    $this->_controller->TrnShipping->recursive = -1;
                    $Shipping_Info = $this->_controller->TrnShipping->find('all',$params);

                    //------------------------------------------------------------------


                    //現在処理中のヘッダ配下の明細ID回ループ（臨時品の場合のみ）
                    foreach($id[$Info['TrnShippingHeader']['id']] as $data_id){
                        foreach($Shipping_Info as $info2){
                            if(($data_id == $info2['TrnShipping']['id']) and ((int)$info2['TrnShipping']['shipping_type'] == (int)Configure::read('ClassesType.Temporary') || (int)$info2['TrnShipping']['shipping_type'] == (int)Configure::read('ShippingType.exshipping')) ){
                                //*************
                                //消費明細の登録
                                //*************
                                $storage_data = array(
                                    //ユーザ情報
                                    'TrnConsume' => array(
                                        'work_no'               => $work_no,
                                        'work_seq'              => $work_seq,
                                        'work_date'             => $this->work_date,
                                        'work_type'             => $info2['TrnShipping']['work_type'],
                                        'recital'               => '',
                                        'use_type'              => Configure::read('UseType.temporary'),//消費区分：臨時
                                        'mst_item_unit_id'      => $info2['TrnShipping']['mst_item_unit_id'],//包装単位参照キー
                                        'mst_department_id'     => $info2['TrnShipping']['department_id_to'],//部署参照キー
                                        'quantity'              => $info2['TrnShipping']['quantity'],//数量
                                        'trade_type'            => (empty($info2['TrnSticker']['trn_set_stock_id']))?Configure::read('ClassesType.ExShipping'):Configure::read('ClassesType.SetItem'),
                                        'trn_sticker_id'        => $info2['TrnShipping']['trn_sticker_id'],//シール参照キー
                                        'trn_storage_id'        => $m_key[$Info['TrnShippingHeader']['id']][$info2['TrnShipping']['id']],//入庫参照キー
                                        'trn_consume_header_id' => $last_id,//消費ヘッダ外部キー
                                        'is_retroactable'       => 0,
                                        'creater'               => $this->Session->read('Auth.MstUser.id'),
                                        'modifier'              => $this->Session->read('Auth.MstUser.id'),
                                        'is_deleted'            => false,
                                        'facility_sticker_no'   => $info2['TrnSticker']['facility_sticker_no'],
                                        'hospital_sticker_no'   => $info2['TrnSticker']['hospital_sticker_no']
                                        )
                                    );

                                //TrnConsumeオブジェクトをcreate
                                $this->_controller->TrnConsume->create();

                                if (!$this->_controller->TrnConsume->save($storage_data)) {
                                    $this->rollbackTables();
                                    throw new Exception('消費明細作成エラー'
                                                        . print_r($storage_data,true)
                                                        . print_r($this->_controller->validateErrors($this->_controller->TrnConsume), true)
                                                        );
                                    exit();
                                    return false;
                                }

                                $work_seq++;
                                $last_id2 = $this->_controller->TrnConsume->getLastInsertID();

                                //----------------------------------------------------------
                                //シールテーブルの消費参照キー・数量を更新しに行く
                                //----------------------------------------------------------
                                $condtion = array( 'TrnSticker.id ='=> $info2['TrnShipping']['trn_sticker_id'] );
                                $updatefield = array( 'trn_consume_id' => "'".$last_id2."'",//消費参照キー
                                                      'quantity'       => "'0'",//数量
                                                      );


                                //TrnStickerオブジェクトをcreate
                                $this->_controller->TrnSticker->create();

                                //SQL実行
                                if (!$this->_controller->TrnSticker->updateAll( $updatefield, $condtion )) {
                                    $this->rollbackTables();
                                    throw new Exception('消費に伴うシール更新エラー'
                                                        . print_r($updatefield,true)
                                                        . print_r($condtion,true)
                                                        . print_r($this->_controller->validateErrors($this->_controller->TrnSticker), true)
                                                        );
                                    exit();
                                    return false;

                                }

                                //******************************************************************
                                //病院在庫を減らす
                                //******************************************************************

                                //取得した出荷明細データから、在庫テーブルの更新対象レコードを探す
                                $params = array (
                                    'fields'     => array(),
                                    'order'      => array('TrnStock.id'),
                                    'recursive'  => 1,
                                    'conditions' => array('TrnStock.is_deleted' => FALSE,
                                                          'TrnStock.mst_item_unit_id'  => $info2['TrnShipping']['mst_item_unit_id'],   //包装単位
                                                          'TrnStock.mst_department_id' => $info2['TrnShipping']['department_id_to'],  //部署参照キー
                                                          ),
                                    );

                                $Stock = $this->_controller->TrnStock->find('all',$params);
                                //******************************************************************
                                $condtion = array( 'TrnStock.id' => $Stock[0]['TrnStock']['id'],);
                                $updatefield = array( //在庫数減らす
                                                      'stock_count'     => "'".($Stock[0]['TrnStock']['stock_count']-$info2['TrnShipping']['quantity'])."'",
                                                      'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                                      'modified'        => "'".$this->now."'",
                                                      );

                                //MstUserオブジェクトをcreate
                                $this->_controller->TrnStock->create();

                                //SQL実行
                                if (!$this->_controller->TrnStock->updateAll( $updatefield, $condtion )) {
                                    $this->rollbackTables();
                                    throw new Exception('消費に伴う在庫更新エラー'
                                                        . print_r($updatefield,true)
                                                        . print_r($condtion,true)
                                                        . print_r($this->_controller->validateErrors($this->_controller->TrnStock), true)
                                                        );
                                    exit();
                                    return false;
                                }

                                //シールに請求がない場合処理をスキップ（セット品の場合を想定）
                                if($info2['TrnSticker']['sale_claim_id']!=""){
                                    //請求テーブルの消費参照キーを更新しに行く
                                    $condtion = array( 'TrnClaim.id ='=> $info2['TrnSticker']['sale_claim_id'] );
                                    $updatefield = array( 'trn_consume_id' => "'".$last_id2."'",//消費参照キー
                                                          );


                                    //TrnClaimオブジェクトをcreate
                                    $this->_controller->TrnClaim->create();

                                    //SQL実行
                                    $this->_controller->TrnClaim->recursive = -1;
                                    if (!$this->_controller->TrnClaim->updateAll( $updatefield, $condtion )) {
                                        $this->rollbackTables();
                                        throw new Exception('消費に伴う請求データ更新エラー'
                                                            . print_r($updatefield,true)
                                                            . print_r($condtion,true)
                                                            . print_r($this->_controller->validateErrors($this->_controller->TrnClaim), true)
                                                            );
                                        exit();
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        return true;
    }

    /**
     * createStickerRecords
     *
     * 受領時に必要な処理その8：シールの移動履歴を作成（全区分）
     */
    function createStickerRecords ($header_id,$id,$h_key,$m_key) {
        $move_seq = 1;
        foreach($header_id as $header){
            foreach($id[$header] as $meisai){

                //出荷明細IDからシールIDを取得
                $params = array (
                    'fields'    => array('TrnSticker.id',
                                         'TrnSticker.receipt_id',
                                         'TrnSticker.trn_shipping_id',
                                         'TrnSticker.facility_sticker_no',
                                         'TrnSticker.hospital_sticker_no'),
                    'conditions' => array('TrnSticker.is_deleted' => FALSE,
                                          'TrnSticker.trn_shipping_id' => $meisai),
                    'order'     => array('TrnSticker.id'),
                    'recursive' => -1
                    );
                $Sticker_Info = $this->_controller->TrnSticker->find('all',$params);
                $s_id = $Sticker_Info[0]['TrnSticker']['id'];

                //出荷明細IDからシールIDを取得
                $params = array (
                    'fields'    => array('TrnShipping.id','TrnShipping.department_id_to'),
                    'conditions' => array('TrnShipping.is_deleted' => FALSE,'TrnShipping.id' => $meisai),
                    'order'     => array('TrnShipping.id'),
                    'recursive' => -1
                    );
                $department_Info = $this->_controller->TrnShipping->find('first',$params);
                $department_id = $department_Info['TrnShipping']['department_id_to'];
                $params = array (
                    'fields'    => array('MstDepartment.id','MstDepartment.mst_facility_id'),
                    'conditions' => array('MstDepartment.is_deleted' => FALSE,'MstDepartment.id' => $department_id),
                    'order'     => array('MstDepartment.id'),
                    'recursive' => -1
                    );
                $facility_Info = $this->_controller->MstDepartment->find('first',$params);
                $facility_id = $facility_Info['MstDepartment']['mst_facility_id'];

                //レコード区分取得

                // stickersの情報を取得
                if(!$this->_controller->isNNs($s_id)){
                    $_sticker = array();
                    $this->_controller->TrnSticker->recursive = -1;
                    $_sticker = $this->_controller->TrnSticker->find('first' , array('conditions' => array('id' => $s_id )));

                }
                $sticker_record = array(

                    'TrnStickerRecord' => array(
                        'move_date'           => $this->work_date,//移動日時
                        'move_seq'            => $move_seq,//移動連番
                        'record_type'         => Configure::read('RecordType.receipts'),//レコード区分「受領」
                        'facility_sticker_no' => (!$this->_controller->isNNs($_sticker['TrnSticker']['facility_sticker_no'])) ? $_sticker['TrnSticker']['facility_sticker_no'] : $_sticker['TrnSticker']['facility_sticker_no'] = ' ',
                        'hospital_sticker_no' => (!$this->_controller->isNNs($_sticker['TrnSticker']['hospital_sticker_no'])) ? $_sticker['TrnSticker']['hospital_sticker_no'] : $_sticker['TrnSticker']['hospital_sticker_no'] = ' ',
                        'record_id'           => $m_key[$header][$meisai],//レコードキー(入荷明細)
                        'trn_sticker_id'      => $s_id,//シール参照キー
                        'mst_facility_id'     => $facility_id,
                        'mst_department_id'   => $department_id,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'is_deleted'          => FALSE,
                        )
                    );


                $this->_controller->TrnStickerRecord->create();
                if (!$this->_controller->TrnStickerRecord->save($sticker_record)) {
                    return false;
                }
                $move_seq++;
            }
        }
        return true;
    }

    /***************************************************
     * 受領取消処理
     ***************************************************/

    /**
     * ReceiptsHistory
     *
     * 受領取消処理
     */
    function ReceiptsHistory($receivings_header_id,$receivings_id){
        $this->Session->write('ShippingHistory.receipt_read_time',date('Y-m-d H:i:s'));

        //受領取消に必要な処理1：引当明細の受領数・未受領数を元に戻す
        if(!$this->UpdatePromise_receipt_count_history($receivings_header_id,$receivings_id)){
            return false;
        }

        //受領取消に必要な処理2：売上がある場合は売上データの削除（シールの売上請求参照キー削除も含む）
        if(!$this->deleteClaims($receivings_header_id,$receivings_id)){
            return false;
        }

        //受領取消に必要な処理10：臨時品の場合は消費データの削除
        if(!$this->deleteConsume($receivings_header_id,$receivings_id)){
            return false;
        }

        //受領取消に必要な処理4：入庫ヘッダ・入庫明細の削除
        if(!$this->deleteStorages($receivings_header_id,$receivings_id)){
            return false;
        }

        //受領取消に必要な処理5：在庫を戻す
        //(1)移動先病院在庫の在庫数を減らし、未入荷数を増やす
        //(2)移動元病院在庫の在庫数を増やす、予約数を増やす
        if(!$this->updateStock($receivings_header_id,$receivings_id)){
            return false;
        }

        //受領取消に必要な処理6：出荷ステータスの変更
        if(!$this->updateShippingHistory($receivings_header_id,$receivings_id)){
            return false;
        }

        //受領取消に必要な処理7：シールの移動履歴の削除
        if(!$this->deleteStickerRecords($receivings_header_id,$receivings_id)){
            return false;
        }


        //受領取消に必要な処理9：入荷データの削除+シールの持ち主を元に戻す
        if(!$this->deleteReceivings($receivings_header_id,$receivings_id)){
            return false;
        }

        //受領取消に必要な処理3：シールの更新（消費参照キー削除・受領参照キーの削除）
        if(!$this->deleteStickerKey($receivings_header_id,$receivings_id)){
            return false;
        }

        //全ての処理が正常に終了
        return true;
    }

    //****************************************************************************
    /**
     * UpdatePromise_receipt_count
     *
     * 受領取消に必要な処理その1：引当明細の受領数を更新（預託品以外の場合）
     */
    function UpdatePromise_receipt_count_history($header_id,$id){

        //入荷明細ごとに更新対象の引当明細IDを取得する（シール経由）
        $promise_id = array();
        foreach($header_id as $header_id_data){
            foreach($id[$header_id_data] as $meisai_id){
                $params = array (
                    'fields'    => array('TrnSticker.id','TrnSticker.trn_promise_id'),
                    'order'     => array('TrnSticker.id'),
                    'conditions'=> array('TrnSticker.receipt_id' => $meisai_id),
                    'recursive' => -1
                    );
                $sticker_id = $this->_controller->TrnSticker->find('all',$params);
                if(!empty($sticker_id)){
                    $promise_id[$meisai_id] = $sticker_id[0]['TrnSticker']['trn_promise_id'];
                } else {
                    $promise_id[$meisai_id] = "";
                }
            }
        }


        //入荷明細から数量を取得(数量=引当明細.受領数から減らす数量)
        foreach($header_id as $header){
            foreach($id[$header] as $meisai){
                //引当明細ID取得
                $params = array (
                    'fields'    => array('TrnReceiving.id','TrnReceiving.quantity'),
                    'order'     => array('TrnReceiving.id'),
                    'conditions'=> array('TrnReceiving.id' => $meisai),
                    'recursive' => -1
                    );
                $data = $this->_controller->TrnReceiving->find('all',$params);
                $promise_quantity[$meisai] = $data[0]['TrnReceiving']['quantity'];
                unset($params,$data);
            }
        }


        //引当明細(TrnPromise)の受領数(receipt_count)を更新（減らす）
        //未受領数(unreceipt_count)を増やす
        foreach($id as $data){
            foreach($data as $meisai_id){
                //出荷明細IDから出荷情報（出荷区分）を取得
                $shipping_type = $this->_controller->TrnShipping->find('first',array('fields'=>'shipping_type','recursive' => -1,'conditions'=>array('id'=>$meisai)));
                //預託品の場合は以降の処理を行わない
                if($shipping_type['TrnShipping']['shipping_type'] != Configure::read('ClassesType.Deposit')){

                    //シール.引当参照キーに値が入っていない場合は処理を行わない
                    if($promise_id[$meisai_id] != ""){

                        //現在の受領数・未受領数を取得し、出荷明細の数量を加算する
                        $params = array (
                            'fields'    => array('TrnPromise.id','TrnPromise.receipt_count','TrnPromise.unreceipt_count'),
                            'order'     => array('TrnPromise.id'),
                            'conditions'=> array('TrnPromise.id' => $promise_id[$meisai_id]),
                            'recursive' => -1
                            );
                        $promise_info = $this->_controller->TrnPromise->find('all',$params);
                        unset($params);


                        //更新時間チェック
                        if(!$this->check_PromiseModified($promise_id[$meisai_id],$this->Session->read('ShippingHistory.receipt_read_time'))){
                            $this->rollbackTables();
                            throw new Exception('引当明細更新(更新時間チェック)エラー：'."id=".$promise_id[$meisai_id]."sessionReadTime = ".$this->Session->read('ShippingHistory.receipt_read_time'));
                            exit;
                        }

                        $receipt_count   = $promise_info[0]['TrnPromise']['receipt_count'] - $promise_quantity[$meisai_id];
                        $unreceipt_count = $promise_info[0]['TrnPromise']['unreceipt_count'] + $promise_quantity[$meisai_id];
                        $condtion = array( 'TrnPromise.id ='    => $promise_id[$meisai_id] );
                        $updatefield = array( 'receipt_count'   => "'".$receipt_count."'",
                                              'unreceipt_count' => "'".$unreceipt_count."'",
                                              'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                              'modified'        => "'".$this->now."'",
                                              );


                        //TrnPromiseオブジェクトをcreate
                        $this->_controller->TrnPromise->create();

                        //SQL実行
                        if (!$this->_controller->TrnPromise->updateAll( $updatefield, $condtion )) {
                            $this->rollbackTables();
                            throw new Exception('引当明細更新エラー'
                                                . print_r($updatefield,true)
                                                . print_r($condtion,true)
                                                . print_r($this->_controller->validateErrors($this->_controller->TrnPromise), true)
                                                );
                            exit;
                            return false;
                        }
                    }
                }
            }

        }
        return true;
    }
    //****************************************************************************
    /**
     * deleteClaims
     *
     * 受領時に必要な処理その2：売上がある場合は削除する(準定数品・臨時品)
     */
    function deleteClaims($header_id,$id) {

        //シール経由で削除対象の売上IDを取得(receipt_id = 受領時の入荷キー)

        $ids = array();

        foreach($id as $a){
            foreach($a as $b){
                $ids[] = $b;
            }
        }


        $sql = '';
        $sql .=' select ';
        $sql .='       b.id               as claim_id ';
        $sql .='     , a.id               as sticker_id  ';
        $sql .='   from ';
        $sql .='     trn_stickers as a  ';
        $sql .='     left join trn_claims as b  ';
        $sql .='       on b.trn_receiving_id = a.receipt_id  ';
        $sql .='       and b.is_deleted = false  ';
        $sql .='   where ';
        $sql .='     a.receipt_id in ('.join(',',$ids).') ';

        $ret = $this->_controller->TrnSticker->query($sql);


        //請求データの削除
        if(count($ret)>0){
            foreach($ret as $r){
                if(!empty($r[0]['claim_id'])){

                    //TrnClaimオブジェクトを作成
                    $this->_controller->TrnClaim->create();

                    $updatefield = array('TrnClaim.is_deleted' => "'".TRUE."'");
                    $condition = array('TrnClaim.id' => $r[0]['claim_id']);

                    //SQL実行
                    if (!$this->_controller->TrnClaim->updateAll($updatefield,$condition)) {
                        $this->rollbackTables();
                        throw new Exception('請求データ削除エラー'
                                            . print_r($updatefield,true)
                                            . print_r($condtion,true)
                                            . print_r($this->validateErrors($this->_controller->TrnClaim), true)
                                            );
                        exit;
                    }
                }
            }
        }
        //------------------------------------------------------------------------
        //シールテーブルの売上IDを削除
        if(count($ret) > 0){
            foreach($ret as $r ){

                $condtion = array( 'TrnSticker.id ='=> $r[0]['sticker_id'] );
                $updatefield = array( 'TrnSticker.sale_claim_id' => NULL,//売上請求参照キー
                                      );
                $this->_controller->TrnSticker->recursive = -1;
                if (!$this->_controller->TrnSticker->updateAll($updatefield,$condtion)) {
                    $this->rollbackTables();
                    throw new Exception('請求データ削除に伴うシール更新エラー'
                                        . print_r($updatefield,true)
                                        . print_r($condtion,true)
                                        . print_r($this->validateErrors($this->_controller->TrnSticker), true)
                                        );
                    exit;
                }
            }
        }
        return true;
    }
    //****************************************************************************
    /**
     * deleteConsume
     *
     * 受領時に必要な処理その10：臨時品の場合は消費データを削除する
     */
    function deleteConsume($header_id,$id){

        $consume_header = array();
        //シール経由で削除対象の消費IDを取得(receipt_id = 受領時の入荷キー)※臨時品の場合のみ
        //消費前数量は入荷データから取得（消費済みの為シールの数量は「0」なので）

        $sale_claim_id = array();
        foreach($header_id as $header_id_data){
            foreach($id[$header_id_data] as $meisai_id){
                $params = array (
                    'fields'    => array('TrnSticker.id',
                                         'TrnSticker.trn_consume_id',
                                         'TrnSticker.trade_type',
                                         'TrnSticker.mst_department_id',
                                         'TrnSticker.mst_item_unit_id'),
                    'order'     => array('TrnSticker.id'),
                    'conditions'=> array('TrnSticker.receipt_id' => $meisai_id,
                                         '(TrnSticker.trade_type =' . Configure::read('ShippingType.extraordinary')
                                         .' OR TrnSticker.trade_type =' . Configure::read('ShippingType.exshipping') . ')'),
                    'recursive' => -1
                    );
                $sticker_id = $this->_controller->TrnSticker->find('all',$params);
                //--------------------------------------------------------------------
                if(!empty($sticker_id)){
                    $receiving_quantity = $this->_controller->TrnReceiving->find('first', array('fields'=>'quantity','conditions' => array('TrnReceiving.id' => $meisai_id),'recursive'  => -1));
                    if($sticker_id[0]['TrnSticker']['trn_consume_id'] == ""){
                        $trn_consume_id[$meisai_id] = "0";
                    } else {
                        $trn_consume_id[$meisai_id] = $sticker_id[0]['TrnSticker']['trn_consume_id'];

                        //在庫更新に必要なデータ（部署・包装単位・以前の数量を消費IDごとに格納）
                        $stock_data[$sticker_id[0]['TrnSticker']['trn_consume_id']]['before_quantity']   = $receiving_quantity['TrnReceiving']['quantity'];
                        $stock_data[$sticker_id[0]['TrnSticker']['trn_consume_id']]['mst_department_id'] = $sticker_id[0]['TrnSticker']['mst_department_id'];
                        $stock_data[$sticker_id[0]['TrnSticker']['trn_consume_id']]['mst_item_unit_id']   = $sticker_id[0]['TrnSticker']['mst_item_unit_id'];

                        //シールの数量を元に戻す
                        //------------------------------------------------------------------
                        $condtion = array( 'TrnSticker.id' => $sticker_id[0]['TrnSticker']['id']);
                        $updatefield = array(
                            'quantity'     => "'".$receiving_quantity['TrnReceiving']['quantity']."'",
                            );

                        //TrnStickerオブジェクトをcreate
                        $this->_controller->TrnSticker->create();

                        //SQL実行
                        if (!$this->_controller->TrnSticker->updateAll( $updatefield, $condtion )) {
                            $this->rollbackTables();
                            throw new Exception('消費データ削除に伴うシール更新エラー'
                                                . print_r($updatefield,true)
                                                . print_r($condtion,true)
                                                . print_r($this->validateErrors($this->_controller->TrnSticker), true)
                                                );
                            exit;
                            return false;
                        }
                    }
                }
            }
        }

        if(!empty($trn_consume_id)){
            //削除対象の消費ヘッダIDを取得
            foreach($trn_consume_id as $c_id){
                $consume_header_data = $this->_controller->TrnConsume->find('first',array('fields'=>array('id','trn_consume_header_id'),'recursive' => -1,'conditions'=>array('id'=>$c_id)));

                if(!in_array($consume_header_data['TrnConsume']['trn_consume_header_id'],$consume_header)){
                    $consume_header[] = $consume_header_data['TrnConsume']['trn_consume_header_id'];
                }
            }
        }
        if(!empty($trn_consume_id)){
            //消費データの削除&在庫の更新（病院在庫数をプラス）
            foreach($trn_consume_id as $c_id){
                if($c_id != "0"){

                    //更新時間チェック
                    if(!$this->check_ConsumeModified($c_id,$this->Session->read('ShippingHistory.receipt_read_time'))){
                        $this->rollbackTables();
                        throw new Exception('消費データ削除（更新時間チェック）エラー id = '.$c_id." sessionReadTime = ".$this->Session->read('ShippingHistory.receipt_read_time'));
                        exit;
                    }

                    //TrnConsumeオブジェクトを作成
                    $this->_controller->TrnConsume->create();
                    $this->_controller->TrnConsume->recursive = -1;
                    $updatefield = array('TrnConsume.is_deleted' => "'".TRUE."'"
                                         ,'TrnConsume.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'"
                                         ,'TrnConsume.modified'   => "'".$this->now."'",
                                         );
                    $condition = array('TrnConsume.id' => $c_id);

                    //SQL実行
                    if (!$this->_controller->TrnConsume->updateAll($updatefield,$condition,-1)) {
                        $this->rollbackTables();
                        throw new Exception('消費データ削除エラー'
                                            . print_r($updatefield,true)
                                            . print_r($condtion,true)
                                            . print_r($this->validateErrors($this->_controller->TrnConsume), true)
                                            );
                        exit;
                        return false;
                    }
                    //--------------------------------------------------------------------
                    //在庫テーブルの更新対象レコードを探す
                    $params = array (
                        'fields'     => array('TrnStock.id','TrnStock.stock_count'),
                        'recursive'  => -1,
                        'conditions' => array('TrnStock.is_deleted' => FALSE,
                                              'TrnStock.mst_item_unit_id'  => $stock_data[$c_id]['mst_item_unit_id'],   //包装単位
                                              'TrnStock.mst_department_id' => $stock_data[$c_id]['mst_department_id'],  //部署参照キー
                                              ),
                        );
                    $Stock = $this->_controller->TrnStock->find('first',$params);
                    //前の在庫数に消費前数量をプラス
                    $update_quantity = $Stock['TrnStock']['stock_count']+$stock_data[$c_id]['before_quantity'];
                    //--------------------------------------------------------------------
                    //在庫の更新

                    $condtion = array( 'TrnStock.id' => $Stock['TrnStock']['id']);
                    $updatefield = array( //在庫数増やす
                                          'stock_count'     => "'".$update_quantity."'",
                                          'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                          'modified'        => "'".$this->now."'",
                                          );

                    //TrnStockオブジェクトをcreate
                    $this->_controller->TrnStock->create();
                    $this->_controller->TrnStock->recursive = -1;
                    //SQL実行
                    if (!$this->_controller->TrnStock->updateAll( $updatefield, $condtion )) {
                        $this->rollbackTables();
                        throw new Exception('消費データ削除に伴う在庫更新エラー'
                                            . print_r($updatefield,true)
                                            . print_r($condtion,true)
                                            . print_r($this->validateErrors($this->_controller->TrnStock), true)
                                            );
                        exit;
                        return false;
                    }
                }
            }

            //消費ヘッダの削除処理
            //消費ヘッダで検索して紐付く消費明細が全て削除済みならヘッダも削除する
            foreach($consume_header as $c_header){
                $data = $this->_controller->TrnConsume->find('first',array('fields'=>array('id'),'recursive' => -1,'conditions'=>array('trn_consume_header_id'=>$c_header,'is_deleted'=>FALSE)));
                //↑ 未削除のデータが無ければ $data は空になる。
                if(empty($data)){
                    //消費ヘッダ削除
                    $condtion = array( 'TrnConsumeHeader.id' => $c_header);
                    $updatefield = array('TrnConsumeHeader.is_deleted' => "'".TRUE."'"
                                         ,'TrnConsumeHeader.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'"
                                         ,'TrnConsumeHeader.modified'   => "'".$this->now."'",
                                         );

                    //TrnConsumeHeaderオブジェクトをcreate
                    $this->_controller->TrnConsumeHeader->create();
                    $this->_controller->TrnConsumeHeader->recursive = -1;
                    //SQL実行
                    if (!$this->_controller->TrnConsumeHeader->updateAll( $updatefield, $condtion )) {
                        $this->rollbackTables();
                        throw new Exception('消費データ削除に伴う消費ヘッダ削除エラー'
                                            . print_r($updatefield,true)
                                            . print_r($condtion,true)
                                            . print_r($this->validateErrors($this->_controller->TrnConsumeHeader), true)
                                            );
                        exit;
                        return false;
                    }
                }
            }
        }
        return true;
    }
    //****************************************************************************
    /**
     * deleteStickerKey
     *
     * 受領時に必要な処理その3：シールの更新（消費参照キー削除・受領参照キーの削除）
     */
    function deleteStickerKey($header_id,$id){
        //更新対象のシールIDを入荷明細ごとに取得
        foreach($header_id as $header_id_data){
            foreach($id[$header_id_data] as $meisai_id){
                $params = array (
                    'fields'    => array('TrnSticker.id'),
                    'order'     => array('TrnSticker.id'),
                    'conditions'=> array('TrnSticker.receipt_id' => $meisai_id),
                    'recursive' => -1
                    );
                $pre_sticker_id = $this->_controller->TrnSticker->find('all',$params);
                $sticker_id[$meisai_id] = $pre_sticker_id[0]['TrnSticker']['id'];
            }
        }

        foreach ($header_id as $header_id_data){
            foreach($id[$header_id_data] as $meisai_id){

                $condtion = array( 'TrnSticker.id ='=> $sticker_id[$meisai_id] );
                $updatefield = array( 'TrnSticker.trn_consume_id' => NULL,//消費参照キー
                                      'TrnSticker.receipt_id'     => NULL,//受領参照キー
                                      );

                //TrnStickerオブジェクトを作成
                $this->_controller->TrnSticker->create();

                //SQL実行
                $this->_controller->TrnSticker->recursive = -1;
                if (!$this->_controller->TrnSticker->updateAll($updatefield,$condtion)) {
                    $this->rollbackTables();
                    throw new Exception('シール更新エラー'
                                        . print_r($updatefield,true)
                                        . print_r($condtion,true)
                                        . print_r($this->validateErrors($this->_controller->TrnSticker), true)
                                        );
                    exit;

                }
            }
        }
        return true;
    }
    //****************************************************************************
    /**
     * deleteStorages
     *
     * 受領時に必要な処理その4：入庫データの削除
     */
    function deleteStorages($header_id,$id){

        //入荷ヘッダIDから入庫ヘッダIDを取得
        $StorageHeader_id = array();
        foreach($header_id as $header){
            $params = array (
                'fields'    => array('TrnStorageHeader.id'),
                'order'     => array('TrnStorageHeader.id'),
                'conditions'=> array('TrnStorageHeader.trn_receiving_header_id' => $header),
                'recursive' => -1
                );
            $pre_StorageHeader_id = $this->_controller->TrnStorageHeader->find('all',$params);

            //入庫ヘッダIDのみ重複を省いて取得
            if(!in_array($pre_StorageHeader_id[0]['TrnStorageHeader']['id'],$StorageHeader_id)){
                $StorageHeader_id[] = $pre_StorageHeader_id[0]['TrnStorageHeader']['id'];
            }
        }
        //入荷明細から入庫明細を取得
        foreach($header_id as $header){
            foreach($id[$header] as $data){
                $params = array (
                    'fields'    => array('TrnStorage.id'),
                    'order'     => array('TrnStorage.id'),
                    'conditions'=> array('TrnStorage.trn_receiving_id' => $data),
                    'recursive' => -1
                    );
                $pre_Storage_id = $this->_controller->TrnStorage->find('all',$params);
                $Storage_id[] = $pre_Storage_id[0]['TrnStorage']['id'];
            }
        }


        //**************
        //入庫明細の削除
        //**************

        //TrnStorageオブジェクトをcreate
        $this->_controller->TrnStorage->create();


        for($i=0;$i<count($Storage_id);$i++){

            //更新時間チェック
            if(!$this->checkStorageModified($Storage_id[$i],$this->Session->read('ShippingHistory.receipt_read_time'))){
                $this->rollbackTables();
                throw new Exception('入庫明細削除（更新時間チェック）エラー'
                                    . 'id = '.$Storage_id[$i]
                                    . 'sessionReadTime ='.$this->Session->read('ShippingHistory.receipt_read_time')
                                    );
                exit;
            }

            $this->_controller->TrnStorage->unbindModel(array('belongsTo' => array('TrnStorageHeader')));
            $updatefield = array('TrnStorage.is_deleted' => "'".TRUE."'"
                                 ,'TrnStorage.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'"
                                 ,'TrnStorage.modified'   => "'".$this->now."'"
                                 );
            $condition = array('TrnStorage.id' => $Storage_id[$i]);

            $this->_controller->TrnStorage->recursive = -1;
            if (!$this->_controller->TrnStorage->updateAll($updatefield, $condition)){
                $this->rollbackTables();
                throw new Exception('入庫明細削除エラー'
                                    . print_r($updatefield,true)
                                    . print_r($condtion,true)
                                    . print_r($this->validateErrors($this->_controller->TrnStorage), true)
                                    );
                exit;
                return false;
            }
        }


        //ヘッダ配下にdeleteではない明細が無ければ、ヘッダも削除
        //オブジェクトをcreate
        $this->_controller->TrnStorageHeader->create();

        foreach($StorageHeader_id as $header){
            $params = array (
                'fields'    => array('TrnStorage.id'),
                'order'     => array('TrnStorage.id'),
                'conditions'=> array('TrnStorage.trn_storage_header_id' => $header,
                                     'TrnStorage.is_deleted' => FALSE),
                'recursive' => 1
                );
            $StorageHeader_info = $this->_controller->TrnStorage->find('all',$params);
            if(count($StorageHeader_info) == 0){
                $delete_header[] = $header;
            }
        }

        if(!empty($delete_header)){
            for($i=0;$i<count($delete_header);$i++){
                //更新時間チェック
                if(!$this->checkStorageHeaderModified($delete_header[$i],$this->Session->read('ShippingHistory.receipt_read_time'))){
                    $this->rollbackTables();
                    throw new Exception('入庫ヘッダ削除（更新時間チェック）エラー'
                                        . 'id = '.$delete_header[$i]
                                        . 'sessionReadTime ='.$this->Session->read('ShippingHistory.receipt_read_time')
                                        );
                    exit;
                }

                $updatefield = array('TrnStorageHeader.is_deleted' => "'".TRUE."'"
                                     ,'TrnStorageHeader.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'"
                                     ,'TrnStorageHeader.modified'   => "'".$this->now."'"
                                     );
                $condition = array('TrnStorageHeader.id' => $delete_header[$i]);

                $this->_controller->TrnStorageHeader->recursive = -1;
                if (!$this->_controller->TrnStorageHeader->updateAll($updatefield, $condition)){
                    $this->rollbackTables();
                    throw new Exception('入庫ヘッダ削除エラー'
                                        . print_r($updatefield,true)
                                        . print_r($condtion,true)
                                        . print_r($this->validateErrors($this->_controller->TrnStorageHeader), true)
                                        );
                    exit;
                }
            }
        }
        return true;
    }
    //****************************************************************************
    /**
     * updateStock
     *
     * 受領時に必要な処理その5：在庫の更新
     *(1)移動先病院在庫の在庫数を減らし、未入荷数を増やす
     *(2)移動元センター在庫の在庫数を増やす、被要求数を増やす
     */
    function updateStock($header_id,$id){
        $ids = array();
        foreach($id as $header){
            foreach($header as $meisai){
                $ids[] = $meisai;
            }
        }
        $uid = array_unique($ids);

        foreach($uid as $id){
            //入荷明細データの取得
            $params = array (
                'fields'     => array(),
                'order'      => array('TrnReceiving.id'),
                'recursive'  => -1,
                'conditions' => array('TrnReceiving.is_deleted' => FALSE,
                                      'TrnReceiving.id' => $id,
                                      ),
                );
            $Receiving_Info = $this->_controller->TrnReceiving->find('all',$params);

            //シールデータの取得
            $shipping_params = array (
                'fields'     => array(),
                'order'      => array('TrnShipping.id'),
                'recursive'  => 1,
                'conditions' => array('TrnShipping.is_deleted' => FALSE,
                                      'TrnShipping.id' => $Receiving_Info[0]['TrnReceiving']['trn_shipping_id'],
                                      ),
                );
            $Shipping_Info = $this->_controller->TrnShipping->find('first', $shipping_params);
            //******************************************************************
            //(1)移動先病院在庫の在庫数を減らし、未入荷数を増やす
            //******************************************************************

            //取得した入荷明細データから、在庫テーブルの更新対象レコードを探す
            $params = array (
                'fields'     => array(),
                'order'      => array('TrnStock.id'),
                'recursive'  => -1,
                'conditions' => array('TrnStock.is_deleted' => FALSE,
                                      'TrnStock.mst_item_unit_id'  => $Receiving_Info[0]['TrnReceiving']['mst_item_unit_id'],   //包装単位
                                      'TrnStock.mst_department_id' => $Receiving_Info[0]['TrnReceiving']['department_id_to'],  //部署参照キー
                                      ),
                );

            $Stock = $this->_controller->TrnStock->find('all',$params);
            //******************************************************************
            $condition = array( 'TrnStock.id' => $Stock[0]['TrnStock']['id'],);
            $updatefield = array( //在庫数減らす
                                  'stock_count'     => "'".($Stock[0]['TrnStock']['stock_count']-$Receiving_Info[0]['TrnReceiving']['quantity'])."'",
                                  'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                  'modified'        => "'".$this->now."'",
                                  );
            //TrnStockオブジェクトの作成
            $this->_controller->TrnStock->create();

            //SQL実行
            $this->_controller->TrnStock->recursive = -1;
            if (!$this->_controller->TrnStock->updateAll( $updatefield, $condition )) {
                $this->rollbackTables();
                throw new Exception('移動先病院在庫の在庫数マイナスエラー'
                                    . print_r($updatefield,true)
                                    . print_r($condtion,true)
                                    . print_r($this->validateErrors($this->_controller->TrnStock), true)
                                    );
                exit;
                return false;
            }

            //********************************************************************
            //(2)移動元センター在庫の在庫数を増やす、予約数を増やす
            //******************************************************************

            //取得した入荷明細IDから、在庫テーブルの更新対象レコードを探す
            $params2 = array (
                'fields'     => array(),
                'order'      => array('TrnStock.id'),
                'recursive'  => -1,
                'conditions' => array('TrnStock.is_deleted' => FALSE,
                                      'TrnStock.mst_item_unit_id'  => $Receiving_Info[0]['TrnReceiving']['mst_item_unit_id'],   //包装単位mst_item_unit_id
                                      'TrnStock.mst_department_id' => $Receiving_Info[0]['TrnReceiving']['department_id_from'],  //部署参照キー
                                      ),
                );

            $Stock2 = $this->_controller->TrnStock->find('all',$params2);
            //******************************************************************
            $condtion2 = array( 'TrnStock.id' => $Stock2[0]['TrnStock']['id'],);
            $updatefield2 = array( //在庫数増やす
                                   'stock_count'     => "'".($Stock2[0]['TrnStock']['stock_count']+$Receiving_Info[0]['TrnReceiving']['quantity'])."'",
                                   //予約数増やす
                                   //'reserve_count'   => "'".($Stock2[0]['TrnStock']['reserve_count']+$Receiving_Info[0]['TrnReceiving']['quantity'])."'",
                                   'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                   'modified'        => "'".$this->now."'",
                                   );

            //TrnStockオブジェクトをcreate
            $this->_controller->TrnStock->create();
            $this->_controller->TrnStock->recursive = -1;
            //SQL実行
            if (!$this->_controller->TrnStock->updateAll( $updatefield2, $condtion2 )) {
                $this->rollbackTables();
                throw new Exception('移動元センター在庫の在庫数をプラスエラー'
                                    . print_r($updatefield2,true)
                                    . print_r($condtion2,true)
                                    . print_r($this->validateErrors($this->_controller->TrnStock), true)
                                    );
                exit;
                return false;
            }
        }
        return true;
    }
    //****************************************************************************
    /**
     * updateShipping
     *
     * 受領時に必要な処理その6：出荷データの出荷状態を変更（ヘッダと明細両方）
     */
    function updateShippingHistory($header_id,$id){

        //入荷明細テーブルから出荷明細IDを取得
        foreach($header_id as $header){
            foreach($id[$header] as $meisai_id){
                $params = array (
                    'fields'     => array('TrnReceiving.trn_shipping_id'),
                    'order'      => array('TrnReceiving.id'),
                    'recursive'  => -1,
                    'conditions' => array('TrnReceiving.is_deleted' => FALSE,
                                          'TrnReceiving.id' => $meisai_id,),
                    );
                $Shipping_Info = $this->_controller->TrnReceiving->find('all',$params);
                $Shipping_id[] = $Shipping_Info[0]['TrnReceiving']['trn_shipping_id'];
            }

        }
        //取得した明細IDから更新対象の出荷ヘッダIDを取得（ヘッダ配下の明細が全部未受領かの確認の為）
        $Shipping_header_id = array();
        foreach($Shipping_id as $s_id){
            $params = array (
                'fields'     => array('TrnShipping.trn_shipping_header_id'),
                'order'      => array('TrnShipping.id'),
                'recursive'  => -1,
                'conditions' => array('TrnShipping.is_deleted' => FALSE,
                                      'TrnShipping.id' => $s_id,
                                      ),
                );
            $Shipping_Info = $this->_controller->TrnShipping->find('all',$params);

            if(!in_array($Shipping_Info[0]['TrnShipping']['trn_shipping_header_id'],$Shipping_header_id) ){
                $Shipping_header_id[] = $Shipping_Info[0]['TrnShipping']['trn_shipping_header_id'];
            }
        }

        //**************
        //出荷明細の更新
        //**************
        foreach($Shipping_id as $s_id){
            //更新時間チェック
            if(!$this->check_ShippingModified($s_id,$this->Session->read('ShippingHistory.receipt_read_time'))){
                $this->rollbackTables();
                throw new Exception('出荷明細更新（更新時間チェック）エラー：id = '.$s_id.'sessionReadTime = '.$this->Session->read('ShippingHistory.receipt_read_time'));
                exit;
            }

            $condtion = array( 'TrnShipping.id' => $s_id,);
            $updatefield = array('shipping_status'  => "'".Configure::read('ShippingStatus.ordered')."'",
                                 'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                 'modified'        => "'".$this->now."'",
                                 );


            //TrnShipping
            $this->_controller->TrnShipping->create();
            $this->_controller->TrnShipping->recursive = -1;

            //SQL実行
            if (!$this->_controller->TrnShipping->updateAll( $updatefield, $condtion )) {
                $this->rollbackTables();
                throw new Exception('出荷明細更新エラー'
                                    . print_r($updatefield,true)
                                    . print_r($condtion,true)
                                    . print_r($this->validateErrors($this->_controller->TrnStock), true)
                                    );
                exit;
                return false;
            }
        }

        //**************
        //出荷ヘッダの更新
        //**************
        foreach($Shipping_header_id as $header_id){
            $receiving_header_status = "";

            //処理中の出荷ヘッダを外部キーに持つ出荷明細を検索
            $params = array (
                'fields'     => array('TrnShipping.id','TrnShipping.trn_shipping_header_id'),
                'order'      => array('TrnShipping.id'),
                'recursive'  => -1,
                'conditions' => array('TrnShipping.is_deleted' => FALSE,
                                      'TrnShipping.trn_shipping_header_id' => $header_id,
                                      ),
                );
            $Shipping_Info = $this->_controller->TrnShipping->find('all',$params);
            $pre1 = array();$data1 = array();
            foreach($Shipping_Info as $data1){$pre1[] = $data1['TrnShipping']['id'];}

            //処理中の出荷ヘッダを外部キーに持ち、かつ初期状態のものを取得
            $params2 = array (
                'fields'     => array('TrnShipping.id','TrnShipping.trn_shipping_header_id'),
                'order'      => array('TrnShipping.id'),
                'recursive'  => -1,
                'conditions' => array('TrnShipping.is_deleted' => FALSE,
                                      'TrnShipping.trn_shipping_header_id' => $header_id,
                                      'TrnShipping.shipping_status' => Configure::read('ShippingStatus.ordered'),),
                );
            $Shipping_Info2 = $this->_controller->TrnShipping->find('all',$params2);
            $pre2 = array();$data2 = array();
            foreach($Shipping_Info2 as $data2){$pre2[] = $data2['TrnShipping']['id'];}

            $err = 0;
            foreach($pre1 as $data){
                if(!in_array($data,$pre2) ){$err++;}
            }
            //上記2配列がイコールなら処理中の出荷ヘッダ配下は全て受領取消されたので、ヘッダも初期状態へ
            if($err == "0"){
                $receiving_header_status = Configure::read('ShippingStatus.ordered');

                //上記2配列がイコールでない場合は、ヘッダは一部受領へ戻す
            } else {
                $receiving_header_status = Configure::read('ShippingStatus.partLoaded');
            }

            //--------------------------------------------------------------------
            //出荷ヘッダ更新
            $condtion = array( 'TrnShippingHeader.id' => $header_id,);
            $updatefield = array('shipping_status'  => "'".$receiving_header_status."'",
                                 'modifier'        => "'".$this->Session->read('Auth.MstUser.id')."'",
                                 'modified'        => "'".$this->now."'",
                                 );

            //TrnShippingHeader
            $this->_controller->TrnShippingHeader->create();
            $this->_controller->TrnShippingHeader->recursive = -1;

            //SQL実行
            if (!$this->_controller->TrnShippingHeader->updateAll( $updatefield, $condtion )) {
                $this->rollbackTables();
                throw new Exception('出荷ヘッダ更新エラー'
                                    . print_r($updatefield,true)
                                    . print_r($condtion,true)
                                    . print_r($this->validateErrors($this->_controller->TrnStock), true)
                                    );
                exit;
                return false;
            }

        }
        return true;
    }
    //**************************************************************************
    /**
     * deleteStickerRecords
     *
     * 受領時に必要な処理その7：シールの移動履歴を削除（全区分）
     */
    function deleteStickerRecords ($header_id,$id) {
        foreach($header_id as $header){
            foreach($id[$header] as $meisai){
                //不要なアソシエーションを一時解除
                $this->_controller->TrnSticker->unbindModel(array('belongsTo' => array('TrnShipping')));
                $this->_controller->TrnSticker->unbindModel(array('belongsTo' => array('TrnStorage')));
                $this->_controller->TrnSticker->unbindModel(array('belongsTo' => array('MstDepartment')));
                $this->_controller->TrnSticker->unbindModel(array('belongsTo' => array('MstItemUnit')));
                $this->_controller->TrnSticker->unbindModel(array('belongsTo' => array('TrnReceiving')));

                $this->_controller->TrnReceiving->unbindModel(array('belongsTo' => array('TrnOrder')));
                $this->_controller->TrnReceiving->unbindModel(array('belongsTo' => array('MstItemUnit')));
                $this->_controller->TrnReceiving->unbindModel(array('belongsTo' => array('MstClass')));
                $this->_controller->TrnReceiving->unbindModel(array('belongsTo' => array('MstDepartment')));
                $this->_controller->TrnReceiving->unbindModel(array('belongsTo' => array('MstUser')));
                $this->_controller->TrnReceiving->unbindModel(array('hasOne' => array('TrnClaim')));
                //TrnStickerからTrnReceivingをhasOneでアソシエーション設定しなおし
                $this->_controller->TrnSticker->bindModel(array('belongsTo' => array('TrnReceiving' => array('conditions' => array('TrnReceiving.is_deleted' => FALSE),'foreignKey' => 'receipt_id'))));

                //出荷明細IDからシールIDを取得
                $params = array (
                    'fields'     => array(''),
                    'conditions' => array('TrnSticker.is_deleted' => FALSE,
                                          'TrnSticker.receipt_id' => $meisai
                                          ),
                    'recursive'  => 2
                    );
                $Sticker_Info = array();
                $Sticker_Info = $this->_controller->TrnSticker->find('all',$params);

                //レコード区分取得

                $condtion = array( 'TrnStickerRecord.trn_sticker_id ='=> $Sticker_Info[0]['TrnSticker']['id'],  //シール参照キー
                                   'TrnStickerRecord.move_date'       => $Sticker_Info[0]['TrnReceiving']['work_date'], //移動日時(この場合入荷明細のwork_date)
                                   'TrnStickerRecord.record_type'     => Configure::read('RecordType.receipts'), //シール移動履歴＠レコード区分「受領」
                                   'TrnStickerRecord.record_id'       => $Sticker_Info[0]['TrnReceiving']['id'],  //レコードキー(この場合入荷明細のID)
                                   );
                $updatefield = array( 'TrnStickerRecord.is_deleted' => "'".TRUE."'",//削除フラグ
                                      'TrnStickerRecord.modifier'   => "'".$this->Session->read('Auth.MstUser.id')."'",
                                      'TrnStickerRecord.modified'   => "'".$this->now."'",
                                      );

                $this->_controller->TrnStickerRecord->recursive = -1;
                if (!$this->_controller->TrnStickerRecord->updateAll($updatefield,$condtion)) {
                    $this->rollbackTables();
                    throw new Exception('シール移動履歴削除エラー'
                                        . print_r($updatefield,true)
                                        . print_r($condtion,true)
                                        . print_r($this->validateErrors($this->_controller->TrnStickerRecord), true)
                                        );
                    exit;
                    return false;
                }
            }
        }

        return true;
    }

    //****************************************************************************
    /**
     * deleteReceivings
     *
     * 受領時に必要な処理その9：入荷ヘッダ・入荷明細の削除
     */
    function deleteReceivings($header_id,$id){
        $delete_header = array();
        foreach($header_id as $header){
            foreach($id[$header] as $meisai){

                //受領明細IDからシールIDと元の部署を取得
                $params = array (
                    'fields'     => array('TrnReceiving.id','TrnReceiving.trn_sticker_id','TrnReceiving.department_id_from'),
                    'conditions' => array('TrnReceiving.is_deleted' => FALSE,
                                          'TrnReceiving.id' => $meisai),
                    'recursive'  => -1
                    );
                $Info = $this->_controller->TrnReceiving->find('first',$params);

                //受領にシールIDが入っていない場合スキップセット品を想定
                if($Info['TrnReceiving']['trn_sticker_id']!=""){
                    //シールの更新
                    $condtion = array( 'TrnSticker.id ='      => $Info['TrnReceiving']['trn_sticker_id'] );
                    $updatefield = array( 'mst_department_id' => "'".$Info['TrnReceiving']['department_id_from']."'",//部署
                                          );


                    //TrnStickerオブジェクトをcreate
                    $this->_controller->TrnSticker->create();
                    $this->_controller->TrnSticker->recursive = -1;

                    //SQL実行
                    if (!$this->_controller->TrnSticker->updateAll( $updatefield, $condtion )) {
                        $this->rollbackTables();
                        throw new Exception('シール更新エラーA1'
                                            . print_r($updatefield,true)
                                            . print_r($condtion,true)
                                            . print_r($this->_controller->validateErrors($this->_controller->TrnSticker), true)
                                            );
                        exit;
                        return false;
                    }
                }
                //入荷明細の削除
                $updatefield = array('TrnReceiving.is_deleted' => "'".TRUE."'",//削除フラグ
                                     'TrnReceiving.modifier'     => "'".$this->Session->read('Auth.MstUser.id')."'",
                                     'TrnReceiving.modified'     => "'".$this->now."'");
                $condition = array('TrnReceiving.id' => $meisai);

                $this->_controller->TrnReceiving->recursive = -1;
                if (!$this->_controller->TrnReceiving->updateAll($updatefield, $condition , -1)){
                    $this->rollbackTables();
                    throw new Exception('入荷明細削除エラー'
                                        . print_r($updatefield,true)
                                        . print_r($condtion,true)
                                        . print_r($this->_controller->validateErrors($this->_controller->TrnReceiving), true)
                                        );
                    exit;
                    return false;
                }
            }
            //現在処理中のヘッダに紐付く明細が全て削除されていたら、ヘッダも削除する

            //TrnReceivingHeaderオブジェクトをcreate
            $this->_controller->TrnReceivingHeader->create();

            $params = array (
                'fields'    => array('TrnReceiving.id'),
                'order'     => array('TrnReceiving.id'),
                'conditions'=> array('TrnReceiving.trn_receiving_header_id' => $header,
                                     'TrnReceiving.is_deleted' => FALSE),
                'recursive' => -1
                );
            $Receiving_info = $this->_controller->TrnReceiving->find('all',$params);
            if(count($Receiving_info) == 0){
                $delete_header[] = $header;
            }

            for($i=0;$i<count($delete_header);$i++){
                $updatefield = array('TrnReceivingHeader.is_deleted' => "'".TRUE."'");
                $condition = array('TrnReceivingHeader.id' => $delete_header[$i]);

                $this->_controller->TrnReceivingHeader->recursive = -1;
                if (!$this->_controller->TrnReceivingHeader->updateAll($updatefield, $condition)){
                    $this->rollbackTables();
                    throw new Exception('入荷ヘッダ削除エラー'
                                        . print_r($updatefield,true)
                                        . print_r($condtion,true)
                                        . print_r($this->_controller->validateErrors($this->_controller->TrnReceivingHeader), true)
                                        );
                    exit;
                    return false;
                }
            }

        }
        return true;
    }

    /***************************************************
     * 共通メソッド
     ***************************************************/


    /**
     * 作業番号を取得
     * 日付がtimestomp型で入ってくるので成型
     * @param <type> $date
     */
    private function setWorkNo4Header($date,$type){
        $date = date('Ymd', strtotime($date));

        return $this->_controller->setWorkNo4Header($date,$type);
    }

    function check_ShippingHeaderModified($header_id,$readtime){
        //IDから更新時間を取得
        $ShippingHeader_modified = $this->_controller->TrnShippingHeader->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnShippingHeader.id = ' => $header_id)));
        //受領確認画面に入った後に更新が行われていた場合はエラーとする
        if(strtotime($ShippingHeader_modified['TrnShippingHeader']['modified']) > strtotime($readtime)){
            return false;
        }
        return true;
    }
    function check_ShippingModified($shipping_id,$readtime){
        //IDから更新時間を取得
        $Shipping_modified = $this->_controller->TrnShipping->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnShipping.id = ' => $shipping_id)));
        //受領確認画面に入った後に更新が行われていた場合はエラーとする
        if(strtotime($Shipping_modified['TrnShipping']['modified']) > strtotime($readtime)){
            return false;
        }
        return true;
    }
    function check_StickerModified($sticker_id,$readtime){

        //シールIDから更新時間を取得
        $Sticker_modified = $this->_controller->TrnSticker->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnSticker.id = ' => $sticker_id)));
        //受領確認画面に入った後に更新が行われていた場合はエラーとする
        if(strtotime($Sticker_modified['TrnSticker']['modified']) > strtotime($readtime)){
            return false;
        }
        return true;
    }
    function check_PromiseModified($promise_id,$readtime){

        //引当明細IDから更新時間を取得
        $Promise_modified = $this->_controller->TrnPromise->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnPromise.id = ' => $promise_id)));
        //受領確認画面に入った後に更新が行われていた場合はエラーとする
        if(strtotime($Promise_modified['TrnPromise']['modified']) > strtotime($readtime)){
            $this->rollbackTables();
            throw new Exception('引当明細更新（更新時間チェック）エラー（詳細）：id = '
                                .$promise_id['TrnSticker']['trn_promise_id']
                                ."readTime = ".$this->Session->read('ShippingHistory.receipt_read_time')
                                .strtotime($Promise_modified['TrnPromise']['modified']) ." > ".strtotime($readtime) ."?");
            exit;
            return false;
        }
        return true;
    }
    function check_OrderModified($order_id,$readtime){

        //シールIDから更新時間を取得
        $Order_modified = $this->_controller->TrnOrder->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnOrder.id = ' => $order_id)));
        //受領確認画面に入った後に更新が行われていた場合はエラーとする
        if(strtotime($Order_modified['TrnOrder']['modified']) > strtotime($readtime)){
            return false;
        }

        return true;
    }
    function check_ClaimModified($claim_id,$readtime){
        //シールIDから更新時間を取得
        $modified = $this->_controller->TrnClaim->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnClaim.id = ' => $claim_id)));
        //受領確認画面に入った後に更新が行われていた場合はエラーとする
        if(strtotime($modified['TrnClaim']['modified']) > strtotime($readtime)){
            return false;
        }

        return true;
    }
    function checkStorageModified($storage_id,$readtime){
        //シールIDから更新時間を取得
        $modified = $this->_controller->TrnStorage->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnStorage.id = ' => $storage_id)));
        //受領確認画面に入った後に更新が行われていた場合はエラーとする
        if(strtotime($modified['TrnStorage']['modified']) > strtotime($readtime)){
            return false;
        }

        return true;
    }

    function checkStorageHeaderModified($storage_id,$readtime){
        //シールIDから更新時間を取得
        $modified = $this->_controller->TrnStorageHeader->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnStorageHeader.id = ' => $storage_id)));
        //受領確認画面に入った後に更新が行われていた場合はエラーとする
        if(strtotime($modified['TrnStorageHeader']['modified']) > strtotime($readtime)){
            return false;
        }

        return true;
    }

    function check_ConsumeModified($consume_id,$readtime){
        //シールIDから更新時間を取得
        $modified = $this->_controller->TrnConsume->find('first',array('fields'=>'modified','recursive' => -1,'conditions' => array('TrnConsume.id = ' => $consume_id)));
        //受領確認画面に入った後に更新が行われていた場合はエラーとする
        if(strtotime($modified['TrnConsume']['modified']) > strtotime($readtime)){
            return false;
        }

        return true;
    }

    private function rollbackTables(){
        $this->_controller->TrnPromise->rollback();//引当明細
        $this->_controller->TrnClaim->rollback();//請求
        $this->_controller->TrnSticker->rollback();//シール
        $this->_controller->TrnStorage->rollback();//入庫明細
        $this->_controller->TrnStorageHeader->rollback();//入庫ヘッダ
        $this->_controller->TrnStock->rollback();//在庫
        $this->_controller->TrnShipping->rollback();//出荷明細
        $this->_controller->TrnShippingHeader->rollback();//出荷ヘッダ
        $this->_controller->TrnStickerRecord->rollback();//シール移動履歴
        $this->_controller->TrnOrder->rollback();//発注明細
        $this->_controller->TrnReceiving->rollback();//入荷明細
        $this->_controller->TrnReceivingHeader->rollback();//入荷ヘッダ
    }
}
?>
