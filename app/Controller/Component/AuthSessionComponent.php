<?php
/**
 * ログインしているユーザの権限をチェックするコンポーネント
 *
 * @version 0.0.1
 * @since 2010/09/07
 */
class AuthSessionComponent extends Component {

    /*
     * セッション内のAuth.*で管理されているデータの名称
     */
    const AUTH_SESSION              = 'Auth';
    const AUTH_MST_USER             = 'Auth.MstUser';
    const AUTH_MST_DEPARTMENT       = 'Auth.MstDepartment';
    const AUTH_FACILITY             = 'Auth.MstFacility';
    const AUTH_FACILITY_RELATION    = 'Auth.MstFacility.MstFacilityRelation';
    const AUTH_MST_ROLE             = 'Auth.MstRole';
    const AUTH_MST_ROLE_TYPE        = 'Auth.MstRole.MstRoleType';
    const AUTH_MST_EDITON           = 'Auth.MstRole.MstEdition';
    const AUTH_MST_PLAN             = 'Auth.MstPlan';
    const AUTH_MST_USER_BELONGING   = 'Auth.MstUserBelonging';
    const AUTH_PARENT_MST_USER      = 'Auth.Parent';
    const AUTH_FACILITY_CONFIG      = 'Auth.Config';
    const AUTH_SELECTED_FACILITY_ID = 'Auth.facility_id_selected';
    const AUTH_FACILITIES           = 'Auth.facilities';
    const AUTH_MASTER_ITEM_INFO     = 'Auth.masterItemInfo';
    const AUTH_MENU_GROUPS          = 'Auth.menuGroups';
    const AUTH_MENUS                = 'Auth.menus';
    const AUTH_ENABLE_MENU          = 'Auth.enableMenu';
    const AUTH_FAVORITE_MENUS       = 'Auth.favorite_menu';

    /**
     *
     * @var array $components
     */
    var $components = array('Session');

    var $_controller = null;

    public function startup(&$controller) {
        $this->_controller = &$controller;
    }


    public static function hasParentUserBy($mstUser) {
        return isset($mstUser['parent_id']);
    }

    public static function isParentUserBy($mstUser) {
        return !self::hasParentUserBy($mstUser);
    }

    public static function isChildUserBy($mstUser) {
        return self::hasParentUserBy($mstUser);
    }


    public function hasParentUser() {
        return self::hasParentUserBy($this->getLoginUser());
    }

    public function isParentUser() {
        return self::isParentUserBy($this->getLoginUser());
    }

    public function isChildUser() {
        return self::isChildUserBy($this->getLoginUser());
    }


    public function isSystemUserRole() {
        $mstRole = $this->getRoleOfLoginUser();
        if (is_null($mstRole)) return false;
        return Configure::read('role.type.system') == $mstRole['mst_role_type_id'];
    }

    public function isEdiUserRole() {
        $mstRole = $this->getRoleOfLoginUser();
        if (is_null($mstRole)) return false;
        return Configure::read('role.type.edi') == $mstRole['mst_role_type_id'];
    }

    public function isLiteUserRole() {
        $mstRole = $this->getRoleOfLoginUser();
        if (is_null($mstRole)) return false;
        return Configure::read('role.type.lite') == $mstRole['mst_role_type_id'];
    }

    public function isCatalogUserRole() {
        $mstRole = $this->getRoleOfLoginUser();
        if (is_null($mstRole)) return false;
        return Configure::read('role.type.catalog') == $mstRole['mst_role_type_id'];
    }

    public function getLoginUser() {
        return $this->Session->read(self::AUTH_MST_USER);
    }

    public function getRoleOfLoginUser() {
        return $this->Session->read(self::AUTH_MST_ROLE);
    }

    public function getEditionOfLoginUser() {
        return $this->Session->read(self::AUTH_MST_EDITON);
    }

    public function getPlanOfLoginUser() {
        return $this->Session->read(self::AUTH_MST_PLAN);
    }

    /**
     * ログイン中ユーザのメニュー情報をグループにまとめて取得する。
     * @param boolean $fromCache キャッシュから取得する場合はtrue, DB上の最新のデータから取得する場合はfalseを指定する。(default: true)
     * @return グループにまとめたメニュー情報
     */
    public function getMenuGroups($fromCache = true) {
        if ($fromCache) {
            $menuGroups = $this->Session->read(self::AUTH_MENU_GROUPS);
            if (!empty($menuGroups)) return $menuGroups;
        }
        return $this->updateMenuGroupsOfLoginUser();
    }

    /**
     * ログイン中ユーザのメニュー情報(HTML形式)を取得する。
     * @param boolean $fromCache キャッシュから取得する場合はtrue, DB上の最新のデータから取得する場合はfalseを指定する。(default: true)
     * @return メニュー情報(HTML形式)
     */
    public function getMenus($fromCache = true) {
        if ($fromCache) {
            $menus = $this->Session->read(self::AUTH_MENUS);
            if (!empty($menus)) return $menus;
        }
        return $this->updateMenusOfLoginUser();
    }

    /**
     * ログイン中ユーザのプロファイル情報を指定されたユーザIDのプロファイル情報へ更新する。
     * また、これに関連する他のキャッシュ情報も連動して最新の情報に更新される。
     *
     * @param integer $loginUserId ログインユーザのID
     * @return 更新後のユーザプロファイル情報
     */
    public function updateProfileOfLoginUser($loginUserId) {
        $profile = $this->_controller->MstUser->find('first', array(
                'conditions' => array(
                        'MstUser.id' => $loginUserId,
                        'MstUser.is_deleted' => false,
                ),
                'recursive' => 2,
        ));
        foreach ($profile as $key => $value) {
            $this->Session->write('Auth.' . $key, $value);
        }

        // 以下、関連する他のキャッシュを更新
        $this->updateFacilitiesOfLoginUser();
        $facilityId = $profile['MstUser']['mst_facility_id'];
        $this->updateSelectedFacilitiyId($facilityId);
        if ($this->isHospitalUserType()) {
            $this->updateFacilityConfig($facilityId);
        }
        $this->updateMenuGroupsOfLoginUser();
        $this->updateMenusOfLoginUser();
        $this->updateFavoriteMenus();

        return $profile;
    }

    /**
     * 選択中の施設IDを指定された施設IDへ更新する。
     * また、これに関連する他のキャッシュ情報も連動して最新の情報に更新される。
     *
     * @param integer $facilityId 施設ID
     */
    public function updateSelectedFacilitiyId($facilityId) {
        $this->Session->delete(self::AUTH_SELECTED_FACILITY_ID);
        $this->Session->write(self::AUTH_SELECTED_FACILITY_ID, $facilityId);
        $this->updateMasterItemInfo();
    }

    /**
     * 選択中の施設のマスター利用情報を最新の情報へ更新する。
     *
     * @param integer $facilityId 施設ID
     * @return 更新後のマスター利用情報
     */
    public function updateMasterItemInfo() {
        $this->Session->delete(self::AUTH_MASTER_ITEM_INFO);
        $masterItemInfo = array(
                'edition_name' => 'エディション不明',
                'plan_name' => 'プラン不明',
                'max_count' => 0,
                'use_count' => 0,
                'claim_price' => 0,
                'grand_count' => 0,
        );

        if (!$this->isEdiUserType()) {
            $edition = $this->getEditionOfLoginUser();
            if (!empty($edition['id'])) {
                $masterItemInfo['edition_name'] = $edition['name'];
            }

            $plan = $this->getPlanOfLoginUser();
            if (!empty($plan['id'])) {
                $masterItemInfo['plan_name'] = $plan['name'];
                $masterItemInfo['max_count'] = $plan['max_items'];
            }

            $facilityId = $this->Session->read(self::AUTH_SELECTED_FACILITY_ID);
            $mu = ClassRegistry::init('MstUser');
            $ret = $mu->query('select count(*) as count from mst_facility_items as a where a.center_facility_id = ' . $facilityId);
            $masterItemInfo['use_count'] = $ret[0][0]['count'];

            // コスロスマスタ件数
            $sql  = ' select ';
            $sql .= '       count(*) as count   ';
            $sql .= '  from ';
            $sql .= '    mst_grand_kyo_materials as a  ';
            
            $ret = $mu->query($sql);
            $masterItemInfo['grand_count'] = $ret[0][0]['count'];
            
        }

        $this->Session->write(self::AUTH_MASTER_ITEM_INFO, $masterItemInfo);
        return $masterItemInfo;
    }

    /**
     * 未実装
     */
    public function updateFavoriteMenus($fromCache = true) {
        $this->Session->delete(self::AUTH_FAVORITE_MENUS);
        $loginUser = $this->getLoginUser();

        $favoriteMenus = "";
        /*
        if (is_null($id)) return false;

        $sql  = " select ";
        $sql .= "     b.name , ";
        $sql .= "     b.url ";
        $sql .= " from ";
        $sql .= "   mst_favorite_views as a  ";
        $sql .= "   left join   ";
        $sql .= "   mst_views as b  ";
        $sql .= "   on ";
        $sql .= "    a.mst_view_id = b.id ";
        $sql .= " where ";
        $sql .= "   a.mst_user_id = "  . $id;
        $sql .= "   and a.is_deleted = false " ;
        $sql .= " order by ";
        $sql .= "   a.order_num ";

        $favorite = $this->MstUser->query($sql);

        $favoriteMenus = '<div id="fav_menu">';
        $favoriteMenus .= '<p class="fav_link"><a href="' . $this->webroot . 'mst_favorite_menu">お気に入り編集</a></p>';
        foreach ($favorite as $fav) {
            $favoriteMenus .= '<p class="fav_link"><a href="' .$this->webroot .  $fav[0]['url'] . '">' . $fav[0]['name'] . '</a></p>';
        }
        $favoriteMenus .= '</div>';
        */
        $this->Session->write(self::AUTH_FAVORITE_MENUS, $favoriteMenus);
        return $favoriteMenus;
    }

    public function updateFacilityConfig($facilityId) {
        $this->Session->delete(self::AUTH_FACILITY_CONFIG);

        // 動作フラグ取得
        $sql  = ' select ';
        $sql .= '       a.export_csv             as "Config__ExportCsv" ';
        $sql .= '     , a.import_csv             as "Config__ImportCsv" ';
        $sql .= '     , a.shipping               as "Config__Shipping" ';
        $sql .= '     , a.d2consumes             as "Config__D2Consumes" ';
        $sql .= '     , a.shipping_consumes      as "Config__ShippingConsumes" ';
        $sql .= '     , a.ex_shipping_consumes   as "Config__ExShippingConsumes" ';
        $sql .= '     , a.ms_corporate           as "Config__MSCorporate" ';
        $sql .= '     , a.order_mail             as "Config__OrderMail"  ';
        $sql .= '     , a.temporary_claim_type   as "Config__TemporaryClaimType" ';
        $sql .= '     , a.code_length            as "Config__CodeLength" ';
        $sql .= '     , a.stock_type             as "Config__StockType" ';
        $sql .= '     , a.layout_type            as "Config__LayoutType" ';
        $sql .= '     , a.ope_set_use_type       as "Config__OpeSetUseType" ';
        $sql .= '     , a.lowlevel_sales_type    as "Config__LowlevelSalesType" ';
        $sql .= '     , a.inner_move_type_change as "Config__InnerMoveTypeChange" ';

        for ($i = 1; $i <= 30; $i++) {
            $sql .= '     , ( case when ';
            $sql .= '         length(coalesce(a.spare_key'.$i.'_title , \'\' )) > 0 then a.spare_key'.$i.'_title ';
            $sql .= "         else '予備キー" . $i . "'";
            $sql .= '        end )                      as "Config__SpareKey'.$i.'Title"';
            $sql .= "     , ( case a.spare_key{$i}_class ";
            foreach (Configure::read('SpareKeyClass.output') as $k => $v) {
                $sql .= "     when '" . $k . "' then '" . $v . "'";
            }
            $sql .= "         else ''  ";
            $sql .= '         end )                     as "Config__SpareKey'.$i.'Class"';
            $sql .= '     , a.spare_key'.$i.'_select    as "Config__SpareKey'.$i.'Select" ';
        }
        $sql .= '     , b.bench_flg              as "Config__BenchFlg" ';
        $sql .= '     , b.zura_flg               as "Config__ZuraFlg" ';
        $sql .= '   from ';
        $sql .= '     mst_facility_options as a  ';
        $sql .= '     left join mst_facilities as b';
        $sql .= '     on b.id = a.mst_facility_id ';
        $sql .= '   where ';
        $sql .= '     a.mst_facility_id = ' . $facilityId;

        $ret = $this->_controller->MstFacilityOption->query($sql);
        $this->Session->write(self::AUTH_FACILITY_CONFIG , $ret[0]['Config']);
    }

    public function updateEnableMenu($flag) {
        $this->Session->write(self::AUTH_ENABLE_MENU, $flag);
    }

    public function deleteAuthSession() {
        $this->Session->delete(self::AUTH_SESSION);
    }


    private function updateFacilitiesOfLoginUser() {
        $this->Session->delete(self::AUTH_FACILITIES);
        $loginUser = $this->getLoginUser();
        if (empty($loginUser)) return null;

        if ($this->isEdiUserRole()) {
            // 部署IDが空以外だったらEDIログイン
            $sql  =' select ';
            $sql .='       c.id             as "MstFacility__id"';
            $sql .='     , c.facility_name  as "MstFacility__facility_name"';
            $sql .='   from ';
            $sql .='     mst_users as a  ';
            $sql .='     left join mst_departments as b  ';
            $sql .='       on b.id = a.mst_department_id  ';
            $sql .='     left join mst_facilities as c  ';
            $sql .='       on c.id = b.mst_facility_id  ';
            $sql .='   where ';
            $sql .='     a.is_deleted = false  ';
            $sql .='     and b.is_deleted = false  ';
            $sql .='     and c.is_deleted = false  ';
            $sql .='     and a.id =' . $loginUser['id'];
            $sql .='     and c.facility_type = ' . Configure::read('FacilityType.supplier'); //業者のみに絞る
        } else {
            $sql  =' select ';
            $sql .='       b.id             as "MstFacility__id"';
            $sql .='     , b.facility_name  as "MstFacility__facility_name"';
            $sql .='   from ';
            $sql .='     mst_user_belongings as a  ';
            $sql .='     left join mst_facilities as b  ';
            $sql .='       on b.id = a.mst_facility_id  ';
            $sql .='   where ';
            $sql .='     a.is_deleted = false  ';
            $sql .='     and b.is_deleted = false  ';
            $sql .='     and a.mst_user_id =' . $loginUser['id'];
            $sql .='     and b.facility_type = ' . Configure::read('FacilityType.center'); //センターのみに絞る
            $sql .=' order by b.id ';
        }

        $facilities = Set::Combine(
                $this->_controller->MstUser->query($sql),
                "{n}.MstFacility.id",
                "{n}.MstFacility.facility_name");
        if (empty($facilities)) return null;

        $this->Session->write(self::AUTH_FACILITIES, $facilities);
        return $facilities;
    }

    private function updateMenuGroupsOfLoginUser() {
        $this->Session->delete(self::AUTH_MENU_GROUPS);
        $loginUser = $this->getLoginUser();

        $sql  = "SELECT ";
        $sql .= "    f.id    AS group__id";
        $sql .= "   ,f.name  AS group__name";
        $sql .= "   ,f.image AS group__image";
        $sql .= "   ,d.id    AS view__id";
        $sql .= "   ,d.name  AS view__name";
        $sql .= "   ,d.url   AS view__url";
        $sql .= "  FROM ";
        $sql .= "    mst_users AS a";                   // a: MstUsers
        $sql .= "    INNER JOIN mst_roles AS b";        // b: MstRoles
        $sql .= "      ON b.id = a.mst_role_id";
        $sql .= "        AND b.is_deleted = false";
        $sql .= "    INNER JOIN mst_privileges AS c";   // c: MstPrivileges
        $sql .= "      ON c.mst_role_id = b.id";
        $sql .= "        AND c.is_deleted = false";
        $sql .= "    INNER JOIN mst_views AS d";        // d: MstViews
        $sql .= "      ON d.view_no = c.view_no";
        $sql .= "        AND d.is_deleted = false";
        $sql .= "    INNER JOIN mst_view_headers AS e"; // e: MstViewHeaders
        $sql .= "      ON e.id = d.view_header_id";
        $sql .= "        AND e.is_deleted = false";
        $sql .= "    INNER JOIN mst_view_groups AS f";  // f: MstViewGroups
        $sql .= "      ON f.id = e.mst_view_group_id";
        $sql .= "        AND f.is_deleted = false";
        $sql .= "  WHERE ";
        $sql .= "    a.id = " . $loginUser['id'] ;
        if(!$this->Session->read('Auth.Config.ZuraFlg')){
            $sql .= "    AND f.id != 15 ";
        }
        $sql .="       AND c.readable = true";
        $sql .= "  ORDER BY";
        $sql .= "    f.order_num";
        $sql .= "   ,d.order_num";
        $menuList = $this->_controller->MstUser->query($sql);

        // メニューグループ単位にまとめる
        $menuGroups = array();
        foreach ($menuList as $data) {
            $group = $data['group'];
            $groupName = $group['name'];
            if (!empty($menuGroups[$groupName])) {
                $group = $menuGroups[$groupName];
            }
            $viewName = $data['view']['name'];
            $group['views'][$viewName] = $data['view'];
            $menuGroups[$groupName] = $group;
        }

        $this->Session->write(self::AUTH_MENU_GROUPS, $menuGroups);
        return $menuGroups;
    }

    private function updateMenusOfLoginUser() {
        $this->Session->delete(self::AUTH_MENUS);
        $loginUser = $this->getLoginUser();
        $menuGroups = $this->getMenuGroups();
        $catalogUrl = Configure::read('catalog.url');
        $webRoot = $this->_controller->webroot;

        $menus ="";
        $menus .= "<nav class='nav-back'>";
        $menus .= "<ul class='menu'>";
        $menus .= "<li><a href='" . $webRoot . "'><img src='" . $webRoot . "/img/nav_icon_home.png' width='30px' height='30px' /><span>TOP</span></a></li>";

        foreach ($menuGroups as $group) {
            $menus .= "<li><a href='#'><img src='" . $webRoot ."img/". $group['image']. "'  width='30px' height='30px'  /><span>" . $group['name'] . "</span></a>";
            $menus .= "<ul>";
            if($group['name'] == '発注') {
                $menus .= "<li><a target='_blank' href='". $catalogUrl . "session.mvc/login?uiand=" . $loginUser['login_id'] . "&paand=" . $loginUser['password']  . "'>部署請求</a></li>";
            }
            foreach ($group['views'] as $view) {
                $menus .= "<li><a href='" . $webRoot . $view['url'] .  "'>" . $view['name'] . "</a></li>";
            }
            $menus .= '</ul>';
            $menus .= '</li>';
        }

        $menus .= "<li class='last-child'><a href='#'><img src='" . $webRoot . "/img/nav_icon_concierge.png' width='30px' height='30px' /><span>ヘルプデスク</span></a>";
        $menus .= "<ul><li><a href='" . $webRoot . "/Concierge'><span>お問い合わせ一覧</span></a></li>";
        $menus .= "<li><a href='" . $webRoot . "/Concierge/add'><span>新規お問い合わせ</span></a></li></ul>";
        $menus .= "</li>";
        $menus .= "</ul>";
        $menus .= "</nav>";

        $this->Session->write(self::AUTH_MENUS, $menus);
        return $menus;
    }

    private function isEdiUserType() {
        $loginUser = $this->getLoginUser();
        return $loginUser['user_type'] == Configure::read('UserType.edi');
    }

    private function isHospitalUserType() {
        $loginUser = $this->getLoginUser();
        return $loginUser['user_type'] == Configure::read('UserType.hospital');
    }

}
