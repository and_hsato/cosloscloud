<?php
/**
 * PagerComponent
 */

class PagerComponent extends Component {
    /* プライベート変数 */
    private $_pageCount = 5;         // ページ表示件数
    private $_nextLink  = true ;     // 次へボタン表示
    private $_nextLabel = "次へ" ;   // 次へボタンラベル
    private $_backLink  = true;      // 前へボタン表示
    private $_backLabel = "前へ" ;   // 前へボタンラベル
    
    /* セッター */
    public function setPageCount($pVal)          { $this->_pageCount = $pVal; }
    public function setNextLink($pVal)           { $this->_nextLink = $pVal; }
    public function setNextLabel($pVal)          { $this->_nextLabel = $pVal; }
    public function setBackLink($pVal)           { $this->_backLink = $pVal; }
    public function setBackLabel($pVal)          { $this->_backLabel = $pVal; }

    /* ゲッター */
    public function getPageCount()               { return $this->_pageCount; }
    public function getNextLink()                { return $this->_nextLink; }
    public function getNextLabel()               { return $this->_nextLabel; }
    public function getBackLink()                { return $this->_backLink; }
    public function getBackLabel()               { return $this->_backLabel; }
    
    function createPager($crrent , $limit  , $maxCount ){
        $pageMax = ceil($maxCount / $limit);
        
        $tmp = floor($_pageCount / 2 );
        
        if($crrent + $tmp > $pageMax){
            $max = $pageMax;
        }else{
            $max = $crrent + $tmp;
        }

        if($crrnt - $tmp < 0){
            $min = 1;
        }else{
            $min = $crrent - $tmp;
        }
        
        $text = '<ul class="pager">';
        if($_backLink){
            $text .= '<li class="prev"><a href="hoge">' . $_backLabel . '</a></li>';
        }

        for ($i = $min; $i <= $max; $i++){
            if($i == $crrent){
                $text .= '<li><em>' . $i .  '</em></li>';
            }else{
                $text .= '<li><a href="hoge">' . $i . '</a></li>';
            }
        }
        if($_nextLink){
            $text .= '<li class="prev"><a href="hoge">' . $_nextLabel . '</a></li>';
        }
        
        $text .= '</ul>';
        
        return $text;
    }

}
?>
