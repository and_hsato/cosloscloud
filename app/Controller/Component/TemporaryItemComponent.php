<?php
/**
 * 臨時品に関するをチェックするコンポーネント
 *
 * @version 0.0.1
 * @since 2015/01/15
 */

class TemporaryItemComponent extends Component {

    //臨時品のタイプ値
    private $item_types;

    function startup(){
        $this->item_types = array(
            //出荷区分
            'Shippings' => array(
              Configure::read('ShippingType.extraordinary'),//臨時品
              Configure::read('ShippingType.exshipping'),   //臨時出荷
            ),
            //消費時に売上で設定する取引区分
            'consumes' => array(
              Configure::read('ClassesType.Temporary'),     //臨時品
              Configure::read('ClassesType.ExShipping'),    //臨時出荷
              Configure::read('ClassesType.ExNoStock'),     //臨時非在庫品
            ),
            //受領時に売上で設定する取引区分
            'Receipts' => array(
              Configure::read('ClassesType.Temporary'),     //臨時品
              Configure::read('ClassesType.ExNoStock'),     //臨時非在庫品
            ),
      );
    }

    /**
     * 臨時品かどうかのチェック
     * @param $parent 比較対象とする区分
     * @param $type   区分値
     * @return true : 臨時品である、false : 臨時品ではない
     */
    function isInclude($parent=null, $type=null){
        //比較対象の決定
        if($parent == null || !isset($this->item_types[$parent])){
            return false;
        }
        if(in_array($type, $this->item_types[$parent])){
            return true;
        } else {
            return false;
        }
    }

    /**
     * 売上データを作成するかどうかのチェック
     * @param $parent     比較対象とする区分
     * @param $type       区分値
     * @param $claim_type 臨時品売上タイプ
     * @return true : 臨時品である、false : 臨時品ではない
     */
    function isClaim($parent = null, $type=null, $claim_type=null){
        if($parent == null || $type == null || $claim_type == null){
            return false;
        }

        //臨時品?
        if(!$this->isInclude($parent, $type)){
            return false;
        }

        //呼び出し元で作成タイミングが変わる
        switch ($parent) {
          case 'Shippings':
          case 'Receipts':
            //出荷、受領の場合は「払出時」
            if($claim_type == "0"){
                return true;
            }
            break;
          case 'consumes':
            //消費の場合は「消費時」
            if($claim_type == "1"){
                return true;
            }
            break;
          default:
            return false;
            break;
        }

        return false;
    }

}