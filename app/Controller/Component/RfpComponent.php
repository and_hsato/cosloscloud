<?php
class RfpComponent extends Component {
    
    const TEMPLATE_FILE_RFP = 'template.xls';
    
    var $_controller;
    
    public function startup(&$controller) {
        $this->_controller = $controller;
    }
    
    /**
     * 見積依頼実行時の一連のトランザクションを実行する。
     *  - 見積依頼ヘッダ登録
     *  - 見積依頼明細登録
     *  - 見積依頼先情報登録
     *  - 見積依頼先メール送信
     *  - 見積依頼ファイル保存(Excel形式)
     *
     * @param mixed $requestData 出力対象のキーとなるTrnOrderHeaderのIDの配列。
     * @return mixed ModelのfindAllのリターン値と同等。データがない場合はfalseを返す。
     * 
     * 以下、$requestData内にセットするキーの説明
     *    'login_user_id' (number)  : ログイン中のユーザID(MstUserの主キー)
     *    'work_no' (string)        : ヘッダと明細を紐付けるための一意なキー
     *    'recital' (string)        : 見積依頼の備考
     *    'limit_date'(string)      : 見積回答の期限
     *    'grandmaster_ids' (array) : 見積依頼対象の商品ID(MstKyomaterialの主キー)の配列
     *    'edi_ids' (array)         : 見積依頼先の業者ID(MstFacilityの主キー)の配列
     *    'with_send_mail' (boolean): 見積依頼先の業者へメール送信をするかどうか
     */
    public function doRfpTransaction($requestData) {
        // 各種共通データ生成
        $loginUserId = $requestData['login_user_id'];
        $workNo = $requestData['work_no'];
        $workDate = date('Y/m/d');
        $now = date('Y/m/d H:i:s');
        $trnData = array();
        
        // トランザクション開始
        // 途中でエラーとなった場合は、全ての更新をロールバックして終了する
        $transaction = $this->_controller->TransactionManager->begin();
        try {
            // 見積依頼ヘッダ
            $trnRfpHeader = array(
                    'work_no'      => $workNo,
                    'work_date'    => $workDate,
                    'recital'      => $requestData['recital'],
                    'limit_date'   => $requestData['limit_date'],
                    'detail_count' => count($requestData['grandmaster_ids']),
                    'is_deleted'   => false,
                    'creater'      => $loginUserId,
                    'created'      => $now,
                    'modifier'     => $loginUserId,
                    'modified'     => $now,
            );
            $trnData['TrnRfpHeader'] = &$trnRfpHeader; // ※参照を格納
            
            // 見積依頼ヘッダ保存
            $this->_controller->TrnRfpHeader->create();
            $ret = $this->_controller->TrnRfpHeader->save($trnRfpHeader);
            if (empty($ret)) throw new Exception('TrnRfpHeader save error.');
            $trnRfpHeader['id'] = $this->_controller->TrnRfpHeader->getLastInsertID();
            
            // 見積依頼明細
            $trnRfps = array();
            $trnData['TrnRfp'] = &$trnRfps; // ※参照を格納
            foreach ($requestData['grandmaster_ids'] as $i => $grandmasterId) {
                $recital = @$requestData['recitals'][$grandmasterId][recital];
                $trnRfp = array(
                        'work_no'            => $workNo,
                        'work_seq'           => $i + 1,
                        'work_date'          => $workDate,
                        'work_type'          => null,
                        'recital'            => $recital,
                        'mst_item_unit_id'   => 0, // 現在未使用
                        'mst_grandmaster_id' => $grandmasterId,
                        'price'              => 0,
                        'supplier_id'        => 0,
                        'trn_rfp_header_id'  => $trnRfpHeader['id'],
                        'trn_proposal_id'    => null,
                        'is_deleted'         => false,
                        'creater'            => $loginUserId,
                        'created'            => $now,
                        'modifier'           => $loginUserId,
                        'modified'           => $now,
                );
                
                // 見積依頼明細保存
                $this->_controller->TrnRfp->create();
                $ret = $this->_controller->TrnRfp->save($trnRfp);
                if (empty($ret)) throw new Exception('TrnRfp save error.');
                $trnRfp['id'] = $this->_controller->TrnRfp->getLastInsertID();
                $trnRfps[] = $trnRfp;
            }
            
            // 見積依頼先情報
            $trnRequestedRfps = array();
            $trnData['TrnRequestedRfp'] = &$trnRequestedRfps; // ※参照を格納
            foreach ($requestData['edi_ids'] as $i => $facilityId) {
                $trnRequestedRfp = array(
                        'trn_rfp_header_id' => $trnRfpHeader['id'],
                        'mst_facility_id'   => $facilityId,
                        'is_deleted'        => false,
                        'creater'           => $loginUserId,
                        'created'           => $now,
                        'modifier'          => $loginUserId,
                        'modified'          => $now,
                );
                
                // 見積依頼先情報保存
                $this->_controller->TrnRequestedRfp->create();
                $ret = $this->_controller->TrnRequestedRfp->save($trnRequestedRfp);
                if (empty($ret)) throw new Exception('TrnRequestedRfp save error.');
                $trnRequestedRfp['id'] = $this->_controller->TrnRequestedRfp->getLastInsertID();
                $trnRequestedRfps[] = $trnRequestedRfp;
            }
            
            // 見積依頼書(Excel形式)を作成
            $ret = $this->createRfplFile($trnRfpHeader);
            if (empty($ret)) throw new Exception('createExcelDataForRfp Error.');
            
            // メール送信
            if (!empty($requestData['with_send_mail'])) {
                $ret = $this->sendRfpMail($requestData['edi_ids'], $trnRfpHeader);
                if (empty($ret)) throw new Exception('sendRfpMail Error.');
            }
            
            $this->_controller->TransactionManager->commit($transaction);
            return $trnData;
        } catch (Exception $e) {
            $this->_controller->TransactionManager->rollback($transaction);
            $this->_controller->log($e->getMessage());
            $this->_controller->log(print_r($trnData, true));
            $this->deleteRfpFile($trnRfpHeader);
            return null;
        }
    }
    
    public function findRfpItems($trnRfpHeader) {
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                'fields' => array(
                        '"TrnRfp"."work_seq"               AS "RfpItem__work_seq"',      // 作業連番
                        '"TrnRfp"."recital"                AS "RfpItem__recital"',       // 備考
                        '"MstGrandKyoMaterial"."matcd"     AS "RfpItem__master_id"',     // マスターID(コスロスID)
                        '"MstFacilityItem"."internal_code" AS "RfpItem__internal_code"', // 商品ID
                        '"MstFacilityItem"."item_code"     AS "RfpItem__item_code"',     // 商品コード
                        '"MstGrandKyoMaterial"."sizdimnam" AS "RfpItem__standard"',      // 規格
                        '"MstGrandKyoMaterial"."stdjancd"  AS "RfpItem__jan_code"',      // JANコード
                        '"MstGrandKyoMaterial"."makcocd"   AS "RfpItem__dealer_name"',   // 販売元
                        self::getItemNameSqlFieldStr() . ' AS "RfpItem__item_name"',     // 商品名
                        self::getUnitNameSqlFieldStr() . ' AS "RfpItem__unit_name"',     // 包装単位
                ),
                // 検索条件
                'conditions' => array(
                        'TrnRfp.trn_rfp_header_id' => $trnRfpHeader['id'],
                        'TrnRfp.is_deleted' => false,
                ),
                // 連結するモデル
                'joins' => array(
                        array(
                                'table' => 'mst_grand_kyo_materials',
                                'alias' => 'MstGrandKyoMaterial',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstGrandKyoMaterial.id = TrnRfp.mst_grandmaster_id',
                                        'MstGrandKyoMaterial.is_deleted' => false,
                                ),
                        ),
                        array(
                                'table' => 'mst_facility_items',
                                'alias' => 'MstFacilityItem',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstFacilityItem.master_id = MstGrandKyoMaterial.matcd',
                                ),
                        ),
                        array(
                                'table' => 'mst_item_units',
                                'alias' => 'MstItemUnit',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstItemUnit.mst_facility_item_id = MstFacilityItem.id',
                                ),
                        ),
                        array(
                                'table' => 'mst_unit_names',
                                'alias' => 'MstUnitName',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstUnitName.id = MstItemUnit.mst_unit_name_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_unit_names',
                                'alias' => 'MstUnitName2',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstUnitName2.id = MstItemUnit.per_unit_name_id',
                                ),
                        ),
                ),
                // 並び順
                'order' => array(
                        'TrnRfp.work_seq' => 'ASC',
                ),
        );
        // 検索実行
        $data = $this->_controller->TrnRfp->find('all', $options);
    
        return $data;
    }
    
    /**
     * 指定された施設宛ての見積依頼ヘッダを取得する。
     *
     * @param number $facilityId 見積依頼先の施設ID(MstFacilityの主キー)。
     * @return mixed ModelのfindAllのリターン値と同等。
     * 
     */
    public function findRfpHeaderDataForFacility($facilityId) {
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                'fields' => array(
                        'trn_rfp_header_id',
                ),
                // 検索条件
                'conditions' => array(
                        'TrnRequestedRfp.mst_facility_id' => $facilityId,
                        'TrnRequestedRfp.is_deleted' => false,
                ),
        );
        // 検索実行
        $data = $this->_controller->TrnRequestedRfp->find('list', $options);
        $trnRfpHeaderIds = array_values($data);
        
        $today = date('Y-m-d');
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                'fields' => array(
                        '*',
                ),
                // 検索条件
                'conditions' => array(
                        'TrnRfpHeader.id' => $trnRfpHeaderIds,
                        "TrnRfpHeader.limit_date >= '$today'",
                        'TrnRfpHeader.is_deleted' => false,
                ),
                // 並び順
                'order' => array(
                        'TrnRfpHeader.limit_date' => 'ASC',
                        'TrnRfpHeader.created' => 'DESC',
                        'TrnRfpHeader.work_no' => 'DESC',
                ),
        );
        // 検索実行
        $data = $this->_controller->TrnRfpHeader->find('all', $options);
        
        return $data;
    }
    
    public function findEdiFacilities($myFacilityId) {
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                'fields' => array(
                        '*',
                ),
                // 検索条件
                'conditions' => array(
                        'MstFacility.facility_type' => Configure::read('FacilityType.supplier'),
                        'MstFacility.is_deleted' => false,
                ),
                // 連結するモデル
                'joins' => array(
                        array(
                                'table' =>  'mst_facility_relations',
                                'alias' => 'MstFacilityRelation',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstFacilityRelation.mst_facility_id' => $myFacilityId,
                                        'MstFacilityRelation.partner_facility_id = MstFacility.id',
                                        'MstFacilityRelation.is_deleted' => false,
                                ),
                        ),
                ),
                // 並び順
                'order' => array(
                        'MstFacility.id' => 'ASC',
                ),
        );
        
        // 検索実行
        $data = $this->_controller->MstFacility->find('all', $options);
        
        return $data;
    }
    
    /**
     * 指定されたTrnRfpHeaderの情報から見積依頼のExcelファイルを作成する。
     *
     * @param mixed $trnRfpHeader 見積依頼ヘッダーのデータ。
     * @return boolean Excelファイルの作成に成功した場合はtrue、失敗した場合はfalseを返す。
     */
    public function createRfplFile($trnRfpHeader) {
        $rfpItems = $this->findRfpItems($trnRfpHeader);
        if (empty($rfpItems)) return false;
        
        // Excelオブジェクト作成
        $templateFilePath = realpath(TMP) . DS . 'excel' . DS . self::TEMPLATE_FILE_RFP;
        $PHPExcel = $this->createDefaultExcelObject($templateFilePath);
    
        // Excel書込処理
        $PHPExcel->setActiveSheetIndex(0);  //一番左のシートをアクティブ化
        $sheet = $PHPExcel->getActiveSheet();
        foreach ($rfpItems as $i => $d) {
            $item = $d['RfpItem'];
            $rowNumber =  (string)($i + 4);
            $sheet->setCellValue        ("A" . $rowNumber, $item['work_seq']);                                           // No
            $sheet->setCellValueExplicit("B" . $rowNumber, $item['master_id'], PHPExcel_Cell_DataType::TYPE_STRING);     // マスターID
            $sheet->setCellValueExplicit("C" . $rowNumber, $item['internal_code'], PHPExcel_Cell_DataType::TYPE_STRING); // 商品ID
            $sheet->setCellValue        ("D" . $rowNumber, $item['item_name']);                                          // 商品名
            $sheet->setCellValue        ("E" . $rowNumber, $item['item_code']);                                          // 製品番号
            $sheet->setCellValue        ("F" . $rowNumber, $item['standard']);                                           // 規格
            $sheet->setCellValueExplicit("G" . $rowNumber, $item['jan_code'], PHPExcel_Cell_DataType::TYPE_STRING);      // JANコード
            $sheet->setCellValue        ("H" . $rowNumber, $item['unit_name']);                                          // 包装単位
            
            $sheet->getStyle("I" . $rowNumber)->getNumberFormat()->setFormatCode('¥#,##0.00'); // IDを非表示
            $sheet->getStyle("I" . $rowNumber)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF99');
            $sheet->getStyle('A' . $rowNumber . ':' . 'I' . $rowNumber)->getBorders()->getAllBorders()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
            $sheet->getStyle("I" . $rowNumber)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED); // 金額のセルのみロックしない
            
        }
        
        $sheet->setCellValue("A2" , $trnRfpHeader['id']);                                   // 内部IDをセット
        $sheet->getStyle("A2")->getNumberFormat()->setFormatCode(';;;');                    // 内部IDを非表示
        $sheet->setCellValue("I2" , date('Y/m/d', strtotime($trnRfpHeader['limit_date']))); // 締切日
        
        $sheet->getProtection()->setPassword('cosloscloud'); // シート保護のパスワードをセット
        $sheet->getProtection()->setSheet(true);             // シートをロック
        $sheet->getProtection()->setSort(true);              // ソートをロック
        $sheet->getProtection()->setInsertRows(true);        // 行追加をロック
        $sheet->getProtection()->setFormatCells(true);       // 書式をロック
        
        // Excelファイル保存
        $objWriter = new PHPExcel_Writer_Excel5($PHPExcel);   //2003形式で保存
        $filePath = self::getRfpAbsoluteFilePath($trnRfpHeader);
        $objWriter->save($filePath);
        return true;
    }
    
    /**
     * 指定されたTrnRfpHeaderの情報から作成された見積依頼のExcelファイルを削除する。
     *
     * @param mixed $trnRfpHeader 見積依頼ヘッダーのデータ。
     * @return boolean 成功した場合にtrueを、失敗した場合にfalseを返す。
     */
    public function deleteRfpFile($trnRfpHeader) {
        $path = $this->getRfpAbsoluteFilePath($trnRfpHeader);
        if (!is_writable($path)) return false;
        return unlink($path);
    }
    
    /**
     * 指定されたTrnRfpHeaderの情報からExcelファイルの絶対パスを返す。
     *
     * @param mixed $trnRfpHeader 見積依頼ヘッダーのデータ。
     * @return string Excelファイルの絶対パス文字列。
     */
    public function getRfpAbsoluteFilePath($trnRfpHeader) {
        $path = $this->getRfpRelativePath($trnRfpHeader, DS);
        $path = WWW_ROOT . $path;
        $path = mb_convert_encoding($path, 'sjis', 'utf-8');
        return $path;
    }
    
    /**
     * 指定されたTrnRfpHeaderの情報からExcelファイルのWEB-ROOTからの相対パスを返す。
     *
     * @param mixed $trnRfpHeader 見積依頼ヘッダーのデータ。
     * @param string $separator ディレクトリの区切り文字。
     * @return string ExcelファイルのWEB-ROOTからの相対パス文字列。
     */
    public function getRfpRelativePath($trnRfpHeader, $separator = '/') {
        $fileName = $this->getRfpFileName($trnRfpHeader);
        $path = 'rfp' . $separator . $fileName;
        return $path;
    }
    
    /**
     * 指定されたTrnRfpHeaderの情報からExcelファイルのファイル名を返す。
     *
     * @param mixed $trnRfpHeader 見積依頼ヘッダーのデータ。
     * @return string Excelファイルのファイル名。
     */
    public function getRfpFileName($trnRfpHeader) {
        $fileName = '見積依頼_' . $trnRfpHeader['work_no'] . '_' . date('YmdHis', strtotime($trnRfpHeader['created'])) . '.xls';
        return $fileName;
    }
    
    
    
    private function createDefaultExcelObject($templateFilePath) {
        // Excel出力用ライブラリ
        App::import('Vendor', 'PHPExcel', array('file'=>'phpexcel' . DS . 'PHPExcel.php'));
        App::import('Vendor', 'PHPExcel_IOFactory', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'IOFactory.php'));
        App::import('Vendor', 'PHPExcel_Cell_AdvancedValueBinder', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'Cell' . DS . 'AdvancedValueBinder.php'));
        
        // Excel95用ライブラリ
        App::import('Vendor', 'PHPExcel_Writer_Excel5', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'Writer' . DS . 'Excel5.php'));
        App::import('Vendor', 'PHPExcel_Reader_Excel5', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'Reader' . DS . 'Excel5.php'));
        try{
            $objReader = PHPExcel_IOFactory::createReader("Excel5");
            $PHPExcel = $objReader->load($templateFilePath);
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return null;
        }
        return $PHPExcel;
    }
    
    private static function getUnitNameSqlFieldStr() {
        $sql = '';
        $sql .= '(';
        $sql .= '  CASE';
        $sql .= '    WHEN "MstFacilityItem"."id" IS NULL THEN "MstUnitName"."unit_name"';
        $sql .= '    WHEN "MstItemUnit"."per_unit" = 1 THEN "MstUnitName"."unit_name"';
        $sql .= '    ELSE "MstUnitName"."unit_name" || \'(\' || "MstItemUnit"."per_unit" || "MstUnitName2"."unit_name" || \')\'';
        $sql .= '  END';
        $sql .= ')';
        return $sql;
    }
    
    private static function getItemNameSqlFieldStr() {
        $sql = '';
        $sql .= '(';
        $sql .= '  CASE';
        $sql .= '    WHEN "MstFacilityItem"."item_name" IS NULL THEN "MstGrandKyoMaterial"."matname"';
        $sql .= '    ELSE "MstFacilityItem"."item_name"';
        $sql .= '  END';
        $sql .= ')';
        return $sql;
    }
    
    private function sendRfpMail($ediIds, $trnRfpHeader) {
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                'fields' => array(
                        '*',
                ),
                // 検索条件
                'conditions' => array(
                        'MstFacility.id' => $ediIds,
                ),
        );
        // 検索実行
        $data = $this->_controller->MstFacility->find('all', $options);
        
        $filePath = $this->getRfpAbsoluteFilePath($trnRfpHeader);
        if (!is_readable($filePath)) {
            $this->log('SendRfpMail: Can not read file.: ' . $filePath);
            return false;
        }
        
        $fileName = $this->getRfpFileName($trnRfpHeader);
        foreach ($data as $d) {
            $supplier_email = $d['MstFacility']['email'];
            if (empty($supplier_email)) continue;
            $supplier_name = $d['MstFacility']['facility_formal_name'];
            $mailParams = array(
                    MailComponent::PARAM_CONFIG       => 'and',
                    MailComponent::PARAM_TO           => array($supplier_email,),
                    MailComponent::PARAM_ATTACHMENTS  => array($fileName => array('file' => $filePath,),),
                    MailComponent::PARAM_VIEW_VARS    => array('supplier_name' => $supplier_name),
            );
            if ($this->_controller->Mail->sendRfpMail($mailParams)) {
                // 成功時の処理
            } else {
                // 失敗時の処理
            }
        }
        return true;
    }

}