<?php

class RetroactUtilsComponent extends Component {
    
    /**
     *
     * @var array $components
     */
    var $components = array('Session','Common');
    
    /**
     *
     * @var mixed $c
     */
    var $c;
    
    /**
     * This component's startup method.
     * @access public
     * @param $controller
     */
    function startup(&$controller) {
        $this->c =& $controller;
    }
    
    
    /**
     * Getting facility id with facility code.
     * @param string $_facility_code
     * @return integegr MstDepartment.id
     */  
    public function getSupplierDepartmentIdWithFacilityCode ($_facility_code = null) {
        $_params = $_aSupplier = $_aSupplierDepartment = array();
        $_params['fields'] = 'Supplier.id';
        $_params['conditions'] = array('Supplier.facility_code'=>$_facility_code,
                                       'Supplier.is_deleted'=>false);
        $_params['recursive'] = -1;
        $_aSupplier = $this->c->Supplier->find('first', $_params);
        $_params = array();
        $_params['fields'] = 'MstDepartment.id';
        $_params['conditions'] = array('MstDepartment.mst_facility_id'=>$_aSupplier['Supplier']['id'],
                                       'MstDepartment.is_deleted'=>false);
        $_params['recursive'] = -1;
        $_aSupplierDepartment = $this->c->MstDepartment->find('first', $_params);    
        return $_aSupplierDepartment['MstDepartment']['id'];
    }
    
    /**
     * Getting Hospital id with facility code.
     * @param string $_hospital_code
     * @return array $_ids
     */  
    public function getHospitalDepartmentIdWithFacilityCode ($_hospital_code = null) {
        $_params = $_aHospital = $_aHospitalDepartments = $_ids = array();
        $_params['fields'] = 'Hospital.id';
        $_params['conditions'] = array('Hospital.facility_code'=>$_hospital_code,
                                       'Hospital.is_deleted'=>false);
        $_params['recursive'] = -1;
        $_aHospital = $this->c->Hospital->find('first', $_params);
        $_params = array();
        $_params['fields'] = 'MstDepartment.id';
        $_params['conditions'] = array('MstDepartment.mst_facility_id'=>$_aHospital['Hospital']['id'],
                                       'MstDepartment.is_deleted'=>false);
        $_params['recursive'] = -1;
        $_aHospitalDepartments = $this->c->MstDepartment->find('all', $_params);
        foreach ($_aHospitalDepartments as $data) {
            $_ids[] = $data['MstDepartment']['id'];
        }
        return $_ids;
    }
}
?>
