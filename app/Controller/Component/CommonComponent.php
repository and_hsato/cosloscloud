<?php
/**
 * common.php
 * Common Component
 * @version 1.0.0
 * @since 2010/07/29
 */
class CommonComponent extends Component
{
    
    /**
     * Usage:
     * 1. set component name to this controller's $components variable.
     *
     * var $components = array('Common'... etc);
     * var $helpers = array('Common', ...etc);
     *
     * 2. call function underbelow in view file.
     * 
     * $this->common->datereformat();
     */
    
    /**
     *
     * @var mixed $c
     */
    var $c;
    
    /**
     * This component's startup method.
     * @access public
     * @param $controller
     */
    function startup(&$controller) {
        $this->c =& $controller;
    }
    
    /**
     * Re:Formatting date type data.
     * @param $val
     */
    public function datereformat($val)
      {
          return vsprintf('%d/%02d/%02d', sscanf($val, '%d-%d-%d'));
      }
    
    /**
     * 包装単位名を取得
     * @param string $unitName
     * @param string $parUnit
     * @param string $parUnitName
     * @access private
     * @return string
     */
    public function getPackingName($unitName,$parUnit,$parUnitName){
        if((integer)$parUnit === 1){
            return $unitName;
        }else{
            return sprintf('%s（%s%s）',$unitName,$parUnit,$parUnitName);
        }
    }
    
    /**
     * Check the max length of value is greater or not greater than max value.
     * @param string $value
     * @return bool
     */
    public function isMax ($value, $max) {
        return (strlen($value) < $max) ? true:false;
    }
    
    /**
     * Check the minor length of value is less or not less than min value.
     * @param string $value
     * @return bool
     */
    public function isMin ($value, $min) {
        return (strlen($value) > $min) ? true:false;
    }
    
    /**
     * Check the value is Alphabetical or Numeric characters.
     * @param string $value
     * @return bool
     */
    public function isAlphaNumeric ($value) {
        return (preg_match("/^[a-zA-Z0-9]+$/", $value)) ? true:false;
    }
    
    /**
     * Check the value is numeric value.
     * @param string $value
     * @return bool
     */
    public function isNumeric ($value) {
        return (preg_match("/^[0-9]+$/", $value)) ? true:false;
    }
    
    /** CHECKING FOR MULTI-BYTES STRINGS                   JAPANESE ONLY */
    
    /**
     * Check the value is Japanese Hankaku Moji-retsu.
     * @param string $value
     * @return bool
     */
    public function isHankakuKanna ($value) {
        return (preg_match("/(?:\xEF\xBD[\xA1-\xBF]|\xEF\xBE[\x80-\x9F])/", $value)) ? true:false;
    }
    
    /**
     * Check the value is Japanese Zenkaku Moji-retsu.
     * @param string $value
     * @return bool
     */
    public function isZenkaku ($value) {
        return (preg_match("/^[\x7F-\xFF]+$/", $value)) ? true:false;
    }
    
    /**
     * Check the value is Japanese Zenkaku Kanna moji-retsu.
     * @param string $value
     * @return bool
     */
    public function isZenkakuKanna ($value) {
        return (preg_match("/^(?:\xE3\x82[\xA1-\xBF]|\xE3\x83[\x80-\xB6])+$/", $value)) ? true:false;
    }
    
    /**
     * Check the value is Japanese Zenkaku Canna character.
     * @param string $value
     * @return bool
     */
    public function isZenkakuCanna ($value) {
        return (preg_match("/^(?:\xE3\x81[\x81-\xBF]|\xE3\x82[\x80-\x93])+$/", $value)) ? true:false;
    }
    
    /**
     * ONLY UTF-8
     * Check the value is Zenkaku Alphabetical or Numeric character.
     * @param string $value
     * @return bool
     */
    public function isZenkakuAlphaNumeric ($value) {
        $value = mb_convert_encoding($value, "UTF-8");
        return (preg_match("/^[０-９ａ-ｚＡ-Ｚ]+$/", $value)) ? true:false;
    }
    
    /**
     * ONLY UTF-8
     * Check the value is only Zenkaku Alphabetical character.
     * @param string $value
     * @return bool
     */
    public function isZenkakuAlphabetical ($value) {
        $value = mb_convert_encoding($value, "UTF-8");
        return (preg_match("/^[ａ-ｚＡ-Ｚ]+$/", $value)) ? true:false;
    }
    
    /**
     * ONLY UTF-8
     * Check the value is only Zenkaku Numeric character.
     * @param string $value
     * @return bool
     */
    public function isZenkakuNumeric ($value) {
        $value = mb_convert_encoding($value, "UTF-8");
        return (preg_match("/^[０-９]+$/", $value)) ? true:false;
    }
    
    
    /**
     * Returning string value with percent quotes.
     * @param string $str
     * @return string
     */
    public function sql_like_quote($str) {
        return '%'. $str. '%';
    }
    
    
    public function price_format($price){
        if(ereg('\.00$', $price)){
            $format_price = number_format($price , 0);
        }else if(ereg('\.[0-9]0$', $price)){
            $format_price = number_format($price , 1);
        }else{
            $format_price = number_format($price , 2);
        }
        
        return $format_price;
    }
}
// EOF