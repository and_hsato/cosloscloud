<?php
class MailComponent extends Component {
    
    const PARAM_CONFIG       = 'config';
    const PARAM_SUBJECT      = 'subject';
    const PARAM_FROM         = 'from';
    const PARAM_TO           = 'to';
    const PARAM_CC           = 'cc';
    const PARAM_BCC          = 'bcc';
    const PARAM_ATTACHMENTS  = 'attachments';
    const PARAM_EMAIL_FORMAT = 'emailFormat';
    const PARAM_TEMPLATE     = 'template';
    const PARAM_LAYOUT       = 'layout';
    const PARAM_VIEW_VARS    = 'viewVars';
    
    var $_controller;
    
    public function startup(&$controller) {
        $this->_controller = $controller;
    }
    
    /**
     * パラメータに指定された情報を使用して発注書のメール送信を行う。
     *
     * @param mixed $params constで定義されているPARAM_TOやPARAM_ATTACHMENTSのパラメータ。CakeEmailのAPI仕様に準拠。
     * @return boolean true:成功/false:失敗
     * @link http://book.cakephp.org/2.0/ja/core-utility-libraries/email.html
     */
    public function sendOrderMail(&$params) {
        if (empty($params[self::PARAM_SUBJECT])) {
            $params[self::PARAM_SUBJECT] = '【Coslos Cloud】発注書送信';
        }
        if (empty($params[self::PARAM_TEMPLATE])) {
            $params[self::PARAM_TEMPLATE] = 'order_mail';
        }
        if (empty($params[self::PARAM_LAYOUT])) {
            $params[self::PARAM_LAYOUT] = 'common_mail';
        }
        return $this->sendMail($params);
    }
    
    /**
     * パラメータに指定された情報を使用して見積依頼のメール送信を行う。
     *
     * @param mixed $params constで定義されているPARAM_TOやPARAM_ATTACHMENTSのパラメータ。CakeEmailのAPI仕様に準拠。
     * @return boolean true:成功/false:失敗
     * @link http://book.cakephp.org/2.0/ja/core-utility-libraries/email.html
     */
    public function sendRfpMail(&$params) {
        if (empty($params[self::PARAM_SUBJECT])) {
            $params[self::PARAM_SUBJECT] = '【Coslos Cloud】見積依頼送信';
        }
        if (empty($params[self::PARAM_TEMPLATE])) {
            $params[self::PARAM_TEMPLATE] = 'rfp_mail';
        }
        if (empty($params[self::PARAM_LAYOUT])) {
            $params[self::PARAM_LAYOUT] = 'common_mail';
        }
        return $this->sendMail($params);
    }
    
    /**
     * パラメータに指定された情報を使用して問い合わせのメール送信を行う。
     *
     * @param mixed $params constで定義されているPARAM_TOやPARAM_ATTACHMENTSのパラメータ。CakeEmailのAPI仕様に準拠。
     * @return boolean true:成功/false:失敗
     * @link http://book.cakephp.org/2.0/ja/core-utility-libraries/email.html
     */
    public function sendInquiryMail(&$params, $ticketId) {
        if (empty($params[self::PARAM_SUBJECT])) {
            $params[self::PARAM_SUBJECT] = "【Coslos Cloud 相談番号: ${ticketId}】お問い合わせ受付";
        }
        if (empty($params[self::PARAM_TEMPLATE])) {
            $params[self::PARAM_TEMPLATE] = 'inquiry_mail';
        }
        if (empty($params[self::PARAM_LAYOUT])) {
            $params[self::PARAM_LAYOUT] = 'common_mail';
        }
        $params[self::PARAM_VIEW_VARS]['ticket_id'] = $ticketId;
        return $this->sendMail($params);
    }
    
    /**
     * パラメータに指定された情報を使用してベンチマーク登録依頼のメール送信を行う。
     *
     * @param mixed $params constで定義されているPARAM_TOやPARAM_ATTACHMENTSのパラメータ。CakeEmailのAPI仕様に準拠。
     * @return boolean true:成功/false:失敗
     * @link http://book.cakephp.org/2.0/ja/core-utility-libraries/email.html
     */
    public function sendBenchMarksMail(&$params) {
        if (empty($params[self::PARAM_SUBJECT])) {
            $params[self::PARAM_SUBJECT] = "【Coslos Cloud】メッカル医療材料ベンチマークのお申し込みを受け付けました。";
        }
        if (empty($params[self::PARAM_TEMPLATE])) {
            $params[self::PARAM_TEMPLATE] = 'benchmarks_mail';
        }
        if (empty($params[self::PARAM_LAYOUT])) {
            $params[self::PARAM_LAYOUT] = 'common_mail';
        }
        return $this->sendMail($params);
    }


    private function sendMail(&$params) {
        App::uses('CakeEmail', 'Network/Email');
        try {
            $email = !empty($params[self::PARAM_CONFIG]) ? new CakeEmail($params[self::PARAM_CONFIG]) : new CakeEmail();
            if (!empty($params[self::PARAM_SUBJECT])) {
                $email->subject($params[self::PARAM_SUBJECT]);
            }
            if (!empty($params[self::PARAM_TO])) {
                $email->to($params[self::PARAM_TO]);
            }
            if (!empty($params[self::PARAM_CC])) {
                $email->cc($params[self::PARAM_CC]);
            }
            if (!empty($params[self::PARAM_BCC])) {
                $email->bcc($params[self::PARAM_BCC]);
            }
            if (!empty($params[self::PARAM_ATTACHMENTS])) {
                $email->attachments($params[self::PARAM_ATTACHMENTS]);
            }
            if (!empty($params[self::PARAM_EMAIL_FORMAT])) {
                $email->emailFormat($params[self::PARAM_EMAIL_FORMAT]);
            }
            if (!empty($params[self::PARAM_VIEW_VARS])) {
                $email->viewVars($params[self::PARAM_VIEW_VARS]);
            }
            $template = !empty($params[self::PARAM_TEMPLATE]) ? $params[self::PARAM_TEMPLATE] : null;
            $layout = !empty($params[self::PARAM_LAYOUT]) ? $params[self::PARAM_LAYOUT] : null;
            if ($template || $layout) {
                $email->template($template, $layout);
            }
            $email->send();
            return true;
        } catch (Exception $e) {
            $this->_controller->log($e->getMessage());
            return false;
        }
    }
}