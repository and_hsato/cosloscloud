<?php
/**
 * 入庫共通処理
 * StorageComponent
 *
 * @since 2010/09/15
 */

class StorageComponent extends Component {
    var $components = array('Session');

    private $TrnStock;
    private $MstDepartment;
    private $TrnStorage;
    private $TrnStorageHeader;
    private $TrnReceivingHeader;
    private $TrnReceiving;
    private $TrnSticker;
    private $TrnStickerRecord;
    private $TrnStickerIssue;
    private $MstFacility;
    private $_controller;

    public function startup(&$controller){
        $this->_controller = $controller;
    }

    /**
     * 在庫情報検索
     * @param string $item_id 施設採用品のKey
     * @access public
     * @return array
     */
    public function getStocks($item_id, $per_unit ){
        $today = date('Y-m-d');
        $departmentCode = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'), Configure::read('DepartmentType.warehouse'));
        $sql =
            ' select ' .
                ' TrnStock.*'.
                ',MstFixedCount.trn_stock_id' .
                ',MstFixedCount.order_point' .
                ',MstFixedCount.fixed_count' .
                ',MstFixedCount.holiday_fixed_count' .
                ',MstFixedCount.spare_fixed_count' .
                ',MstFixedCount.mst_shelf_name_id' .
                ',MstFixedCount.trade_type' .
                ',MstFixedCount.substitute_unit_id' .
                ',MstItemUnit.mst_facility_item_id'.
                ',MstItemUnit.mst_facility_id'.
                ',MstItemUnit.per_unit'.
                ',MstItemUnit.mst_unit_name_id'.
                ',MstItemUnit.is_standard'.
                ',MstItemUnit.per_unit_name_id'.
                ',MstItemUnit.mst_facility_item_id'.
                ',MstItemUnit.id as mst_item_unit_id'.
                ',MstItemUnit.is_deleted as miu_is_deleted'.
                ',MstItemUnit.start_date as miu_start_date'.
                ',MstItemUnit.end_date as miu_end_date'.
                ',MstFacilityItem.mst_facility_id'.
                ',MstFacilityItem.internal_code'.
                ',MstFacilityItem.item_name'.
                ',MstFacilityItem.standard'.
                ',MstFacilityItem.item_code'.
                ',MstFacilityItem.jan_code'.
                ',MstFacilityItem.mst_unit_name_id'.
                ',MstFacilityItem.unit_price'.
                ',MstFacilityItem.price'.
                ',MstFacilityItem.refund_price'.
                ',MstFacilityItem.insurance_claim_type'.
                ',MstFacilityItem.insurance_claim_code'.
                ',MstFacilityItem.item_category1'.
                ',MstFacilityItem.item_category2'.
                ',MstFacilityItem.item_category3'.
                ',MstFacilityItem.biogenous_type'.
                ',MstFacilityItem.jmdn_code'.
                ',MstFacilityItem.class_separation'.
                ',MstFacilityItem.tax_type'.
                ',MstFacilityItem.spare_key1'.
                ',MstFacilityItem.spare_key2'.
                ',MstFacilityItem.spare_key3'.
                ',MstFacilityItem.spare_key4'.
                ',MstFacilityItem.spare_key5'.
                ',MstFacilityItem.spare_key6'.
                ',MstFacilityItem.converted_num'.
                ',MstFacilityItem.item_type'.
                ',MstFacilityItem.mst_owner_id'.
                ',MstFacilityItem.medical_code'.
                ',MstFacilityItem.mst_dealer_id'.
                ',MstFacilityItem.is_lowlevel'.
                ',MstFacilityItem.is_auto_storage'.
                ',MstDealer.dealer_name'.
                ',MstUnitName.unit_name as MstUnitName__unit_name'.
                ',MstParUnitName.unit_name as MstParUnitName__unit_name'.
                ",(case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstParUnitName.unit_name || ')' end ) as packing_name".
                ',MstShelfName.code as MstShelfName__code'.
                ',Owner.facility_name as owner_name'.
            ' from ' .
                ' trn_stocks as TrnStock '.
                ' left join mst_fixed_counts as MstFixedCount on TrnStock.id = MstFixedCount.trn_stock_id and MstFixedCount.is_deleted = false ' .
                ' left join mst_shelf_names as MstShelfName on MstFixedCount.mst_shelf_name_id = MstShelfName.id'.
                ' inner join mst_item_units MstItemUnit on TrnStock.mst_item_unit_id = MstItemUnit.id' .
                ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id '.
                ' left join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id' .
                ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id'.
                ' inner join mst_unit_names as MstParUnitName on MstItemUnit.per_unit_name_id = MstParUnitName.id'.
                ' inner join mst_departments as MstDepartment on TrnStock.mst_department_id = MstDepartment.id ' .
                ' inner join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id' .
                ' left join mst_facilities as Owner on MstFacilityItem.mst_owner_id = Owner.id'.
            ' where ' .
                " 1 = 1 " .
                " and TrnStock.mst_department_id = {$departmentCode}".
                " and MstItemUnit.per_unit <= {$per_unit}".
                " and MstFacilityItem.id = {$item_id}".
                " and MstItemUnit.is_deleted = false ".
            ' order by ' .
                ' MstItemUnit.per_unit desc , TrnStock.id asc';
        $_result = array();
        $_res = $this->_controller->TrnStock->query($sql , false);
        foreach($_res as $_row){
            $_r = array();
            array_walk_recursive($_row, array('StorageComponent','BuildSearchResult'), &$_r);
            $_r['promisable_count'] = (integer)$_row[0]['stock_count'] - (integer)$_row[0]['reserve_count'] - (integer)$_row[0]['promise_count'];
            $_r['shelf_name'] = $_row['mstshelfname']['code'];
            $_r['max_creatable_count'] = floor((integer)$per_unit / (integer)$_row[0]['per_unit']);

            //包装単位のチェック
            if($_row[0]['miu_is_deleted'] == TRUE){
              $_r['is_right_term_pack'] = FALSE;
            }else{
              if( ($_row[0]['miu_start_date'] <= $today) && (($_row[0]['miu_end_date'] >= $today) || (is_null($_row[0]['miu_end_date']) ) ) ){
                $_r['is_right_term_pack'] = TRUE;
              }else{
                $_r['is_right_term_pack'] = FALSE;
              }
            }
            $_result[] = $_r;
        }
        return $_result;
    }

    /**
     * 部署コードを取得
     * @param string $facilityId
     * @param string $type
     * @return string
     */
    public function getDepartmentId($facilityId, $type){
        $_res = $this->_controller->MstDepartment->find('first', array(
            'conditions' => array(
                'mst_facility_id' => $facilityId,
                'department_type' => $type,
            ),
        ));
        return $_res['MstDepartment']['id'];
    }

    /**
     * 入庫関係のデータ作成処理
     * @param array $receiving
     * @param array $stocks
     * @param void $params
     */
    public function createStorages($receiving, $stocks, $params){

        //在庫情報がセットされているかの確認
        if(empty($stocks)){
            $this->_controller->Session->setFlash('在庫情報が取得できませんでした。最初からやり直してください。' , 'growl', array('type'=>'error'));
            if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                $this->_controller->redirect('/storages_select');
            }else{
                $this->_controller->set('isError' , true);
                $this->_controller->redirect('/storages');
            }
            return;
        }

        $now = date('Y/m/d H:i:s.u');
        $this->_controller->TrnStorage->begin();

        //行ロック
        $sql  = ' select ';
        $sql .= '       *  ';
        $sql .= '   from ';
        $sql .= '     trn_receivings as a  ';
        $sql .= '   where ';
        if(is_array($receiving['id'])){
            $sql .= '     a.id in (' . join(',' ,  $receiving['id'] ). ') for update ';
        }else{
            $sql .= '     a.id = ' . $receiving['id'] . ' for update ';
        }
        $ret = $this->_controller->TrnReceiving->query($sql , false);

        //更新時間チェック
        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     trn_receivings as a  ';
        $sql .= '   where ';
        if(is_array($receiving['id'])){
            $sql .= '     a.id in (' . join(',' ,  $receiving['id'] ). ')';
        }else{
            $sql .= '     a.id = ' . $receiving['id'] ;
        }
        $sql .= "     and a.modified > '" . $params['time'] ."'";

        $ret = $this->_controller->TrnReceiving->query($sql , false);

        if($ret[0][0]['count']> 0 ){
            $this->_controller->TrnStorage->rollback();
            $this->_controller->Session->setFlash('ほかユーザによって更新されています。最初からやり直してください。' , 'growl', array('type'=>'error'));
            if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                $this->_controller->redirect('/storages_select');
            }else{
                $this->_controller->set('isError' , true);
                $this->_controller->redirect('/storages');
            }
            return;
        }

        $_dep = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'), Configure::read('DepartmentType.warehouse'));

        //入荷明細更新
        if( !$this->_controller->TrnReceiving->updateAll(array(
            'TrnReceiving.remain_count'     => "TrnReceiving.remain_count - {$receiving['storage_quantity']}",
            'TrnReceiving.receiving_status' => sprintf("(case when TrnReceiving.remain_count = {$receiving['storage_quantity']} then %s else %s end)",  Configure::read('ReceivingStatus.stock') , Configure::read('ReceivingStatus.partStocked')),
            'TrnReceiving.modifier'         => $this->Session->read('Auth.MstUser.id'),
            'TrnReceiving.modified'         => "'".$now."'",
            ),array(
                'TrnReceiving.id' => $receiving['id'],
                ),-1)) {

            $this->_controller->TrnStorage->rollback();
            $this->_controller->Session->setFlash('233:入荷明細の更新に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
            if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                $this->_controller->redirect('/storages_select');
            }else{
                $this->_controller->set('isError' , true);
                $this->_controller->redirect('/storages');
            }
            return;
        }

           //入荷ヘッダ
        $_cnt = $this->_controller->TrnReceiving->find('count', array(
            'conditions' => array(
                'trn_receiving_header_id' => $receiving['trn_receiving_header_id'],
                'receiving_status <> '    => Configure::read('ReceivingStatus.stock'),
            ),
            'recursive' => -1,
        ));
        if($_cnt === 0){
            if( !$this->_controller->TrnReceivingHeader->updateAll(array(
                'TrnReceivingHeader.receiving_status' => Configure::read('ReceivingStatus.stock'),
                'TrnReceivingHeader.modifier'         => $this->Session->read('Auth.MstUser.id'),
                'TrnReceivingHeader.modified'         => "'".$now."'",
            ),array(
                'TrnReceivingHeader.id' => $receiving['trn_receiving_header_id'],
                ),-1)){

                $this->_controller->TrnStorage->rollback();
                $this->_controller->Session->setFlash('入荷ヘッダの更新に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
                if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                    $this->_controller->redirect('/storages_select');
                }else{
                    $this->_controller->set('isError' , true);
                    $this->_controller->redirect('/storages');
                }
                return;
            }
        }else{
            if(!$this->_controller->TrnReceivingHeader->updateAll(array(
                'TrnReceivingHeader.receiving_status' => Configure::read('ReceivingStatus.partStocked'),
                'TrnReceivingHeader.modifier'         => $this->Session->read('Auth.MstUser.id'),
                'TrnReceivingHeader.modified'         => "'".$now."'",
                ),array(
                    'TrnReceivingHeader.id' => $receiving['trn_receiving_header_id'],
                    ),-1)){
                $this->_controller->TrnStorage->rollback();
                $this->_controller->Session->setFlash('入荷ヘッダの更新に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
                if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                    $this->_controller->redirect('/storages_select');
                }else{
                    $this->_controller->set('isError' , true);
                    $this->_controller->redirect('/storages');
                }
                return;
            }
        }

        $_workDate = date('Y/m/d');
        $_workNo = $this->_controller->setWorkNo4Header($_workDate, '10');

        $_roundType = $this->_controller->getRoundType($this->Session->read('Auth.facility_id_selected'));
        $_stickers = array();
        $_biogenous_types = Configure::read('FacilityItems.biogenous_types');
        $_biogenous_types[''] = '';
        $_facility_code = $this->_controller->getFacilityCode($this->Session->read('Auth.facility_id_selected'));

        $_detail_count = 0;
        foreach($stocks as $_row){
            //シール発行
            for($i = 0;$i < (integer)$_row['input_quantity'];$i++){
                $_detail_count++;
                $_stickerNo = $this->_controller->getCenterStickerNo($_workDate, $_facility_code);
                $this->_controller->TrnSticker->create(false);
                if(!$this->_controller->TrnSticker->save(array(
                    'facility_sticker_no' => $_stickerNo,
                    'mst_item_unit_id'    => $_row['mst_item_unit_id'],
                    'mst_department_id'   => $_dep,
                    'trade_type'          => Configure::read('ClassesType.Constant'),
                    'quantity'            => 1,
                    'lot_no'              => trim($params['lot_no']),
                    'validated_date'      => $this->_controller->getFormatedValidateDate($params['validated_date']),
                    'original_price'      => $receiving['stocking_price'],
                    'transaction_price'   => $this->getTransactionPrice($receiving['per_unit'], $receiving['stocking_price'], $_row['per_unit'], $_roundType),
                    'trn_order_id'        => $receiving['trn_order_id'],
                    'trn_receiving_id'    => $receiving['id'],
                    'buy_claim_id'        => $receiving['trn_claim_id'],
                    'is_deleted'          => false,
                    'has_reserved'        => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $now,
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $now,
                    'class_name'          => $receiving['class_name'],
                    'type_name'           => $receiving['type_name'],
                    ))){
                    $this->_controller->TrnStorage->rollback();
                    $this->_controller->Session->setFlash('シール情報の作成に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
                    if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                        $this->_controller->redirect('/storages_select');
                    }else{
                        $this->_controller->set('isError' , true);
                        $this->_controller->redirect('/storages');
                    }
                    return;
                }
                $_lastStickerId = $this->_controller->TrnSticker->getLastInsertID();

                //入庫明細
                $this->_controller->TrnStorage->create(false);
                if(!$this->_controller->TrnStorage->save(array(
                    'id'                  => false,
                    'work_no'             => $_workNo,
                    'work_seq'            => $i + 1,
                    'work_date'           => $_workDate,
                    'recital'             => $params['recital'],
                    'storage_type'        => Configure::read('StorageType.normal'),
                    'storage_status'      => Configure::read('StorageStatus.arrival'),
                    'mst_item_unit_id'    => $_row['mst_item_unit_id'],
                    'mst_department_id'   => $_dep,
                    'quantity'            => 1,
                    'trn_sticker_id'      => $_lastStickerId,
                    'trn_receiving_id'    => $receiving['id'],
                    'is_deleted'          => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $now,
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $now,
                    'facility_sticker_no' => $_stickerNo,
                    ))){
                    $this->_controller->TrnStorage->rollback();
                    $this->_controller->Session->setFlash('入庫明細の作成に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
                    if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                        $this->_controller->redirect('/storages_select');
                    }else{
                        $this->_controller->set('isError' , true);
                        $this->_controller->redirect('/storages');
                    }
                    return;
                }
                $_lastStorageId = $this->_controller->TrnStorage->getLastInsertID();

                //シール更新
                if(!$this->_controller->TrnSticker->updateAll(array(
                    'TrnSticker.trn_storage_id' => $_lastStorageId,
                ),array(
                    'TrnSticker.id' => $_lastStickerId,
                    ),-1)){
                    $this->_controller->TrnStorage->rollback();
                    $this->_controller->Session->setFlash('シール情報の更新に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
                    if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                        $this->_controller->redirect('/storages_select');
                    }else{
                        $this->_controller->set('isError' , true);
                        $this->_controller->redirect('/storages');
                    }
                    return;
                }

                //シール発行履歴
                if($receiving['trn_order_id'] != ''){
                    $this->_controller->TrnStickerRecord->create(false);
                    if(!$this->_controller->TrnStickerRecord->save(array(
                        'id'                  => false,
                        'move_date'           => $receiving['order_work_date'],
                        'move_seq'            => $this->_controller->getNextRecord($_lastStickerId),
                        'record_type'         => Configure::read('RecordType.order'),
                        'record_id'           => $_lastStorageId,
                        'trn_sticker_id'      => $_lastStickerId,
                        'facility_sticker_no' => $_stickerNo,
                        'mst_department_id'   => $_dep,
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        ))
                    ){

                        $this->_controller->TrnStorage->rollback();
                        $this->_controller->Session->setFlash('シール履歴の作成に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
                        if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                            $this->_controller->redirect('/storages_select');
                        }else{
                            $this->_controller->set('isError' , true);
                            $this->_controller->redirect('/storages');
                        }
                        return;
                    }
                }

                $this->_controller->TrnStickerRecord->create(false);
                if(!$this->_controller->TrnStickerRecord->save(array(
                    'id'                  => false,
                    'move_date'           => $receiving['work_date'],
                    'move_seq'            => $this->_controller->getNextRecord($_lastStickerId),
                    'record_type'         => Configure::read('RecordType.receiving'),
                    'record_id'           => $_lastStorageId,
                    'trn_sticker_id'      => $_lastStickerId,
                    'facility_sticker_no' => $_stickerNo,
                    'mst_department_id'   => $_dep,
                    'is_deleted'          => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $now,
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $now,
                    ))){
                    $this->_controller->TrnStorage->rollback();
                    $this->_controller->Session->setFlash('シール履歴の作成に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
                    if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                        $this->_controller->redirect('/storages_select');
                    }else{
                        $this->_controller->set('isError' , true);
                        $this->_controller->redirect('/storages');
                    }
                    return;
                }
                $this->_controller->TrnStickerRecord->create(false);
                if(!$this->_controller->TrnStickerRecord->save(array(
                    'id'                  => false,
                    'move_date'           => $_workDate,
                    'move_seq'            => $this->_controller->getNextRecord($_lastStickerId),
                    'record_type'         => Configure::read('RecordType.storage'),
                    'record_id'           => $_lastStorageId,
                    'trn_sticker_id'      => $_lastStickerId,
                    'facility_sticker_no' => $_stickerNo,
                    'mst_department_id'   => $_dep,
                    'is_deleted'          => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $now,
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $now,
                    ))){
                    $this->_controller->TrnStorage->rollback();
                    $this->_controller->Session->setFlash('シール履歴の作成に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
                    if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                        $this->_controller->redirect('/storages_select');
                    }else{
                        $this->_controller->set('isError' , true);
                        $this->_controller->redirect('/storages');
                    }
                    return;
                }

                //シール情報
                $_stickers[] = array(
                    '商品ID'         => $_row['internal_code'],
                    'シール番号'     => $_stickerNo,
                    '商品名'         => $_row['item_name'],
                    '規格'           => $_row['standard'],
                    '製品番号'     => $_row['item_code'],
                    'バーコード'     => $_stickerNo,
                    '保険請求'       => $_row['insurance_claim_code'],
                    '包装単位'       => $_row['packing_name'],
                    'ロット番号'     => $params['lot_no'],
                    'センター情報'   => $_row['owner_name'],
                    '販売元'         => $_row['dealer_name'],
                    '生物由来'       => (isset($_biogenous_types[$_row['biogenous_type']])) ? $_biogenous_types[$_row['biogenous_type']] : "",
                    'センター棚番号' => $_row['shelf_name'],
                    '定価'           => $this->getTransactionPrice($receiving['per_unit'], $receiving['stocking_price'], $_row['per_unit'], $_roundType),
                    '有効期限'       => $this->_controller->getFormatedValidateDate($params['validated_date']),
                );
            }

            //包装単位ID、部署IDが取得できていない場合エラーとしてロールバック
            if(is_null($_row['mst_item_unit_id']) || is_null($_row['mst_department_id'] || count($stocks) == 0 )){
                $this->_controller->TrnStorage->rollback();
                $this->_controller->Session->setFlash('入庫明細の作成に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
                if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                    $this->_controller->redirect('/storages_select');
                }else{
                    $this->_controller->set('isError' , true);
                    $this->_controller->redirect('/storages');
                }
                return;
            }

            //在庫テーブル更新
            //在庫△
            if(!$this->_controller->TrnStock->updateAll(array(
                'TrnStock.stock_count'     => "TrnStock.stock_count + {$_row['input_quantity']}",
                'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                'TrnStock.modified'        => "'".$now."'",
            ),array(
                'TrnStock.mst_item_unit_id'  => $_row['mst_item_unit_id'],
                'TrnStock.mst_department_id' => $_row['mst_department_id'],
                ),-1)){
                $this->_controller->TrnStorage->rollback();
                $this->_controller->Session->setFlash('在庫更新に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
                if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                    $this->_controller->redirect('/storages_select');
                }else{
                    $this->_controller->set('isError' , true);
                    $this->_controller->redirect('/storages');
                }
                return;
            }
        }

        //在庫テーブル更新
        //未入庫▽
        if(!$this->_controller->TrnStock->updateAll(array(
            'TrnStock.receipted_count' => "TrnStock.receipted_count - {$receiving['storage_quantity']}",
            'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
            'TrnStock.modified'        => "'".$now."'",
            ),array(
                'TrnStock.mst_item_unit_id'  => $receiving['mst_item_unit_id'],
                'TrnStock.mst_department_id' => $receiving['department_id_to'],
                ),-1)){
            $this->_controller->TrnStorage->rollback();
            $this->_controller->Session->setFlash('在庫更新に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
            if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                $this->_controller->redirect('/storages_select');
            }else{
                $this->_controller->set('isError' , true);
                $this->_controller->redirect('/storages');
            }
            return;
        }

        //入庫ヘッダ作成
        $this->_controller->TrnStorageHeader->create(false);
        if(!$this->_controller->TrnStorageHeader->save(array(
            'id'                      => false,
            'work_no'                 => $_workNo,
            'work_date'               => $_workDate,
            'recital'                 => $params['recital'],
            'storage_type'            => Configure::read('StorageType.normal'),
            'storage_status'          => Configure::read('StorageStatus.arrival'),
            'mst_department_id'       => $_dep,
            'detail_count'            => $_detail_count,
            'is_deleted'              => false,
            'creater'                 => $this->Session->read('Auth.MstUser.id'),
            'created'                 => $now,
            'modifier'                => $this->Session->read('Auth.MstUser.id'),
            'modified'                => $now,
            'trn_receiving_header_id' => $receiving['trn_receiving_header_id'],
            ))){
            $this->_controller->TrnStorage->rollback();
            $this->_controller->Session->setFlash('入庫ヘッダに失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
            if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                $this->_controller->redirect('/storages_select');
            }else{
                $this->_controller->set('isError' , true);
                $this->_controller->redirect('/storages');
            }
            return;
        }
        $_lastHeaderId = $this->_controller->TrnStorageHeader->getLastInsertID();

        $this->Session->write('Storages.lastHeaderId', $_lastHeaderId);

        if(!$this->_controller->TrnStorage->updateAll(array(
            'TrnStorage.trn_storage_header_id' => $_lastHeaderId,
        ),array(
            'TrnStorage.work_no' => $_workNo,
            ),-1)){
            $this->_controller->TrnStorage->rollback();
            $this->_controller->Session->setFlash('入庫明細の作成に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
            if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                $this->_controller->redirect('/storages_select');
            }else{
                $this->_controller->set('isError' , true);
                $this->_controller->redirect('/storages');
            }
            return;
        }

        //入荷レコードの残数がマイナスになっていないかチェックをしてみる
        $sql  = ' select ';
        $sql .= '       count(*) as count  ';
        $sql .= '   from ';
        $sql .= '     trn_receivings as a  ';
        $sql .= '   where ';
        if(is_array($receiving['id'])){
            $sql .= '     a.id in (' . join(',' ,  $receiving['id'] ). ') ';
        }else{
            $sql .= '     a.id = ' . $receiving['id'] . ' ';
        }
        $sql .= ' and a.remain_count < 0 ' ;
        $ret = $this->_controller->TrnReceiving->query($sql , false);
        if($ret[0][0]['count'] > 0 ){
            //マイナスができているのでエラー
            $this->_controller->TrnStorage->rollback();
            $this->_controller->Session->setFlash('ほかユーザによって更新されています。最初からやり直してください。' , 'growl', array('type'=>'error'));
            if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                $this->_controller->redirect('/storages_select');
            }else{
                $this->_controller->set('isError' , true);
                $this->_controller->redirect('/storages');
            }
            return;
        }

        //コミット直前に更新時間チェック
        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     trn_receivings as a  ';
        $sql .= '   where ';
        if(is_array($receiving['id'])){
            $sql .= '     a.id in (' . join(',' ,  $receiving['id'] ). ')';
        }else{
            $sql .= '     a.id = ' . $receiving['id'] ;
        }
        $sql .= "     and a.modified > '" . $params['time'] ."'";
        $sql .= "     and a.modified <> '" . $now ."'";

        $ret = $this->_controller->TrnReceiving->query($sql , false);

        if($ret[0][0]['count']> 0 ){
            $this->_controller->TrnStorage->rollback();
            $this->_controller->Session->setFlash('ほかユーザによって更新されています。最初からやり直してください。' , 'growl', array('type'=>'error'));
            if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                $this->_controller->redirect('/storages_select');
            }else{
                $this->_controller->set('isError' , true);
                $this->_controller->redirect('/storages');
            }
            return;
        }

        $errors = array();
        $errors = array_merge(
                $errors,
                is_array($this->_controller->validateErrors($this->_controller->TrnReceiving)) ? $this->_controller->validateErrors($this->_controller->TrnReceiving) : array(),
                is_array($this->_controller->validateErrors($this->_controller->TrnReceivingHeader)) ? $this->_controller->validateErrors($this->_controller->TrnReceivingHeader) : array(),
                is_array($this->_controller->validateErrors($this->_controller->TrnStorage)) ? $this->_controller->validateErrors($this->_controller->TrnStorage) : array(),
                is_array($this->_controller->validateErrors($this->_controller->TrnStorageHeader)) ? $this->_controller->validateErrors($this->_controller->TrnStorageHeader) : array(),
                is_array($this->_controller->validateErrors($this->_controller->TrnSticker)) ? $this->_controller->validateErrors($this->_controller->TrnSticker) : array(),
                is_array($this->_controller->validateErrors($this->_controller->TrnStickerRecord)) ? $this->_controller->validateErrors($this->_controller->TrnStickerRecord) : array(),
                is_array($this->_controller->validateErrors($this->_controller->TrnStock)) ? $this->_controller->validateErrors($this->_controller->TrnStock) : array()
        );
        if(count($errors) > 0){
            $this->_controller->TrnStorage->rollback();
            $this->_controller->set('isError' , true);
            $this->_controller->Session->setFlash('入庫明細の作成に失敗しました。最初からやり直してください。' , 'growl', array('type'=>'error'));
            if(isset($this->_controller->data['TrnStorage']['select_mode'])){
                $this->_controller->redirect('/storages_select');
            }else{
                $this->_controller->redirect('/storages');
            }
            return;
        }else{
            $this->_controller->TrnStorage->commit();
        }

        //シール発行データを保存
        $this->Session->write('Storages.stickers', $_stickers);
        return $_lastHeaderId;
    }

    public function getTransactionPrice($per_unit, $price, $divide_per_unit, $roundType){
        $val = $price / $per_unit * $divide_per_unit;
        return $this->getRoundValue($val, $roundType, 3);
    }

    
    /**
     * 少数桁考慮した各種丸め
     * @param float $value
     * @param string $round
     * @param int $digit 対象桁
     * @access public
     * @return float
     */
    public function getRoundValue($value, $round, $digit){
        switch($round){
            case Configure::read('RoundType.Round'):
                return round($value, 3);
                break;
            case Configure::read('RoundType.Ceil'):
                return ceil($value*pow(10,$digit))/pow(10, $digit);
                break;
            case Configure::read('RoundType.Floor'):
            case Configure::read('RoundType.Normal'):
            default:
                return floor($value*pow(10,$digit))/pow(10, $digit);
                break;
        }
    }

   /**
     * 施主名を取得
     * @param string $_id 施設コード
     * @return string
     */
    public function getOwnerName($_id){
        $res = $this->MstFacility->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'id' => $_id,
            ),
        ));
        return $res['MstFacility']['facility_name'];
    }

    private function isNNs($val){
        return $this->_controller->isNNs($val);
    }

    /**
     * Use this with php function: array_walk_recursive
     * @param mixed $item
     * @param string $key
     * @param array $res
     * @access public
     * @static
     */
    public static function BuildSearchResult($item, $key, $res){
        //HACK:  最上位の配列値を優先するようにしてます・・
        if(false === isset($res[$key])){
            $res[$key] = $item;
        }
    }

    /**
     * 在庫情報取得
     * @param string $id
     * @param string $target
     * @access public
     * @return array
     */
    public function getStockById($id, $target){
        foreach($target as $_row){
            if($_row['id'] == $id){
                return $_row;
            }
        }
        return null;
    }


    /**
     * 検索処理
     *
     * @param $conditions
     * @access private
     * @return array
     */
    public function getStorages($conditions, $showDeleted = false, $where = ""){
        $sql =
            ' select ' .
                ' a.id'.
                ',a.work_no' .
                ",to_char(a.work_date,'YYYY/mm/dd') as work_date" .
                ',a.mst_department_id' .
                ',a.trn_receiving_id'.
                ',a.mst_item_unit_id'.
                ',a.trn_storage_header_id' .
                ',b.trn_receiving_header_id' .
                ',b.id as trn_receiving_id'.
                ',b.department_id_to as receiving_department_id'.
                ',b.mst_item_unit_id as receiving_item_unit_id'.
                ',c.id as trn_order_id'.
                ',f.internal_code'.
                ',f.item_name'.
                ',f.item_code'.
                ',f.insurance_claim_code'.
                ',f.biogenous_type'.
                ',f.mst_owner_id'.
                //',f.price'.
                ',round((f.unit_price * e.per_unit * f.converted_num),2) as price'.
                ',d.lot_no' .
                ',j.facility_name'.
                ',f.standard' .
                ',g.dealer_name' .
                ',d.transaction_price'.
                ",to_char(d.validated_date,'YYYY/mm/dd') as validated_date".
                ',COALESCE(a.facility_sticker_no,d.facility_sticker_no) as facility_sticker_no' .
                ',d.id as sticker_id'.
                ',a.recital'.
                ',e.per_unit'.
                ',k.user_name as modifier_name'.
                ',h.unit_name as unitName'.
                ',l.unit_name as perUnitName'.
                ",(case when e.per_unit = 1 then h.unit_name else h.unit_name || '(' || e.per_unit || l.unit_name || ')' end ) as packing_name".
                ',m."name" as work_type_name'.
                ',o.code as shelf_code'.
                ',o.name as shelf_name'.
                ',p.const_nm as claim_name'.
            ' from ' .
                ' trn_storages a'.
                    ' inner join trn_receivings b on'.
                        ' a.trn_receiving_id = b.id'.
                    ' left join trn_orders c on'.
                        ' b.trn_order_id = c.id'.
                    ' inner join trn_stickers d on'.
                        ' a.trn_sticker_id = d.id'.
                    ' inner join mst_item_units e on'.
                        ' a.mst_item_unit_id = e.id'.
                    ' inner join mst_facility_items f on'.
                        ' e.mst_facility_item_id = f.id' .
                    ' left join mst_dealers g on'.
                        ' f.mst_dealer_id = g.id'.
                    ' inner join mst_unit_names h on'.
                        ' e.mst_unit_name_id = h.id'.
                    ' inner join mst_departments i on'.
                        ' b.department_id_from = i.id' .
                    ' inner join mst_facilities j on' .
                        ' i.mst_facility_id = j.id'.
                    ' inner join mst_users k on' .
                        ' a.modifier = k.id' .
                    ' inner join mst_unit_names l on' .
                        ' e.per_unit_name_id = l.id ' .
                    ' left join mst_classes m on ' .
                        ' a.work_type = m.id ' .
                    ' left join mst_fixed_counts n on' .
                        ' a.mst_item_unit_id = n.mst_item_unit_id and a.mst_department_id = n.mst_department_id ' .
                    ' left join mst_shelf_names o on' .
                        ' n.mst_shelf_name_id = o.id ' .
                    ' left join mst_consts p on' .
                        ' f.insurance_claim_type::varchar = p.const_cd ' .
                        " and p.const_group_cd='". Configure::read('ConstGroupType.insuranceClaimType') . "'" .
            ' where ' .
                      ' 1 = 1 ' .
                     // " and d.has_reserved = false".
                      " and d.trn_return_receiving_id IS NULL".
                      " and a.storage_type <> '5'".
                      $where.
                ($showDeleted === false ? ' and a.is_deleted = false ' : '');
        if(true === isset($conditions['TrnStorageHeader'])){
            $sql .= ' and a.trn_storage_header_id in (' . join(',',$conditions['TrnStorageHeader']) . ')';
        }
        if(true === isset($conditions['TrnStorage'])){
            $sql .= ' and a.id in (' . join(',',$conditions['TrnStorage']) . ')';
        }

        $sql .= ' order by a.id asc ';
        $res = $this->_controller->TrnStorageHeader->query($sql);
        $result = array();
        foreach($res as $_row){
            $_r = $_row[0];
            $result[] = $_r;
        }
        return $result;
    }

}
?>
