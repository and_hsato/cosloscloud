<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class StickersComponent extends Component {
    private $_controller;
    public function startup(&$controller){
        $this->_controller = $controller;
    }

    /**
     * センターシールを検索
     * @param string $fs
     * @param $fname
     * @param $fid 選択中のセンターID
     * @return array
     */
    public function getFacilityStickers($fs, $fname, $fid , $type=1){
        App::import('Sanitize');
        $sql  = ' select ' ;
        $sql .=         ' a.* ';
        $sql .=         ",to_char(a.validated_date,'YYYY/mm/dd') as validated_date";
        $sql .=         ',b.mst_facility_item_id';
        $sql .=         ',c.internal_code';
        $sql .=         ',c.item_code';
        $sql .=         ',c.item_name';
        $sql .=         ',c.standard' ;
        $sql .=         ',round((c.unit_price * b.per_unit * c.converted_num),2) as price';
        $sql .=         ',c.mst_owner_id';
        $sql .=         ',j.const_nm as biotype_name';
        $sql .=         ',f.dealer_name';
        $sql .=         ',(case ';
        $sql .=         '   when b.per_unit = 1 then d.unit_name';
        $sql .=         "   else d.unit_name || '(' || b.per_unit || e.unit_name || ')' ";
        $sql .=         '  end ) as packing_name ';
        $sql .=         ',d.unit_name as unitName' ;
        $sql .=         ',e.unit_name as p_unit_name';
        $sql .=         ',b.per_unit as p_unit' ;
        $sql .=         ',h.name as shelf_name ';
        $sql .=         ',i.const_nm as insurance_claim_department_name ';
        $sql .=  ' from ' ;
        $sql .=         ' trn_stickers a ' ;
        $sql .=         ' inner join mst_item_units b on a.mst_item_unit_id = b.id and b.mst_facility_id = ' . $fid . ' ' ;
        $sql .=         ' inner join mst_facility_items c on b.mst_facility_item_id = c.id and c.mst_facility_id = ' . $fid . ' ' ;
        $sql .=         ' inner join mst_unit_names d on b.mst_unit_name_id = d.id ' ;
        $sql .=         ' inner join mst_unit_names e on b.per_unit_name_id = e.id ' ;
        $sql .=         ' left join mst_dealers f on c.mst_dealer_id = f.id' ;
        $sql .=         ' left join mst_departments aa on aa.mst_facility_id = ' . $fid . ' and a.mst_department_id = aa.id ' ;
        $sql .=         ' left join mst_fixed_counts g on aa.id = g.mst_department_id and a.mst_item_unit_id = g.mst_item_unit_id ' ;
        $sql .=         ' left join mst_shelf_names h on g.mst_shelf_name_id = h.id ' ;
        $sql .=         " left join mst_consts i on i.const_cd = c.insurance_claim_type::varchar and i.const_group_cd = '" . Configure::read('ConstGroupType.insuranceClaimType') . "'" ;
        $sql .=         " left join mst_consts j on j.const_cd = c.biogenous_type and j.const_group_cd = '" . Configure::read('ConstGroupType.biogenousTypes') . "'" ;
        $sql .= ' where ';
        if(false === is_array($fs)){
            $sql .= " a.facility_sticker_no = '" . Sanitize::escape($fs) . "' ";
        }else{
            $c = array();
            foreach($fs as $_no){
                $c[] = "'" . Sanitize::escape(trim($_no)) . "'";
            }
            $sql .= ' a.facility_sticker_no in (' . join(',', $c) . ')';
        }
        $sql .= ' order by a.id asc';
        $res = $this->_controller->TrnSticker->query($sql);
        $result = array();
        if(empty($res)){
            return false;
        }else{
            foreach($res as $_row){
                $r = array();
                array_walk_recursive($_row, array('StickersComponent','BuildSearchResult'), &$r);
                $result[] = $r;
            }
        }
        
        $i = 0;
        $prePerUnit = null;
        $stickers = array();
        if($type==1){
            foreach($result as $_row){
                $stickers[] = array(
                    '商品ID'         => $_row['internal_code'],
                    'シール番号'     => $_row['facility_sticker_no'],
                    '商品名'         => $_row['item_name'],
                    '規格'           => $_row['standard'],
                    '製品番号'     => $_row['item_code'],
                    'バーコード'     => $_row['facility_sticker_no'],
                    '保険請求'       => $_row['insurance_claim_department_name'],
                    '包装単位'       => $_row['packing_name'],
                    'ロット番号'     => $_row['lot_no'],
                    'センター情報'   => $fname,
                    '販売元'         => $_row['dealer_name'],
                    '生物由来'       => $_row['biotype_name'],
                    'センター棚番号' => $_row['shelf_name'],
                    '定価'           => $_row['price'],
                    '有効期限'       => $_row['validated_date'],
                    );
            }
        }else{
            foreach($result as $_row){
                if($prePerUnit !== $_row['p_unit'] && !is_null($prePerUnit)){
                    $i++; // 包装単位が違ったらカットする。
                }
                
                $stickers[$i][] = array(
                    '商品ID'         => $_row['internal_code'],
                    'シール番号'     => $_row['facility_sticker_no'],
                    '商品名'         => $_row['item_name'],
                    '規格'           => $_row['standard'],
                    '製品番号'     => $_row['item_code'],
                    'バーコード'     => $_row['facility_sticker_no'],
                    '保険請求'       => $_row['insurance_claim_department_name'],
                    '包装単位'       => $_row['packing_name'],
                    'ロット番号'     => $_row['lot_no'],
                    'センター情報'   => $fname,
                    '販売元'         => $_row['dealer_name'],
                    '生物由来'       => $_row['biotype_name'],
                    'センター棚番号' => $_row['shelf_name'],
                    '定価'           => $_row['price'],
                    '有効期限'       => $_row['validated_date'],
                    );
                $prePerUnit = $_row['p_unit'];
            }
        }
        return $stickers;
    }

    public function getHospitalStickers($cond, $order,$type=1){
        App::import('Sanitize');
        if($type==1){
            //部署シールテーブルが有る場合
            $sql = ' select ' ;
            $sql .=     ' MstFacilityItem.internal_code';
            $sql .=     ',TrnStickerIssue.hospital_sticker_no';
            $sql .=     ',TrnSTicker.lot_no';
            $sql .=     ",to_char(TrnSticker.validated_date,'YYYY/mm/dd') as validated_date";
            $sql .=     ',TrnStickerIssue.promise_type';
            $sql .=     ',MstFacilityItem.item_name';
            $sql .=     ',MstFacilityItem.standard';
            $sql .=     ',MstFacilityItem.item_code';
            $sql .=     ',MstFixedCountTo.print_type';
            $sql .=     ',MstInsuranceClaimDepartment.const_nm as insurance_claim_department_name';
            $sql .=     ",(MstDepartment.department_name) as hospital_department";
            $sql .=     ',MstDealer.dealer_name';
            $sql .=     ',MstBioType.const_nm as biotype_name';
            $sql .=     ',MstShelfNameFrom.name as center_shelf_name';
            $sql .=     ',coalesce(round((MstFacilityItem.unit_price * MstItemUnit.per_unit * MstFacilityItem.converted_num),2),0) as price';
            $sql .=     ',MstShelfNameTo.name as hospital_shelf_name';
            $sql .=     ',MstItemUnit.per_unit';
            $sql .=     ',MstUnitName.unit_name as unit_name';
            $sql .=     ',MstPerUnitName.unit_name as per_unit_name';
            $sql .=     ',(case ';
            $sql .=     '   when MstItemUnit.per_unit = 1 then MstUnitName.unit_name ';
            $sql .=     "   else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')' ";
            $sql .=     '  end ) as packing_name';
            $sql .=     ',( case when MstFixedCountTo.trade_type is null then ' . Configure::read('ClassesType.Temporary');
            $sql .=     '        when MstFixedCountTo.trade_type = ' . Configure::read('TeisuType.NonStock') . ' then ' . Configure::read('ClassesType.NoStock');
            $sql .=     '        else MstFixedCountTo.trade_type ';
            $sql .=     '   end ) as trade_type';
            $sql .=     ',MstFixedCountTo.fixed_count';
            $sql .=     ',MstFixedCountTo.holiday_fixed_count';
            $sql .=     ',MstFixedCountTo.spare_fixed_count';
            $sql .=     ',coalesce(TrnPromise.fixed_num_used,TrnOrder.fixed_num_used) as fixed_num_used';
            $sql .=     ',MstFacilityItem.buy_flg';
            $sql .=     ",to_char(coalesce(TrnShipping.work_date,TrnSticker.last_move_date),'YYYY/MM/DD') as work_date";
            $sql .=     ',MstFacilityOption.layout_type ';
            $sql .= ' from ' ;
            $sql .=     ' trn_sticker_issues as TrnStickerIssue' ;
            $sql .=     ' left join trn_stickers as TrnSticker on TrnSticker.hospital_sticker_no = TrnStickerIssue.hospital_sticker_no' ;
            $sql .=     ' left join trn_fixed_promises as TrnFixedPromise on TrnStickerIssue.trn_fixed_promise_id = TrnFixedPromise.id' ;
            $sql .=     ' left join trn_promises as TrnPromise on TrnFixedPromise.trn_promise_id = TrnPromise.id' ;
            $sql .=     ' left join mst_item_units as MstItemUnit on TrnStickerIssue.mst_item_unit_id = MstItemUnit.id';
            $sql .=     ' left join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
            $sql .=     ' left join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id' ;
            $sql .=     ' left join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id';
            $sql .=     ' left join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id';
            $sql .=     ' left join mst_departments as MstDepartment on TrnStickerIssue.department_id_to = MstDepartment.id';
            $sql .=     ' left join mst_departments as MstDepartmentCenter on MstItemUnit.mst_facility_id = MstDepartmentCenter.mst_facility_id and MstDepartmentCenter.department_type = ' . Configure::read('DepartmentType.warehouse');
            $sql .=     ' left join mst_fixed_counts as MstFixedCountFrom on MstFixedCountFrom.mst_department_id = MstDepartmentCenter.id and MstFixedCountFrom.mst_item_unit_id = TrnStickerIssue.mst_item_unit_id';
            $sql .=     ' left join mst_fixed_counts as MstFixedCountTo on MstFixedCountTo.mst_department_id = TrnStickerIssue.department_id_to and MstFixedCountTo.mst_item_unit_id = TrnStickerIssue.mst_item_unit_id';
            $sql .=     ' left join mst_shelf_names as MstShelfNameFrom on MstFixedCountFrom.mst_shelf_name_id = MstShelfNameFrom.id';
            $sql .=     ' left join mst_shelf_names as MstShelfNameTo on MstFixedCountTo.mst_shelf_name_id = MstShelfNameTo.id';
            $sql .=     ' left join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id';
            $sql .=     " left join mst_consts as MstInsuranceClaimDepartment on MstInsuranceClaimDepartment.const_cd = MstFacilityItem.insurance_claim_type::varchar and MstInsuranceClaimDepartment.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
            $sql .=     ' left join trn_orders as TrnOrder on TrnOrder.id = TrnSticker.trn_order_id';
            $sql .=     " left join mst_consts as MstBioType on MstBioType.const_cd = MstFacilityItem.biogenous_type and MstBioType.const_group_cd = '" . Configure::read('ConstGroupType.biogenousTypes') . "'";
            $sql .=     ' left join trn_shippings as TrnShipping on TrnShipping.id = TrnSticker.trn_shipping_id ';
            $sql .=     ' left join mst_facility_options as MstFacilityOption on MstFacilityOption.mst_facility_id = MstFacilityItem.mst_facility_id ';
            $sql .= ' where ';
            if(false === is_array($cond)){
                $sql .= " TrnStickerIssue.hospital_sticker_no = '" . Sanitize::escape($cond) . "' ";
            }else{
                $c = array();
                foreach($cond as $_no){
                    $c[] = "'" . $_no . "'";
                }
                $sql .= ' TrnStickerIssue.hospital_sticker_no in (' . join(',', $c) . ')';
            }
        }elseif($type==2){
            //部署シールテーブルが無い場合（臨時出荷、直納、シール検索）
            $sql = ' select ' ;
            $sql .=    ' MstFacilityItem.internal_code';
            $sql .=     ',TrnSticker.hospital_sticker_no';
            $sql .=     ',TrnSticker.lot_no';
            $sql .=     ",to_char(TrnSticker.validated_date,'YYYY/mm/dd') as validated_date";
            $sql .=     ',TrnStickerIssue.promise_type';
            $sql .=     ',MstFacilityItem.item_name';
            $sql .=     ',MstFacilityItem.standard';
            $sql .=     ',MstFacilityItem.item_code';
            $sql .=     ',MstFixedCountTo.print_type';
            $sql .=     ',MstInsuranceClaimDepartment.const_nm as insurance_claim_department_name';
            $sql .=     ",(MstDepartment.department_name) as hospital_department";
            $sql .=     ',MstDealer.dealer_name';
            $sql .=     ',MstBioType.const_nm as biotype_name';
            $sql .=     ',MstShelfNameFrom.name as center_shelf_name';
            $sql .=     ',coalesce(round((MstFacilityItem.unit_price * MstItemUnit.per_unit * MstFacilityItem.converted_num),2),0) as price';
            $sql .=     ',MstShelfNameTo.name as hospital_shelf_name';
            $sql .=     ',MstItemUnit.per_unit';
            $sql .=     ',MstUnitName.unit_name as unit_name';
            $sql .=     ',MstPerUnitName.unit_name as per_unit_name';
            $sql .=     ',(case ';
            $sql .=     '   when MstItemUnit.per_unit = 1 then MstUnitName.unit_name ';
            $sql .=     "   else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')' ";
            $sql .=     '  end ) as packing_name';
            $sql .=     ',TrnSticker.trade_type';
            $sql .=     ',MstFixedCountTo.fixed_count';
            $sql .=     ',MstFixedCountTo.holiday_fixed_count';
            $sql .=     ',MstFixedCountTo.spare_fixed_count';
            $sql .=     ',coalesce(TrnPromise.fixed_num_used,TrnOrder.fixed_num_used) as fixed_num_used';
            $sql .=     ',MstFacilityItem.buy_flg';
            $sql .=     ",to_char(coalesce(TrnShipping.work_date,TrnSticker.last_move_date),'YYYY/MM/DD') as work_date";
            $sql .=     ',MstFacilityOption.layout_type ';
            $sql .= ' from ' ;
            $sql .=     ' trn_stickers as TrnSticker' ;
            $sql .=     ' left join trn_sticker_issues as TrnStickerIssue on TrnSticker.hospital_sticker_no = TrnStickerIssue.hospital_sticker_no' ;
            $sql .=     ' left join trn_fixed_promises as TrnFixedPromise on TrnStickerIssue.trn_fixed_promise_id = TrnFixedPromise.id' ;
            $sql .=     ' left join trn_promises as TrnPromise on TrnFixedPromise.trn_promise_id = TrnPromise.id' ;
            $sql .=     ' inner join mst_item_units as MstItemUnit on TrnSticker.mst_item_unit_id = MstItemUnit.id';
            $sql .=     ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
            $sql .=     ' left join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id' ;
            $sql .=     ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id';
            $sql .=     ' inner join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id';
            $sql .=     ' inner join mst_departments as MstDepartment on TrnSticker.mst_department_id = MstDepartment.id';
            $sql .=     ' inner join mst_departments as MstDepartmentCenter on MstItemUnit.mst_facility_id = MstDepartmentCenter.mst_facility_id and MstDepartmentCenter.department_type = ' . Configure::read('DepartmentType.warehouse');
            $sql .=     ' left join mst_fixed_counts as MstFixedCountFrom on MstFixedCountFrom.mst_department_id = MstDepartmentCenter.id and MstFixedCountFrom.mst_item_unit_id = TrnSticker.mst_item_unit_id';
            $sql .=     ' left join mst_fixed_counts as MstFixedCountTo on MstFixedCountTo.mst_department_id = TrnSticker.mst_department_id and MstFixedCountTo.mst_item_unit_id = TrnSticker.mst_item_unit_id';
            $sql .=     ' left join mst_shelf_names as MstShelfNameFrom on MstFixedCountFrom.mst_shelf_name_id = MstShelfNameFrom.id';
            $sql .=     ' left join mst_shelf_names as MstShelfNameTo on MstFixedCountTo.mst_shelf_name_id = MstShelfNameTo.id';
            $sql .=     ' left join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id';
            $sql .=     " left join mst_consts as MstInsuranceClaimDepartment on MstInsuranceClaimDepartment.const_cd = MstFacilityItem.insurance_claim_type::varchar and MstInsuranceClaimDepartment.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
            $sql .=     ' left join trn_orders as TrnOrder on TrnOrder.id = TrnSticker.trn_order_id';
            $sql .=     " left join mst_consts as MstBioType on MstBioType.const_cd = MstFacilityItem.biogenous_type and MstBioType.const_group_cd = '" . Configure::read('ConstGroupType.biogenousTypes') . "'";
            $sql .=     ' left join trn_shippings as TrnShipping on TrnShipping.id = TrnSticker.trn_shipping_id ';
            $sql .=     ' left join mst_facility_options as MstFacilityOption on MstFacilityOption.mst_facility_id = MstFacilityItem.mst_facility_id ';
            $sql .= ' where ';
            if(false === is_array($cond)){
                $sql .= " TrnSticker.hospital_sticker_no = '" . Sanitize::escape($cond) . "' ";
            }else{
                $c = array();
                foreach($cond as $_no){
                    $c[] = "'" . $_no . "'";
                }
                $sql .= ' TrnSticker.hospital_sticker_no in (' . join(',', $c) . ')';
            }
        }elseif($type==3){
            //EDIからの印刷
            $sql = ' select ' ;
            $sql .=    ' MstFacilityItem.internal_code';
            $sql .=     ',TrnSticker.hospital_sticker_no';
            $sql .=     ',TrnSticker.lot_no';
            $sql .=     ",to_char(TrnSticker.validated_date,'YYYY/mm/dd') as validated_date";
            $sql .=     ',TrnStickerIssue.promise_type';
            $sql .=     ',MstFacilityItem.item_name';
            $sql .=     ',MstFacilityItem.standard';
            $sql .=     ',MstFacilityItem.item_code';
            $sql .=     ',MstFixedCountTo.print_type';
            $sql .=     ',MstInsuranceClaimDepartment.const_nm as insurance_claim_department_name';
            $sql .=     ",(MstDepartment.department_name) as hospital_department";
            $sql .=     ',MstDealer.dealer_name';
            $sql .=     ',MstBioType.const_nm as biotype_name';
            $sql .=     ',MstShelfNameFrom.name as center_shelf_name';
            $sql .=     ',coalesce(round((MstFacilityItem.unit_price * MstItemUnit.per_unit * MstFacilityItem.converted_num),2),0) as price';
            $sql .=     ',MstShelfNameTo.name as hospital_shelf_name';
            $sql .=     ',MstItemUnit.per_unit';
            $sql .=     ',MstUnitName.unit_name as unit_name';
            $sql .=     ',MstPerUnitName.unit_name as per_unit_name';
            $sql .=     ',(case ';
            $sql .=     '   when MstItemUnit.per_unit = 1 then MstUnitName.unit_name ';
            $sql .=     "   else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstPerUnitName.unit_name || ')' ";
            $sql .=     '  end ) as packing_name';
            $sql .=     ',TrnSticker.trade_type';
            $sql .=     ',MstFixedCountTo.fixed_count';
            $sql .=     ',MstFixedCountTo.holiday_fixed_count';
            $sql .=     ',MstFixedCountTo.spare_fixed_count';
            $sql .=     ',coalesce(TrnPromise.fixed_num_used,TrnOrder.fixed_num_used) as fixed_num_used';
            $sql .=     ',MstFacilityItem.buy_flg';
            $sql .=     ",to_char(coalesce(TrnShipping.work_date,TrnSticker.last_move_date),'YYYY/MM/DD') as work_date";
            $sql .=     ',MstFacilityOption.layout_type ';
            $sql .= ' from ' ;
            $sql .=     ' trn_stickers as TrnSticker' ;
            $sql .=     ' left join trn_orders as TrnOrder on TrnOrder.id = TrnSticker.trn_order_id';
            $sql .=     ' left join trn_sticker_issues as TrnStickerIssue on TrnSticker.hospital_sticker_no = TrnStickerIssue.hospital_sticker_no' ;
            $sql .=     ' left join trn_fixed_promises as TrnFixedPromise on TrnStickerIssue.trn_fixed_promise_id = TrnFixedPromise.id' ;
            $sql .=     ' left join trn_promises as TrnPromise on TrnFixedPromise.trn_promise_id = TrnPromise.id' ;
            $sql .=     ' inner join mst_item_units as MstItemUnit on TrnSticker.mst_item_unit_id = MstItemUnit.id';
            $sql .=     ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
            $sql .=     ' left join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id' ;
            $sql .=     ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id';
            $sql .=     ' inner join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id';
            $sql .=     ' inner join mst_departments as MstDepartment on TrnOrder.department_id_from = MstDepartment.id';
            $sql .=     ' inner join mst_departments as MstDepartmentCenter on MstItemUnit.mst_facility_id = MstDepartmentCenter.mst_facility_id and MstDepartmentCenter.department_type = ' . Configure::read('DepartmentType.warehouse');
            $sql .=     ' left join mst_fixed_counts as MstFixedCountFrom on MstFixedCountFrom.mst_department_id = MstDepartmentCenter.id and MstFixedCountFrom.mst_item_unit_id = TrnSticker.mst_item_unit_id';
            $sql .=     ' left join mst_fixed_counts as MstFixedCountTo on MstFixedCountTo.mst_department_id = TrnSticker.mst_department_id and MstFixedCountTo.mst_item_unit_id = TrnSticker.mst_item_unit_id';
            $sql .=     ' left join mst_shelf_names as MstShelfNameFrom on MstFixedCountFrom.mst_shelf_name_id = MstShelfNameFrom.id';
            $sql .=     ' left join mst_shelf_names as MstShelfNameTo on MstFixedCountTo.mst_shelf_name_id = MstShelfNameTo.id';
            $sql .=     ' left join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id';
            $sql .=     " left join mst_consts as MstInsuranceClaimDepartment on MstInsuranceClaimDepartment.const_cd = MstFacilityItem.insurance_claim_type::varchar and MstInsuranceClaimDepartment.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
            $sql .=     " left join mst_consts as MstBioType on MstBioType.const_cd = MstFacilityItem.biogenous_type and MstBioType.const_group_cd = '" . Configure::read('ConstGroupType.biogenousTypes') . "'";
            $sql .=     ' left join trn_shippings as TrnShipping on TrnShipping.id = TrnSticker.trn_shipping_id ';
            $sql .=     ' left join mst_facility_options as MstFacilityOption on MstFacilityOption.mst_facility_id = MstFacilityItem.mst_facility_id ';
            $sql .= ' where ';
            if(false === is_array($cond)){
                $sql .= " TrnSticker.hospital_sticker_no = '" . Sanitize::escape($cond) . "' ";
            }else{
                $c = array();
                foreach($cond as $_no){
                    $c[] = "'" . $_no . "'";
                }
                $sql .= ' TrnSticker.hospital_sticker_no in (' . join(',', $c) . ')';
            }
            
        }
        $sql .= ' order by ' . $order;

        $res = $this->_controller->TrnSticker->query($sql);
        $result = array();

        $trade_types = Configure::read('Classes');
        $print_type = 0;
        
        $header_text = '要返却￥このラベルは金券と同じです￥';
        $stamp = '預';
        
        foreach($res as $_row){
            $r = $_row[0];
            //区分によってプリンターが変わる
            
            if($r['print_type'] == 0){
                if($r['buy_flg'] == true){
                    //薬品
                    $print_type = Configure::read('PrinterType.hospital.medicine');
                    $header_text = '           発注ラベル（要返却）';
                    $stamp = '買';
                }else if($r['promise_type'] == Configure::read('PromiseType.Temporary') || // 要求区分が臨時
                         $r['trade_type']   == Configure::read('ClassesType.Temporary') || // シール区分が臨時
                         $r['trade_type']   == Configure::read('ClassesType.ExShipping')|| // シール区分が臨時出荷
                         $r['trade_type']   == Configure::read('ClassesType.ExNoStock')){  // シール区分が臨時非在庫
                    //臨時品
                    $print_type = Configure::read('PrinterType.hospital.extraordinary');
                }else if($r['trade_type'] == Configure::read('ClassesType.Constant')){
                    //定数品
                    $print_type = Configure::read('PrinterType.hospital.fixed');
                }else if($r['trade_type'] == Configure::read('ClassesType.SemiConstant')){
                    //準定数品
                    $print_type = Configure::read('PrinterType.hospital.semifixed');
                }else if($r['trade_type'] == Configure::read('ClassesType.Deposit')){
                    //預託品
                    $print_type = Configure::read('PrinterType.hospital.deposit');
                }else if($r['trade_type'] == Configure::read('ClassesType.NoStock')){
                    //非在庫品
                    $print_type = Configure::read('PrinterType.hospital.nostock');
                }
            }else{
                if($r['print_type'] == 1){
                    $print_type = Configure::read('PrinterType.spare1');
                }else if ($r['print_type'] == 2){
                    $print_type = Configure::read('PrinterType.spare2');
                }else if ($r['print_type'] == 3){
                    $print_type = Configure::read('PrinterType.spare3');
                }
            }
            //管理区分定数
            // 通常
            if($r['fixed_num_used'] == Configure::read('FixedType,Constant')){
                $r['class_fixed'] = $trade_types[$r['trade_type']].($r['fixed_count'] !== '' ? $r['fixed_count']:$r['fixed_count'] = '');
                // 休日
            }elseif($r['fixed_num_used'] == Configure::read('FixedType.Holiday')){
                $r['class_fixed'] = '●'.$trade_types[$r['trade_type']].($r['holiday_fixed_count'] !== '' ? $r['holiday_fixed_count']:$r['holiday_fixed_count'] = '');
                // 予備
            }elseif($r['fixed_num_used'] == Configure::read('FixedType.Spare')){
                $r['class_fixed'] = '○'.$trade_types[$r['trade_type']].($r['spare_fixed_count'] !== '' ? $r['spare_fixed_count']:$r['spare_fixed_count'] = '');
                // それ以外
            }else{
                $r['class_fixed'] = $trade_types[$r['trade_type']];
            }
            if($r['promise_type'] == Configure::read('PromiseType.Temporary')){
                $r['class_fixed'] = $trade_types[Configure::read('ClassesType.Temporary')];
            }

            // カスタマイズ1の場合
            if( $r['layout_type'] == '1' ){
                if(empty($r['lot_no'])){
                    $r['lot_no'] = '無し';
                }
                if(empty($r['validated_date'])){
                    $r['validated_date'] = '無し';
                }
                $r['internal_code']  = '物品CODE:' . $r['internal_code'];
                $r['lot_no']         = 'LOT:' . $r['lot_no'];
                $r['validated_date'] = '期限:' . $r['validated_date'];
                $r['price']          = $r['price'] . ' / ' . $r['unit_name'];
            }
            
            $result[$print_type][] = array(
                '商品ID'         => $r['internal_code'],
                'シール番号'     => $r['hospital_sticker_no'],
                '商品名'         => $r['item_name'],
                '規格'           => $r['standard'],
                '製品番号'     => $r['item_code'],
                'バーコード'     => $r['hospital_sticker_no'],
                '保険請求'       => $r['insurance_claim_department_name'],
                '包装単位'       => $r['packing_name'],
                '管理区分定数'   => $r['class_fixed'],
                '病院情報'       => $r['hospital_department'],
                '販売元'         => $r['dealer_name'],
                '生物由来'       => $r['biotype_name'],
                'センター棚番号' => $r['center_shelf_name'],
                '定価'           => $r['price'],
                '病院棚番号'     => $r['hospital_shelf_name'],
                'ロット番号'     => $r['lot_no'],
                '有効期限'       => $r['validated_date'],
                'スタンプ'       => $stamp,
                'ヘッダ文言'     => $header_text,
                '入荷日'         => '入荷日:' . $r['work_date'],
                'レイアウト'     => $r['layout_type'],
                );
        }
        return $result;
    }

    /**
     * コストシールを検索
     * @param string $fs
     * @param $fname
     * @return array
     */
    public function getCostStickers($ids,$type = 1,$count = 0){
        App::import('Sanitize');
        if($type==1 || $type==5){
            //TrnStickerベース
            $sql  = ' select ';
            $sql .= '       a.lot_no ';
            $sql .= "     , ( case when a.hospital_sticker_no = '' then a.facility_sticker_no else  a.hospital_sticker_no end ) as sticker_no";
            $sql .= "     , to_char(a.validated_date,'YYYY/mm/dd') as validated_date ";
            $sql .= '     , c.internal_code ';
            $sql .= '     , c.standard ';
            $sql .= '     , c.item_name ';
            $sql .= '     , coalesce(i.medical_code, c.medical_code) as medical_code ';
            $sql .= '     , c.item_code ';
            $sql .= '     , c.insurance_claim_code ';
            $sql .= '     , j.insurance_claim_name_s ';
            $sql .= '     , k.const_nm as insurance_claim_department_name ';
            $sql .= '     , h.facility_name ';
            $sql .= '     , d.dealer_name ';
            $sql .= '     , l.const_nm as biotype_name ';
            $sql .= '     , c.refund_price ';
            $sql .= '     , c.unit_price ';
            $sql .= '     , b.per_unit ';
            $sql .= '     , e.unit_name ';
            $sql .= '     , f.unit_name                              as per_unit_name  ';
            $sql .= '     , m.layout_type';
            $sql .= '     , c.spare_key18';
            $sql .= '     , c.spare_key17';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '     inner join mst_item_units as b  ';
            $sql .= '       on a.mst_item_unit_id = b.id  ';
            $sql .= '     inner join mst_facility_items as c  ';
            $sql .= '       on b.mst_facility_item_id = c.id  ';
            $sql .= '     left join mst_dealers as d  ';
            $sql .= '       on c.mst_dealer_id = d.id  ';
            $sql .= '     inner join mst_unit_names as e  ';
            $sql .= '       on b.mst_unit_name_id = e.id  ';
            $sql .= '     inner join mst_unit_names as f  ';
            $sql .= '       on b.per_unit_name_id = f.id  ';
            $sql .= '     inner join mst_departments as g  ';
            $sql .= '       on a.mst_department_id = g.id  ';
            $sql .= '     left join mst_facilities as h  ';
            $sql .= '       on g.mst_facility_id = h.id  ';
            $sql .= '       and h.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .= '     left join mst_medical_codes as i  ';
            $sql .= '       on c.id = i.mst_facility_item_id  ';
            $sql .= '       and i.mst_facility_id = g.mst_facility_id  ';
            $sql .= '       and i.is_deleted = false  ';
            $sql .= '     left join mst_insurance_claims as j  ';
            $sql .= '       on c.insurance_claim_code = j.insurance_claim_code  ';
            $sql .= '     left join mst_consts as k  ';
            $sql .= '       on c.insurance_claim_type::varchar = k.const_cd  ';
            $sql .= "       and k.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
            $sql .= '     left join mst_consts as l  ';
            $sql .= '       on c.biogenous_type = l.const_cd  ';
            $sql .= "       and l.const_group_cd='" . Configure::read('ConstGroupType.biogenousTypes') . "'";
            $sql .= '     left join mst_facility_options as m ';
            $sql .= '       on m.mst_facility_id = c.mst_facility_id ';
            $sql .= '   where ';
            if($type == 1){
                $sql .= '     c.enable_medicalcode > 0 ';
            }elseif($type == 5){
                $sql .= '     c.enable_medicalcode = 2 ';
            }
            $sql .= '     and a.id in ('.join(',',$ids).')';
            $sql .= '   order by ';
            $sql .= '     a.hospital_sticker_no asc ';
        }elseif($type == 2){
            //TrnStickerIssue ベース
            $sql  = ' select ';
            $sql .= '       aa.lot_no ';
            $sql .= '     , a.hospital_sticker_no as sticker_no';
            $sql .= "     , to_char(aa.validated_date,'YYYY/mm/dd') as validated_date ";
            $sql .= '     , c.internal_code ';
            $sql .= '     , c.standard ';
            $sql .= '     , c.item_name ';
            $sql .= '     , coalesce(i.medical_code, c.medical_code) as medical_code ';
            $sql .= '     , c.item_code ';
            $sql .= '     , c.insurance_claim_code ';
            $sql .= '     , j.insurance_claim_name_s ';
            $sql .= '     , k.const_nm as insurance_claim_department_name ';
            $sql .= '     , h.facility_name ';
            $sql .= '     , d.dealer_name ';
            $sql .= '     , l.const_nm as biotype_name ';
            $sql .= '     , c.refund_price ';
            $sql .= '     , c.unit_price ';
            $sql .= '     , b.per_unit ';
            $sql .= '     , e.unit_name ';
            $sql .= '     , f.unit_name                              as per_unit_name  ';
            $sql .= '     , m.layout_type';
            $sql .= '     , c.spare_key18';
            $sql .= '     , c.spare_key17';
            $sql .= '   from ';
            $sql .= '     trn_sticker_issues as a  ';
            $sql .= '     left join trn_stickers as aa  ';
            $sql .= '       on a.hospital_sticker_no = aa.hospital_sticker_no  ';
            $sql .= '     inner join mst_item_units as b  ';
            $sql .= '       on a.mst_item_unit_id = b.id  ';
            $sql .= '     inner join mst_facility_items as c  ';
            $sql .= '       on b.mst_facility_item_id = c.id  ';
            $sql .= '     left join mst_dealers as d  ';
            $sql .= '       on c.mst_dealer_id = d.id  ';
            $sql .= '     inner join mst_unit_names as e  ';
            $sql .= '       on b.mst_unit_name_id = e.id  ';
            $sql .= '     inner join mst_unit_names as f  ';
            $sql .= '       on b.per_unit_name_id = f.id  ';
            $sql .= '     inner join mst_departments as g  ';
            $sql .= '       on a.department_id_to = g.id  ';
            $sql .= '     left join mst_facilities as h  ';
            $sql .= '       on g.mst_facility_id = h.id  ';
            $sql .= '       and h.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .= '     left join mst_medical_codes as i  ';
            $sql .= '       on c.id = i.mst_facility_item_id  ';
            $sql .= '       and i.mst_facility_id = g.mst_facility_id  ';
            $sql .= '       and i.is_deleted = false  ';
            $sql .= '     left join mst_insurance_claims as j  ';
            $sql .= '       on c.insurance_claim_code = j.insurance_claim_code  ';
            $sql .= '     left join mst_consts as k  ';
            $sql .= '       on c.insurance_claim_type::varchar = k.const_cd  ';
            $sql .= "      and k.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
            $sql .= '     left join mst_consts as l  ';
            $sql .= '       on c.biogenous_type = l.const_cd  ';
            $sql .= "       and l.const_group_cd='" . Configure::read('ConstGroupType.biogenousTypes') . "'";
            $sql .= '     left join mst_facility_options as m ';
            $sql .= '       on m.mst_facility_id = c.mst_facility_id ';
            $sql .= '   where ';
            $sql .= '     c.enable_medicalcode > 0  ';
            $sql .= '     and a.id in ('.join(',',$ids).')';
            $sql .= '   order by ';
            $sql .= '     a.hospital_sticker_no asc ';
        }elseif($type == 3){
            //画面入力情報ベース
            $sql  = ' select ';
            $sql .= "       '".$this->_controller->data['TrnSticker']['lot_no']."' as lot_no ";
            $sql .= "     , '' as sticker_no";
            $sql .= "     , '".$this->_controller->data['TrnSticker']['validated_date']."' as validated_date ";
            $sql .= '     , c.internal_code ';
            $sql .= '     , c.standard ';
            $sql .= '     , c.item_name ';
            $sql .= "     , '". $this->_controller->data['MstFacilityItem']['medical_code'] ."' as medical_code ";
            $sql .= '     , c.item_code ';
            $sql .= '     , c.insurance_claim_code ';
            $sql .= '     , j.insurance_claim_name_s ';
            $sql .= '     , k.const_nm as insurance_claim_department_name ';
            $sql .= "     , '". $this->_controller->data['MstFacilityItem']['customer_name'] ."' as facility_name ";
            $sql .= '     , d.dealer_name ';
            $sql .= '     , l.const_nm as biotype_name ';
            $sql .= '     , c.refund_price ';
            $sql .= '     , c.unit_price ';
            $sql .= "     , '"  . $this->_controller->data['TrnSticker']['sheets'] . "'as per_unit ";
            $sql .= '     , n.unit_name ';
            $sql .= '     , o.layout_type';
            $sql .= '     , c.spare_key18';
            $sql .= '     , c.spare_key17';
            $sql .= '   from ';
            $sql .= '     mst_facility_items as c  ';
            $sql .= '     left join mst_dealers as d  ';
            $sql .= '       on c.mst_dealer_id = d.id  ';
            $sql .= '     left join mst_insurance_claims as j  ';
            $sql .= '       on c.insurance_claim_code = j.insurance_claim_code  ';
            $sql .= '     left join mst_consts as k  ';
            $sql .= '       on c.insurance_claim_type::varchar = k.const_cd  ';
            $sql .= "       and k.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
            $sql .= '     left join mst_consts as l  ';
            $sql .= '       on c.biogenous_type = l.const_cd  ';
            $sql .= "       and l.const_group_cd='" . Configure::read('ConstGroupType.biogenousTypes') . "'";
            $sql .= '     left join mst_item_units as m ';
            $sql .= '       on m.mst_facility_item_id = c.id ';
            $sql .= '       and m.per_unit = 1 ';
            $sql .= '     left join mst_unit_names as n ';
            $sql .= '       on n.id = m.mst_unit_name_id ';
            $sql .= '     left join mst_facility_options as o ';
            $sql .= '       on o.mst_facility_id = c.mst_facility_id ';
            $sql .= '   where ';
            $sql .= '     c.id in ('.join(',',$ids).')';
        }elseif($type == 4 || $type == 6){
            //TrnOrder ベース（預託品、非在庫品、直納品）
            $sql  = ' select ';
            $sql .= '       aa.lot_no ';
            $sql .= "     , ( case when aa.hospital_sticker_no = '' then aa.facility_sticker_no else  aa.hospital_sticker_no end ) as sticker_no";
            $sql .= "     , to_char(aa.validated_date,'YYYY/mm/dd') as validated_date ";
            $sql .= '     , c.internal_code ';
            $sql .= '     , c.standard ';
            $sql .= '     , c.item_name ';
            $sql .= '     , coalesce(i.medical_code, c.medical_code) as medical_code ';
            $sql .= '     , c.item_code ';
            $sql .= '     , c.insurance_claim_code ';
            $sql .= '     , j.insurance_claim_name_s ';
            $sql .= '     , k.const_nm as insurance_claim_department_name ';
            $sql .= '     , d.dealer_name ';
            $sql .= '     , l.const_nm as biotype_name ';
            $sql .= '     , c.refund_price ';
            $sql .= '     , c.unit_price ';
            $sql .= '     , b.per_unit';
            $sql .= '     , e.unit_name ';
            $sql .= '     , f.unit_name                              as per_unit_name  ';
            $sql .= '     , m.layout_type';
            $sql .= '     , c.spare_key18';
            $sql .= '     , c.spare_key17';
            $sql .= '   from ';
            $sql .= '     trn_orders as a  ';
            $sql .= '     left join trn_stickers as aa  ';
            $sql .= '       on aa.trn_order_id = a.id  ';
            $sql .= '     left join mst_item_units as b  ';
            $sql .= '       on a.mst_item_unit_id = b.id  ';
            $sql .= '     left join mst_facility_items as c  ';
            $sql .= '       on b.mst_facility_item_id = c.id  ';
            $sql .= '     left join mst_dealers as d  ';
            $sql .= '       on c.mst_dealer_id = d.id  ';
            $sql .= '     left join mst_unit_names as e  ';
            $sql .= '       on b.mst_unit_name_id = e.id  ';
            $sql .= '     left join mst_unit_names as f  ';
            $sql .= '       on b.per_unit_name_id = f.id  ';
            $sql .= '     left join mst_departments as g  ';
            $sql .= '       on a.department_id_from = g.id  ';
            $sql .= '     left join mst_facilities as h  ';
            $sql .= '       on g.mst_facility_id = h.id  ';
            $sql .= '       and h.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .= '     left join mst_medical_codes as i  ';
            $sql .= '       on c.id = i.mst_facility_item_id  ';
            $sql .= '       and i.mst_facility_id = g.mst_facility_id  ';
            $sql .= '       and i.is_deleted = false  ';
            $sql .= '     left join mst_insurance_claims as j  ';
            $sql .= '       on c.insurance_claim_code = j.insurance_claim_code  ';
            $sql .= '     left join mst_consts as k  ';
            $sql .= '       on c.insurance_claim_type::varchar = k.const_cd  ';
            $sql .= "       and k.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
            $sql .= '     left join mst_consts as l  ';
            $sql .= '       on c.biogenous_type = l.const_cd  ';
            $sql .= "       and l.const_group_cd='" . Configure::read('ConstGroupType.biogenousTypes') . "'";
            $sql .= '     left join mst_facility_options as m ';
            $sql .= '       on m.mst_facility_id = c.mst_facility_id ';
            $sql .= '   where ';
            $sql .= '     c.enable_medicalcode > 0 ';
            $sql .= '     and a.order_type in ( ' . Configure::read('OrderTypes.replenish') . ' , '. Configure::read('OrderTypes.direct') . ' , ' .Configure::read('OrderTypes.nostock') . ' , ' . Configure::read('OrderTypes.ExNostock') . ' ) ' ;
            $sql .= '     and a.id in ('.join(',',$ids).')';
            $sql .= '   order by ';
            $sql .= '     aa.hospital_sticker_no asc ';
        }

        if($type==4){
            $res = $this->_controller->TrnSticker->query($sql);
        }elseif($type==6){
            $res = $this->_controller->TrnSticker->query($sql , false, false );
        }else{
            $res = $this->_controller->TrnSticker->query($sql);
        }
        $result = array();
        
        if(empty($res)){
            return false;
        }else{
            foreach($res as $_row){
                $r = array();
                array_walk_recursive($_row, array('StickersComponent','BuildSearchResult'), &$r);
                $result[] = $r;
            }
        }

        $stickers = array();
            // 標準
            foreach($result as $_row){
                if( $_row['layout_type'] == '0' ){
                    // 標準
                    $barcode = ($this->_controller->isNNs($_row['medical_code']) ? '' : strtoupper($_row['medical_code']));
                }else {
                    // カスタマイズ1
                    if(empty($_row['refund_price'])){
                        $_row['refund_price'] = $_row['unit_price'];
                    }
                    $_row['refund_price'] = $_row['refund_price'] . ' / ' . $_row['unit_name'];
                    $_row['internal_code'] = 'a'. $_row['internal_code'] . 'a';
                    $barcode = 'A' . $_row['internal_code'] . 'A';
                    $_row['insurance_claim_name_s'] = $_row['spare_key18'];
                    $_row['insurance_claim_department_name'] = $_row['spare_key17'];
                }
                
                // 入数分のシールを生成する
                for($i = 0; $i < (integer)$_row['per_unit']; $i++){
                    $stickers[] = array(
                        '保険請求名'       => $_row['insurance_claim_name_s'],
                        '商品ID'           => $_row['internal_code'],
                        '医事コード'       => $_row['medical_code'],
                        '商品名'           => $_row['item_name'],
                        '規格'             => $_row['standard'],
                        '製品番号'       => $_row['item_code'],
                        'バーコード'       => $barcode,
                        '保険請求'         => $_row['insurance_claim_department_name'],
                        '償還価格'         => $_row['refund_price'],
                        '販売元'           => $_row['dealer_name'],
                        '生物由来'         => $_row['biotype_name'],
                        '部署シール番号'   => $_row['sticker_no'],
                        '病院名'           => $_row['facility_name'],
                        '包装単位'         => '1' . $_row['unit_name'],
                        '発行日'           => date('Y/m/d'),
                        'バーコードガイド' => ($this->_controller->isNNs($_row['medical_code']) ? '' : $_row['medical_code']) . ',' . $_row['lot_no'] . ',' . $_row['validated_date'],
                        );
                }
            }
        return $stickers;
    }
    
    /**
     * Use this with php function: array_walk_recursive
     * @param mixed $item
     * @param string $key
     * @param array $res
     * @access public
     * @static
     */
    public static function BuildSearchResult($item, $key, $res){
        //HACK:  最上位の配列値を優先するようにしてます・・
        if(false === isset($res[$key])){
            $res[$key] = $item;
        }
    }

}
?>
