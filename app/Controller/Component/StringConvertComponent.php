<?php
/**
 * 文字列変換コンポーネント
 * 主にマルチバイト処理
 *
 * @version 1.0.0
 * @since 2010/04/21
 */
class StringConvertComponent extends Component {

    /**
     * マルチバイト対応のtrim
     *
     * @name mbTrim
     * @access public
     * @param mixed $value 変換したい値
     * @return string 変換した値
     */
    function mbTrim($value) {
        if (is_array($value))
            return array_map(array($this, 'mbTrim'), $value);
        return trim($this->toHalfSpace($value));
    }

    /**
     * カナ文字列を半角に変換する
     *
     * @name toHalfKana
     * @access public
     * @param mixed $value 変換したい値
     * @return mixed 変換した値
     */
    function toHalfKana($value) {
        if (is_array($value))
            return array_map(array($this, 'toHalfKana'), $value);
        return mb_convert_kana($value, 'rak', mb_internal_encoding());
    }

    /**
     * カナ文字列を全角に変換する
     *
     * @name toFullKana
     * @access public
     * @param mixed $value 変換したい値
     * @return mixed 変換した値
     */
    function toFullKana($value) {
        if (is_array($value))
            return array_map(array($this, 'toFullKana'), $value);
        return mb_convert_kana($value, 'RAKV', mb_internal_encoding());
    }

    /**
     * 英数字を半角に変換する
     *
     * @name toHalfAlnum
     * @access public
     * @param mixed $value
     * @return mixed
     */
    function toHalfAlnum($value) {
        if (is_array($value))
            return array_map(array($this, 'toHalfAlnum'), $value);
        return mb_convert_kana($value, 'a', mb_internal_encoding());
    }

    /**
     * 英数字を全角に変換する
     *
     * @name toFullAlnum
     * @access public
     * @param mixed $value
     * @return mixed
     */
    function toFullAlnum($value) {
        if (is_array($value))
            return array_map(array($this, 'toFullAlnum'), $value);
        return mb_convert_kana($value, 'A', mb_internal_encoding());
    }

    /**
     * 数字を半角に変換する
     *
     * @name toHalfAlnum
     * @access public
     * @param mixed $value
     * @return mixed
     */
    function toHalfNum($value) {
        if (is_array($value))
            return array_map(array($this, 'toHalfNum'), $value);
        return mb_convert_kana($value, 'n', mb_internal_encoding());
    }

    /**
     * 数字を全角に変換する
     *
     * @name toFullAlnum
     * @access public
     * @param mixed $value
     * @return mixed
     */
    function toFullNum($value) {
        if (is_array($value))
            return array_map(array($this, 'toFullNum'), $value);
        return mb_convert_kana($value, 'N', mb_internal_encoding());
    }

    /**
     * 半角スペースを全角に変換する
     *
     * @name toFullSpace
     * @access public
     * @param mixed $value
     * @return mixed
     */
    function toFullSpace($value) {
        if (is_array($value))
            return array_map(array($this, 'toFullSpace'), $value);
        return mb_convert_kana($value, 'S', mb_internal_encoding());
    }

    /**
     * 全角スペースを半角に変換する
     *
     * @name toHalfSpace
     * @access public
     * @param mixed $value
     * @return mixed
     */
    function toHalfSpace($value) {
        if (is_array($value))
            return array_map(array($this, 'toHalfSpace'), $value);
        return mb_convert_kana($value, 's', mb_internal_encoding());
    }

    /**
     * スペースを削除する
     *
     * @name rmSpace
     * @access public
     * @param mixed $value
     * @return mixed
     */
    function rmSpace($value) {
        return r(' ', '', $this->toHalfSpace($value));
    }

    /**
     * アッパーキャメルケースに変換する
     *
     * @name toUcc
     * @access public
     * @param string $value アンダースコア方式の文字列
     * @return string 変換した値
     */
    function toUcc($value) {
        return implode('', array_map('ucfirst', explode('_', $value)));
    }

    /**
     * ローワーキャメルケースに変換する
     *
     * @name toLcc
     * @access public
     * @param string $value アンダースコア方式の文字列
     * @return string
     */
    function toLcc($value) {
        $tmp = $this->toUcc($value);
        $tmp[0] = strtolower($tmp[0]);
        return $tmp;
    }

    /**
     * 改行コードを[CR+LF]に統一
     *
     * @name eol2CrLf
     * @access public
     * @param mixed $value 変換したい値
     * @return string 変換した値
     */
    function eol2CrLf($value) {
        if (is_array($value))
            return array_map(array($this, 'eol2CrLf'), $value);
        return r("\n", "\r\n", $this->eol2Cr($value));
    }

    /**
     * 改行コードを[CR]に統一
     *
     * @name eol2Cr
     * @access public
     * @param mixed $value 変換したい値
     * @return mixed 変換した値
     */
    function eol2Cr($value) {
        if (is_array($value))
            return array_map(array($this, 'eol2Cr'), $value);
        return r("\n", "\r", r("\r\n", "\n", $value));
    }

    /**
     * 改行コードを[LF]に統一
     *
     * @name eol2Lf
     * @access public
     * @param mixed $value 変換したい値
     * @return mixed 変換した値
     */
    function eol2Lf($value) {
        if (is_array($value))
            return array_map(array($this, 'eol2Lf'), $value);
        return r("\r", "\n", r("\r\n", "\n", $value));
    }

    /**
     * 改行コードをOSに合わせて統一
     *
     * @name eol2SysEol
     * @access public
     * @param mixed $value 変換したい値
     * @return mixed 変換した値
     */
    function eol2SysEol() {
        if (is_array($value))
            return array_map(array($this, 'eol2SysEol'), $value);
        return (string) r(array("\r", "\n"), PHP_EOL, r("\r\n", "\n", $value));
    }

    /**
     * SJISに変換
     *
     * @name toSjis
     * @access public
     * @param mixed $value 変換したい値
     * @return mixed 変換した値
     */
    function toSjis($value) {
        if (is_array($value))
            return array_map(array($this, 'toSjis'), $value);
        return mb_convert_encoding($this->eol2CrLf($value), 'SJIS-win', mb_internal_encoding());
    }

    /**
     * EUCに変換
     *
     * @name toEuc
     * @access public
     * @param mixed $value 変換したい値
     * @return mixed 変換した値
     */
    function toEuc($value) {
        if (is_array($value))
            return array_map(array($this, 'toEuc'), $value);
        return mb_convert_encoding($this->eol2Lf($value), 'EUC-JP', mb_internal_encoding());
    }

    /**
     * UTF8に変換
     *
     * @name toUtf8
     * @access public
     * @param mixed $value
     * @return mixed
     */
    function toUtf8($value) {
        if (is_array($value))
            return array_map(array($this, 'toUtf8'), $value);
        return mb_convert_encoding($this->eol2Lf($value), 'UTF-8', mb_internal_encoding());
    }
}
?>