<?php
/**
 * Utils.php
 *　utilコンポーネント
 * @author 
 * @version 1.0.0
 * @since 2010/06
 */

class UtilsComponent extends Component {
    
    var $name ="utils";
    
    /**
     * csv出力
     * @param 配列,ファイル名
     * @return CSV出力
     */
    function publish_csv($data_array,$filename = null){
        
        //文字コード変換
        mb_convert_variables("SJIS-win","UTF-8",$data_array);
        
        if(is_null($filename)){
            $filename = "test.csv";
        }
        
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        
        if(count($data_array)) foreach($data_array as $content) {
            foreach($content as $value){
                echo $this->_array2csv($value)."\n";
            }
        }
        exit(0);
    }
    
    /**
     *　配列からCSVへ
     * @param 配列
     * @return SCVフォーマットの文字列
     */
    function _array2csv($array) {
        $contents = array();
        if (count($array)) foreach($array as $content) {
            $content = str_replace('"', '""', $content);
            if(preg_match('/[",]/', $content)) $content = '"'. $content. '"';
            $contents[] = $content;
        }
        return join(',', $contents);
    }
    
    
    /**
     * csv入力テスト用
     * @param
     * @return
     */
    function input_csv($csv_text){
        $csv_array = preg_split('/\r\n|[\r\n]/',$csv_text);
        foreach($csv_array as $content){
            $value = $this->_csv2array($content);
            $result[] = $value;
        }
        return $result;
    }
    
    /**
     *　CSVから配列へ
     * @param SCVフォーマットの文字列
     * @return 配列
     */
    function _csv2array($csv) {
        $array = array();
        $csv = preg_replace('/\r\n|[\r\n]/', '', $csv). ',';
        while(preg_match('/("[^"]*(?:""[^"]*)*"|[^,]*),/', $csv, $match)) {
            $array[] = str_replace('""', '"', preg_replace('/^"(.*)"$/', '$1', preg_replace('/^"(.*)"$/', '$1', $match[1])));
            $csv = preg_replace('/(?:"[^"]*(?:""[^"]*)*"|[^,]*),/', '', $csv, 1);
        }
        return $array;
    }
    
    /**
     * csv入力テスト用
     * @param
     * @return
     */
    function input_tsv($tsv_text){
        $tsv_array = preg_split('/\r\n|[\r\n]/',$tsv_text);
        foreach(str_replace('"','',$tsv_array) as $content){
            $value = explode("\t", $content);
            $result[] = $value;
        }
        return $result;
    }
}

?>
