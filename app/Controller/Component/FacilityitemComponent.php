<?php
/**
 * FacilityitemComponent
 *　施設採用品マスタメンテ用コンポーネント
 * @version 1.0.0
 * @since 2010/09/01
 */
class FacilityitemComponent extends Component {

    /**
     *
     * @var array $components
     */
    var $components = array('Session');

    /**
     *
     * @var mixed $c
     */
    var $c;

    /**
     * This component's startup method.
     * @access public
     * @param $controller
     */
    function startup(&$controller) {
        $this->c =& $controller;
    }

    /**
     * Setting values from Grandmaster item Object to each properties of Item Object.
     * @access public
     * @param array $_gramdmaster
     * @return array $_item
     */
    public function setItem ($_grandmaster = array()) {
        $_items = array();
        $_item['MstItem']['internal_code']        = (isset($_grandmaster['MstGrandmaster']['master_id']) && $_grandmaster['MstGrandmaster']['master_id'] != '') ? $_grandmaster['MstGrandmaster']['master_id']:'0';
        $_item['MstItem']['item_name']            = $_grandmaster['MstGrandmaster']['item_name'];
        $_item['MstItem']['standard']             = $_grandmaster['MstGrandmaster']['standard'];
        $_item['MstItem']['item_code']            = $_grandmaster['MstGrandmaster']['item_code'];
        $_item['MstItem']['dealer_code']          = $_grandmaster['MstGrandmaster']['dealer_code'];
        $_item['MstItem']['jan_code']             = $_grandmaster['MstGrandmaster']['jan_code'];
        $_item['MstItem']['mst_unit_name_id']     = 0;
        $_item['MstItem']['unit_price']           = $_grandmaster['MstGrandmaster']['unit_price'];
        $_item['MstItem']['refund_price']         = $_grandmaster['MstGrandmaster']['refund_price'];
        $_item['MstItem']['insurance_claim_type'] = $_grandmaster['MstGrandmaster']['insurance_claim_type'];
        $_item['MstItem']['insurance_claim_code'] = $_grandmaster['MstGrandmaster']['insurance_claim_code'];
        $_item['MstItem']['item_category1']       = $_grandmaster['MstGrandmaster']['item_category1'];
        $_item['MstItem']['item_category2']       = $_grandmaster['MstGrandmaster']['item_category2'];
        $_item['MstItem']['item_category3']       = $_grandmaster['MstGrandmaster']['item_category3'];
        $_item['MstItem']['biogenous_type']       = $_grandmaster['MstGrandmaster']['biogenous_type'];
        $_item['MstItem']['jmdn_code']            = $_grandmaster['MstGrandmaster']['jmdn_code'];
        $_item['MstItem']['class_separation']     = $_grandmaster['MstGrandmaster']['class_separation'];
        $_item['MstItem']['start_date']           =  (isset($_grandmaster['MstGrandmaster']['start_date'])) ? $_grandmaster['MstGrandmaster']['start_date'] : $_grandmaster['MstGrandmaster']['start_date'] = '1900/01/01';
        $_item['MstItem']['end_date']             = $_grandmaster['MstGrandmaster']['end_date'];
        $_item['MstItem']['master_id']            = $_grandmaster['MstGrandmaster']['master_id'];
        $_item['MstItem']['converted_num']        = 1;
        $_item['MstItem']['is_deleted']           = false;
        $_item['MstItem']['creater']              = $this->Session->read('Auth.MstUser.id');
        $_item['MstItem']['created']              = $this->c->data['now'];
        $_item['MstItem']['modifier']             = $this->Session->read('Auth.MstUser.id');
        $_item['MstItem']['modified']             = $this->c->data['now'];
        return $_item;
    }

    /**
     * Getting grandmaster unit name with master_id.
     * @param string $master_id
     * @return MstGrandmaster|bool
     */
    public function getGrandmasterItemUnit ($master_id) {
        $_params = $_aGrandmaster = array();
        $_params['fields'] = array('MstGrandmaster.unit_name');
        $_params['conditions']['MstGrandmaster.master_id'] = $master_id;
        $_params['recursive'] = -1;
        return $_aGrandmaster = $this->c->MstGrandmaster->find('first',$_params);
    }
}
// EOF