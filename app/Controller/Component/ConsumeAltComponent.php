<?php
/**
 * ConsumeAltComponent
 * 消費登録・消費履歴用コンポーネント ** ALTERNATIVE ***
 * @version 1.0.0
 * @since 2010/09/01
 */
class ConsumeAltComponent extends Component {

    /**
     *
     * @var array $components
     */
    var $components = array('Session','Common');

    /**
     *
     * @var mixed $c
     */
    var $c;
    
    /**
     * This component's startup method.
     * @access public
     * @param $controller
     */
    function startup(&$controller) {
        $this->c =& $controller;
    }
    
    /**
     * 有効期限警告チェック
     * @param string $_consumed
     * @param string $_expired
     * @param integer $_pre_date
     * @return bool
     */
    public function isExpiredWarningTerm ($_consumed = null, $_expired = null, $_pre_date = null) {
        if ($_expired == '' || is_null($_expired)) {
            return false;
        } else {
            return ( (integer)(strtotime($_expired) - ((integer)$_pre_date * 86400)) < strtotime($_consumed)) ? true:false;
        }
    }
    
    /**
     * 有効期限切れチェック
     * @param string $_consumed
     * @param string $_expired
     * @return bool
     */
    public function isOverExpiredDate ($_consumed = null, $_expired = null) {
        if ($_expired == '' || is_null($_expired)) {
            return false;
        } else {
            return ( strtotime($_consumed) > strtotime($_expired)) ? true:false;
        }
    }
}// EOF
?>