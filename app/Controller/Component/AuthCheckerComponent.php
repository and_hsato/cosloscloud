<?php
/**
 * ログインしているユーザの権限をチェックするコンポーネント
 * 
 * @version 0.0.1
 * @since 2010/09/07
 */
class AuthCheckerComponent extends Component {
    var $controller = null;
    var $center_type = 1;
    var $hospital_type= 2;
    
    function startup(&$controller)
      {
          $this->controller = &$controller;
          $this->center_type = Configure::read('FacilityType.center');
          $this->hospital_type= Configure::read('FacilityType.hospital');
      }
    
    function isCenter() {
        return $this->__getUserFacility() == $this->center_type;
    }
    
    function isHospital() {
        return $this->__getUserFacility() == $this->hospital_type;
    }
    
    function isSystemUser() {
        $mstRole = $this->__getUserRole();
        if (is_null($mstRole)) return false;
        return Configure::read('role.type.system') == $mstRole['mst_role_type_id'];
    }
    
    function isEdiUser() {
        $mstRole = $this->__getUserRole();
        if (is_null($mstRole)) return false;
        return Configure::read('role.type.edi') == $mstRole['mst_role_type_id'];
    }
    
    function isLiteUser() {
        $mstRole = $this->__getUserRole();
        if (is_null($mstRole)) return false;
        return Configure::read('role.type.lite') == $mstRole['mst_role_type_id'];
    }
    
    function isCatalogUser() {
        $mstRole = $this->__getUserRole();
        if (is_null($mstRole)) return false;
        return Configure::read('role.type.catalog') == $mstRole['mst_role_type_id'];
    }
    
    
    static function hasParentUserBy($mstUser) {
        return !empty($mstUser['parent_id']);
    }
    
    static function isParentUserBy($mstUser) {
        return !self::hasParentUserBy($mstUser);
    }
    
    static function isChildUserBy($mstUser) {
        return !self::isParentUserBy($mstUser);
    }
    
    
    private function __getUserRole() {
        return $this->controller->Session->read('Auth.MstRole');
    }
    
    private function __getUserFacility() {
        $loginuser_facility_type = -1;
        if( $this->controller->Session->check('Auth.facility_type_selected') )
          $loginuser_facility_type = $this->controller->Session->read('Auth.facility_type_selected');
        return $loginuser_facility_type;
    }
    
}