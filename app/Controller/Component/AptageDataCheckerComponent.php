<?php
/**
 * アプテージデータを色々チェックするコンポーネント
 * 指定桁数は、基本的にはDBの桁数に準ずる
 * @version 0.0.1
 * @since 2015/01/23
 */
class AptageDataCheckerComponent extends Component {

    // 呼び出し元コントローラ
    var $controller;

    function initialize(&$controller) {
        $this->controller =& $controller;
    }

    function startup() {
    }

    /**
     * 得意先コードチェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isFacilityCode($facility_code) {
        return $this->_isFormat($facility_code, 20);
    }

    /**
     * 得意名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isFacilityName($facility_name) {
        return $this->_isFormat($facility_name, 100);
    }

    /**
     * 仕入先コードチェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isSupplierCode($supplier_code) {
        return $this->_isFormat($supplier_code, 20);
    }

    /**
     * 仕入先名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isSupplierName($supplier_code) {
        return $this->_isFormat($supplier_code, 100);
    }

    /**
     * 製品番号チェック
     * DBはmst_facility_items.spare_key2に合わせてVARCHAR(50)だが、8桁でチェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isItemCode($item_code) {
        return $this->_isFormat($item_code, 8, 'numeric');
    }

    /**
     * 商品名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isItemName($item_name) {
        return $this->_isFormat($item_name, 100);
    }

    /**
     * 規格チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isStandard($standard) {
        return $this->_isFormat($standard, 100);
    }

    /**
     * メーカコードチェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isMakerCode($maker_code) {
        return $this->_isFormat($maker_code, 50);
    }

    /**
     * メーカ名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isMakerName($maker_name) {
        return $this->_isFormat($maker_name, 100);
    }

    /**
     * メーカ品番チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isMakerItemNumber($maker_item_number) {
        return $this->_isFormat($maker_item_number, 50);
    }

    /**
     * 入数ランクチェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isPerUnitRank($per_unit_rank) {
        return $this->_isFormat($per_unit_rank, 10, 'numeric');
    }

    /**
     * 入数名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isPerUnitName($per_unit_name) {
        return $this->_isFormat($per_unit_name, 20);
    }

    /**
     * 単位コードチェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isUnitCode($unit_code) {
        return $this->_isFormat($unit_code, 20, 'numeric');
    }

    /**
     * 単位名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isUnitName($unit_name) {
        return $this->_isFormat($unit_name, 10);
    }

    /**
     * 売上単価チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isSalesPrice($sales_price) {
        return $this->_isFormat($sales_price, 0, 'numeric');
    }

    /**
     * 仕入単価チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isPurchasePrice($purchase_price) {
        return $this->_isFormat($purchase_price, 0, 'numeric');
    }

    /**
     * 補償単価チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isCompensationPrice($compensation_price) {
        return $this->_isFormat($compensation_price, 0, 'numeric');
    }

    /**
     * 補償区分チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isCompensationType($compensation_type) {
        return $this->_isFormat($compensation_type, 1, 'numeric');
    }

    /**
     * 補償区分名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isCompensationName($compensation_name) {
        return $this->_isFormat($compensation_name, 20);
    }

    /**
     * 単価契約区分チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isContractType($contract_type) {
        return $this->_isFormat($contract_type, 1);
    }

    /**
     * 単価契約区分名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isContractName($contract_name) {
        return $this->_isFormat($contract_name, 20);
    }

    /**
     * 単価契約番号チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isContractNumber($contract_number) {
        return $this->_isFormat($contract_number, 50);
    }

    /**
     * 短縮製品番号チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isShortItemCode($short_item_code) {
        return $this->_isFormat($short_item_code, 30);
    }

    /**
     * 相手先製品番号チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isDestItemCode($dest_item_code) {
        return $this->_isFormat($dest_item_code, 50);
    }

    /**
     * 相手先商品名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isDestItemName($dest_item_name) {
        return $this->_isFormat($dest_item_name, 100);
    }

    /**
     * 相手先規格チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isDestStandard($dest_standard) {
        return $this->_isFormat($dest_standard, 100);
    }

    /**
     * 相手先メーカ名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isDestMakerName($dest_maker_name) {
        return $this->_isFormat($dest_maker_name, 100);
    }

    /**
     * 相手先入数名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isDestPerUnitName($dest_per_unit_name) {
        return $this->_isFormat($dest_per_unit_name, 100);
    }

    /**
     * 相手先単位コードチェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isDestUnitCode($dest_unit_code) {
        return $this->_isFormat($dest_unit_code, 20, 'numeric');
    }

    /**
     * 相手先単位名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isDestUnitName($dest_unit_name) {
        return $this->_isFormat($dest_unit_name, 20);
    }

    /**
     * 実績担当者コードチェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isResultPersonCode($result_person_code) {
        return $this->_isFormat($result_person_code, 20);
    }

    /**
     * 実績担当者名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isResultPersonName($result_person_name) {
        return $this->_isFormat($result_person_name, 100);
    }

    /**
     * 売上_実績報告用仕入先コードチェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isSalesSupplierCode($sales_supplier_code) {
        return $this->_isFormat($sales_supplier_code, 20);
    }

    /**
     * 売上_実績報告用仕入先名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isSalesSupplierName($sales_supplier_name) {
        return $this->_isFormat($sales_supplier_name, 100);
    }

    /**
     * 売上_備考チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isSalesNote($sales_note) {
        return $this->_isFormat($sales_note, 200);
    }

    /**
     * 仕入_実績報告用仕入先コードチェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isPurchaseSupplierCode($purchase_supplier_code) {
        return $this->_isFormat($purchase_supplier_code, 20);
    }

    /**
     * 仕入_実績報告用仕入先名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isPurchaseSupplierName($purchase_supplier_name) {
        return $this->_isFormat($purchase_supplier_name, 100);
    }

    /**
     * 仕入_備考チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isPurchaseNote($purchase_note) {
        return $this->_isFormat($purchase_note, 200);
    }

    /**
     * 遡及処理区分チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isRetroactiveType($retroactive_type) {
        return $this->_isFormat($retroactive_type, 1, 'numeric');
    }

    /**
     * 遡及処理区分名チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isRetroactiveName($retroactive_name) {
        return $this->_isFormat($retroactive_name, 20);
    }

    /**
     * 開始遡及処理日付チェック
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    function isRetroactiveDate($retroactive_date) {
        //return $this->_isFormat($retroactive_date, 0, 'date');
    }

    /**
     * フォーマットチェック
     * @param $val        チェック対象
     * @param $size       桁数
     * @param $check_type 個別タイプ
     * @return Array ['RESULT'] => boolean, ['MESSAGE'] => String
     */
    private function _isFormat($val, $size=null, $check_type=null) {
        $result = array('RESULT'  => false, 'MESSAGE' => '');

        if($val == null || $val == '') {
          $result['RESULT'] = true;
          return $result;
        }

        if(empty($check_type) && empty($size)) {
            $result['MESSAGE'] = "システムエラーが発生しました。";
            return $result;
        }

        // 桁数
        if($size > 0) {
          $length = mb_strlen($val);
          if($length > $size) {
              $result['MESSAGE'] = "桁数超過しました。現在の桁数：[" . $length . "], " . $size ."桁まで登録可能です。";
              return $result;
          }
        }

        switch ($check_type) {
          case 'numeric':
            // 数値？
            if(!is_numeric($val)) {
              $result['MESSAGE'] = "数値フォーマットではありません。";
              return $result;
            }
            break;
          case 'string':
            break;
          case 'date':
            break;
          default:
            break;
        }

        $result['RESULT'] = true;
        return $result;
    }

    //アプテージ連携 発注時エラーチェック
    function order($cond){
        if(empty($cond) || count($cond) == 0){
            $res['MESSAGE'] = "＜情報が不足しています＞";
            return $res;
        }

        $res           = array('RESULT' => false, 'MESSAGE' => '');
        $aptage_code   = $cond['aptage_item_code'];
        $facility_code = $cond['aptage_facility_code'];
        $unit_name     = $cond['aptage_unit_name'];
        $is_lowlevel   = $cond['is_lowlevel'];
        $order_type    = $cond['order_type'];
        $aptage_items  = $this->controller->aptage_items;

        //低レベル品はチェック対象外
        if($is_lowlevel == false && $order_type != Configure::read('OrderTypes.replenish') ){
            //アプテージコードが空の場合NG
            if(empty($aptage_code)){
                $res['MESSAGE'] = "＜アプテージコードが空です＞";
                return $res;
            }

            //アプテージコードの存在チェック
            if(!isset($aptage_items[$facility_code][$aptage_code])){
                $res['MESSAGE'] = "＜アプテージ取込データに存在しないアプテージコードです＞";
                return $res;
            }

            //単位チェック
            $import_unit_name = $aptage_items[$facility_code][$aptage_code];
            if($unit_name != $import_unit_name){
                $res['MESSAGE'] = "＜アプテージ取込データと単位が違います＞";
                return $res;
            }
        }
        $res['RESULT'] = true;
        return $res;
    }
}