<?php
/**
 * ConsumeComponent
 * 消費登録・消費履歴用コンポーネント
 * @version 1.0.0
 * @since 2010/09/01
 */
class ConsumeComponent extends Component {

    /**
     *
     * @var array $components
     */
    var $components = array('Session','ConsumeAlt');

    /**
     *
     * @var mixed $c
     */
    var $c;

    /** constants of this class. */
    const IS_SALE  = 2;
    const IS_STOCK = 1;

    /**
     * This component's startup method.
     * @access public
     * @param $controller
     */
    function startup(&$controller) {
        $this->c =& $controller;
    }
}
?>
