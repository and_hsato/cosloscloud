<?php
/**
 * csv_write_utils.php
 * CsvWriteUtilsコンポーネント
 * @author 
 * @version 1.0.0
 * @since 2011/01/01
 */

class CsvWriteUtilsComponent extends Component {

    private $_fileName     = "";              // ファイル名
    private $_lineFeedCd   = "";              // 改行コード  \r\n \n 
    private $_quote        = "\"";            // 囲み文字    \" 
    private $_delimiter    = "";              // 区切り文字  , \t
    private $_charCd       = "";              // 文字コード(CSV文字コード)  SJIS-WIN(SJIS) EUCJP-WIN(EUC-JP) UTF-8

    private $_fields       = array();         // csv格納用配列

    //*************************************************************************
    //* セッター
    //*************************************************************************
    public function setFileName($pVal)          { $this->_fileName = $pVal; }                              // ファイル名
    public function setLineFeedCd($pVal)        { $this->_lineFeedCd = $pVal; }                            // 改行コード
    public function setQuote($pVal)             { $this->_quote = $pVal; }                                 // 囲み文字
    public function setDelimiter($pVal)         { $this->_delimiter = $pVal; }                             // 区切り文字
    public function setCharCd($pVal) {                                                                     // 文字コード(出力文字コード)
        // SJIS → SJIS-win(文字化け対策)
        if ( preg_match( "/^SJIS$/i" , $pVal ) ) {
            $pVal = "SJIS-win";
        }
        // EUC-JP → eucJP-win(文字化け対策)
        if ( preg_match( "/^EUC-JP$/i" , $pVal ) ) {
            $pVal = "eucJP-win";
        }
        $this->_charCd = $pVal;
    }

    //*************************************************************************
    //* ゲッター
    //*************************************************************************
    public function getFileName()               { return $this->_fileName; }                               // ファイル名
    public function getLineFeedCd()             { return $this->_lineFeedCd; }                             // 改行コード
    public function getQuote()                  { return $this->_quote; }                                  // 囲み文字
    public function getDelimiter()              { return $this->_delimiter; }                              // 区切り文字
    public function getCharCd()                 { return $this->_charCd; }                                 // 文字コード(出力文字コード)

    //*************************************************************************
    //* CSV開始処理
    //*************************************************************************
    public function open() {
        // 初期設定
        if ( $this->_lineFeedCd == "" ) { $this->_lineFeedCd = "\r\n"; }
        if ( $this->_quote == "" )      { $this->_quote      = "";   }
        if ( $this->_delimiter == "" )  { $this->_delimiter  = ",";    }
        if ( $this->_charCd == "" )     { $this->_charCd     = "SJIS-win"; }

        $wFileName = $this->convertCharCd( $this->_fileName );

        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"" . $wFileName . "\"");
    }

    //*************************************************************************
    //* CSV終了処理
    //*************************************************************************
    public function close() {
    
    }

    //*************************************************************************
    //* 項目追加
    //*************************************************************************
    public function fieldAdd($pField) {
        array_push($this->_fields,$pField);
    }

    //*************************************************************************
    //* 項目クリア
    //*************************************************************************
    public function fieldClear() {
        $this->_fields = array();
    }

    //*************************************************************************
    //* 一行出力
    //*************************************************************************
    public function writeLine(){
        $wFirstFlag = true;

        // フィールドループ
        $wLineData = "";
        foreach ($this->_fields As $wField) {
            // 区切り文字付加
            if ( $wFirstFlag ) {
                $wFirstFlag = false;
            } else {
                $wLineData .= $this->_delimiter;
            }

            // 囲み文字付加＆文字コード変換
            $wLineData .= $this->_quote . $this->convertCharCd( $wField ) . $this->_quote;
        }

        // 改行コード
        $wLineData .= $this->_lineFeedCd;

        print $wLineData;
    }

    //*************************************************************************
    //* 一行出力 $pArr(3次元配列まで対応)
    //*************************************************************************
    public function writeLineArray($pArr){

        $_dimension = $this->arrayDimension($pArr);

        switch ( $_dimension ) {
            // 1次元配列
            case 1:
                $this->fieldClear();
                foreach ( $pArr As $data1) {
                    $this->fieldAdd($data1);
                }
                $this->writeLine();
                break;
            // 2次元配列
            case 2:
                foreach ( $pArr As $data2) {
                    $this->fieldClear();
                    foreach( $data2 As $data1) {
                        $this->fieldAdd($data1);
                    }
                    $this->writeLine();
                }
                break;
            // 3次元配列
            case 3:
                foreach( $pArr As $data3) {
                    foreach( $data3 As $data2 ) {
                        $this->fieldClear();
                        foreach( $data2 As $data1 ) {
                            $this->fieldAdd($data1);
                        }
                        $this->writeLine();
                    }
                }
                break;
            default:
                break;
        }
    }

    //*************************************************************************
    //* 配列次元数取得 $array:配列 $cnt:再帰で利用する(外部から引数設定禁止)
    //*************************************************************************
    private function arrayDimension($array,$cnt=0){
        if (is_array($array)) {
            $cnt++;
        } else {
            return $cnt;
        }

        $max = $cnt;
        $i = 0;
        foreach ($array as $w_array) {
            if(is_array($w_array)) {
                $i = $this->arrayDimension($w_array, $cnt);
                if ($i > $max) {$max = $i;}
            }
        }

        return $max;

    }

    //*************************************************************************
    //* 文字コード変換
    //*************************************************************************
    private function convertCharCd($pStr) {
        if ( $this->_charCd == "" ) {
            return $pStr;
        } else {
            return mb_convert_encoding($pStr,$this->_charCd,"UTF-8");
        }
    }
}

?>
