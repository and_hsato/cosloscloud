<?php
class PdfReportComponent extends Component {

    const ACTION_ORDER    = 'order';
    const ACTION_STICKER  = 'sticker';
    const ACTION_CLAIM    = 'claim';
    const ACTION_SHIPPING = 'shipping';
    const ACTION_COMMON   = 'common';
    const VIEW_PREFIX     = '/PdfReport/_';


    /**
     *
     * @var array $components
     */
    var $components = array('AuthSession');

    var $_controller;

    public function startup(&$controller) {
        $this->_controller = $controller;
    }

    /**
     * 発注書をPDF形式で出力する。
     *
     * @param array $trnOrderHeaderIds 出力対象のキーとなるTrnOrderHeaderのIDの配列。
     * @return byte[] PDF形式のバイナリデータ、失敗した場合はfalseを返す。
     */
    public function getPdfDataOfOrder($trnOrderHeaderIds) {
        $data = $this->findOrderData($trnOrderHeaderIds);
        if(empty($data)) return false;

        // 取得したデータを発注番号毎にまとめる
        // データの構造は他のViewと同じになるようにあわせている
        $dataByWorkNo = array();
        foreach ($data as $i => $d) {
            $workNo = $d[0]['work_no'];
            $dataByWorkNo[$workNo]['dataList'][] = $d;

            // 合計金額を計算
            if (empty($dataByWorkNo[$workNo]['totalPrice'])) {
                $dataByWorkNo[$workNo]['totalPrice'] = $d[0]['claim_price'];
            } else {
                $dataByWorkNo[$workNo]['totalPrice'] += $d[0]['claim_price'];
            }
        }

        // View用にデータをセット
        $title = '発注書';
        $this->_controller->set('title' , $title);

        // 以下、PDF出力処理
        $pdf = $this->createPdfObjectForReport('A4', 'P', 84);

        // 発注番号毎に出力
        $count = 0;
        foreach ($dataByWorkNo as $workNo => $d) {
            $count++;
            $this->_controller->set('data', $d['dataList']);
            $this->_controller->set('totalPrice', $d['totalPrice']);

            // ヘッダ
            $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_ORDER . '_header', '');
            $pdf->SetHTMLHeader($response->body());

            // フッタ
            $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_COMMON . '_footer', '');
            $pdf->SetHTMLFooter($response->body());

            // 改ページ
            $pdf->AddPage('', '', 1, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '');

            // 1件目の場合のみCSSを出力
            if ($count == 1) {
                // CSS
                $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_COMMON . '_css', '');
                $pdf->writeHTML($response->body());
                $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_ORDER . '_css', '');
                $pdf->writeHTML($response->body());
            }

            // データ
            $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_ORDER . '_data', '');
            $pdf->writeHTML($response->body());

            // 合計
            $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_COMMON . '_total', '');
            $pdf->writeHTML($response->body());
        }

        // PDF出力画面左上のタイトル
        $subject = $title;
        $pdf->SetTitle($subject);

        // ファイル保存時のファイルのプロパティー上のサブタイトル
        $pdf->SetSubject($subject);

        // ファイル名
        $fileName = str_replace(' ', '_', $subject) . '.pdf';
        header('Content-disposition: inline; filename="' . $fileName . '"');
        $buffer = $pdf->Output($fileName, 'S'); // mode = 'S' (バッファーデータ取得)の場合、ファイル名は無視される。
        return $buffer;
    }

    /**
     * 出荷伝票をPDF形式で出力する。
     *
     * @param array $trnShippingHeaderIds 出力対象のキーとなるTrnShippingHeaderのIDを配列の配列。
     * @return byte[] PDF形式のバイナリデータ、失敗した場合はfalseを返す。
     */
    public function getPdfDataOfShipping($trnShippingHeaderIds) {
        $data = $this->findShippingData($trnShippingHeaderIds);
        if(empty($data)) return false;

        // 取得したデータを発注番号毎にまとめる
        // データの構造は他のViewと同じになるようにあわせている
        $dataByWorkNo = array();
        foreach ($data as $i => $d) {
            $workNo = $d[0]['work_no'];
            $dataByWorkNo[$workNo]['dataList'][] = $d;

            // 合計金額を計算
            if (empty($dataByWorkNo[$workNo]['totalPrice'])) {
                $dataByWorkNo[$workNo]['totalPrice'] = $d[0]['claim_price'];
            } else {
                $dataByWorkNo[$workNo]['totalPrice'] += $d[0]['claim_price'];
            }
        }
        $this->_controller->set('data', $data);

        // View用にデータをセット
        $title = '出荷伝票';
        $this->_controller->set('title' , $title);

        // 以下、PDF出力処理
        $pdf = $this->createPdfObjectForReport('A4', 'P', 82);

        // 発注番号毎に出力
        $count = 0;
        foreach ($dataByWorkNo as $workNo => $d) {
            $count++;
            $this->_controller->set('data', $d['dataList']);
            $this->_controller->set('totalPrice', $d['totalPrice']);

            // ヘッダ
            $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_SHIPPING . '_header', '');
            $pdf->SetHTMLHeader($response->body());

            // フッタ
            $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_COMMON . '_footer', '');
            $pdf->SetHTMLFooter($response->body());

            // 改ページ
            $pdf->AddPage('', '', 1, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '');

            // 1件目の場合のみCSSを出力
            if ($count == 1) {
                // CSS
                $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_COMMON . '_css', '');
                $pdf->writeHTML($response->body());
                $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_SHIPPING . '_css', '');
                $pdf->writeHTML($response->body());
            }

            // データ
            $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_SHIPPING . '_data', '');
            $pdf->writeHTML($response->body());

            // 合計
            $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_COMMON . '_total', '');
            $pdf->writeHTML($response->body());
        }

        // PDF出力画面左上のタイトル
        $subject = $title;
        $pdf->SetTitle($subject);

        // ファイル保存時のファイルのプロパティー上のサブタイトル
        $pdf->SetSubject($subject);

        // ファイル名
        $fileName = str_replace(' ', '_', $subject) . '.pdf';
        header('Content-disposition: inline; filename="' . $fileName . '"');
        $buffer = $pdf->Output($fileName, 'S'); // mode = 'S' (バッファーデータ取得)の場合、ファイル名は無視される。
        return $buffer;
    }

    /**
     * 部署シールをPDF形式で出力する。
     *
     * @param array $trnShippingHeaderIds 出力対象のキーとなるTrnStickerのIDの配列。
     * @return byte[] PDF形式のバイナリデータ、失敗した場合はfalseを返す。
     */
    public function getPdfDataOfSticker($trnStickerIds) {
        $data = $this->findStickerData($trnStickerIds);
        if(empty($data)) return false;

        $truncateKeyLenMap = array(
                'insurance_claim' => 14,
        );
        foreach ($data as &$d) {
            $keyValues = $d[0];
            $d[0] = self::truncateStr($keyValues, $truncateKeyLenMap);
        }
        unset($d);

        // 以下、PDF出力処理
        $pdf = $this->createPdfObjectForSticker(array(52, 80), 'L');

        // PDF出力画面左上のタイトル
        $title = '部署シール';
        $pdf->SetTitle($title);

        // ファイル保存時のファイルのプロパティー上のサブタイトル
        $subject = $title;
        $pdf->SetSubject($subject);

        // CSS
        $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_COMMON . '_css', '');
        $pdf->writeHTML($response->body());
        $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_STICKER . '_css', '');
        $pdf->writeHTML($response->body());

        // データ
        $this->_controller->set('data' , $data);
        $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_STICKER . '_data', '');
        $pdf->writeHTML($response->body());

        // ファイル名
        $fileName = $subject . '.pdf';
        header('Content-disposition: inline; filename="' . $fileName . '"');
        $buffer = $pdf->Output($fileName, 'S'); // mode = 'S' (バッファーデータ取得)の場合、ファイル名は無視される。
        return $buffer;
    }

    /**
     * 指定された請求明細IDの請求明細をPDF形式で出力する。
     *
     * @param array $facilityId 出力対象のキーとなる施設IDの配列。
     * @param string $facilityName 出力対象の施設名称。
     * @param string $loginUserName 出力対象の請求先担当者名
     * @param string $trnClaimSpecificationId 出力対象の請求書id
     * @return byte[] PDF形式のバイナリデータ、失敗した場合はfalseを返す。
     */
    public function getPdfDataOfClaim($facilityName, $loginUserName, $trnClaimSpecificationId) {
        $data = $this->findClaimSpecification($trnClaimSpecificationId);
        if (isset($data) === false || isset($data[0]) === false || isset($data[0][0]) === false){ return false; }

        // 以下、PDF出力処理
        $pdf = $this->createPdfObjectForReport('A4', 'P', 24);

        // View用にデータをセット
        $title = '請求書';
        $this->_controller->set('title' , $title);
        $pdf->SetTitle($title);
        $pdf->SetSubject($title);

        // ヘッダ
        $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_CLAIM . '_header', '');
        $pdf->SetHTMLHeader($response->body());

        // フッタ
        $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_COMMON . '_footer', '');
        $pdf->SetHTMLFooter($response->body());

        // CSS
        $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_COMMON . '_css', '');
        $pdf->writeHTML($response->body());
        $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_CLAIM . '_css', '');
        $pdf->writeHTML($response->body());

        // データ　連絡先
        $closing_date = $this->_controller->DateUtil->getJpDate(strtotime($data[0][0]['closing_date']), false);
        $this->_controller->set('data' , $data);
        $this->_controller->set('closing_date' , $closing_date);
        $this->_controller->set('loginUserName', $loginUserName);
        $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_CLAIM . '_address', '');
        $pdf->writeHTML($response->body());

        // データ　料金項目
        $last_time_purchase_in_tax = isset($data[0][0]['last_time_id']) ? $data[0][0]['last_time_purchase_in_tax'] + $data[0][0]['last_time_commission_in_tax'] + $data[0][0]['last_time_extension_in_tax'] : 0;
        $last_time_payment_track_record_in_tax = isset($data[0][0]['last_time_id']) ? $data[0][0]['last_time_payment_track_record_in_tax'] : 0;
        $this->_controller->set('data' , $data);
        $this->_controller->set('last_time_purchase_in_tax' , $last_time_purchase_in_tax);
        $this->_controller->set('last_time_payment_track_record_in_tax' , $last_time_payment_track_record_in_tax);
        $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_CLAIM . '_fee', '');
        $pdf->writeHTML($response->body());

        // データ
        $deadline_date = $this->_controller->DateUtil->getJpDate(strtotime($data[0][0]['deposit_plans_date']));
        $slip_no       = date("Ymd", strtotime($data[0][0]['closing_date'])).sprintf('%05d', $data[0][0]['billing_facility_id']);
        $this->_controller->set('data' , $data);
        $this->_controller->set('slip_no' , $slip_no);
        $this->_controller->set('slip_date' , date("Y/m/d", strtotime($data[0][0]['closing_date'])));
        $this->_controller->set('deadline_date' , $deadline_date);
        $this->_controller->set('sum_purchase_price', $data[0][0]['purchase_price'] + $data[0][0]['commission'] + $data[0][0]['extension_price']);
        $this->_controller->set('total_price', $data[0][0]['purchase_price'] + $data[0][0]['commission'] + $data[0][0]['extension_price'] + $data[0][0]['total_tax']);
        $response = $this->_controller->render(self::VIEW_PREFIX . self::ACTION_CLAIM . '_data', '');
        $pdf->writeHTML($response->body());

        // ファイル名
        $buffer = $pdf->Output("", 'S'); // mode = 'S' (バッファーデータ取得)の場合、ファイル名は無視される。
        return $buffer;
    }


// 以下、非公開メソッド
// 以下、各種データ検索用のメソッド
    private function findOrderData($trnOrderHeaderIds) {
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                // 取得するフィールド
                'fields' => array(
                        '"TrnOrderHeader"."work_no"                   AS "work_no"',                // 発注番号
                        '"MstDepartmentFrom"."department_formal_name" AS "department_formal_name"', // 送付元部署名
                        '"MstFacilityFrom"."facility_formal_name"     AS "facility_formal_name"',   // 送付元施設名
                        '"MstFacilityFrom"."zip"                      AS "zip"',                    // 送付元施設郵便番号
                        '"MstFacilityFrom"."address"                  AS "address"',                // 送付元施設住所
                        '"MstFacilityFrom"."tel"                      AS "tel"',                    // 送付元施設電話番号
                        '"MstFacilityFrom"."fax"                      AS "fax"',                    // 送付元施設FAX番号
                        '"MstFacilityTo"."facility_formal_name"       AS "supplier_name"',          // 送付先業者名
                        '"MstFacilityTo"."zip"                        AS "supplier_zip"',           // 送付先業者郵便番号
                        '"MstFacilityTo"."address"                    AS "supplier_address"',       // 送付先施設住所
                        '"MstFacilityTo"."tel"                        AS "supplier_tel"',           // 送付先業者電話番号
                        '"MstFacilityTo"."fax"                        AS "supplier_fax"',           // 送付先業者FAX番号
                        '"MstFacilityItem"."internal_code"            AS "internal_code"',          // 商品ID
                        '"MstFacilityItem"."item_name"                AS "item_name"',              // 商品名
                        '"MstFacilityItem"."item_code"                AS "item_code"',              // 商品コード
                        '"MstFacilityItem"."standard"                 AS "standard"',               // 規格
                        '"MstDealer"."dealer_name"                    AS "dealer_name"',            // 販売元
                        '"TrnOrder"."stocking_price"                  AS "stocking_price"',         // 単価
                        '"TrnOrder"."quantity"                        AS "quantity"',               // 数量
                        '"TrnOrder"."is_deleted"                      AS "is_deleted"',             // 削除済みフラグ
                        'round("TrnOrder"."stocking_price" * "TrnOrder"."quantity") AS "claim_price"', // 金額
                        'to_char("TrnOrderHeader"."work_date", \'YYYY年MM月DD日\') AS "work_date"', // 発注日時
                        self::getUnitNameSqlFieldStr() .            ' AS "unit_name"',              // 包装単位
                ),
                // 検索条件
                'conditions' => array(
                        'TrnOrderHeader.id' => $trnOrderHeaderIds
                ),
                // 連結するモデル
                'joins' => array(
                        array(
                                'table' => 'trn_orders',
                                'alias' => 'TrnOrder',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'TrnOrder.trn_order_header_id = TrnOrderHeader.id',
                                ),
                        ),
                        array(
                                'table' => 'mst_item_units',
                                'alias' => 'MstItemUnit',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstItemUnit.id = TrnOrder.mst_item_unit_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_unit_names',
                                'alias' => 'MstUnitName',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstUnitName.id = MstItemUnit.mst_unit_name_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_unit_names',
                                'alias' => 'MstUnitName2',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstUnitName2.id = MstItemUnit.per_unit_name_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_facility_items',
                                'alias' => 'MstFacilityItem',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstFacilityItem.id = MstItemUnit.mst_facility_item_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_dealers',
                                'alias' => 'MstDealer',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstDealer.id = MstFacilityItem.mst_dealer_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_departments',
                                'alias' => 'MstDepartmentFrom',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstDepartmentFrom.id = TrnOrder.department_id_from',
                                ),
                        ),
                        array(
                                'table' => 'mst_facilities',
                                'alias' => 'MstFacilityFrom',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstFacilityFrom.id = MstDepartmentFrom.mst_facility_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_departments',
                                'alias' => 'MstDepartmentTo',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstDepartmentTo.id = TrnOrder.department_id_to',
                                ),
                        ),
                        array(
                                'table' => 'mst_facilities',
                                'alias' => 'MstFacilityTo',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstFacilityTo.id = MstDepartmentTo.mst_facility_id',
                                ),
                        ),
                ),
                // 並び順
                'order' => array(
                        'TrnOrder.trn_order_header_id',
                        'MstFacilityFrom.id',
                        'MstDealer.dealer_name',
                        'MstFacilityItem.item_name',
                        'MstFacilityItem.item_code',
                ),
        );

        // 検索実行
        $data = $this->_controller->TrnOrderHeader->find('all', $options, !$this->AuthSession->isEdiUserRole());

        return $data;
    }

    private function findStickerData($trnStickerIds) {
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                // 取得するフィールド
                'fields' => array(
                        '"MstFacility"."facility_name"                AS "facility_name"',       // 施設名
                        '"MstDepartment"."department_name"            AS "department_name"',     // 部署名
                        '"MstFacilityItem"."internal_code"            AS "internal_code"',       // 商品ID
                        '"MstFacilityItem"."item_name"                AS "item_name"',           // 商品名
                        '"MstFacilityItem"."standard"                 AS "standard"',            // 規格
                        '"MstFacilityItem"."item_code"                AS "item_code"',           // 商品コード
                        '"MstDealer"."dealer_name"                    AS "dealer_name"',         // 販売元
                        '"MstFacilityItem"."unit_price"               AS "unit_price"',          // 定価単価
                        '"TrnSticker"."hospital_sticker_no"           AS "hospital_sticker_no"', // 病院シール番号
                        '"TrnSticker"."validated_date"                AS "validated_date"',      // 有効期限
                        '"TrnSticker"."lot_no"                        AS "lot_no"',              // ロット番号
                        '"MstInsuranceClaim"."insurance_claim_name_s" AS "insurance_claim"',     // 保険請求
                        '"MstConst"."const_nm"                        AS "biogenous"',           // 生物由来
                        'to_char("TrnSticker"."validated_date", \'YYYY年MM月DD日\') AS "validated_date"', // 発注日時
                        self::getUnitNameSqlFieldStr() .            ' AS "unit_name"',           // 包装単位
                ),
                // 検索条件
                'conditions' => array(
                        'TrnSticker.id' => $trnStickerIds
                ),
                // 連結するモデル
                'joins' => array(
                        array(
                                'table' => 'mst_departments',
                                'alias' => 'MstDepartment',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstDepartment.id = TrnSticker.mst_department_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_facilities',
                                'alias' => 'MstFacility',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstFacility.id = MstDepartment.mst_facility_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_item_units',
                                'alias' => 'MstItemUnit',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstItemUnit.id = TrnSticker.mst_item_unit_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_unit_names',
                                'alias' => 'MstUnitName',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstUnitName.id = MstItemUnit.mst_unit_name_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_unit_names',
                                'alias' => 'MstUnitName2',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstUnitName2.id = MstItemUnit.per_unit_name_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_facility_items',
                                'alias' => 'MstFacilityItem',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstFacilityItem.id = MstItemUnit.mst_facility_item_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_dealers',
                                'alias' => 'MstDealer',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstDealer.id = MstFacilityItem.mst_dealer_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_insurance_claims',
                                'alias' => 'MstInsuranceClaim',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstInsuranceClaim.insurance_claim_code = MstFacilityItem.insurance_claim_code',
                                ),
                        ),
                        array(
                                'table' => 'mst_consts',
                                'alias' => 'MstConst',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstConst.const_group_cd = \'' . Configure::read('ConstGroupType.biogenousTypes') . '\'',
                                        'MstConst.const_cd = MstFacilityItem.biogenous_type',
                                ),
                        ),
                ),
                // 並び順
                'order' => array(
                        'TrnSticker.hospital_sticker_no',
                ),
        );

        // 検索実行
        $data = $this->_controller->TrnSticker->find('all', $options, !$this->AuthSession->isEdiUserRole());

        // 入手元が不明な項目のダミーデータをセット
        foreach ($data as $i => &$d) {
            // 取り方不明の項目に空をセット
            $d[0]['center_shelf_no']     = ''; // センター棚番号 空
            $d[0]['pospital_shelf_no']   = ''; // 病院棚番号 空
            $d[0]['fixed_count']         = ''; // [管理区分][定数] 空
        }

        return $data;
    }

    private function findClaimData($facilityId, $fromDate, $toDate) {
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                // 取得するフィールド
                'fields' => array(
                        '"TrnClaim"."id"                   AS "id"',              // 請求ID
                        '"MstDepartment"."department_name" AS "department_name"', // 部署名
                        '"MstFacilityItem"."item_name"     AS "item_name"',       // 商品名
                        '"MstFacilityItem"."standard"      AS "standard"',        // 規格
                        '"MstFacilityItem"."item_code"     AS "item_code"',       // 商品コード
                        '"MstDealer"."dealer_name"         AS "dealer_name"',     // 販売元
                        '"TrnClaim"."count"                AS "count"',           // 請求数
                        '"MstFacilityItem"."internal_code" AS "item_id"',         // 商品ID
                        '"TrnClaim"."unit_price"           AS "unit_price"',      // 請求単価
                        '"TrnSticker"."lot_no"             AS "lot_no"',          // ロット番号
                        '"TrnClaim"."claim_price"          AS "claim_price"',     // 金額
                        //'"TrnClaim"."zura_claim_date"      AS "zura_claim_date"', // 売上日
                        'to_char("TrnClaim"."claim_date", \'YYYY/MM/DD\') AS "claim_date"', // 売上日
                ),
                // 検索条件
                'conditions' => array(
                        'TrnClaim.center_facility_id' => $facilityId,
                        'TrnClaim.is_stock_or_sale' => 2,
                        'OR' => array(
                                'TrnClaim.claim_date between ? AND ?' => array($fromDate, $toDate),
                                //'TrnClaim.zura_claim_date >= ' => $fromDate,
                        ),
                        'TrnClaim.is_deleted' => false,
                ),
                // 連結するモデル
                'joins' => array(
                        array(
                                'table' => 'mst_departments',
                                'alias' => 'MstDepartment',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstDepartment.id = TrnClaim.department_id_to',
                                ),
                        ),
                        array(
                                'table' => 'mst_item_units',
                                'alias' => 'MstItemUnit',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstItemUnit.id = TrnClaim.mst_item_unit_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_facility_items',
                                'alias' => 'MstFacilityItem',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstFacilityItem.id = MstItemUnit.mst_facility_item_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_dealers',
                                'alias' => 'MstDealer',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstDealer.id = MstFacilityItem.mst_dealer_id',
                                ),
                        ),
                        array(
                                'table' => 'trn_stickers',
                                'alias' => 'TrnSticker',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'TrnSticker.sale_claim_id = TrnClaim.id',
                                ),
                        ),
                ),
                // 並び順
                'order' => array(
                        'claim_date',
                        'department_name',
                        'item_id',
                ),
        );

        // 検索実行
        $data = $this->_controller->TrnClaim->find('all', $options, !$this->AuthSession->isEdiUserRole());

        return $data;
    }

    private function findClaimSpecification($trnClaimSpecificationId) {
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                // 取得するフィールド
                'fields' => array(
                        '"TrnClaimSpecifications"."id"                           AS "id"', // 請求明細ID
                        '"TrnClaimSpecifications"."closing_date"                 AS "closing_date"', //
                        '"TrnClaimSpecifications"."deposit_plans_date"           AS "deposit_plans_date"', //
                        '"TrnClaimSpecifications"."last_month_carry_over"        AS "last_month_carry_over"', //
                        '"TrnClaimSpecifications"."last_month_carry_over_in_tax" AS "last_month_carry_over_in_tax"', //
                        '"TrnClaimSpecifications"."carry_over"                   AS "carry_over"', //
                        '"TrnClaimSpecifications"."carry_over_in_tax"            AS "carry_over_in_tax"', //
                        '"TrnClaimSpecifications"."purchase_price"               AS "purchase_price"', //
                        '"TrnClaimSpecifications"."purchase_in_tax"              AS "purchase_in_tax"', //
                        '"TrnClaimSpecifications"."commission"                   AS "commission"', //
                        '"TrnClaimSpecifications"."commission_in_tax"            AS "commission_in_tax"', //
                        '"TrnClaimSpecifications"."extension_price"              AS "extension_price"', //
                        '"TrnClaimSpecifications"."extension_in_tax"             AS "extension_in_tax"', //
                        '"TrnClaimSpecifications"."total_tax"                    AS "total_tax"', //
                        '"TrnClaimSpecifications"."payment_track_record"         AS "payment_track_record"', //
                        '"TrnClaimSpecifications"."payment_track_record_in_tax"  AS "payment_track_record_in_tax"', //
                        '"MstFacility"."id"                                               AS "billing_facility_id"', //
                        '"MstFacility"."facility_name"                                    AS "billing_facility_name"', //
                        '"MstFacility"."zip"                                              AS "billing_zip"', //
                        '"MstFacility"."address"                                          AS "billing_address"', //
                        '"last_time_TrnClaimSpecifications"."id"                          AS "last_time_id"',
                        '"last_time_TrnClaimSpecifications"."purchase_in_tax"             AS "last_time_purchase_in_tax"',
                        '"last_time_TrnClaimSpecifications"."commission_in_tax"           AS "last_time_commission_in_tax"',
                        '"last_time_TrnClaimSpecifications"."extension_in_tax"            AS "last_time_extension_in_tax"',
                        '"last_time_TrnClaimSpecifications"."payment_track_record_in_tax" AS "last_time_payment_track_record_in_tax"',

                ),
                'joins' => array(
                        array(
                                'table' => 'mst_facilities',
                                'alias' => 'MstFacility',
                                'type'  => 'INNER',
                                'conditions' => array(
                                        'MstFacility.id = TrnClaimSpecifications.mst_facility_id',
                                ),
                        ),
                        array(
                                'table' => 'trn_claim_specifications',
                                'alias' => 'last_time_TrnClaimSpecifications',
                                'type'  => 'LEFT',
                                'conditions' => array(
                                        'last_time_TrnClaimSpecifications.mst_facility_id = TrnClaimSpecifications.mst_facility_id',
                                        'last_time_TrnClaimSpecifications.is_deleted' => false,
                                        'last_time_TrnClaimSpecifications.deposit_plans_date < TrnClaimSpecifications.deposit_plans_date',

                                ),
                        ),
                ),
                // 検索条件
                'conditions' => array(
                        'TrnClaimSpecifications.id' => $trnClaimSpecificationId,
                        'TrnClaimSpecifications.is_deleted' => false,
                ),
                'limit' => '1',
                'order' => array(
                        'last_time_TrnClaimSpecifications.deposit_plans_date DESC'
                ),
        );

        // 検索実行
        $data = $this->_controller->TrnClaimSpecifications->find('all', $options, !$this->AuthSession->isEdiUserRole());

        return $data;
    }

    private function findShippingData($trnEdiShippingHeaderIds) {
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                'fields' => array(
                        self::getShippingTypeNameSqlFieldStr() .  ' AS "type_name"',              // 管理区分
                        '"TrnEdiShipping"."work_no"                 AS "work_no"',                // 出荷番号
                        '"MstDepartmentTo"."department_formal_name" AS "department_formal_name"', // 送付先部署名
                        '"MstFacilityTo"."facility_formal_name"     AS "facility_formal_name"',   // 送付先施設名
                        '"MstFacilityTo"."zip"                      AS "zip"',                    // 送付先施設郵便番号
                        '"MstFacilityTo"."address"                  AS "address"',                // 送付先施設住所
                        '"MstFacilityTo"."tel"                      AS "tel"',                    // 送付先施設電話番号
                        '"MstFacilityTo"."fax"                      AS "fax"',                    // 送付先施設FAX番号
                        '"MstFacilityFrom"."facility_formal_name"   AS "supplier_name"',          // 送付元業者名
                        '"MstFacilityFrom"."zip"                    AS "supplier_zip"',           // 送付元業者郵便番号
                        '"MstFacilityFrom"."address"                AS "supplier_address"',       // 送付元施設住所
                        '"MstFacilityFrom"."tel"                    AS "supplier_tel"',           // 送付元業者電話番号
                        '"MstFacilityFrom"."fax"                    AS "supplier_fax"',           // 送付元業者FAX番号
                        '"MstFacilityItem"."internal_code"          AS "internal_code"',          // 商品ID
                        '"MstFacilityItem"."item_name"              AS "item_name"',              // 商品名
                        '"MstFacilityItem"."item_code"              AS "item_code"',              // 商品コード
                        '"MstFacilityItem"."standard"               AS "standard"',               // 規格
                        '"MstDealer"."dealer_name"                  AS "dealer_name"',            // 販売元
                        '"TrnReceiving"."stocking_price"            AS "stocking_price"',         // 単価
                        '"TrnReceiving"."quantity"                  AS "quantity"',               // 数量
                        '"TrnEdiShipping"."lot_no"                  AS "lot_no"',                 // ロット番号
                        '"TrnEdiShipping"."recital"                 AS "recital"',                // 明細備考
                        '"TrnEdiShipping"."is_deleted"              AS "is_deleted"',             // 削除済みフラグ
                        'to_char("TrnEdiShipping"."work_date", \'YYYY年MM月DD日\') AS "work_date"', // 出荷日時
                        'to_char("TrnEdiShipping"."validated_date", \'YYYY/MM/DD\') AS "validated_date"', // 有効期限
                        'round("TrnReceiving"."stocking_price" * "TrnReceiving"."quantity") AS "claim_price"', // 金額
                        self::getUnitNameSqlFieldStr() .          ' AS "unit_name"',              // 包装単位
                ),
                // 検索条件
                'conditions' => array(
                        'TrnEdiShippingHeader.id' => $trnEdiShippingHeaderIds
                ),
                // 連結するモデル
                'joins' => array(
                        array(
                                'table' => 'trn_edi_shippings',
                                'alias' => 'TrnEdiShipping',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'TrnEdiShipping.trn_edi_shipping_header_id = TrnEdiShippingHeader.id',
                                ),
                        ),
                        array(
                                'table' => 'mst_item_units',
                                'alias' => 'MstItemUnit',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstItemUnit.id = TrnEdiShipping.mst_item_unit_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_unit_names',
                                'alias' => 'MstUnitName',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstUnitName.id = MstItemUnit.mst_unit_name_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_unit_names',
                                'alias' => 'MstUnitName2',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstUnitName2.id = MstItemUnit.per_unit_name_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_facility_items',
                                'alias' => 'MstFacilityItem',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstFacilityItem.id = MstItemUnit.mst_facility_item_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_dealers',
                                'alias' => 'MstDealer',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstDealer.id = MstFacilityItem.mst_dealer_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_departments',
                                'alias' => 'MstDepartmentFrom',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstDepartmentFrom.id = TrnEdiShipping.department_id_from',
                                ),
                        ),
                        array(
                                'table' => 'mst_facilities',
                                'alias' => 'MstFacilityFrom',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstFacilityFrom.id = MstDepartmentFrom.mst_facility_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_departments',
                                'alias' => 'MstDepartmentTo',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstDepartmentTo.id = TrnEdiShipping.department_id_to',
                                ),
                        ),
                        array(
                                'table' => 'mst_facilities',
                                'alias' => 'MstFacilityTo',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'MstFacilityTo.id = MstDepartmentTo.mst_facility_id',
                                ),
                        ),
                        array(
                                'table' => 'trn_stickers',
                                'alias' => 'TrnSticker',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'TrnSticker.trn_edi_shipping_id = TrnEdiShipping.id',
                                ),
                        ),
                        array(
                                'table' => 'trn_storages',
                                'alias' => 'TrnStorage',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'TrnStorage.id = TrnSticker.trn_storage_id',
                                ),
                        ),
                        array(
                                'table' => 'trn_receivings',
                                'alias' => 'TrnReceiving',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'TrnReceiving.id = TrnStorage.trn_receiving_id',
                                ),
                        ),
                        array(
                                'table' => 'trn_edi_receivings',
                                'alias' => 'TrnEdiReceiving',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'TrnEdiReceiving.id = TrnEdiShipping.trn_edi_receiving_id',
                                ),
                        ),
                        array(
                                'table' => 'trn_orders',
                                'alias' => 'TrnOrder',
                                'type' => 'INNER',
                                'conditions' => array(
                                        'TrnOrder.id = TrnEdiReceiving.trn_order_id',
                                ),
                        ),
                ),
                // 並び順
                'order' => array(
                        'TrnEdiShipping.trn_edi_shipping_header_id',
                        'MstFacilityFrom.id',
                        'MstDealer.dealer_name',
                        'MstFacilityItem.item_name',
                        'MstFacilityItem.item_code',
                ),
        );

        // 検索実行
        $data = $this->_controller->TrnEdiShippingHeader->find('all', $options, !$this->AuthSession->isEdiUserRole());

        return $data;
    }


// 以下、PDFオブジェクト作成用のメソッド
    private function createPdfObjectForReport($format, $orientation, $topMargin) {
        $pdf = $this->createDefaultPdfObject($format, $orientation, 6, 6, $topMargin, 16, 6, 6);
        return $pdf;
    }

    private function createPdfObjectForSticker($format, $orientation) {
        $pdf = $this->createDefaultPdfObject($format, $orientation, 1, 1, 1, 1, 0, 0);
        $pdf->SetAutoPageBreak(false);
        return $pdf;
    }

    private function createDefaultPdfObject($format, $orientation, $mgl, $mgr, $mgt, $mgb, $mgh, $mgf) {
        App::import('Vendor', 'mpdf60/mpdf');
        $pdf = new mPDF(
                'ja',
                $format,
                $default_font_size = 0, $default_font = '',
                $mgl, $mgr, $mgt, $mgb,
                $mgh, $mgf,
                $orientation);
        $pdf->defaultheaderline = 0;
        $pdf->defaultfooterline = 0;
        return $pdf;
    }


// 以下、Utilityメソッド
    private static function getUnitNameSqlFieldStr() {
        $sql = '';
        $sql .= '(';
        $sql .= '  CASE';
        $sql .= '    WHEN "MstItemUnit"."per_unit" = 1 THEN "MstUnitName"."unit_name"';
        $sql .= '    ELSE "MstUnitName"."unit_name"';
        $sql .= '            || \'(\' || "MstItemUnit"."per_unit" || "MstUnitName2"."unit_name" || \')\'';
        $sql .= '  END';
        $sql .= ')';
        return $sql;
    }

    private static function getShippingTypeNameSqlFieldStr() {
        $sql = '';
        $sql .= '(';
        $sql .= '  CASE';
        foreach (Configure::read('OrderTypes.orderTypeShortName') as $key => $value) {
            $sql .= sprintf(' WHEN "TrnOrder"."order_type" = \'%s\' THEN \'%s\'', $key, $value);
        }
        $sql .= sprintf(' ELSE \'\'');
        $sql .= '  END';
        $sql .= ')';
        return $sql;
    }

    private static function truncateStr($keyValues, $truncateKeyLenMap, $exact = '...') {
        foreach ($truncateKeyLenMap as $key => $len) {
            $text = $keyValues[$key];
            $text = mb_convert_kana($text, 'rnaskhcV');
            $text = mb_strimwidth($text, 0, $len, $exact);
            $keyValues[$key] = $text;
        }
        return $keyValues;
    }


}
