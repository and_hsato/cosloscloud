<?php
/**
 * バーコード関連
 * @since 2010/09/09
 */
class BarcodeComponent  extends Component {
    var $name = 'Barcode';
    /**
     *
     * @var array $components
     */
    var $components = array('Session');

    /**
     * EAN128を読んで情報を取得
     *
     * JANが取得できれば商品情報取得します
     * 正しいJANではない場合はfalseを返します
     *
     * @access public
     * @param string $value
     * @return mixed
     */
    public function readEAN128($value, $center_id = null ){
        if(is_null($center_id)){
            $center_id = $this->Session->read('Auth.facility_id_selected');
        }
        $_result = array(
            'jan_code'        => '',
            'pure_jan_code'   => '',
            'check_digit'     => '',
            'lot_no'          => '',
            'validated_date'  => '',
            'quantity'        => null,
            'serial_no'       => '',
            'MstFacilityItem' => array(),
        );

        //janのみの場合
        if(0 !== preg_match('/^[0-9]{13}$/', $value, $matches)){
            $_result['jan_code'] = $matches[0];
            $_result['pure_jan_code'] = substr($matches[0], 0, strlen($matches[0]) - 1);
            $_result['check_digit'] = substr($matches[0], strlen($matches[0]) - 1, 1);
            // return $_result;
        } else { 

            //Janチェック
            if(0 === preg_match('/^01[0-9]([0-9]{13})/', $value, $matches)){
                return false;
            }else{
                $_result['jan_code'] = $matches[1];
                $_result['pure_jan_code'] = substr($matches[1], 0, strlen($matches[1]) - 1);
                $_result['check_digit'] = substr($matches[1], strlen($matches[1]) - 1, 1);
            }
            
            //その他
            $_code = substr($value,16);//JAN以降の文字列を取得
            $roop_flg = true;
            $i=0;
            //無限ループ $_codes がなくなるまで。
            while($roop_flg){
                //頭の2文字を取得
                switch(substr($_code, 0, 2)){
                    //有効期限
                  case '17':
                    if(preg_match('/^17([0-9]{6})/', $_code, $_m) > 0){
                        $_result['validated_date'] = $_m[1];
                        $_code = substr($_code, strlen($_m[1]) + 2);
                    }
                    break;
                    
                    //数量
                  case '30':
                    $min = 100;
                    $__l = strpos(substr($_code,3),"10");
                    //$__s = strpos(substr($_code,3),"21");
                    
                    if($__l !== false){
                        $min = $__l;
                    }
                    //if($__s !== false){
                    //    if($__s < $min){
                    //        $min = $__s;
                    //    }
                    //}
                    if($min == 100){
                        $_result['quantity'] = substr($_code ,2);
                        $_code = '';
                    }else{
                        $min = $min + 1;
                        $_result['quantity'] = substr($_code , 2 , $min);
                        $_code = substr($_code , strlen($_result['quantity']) + 2);
                    }
                    if(strlen($_result['quantity']) > 8){
                        return false;
                    }
                    break;
                    //ロット
                  case '10':
                    $min = 100;
                    //$__s = strpos(substr($_code,3),'21');
                    //if($__s !== false ){
                    //    if($__s < $min){
                    //        $min = $__s;
                    //    }
                    //}
                    if($min == 100){
                        $_result['lot_no'] = substr($_code ,2);
                        $_code = '';
                    }else{
                        $min = $min + 1;
                        $_result['lot_no'] = substr($_code , 2 , $min);
                        $_code = substr($_code , strlen($_result['lot_no']) + 2);
                    }
                    if(strlen($_result['lot_no']) > 20){
                        return false;
                    }
                    break;
                }
                $i++;
                if($i > 4){
                    $roop_flg = false;
                }
                if(strlen($_code) == 0) {
                    $roop_flg = false;
                }
            }
        }
        if($_result['jan_code'] != ''){
            //janがあれば商品情報を付与
            $_model = ClassRegistry::init('MstFacilityItem');
            $_model->Behaviors->attach('Containable');
            $_res = $_model->find('all', array(
                'conditions' => array(
                    'MstFacilityItem.jan_code LIKE ' => "{$_result['pure_jan_code']}%",
                    'MstFacilityItem.mst_facility_id' => $center_id
                    ),
                )
                ,false);
            
            if(false !== $_res){
                $_result['MstFacilityItem'] = array_merge($_result['MstFacilityItem'], $_res);
            }
        }

        return $_result;
    }


    /**
     * チェックデジットが正しいかどうか
     * @param string $_jan
     * @param string $_checkDegit
     * @access public
     * @return boolean
     */
    public function isCorrectCheckDegit($_jan, $_checkDegit){
        $_evenSum = 0;
        $_oddSum = 0;
        $_jan = strrev($_jan);

        for($i = 1;$i <= strlen($_jan); $i++){
            if($i%2 === 0){
                $_evenSum += (integer)substr($_jan, $i - 1, 1);
            }else{
                $_oddSum += (integer)substr($_jan, $i - 1, 1);
            }
        }
        $_sum = (string)(($_oddSum * 3) + $_evenSum);
        $_check = 10 - (integer)substr($_sum, strlen($_sum) -1 , 1);
        if($_check === 10){
            $_check = 0;
        }
        return ($_check == $_checkDegit);
    }

}
?>
