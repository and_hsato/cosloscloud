<?php
/**
 * csv_read_utils.php
 * CsvReadUtilsコンポーネント
 * @author 
 * @version 1.0.0
 * @since 2011/01/01
 */

class CsvReadUtilsComponent extends Component {

    private $_fileName     = "";              // ファイル名
    private $_lineFeedCd   = "";              // 改行コード  \r\n \n 
    private $_quote        = "";              // 囲み文字    \" 
    private $_delimiter    = "";              // 区切り文字  , \t
    private $_charCd       = "";              // 文字コード(CSV文字コード)  SJIS-WIN(SJIS) EUCJP-WIN(EUC-JP) UTF-8
    private $_fileHandle;                     // ファイルハンドル
    private $_fileHandleTemp;                 // ファイルハンドル(文字コード変換)
    private $_lineMaxLength;                  // 読み込む長さ(バイト)

    private $_hashKeys      = array();        // 連想配列のkey要素

    private $_data;

    //*************************************************************************
    //* セッター
    //*************************************************************************
    public function setFileName($pVal)          { $this->_fileName = $pVal; }                              // ファイル名
    public function setLineFeedCd($pVal)        { $this->_lineFeedCd = $pVal; }                            // 改行コード
    public function setQuote($pVal)             { $this->_quote = $pVal; }                                 // 囲み文字
    public function setDelimiter($pVal)         { $this->_delimiter = $pVal; }                             // 区切り文字
    public function setCharCd($pVal) {                                                                     // 文字コード(出力文字コード)
        // SJIS → SJIS-win(文字化け対策)
        if ( preg_match( "/^SJIS$/i" , $pVal ) ) {
            $pVal = "SJIS-win";
        }
        // EUC-JP → eucJP-win(文字化け対策)
        if ( preg_match( "/^EUC-JP$/i" , $pVal ) ) {
            $pVal = "eucJP-win";
        }
        $this->_charCd = $pVal;
    }
    public function setLineMaxLength($pVal)     { $this->_lineMaxLength = $pVal; }                         // 読み込む長さ(バイト)

    //*************************************************************************
    //* ゲッター
    //*************************************************************************
    public function getFileName()               { return $this->_fileName; }                               // ファイル名
    public function getLineFeedCd()             { return $this->_lineFeedCd; }                             // 改行コード
    public function getQuote()                  { return $this->_quote; }                                  // 囲み文字
    public function getDelimiter()              { return $this->_delimiter; }                              // 区切り文字
    public function getCharCd()                 { return $this->_charCd; }                                 // 文字コード(出力文字コード)
    public function getFileHandle()             { return $this->_fileHandle; }                             // ファイルハンドル
    public function getLineMaxLength()          { return $this->_lineMaxLength; }                          // 読み込む長さ(バイト)

    //*************************************************************************
    //* KEY追加
    //*************************************************************************
    public function hashKeyAdd($pHashKey) {
        array_push($this->_hashKeys,$pHashKey);
    }

    //*************************************************************************
    //* KEYクリア
    //*************************************************************************
    public function hashKeyClear() {
        $this->_hashKeys = array();
    }

    //*************************************************************************
    //* CSVファイルオープン
    //*************************************************************************
    public function open(){
        // 初期設定
        if ( $this->_lineFeedCd == "" )    { $this->_lineFeedCd    = "\r\n"; }
        if ( $this->_quote == "" )         { $this->_quote         = "\"";   }
        if ( $this->_delimiter == "" )     { $this->_delimiter     = ",";    }
        if ( $this->_charCd == "" )        { $this->_charCd        = "SJIS-win"; }
        if ( $this->_lineMaxLength == "" ) { $this->_lineMaxLength = 1000; }

        if ( $this->_charCd == "" && $this->_charCd != "UTF-8" ) { 
            $this->_fileHandle = fopen($this->_fileName,"r");
        }else{
            // 強制文字コード変換処理 例)能"等のエラー回避
            $this->_fileHandleTemp = fopen($this->_fileName,"r");

            $this->_fileHandle = tmpfile();
            while ( $line = fgets($this->_fileHandleTemp) ) {
                // 文字コード変換
                fwrite($this->_fileHandle, $this->convertCharCd( $line ) );
            }

            rewind($this->_fileHandle);
            fclose($this->_fileHandleTemp);
        }
    }

    //*************************************************************************
    //* eof判定
    //*************************************************************************
    public function eof() {
        return feof($this->_fileHandle);
    }

    //*************************************************************************
    //* CSV読込(一行)
    //*************************************************************************
    public function readCsvLine() {
        $this->_data = "";

        // 一行読込(EOFだけの行は空行なので、空文字を返す)
        if ( $wcsvLineData = fgetcsv($this->_fileHandle,$this->_lineMaxLength,$this->_delimiter,$this->_quote) ) {

            // hashKeysが存在している場合は、連想配列に設定する
            if ( count($this->_hashKeys) > 0 ) {
                $index = 0;
                foreach ( $this->_hashKeys As $wHashKey ) {
                    if ( count($wcsvLineData) > $index ) {
                        $this->_data["$wHashKey"] =  $wcsvLineData[$index] ;
                        $index++;
                    }else{
                        $this->_data["$wHashKey"] =  "" ;
                    }
                }
            } else {
                // hashKeysが存在しない場合は、普通の配列に設定する
                $index = 0;
                foreach ( $wcsvLineData As $wdata ) {
                    $this->_data[$index] = $wdata;
                    $index++;
                }
            }

            return $this->_data;
        }else {
            return "";
        }
    }

    //*************************************************************************
    //* CSV読込(一括)
    //*************************************************************************
    public function readCsv() {
        $data = array();
        $index = 0;
        while ( ! $this->eof() ) {
            $this->_data = "";
            $this->_data = $this->readCsvLine();

            if ( $this->_data == "" ) {
            } else {
                $data[$index] = $this->_data;
                $index++;
            }
        }
        return $data;
    }

    //*************************************************************************
    //* 文字コード変換
    //*************************************************************************
    public function convertCharCd($pStr) {
        if ( $this->_charCd == "" ) {
            return $pStr;
        } else {
            return mb_convert_encoding($pStr,"UTF-8",$this->_charCd);
        }
    }

    //*************************************************************************
    //* CSVファイルクローズ
    //*************************************************************************
    public function close(){
        fclose($this->_fileHandle);
    }
}
?>
