<?php
class ExcelReportComponent extends Component {

    const TEMPLATE_FILE_RFP = 'template.xls';
    
    var $_controller;

    public function startup(&$controller) {
        $this->_controller = $controller;
    }
    
    /**
     * 指定されたTrnRfpHeaderの情報から見積依頼のExcelファイルを作成する。
     *
     * @param mixed $trnRfpHeader 見積依頼ヘッダーのデータ。
     * @return boolean Excelファイルの作成に成功した場合はtrue、失敗した場合はfalseを返す。
     */
    public function createExcelFileForRfp($trnRfpHeader) {
        $items = $this->findRfpData($trnRfpHeader);
        if(empty($items)) return false;
        
        // Excelオブジェクト作成
        $templateFilePath = realpath(TMP) . DS . 'excel' . DS . self::TEMPLATE_FILE_RFP;
        $PHPExcel = $this->createDefaultExcelObject($templateFilePath);
        
        // Excel書込処理
        $PHPExcel->setActiveSheetIndex(0);  //一番左のシートをアクティブ化
        $sheet = $PHPExcel->getActiveSheet();
        foreach ($items as $i => $item) {
            $rowNumber =  (string)($i + 4);
            $sheet->setCellValue        ("A" . $rowNumber, $item[0]['work_seq']);                                           // No
            $sheet->setCellValueExplicit("B" . $rowNumber, $item[0]['master_id'], PHPExcel_Cell_DataType::TYPE_STRING);     // マスターID
            $sheet->setCellValueExplicit("C" . $rowNumber, $item[0]['internal_code'], PHPExcel_Cell_DataType::TYPE_STRING); // 商品ID
            $sheet->setCellValue        ("D" . $rowNumber, $item[0]['item_name']);                                          // 商品名
            $sheet->setCellValue        ("E" . $rowNumber, $item[0]['item_code']);                                          // 製品番号
            $sheet->setCellValue        ("F" . $rowNumber, $item[0]['standard']);                                           // 規格
            $sheet->setCellValueExplicit("G" . $rowNumber, $item[0]['jan_code'], PHPExcel_Cell_DataType::TYPE_STRING);      // JANコード
            $sheet->setCellValue        ("H" . $rowNumber, $item[0]['unit_name']);                                          // 包装単位
        }
        $sheet->setCellValue("B2" , $trnRfpHeader['id']);                     // ID
        $sheet->setCellValue("I2" , date('Y/m/d', strtotime($trnRfpHeader['limit_date']))); // 締切日
        
        // Excelファイル保存
        $objWriter = new PHPExcel_Writer_Excel5($PHPExcel);   //2003形式で保存
        $filePath = self::getRfpAbsoluteFilePath($trnRfpHeader);
        $objWriter->save($filePath);
        return true;
    }
    
    /**
     * 指定されたTrnRfpHeaderの情報から作成された見積依頼のExcelファイルを削除する。
     *
     * @param mixed $trnRfpHeader 見積依頼ヘッダーのデータ。
     * @return boolean 成功した場合にtrueを、失敗した場合にfalseを返す。
     */
    public function deleteExcelFileForRfp($trnRfpHeader) {
        $path = $this->getRfpAbsoluteFilePath($trnRfpHeader);
        if (!is_writable($path)) return false;
        return unlink($path);
    }
    
    /**
     * 指定されたTrnRfpHeaderの情報からExcelファイルの絶対パスを返す。
     *
     * @param mixed $trnRfpHeader 見積依頼ヘッダーのデータ。
     * @return string Excelファイルの絶対パス文字列。
     */
    public function getRfpAbsoluteFilePath($trnRfpHeader) {
        $userAget = $this->_controller->request->header('User-Agent');
        $encoding = stristr($userAget, 'Windows') ? 'sjis' : 'utf-8';
        $path = $this->getRfpRelativePath($trnRfpHeader, DS, $encoding);
        return WWW_ROOT . $path;
    }
    
    /**
     * 指定されたTrnRfpHeaderの情報からExcelファイルのWEB-ROOTからの相対パスを返す。
     *
     * @param mixed $trnRfpHeader 見積依頼ヘッダーのデータ。
     * @param string $encoding 文字コード。UTF-8のパス文字列を指定された文字コードに変換する。
     * @return string ExcelファイルのWEB-ROOTからの相対パス文字列。
     */
    public function getRfpRelativePath($trnRfpHeader, $separator = '/', $encoding = null) {
        $fileName = $this->getRfpFileName($trnRfpHeader);
        $path = 'rfp' . $separator . $fileName;
        if ($encoding) $path = mb_convert_encoding($path, $encoding, 'utf-8');
        return $path;
    }
    
    /**
     * 指定されたTrnRfpHeaderの情報からExcelファイルのファイル名を返す。
     *
     * @param mixed $trnRfpHeader 見積依頼ヘッダーのデータ。
     * @return string Excelファイルのファイル名。
     */
    public function getRfpFileName($trnRfpHeader) {
        $fileName = '見積依頼_' . $trnRfpHeader['work_no'] . '_' . date('YmdHis', strtotime($trnRfpHeader['created'])) . '.xls';
        return $fileName;
    }
    
// 以下、非公開メソッド
// 以下、各種データ検索用のメソッド
    private function findRfpData($trnRfpHeader) {
        $defaultJoinType = 'INNER';
        $options = array(
                // 再帰検索しない
                'recursive' => -1,
                'fields' => array(
                        '"TrnRfp"."work_seq"               AS "work_seq"',      // 作業番号
                        '"MstGrandmaster"."master_id"      AS "master_id"',     // マスターID(コスロスID)
                        '"MstFacilityItem"."internal_code" AS "internal_code"', // 商品ID
                        '"MstFacilityItem"."item_code"     AS "item_code"',     // 商品コード
                        '"MstGrandmaster"."standard"       AS "standard"',      // 規格
                        '"MstGrandmaster"."jan_code"       AS "jan_code"',      // JANコード
                        self::getItemNameSqlFieldStr() . ' AS "item_name"',     // 商品名
                        self::getUnitNameSqlFieldStr() . ' AS "unit_name"',     // 包装単位
                ),
                // 検索条件
                'conditions' => array(
                        'TrnRfp.trn_rfp_header_id' => $trnRfpHeader['id'],
                        'TrnRfp.is_deleted' => false,
                ),
                // 連結するモデル
                'joins' => array(
                        array(
                                'table' =>  'mst_grandmasters',
                                'alias' => 'MstGrandmaster',
                                'type' => $defaultJoinType,
                                'conditions' => array(
                                        'MstGrandmaster.id = TrnRfp.mst_grandmaster_id',
                                        'MstGrandmaster.is_deleted' => false,
                                ),
                        ),
                        array(
                                'table' =>  'mst_facility_items',
                                'alias' => 'MstFacilityItem',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstFacilityItem.master_id = MstGrandmaster.master_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_item_units',
                                'alias' => 'MstItemUnit',
                                'type' => 'LEFT',
                                'conditions' => array(
                                        'MstItemUnit.mst_facility_item_id = MstFacilityItem.id',
                                ),
                        ),
                        array(
                                'table' => 'mst_unit_names',
                                'alias' => 'MstUnitName',
                                'type' => $defaultJoinType,
                                'conditions' => array(
                                        'MstUnitName.id = MstItemUnit.mst_unit_name_id',
                                ),
                        ),
                        array(
                                'table' => 'mst_unit_names',
                                'alias' => 'MstUnitName2',
                                'type' => $defaultJoinType,
                                'conditions' => array(
                                        'MstUnitName2.id = MstItemUnit.per_unit_name_id',
                                ),
                        ),
                ),
                // 並び順
                'order' => array(
                        'MstGrandmaster.master_id' => 'DESC',
                ),
        );
        // 検索実行
        $data = $this->_controller->TrnRfp->find('all', $options);
        
        return $data;
    }
    
    
// Excelオブジェクト作成用のメソッド
    private function createDefaultExcelObject($templateFilePath) {
        // Excel出力用ライブラリ
        App::import('Vendor', 'PHPExcel', array('file'=>'phpexcel' . DS . 'PHPExcel.php'));
        App::import('Vendor', 'PHPExcel_IOFactory', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'IOFactory.php'));
        App::import('Vendor', 'PHPExcel_Cell_AdvancedValueBinder', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'Cell' . DS . 'AdvancedValueBinder.php'));
    
        // Excel95用ライブラリ
        App::import('Vendor', 'PHPExcel_Writer_Excel5', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'Writer' . DS . 'Excel5.php'));
        App::import('Vendor', 'PHPExcel_Reader_Excel5', array('file'=>'phpexcel' . DS . 'PHPExcel' . DS . 'Reader' . DS . 'Excel5.php'));
        try{
            $objReader = PHPExcel_IOFactory::createReader("Excel5");
            $PHPExcel = $objReader->load($templateFilePath);
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return null;
        }
        return $PHPExcel;
    }
    

// 以下、Utilityメソッド
    private static function getUnitNameSqlFieldStr() {
        $sql = '';
        $sql .= '(';
        $sql .= '  CASE';
        $sql .= '    WHEN "MstFacilityItem"."id" IS NULL THEN "MstGrandmaster"."per_unit1" || "MstGrandmaster"."unit_name1"';
        $sql .= '    WHEN "MstItemUnit"."per_unit" = 1 THEN "MstUnitName"."unit_name"';
        $sql .= '    ELSE "MstUnitName"."unit_name"';
        $sql .= '            || \'(\' || "MstItemUnit"."per_unit" || "MstUnitName2"."unit_name" || \')\'';
        $sql .= '  END';
        $sql .= ')';
        return $sql;
    }
    
    private static function getItemNameSqlFieldStr() {
        $sql = '';
        $sql .= '(';
        $sql .= '  CASE';
        $sql .= '    WHEN "MstFacilityItem"."item_name" IS NULL THEN "MstGrandmaster"."item_name"';
        $sql .= '    ELSE "MstFacilityItem"."item_name"';
        $sql .= '  END';
        $sql .= ')';
        return $sql;
    }
    
}