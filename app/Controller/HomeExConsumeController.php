<?php
/**
 * HomeExConsumeController
 * 在宅（臨時消費）
 * @version 1.0.0
 */
class HomeExConsumeController extends AppController {

    /**
     * @var $name
     */
    var $name = 'HomeExConsume';

    /**
     * @var array $uses
     */
    var $uses = array('TrnSticker',
                      'TrnStickerIssue',
                      'TrnStickerIssueHeader',
                      'TrnStickerRecord',
                      'MstFacility',
                      'MstFacilityItem',
                      'MstClass',
                      'MstUserBelonging',
                      'MstDepartment',
                      'MstUser',
                      'MstShelfName',
                      'MstFixedCount',
                      'MstInsuranceClaimDepartment',
                      'MstPrivilege',
                      'TrnShipping',
                      'TrnShippingHeader',
                      'TrnCloseHeader',
                      'TrnPromiseHeader',
                      'TrnPromise',
                      'TrnFixedPromise',
                      'MstTransactionConfig',
                      'MstSalesConfig',
                      'TrnReceivingHeader',
                      'TrnReceiving',
                      'TrnStorageHeader',
                      'TrnStorage',
                      'TrnStock',
                      'TrnClaim',
                      'MstUnitName',
                      'TrnConsumeHeader',
                      'TrnConsume',
                      'TrnCloseHeader',
                      'TrnOrderHeader',
                      'TrnOrder',
                      );


    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker','common');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Common','Barcode','Stickers','CsvWriteUtils','CsvReadUtils', 'TemporaryItem');

    /**
     * 機能別の2桁の番号
     * @var int $work_no_model
     */
    var $work_no_model = '06';

    /**
     * 納品書に出力しない出荷区分名(管理区分名)
     */
    var $invoice_not_print_type = array('定数品');

    /**
     * エラーメッセージ(商品採用日時切れ)
     */
    var $item_limit_error = '商品採用日時を過ぎています。';

    /**
     * エラーメッセージ(有効期限切れ)
     */
    var $validated_limit_error = ' 有効期限切れ ';

    /**
     * エラーメッセージ(有効期限警告)
     */
    var $validated_alert_error = ' 有効期限警告 ';

    /**
     *
     */
    function index (){
    }

    /**
     * 在宅臨時消費登録
     */
    function ex_add () {
        $this->setRoleFunction(129); //在宅(臨時消費)
        $this->request->data['TrnShippingHeader']['work_date'] = date('Y/m/d');
        $this->set('facility_list', $this->getFacilityList( $this->Session->read('Auth.facility_id_selected') ,
                                                                 array(Configure::read('FacilityType.hospital'))
                                                                 ));

        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));
    }

    /**
     * 在宅臨時消費 シール読み込み
     */
    function ex_add_input_sticker () {
        // セッション処理
        $this->Session->write('ExShipping.input_type','INPUT_STICKER');

        // 施設コードから施設IDを取得
        $this->request->data['TrnShippingHeader']['facilityId'] = $this->getFacilityId($this->request->data['TrnShippingHeader']['facilityCode'] ,
                                                                             Configure::read('FacilityType.hospital'));
        // 部署コードから部署IDを取得
        $this->request->data['TrnShippingHeader']['departmentId'] = $this->getDepartmentId($this->request->data['TrnShippingHeader']['facilityId'],
                                                                                  Configure::read('DepartmentType.hospital'),
                                                                                  $this->request->data['TrnShippingHeader']['departmentCode']
                                                                                  );
        
        // 締めチェック
        $this->closedCheck('ex_add', $this->request->data['TrnShippingHeader']['work_date'], null, $this->request->data['TrnShippingHeader']['facilityCode']);
    }

    /**
     * 在宅臨時消費確認
     */
    function ex_add_confirm () {
        // セッション処理
        $this->set('input_type' ,$this->Session->read('ExShipping.input_type'));
        $this->Session->write('ExShipping.readTime',date('Y-m-d H:i:s'));
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));

        // ループ開始
        // シール番号から、商品情報を取得
        $result = array();
        $i = 0;
        foreach(explode(',',substr($this->request->data['codez'],0,-1)) as $barcbode){
            $r = $this->getExShippingData($barcbode);
            if(empty($r)){
                //シールが存在しない場合ダミーで埋める
                $dummy = array(
                    'TrnShipping' => array(
                        'id'                  => null,
                        'internal_code'       => '',
                        'item_name'           => '該当なし',
                        'item_code'           => '' ,
                        'standard'            => '' ,
                        'unit_name'           => '',
                        'dealer_name'         => '',
                        'facility_sticker_no' => $barcbode,
                        'hospital_sticker_no' => '',
                        'is_check'            => false,
                        'alert'               => '無効なシールです',
                        'validated_date'      => '',
                        'lot_no'              => '',
                        'sales_price'         => '',
                        )
                    );
                $result[$i] = $dummy;
            }else{
                //シール情報を一覧に移す
                $result[$i] = $r[0];
            }
            $i++;
        }

        $this->set('result' , $result);
        $this->set('input_type' ,$this->Session->read('ExShipping.input_type'));

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('ExShipping.token' , $token);
        $this->request->data['ExShipping']['token'] = $token;
    }

    /**
     * 在宅臨時消費結果
     */
    function ex_add_result () {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['ExShipping']['token'] === $this->Session->read('ExShipping.token')) {
            $now = date('Y/m/d H:i:s');
            $this->Session->delete('ExShipping.token');
            $result = array();
            // トランザクション開始
            $this->TrnSticker->begin();
            // 行ロック
            $this->TrnSticker->query(' select * from trn_stickers as a where a.id in ( ' . join(',',$this->request->data['TrnShipping']['id']) .')  for update ' );

            //更新チェックを行う
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $this->request->data['TrnShipping']['id']). ')';
            $sql .= "     and a.modified > '" . $this->request->data['ExShipping']['time'] ."'";

            $ret = $this->TrnSticker->query($sql);

            if($ret[0][0]['count']> 0 ){
                $this->TrnSticker->rollback();
                $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }
            
            $work_no = $this->setWorkNo4Header( date('Ymd') , "28");
            // 倉庫部署ID取得
            $center_department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') , array(Configure::read('DepartmentType.warehouse')));
            // 出荷先部署ID取得
            $hospital_department_id = $this->request->data['TrnShippingHeader']['departmentId'];
            // 病院施設コード
            $hospital_facility_code = $this->request->data['TrnShippingHeader']['facilityCode'];
            // 出荷ヘッダ
            $TrnShippingHeader = array(
                'TrnShippingHeader'=> array(
                    'work_no'            => $work_no,
                    'work_date'          => $this->request->data['TrnShippingHeader']['work_date'],
                    'work_type'          => $this->request->data['TrnShippingHeader']['classId'],
                    'recital'            => $this->request->data['TrnShippingHeader']['recital'],
                    'shipping_type'      => Configure::read('ShippingType.exconsume'),
                    'shipping_status'    => Configure::read('ShippingStatus.loaded'), //受領済
                    'department_id_from' => $center_department_id,
                    'department_id_to'   => $hospital_department_id,
                    'detail_count'       => count($this->request->data['TrnShipping']['id']),
                    'is_deleted'         => false,
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'created'            => $now,
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'modified'           => $now,
                    )
                );
            
            $this->TrnShippingHeader->create();
            // SQL実行
            if (!$this->TrnShippingHeader->save($TrnShippingHeader)) {
                $this->TrnSticker->rollback();
                $this->Session->setFlash('消費登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }
            
            $TrnShippingHeaderId = $this->TrnShippingHeader->getLastInsertID();
            $this->request->data['TrnShipping']['trn_shipping_header_id'][0] = $TrnShippingHeaderId;

            // 受領ヘッダ作成
            $TrnReceivingHeader = array(
                'TrnReceivingHeader'=> array(
                    'work_no'                => $work_no,
                    'work_date'              => $this->request->data['TrnShippingHeader']['work_date'],
                    'recital'                => $this->request->data['TrnShippingHeader']['recital'],
                    'receiving_type'         => Configure::read('ReceivingType.receipt'),
                    'receiving_status'       => Configure::read('ReceivingStatus.stock'),
                    'department_id_from'     => $center_department_id,
                    'department_id_to'       => $hospital_department_id,
                    'detail_count'           => count($this->request->data['TrnShipping']['id']),
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $now,
                    'trn_shipping_header_id' => $TrnShippingHeaderId,//出荷ヘッダ参照キー
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'modified'               => $now,
                    )
                );
            
            $this->TrnReceivingHeader->create();
            // SQL実行
            if (!$this->TrnReceivingHeader->save($TrnReceivingHeader)) {
                $this->TrnSticker->rollback();
                $this->Session->setFlash('消費登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }
            
            $TrnReceivingHeaderId = $this->TrnReceivingHeader->getLastInsertID();
            // 消費ヘッダ作成
            $TrnConsumeHeader = array(
                'TrnConsumeHeader'=> array(
                    'work_no'            => $work_no,
                    'work_date'          => $this->request->data['TrnShippingHeader']['work_date'],
                    'recital'            => $this->request->data['TrnShippingHeader']['recital'],
                    'use_type'           => Configure::read('UseType.home'),  // 消費区分：在宅
                    'mst_department_id'  => $hospital_department_id,
                    'detail_count'       => count($this->request->data['TrnShipping']['id']),
                    'is_deleted'         => false,
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'created'            => $now,
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'modified'           => $now,
                    )
                );
            
            $this->TrnConsumeHeader->create();
            // SQL実行
            if (!$this->TrnConsumeHeader->save($TrnConsumeHeader)) {
                $this->TrnSticker->rollback();
                $this->Session->setFlash('消費登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }
            $TrnConsumeHeaderId = $this->TrnConsumeHeader->getLastInsertID();
            
            // 必要情報取得
            $sql  = ' select ';
            $sql .= '       a.id                   as "TrnSticker__id"';
            $sql .= '     , a.mst_item_unit_id     as "TrnSticker__mst_item_unit_id"';
            $sql .= '     , a.facility_sticker_no  as "TrnSticker__facility_sticker_no"';
            $sql .= '     , a.hospital_sticker_no  as "TrnSticker__hospital_sticker_no"';
            $sql .= '     , a.quantity             as "TrnSticker__quantity"';
            $sql .= '     , a.trn_order_id         as "TrnSticker__trn_order_id"';
            $sql .= '     , a.transaction_price    as "TrnSticker__transaction_price"';
            $sql .= '     , a.sales_price          as "TrnSticker__sales_price"';
            $sql .= '     , a.trn_storage_id       as "TrnSticker__trn_storage_id"';
            $sql .= '     , b.id                   as "TrnSticker__center_stock_id" ';
            $sql .= '     , a.sale_claim_id        as "TrnSticker__sale_claim_id"';
            $sql .= '     , d.buy_flg              as "TrnSticker__buy_flg"';
            $sql .= '     , a.sales_flg            as "TrnSticker__sales_flg"';
            $sql .= '     , a.class_name           as "TrnSticker__class_name"';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '     left join trn_stocks as b  ';
            $sql .= '       on b.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '       and b.mst_department_id = a.mst_department_id  ';
            $sql .= '     left join mst_item_units as c ';
            $sql .= '       on c.id = a.mst_item_unit_id ';
            $sql .= '     left join mst_facility_items as d ';
            $sql .= '       on d.id = c.mst_facility_item_id ';
            $sql .= '   where ';
            $sql .= '     a.id in ( ' . join(',',$this->request->data['TrnShipping']['id']) . ' )  ';

            $sticker_data = $this->TrnSticker->query($sql);
            $seq = 1;
            foreach($sticker_data as $s){
                // 部署シール番号採番
                $hospital_sticker_no = $this->getHospitalStickerNo(date('ymd'),$hospital_facility_code);
                // 出荷明細作成
                $TrnShipping = array(
                    'TrnShipping'=> array(
                        'work_no'                => $work_no,
                        'work_seq'               => $seq,
                        'work_date'              => $this->request->data['TrnShippingHeader']['work_date'],
                        'work_type'              => $this->request->data['TrnShipping']['classId'][$s['TrnSticker']['id']], //作業区分
                        'recital'                => $this->request->data['TrnShipping']['recital'][$s['TrnSticker']['id']], //
                        'shipping_type'          => Configure::read('ShippingType.exconsume'), //臨時消費
                        'shipping_status'        => Configure::read('ShippingStatus.loaded'), //受領済
                        'mst_item_unit_id'       => $s['TrnSticker']['mst_item_unit_id'],
                        'department_id_from'     => $center_department_id,
                        'department_id_to'       => $hospital_department_id,
                        'quantity'               => $s['TrnSticker']['quantity'],
                        'trn_sticker_id'         => $s['TrnSticker']['id'],
                        'is_deleted'             => false,
                        'creater'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                => $now,
                        'modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'modified'               => $now,
                        'trn_shipping_header_id' => $TrnShippingHeaderId,
                        'facility_sticker_no'    => $s['TrnSticker']['facility_sticker_no'],
                        'hospital_sticker_no'    => $hospital_sticker_no,
                        )
                    );
                
                $this->TrnShipping->create();
                // SQL実行
                if (!$this->TrnShipping->save($TrnShipping)) {
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('消費登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                $TrnShippingId = $this->TrnShipping->getLastInsertID();
                
                // シール移動履歴作成（臨時出荷）
                $TrnStickerRecord = array(
                    'TrnStickerRecord'=> array(
                        'move_date'           => $this->request->data['TrnShippingHeader']['work_date'],
                        'move_seq'            => $this->getNextRecord($s['TrnSticker']['id']),
                        'record_type'         => Configure::read('RecordType.ExShipping'),
                        'record_id'           => $TrnShippingId,
                        'trn_sticker_id'      => $s['TrnSticker']['id'],
                        'mst_department_id'   => $hospital_department_id,
                        'facility_sticker_no' => $s['TrnSticker']['facility_sticker_no'],
                        'hospital_sticker_no' => $hospital_sticker_no,
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        )
                    );
                $this->TrnStickerRecord->create();
                // SQL実行
                if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('消費登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                
                // 受領明細作成
                $TrnReceiving = array(
                    'TrnReceiving' => array(
                        'work_no'                 => $work_no,
                        'work_seq'                => $seq,
                        'work_date'               => $this->request->data['TrnShippingHeader']['work_date'],
                        'recital'                 => $this->request->data['TrnShipping']['recital'][$s['TrnSticker']['id']],
                        'receiving_type'          => Configure::read('ReceivingType.receipt'),    //入荷区分:受領による入荷
                        'receiving_status'        => Configure::read('ShippingStatus.loaded'),    //入荷状態（受領済み）
                        'mst_item_unit_id'        => $s['TrnSticker']['mst_item_unit_id'],        //包装単位参照キー
                        'department_id_from'      => $center_department_id,
                        'department_id_to'        => $hospital_department_id,
                        'quantity'                => $s['TrnSticker']['quantity'],                //数量
                        'trade_type'              => Configure::read('ClassesType.Temporary') ,   //臨時品固定
                        'stocking_price'          => $s['TrnSticker']['transaction_price'],       //仕入れ価格
                        'sales_price'             => $s['TrnSticker']['sales_price'],             //売上価格
                        'trn_sticker_id'          => $s['TrnSticker']['id'],                      //シール参照キー
                        'trn_shipping_id'         => $TrnShippingId,                              //出荷参照キー
                        'trn_order_id'            => $s['TrnSticker']['trn_order_id'],            //発注参照キー
                        'trn_receiving_header_id' => $TrnReceivingHeaderId,                       //入荷ヘッダ外部キー
                        'facility_sticker_no'     => $s['TrnSticker']['facility_sticker_no'],
                        'hospital_sticker_no'     => $hospital_sticker_no,
                        'creater'                 => $this->Session->read('Auth.MstUser.id'),
                        'modifier'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                 => $now,
                        'modified'                => $now,
                        )
                    );

                $this->TrnReceiving->create();
                // SQL実行
                if (!$this->TrnReceiving->save($TrnReceiving)) {
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('消費登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }

                $TrnReceivingId = $this->TrnReceiving->getLastInsertID();
                
                // 消費明細
                $TrnConsume = array(
                    'TrnConsume' => array(
                        'work_no'               => $work_no,
                        'work_seq'              => $seq,
                        'work_date'             => $this->request->data['TrnShippingHeader']['work_date'],
                        'recital'               => $this->request->data['TrnShipping']['recital'][$s['TrnSticker']['id']],
                        'use_type'              => Configure::read('UseType.home'),      //消費区分：在宅
                        'mst_item_unit_id'      => $s['TrnSticker']['mst_item_unit_id'], //包装単位参照キー
                        'mst_department_id'     => $hospital_department_id,              //部署参照キー
                        'quantity'              => $s['TrnSticker']['quantity'],         //数量
                        'trn_sticker_id'        => $s['TrnSticker']['id'],               //シール参照キー
                        'trn_storage_id'        => $s['TrnSticker']['trn_storage_id'],   //入庫参照キー
                        'trn_consume_header_id' => $TrnConsumeHeaderId,                  //消費ヘッダ外部キー
                        'is_retroactable'       => false,
                        'creater'               => $this->Session->read('Auth.MstUser.id'),
                        'created'               => $now,
                        'modifier'              => $this->Session->read('Auth.MstUser.id'),
                        'modified'              => $now,
                        'is_deleted'            => false,
                        'facility_sticker_no'   => $s['TrnSticker']['facility_sticker_no'],
                        'hospital_sticker_no'   => $hospital_sticker_no,
                        )
                    );
                
                $this->TrnConsume->create();
                // SQL実行
                if (!$this->TrnConsume->save($TrnConsume)) {
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('消費登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                
                $TrnConsumeId = $this->TrnConsume->getLastInsertID();

                // シール移動履歴作成（消費）
                $TrnStickerRecord = array(
                    'TrnStickerRecord'=> array(
                        'move_date'           => $this->request->data['TrnShippingHeader']['work_date'],
                        'move_seq'            => $this->getNextRecord($s['TrnSticker']['id']),
                        'record_type'         => Configure::read('RecordType.consume'),
                        'record_id'           => $TrnConsumeId,
                        'trn_sticker_id'      => $s['TrnSticker']['id'],
                        'mst_department_id'   => $hospital_department_id,
                        'facility_sticker_no' => $s['TrnSticker']['facility_sticker_no'],
                        'hospital_sticker_no' => $hospital_sticker_no,
                        'is_deleted'          => false,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        )
                    );
                $this->TrnStickerRecord->create();
                // SQL実行
                if (!$this->TrnStickerRecord->save($TrnStickerRecord)) {
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('消費登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }

                
                // 倉庫在庫減
                $this->TrnStock->create();
                $c = $this->TrnStock->updateAll(
                    array(
                        'TrnStock.stock_count' => 'TrnStock.stock_count - ' . $s['TrnSticker']['quantity'],
                        'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                        'TrnStock.modified'    => "'" . $now . "'"
                        ),
                    array(
                        'TrnStock.id' => $s['TrnSticker']['center_stock_id'],
                        ),
                    -1
                    );
                if(!$c){
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('消費登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                // シール情報更新（部署ID、出荷ID、受領ID、最終移動日付更新）
                $this->TrnSticker->create();
                $c = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.mst_department_id'   => $hospital_department_id,
                        'TrnSticker.trn_shipping_id'     => $TrnShippingId,
                        'TrnSticker.receipt_id'          => $TrnReceivingId,
                        'TrnSticker.last_move_date'      => "'" . $this->request->data['TrnShippingHeader']['work_date'] . "'",
                        'TrnSticker.hospital_sticker_no' => $hospital_sticker_no,
                        'TrnSticker.trade_type'          => "'" . Configure::read('ClassesType.ExConsume') . "'" , 
                        'TrnSticker.modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'TrnSticker.modified'            => "'" . $now . "'"
                        ),
                    array(
                        'TrnSticker.id' => $s['TrnSticker']['id'],
                        ),
                    -1
                    );
                if(!$c){
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('消費登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                
                // 薬品ではない場合、売上データを作成する
                if($s['TrnSticker']['buy_flg'] == false &&
                   $s['TrnSticker']['sales_flg'] == false &&
                   $s['TrnSticker']['class_name'] != '病院資産' 
                   ){
                    
                    // 売上明細
                    $TrnClaimId = $this->createSalesData($s['TrnSticker']['id'],
                                                         $hospital_department_id,
                                                         $this->request->data['TrnShippingHeader']['work_date']
                                                         );
                }


                
                $this->TrnSticker->create();
                $c = $this->TrnSticker->updateAll(
                    array(
                        'TrnSticker.trn_consume_id' => $TrnConsumeId,
                        'TrnSticker.quantity'       => 0,
                        ),
                    array(
                        'TrnSticker.id' => $s['TrnSticker']['id'],
                        ),
                    -1
                    );
                if(!$c){
                    $this->TrnSticker->rollback();
                    $this->Session->setFlash('消費登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('ex_add');
                }
                
                $seq++;
                $r = $this->getExShippingData($s['TrnSticker']['facility_sticker_no']);
                $result[] = $r[0];
            }
            
            //コミット直前に更新チェックを行う
            $sql  = ' select ';
            $sql .= '       count(*)  ';
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',' , $this->request->data['TrnShipping']['id']). ')';
            $sql .= "     and a.modified > '" . $this->request->data['ExShipping']['time'] ."'";
            $sql .= "     and a.modified <> '" . $now ."'";

            $ret = $this->TrnSticker->query($sql);

            if($ret[0][0]['count']> 0 ){
                $this->TrnSticker->rollback();
                $this->Session->setFlash('ほかユーザによって更新されました。最初から作業をやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('ex_add');
            }

            // コミット
            $this->TrnSticker->commit();
            $this->Session->write('Shipping.result',$result);
            // 結果表示用データ取得
            $this->set('result' , $result);
            $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));
            $this->set('input_type' ,$this->Session->read('ExShipping.input_type'));
            $this->set('facility_from_id',$this->Session->read('Auth.facility_id_selected'));
            $this->set('user_id',$this->Session->read('Auth.MstUser.id'));
        }else{
            // トランザクショントークン不正
            // リロード or ２度押し
            $this->set('result',$this->Session->read('Shipping.result'));
            $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),$this->work_no_model));
            $this->set('input_type' ,$this->Session->read('ExShipping.input_type'));
            $this->set('facility_from_id',$this->Session->read('Auth.facility_id_selected'));
            $this->set('user_id',$this->Session->read('Auth.MstUser.id'));
        }
    }

    /**
     * 締めチェック
     */
    private function closedCheck($url, $work_date, $facility_id = null, $facility_code = null){
        if($facility_id === null && $facility_code === null){
            $this->Session->setflash('消費施設情報が取得できませんでした', 'growl', array('type'=>'error','view_no') );
            $this->redirect($url);
        }
        if($facility_id === null){
            $facility_data = $this->MstFacility->find('first',array('conditions'=>array('MstFacility.facility_code' => $facility_code,
                                                                                        'MstFacility.facility_type' => array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital'))
                                                                                        )));
            $facility_id = $facility_data['MstFacility']['id'];
        }
        if($this->canUpdateAfterClose($work_date,$facility_id) === false){
            $this->Session->setflash('消費施設は締められています', 'growl', array('type'=>'error') );
            $this->redirect($url);
        }
        return true;
    }

    function getExShippingData($sticker_no){
        $sql  = ' select ';
        $sql .= '       a.id                       as "TrnShipping__id"';
        $sql .= '     , c.internal_code            as "TrnShipping__internal_code"';
        $sql .= '     , c.item_name                as "TrnShipping__item_name"';
        $sql .= '     , c.item_code                as "TrnShipping__item_code"';
        $sql .= '     , c.standard                 as "TrnShipping__standard"';
        $sql .= '     , d.dealer_name              as "TrnShipping__dealer_name"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then b1.unit_name  ';
        $sql .= "         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .= '         end ';
        $sql .= '     )                            as "TrnShipping__unit_name" ';
        $sql .= '     , a.facility_sticker_no      as "TrnShipping__facility_sticker_no"';
        $sql .= '     , a.hospital_sticker_no      as "TrnShipping__hospital_sticker_no"';
        $sql .= '     , a.lot_no                   as "TrnShipping__lot_no"';
        $sql .= "     , to_char(a.validated_date, 'YYYY/mm/dd') " ;
        $sql .= '                                  as "TrnShipping__validated_date" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.id is null  ';
        $sql .= '         then false  ';
        $sql .= '         when e.id <> f.id  ';
        $sql .= '         then false  ';
        $sql .= '         when a.quantity = 0  ';
        $sql .= '         then false  ';
        $sql .= '         when a.is_deleted = true  ';
        $sql .= '         then false  ';
        $sql .= '         when a.has_reserved = true  ';
        $sql .= '         then false  ';
        $sql .= '         else true  ';
        $sql .= '         end ';
        $sql .= '     )                            as "TrnShipping__is_check" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.id is null  ';
        $sql .= "         then '無効なシール番号です' ";
        $sql .= '         when e.id <> f.id  ';
        $sql .= "         then '別部署に存在するシールです。' ";
        $sql .= '         when a.quantity = 0  ';
        $sql .= "         then '使用済みシールです'  ";
        $sql .= '         when a.is_deleted = true  ';
        $sql .= "         then '使用済みシールです'  ";
        $sql .= '         when a.has_reserved = true  ';
        $sql .= "         then '予約状態です'  ";
        $sql .= "         else '' ";
        $sql .= '         end ';
        $sql .= '     )                            as "TrnShipping__alert" ';
        $sql .= '     , g.sales_price              as "TrnShipping__sales_price"';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as d  ';
        $sql .= '       on d.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_departments as e  ';
        $sql .= '       on e.id = a.mst_department_id  ';
        $sql .= '     left join mst_departments as f  ';
        $sql .= '       on f.mst_facility_id = c.mst_facility_id  ';
        $sql .= '      and f.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '     left join mst_sales_configs as g ';
        $sql .= '       on g.mst_item_unit_id = b.id ';
        $sql .= '      and g.partner_facility_id = ' . $this->request->data['TrnShippingHeader']['facilityId'];
        $sql .= "      and g.start_date <= '" . $this->request->data['TrnShippingHeader']['work_date']. "'";
        $sql .= "      and g.end_date > '" . $this->request->data['TrnShippingHeader']['work_date']. "'";
        $sql .= "      and g.is_deleted = false ";
        $sql .= '   where ';
        $sql .= "     a.facility_sticker_no = '" . $sticker_no . "'";
        $sql .= "     and a.lot_no != ' ' ";
        return $this->TrnSticker->query($sql);
    }
}