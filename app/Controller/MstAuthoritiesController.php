<?php
/**
 * MstAuthoritiesController
 */
class MstAuthoritiesController extends AppController {

    var $name = 'MstAuthorities';
    var $helpers = array('Html', 'Form');

    function beforeFilter() {
        parent::beforeFilter();
    }
    
    function index() {
        $_results = $this->paginate('MstAuthority');
        $this->set('mstAuthorities', $_results);
    }

    function add() {
        if (!empty($this->request->data)) {
            $this->MstAuthority->create();
            $this->MstAuthority->save($this->request->data['MstAuthority']);
            $this->MstAuthority->save();
            $this->Session->setFlash('保存しました。');
        }
    }
    
    function edit() {
        if (!empty($this->request->data)) {
            $this->MstAuthority->create();
            $this->MstAuthority->set($this->request->data['MstAuthority']);
            if ($this->MstAuthority->save()) {
                $this->Session->setFlash('保存しました。');
            } else {
                $this->Session->setFlash('保存に失敗しました。');
            }
        }
    }
    
    function view() {
    }
    
    function afterFilter() {
        parent::afterFilter();
    }
}
?>