<?php
/**
 * StoragesSelectController
 *　入庫選択
 * @version 1.0.0
 * @since 2010/09/06
 */
class StoragesSelectController extends AppController {

    /**
    * @var $name
    */
    var $name = 'StoragesSelect';

    /**
    * @var array $uses
    */
    var $uses = array(
        'MstFacility',
        'MstClass',
        'MstTransactionConfig',
        'MstDepartment',
        'TrnReceivingHeader',
        'TrnReceiving',
        'TrnSticker',
        'TrnStickerRecord',
        'TrnStock',
        'TrnStorage',
        'TrnStorageHeader',
        'TrnStickerIssue',
    );

    /**
    * @var array $helpers
    */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    /**
     * @var array
     */
    var $components = array('RequestHandler','Storage');

    public function index() {
        $this->setRoleFunction(24); //入庫登録
        $this->redirect('search');
    }

    public function search(){
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        $fid = $this->Session->read('Auth.facility_id_selected');
        $selects = $this->Session->read('Auth.facilities');
        $this->set('facility_name', $selects[$fid]);
        $_searchResult = array();
        if(isset($this->request->data['IsSearch']) && $this->request->data['IsSearch'] == '1'){
            $_tr = $_fi = $_md = array();

            App::import('Sanitize');
            $_tr['department_id_to'] = $this->Storage->getDepartmentId($this->Session->read('Auth.facility_id_selected'), Configure::read('DepartmentType.warehouse'));

            if($this->request->data['TrnReceiving']['work_no'] != ''){
                $_tr['work_no LIKE '] = "%" . Sanitize::escape($this->request->data['TrnReceiving']['work_no']) . "%";
            }

            if($this->request->data['TrnReceiving']['work_date_from'] != ''){
                $_tr['work_date >= '] = date('Y/m/d',strtotime($this->request->data['TrnReceiving']['work_date_from']));
            }

            if($this->request->data['TrnReceiving']['work_date_to'] != ''){
                $_tr['work_date < '] = date('Y/m/d', strtotime('1 day', strtotime($this->request->data['TrnReceiving']['work_date_to'])));
            }

            //conditions for mst_facility_items
            if($this->request->data['MstFacilityItem']['internal_code'] != ''){
                $_fi['internal_code'] = Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']);
            }

            if($this->request->data['MstFacilityItem']['item_code'] != ''){
                $_fi['item_code LIKE '] = "%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_code']) . "%";
            }

            if($this->request->data['MstFacilityItem']['item_name'] != ''){
                $_fi['item_name LIKE '] = "%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%";
            }

            if($this->request->data['MstFacilityItem']['standard'] != ''){
                $_fi['standard LIKE '] = "%" . Sanitize::escape($this->request->data['MstFacilityItem']['standard']) . "%";
            }

            if($this->request->data['MstFacilityItem']['jan_code'] != ''){
                $_fi['jan_code LIKE '] = "" . Sanitize::escape($this->request->data['MstFacilityItem']['jan_code']). "%";
            }

            //conditions for MstDealer
            if($this->request->data['MstDealer']['dealer_name'] != '') {
                $_md['dealer_name LIKE '] = "%" . Sanitize::escape($this->request->data['MstDealer']['dealer_name']) . "%";
            }
            $_limit = isset($this->request->data['limit']) ? (int)$this->request->data['limit'] : min(array_keys(Configure::read('displaycounts_combobox')));

            $_params = $this->getSearchListParamater(array(), $_tr, $_fi, $_md);
            //表示データ取得
            $_searchResult = $this->getReceivings($_params, $_limit);
        }else{
            $this->request->data['TrnReceiving']['work_date_from'] =  date('Y/m/d', strtotime('-6 day', time()));
            $this->request->data['TrnReceiving']['work_date_to'] =  date('Y/m/d', time());
        }

        $this->set('data' , $this->request->data);
        $_searchResult = AppController::outputFilter($_searchResult);
        $this->set('SearchResult', $_searchResult);
    }


    public function confirm(){
        //在庫締め
        if(false === $this->canUpdateAfterClose(date('Y/m/d', time()), $this->Session->read('Auth.facility_id_selected'))){
            $this->Session->setFlash("閉め済みです。", 'growl', array('type'=>'error'));
            $this->index();
            return;
        }

        //入荷情報取得
        $params = $this->getSearchParamater(array(), array('id' => $this->request->data['TrnReceiving']['selected']));
        $res = $this->getReceivings($params);
        if(count($res) == 0){
            //ほかユーザで更新されている
            $this->Session->setFlash('既に入庫済みのデータです。\nもう一度最初からやり直してください。', 'growl', array('type'=>'error'));
            $this->index();
            return;
        }
        $this->Session->write('storages.receivingCondition', $params);
        $this->Session->write('storages.receiving', $res);

        //在庫情報取得
        $stocks = $this->Storage->getStocks($res[0]['mst_facility_item_id'], $res[0]['per_unit']);
        $this->Session->write('storages.stocks', $stocks);

        $lot_ubd_alert = $res[0]['lot_ubd_alert'];
        $expired_date = $res[0]['expired_date'];
        //Lot/有効期限警告設定確認
        if ($this->isTypeLotAndValidatedDate($lot_ubd_alert, $lotchk, $datechk)){
            $this->set('lotchk', $lotchk);
            $this->set('datechk', $datechk);
            $this->set('expired_date', $expired_date);
            $current = date("Y/m/d");
            $this->set('current_date', $current);
        }

        $this->set('res', $this->outputFilter($res[0]));
        $this->set('stocks', $stocks);
    }

    public function commit(){
        //更新チェック
        $condition = $this->Session->read('storages.receivingCondition');
        $nowR = $this->getReceivings($condition);
        if(true === $this->hasModefiedRow($this->Session->read('storages.receiving'), $nowR)){
            $this->Session->setFlash("入荷情報の更新に失敗しました。", 'growl', array('type'=>'error'));
            $this->index();
            return;
        }
        $receiving = $nowR[0];

        $nowS = $this->Storage->getStocks($receiving['mst_facility_item_id'], $receiving['per_unit']);
        if(true === $this->hasModefiedRow($this->Session->read('storages.stocks'), $nowS)){
            $this->Session->setFlash("在庫情報の更新に失敗しました。" , 'growl', array('type'=>'error'));
            $this->index();
            return;
        }

        //更新対象の在庫
        $stocks = array();
        foreach($this->request->data['TrnStock']['Rows'] as $_key => $_val){
            if($_val['selected'] == '1'){
                $_r = $this->Storage->getStockById($_key, $nowS);
                $_r['input_quantity'] = $_val['quantity'];
                $stocks[] = $_r;
            }
        }

        //入荷数量の入庫分
        $receiving['storage_quantity'] = $this->request->data['TrnStorage']['storage_quantity'];

        foreach($stocks as $key=>$value){
            $stock_sort[$key] = $value['per_unit'];
        }

        array_multisort($stock_sort ,SORT_DESC,$stocks);

        //入庫データ作成
        $this->Storage->createStorages($receiving, $stocks, array(
            'lot_no'         => $this->request->data['TrnSticker']['lot_no'],
            'validated_date' => $this->request->data['TrnSticker']['validated_date'],
            'recital'        => $this->request->data['TrnSticker']['recital'],
            'time'           => $this->request->data['TrnStorage']['time'],
        ));
        $this->set('isPrintSticker', true);
        $this->set('lastHeaderId', $this->Session->read('Storages.lastHeaderId'));
        $fid = $this->Session->read('Auth.facility_id_selected');
        $selects = $this->Session->read('Auth.facilities');
        $this->set('facility_name', $selects[$fid]);
        $this->Session->setFlash("入庫を行いました。", 'growl', array('type'=>'star'));
        
        $this->set('SearchResult', array());
        $this->render('search');
    }

    /**
     * 入荷情報検索
     * @param array $_params
     * @access private
     * @return array
     */
    private function getReceivings($_params, $limit = null ){
        $o = $this->getSortOrder();
        if(isset($o)){
            $order = $o[0];
        }else{
            $order = 'TrnReceiving.id desc';
        }
        $sql =
            ' select ' .
                ' TrnReceiving.*' .
                ',MstItemUnit.mst_facility_item_id'.
                ',MstItemUnit.mst_facility_id'.
                ',MstItemUnit.per_unit'.
                ',MstItemUnit.mst_unit_name_id'.
                ',MstItemUnit.is_standard'.
                ',MstItemUnit.per_unit_name_id'.
                ',MstFacilityItem.mst_facility_id'.
                ',MstFacilityItem.internal_code'.
                ',MstFacilityItem.item_name'.
                ',MstFacilityItem.standard'.
                ',MstFacilityItem.item_code'.
                ',MstFacilityItem.jan_code'.
                ',MstFacilityItem.mst_unit_name_id'.
                ',MstFacilityItem.unit_price'.
                ',MstFacilityItem.price'.
                ',MstFacilityItem.refund_price'.
                ',MstFacilityItem.class_separation'.
                ',MstFacilityItem.lot_ubd_alert'.
                ',MstFacilityItem.spare_key7 as type_name'.
                ',MstFacilityItem.spare_key8 as class_name'.
                ',COALESCE(MstFacilityItem.expired_date , MstFacility2.days_before_expiration) as expired_date'.
                ',MstDealer.dealer_name'.
                ',MstUnitName.unit_name as MstUnitName__unit_name'.
                ',MstParUnitName.unit_name as MstParUnitName__unit_name'.
                ",(case when MstItemUnit.per_unit = 1 then MstUnitName.unit_name else MstUnitName.unit_name || '(' || MstItemUnit.per_unit || MstParUnitName.unit_name || ')' end ) as packing_name ".
                ',MstFacility.facility_name'.
                ',TrnClaim.id as trn_claim_id'.
                ',TrnOrder.order_type as TrnOrder__order_type'.
                ',TrnOrder.work_date as TrnOrder__work_date'.
                ',MstUser.user_name'.
            ' from ' .
                ' trn_receivings as TrnReceiving '.
                ' left join trn_orders as TrnOrder on TrnReceiving.trn_order_id = TrnOrder.id'.
                ' inner join mst_item_units as MstItemUnit on MstItemUnit.id = TrnReceiving.mst_item_unit_id' .
                ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id '.
                ' left join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id' .
                ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id'.
                ' inner join mst_unit_names as MstParUnitName on MstItemUnit.per_unit_name_id = MstParUnitName.id'.
                ' inner join mst_departments as MstDepartment on TrnReceiving.department_id_from = MstDepartment.id ' .
                ' inner join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id' .
                ' inner join mst_facilities as MstFacility2 on MstFacilityItem.mst_facility_id = MstFacility2.id' .
                ' inner join mst_users as MstUser on TrnReceiving.modifier = MstUser.id ' .
                " left join trn_claims as TrnClaim on TrnReceiving.id = TrnClaim.trn_receiving_id and TrnClaim.is_stock_or_sale = '1'".
            ' where ' .
                $_params .
                ' and TrnReceiving.receiving_type = ' . Configure::read('ReceivingType.arrival') .
                ' and TrnReceiving.remain_count > 0' .
                ' and MstFacilityItem.is_lowlevel = false '.
            ' order by ' .
                $order;

        //全件取得
        if(isset($this->request->data['Search']['center_move']) && $this->request->data['Search']['center_move'] == 1){ //暫定対応
            $this->set('max' , $this->getMaxCount($sql , 'TrnReceiving' , false ));
        }else{
            $this->set('max' , $this->getMaxCount($sql , 'TrnReceiving'));
        }
        if(isset($limit)){
            $sql .= ' limit ' . $limit;
        }
        if(isset($this->request->data['Search']['center_move']) && $this->request->data['Search']['center_move'] == 1){ //暫定対応
            $_res = $this->TrnReceiving->query($sql , false , false );
        }else{
            $_res = $this->TrnReceiving->query($sql);
        }
        $_orderType = Configure::read('OrderTypes.orderTypeName');
        $_orderType['']='';
        $_classes = Set::Combine($this->MstClass->find('all', array(
            'order' => 'MstClass.id ASC',
            'recursive' => -1,
            'conditions' => array('MstClass.mst_menu_id' => '09'),
        )),'{n}.MstClass.id','{n}.MstClass.name');
        $_result = array();
        foreach($_res as $_row){
            $_r = array();
            array_walk_recursive($_row, array('StoragesSelectController','BuildSearchResult'), &$_r);
            $_r['order_type_name'] = $_orderType[$_row['trnorder']['order_type']];
            $_r['work_type_name'] = $_row[0]['work_type'] == '' ? '' : $_classes[$_row[0]['work_type']];
            $_r['order_work_date'] = $_row['trnorder']['work_date'];
            $_r['receiving_work_date'] = $_row[0]['work_date'];
            $_result[] = $_r;
        }
        return $_result;
    }

    /**
     * 検索条件を取得
     *
     * @param array $_to
     * @param array $_rh
     * @param array $_rs
     * @param array $_fi
     * @param array $_md
     * @access private
     * @return array
     */
    private function getSearchListParamater($_to = array(), $_rs = array(), $_fi = array(), $_md = array()){
        $where = ' 1 = 1 ';
        $where .= "AND TrnReceiving.is_deleted = false";
        foreach($_to as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnOrder.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnOrder.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND TrnOrder.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_rs as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnReceiving.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnReceiving.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND TrnReceiving.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_fi as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND MstFacilityItem.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND MstFacilityItem.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND MstFacilityItem.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_md as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND MstDealer.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND MstDealer.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND MstDealer.{$_key} '{$_val}' ";
                }
            }
        }
        return $where;
    }


    /**
     * 検索条件を取得
     * @param array $_rh
     * @param array $_rs
     * @param array $_fi
     * @param array $_md
     * @param array $_to
     * @param array $_mf
     * @return array
     */
    private function getSearchParamater($_rh = array(), $_rs = array(), $_fi = array(), $_md = array(), $_to = array(), $_mf = array()){
        $where = ' 1 = 1 ';
        foreach($_rh as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnReceivingHeader.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnReceivingHeader.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND TrnReceivingHeader.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_rs as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnReceiving.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnReceiving.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND TrnReceiving.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_fi as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND MstFacilityItem.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND MstFacilityItem.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND MstFacilityItem.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_md as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND MstDealer.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND MstDealer.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND MstDealer.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_to as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND TrnOrder.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND TrnOrder.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND TrnOrder.{$_key} '{$_val}' ";
                }
            }
        }

        foreach($_mf as $_key => $_val){
            if(is_array($_val)){
                $where .= " AND MstFacility.{$_key} IN (" . join(',', $_val) . ') ';
            }else{
                if(preg_match('/like|>|</i', $_key) === 0){
                    $where .= " AND MstFacility.{$_key} = '{$_val}' ";
                }else{
                    $where .= " AND MstFacility.{$_key} '{$_val}' ";
                }
            }
        }

        return $where;
    }

    /**
     * Use this with php function: array_walk_recursive
     * @param mixed $item
     * @param string $key
     * @param array $res
     * @access public
     * @static
     */
    public static function BuildSearchResult($item, $key, $res){
        //HACK:  最上位の配列値を優先するようにしてます・・
        if(false === isset($res[$key])){
            $res[$key] = $item;
        }
    }

    /**
     * 明細が更新されているか
     * @param array $pre
     * @param array $now
     * @return boolean
     */
    protected function hasModefiedRow($pre, $now){
        if(count($pre) !== count($now)){
            return true;
        }
        for($i = 0; $i < count($pre); $i++){
            if($pre[$i]['modified'] != $now[$i]['modified']){
                return true;
            }
        }
        return false;
    }


}
