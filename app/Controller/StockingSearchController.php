<?php
/**
 * Stoking_Search_Controller
 *　仕入検索　
 * @version 1.0.0
 * @since 2010/05/18
 */
class StockingSearchController extends AppController {

    /**
     * @var $name
     */
    var $name = 'StockingSearch';

    /**
     * @var array $uses
     */

    var $uses = array(
        'TrnClaim',
        'TrnReceiving',
        'TrnOrder',
        'MstDepartment',
        'MstTransactionConfig',
        'MstFacility',
        'MstItemUnit',
        'MstFacilityItem',
        'MstUserBelonging',
        'MstAccountlist'
    );

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','CsvWriteUtils');

    /**
     * @var $paginate
     */

    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    public function index(){
        $this->setRoleFunction(59); //仕入検索
        $this->redirect('search');
    }

    public function search(){
        // 勘定科目
        $this->set('accountlists' , $this->MstAccountlist->find('list' ,
                                                     array(
                                                         'conditions' => array(
                                                             'MstAccountlist.mst_facility_id' => $this->Session->read('Auth.facility_id_selected')
                                                             ),
                                                         'order' => array('MstAccountlist.id')
                                                         )
                                                     )
                   );
        
        //仕入先取得
        $_facilities = $this->getFacilityList($this->Session->read('Auth.facility_id_selected') ,
                                              array(Configure::read('FacilityType.supplier')) ,
                                              false,
                                              array('facility_code' , 'facility_name'),
                                              'facility_name, facility_code'
                                              );
        $this->set('facilities', $_facilities);

        //施主名取得
        $_donor = $this->getFacilityList($this->Session->read('Auth.facility_id_selected') , array(Configure::read('FacilityType.donor')) , false);
        $this->set('donor', $_donor);

        $_SearchResult = array();
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        App::import("sanitize");
        //検索ボタンが押されたら
        if(isset($this->request->data["IsSearch"]) && $this->request->data['IsSearch'] == '1'){

            //検索条件
            $_conditions = array('header' => '', 'detail' => '');

            if($this->request->data['TrnClaim']['is_deleted'] != '0'){
                $_conditions['header'] .= " and a.is_deleted = false";
                $check = true;//チェックボックスにチェックをつけるかつけないかのフラグ（チェックをつけて検索した場合はつけたまま、つけないで検索した場合はつけないまま）

            }else{
                $check = false;
            }

            if($this->request->data['MstFacility']['supplier_code'] != ''){
                $_conditions['header'] .= " and a5.facility_code LIKE '%".Sanitize::escape($this->request->data['MstFacility']['supplier_code'])."%'";
            }

            if($this->request->data['MstFacility']['donor_code'] != ''){
                $_conditions['header'] .= " and c1.facility_code LIKE '%".Sanitize::escape($this->request->data['MstFacility']['donor_code'])."%'";
            }

            if($this->request->data['MstFacilityItem']['internal_code'] != ''){
                $_conditions['header'] .= " and c.internal_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."%'";
            }

            if($this->request->data['MstFacilityItem']['item_code'] != ''){
                $_conditions['header'] .= " and c.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
            }

            if($this->request->data['MstFacilityItem']['item_name'] != ''){
                $_conditions['header'] .= " and c.item_name LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
            }

            if($this->request->data['MstFacilityItem']['standard'] != ''){
                $_conditions['header'] .= " and c.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
            }
            if($this->request->data['MstFacilityItem']['jan_code'] != ''){
                $_conditions['header'] .= " and c.jan_code LIKE '".Sanitize::escape($this->request->data['MstFacilityItem']['jan_code'])."%'";
            }
            if($this->request->data['MstFacilityItem']['mst_accountlist_id'] != ''){
                $_conditions['header'] .= " and c.mst_accountlist_id = ".$this->request->data['MstFacilityItem']['mst_accountlist_id'];
            }

            if($this->request->data['TrnClaim']['claim_date_from'] != ''){
                $_conditions['detail'] .= " and a.claim_date >= '".Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnClaim']['claim_date_from'])))."'";
                $this->set('date_from',$this->request->data['TrnClaim']['claim_date_from']);
            }

            if($this->request->data['TrnClaim']['claim_date_to'] != ''){
                $_conditions['detail'] .= " and a.claim_date < '".Sanitize::escape(date('Y/m/d', strtotime('1 day',strtotime($this->request->data['TrnClaim']['claim_date_to']))))."'";
                $this->set('date_to',$this->request->data['TrnClaim']['claim_date_to']);
            }

            if($this->request->data['MstDealer']['dealer_name'] != ''){
                $_conditions['header'] .= " and e.dealer_name LIKE '%".Sanitize::escape($this->request->data['MstDealer']['dealer_name'])."%'";
            }

            $_limit = $this->_getLimitCount();

            $_SearchResult = $this->TrnClaim->query($this->getSearchSQL($_conditions,$_limit));
            $this->set('check',$check);
            $search = true;
            $this->set('search',$search);
        }else{
            $this->request->data['TrnClaim']['claim_date_from'] = date('Y/m/01');
            $this->request->data['TrnClaim']['claim_date_to'] = date('Y/m/t');
        }
        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set('data', $this->request->data);
        $this->set('SearchResult', $_SearchResult);
    }

    public function confirm(){
        $_SearchResult = $this->TrnClaim->query($this->getDetailSQL(join(',' , $this->request->data['TrnClaim']['id'])));
        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set('SearchResult', $_SearchResult);
        $this->render('confirm');
    }

    public function result(){
        $now = date('Y/m/d H:i:s.u');

        //トランザクション
        $this->TrnClaim->begin();
        
        $ids = join(',',$this->request->data['TrnClaim']['id']);
        //行ロック
        $this->TrnClaim->query("select * from trn_claims as a where a.id in ( " . $ids . ") for update  " );
        
        // 必要情報を取得
        $sql  = ' select ';
        $sql .= '   a.id         as "TrnClaim__id" ';
        $sql .= ' , a.count      as "TrnClaim__count" ';
        $sql .= ' , a.unit_price as "TrnClaim__unit_price" ';
        $sql .= ' , a.round      as "TrnClaim__round" ';
        $sql .= ' , a.gross      as "TrnClaim__gross" ';
        $sql .= ' from trn_claims as a ';
        $sql .= ' where id in (' . $ids . ')' ;
        $ret = $this->TrnClaim->query($sql);
        
        foreach($ret as $r ){
            if(isset($this->request->data['TrnClaim']['unit_price'][$r['TrnClaim']['id']])){
                $unit_price = $this->request->data['TrnClaim']['unit_price'][$r['TrnClaim']['id']];
            }else{
                $unit_price = $r['TrnClaim']['unit_price'];
            }
            if(isset($this->request->data['TrnClaim']['retroactive'][$r['TrnClaim']['id']])){
                $is_not_retroactive = true;
            }else{
                $is_not_retroactive = false;
            }
            if(isset($this->request->data['TrnClaim']['claim_date'][$r['TrnClaim']['id']])){
                $claim_date = $this->request->data['TrnClaim']['claim_date'][$r['TrnClaim']['id']];
            }else{
                $claim_date = $r['TrnClaim']['claim_date'];
            }
            
            
            // 受注ヘッダ
            $this->TrnClaim->create();
            $result = $this->TrnClaim->updateAll(
                array(
                    'TrnClaim.claim_date'          => "'" . $claim_date . "'", 
                    'TrnClaim.unit_price'          => $unit_price,
                    'TrnClaim.claim_price'         => $this->getFacilityUnitPrice($r['TrnClaim']['round'], $unit_price , $r['TrnClaim']['count']),
                    'TrnClaim.is_not_retroactive'  => $is_not_retroactive,
                    'TrnClaim.modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'TrnClaim.modified'            => "'" . $now . "'"
                    ),
                array(
                    'TrnClaim.id' => $r['TrnClaim']['id'],
                    ),
                -1
                );
            if(!$result){
                $this->TrnClaim->rollback();
                $this->Session->setFlash('仕入編集処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('search');
            }
        }

        //コミット前に更新チェック
        $ret = $this->TrnClaim->query("select count(*) from trn_claims as a where a.id in ( " . $ids . ")  and a.modified <> '" .$now. "' and a.modified > '" . $this->request->data['TrnClaim']['time'] . "'" );
        if($ret[0][0]['count'] > 0){
            // ロールバック
            $this->TrnClaim->rollback();
            $this->Session->setFlash('ほかのユーザによって更新されています。最初からやり直してください', 'growl', array('type'=>'error') );
            $this->redirect('search');
        }
        $this->TrnClaim->commit();
        //表示データを取得
        $_SearchResult = $this->TrnClaim->query($this->getDetailSQL($ids));
        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set('SearchResult', $_SearchResult);
        $this->render('result');
    }

    public function report(){
        App::import("sanitize");
        $_conditions = array('header' => '', 'detail' => '');
        
        if($this->request->data['TrnClaim']['is_deleted'] != '0'){
            $_conditions['header'] .= " and a.is_deleted = false";
        }
        
        if($this->request->data['MstFacility']['supplier_code'] != ''){
            $_conditions['header'] .= " and a5.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['supplier_code'])."'";
        }
        
        if($this->request->data['MstFacility']['donor_code'] != ''){
            $_conditions['header'] .= " and c1.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['donor_code'])."'";
        }
        
        if($this->request->data['MstFacilityItem']['internal_code'] != ''){
            $_conditions['header'] .= " and c.internal_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."%'";
        }
        
        if($this->request->data['MstFacilityItem']['item_code'] != ''){
            $_conditions['header'] .= " and c.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        
        if($this->request->data['MstFacilityItem']['item_name'] != ''){
            $_conditions['header'] .= " and c.item_name LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
        }
        
        if($this->request->data['MstFacilityItem']['standard'] != ''){
            $_conditions['header'] .= " and c.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }
        //JAN(前方一致)
        if($this->request->data['MstFacilityItem']['jan_code'] != ''){
            $_conditions['header'] .= " and c.jan_code LIKE '".Sanitize::escape($this->request->data['MstFacilityItem']['jan_code'])."%'";
        }
        if($this->request->data['MstFacilityItem']['mst_accountlist_id'] != ''){
            $_conditions['header'] .= " and c.mst_accountlist_id = " . $this->request->data['MstFacilityItem']['mst_accountlist_id'];
        }
        
        if($this->request->data['TrnClaim']['claim_date_from'] != ''){
            $_conditions['detail'] .= " and a.claim_date >= '".Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnClaim']['claim_date_from'])))."'";
            $this->set('date_from',$this->request->data['TrnClaim']['claim_date_from']);
        }
        
        if($this->request->data['TrnClaim']['claim_date_to'] != ''){
            $_conditions['detail'] .= " and a.claim_date < '".Sanitize::escape(date('Y/m/d', strtotime('1 day',strtotime($this->request->data['TrnClaim']['claim_date_to']))))."'";
            $this->set('date_to',$this->request->data['TrnClaim']['claim_date_to']);
        }
        
        if($this->request->data['MstDealer']['dealer_name'] != ''){
            $_conditions['header'] .= " and e.dealer_name LIKE '%".Sanitize::escape($this->request->data['MstDealer']['dealer_name'])."%'";
        }
        $_limit = $this->_getLimitCount();
        $_SearchResult = $this->TrnClaim->query($this->getPrintSQL($_conditions,$_limit));
        
        //帳票ヘッダ
        $head = array(
            '仕入先名',
            '期間',
            '行番号',
            '商品名',
            '規格',
            '製品番号',
            '販売元',
            '金額',
            '単価',
            '数量',
            '包装単位',
            '課税区分',
            '売上日',
            '商品ID',
            '発注書番号',
            '印刷年月日',
        );

        //帳票データ
        $result = array();
        $i = 1;
        $pre_supplier = null;

        foreach($_SearchResult as $_row){
            $r = $_row[0];

            $tax_type = Configure::read('PrintFacilityItems.taxes');
            if(isset($tax_type[$r['tax_type']])){
                $r['tax_type'] = $tax_type[$r['tax_type']];
            }else{
                $r['tax_type'] = '';
            }

            if($r['claim_price'] != ""){
                 switch($r['round']){
                    case Configure::read('RoundType.Round'):
                        $r['claim_price'] = number_format(round($r['claim_price']));
                        break;
                    case Configure::read('RoundType.Ceil'):
                        $r['claim_price'] = number_format(ceil($r['claim_price']));
                        break;
                    case Configure::read('RoundType.Floor'):
                        $r['claim_price'] = number_format(floor($r['claim_price']));
                        break;
                 }
            }

            //仕入先は切り替わったら行番号はカウントしなおし
            if($pre_supplier != null && $pre_supplier !=$r['supplier'] ){
                $i = 1;
            }
            $result[] = array(
                '仕入先名'   => $r['supplier'],
                '期間'       => $this->request->data['TrnClaim']['claim_date_from']."～".$this->request->data['TrnClaim']['claim_date_to'],
                '行番号'     => $i,
                '商品名'     => $r['item_name'],
                '規格'       => $r['standard'],
                '製品番号' => $r['item_code'],
                '販売元'     => $r['dealer_name'],
                '金額'       => $r['claim_price'],
                '単価'       => $r['unit_price'],
                '数量'       => $r['amount'],
                '包装単位'   => $r['packing_name'],
                '課税区分'   => $r['tax_type'],
                '売上日'     => $r['claim_date'],
                '商品ID'     => $r['internal_code'],
                '発注書番号' => $r['work_no'],
                '印刷年月日' => date("Y/m/d")
                );
            $i++;
            $pre_supplier = $r['supplier'];
        }

        $this->layout = 'xml/default';
        $this->set('head', $head);
        $this->set('result',$result);

        $this->render('print');
    }

    private function getSearchSQL($conditions, $limit){
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , a.unit_price ';
        $sql .= '     , a.claim_price ';
        $sql .= '     , a.count ';
        $sql .= '     , a.round ';
        $sql .= "     , to_char(a.claim_date,'YYYY/mm/dd') as claim_date ";
        $sql .= "     , ( case when a.is_not_retroactive = '1' then '○' else '' end )";
        $sql .= '                                          as is_not_retroactive ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then g.unit_name  ';
        $sql .= "         else g.unit_name || '(' || b.per_unit || h.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                        as packing_name ';
        $sql .= '     , a2.work_no ';
        $sql .= '     , a4.transaction_price ';
        $sql .= '     , a5.facility_name                       as supplier ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , c.item_name ';
        $sql .= '     , c.standard ';
        $sql .= '     , c.item_code ';
        $sql .= '     , (  ';
        $sql .= '       case a.claim_type ';
        foreach(Configure::read('ClaimTypes') as $k => $v ){
            $sql .= "   when {$k} then '${v}'";
        }
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                                        as type_name ';
        $sql .= '     , c1.facility_name                       as donor_name ';
        $sql .= '     , e.dealer_name  ';
        $sql .= '   from ';
        $sql .= '     trn_claims as a  ';
        $sql .= '     left join trn_receivings as a1  ';
        $sql .= '       on a.trn_receiving_id = a1.id  ';
        $sql .= '     left join trn_orders as a2  ';
        $sql .= '       on a1.trn_order_id = a2.id  ';
        $sql .= '     left join mst_departments a3  ';
        $sql .= '       on a.department_id_from = a3.id  ';
        $sql .= '     left join mst_transaction_configs as a4  ';
        $sql .= '       on a2.mst_item_unit_id = a4.mst_item_unit_id  ';
        $sql .= '       and a3.mst_facility_id = a4.partner_facility_id  ';
        $sql .= '       and a4.start_date <= a.claim_date  ';
        $sql .= '       and a4.end_date > a.claim_date  ';
        $sql .= '     left join mst_facilities as a5  ';
        $sql .= '       on a3.mst_facility_id = a5.id  ';
        $sql .= '     left join trn_retroact_records as a6  ';
        $sql .= '       on a6.id = a.trn_retroact_record_id  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on c.mst_dealer_id = e.id  ';
        $sql .= '     left join mst_facilities as c1  ';
        $sql .= '       on c.mst_owner_id = c1.id  ';
        $sql .= "       and c1.facility_type =".Configure::read('FacilityType.donor');
        $sql .= '     left join mst_unit_names as g  ';
        $sql .= '       on b.mst_unit_name_id = g.id  ';
        $sql .= '     left join mst_unit_names as h  ';
        $sql .= '       on b.per_unit_name_id = h.id  ';
        $sql .= '   where ';
        $sql .= "     a.is_stock_or_sale = '1'  ";
        $sql .= '     and c.mst_facility_id =' . $this->Session->read('Auth.facility_id_selected');
//        $sql .= '     and a.is_deleted = false  ';

        $sql .= ($this->isNNs($conditions['header']) ? '' : $conditions['header']) ;
        $sql .= ($this->isNNs($conditions['detail']) ? '' : $conditions['detail']) ;

        if($this->getSortOrder() === null){
            $sql .= ' order by a.id ASC';
        }else{
            $arrayOrder = $this->getSortOrder();
            $sql .= ' order by '.$arrayOrder[0];
        }
        if((integer)$limit > 0){
            $this->set('max' , $this->getMaxCount($sql , 'TrnClaim'));
            $sql .= " limit {$limit} ";
        }
        return $sql;
    }

    private function getPrintSQL($conditions, $limit){
        $sql =
            ' select '.
                    ' a5.facility_name as supplier'.
                    ',c.item_name'.
                    ',c.standard'.
                    ',c.item_code'.
                    ',e.dealer_name'.
                    ',a.claim_price '.
                    ',a.unit_price '.
                    ',a.count as amount'.
                    ',a.round'.
                    ",(case when b.per_unit = 1 then g.unit_name else g.unit_name || '(' || b.per_unit || h.unit_name || ')' end) as packing_name".
                    ',c.tax_type'.
                    ",to_char(a.claim_date,'YYYY/mm/dd') as claim_date".
                    ',c.internal_code'.
                    ',a2.work_no'.
            ' from '.
                    ' trn_claims as a'.
                    ' left join trn_receivings as a1'.
                        ' on a.trn_receiving_id = a1.id'.
                    ' left join trn_orders as a2'.
                        ' on a1.trn_order_id = a2.id'.
                    ' left join mst_departments a3'.
                        ' on a.department_id_from = a3.id'.
                    ' left join mst_transaction_configs as a4'.
                        ' on a2.mst_item_unit_id = a4.mst_item_unit_id'.
                        ' and a3.mst_facility_id = a4.partner_facility_id'.
                        ' and a4.start_date <= a.claim_date'.
                        ' and a4.end_date > a.claim_date'.
                    ' left join mst_facilities as a5'.
                        ' on a3.mst_facility_id = a5.id'.
                        " and a5.facility_type=" . Configure::read('FacilityType.supplier') .
                        ' ,mst_item_units as b'.
                        ' ,mst_facility_items as c'.
                    ' left join mst_dealers as e'.
                        ' on c.mst_dealer_id = e.id'.
                    ' left join mst_facilities as c1'.
                        ' on c.mst_owner_id = c1.id'.
                        " and c1.facility_type=" . Configure::read('FacilityType.donor').
                    ',mst_unit_names as g'.
                    ',mst_unit_names as h'.
            ' where '.
                    ' a.mst_item_unit_id = b.id'.
                    ' and b.mst_facility_item_id = c.id'.

                    " and a.is_stock_or_sale = '1'".
                    ' and b.mst_unit_name_id = g.id ' .
                    ' and b.per_unit_name_id = h.id '.
                    ' and c.mst_facility_id = '.$this->request->data['session'].
                    ($this->isNNs($conditions['header']) ? '' : $conditions['header']) .
                    ($this->isNNs($conditions['detail']) ? '' : $conditions['detail']) .
            ' order by ' .
                    ' a5.facility_name asc';

        return $sql;
    }

    private function getDetailSQL($conditions){
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , a.unit_price ';
        $sql .= '     , a.claim_price ';
        $sql .= '     , a.count ';
        $sql .= '     , a.round ';
        $sql .= '     , a.is_deleted ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then g.unit_name  ';
        $sql .= "         else g.unit_name || '(' || b.per_unit || h.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                        as packing_name ';
        $sql .= '     , cast (a.is_not_retroactive as varchar) as is_not_retroactive ';
        $sql .= "     , to_char(a.claim_date ,'YYYY/MM/DD') as claim_date ";
        $sql .= '     , a2.work_no ';
        $sql .= '     , a4.transaction_price ';
        $sql .= '     , a5.facility_name                       as supplier ';
        $sql .= '     , (  ';
        $sql .= '       case a.claim_type ';
        foreach(Configure::read('ClaimTypes') as $k => $v ){
            $sql .= "   when {$k} then '${v}'";
        }
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                                        as type_name ';
        $sql .= '    , ( ';
        $sql .= '      case  ';
        $sql .= '        when a6.retroact_type in ( 2 , 4 , 5 )  ';
        $sql .= '        then false  ';
        $sql .= '        when a.is_deleted = true  ';
        $sql .= '        then false  ';
        $sql .= '        when a.stocking_close_type <> 0  ';
        $sql .= '        then false  ';
        $sql .= '        when a.facility_close_type <> 0  ';
        $sql .= '        then false  ';
        $sql .= '        else true  ';
        $sql .= '        end ';
        $sql .= '    )                                        as status ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , c.item_name ';
        $sql .= '     , c.standard ';
        $sql .= '     , c.item_code ';
        $sql .= '     , c1.facility_name                       as donor_name ';
        $sql .= '     , e.dealer_name  ';
        $sql .= '   from ';
        $sql .= '     trn_claims as a  ';
        $sql .= '     left join trn_receivings as a1  ';
        $sql .= '       on a.trn_receiving_id = a1.id  ';
        $sql .= '     left join trn_orders as a2  ';
        $sql .= '       on a1.trn_order_id = a2.id  ';
        $sql .= '     left join mst_departments a3  ';
        $sql .= '       on a.department_id_from = a3.id  ';
        $sql .= '     left join mst_transaction_configs as a4  ';
        $sql .= '       on a2.mst_item_unit_id = a4.mst_item_unit_id  ';
        $sql .= '       and a3.mst_facility_id = a4.partner_facility_id  ';
        $sql .= '       and a4.start_date <= a.claim_date  ';
        $sql .= '       and a4.end_date > a.claim_date  ';
        $sql .= '     left join mst_facilities as a5  ';
        $sql .= '       on a3.mst_facility_id = a5.id  ';
        $sql .= '     left join trn_retroact_records as a6  ';
        $sql .= '       on a6.id = a.trn_retroact_record_id  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on c.mst_dealer_id = e.id  ';
        $sql .= '     left join mst_facilities as c1  ';
        $sql .= '       on c.mst_owner_id = c1.id  ';
        $sql .= "       and c1.facility_type = " . Configure::read('FacilityType.donor');
        $sql .= '     left join mst_unit_names as g  ';
        $sql .= '       on b.mst_unit_name_id = g.id  ';
        $sql .= '     left join mst_unit_names as h  ';
        $sql .= '       on b.per_unit_name_id = h.id  ';
        $sql .= '   where ';
        $sql .= "     a.is_stock_or_sale = '1'  ";
        $sql .= '     and a.id in (' . $conditions . ')';
        $sql .= '   order by ';
        $sql .= '     a.id asc ';

        return $sql;
    }

    public function export_csv() {
        App::import("sanitize");

        //条件の作成
        $where = "";
        if($this->request->data['TrnClaim']['is_deleted'] != '0'){
            $where .= " and a.is_deleted = false";
        }
        if($this->request->data['TrnClaim']['claim_date_from'] != ''){
            $where .= " and a.claim_date >= '".Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnClaim']['claim_date_from'])))."'";
        }
        if($this->request->data['TrnClaim']['claim_date_to'] != ''){
            $where .= " and a.claim_date < '".Sanitize::escape(date('Y/m/d', strtotime('1 day',strtotime($this->request->data['TrnClaim']['claim_date_to']))))."'";
        }
        if($this->request->data['MstFacilityItem']['internal_code'] != ''){
            $where .= " and c.internal_code LIKE '".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."%'";
        }
        if($this->request->data['MstFacilityItem']['item_code'] != ''){
            $where .= " and c.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        if($this->request->data['MstFacilityItem']['item_name'] != ''){
            $where .= " and c.item_name LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
        }
        if($this->request->data['MstFacilityItem']['standard'] != ''){
            $where .= " and c.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }
        //JAN(前方一致)
        if($this->request->data['MstFacilityItem']['jan_code'] != ''){
            $where .= " and c.jan_code LIKE '".Sanitize::escape($this->request->data['MstFacilityItem']['jan_code'])."%'";
        }
        if($this->request->data['MstFacilityItem']['mst_accountlist_id'] != ''){
            $where .= " and c.mst_accountlist_id = " . $this->request->data['MstFacilityItem']['mst_accountlist_id'];
        }
        if($this->request->data['MstDealer']['dealer_name'] != ''){
            $where .= " and c1.dealer_name LIKE '%".Sanitize::escape($this->request->data['MstDealer']['dealer_name'])."%'";
        }
        if($this->request->data['MstFacility']['supplier_code'] != ''){
            $where .= " and a2.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['supplier_code'])."'";
        }
        if($this->request->data['MstFacility']['donor_code'] != ''){
            $where .= " and c3.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['donor_code'])."'";
        }
        if($this->Session->read('Auth.facility_id_selected') != ''){
            $where .= " and c2.id = " . $this->Session->read('Auth.facility_id_selected');
        }

        $sql  = ' select ';
        $sql .= '       a2.facility_code                as 仕入先コード ';
        $sql .= '     , a2.facility_name                as 仕入先名 ';
        $sql .= "     , to_char(a.claim_date,'YYYY/mm/dd')   as 仕入日 ";
        $sql .= '     , c2.facility_code                as 施設コード ';
        $sql .= '     , c.internal_code                 as 商品id ';
        $sql .= '     , c.item_name                     as 商品名 ';
        $sql .= '     , c.standard                      as 規格 ';
        $sql .= '     , c.item_code                     as 製品番号 ';
        $sql .= '     , c4.name                         as 勘定科目';
        $sql .= '     , c1.dealer_name                  as 販売元名 ';
        $sql .= '     , c.jan_code                      as janコード ';
        $sql .= '     , b1.unit_name                    as 包装単位名 ';
        $sql .= '     , b.per_unit                      as 包装単位入数 ';
        $sql .= '     , b2.unit_name                    as 基本単位名 ';
        $sql .= '     , c3.facility_code                as 施主コード ';
        $sql .= '     , c3.facility_name                as 施主名 ';
        $sql .= '     , a.count                         as 数量 ';
        $sql .= '     , a.unit_price                    as 単価 ';
        $sql .= '     , a.claim_price                   as 金額 ';
        $sql .= '     , d.transaction_price             as マスタ単価 ';
        $sql .= '     , (  ';
        $sql .= '       case c.tax_type  ';
        $sql .= "         when '1' then '課税'  ";
        $sql .= "         when '0' then '非課税'  ";
        $sql .= "         else '未設定'  ";
        $sql .= '         end ';
        $sql .= '     )                                 as 課税区分 ';
        $sql .= '     , e1.work_no                      as 発注番号 ';
        $sql .= '     , (  ';
        $sql .= '       case a.claim_type ';
        foreach(Configure::read('ClaimTypes') as $k => $v ){
            $sql .= "   when {$k} then '${v}'";
        }
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                                 as 区分 ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
//        $sql .= '         when f.trade_type = 5  ';
        $sql .= '         when XX.storage_type = ' . Configure::read('StorageType.direct');
        $sql .= '         then j.facility_name  ';
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                                 as 病院名 ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.is_deleted = true  ';
        $sql .= "         then '1'  ";
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                                 as 削除フラグ ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.is_not_retroactive = true  ';
        $sql .= "         then '1'  ";
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                                 as 遡及除外  ';
        $sql .= '   from ';
        $sql .= '     trn_claims as a  ';
        $sql .= '     left join mst_departments as a1  ';
        $sql .= '       on a.department_id_from = a1.id  ';
        $sql .= '     left join mst_facilities as a2  ';
        $sql .= '       on a1.mst_facility_id = a2.id  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on a.mst_item_unit_id = b.id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b.mst_unit_name_id = b1.id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b.per_unit_name_id = b2.id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on b.mst_facility_item_id = c.id  ';
        $sql .= '     left join mst_dealers as c1  ';
        $sql .= '       on c.mst_dealer_id = c1.id  ';
        $sql .= '     left join mst_facilities as c2  ';
        $sql .= '       on c.mst_facility_id = c2.id  ';
        $sql .= '     left join mst_facilities as c3  ';
        $sql .= '       on c.mst_owner_id = c3.id  ';
        $sql .= '     left join mst_accountlists as c4';
        $sql .= '       on c4.id = c.mst_accountlist_id ';
        $sql .= '     left join mst_transaction_configs as d  ';
        $sql .= '       on a1.mst_facility_id = d.partner_facility_id  ';
        $sql .= '       and a.mst_item_unit_id = d.mst_item_unit_id  ';
        $sql .= '       and a.claim_date >= d.start_date  ';
        $sql .= '       and a.claim_date < d.end_date  ';
        $sql .= '     left join trn_receivings as e  ';
        $sql .= '       on a.trn_receiving_id = e.id  ';
        $where_trnstorages = ' and 1=1 ';
        if($this->request->data['TrnClaim']['claim_date_from'] != ''){
            $where_trnstorages .= " and X.work_date >= '".Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnClaim']['claim_date_from'])))."'";
        }
        if($this->request->data['TrnClaim']['claim_date_to'] != ''){
            $where_trnstorages .= " and X.work_date <  '".Sanitize::escape(date('Y/m/d', strtotime('1 day',strtotime($this->request->data['TrnClaim']['claim_date_to']))))."'";
        }
        $sql .= '     left join (select X.trn_receiving_id ';
        $sql .= '                      ,X.storage_type';
        $sql .= '                      ,max(X.trn_sticker_id) as trn_sticker_id';
        $sql .= '                  from trn_storages X ';
        $sql .= '                 where X.storage_type=' . Configure::read('StorageType.direct');
        $sql .= '                   and X.is_deleted = false';
        $sql .= $where_trnstorages;
        $sql .= '                 group by trn_receiving_id';
        $sql .= '                         ,X.storage_type';
        $sql .= '               ) XX ';
        $sql .= '       on e.id=XX.trn_receiving_id';
        $sql .= '     left join trn_orders as e1  ';
        $sql .= '       on e.trn_order_id = e1.id  ';
        $sql .= '     left join trn_stickers as f  ';
        $sql .= '       on f.id = XX.trn_sticker_id  ';
        $sql .= '     left join trn_retroact_records as g  ';
        $sql .= '       on g.id = a.trn_retroact_record_id  ';
        $sql .= '     left join trn_shippings as h  ';
        $sql .= '       on h.id = f.trn_shipping_id  ';
        $sql .= '     left join mst_departments as i  ';
        $sql .= '       on h.department_id_to = i.id  ';
        $sql .= '     left join mst_facilities as j  ';
        $sql .= '       on j.id = i.mst_facility_id  ';
        $sql .= '   where ';
        $sql .= "     a.is_stock_or_sale = '1'  ";
        $sql .= $where ;
        $sql .= '   order by ';
        $sql .= '     a2.facility_code ';
        $sql .= '     , a.claim_date ';
        $sql .= '     , c.item_name ';

        $this->db_export_csv($sql , "仕入一覧", 'index');
    }
}
