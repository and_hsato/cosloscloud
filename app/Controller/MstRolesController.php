<?php
/**
 * MstRolesController
 *
 * @version 1.0.0
 * @since 2010/07/13
 */

class MstRolesController extends AppController {
    var $name = 'MstRoles';
    
    /**
     *
     * @var array $uses
     */
    var $uses = array('MstRole','MstPrivilege','MstMenu', 'MstEdition', 'MstRoleType');
    
    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;
    
    /**
     * @var MstFacilities
     */
    var $MstRoles;
    
    
    function beforeRender() {
        // エディション一覧をセット
        $editions = $this->MstEdition->find('list', array(
                'conditions' => array('is_deleted' => false),
                'order' => array('id' => 'asc')));
        $this->set('mstEditions', $editions);
        
        // ロールタイプ一覧をセット
        $roleTypes = $this->MstRoleType->find('list', array(
                'conditions' => array('is_deleted' => false),
                'order' => array('id' => 'asc')));
        $this->set('mstRoleTypes', $roleTypes); 
    }
    
    function index(){
        $this->roles_list();
    }
    
    /**
     * roles_list
     *
     * 施設一覧
     */
    function roles_list() {
        $this->setRoleFunction(87); //ロールマスタ
        
        //ロール情報の取得
        $params = array (
            'conditions' => array(' 1 = 1 '),
            'fields'     => array('MstRole.id',
                                  'MstRole.role_name',
                                  'MstRole.role_name_s',
                                  'MstRole.mst_role_type_id',
                                  'MstRole.mst_edition_id',
                                  'MstRole.is_deleted'),
            'order'      => array('MstRole.mst_role_type_id', 'MstRole.id'),
            'recursive'  => -1
            );
        
        $result = array();
        
        if(isset($this->request->data['search']['is_search'])){
            
            //ユーザ入力値による検索条件の作成--------------------------------------------
            //ロール名(LIKE検索)
            if($this->request->data['MstRole']['search_role_name'] != ""){
                $where = array('MstRole.role_name LIKE' => "%".$this->request->data['MstRole']['search_role_name']."%");
                $params['conditions'] = array_merge($params['conditions'],$where);
            }
            //削除済みも表示
            if(!isset($this->request->data['MstRole']['search_is_deleted'])){
                $where = array('MstRole.is_deleted' => false);
                $params['conditions'] = array_merge($params['conditions'],$where);
            }
            //検索条件の作成終了---------------------------------------------------------
            //SQL実行
            $result = $this->MstRole->find('all', $params);
        }
        
        $this->set('result',$result);
        $this->render('roles_list');
    }
    
    /**
     * Add 施設新規登録
     */
    function add() {
        
        $sql  =' select ';
        $sql .='       c.name       as "MstRole__Headername" ';
        $sql .='     , b.name       as "MstRole__name"';
        $sql .='     , b.view_no    as "MstRole__view_id"';
        $sql .='   from ';
        $sql .='     mst_views as b  ';
        $sql .='     inner join mst_view_headers as c  ';
        $sql .='       on b.view_header_id = c.id  ';
        $sql .='     left join mst_view_groups as d';
        $sql .='       on d.id = c.mst_view_group_id ';
        $sql .='   order by ';
        $sql .='     d.order_num , c.order_num , b.order_num ';
        
        $result = $this->MstRole->query($sql);
        
        
        $this->set('result',$result);//権限情報をセット
        $this->render('add');
    }
    
    /**
     * Mod ロール情報編集
     */
    function mod() {
        $sql  =' select ';
        $sql .='       c.name             as "MstRole__Headername" ';
        $sql .='     , b.name             as "MstRole__name"';
        $sql .='     , b.view_no          as "MstRole__view_id"';
        $sql .='     , d.is_deleted       as "MstRole__is_deleted"';
        $sql .='     , d.role_name        as "MstRole__role_name"';
        $sql .='     , d.role_name_s      as "MstRole__role_name_s"';
        $sql .='     , d.mst_role_type_id as "MstRole__mst_role_type_id"';
        $sql .='     , d.mst_edition_id   as "MstRole__mst_edition_id"';
        $sql .='     , (  ';
        $sql .='       case  ';
        $sql .='         when a.updatable = true then 3 ';
        $sql .='         when a.creatable = true then 2 ';
        $sql .='         when a.readable  = true then 1 ';
        $sql .='         else 0  ';
        $sql .='          end ';
        $sql .='     ) as "MstRole__Authority"';
        $sql .='   from ';
        $sql .='     mst_views as b ';
        $sql .='     left join mst_privileges as a ';
        $sql .='       on b.view_no = a.view_no ';
        $sql .='      and a.mst_role_id = ' . $this->request->data['MstRole']['id'];
        $sql .='     left join mst_view_headers as c ';
        $sql .='       on b.view_header_id = c.id ';
        $sql .='     left join mst_view_groups as c1';
        $sql .='       on c1.id = c.mst_view_group_id ';
        $sql .='     left join mst_roles as d ';
        $sql .='       on d.id = a.mst_role_id ';
        $sql .='   where ';
        $sql .='     a.mst_role_id = ' . $this->request->data['MstRole']['id'];
        $sql .='     or a.mst_role_id is null';
        $sql .='   order by ';
        $sql .='     c1.order_num , c.order_num , b.order_num ';
        
        $result = $this->MstRole->query($sql);
        
        foreach($result as $data){
            if(!empty($data['MstRole']['role_name'])){
                $this->request->data['MstRole']['role_name']        = $data['MstRole']['role_name'];
                $this->request->data['MstRole']['role_name_s']      = $data['MstRole']['role_name_s'];
                $this->request->data['MstRole']['mst_role_type_id'] = $data['MstRole']['mst_role_type_id'];
                $this->request->data['MstRole']['mst_edition_id']   = $data['MstRole']['mst_edition_id'];
                $this->request->data['MstRole']['is_deleted']       = $data['MstRole']['is_deleted'];
                break;
            }
        }
        
        $this->set('result',$result);//権限情報をセット
        $this->render('mod');
    }
    
    /**
     * result
     *
     * ロール情報更新（新規登録・更新）
     */
    function result() {
        $now = date('Y/m/d H:i:s');
        //トランザクション開始
        $this->MstRole->begin();
        
        if(!empty($this->request->data['MstRole']['id'])) {
            //更新処理
            $Role = array(
                'MstRole' => array(
                    'id'               => $this->request->data['MstRole']['id'],
                    'role_name'        => $this->request->data['MstRole']['role_name'],
                    'role_name_s'      => $this->request->data['MstRole']['role_name_s'],
                    'mst_role_type_id' => $this->request->data['MstRole']['mst_role_type_id'],
                    'mst_edition_id'   => $this->request->data['MstRole']['mst_edition_id'],
                    'is_deleted'       => (isset($this->request->data['MstRole']['is_deleted'])?true:false),
                    'modifier'         => $this->Session->read('Auth.MstUser.id'),
                    'modified'         => $now,
                ));
            if(!$this->MstRole->save($Role)){
                //エラー処理
                $this->MstRole->rollback();
            }
            
            foreach($this->request->data['MstRole']['Authority'] as $view_id => $val){
                $count = $this->MstPrivilege->find('count', array(
                    'conditions' => array('mst_role_id' => $this->request->data['MstRole']['id'] ,
                                          'view_no'     => $view_id
                                          )
                    ));
                if($count == 0){
                    $Privilege = array(
                        'MstPrivilege' => array(
                            'mst_role_id' => $this->request->data['MstRole']['id'],
                            'view_no'     => $view_id,
                            'updatable'   => (($val == 3)?true:false),
                            'creatable'   => (($val >= 2)?true:false),
                            'readable'    => (($val >= 1)?true:false),
                            'deletable'   => false,
                            'is_deleted'  => false,
                            'creater'     => $this->Session->read('Auth.MstUser.id'),
                            'modifier'    => $this->Session->read('Auth.MstUser.id'),
                            'created'     => $now,
                            'modified'    => $now,
                            
                            )
                        );
                    $this->MstPrivilege->create();
                    if(!$this->MstPrivilege->save($Privilege)){
                        //エラー処理
                        $this->MstRole->rollback();
                    }
                }else{
                    // 更新
                    $this->MstPrivilege->create();
                    if(!$this->MstPrivilege->updateAll(
                        array(
                            'updatable'   => (($val == 3)? 'true' : 'false' ),
                            'creatable'   => (($val >= 2)? 'true' : 'false' ),
                            'readable'    => (($val >= 1)? 'true' : 'false' ),
                            'modifier'    => $this->Session->read('Auth.MstUser.id'),
                            'modified'    => "'" . $now . "'"
                            )
                        ,
                        array(
                            'mst_role_id = ' => $this->request->data['MstRole']['id'] ,
                            'view_no = '     => $view_id ,
                            ),
                        -1
                        )) {
                        //エラー処理
                        $this->MstRole->rollback();
                    }
                }
            }
        }else{
            //新規作成
            $Role = array(
                'MstRole' => array(
                    'role_name'        => $this->request->data['MstRole']['role_name'],
                    'role_name_s'      => $this->request->data['MstRole']['role_name_s'],
                    'mst_role_type_id' => $this->request->data['MstRole']['mst_role_type_id'],
                    'mst_edition_id'   => $this->request->data['MstRole']['mst_edition_id'],
                    'is_deleted'       => false,
                    'creater'          => $this->Session->read('Auth.MstUser.id'),
                    'modifier'         => $this->Session->read('Auth.MstUser.id'),
                    'created'          => $now,
                    'modified'         => $now,
                    )
                );
            if(!$this->MstRole->save($Role)){
                //エラー処理
                $this->MstRole->rollback();
            }
            
            $this->request->data['MstRole']['id'] = $this->MstRole->getLastInsertID();
            
            foreach($this->request->data['MstRole']['Authority'] as $view_id => $val){
                $Privilege[] = array(
                    'MstPrivilege' => array(
                        'mst_role_id' => $this->request->data['MstRole']['id'],
                        'view_no'     => $view_id,
                        'updatable'   => (($val == 3)?true:false),
                        'creatable'   => (($val >= 2)?true:false),
                        'readable'    => (($val >= 1)?true:false),
                        'deletable'   => false,
                        'is_deleted'  => false,
                        'creater'     => $this->Session->read('Auth.MstUser.id'),
                        'modifier'    => $this->Session->read('Auth.MstUser.id'),
                        'created'     => $now,
                        'modified'    => $now,
                        
                        )
                    );
            }

            $this->MstPrivilege->create();
            if(!$this->MstPrivilege->saveAll($Privilege)){
                //エラー処理
                $this->MstRole->rollback();
            }
        }
        
        //コミット
        $this->MstRole->commit();

        //結果表示用データ取得
        $sql  =' select ';
        $sql .='       c.name             as "MstRole__Headername" ';
        $sql .='     , b.name             as "MstRole__name"';
        $sql .='     , b.id               as "MstRole__view_id"';
        $sql .='     , d.is_deleted       as "MstRole__is_deleted"';
        $sql .='     , d.role_name        as "MstRole__role_name"';
        $sql .='     , d.role_name_s      as "MstRole__role_name_s"';
        $sql .='     , d.mst_role_type_id as "MstRole__mst_role_type_id"';
        $sql .='     , d.mst_edition_id   as "MstRole__edition_id"';
        $sql .='     , (  ';
        $sql .='       case  ';
        $sql .='         when a.updatable = true then 3 ';
        $sql .='         when a.creatable = true then 2 ';
        $sql .='         when a.readable  = true then 1 ';
        $sql .='         else 0 ';
        $sql .='          end ';
        $sql .='     ) as "MstRole__Authority"';
        $sql .='   from ';
        $sql .='     mst_privileges as a ';
        $sql .='     left join mst_views as b ';
        $sql .='       on b.view_no = a.view_no ';
        $sql .='     inner join mst_view_headers as c ';
        $sql .='       on b.view_header_id = c.id ';
        $sql .='     left join mst_view_groups as c1';
        $sql .='       on c1.id = c.mst_view_group_id ';
        $sql .='     left join mst_roles as d ';
        $sql .='       on d.id = a.mst_role_id ';
        $sql .='   where ';
        $sql .='     a.mst_role_id = ' . $this->request->data['MstRole']['id'];
        $sql .='   order by ';
        $sql .='     c1.order_num , c.order_num , b.order_num ';
        
        $this->set('result' , $this->MstRole->query($sql));
        $this->render('result');
    }
    
}
