<?php
/**
 * MstDealersController
 * @version 1.0.0
 * @since 2010/07/13
 */

class MstDealersController extends AppController {
    var $name = 'MstDealers';

    /**
     *
     * @var array $uses
     */
    var $uses = array('MstDealer');

    /**
     * @var array $components
     */
    var $components = array('CsvWriteUtils');

    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;

    /**
     * @var MstFacilities
     */
    var $MstRoles;

    function beforeFilter() {
        parent::beforeFilter();
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    /**
     * dealers_list
     *
     * 販売元一覧
     */
    function dealers_list() {
        $this->setRoleFunction(93); //販売元マスタ
        $Dealers_List = array();
        App::import('Sanitize');

        //検索ボタン押下
        if(isset($this->request->data['MstDealer']['is_search'])){
            $limit = $this->_getLimitCount();

            $sql  = 'select ';
            $sql .= '      id          as "MstDealer__id" ';
            $sql .= '    , dealer_code as "MstDealer__dealer_code" ';
            $sql .= '    , dealer_name as "MstDealer__dealer_name" ';
            $sql .= '    , dealer_type as "MstDealer__dealer_type" ';
            $sql .= '    , is_deleted  as "MstDealer__is_deleted"  ';
            $sql .= '  from ';
            $sql .= '    mst_dealers as MstDealer  ';
            $sql .= '  where 1=1';

            $where = '';
            //販売元コード(部分一致)
            if((isset($this->request->data['MstDealer']['search_dealer_code'])) && ($this->request->data['MstDealer']['search_dealer_code'] != "")){
                $where .= " and MstDealer.dealer_code LIKE '%" .Sanitize::escape($this->request->data['MstDealer']['search_dealer_code'])."%'";
            }
            //販売元名(LIKE検索)
            if((isset($this->request->data['MstDealer']['search_dealer_name'])) && ($this->request->data['MstDealer']['search_dealer_name'] != "")){
                $where .= " and MstDealer.dealer_name LIKE '%".Sanitize::escape($this->request->data['MstDealer']['search_dealer_name'])."%'";
            }
            //販売元区分(完全一致)
            if((isset($this->request->data['MstDealer']['search_dealer_type'])) && ($this->request->data['MstDealer']['search_dealer_type'] != "")){
                $where .= ' and MstDealer.dealer_type = ' . Sanitize::escape($this->request->data['MstDealer']['search_dealer_type']);
            }
            //削除済み表示
            if( !isset($this->request->data['search_is_deleted'])){
                $where .= ' and MstDealer.is_deleted = FALSE';
                $where .= ' and MstDealer.start_date <= now() ';
                $where .= ' and (MstDealer.end_date > now() or MstDealer.end_date is null) ';

            }
            $sql .= $where;
            $sql .= ' order by MstDealer.dealer_type,MstDealer.dealer_name,MstDealer.dealer_code';

            $this->set('max' , $this->getMaxCount($sql , 'MstDealer'));
            $sql .= ' limit ' . $limit;

            $Dealers_List = $this->MstDealer->query($sql);
        }
        $this->set('Dealers_List',$Dealers_List);
    }



    /**
     * 新規登録
     */
    function add() {
        $this->setRoleFunction(93); //販売元マスタ
    }

    /**
     * 編集
     */
    function mod() {
        $this->setRoleFunction(93); //販売元マスタ
        //更新時間チェック用にアクセス時間を保持
        $this->Session->write('Dealer.readTime',date('Y-m-d H:i:s'));

        $params = array (
            'conditions' => array('MstDealer.id' => $this->request->data['MstDealer']['id'],),
            'fields'     => array('MstDealer.id',
                                  'MstDealer.dealer_code',
                                  'MstDealer.dealer_name',
                                  'to_char(start_date ,\'YYYY/mm/dd\') as "MstDealer__start_date"',
                                  'to_char(end_date ,\'YYYY/mm/dd\') as "MstDealer__end_date"',
                                  'MstDealer.is_deleted',
                                  ),
            'order'      => array('MstDealer.id'),
            'recursive'  => -1
            );

        $this->request->data = $this->MstDealer->find('first', $params);
    }

    /**
     * 完了
     */
    function result() {
        $this->setRoleFunction(93); //販売元マスタ
        $dealer_data = array();
        $now = date('Y/m/d H:i:s.u');

        //トランザクション開始
        $this->MstDealer->begin();
        //行ロック（更新時のみ）
        if(isset($this->request->data['MstDealer']['id'])){
            $this->MstDealer->query('select * from mst_dealers as a where a.id = ' .$this->request->data['MstDealer']['id']. ' for update ');
        }

        //保存データの整形
        if(isset($this->request->data['MstDealer']['id'])){
            //更新の場合
            $dealer_data['MstDealer']['id']            = $this->request->data['MstDealer']['id'];
        }else{
            //新規の場合
            $dealer_data['MstDealer']['creater']   = $this->Session->read('Auth.MstUser.id');
            $dealer_data['MstDealer']['created']   = $now;
        }

        $dealer_data['MstDealer']['dealer_type']   = Configure::read('DealerTypes.cooperation');
        $dealer_data['MstDealer']['dealer_code']   = $this->request->data['MstDealer']['dealer_code'];
        $dealer_data['MstDealer']['dealer_name']   = $this->request->data['MstDealer']['dealer_name'];
        $dealer_data['MstDealer']['start_date']    = $this->request->data['MstDealer']['start_date'];
        $dealer_data['MstDealer']['end_date']      = $this->request->data['MstDealer']['end_date'];
        $dealer_data['MstDealer']['is_deleted']    = (isset($this->request->data['MstDealer']['is_deleted'])?true:false);
        $dealer_data['MstDealer']['modifier']      = $this->Session->read('Auth.MstUser.id');
        $dealer_data['MstDealer']['modified']      = $now;

        //SQL実行
        if(!$this->MstDealer->save($dealer_data)){
            //ロールバック
            $this->MstDealer->rollback();
            //エラーメッセージ
            $this->Session->setFlash('販売元情報の登録に失敗しました。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('dealers_list');
        }
        $this->MstDealer->commit();
    }

    public function export_csv(){
        App::import('Sanitize');
        $sql  = 'select ';
        $sql .= '      dealer_code as "販売元コード" ';
        $sql .= '    , dealer_name as "販売元名" ';
        $sql .= '    , ( case dealer_type ';
        foreach(Configure::read('DealerType') as $k => $v ){
            $sql .= '     when ' . $k . " then '$v' ";
        }
        $sql .= '    end ) as "登録区分" ';
        $sql .= "    , to_char(start_date , 'YYYY/mm/dd') as 有効期間開始日";
        $sql .= "    , to_char(end_date , 'YYYY/mm/dd')   as 有効期間終了日";
        $sql .= "    , ( case when is_deleted = true then '○' else '' end )  as 削除  ";
        $sql .= '  from ';
        $sql .= '    mst_dealers as MstDealer  ';
        $sql .= '  where 1=1';

        $where = '';
        //販売元コード(部分一致)
        if((isset($this->request->data['MstDealer']['search_dealer_code'])) && ($this->request->data['MstDealer']['search_dealer_code'] != "")){
            $where .= " and MstDealer.dealer_code LIKE '%" .Sanitize::escape($this->request->data['MstDealer']['search_dealer_code'])."%'";
        }
        //販売元名(LIKE検索)
        if((isset($this->request->data['MstDealer']['search_dealer_name'])) && ($this->request->data['MstDealer']['search_dealer_name'] != "")){
            $where .= " and MstDealer.dealer_name LIKE '%".Sanitize::escape($this->request->data['MstDealer']['search_dealer_name'])."%'";
        }
        //販売元区分(完全一致)
        if((isset($this->request->data['MstDealer']['search_dealer_type'])) && ($this->request->data['MstDealer']['search_dealer_type'] != "")){
            $where .= ' and MstDealer.dealer_type = ' . Sanitize::escape($this->request->data['MstDealer']['search_dealer_type']);
        }
        //削除済み表示
        if( !isset($this->request->data['search_is_deleted'])){
            $where .= ' and MstDealer.is_deleted = FALSE';
            $where .= ' and MstDealer.start_date <= now() ';
            $where .= ' and (MstDealer.end_date > now() or MstDealer.end_date is null) ';
        }
        $sql .= $where;
        $sql .= ' order by MstDealer.dealer_type,MstDealer.dealer_name,MstDealer.dealer_code';

        $this->db_export_csv($sql , "販売元一覧", 'dealers_list');
    }
}
