<?php
/**
 * EdiController
 * 業者機能
 * @since 2013/11/01
 * @author 
 */

App::uses('OrdersController', 'Controller');
class EdiController extends AppController {
    var $name = 'Edi';

    var $uses = array(
        'Vesta'
        ,'MstUser'
        ,'MstAnalyze'
        ,'MstAnalyzeRole'
        ,'MstAnalyzeOrder'
        ,'MstAptageItem'
        ,'MstClass'
        ,'MstFacility'
        ,'MstFacilityItem'
        ,'MstFacilityOption'
        ,'MstDepartment'
        ,'MstGrandmaster'
        ,'MstItemCategory'
        ,'MstUnitName'
        ,'TrnSticker'
        ,'TrnStickerRecord'
        ,'TrnOrder'
        ,'TrnOrderHeader'
        ,'TrnEdiReceiving'
        ,'TrnEdiReceivingHeader'
        ,'TrnEdiShipping'
        ,'TrnEdiShippingHeader'
        ,'TrnReceivingHeader'
        ,'TrnReceiving'
        ,'TrnStorageHeader'
        ,'TrnStorage'
        ,'TrnShippingHeader'
        ,'TrnShipping'
        );

    var $Auth;
    var $Session;
    var $MstRoles;

    var $components = array('RequestHandler','CsvWriteUtils','Barcode','Stickers', 'AptageDataChecker', 'Facilityitem');

    /**
     * 変数：アプテージ取込データ
     */
    var $aptage_items = array();
    
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        // 汎用モデルに分割有無を登録
        $this->Vesta->setSplit($this->Session->read('Auth.MstUser.user_type'));
    }

    public function index(){
        $this->redirect('/');
    }
    
    public function receiving(){
        App::import('Sanitize');
        //権限ロール
        $this->setRoleFunction(105); //受注登録

        $SearchResult = array();

        $this->set('order_type_List', Configure::read('OrderTypes.orderTypeName'));
        $this->set('Facility_List' , $this->__getFacilityList($this->Session->read('Auth.facility_id_selected')));
        
        $where = "";
        
        if( isset($this->request->data['Order']['is_search'])) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            
            $sql  = ' select ';
            $sql .= '     b.id             as "Order__id"';
            $sql .= '   , ( case when f.facility_type = ' . Configure::read('FacilityType.center'). ' then f.facility_name ';
            $sql .= "       else f.facility_name || ' / '  || e.department_name ";
            $sql .= '      end )           as "Order__facility_name"';
            $sql .= '   , b.work_no        as "Order__work_no"';
            $sql .= '      , ( case b.order_type ';
            foreach (Configure::read('OrderTypes.orderTypeName') as $k=>$v ){
                $sql .= "           when " . $k . " then '" . $v ."' ";
            }
            $sql .= "        else '' ";
            $sql .= '        end )         as "Order__order_type_name" ';
            $sql .= "   , to_char(b.work_date,'yyyy/mm/dd') ";
            $sql .= '                      as "Order__work_date"';
            $sql .= '   , g.user_name      as "Order__user_name"';
            $sql .= "   , count(a.id) || '/' || b.detail_count ";
            $sql .= '                      as "Order__count"';
            $sql .= " from ";
            $sql .= "   trn_orders as a  ";
            $sql .= "   inner join trn_order_headers as b "; //発注確定状態のみ対象とする
            $sql .= "     on b.id = a.trn_order_header_id ";
            $sql .= "   left join mst_item_units as c ";
            $sql .= "     on c.id = a.mst_item_unit_id ";
            $sql .= "   left join mst_facility_items as d ";
            $sql .= "     on d.id = c.mst_facility_item_id ";
            $sql .= "   left join mst_departments as e ";
            $sql .= "     on e.id = b.department_id_from ";
            $sql .= "   left join mst_facilities as f ";
            $sql .= "     on f.id = e.mst_facility_id ";
            $sql .= "   left join mst_users as g ";
            $sql .= "     on g.id = b.modifier ";
            $sql .= "   left join trn_edi_receivings as h ";
            $sql .= "     on h.trn_order_id = a.id ";
            $sql .= "    and h.is_deleted = false ";
            $sql .= " where ";
            $sql .= "   a.is_deleted = false  ";    //発注取消済みは除く
            $sql .= "   and a.remain_count > 0  ";  //発注残数がないものは除く
            $sql .= "   and h.id is null ";         // すでに受注しているものは除く
            $sql .= "   and a.department_id_to = " . $this->Auth->user('mst_department_id'); //業者で絞込み
            $sql .= $where;

            // 施設
            if( $this->request->data['Order']['facility_id'] != "" ) {
                $sql .= " and f.id = " . $this->request->data['Order']['facility_id'];
            }
            // 発注番号
            if( $this->request->data['Order']['work_no'] != "" ) {
                $sql .= " and a.work_no = '" . $this->request->data['Order']['work_no'] . "'";
            }
            // 発注日 From
            if( $this->request->data['Order']['order_date_from'] != "" ){
                $sql .= " and b.work_date >= '" . $this->request->data['Order']['order_date_from'] . "'";
            }
            // 発注日 To
            if( $this->request->data['Order']['order_date_to'] != "" ){
                $sql .= " and b.work_date <= '" . $this->request->data['Order']['order_date_to'] . "'";
            }
            // 発注種別
            if( $this->request->data['Order']['order_type'] != "" ){
                $sql .= " and b.order_type = '" . $this->request->data['Order']['order_type'] . "'";
            }

            $sql .= " group by ";
            $sql .= "   b.id ";
            $sql .= "   , b.work_no ";
            $sql .= "   , b.detail_count ";
            $sql .= "   , b.order_type ";
            $sql .= "   , f.facility_name";
            $sql .= "   , f.facility_code";
            $sql .= '   , f.facility_type';
            $sql .= '   , e.department_name ';
            $sql .= "   , b.work_date ";
            $sql .= "   , g.user_name";
            $sql .= " order by ";
            $sql .= "   f.facility_code ";
            $sql .= "   , b.work_no ";
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnOrder' , false ));
            if(isset($this->request->data['Order']['limit'])){
                $sql .= ' limit ' . $this->request->data['Order']['limit'];
            }else{
                $sql .= ' limit ' . min(array_keys(Configure::read('displaycounts_combobox')));
            }
            $SearchResult = $this->TrnOrder->query($sql,false,false);
            $this->request->data = $data;
        }else{
            // 発注日 From
            $this->request->data['Order']['order_date_from'] = date('Y/m/d',mktime(0, 0, 0,date("m"), date("d")-7,date("Y"))); ;
            // 発注日 To
            $this->request->data['Order']['order_date_to'] = date('Y/m/d');;

        }

        $this->set('SearchResult' , $SearchResult);
    }

    public function receiving_confirm(){

        $data = $this->request->data;
        $this->request->data = Sanitize::clean($this->request->data);
        // 絞り込み条件追加
        $where = "";
        
        // センター
        if( $this->request->data['Order']['facility_id'] != "" ) {
            $where .= " and f.id = " . $this->request->data['Order']['facility_id'];
        }
        // 発注番号
        if( $this->request->data['Order']['work_no'] != "" ) {
            $where .= " and a.work_no = '" . $this->request->data['Order']['work_no'] . "'";
        }
        // 発注日 From
        if( $this->request->data['Order']['order_date_from'] != "" ){
            $where .= " and b.work_date >= '" . $this->request->data['Order']['order_date_from'] . "'";
        }
        // 発注日 To
        if( $this->request->data['Order']['order_date_to'] != "" ){
            $where .= " and b.work_date <= '" . $this->request->data['Order']['order_date_to'] . "'";
        }
        // 発注種別
        if( $this->request->data['Order']['order_type'] != "" ){
            $where .= " and a.order_type = '" . $this->request->data['Order']['order_type'] . "'";
        }
        
        // 選択ID
        $where .= " and b.id in ( " . join(',' ,$this->request->data['Order']['id']) . " ) " ;
        $where .= ' and i.id is null ';
        
        $SearchResult = $this->order_search($where);
        $this->request->data['Order']['edi_date'] = date('Y/m/d');

        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Edi.token' , $token);
        $this->request->data['Edi']['token'] = $token;
        
        $this->set("SearchResult" , $SearchResult);
    }
    
    public function receiving_result(){
        if($this->request->data['Edi']['token'] === $this->Session->read('Edi.token')) {
            $this->Session->delete('Edi.token');
            // 登録処理
            $now = date('Y/m/d H:i:s');
            $this->TrnOrder->begin();

            $ids = join(',',$this->request->data['Order']['id']);
            //行ロック
            $this->TrnOrder->query("select * from trn_orders as a where a.id in ( " . $ids . ") for update  " , false , false);

            $headerArray = $this->registEdiHeader($ids);
            foreach ( $headerArray as &$h ) {
                $h['OrderHeader']['work_no'] = $this->setWorkNo4Header( date('Ymd') , "28" , $h['OrderHeader']['center_id']);

                // 受注ヘッダ
                $TrnEdiReceivingHeader = array(
                    'TrnEdiReceivingHeader'=> array(
                        'work_no'                => $h['OrderHeader']['work_no'],
                        'work_date'              => $this->request->data['Order']['edi_date'],
                        'recital'                => $this->request->data['Order']['recital'],
                        'department_id_from'     => $h['OrderHeader']['department_id_from'],      // センタ部署ID
                        'department_id_to'       => $h['OrderHeader']['department_id_to'],        // 業者部署ID
                        'department_id_to2'      => $h['OrderHeader']['department_id_to2'],       // 業者部署ID2
                        'detail_count'           => $h['OrderHeader']['count'],
                        'is_deleted'             => false,
                        'creater'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                => $now,
                        'modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'modified'               => $now

                        )
                    );

                $this->TrnEdiReceivingHeader->create();
                // SQL実行
                if (!$this->TrnEdiReceivingHeader->save($TrnEdiReceivingHeader)) {
                    $this->TrnOrder->rollback();
                    $this->Session->setFlash('受注登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
                $h['OrderHeader']['id'] = $this->TrnEdiReceivingHeader->getLastInsertID();
            }
            unset($h);

            foreach ($this->request->data['Order']['id'] as $row => $id) {
                $sql  = "";
                $sql .= " update trn_orders ";
                $sql .= " set modified =  '" . $now . "'";
                $sql .= " ,   modifier = '" . $this->Session->read('Auth.MstUser.id') . "'";
                $sql .= " where ";
                $sql .= " id = " . $id;
                $ret = $this->TrnOrder->query($sql , false , false);
                if($ret === FALSE){
                    // エラー
                    $this->TrnOrder->rollback();
                    $this->Session->setFlash('受注登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }

                // 受注明細
                // TrnOrderの情報を取得
                $sql  = "";
                $sql .= " select ";
                $sql .= "    a.id ";
                $sql .= "  , a.quantity ";
                $sql .= "  , a.mst_item_unit_id ";
                $sql .= "  , a.department_id_to ";
                $sql .= "  , a.department_id_to2 ";
                $sql .= "  , a.department_id_from ";
                $sql .= "  , a.order_type ";
                $sql .= " from ";
                $sql .= "  trn_orders  as a ";
                $sql .= " where ";
                $sql .= "  a.id = " .$id;
                $order = $this->TrnOrder->query($sql, false , false );
                foreach($headerArray as $h){
                    if( $h['OrderHeader']['department_id_from'] == $order[0][0]['department_id_from']
                        && $h['OrderHeader']['department_id_to'] == $order[0][0]['department_id_to']
                        && $h['OrderHeader']['department_id_to2'] == $order[0][0]['department_id_to2']
                        && $h['OrderHeader']['order_type'] == $order[0][0]['order_type']
                        ){

                        // 受注明細
                        $TrnEdiReceiving = array(
                            'TrnEdiReceiving'=> array(
                                'work_no'                     => $h['OrderHeader']['work_no'],
                                'work_seq'                    => $h['OrderHeader']['work_seq'] + 1,
                                'work_date'                   => $this->request->data['Order']['edi_date'],
                                'work_type'                   => null,
                                'recital'                     => $this->request->data['Order']['edi_comment'][$id],
                                'mst_item_unit_id'            => $order[0][0]['mst_item_unit_id'],
                                'department_id_from'          => $h['OrderHeader']['department_id_from'],
                                'department_id_to'            => $h['OrderHeader']['department_id_to'],
                                'department_id_to2'           => $h['OrderHeader']['department_id_to2'],
                                'quantity'                    => $order[0][0]['quantity'],
                                'remain_count'                => $order[0][0]['quantity'],
                                'trn_order_id'                => $id,
                                'trn_edi_receiving_header_id' => $h['OrderHeader']['id'],
                                'is_deleted'                  => false,
                                'creater'                     => $this->Session->read('Auth.MstUser.id'),
                                'created'                     => $now,
                                'modifier'                    => $this->Session->read('Auth.MstUser.id'),
                                'modified'                    => $now
                                )
                            );
                        $this->TrnEdiReceiving->create();
                        // SQL実行
                        if (!$this->TrnEdiReceiving->save($TrnEdiReceiving)) {
                            $this->TrnOrder->rollback();
                            $this->Session->setFlash('受注登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('/');
                        }
                        $h['OrderHeader']['work_seq']++;
                    }
                }
            }
            $this->TrnOrder->commit();

            // 表示用のデータを取得
            $where = " and a.id in ("  .  join(',',$this->request->data['Order']['id']) . ")";
            $SearchResult = $this->order_search($where);
            $this->Session->write('Edi.Receiving.result' , $SearchResult);
            $this->set('SearchResult' , $SearchResult);
            
        }else{
            $this->set('SearchResult' , $this->Session->read('Edi.Receiving.result'));
        }
    }

    public function order_search($where=""){
        $sql  = " select ";
        $sql .= '     a.id                               as "Order__id" ';
        $sql .= '   , ( case ';
        $sql .= '       when f.facility_type = ' . Configure::read('FacilityType.center')  . ' then f.facility_name ';
        $sql .= "       else f.facility_name || ' / ' || e.department_name ";
        $sql .= '      end )                             as "Order__facility_name" ';
        $sql .= '   , b.work_no                          as "Order__work_no" ';
        $sql .= '      , ( case b.order_type ';
        foreach (Configure::read('OrderTypes.orderTypeName') as $k=>$v ){
            $sql .= "           when " . $k . " then '" . $v ."' ";
        }
        $sql .= "        else '' ";
        $sql .= '        end )                           as "Order__order_type_name" ';
        $sql .= "   , to_char(b.work_date, 'yyyy/mm/dd') ";
        $sql .= '                                        as "Order__work_date" ';
        $sql .= '   , g.user_name                        as "Order__user_name" ';
        $sql .= '   , d.internal_code                    as "Order__internal_code" ';
        $sql .= '   , d.item_name                        as "Order__item_name" ';
        $sql .= '   , d.item_code                        as "Order__item_code" ';
        $sql .= '   , d.standard                         as "Order__standard" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when c.per_unit = 1  ';
        $sql .= '         then c1.unit_name  ';
        $sql .= "         else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                  as "Order__unit_name"  ';
        $sql .= '   , h.dealer_name                      as "Order__dealer_name" ';
        $sql .= '   , a.quantity                         as "Order__quantity" ';
        $sql .= '   , a.remain_count                     as "Order__remain_count" ';
        $sql .= '   , a.stocking_price                   as "Order__stocing_price"  ';
        $sql .= ' from ';
        $sql .= '   trn_orders as a ';
        $sql .= '   inner join trn_order_headers as b ';
        $sql .= '     on b.id = a.trn_order_header_id ';
        $sql .= '   left join mst_item_units as c  ';
        $sql .= '     on c.id = a.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as c1  ';
        $sql .= '     on c1.id = c.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as c2  ';
        $sql .= '     on c2.id = c.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as d  ';
        $sql .= '     on d.id = c.mst_facility_item_id  ';
        $sql .= '   left join mst_departments as e  ';
        $sql .= '     on e.id = b.department_id_from  ';
        $sql .= '   left join mst_facilities as f  ';
        $sql .= '     on f.id = e.mst_facility_id  ';
        $sql .= '   left join mst_users as g  ';
        $sql .= '     on g.id = a.modifier  ';
        $sql .= '   left join mst_dealers as h  ';
        $sql .= '     on h.id = d.mst_dealer_id  ';
        $sql .= '   left join trn_edi_receivings as i ';
        $sql .= '     on i.trn_order_id = a.id ';
        $sql .= '    and i.is_deleted = false ';
        $sql .= ' where ';
        $sql .= '   a.is_deleted = false  ';
        $sql .= '   and a.remain_count > 0  ';
        $sql .= '   and a.department_id_to = ' . $this->Auth->user('mst_department_id');
        $sql .=$where;
        $sql .= ' order by ';
        $sql .= '   f.facility_code ';
        $sql .= '   , b.work_no ';
        $sql .= '   , a.work_seq ';
        
        $SearchResult = $this->TrnOrder->query($sql,false,false);
        return $SearchResult;
    }

    public function registEdiHeader ($ids) {
        $sql  = " select ";
        $sql .= '     count(*)             as "OrderHeader__count" ';
        $sql .= '   , a.department_id_from as "OrderHeader__department_id_from"';
        $sql .= '   , a.department_id_to   as "OrderHeader__department_id_to"';
        $sql .= '   , a.department_id_to2  as "OrderHeader__department_id_to2"';
        $sql .= '   , a.order_type         as "OrderHeader__order_type"';
        $sql .= '   , 0                    as "OrderHeader__work_seq"';
        $sql .= '   , 0                    as "OrderHeader__work_no"';
        $sql .= '   , b.mst_facility_id    as "OrderHeader__center_id"';
        $sql .= ' from ';
        $sql .= '   trn_orders as a ';
        $sql .= '   left join mst_item_units as b ';
        $sql .= '     on b.id = a.mst_item_unit_id ';
        $sql .= ' where ';
        $sql .= '   a.is_deleted = false ';
        $sql .= '   and a.trn_order_header_id is not null ';
        $sql .= '   and a.id in (' . $ids . ')';
        $sql .= ' group by ';
        $sql .= '   a.department_id_from ';
        $sql .= '   , a.department_id_to ';
        $sql .= '   , a.department_id_to2 ';
        $sql .= '   , a.order_type ';
        $sql .= '   , b.mst_facility_id ';

        $result = $this->TrnOrder->query($sql,  false , false );
        return $result;
    }

    public function receiving_history(){
        App::import('Sanitize');
        //権限ロール
        $this->setRoleFunction(106); //受注履歴
        $result = array();
        $department_list = array();
        
        if(isset($this->request->data['Edi']['is_search'])){
            $sql  = " select ";
            $sql .= '     a.id                               as "Edi__id" ';
            $sql .= '   , a.work_no                          as "Edi__work_no" ';
            $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') ";
            $sql .= '                                        as "Edi__work_date" ';
            $sql .= '   , d.facility_name                    as "Edi__facility_name" ';
            $sql .= '   , c.department_name                  as "Edi__department_name" ';
            $sql .= '   , e.user_name                        as "Edi__user_name" ';
            $sql .= "   , to_char(a.created, 'YYYY/mm/dd HH24:MI:SS') ";
            $sql .= '                                        as "Edi__created" ';
            $sql .= '   , a.recital                          as "Edi__recital" ';
            $sql .= '   , count(a.id)                        as "Edi__count" ';
            $sql .= '   , a.detail_count                     as "Edi__detail_count"  ';
            $sql .= " from ";
            $sql .= "   trn_edi_receiving_headers as a  ";
            $sql .= "   left join trn_edi_receivings as b  ";
            $sql .= "     on b.trn_edi_receiving_header_id = a.id  ";
            $sql .= "   left join mst_departments as c  ";
            $sql .= "     on c.id = a.department_id_from  ";
            $sql .= "   left join mst_facilities as d  ";
            $sql .= "     on d.id = c.mst_facility_id  ";
            $sql .= "   left join mst_users as e  ";
            $sql .= "     on e.id = a.creater  ";
            $sql .= "   left join mst_item_units as f  ";
            $sql .= "     on f.id = b.mst_item_unit_id  ";
            $sql .= "   left join mst_facility_items as g  ";
            $sql .= "     on g.id = f.mst_facility_item_id  ";
            $sql .= "   left join mst_dealers as h  ";
            $sql .= "     on h.id = g.mst_dealer_id  ";
            $sql .= "   left join mst_departments as i ";
            $sql .= "     on i.id = a.department_id_to ";
            $sql .= "   inner join trn_orders as j ";
            $sql .= "     on j.id = b.trn_order_id ";
            $sql .= "    and j.is_deleted = false ";
            $sql .= " where 1=1 ";
            $sql .= "   and i.id = " . $this->Auth->user('mst_department_id');
            /* 絞り込み条件 */
            // 受注番号
            if( $this->request->data['Edi']['search_receiving_no'] != "" ){
                $sql .= "   and a.work_no = '" . $this->request->data['Edi']['search_receiving_no'] . "'";
            }
            
            // 受注日To
            if( $this->request->data['Edi']['search_start_date'] != "" ){
                $sql .= "   and a.work_date >= '" . $this->request->data['Edi']['search_start_date'] . "'";
            }
            // 受注日From
            if( $this->request->data['Edi']['search_end_date'] != "" ){
                $sql .= "   and a.work_date <= '" . $this->request->data['Edi']['search_end_date'] . "'";
            }
            
            // 取消も表示する
            if( $this->request->data['Edi']['search_display_deleted'] != "1" ){
                $sql .= "   and a.is_deleted = false  ";
                $sql .= "   and b.is_deleted = false  ";
            }
            
            // 受注施設
            if( $this->request->data['Edi']['mst_facility_code'] != "" ){
                $sql .= "   and d.facility_code = '" . $this->request->data['Edi']['mst_facility_code'] . "'";
                $department_list = $this->getDepartmentsList($this->request->data['Edi']['mst_facility_code']);
            }
            // 受注部署
            if( $this->request->data['Edi']['mst_department_code'] != "" ){
                $sql .= "   and c.department_code = '" . $this->request->data['Edi']['mst_department_code'] . "'";
            }
            // 商品ID 
            if( $this->request->data['Edi']['search_internal_code'] != "" ){
                $sql .= "   and g.internal_code = '" . $this->request->data['Edi']['search_internal_code'] . "'";
            }            
            // 製品番号
            if( $this->request->data['Edi']['search_item_code'] != "" ){
                $sql .= "   and g.item_code like '%" . $this->request->data['Edi']['search_item_code'] . "%'";
            }
            // 商品名
            if( $this->request->data['Edi']['search_item_name'] != "" ){
                $sql .= "   and g.item_name like '%" . $this->request->data['Edi']['search_item_name'] . "%'";
            }
            // 販売元
            if( $this->request->data['Edi']['search_dealer_name'] != "" ){
                $sql .= "   and h.dealer_name like '%" . $this->request->data['Edi']['search_dealer_name'] . "%'";
            }
            // 規格
            if( $this->request->data['Edi']['search_standard'] != "" ){
                $sql .= "   and g.standard like '%" . $this->request->data['Edi']['search_standard'] . "%'";
            }
            // 作業区分
            if( $this->request->data['Edi']['search_classId'] != "" ){
                $sql .= "   and a.work_type = '" . $this->request->data['Edi']['search_classId'] . "'";
            }
            
            $sql .= " group by ";
            $sql .= "   a.id ";
            $sql .= "   , a.work_no ";
            $sql .= "   , a.work_date ";
            $sql .= "   , d.facility_name ";
            $sql .= '   , c.department_name ';
            $sql .= "   , e.user_name ";
            $sql .= "   , a.created ";
            $sql .= "   , a.recital ";
            $sql .= "   , a.detail_count  ";
            $sql .= " order by ";
            $sql .= "   a.id ";
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnEdiReceiving' , false));
            
            $sql .= ' limit '.  $this->request->data['limit'];
            $result = $this->TrnEdiReceiving->query($sql , false , false );
        } else {
            $this->request->data['Edi']['search_start_date'] = date('Y/m/d', strtotime("-6 day"));
            $this->request->data['Edi']['search_end_date']   = date('Y/m/d');
        }
        
        $this->set('result' , $result);
        $this->set('class_list' , array());
        $this->set('facility_list' , $this->__getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                              array('facility_code' , 'facility_name')));
        $this->set('department_list' , $department_list);
    }

    public function receiving_history_confirm(){
        $result = array();
        $where = " and a.trn_edi_receiving_header_id  in ( " . join(",",$this->request->data['Edi']['id']) . ")" ;
        /* 前画面での検索条件引き継ぎ */
        // 受注番号
        if( $this->request->data['Edi']['search_receiving_no'] != "" ){
            $where .= "   and a.work_no = '" . $this->request->data['Edi']['search_receiving_no'] . "'";
        }
        
        // 受注日To
        if( $this->request->data['Edi']['search_start_date'] != "" ){
            $where .= "   and a.work_date >= '" . $this->request->data['Edi']['search_start_date'] . "'";
        }
        // 受注日From
        if( $this->request->data['Edi']['search_end_date'] != "" ){
            $where .= "   and a.work_date <= '" . $this->request->data['Edi']['search_end_date'] . "'";
        }
        
        // 取消も表示する
        if( $this->request->data['Edi']['search_display_deleted'] != "1" ){
            $where .= "   and a.is_deleted = false  ";
        }
        
        // 受注施設
        if( $this->request->data['Edi']['mst_facility_code'] != "" ){
            $where .= "   and f.facility_code = '" . $this->request->data['Edi']['mst_facility_code'] . "'";
        }
        // 受注部署
        if( $this->request->data['Edi']['mst_department_code'] != "" ){
            $where .= "   and e.department_code = '" . $this->request->data['Edi']['mst_department_code'] . "'";
        }
        // 商品ID 
        if( $this->request->data['Edi']['search_internal_code'] != "" ){
            $where .= "   and c.internal_code = '" . $this->request->data['Edi']['search_internal_code'] . "'";
        }
        // 製品番号
        if( $this->request->data['Edi']['search_item_code'] != "" ){
            $where .= "   and c.item_code like '%" . $this->request->data['Edi']['search_item_code'] . "%'";
        }
        // 商品名
        if( $this->request->data['Edi']['search_item_name'] != "" ){
            $where .= "   and c.item_name like '%" . $this->request->data['Edi']['search_item_name'] . "%'";
        }
        // 販売元
        if( $this->request->data['Edi']['search_dealer_name'] != "" ){
            $where .= "   and g.dealer_name like '%" . $this->request->data['Edi']['search_dealer_name'] . "%'";
        }
            // 規格
        if( $this->request->data['Edi']['search_standard'] != "" ){
            $where .= "   and c.standard like '%" . $this->request->data['Edi']['search_standard'] . "%'";
        }
        // 作業区分
        if( $this->request->data['Edi']['search_classId'] != "" ){
            $where .= "   and a.work_type = '" . $this->request->data['Edi']['search_classId'] . "'";
        }
        
        $result = $this->receiving_history_search($where);
        $this->set('result' , $result);
        
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Edi.token' , $token);
        $this->request->data['Edi']['token'] = $token;
    }

    public function receiving_history_result(){
        if($this->request->data['Edi']['token'] === $this->Session->read('Edi.token')) {
            $this->Session->delete('Edi.token');
        
            // 登録処理
            $now = date('Y/m/d H:i:s');
            $this->TrnEdiReceiving->begin();
        
            $ids = join(',',$this->request->data['Edi']['id']);
            //行ロック
            $this->TrnEdiReceiving->query("select * from trn_edi_receivings as a where a.id in ( " . $ids . ") for update  " );
            
            // 受注ヘッダ
            $this->TrnEdiReceiving->create();
            $ret = $this->TrnEdiReceiving->updateAll(
                array(
                    'TrnEdiReceiving.is_deleted' => "'true'",
                    'TrnEdiReceiving.modifier'   => $this->Session->read('Auth.MstUser.id'),
                    'TrnEdiReceiving.modified'   => "'" . $now . "'"
                    ),
                array(
                    'TrnEdiReceiving.id' => $this->request->data['Edi']['id'],
                    ),
                -1
                );
            if(!$ret){
                $this->TrnEdiReceiving->rollback();
                $this->Session->setFlash('受注取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
            
            $this->TrnEdiReceiving->commit();
            $where = " and a.id in ( " . $ids . ")";
            $result = $this->receiving_history_search($where);
            $this->Session->write('Edi.result',$result);
            $this->set('result' , $result);
        }else{
            $this->set('result' ,  $this->Session->read('Edi.result'));
        }
    }


    public function receiving_history_search($where){
        $sql  = " select ";
        $sql .= '     a.id               as "Edi__id" ';
        $sql .= '   , a.work_no          as "Edi__work_no"';
        $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') ";
        $sql .= '                        as "Edi__work_date"';
        $sql .= '   , c.internal_code    as "Edi__internal_code"';
        $sql .= '   , c.item_name        as "Edi__item_name"';
        $sql .= '   , c.item_code        as "Edi__item_code" ';
        $sql .= "   , ( case  ";
        $sql .= "     when b.per_unit = 1  ";
        $sql .= "     then b1.unit_name  ";
        $sql .= "     else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '     end )              as "Edi__unit_name"';
        $sql .= '   , d.work_no          as "Edi__order_work_no"';
        $sql .= '   , a.work_type        as "Edi__work_type"';
        $sql .= '   , a.recital          as "Edi__recital"';
        $sql .= '   , a.quantity         as "Edi__quantity"';
        $sql .= '   , f.facility_name    as "Edi__facility_name"';
        $sql .= '   , e.department_name  as "Edi__department_name"';
        $sql .= '   , c.standard         as "Edi__standard"';
        $sql .= '   , g.dealer_name      as "Edi__dealer_name" ';
        $sql .= '   , h.user_name        as "Edi__user_name"';
        $sql .= "   , (  ";
        $sql .= "     case  ";
        $sql .= "       when a.is_deleted = true  ";
        $sql .= "       then false  ";
        $sql .= "       when x.trn_edi_receiving_id is not null  ";
        $sql .= "       then false  ";
        $sql .= "       else true  ";
        $sql .= "       end ";
        $sql .= '   )                                    as "Edi__canCancel" ';
        
        $sql .= " from ";
        $sql .= "   trn_edi_receivings as a  ";
        $sql .= "   left join mst_item_units as b  ";
        $sql .= "     on b.id = a.mst_item_unit_id  ";
        $sql .= "   left join mst_unit_names as b1  ";
        $sql .= "     on b1.id = b.mst_unit_name_id  ";
        $sql .= "   left join mst_unit_names as b2  ";
        $sql .= "     on b2.id = b.per_unit_name_id  ";
        $sql .= "   left join mst_facility_items as c  ";
        $sql .= "     on c.id = b.mst_facility_item_id  ";
        $sql .= "   left join trn_orders as d  ";
        $sql .= "     on d.id = a.trn_order_id  ";
        $sql .= "   left join mst_departments as e  ";
        $sql .= "     on e.id = a.department_id_from  ";
        $sql .= "   left join mst_facilities as f  ";
        $sql .= "     on f.id = e.mst_facility_id  ";
        $sql .= "   left join mst_dealers as g  ";
        $sql .= "     on g.id = c.mst_dealer_id  ";
        $sql .= "   left join mst_users as h  ";
        $sql .= "     on h.id = a.modifier ";
        $sql .= "   left join trn_orders as i";
        $sql .= "     on i.id = a.trn_order_id ";
        $sql .= "   left join (  ";
        $sql .= "     select ";
        $sql .= "         xa.trn_edi_receiving_id  ";
        $sql .= "     from ";
        $sql .= "       trn_edi_shippings as xa  ";
        $sql .= "     where ";
        $sql .= "       xa.is_deleted = false  ";
        $sql .= "     group by ";
        $sql .= "       xa.trn_edi_receiving_id ";
        $sql .= "   ) as x  ";
        $sql .= "     on x.trn_edi_receiving_id = a.id ";
        
        $sql .= " where 1=1 " ;
        $sql .= " and i.is_deleted = false ";
        $sql .= $where;
        $sql .= ' order by ';
        $sql .= '  a.work_no  ';
        $sql .= '  , a.work_seq ';
        $result = $this->TrnEdiReceiving->query($sql , false , false );
        return $result;
    }

    public function shipping_select(){
        App::import('Sanitize');
        //権限ロール
        $this->setRoleFunction(107); //出荷登録

        $SearchResult = array();
        //$this->set('Facility_List' , $this->getFacilityList( null , array(1) , true , array('id' , 'facility_name') , 'facility_code' ));
        $this->set('Facility_List' , $this->__getFacilityList($this->Session->read('Auth.facility_id_selected')));
        
        $where = "";
        
        if( isset($this->request->data['Receiving']['is_search'])) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $sql  = ' select ';
            $sql .= '     a1.id             as "Edi__id"';
            $sql .= '   , a1.work_no        as "Edi__work_no"';
            $sql .= "   , to_char(a1.work_date,'YYYY/mm/dd') ";
            $sql .= '                       as "Edi__work_date"';
            $sql .= '   , e.facility_name   as "Edi__facility_name"';
            $sql .= '   , d.department_name as "Edi__department_name"';
            $sql .= '   , d.id              as "Edi__department_id"';
            $sql .= '   , count(a.id)       as "Edi__count" ';
            $sql .= '   , a1.detail_count   as "Edi__detail_count"';
            $sql .= '   , f.user_name       as "Edi__user_name"';
            $sql .= ' from ';
            $sql .= '   trn_edi_receivings as a  ';
            $sql .= '   left join trn_edi_receiving_headers as a1  ';
            $sql .= '     on a1.id = a.trn_edi_receiving_header_id  ';
            $sql .= '   left join mst_item_units as b  ';
            $sql .= '     on b.id = a.mst_item_unit_id  ';
            $sql .= '   left join mst_facility_items as c  ';
            $sql .= '     on c.id = b.mst_facility_item_id  ';
            $sql .= '   left join mst_departments as d  ';
            $sql .= '     on d.id = a.department_id_from  ';
            $sql .= '   left join mst_facilities as e  ';
            $sql .= '     on e.id = d.mst_facility_id  ';
            $sql .= '   left join mst_users as f  ';
            $sql .= '     on f.id = a1.creater  ';
            $sql .= '   left join trn_orders as g ';
            $sql .= '     on g.id = a.trn_order_id';
            $sql .= ' where ';
            $sql .= '   a.remain_count > 0  ';
            $sql .= '   and a.is_deleted = false ';
            $sql .= '   and g.is_deleted = false ';
            $sql .= '   and g.remain_count > 0 ';
            $sql .= "   and a.department_id_to = " . $this->Auth->user('mst_department_id');
            
            // センター
            if( $this->request->data['Receiving']['facility_id'] != "" ) {
                $sql .= " and e.id = " . $this->request->data['Receiving']['facility_id'];
            }
            // 受注番号
            if( $this->request->data['Receiving']['work_no'] != "" ) {
                $sql .= " and a1.work_no = '" . $this->request->data['Receiving']['work_no'] . "'";
            }
            // 受注日 From
            if( $this->request->data['Receiving']['work_date_from'] != "" ){
                $sql .= " and a1.work_date >= '" . $this->request->data['Receiving']['work_date_from'] . "'";
            }
            // 受注日 To
            if( $this->request->data['Receiving']['work_date_to'] != "" ){
                $sql .= " and a1.work_date <= '" . $this->request->data['Receiving']['work_date_to'] . "'";
            }
            
            $sql .= ' group by ';
            $sql .= '   a1.id ';
            $sql .= '   , a1.work_no ';
            $sql .= '   , a1.work_date ';
            $sql .= '   , e.id ';
            $sql .= '   , e.facility_name ';
            $sql .= '   , d.id ';
            $sql .= '   , d.department_name ';
            $sql .= '   , a1.detail_count ';
            $sql .= '   , f.user_name  ';
            $sql .= ' order by ';
            $sql .= '   a1.work_date ';
            $sql .= '   , a1.id ';

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnEdiReceiving' , false ));
            if(isset($this->request->data['Receiving']['limit'])){
                $sql .= ' limit ' . $this->request->data['Receiving']['limit'];
            }else{
                $sql .= ' limit ' . min(array_keys(Configure::read('displaycounts_combobox')));
            }
            $SearchResult = $this->TrnEdiReceiving->query($sql,false,false);
            $this->request->data = $data;
        }

        $this->set('SearchResult' , $SearchResult);
    }

    public function shipping_read(){
        App::import('Sanitize');
        //権限ロール
        $this->setRoleFunction(107); //出荷登録
        $data = $this->request->data;
        $this->request->data = Sanitize::clean($this->request->data);
        
        $where  = " and a.id in (" . join(',',$this->request->data['Edi']['id']) . ")";
        $where .= " and b.remain_count > 0";
        $result = $this->receiving_search($where);
        
        $this->request->data['EdiShipping']['facility_name'] = $result[0]['Edi']['facility_name'];
        $this->request->data['EdiShipping']['center_id']     = $result[0]['Edi']['center_id'];
    }
    
    public function shipping_read_confirm(){
        // シール読み込み確認画面
        $this->request->data['Edi']['work_date'] = date('Y/m/d');
        
        $data = $this->request->data;
        $this->request->data = Sanitize::clean($this->request->data);
        
        $result = array();
        $ean = array();
        
        $tempArray = array();
        foreach(explode(',',$this->request->data['codez']) as $barcode){
            if(isset($tempArray[$barcode])){
                $tempArray[$barcode]++;
            }else{
                $tempArray[$barcode] = 1;
            }
        }

        //読み込みバーコードから商品情報を展開
        $i = 0;
        foreach($tempArray as $barcode => $count ){
            $ean[$i] = $this->Barcode->readEAN128($barcode ,
                                                $this->request->data['EdiShipping']['center_id']
                                                );
            $ean[$i]['count'] = $count;
            $i++;
        }
        foreach($ean as $e){
            $ret = array();
            if(isset($e['MstFacilityItem'][0]['MstFacilityItem']['id'])){
                $data = $e['MstFacilityItem'][0];
                $ret['MstFacilityItem']['internal_code']     = $data['MstFacilityItem']['internal_code'];
                $ret['MstFacilityItem']['item_name']         = $data['MstFacilityItem']['item_name'];
                $ret['MstFacilityItem']['item_code']         = $data['MstFacilityItem']['item_code'];
                $ret['MstFacilityItem']['standard']          = $data['MstFacilityItem']['standard'];
                $ret['MstFacilityItem']['lot_no']            = $e['lot_no'];
                $ret['MstFacilityItem']['validated_date']    = $this->getFormatedValidateDate($e['validated_date']);
                $ret['MstFacilityItem']['dealer_name']       = $data['MstDealer']['dealer_name'];
                $ret['MstFacilityItem']['check']             = true;
                $ret['MstFacilityItem']['count']             = $e['count'];
            }else{
                $ret['MstFacilityItem']['internal_code']     = '該当商品がありません';
                $ret['MstFacilityItem']['item_name']         = '';
                $ret['MstFacilityItem']['item_code']         = '';
                $ret['MstFacilityItem']['standard']          = '';
                $ret['MstFacilityItem']['lot_no']            = '';
                $ret['MstFacilityItem']['validated_date']    = '';
                $ret['MstFacilityItem']['dealer_name']       = '';
                $ret['MstFacilityItem']['check']             = false;
                $ret['MstFacilityItem']['count']             = '';
            }
            $BarcodeResult[] = $ret;
        }
        //登録済み受注情報取得
        $where  = ' and a.id in ( ' . join(',',$this->request->data['EdiShipping']['header_id']) . ')';
        $where .= ' and b.remain_count > 0 ';
        $SearchResult = $this->receiving_search($where);
        
        $Result = array();
        $dummy = array(
            'Edi' => array(
                'id'              => null,
                'header_id'       => null,
                'work_date'       => null,
                'work_no'         => null,
                'internal_code'   => '該当商品がありません',
                'item_name'       => null,
                'item_code'       => null,
                'standard'        => null,
                'jan_code'        => null,
                'unit_name'       => null,
                'remain_count'    => null,
                'dealer_name'     => null,
                'facility_name'   => null,
                'department_name' => null,
                'center_id'       => null,
                
                )
            );


        //読み込み情報と、受注情報をマージ
        foreach($BarcodeResult as $b){
            $tmp = array();
            foreach($SearchResult as $s){
                if($b['MstFacilityItem']['internal_code'] == $s['Edi']['internal_code']){
                    if($b['MstFacilityItem']['count'] > $s['Edi']['remain_count']){
                        $tmpCount = $b['MstFacilityItem']['count'];
                        $b['MstFacilityItem']['count'] =  $s['Edi']['remain_count'];
                        $tmp = array_merge($b , $s);
                        $Result[] = $tmp;
                        $b['MstFacilityItem']['count'] = $tmpCount - $s['Edi']['remain_count'];
                    }else{
                        $tmp = array_merge($b , $s);
                        $Result[] = $tmp;
                        break;
                    }
                }
            }
            if($tmp == array()){
                $Result[] = array_merge( $b,$dummy);
            }
        }

        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Edi.token' , $token);
        $this->request->data['Edi']['token'] = $token;
        $this->set('Result' , $Result);
    }

    
    public function shipping_confirm(){
        $data = $this->request->data;
        $this->request->data = Sanitize::clean($this->request->data);
        
        $where  = " and a.id in (" . join(',',$this->request->data['Edi']['id']) . ")";
        $where .= " and b.remain_count > 0";
        $SearchResult = $this->receiving_search($where);
        $this->request->data['Edi']['work_date'] = date('Y/m/d');
        $this->set("SearchResult" , $SearchResult);

        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Edi.token' , $token);
        $this->request->data['Edi']['token'] = $token;
    }
    
    public function shipping_result(){
        if($this->request->data['Edi']['token'] === $this->Session->read('Edi.token')) {
            $this->Session->delete('Edi.token');
            $hospital_flg = false;
            $header_ids = array();
            $trn_order_ids = array();

            // LOT/UBD入力処理用
            $order_count = array();
            $order_data = array();
            
            // 登録処理
            $now = date('Y/m/d H:i:s');
            $this->TrnEdiReceiving->begin();
            
            $ids = join(',',$this->request->data['Edi']['id']);
            //行ロック
            $this->TrnEdiReceiving->query("select * from trn_edi_receivings as a where a.id in ( " . $ids . ") for update " );
            
            $headerArray = $this->registEdi(" and a.id in (" . $ids . ")");
            foreach ( $headerArray as &$h ) {
                $h['Edi']['work_no'] = $this->setWorkNo4Header( date('Ymd') , "29" , $h['Edi']['center_id']);
                
                // 出荷ヘッダ
                $TrnEdiShippingHeader = array(
                    'TrnEdiShippingHeader'=> array(
                        'work_no'                => $h['Edi']['work_no'],
                        'work_date'              => $this->request->data['Edi']['work_date'],
                        'recital'                => $this->request->data['Edi']['recital'],
                        'department_id_from'     => $h['Edi']['department_id_to'],          // センタ部署ID
                        'department_id_to'       => $h['Edi']['department_id_from'],        // 業者部署ID
                        'detail_count'           => 0,
                        'is_deleted'             => false,
                        'creater'                => $this->Session->read('Auth.MstUser.id'),
                        'created'                => $now,
                        'modifier'               => $this->Session->read('Auth.MstUser.id'),
                        'modified'               => $now 
                        )
                    );
                
                $this->TrnEdiShippingHeader->create();
                // SQL実行
                if (!$this->TrnEdiShippingHeader->save($TrnEdiShippingHeader)) {
                    $this->TrnEdiReceiving->rollback();
                    $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
                $h['Edi']['id'] = $this->TrnEdiShippingHeader->getLastInsertID();
                $header_ids[] = $h['Edi']['id'];
            }
            unset($h);
            
            foreach ($this->request->data['Edi']['id'] as $row => $id) {
                $sql  = "";
                $sql .= " update trn_edi_receivings ";
                $sql .= " set modified =  '" . $now . "'";
                $sql .= " ,   modifier = '" . $this->Session->read('Auth.MstUser.id') . "'";
                $sql .= " ,   remain_count = remain_count - " . $this->request->data['Edi']['quantity'][$row];
                $sql .= " where ";
                $sql .= " id = " . $id;
                $ret = $this->TrnOrder->query($sql , false , false);
                if($ret === FALSE){
                    // エラー
                    $this->TrnEdiReceiving->rollback();
                    $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('/');
                }
                // 出荷明細
                // TrnEdiReceivingの情報を取得
                $receiving = $this->TrnEdiReceiving->query(" select a.id , a.mst_item_unit_id, b.trn_order_header_id , b.id as trn_order_id , b.order_type from trn_edi_receivings as a left join trn_orders as b on b.id = a.trn_order_id where a.id =" . $id , false , false );
                foreach($headerArray as &$h){
                    if( $h['Edi']['order_header_id'] == $receiving[0][0]['trn_order_header_id'] ){
                        
                        // 出荷明細
                        $TrnEdiShipping = array(
                            'TrnEdiShipping'=> array(
                                'work_no'                    => $h['Edi']['work_no'],
                                'work_seq'                   => $h['Edi']['work_seq'] + 1,
                                'work_date'                  => $this->request->data['Edi']['work_date'],
                                'work_type'                  => null,
                                'recital'                    => $this->request->data['Edi']['comment'][$row],
                                'mst_item_unit_id'           => $receiving[0][0]['mst_item_unit_id'],
                                'department_id_from'         => $h['Edi']['department_id_to'],
                                'department_id_to'           => $h['Edi']['department_id_from'],
                                'quantity'                   => $this->request->data['Edi']['quantity'][$row],
                                'shipping_status'            => Configure::read('ShippingStatus.ordered'),
                                'lot_no'                     => $this->request->data['Edi']['lot_no'][$row],
                                'validated_date'             => $this->request->data['Edi']['validated_date'][$row],
                                'trn_edi_shipping_header_id' => $h['Edi']['id'],
                                'trn_edi_receiving_id'       => $receiving[0][0]['id'], 
                                'is_deleted'                 => false,
                                'creater'                    => $this->Session->read('Auth.MstUser.id'),
                                'created'                    => $now,
                                'modifier'                   => $this->Session->read('Auth.MstUser.id'),
                                'modified'                   => $now
                                )
                            );
                        $this->TrnEdiShipping->create();
                        // SQL実行
                        if (!$this->TrnEdiShipping->save($TrnEdiShipping)) {
                            $this->TrnEdiReceiving->rollback();
                            $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                            $this->redirect('/');
                        }
                        
                        $edi_shipping_id = $this->TrnEdiShipping->getLastInsertID();
                        
                        $h['Edi']['work_seq']++;
                        $trn_order_ids[] = $receiving[0][0]['trn_order_id'];
                        // 預託、非在庫、直納の場合シール情報の ロット番号と、有効期限を更新する。
                        if($receiving[0][0]['order_type'] == Configure::read('OrderTypes.replenish') ||
                           $receiving[0][0]['order_type'] == Configure::read('OrderTypes.direct') ||
                           $receiving[0][0]['order_type'] == Configure::read('OrderTypes.nostock') ||
                           $receiving[0][0]['order_type'] == Configure::read('OrderTypes.ExNostock')
                           ){
                            $order_id = $receiving[0][0]['trn_order_id'];
                            if(!isset($order_data[$order_id])){
                                // 発注IDからひもづくシールIDを取得
                                $sql  = ' select ';
                                $sql .= '       a.quantity ';
                                $sql .= '     , b.id  ';
                                $sql .= '   from ';
                                $sql .= '     trn_orders as a  ';
                                $sql .= '     left join trn_stickers as b  ';
                                $sql .= '       on b.trn_order_id = a.id  ';
                                $sql .= '   where ';
                                $sql .= '     a.id = ' . $order_id;
                                $sql .= '     and b.trn_edi_shipping_id is null ';
                                $sql .= '   order by b.id ';
                                $order_data[$order_id] = $this->TrnOrder->query($sql,false,false);
                                $order_count[$order_id] = 0;
                            }
                            $input_quantity = $this->request->data['Edi']['quantity'][$row];
                            
                            //数量分ループ
                            for( $count = 0 ; $input_quantity > $count ; $count++ ){
                                $sql  = "";
                                $sql .= " update trn_stickers ";
                                $sql .= " set modified =  '" . $now . "'";
                                $sql .= " ,   modifier = '" . $this->Session->read('Auth.MstUser.id') . "'";
                                
                                if($this->request->data['Edi']['lot_no'][$row] != ''){
                                    $sql .= " ,   lot_no         = '" . $this->request->data['Edi']['lot_no'][$row] . "'";
                                }
                                if($this->request->data['Edi']['validated_date'][$row] != '' ){
                                    $sql .= " ,   validated_date = '" . $this->request->data['Edi']['validated_date'][$row] . "'";
                                }
                                $sql .= " ,trn_edi_shipping_id = " . $edi_shipping_id ;
                                $sql .= " where ";
                                $sql .= " id = " . $order_data[$order_id][$order_count[$order_id]][0]['id'] ;
                                $ret = $this->TrnOrder->query($sql , false , false);

                                $order_count[$order_id]++;
                            }
                            
                            $hospital_flg = true ;
                        }
                    }
                }
                unset($h);
            }
            
            //出荷ヘッダの明細件数を更新
            $sql  = ' update trn_edi_shipping_headers x  ';
            $sql .= '   set ';
            $sql .= '     detail_count = y.count  ';
            $sql .= '   from ';
            $sql .= '     (  ';
            $sql .= '       select ';
            $sql .= '             a.trn_edi_shipping_header_id ';
            $sql .= '           , count(*)        as count  ';
            $sql .= '         from ';
            $sql .= '           trn_edi_shippings as a  ';
            $sql .= '         where ';
            $sql .= '           a.trn_edi_shipping_header_id in (' . join(',',$header_ids) . ')';
            $sql .= '         group by ';
            $sql .= '           a.trn_edi_shipping_header_id ';
            $sql .= '     ) as y  ';
            $sql .= '   where ';
            $sql .= '     x.id = y.trn_edi_shipping_header_id ';
            $this->TrnEdiReceiving->query($sql);
            
            $this->TrnEdiReceiving->commit();
            
            // 表示用のデータを取得
            $sql  = ' select ';
            $sql .= '       g.facility_name    as "Edi__facility_name" ';
            $sql .= '     , f.department_name  as "Edi__department_name"';
            $sql .= '     , d.internal_code    as "Edi__internal_code"';
            $sql .= '     , d.item_name        as "Edi__item_name"';
            $sql .= '     , d.item_code        as "Edi__item_code"';
            $sql .= '     , d.standard         as "Edi__standard"';
            $sql .= '     , d1.dealer_name     as "Edi__dealer_name"';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when c.per_unit = 1  ';
            $sql .= '         then c1.unit_name  ';
            $sql .= "         else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
            $sql .= '         end ';
            $sql .= '     )                    as "Edi__unit_name"';
            $sql .= '     , e.remain_count     as "Edi__remain_count"';
            $sql .= '     , e.work_no          as "Edi__work_no"';
            $sql .= "     , to_char(e.work_date,'YYYY/mm/dd')";
            $sql .= '                          as "Edi__work_date"';
            $sql .= '     , b.quantity         as "Edi__quantity"';
            $sql .= '     , b.lot_no           as "Edi__lot_no"';
            $sql .= "     , to_char(b.validated_date,'YYYY/mm/dd')";
            $sql .= '                          as "Edi__validated_date"';
            $sql .= '     , b.recital          as "Edi__recital"';
            $sql .= '   from ';
            $sql .= '     trn_edi_shipping_headers as a  ';
            $sql .= '     left join trn_edi_shippings as b  ';
            $sql .= '       on b.trn_edi_shipping_header_id = a.id  ';
            $sql .= '     left join mst_item_units as c  ';
            $sql .= '       on c.id = b.mst_item_unit_id  ';
            $sql .= '     left join mst_unit_names as c1  ';
            $sql .= '       on c1.id = c.mst_unit_name_id  ';
            $sql .= '     left join mst_unit_names as c2  ';
            $sql .= '       on c2.id = c.per_unit_name_id  ';
            $sql .= '     left join mst_facility_items as d  ';
            $sql .= '       on d.id = c.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as d1  ';
            $sql .= '       on d1.id = d.mst_dealer_id  ';
            $sql .= '     left join trn_edi_receivings as e  ';
            $sql .= '       on e.id = b.trn_edi_receiving_id  ';
            $sql .= '     left join mst_departments as f  ';
            $sql .= '       on f.id = e.department_id_from  ';
            $sql .= '     left join mst_facilities as g  ';
            $sql .= '       on g.id = f.mst_facility_id  ';
            $sql .= '   where ';
            $sql .= '     a.id in ( ' . join(',',$header_ids) . ')';
            $sql .= '   order by ';
            $sql .= '     a.work_no ';
            $sql .= '     , b.work_seq ';
            
            $SearchResult = $this->TrnEdiReceiving->query($sql,false,false);
            $this->set('SearchResult' , $SearchResult);
            $this->Session->write('Edi.ShippingResult' , $SearchResult);
            $this->set('hospital_flg', $hospital_flg);
            $this->Session->write('Edi.hospital_flg' , $hospital_flg);
            $this->set('trn_order_ids' , $trn_order_ids);
            $this->Session->write('Edi.trn_order_ids' , $trn_order_ids);
            $this->set('header_ids' , $header_ids);
            $this->Session->write('Edi.header_ids' , $header_ids);
        }else{
            //2度押し、リロード対策
            $this->set('SearchResult' , $this->Session->read('Edi.ShippingResult'));
            $this->set('hospital_flg', $this->Session->read('Edi.hospital_flg'));
            $this->set('trn_order_ids' , $this->Session->read('Edi.trn_order_ids'));
            $this->set('header_ids' , $this->Session->read('Edi.header_ids'));
        }
    }

    public function receiving_search($where=""){
        $sql  = " select ";
        $sql .= '     b.id               as "Edi__id"';
        $sql .= '   , a.id               as "Edi__header_id"';
        $sql .= "   , to_char(b.work_date,'YYYY/mm/dd') ";
        $sql .= '                        as "Edi__work_date"';
        $sql .= '   , b.work_no          as "Edi__work_no"';
        $sql .= '   , d.internal_code    as "Edi__internal_code"';
        $sql .= '   , d.item_name        as "Edi__item_name"';
        $sql .= '   , d.item_code        as "Edi__item_code"';
        $sql .= '   , d.standard         as "Edi__standard"';
        $sql .= '   , d.jan_code         as "Edi__jan_code"';
        $sql .= '   , d.class_separation as "Edi__class_separation"';
        $sql .= "   , (  ";
        $sql .= "     case  ";
        $sql .= "       when c.per_unit = 1  ";
        $sql .= "       then c1.unit_name  ";
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= "       end ";
        $sql .= '   )                    as "Edi__unit_name"';
        $sql .= '   , b.remain_count     as "Edi__remain_count"';
        $sql .= '   , e.dealer_name      as "Edi__dealer_name"';
        $sql .= '   , g.facility_name    as "Edi__facility_name"';
        $sql .= '   , f.department_name  as "Edi__department_name"';
        $sql .= '   , c.mst_facility_id  as "Edi__center_id"';
        $sql .= " from ";
        $sql .= "   trn_edi_receiving_headers as a  ";
        $sql .= "   left join trn_edi_receivings as b  ";
        $sql .= "     on b.trn_edi_receiving_header_id = a.id  ";
        $sql .= "   left join mst_item_units as c  ";
        $sql .= "     on c.id = b.mst_item_unit_id  ";
        $sql .= "   left join mst_unit_names as c1  ";
        $sql .= "     on c1.id = c.mst_unit_name_id  ";
        $sql .= "   left join mst_unit_names as c2  ";
        $sql .= "     on c2.id = c.per_unit_name_id  ";
        $sql .= "   left join mst_facility_items as d  ";
        $sql .= "     on d.id = c.mst_facility_item_id  ";
        $sql .= "   left join mst_dealers as e  ";
        $sql .= "     on e.id = d.mst_dealer_id  ";
        $sql .= "   left join mst_departments as f  ";
        $sql .= "     on f.id = a.department_id_from  ";
        $sql .= "   left join mst_facilities as g  ";
        $sql .= "     on g.id = f.mst_facility_id ";
        $sql .= "   left join trn_orders as h ";
        $sql .= "     on h.id = b.trn_order_id";

        $sql .= " where ";
        $sql .= "   b.is_deleted = false  ";
        $sql .= "   and h.is_deleted = false ";
        $sql .= "   and b.department_id_to = " . $this->Auth->user('mst_department_id');
        $sql .=$where;
        $sql .= " order by ";
        $sql .= "   b.work_date ";
        $sql .= "   , b.id ";
        
        $SearchResult = $this->TrnOrder->query($sql,false,false);
        return $SearchResult;
    }


    public function registEdi($where = ""){
        $sql  = " select ";
        $sql .= '     c.id                 as "Edi__order_header_id"';
        $sql .= '   , c.department_id_from as "Edi__department_id_from" ';
        $sql .= '   , c.department_id_to   as "Edi__department_id_to" ';
        $sql .= '   , count(c.id)          as "Edi__count" ';
        $sql .= '   , 0                    as "Edi__work_no"';
        $sql .= '   , 0                    as "Edi__work_seq"';
        $sql .= '   , 0                    as "Edi__id"';
        $sql .= '   , b.center_facility_id as "Edi__center_id"';
        
        $sql .= " from ";
        $sql .= "   trn_edi_receivings as a  ";
        $sql .= "   left join trn_orders as b  ";
        $sql .= "     on b.id = a.trn_order_id  ";
        $sql .= "   left join trn_order_headers as c  ";
        $sql .= "     on c.id = b.trn_order_header_id  ";
        $sql .= " where 1=1";
        $sql .= $where;
        $sql .= " group by ";
        $sql .= "   c.id ";
        $sql .= "   , c.department_id_from ";
        $sql .= "   , c.department_id_to ";
        $sql .= "   , b.center_facility_id ";
        
        $SearchResult = $this->TrnEdiReceiving->query($sql,false,false);
        return $SearchResult;
    }

    public function shipping_history(){
        App::import('Sanitize');
        //権限ロール
        $this->setRoleFunction(108); //出荷履歴
        $result = array();
        $department_list = array();
        
        if(isset($this->request->data['Edi']['is_search'])){
            $sql  = ' select ';
            $sql .= '     a.id                               as "Edi__id" ';
            $sql .= '   , a.work_no                          as "Edi__work_no" ';
            $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') ";
            $sql .= '                                        as "Edi__work_date" ';
            $sql .= '   , d.facility_name                    as "Edi__facility_name" ';
            $sql .= '   , c.department_name                  as "Edi__department_name" ';
            $sql .= '   , e.user_name                        as "Edi__user_name" ';
            $sql .= "   , to_char(a.created, 'YYYY/mm/dd HH24:MI:SS') ";
            $sql .= '                                        as "Edi__created" ';
            $sql .= '   , a.recital                          as "Edi__recital" ';
            $sql .= '   , count(a.id)                        as "Edi__count" ';
            $sql .= '   , a.detail_count                     as "Edi__detail_count"  ';
            $sql .= ' from ';
            $sql .= '   trn_edi_shipping_headers as a ';
            $sql .= '   left join trn_edi_shippings as b ';
            $sql .= '     on b.trn_edi_shipping_header_id = a.id ';
            $sql .= '   left join mst_departments as c ';
            $sql .= '     on c.id = a.department_id_to ';
            $sql .= '   left join mst_facilities as d ';
            $sql .= '     on d.id = c.mst_facility_id ';
            $sql .= '   left join mst_users as e ';
            $sql .= '     on e.id = a.creater ';
            $sql .= '   left join mst_item_units as f ';
            $sql .= '     on f.id = b.mst_item_unit_id ';
            $sql .= '   left join mst_facility_items as g ';
            $sql .= '     on g.id = f.mst_facility_item_id ';
            $sql .= '   left join mst_dealers as h ';
            $sql .= '     on h.id = g.mst_dealer_id ';
            $sql .= '   left join mst_departments as i ';
            $sql .= '     on i.id = a.department_id_from ';
            $sql .= '   left join trn_edi_receivings as j ';
            $sql .= '     on j.id = b.trn_edi_receiving_id ';
            $sql .= '   left join trn_orders as k ';
            $sql .= '     on k.id = j.trn_order_id ';
            $sql .= ' where 1=1 ';
            $sql .= '   and i.id = ' . $this->Auth->user('mst_department_id');
            $sql .= '   and k.is_deleted = false ';
            /* 絞り込み条件 */
            // 出荷番号
            if( $this->request->data['Edi']['search_receiving_no'] != "" ){
                $sql .= "   and a.work_no = '" . $this->request->data['Edi']['search_receiving_no'] . "'";
            }
            
            // 出荷日To
            if( $this->request->data['Edi']['search_start_date'] != "" ){
                $sql .= "   and a.work_date >= '" . $this->request->data['Edi']['search_start_date'] . "'";
            }
            // 出荷日From
            if( $this->request->data['Edi']['search_end_date'] != "" ){
                $sql .= "   and a.work_date <= '" . $this->request->data['Edi']['search_end_date'] . "'";
            }
            
            // 取消も表示する
            if( $this->request->data['Edi']['search_display_deleted'] != "1" ){
                $sql .= '   and a.is_deleted = false ';
                $sql .= '   and b.is_deleted = false ';
            }
            
            // 出荷施設
            if( $this->request->data['Edi']['mst_facility_code'] != "" ){
                $sql .= "   and d.facility_code = '" . $this->request->data['Edi']['mst_facility_code'] . "'";
                $department_list = $this->getDepartmentsList($this->request->data['Edi']['mst_facility_code']);
            }
            
            // 出荷部署
            if( $this->request->data['Edi']['mst_department_code'] != "" ){
                $sql .= "   and c.department_code = '" . $this->request->data['Edi']['mst_department_code'] . "'";
            }
            
            // 商品ID 
            if( $this->request->data['Edi']['search_internal_code'] != "" ){
                $sql .= "   and g.internal_code = '" . $this->request->data['Edi']['search_internal_code'] . "'";
            }            
            // 製品番号
            if( $this->request->data['Edi']['search_item_code'] != "" ){
                $sql .= "   and g.item_code like '%" . $this->request->data['Edi']['search_item_code'] . "%'";
            }
            // 商品名
            if( $this->request->data['Edi']['search_item_name'] != "" ){
                $sql .= "   and g.item_name like '%" . $this->request->data['Edi']['search_item_name'] . "%'";
            }
            // 販売元
            if( $this->request->data['Edi']['search_dealer_name'] != "" ){
                $sql .= "   and h.dealer_name like '%" . $this->request->data['Edi']['search_dealer_name'] . "%'";
            }
            // 規格
            if( $this->request->data['Edi']['search_standard'] != "" ){
                $sql .= "   and g.standard like '%" . $this->request->data['Edi']['search_standard'] . "%'";
            }
            // 作業区分
            if( $this->request->data['Edi']['search_classId'] != "" ){
                $sql .= "   and a.work_type = '" . $this->request->data['Edi']['search_classId'] . "'";
            }
            
            $sql .= ' group by ';
            $sql .= '   a.id ';
            $sql .= '   , a.work_no ';
            $sql .= '   , a.work_date ';
            $sql .= '   , c.department_name ';
            $sql .= '   , d.facility_name ';
            $sql .= '   , e.user_name ';
            $sql .= '   , a.created ';
            $sql .= '   , a.recital ';
            $sql .= '   , a.detail_count ';
            $sql .= ' order by ';
            $sql .= '   a.id ';
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnEdiShipping' , false));
            
            $sql .= ' limit '.  $this->request->data['limit'];
            $result = $this->TrnEdiShipping->query($sql , false , false );
        }else{
            $this->request->data['Edi']['search_start_date'] = date('Y/m/d', strtotime("-6 day"));
            $this->request->data['Edi']['search_end_date']   = date('Y/m/d');
        }
        
        $this->set('result' , $result);
        $this->set('class_list' , array());
        $this->set('facility_list' , $this->__getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                              array('facility_code' , 'facility_name')));
        $this->set('department_list' , $department_list);
        
    }

    public function shipping_history_confirm(){
        $result = array();
        $where = " and a.trn_edi_shipping_header_id  in ( " . join(",",$this->request->data['Edi']['id']) . ")" ;
        /* 前画面での検索条件引き継ぎ */
        // 出荷番号
        if( $this->request->data['Edi']['search_receiving_no'] != "" ){
            $where .= "   and a.work_no = '" . $this->request->data['Edi']['search_receiving_no'] . "'";
        }
        
        // 出荷日To
        if( $this->request->data['Edi']['search_start_date'] != "" ){
            $where .= "   and a.work_date >= '" . $this->request->data['Edi']['search_start_date'] . "'";
        }
        // 出荷日From
        if( $this->request->data['Edi']['search_end_date'] != "" ){
            $where .= "   and a.work_date <= '" . $this->request->data['Edi']['search_end_date'] . "'";
        }
        
        // 取消も表示する
        if( $this->request->data['Edi']['search_display_deleted'] != "1" ){
            $where .= '   and a.is_deleted = false ';
        }
        
        // 出荷施設
        if( $this->request->data['Edi']['mst_facility_code'] != "" ){
            $where .= "   and f.facility_code = '" . $this->request->data['Edi']['mst_facility_code'] . "'";
        }
        
        // 出荷部署
        if( $this->request->data['Edi']['mst_department_code'] != "" ){
            $where .= "   and e.department_code = '" . $this->request->data['Edi']['mst_department_code'] . "'";
        }
        
        // 商品ID 
        if( $this->request->data['Edi']['search_internal_code'] != "" ){
            $where .= "   and c.internal_code = '" . $this->request->data['Edi']['search_internal_code'] . "'";
        }            
        // 製品番号
        if( $this->request->data['Edi']['search_item_code'] != "" ){
            $where .= "   and c.item_code like '%" . $this->request->data['Edi']['search_item_code'] . "%'";
        }
        // 商品名
        if( $this->request->data['Edi']['search_item_name'] != "" ){
            $where .= "   and c.item_name like '%" . $this->request->data['Edi']['search_item_name'] . "%'";
        }
        // 販売元
        if( $this->request->data['Edi']['search_dealer_name'] != "" ){
            $where .= "   and g.dealer_name like '%" . $this->request->data['Edi']['search_dealer_name'] . "%'";
        }
        // 規格
        if( $this->request->data['Edi']['search_standard'] != "" ){
            $where .= "   and c.standard like '%" . $this->request->data['Edi']['search_standard'] . "%'";
        }
        // 作業区分
        if( $this->request->data['Edi']['search_classId'] != "" ){
            $where .= "   and a.work_type = '" . $this->request->data['Edi']['search_classId'] . "'";
        }
        
        $result = $this->shipping_history_search($where);
        $this->set('result' , $result);

        $sql  = ' select ';
        $sql .= '       b.mst_facility_id  ';
        $sql .= '   from ';
        $sql .= '     trn_edi_shippings as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '   where ';
        $sql .= '     a.trn_edi_shipping_header_id in (' . join(",",$this->request->data['Edi']['id']) . ')  ';
        $sql .= '   group by ';
        $sql .= '     b.mst_facility_id ';
        
        $cnt = $this->TrnEdiShipping->query($sql , false , false );
        if(count($cnt) == 1) {
            $this->set('center_id' , $cnt[0][0]['mst_facility_id']);
        }
    }

    public function shipping_history_result(){
        // 登録処理
        $now = date('Y/m/d H:i:s');
        $this->TrnEdiShipping->begin();
        
        $ids = join(',',$this->request->data['Edi']['id']);
        //行ロック
        $res = $this->TrnEdiShipping->query("select id , quantity , trn_edi_receiving_id from trn_edi_shippings as a where a.id in ( " . $ids . ") for update  " );
        
        // 出荷明細
        // 削除フラグを立てる
        $this->TrnEdiShipping->create();
        $ret = $this->TrnEdiShipping->updateAll(
            array(
                'TrnEdiShipping.is_deleted'  => "'true'",
                'TrnEdiShipping.modifier'      => $this->Session->read('Auth.MstUser.id'),
                'TrnEdiShipping.modified'      => "'" . $now . "'"
                ),
            array(
                'TrnEdiShipping.id'  => $this->request->data['Edi']['id'] ,
                ),
            -1
            );
        if(!$ret){
            $this->TrnEdiShipping->rollback();
            $this->Session->setFlash('出荷取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        
        // シールのEDI出荷明細をNULLにする
        $ret = $this->TrnSticker->updateAll(
            array(
                'TrnSticker.trn_edi_shipping_id'  => null,
                'TrnSticker.modifier'             => $this->Session->read('Auth.MstUser.id'),
                'TrnSticker.modified'             => "'" . $now . "'"
                ),
            array(
                'TrnSticker.trn_edi_shipping_id'  => $this->request->data['Edi']['id'] ,
                ),
            -1
            );
        if(!$ret){
            $this->TrnEdiShipping->rollback();
            $this->Session->setFlash('出荷取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        
        
        // 受注明細
        // 残数を増やす
        $i = 0 ;
        foreach($res as $r){
            $this->TrnEdiReceiving->create();
            $ret = $this->TrnEdiReceiving->updateAll(
                array(
                    'TrnEdiReceiving.remain_count'  => 'TrnEdiReceiving.remain_count + ' . $r[0]['quantity'],
                    'TrnEdiReceiving.modifier'      => $this->Session->read('Auth.MstUser.id'),
                    'TrnEdiReceiving.modified'      => "'" . $now . "'"
                    ),
                array(
                    'TrnEdiReceiving.id'  => $r[0]['trn_edi_receiving_id'] ,
                    ),
                -1
                );
            if(!$ret){
                $this->TrnEdiShipping->rollback();
                $this->Session->setFlash('出荷取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
        }
        
        $this->TrnEdiShipping->commit();
        $where = " and a.id in ( " . $ids . ")";
        $result = $this->shipping_history_search($where);
        $this->set('result' , $result);
    }

    /**
     * 業者出荷履歴検索
     */
    public function shipping_history_search($where){
        $sql  = " select ";
        $sql .= '     a.id               as "Edi__id" ';
        $sql .= '   , a.work_no          as "Edi__work_no"';
        $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') ";
        $sql .= '                        as "Edi__work_date"';
        $sql .= '   , c.internal_code    as "Edi__internal_code"';
        $sql .= '   , c.item_name        as "Edi__item_name"';
        $sql .= '   , c.item_code        as "Edi__item_code" ';
        $sql .= "   , ( case  ";
        $sql .= "     when b.per_unit = 1  ";
        $sql .= "     then b1.unit_name  ";
        $sql .= "     else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '     end )              as "Edi__unit_name"';
        $sql .= '   , d.work_no          as "Edi__order_work_no"';
        $sql .= '   , ( case ';
        $sql .= '     when d.order_type = ' . Configure::read('OrderTypes.replenish') .  ' then true ' ;
        $sql .= '     when d.order_type = ' . Configure::read('OrderTypes.direct') . ' then true ' ;
        $sql .= '     when d.order_type = ' . Configure::read('OrderTypes.nostock') . ' then true ' ;
        $sql .= '     when d.order_type = ' . Configure::read('OrderTypes.ExNostock') . ' then true ' ;
        $sql .= '     else false ';
        $sql .= '     end )              as "Edi__hospital_flg"';
        $sql .= '   , a.work_type        as "Edi__work_type"';
        $sql .= '   , a.recital          as "Edi__recital"';
        $sql .= '   , f.facility_name    as "Edi__facility_name"';
        $sql .= '   , e.department_name  as "Edi__department_name"';
        $sql .= '   , c.standard         as "Edi__standard"';
        $sql .= '   , g.dealer_name      as "Edi__dealer_name" ';
        $sql .= '   , h.user_name        as "Edi__user_name"';
        $sql .= '   , a.quantity         as "Edi__quantity"';
        $sql .= '   , a.lot_no           as "Edi__lot_no"';
        $sql .= '   , ( case ';
        $sql .= '         when c.enable_medicalcode > 0 ';
        $sql .= '         then true ';
        $sql .= '         else false ';
        $sql .= '      end )             as "Edi__cost_flg" ';
        $sql .= "   , to_char(a.validated_date,'YYYY/mm/dd') ";
        $sql .= '                        as "Edi__validated_date"';
        $sql .= '   , ( case when a.is_deleted = true then false else true end ) as "Edi__canCancel"';
        $sql .= '   , d.remain_count     as "Edi__order_remain_count"';
        $sql .= " from ";
        $sql .= "   trn_edi_shippings as a  ";
        $sql .= "   left join mst_item_units as b  ";
        $sql .= "     on b.id = a.mst_item_unit_id  ";
        $sql .= "   left join mst_unit_names as b1  ";
        $sql .= "     on b1.id = b.mst_unit_name_id  ";
        $sql .= "   left join mst_unit_names as b2  ";
        $sql .= "     on b2.id = b.per_unit_name_id  ";
        $sql .= "   left join mst_facility_items as c  ";
        $sql .= "     on c.id = b.mst_facility_item_id  ";
        $sql .= "   left join trn_edi_receivings as a1";
        $sql .= "     on a1.id = a.trn_edi_receiving_id ";
        $sql .= "   left join trn_orders as d  ";
        $sql .= "     on d.id = a1.trn_order_id  ";
        $sql .= "   left join mst_departments as e  ";
        $sql .= "     on e.id = a.department_id_to  ";
        $sql .= "   left join mst_facilities as f  ";
        $sql .= "     on f.id = e.mst_facility_id  ";
        $sql .= "   left join mst_dealers as g  ";
        $sql .= "     on g.id = c.mst_dealer_id  ";
        $sql .= "   left join mst_users as h  ";
        $sql .= "     on h.id = a.modifier ";

        $sql .= "   left join trn_edi_receivings as i  ";
        $sql .= "     on i.id = a.trn_edi_receiving_id ";
        $sql .= "   left join trn_orders as j  ";
        $sql .= "     on j.id = i.trn_order_id ";
        
        $sql .= " where 1=1 " ;
        $sql .= " and j.is_deleted = false ";
        $sql .= $where;
        $sql .= ' order by ';
        $sql .= '   a.work_no ';
        $sql .= '   , a.work_seq ';
        
        $result = $this->TrnEdiShipping->query($sql , false , false );
        return $result;
    }

    public function export_csv($mode=""){
        switch ($mode) {
          case "receiving":
            // 受注履歴
            $sql  = " select ";
            $sql .= "     a.work_no                          as 作業番号 ";
            $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') as 受注日 ";
            $sql .= "   , c.internal_code                    as 商品ID ";
            $sql .= "   , c.item_name                        as 商品名 ";
            $sql .= "   , c.item_code                        as 製品番号 ";
            $sql .= "   , (  ";
            $sql .= "     case  ";
            $sql .= "       when b.per_unit = 1  ";
            $sql .= "       then b1.unit_name  ";
            $sql .= "       else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
            $sql .= "       end ";
            $sql .= "   )                                    as 包装単位 ";
            $sql .= "   , d.work_no                          as 発注番号 ";
            $sql .= "   , a.work_type                        as 作業区分 ";
            $sql .= "   , a.recital                          as 備考 ";
            $sql .= "   , f.facility_name                    as 施設名 ";
            $sql .= "   , c.standard                         as 規格 ";
            $sql .= "   , g.dealer_name                      as 販売元 ";
            $sql .= "   , h.user_name                        as 更新者  ";
            $sql .= " from ";
            $sql .= "   trn_edi_receivings as a  ";
            $sql .= "   left join mst_item_units as b  ";
            $sql .= "     on b.id = a.mst_item_unit_id  ";
            $sql .= "   left join mst_unit_names as b1  ";
            $sql .= "     on b1.id = b.mst_unit_name_id  ";
            $sql .= "   left join mst_unit_names as b2  ";
            $sql .= "     on b2.id = b.per_unit_name_id  ";
            $sql .= "   left join mst_facility_items as c  ";
            $sql .= "     on c.id = b.mst_facility_item_id  ";
            $sql .= "   left join trn_orders as d  ";
            $sql .= "     on d.id = a.trn_order_id  ";
            $sql .= "   left join mst_departments as e  ";
            $sql .= "     on e.id = a.department_id_from  ";
            $sql .= "   left join mst_facilities as f  ";
            $sql .= "     on f.id = e.mst_facility_id  ";
            $sql .= "   left join mst_dealers as g  ";
            $sql .= "     on g.id = c.mst_dealer_id  ";
            $sql .= "   left join mst_users as h  ";
            $sql .= "     on h.id = a.modifier  ";
            $sql .= "   left join mst_departments as i ";
            $sql .= "     on i.id = a.department_id_to ";
            $sql .= " where ";
            $sql .= "   1 = 1 ";
            $sql .= "   and i.id = " . $this->Auth->user('mst_department_id');
            /* 絞り込み条件 */
            // 受注番号
            if( $this->request->data['Edi']['search_receiving_no'] != "" ){
                $sql .= "   and a.work_no = '" . $this->request->data['Edi']['search_receiving_no'] . "'";
            }
            
            // 受注日To
            if( $this->request->data['Edi']['search_start_date'] != "" ){
                $sql .= "   and a.work_date >= '" . $this->request->data['Edi']['search_start_date'] . "'";
            }
            // 受注日From
            if( $this->request->data['Edi']['search_end_date'] != "" ){
                $sql .= "   and a.work_date <= '" . $this->request->data['Edi']['search_end_date'] . "'";
            }
            
            // 取消も表示する
            if( $this->request->data['Edi']['search_display_deleted'] != "1" ){
                $sql .= "   and a.is_deleted = false  ";
                $sql .= "   and b.is_deleted = false  ";
            }
            
            // 受注施設
            if( $this->request->data['Edi']['mst_facility_code'] != "" ){
                $sql .= "   and d.facility_code = '" . $this->request->data['Edi']['mst_facility_code'] . "'";
            }
            
            // 商品ID 
            if( $this->request->data['Edi']['search_internal_code'] != "" ){
                $sql .= "   and g.internal_code = '" . $this->request->data['Edi']['search_internal_code'] . "'";
            }            
            // 製品番号
            if( $this->request->data['Edi']['search_item_code'] != "" ){
                $sql .= "   and g.item_code like '%" . $this->request->data['Edi']['search_item_code'] . "%'";
            }
            // 商品名
            if( $this->request->data['Edi']['search_item_name'] != "" ){
                $sql .= "   and g.item_name like '%" . $this->request->data['Edi']['search_item_name'] . "%'";
            }
            // 販売元
            if( $this->request->data['Edi']['search_dealer_name'] != "" ){
                $sql .= "   and h.dealer_name like '%" . $this->request->data['Edi']['search_dealer_name'] . "%'";
            }
            // 規格
            if( $this->request->data['Edi']['search_standard'] != "" ){
                $sql .= "   and g.standard like '%" . $this->request->data['Edi']['search_standard'] . "%'";
            }
            // 作業区分
            if( $this->request->data['Edi']['search_classId'] != "" ){
                $sql .= "   and a.work_type = '" . $this->request->data['Edi']['search_classId'] . "'";
            }
            
            $filename = "受注履歴";
            $redirecturl = "receiving_history";
            break;
          case "shipping":
            // 出荷履歴
            $sql  = " select ";
            $sql .= "     a.work_no                          as 作業番号 ";
            $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') as 出荷日 ";
            $sql .= "   , c.internal_code                    as 商品ID ";
            $sql .= "   , c.item_name                        as 商品名 ";
            $sql .= "   , c.item_code                        as 製品番号 ";
            $sql .= "   , (  ";
            $sql .= "     case  ";
            $sql .= "       when b.per_unit = 1  ";
            $sql .= "       then b1.unit_name  ";
            $sql .= "       else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
            $sql .= "       end ";
            $sql .= "   )                                    as 包装単位 ";
            $sql .= "   , d.work_no                          as 発注番号 ";
            $sql .= "   , a.work_type                        as 作業区分 ";
            $sql .= "   , a.recital                          as 備考 ";
            $sql .= "   , f.facility_name                    as 施設名 ";
            $sql .= "   , c.standard                         as 規格 ";
            $sql .= "   , g.dealer_name                      as 販売元 ";
            $sql .= "   , h.user_name                        as 更新者 ";
            $sql .= "   , a.quantity                         as 数量  ";
            $sql .= " from ";
            $sql .= "   trn_edi_shippings as a ";
            $sql .= "   left join mst_item_units as b ";
            $sql .= "     on b.id = a.mst_item_unit_id ";
            $sql .= "   left join mst_unit_names as b1 ";
            $sql .= "     on b1.id = b.mst_unit_name_id ";
            $sql .= "   left join mst_unit_names as b2 ";
            $sql .= "     on b2.id = b.per_unit_name_id ";
            $sql .= "   left join mst_facility_items as c ";
            $sql .= "     on c.id = b.mst_facility_item_id ";
            $sql .= "   left join trn_edi_receivings as a1 ";
            $sql .= "     on a1.id = a.trn_edi_receiving_id ";
            $sql .= "   left join trn_orders as d ";
            $sql .= "     on d.id = a1.trn_order_id ";
            $sql .= "   left join mst_departments as e ";
            $sql .= "     on e.id = a.department_id_from ";
            $sql .= "   left join mst_facilities as f ";
            $sql .= "     on f.id = e.mst_facility_id ";
            $sql .= "   left join mst_dealers as g ";
            $sql .= "     on g.id = c.mst_dealer_id ";
            $sql .= "   left join mst_users as h ";
            $sql .= "     on h.id = a.modifier ";
            $sql .= "   left join mst_departments as i ";
            $sql .= "     on i.id = a.department_id_to ";
            $sql .= " where ";
            $sql .= "   1 = 1  ";
            $sql .= "   and e.id = " . $this->Auth->user('mst_department_id');
            /* 絞り込み条件 */
            // 出荷番号
            if( $this->request->data['Edi']['search_receiving_no'] != "" ){
                $sql .= "   and a.work_no = '" . $this->request->data['Edi']['search_receiving_no'] . "'";
            }
            
            // 出荷日To
            if( $this->request->data['Edi']['search_start_date'] != "" ){
                $sql .= "   and a.work_date >= '" . $this->request->data['Edi']['search_start_date'] . "'";
            }
            // 出荷日From
            if( $this->request->data['Edi']['search_end_date'] != "" ){
                $sql .= "   and a.work_date <= '" . $this->request->data['Edi']['search_end_date'] . "'";
            }
            
            // 取消も表示する
            if( $this->request->data['Edi']['search_display_deleted'] != "1" ){
                $sql .= "   and a.is_deleted = false  ";
                $sql .= "   and b.is_deleted = false  ";
            }
            
            // 出荷施設
            if( $this->request->data['Edi']['mst_facility_code'] != "" ){
                $sql .= "   and d.facility_code = '" . $this->request->data['Edi']['mst_facility_code'] . "'";
            }
            
            // 商品ID 
            if( $this->request->data['Edi']['search_internal_code'] != "" ){
                $sql .= "   and g.internal_code = '" . $this->request->data['Edi']['search_internal_code'] . "'";
            }            
            // 製品番号
            if( $this->request->data['Edi']['search_item_code'] != "" ){
                $sql .= "   and g.item_code like '%" . $this->request->data['Edi']['search_item_code'] . "%'";
            }
            // 商品名
            if( $this->request->data['Edi']['search_item_name'] != "" ){
                $sql .= "   and g.item_name like '%" . $this->request->data['Edi']['search_item_name'] . "%'";
            }
            // 販売元
            if( $this->request->data['Edi']['search_dealer_name'] != "" ){
                $sql .= "   and h.dealer_name like '%" . $this->request->data['Edi']['search_dealer_name'] . "%'";
            }
            // 規格
            if( $this->request->data['Edi']['search_standard'] != "" ){
                $sql .= "   and g.standard like '%" . $this->request->data['Edi']['search_standard'] . "%'";
            }
            // 作業区分
            if( $this->request->data['Edi']['search_classId'] != "" ){
                $sql .= "   and a.work_type = '" . $this->request->data['Edi']['search_classId'] . "'";
            }
            $filename = "出荷履歴";
            $redirecturl = "shipping_history";
            break;
          default:
            // 想定以外の場合何もしない
            break;
        }

        $this->db_export_csv($sql , $filename, $redirecturl, false);
    }

    /* 帳票印刷 */
    function report($mode=''){
        if($mode=='order'){
            //発注
            $this->order_report();
        }elseif($mode=='replenish'){
            //預託品
            $this->replenish_report();
        }elseif($mode=='remainlist'){
            //残数
            $this->remain_report();
        }elseif($mode=='shipping'){
            //出荷伝票
            $this->shipping_report();
        }elseif($mode=='d2shipping'){
            //直納出荷伝票
            $this->d2shipping_report();
        }
    }

    /* 発注書印刷 */
    function order_report(){
        $header_id = $this->request->data['Order']['id'];
        $order = $this->_getPrintData($header_id , 1 );
        $columns = array("発注番号",
                         "仕入先名",
                         "納品先名",
                         "施設名",
                         "発注日",
                         "納品先住所",
                         "施設住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "金額",
                         "単価",
                         "明細備考",
                         "包装単位",
                         "課税区分",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $tmpHeader = "";
        $i = 1;
        $tmp_page_no = '';

        foreach($order as $detail){
            $page_no = $detail['TrnOrder']['work_no'].$detail['MstOwner']['id'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }

            if($tmpHeader !== $detail['TrnOrderHeader']['id']){
                $tmpHeader = $detail['TrnOrderHeader']['id'];
            }
            
            $price = $detail['TrnOrder']['stocking_price'] * $detail['TrnOrder']['quantity'];
            if($detail['FacilityTo']['round'] == Configure::read("RoundType.Round") ){
                $price = round($price);
            }else if( $detail['FacilityTo']['round'] == Configure::read("RoundType.Ceil") ){
                $price = ceil($price);
            }else if( $detail['FacilityTo']['round'] == Configure::read("RoundType.Floor") ){
                $price = floor($price);
            }else{
                $price = (int)$price;
            }
            $facility_name = $detail['FacilityTo']['facility_formal_name'];
            if( !empty($detail['FacilityTo2']['facility_formal_name']) && 
                $detail['FacilityTo']['facility_formal_name'] != $detail['FacilityTo2']['facility_formal_name']
            ){
                $facility_name .= "／" . $detail['FacilityTo2']['facility_formal_name'];
            }
            $facility_item_code = $detail['MstFacilityItem']['internal_code'];
            if(!empty($detail['MstFacilityItem']['aptage_code'])){
                $facility_item_code .= "／" . $detail['MstFacilityItem']['aptage_code'];
            }

            $data[] = array($detail['TrnOrder']['work_no']                                              //発注番号
                            ,$facility_name."様"                                                        //仕入先名
                            ,$detail['FacilityFrom']['name']                                            //納品先名
                            ,$detail['MstOwner']['facility_formal_name']                                //施主名
                            ,$detail['TrnOrder']['work_date']                                           //発注日
                            ,$detail['FacilityFrom']['address']                                         //納品先住所
                            ,$detail['MstOwner']['address']                                             //施設住所
                            ,$i                                                                         //行番号
                            ,$detail['MstFacilityItem']['item_name']                                    //商品名
                            ,$detail['MstFacilityItem']['standard']                                     //規格
                            ,$detail['MstFacilityItem']['item_code']                                    //製品番号
                            ,$detail['MstDealer']['dealer_name']                                        //販売元
                            ,$facility_item_code                                                        //商品ID
                            ,number_format($price)                                                      //金額
                            ,$detail['TrnOrder']['stocking_price']                                      //単価
                            ,$detail['TrnOrder']['recital']                                             //明細備考
                            ,$detail['TrnOrder']['quantity'] . $detail['MstFacilityItem']['unit_name']  //包装単位
                            ,$detail['MstFacilityItem']['tax_type_name']                                //課税区分
                            ,$detail['TrnOrder']['quantity']                                            //数量
                            ,$detail['TrnOrderHeader']['recital']                                       //ヘッダ備考
                            ,$detail['TrnOrderHeader']['printed_date']                                  //印刷年月日
                            ,$detail['FacilityTo']['round']                                             //丸め区分
                            ,$detail['TrnOrder']['is_deleted']                                          //削除フラグ
                            ,$detail['TrnOrder']['change']                                              //変更フラグ
                            ,$page_no                                                                   //改ページ番号
                            );
            $i++;
        }
        pr($data);
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'発注書');
        $this->xml_output($data,join("\t",$columns),$layout_name['10'],$fix_value);
    }

    /* 預託品補充印刷 */
    function replenish_report(){
        $header_id = $this->request->data['Order']['id'];
        $order = $this->_getPrintData($header_id , 2);
        
        $columns = array("補充依頼番号",
                         "仕入先名",
                         "補充先名",
                         "施主名",
                         "補充依頼日",
                         "納品先住所",
                         "施主住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "明細備考",
                         "包装単位",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $tmpHeader = "";
        $i = 1;
        $tmp_page_no = '';
        foreach($order as $detail){
            $page_no = $detail['TrnOrder']['work_no'].$detail['MstOwner']['id'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }

            if($tmpHeader !== $detail['TrnOrderHeader']['id']){
                $tmpHeader = $detail['TrnOrderHeader']['id'];
            }
            $facility_name = $detail['FacilityTo']['facility_formal_name'];
            if($detail['FacilityTo']['facility_formal_name'] != $detail['FacilityTo2']['facility_formal_name']){
                $facility_name .= "／" . $detail['FacilityTo2']['facility_formal_name'];
            }
            $facility_item_code = $detail['MstFacilityItem']['internal_code'];
            if(!empty($detail['MstFacilityItem']['aptage_code'])){
                $facility_item_code .= "／" . $detail['MstFacilityItem']['aptage_code'];
            }

            $data[] = array( $detail['TrnOrder']['work_no']                                             //補充依頼番号
                            ,$facility_name                                                             //仕入先名
                            ,$detail['FacilityFrom']['name']                                            //補充先名
                            ,$detail['MstOwner']['facility_formal_name']                                //施主名
                            ,$detail['TrnOrder']['work_date']                                           //補充依頼日
                            ,$detail['FacilityFrom']['address']                                         //納品先住所
                            ,$detail['MstOwner']['address']                                             //施主住所
                            ,$i                                                                         //行番号
                            ,$detail['MstFacilityItem']['item_name']                                    //商品名
                            ,$detail['MstFacilityItem']['standard']                                     //規格
                            ,$detail['MstFacilityItem']['item_code']                                    //製品番号
                            ,$detail['MstDealer']['dealer_name']                                        //販売元
                            ,$facility_item_code                                                        //商品ID
                            ,$detail['TrnOrder']['recital']                                             //明細備考
                            ,$detail['MstFacilityItem']['unit_name']                                    //包装単位
                            ,$detail['TrnOrder']['quantity']                                            //数量
                            ,$detail['TrnOrderHeader']['recital']                                       //ヘッダ備考
                            ,$detail['TrnOrderHeader']['printed_date']                                  //印刷年月日
                            ,$detail['FacilityTo']['round']                                             //丸め区分
                            ,$detail['TrnOrder']['is_deleted']                                          //削除フラグ
                            ,$detail['TrnOrder']['change']                                              //変更フラグ
                            ,$page_no                                                                   //改ページ番号
                            );
            $i++;
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'補充依頼票');
        $this->xml_output($data,join("\t",$columns),$layout_name['11'],$fix_value);
    }

    //発注帳票データ取得
    function _getPrintData ($header_id , $type) {
        $sql  = ' select ';
        $sql .= '     a.id                     as "TrnOrder__id"';
        $sql .= '   , a.work_no                as "TrnOrder__work_no"';
        $sql .= '   , a.work_seq               as "TrnOrder__work_seq"';
        $sql .= "   , to_char(a.work_date,'YYYY年MM月dd日')";
        $sql .= '                              as "TrnOrder__work_date"';
        $sql .= '   , a.work_type              as "TrnOrder__work_type"';
        $sql .= '   , a.recital                as "TrnOrder__recital"';
        $sql .= '   , a.order_type             as "TrnOrder__order_type"';
        $sql .= '   , a.order_status           as "TrnOrder__order_status"';
        $sql .= '   , a.quantity               as "TrnOrder__quantity"';
        $sql .= '   , ( case ';
        $sql .= '       when a.before_quantity = a.quantity then 0 ';
        $sql .= '       else 1 ';
        $sql .= '       end )                  as "TrnOrder__change"';
        $sql .= '   , a.remain_count           as "TrnOrder__remain_count"';
        $sql .= '   , a.stocking_price         as "TrnOrder__stocking_price"';
        $sql .= '   , ( case ';
        $sql .= '       when a.is_deleted = true then 1 ';
        $sql .= '       else 0 ';
        $sql .= '       end )                  as "TrnOrder__is_deleted"';
        $sql .= '   , a1.id                    as "TrnOrderHeader__id"';
        $sql .= '   , a1.recital               as "TrnOrderHeader__recital"';
        $sql .= '   , a1.printed_date          as "TrnOrderHeader__printed_date"';
        $sql .= '   , ( case when b.per_unit = 1 then b1.unit_name ';
        $sql .= "       else b1.unit_name || '(' ||  b.per_unit || b2.unit_name || ')' ";
        $sql .= '       end )                  as "MstFacilityItem__unit_name"';
        $sql .= '   , c.internal_code          as "MstFacilityItem__internal_code"';
        $sql .= '   , c.item_name              as "MstFacilityItem__item_name"';
        $sql .= '   , c.standard               as "MstFacilityItem__standard"';
        $sql .= '   , c.item_code              as "MstFacilityItem__item_code"';
        $sql .= '   , c.spare_key2             as "MstFacilityItem__aptage_code"';
        $sql .= '   , ( case ';
        foreach(Configure::read('PrintFacilityItems.taxes') as $k=>$v){
            $sql .= '       when c.tax_type =' . $k . " then '" . $v . "'";
        }
        $sql .= "       else '' ";
        $sql .= '      end )                   as "MstFacilityItem__tax_type_name"';
        $sql .= '   , d.dealer_name            as "MstDealer__dealer_name"';
        $sql .= '   , e.id                     as "MstOwner__id"';
        $sql .= '   , e.facility_formal_name   as "MstOwner__facility_formal_name"';
        $sql .= "   , '〒' || coalesce(e.zip,'') || ' ' || coalesce(e.address,'') || '\nTel:' || coalesce(e.tel,'') || '／Fax:' || coalesce(e.fax,'')";
        $sql .= '                              as "MstOwner__address"';
        $sql .= '   , f.id                     as "DepartmentFrom__id"';
        $sql .= '   , f.department_formal_name as "DepartmentFrom__department_formal_name"';
        $sql .= '   , ( case ';
        $sql .= '       when g.facility_type = ' . Configure::read('FacilityType.hospital') ;
        $sql .= "       then g.facility_formal_name || ' ' || f.department_formal_name ";
        $sql .= '       else g.facility_formal_name ';
        $sql .= '       end )                 as "FacilityFrom__name"';
        
        $sql .= '   , g.id                     as "FacilityFrom__id"';
        $sql .= '   , g.facility_formal_name   as "FacilityFrom__facility_formal_name"';
        $sql .= "   , '〒' || coalesce(g.zip,'') || ' ' || coalesce(g.address,'') || '\nTel:' || coalesce(g.tel,'') || '／Fax:' || coalesce(g.fax,'')";
        $sql .= '                              as "FacilityFrom__address"';
        $sql .= '   , h.id                     as "DepartmentTo__id"';
        $sql .= '   , h.department_formal_name as "DepartmentTo__department_formal_name"';
        $sql .= '   , i.id                     as "FacilityTo__id"';
        $sql .= '   , i.facility_formal_name   as "FacilityTo__facility_formal_name"';
        $sql .= '   , i.facility_type          as "FacilityTo__facility_type"';
        $sql .= '   , i.zip                    as "FacilityTo__zip"';
        $sql .= '   , i.address                as "FacilityTo__address"';
        $sql .= '   , i.tel                    as "FacilityTo__tel"';
        $sql .= '   , i.fax                    as "FacilityTo__fax"';
        $sql .= '   , i.gross                  as "FacilityTo__gross"';
        $sql .= '   , i.round                  as "FacilityTo__round"';
        $sql .= '   , k.id                     as "FacilityTo2__id"';
        $sql .= '   , k.facility_formal_name   as "FacilityTo2__facility_formal_name"';
        $sql .= ' from ';
        $sql .= '   trn_orders as a  ';
        $sql .= '   left join trn_order_headers as a1  ';
        $sql .= '     on a1.id = a.trn_order_header_id  ';
        $sql .= '   left join mst_item_units as b  ';
        $sql .= '     on b.id = a.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as b1  ';
        $sql .= '     on b1.id = b.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as b2  ';
        $sql .= '     on b2.id = b.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as c  ';
        $sql .= '     on c.id = b.mst_facility_item_id  ';
        $sql .= '   left join mst_dealers as d  ';
        $sql .= '     on d.id = c.mst_dealer_id  ';
        $sql .= '   left join mst_facilities as e  ';
        $sql .= '     on e.id = c.mst_owner_id  ';
        $sql .= '     and e.facility_type = ' . Configure::read('FacilityType.donor');
        $sql .= '   left join mst_departments as f  ';
        $sql .= '     on f.id = a.department_id_from  ';
        $sql .= '   left join mst_facilities as g  ';
        $sql .= '     on g.id = f.mst_facility_id  ';
        $sql .= '   left join mst_departments as h  ';
        $sql .= '     on h.id = a.department_id_to  ';
        $sql .= '   left join mst_facilities as i  ';
        $sql .= '     on i.id = h.mst_facility_id  ';
        $sql .= '   left join mst_departments as j  ';
        $sql .= '     on j.id = a.department_id_to2  ';
        $sql .= '   left join mst_facilities as k  ';
        $sql .= '     on k.id = j.mst_facility_id  ';
        $sql .= ' where ';
        if(is_array($header_id)){
            $sql .= '   a.trn_order_header_id in (' . join(',',$header_id) . ')';
        }else{
            $sql .= '   a.trn_order_header_id = ' . $header_id;
        }
        if($type == 1 ){
            $sql .= ' and a.order_type <> ' . Configure::read('OrderTypes.replenish');
        }else{
            $sql .= ' and a.order_type = ' . Configure::read('OrderTypes.replenish');
        }
        $sql .= ' order by ';
        $sql .= '   a.trn_order_header_id ';
        $sql .= '   , e.id ';
        $sql .= '   , d.dealer_name ';
        $sql .= '   , c.item_name ';
        $sql .= '   , c.item_code ';
        
        $order = $this->TrnOrder->query($sql , false , false);
        
        return $order;
    }

   
    /* 残数一覧印刷 */
    function remain_report(){
        $where = '';
        /* 日付指定From */
        if(!empty($this->request->data['Edi']['search_start_date'])){
            $where .= " and i.work_date >= '" . $this->request->data['Edi']['search_start_date'] . "'";
        }
        /* 日付指定To */
        if(!empty($this->request->data['Edi']['search_end_date'])){
            $where .= " and i.work_date <= '" . $this->request->data['Edi']['search_end_date'] . "'";
        }
        $where .= ' and f.id = ' . $this->request->data['Edi']['facility_id'];
        
        $remain = $this->getRemainData($where);
        $columns = array("発注番号",
                         "仕入先名",
                         "納品先名",
                         "施設名",
                         "発注日",
                         "納品先住所",
                         "施設住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "金額",
                         "単価",
                         "明細備考",
                         "包装単位",
                         "課税区分",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $i = 1;
        $tmp_page_no = '';

        foreach($remain as $r){
            $page_no = $r[0]['work_date'].$r[0]['facility_name'].$r[0]['edi_name'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }
            
            $data[] = array(''                                       //発注番号
                            ,$r[0]['edi_name']."様"                  //仕入先名
                            ,''                                      //納品先名
                            ,$r[0]['facility_name']                  //施主名
                            ,$r[0]['work_date']                      //発注日
                            ,''                                      //納品先住所
                            ,$r[0]['address']                        //施設住所
                            ,$i                                      //行番号
                            ,$r[0]['item_name']                      //商品名
                            ,$r[0]['standard']                       //規格
                            ,$r[0]['item_code']                      //製品番号
                            ,$r[0]['dealer_name']                    //販売元
                            ,$r[0]['internal_code']                  //商品ID
                            ,$r[0]['total_price']                    //金額
                            ,$r[0]['stocking_price']                 //単価
                            ,''                                      //明細備考
                            ,$r[0]['quantity'] . $r[0]['unit_name']  //包装単位
                            ,''                                      //課税区分
                            ,$r[0]['quantity']                       //数量
                            ,''                                      //ヘッダ備考
                            ,''                                      //印刷年月日
                            ,''                                      //丸め区分
                            ,0                                       //削除フラグ
                            ,0                                       //変更フラグ
                            ,$page_no                                //改ページ番号
                            );
            $i++;
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'日別発注集計');
        $this->xml_output($data,join("\t",$columns),$layout_name['10'],$fix_value);
        
    }

    function getRemainData($where){
        $sql  = ' select ';
        $sql .= '       f.facility_name as edi_name';
        $sql .= '     , h.facility_name as facility_name';
        $sql .= "     , '〒' || h.zip || ' ' || h.address || '\nTel ' || h.tel || '／Fax ' || h.fax  as address ";
        $sql .= "     , to_char(a.work_date, 'YYYY年mm月dd日') as work_date ";
        $sql .= '     , c.internal_code ';
        $sql .= '     , c.item_name ';
        $sql .= '     , c.item_code ';
        $sql .= '     , c.standard ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then b1.unit_name  ';
        $sql .= "         else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                            as unit_name ';
        $sql .= '     , d.dealer_name ';
        $sql .= '     , sum(a.quantity)            as quantity ';
        $sql .= '     , a.stocking_price ';
        $sql .= '     , sum(a.stocking_price * a.quantity ) as total_price ';
        $sql .= '   from ';
        $sql .= '     trn_orders as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1  ';
        $sql .= '       on b1.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as b2  ';
        $sql .= '       on b2.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as d  ';
        $sql .= '       on d.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_departments as e  ';
        $sql .= '       on e.id = a.department_id_to  ';
        $sql .= '     left join mst_facilities as f  ';
        $sql .= '       on f.id = e.mst_facility_id  ';
        $sql .= '     left join mst_departments as g  ';
        $sql .= '       on g.id = a.department_id_from  ';
        $sql .= '     left join mst_facilities as h  ';
        $sql .= '       on h.id = g.mst_facility_id  ';
        $sql .= '     left join trn_edi_receivings as i ';
        $sql .= '       on i.trn_order_id = a.id ';
        $sql .= '   where i.is_deleted = false ';
        $sql .= '       and a.is_deleted = false ';
        $sql .= '       and i.remain_count > 0';
        $sql .= $where;
        $sql .= '   group by ';
        $sql .= '     f.facility_name ';
        $sql .= '     , h.facility_name ';
        $sql .= '     , a.work_date ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , c.item_name ';
        $sql .= '     , c.item_code ';
        $sql .= '     , c.standard ';
        $sql .= '     , d.dealer_name ';
        $sql .= '     , b1.unit_name ';
        $sql .= '     , b.per_unit ';
        $sql .= '     , b2.unit_name ';
        $sql .= '     , a.stocking_price  ';
        $sql .= '     , h.zip ';
        $sql .= '     , h.address ';
        $sql .= '     , h.tel ';
        $sql .= '     , h.fax ' ;
        $sql .= '   order by ';
        $sql .= '     f.facility_name ';
        $sql .= '     , a.work_date ';
        $sql .= '     , h.facility_name ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , b.per_unit ';

        $result = $this->TrnOrder->query($sql , false , false );
        
        return $result;
    }
    
    /* 出荷伝票印刷 */
    function shipping_report(){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        if(isset($this->request->data['Edi']['id'])){
            $cond = $this->request->data['Edi']['id'];
        }else if(isset($this->request->data['d2promises']['trn_order_header_id'])){
            // 業者直納履歴から
            $sql  = ' select ';
            $sql .= '       e.id  ';
            $sql .= '   from ';
            $sql .= '     trn_order_headers as a  ';
            $sql .= '     left join trn_orders as b  ';
            $sql .= '       on b.trn_order_header_id = a.id  ';
            $sql .= '     left join trn_edi_receivings as c  ';
            $sql .= '       on c.trn_order_id = b.id  ';
            $sql .= '     left join trn_edi_shippings as d  ';
            $sql .= '       on d.trn_edi_receiving_id = c.id  ';
            $sql .= '     left join trn_edi_shipping_headers as e  ';
            $sql .= '       on e.id = d.trn_edi_shipping_header_id  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',',$this->request->data['d2promises']['trn_order_header_id']) . ') ';
            $sql .= '   group by ';
            $sql .= '     e.id ';
            
            $ret = $this->TrnEdiShipping->query($sql);
            foreach($ret as $r){
                $cond[] = $r[0]['id'];
            }
        }
        $where = null;
        if(empty($cond)){
            //発注履歴からの出荷伝票
            //絞り込み条件を追加
            $where .= ' and d.mst_facility_id = ' . $this->request->data['SplitTable']['center_facility_id'];
            $where .= ' and a.is_deleted = false ';
            $where .= ' and b.is_deleted = false ';
            $where .= ' and c.is_deleted = false ';
            $where .= ' and k.is_deleted = false ';
            $where .= ' and l.is_deleted = false ';
            $where .= ' and l.remain_count > 0 ';
            //仕入先
            if($this->request->data['search']['supplierCode'] != ''){
                $where .= " and g.facility_code = '" . $this->request->data['search']['supplierCode'] . "'";
            }
        }
        $res = $this->getReportData($cond , $where);
        $columns = array(
            '納品先名',
            '納品番号',
            '納品先部署名',
            '施設名',
            '納品日',
            '納品先住所',
            '施設住所',
            '行番号',
            '商品名',
            '規格',
            '製品番号',
            '販売元',
            'ロット番号',
            '有効期限',
            '明細備考',
            '包装単位',
            '管理区分',
            '商品ID',
            '単価',
            '金額',
            '印刷年月日',
            'ヘッダ備考',
            '丸め区分',
            '削除フラグ',
            '改ページ番号'
        );
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'出荷伝票','sortkey' =>'納品番号,改ページ番号');

        $this->xml_output($res,join("\t",$columns),$layout_name['32'],$fix_value);
    }

    /* 直納出荷伝票印刷 */
    function d2shipping_report(){
        $this->request->data['now'] = date('Y/m/d H:i:s');
        if(isset($this->request->data['Edi']['id'])){
            $cond = $this->request->data['Edi']['id'];
        }else if(isset($this->request->data['d2promises']['trn_order_header_id'])){
            // 業者直納履歴から
            $sql  = ' select ';
            $sql .= '       e.id  ';
            $sql .= '   from ';
            $sql .= '     trn_order_headers as a  ';
            $sql .= '     left join trn_orders as b  ';
            $sql .= '       on b.trn_order_header_id = a.id  ';
            $sql .= '     left join trn_edi_receivings as c  ';
            $sql .= '       on c.trn_order_id = b.id  ';
            $sql .= '     left join trn_edi_shippings as d  ';
            $sql .= '       on d.trn_edi_receiving_id = c.id  ';
            $sql .= '     left join trn_edi_shipping_headers as e  ';
            $sql .= '       on e.id = d.trn_edi_shipping_header_id  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' . join(',',$this->request->data['d2promises']['trn_order_header_id']) . ') ';
            $sql .= '   group by ';
            $sql .= '     e.id ';
            
            $ret = $this->TrnEdiShipping->query($sql);
            foreach($ret as $r){
                $cond[] = $r[0]['id'];
            }
        }
        $where = null;

        $res = $this->getD2ReportData($cond , $where);
        $columns = array(
            'タイトル',
            '納品先名',
            '納品番号',
            '納品先部署名',
            '施設名',
            '納品日',
            '納品先住所',
            '施設住所',
            '行番号',
            '商品名',
            '規格',
            '製品番号',
            '定価',
            '償還価格',
            '保険請求名',
            '販売元',
            'ロット番号',
            '有効期限',
            '備考',
            '医事コード',
            '包装単位',
            '管理区分',
            '商品ID',
            '単価',
            '金額',
            '印刷年月日',
            'ヘッダ備考',
            '診療科',
            '手術名',
            '丸め区分',
            '削除フラグ',
            '改ページ番号'
        );
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('sortkey' =>'納品番号,改ページ番号');

        $this->xml_output($res,join("\t",$columns),$layout_name['40'],$fix_value);
    }

    private function getReportData($cond, $where = null ){
        $sql  = ' select ';
        $sql .= '     a.work_no ';
        $sql .= "   , to_char(a.work_date, 'YYYY年mm月dd日')                                                     as work_date ";
        $sql .= '   , a.recital                                                                                  as header_recital';
        $sql .= '   , b.recital ';
        $sql .= "   , ( case when d.spare_key2 <> '' then d.internal_code || '/' || d.spare_key2 ";
        $sql .= '     else d.internal_code ';
        $sql .= '     end ) ';
        $sql .= '   , d.item_name ';
        $sql .= '   , d.item_code ';
        $sql .= '   , d.standard ';
        $sql .= '   , e.dealer_name ';
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when c.per_unit = 1  ';
        $sql .= '       then c1.unit_name  ';
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')' ";
        $sql .= '       end ';
        $sql .= '   )                                                                                            as unit_name ';
        $sql .= '   , b.quantity ';
        $sql .= '   , b.lot_no ';
        $sql .= "   , to_char(b.validated_date, 'YYYY/mm/dd')                                                    as validated_date ";
        $sql .= '   , b.is_deleted ';
        $sql .= '   , j2.stocking_price                                                                          as price ';
        $sql .= '   , i.facility_formal_name                                                                     as to_facility_name ';
        $sql .= "   , '〒' || i.zip || ' ' || i.address || '\nTel ' || i.tel || '／Fax ' || i.fax                as to_address ";
        $sql .= '   , g.facility_formal_name                                                                     as from_facility_name ';
        $sql .= '   , h.department_formal_name                                                                   as to_department_name ';
        $sql .= "   , '〒' || g.zip || ' ' || g.address || '\nTel ' || g.tel || ' ／Fax ' || g.fax               as from_address ";
        $sql .= '   , i.round                                                                                    as to_round ';
        $sql .= '   , i.gross                                                                                    as to_gross  ';
        $sql .= '   , ( case ';
        foreach( Configure::read('OrderTypes.orderTypeShortName') as $k=>$v){
        $sql .= '       when l.order_type = ' . $k . " then '" . $v . "'";
        }
        $sql .= "      else '' ";
        $sql .= '      end )                                                                                     as type_name';
        $sql .= ' from ';
        $sql .= '   trn_edi_shipping_headers as a ';
        $sql .= '   left join trn_edi_shippings as b ';
        $sql .= '     on b.trn_edi_shipping_header_id = a.id ';
        $sql .= '   left join mst_item_units as c ';
        $sql .= '     on c.id = b.mst_item_unit_id ';
        $sql .= '   left join mst_unit_names as c1 ';
        $sql .= '     on c1.id = c.mst_unit_name_id ';
        $sql .= '   left join mst_unit_names as c2 ';
        $sql .= '     on c2.id = c.per_unit_name_id ';
        $sql .= '   left join mst_facility_items as d ';
        $sql .= '     on d.id = c.mst_facility_item_id ';
        $sql .= '   left join mst_dealers as e  ';
        $sql .= '     on e.id = d.mst_dealer_id  ';
        $sql .= '   left join mst_departments as f  ';
        $sql .= '     on f.id = b.department_id_from  ';
        $sql .= '   left join mst_facilities as g  ';
        $sql .= '     on g.id = f.mst_facility_id  ';
        $sql .= '   left join mst_departments as h  ';
        $sql .= '     on h.id = b.department_id_to  ';
        $sql .= '   left join mst_facilities as i  ';
        $sql .= '     on i.id = h.mst_facility_id  ';
        $sql .= '   left join trn_stickers as j ';
        $sql .= '     on j.trn_edi_shipping_id = b.id  ';
        $sql .= '   left join trn_storages as j1 ';
        $sql .= '     on j1.id = j.trn_storage_id ';
        $sql .= '   left join trn_receivings as j2 ';
        $sql .= '     on j2.id = j1.trn_receiving_id ';
        $sql .= '   left join trn_edi_receivings as k ';
        $sql .= '     on k.id = b.trn_edi_receiving_id ';
        $sql .= '   left join trn_orders as l ';
        $sql .= '     on l.id = k.trn_order_id ';
        $sql .= ' where 1=1';
        if(!empty($cond)){
            if(is_array($cond)){
                $sql .= '   and a.id in (' . join(',', $cond) . ') ';
            }else{
                $sql .= '   and a.id = ' . $cond;
            }
        }
        if(!is_null($where)){
            $sql .= $where ;
        }
        $sql .= ' group by ';
        $sql .= '     a.work_no ';
        $sql .= "   , a.work_date";
        $sql .= '   , a.recital';
        $sql .= '   , b.recital ';
        $sql .= "   , d.spare_key2 ";
        $sql .= "   , d.internal_code ";
        $sql .= '   , d.item_name ';
        $sql .= '   , d.item_code ';
        $sql .= '   , d.standard ';
        $sql .= '   , e.dealer_name ';
        $sql .= '   , c.per_unit ';
        $sql .= '   , c1.unit_name ';
        $sql .= '   , c2.unit_name ';
        $sql .= '   , b.quantity ';
        $sql .= '   , b.lot_no ';
        $sql .= "   , b.validated_date ";
        $sql .= '   , b.is_deleted ';
        $sql .= '   , j2.stocking_price ';
        $sql .= '   , i.facility_formal_name ';
        $sql .= "   , i.zip , i.address , i.tel , i.fax ";
        $sql .= '   , g.facility_formal_name ';
        $sql .= '   , h.department_formal_name ';
        $sql .= "   , g.zip ,  g.address , g.tel , g.fax ";
        $sql .= '   , i.round ';
        $sql .= '   , i.gross ';
        $sql .= '   , l.order_type ';
        $sql .= '   order by a.work_no  ';
        $sql .= '   , e.dealer_name ';
        $sql .= '   , d.item_name ';
        $sql .= '   , d.item_code ';

        $res = $this->TrnEdiShipping->query($sql);
        
        $before_work_no = '';
        $results = array();
        foreach($res as $row){
            $r = $row[0];
            if($r['work_no'] != $before_work_no){
                $i = 1;
                $before_work_no = $r['work_no'];
            }

            $results[] = array(
                $r['to_facility_name'],            //納品先名
                $r['work_no'],                     //納品番号
                $r['to_department_name'] ,         //納品先部署名
                $r['from_facility_name'] ,         //施設名
                $r['work_date'] ,                  //納品日
                $r['to_address'],                  //納品先住所
                $r['from_address'] ,               //施設住所
                $i,                                //行番号
                $r['item_name'],                   //商品名
                $r['standard'] ,                   //規格
                $r['item_code'] ,                  //製品番号
                $r['dealer_name'] ,                //販売元
                $r['lot_no'],                      //ロット番号
                $r['validated_date'],              //有効期限
                $r['recital'],                     //明細備考
                $r['quantity'] . $r['unit_name'] , //包装単位
                $r['type_name'] ,                  //管理区分
                $r['internal_code'] ,              //商品ID
                $r['price'],                       //単価,
                $r['price'] * $r['quantity'] ,     //金額
                date('y/m/d'),                     //印刷年月日
                $r['header_recital'] ,             //ヘッダ備考
                $r['to_gross'],                    //丸め区分
                $r['is_deleted'],                  //削除フラグ
                $r['work_no']                      //改ページ番号
                );
            $i++;
        }
        return $results;
    }
    
    private function getD2ReportData($cond, $where = null ){
        $sql  = ' select ';
        $sql .= '     a.work_no ';
        $sql .= "   , to_char(a.work_date, 'YYYY年mm月dd日')                                                     as work_date ";
        $sql .= '   , a.recital                                                                                  as header_recital';
        $sql .= '   , a.spare_recital1';
        $sql .= '   , a.spare_recital2';
        $sql .= '   , b.recital ';
        $sql .= "   , ( case when d.spare_key2 <> '' then d.internal_code || '/' || d.spare_key2 ";
        $sql .= '     else d.internal_code ';
        $sql .= '     end ) ';
        $sql .= '   , d.item_name ';
        $sql .= '   , d.item_code ';
        $sql .= '   , d.standard ';
        $sql .= '   , d.unit_price ';
        $sql .= '   , d.refund_price ';
        $sql .= '   , d2.insurance_claim_name_s ';
        $sql .= '   , e.dealer_name ';
        $sql .= '   , (  ';
        $sql .= '     case  ';
        $sql .= '       when c.per_unit = 1  ';
        $sql .= '       then c1.unit_name  ';
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')' ";
        $sql .= '       end ';
        $sql .= '   )                                                                                            as unit_name ';
        $sql .= '   , b.quantity ';
        $sql .= '   , b.lot_no ';
        $sql .= "   , to_char(b.validated_date, 'YYYY/mm/dd')                                                    as validated_date ";
        $sql .= '   , b.is_deleted ';
        $sql .= '   , j2.stocking_price                                                                          as price ';
        $sql .= '   , i.facility_formal_name                                                                     as to_facility_name ';
        $sql .= "   , '〒' || i.zip || ' ' || i.address || '\nTel ' || i.tel || '／Fax ' || i.fax                as to_address ";
        $sql .= '   , g.facility_formal_name                                                                     as from_facility_name ';
        $sql .= '   , h.department_formal_name                                                                   as to_department_name ';
        $sql .= "   , '〒' || g.zip || ' ' || g.address || '\nTel ' || g.tel || ' ／Fax ' || g.fax               as from_address ";
        $sql .= '   , i.round                                                                                    as to_round ';
        $sql .= '   , i.gross                                                                                    as to_gross  ';
        $sql .= '   , ( case ';
        foreach( Configure::read('OrderTypes.orderTypeShortName') as $k=>$v){
        $sql .= '       when l.order_type = ' . $k . " then '" . $v . "'";
        }
        $sql .= "      else '' ";
        $sql .= '      end )                                                                                     as type_name';
        $sql .= '    , d.medical_code';
        $sql .= ' from ';
        $sql .= '   trn_edi_shipping_headers as a ';
        $sql .= '   left join trn_edi_shippings as b ';
        $sql .= '     on b.trn_edi_shipping_header_id = a.id ';
        $sql .= '   left join mst_item_units as c ';
        $sql .= '     on c.id = b.mst_item_unit_id ';
        $sql .= '   left join mst_unit_names as c1 ';
        $sql .= '     on c1.id = c.mst_unit_name_id ';
        $sql .= '   left join mst_unit_names as c2 ';
        $sql .= '     on c2.id = c.per_unit_name_id ';
        $sql .= '   left join mst_facility_items as d ';
        $sql .= '     on d.id = c.mst_facility_item_id ';
        $sql .= '   left join mst_insurance_claims as d2';
        $sql .= '     on d2.insurance_claim_code = d.insurance_claim_code';
        $sql .= '   left join mst_dealers as e  ';
        $sql .= '     on e.id = d.mst_dealer_id  ';
        $sql .= '   left join mst_departments as f  ';
        $sql .= '     on f.id = b.department_id_from  ';
        $sql .= '   left join mst_facilities as g  ';
        $sql .= '     on g.id = f.mst_facility_id  ';
        $sql .= '   left join mst_departments as h  ';
        $sql .= '     on h.id = b.department_id_to  ';
        $sql .= '   left join mst_facilities as i  ';
        $sql .= '     on i.id = h.mst_facility_id  ';
        $sql .= '   left join trn_stickers as j ';
        $sql .= '     on j.trn_edi_shipping_id = b.id  ';
        $sql .= '   left join trn_storages as j1 ';
        $sql .= '     on j1.id = j.trn_storage_id ';
        $sql .= '   left join trn_receivings as j2 ';
        $sql .= '     on j2.id = j1.trn_receiving_id ';
        $sql .= '   left join trn_edi_receivings as k ';
        $sql .= '     on k.id = b.trn_edi_receiving_id ';
        $sql .= '   left join trn_orders as l ';
        $sql .= '     on l.id = k.trn_order_id ';
        $sql .= ' where 1=1';
        if(!empty($cond)){
            if(is_array($cond)){
                $sql .= '   and a.id in (' . join(',', $cond) . ') ';
            }else{
                $sql .= '   and a.id = ' . $cond;
            }
        }
        if(!is_null($where)){
            $sql .= $where ;
        }
        $sql .= '   order by a.work_no  ';
        $sql .= '   , e.dealer_name ';
        $sql .= '   , d.item_name ';
        $sql .= '   , d.item_code ';

        $res = $this->TrnEdiShipping->query($sql);
        
        $results = array();
        $title_array = array('1'=>'使用部署控' , '2'=>'事務課控' , '3'=>'SPD控');
        
        foreach($title_array as $k => $v){
            $before_work_no = '';
            foreach($res as $row){
                $r = $row[0];
                if($r['work_no'] . '_'.$k != $before_work_no){
                    $i = 1;
                    $before_work_no = $r['work_no'] . '_' . $k;
                }
                
                $results[] = array(
                    '出荷伝票（' . $v . '）',
                    $r['to_facility_name'],            //納品先名
                    $r['work_no'],                     //納品番号
                    $r['to_department_name'] ,         //納品先部署名
                    $r['from_facility_name'] ,         //施設名
                    $r['work_date'] ,                  //納品日
                    $r['to_address'],                  //納品先住所
                    $r['from_address'] ,               //施設住所
                    $i,                                //行番号
                    $r['item_name'],                   //商品名
                    $r['standard'] ,                   //規格
                    $r['item_code'] ,                  //製品番号
                    $r['unit_price'] ,                 //定価
                    $r['refund_price'],                //償還価格
                    $r['insurance_claim_name_s'],      //保険請求名
                    $r['dealer_name'] ,                //販売元
                    $r['lot_no'],                      //ロット番号
                    $r['validated_date'],              //有効期限
                    $r['recital'],                     //明細備考
                    $r['medical_code'],                //医事コード
                    $r['quantity'] . $r['unit_name'] , //包装単位
                    $r['type_name'] ,                  //管理区分
                    $r['internal_code'] ,              //商品ID
                    $r['price'],                       //単価,
                    $r['price'] * $r['quantity'] ,     //金額
                    date('y/m/d'),                     //印刷年月日
                    $r['header_recital'] ,             //ヘッダ備考
                    $r['spare_recital1'],              //診療科
                    $r['spare_recital2'],              //手術名
                    $r['to_gross'],                    //丸め区分
                    $r['is_deleted'],                  //削除フラグ
                    $before_work_no                    //改ページ番号
                    );
                $i++;
            }
        }
        return $results;
    }

    function seal($type=''){
        if($type=='hospital'){
            //部署シール印字（確定画面）
            $_hno = array();
            
            $sql  = ' select ';
            $sql .= '     d.hospital_sticker_no  ';
            $sql .= ' from ';
            $sql .= '   trn_edi_shippings as a  ';
            $sql .= '   left join trn_stickers as d ';
            $sql .= '     on d.trn_edi_shipping_id = a.id';
            $sql .= ' where ';
            $sql .= '   a.trn_edi_shipping_header_id in ( ' . join(',',$this->request->data['Edi']['id']). ')';
            $sql .= "   and d.hospital_sticker_no != '' ";
            
            $result = $this->TrnEdiShipping->query($sql , false , false );
            foreach($result as $r){
                $_hno[] = $r[0]['hospital_sticker_no'];
            }
            
            if($_hno == array()){
                $data = array();
            }else{
                $order = 'MstDepartment.department_name asc, MstShelfNameTo.code asc';
                $data = $this->Stickers->getHospitalStickers($_hno, $order,3);
            }
            $this->set('sortkey' , '病院情報,センター棚番号,商品ID');
            $this->set('Sticker' , $data);
            $this->layout = 'xml/default';
            $this->render('/stickerissues/hospital_sticker');
            
        }elseif($type=='hospital_history'){
            //部署シール印字（履歴画面）
            $_hno = array();
            
            $sql  = ' select ';
            $sql .= '     d.hospital_sticker_no  ';
            $sql .= ' from ';
            $sql .= '   trn_edi_shippings as a  ';
            $sql .= '   left join trn_stickers as d ';
            $sql .= '     on d.trn_edi_shipping_id = a.id';
            $sql .= ' where ';
            $sql .= '   a.id in ( ' . join(',',$this->request->data['Edi']['id']). ')';
            $sql .= "   and d.hospital_sticker_no != ''";
            
            $result = $this->TrnEdiShipping->query($sql , false , false );
            foreach($result as $r){
                $_hno[] = $r[0]['hospital_sticker_no'];
            }
            if($_hno == array()){
                $data = array();
            }else{
                $order = 'MstDepartment.department_name asc, MstShelfNameTo.code asc';
                $data = $this->Stickers->getHospitalStickers($_hno, $order,3);
            }
            $this->set('sortkey' , '病院情報,センター棚番号,商品ID');
            $this->set('Sticker' , $data);
            $this->layout = 'xml/default';
            $this->render('/stickerissues/hospital_sticker');

        }elseif($type=='cost'){
            //コストシール印字（確定画面）
            $records = $this->Stickers->getCostStickers($this->request->data['Edi']['trn_order_id'],6);
            // 空なら初期化
            if(!$records){
                $records = array();
            }
            $this->layout = 'xml/default';
            $this->set('records', $records);
            $this->render('/stickerissues/cost_sticker');
        }elseif($type=='cost_history'){
            //コストシール印字（履歴画面）
            $sql  = ' select ';
            $sql .= '     c.id  ';
            $sql .= ' from ';
            $sql .= '   trn_edi_shippings as a  ';
            $sql .= '   left join trn_edi_receivings as b  ';
            $sql .= '     on a.trn_edi_receiving_id = b.id  ';
            $sql .= '   left join trn_orders as c  ';
            $sql .= '     on c.id = b.trn_order_id  ';
            $sql .= ' where ';
            $sql .= '   a.id in ( ' . join(',',$this->request->data['Edi']['id']). ')';
            $result = $this->TrnEdiShipping->query($sql , false , false );
            foreach($result as $r){
                $ids[] = $r[0]['id'];
            }
            $records = $this->Stickers->getCostStickers($ids,6);
            // 空なら初期化
            if(!$records){
                $records = array();
            }
            $this->layout = 'xml/default';
            $this->set('records', $records);
            $this->render('/stickerissues/cost_sticker');
        }
    }

    private function __getFacilityList($id , $field = array('id' , 'facility_name') ){
        list($a , $b) = $field;
        $sql  = ' select ';
        $sql .= "       d.{$a} as \"MstFacility__{$a}\" ";
        $sql .= "     , d.{$b} as \"MstFacility__{$b}\" ";
        $sql .= '   from ';
        $sql .= '     mst_facility_relations as a  ';
        $sql .= '     left join mst_facilities as b  ';
        $sql .= '       on b.id = a.mst_facility_id  ';
        $sql .= '     left join mst_facility_relations as c  ';
        $sql .= '       on c.mst_facility_id = b.id  ';
        $sql .= '       or c.partner_facility_id = b.id  ';
        $sql .= '     left join mst_facilities as d  ';
        $sql .= '       on d.id = c.mst_facility_id  ';
        $sql .= '       or d.id = c.partner_facility_id  ';
        $sql .= '   where ';
        $sql .= '     a.partner_facility_id = ' . $id;
        $sql .= '     and d.facility_type in ( ' . Configure::read('FacilityType.center') .  ' , ' . Configure::read('FacilityType.hospital') . ')  ';
        $sql .= '     and c.is_deleted = false  ';
        $sql .= '     and d.is_deleted = false  ';
        $sql .= '   group by ';
        $sql .= '     d.id ';
        $sql .= '     , d.facility_code ';
        $sql .= '     , d.facility_name  ';
        $sql .= '   order by ';
        $sql .= '     d.facility_code ';
        
        $list = Set::Combine(
            $this->MstFacility->query($sql),
            "{n}.MstFacility.{$a}",
            "{n}.MstFacility.{$b}"
            );
        return $list;
        
    }
    
    /**
     * 直納品条件入力画面
     */
    public function d2promises_add() {
        $this->setRoleFunction(122); //直納品登録

        //得意先
        $this->set('hospital_list',$this->getClientList());
    }

    /**
     * シール読み込み
     */
    public function d2promises_read_sticker(){
        //直納品条件入力の入力データをセッションに保持
        $this->Session->write('input_D2customer', $this->request->data['d2promises']);
        
        //得意先コードから施設IDを取得
        $this->request->data['d2promises']['hospitalId'] = $this->getFacilityId($this->request->data['d2promises']['hospitalCode'], array(Configure::read('FacilityType.hospital')));
        //得意先部署コードから部署IDを取得
        $this->request->data['d2promises']['departmentId'] = $this->getDepartmentId( $this->request->data['d2promises']['hospitalId'], array(Configure::read('DepartmentType.hospital')) , $this->request->data['d2promises']['departmentCode']);
        //仕入先IDを取得
        $this->request->data['d2promises']['supplierId'] = $this->Session->read('Auth.facility_id_selected');

        // 病院部署IDから、センターIDを取得
        $sql  = ' select ';
        $sql .= '     b.mst_facility_id  as "centerId", ';
        $sql .= '     c.id               as "centerDepartmentId" ';
        $sql .= ' from ';
        $sql .= '   mst_departments as a  ';
        $sql .= '   left join mst_facility_relations as b  ';
        $sql .= '     on b.partner_facility_id = a.mst_facility_id  ';
        $sql .= '   left join mst_departments as c  ';
        $sql .= '     on c.mst_facility_id = b.mst_facility_id  ';
        $sql .= ' where ';
        $sql .= '   a.id = ' . $this->request->data['d2promises']['departmentId'];
        $ret = $this->MstDepartment->query($sql);
        $this->request->data['d2promises']['centerId'] = $ret[0][0]['centerId'];
        $this->request->data['d2promises']['centerDepartmentId'] = $ret[0][0]['centerDepartmentId'];
        
        $this->set('data' , $this->request->data);
        $this->render('d2promises_read_sticker');
    }

    /**
     * シール読み込み確認画面
     */
    function d2promises_read_confirm(){
        $ean = array();
        $result = array();
        foreach(explode(',',$this->request->data['codez']) as $barcode){
            $ean[] = $this->Barcode->readEAN128($barcode,$this->request->data['d2promises']['centerId']);
        }

        foreach($ean as $e){
            $ret = array();
            $data = isset($e['MstFacilityItem'][0])?$e['MstFacilityItem'][0]:array();
            if(isset($data['MstFacilityItem']['id'])){
                $ret['MstFacilityItem']['internal_code']               = $data['MstFacilityItem']['internal_code'];
                $ret['MstFacilityItem']['item_name']                   = $data['MstFacilityItem']['item_name'];
                $ret['MstFacilityItem']['item_code']                   = $data['MstFacilityItem']['item_code'];
                $ret['MstFacilityItem']['standard']                    = $data['MstFacilityItem']['standard'];
                $ret['MstFacilityItem']['class_separation']            = $data['MstFacilityItem']['class_separation'];
                $ret['MstFacilityItem']['is_lowlevel']                 = $data['MstFacilityItem']['is_lowlevel'];
                $ret['MstFacilityItem']['lot_no']                      = $e['lot_no'];
                $ret['MstFacilityItem']['validated_date']              = $this->getFormatedValidateDate($e['validated_date']);
                $itemUnit = $this->getItemUnit($data['MstFacilityItem']['id'],
                                               $this->request->data['d2promises']['supplierId'],
                                               $this->request->data['d2promises']['hospitalId'],
                                               $this->request->data['d2promises']['work_date']
                                               );
                if(!empty($itemUnit)){
                    $ret['MstFacilityItem']['item_unit'] = Set::Combine(
                        $itemUnit,
                        "{n}.MstItemUnit.id",
                        "{n}.MstItemUnit.unit_name"
                        );
                    $ret['MstFacilityItem']['sales_price']       = $itemUnit[0]['MstItemUnit']['sales_price'];
                    $ret['MstFacilityItem']['transaction_price'] = $itemUnit[0]['MstItemUnit']['transaction_price'];
                    $ret['MstFacilityItem']['check']             = true;
                }else{
                    $ret['MstFacilityItem']['item_unit']         = array();
                    $ret['MstFacilityItem']['sales_price']       = '';
                    $ret['MstFacilityItem']['transaction_price'] = '';
                    $ret['MstFacilityItem']['check']             = false;
                }
                $ret['MstFacilityItem']['dealer_name']    = $data['MstDealer']['dealer_name'];
            }else{
                $ret['MstFacilityItem']['internal_code']     = '該当商品がありません';
                $ret['MstFacilityItem']['item_name']         = '';
                $ret['MstFacilityItem']['item_code']         = '';
                $ret['MstFacilityItem']['standard']          = '';
                $ret['MstFacilityItem']['class_separation']  = '';
                $ret['MstFacilityItem']['is_lowlevel']       = '';
                $ret['MstFacilityItem']['lot_no']            = '';
                $ret['MstFacilityItem']['validated_date']    = '';
                $ret['MstFacilityItem']['item_unit']         = array();
                $ret['MstFacilityItem']['dealer_name']       = '';
                $ret['MstFacilityItem']['sales_price']       = '';
                $ret['MstFacilityItem']['transaction_price'] = '';
                $ret['MstFacilityItem']['check']             = false;
            }
            $result[] = $ret;
        }
        $this->set('result' , $result);
        $this->set('class_list', $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'18'));

        /* トランザクショントークン */
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('d2promises.token' , $token);
        $this->request->data['d2promises']['token'] = $token;
        
    }

    
    /**
     * 採用品選択
     * @param
     * @return
     */
    public function d2promises_item_select() {
        App::import('Sanitize');

        $SearchResult = array();
        $CartSearchResult = array();

        //初回時は検索を行わない
        if (isset($this->request->data['add_search']['is_search'])) {
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $limit = '' ;
            if(isset($this->request->data['FacilityItems']['limit'])){
                $limit .= 'limit ' . $this->request->data['FacilityItems']['limit'];
            }
            $order =' order by a.internal_code ,b.per_unit ';
            $where  ='';
            $where .=' and a.mst_facility_id = ' . $this->request->data['d2promises']['centerId'] ;
            $where .=' and a.item_type = ' . Configure::read('Items.item_types.normalitem'); // 通常品のみ
            $where .=' and a.is_deleted = false '; //施設採用品で削除されているものは除外
            $where .=' and b.is_deleted = false '; //包装単位で適用が外れているものは除外
            $where .=' and a.is_lowlevel = false '; // 低レベル品は除外

            $where .=" and a.start_date <= '" . $this->request->data['d2promises']['work_date'] . "'"; //施設採用品開始日
            $where .=" and ( a.end_date >= '" . $this->request->data['d2promises']['work_date'] . "'"; //施設採用品終了日
            $where .=' or a.end_date is null ) ';

            $where .=" and b.start_date <= '" . $this->request->data['d2promises']['work_date'] . "'"; //包装単位開始日
            $where .=" and ( b.end_date >= '" . $this->request->data['d2promises']['work_date'] . "'"; //包装単位終了日
            $where .=' or b.end_date is null ) ';

            $where2 = $where;

            if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and b.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 .= " and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 .= " and 0 = 1 ";
            }

            if(isset($this->request->data['MstFacilityItems']['internal_code']) && $this->request->data['MstFacilityItems']['internal_code'] !== ''){
                $where .=" and a.internal_code ilike '%" .$this->request->data['MstFacilityItems']['internal_code']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['standard']) && $this->request->data['MstFacilityItems']['standard'] !== '' ){
                $where .=" and a.standard ilike '%" .$this->request->data['MstFacilityItems']['standard']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['item_name']) && $this->request->data['MstFacilityItems']['item_name'] !== ''){
                $where .=" and a.item_name ilike '%" .$this->request->data['MstFacilityItems']['item_name']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['item_code']) && $this->request->data['MstFacilityItems']['item_code'] !== ''){
                $where .=" and a.item_code ilike '%" .$this->request->data['MstFacilityItems']['item_code']."%'";
            }
            if(isset($this->request->data['MstFacilityItems']['dealer_name']) && $this->request->data['MstFacilityItems']['dealer_name'] !== ''){
                $where .=" and c.dealer_name ilike '%" .$this->request->data['MstFacilityItems']['dealer_name']."%'";
            }
            //JAN(前方一致)
            if(isset($this->request->data['MstFacilityItems']['jan_code']) && $this->request->data['MstFacilityItems']['jan_code'] !== ''){
                $where .=" and a.jan_code ilike '" .$this->request->data['MstFacilityItems']['jan_code']."%'";
            }

            $where .=' and f.id is not null ';
            $where .=' and g.id is not null ';

            // 検索（画面上）用一覧取得
            $SearchResult = $this->MstFacilityItem->query($this->searchFacilityItems($where , $order , $limit , false), false, false);

            // カート（画面下）用一覧取得
            $CartSearchResultTmp = $this->MstFacilityItem->query($this->searchFacilityItems($where2 , $order , null , false) , false , false);

            if(!empty($this->request->data['cart']['tmpid'])){
                //カートに追加した順に並べなおす。
                foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
                    foreach($CartSearchResultTmp as $tmp){
                        if($tmp['MstFacilityItems']['id'] == $id){
                            $CartSearchResult[] = $tmp;
                            break;
                        }
                    }
                }
            }
            $this->request->data = $data;
            $this->set('SearchResult' , $SearchResult);
            $this->set('CartSearchResult' , $CartSearchResult);
            $this->set('data' , $this->request->data);
        } else {
            //直納品条件入力の入力データをセッションに保持
            $this->Session->write('input_D2promises', $this->request->data['d2promises']);
            
            //得意先コードから施設IDを取得
            $this->request->data['d2promises']['hospitalId'] = $this->getFacilityId($this->request->data['d2promises']['hospitalCode'], array(Configure::read('FacilityType.hospital')));
            //得意先部署コードから部署IDを取得
            $this->request->data['d2promises']['departmentId'] = $this->getDepartmentId( $this->request->data['d2promises']['hospitalId'], array(Configure::read('DepartmentType.hospital')) , $this->request->data['d2promises']['departmentCode']);
            //仕入先IDを取得
            $this->request->data['d2promises']['supplierId'] = $this->Session->read('Auth.facility_id_selected');
            
            // 病院部署IDから、センターIDを取得
            $sql  = ' select ';
            $sql .= '     b.mst_facility_id  as "centerId", ';
            $sql .= '     c.id               as "centerDepartmentId" ';
            $sql .= ' from ';
            $sql .= '   mst_departments as a  ';
            $sql .= '   left join mst_facility_relations as b  ';
            $sql .= '     on b.partner_facility_id = a.mst_facility_id  ';
            $sql .= '   left join mst_departments as c  ';
            $sql .= '     on c.mst_facility_id = b.mst_facility_id  ';
            $sql .= ' where ';
            $sql .= '   a.id = ' . $this->request->data['d2promises']['departmentId'];
            $ret = $this->MstDepartment->query($sql);
            $this->request->data['d2promises']['centerId'] = $ret[0][0]['centerId'];
            $this->request->data['d2promises']['centerDepartmentId'] = $ret[0][0]['centerDepartmentId'];

            $this->set('data' , $this->request->data);
        }
    }

    /**
     * 直納品登録確認画面
     */
    public function d2promises_add_confirm() {
        $where = ' and b.id in ( ' . $this->request->data['cart']['tmpid'] . ')';
        $FacilityItemListTmp = $this->MstFacilityItem->query($this->searchFacilityItems($where , null , null , false), false, false);

        //カートに追加した順に並べなおす。
        foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
            foreach($FacilityItemListTmp as $tmp){
                if($tmp['MstFacilityItems']['id'] == $id){
                    $FacilityItemList[] = $tmp;
                    break;
                }
            }
        }
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('d2promises.token' , $token);
        $this->request->data['d2promises']['token'] = $token;

        $this->set('FacilityItemList', $FacilityItemList);
    }

    /**
     * 直納品登録
     */
    public function d2promises_add_result(){
        if($this->request->data['d2promises']['token'] === $this->Session->read('d2promises.token')) {
            $this->Session->delete('d2promises.token');

            $facility_type = Configure::read('FacilityType.supplier');
            $useNumberType = Configure::read('NumberType.WorkNo_EdiD2Promise'); // 業者直納品

            // 各データ作成で利用する共通情報
            $base = array_merge(
                array(
                    'work_no'      => $this->setWorkNo4Header(
                                        date('Ymd'),
                                        $useNumberType,
                                        $this->request->data['d2promises']['centerId']
                                      ),
                    'toId'         => $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'), $facility_type),
                    'toId2'        => $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'), $facility_type),
                    'detail_count' => count($this->request->data['d2promises']['checked']),
                    'now'          => date('Y/m/d H:i:s')
                ),
                $this->request->data['d2promises']
            );

            $this->TrnOrderHeader->begin();

            try {
                // 各ヘッダ作成 関数内で作成するので、作成したIDを受け取る
                $base['trn_order_header_id']         = $this->createD2TrnOrderHeader($base);          // 発注ヘッダ
                $base['trn_edi_receiving_header_id'] = $this->createD2TrnEdiReceivingHeader($base);   // 受注ヘッダ
                $base['trn_edi_shipping_header_id']  = $this->createD2TrnEdiShippingHeader($base);    // 業者出荷ヘッダ
                $base['trn_receiving_header_id']     = $this->createD2TrnReceivingHeader($base);      // 入荷ヘッダ
                $base['trn_storage_header_id']       = $this->createD2TrnStorageHeader($base);        // 入庫ヘッダ
                $base['trn_shipping_header_id']      = $this->createD2TrnShippingHeader(
                    $base, $this->request->data['d2items']);// 出荷ヘッダ（センタ）

                // 各明細作成 関数内で作成するので、作成したIDなどを受け取る
                $cnt = 1;
                foreach($this->request->data['d2promises']['checked'] as $line){
                    $detailData = array_merge($base, $this->request->data['d2items'][$line]);
                    $detailData['supplierId'] = $this->Session->read('Auth.facility_id_selected');
                    $detailData['work_seq']             = $cnt++;
                    $detailData['trn_order_id']         = $this->createD2TrnOrder($detailData);        // 発注明細
                    $detailData['trn_edi_receiving_id'] = $this->createD2TrnEdiReceiving($detailData); // 受注明細
                    $detailData['trn_edi_shipping_id']  = $this->createD2TrnEdiShipping($detailData);  // 業者出荷明細
                    $detailData['trn_receiving_id']     = $this->createD2TrnReceiving($detailData);    // 入荷明細

                    $etcData                            = $this->createD2TrnStorage($detailData);      // シール、入庫明細
                    $detailData['trn_sticker_id']       = $etcData['trn_sticker_id'];
                    $detailData['facility_sticker_no']  = $etcData['facility_sticker_no'];
                    $detailData['hospital_sticker_no']  = $etcData['hospital_sticker_no'];

                    $detailData['trn_shipping_id']      = $this->createD2TrnShipping($detailData);     // 出荷明細(センタ)
                    // シール情報更新
                    $this->updateD2TrnSticker($detailData);
                }
            } catch (Exception $ex) {
                Debugger::log($ex->getMessage());

                $this->TrnOrderHeader->rollback();
                $this->Session->setFlash('直納品登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }

            $this->TrnOrderHeader->commit();

            $FacilityItemList = array();
            foreach ($this->request->data['d2promises']['checked'] as $line) {
                $FacilityItemList[] = $this->request->data['d2items'][$line];
            }

            $this->request->data['Edi']['id'] = $base['trn_edi_shipping_header_id'];
            $this->set('FacilityItemList', $FacilityItemList);
            $this->Session->write('d2customers.FacilityItemList', $FacilityItemList);
            $this->set('centerId' , $this->request->data['d2promises']['centerId']);
            
        }else{
            // トランザクショントークン不正
            // リロード or ２度押し
            $this->set('FacilityItemList', $this->Session->read('d2customers.FacilityItemList'));
            $this->set('centerId' , $this->request->data['d2promises']['centerId']);
        }
    }

    /**
     * 直納品履歴
     */
    public function d2promises_history(){
        $this->setRoleFunction(123); //直納品履歴
        $result = array();
        $department_list = array();

        //得意先
        $this->set('facility_list', $this->getClientList());

        $sql  = ' select ';
        $sql .= ' a.mst_facility_id ';
        $sql .= ' from ';
        $sql .= '   mst_facility_relations as a  ';
        $sql .= ' where ';
        $sql .= '   a.partner_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '   and a.is_deleted = false ';
        
        $ret = $this->TrnEdiShipping->query($sql);
        
        $this->request->data['SplitTable']['center_facility_id'] = $ret[0][0]['mst_facility_id'];
        
        if(isset($this->request->data['d2promises']['facilityCode']) && $this->request->data['d2promises']['facilityCode']!=""){
            //施設が選択済みだったら部署一覧を取得
            $department_list = $this->getDepartmentsList($this->request->data['d2promises']['facilityCode'], false);
        }
        $this->set('department_list' , $department_list);

        if(isset($this->request->data['d2promises']['is_search'])){
            $where = "";
            // 絞り込み条件追加

            //作業番号
            if(isset($this->request->data['d2promises']['work_no']) && $this->request->data['d2promises']['work_no'] != "" ){
                $where .= " and a.work_no = '" . $this->request->data['d2promises']['work_no'] . "'";
            }

            //作業日From
            if(isset($this->request->data['d2promises']['work_date_from']) && $this->request->data['d2promises']['work_date_from'] != "" ){
                $where .= " and a.work_date >= '" . $this->request->data['d2promises']['work_date_from'] . "'";
            }

            //作業日To
            if(isset($this->request->data['d2promises']['work_date_to']) && $this->request->data['d2promises']['work_date_to'] != "" ){
                $where .= " and a.work_date <= '" . $this->request->data['d2promises']['work_date_to'] . "'";
            }

            //取消も表示する
            if(isset($this->request->data['d2promises']['is_deleted']) && $this->request->data['d2promises']['is_deleted'] != "1" ){
                $where .= " and a.is_deleted = false ";
                $where .= " and b.is_deleted = false ";
            }

            //仕入先２
            if(isset($this->request->data['d2promises']['supplierCode']) && $this->request->data['d2promises']['supplierCode'] != "" ){
                $where .= " and g2.facility_code = '" . $this->request->data['d2promises']['supplierCode'] . "'";
            }

            //備考
            if(isset($this->request->data['d2promises']['recital']) && $this->request->data['d2promises']['recital'] != "" ){
                $where .= " and a.recital like '%" . $this->request->data['d2promises']['recital'] . "%'";
            }

            //得意先
            if(isset($this->request->data['d2promises']['facilityCode']) && $this->request->data['d2promises']['facilityCode'] != "" ){
                $where .= " and g.facility_code = '" . $this->request->data['d2promises']['facilityCode'] . "'";
            }

            //部署
            if(isset($this->request->data['d2promises']['departmentCode']) && $this->request->data['d2promises']['departmentCode'] != "" ){
                $where .= " and f.department_code = '" . $this->request->data['d2promises']['departmentCode'] . "'";
            }

            //商品ID
            if(isset($this->request->data['d2promises']['internal_code']) && $this->request->data['d2promises']['internal_code'] != "" ){
                $where .= " and d.internal_code = '" . $this->request->data['d2promises']['internal_code'] . "'";
            }

            //製品番号
            if(isset($this->request->data['d2promises']['item_code']) && $this->request->data['d2promises']['item_code'] != "" ){
                $where .= " and d.item_code like '%" . $this->request->data['d2promises']['item_code'] . "%'";
            }

            //商品名
            if(isset($this->request->data['d2promises']['item_name']) && $this->request->data['d2promises']['item_name'] != "" ){
                $where .= " and d.item_name = '" . $this->request->data['d2promises']['item_name'] . "'";
            }

            //販売元
            if(isset($this->request->data['d2promises']['dealer_name']) && $this->request->data['d2promises']['dealer_name'] != "" ){
                $where .= " and h.dealer_name like '%" . $this->request->data['d2promises']['dealer_name'] . "%'";
            }

            //作業区分
            if(isset($this->request->data['d2promises']['work_type']) && $this->request->data['d2promises']['work_type'] != "" ){
                $where .= " and b.work_type = '" . $this->request->data['d2promises']['work_type'] . "'";
            }

            //規格
            if(isset($this->request->data['d2promises']['standard']) && $this->request->data['d2promises']['standard'] != "" ){
                $where .= " and d.standard like '%" . $this->request->data['d2promises']['standard'] . "%'";
            }

            //JANコード
            if(isset($this->request->data['d2promises']['jan_code']) && $this->request->data['d2promises']['jan_code'] != "" ){
                $where .= " and d.jan_code = '" . $this->request->data['d2promises']['jan_code'] . "'";
            }

            $result = $this->getD2promisesHistory($where , $this->request->data['limit']);
        }else{
            // 初回のみ
            $this->request->data['d2promises']['work_date_from'] = date('Y/m/d' , strtotime("-7 day"));
            $this->request->data['d2promises']['work_date_to'] = date('Y/m/d');
        }

        $this->set('result', $result);
    }

    /**
     * 直納品履歴編集
     */
    public function d2promises_history_confirm(){
        // 選択ID 一覧からはTrnOrderHeaderのIDが来る
        $where = " and a.id in ( " . implode(',', $this->request->data['d2promises']['trn_order_header_id']) . ") ";
        if (empty($this->request->data['d2promises']['is_deleted'])) {
            $where .= " and m.is_deleted = false ";
        }

        $result = $this->getD2promisesHistoryDetail($where);
        $this->set('result', $result);

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('d2promises.history.token' , $token);
        $this->request->data['d2promises']['history']['token'] = $token;
    }

    /**
     * 直納品履歴結果
     */
    public function d2promises_history_result(){
        if($this->request->data['d2promises']['history']['token'] === $this->Session->read('d2promises.history.token')) {
            $this->Session->delete('d2promises.history.token');
            $now = date('Y/m/d H:i:s');

            // 取消対象のシールID
            $cancel = $this->request->data['d2promises']['id'];
            if (count($cancel) == 0) {
                // 取消対象が無かった場合
                $this->Session->setFlash('直納品取消対象がありませんでした。', 'growl', array('type'=>'error') );
                $this->redirect('d2promises_history');
                return;
             }

            $this->Vesta->begin();

            try {
                $headers = array();

                // シールと一対になっている明細の削除
                $ret = $this->delD2DetailInSeal($cancel);
                if($ret == false){
                    $this->Vesta->rollback();
                    $this->Session->setFlash('直納品取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('d2promises_history');
                }
                
                // 数量調整
                foreach ($ret as $detail) {
                    // 有効な数量を取得
                    $from  = ' trn_stickers as a ';
                    $from .= ' left join trn_shippings as b on a.id = b.trn_sticker_id ';
                    $where = ' a.trn_order_id = ' . $detail['trn_order_id'] . ' and b.is_deleted = false';
                    $detail['quantity'] = $this->Vesta->getRecordCount($from, array('where' => $where));

                    $detail_ret = $this->updateD2DetailQuantity($detail);
                    if(!$detail_ret){
                        $this->Vesta->rollback();
                        $this->Session->setFlash('直納品取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('d2promises_history');
                    }

                    // 更新するヘッダ
                    $headers['trn_order_header_id'][]         = $detail_ret['trn_order_header_id'];
                    $headers['trn_edi_receiving_header_id'][] = $detail_ret['trn_edi_receiving_header_id'];
                    $headers['trn_edi_shipping_header_id'][]  = $detail_ret['trn_edi_shipping_header_id'];
                    $headers['trn_receiving_header_id'][]     = $detail_ret['trn_receiving_header_id'];
                    $headers['trn_shipping_header_id'][]      = $detail['trn_shipping_header_id'];
                    $headers['trn_storage_header_id'][]       = $detail['trn_storage_header_id'];
                }

                // ヘッダ調整
                $headers['trn_order_header_id']         = array_unique($headers['trn_order_header_id'], SORT_NUMERIC);
                $headers['trn_edi_receiving_header_id'] = array_unique($headers['trn_edi_receiving_header_id'], SORT_NUMERIC);
                $headers['trn_edi_shipping_header_id']  = array_unique($headers['trn_edi_shipping_header_id'], SORT_NUMERIC);
                $headers['trn_receiving_header_id']     = array_unique($headers['trn_receiving_header_id'], SORT_NUMERIC);
                $headers['trn_storage_header_id']       = array_unique($headers['trn_storage_header_id'], SORT_NUMERIC);
                if(!$this->delD2Headers($headers)){
                    $this->Vesta->rollback();
                    $this->Session->setFlash('直納品取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('d2promises_history');
                }
            } catch (Exception $ex) {
                Debugger::log($ex->getMessage());

                $this->Vesta->rollback();
                $this->Session->setFlash('直納品取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }

            $this->Vesta->commit();

            $where = " and l.id in ( " . join(',',$this->request->data['d2promises']['id']) . ")";
            $result = $this->getD2promisesHistoryDetail($where);
            $this->set('result', $result);
            $this->Session->write('d2promises.history.result' , $result);
        } else {
            // トランザクショントークン不正
            // リロード or ２度押し
            $this->set('result', $this->Session->read('d2promises.history.result'));
        }
    }

    /**
     * 商品マスタ登録：商品検索＆選択
     */
    public function additem_search(){
        $this->setRoleFunction(124); //施設採用登録
        $ret = $item_category2 = $item_category3 = array();
        $item_category1 = $this->Vesta->getItemCategoryList();
        if (isset($this->request->data['search']['is_search'])) {
            // 得意先コードからセンターのIDを取得する
            $this->request->data['centerId'] = $this->Vesta->getCenterIdFromFacilityCode($this->request->data['hospitalCode']);

            // SELECT句
            $select  = '   a.master_id                as "MstGrandmaster__master_id" ';
            $select .= ' , b.master_id                as "MstGrandmaster__accepted"';
            $select .= ' , a.jan_code                 as "MstGrandmaster__jan_code" ';
            $select .= ' , a.item_name                as "MstGrandmaster__item_name" ';
            $select .= ' , a.standard                 as "MstGrandmaster__standard" ';
            $select .= ' , a.item_code                as "MstGrandmaster__item_code" ';
            $select .= ' , c.dealer_name              as "MstGrandmaster__dealer_name" ';
            $select .= ' , a.unit_name                as "MstGrandmaster__unit_name" ';
            $select .= ' , (  ';
            $select .= '   case  ';
            $select .= '     when b.master_id is not null  ';
            $select .= "     then '済'  ";
            $select .= "     else '未'  ";
            $select .= '     end ';
            $select .= ' )                            as "MstGrandmaster__hasAccepted"  ';

            // FROM句
            $from  = ' mst_grandmasters as a ';
            $from .= ' left join mst_facility_items as b ';
            $from .= '   on a.master_id = b.master_id  ';
            $from .= '   and b.mst_facility_id = ' . $this->request->data['centerId'];
            $from .= ' left join mst_dealers as c ';
            $from .= '   on c.dealer_code = a.dealer_code ';
            $from .= '      and c.is_deleted = false ';

            // WHERE句
            $where  = ' (a.end_date > now() or a.end_date is null) ';
            $where .= ' and (a.start_date < now() or a.start_date is null) ';
             if ($this->request->data['MstGrandmaster']['master_id'] != ''){
                $where .= " and a.master_id like '%" .$this->request->data['MstGrandmaster']['master_id']. "%'";
            }
            if ($this->request->data['MstGrandmaster']['item_name'] != ''){
                $where .= " and a.item_name like '%" .$this->request->data['MstGrandmaster']['item_name']. "%'";
            }
            if ($this->request->data['MstGrandmaster']['jan_code'] != ''){
                $where .= " and a.jan_code like '" .$this->request->data['MstGrandmaster']['jan_code'] . "%'";
            }
            if ($this->request->data['MstGrandmaster']['standard'] != ''){
                $where .= " and a.standard like '%" .$this->request->data['MstGrandmaster']['standard'] . "%'";
            }
            if ($this->request->data['MstGrandmaster']['item_code'] != ''){
                $where .= " and a.item_code like '%" .$this->request->data['MstGrandmaster']['item_code'] . "%'";
            }
            if ($this->request->data['MstGrandmaster']['dealer_code'] != '') {
                $where .= " and c.dealer_name like '%" .$this->request->data['MstGrandmaster']['dealer_code'] . "%'";
            }
            if ($this->request->data['MstGrandmaster']['item_category1'] != '') {
                $where .= " and a.item_category1 = " .$this->request->data['MstGrandmaster']['item_category1'] ;
                // 中分類を取得
                $item_category2 = $this->getItemCategory(2,$this->request->data['MstGrandmaster']['item_category1']);
            }
            if (isset($this->request->data['MstGrandmaster']['item_category2']) && $this->request->data['MstGrandmaster']['item_category2'] != '') {
                $where .= " and a.item_category2 = " .$this->request->data['MstGrandmaster']['item_category2'] ;
                // 小分類を取得
                $item_category3 = $this->getItemCategory(3,$this->request->data['MstGrandmaster']['item_category2']);
            }
            if (isset($this->request->data['MstGrandmaster']['item_category3']) && $this->request->data['MstGrandmaster']['item_category3'] != '') {
                $where .= " and a.item_category3 = " .$this->request->data['MstGrandmaster']['item_category3'] ;
            }

            if (isset($this->request->data['MstGrandmaster']['isAccepted'])){
                $where .= " and b.master_id is null ";
            }

            $option = array(
                'where' => $where,
                'order' => 'a.master_id desc',
                'limit' => $this->_getLimitCount()
            );
            $result = $this->Vesta->customQuery($select, $from, $option, true);

            // 件数
            $this->set('max', $result['count']);

            // 表示内容
            $ret = $result['query'];
        } else {
            $this->request->data['MstGrandmaster']['isAccepted'] = 1;
        }

        $this->set('item_category1' , $item_category1);
        $this->set('item_category2' , $item_category2);
        $this->set('item_category3' , $item_category3);

        // 得意先
        $this->set('hospital_list', $this->getClientList());

        $this->set('gm_items', $ret);
    }

    /**
     * 商品マスタ登録：商品登録（グランドマスタから）
     */
    public function additem() {
        if ($this->request->data['MstGrandmaster']['opt'] != '') {
            // グランドマスタの情報を取得
            $data = $this->Vesta->getGrandMasterData($this->request->data['MstGrandmaster']['opt']);

            // 取得対象のセンター情報
            $data['hospitalName'] = $this->request->data['hospitalName'];
            $data['centerId']     = $this->request->data['centerId'];

            // 既定の商品IDのMAX + 1 を取得
            $data['MstFacilityItem']['internal_code'] = $this->Vesta->getFacilityItemNextInternalCode(
                Configure::read('ItemType.masterItem'), $data['centerId']);

            // 選択されたセンターの予備項目などを取得
            $this->AuthSession->updateFacilityConfig($data['centerId']);

            // プルダウン作成
            $this->_createPullDown($data);

            $this->request->data = $data;
        }
    }

    /**
     * 商品マスタ登録：商品登録（独自採用品）
     */
    public function additem_original() {
        // 得意先コードからセンターのIDを取得する
        $this->request->data['centerId'] = $this->Vesta->getCenterIdFromFacilityCode($this->request->data['hospitalCode']);

        // 既定の商品IDのMAX + 1 を取得
        $this->request->data['MstFacilityItem']['internal_code'] = $this->Vesta->getFacilityItemNextInternalCode(
            Configure::read('ItemType.originalItem'),
            $this->request->data['centerId']
        );

        // 選択されたセンターの予備項目などを取得
        $this->AuthSession->updateFacilityConfig($this->request->data['centerId']);

        // プルダウン作成
        $this->_createPullDown($this->request->data);
    }


    /**
     * 商品マスタ登録：商品登録確認
     */
    public function additem_confirm() {
        // プルダウン作成
        $this->_createPullDown($this->request->data);

        //2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);

    }

    /**
     * 商品マスタ登録：商品登録結果
     */
    public function additem_result() {
        //トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)) {
            $this->request->data = $this->Session->read('Facilityitems.saved');
            $this->Session->delete('Facilityitems.saved');

            $this->_createPullDown($this->request->data);
            $this->set('data', $this->request->data);
            return;
        }

       // 施設採用品登録用にデータ整形
        $this->request->data['MstFacilityItem']['is_auto_storage']    = (isset($this->request->data['MstFacilityItem']['is_auto_storage'])?true:false);
        $this->request->data['MstFacilityItem']['mst_dealer_id']      = $this->request->data['MstDealer']['id'];
        $this->request->data['MstFacilityItem']['per_unit']           = 1;
        $this->request->data['MstFacilityItem']['creater']            = $this->Session->read('Auth.MstUser.id');
        $this->request->data['MstFacilityItem']['modifier']           = $this->Session->read('Auth.MstUser.id');
        $this->request->data['MstFacilityItem']['mst_facility_id']    = $this->request->data['centerId'];
        $this->request->data['MstFacilityItem']['center_facility_id'] = $this->request->data['centerId'];
        $this->request->data['MstFacilityItem']['now']                = date('Y/m/d H:i:s');

        // 施主の施設IDを取得
        $this->request->data['MstFacilityItem']['mst_owner_id']    = $this->getFacilityId(
            $this->request->data['MstFacilityItem']['mst_owner_code'],
            Configure::read('FacilityType.donor'),
            $this->request->data['MstFacilityItem']['mst_facility_id']
        );

        // 重複チェック
        $result = $this->MstFacilityItem->chkDuplicateInternalCode(
            $this->request->data['MstFacilityItem']['internal_code'],
            null,
            $this->request->data['MstFacilityItem']['mst_facility_id']
        );

        if (!$result) {
            $this->Session->setFlash('既に同じ商品IDが登録済みです。新しいIDに置き換えました', 'growl', array('type'=>'error') );
            $this->_createPullDown($this->request->data);
            if($this->request->data['MstFacilityItem']['isOriginal']==1){
                //独自採用品の場合
                $this->request->data['MstFacilityItem']['internal_code'] = $this->Vesta->getFacilityItemNextInternalCode(
                    Configure::read('ItemType.originalItem'),
                    $this->request->data['MstFacilityItem']['mst_facility_id']
                );
                $this->render('add_original_item');
            }else{
                //通常品の場合
                $this->request->data['MstFacilityItem']['internal_code'] = $this->Vesta->getFacilityItemNextInternalCode(
                    Configure::read('ItemType.masterItem') ,
                    $this->request->data['MstFacilityItem']['mst_facility_id']
                );
                $this->render('additem');
            }
            return;
        }

        //トランザクション開始
        $this->MstFacilityItem->begin();

        try {
            //施設採用品登録
            $this->MstFacilityItem->create();
            if(!$this->MstFacilityItem->save($this->request->data['MstFacilityItem'], array('validate' => false))){
                // エラー
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash('施設際用品レコードの作成中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('additem_confirm');
            }

            //包装単位を登録
            $addItemUnitId = $this->Vesta->saveOneItemUnit(
                $this->request->data['MstFacilityItem'],
                $this->MstFacilityItem->getLastInsertId()
            );
            if (!$addItemUnitId) {
                // エラー
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash('包装単位レコードの作製中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('additem_confirm');
            }
            // センター倉庫の在庫レコードを作成
            $stock_data = array(
                'mst_item_unit_id'  => $addItemUnitId,
                'mst_department_id' => $this->getDepartmentId(
                    $this->request->data['MstFacilityItem']['center_facility_id'],
                    Configure::read('DepartmentType.warehouse')
                ),
                'center_facility_id' => $this->request->data['MstFacilityItem']['center_facility_id'],
                'creater'            => $this->request->data['MstFacilityItem']['creater'],
                'modifier'           => $this->request->data['MstFacilityItem']['creater']
            );
            if (!$this->Vesta->getStockRecode($stock_data)) {
                // エラー
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash('在庫レコードの作製中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('additem_confirm');
            }

            $this->MstFacilityItem->commit();

            $this->_createPullDown($this->request->data);
            $this->Session->write('Facilityitems.saved', $this->request->data);
            $this->Session->setFlash('施設採用品が正しく保存できました', 'growl', array('type'=>'star') );
        } catch (Exception $ex) {
            Debugger::log($ex->getMessage());

            $this->MstFacilityItem->rollback();
            $this->Session->setFlash('施設採用品登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
    }

    /**
     * 商品マスタ編集：商品検索＆選択
     */
    public function edititem_search () {
        $this->setRoleFunction(125);
        $fitems = array();
        $ret = $item_category2 = $item_category3 = array();
        $item_category1 = $this->Vesta->getItemCategoryList();

        if (isset($this->request->data['hide_action']) && $this->request->data['hide_action'] == 1) {
            // 得意先コードからセンターのIDを取得する
            $this->request->data['centerId'] = $this->Vesta->getCenterIdFromFacilityCode($this->request->data['hospitalCode']);

            $where = '';
            if ($this->request->data['MstFacilityItem']['internal_code'] != ''){
                $where .= " and a.internal_code LIKE '" .$this->request->data['MstFacilityItem']['internal_code']."%'";
            }
            if ($this->request->data['MstFacilityItem']['item_name'] != ''){
                $where .= " and a.item_name LIKE '%".$this->request->data['MstFacilityItem']['item_name']."%'";
            }
            if ($this->request->data['MstFacilityItem']['jan_code'] != ''){
                $where .= " and a.jan_code LIKE '" .$this->request->data['MstFacilityItem']['jan_code']."%'";
            }
            if ($this->request->data['MstFacilityItem']['standard'] != ''){
                $where .= " and a.standard LIKE '%" .$this->request->data['MstFacilityItem']['standard']."%'";
            }
            if ($this->request->data['MstFacilityItem']['item_code'] != ''){
                $where .= " and a.item_code LIKE '%".$this->request->data['MstFacilityItem']['item_code']."%'";
            }
            if ($this->request->data['MstFacilityItem']['dealer_code'] != '') {
                $where .= " and b.dealer_name LIKE '%".$this->request->data['MstFacilityItem']['dealer_code']."%'";
            }
            if ($this->request->data['MstFacilityItem']['item_category1'] != ''){
                $where .= ' and a.item_category1 = ' . $this->request->data['MstFacilityItem']['item_category1'];
                // 中分類を取得
                $item_category2 = $this->getItemCategory(2,$this->request->data['MstFacilityItem']['item_category1']);
            }
            if (isset($this->request->data['MstFacilityItem']['item_category2']) && $this->request->data['MstFacilityItem']['item_category2'] != ''){
                $where .= ' and a.item_category2 = ' . $this->request->data['MstFacilityItem']['item_category2'];
                // 小分類を取得
                $item_category3 = $this->getItemCategory(2,$this->request->data['MstFacilityItem']['item_category2']);
            }
            if (isset($this->request->data['MstFacilityItem']['item_category3']) && $this->request->data['MstFacilityItem']['item_category3'] != ''){
                $where .= ' and a.item_category3 = ' . $this->request->data['MstFacilityItem']['item_category3'];
            }
            if ($this->request->data['MstFacilityItem']['is_deleted'] == '1') {
                $where .= ' and a.is_deleted = false ';
            }

            $sql  = ' select ';
            $sql .= '       a.id                   as "MstFacilityItem__id" ';
            $sql .= '     , a.master_id            as "MstFacilityItem__master_id" ';
            $sql .= '     , a.jan_code             as "MstFacilityItem__jan_code" ';
            $sql .= '     , a.internal_code        as "MstFacilityItem__internal_code" ';
            $sql .= '     , a.item_name            as "MstFacilityItem__item_name" ';
            $sql .= '     , a.standard             as "MstFacilityItem__standard" ';
            $sql .= '     , a.item_code            as "MstFacilityItem__item_code" ';
            $sql .= '     , b.dealer_code          as "MstFacilityItem__dealer_code" ';
            $sql .= '     , a.per_unit             as "MstFacilityItem__per_unit" ';
            $sql .= '     , b.dealer_name          as "MstDealer__dealer_name" ';
            $sql .= '     , d.unit_name            as "MstUnitName__unit_name" ';
            $sql .= '     , a.start_date           as "MstFacilityItem__start_date" ';
            $sql .= '     , a.end_date             as "MstFacilityItem__end_date"  ';
            $sql .= '   from ';
            $sql .= '     mst_facility_items as a  ';
            $sql .= '     left join mst_dealers as b  ';
            $sql .= '       on a.mst_dealer_id = b.id  ';
            $sql .= '       and b.is_deleted = false  ';
            $sql .= '     left join mst_facilities as c  ';
            $sql .= '       on a.mst_facility_id = c.id  ';
            $sql .= '     left join mst_unit_names as d  ';
            $sql .= '       on a.mst_unit_name_id = d.id  ';
            $sql .= '   where ';
            $sql .= '     a.mst_facility_id = ' . $this->request->data['centerId'];
            $sql .= '     and a.item_type = ' . Configure::read('Items.item_types.normalitem');
            $sql .= '     and a.creater = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= $where;
            $sql .= '   order by ';
            $sql .= '     a.internal_code desc ';
            //全件数取得
            $this->set('max', $this->getMaxCount($sql , 'MstFacilityItem', false));
            $sql .= '   limit ' . $this->_getLimitCount();

            $fitems =  $this->MstFacilityItem->query($sql, false, false);
        }
        $this->set('fitems', $fitems);
        $this->set('item_category1', $item_category1);
        $this->set('item_category2', $item_category2);
        $this->set('item_category3', $item_category3);

        //得意先
        $this->set('hospital_list',$this->getClientList());
        $this->set('data', $this->request->data);
    }

    /**
     * 商品マスタ編集：商品編集
     */
    public function edititem($id=null){
        $_fitem = array();

        // 施設採用品の情報を取得
        $_fitem = $this->Vesta->getFacilityItemData($this->request->data['MstFacilityItem']['id']);
        $_fitem['MstFacilityItem']['is_lowlevel']     = (($_fitem['MstFacilityItem']['is_lowlevel'])? '1' : '0');
        $_fitem['MstFacilityItem']['buy_flg']         = (($_fitem['MstFacilityItem']['buy_flg'])? '1' : '0');
        $_fitem['MstFacilityItem']['is_ms_corporate'] = (($_fitem['MstFacilityItem']['is_ms_corporate'])? '1' : '0');

        // 施設情報
        $_fitem['centerId']     = $this->request->data['centerId'];
        $_fitem['hospitalName'] = $this->request->data['hospitalName'];

        if (!is_null($id)) {
            // マスタIDの付替
            $tmp = $this->Vesta->getGrandMasterData($id);
            if (count($tmp) > 0) {
                $_fitem = $this->changeMasterId($_fitem, $tmp);
                $_fitem['MstFacilityItem']['master_id'] = $id;
                $this->Session->setFlash('マスタIDに対応した商品情報を読み込みました', 'growl', array('type'=>'star') );
            } else {
                $this->Session->setFlash('マスタIDに対応した商品がありません。', 'growl', array('type'=>'error') );
            }
        }

        $_grandmaster = $this->Facilityitem->getGrandmasterItemUnit($_fitem['MstFacilityItem']['master_id']);
        if(!empty($_grandmaster)){
            $_fitem['MstGrandmaster']['unit_name'] = $_grandmaster['MstGrandmaster']['unit_name'];
        }

        $this->request->data = $_fitem;

        // 包装単位を取得
        $this->set('item_units_list', $this->Vesta->getItemUnitData(
                $_fitem['MstFacilityItem']['id'],
                $this->request->data['centerId']
            )
        );

        // 選択されたセンターの予備項目などを取得
        $this->AuthSession->updateFacilityConfig($this->request->data['centerId']);

        // プルダウンの作成
        $this->_createPullDown($this->request->data);

        // 2度押し対策用にトランザクショントークンを作る
        $this->request->data[$this->name]['token'] = $this->createToken($this->name);
        $this->set('data', $this->request->data);
    }

    /**
     * 施設採用品編集結果
     * @access public
     * @return void
     */
    public function edititem_result () {
        //トークン検証
        if (!$this->validateToken($this->request->data[$this->name]['token'], $this->name)) {
            $this->request->data = $this->Session->read('data');
            $this->Session->delete('data');

            //プルダウンの作成
            $this->_createPullDown($this->request->data);
            $this->set('data',$this->request->data);
            return;
        }
        $now = date('Y/m/d H:i:s.u');

        //重複チェック
        $result = $this->MstFacilityItem->chkDuplicateInternalCode(
            $this->request->data['MstFacilityItem']['internal_code'],
            $this->request->data['MstFacilityItem']['id'],
            $this->request->data['centerId']
        );
        if (!$result) {
            $this->Session->setFlash('既に同じ商品IDが登録済みです。新しいIDに置き換えました', 'growl', array('type'=>'error') );

            //商品ID採番しなおし
            $this->request->data['MstFacilityItem']['internal_code'] = $this->Vesta->getFacilityItemNextInternalCode(
                substr($this->request->data['MstFacilityItem']['internal_code'] , 0 , 1),
                $this->request->data['centerId']
            );

            //包装単位を取得
            $this->set('item_units_owned', $this->Vesta->getItemUnitData(
                $this->request->data['MstFacilityItem']['id'],
                $this->request->data['centerId']
            ));

            //各種プルダウン
            $this->_createPullDown($this->request->data);

            //2度押し対応トークン作成
            $this->request->data[$this->name]['token'] = $this->createToken($this->name);
            $this->render('edititem');
            return;
        }

        //施設採用品登録用にデータ整形
        $this->request->data['MstFacilityItem']['mst_dealer_id']         = $this->request->data['MstDealer']['id'];
        $this->request->data['MstFacilityItem']['dealer_code']           = $this->request->data['MstDealer']['dealer_code'];
        $this->request->data['MstFacilityItem']['per_unit']              = 1;
        $this->request->data['MstFacilityItem']['modifier']              = $this->Session->read('Auth.MstUser.id');
        $this->request->data['MstFacilityItem']['modified']              = $now;
        $this->request->data['MstFacilityItem']['is_deleted']            = 'false';
        $this->request->data['MstFacilityItem']['mst_facility_id']       = $this->request->data['centerId'];
        $this->request->data['MstFacilityItem']['item_type']             = Configure::read('Items.item_types.normalitem'); //採用品区分
        $this->request->data['MstFacilityItem']['mst_owner_id']          = $this->getFacilityId(
                                                                                $this->request->data['MstFacilityItem']['mst_owner_code'],
                                                                                Configure::read('FacilityType.donor'),
                                                                                $this->request->data['centerId']
                                                                            );
        $this->request->data['MstFacilityItem']['mst_unit_name_id']      = $this->request->data['MstFacilityItem']['basic_unit_name_id'];
        $this->request->data['MstFacilityItem']['enable_medicalcode']    = (isset($this->request->data['MstFacilityItem']['enable_medicalcode'])?$this->request->data['MstFacilityItem']['enable_medicalcode']:null);
        $this->request->data['MstFacilityItem']['is_auto_storage']       = (isset($this->request->data['MstFacilityItem']['is_auto_storage'])?'true':'false');
        $this->request->data['MstFacilityItem']['buy_flg']               = ($this->request->data['MstFacilityItem']['buy_flg']==1?'true':'false');

        $this->MstFacilityItem->begin();

        try {
            //更新チェック
            $sql  = " SELECT count(*) FROM mst_facility_items AS a ";
            $sql .= " WHERE a.id = " . $this->request->data['MstFacilityItem']['id'];
            $sql .= "   AND a.modified > '" . $this->request->data['MstFacilityItem']['time'] . "'";
            $ret = $this->MstFacilityItem->query($sql, false, false);
            if ($ret[0][0]['count'] > 0) {
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash("別ユーザによって更新されています", 'growl', array('icon'=>'error'));
                $this->redirect('edititem_search');
                return;
            }

            // 行ロック
            $sql  = ' SELECT * FROM mst_facility_items AS a ';
            $sql .= ' WHERE a.id = ' . $this->request->data['MstFacilityItem']['id'] . '  for update ';
            $this->MstFacilityItem->query($sql, false, false);

            //代表フラグ
            $standard = $this->request->data['MstItemUnit']['is_standard'];

            //包装単位分ループして登録していく
            foreach ($this->request->data['MstItemUnit']['id'] as $i => $id ) {
                $item_unit_data = array();

                $item_unit_data['MstItemUnit']['per_unit']             = $this->request->data['MstItemUnit']['per_unit'][$i];
                $item_unit_data['MstItemUnit']['mst_unit_name_id']     = $this->request->data['MstItemUnit']['mst_unit_name_id'][$i];
                $item_unit_data['MstItemUnit']['per_unit_name_id']     = $this->request->data['MstFacilityItem']['basic_unit_name_id'];
                $item_unit_data['MstItemUnit']['start_date']           = $this->request->data['MstItemUnit']['start_date'][$i];
                $item_unit_data['MstItemUnit']['end_date']             = $this->request->data['MstItemUnit']['end_date'][$i];
                $item_unit_data['MstItemUnit']['mst_facility_item_id'] = $this->request->data['MstFacilityItem']['id'];
                $item_unit_data['MstItemUnit']['mst_facility_id']      = $this->request->data['MstFacilityItem']['mst_facility_id'];
                $item_unit_data['MstItemUnit']['modifier']             = $this->Session->read('Auth.MstUser.id');
                $item_unit_data['MstItemUnit']['modified']             = $now;
                $item_unit_data['MstItemUnit']['is_standard']          = 'false';
                $item_unit_data['MstItemUnit']['is_deleted']           = 'false';
                $item_unit_data['MstItemUnit']['center_facility_id']   = $this->request->data['MstFacilityItem']['mst_facility_id'];

                if($i == $standard){
                    $item_unit_data['MstItemUnit']['is_standard'] = 'true';
                }
                if(!isset($this->request->data['MstItemUnit']['is_deleted'][$i])){
                    $item_unit_data['MstItemUnit']['is_deleted'] = 'true';
                }

                $ItemUnitResult = false;
                if (empty($id)) {
                    // 新規
                    $item_unit_data['MstItemUnit']['creater'] = $this->Session->read('Auth.MstUser.id');
                    $item_unit_data['MstItemUnit']['created'] = $now;
                    $addItemUnitId = $this->Vesta->enhanceSave('MstItemUnit', $item_unit_data);

                    //新規の場合、センター在庫レコード作成
                    $trn_stock['TrnStock']['mst_item_unit_id']   = $addItemUnitId;
                    $trn_stock['TrnStock']['mst_department_id']  = $this->request->data['MstFacilityItem']['mst_owner_id'];
                    $trn_stock['TrnStock']['is_deleted']         = 'false';
                    $trn_stock['TrnStock']['creater']            = $this->Session->read('Auth.MstUser.id');
                    $trn_stock['TrnStock']['created']            = $now;
                    $trn_stock['TrnStock']['modifier']           = $this->Session->read('Auth.MstUser.id');
                    $trn_stock['TrnStock']['modified']           = $now;
                    $trn_stock['TrnStock']['center_facility_id'] = $this->request->data['centerId'];
                    $ItemUnitResult = $this->Vesta->enhanceSave('TrnStock', $trn_stock);
                } else {
                    // 更新
                    if ($this->Vesta->chkSplitSave('MstItemUnit', $item_unit_data['MstItemUnit'], array('id'=>$id))) {
                        $ItemUnitResult = true;
                    }
                }

                if (!$ItemUnitResult) {
                    $this->Vesta->rollback();
                    $this->Session->setFlash('包装単位登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect('edititem_search');
                }
            }

            //施設採用品更新
            if ($this->Vesta->chkSplitSave(
                'MstFacilityItem',
                $this->request->data['MstFacilityItem'],
                array('id'=>$this->request->data['MstFacilityItem']['id']))
            ) {
                //コミット
                $this->MstFacilityItem->commit();

                //プルダウンの作成
                $this->_createPullDown($this->request->data);

                $this->Session->setFlash('施設採用品が正しく保存できました','growl',array('icon'=>'star'));
                $this->Session->write('data', $this->request->data);
            } else {
                //施設採用品更新失敗
                $this->MstFacilityItem->rollback();
                $this->Session->setFlash('施設採用品の編集が完了できませんでした', 'growl', array('icon'=>'error'));
                $this->redirect('edititem_search');
            }
        } catch (Exception $ex) {
            Debugger::log($ex->getMessage());

            $this->MstFacilityItem->rollback();
            $this->Session->setFlash('施設採用品の編集が完了できませんでした', 'growl', array('icon'=>'error'));
            $this->redirect('edititem_search');
        }
    }

    /**
     * transaction_search
     * 仕入設定一覧
     */
    public function transaction_search() {
        $this->setRoleFunction(124); //仕入設定
        $TransactionConfig_Info = array();

        //得意先
        $this->set('hospital_list',$this->getClientList());

        // 仕入先情報
        $this->set('suppliers_enabled', $this->getSupplierList());

        //検索ボタン押下
        if(isset($this->request->data['MstTransactionConfig']['is_search'])){
            // 得意先コードからセンターのIDを取得する
            $this->request->data['centerId'] = $this->Vesta->getCenterIdFromFacilityCode($this->request->data['hospitalCode']);

            // SELECT句
            $select  = ' a.id                   as "MstFacilityItem__id" ';
            $select .= ' , a.item_code          as "MstFacilityItem__item_code" ';
            $select .= ' , a.internal_code      as "MstFacilityItem__internal_code" ';
            $select .= ' , a.item_name          as "MstFacilityItem__item_name" ';
            $select .= ' , a.standard           as "MstFacilityItem__standard" ';
            $select .= ' , b.dealer_name        as "MstFacilityItem__dealer_name" ';

            // FROM句
            $from  = ' mst_facility_items as a ';
            $from .= ' left join mst_dealers as b ';
            $from .= '   on a.mst_dealer_id = b.id ';
            $from .= ' left join mst_transaction_mains as c ';
            $from .= '   on c.mst_facility_item_id = a.id ';

            // WHERE句
            $where  = ' a.is_deleted    = false ';
            $where .= ' and a.item_type = ' . Configure::read('Items.item_types.normalitem');
            $where .= ' and a.center_facility_id = ' . $this->request->data['centerId'];
            $where .= ' and ( c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $where .= ' or  ( c.mst_facility_id is NULL and a.creater = ' . $this->Session->read('Auth.MstUser.id') . ') )';

            //ユーザ入力値による検索条件の作成------------------------------------------
            //商品ID(前方一致)
            if((isset($this->request->data['MstFacilityItem']['search_internal_code'])) && ($this->request->data['MstFacilityItem']['search_internal_code'] != "")){
                $where .= " and a.internal_code  LIKE '" . $this->request->data['MstFacilityItem']['search_internal_code'] . "%'";
            }

            //商品名(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_item_name'])) && ($this->request->data['MstFacilityItem']['search_item_name'] != "")){
                $where .= " and a.item_name LIKE '%". $this->request->data['MstFacilityItem']['search_item_name']."%'";
            }

            //規格(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_standard'])) && ($this->request->data['MstFacilityItem']['search_standard'] != "")){
                $where .= " and a.standard LIKE '%" . $this->request->data['MstFacilityItem']['search_standard'] . "%'";
            }

            //製品番号(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_item_code'])) && ($this->request->data['MstFacilityItem']['search_item_code'] != "")){
                $where .= " and a.item_code  LIKE '%" . $this->request->data['MstFacilityItem']['search_item_code'] ."%'";
            }

            //販売元(LIKE検索)
            if((isset($this->request->data['MstFacilityItem']['search_dealer_name'])) && ($this->request->data['MstFacilityItem']['search_dealer_name'] != "")){
                $where .=  " and b.dealer_name LIKE '%" . $this->request->data['MstFacilityItem']['search_dealer_name'] . "%'";
            }

            //JANコード(前方一致)
            if((isset($this->request->data['MstFacilityItem']['search_jan_code'])) && ($this->request->data['MstFacilityItem']['search_jan_code'] != "")){
                $where .= " and a.jan_code LIKE '" . $this->request->data['MstFacilityItem']['search_jan_code'] ."%'";
            }

            //仕入設定なし
            if(isset($this->request->data['MstTransactionConfig']['is_setting'])){
                $where .= ' and c.id is null ';
            }

            $option = array(
                'where' => $where,
                'order' => ' a.internal_code asc ',
                'limit' => $this->_getLimitCount()
            );

            $data = $this->Vesta->customQuery($select, $from, $option, true);

            $this->set('max', $data['count']);
            $TransactionConfig_Info = $data['query'];
        }

        $TransactionConfig_Info = AppController::outputFilter($TransactionConfig_Info);
        $this->set('TransactionConfig_Info', $TransactionConfig_Info);
    }

    /**
     * transaction_mod
     * 仕入設定情報編集
     */
    public function transaction_mod() {
        if ($this->Session->read('RequestData')) {
            $this->request->data = $this->Session->read('RequestData');
            $this->Session->delete('RequestData');
        }
        $this->Session->write('TransactionConfig.readTime',date('Y-m-d H:i:s'));

        // 選択した施設設定を読み込む
        $centerId = $this->request->data['centerId'];
        $this->AuthSession->updateFacilityConfig($centerId);

        // 商品情報取得
        $this->request->data = $this->Vesta->getFacilityItemData($this->request->data['MstFacilityItem']['id']);
        $this->request->data['centerId'] = $centerId;

        // 仕入設定の取得
        $select  = ' a.id                                    as "MstTransactionConfig__id" ';
        $select .= ' , a.mst_item_unit_id                    as "MstTransactionConfig__mst_item_unit_id" ';
        $select .= ' , to_char(a.start_date, \'YYYY/mm/dd\') as "MstTransactionConfig__start_date" ';
        $select .= ' , to_char(a.end_date, \'YYYY/mm/dd\')   as "MstTransactionConfig__end_date" ';
        $select .= ' , a.partner_facility_id                 as "MstTransactionConfig__partner_facility_id" ';
        $select .= ' , a.partner_facility_id2                as "MstTransactionConfig__partner_facility_id2" ';
        $select .= ' , a.transaction_price                   as "MstTransactionConfig__transaction_price" ';
        $select .= ' , a.ms_transaction_price                as "MstTransactionConfig__ms_transaction_price" ';
        $select .= ' , a.is_deleted                          as "MstTransactionConfig__is_deleted" ';
        $select .= ' , ( case when a.end_date > now() then true else false end ) ';
        $select .= '                                         as "MstTransactionConfig__status" ';

        $from = ' mst_transaction_configs as a ';

        $where  = '     a.mst_facility_item_id = ' . $this->request->data['MstFacilityItem']['id'];
        $where .= ' and a.partner_facility_id = ' . $this->Session->read('Auth.facility_id_selected');

        $option = array('where' => $where);

        $tmp = $this->Vesta->customQuery($select, $from, $option);
        $transaction_config = $tmp['query'];

        $this->set('TransactionConfig', $transaction_config);

        //代表仕入設定を取得
        $select  = ' id as "MstTransactionMain__id" ';
        $select .= ' , mst_facility_id  as "MstTransactionMain__mst_facility_id" ';
        $select .= ' , mst_item_unit_id as "MstTransactionMain__mst_item_unit_id" ';

        $from = ' mst_transaction_mains ';

        $option = array('where' => ' mst_facility_item_id = ' . $this->request->data['MstFacilityItem']['id']);
        $tmp = $this->Vesta->customQuery($select, $from, $option);
        $TrnMain = $tmp['query'];
        if($TrnMain == array()){
            $TrnMain[0]['MstTransactionMain']['id']               = null;
            $TrnMain[0]['MstTransactionMain']['mst_facility_id']  = null;
            $TrnMain[0]['MstTransactionMain']['mst_item_unit_id'] = null;
        }
        $this->set('TrnMain' ,$TrnMain);

        // 包装単位プルダウン用
        $this->set('UnitName_List', $this->Vesta->setItemUnits($this->request->data['MstFacilityItem']['id']));

        // 仕入先プルダウン用
        $this->set('Suppliers_List', $this->getFacilityList(
                                        $this->request->data['centerId'],
                                        array(Configure::read('FacilityType.supplier')),
                                        true,
                                        array('id', 'facility_name'),
                                        'facility_name, facility_code'
                                     )
        );

        // 売上設定の取得
        $SalesConfig = $this->Vesta->getSalesConfig($this->request->data['MstFacilityItem']['id'], $this->Session->read('Auth.MstUser.id'));

        $this->set('me', $this->Session->read('Auth.facility_id_selected'));

        $this->set('SalesConfig', $SalesConfig);

        // トークン生成
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Edi.token' , $token);
        $this->request->data['Edi']['token'] = $token;
    }

    /**
     * transaction_result
     * 仕入情報更新（新規登録・更新）
     */
    public function transaction_result() {
        if ($this->request->data['Edi']['token'] !== $this->Session->read('Edi.token')) {
            $this->Session->setFlash('仕入設定が正しく登録できなかった可能性があります。ご確認ください。', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        $this->Session->delete('Edi.token');

        $ids           = array();
        $validations   = array();
        $facility_item = $this->request->data['MstFacilityItem'];
        $config        = $this->request->data['MstTransactionConfig'];
        $centerId      = $this->request->data['centerId'];

        //チェック & 適用終了日の調整
        $config = $this->checkTransactionSalesData($config);

        $select = ' id ';
        $from   = ' mst_facility_relations ';
        $where  = ' mst_facility_id = ' . $centerId;
        $where .= ' and partner_facility_id = ' . $this->Session->read('Auth.facility_id_selected');

        $tmp = $this->Vesta->customQuery($select, $from, array('where' => $where));
        $relation_id = $tmp['query'][0][0]['id'];

        // 登録／更新用データ整形
        $save_data = $this->createTransactionSalesData($config, $facility_item, $centerId, $relation_id);

        try {
            //トランザクションの開始
            $this->Vesta->begin();

            if ($ids) {
                $this->Vesta->query(' select * from mst_transaction_configs as a where a.id in (' . join(',', $ids) . ') for update ');
            }

            foreach ($save_data as $value) {
                if (!$this->Vesta->dispatchSave('MstTransactionConfig', $value, array('id'))) {
                    $this->Vesta->rollback();
                    $this->Session->setFlash('登録が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                    $this->redirect('transaction_search');
                }
            }

            // 売上設定
            if (Configure::read('SalesCreate.flag') == 1) {
                if(!$this->Vesta->createSalesConfig($save_data)){
                    $this->Vesta->rollback();
                    $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                    $this->redirect('transaction_search');
                }
            }

            // 代表仕入
            $this->request->data['MstTransactionMain']['user_id'] = $this->Session->read('Auth.MstUser.id');
            if (!$this->Vesta->saveTrnMain($this->request->data['MstTransactionMain'], $facility_item)) {
                $this->Vesta->rollback();
                $this->Session->setFlash('更新が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                $this->redirect('transaction_search');
            }

            //コミット
            $this->Vesta->commit();

            $this->set('UnitName_List',  $this->Vesta->setItemUnits($this->request->data['MstFacilityItem']['id']));
            $this->set('Suppliers_List', $this->getSupplierList(array('id', 'facility_name')));
            $this->set('main_result',    $this->request->data['MstTransactionMain']);
            $this->set('result',         $save_data);
        } catch (Exception $ex) {
            Debugger::log($ex->getMessage());

            $this->Vesta->rollback();
            $this->Session->setFlash('仕入設定の登録が完了しませんでした', 'growl', array('icon'=>'error'));
            $this->redirect('transaction_search');
        }
    }

    /**
     * 仕入設定データのチェック
     */
    public function checkTransactionSalesData ($config) {
        $same         = array();
        $format       = array();
        $is_duplicate = false;
        foreach ($config as $line => $data) {
            $check_key = $data['partner_facility_id2'] . '_' . $data['mst_item_unit_id'] . '_' . $data['start_date'];

            // 完全一致はエラー
            if (isset($format[$check_key])) {
                $is_duplicate = true;
                break;
            }
            $format[$check_key] = $line;
        }

        if ($is_duplicate == true) {
            //同じ包装単位で同じ開始日なのでエラーにする
            $this->Session->setFlash('適用開始日・包装単位・仕入先が同一の仕入設定があります。', 'growl', array('type'=>'error'));
            $this->Session->write('RequestData', $this->request->data);
            $this->redirect('transaction_mod');
        }

        // 重複が無ければ、キーでソート
        ksort($format);

        foreach ($format as $line) {
            $data      = $config[$line];
            $facility  = $data['partner_facility_id2'];
            $item_unit = $data['mst_item_unit_id'];
            $start     = $data['start_date'];

            // 仕入先、包装単位は同じだが、開始日が違う場合、終了日を調整する
            if (isset($same[$facility][$item_unit])) {
                    $old_line = $same[$facility][$item_unit];
                    $config[$old_line]['end_date'] = $data['start_date'];
            }
            $same[$facility][$item_unit] = $line;
        }

        return $config;
    }

    /**
     * 仕入設定をDBに格納する為のデータ整形
     */
    public function createTransactionSalesData ($config, $facility_item, $centerId, $relation_id) {
        $save_data = array();
        $now = date('Y/m/d H:i:s');
        foreach($config as $line => $data){
            if($data['id']){
                $ids[] = $data['id'];
            } else {
                $data['creater'] = $this->Session->read('Auth.MstUser.id');
                $data['created'] = $now;
            }

            $data['mst_facility_item_id']     = $facility_item['id'];
            $data['mst_facility_id']          = $centerId;
            $data['mst_facility_relation_id'] = $relation_id;
            $data['partner_facility_id']      = $this->Session->read('Auth.facility_id_selected');
            if($this->Session->read('Auth.Config.MSCorporate') == '1'){
                $data['ms_transaction_price'] = $data['ms_transaction_price'];
            } else {
                $data['ms_transaction_price'] = $data['transaction_price'];
            }
            $data['is_deleted']               = (isset($data['is_deleted'])? false : true);
            $data['modifier']                 = $this->Session->read('Auth.MstUser.id');
            $data['modified']                 = $now;
            $data['center_facility_id']       = $centerId;
            $save_data[] = array('MstTransactionConfig' => $data);
        }
        return $save_data;
    }

    /**
     * AppControllerのものをオーバーライド
     */
    public function _createPullDown ($data) {
        // 定数テーブル情報取得
        $this->constPullDown();

        // 商品カテゴリ
        $this->itemCategoryPullDown($data['MstFacilityItem']);

        $this->set('owners',              $this->getFacilityList($data['centerId'], array(Configure::read('FacilityType.donor'))));
        $this->set('item_units',          $this->Vesta->getUnitNameList());
        $this->set('accountlists',        $this->Vesta->getAccountList($data['centerId']));
        $this->set('favoritelists',       $this->Vesta->getFavoriteGroupList($data['centerId']));
    }

    // 定数系プルダウン
    public function constPullDown () {
        $constData = $this->Vesta->getConstList();

        $this->set('insuranceClaimTypes', $constData[Configure::read('ConstGroupType.insuranceClaimType')]);
        $this->set('biogenous_types',     $constData[Configure::read('ConstGroupType.biogenousTypes')]);
        $this->set('classseparations',    $constData[Configure::read('ConstGroupType.classSeparations')]);
        $this->set('taxes',               $constData[Configure::read('ConstGroupType.taxes')]);
        $this->set('keep_type',           $constData[Configure::read('ConstGroupType.keep')]);
        $this->set('cost_sticker',        $constData[Configure::read('ConstGroupType.costSticker')]);
        $this->set('lotubd',              $constData[Configure::read('ConstGroupType.lotubd')]);

        unset($constData);
    }

    // 商品カテゴリ
    public function itemCategoryPullDown ($item) {
        $this->set('item_category1', $this->Vesta->getItemCategoryList());

        $item_category2 = array();
        if(isset($item['item_category1'])){
            $item_category2 = $this->Vesta->getItemCategoryList(2, $item['item_category1']);
        }
        $this->set('item_category2', $item_category2);

        $item_category3 = array();
       if(isset($item['item_category2'])){
            $item_category3 = $this->Vesta->getItemCategoryList(3, $item['item_category2']);
        }
        $this->set('item_category3', $item_category3);
    }

    public function changeMasterId ($item, $gmItem) {
        if(!is_null($gmItem)){
            //商品IDが独自採用品のもの「9」から始まり、続く文字列が数字の並びなら先頭の文字を「1」に変更
            $pattern = '/^('.Configure::read('ItemType.originalItem').')([0-9]+)/i';
            $replacement = Configure::read('ItemType.masterItem') . '$2';

            $fitem  = $gmItem['MstFacilityItem'];
            $dealer = $gmItem['MstDealer'];

            // 登録する商品情報
            $item['MstFacilityItem']['internal_code']         =  preg_replace($pattern, $replacement, $item['MstFacilityItem']['internal_code']);
            $item['MstFacilityItem']['standard']              = $fitem['standard'];
            $item['MstFacilityItem']['insurance_claim_type']  = $fitem['insurance_claim_type'];
            $item['MstFacilityItem']['jmdn_code']             = $fitem['jmdn_code'];
            $item['MstFacilityItem']['item_code']             = $fitem['item_code'];
            $item['MstFacilityItem']['class_separation']      = $fitem['class_separation'];
            $item['MstFacilityItem']['item_name']             = $fitem['item_name'];
            $item['MstFacilityItem']['insurance_claim_code']  = $fitem['insurance_claim_code'];
            $item['MstFacilityItem']['jan_code']              = $fitem['jan_code'];
            $item['MstFacilityItem']['unit_price']            = $fitem['unit_price'];
            $item['MstFacilityItem']['item_category1']        = $fitem['item_category1'];
            $item['MstFacilityItem']['item_category2']        = $fitem['item_category2'];
            $item['MstFacilityItem']['item_category3']        = $fitem['item_category3'];
            $item['MstFacilityItem']['refund_price']          = $fitem['refund_price'];
            $item['MstFacilityItem']['biogenous_type']        = $fitem['biogenous_type'];
            $item['MstFacilityItem']['tax_type']              = $fitem['tax_type'];

            $item['Grandmaster']['unit_name']                 = $fitem['unit_name'];

            $item['MstDealer']['id']                          = $dealer['id'];
            $item['MstDealer']['dealer_name']                 = $dealer['dealer_name'];
            $item['MstDealer']['dealer_code']                 = $dealer['dealer_code'];
        }
        return $item;
    }

    /**
     * 商品情報検索
     */
    private function searchFacilityItems($where = null , $order = null , $limit = null , $split_flg=true){
        $sql  =' select ';
        $sql .='       b.id                 as "MstFacilityItems__id"';
        $sql .='     , a.internal_code      as "MstFacilityItems__internal_code"';
        $sql .='     , a.item_name          as "MstFacilityItems__item_name"';
        $sql .='     , a.standard           as "MstFacilityItems__standard"';
        $sql .='     , a.item_code          as "MstFacilityItems__item_code"';
        $sql .='     , c.dealer_name        as "MstFacilityItems__dealer_name"';
        $sql .='     , ( case ';
        $sql .='         when b.per_unit = 1 then d.unit_name ';
        $sql .="         else d.unit_name || '(' || b.per_unit || e.unit_name || ')' ";
        $sql .='         end )          as "MstFacilityItems__unit_name"';
        $sql .='     , ( case ';
        $sql .='         when f.id is null then false ';
        $sql .='         when g.id is null then false ';
        $sql .='         else true ';
        $sql .='         end )              as "MstFacilityItems__check" ';
        $sql .= '     , a.spare_key2        as "MstFacilityItems__aptage_item_code" ';
        $sql .= '     , a.spare_key5        as "MstFacilityItems__aptage_facility_code" ';
        $sql .= '     , d.internal_code     as "MstFacilityItems__unit_code" ';
        $sql .= '     , a.is_lowlevel       as "MstFacilityItems__is_lowlevel" ';
        $sql .= '     , f.sales_price       as "MstFacilityItems__sales_price" ';
        $sql .= '     , g.transaction_price as "MstFacilityItems__transaction_price" ';
        $sql .= '     , a.class_separation  as "MstFacilityItems__class_separation" ';
        $sql .='   from ';
        $sql .='     mst_facility_items as a  ';
        $sql .='     left join mst_item_units as b  ';
        $sql .='       on a.id = b.mst_facility_item_id  ';
        $sql .='     left join mst_dealers as c  ';
        $sql .='       on c.id = a.mst_dealer_id  ';
        $sql .='     left join mst_unit_names as d  ';
        $sql .='       on d.id = b.mst_unit_name_id  ';
        $sql .='     left join mst_unit_names as e  ';
        $sql .='       on e.id = b.per_unit_name_id  ';
        $sql .='    left join mst_sales_configs as f  ';
        $sql .='      on f.mst_item_unit_id = b.id  ';
        $sql .='      and f.is_deleted = false  ';
        $sql .='      and f.partner_facility_id = ' . $this->request->data['d2promises']['hospitalId'];
        $sql .="      and f.start_date <= '" . $this->request->data['d2promises']['work_date'] . "'"; //売上設定開始日
        $sql .="      and ( f.end_date >  '" . $this->request->data['d2promises']['work_date'] . "'"; //売上設定終了日
        $sql .='      or f.end_date is null ) ';

        $sql .='    left join mst_transaction_configs as g  ';
        $sql .='      on g.mst_item_unit_id = b.id  ';
        $sql .='      and g.is_deleted = false  ';
        $sql .='      and g.partner_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .="      and g.start_date <= '" . $this->request->data['d2promises']['work_date'] . "'"; //仕入設定開始日
        $sql .="      and ( g.end_date >  '" . $this->request->data['d2promises']['work_date'] . "'"; //仕入設定終了日
        $sql .='      or g.end_date is null ) ';

        if(!is_null($where)){
            $sql .=' where 1 = 1 ';
            $sql .=' ' . $where;
        }
        if(!is_null($order)){
            $sql .=' ' . $order;
        }
        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem' ,$split_flg));
            $sql .=' ' .$limit;
        }
        return $sql;
    }

    /**
     * 得意先一覧取得
     */
    private function getClientList(){
        $sql  = '';
        $list = array();

        $sql  = ' select ';
        $sql .= '     c.facility_code as "MstFacility__facility_code"';
        $sql .= '   , c.facility_name as "MstFacility__facility_name" ';
        $sql .= ' from ';
        $sql .= '   mst_facility_relations as a  ';
        $sql .= '   left join mst_facility_relations as b  ';
        $sql .= '     on b.mst_facility_id = a.mst_facility_id  ';
        $sql .= '   inner join mst_facilities as c  ';
        $sql .= '     on c.id = b.partner_facility_id  ';
        $sql .= '     and c.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= ' where ';
        $sql .= '   a.partner_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '   and a.is_deleted = false ';
        $sql .= '   and b.is_deleted = false ';
        $list = Set::Combine(
            $this->MstFacility->query($sql, false, false),
            "{n}.MstFacility.facility_code",
            "{n}.MstFacility.facility_name"
            );

        return $list;
    }

    /**
     * 仕入先２一覧取得
     */
    private function getSupplierList($field = array('facility_code', 'facility_name')){
        list($a , $b) = $field;

        $sql  = '';
        $list = array();

        $sql  = ' select ';
        $sql .= "     c.{$a} as \"MstFacility__{$a}\" ";
        $sql .= "   , c.{$b} as \"MstFacility__{$b}\" ";
        $sql .= ' from ';
        $sql .= '   mst_transaction_configs a';
        $sql .= ' inner join ';
        $sql .= '   mst_facility_items b  ';
        $sql .= ' on  b.id = a.mst_facility_item_id ';
        $sql .= ' and b.is_lowlevel = false '; //低レベル品は除外
        $sql .= ' and b.is_deleted = false ';
        $sql .= ' left join ';
        $sql .= '   mst_facilities c  ';
        $sql .= ' on  c.id = a.partner_facility_id2 ';
        $sql .= ' and c.is_deleted = false ';
        $sql .= ' where ';
        $sql .= '     a.partner_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= ' and a.is_deleted = false  ';
        $sql .= ' order by c.facility_code';
        $list = Set::Combine(
            $this->MstFacility->query($sql, false, false),
            "{n}.MstFacility.{$a}",
            "{n}.MstFacility.{$b}"
            );
        return $list;
    }

    /**
     * 直納品登録：発注ヘッダ作成
     * @return 作成した発注ヘッダのID
     */
    private function createD2TrnOrderHeader($data){
        $TrnOrderHeader = array(
            'TrnOrderHeader' => array(
                'work_no'            => $data['work_no'],
                'work_date'          => $data['work_date'],
                'work_type'          => null,
                'recital'            => $data['recital1'],
                'order_type'         => Configure::read('OrderTypes.direct'),  // 発注区分：直納発注
                'order_status'       => Configure::read('OrderStatus.loaded'), // 発注状態：入庫済
                'department_id_from' => $data['departmentId'],
                'department_id_to'   => $data['toId'],
                'department_id_to2'  => $data['toId2'],
                'detail_count'       => $data['detail_count'],
                'is_deleted'         => false,
                'creater'            => $this->Session->read('Auth.MstUser.id'),
                'created'            => $data['now'],
                'modifier'           => $this->Session->read('Auth.MstUser.id'),
                'modified'           => $data['now'],
                'center_facility_id' => $data['centerId']
                )
            );

        // SQL実行
        $this->TrnOrderHeader->create();
        if (!$this->TrnOrderHeader->save($TrnOrderHeader)) {
            $this->TrnOrderHeader->rollback();
            $this->Session->setFlash('直納品処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }

        return $this->TrnOrderHeader->getLastInsertID();
    }

    /**
     * 直納品登録：受注ヘッダ作成
     * @return 作成した受注ヘッダのID
     */
    private function createD2TrnEdiReceivingHeader($data){
        $TrnEdiReceivingHeader = array(
            'TrnEdiReceivingHeader'=> array(
                'work_no'            => $data['work_no'],
                'work_date'          => $data['work_date'],
                'recital'            => $data['recital1'],
                'department_id_from' => $data['departmentId'],
                'department_id_to'   => $data['toId'],
                'department_id_to2'  => $data['toId2'],
                'detail_count'       => $data['detail_count'],
                'is_deleted'         => false,
                'creater'            => $this->Session->read('Auth.MstUser.id'),
                'created'            => $data['now'],
                'modifier'           => $this->Session->read('Auth.MstUser.id'),
                'modified'           => $data['now']
                )
            );

        // SQL実行
        $this->TrnEdiReceivingHeader->create();
        if (!$this->TrnEdiReceivingHeader->save($TrnEdiReceivingHeader)) {
            $this->TrnEdiReceivingHeader->rollback();
            $this->Session->setFlash('直納品処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }

        return $this->TrnEdiReceivingHeader->getLastInsertID();
    }

    /**
     * 直納品登録：業者出荷ヘッダ作成
     * @return 作成した出荷ヘッダのID
     */
    private function createD2TrnEdiShippingHeader($data){
        $TrnEdiShippingHeader = array(
            'TrnEdiShippingHeader'=> array(
                'work_no'            => $data['work_no'],
                'work_date'          => $data['work_date'],
                'recital'            => $data['recital1'],
                'spare_recital1'     => $data['recital2'],
                'spare_recital2'     => $data['recital3'],
                'department_id_from' => $data['toId'],
                'department_id_to'   => $data['departmentId'],
                'detail_count'       => $data['detail_count'],
                'is_deleted'         => false,
                'creater'            => $this->Session->read('Auth.MstUser.id'),
                'created'            => $data['now'],
                'modifier'           => $this->Session->read('Auth.MstUser.id'),
                'modified'           => $data['now']
                )
            );

        // SQL実行
        $this->TrnEdiShippingHeader->create();
        if (!$this->TrnEdiShippingHeader->save($TrnEdiShippingHeader)) {
            $this->TrnEdiShippingHeader->rollback();
            $this->Session->setFlash('業者出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }

        return $this->TrnEdiShippingHeader->getLastInsertID();
    }

    /**
     * 直納品登録:入荷ヘッダ作成
     * @return 作成した入荷ヘッダのID
     */
    private function createD2TrnReceivingHeader($data){
        $TrnReceivingHeader = array(
            'TrnReceivingHeader' => array(
                'work_no'            => $data['work_no'],
                'work_date'          => $data['work_date'],
                'recital'            => $data['recital1'],
                'receiving_type'     => Configure::read('ReceivingType.arrival'), //入荷区分：入荷
                'receiving_status'   => Configure::read('ReceivingStatus.stock'), //入荷状態：入庫済
                'department_id_from' => $data['toId'],                            //要求元部署参照キー（仕入先）
                'department_id_to'   => $data['centerDepartmentId'],              //要求先部署参照キー（センター）
                'detail_count'       => $data['detail_count'],                    //明細数
                'creater'            => $this->Session->read('Auth.MstUser.id'),
                'created'            => $data['now'],
                'modifier'           => $this->Session->read('Auth.MstUser.id'),
                'modified'           => $data['now'],
                'center_facility_id' => $data['centerId'],
                )
            );
        // SQL実行
        $this->TrnReceivingHeader->create();
        if (!$this->TrnReceivingHeader->save($TrnReceivingHeader)) {
            $this->TrnReceivingHeader->rollback();
            $this->Session->setFlash('入荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }

        return $this->TrnReceivingHeader->getLastInsertID();
    }

    /**
     * 直納品登録:入庫ヘッダ作成
     * @return 作成した入庫ヘッダのID
     */
    private function createD2TrnStorageHeader($data){
        $TrnStorageHeader = array(
            'TrnStorageHeader' => array(
                'work_no'                 => $data['work_no'],
                'work_date'               => $data['work_date'],
                'recital'                 => $data['recital1'],
                'storage_type'            => Configure::read('StorageType.direct'),
                'storage_status'          => Configure::read('StorageStatus.arrival'),
                'mst_department_id'       => $data['centerDepartmentId'],
                'detail_count'            => $data['detail_count'],
                'is_deleted'              => false,
                'creater'                 => $this->Session->read('Auth.MstUser.id'),
                'created'                 => $data['now'],
                'modifier'                => $this->Session->read('Auth.MstUser.id'),
                'modified'                => $data['now'],
                'trn_receiving_header_id' => $data['trn_receiving_header_id'],
                'center_facility_id'      => $data['centerId'],
                )
        );

        $this->TrnStorageHeader->create();
        if(!$this->TrnStorageHeader->save($TrnStorageHeader)){
            $this->TrnStorageHeader->rollback();
            $this->Session->setFlash('入庫登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }

        return $this->TrnStorageHeader->getLastInsertID();
    }

    /**
     * 直納品登録:出荷ヘッダ作成
     * @return 作成した出荷ヘッダのID
     */
    private function createD2TrnShippingHeader($data, $d2item){
        $quantity_all = 0;
        foreach($data['checked'] as $line){
            $quantity_all += $d2item[$line]['quantity'];
        }

        $TrnShippingHeader = array(
            'TrnShippingHeader' => array(
                'work_no'            => $data['work_no'],
                'work_date'          => $data['work_date'],
                'recital'            => $data['recital1'],
                'shipping_type'      => Configure::read('ShippingType.direct'),
                'shipping_status'    => Configure::read('ShippingStatus.ordered'), //初期
                'department_id_from' => $data['centerDepartmentId'],
                'department_id_to'   => $data['departmentId'],
                'detail_count'       => $quantity_all,
                'is_deleted'         => false,
                'creater'            => $this->Session->read('Auth.MstUser.id'),
                'created'            => $data['now'],
                'modifier'           => $this->Session->read('Auth.MstUser.id'),
                'modified'           => $data['now'],
                'center_facility_id' => $data['centerId']
            )
        );
        $this->TrnShippingHeader->create();
        if(!$this->TrnShippingHeader->save($TrnShippingHeader)){
            $this->TrnShippingHeader->rollback();
            $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }

        return $this->TrnShippingHeader->getLastInsertID();
    }

    /**
     * 直納品登録：発注明細作成
     * @return 作成した発注明細のID
     */
    private function createD2TrnOrder($data){
        $TrnOrder = array(
            'TrnOrder'=> array(
                'work_no'             => $data['work_no'],
                'work_seq'            => $data['work_seq'],
                'work_date'           => $data['work_date'],
                'work_type'           => '', //業者側では紐付を固定出来ない
                'recital'             => $data['recital'],
                'order_type'          => Configure::read('OrderTypes.direct'),  // 発注区分：直納発注
                'order_status'        => Configure::read('OrderStatus.loaded'), // 発注状態：入庫済
                'mst_item_unit_id'    => $data['mst_item_unit_id'],
                'department_id_from'  => $data['departmentId'],
                'department_id_to'    => $data['toId'],
                'department_id_to2'   => $data['toId2'],
                'quantity'            => $data['quantity'],
                'before_quantity'     => $data['quantity'],
                'remain_count'        => 0,
                'stocking_price'      => $this->Vesta->getStokingPrice($data['mst_item_unit_id'],
                                                                       $this->Session->read('Auth.facility_id_selected'),
                                                                       $this->Session->read('Auth.facility_id_selected'),
                                                                       $data['work_date']),
                'is_deleted'          => false,
                'creater'             => $this->Session->read('Auth.MstUser.id'),
                'created'             => $data['now'],
                'modifier'            => $this->Session->read('Auth.MstUser.id'),
                'modified'            => $data['now'],
                'fixed_num_used'      => 0,
                'trn_order_header_id' => $data['trn_order_header_id'],
                'center_facility_id'  => $data['centerId']
                )
            );

        // SQL実行
        $this->TrnOrder->create();
        if (!$this->TrnOrder->save($TrnOrder)) {
            $this->TrnOrder->rollback();
            $this->Session->setFlash('直納品処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }

        return $this->TrnOrder->getLastInsertID();
    }

    /**
     * 直納品登録：受注明細作成
     * @return 作成した受注明細のID
     */
    private function createD2TrnEdiReceiving($data){
        $TrnEdiReceiving = array(
            'TrnEdiReceiving'=> array(
                'work_no'                     => $data['work_no'],
                'work_seq'                    => $data['work_seq'],
                'work_date'                   => $data['work_date'],
                'work_type'                   => null,
                'recital'                     => $data['recital'],
                'mst_item_unit_id'            => $data['mst_item_unit_id'],
                'department_id_from'          => $data['departmentId'],
                'department_id_to'            => $data['toId'],
                'department_id_to2'           => $data['toId2'],
                'quantity'                    => $data['quantity'],
                'remain_count'                => 0,                 // そのまま出荷されるので0としておく
                'trn_order_id'                => $data['trn_order_id'],
                'trn_edi_receiving_header_id' => $data['trn_edi_receiving_header_id'],
                'is_deleted'                  => false,
                'creater'                     => $this->Session->read('Auth.MstUser.id'),
                'created'                     => $data['now'],
                'modifier'                    => $this->Session->read('Auth.MstUser.id'),
                'modified'                    => $data['now']
                )
            );

        // SQL実行
        $this->TrnEdiReceiving->create();
        if (!$this->TrnEdiReceiving->save($TrnEdiReceiving)) {
            $this->TrnEdiReceiving->rollback();
            $this->Session->setFlash('受注登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }

        return $this->TrnEdiReceiving->getLastInsertID();
    }

    /**
     * 直納品登録：業者出荷明細作成
     * @return 作成した業者出荷明細のID
     */
    private function createD2TrnEdiShipping($data){
        $TrnEdiShipping = array(
            'TrnEdiShipping'=> array(
                'work_no'                    => $data['work_no'],
                'work_seq'                   => $data['work_seq'],
                'work_date'                  => $data['work_date'],
                'work_type'                  => null,
                'recital'                    => $data['recital'],
                'mst_item_unit_id'           => $data['mst_item_unit_id'],
                'department_id_from'         => $data['toId'],
                'department_id_to'           => $data['departmentId'],
                'quantity'                   => $data['quantity'],
                'shipping_status'            => Configure::read('ShippingStatus.ordered'),
                'lot_no'                     => trim($data['lot_no']),
                'validated_date'             => $this->getFormatedValidateDate($data['validated_date']),
                'trn_edi_shipping_header_id' => $data['trn_edi_shipping_header_id'],
                'trn_edi_receiving_id'       => $data['trn_edi_receiving_id'],
                'trn_order_id'               => $data['trn_order_id'],
                'is_deleted'                 => false,
                'creater'                    => $this->Session->read('Auth.MstUser.id'),
                'created'                    => $data['now'],
                'modifier'                   => $this->Session->read('Auth.MstUser.id'),
                'modified'                   => $data['now']
                )
            );

        // SQL実行
        $this->TrnEdiShipping->create();
        if (!$this->TrnEdiShipping->save($TrnEdiShipping)) {
            $this->TrnEdiShipping->rollback();
            $this->Session->setFlash('業者出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }
        return $this->TrnEdiShipping->getLastInsertID();
    }

    /**
     * 直納品登録：入荷明細作成
     * @return 作成した入荷明細のID
     */
    private function createD2TrnReceiving($data){
        $TrnReceiving = array(
            'TrnReceiving' => array(
                'work_no'                 => $data['work_no'],
                'work_seq'                => $data['work_seq'],
                'work_date'               => $data['work_date'],
                'work_type'               => null,
                'recital'                 => $data['recital'],
                'receiving_type'          => Configure::read('ReceivingType.arrival'),    //入荷区分：入荷
                'receiving_status'        => Configure::read('ReceivingStatus.stock'),    //入荷状態：入庫済
                'mst_item_unit_id'        => $data['mst_item_unit_id'],                   //包装単位参照キー
                'department_id_from'      => $data['toId'],                               //要求元部署参照キー（仕入先）
                'department_id_to'        => $data['centerDepartmentId'],                 //要求先部署参照キー（センター）
                'quantity'                => $data['quantity'],                           //数量
                'remain_count'            => 0,
                'stocking_price'          => $data['transaction_price'],                  //仕入価格
                'sales_price'             => $data['sales_price'],                        //売上価格
                'trn_sticker_id'          => '',                                          //シール参照キー
                'trn_order_id'            => $data['trn_order_id'],                       //発注参照キー
                'trn_receiving_header_id' => $data['trn_receiving_header_id'],            //入荷ヘッダ外部キー
                'is_retroactable'         => (isset($data['sokyu']) && $data['sokyu'] == '1') ? 'false' : 'true', //遡及可能フラグ
                'creater'                 => $this->Session->read('Auth.MstUser.id'),
                'created'                 => $data['now'],
                'modifier'                => $this->Session->read('Auth.MstUser.id'),
                'modified'                => $data['now'],
                'center_facility_id'      => $data['centerId']
            )
        );

        // SQL実行
        $this->TrnReceiving->create();
        if (!$this->TrnReceiving->save($TrnReceiving)) {
            $this->TrnReceiving->rollback();
            $this->Session->setFlash('入荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('/');
        }

        return $this->TrnReceiving->getLastInsertID();
    }

    /**
     * 直納品登録：入庫明細作成
     * @return 作成した入庫明細のID
     */
   private function createD2TrnStorage($data) {
        $ret = array();

        $facilityCode         = $this->getFacilityCode($data['centerId']);
        $hospitalFacilityCode = $this->getFacilityCode($data['hospitalId']);

        for($i = 0; $i < $data['quantity']; $i++){
            $stickerNo         = $this->getCenterStickerNo($data['work_date'], $facilityCode, $data['centerId']);
            $hospitalStickerNo = $this->getHospitalStickerNo($data['work_date'], $hospitalFacilityCode, $data['centerId']);

            //シール発行
            $TrnSticker = array(
                'TrnSticker' => array(
                    'facility_sticker_no' => $stickerNo,
                    'hospital_sticker_no' => $hospitalStickerNo,
                    'mst_item_unit_id'    => $data['mst_item_unit_id'],
                    'mst_department_id'   => $data['departmentId'],
                    'trade_type'          => Configure::read('ClassesType.PayDirectly'),
                    'quantity'            => 1,
                    'lot_no'              => trim($data['lot_no']),
                    'validated_date'      => $this->getFormatedValidateDate($data['validated_date']),
                    'original_price'      => $data['transaction_price'],
                    'transaction_price'   => $data['transaction_price'],
                    'sales_price'         => $data['sales_price'],
                    'trn_order_id'        => $data['trn_order_id'],
                    'trn_receiving_id'    => $data['trn_receiving_id'],
                    'trn_edi_shipping_id' => $data['trn_edi_shipping_id'],
                    'buy_claim_id'        => null,
                    'is_deleted'          => true,
                    'has_reserved'        => false,
                    'creater'             => $this->Session->read('Auth.MstUser.id'),
                    'created'             => $data['now'],
                    'modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'modified'            => $data['now'],
                    'center_facility_id'  => $data['centerId']
                )
            );

            $this->TrnSticker->create();
            if(!$this->TrnSticker->save($TrnSticker)){
                Debugger::log('シール発行でエラー発生');

                $this->TrnSticker->rollback();
                $this->Session->setFlash('入庫登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
            $lastStickerId = $this->TrnSticker->getLastInsertID();

            //入庫明細
            $TrnStorage = array(
                'TrnStorage' => array(
                    'work_no'               => $data['work_no'],
                    'work_seq'              => $i + 1,
                    'work_date'             => $data['work_date'],
                    'work_type'             => null,
                    'recital'               => $data['recital'],
                    'storage_type'          => Configure::read('StorageType.direct'),
                    'storage_status'        => Configure::read('StorageStatus.arrival'),
                    'mst_item_unit_id'      => $data['mst_item_unit_id'],
                    'mst_department_id'     => $data['centerDepartmentId'],
                    'quantity'              => 1,
                    'trn_sticker_id'        => $lastStickerId,
                    'trn_receiving_id'      => $data['trn_receiving_id'],
                    'trn_storage_header_id' => $data['trn_storage_header_id'],
                    'is_deleted'            => false,
                    'creater'               => $this->Session->read('Auth.MstUser.id'),
                    'created'               => $data['now'],
                    'modifier'              => $this->Session->read('Auth.MstUser.id'),
                    'modified'              => $data['now'],
                    'facility_sticker_no'   => $stickerNo,
                    'hospital_sticker_no'   => $hospitalStickerNo,
                    'center_facility_id'    => $data['centerId']
                )
            );
            $this->TrnStorage->create();
            if(!$this->TrnStorage->save($TrnStorage)){
                Debugger::log('入庫明細作成でエラー発生');

                $this->TrnStorage->rollback();
                $this->Session->setFlash('入庫登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
            $lastStorageId = $this->TrnStorage->getLastInsertID();

            //シール更新
            $sticker_update_sql = 'UPDATE trn_stickers SET trn_storage_id = ' .$lastStorageId . ' where id = ' . $lastStickerId;
            $this->TrnSticker->query($sticker_update_sql, false, false);

            // 返却配列に格納
            $ret['trn_sticker_id'][$i]      = $lastStickerId;
            $ret['facility_sticker_no'][$i] = $stickerNo;
            $ret['hospital_sticker_no'][$i] = $hospitalStickerNo;
        }

        return $ret;
    }

    /**
     * 直納品登録：出荷明細作成
     * @return 作成した出荷明細のID
     */
   private function createD2TrnShipping($data) {
        $retrun_array = array();
        for($i = 0; $i < $data['quantity']; $i++){
            $TrnShipping = array(
                'TrnShipping' => array(
                    'work_no'                => $data['work_no'],
                    'work_seq'               => $i + 1,
                    'work_date'              => $data['work_date'],
                    'work_type'              => null,
                    'recital'                => $data['recital'],
                    'shipping_type'          => Configure::read('ShippingType.edidirect'),
                    'shipping_status'        => Configure::read('ShippingStatus.ordered'), //初期
                    'mst_item_unit_id'       => $data['mst_item_unit_id'],
                    'department_id_from'     => $data['centerDepartmentId'],
                    'department_id_to'       => $data['departmentId'],
                    'quantity'               => 1,
                    'trn_sticker_id'         => $data['trn_sticker_id'][$i],
                    'is_deleted'             => false,
                    'creater'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                => $data['now'],
                    'modifier'               => $this->Session->read('Auth.MstUser.id'),
                    'modified'               => $data['now'],
                    'trn_shipping_header_id' => $data['trn_shipping_header_id'],
                    'facility_sticker_no'    => $data['facility_sticker_no'][$i],
                    'hospital_sticker_no'    => $data['hospital_sticker_no'][$i],
                    'center_facility_id'     => $data['centerId']
                )
            );
            $this->TrnShipping->create();
            if(!$this->TrnShipping->save($TrnShipping)){
                $this->TrnShipping->rollback();
                $this->Session->setFlash('出荷登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('/');
            }
            $retrun_array[$i] = $this->TrnShipping->getLastInsertID();
        }

        return $retrun_array;
    }

    /**
     * 直納品登録：全データ作成後のシール更新
     */
    private function updateD2TrnSticker($data){
        foreach ($data['trn_sticker_id'] as $key => $trn_sticker_id) {
            $update  = " UPDATE trn_stickers ";
            $update .= " SET trn_shipping_id = " . $data['trn_shipping_id'][$key];
            $update .= " WHERE id = " . $trn_sticker_id;

            $ret = $this->TrnSticker->query($update, false, false);
        }
    }

    /**
     * 直納品履歴検索
     */
    private function getD2promisesHistory($in_where, $limit=null){
        // SELECT 句
        $select  = '   a.id                                          as "d2promises__trn_order_header_id"';
        $select .= ' , a.work_no                                     as "d2promises__work_no"';
        $select .= ' , to_char(a.work_date, \'YYYY/mm/dd\')          as "d2promises__work_date"';
        $select .= ' , g.facility_name                               as "d2promises__facility_name"';
        $select .= ' , g2.facility_name                              as "d2promises__supplier_name"';
        $select .= ' , f.department_name                             as "d2promises__department_name" ';
        $select .= ' , a.recital                                     as "d2promises__recital"';
        $select .= ' , count(*)                                      as "d2promises__count"';
        $select .= ' , a.detail_count                                as "d2promises__detail_count" ';
        $select .= ' , i.user_name                                   as "d2promises__user_name" ';
        $select .= ' , to_char(a.created, \'YYYY/MM/DD HH24:MI:DD\') as "d2promises__created" ';

        // FROM 句
        $from  = ' trn_order_headers as a  ';
        $from .= ' left join trn_orders as b  ';
        $from .= '   on b.trn_order_header_id = a.id  ';
        $from .= ' left join mst_item_units as c  ';
        $from .= '   on c.id = b.mst_item_unit_id  ';
        $from .= ' left join mst_facility_items as d  ';
        $from .= '   on d.id = c.mst_facility_item_id  ';
        $from .= ' left join mst_departments as f  ';
        $from .= '   on f.id = a.department_id_from  ';
        $from .= ' left join mst_facilities as g  ';
        $from .= '   on g.id = f.mst_facility_id  ';
        $from .= ' left join mst_departments as f2  ';
        $from .= '   on f2.id = a.department_id_to2  ';
        $from .= ' left join mst_facilities as g2  ';
        $from .= '   on g2.id = f2.mst_facility_id  ';
        $from .= ' left join mst_dealers as h ';
        $from .= '   on h.id = d.mst_dealer_id';
        $from .= ' left join mst_users as i ';
        $from .= '   on i.id = a.creater';

        // WHERE 句
        $where  = ' a.order_type = ' . Configure::read('OrderTypes.direct');
        $where .= ' and g.facility_type = ' . Configure::read('FacilityType.hospital');
        $where .= $in_where;

        // GROUP BY
        $group  = ' a.id ';
        $group .= ' , a.work_no ';
        $group .= ' , a.work_date ';
        $group .= ' , b.created ';
        $group .= ' , g.facility_name ';
        $group .= ' , g2.facility_name ';
        $group .= ' , f.department_name ';
        $group .= ' , a.detail_count ';
        $group .= ' , a.recital  ';
        $group .= ' , i.user_name ';
        $group .= ' , a.created ';

        // SQL実行オプション
        $option = array(
            'where' => $where,
            'group' => $group,
            'order' => ' a.work_no ',
            'limit' => $limit
        );

        // 件数集計付きで実行
        $result = $this->Vesta->customQuery($select, $from, $option, true);

        if(!is_null($limit)){
            $this->set('max' , $result['count']);
        }

        return $result['query'];
    }

    /**
     * 直納品履歴明細検索
     */
    private function getD2promisesHistoryDetail($where){
        $sql  = ' select ';
        $sql .= '     l.id                as "d2promises__id"';
        $sql .= '   , a.work_no           as "d2promises__work_no"';
        $sql .= "   , to_char(a.work_date, 'YYYY/mm/dd') ";
        $sql .= '                         as "d2promises__work_date"';
        $sql .= '   , g.facility_name     as "d2promises__facility_name"';
        $sql .= '   , f.department_name   as "d2promises__department_name" ';
        $sql .= '   , e.user_name         as "d2promises__user_name"';
        $sql .= "   , to_char(b.created, 'YYYY/mm/dd HH24:MI:SS') ";
        $sql .= '                         as "d2promises__created"';
        $sql .= '   , b.recital           as "d2promises__recital"';
        $sql .= '   , d.internal_code     as "d2promises__internal_code"';
        $sql .= '   , d.item_name         as "d2promises__item_name"';
        $sql .= '   , d.item_code         as "d2promises__item_code"';
        $sql .= '   , d.jan_code          as "d2promises__jan_code"';
        $sql .= '   , d.standard          as "d2promises__standard"';
        $sql .= '   , h.dealer_name       as "d2promises__dealer_name"';
        $sql .= '   , ( case ';
        $sql .= '       when c.per_unit = 1 then c1.unit_name ';
        $sql .= "       else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')' ";
        $sql .= '       end )             as "d2promises__unit_name"';
        $sql .= '   , ( case ';
        $sql .= '       when m.is_deleted = true then false ';
        $sql .= '       when m.shipping_status = ' . Configure::read('ShippingStatus.loaded') . ' then false ';
        $sql .= '       else true ';
        $sql .= '       end )             as "d2promises__canCancel"';
        $sql .= '   , i.name              as "d2promises__class_name"';
        $sql .= '   , b.quantity          as "d2promises__quantity"';
        $sql .= '   , k.lot_no            as "d2promises__lot_no" ';
        $sql .= "   , to_char(l.validated_date, 'YYYY/mm/dd') ";
        $sql .= '                         as "d2promises__validated_date" ';
        $sql .= '   , l.transaction_price as "d2promises__transaction_price" ';
        $sql .= '   , l.sales_price       as "d2promises__sales_price" ';
        $sql .= '   , coalesce( m.hospital_sticker_no, l.hospital_sticker_no ) ';
        $sql .= '                         as "d2promises__hospital_sticker_no" ';
        $sql .= '   , n.is_retroactable   as "d2promises__is_retroactable" ';
        $sql .= '   , ( case when m.is_deleted = true then \'＜取消済み＞\' ';
        $sql .= '            when m.shipping_status = ' . Configure::read('ShippingStatus.loaded') . ' then \'＜受領済み＞\' ';
        $sql .= '            else null ';
        $sql .= '       end )             as "d2promises__recital2" ';
        $sql .= ' from ';
        $sql .= '   trn_order_headers as a  ';
        $sql .= '   left join trn_orders as b  ';
        $sql .= '     on b.trn_order_header_id = a.id  ';
        $sql .= '   left join mst_item_units as c  ';
        $sql .= '     on c.id = b.mst_item_unit_id  ';
        $sql .= '   left join mst_unit_names as c1  ';
        $sql .= '     on c1.id = c.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as c2  ';
        $sql .= '     on c2.id = c.per_unit_name_id  ';
        $sql .= '   left join mst_facility_items as d  ';
        $sql .= '     on d.id = c.mst_facility_item_id  ';
        $sql .= '   left join mst_users as e  ';
        $sql .= '     on e.id = b.creater  ';
        $sql .= '   left join mst_departments as f  ';
        $sql .= '     on f.id = a.department_id_from  ';
        $sql .= '   left join mst_facilities as g  ';
        $sql .= '     on g.id = f.mst_facility_id  ';
        $sql .= '   left join mst_dealers as h ';
        $sql .= '     on h.id = d.mst_dealer_id';
        $sql .= '   left join mst_classes as i ';
        $sql .= '     on i.id = b.work_type ';
        $sql .= '   left join trn_edi_receivings j ';
        $sql .= '     on j.trn_order_id = b.id ';
        $sql .= '   left join trn_edi_shippings k ';
        $sql .= '     on k.trn_edi_receiving_id = j.id ';
        $sql .= '   left join trn_stickers l ';
        $sql .= '     on l.trn_order_id = b.id ';
        $sql .= '   left join trn_shippings m ';
        $sql .= '     on m.trn_sticker_id = l.id';
        $sql .= '   left join trn_receivings n ';
        $sql .= '     on n.id = l.trn_receiving_id ';
        $sql .= ' where ';
        $sql .= '   a.order_type = ' . Configure::read('OrderTypes.direct');
        $sql .= '   and g.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= $where;
        $sql .= ' order by ';
        $sql .= '   b.work_no ';
        $sql .= ' , b.work_seq ';
        $sql .= ' , m.hospital_sticker_no ';
        $sql .= ' , l.hospital_sticker_no';

        $result = $this->TrnOrder->query($sql , false , false);
        return $result;
    }

    /**
     * 直納品履歴取消：シールと対になってる明細
     * @return $after_updates 後続処理で使うカラム達
     */
    public function delD2DetailInSeal ($cancel) {
        // 明細取消による数量調整を行う テーブル => array( Key => Value )
        $after_updates = array();
        foreach ($cancel as $sticker_id) {
            $line = array();

            // シール データ取得
            $sql = " select * from trn_stickers where id  = " . $sticker_id;
            $tmp = $this->Vesta->query($sql);
            $sticker_data = $tmp[0][0];

            // 出荷（センタ）
            $tmp = $this->Vesta->lockAfterDel('TrnShipping',
                                              array('trn_sticker_id' => $sticker_data['id']),
                                              array('trn_shipping_header_id'));
            if ($tmp == false) {
                return false;
            } else {
                $line['trn_shipping_header_id'] = $tmp[0][0]['trn_shipping_header_id'];
            }

            // シール
            if (!$this->Vesta->lockAfterDel('TrnSticker', array('id' => $sticker_data['id']))) {
                return false;
            }

            // 入庫明細
            $tmp = $this->Vesta->lockAfterDel('TrnStorage',
                                              array('id' => $sticker_data['trn_storage_id']),
                                              array('trn_storage_header_id'));
            if ($tmp == false) {
                return false;
            } else {
                $line['trn_storage_header_id']= $tmp[0][0]['trn_storage_header_id'];
            }

            // 数量変更などでのちほど利用する
            $line['trn_order_id']        = $sticker_data['trn_order_id'];
            $line['trn_storage_id']      = $sticker_data['trn_storage_id'];
            $line['trn_receiving_id']    = $sticker_data['trn_receiving_id'];
            $line['trn_edi_shipping_id'] = $sticker_data['trn_edi_shipping_id'];

            $after_updates[] = $line;
        }

        // 最後まで生き残れば成功
        return $after_updates;
    }

    /**
     * 直納品取消：各明細の数量更新、0なら削除
     * @param  $detail 取消を実施したデータの詳細情報
     * @return true : false
     */
    public function updateD2DetailQuantity ($detail) {
        if (is_null($detail) || !is_array($detail)) {
            return false;
        }

        $is_deleted = false;
        if ($detail['quantity'] <= 0) {
            // 0なら削除
            $is_deleted = true;
        }

        $headers = array();
        $update  = array('quantity' => $detail['quantity'], 'is_deleted' => $is_deleted);

        // 入荷明細
        $res = $this->Vesta->lockAfterUpdate('TrnReceiving', array('id' => $detail['trn_receiving_id']),
                                             $update, array('trn_receiving_header_id'));
        if ($res == false) {
            return false;
        } else {
            $headers['trn_receiving_header_id'] = $res[0][0]['trn_receiving_header_id'];
        }

        // 業者出荷明細
        $res = $this->Vesta->lockAfterUpdate('TrnEdiShipping', array('id' => $detail['trn_edi_shipping_id']),
                                             $update, array('trn_edi_shipping_header_id'));
        if ($res == false) {
            return false;
        } else {
            $headers['trn_edi_shipping_header_id'] = $res[0][0]['trn_edi_shipping_header_id'];
        }

        // 受注明細
        $res = $this->Vesta->lockAfterUpdate('TrnEdiReceiving', array('trn_order_id' => $detail['trn_order_id']),
                                             $update, array('trn_edi_receiving_header_id'));
        if ($res == false) {
            return false;
        } else {
            $headers['trn_edi_receiving_header_id'] = $res[0][0]['trn_edi_receiving_header_id'];
        }

        // 発注明細
        $res = $this->Vesta->lockAfterUpdate('TrnOrder', array('id' => $detail['trn_order_id']),
                                             $update, array('trn_order_header_id'));
        if ($res == false) {
            return false;
        } else {
            $headers['trn_order_header_id'] = $res[0][0]['trn_order_header_id'];
        }

        // ここまでくれば成功
        return $headers;
    }

    /**
     * 直納品取消：ヘッダー調整
     */
    public function delD2Headers ($data) {
        if (is_null($data)) {
            return false;
        }

        // 取消を行うヘッダテーブルと明細テーブルの情報
        $header_tables = array(
            // 出荷ヘッダ
            'TrnShippingHeader' => array(
                'parent' => 'trn_shipping_headers',
                'child'  => 'trn_shippings',
                'key'    => 'trn_shipping_header_id'
            ),
            // 入庫ヘッダ
            'TrnStorageHeader' => array(
                'parent' => 'trn_storage_headers',
                'child'  => 'trn_storages',
                'key'    => 'trn_storage_header_id'
            ),
            // 入荷ヘッダ
            'TrnReceivingHeader' => array(
                'parent' => 'trn_receiving_headers',
                'child'  => 'trn_receivings',
                'key'    => 'trn_receiving_header_id'
            ),
            // 業者出荷ヘッダ
            'TrnEdiShippingHeader' => array(
                'parent' => 'trn_edi_shipping_headers',
                'child'  => 'trn_edi_shippings',
                'key'    => 'trn_edi_shipping_header_id'
            ),
            // 業者受注ヘッダ
            'TrnEdiReceivingHeader' => array(
                'parent' => 'trn_edi_receiving_headers',
                'child'  => 'trn_edi_receivings',
                'key'    => 'trn_edi_receiving_header_id'
            ),
            // 発注ヘッダ
            'TrnOrderHeader' => array(
                'parent' => 'trn_order_headers',
                'child'  => 'trn_orders',
                'key'    => 'trn_order_header_id'
            ),
        );

        // 実行
        foreach ($header_tables as $model => $table) {
            if (!$this->execDelHeader($model, $table, $data)) {
                return false;
            }
        }

        // ここまでくれば成功
        return true;
    }

    /**
     * 直納品履歴：ヘッダ取消実行部分
     */
    public function execDelHeader ($model, $table, $data) {
        if (empty($data[$table['key']])) {
            // 削除対象が無い場合
            return true;
        }

        foreach ($data[$table['key']] as $id) {
            $option = array('where' => $table['key'] . ' = ' . $id . ' AND is_deleted = false');
            $count = $this->Vesta->getRecordCount($table['child'], $option);

            // 一つでも有効なものがあれば削除対象外
            if ($count > 0) {
                continue;
            }

            // 削除実施
            if (!$this->Vesta->lockAfterDel($model, array('id' => $id))) {
                return false;
            }
        }

        return true;
    }

    function getItemUnit($facility_item_id , $supplier_id, $hospital_id,$work_date){
        $sql  = ' select ';
        $sql .= '     a.id                as "MstItemUnit__id"';
        $sql .= '   , ( case  ';
        $sql .= '       when a.per_unit = 1  ';
        $sql .= '       then a1.unit_name  ';
        $sql .= "       else a1.unit_name || '(' || a.per_unit || a2.unit_name || ')'  ";
        $sql .= '       end )             as "MstItemUnit__unit_name" ';
        $sql .= '   , b.sales_price       as "MstItemUnit__sales_price"';
        $sql .= '   , c.transaction_price as "MstItemUnit__transaction_price" ';
        $sql .= ' from ';
        $sql .= '   mst_item_units as a  ';
        $sql .= '   left join mst_unit_names as a1  ';
        $sql .= '     on a1.id = a.mst_unit_name_id  ';
        $sql .= '   left join mst_unit_names as a2  ';
        $sql .= '     on a2.id = a.per_unit_name_id  ';
        $sql .= '   inner join mst_sales_configs as b  ';
        $sql .= '     on b.mst_item_unit_id = a.id  ';
        $sql .= '     and b.partner_facility_id = ' . $hospital_id;
        $sql .= '     and b.is_deleted = false  ';
        $sql .= "     and b.start_date <= '" . $work_date . "'";
        $sql .= "     and b.end_date > '" . $work_date . "'";
        $sql .= '   inner join mst_transaction_configs as c  ';
        $sql .= '     on c.mst_item_unit_id = a.id  ';
        $sql .= '     and c.partner_facility_id = ' . $supplier_id;
        $sql .= '     and c.is_deleted = false  ';
        $sql .= "     and c.start_date <= '" . $work_date . "'";
        $sql .= "     and c.end_date > '" . $work_date . "'";
        $sql .= ' where ';
        $sql .= '   a.mst_facility_item_id = ' . $facility_item_id;
        $sql .= ' order by ';
        $sql .= '   a.per_unit desc ';
        $ret = $this->MstFacilityItem->query($sql,false,false);
        return $ret;
    }
        
}
?>
