<?php
/**
 * LoginController
 *
 * @version 1.0.0
 * @since 2010/05/17
 */

class LoginController extends AppController {
    var $name = 'Login';

    /**
     *
     * @var array $uses
   */
    var $uses = array(
            'MstUser',
            'MstDepartment',
            'MstFacility',
            'MstFacilityOption',
            'MstRole',
            'MstUserBelonging',
            'MstNumber',
            'MstEdition',
            'MstPlan',
            'TrnClaimSpecifications',
            'TrnOrder',
            'TrnInformation',
            'TrnRequestedRfp',
            'TrnRfpHeader',
    );

    /**
     * @var AuthComponent
     */
    var $Auth;
    /**
     * @var SessionComponent
     */
    var $Session;

    public $components = array('Rfp');

    function beforeFilter() {
        //ログインを必要としない処理の設定
        $this->Auth->allow('login', 'logout');
        //ログイン後にリダイレクトするURL
        $this->Auth->autoRedirect = false;
        //ユーザIDとパスワードがあるmodelを指定(’User’がデフォルト)
        //$this->Auth->userModel = 'MstUser';
        //ユーザIDとパスワードのフィールドを指定（username、password がデフォルト)
        $this->Auth->fields = array('username'=>'login_id','password'=> 'password');
        //Authコンポーネントでの暗号化をせず、MstUserモデルで定義したものを使う
        if(Configure::read('Password.Security') == 0 ){
            $this->Auth->authenticate = ClassRegistry::init('MstUser');
        }else{
            Security::setHash('md5');
        }
    }

    function index(){
        $this->home();
    }

    /**
     *
     */
    function home() {
        $user = $this->Auth->user();
        $this->set('user', $user);
        $info = array();
        $selectedFacilityId = $this->Session->read('Auth.facility_id_selected');

        //お知らせ情報取得
        $sql  = ' select ';
        $sql .= "       to_char(a.start_date , 'YYYY年mm月dd日' ) || ";
        $sql .= "     '(' || (  ";
        $sql .= "     case to_char(now(), 'ID')  ";
        $sql .= "       when '0' then '日'  ";
        $sql .= "       when '1' then '月'  ";
        $sql .= "       when '2' then '火'  ";
        $sql .= "       when '3' then '水'  ";
        $sql .= "       when '4' then '木'  ";
        $sql .= "       when '5' then '金'  ";
        $sql .= "       when '6' then '土'  ";
        $sql .= '       end ';
        $sql .= "   )  || ')'";
        $sql .= '                     as "TrnInformation__start_date" , ';
        $sql .= '       a.title       as "TrnInformation__title" , ';
        $sql .= '       a.message     as "TrnInformation__message" ';
        $sql .= '   from ';
        $sql .= '     trn_informations as a  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.start_date <= now()::date ';
        $sql .= '     and ( a.end_date >= now()::date or a.end_date is null ) ';
        $sql .= '     and ( a.mst_facility_id = ' . $selectedFacilityId;
        $sql .= '           or a.mst_facility_id is null ) ';
        $sql .= '   order by ';
        $sql .= '     a.start_date desc ';
        $sql .= '     , a.id desc';

        $info =  $this->TrnInformation->query($sql);

        //特定文字をパスに置換
        foreach($info as &$i){
            $i['TrnInformation']['message'] = preg_replace('/<@WEBROOT>/i', $this->webroot , $i['TrnInformation']['message']);
        }
        unset($i);
        $this->set('info' , $info);
        if( $this->Session->read('Auth.MstUser.user_type') == Configure::read('UserType.hospital') ){
            // 発注状況
            $sql  = ' select';
            $sql .= '       sum(a.quantity)       as "Order__count"';
            $sql .= '     , sum(a.stocking_price) as "Order__price"';
            $sql .= '   from';
            $sql .= '     trn_orders as a ';
            $sql .= '   where';
            $sql .= "     to_char(a.work_date, 'MM') = to_char(now(), 'MM') ";
            $sql .= '     and a.is_deleted = false ';
            $sql .= '     and a.trn_order_header_id is not null ';

            $tmp = $this->TrnOrder->query($sql);
            if(empty($tmp)){
                $order = array(
                    'Order' => array(
                        'count' => 0 ,
                        'price' => 0
                        )
                    );
            }else{
                $order = $tmp[0];
            }
            $order['Order']['price'] = number_format($order['Order']['price'],0);

            $this->set('Order' , $order);

            // 部署件数
            $sql  = ' select ';
            $sql .= '       count(*)                       as count  ';
            $sql .= '   from ';
            $sql .= '     mst_users as a  ';
            $sql .= '     left join mst_facility_relations as b  ';
            $sql .= '       on a.mst_facility_id = b.mst_facility_id  ';
            $sql .= '     left join mst_facilities as c  ';
            $sql .= '       on c.id = b.partner_facility_id  ';
            $sql .= '       and c.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .= '     left join mst_departments as d  ';
            $sql .= '       on d.mst_facility_id = c.id  ';
            $sql .= '   where ';
            $sql .= '     a.id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '     and d.is_deleted = false ';
            $ret = $this->MstUser->query($sql);
            $this->set('CntDepartment', $ret[0][0]['count']);

            // ユーザ件数
            $sql  = ' select ';
            $sql .= '       count(*)                       as count  ';
            $sql .= '   from ';
            $sql .= '     mst_users as a  ';
            $sql .= '     left join mst_facility_relations as b  ';
            $sql .= '       on a.mst_facility_id = b.mst_facility_id  ';
            $sql .= '     left join mst_facilities as c  ';
            $sql .= '       on c.id = b.partner_facility_id  ';
            $sql .= '       and c.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .= '     left join mst_departments as d  ';
            $sql .= '       on d.mst_facility_id = c.id  ';
            $sql .= '     inner join mst_users as e  ';
            $sql .= '       on e.mst_department_id = d.id  ';
            $sql .= '     inner join mst_roles as f  ';
            $sql .= '       on e.mst_role_id = f.id  ';
            $sql .= '   where ';
            $sql .= '     a.id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '     and e.user_type = ' . Configure::read('UserType.hospital');
            $sql .= '     and e.is_deleted = false ';
            $sql .= '     and f.mst_role_type_id = ' . Configure::read('role.type.catalog');

            $ret = $this->MstUser->query($sql);
            $this->set('CntUser', $ret[0][0]['count']);

            // 仕入先
            $sql  = ' select ';
            $sql .= '       count(*) as count   ';
            $sql .= '  from ';
            $sql .= '    mst_facility_relations as a  ';
            $sql .= '    left join mst_facilities as b  ';
            $sql .= '      on b.id = a.partner_facility_id  ';
            $sql .= '  where ';
            $sql .= '    a.mst_facility_id = ' . $selectedFacilityId;
            $sql .= '    and b.facility_type = ' . Configure::read('FacilityType.supplier');
            $sql .= '    and b.is_deleted = false ';

            $ret = $this->MstUser->query($sql);
            $this->set('CntSupplier', $ret[0][0]['count']);

            // 発注未確定
            $sql  = ' select ';
            $sql .= '       count(*) as count   ';
            $sql .= '  from ';
            $sql .= '    trn_orders as a  ';
            $sql .= '  where ';
            $sql .= '    a.center_facility_id = ' . $selectedFacilityId;
            $sql .= '    and a.is_deleted = false ';
            $sql .= "    and a.work_no = '' ";

            $ret = $this->MstUser->query($sql);
            $this->set('CntOrder', $ret[0][0]['count']);

            // 未入荷件数
            $sql  = ' select ';
            $sql .= '       count(*) as count   ';
            $sql .= '  from ';
            $sql .= '    trn_orders as a  ';
            $sql .= '  where ';
            $sql .= '    a.center_facility_id = ' . $selectedFacilityId;
            $sql .= '    and a.is_deleted = false ';
            $sql .= "    and a.work_no != '' ";
            $sql .= '    and a.remain_count > 0 ';

            $ret = $this->MstUser->query($sql);
            $this->set('CntReceiving', $ret[0][0]['count']);

            // 未入荷件数(1週間)
            $sql  = ' select ';
            $sql .= '       count(*) as count   ';
            $sql .= '  from ';
            $sql .= '    trn_orders as a  ';
            $sql .= '  where ';
            $sql .= '    a.center_facility_id = ' . $selectedFacilityId;
            $sql .= '    and a.is_deleted = false ';
            $sql .= "    and a.work_no != '' ";
            $sql .= '    and a.remain_count > 0 ';
            $sql .= '    and a.work_date < now()::date - 7';

            $ret = $this->MstUser->query($sql);
            if(empty($ret[0][0]['count'])){
                $ret[0][0]['count'] = 0;
            }
            $this->set('CntOneWeekReceiving', $ret[0][0]['count']);

            /*
            // コスロスマスタ件数
            $sql  = ' select ';
            $sql .= '       count(*) as count   ';
            $sql .= '  from ';
            $sql .= '    mst_grand_kyo_materials as a  ';

            $ret = $this->MstUser->query($sql);
            $this->set('CntCoslosMaster', $ret[0][0]['count']);
            */

            /*
            // 見積り件数（期限前）
            $sql  = ' select ';
            $sql .= '       count(*) count  ';
            $sql .= '   from ';
            $sql .= '     trn_rfp_headers as a  ';
            $sql .= '   where ';
            $sql .= '     a.is_deleted = false  ';
            $sql .= '     and a.limit_date > now() ';
            $ret = $this->MstUser->query($sql);
            $this->set('CntRfp', $ret[0][0]['count']);

            // 見積り件数（期限越え）
            $sql  = ' select ';
            $sql .= '       count(*) count  ';
            $sql .= '   from ';
            $sql .= '     trn_rfp_headers as a  ';
            $sql .= '   where ';
            $sql .= '     a.is_deleted = false  ';
            $sql .= '     and a.limit_date < now() ';
            $ret = $this->MstUser->query($sql);
            $this->set('CntLimitRfp', $ret[0][0]['count']);
            */

            //　延伸可能金額
            $extensionUpperResult = $this->search_extensionUpper($selectedFacilityId);
            $extensionUpper = number_format($extensionUpperResult);
            $this->set('extensionUpper', $extensionUpper);

            // あとXX日
            if( date('d') > Configure::read('Zura.closeday')){
                $close = mktime(0, 0, 0, date("m")+1, Configure::read('Zura.closeday'),   date("Y"));
            }else{
                $close= mktime(0, 0, 0, date("m"), Configure::read('Zura.closeday'),   date("Y"));
            }
            $day = ceil(($close - time()) / (24 * 60 * 60)) + 1;
            $this->set('Day',$day);
        }

        $menuGroups = $this->AuthSession->getMenuGroups(false);
        $this->set('menuGroups', $menuGroups);
        $this->set('masterItemInfo', $this->Session->read('Auth.masterItemInfo'));

        if ($this->AuthSession->isEdiUserRole()) {
            $data = $this->Rfp->findRfpHeaderDataForFacility($selectedFacilityId);
            foreach ($data as &$d) {
                $rfpHeader = &$d['TrnRfpHeader'];
                $rfpHeader['report_url'] = '/' . $this->Rfp->getRfpRelativePath($rfpHeader, '/');
                unset($rfpHeader);
            }
            unset($d);
            $this->set('data', $data);
            $this->render('home_edi');
            return;
        }
    }

    /**
     * login
     * AuthComponent用に空メソッド
     *
     */
    function login() {
        if ($this->Session->check('Auth.MstUser')) {
            $this->redirect($this->Auth->redirect());
            return;
        }

        // ログイン処理(POSTの場合のみ）
        if (!$this->request->is('post')) return;

        // ログイン失敗
        if (!$this->Auth->login() || $this->Auth->user('is_deleted')) {
            $this->Session->setFlash('ログイン名、パスワードに誤りがあります。', 'growl', array('type'=>'star') );
            $this->redirect($this->Auth->logout());
            return;
        }


        // ログインユーザのプロファイルキャッシュを更新
        $profile = $this->AuthSession->updateProfileOfLoginUser($this->Auth->user('id'));
        if (empty($profile) || $profile['MstUser']['is_deleted'] === true) {
            // 削除済みの場合はログイン不可
            $this->Session->setFlash('このログインIDではログインできません。', 'growl', array('type'=>'star') );
            $this->redirect($this->Auth->logout());
            return;
        }

        //有効期限チェック
        $sql  = ' select ';
        $sql .= '       (  ';
        $sql .= '       case  ';
        $sql .= '         when a.is_indefinite = true '; //無制限フラグ
        $sql .= '         then 0 ';
        $sql .= '         when a.is_temp_issue = true '; //仮発行
        $sql .= '         then 1 ';
        $sql .= '         when a.effective_date <= now() '; //有効期限切れ
        $sql .= '         then 1 ';
        $sql .= '         when a.effective_date::date <= now()::date + ' . Configure::read('UserPassword.AlertDay'); //有効期限警告
        $sql .= '         then 2 ';
        $sql .= '         else 0 ';
        $sql .= '         end ';
        $sql .= '     ) as "Login__status" ';
        $sql .= '   from ';
        $sql .= '     mst_users as a ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->Auth->user('id');

        $check = $this->MstUser->query($sql);

        switch ($check[0]['Login']['status']) {
            case 1:
                $this->redirect('loginException');
                break;
            case 2:
                if(!$this->Session->check('Login.is_nonAlert')){
                    $this->redirect('loginAlert');
                    break;
                }
        }

        $this->AuthSession->updateEnableMenu(true);
        $this->redirect($this->Auth->redirect());
    }

    /**
     * ログアウト処理
     * @access public
     * @return void
     */
    function logout() {
        $this->Session->destroy(); // セッション破棄
        $this->redirect($this->Auth->logout()); // リダイレクト
    }

    /**
     * 操作施設変更
     * @access public
     * @param integer $_id
     * @return void
     */
    function change_facility($_id = null) {
        $this->AuthSession->updateSelectedFacilitiyId($_id);
        $this->redirect($this->Auth->redirect());
    }


    function download($filetitle = null){
        $this->autoRender = false;
        if($filetitle == "saveX"){
            $filename=$filetitle . ".zip";
        }elseif ($filetitle == "setup"){
            $filename=$filetitle . ".exe";
        }
        $result = file_get_contents(WWW_ROOT . $filename);

        Configure::write('debug',0);
        header("Content-disposition: attachment; filename=" . $filename);
        header("Content-type: application/octet-stream; name=" . $filename);
        print($result);
        return;
    }

    /**
     * ログイン期限切れ
     */
    function loginException(){
        $this->AuthSession->deleteAuthSession();
    }

    /**
     * ログイン警告
     */
    function loginAlert() {
        //有効期限取得
        $sql  = ' select ';
        $sql .= "      to_char(a.effective_date, 'YYYY/mm/dd')  ";
        $sql .= '                   as "MstUser__effective_date"';
        $sql .= '   from ';
        $sql .= '     mst_users as a  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->Auth->user('id');
        $ret = $this->MstUser->query($sql);
        $this->set('effective_date' , $ret[0]['MstUser']['effective_date']);
        //続行押下用にフラグを立てておく
        $this->Session->write('Login.is_nonAlert' , true);
    }

    /**
     * パスワード変更画面
     */
    function loginPasswordChange(){
        $this->Session->delete('Login.is_nonAlert');
    }

    /**
     * パスワード変更完了画面
     */
    function loginPasswordResult(){
        //ログインパスワード確認
        $sql  = ' select ';
        $sql .= '       password  ';
        $sql .= '     , effective_date ';
        $sql .= '   from ';
        $sql .= '     mst_users as a  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' .$this->request->data['MstUser']['id'];

        $ret = $this->MstUser->query($sql);

        if($ret[0][0]['password'] != $this->getHashPassword($this->request->data['MstUser']['old_password']) ){
            //一致せず、エラー
            $this->Session->setFlash('現行のパスワードが一致しません。', 'growl', array('type'=>'error') );
            //入力画面にリダイレクト
            $this->redirect('login/loginPasswordChange');
        }

        $now = date('Y/m/d H:i:s.u');
        $user_data = array();

        //トランザクションを開始
        $this->MstUser->begin();
        //行ロック
        $this->MstUser->query( ' select * from mst_users as a where a.id = ' . $this->request->data['MstUser']['id'] . ' for update ');

        $user_data['MstUser']['id']                = $this->request->data['MstUser']['id'];
        $user_data['MstUser']['password']          = $this->getHashPassword($this->request->data['MstUser']['password']);
        $user_data['MstUser']['modifier']          = $this->Session->read('Auth.MstUser.id');
        $user_data['MstUser']['modified']          = $now;
        $user_data['MstUser']['password_modified'] = $now;
        $user_data['MstUser']['effective_date']    = $this->getNextEffectiveDate($ret[0][0]['effective_date'] , true );
        $user_data['MstUser']['is_temp_issue']     = false;

        if(!$this->MstUser->save($user_data)){
            //ロールバック
            $this->MstUser->rollback();
            //エラーメッセージ
            $this->Session->setFlash('パスワードの更新に失敗しました。', 'growl', array('type'=>'error') );
            //リダイレクト
            $this->redirect('login/loginPasswordChange');
        }
        //コミット
        $this->MstUser->commit();
    }

    /**
     * 延伸上限金額検索
     */
    function search_extensionUpper($facility_id){
        $paymentDueDate = $this->get_paymentDueDate();

        $presentmonth_minDate = date("Y-m-01", strtotime($paymentDueDate.""));
        $presentmonth_maxDate = date("Y-m-t", strtotime($paymentDueDate.""));
        $_1monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -1 Month"));
        $_1monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -1 Month"));
        $_2monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -2 Month"));
        $_2monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -2 Month"));
        $_3monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -3 Month"));
        $_3monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -3 Month"));
        $_4monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -4 Month"));
        $_4monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -4 Month"));
        $_5monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -5 Month"));
        $_5monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -5 Month"));
        $_6monthLater_minDate = date("Y-m-01", strtotime($paymentDueDate." -6 Month"));
        $_6monthLater_maxDate = date("Y-m-t", strtotime($paymentDueDate." -6 Month"));

        $sql  = ' SELECT SUM(TrnClaim.claim_price) AS "TrnClaim__extension_price" ';
        $sql .= '       ,MstFacility.zura_maximum_amount AS "MstFacility__zura_maximum_amount" ';
        $sql .= '   FROM trn_claims AS TrnClaim';
        $sql .= '  INNER JOIN mst_departments AS MstDepartment';
        $sql .= '     ON TrnClaim.department_id_to = MstDepartment.id';
        $sql .= '  INNER JOIN mst_facilities AS MstFacility';
        $sql .= '     ON MstDepartment.mst_facility_id = MstFacility.id';
        $sql .= '  INNER JOIN mst_item_units AS MstItemUnit';
        $sql .= '     ON TrnClaim.mst_item_unit_id = MstItemUnit.id';
        $sql .= '  INNER JOIN mst_facility_items AS MstFacilityItem';
        $sql .= '     ON MstItemUnit.mst_facility_item_id = MstFacilityItem.id';
        $sql .= '  INNER JOIN mst_unit_names AS MstUnitName';
        $sql .= '     ON MstUnitName.id = MstItemUnit.mst_unit_name_id ';
        $sql .= '   LEFT JOIN trn_claim_specifications AS TrnSpecifications';
        $sql .= '     ON TrnSpecifications.mst_facility_id = '.$facility_id;
        $sql .= '    ANd TrnSpecifications.is_deleted = false';
        $sql .= "    AND TrnSpecifications.closing_date BETWEEN '".$presentmonth_minDate."' AND '".$presentmonth_maxDate."'";
        //当月購入分　（延伸あり）
        $sql .= "  WHERE ((TrnClaim.claim_date BETWEEN '".$presentmonth_minDate."' AND '".$presentmonth_maxDate."'";
        $sql .= '              AND TrnClaim.zura_number_of_month > 0)';
        //過去購入分　（当月延伸）
        $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_1monthLater_minDate."' AND '".$_1monthLater_maxDate."'";
        $sql .= '              AND TrnClaim.zura_number_of_month >= 1)';
        $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_2monthLater_minDate."' AND '".$_2monthLater_maxDate."'";
        $sql .= '              AND TrnClaim.zura_number_of_month >= 2)';
        $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_3monthLater_minDate."' AND '".$_3monthLater_maxDate."'";
        $sql .= '              AND TrnClaim.zura_number_of_month >= 3)';
        $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_4monthLater_minDate."' AND '".$_4monthLater_maxDate."'";
        $sql .= '              AND TrnClaim.zura_number_of_month >= 4)';
        $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_5monthLater_minDate."' AND '".$_5monthLater_maxDate."'";
        $sql .= '              AND TrnClaim.zura_number_of_month >= 5)';
        $sql .= "          OR (TrnClaim.claim_date BETWEEN '".$_6monthLater_minDate."' AND '".$_6monthLater_maxDate."'";
        $sql .= '              AND TrnClaim.zura_number_of_month >= 6))';
        $sql .= '    AND MstFacility.id = '.$facility_id;
        $sql .= "    AND TrnClaim.is_stock_or_sale = '2'";
        $sql .= '    AND TrnClaim.is_deleted = false';
        $sql .= '    AND MstDepartment.is_deleted = false';
        $sql .= '    AND MstFacility.is_deleted = false';
        $sql .= '    AND MstItemUnit.is_deleted = false';
        $sql .= '    AND MstFacilityItem.is_deleted = false';
        $sql .= '    AND MstUnitName.is_deleted = false';
        $sql .= '  GROUP BY MstFacility.id';

        $result = $this->TrnClaimSpecifications->query($sql, false, false);
        $maximum_amount = 0;
        $extension_price = 0;

        if (isset($result[0]) == false || isset($result[0]['MstFacility']) == false || isset($result[0]['TrnClaim']) == false){
            return 0;
        }
        else {
            $maximum_amount = $result[0]['MstFacility']['zura_maximum_amount'];
            $extension_price = $result[0]['TrnClaim']['extension_price'];
            $maximum_amount <= $extension_price ? $extensionUpperLimit = 0 : $extensionUpperLimit = $maximum_amount - $extension_price;

            return $extensionUpperLimit;
        }
    }

    /**
     * 購入金額請求年月　取得
     */
    function get_paymentDueDate(){
        $now = date('Y-m-d 00:00:00');
        if(date('d', strtotime($now."")) > 10){
            return date('Y-m-01 00:00:00', strtotime($now.""));
        }else{
            return date('Y-m-01 00:00:00', strtotime($now." -1 Month"));
        }
    }
}
