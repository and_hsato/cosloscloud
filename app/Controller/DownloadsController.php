<?php
/**
 * download_Controller
 *　ファイルダウンロード　
 * @version 1.0.0
 * @since 2013/02/20
 */
class DownloadsController extends AppController {

    /**
     * @var $name
     */
    var $name = 'Download';

    /**
     * @var array $uses
     */

    var $uses = array(
        'MstItemUnit',
        'MstFacilityItem',
        'MstUserBelonging'
    );

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler');

    /**
     * @var $paginate
     */

    public function index(){
        //$this->setRoleFunction(24); //
        $this->search();
    }

    public function search(){
        $fileTypeList = array();
        $fileTypeList['01'] = 'シールデータ';
        $fileTypeList['02'] = '仕入データ';
        $fileTypeList['03'] = '売上データ';

        $this->set('fileTypes', $fileTypeList);


        $_SearchResult = array();
        App::import("sanitize");
        //検索ボタンが押されたら
        if(isset($this->request->data["IsSearch"]) && $this->request->data['IsSearch'] == '1'){
            //検索条件の取得
            $start_day = date('Ymd',strtotime($this->request->data['Download']['day_from']));

            $end_day   = date('Ymd',strtotime($this->request->data['Download']['day_to']));
            $file_type = $this->request->data['Download']['file_type'];

            $res_dir = opendir('./files');

            //ディレクトリ内のファイル名を１つずつを取得
            while( $file_name = readdir( $res_dir ) ){

                if($file_name == '.' || $file_name == '..') continue;

                $file_name = mb_convert_encoding($file_name, "UTF-8", "SJIS-win");
                echo $file_name;
            }
            closedir( $res_dir );
            exit;

        }else{

        }
        
        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set('data', $this->request->data);
        $this->set('SearchResult', $_SearchResult);
        $this->render('/downloads/index');
    }

    private function _filterFileCondition($start_day, $end_day, $file_type, $file_name) {
        //ファイル名をアンダーバーで分解して、ファイル名と日付を取得
        $fileInfoList = explode("_", $file_name);

        $type = $fileInfoList[0];
        $file = $fileInfoList[1];
        $date = $fileInfoList[2];
        
        $dateList = explode(".", $date);
        $date = date('Ymd',strtotime($dateList[0]));

        $resultList = array();

        if (isset($file_type) || $file_type == $type) {



        }

    }


}
