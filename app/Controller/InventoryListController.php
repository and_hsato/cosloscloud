<?php
/**
 * 在庫表
 *
 * @since 2010/10/17
 */
class InventoryListController extends AppController {
    var $name = 'InventoryList';
    var $uses = array(
        'MstFacility',
        'MstFacilityRelation',
        'TrnCloseHeader',
    );

    var $helpers = array('Form', 'Html', 'Time');
    var $components = array('RequestHandler');

    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
    }


    public function index(){
        $months = array();
        for($i=1;$i<=12;$i++){
            $months[(string)$i] = sprintf('%02d', $i);
        }
        $nowYear = date('Y', time());

        $years=$this->getTargetYear();

        if (count($years) == 0) {
            $years = array($nowYear => $nowYear,
                           (string)((integer)$nowYear - 1) => (string)((integer)$nowYear - 1),
                           (string)((integer)$nowYear - 2) => (string)((integer)$nowYear - 2),
            );
        }

        //表示用コンボリスト作成
        $this->set('facilities', $this->getFacilityList($this->Session->read('Auth.facility_id_selected') , array(Configure::read('FacilityType.center'))));
        $this->set('donor', $this->getFacilityList($this->Session->read('Auth.facility_id_selected') , array(Configure::read('FacilityType.donor'))));
        $this->set('years', $years);
        $this->set('months',$months);
    }

    /**
     *  在庫表出力
     * @access public
     * @return void
     */
    public function report(){
        $res = $this->getInventories();
        $columns = array(
            '行番号',
            '期間',
            '部門情報',
            '商品id',
            '商品名',
            '規格',
            '製品番号',
            '販売元',
            '課税区分',
            '施主コード',
            '施主名',
            '前残',
            '前残単価',
            '前残金額',
            '受入合計',
            '受入金額合計',
            '売上',
            '移動',
            '調整',
            '払出合計',
            '売上金額',
            '移動金額',
            '調整金額',
            '払出金額',
            '評価単価',
            '病院当残',
            '倉庫当残',
            '当残',
            '当残金額',
            '印刷年月日'
        );
        $this->layout = 'xml/default';
        $this->set('columns', $columns);
        $this->set('records',$this->outputFilter($res));
        $this->render('report');
    }


    /**
     * 在庫受入CSV出力
     * @access public
     * @return void
     */
    public function exportCsv(){
        $res = $this->getInventories();
        if(0 === count($res)){
            $this->Session->setFlash('出力対象のレコードがありませんでした', 'growl', array('type'=>'error'));
            $this->redirect('/inventory_list/index');
            return;
        }
        header("Content-type: application/octet-stream");
        header('Content-disposition: attachment;filename=' . mb_convert_encoding('在庫受払', 'SJIS-win', 'UTF-8') . '.csv');
        $columns = self::convertToSjis(array_keys($res[0]));
        print join("\t", $columns);
        print "\n";
        foreach($res as $row){
            $values = self::convertToSjis(array_values($row));
            print join("\t", $values);
            print "\n";
        }
        exit();
    }

    static public function convertToSjis($obj){
        if(is_array($obj)){
            return array_map(array('InventoryListController', 'convertToSjis'), $obj);
        }else{
            return mb_convert_encoding($obj, 'SJIS-win', 'UTF-8');
        }
    }

    private function getInventories(){
        $startDate = $this->request->data['Inventory']['year'] . '/' . $this->request->data['Inventory']['month'] . '/01';
        $endDate = date('Y/m/d', strtotime('1 month -1 day', strtotime($startDate)));
        $toDay = date('Y/m/d');
        $now = date('Y/m/d H:i:s');
        
        //施主
        if($this->request->data['Inventory']['donor_input'] != ''){
            $donorId = $this->getFacilityByCode($this->request->data['Inventory']['donor_input'] , array(Configure::read('FacilityType.donor')));
        }else{
            $donorId = null;
        }

        //施設
        if($this->request->data['Inventory']['facility_input'] != ''){
            $facilityId = $this->getFacilityByCode($this->request->data['Inventory']['facility_input'] , array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')));
        }else{
            $facilityId = $this->request->data['Inventory']['f'];
        }

        //サブコード
        if($this->request->data['Inventory']['subcode'] != ''){
            $subcode = $this->request->data['Inventory']['subcode'];
        }else{
            $subcode = null;
        }

        $res = $this->TrnCloseHeader->find('all', array(
            'recursive' => -1,
            'conditions' => array(
                'is_deleted' => false,
                'end_date' => $endDate,
                'mst_facility_id' => $facilityId,
            ),
            'fields' => array(
                'is_provisional',
                'end_date',
            ),
            'group' => array(
                'is_provisional',
                'end_date',
            ),
        ));

        if(count($res) === 0){
            $isProvisional = 'true';
        }elseif(count($res) === 1){
            $isProvisional = 'true';
        }else{
            $isProvisional = 'false';
        }

        //在庫表取得
        $res = $this->getInventoryListRecords(array(
            'startDate'       => $startDate,
            'endDate'         => $endDate,
            'isProvisional'   => $isProvisional,
            'facilityId'      => $facilityId,
            'subcode'         => $subcode,
            'donorId'         => $donorId,
        ),TRUE);

        // 0件の時は現在の在庫から金額を取得する。
        if(count($res) == 0){
            $sql  = ' select ';
            $sql .= '       row_number() over (order by c.internal_code)                                              as 行番号 ';
            $sql .= "     , '".$startDate."' || '～' || '".$toDay."'                                                  as 期間 ";
            $sql .= "     , ''                                                                                        as サブコード ";
            $sql .= "     , c.internal_code                                                                           as 商品id ";
            $sql .= "     , c.item_name                                                                               as 商品名 ";
            $sql .= "     , c.standard                                                                                as 規格 ";
            $sql .= "     , c.item_code                                                                               as 製品番号 ";
            $sql .= "     , d.dealer_name                                                                             as 販売元 ";
            $sql .= '     , (  ';
            $sql .= '       case c.tax_type  ';
            $sql .= "         when 1 then '課税' ";
            $sql .= "         when 0 then '非課税' ";
            $sql .= "         else '未設定' ";
            $sql .= '         end ';
            $sql .= '     )                                                                                           as 課税区分 ';
            $sql .= "     , e.facility_code                                                                           as 施主コード ";
            $sql .= "     , e.facility_name                                                                           as 施主名 ";
            $sql .= "     , sum(a.quantity * b.per_unit) + coalesce(i.count,0) - coalesce(g.count,0)                  as 前残 ";
            $sql .= "     ,  trunc( sum(a.transaction_price) / sum(a.quantity * b.per_unit) , 2)                      as 前残単価 ";
            $sql .= "     , (sum(a.quantity * b.per_unit) + coalesce(i.count,0) - coalesce(g.count,0)) * trunc( sum(a.transaction_price) / sum(a.quantity * b.per_unit) , 2) as 前残金額 ";
            $sql .= "     , coalesce(g.count,0)                                                                       as 受入合計 ";
            $sql .= "     , coalesce(g.price,0)                                                                       as 受入金額合計 ";
            $sql .= "     , coalesce(i.count,0)                                                                       as 売上 ";
            $sql .= "     , 0                                                                                         as 移動 ";
            $sql .= "     , 0                                                                                         as 調整 ";
            $sql .= "     , coalesce(h.count,0)                                                                       as 払出合計 ";
            $sql .= "     , coalesce(i.count,0) * trunc( sum(a.transaction_price) / sum(a.quantity * b.per_unit) , 2) as 売上金額 ";
            $sql .= "     , 0                                                                                         as 移動金額 ";
            $sql .= "     , 0                                                                                         as 調整金額 ";
            $sql .= "     , coalesce(h.count,0) * trunc( sum(a.transaction_price) / sum(a.quantity * b.per_unit) , 2) as 払出金額 ";
            $sql .= "     , trunc( sum(a.transaction_price) / sum(a.quantity * b.per_unit) , 2)                       as 評価単価 ";
            $sql .= '     , sum(  ';
            $sql .= '       case  ';
            $sql .= '         when f.department_type = 2  ';
            $sql .= '         then (a.quantity * b.per_unit)  ';
            $sql .= '         else 0  ';
            $sql .= '         end ';
            $sql .= '     )                                                                                           as 病院当残 ';
            $sql .= '     , sum(  ';
            $sql .= '       case  ';
            $sql .= '         when f.department_type <> 2  ';
            $sql .= '         then (a.quantity * b.per_unit)  ';
            $sql .= '         else 0  ';
            $sql .= '         end ';
            $sql .= '     )                                                                                           as 倉庫当残 ';
            $sql .= '     , sum(a.quantity * b.per_unit)                                                              as 当残 ';
            $sql .= '     , sum(a.transaction_price)                                                                  as 当残金額 ';
            $sql .= "     , '".$now."'                                                                                as 印刷日時  ";
            $sql .= '   from ';
            $sql .= '     trn_stickers as a  ';
            $sql .= '     left join mst_item_units as b  ';
            $sql .= '       on b.id = a.mst_item_unit_id  ';
            $sql .= '     left join mst_facility_items as c  ';
            $sql .= '       on c.id = b.mst_facility_item_id  ';
            $sql .= '     left join mst_dealers as d  ';
            $sql .= '       on d.id = c.mst_dealer_id  ';
            $sql .= '     left join mst_facilities as e  ';
            $sql .= '       on e.id = c.mst_owner_id  ';
            $sql .= '     left join mst_departments as f  ';
            $sql .= '       on f.id = a.mst_department_id  ';
            $sql .= '     left join (  ';
            $sql .= '       select ';
            $sql .= '             g2.mst_facility_item_id ';
            $sql .= '           , sum(g1.quantity)       as count ';
            $sql .= '           , sum(g1.stocking_price) as price';
            $sql .= '         from ';
            $sql .= '           trn_receivings as g1  ';
            $sql .= '           left join mst_item_units as g2  ';
            $sql .= '             on g2.id = g1.mst_item_unit_id  ';
            $sql .= '         where ';
            $sql .= '           g1.is_deleted = false ';
            $sql .= '           and g1.receiving_type = ' . Configure::read('ReceivingType.arrival');
            $sql .= "           and g1.work_date between '".$startDate."' and '".$toDay."' ";
            $sql .= '         group by ';
            $sql .= '           g2.mst_facility_item_id ';
            $sql .= '     ) as g  ';
            $sql .= '       on g.mst_facility_item_id = c.id  ';
            $sql .= '     left join (  ';
            $sql .= '       select ';
            $sql .= '             h2.mst_facility_item_id ';
            $sql .= '           , sum(h1.quantity)       as count ';
            $sql .= '         from ';
            $sql .= '           trn_shippings as h1  ';
            $sql .= '           left join mst_item_units as h2  ';
            $sql .= '             on h2.id = h1.mst_item_unit_id  ';
            $sql .= '         where ';
            $sql .= '           h1.is_deleted = false  ';
            $sql .= "           and h1.work_date between '".$startDate."' and '".$toDay."' ";
            $sql .= '         group by ';
            $sql .= '           h2.mst_facility_item_id ';
            $sql .= '     ) as h  ';
            $sql .= '       on h.mst_facility_item_id = c.id  ';
            $sql .= '     left join (  ';
            $sql .= '       select ';
            $sql .= '             i2.mst_facility_item_id ';
            $sql .= '           , sum(i1.quantity)       as count ';
            $sql .= '         from ';
            $sql .= '           trn_consumes as i1  ';
            $sql .= '           left join mst_item_units as i2  ';
            $sql .= '             on i2.id = i1.mst_item_unit_id  ';
            $sql .= '         where ';
            $sql .= '           i1.is_deleted = false  ';
            $sql .= "           and i1.work_date between '".$startDate."' and '".$toDay."' ";
            $sql .= '         group by ';
            $sql .= '           i2.mst_facility_item_id ';
            $sql .= '     ) as i  ';
            $sql .= '       on i.mst_facility_item_id = c.id  ';
            $sql .= '   where ';
            $sql .= '     a.is_deleted = false  ';
            $sql .= '     and a.quantity > 0  ';
            $sql .= '   group by ';
            $sql .= '     c.id ';
            $sql .= '     , c.internal_code ';
            $sql .= '     , c.item_name ';
            $sql .= '     , c.standard ';
            $sql .= '     , c.item_code ';
            $sql .= '     , d.dealer_name ';
            $sql .= '     , e.facility_code ';
            $sql .= '     , e.facility_name ';
            $sql .= '     , g.count  ';
            $sql .= '     , g.price  ';
            $sql .= '     , h.count ';
            $sql .= '     , i.count ';

            $res = $this->TrnCloseHeader->query($sql);
            $result = array();
            foreach($res as $row){
                $result[] = $row[0];
            }
            return $result;
        }
        
        return $res;
    }


    /**
     * 在庫表データ取得
     * @param array $params
     */
    private function getInventoryListRecords($params,$type=FALSE){
        $now = date('Y/m/d H:i:s');
        $whereFacility = '';
        App::import('Sanitize');
        //施設指定
        if(isset($params['facilityId'])){
            $whereFacility .= " and m.id = {$params['facilityId']} ";
        }

        //サブコード指定
        if(isset($params['subcode'])){
            $whereFacility .= " and m.subcode similar to '(_*)".Configure::read('subcodeSplitter')."(_*)".Configure::read('subcodeSplitter'). Sanitize::escape($params['subcode']) . "' ";
        }

        //施主
        if(isset($params['donorId'])){
            $whereFacility .= " and s.id = {$params['donorId']}";
        }

        /*
        $sql =
            ' select ' .
                " row_number() over(order by a.商品id) as 行番号".
                ",'" . $params['startDate'] . '～' . $params['endDate'] . "' as 期間".
                ',a.サブコード' .
                ',a.商品id' .
                ',a.商品名' .
                ',a.規格'.
                ',a.製品番号'.
                ',a.販売元'.
                ',a.課税区分'.
                ',a.施主コード'.
                ',a.施主名'.
                ',a.前残'.
                ',a.前残単価'.
                ',a.前残金額'.
                ',a.受入合計'.
                ',a.受入金額合計'.
                ',a.売上'.
                ',a.移動'.
                ',a.調整' .
                ',a.払出合計'.
                ',(a.売上 * b.price) as 売上金額' .
                ',(a.移動 * b.price) as 移動金額' .
                ',(a.調整 * b.price) as 調整金額' .
                ',(a.前残金額 + a.受入金額合計) - b.remain_price as 払出金額'.
                ',b.price as 評価単価' .
                ',b.hospital_remain_count as 病院当残' .
                ',coalesce(b.remain_count , 0) - coalesce(b.hospital_remain_count , 0) as 倉庫当残' .
                ',b.remain_count as 当残' .
                ',b.remain_price as 当残金額';
        if($type==FALSE){
            $sql .= ",'" . $now ."' as 印刷日時 ";
        }
        $sql .= ' from ' .
                '(select' .
                    ' a.internal_code as 商品id' .
                    ',a.item_name as 商品名' .
                    ',a.standard as 規格' .
                    ',a.item_code as 製品番号' .
                    ',m.subcode as サブコード' .
                    ',b.dealer_name as 販売元' .
                    ",(case a.tax_type" .
                    " when '0' then '課税'" .
                    " when '1' then '非課税'" .
                    " else '未設定' end) as 課税区分" .
                    ",s.facility_code as 施主コード" .
                    ",s.facility_name as 施主名" .
                    ',coalesce(c.remain_count,0) as 前残' .
                    ',coalesce(c.price,0) as 前残単価' .
                    ',coalesce(c.remain_price,0) as 前残金額' .
                    ',coalesce(d.quantity,0) as 仕入' .
                    ',coalesce(d.amount,0) as 仕入金額' .
                    ',coalesce(f.quantity,0) as 売上' .
                    ',coalesce(g.quantity,0) as 移動' .
                    ',coalesce(h.quantity,0) as 調整' .
                    ',coalesce(d.quantity,0) as 受入合計' .
                    ',coalesce(d.amount,0) as 受入金額合計' .
                    ',(coalesce(f.quantity,0) + coalesce(g.quantity,0) + coalesce(h.quantity,0)) as 払出合計' .
                    ',round(
                      ( case (coalesce(c.remain_count,0) + coalesce(d.quantity,0)) when 0 then 0
                        else (coalesce(c.remain_price,0) + coalesce(d.amount,0)) / (coalesce(c.remain_count,0) + coalesce(d.quantity,0))
                        end )
                      ,2)
                      as 評価単価' .
                    ',a.id as mst_facility_item_id' .
                ' from ' .
                    ' mst_facility_items as a' .
                    ' inner join mst_facilities as m on a.mst_facility_id = m.id '.
                    ' left join mst_facilities as s on a.mst_owner_id = s.id and s.facility_type = ' . Configure::read('FacilityType.donor') .
                    ' left join mst_dealers as b on a.mst_dealer_id = b.id' .
                    ' left join (' . //前残
                        ' select ' .
                            ' a.remain_count' .
                            ',a.price' .
                            ',a.remain_price' .
                            ',a.mst_facility_item_id'.
                        ' from ' .
                            ' trn_appraised_values as a' .
                            ' inner join trn_close_headers as b on a.trn_close_header_id = b.id ' .
                        ' where ' .
                                " a.work_date = '" . date('Y/m/d', strtotime('-1 day', strtotime($params['startDate']))) . "' " .
                            ' and b.is_provisional = false and a.is_deleted = false ' .
                    ') as c on a.id = c.mst_facility_item_id' .
                    ' left join (' . //受入(仕入)
                        ' select' .
                            ' a.mst_facility_item_id' .
                            ',sum(a.quantity * b.per_unit) as quantity' .
                            ',sum(a.taxable_amount + a.nontaxable_amount) as amount' .
                        ' from ' .
                            ' trn_close_records as a' .
                            ' inner join mst_item_units as b on a.mst_item_unit_id=b.id ' .
                        ' where ' .
                                " a.target_date between '{$params['startDate']}' and '{$params['endDate']}' " .
                            ' and a.close_type = ' . Configure::read('CloseType.supply') .
                            ' and a.is_deleted = false ' .
                            " and a.is_provisional = {$params['isProvisional']} ".
                        ' group by ' .
                            ' a.mst_facility_item_id' .
                    ') as d on a.id = d.mst_facility_item_id' .
                    ' left join (' . //払出(売上)
                        ' select' .
                            ' a.mst_facility_item_id' .
                            ',sum(a.quantity * b.per_unit) as quantity' .
                            ',sum(a.taxable_amount + a.nontaxable_amount) as amount' .
                        ' from ' .
                            ' trn_close_records as a' .
                            ' inner join mst_item_units as b on a.mst_item_unit_id=b.id ' .
                        ' where ' .
                                " a.target_date between '{$params['startDate']}' and '{$params['endDate']}' " .
                            ' and a.close_type = ' . Configure::read('CloseType.sales') .
                            ' and a.is_deleted = false ' .
                            " and a.is_provisional = {$params['isProvisional']} " .
                        ' group by ' .
                            ' a.mst_facility_item_id' .
                    ') as f on a.id = f.mst_facility_item_id' .
                    ' left join (' .
                        ' select' . //払出(移動)
                            ' a.mst_facility_item_id' .
                            ',sum(a.quantity * b.per_unit) as quantity' .
                            ',sum(a.taxable_amount + a.nontaxable_amount) as amount' .
                        ' from ' .
                            ' trn_close_records as a' .
                            ' inner join mst_item_units as b on a.mst_item_unit_id=b.id ' .
                        ' where ' .
                                " a.target_date between '{$params['startDate']}' and '{$params['endDate']}' " .
                            ' and a.close_type = ' . Configure::read('CloseType.stock') .
                            ' and a.sum_type = 1' .
                            ' and a.is_deleted = false ' .
                            " and a.is_provisional = {$params['isProvisional']} " .
                        ' group by ' .
                            ' a.mst_facility_item_id' .
                    ') as g on a.id = g.mst_facility_item_id' .
                    ' left join (' . //払出(調整)
                        ' select' .
                            ' a.mst_facility_item_id' .
                            ',sum((case when sum_type = 3 then a.quantity else -1 * a.quantity end) * b.per_unit) as quantity' .
                            ',sum(a.taxable_amount + a.nontaxable_amount) as amount' .
                        ' from ' .
                            ' trn_close_records as a' .
                            ' inner join mst_item_units as b on a.mst_item_unit_id=b.id ' .
                        ' where ' .
                                " a.target_date between '{$params['startDate']}' and '{$params['endDate']}' " .
                            ' and a.close_type = ' . Configure::read('CloseType.stock') .
                            ' and a.sum_type in (3,4)' .
                            ' and a.is_deleted = false ' .
                            " and a.is_provisional = {$params['isProvisional']} " .
                        ' group by ' .
                            ' a.mst_facility_item_id' .
                    ') as h on a.id = h.mst_facility_item_id' .
            ' where ' .
                   ' 1 = 1 '.
                   $whereFacility.
                ' and (c.mst_facility_item_id is not null' .
                ' or d.mst_facility_item_id is not null' .
                ' or f.mst_facility_item_id is not null' .
                ' or g.mst_facility_item_id is not null' .
                ' or h.mst_facility_item_id is not null)' .
            ' ) as a' .
              ' inner join (' .
              ' select a.mst_facility_item_id,a.hospital_remain_count,a.remain_count,a.remain_price,price' .
              ' from trn_appraised_values as a' .
              ' inner join trn_close_headers as b' .
              ' on a.trn_close_header_id=b.id' .
              " and b.work_date='{$params['endDate']}' " .
              ' and b.close_type = ' . Configure::read('CloseType.stock') .
              " and b.is_provisional = {$params['isProvisional']} " .
              ' and b.is_deleted=false' .
              ' ) as b on a.mst_facility_item_id = b.mst_facility_item_id' .
              ' order by a.商品id';
*/

        $sql  = ' select ';
        $sql .= '       row_number() over (order by a.internal_code) as 行番号 ';
        //$sql .= '     ,'" . $params['startDate'] . '～' . $params['endDate'] . "' as 期間". ';
        $sql .= '     , a.sub_code                            as サブコード ';
        $sql .= '     , a.internal_code                       as 商品id ';
        $sql .= '     , a.item_name                           as 商品名 ';
        $sql .= '     , a.standard                            as 規格 ';
        $sql .= '     , a.item_code                           as 製品番号 ';
        $sql .= '     , a.dealer_name                         as 販売元 ';
        $sql .= '     , a.tax_type                            as 課税区分 ';
        $sql .= '     , a.owner_code                          as 施主コード ';
        $sql .= '     , a.owner_name                          as 施主名 ';
        $sql .= '     , a.remain_count                        as 前残 ';
        $sql .= '     , a.remain_unit_price                   as 前残単価 ';
        $sql .= '     , a.remain_price                        as 前残金額 ';
        $sql .= '     , a.input_total_count                   as 受入合計 ';
        $sql .= '     , a.input_total_price                   as 受入金額合計 ';
        $sql .= '     , a.claim_count                         as 売上 ';
        $sql .= '     , a.move_count                          as 移動 ';
        $sql .= '     , a.adjust_count                        as 調整 ';
        $sql .= '     , a.shipping_count                      as 払出合計 ';
        $sql .= '     , a.claim_price                         as 売上金額 ';
        $sql .= '     , a.move_price                          as 移動金額 ';
        $sql .= '     , a.adjust_price                        as 調整金額 ';
        $sql .= '     , a.shipping_price                      as 払出金額 ';
        $sql .= '     , a.price                               as 評価単価 ';
        $sql .= '     , a.hospital_remain_count               as 病院当残 ';
        $sql .= '     , a.center_remain_count                 as 倉庫当残 ';
        $sql .= '     , a.quantity                            as 当残 ';
        $sql .= '     , a.price                               as 当残金額 ';
        $sql .= "     , '".$now."'                            as 印刷日時  ";
        $sql .= '   from ';
        $sql .= '     trn_close_dummy as a ';
        //$sql .= '   where a.work_date = '$params['endDate']' ';
        $sql .= '         and a.center_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '   order by a.internal_code ';
        
        $res = $this->TrnCloseHeader->query($sql);
        $result = array();
        foreach($res as $row){
            $result[] = $row[0];
        }
        return $result;
    }

    private function getFacilityByCode($code , $type){
        $res = $this->MstFacility->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'is_deleted'    => false,
                'facility_code' => $code,
                'facility_type' => $type,
            ),
        ));
        return $res['MstFacility']['id'];
    }

    /**
     * 仮締め指定かどうか
     * @param string $targetDate
     * @return string #SQL指定しやすいようにbooleanをstringで。
     */
    private function isProvisional($targetDate){
        $res = $this->TrnCloseHeader->find('all', array(
            'recursive' => -1,
            'conditions' => array(
                'is_deleted' => false,
                'end_date'   => $targetDate,
            ),
            'fields' => array(
                'is_provisional',
                'end_date',
            ),
            'group' => array(
                'is_provisional',
                'end_date',
            ),
        ));
        if(count($res) === 0){
            return 'true';
        }elseif(count($res) === 1){
            return 'true';
        }else{
            return 'false';
        }
    }

    /**
     * 対象年の取得
     * @return array 対象年文字列のリスト。
     */
    private function getTargetYear(){
        $sql  = ' select ';
        $sql .= '    to_char(start_date,\'YYYY\') as "a__targetYear" ';
        $sql .= ' from trn_close_headers as a ';
        $sql .= ' where ';
        $sql .= '     a.is_deleted = false ';
        $sql .= '     and a.mst_facility_id = '. $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and a.close_type = ' . Configure::read('CloseType.stock');
        $sql .= " group by to_char(start_date,'YYYY') ";
        $sql .= " order by to_char(start_date,'YYYY') desc ";

        $result = Set::combine($this->TrnCloseHeader->query($sql), '{n}.a.targetYear', '{n}.a.targetYear');

        return $result;
    }
}
?>
