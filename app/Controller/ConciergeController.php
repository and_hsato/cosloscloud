<?php
/**
 * ConciergeController
 * コンシェルジュ
 * @version 1.0.0
 * @since 2015/10/14
 */
class ConciergeController extends AppController {

    /**
     * Controller's name.
     * @var $name
     */
    var $name = 'Concierge';

    /**
     * Model Objects using.
     * @var array $uses
     */
    var $uses = array('MstUser',
                      'TrnTicket',
                      'TrnChildticket',
                      'TransactionManager',
                      );

    /**
     * Helpers using.
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'common');

    /**
     * Components using.
     * @var array $components
     */
    var $components = array('CsvWriteUtils','CsvReadUtils', 'Mail');

    /**
     *
     * @var array errors
     */
    var $errors;

    var $successes;

    /**
     * 一覧表示
     */
    function index (){
        // 絞込み条件
        $where = '';
        // キーワード
        if(isset($this->request->data['Ticket']['keyword']) && $this->request->data['Ticket']['keyword'] != ''){
            $where .= " and a.title like '%" . $this->request->data['Ticket']['keyword'] . "%'";
            $where .= " and b.inquiry like '%" . $this->request->data['Ticket']['keyword'] . "%'";
        }
        // ステータス
        if(isset($this->request->data['Ticket']['status']) && $this->request->data['Ticket']['status'] != ''){
            $where .= " and a.status = " . $this->request->data['Ticket']['status'] ;
        }
        
        // 日付From
        if(isset($this->request->data['Ticket']['work_date_from']) && $this->request->data['Ticket']['work_date_from'] != ''){
            $where .= " and to_char(a.created,'YYYY/MM/DD') >= '" . $this->request->data['Ticket']['work_date_from']. "'";
        }
        // 日付To
        if(isset($this->request->data['Ticket']['work_date_to']) && $this->request->data['Ticket']['work_date_to'] != ''){
            $where .= " and to_char(a.created,'YYYY/MM/DD') <= '" . $this->request->data['Ticket']['work_date_to']. "'";
        }
        
        $sql  = ' select ';
        $sql .= '       a.id as ticketid ';
        $sql .= '     , a.userid ';
        $sql .= '     , a.title ';
        $sql .= '     , (case a.status ';
        foreach( Configure::read('Concierge.status') as $k => $v ){
            $sql .= "        when '" . $k . "' then '" . $v . "'";
        }
        $sql .= '         end ) as status_name ';
        $sql .= '     , a.status';
        $sql .= '     , a.deleteflg ';
        $sql .= "     , to_char(a.created,'YYYY/MM/DD') as created";
        $sql .= "     , to_char(a.modified,'YYYY/MM/DD  HH24:MI') as modified";
        $sql .= '     , a.user_name ';
        $sql .= '   from ';
        $sql .= '     trn_tickets as a ';
        $sql .= '     left join trn_childtickets as b ';
        $sql .= '      on b.ticketid = a.id ';
        $sql .= '   where ';
        $sql .= '     a.userid = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '     and b.deleteflg = 0 '; // 削除フラグがたっているものは除く
        $sql .= $where ;
        $sql .= '   group by ';
        $sql .= '       a.id ';
        $sql .= '     , a.userid ';
        $sql .= '     , a.title ';
        $sql .= '     , a.status ';
        $sql .= '     , a.deleteflg ';
        $sql .= '     , a.created ';
        $sql .= '     , a.modified ';
        $sql .= '     , a.user_name ';
        $sql .= '   order by ';
        $sql .= '     a.modified desc';
        
        $result = $this->TrnTicket->query($sql);
        foreach($result as &$r){
            $sql  = ' select ';
            $sql .= '     a.addusertype ';
            $sql .= '   , a.inquiry ';
            $sql .= "   , to_char(a.modified,'YYYY/MM/DD HH24:MI') as modified";
            $sql .= ' from trn_childtickets as a ';
            $sql .= ' where a.ticketid = ' . $r[0]['ticketid'] ;
            $sql .= ' order by a.id ';
            $r['childtickets'] = $this->TrnChildticket->query($sql);
        }
        unset($r);
        $this->set('result' , $result);
    }
    
    
    /**
     * 新規質問
     */
    function add(){
        
    }

    /**
     * 登録処理
     */
    function result($id=null){
        $now = date('Y/m/d H:i:s');
        $loginUser = $this->Session->read('Auth.MstUser');
        $trnData = array();
        
        // トランザクション開始
        // 途中でエラーとなった場合は、全ての更新をロールバックして終了する
        $transaction = $this->TransactionManager->begin();
        try {
            if(is_null($id)){
                // 新規の場合
                // 親チケットを作成
                $trnData['TrnTicket'] = array(
                        'userid'    => $this->Session->read('Auth.MstUser.id'),
                        'title'     => $this->request->data['Ticket']['title'],
                        'status'    => 0, //初期
                        'deleteflg' => 0,
                        'created'   => $now,
                        'modified'  => $now,
                        'user_name' => $loginUser['user_name'],
                    );
                
                //TrnTicketオブジェクトをcreate
                $this->TrnTicket->create();
                
                //SQL実行
                if (!$this->TrnTicket->save($trnData['TrnTicket'])) {
                    throw new Exception('TrnTicket Save Error.');
                }
                $trnData['TrnTicket']['id'] = $this->TrnTicket->getLastInsertID();
            } else {
                // 子チケット追加の場合
                $ret = $this->TrnTicket->findById($id);
                if (empty($ret['TrnTicket'])) {
                    throw new Exception('TrnTicket Not Found. id = ' . $id);
                }
                $trnData['TrnTicket'] = $ret['TrnTicket'];
            }
            
            // 子チケットを作成
            $trnData['TrnChildTicket'] = array(
                    'ticketid'      => $trnData['TrnTicket']['id'] , 
                    'childticketno' => $this->getChildTicketNo($trnData['TrnTicket']['id']),
                    'inquiry'       => $this->request->data['Ticket']['inquiry'],
                    'addusertype'   => 0,
                    'deleteflg'     => 0,
                    'created'       => $now,
                    'modified'      => $now,
                );
            
            //TrnChildTicketオブジェクトをcreate
            $this->TrnChildticket->create();
            
            //SQL実行
            if (!$this->TrnChildticket->save($trnData['TrnChildTicket'])) {
                throw new Exception('TrnChildticket Save Error.');
            }
            
            // メール送信
            if ($this->AuthSession->isLiteUserRole()) {
                $ret = $this->sendInquiryMail($trnData['TrnChildTicket'], $loginUser);
                if (empty($ret)) throw new Exception('sendInquiryMail Error.');
            }
            
            $this->TrnTicket->commit();
            $this->Session->setFlash('問合せを受付ました。', 'growl', array('type'=>'star') );
            $this->redirect('index');
        } catch (Exception $e) {
            $this->TransactionManager->rollback($transaction);
            $this->log($e->getMessage());
            $this->log(print_r($trnData, true));
            $this->Session->setFlash('問合登録に失敗しました。', 'growl', array('type'=>'err') );
            $this->redirect('index');
        }
    }

    /**
     * 明細表示
     */
    function detail($id=null){
        if(is_null($id)){
            $this->redirect('index');
        }
        
        // 表示リストを取得
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , a.title ';
        $sql .= "     , to_char(a.created, 'YYYY/MM/DD') as created ";
        $sql .= '     , (case a.status ';
        foreach( Configure::read('Concierge.status') as $k => $v ){
            $sql .= "        when '" . $k . "' then '" . $v . "'";
        }
        $sql .= '         end ) as status_name ';
        $sql .= '     , a.status ';
        $sql .= "     , to_char(b.created, 'YYYY/MM/DD HH24:MI') as send_date ";
        $sql .= '     , b.inquiry  ';
        $sql .= '     , b.addusertype';
        $sql .= '     , a.user_name ';
        $sql .= '   from ';
        $sql .= '     trn_tickets as a  ';
        $sql .= '     left join trn_childtickets as b  ';
        $sql .= '       on b.ticketid = a.id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $id;
        $sql .= '     and b.deleteflg = 0  ';
        $sql .= '   order by ';
        $sql .= '     b.created ';
        
        $result = $this->TrnTicket->query($sql);
        
        $this->set('result' , $result);
    }

    /**
     * チケット完了
     */
    function ticketFinish($id){
        if(is_null($id)){
            $this->redirect('index');
        }
        $this->changeStatus($id , 3 );
        $this->Session->setFlash('問合せを完了しました。', 'growl', array('type'=>'star') );
        $this->redirect('index');
    }

    /**
     * ステータス変更
     */
    private function changeStatus($id , $status){
        // 登録処理
        $now = date('Y/m/d H:i:s');
        
        // トランザクション開始
        $this->TrnTicket->begin();

        $res = $this->TrnTicket->updateAll(array(
            'TrnTicket.status'   => $status,
            'TrnTicket.modified' => "'" . $now . "'",
            ), array(
            'TrnTicket.id' => $id,
         ), -1);
        
        if(false === $res){
            $this->TrnTicket->rollback();
            $this->Session->setFlash('更新に失敗しました。', 'growl', array('type'=>'err') );
            $this->redirect('index');
        }
        $this->TrnTicket->commit();
    }
    
    /**
     * 子チケット番号を返す
     */
    private function getChildTicketNo($id){
        $sql  = ' select ';
        $sql .= '       max(childticketno) as no ';
        $sql .= '   from ';
        $sql .= '     trn_childtickets as a  ';
        $sql .= '   where ';
        $sql .= '     a.ticketid = ' . $id ;
        
        $ret = $this->TrnTicket->query($sql);
        if(is_null($ret[0][0]['no'])){
            return 0;
        }else{
            return $ret[0][0]['no'];
        }
    }
    
    private function sendInquiryMail($trnChildTicket, $mstUser) {
        $mailParams = array(
                MailComponent::PARAM_CONFIG       => 'and',
                MailComponent::PARAM_TO           => array($mstUser['login_id'],),
                MailComponent::PARAM_BCC          => Configure::read('admin.inquiry.emails'),
                MailComponent::PARAM_VIEW_VARS    => array(
                        'inquiry' => $trnChildTicket['inquiry'],
                        'user_name' => $mstUser['user_name'],
                ),
        );
        return $this->Mail->sendInquiryMail($mailParams, $trnChildTicket['ticketid']);
    }
    
    
}// EOF
