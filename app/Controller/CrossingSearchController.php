<?php
/**
 * CrossingSeachController
 * 
 * @version 1.0.0
 * @since 2012/01/05
 */

class CrossingSearchController extends AppController {
    
    /**
     * @var $name
     */
    var $name = 'CrossingSearch';

    /**
     * @var array $uses
     */
    var $uses = array(
        'MstFixedCount',
        'MstItemUnit',
        'MstDepartment',
        'MstFacility',
        'MstFacilityItem',
        'MstItem',
        'MstUserBelonging',
        'MstItemCategory',
        'MstJmdnName',
        'MstInsuranceClaimDepartment',
        'MstClassSeparation',
        'MstUnitName',
        'MstAccountlist'
        );
    
    /**
     * @var bool $scaffold
     */
    //var $scaffold;
    
    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');
    
    /**
     * @var array $components
     */
    var $components = array('RequestHandler','xml','CsvWriteUtils');
    
    /**
     * @var $paginate
     */
    var $paginate ;

    public function beforeFilter(){
        parent::beforeFilter();
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }
    
    /**
     * 横断検索メイン
     */
    public function index (){
        App::import('Sanitize');
        $this->set('Facility_List' , $this->getFacilityList(null , array(1) , false , array('id' , 'facility_name')));
        $this->set('JMDN_List' , $this->MstJmdnName->find('list' ,array( 'fields' => array('jmdn_code','jmdn_name'), )));
        $this->set('ItemCategory_List' , $this->MstItemCategory->find('list' ,array( 'fields' => array('category_code','category_name'), )));
        $SearchResult = array();
        if(isset($this->request->data['search']['is_search']) && $this->request->data['search']['is_search'] == 1){
            //商品検索ボタン押下
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            
            //検索条件取得
            $where = '';
            $where2 = '';
            //AWID
            if(isset($this->request->data['search']['awid']) && $this->request->data['search']['awid'] != '' ){
                $where .= " and a.internal_code = '" . $this->request->data['search']['awid'] ."'";
            }
            
            //マスタID
            if(isset($this->request->data['search']['master_id']) && $this->request->data['search']['master_id'] != '' ){
                $where .= " and a.master_id = '" . $this->request->data['search']['master_id'] ."'";
            }
            
            //商品ID
            if(isset($this->request->data['search']['internal_code']) && $this->request->data['search']['internal_code'] != '' ){
                $where .= " and b.internal_code like '" . $this->request->data['search']['internal_code'] ."%'";
            }
            
            //製品番号
            if(isset($this->request->data['search']['item_code']) && $this->request->data['search']['item_code'] != '' ){
                $where .= " and b.item_code like '%" . $this->request->data['search']['item_code'] ."%'";
            }
            
            //商品名
            if(isset($this->request->data['search']['item_name']) && $this->request->data['search']['item_name'] != '' ){
                $where .= " and b.item_name like '%" . $this->request->data['search']['item_name'] ."%'";
            }
            
            //販売元
            if(isset($this->request->data['search']['dealer_name']) && $this->request->data['search']['dealer_name'] != '' ){
                $where .= " and a1.dealer_name like '%" . $this->request->data['search']['dealer_name'] ."%'";
            }
            
            //規格
            if(isset($this->request->data['search']['standard']) && $this->request->data['search']['standard'] != '' ){
                $where .= " and b.standard = '" . $this->request->data['search']['standard'] ."'";
            }
            
            //JANコード
            if(isset($this->request->data['search']['jan_code']) && $this->request->data['search']['jan_code'] != '' ){
                $where .= " and b.jan_code = '" . $this->request->data['search']['jan_code'] ."'";
            }
            
            //商品カテゴリ
            if(isset($this->request->data['search']['itemcategory']) && $this->request->data['search']['itemcategory'] != '' ){
                $where .= " and b.item_category1 = '" . $this->request->data['search']['itemcategory'] ."'";
            }
            
            //JMDNコード
            if(isset($this->request->data['search']['jmdn_code']) && $this->request->data['search']['jmdn_code'] != '' ){
                $where .= " and b.jmdn_code = '" . $this->request->data['search']['jmdn_code'] ."'";
            }
            //在庫の時はTrnClaim の絞込みはしない
            if($this->request->data['search']['type'] <> 3){
                //種別
                if(isset($this->request->data['search']['claim_type']) && $this->request->data['search']['claim_type'] != '' ){
                    $where2 .= " and x.is_stock_or_sale = '" . $this->request->data['search']['type'] % 3  ."'";
                }
                //日付From
                if(isset($this->request->data['search']['work_date_from']) && $this->request->data['search']['work_date_from'] != '' ){
                    $where2 .= " and x.claim_date >= '" . $this->request->data['search']['work_date_from'] ."'";
                }
                
                //日付To
                if(isset($this->request->data['search']['work_date_to']) && $this->request->data['search']['work_date_to'] != '' ){
                    $where2 .= " and x.claim_date <= '" . $this->request->data['search']['work_date_to'] ."'";
                }
            }
            //検索
            $SearchResult = $this->getCrossingItem($where , $where2 , $this->request->data['search']['limit']);
            $this->Session->write('SearchResult' , $SearchResult);
            $this->request->data = $data;
        }
        $this->set('SearchResult' , $SearchResult);

        if(isset($this->request->data['search']['is_cross_search']) && $this->request->data['search']['is_cross_search']==1){
            //検索ボタン押下
            $sql = $this->crossing_search();
            $result =  $this->MstItem->query($sql);

            foreach($result[0][0] as $title => $val){
                 $header[] = $title;
            }
            $this->set('CrossingSearchResult' , $result);
            $this->set('header' , $header);
            $this->set('SearchResult', $this->Session->read('SearchResult'));
            
        }
    }
    
    /**
     * CSV出力
     */
    public function export_csv(){
        App::import('Sanitize');
        
        $sql = $this->crossing_search();
        $this->db_export_csv($sql , "横断検索", 'index');

    }
    
    /**
     * AWGM関連付け：検索
     */
    public function item_relation_search(){
        App::import('Sanitize');
        $this->set('Facility_List' , $this->getFacilityList(null , array(1) , false , array('id' , 'facility_name')));
        $this->set('JMDN_List' , $this->MstJmdnName->find('list' ,array( 'fields' => array('jmdn_code','jmdn_name'), )));
        $this->set('ItemCategory_List' , $this->MstItemCategory->find('list' ,array( 'fields' => array('category_code','category_name'), )));
        
        $SearchResult = array();
        $CartSearchResult = array();
        if(isset($this->request->data['add_search']['is_search'])){
            //検索ボタン押下
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            
            //絞込み条件追加
            $where= '' ;
            
            //AWID
            if(isset($this->request->data['search']['awid']) && $this->request->data['search']['awid'] != '' ){
                $where .= " and c.internal_code = '" . $this->request->data['search']['awid'] ."'";
            }
            
            //マスタID
            if(isset($this->request->data['search']['master_id']) && $this->request->data['search']['master_id'] != '' ){
                $where .= " and a.master_id = '" . $this->request->data['search']['master_id'] ."'";
            }
            
            //商品ID
            if(isset($this->request->data['search']['internal_code']) && $this->request->data['search']['internal_code'] != '' ){
                $where .= " and a.internal_code like '" . $this->request->data['search']['internal_code'] ."%'";
            }
            
            //製品番号
            if(isset($this->request->data['search']['item_code']) && $this->request->data['search']['item_code'] != '' ){
                $where .= " and a.item_code like '%" . $this->request->data['search']['item_code'] ."%'";
            }
            
            //商品名
            if(isset($this->request->data['search']['item_name']) && $this->request->data['search']['item_name'] != '' ){
                $where .= " and a.item_name like '%" . $this->request->data['search']['item_name'] ."%'";
            }
            
            //販売元
            if(isset($this->request->data['search']['dealer_name']) && $this->request->data['search']['dealer_name'] != '' ){
                $where .= " and b.dealer_name like '%" . $this->request->data['search']['dealer_name'] ."%'";
            }
            
            //規格
            if(isset($this->request->data['search']['standard']) && $this->request->data['search']['standard'] != '' ){
                $where .= " and a.standard like '%" . $this->request->data['search']['standard'] ."%'";
            }
            
            //JANコード
            if(isset($this->request->data['search']['jan_code']) && $this->request->data['search']['jan_code'] != '' ){
                $where .= " and a.jan_code = '" . $this->request->data['search']['jan_code'] ."'";
            }
            
            //センター
            if(isset($this->request->data['search']['mst_facility_id']) && $this->request->data['search']['mst_facility_id'] != '' ){
                $where .= " and d.id = '" . $this->request->data['search']['mst_facility_id'] ."'";
            }
            
            //商品カテゴリ
            if(isset($this->request->data['search']['itemcategory']) && $this->request->data['search']['itemcategory'] != '' ){
                $where .= " and a.item_category = '" . $this->request->data['search']['itemcategory'] ."'";
            }
            
            //JMDNコード
            if(isset($this->request->data['search']['jmdn_code']) && $this->request->data['search']['jmdn_code'] != '' ){
                $where .= " and a.jmdn_code = '" . $this->request->data['search']['jmdn_code'] ."'";
            }
            
            //カート
            if(isset($this->request->data['cart']['tmpid']) && $this->request->data['cart']['tmpid'] != '' ){
                $where .= ' and a.id not in (' . $this->request->data['cart']['tmpid'] . ') ' ;
            }
            //画面上部検索
            $SearchResult = $this->getItemData($where , $this->request->data['search']['limit']);

            //カート検索
            $where = '' ;
            if(isset($this->request->data['cart']['tmpid']) && $this->request->data['cart']['tmpid'] != '' ){
                $where .= ' and a.id in (' . $this->request->data['cart']['tmpid'] . ') ' ;
            }else{
                $where .= ' and 1 = 0 ';
            }
            $CartSearchResult = $this->getItemData($where);
            $this->request->data = $data;
        }
        $this->set('SearchResult' , $SearchResult);
        $this->set('CartSearchResult' , $CartSearchResult);
    }
    
    /**
     * AWGM関連付け：確認
     */
    public function item_relation_confirm(){
        $where = ' and a.id in (' . $this->request->data['cart']['tmpid'] . ') ' ;
        $FacilityItemList = $this->getItemData($where);
        $this->set('FacilityItemList' , $FacilityItemList);
    }

    /**
     * AWGM関連付け：結果
     */
    public function item_relation_result(){
        $err = false ;
        $ids = array();
        //トランザクション開始
        $this->MstItem->begin();
        
        foreach($this->request->data['id'] as $id => $item_id){
            $ids[] = $id; //結果表示用にIDを確保
            
            //一行取得
            $tmp = $this->MstItem->find('first', array('fields' => 'id', 'conditions' => array('MstItem.internal_code' => $this->request->data['awid'][$id]), 'recursive' => -1, 'order'=> array('MstItem.id')));
            $this->MstItem->create();
            if(isset($tmp['MstItem']['id'])){
                //既に使っているAWIDだったら、施設採用品.mst_item_id を更新
                if (!$this->MstFacilityItem->updateAll(array(  'MstFacilityItem.mst_item_id' => "'". $tmp['MstItem']['id'] . "'",
                                                       )
                                               ,
                                           array(
                                               'MstFacilityItem.id'  => $id,
                                               )
                                               ,
                                               -1
                                               )
                    ) {
                    $err = true ;
                }
            }else{
                //まだ使われていないAWIDだったら、mst_items.internal_code を更新
                if (!$this->MstItem->updateAll(array(  'MstItem.internal_code' => "'". $this->request->data['awid'][$id] . "'",
                                                       )
                                               ,
                                           array(
                                               'MstItem.id'  => $item_id,
                                               )
                                               ,
                                               -1
                                               )
                    ) {
                    $err = true ;
                }
            }
        }
        
        if($err){
            $this->MstItem->rollback();
            $msg = '登録に失敗しました。';
        }else{
            $this->MstItem->commit();
            $msg = 'マスタIDを更新しました';
        }
        
        //結果画面表示用に情報を取得する。
        $result = $this->getItemData(' and a.id in ( ' . join(',',$ids) . ')');
        
        $this->set('msg' , $msg);
        $this->set('result' , $result);
    }

    /**
     * AWGMメンテ：検索
     */
    function item_search(){
        App::import('Sanitize');
        $result = array();
        if(isset($this->request->data['search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);
            $where = '';
            //検索条件
            
            //AWID
            if($this->request->data['search']['awid']!=''){
                $where .= " and a.internal_code = '" . $this->request->data['search']['awid']. "'";
            }
            //商品名
            if($this->request->data['search']['item_name']!=''){
                $where .= " and a.item_name like '%" . $this->request->data['search']['item_name']. "%'";
            }
            //製品番号
            if($this->request->data['search']['item_code']!=''){
                $where .= " and a.item_code like '%" . $this->request->data['search']['item_code']. "%'";
            }
            //規格
            if($this->request->data['search']['standard']!=''){
                $where .= " and a.standard like '%" . $this->request->data['search']['standard']. "%'";
            }
            //JANコード
            if($this->request->data['search']['jan_code']!=''){
                $where .= " and a.jan_code = '" . $this->request->data['search']['jan_code']. "'";
            }
            //販売元
            if($this->request->data['search']['dealer_name']!=''){
                $where .= " and c.dealer_name like '%" . $this->request->data['search']['dealer_name']. "%'";
            }
            
            
            $sql  = ' select ';
            $sql .= '       a.id                       as "aw__id" ';
            $sql .= '     , a.internal_code            as "aw__awid" ';
            $sql .= '     , a.item_name                as "aw__item_name" ';
            $sql .= '     , a.item_code                as "aw__item_code" ';
            $sql .= '     , a.standard                 as "aw__standard" ';
            $sql .= '     , a.jan_code                 as "aw__jan_code" ';
            $sql .= '     , c.dealer_name              as "aw__dealer_name"  ';
            $sql .= '   from ';
            $sql .= '     mst_items as a  ';
            $sql .= '     left join mst_facility_items as b  ';
            $sql .= '       on b.mst_item_id = a.id  ';
            $sql .= '     left join mst_dealers as c  ';
            $sql .= '       on c.id = a.mst_dealer_id  ';
            $sql .= '   where ';
            $sql .= '     b.is_deleted = false  ';
            $sql .= '     and b.id is not null  ';
            $sql .= $where;
            $sql .= '   group by ';
            $sql .= '     a.id ';
            $sql .= '     , a.internal_code ';
            $sql .= '     , a.item_name ';
            $sql .= '     , a.item_code ';
            $sql .= '     , a.standard ';
            $sql .= '     , a.jan_code ';
            $sql .= '     , c.dealer_name ';
            $sql .= '  order by a.internal_code ';
            
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'MstItem'));
            $sql .= ' limit ' . $this->request->data['limit'];
            
            //表示データ取得
            $result = $this->MstItem->query($sql);
            $this->request->data = $data;
        }
        $this->set('result' , $result);
    }
    
    /**
     * AWGMメンテ：確認
     */
    function item_confirm(){
        /* 必要なプルダウンを作成 */
        $this->_setPullDown();
        
        //商品情報を取得
        $sql  = ' select';
        $sql .= '       a.id                     as "MstItem__id"';
        $sql .= '     , a.internal_code          as "MstItem__internal_code"';
        $sql .= '     , a.item_name              as "MstItem__item_name"';
        $sql .= '     , a.standard               as "MstItem__standard"';
        $sql .= '     , a.item_code              as "MstItem__item_code"';
        $sql .= '     , a.dealer_code            as "MstItem__dealer_code"';
        $sql .= '     , a.jan_code               as "MstItem__jan_code"';
        $sql .= '     , a.unit_price             as "MstItem__unit_price"';
        $sql .= '     , a.price                  as "MstItem__price"';
        $sql .= '     , a.refund_price           as "MstItem__refund_price" ';
        $sql .= '     , to_char(a.start_date, \'YYYY/mm/dd\') as "MstItem__start_date"';
        $sql .= '     , to_char(a.end_date, \'YYYY/mm/dd\')   as "MstItem__end_date"';
        $sql .= '     , a.spare_key1             as "MstItem__spare_key1"';
        $sql .= '     , a.spare_key2             as "MstItem__spare_key2"';
        $sql .= '     , a.spare_key3             as "MstItem__spare_key3"';
        $sql .= '     , a.spare_key4             as "MstItem__spare_key4"';
        $sql .= '     , a.spare_key5             as "MstItem__spare_key5"';
        $sql .= '     , a.spare_key6             as "MstItem__spare_key6"';
        $sql .= '     , a.recital                as "MstItem__recital"';
        $sql .= '     , a.category_code          as "MstItem__category_code"';
        $sql .= '     , b.category_name          as "MstItem__item_category_txt"';
        $sql .= '     , a.tax_type               as "MstItem__tax_type"';
        $sql .= '     , a.biogenous_type         as "MstItem__biogenous_type" ';
        $sql .= '     , a.insurance_claim_type   as "MstItem__insurance_claim_type"';
        $sql .= '     , a.insurance_claim_type   as "MstItem__insurance_claim_type_txt"';
        $sql .= '     , a.insurance_claim_code   as "MstItem__insurance_claim_code"';
        $sql .= '     , a.insurance_claim_code   as "MstItem__insurance_claim_code_hidden"';
        $sql .= '     , c.insurance_claim_name_s as "MstItem__insurance_claim_short_name"';
        $sql .= '     , c.insurance_claim_name   as "MstItem__insurance_claim_name"';
        $sql .= '     , a.jmdn_code              as "MstItem__jmdn_code"';
        $sql .= '     , d.jmdn_name              as "MstItem__jmdn_name"';
        $sql .= '     , a.class_separation       as "MstItem__class_separation"';
        $sql .= '     , a.class_separation       as "MstItem__class_separation__hidden"';
        $sql .= '     , a.mst_dealer_id          as "MstItem__mst_dealer_id"';
        $sql .= '     , e.dealer_name            as "MstDealer__dealer_name"';
        $sql .= '     , e.dealer_code            as "MstDealer__dealer_code"';
        $sql .= '   from';
        $sql .= '     mst_items as a ';
        $sql .= '   left join mst_item_categories as b ';
        $sql .= '     on b.category_code = a.category_code';
        $sql .= '   left join mst_insurance_claims as c ';
        $sql .= '     on a.insurance_claim_code = c.insurance_claim_code ';
        $sql .= '   left join mst_jmdn_names as d ';
        $sql .= '     on d.jmdn_code = a.jmdn_code';
        $sql .= '   left join mst_dealers as e';
        $sql .= '     on e.id = a.mst_dealer_id';
        $sql .= '   where';
        $sql .= '     a.id = ' . $this->request->data['MstItem']['id'];
        
        $ret = $this->MstItem->query($sql);
        $this->request->data = $ret[0];
    }
    
    /**
     * AWGMメンテ：結果
     */
    function item_result(){
        /* 必要なプルダウンを作成 */
        $this->_setPullDown();
        
        //トランザクション開始
        $this->MstItem->begin();
        if(!$this->MstItem->save($this->request->data['MstItem'])){
            //登録失敗
            $this->MstItem->rollback();
            $this->set('msg' , '登録に失敗しました。');
        }else{
            //登録成功
            $this->MstItem->commit();
            $this->set('msg' , '登録しました。');
        }
    }
    
    /**
     * 商品検索
     */
    private function getItemData($where = '' , $limit = null){
        $sql  = ' select ';
        $sql .= '       a.id                   as "aw__id" ';
        $sql .= '     , a.internal_code        as "aw__internal_code" ';
        $sql .= '     , a.item_name            as "aw__item_name" ';
        $sql .= '     , a.standard             as "aw__standard" ';
        $sql .= '     , a.item_code            as "aw__item_code" ';
        $sql .= '     , b.dealer_name          as "aw__dealer_name" ';
        $sql .= '     , d.facility_name        as "aw__facility_name" ';
        $sql .= '     , c.internal_code        as "aw__awid" ';
        $sql .= '     , a.master_id               as "aw__master_id" ';
        $sql .= '     , a.jan_code             as "aw__jan_code" ';
        $sql .= '     , c.id                   as "aw__item_id" ';
        $sql .= '   from ';
        $sql .= '     mst_facility_items as a  ';
        $sql .= '     left join mst_dealers as b  ';
        $sql .= '       on b.id = a.mst_dealer_id  ';
        $sql .= '     left join mst_items as c  ';
        $sql .= '       on c.id = a.mst_item_id  ';
        $sql .= '     left join mst_facilities as d  ';
        $sql .= '       on d.id = a.mst_facility_id  ';
        $sql .= '   where ';
        $sql .= '     a.item_type = ' . Configure::read('Items.item_types.normalitem');
        $sql .= '     and d.facility_type = ' . Configure::read('FacilityType.center') ;
        $sql .= $where;
        $sql .= '   order by ';
        $sql .= '     c.internal_code , d.id ';
        if(!is_null($limit)){
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'MstItem'));
            $sql .= ' limit ' . $limit;
        }
        
        return $this->MstItem->query($sql);
    }

    /**
     * 横断検索：事前の商品検索
     */
    private function getCrossingItem($where , $where2 , $limit = null ){
        $sql  = ' select ';
        $sql .= '       a.internal_code  as "aw__awid"';
        $sql .= '     , a.item_name      as "aw__item_name"';
        $sql .= '     , a.standard       as "aw__standard"';
        $sql .= '     , a.item_code      as "aw__item_code"';
        $sql .= '     , a1.dealer_name   as "aw__dealer_name"';
        $sql .= '   from ';
        $sql .= '     mst_items as a  ';
        $sql .= '     left join mst_dealers as a1  ';
        $sql .= '       on a1.id = a.mst_dealer_id  ';
        $sql .= '     left join mst_facility_items as b  ';
        $sql .= '       on b.mst_item_id = a.id  ';
        $sql .= '       and b.item_type =  ' . Configure::read('Items.item_types.normalitem');
        if($this->request->data['search']['type'] <> 3){        
            $sql .= '     left join mst_item_units as c  ';
            $sql .= '       on c.mst_facility_item_id = b.id  ';
            $sql .= '    inner join (  ';
            $sql .= '      select ';
            $sql .= '            mst_item_unit_id  ';
            $sql .= '        from ';
            $sql .= '          trn_claims as x  ';
            $sql .= '        where ';
            $sql .= '          x.is_deleted = false  ';
            $sql .= $where2;
            $sql .= '        group by ';
            $sql .= '          x.mst_item_unit_id ';
            $sql .= '    ) as d  ';
            $sql .= '      on d.mst_item_unit_id = c.id ';
        }
        $sql .= '   where ' ;
        $sql .= '     1 = 1  ';
        $sql .= $where ;
        $sql .= '   group by ';
        $sql .= '     a.internal_code ';
        $sql .= '     , a.item_name ';
        $sql .= '     , a.item_code ';
        $sql .= '     , a.standard ';
        $sql .= '     , a1.dealer_name ';
        $sql .= '   order by ';
        $sql .= '     a.internal_code ';
        if(!is_null($limit)){
            //全件数取得
            $this->set('max' , $this->getMaxCount($sql , 'MstItem'));
            $sql .=  '  limit ' . $limit;
        }

        return $this->MstItem->query($sql);
    }

    /**
     * 横断検索メイン処理
     */
    private function crossing_search(){
        //種別
        $type = ($this->request->data['search']['type'] % 3==0)?1:($this->request->data['search']['type'] % 3);
        
        $sql  = ' select ';
        $sql .= '       a.id                 as "MstFacility__id"';
        $sql .= '     , a.facility_name      as "MstFacility__facility_name"';
        $sql .= '     , coalesce(c.id,a.id)  as "MstFacility__center_id"';
        $sql .= '   from ';
        $sql .= '     mst_facilities as a  ';
        $sql .= '    left join mst_facility_relations as b  ';
        $sql .= '      on b.partner_facility_id = a.id ';
        $sql .= '    left join mst_facilities as c ';
        $sql .= '      on c.id = b.mst_facility_id ';
        $sql .= '   where ';
        $sql .= '     a.facility_type = ' . $type;
        $sql .= '   order by ';
        $sql .= '     a.facility_code ';
        // 施設一覧
        $facility_list = $this->MstFacility->query($sql);
        
        $sql  = ' select ';
        $sql .= '       x.internal_code  as "マスタID"';
        $sql .= '     , x.item_name      as 商品名';
        $sql .= '     , x.standard       as 規格';
        $sql .= '     , x.item_code      as 製品番号';
        $sql .= '     , x.dealer_name    as 販売元';
        $sql .= '     , x.jan_code       as "JANコード"';
        $sql .= '     , x.unit_name      as 包装単位 ';
        $sql .= '     , z.max            as 最大単価 ';
        $sql .= '     , z.min            as 最小単価 ';
        $sql .= '     , trunc(z.avg , 2) as 平均単価 ';
        foreach($facility_list as $f ){
            
            $sql .= '     , y' . $f['MstFacility']['id'] . '.quantity      as "' . $f['MstFacility']['facility_name'] . '_数量"';
            $sql .= '     , y' . $f['MstFacility']['id'] . '.unit_price    as "' . $f['MstFacility']['facility_name'] . '_単価"';
            if($this->request->data['search']['type']%3<>0){
                $sql .= '     , y' . $f['MstFacility']['id'] . '.converted_num as "' . $f['MstFacility']['facility_name'] . '_換算数"';
            }
            $sql .= '     , y' . $f['MstFacility']['id'] . '.total         as "' . $f['MstFacility']['facility_name'] . '_合計"';
            
        }
        $sql .= '   from ';
        $sql .= '     (  ';
        $sql .= '       select ';
        $sql .= '             a.internal_code ';
        $sql .= '           , a.item_name ';
        $sql .= '           , a.item_code ';
        $sql .= '           , a.standard ';
        $sql .= '           , a.jan_code ';
        $sql .= '           , a1.dealer_name ';
        $sql .= '           , c.per_unit ';
        $sql .= '           , (  ';
        $sql .= '             case  ';
        $sql .= '               when c.per_unit = 1  ';
        $sql .= '               then c1.unit_name  ';
        $sql .= "               else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '               end ';
        $sql .= '           )                             as unit_name  ';
        $sql .= '         from ';
        $sql .= '           mst_items as a  ';
        $sql .= '           left join mst_dealers as a1  ';
        $sql .= '             on a1.id = a.mst_dealer_id  ';
        $sql .= '           inner join mst_facility_items as b  ';
        $sql .= '             on b.mst_item_id = a.id  ';
        $sql .= '            and b.item_type = ' . Configure::read('Items.item_types.normalitem');
        $sql .= '           inner join mst_item_units as c  ';
        $sql .= '             on c.mst_facility_item_id = b.id  ';
        $sql .= '           left join mst_unit_names as c1  ';
        $sql .= '             on c1.id = c.mst_unit_name_id  ';
        $sql .= '           left join mst_unit_names as c2  ';
        $sql .= '             on c2.id = c.per_unit_name_id  ';
        $sql .= '         where ';
        $sql .= "           a.internal_code in ('" . join('\',\'',$this->request->data['aw']['awid']) . "') ";
        $sql .= '         group by ';
        $sql .= '           a.internal_code ';
        $sql .= '           , a.item_name ';
        $sql .= '           , a.item_code ';
        $sql .= '           , a.standard ';
        $sql .= '           , a.jan_code ';
        $sql .= '           , a1.dealer_name ';
        $sql .= '           , c.per_unit ';
        $sql .= '           , c1.unit_name ';
        $sql .= '           , c2.unit_name ';
        $sql .= '     ) as x  ';
        
        foreach($facility_list as $f ){
            $sql .= '     left join (  ';
            $sql .= '       select ';
            $sql .= '             a.internal_code ';
            $sql .= '           , c.per_unit ';
            //在庫の場合
            if($this->request->data['search']['type']%3==0){
                $sql .= '           , sum(f.stock_count)                            as quantity ';
            }else{
            //在庫以外の場合
                $sql .= '           , sum(d.count)                                  as quantity ';
            }
            //仕入(マスタ)の場合
            if($this->request->data['search']['type'] == 4){
                $sql .= '           , trunc( f.transaction_price / g.per_unit * c.per_unit ,2) * b.converted_num as unit_price ';
            //売上(マスタ)の場合
            }elseif($this->request->data['search']['type'] == 5){
                $sql .= '           , f.sales_price * b.converted_num              as unit_price ';
            //仕入(実績)、売上(実績)の場合
            }elseif($this->request->data['search']['type'] == 1 || $this->request->data['search']['type'] == 2){
                $sql .= '           , d.unit_price * b.converted_num                as unit_price ';
            //在庫の場合
            }elseif($this->request->data['search']['type']%3==0){
                $sql .= '           , x.price * c.per_unit                          as unit_price ';
            }
            if($this->request->data['search']['type']%3<>0){
                $sql .= '           , b.converted_num                               as converted_num ';
            }
            //仕入(マスタ)の場合
            if($this->request->data['search']['type'] == 4){
                $sql .= '           , sum(d.count) * trunc( f.transaction_price / g.per_unit * c.per_unit ,2 )* b.converted_num as total ';
            //売上(マスタ)の場合
            }elseif($this->request->data['search']['type'] == 5){
                $sql .= '           , sum(d.count) * f.sales_price * b.converted_num as total ';
            //仕入(実績)、売上(実績)の場合
            }elseif($this->request->data['search']['type'] == 1 || $this->request->data['search']['type'] == 2){
                $sql .= '           , sum(d.count) * d.unit_price * b.converted_num as total ';
            //在庫の場合
            }elseif($this->request->data['search']['type']%3==0){
                $sql .= '           , sum(f.stock_count) * x.price * c.per_unit  as total ';
            }
            $sql .= '           , (  ';
            $sql .= '             case  ';
            $sql .= '               when c.per_unit = 1  ';
            $sql .= '               then c1.unit_name  ';
            $sql .= "               else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
            $sql .= '               end ';
            $sql .= '           )                             as unit_name  ';
            $sql .= '         from ';
            $sql .= '           mst_items as a ';
            $sql .= '          inner join mst_facility_items as b ';
            $sql .= '             on b.mst_item_id = a.id ';
            $sql .= '            and b.item_type = ' . Configure::read('Items.item_types.normalitem');
            $sql .= '           left join mst_item_units as c ';
            $sql .= '             on c.mst_facility_item_id = b.id ';
            $sql .= '           left join mst_unit_names as c1 ';
            $sql .= '             on c1.id = c.mst_unit_name_id ';
            $sql .= '           left join mst_unit_names as c2 ';
            $sql .= '             on c2.id = c.per_unit_name_id ';
            
            //在庫のときは実績をJOINしない
            if($this->request->data['search']['type']%3<>0){
                $sql .= '           left join trn_claims as d  ';
                $sql .= '             on d.mst_item_unit_id = c.id  ';
                $sql .= '            and d.is_deleted = false ';
                $sql .= "            and d.is_stock_or_sale = '" . $type . "'";
                $sql .= '           left join mst_departments as e  ';
                $sql .= '             on d.department_id_to = e.id ';
            }
            //仕入(マスタ)の場合
            if($this->request->data['search']['type'] == 4 ){
                $sql .= '           left join mst_transaction_configs as f  ';
                $sql .= '             on f.mst_facility_item_id = b.id ';
                $sql .= '            and f.is_main = true ';
                $sql .= '            and f.is_deleted = false ';
                $sql .= '           left join mst_item_units as g ';
                $sql .= '             on f.mst_item_unit_id = g.id ';
            //売上(マスタ)の場合
            }elseif($this->request->data['search']['type'] == 5 ){
                $sql .= '           left join mst_sales_configs as f  ';
                $sql .= '             on f.mst_item_unit_id = c.id ';
                $sql .= '            and f.partner_facility_id = ' . $f['MstFacility']['id'];
                $sql .= '            and f.start_date <= d.claim_date ';
                $sql .= '            and f.end_date > d.claim_date ';
            //在庫の場合
            }elseif($this->request->data['search']['type'] %3 == 0 ){
                $sql .= '           left join trn_stocks as f  ';
                $sql .= '             on f.mst_item_unit_id = c.id ';
                $sql .= '          inner join mst_departments as g  ';
                $sql .= '             on f.mst_department_id = g.id  ';
                //センターのみの場合
                if($this->request->data['search']['type'] == 3 ){
                    $sql .= '            and g.department_type = ' . Configure::read('DepartmentType.warehouse');
                }
                $sql .= '           left join ( ';
                $sql .= '               select ';
                $sql .= '                   x.mst_facility_item_id ';
                $sql .= '                   , y.price  ';
                $sql .= '               from ';
                $sql .= '               (  ';
                $sql .= '                   select ';
                $sql .= '                       max(a.id)          as id ';
                $sql .= '                       , a.mst_facility_item_id  ';
                $sql .= '                   from ';
                $sql .= '                       trn_appraised_values as a ';
                $sql .= '                   where ';
                $sql .= '                       a.is_deleted = false ';
                $sql .= '                       and a.mst_facility_id = ' . $f['MstFacility']['id'];
                $sql .= '                   group by ';
                $sql .= '                       a.mst_facility_item_id ';
                $sql .= '               ) as x ';
                $sql .= '               left join trn_appraised_values as y ';
                $sql .= '                   on x.id = y.id ';
                $sql .= '           ) as x ';
                $sql .= '             on x.mst_facility_item_id = b.id ';
            }
            $sql .= '         where ';
            $sql .= '           b.mst_facility_id = ' . $f['MstFacility']['center_id'];
            $sql .= "           and a.internal_code in ('" . join('\',\'',$this->request->data['aw']['awid']) . "') ";
            
            //在庫以外
            if($this->request->data['search']['type']%3<>0){
                $sql .= '           and e.mst_facility_id = ' . $f['MstFacility']['id'];
                //日付開始
                if($this->request->data['search']['work_date_from'] != ''){
                    $sql .= " and d.claim_date >= '" . $this->request->data['search']['work_date_from'] . "'";
                }
                //日付終了
                if($this->request->data['search']['work_date_to'] != ''){
                    $sql .= " and d.claim_date <= '" . $this->request->data['search']['work_date_to'] . "'";
                }
            }
            $sql .= '         group by ';
            $sql .= '           a.internal_code ';
            $sql .= '           , b.converted_num ';
            $sql .= '           , c.per_unit ';
            $sql .= '           , c1.unit_name ';
            $sql .= '           , c2.unit_name ';
            //仕入(実績) 、売上(実績)の場合
            if($this->request->data['search']['type'] == 1 || $this->request->data['search']['type'] == 2){
                $sql .= '           , d.unit_price ';
            }
            //仕入(マスタ)の場合
            if($this->request->data['search']['type'] == 4){
                $sql .= '           , f.transaction_price ';
                $sql .= '           , g.per_unit ';
            //売上(マスタ)の場合
            }elseif($this->request->data['search']['type'] == 5){
                $sql .= '           , f.sales_price ';
            //在庫の場合
            }elseif($this->request->data['search']['type']%3==0){
                $sql .= '           , x.price ';
            }

            $sql .= '     ) as y' . $f['MstFacility']['id']  ;
            $sql .= '       on x.internal_code = y' . $f['MstFacility']['id'] . '.internal_code  ';
            $sql .= '       and x.per_unit = y' . $f['MstFacility']['id'] . '.per_unit  ';
            $sql .= '       and x.unit_name = y' . $f['MstFacility']['id'] . '.unit_name  ';
        }
        
        $sql .= '     left join (  ';
        $sql .= '       select ';
        $sql .= '           a.internal_code ';
        $sql .= '           , c.per_unit ';
        //仕入(マスタ)の場合
        if($this->request->data['search']['type'] == 4){
            $sql .= '           , max(trunc( f.transaction_price / g.per_unit * c.per_unit ,2)) as max';
            $sql .= '           , min(trunc( f.transaction_price / g.per_unit * c.per_unit ,2)) as min';
            $sql .= '           , avg(trunc( f.transaction_price / g.per_unit * c.per_unit ,2)) as avg';
            //売上(マスタ)の場合
        }elseif($this->request->data['search']['type'] == 5){
            $sql .= '           , max(f.sales_price) as max';
            $sql .= '           , min(f.sales_price) as min';
            $sql .= '           , avg(f.sales_price) as avg';
            //仕入(実績)、売上(実績)の場合
        }elseif($this->request->data['search']['type'] == 1 || $this->request->data['search']['type'] == 2){
            $sql .= '           , max(d.unit_price) as max';
            $sql .= '           , min(d.unit_price) as min';
            $sql .= '           , avg(d.unit_price) as avg';
        }elseif($this->request->data['search']['type']%3==0){
            //在庫の場合
            $sql .= '           , max(x.price * c.per_unit) as max';
            $sql .= '           , min(x.price * c.per_unit) as min';
            $sql .= '           , avg(x.price * c.per_unit) as avg';
        }
        
        $sql .= '           , (  ';
        $sql .= '             case  ';
        $sql .= '               when c.per_unit = 1  ';
        $sql .= '               then c1.unit_name  ';
        $sql .= "               else c1.unit_name || '(' || c.per_unit || c2.unit_name || ')'  ";
        $sql .= '               end ';
        $sql .= '           )                             as unit_name  ';
        
        $sql .= '         from ';
        $sql .= '           mst_items as a  ';
        $sql .= '          inner join mst_facility_items as b  ';
        $sql .= '             on b.mst_item_id = a.id  ';
        $sql .= '            and b.item_type = ' . Configure::read('Items.item_types.normalitem');
        $sql .= '           left join mst_item_units as c  ';
        $sql .= '             on c.mst_facility_item_id = b.id  ';
        $sql .= '           left join mst_unit_names as c1  ';
        $sql .= '             on c1.id = c.mst_unit_name_id  ';
        $sql .= '           left join mst_unit_names as c2  ';
        $sql .= '             on c2.id = c.per_unit_name_id  ';
        //在庫のときは実績をJOINしない
        if($this->request->data['search']['type']%3<>0){
            $sql .= '           left join trn_claims as d  ';
            $sql .= '             on d.mst_item_unit_id = c.id  ';
            $sql .= '            and d.is_deleted = false ';
            $sql .= "            and d.is_stock_or_sale = '" . $type . "'";
            $sql .= '           left join mst_departments as e  ';
            $sql .= '             on d.department_id_to = e.id ';
        }
        //仕入(マスタ)の場合
        if($this->request->data['search']['type']==4){
            $sql .= '           left join mst_transaction_configs as f  ';
            $sql .= '             on f.mst_facility_item_id = b.id ';
            $sql .= '            and f.is_main = true ';
            $sql .= '            and f.is_deleted = false ';
            $sql .= '           left join mst_item_units as g ';
            $sql .= '             on f.mst_item_unit_id = g.id ';
        //売上(マスタ)の場合
        }elseif($this->request->data['search']['type']==5){
            $sql .= '           left join mst_sales_configs as f  ';
            $sql .= '             on f.mst_item_unit_id = c.id ';
            $sql .= '            and f.start_date <= d.claim_date ';
            $sql .= '            and f.end_date > d.claim_date ';
        //在庫の場合
        }elseif($this->request->data['search']['type']%3==0){
            $sql .= '           left join ( ';
            $sql .= '               select ';
            $sql .= '                   x.mst_facility_item_id ';
            $sql .= '                   , y.price  ';
            $sql .= '               from ';
            $sql .= '               (  ';
            $sql .= '                   select ';
            $sql .= '                       max(a.id)          as id ';
            $sql .= '                       , a.mst_facility_item_id  ';
            $sql .= '                   from ';
            $sql .= '                       trn_appraised_values as a ';
            $sql .= '                   where ';
            $sql .= '                       a.is_deleted = false ';
            $sql .= '                   group by ';
            $sql .= '                       a.mst_facility_item_id ';
            $sql .= '               ) as x ';
            $sql .= '               left join trn_appraised_values as y ';
            $sql .= '                   on x.id = y.id ';
            $sql .= '           ) as x ';
            $sql .= '             on x.mst_facility_item_id = b.id ';
        }
        $sql .= '         where ';
        $sql .= "           a.internal_code in ('" . join('\',\'',$this->request->data['aw']['awid']) . "') ";
        $sql .= '         group by ';
        $sql .= '           a.internal_code ';
        $sql .= '           , c.per_unit ';
        $sql .= '           , c1.unit_name ';
        $sql .= '           , c2.unit_name ';
        $sql .= '     ) as z ';
        $sql .= '       on x.internal_code = z.internal_code  ';
        $sql .= '       and x.per_unit = z.per_unit  ';
        $sql .= '       and x.unit_name = z.unit_name  ';
        $sql .= '   where ';
        $sql .= "     x.internal_code in ('" . join('\',\'',$this->request->data['aw']['awid']) . "') ";
        $sql .= ' order by  ';
        $sql .= ' x.internal_code , ';
        $sql .= ' x.per_unit ';

        return $sql;
    }

    //商品メンテ画面のプルダウンを一括作成
    function _setPullDown(){
        // 保険請求区分
        $_icds = array();
        $this->set('insuranceClaimDepartments', $_icds);
        
        // クラス分類マスタ
        $_classseparations = $this->MstClassSeparation->find('list' ,
                                                             array(
                                                                 'fields' => array('class_separation_code',
                                                                                   'class_separation_name'),
                                                                 )
                                                             );
        
        $this->set ('classseparations', $_classseparations);
        
        // 生物由来区分
        $this->set('biogenous_types',Configure::read('FacilityItems.biogenous_types'));
        // 商品区分
        $this->set('item_types',Configure::read('FacilityItems.item_types'));
        // 課税区分
        $this->set('taxes', Configure::read('FacilityItems.taxes'));
    }
}
