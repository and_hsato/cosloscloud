<?php
/**
 * RecombinationHistory
 * 組換履歴
 * @since 2010/09/23
 */
class RecombinationHistoryController extends AppController {
    var $name = 'RecombinationHistory';
    var $uses = array(
        'MstClass',
        'TrnSticker',
        'TrnStorage',
        'TrnStorageHeader',
        'TrnReceiving',
        'TrnMoveHeader',
        'TrnShipping',
        'TrnStock',
        'MstFacility',
    );

    /**
    * @var array $helpers
    */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');
    
    var $components = array('Stickers');

    public function index(){
        $this->setRoleFunction(27); //組み換え履歴
        $this->search();
    }

    public function search(){
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        //作業区分取得
        $_classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'10');
        $this->set('classes', $_classes);
        $_SearchResult = array();

        App::import("sanitize");
        if(isset($this->request->data['IsSearch']) && $this->request->data['IsSearch'] == '1'){
            $_conditions = array('header' => '', 'detail' => '');

            if($this->request->data['TrnMoveHeader']['work_no'] != ''){
                $_conditions['header'] .= " and a.work_no LIKE '%".Sanitize::escape($this->request->data['TrnMoveHeader']['work_no'])."%'";
            }

            if($this->request->data['TrnMoveHeader']['work_date_from'] != ''){
                $_conditions['header'] .= " and a.work_date >= '".Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnMoveHeader']['work_date_from'])))."'";
            }

            if($this->request->data['TrnMoveHeader']['work_date_to'] != ''){
                $_conditions['header'] .= " and a.work_date < '".Sanitize::escape(date('Y/m/d', strtotime('1 day',strtotime($this->request->data['TrnMoveHeader']['work_date_to']))))."'";
            }

            if($this->request->data['MstFacilityItem']['internal_code'] != ''){
                $_conditions['detail'] .= " and d.internal_code = '".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."'";
            }

            if($this->request->data['MstFacilityItem']['item_code'] != ''){
                $_conditions['header'] .= " and d.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
            }

            if($this->request->data['MstFacilityItem']['item_name'] != ''){
                $_conditions['header'] .= " and d.item_name LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
            }

            if($this->request->data['MstFacilityItem']['standard'] != ''){
                $_conditions['header'] .= " and d.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
            }

            if($this->request->data['TrnSticker']['lot_no'] != ''){
                $_conditions['detail'] .= " and g.lot_no LIKE '%".Sanitize::escape($this->request->data['TrnSticker']['lot_no'])."%'";
            }

            if($this->request->data['MstDealer']['dealer_name'] != ''){
                $_conditions['header'] .= " and f.dealer_name LIKE '%".Sanitize::escape($this->request->data['MstDealer']['dealer_name'])."%'";
            }

            if($this->request->data['TrnSticker']['sticker_no'] != ''){
                $_conditions['detail'] .= " and (g.facility_sticker_no LIKE '%".Sanitize::escape($this->request->data['TrnSticker']['sticker_no'])."%' or i.facility_sticker_no LIKE '%".Sanitize::escape($this->request->data['TrnSticker']['sticker_no'])."%' or g.hospital_sticker_no LIKE '%".Sanitize::escape($this->request->data['TrnSticker']['sticker_no'])."%' or i.hospital_sticker_no LIKE '%".Sanitize::escape($this->request->data['TrnSticker']['sticker_no'])."%')";
            }

            if($this->request->data['TrnShipping']['work_type'] != ''){
                $_conditions['detail'] .= " and b.work_type = '".Sanitize::escape($this->request->data['TrnShipping']['work_type'])."'";
            }

            $_limit = $this->_getLimitCount();
            //表示データ取得
            $_SearchResult = $this->TrnMoveHeader->query($this->getSearchMoveHeaderSQL($_conditions,$_limit));
        }else{
            $this->request->data['TrnMoveHeader']['work_date_from'] = date('Y/m/d', strtotime('-7 day', time()));
            $this->request->data['TrnMoveHeader']['work_date_to'] = date('Y/m/d', time());
        }
        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set("data",$this->request->data);
        $this->set('SearchResult', $this->outputFilter($_SearchResult));
        $this->render('search');
    }

    private function getSearchMoveHeaderSQL($conditions, $limit , $count=false){
        $sql =
            ' select ' .
                ' a.id'.
                ',a.work_no'.
                ',a.created'.
                ',d.internal_code'.
                ',d.item_name'.
                ',d.standard'.
                ',d.item_code'.
                ',e.user_name'.
                ',f.dealer_name'.
            ' from '.
                ' trn_move_headers a'.
                    ' inner join trn_shippings b on'.
                        ' a.id = b.trn_move_header_id'.
                    ' inner join trn_receivings h on'.
                        ' a.id = h.trn_move_header_id'.
                    ' inner join mst_item_units c on'.
                        ' b.mst_item_unit_id = c.id'.
                    ' inner join mst_facility_items d on'.
                        ' c.mst_facility_item_id = d.id'.
                    ' inner join mst_users e on'.
                        ' a.creater = e.id'.
                    ' left join mst_dealers f on'.
                        ' d.mst_dealer_id = f.id'.
                    ' inner join trn_stickers g on'.
                        ' b.trn_sticker_id = g.id'.
                    ' inner join trn_stickers i on'.
                        ' h.trn_sticker_id = i.id'.
            ' where '.
                  ' 1 = 1' .
                ' and a.move_type = '. Configure::read('MoveType.recombination') .
                ($this->isNNs($conditions['header']) ? '' : $conditions['header']) .
                ($this->isNNs($conditions['detail']) ? '' : $conditions['detail']) .
                ' and c.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected').
            ' group by '.
                ' a.id,a.work_no,a.created,d.internal_code,d.item_name,d.standard,d.item_code,e.user_name,f.dealer_name';
        if($this->getSortOrder() === null){
            $sql .= ' order by a.id ASC';
        }else{
            $arrayOrder = $this->getSortOrder();
            $sql .= ' order by '.$arrayOrder[0];
        }

        //全件取得
        $this->set('max' , $this->getMaxCount($sql , 'TrnMoveHeader'));
        
        if($limit > 0){
            $sql .= " limit {$limit} ";
        }
        return $sql;
    }

    public function edit(){
        $checkedId = array();
        $_DetailInfo = array();
        //選択したチェックボックス（移動ヘッダのid）を配列に格納
        $_condition = "";
        foreach($this->request->data['TrnMoveHeader']['Rows'] as $_key => $_val){
            if($_val['selected'] == '1'){
                //移動ヘッダidをkeyにデータを取得
                if($_condition == ""){
                    $_condition .= "a.id = '{$_key}'";
                }else{
                    $_condition .= " or a.id = '{$_key}'";
                }
            }
        }
        //新しいシール
        $_NewStickerDetail = $this->TrnMoveHeader->query($this->SearchNewStickerDetailSQL($_condition));
        //古いシール
        $_OldStickerDetail = $this->TrnMoveHeader->query($this->SearchOldStickerDetailSQL($_condition));

        $res = array();
        foreach($_NewStickerDetail as $_row){
            $r = $_row[0];
            $r['packing_name'] = $this->getPackingName($r['unit_name'],$r['per_unit'],$r['per_unit_name']);
            $res['new'][$r['id']][] = $r;
        }

        foreach($_OldStickerDetail as $_row){
            $res['old'][$_row[0]['id']]['sticker_no'][] = $_row[0]['facility_sticker_no'];
            $res['old'][$_row[0]['id']]['packing_name'][] = $this->getPackingName($_row[0]['unit_name'],$_row[0]['per_unit'],$_row[0]['per_unit_name']);
        }
        $this->set("Res",$res);

        //作業区分
        $_classes = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'10');
        $this->set('classes', $_classes);
        
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Recombin.token' , $token);
        $this->request->data['Recombin']['token'] = $token;
        
        $fid = $this->Session->read('Auth.facility_id_selected');
        $selects = $this->Session->read('Auth.facilities');
        $this->set('facility_name', $selects[$fid]);
    }


    public function result(){
        $now = date('Y/m/d H:i:s.u');
        $ids = array();
        $moveheader_ids = array();

        if($this->request->data['Recombin']['token'] === $this->Session->read('Recombin.token')) {
            $this->Session->delete('Recombin.token');
            
            //トランザクション開始
            $this->TrnMoveHeader->begin();
            
            foreach($this->request->data['TrnSticker']['Rows'] as $id => $val){
                
                if($val['selected'] == 1){
                    $ids[] = $id;
                }
            }
            //行ロック
            $this->TrnMoveHeader->query(' select * from trn_stickers as a where a.id in (' .join(',',$ids). ') for update ');
            
            //更新チェック
            
            //取消し可否チェック
            $sql  = ' select ';
            $sql .= '       a.id ';
            $sql .= '     , a.detail_count ';
            $sql .= '     , count(c.id) ';
            $sql .= '     , (  ';
            $sql .= '       case  ';
            $sql .= '         when a.detail_count = count(c.id)  ';
            $sql .= '         then true  ';
            $sql .= '         else false  ';
            $sql .= '         end ';
            $sql .= '     )                         as is_cancel  ';
            $sql .= '   from ';
            $sql .= '     trn_move_headers as a  ';
            $sql .= '     left join trn_receivings as b  ';
            $sql .= '       on b.trn_move_header_id = a.id  ';
            $sql .= '     left join trn_stickers as c  ';
            $sql .= '       on c.id = b.trn_sticker_id  ';
            $sql .= '     left join mst_departments as d  ';
            $sql .= '       on d.id = c.mst_department_id  ';
            $sql .= '   where ';
            $sql .= '     a.move_type = ' . Configure::read('MoveType.recombination');
            $sql .= '     and c.is_deleted = false  ';
            $sql .= '     and c.has_reserved = false  ';
            $sql .= '     and c.quantity > 0 ';
            $sql .= '     and d.department_type = ' . Configure::read('DepartmentType.warehouse');
            $sql .= "     and c.facility_sticker_no in (  '" . join('\',\'' , $ids) . "')" ;
            
            $sql .= '   group by ';
            $sql .= '     a.id ';
            $sql .= '     , a.detail_count ';
            $res = $this->TrnMoveHeader->query($sql);
            
            if(count($res) == 0 ){
                $this->TrnMoveHeader->rollback();
                $this->Session->setFlash('取り消せない明細を含んでいます', 'growl', array('type'=>'error'));
                $this->request->data['IsSearch'] =  '0';
                $this->index();
                return;
            }
            foreach($res as $r){
                if($r[0]['is_cancel'] == false ){
                    //取消し不可なのでロールバック
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('取り消せない明細を含んでいます', 'growl', array('type'=>'error'));
                    $this->request->data['IsSearch'] =  '0';
                    $this->index();
                    return;
                }
                
                //組替ヘッダ取消し
                $this->TrnMoveHeader->create();
                //SQL実行
                if (!$this->TrnMoveHeader->updateAll(array('TrnMoveHeader.is_deleted'  => 'true',
                                                           'TrnMoveHeader.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                           'TrnMoveHeader.modified'    => "'" . $now . "'"
                                                           )
                                                     ,
                                                     array(
                                                         'TrnMoveHeader.id'  => $r[0]['id'],
                                                         )
                                                     ,
                                                     -1
                                                     )
                    ) {
                    $this->TrnMoveHeader->rollback();
                    $this->Session->setFlash('組替ヘッダの削除に失敗しました', 'growl', array('type'=>'error'));
                    $this->request->data['IsSearch'] =  '0';
                    $this->index();
                    return;
                }
                
                $moveheader_ids[] = $r[0]['id'];
                
                //組替ヘッダに紐付く新シール一覧取得
                $sql  =' select ';
                $sql .='       a.trn_sticker_id ';
                $sql .='     , b.id               as trn_stock_id  ';
                $sql .='   from ';
                $sql .='     trn_receivings as a  ';
                $sql .='     left join trn_stocks as b  ';
                $sql .='       on b.mst_item_unit_id = a.mst_item_unit_id  ';
                $sql .='       and b.mst_department_id = a.department_id_from  ';
                $sql .='   where ';
                $sql .='     a.trn_move_header_id = ' . $r[0]['id'];
                $news = $this->TrnMoveHeader->query($sql);
                
                foreach($news as $n){
                    //新シール削除
                    if (!$this->TrnSticker->updateAll(array('TrnSticker.is_deleted'  => 'true',
                                                            'TrnSticker.quantity'    => 'TrnSticker.quantity - 1',
                                                            'TrnSticker.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                            'TrnSticker.modified'    => "'" . $now . "'"
                                                            )
                                                      ,
                                                      array(
                                                          'TrnSticker.id'  => $n[0]['trn_sticker_id'],
                                                          )
                                                      ,
                                                      -1
                                                      )
                        ) {
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('新シールの削除に失敗しました', 'growl', array('type'=>'error'));
                        $this->request->data['IsSearch'] =  '0';
                        $this->index();
                        return;
                    }
                    //新シール在庫減
                    if (!$this->TrnStock->updateAll(array('TrnStock.stock_count' => 'TrnStock.stock_count - 1',
                                                          'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                          'TrnStock.modified'    => "'" . $now . "'"
                                                          )
                                                    ,
                                                    array(
                                                        'TrnStock.id'  => $n[0]['trn_stock_id'],
                                                        )
                                                    ,
                                                    -1
                                                    )
                        ) {
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('新シールの在庫減に失敗しました', 'growl', array('type'=>'error'));
                        $this->request->data['IsSearch'] =  '0';
                        $this->index();
                        return;
                    }
                }
                
                //組替ヘッダに紐付く旧シール一覧取得
                $sql  =' select ';
                $sql .='       a.trn_sticker_id ';
                $sql .='     , b.id               as trn_stock_id  ';
                $sql .='   from ';
                $sql .='     trn_shippings as a  ';
                $sql .='     left join trn_stocks as b  ';
                $sql .='       on b.mst_item_unit_id = a.mst_item_unit_id  ';
                $sql .='       and b.mst_department_id = a.department_id_from  ';
                $sql .='   where ';
                $sql .='     a.trn_move_header_id =  ' . $r[0]['id']; 
                $olds = $this->TrnMoveHeader->query($sql);
                
                foreach($olds as $o){
                    //旧シール復活
                    if (!$this->TrnSticker->updateAll(array('TrnSticker.is_deleted'  => 'false',
                                                            'TrnSticker.quantity'    => 'TrnSticker.quantity + 1',
                                                            'TrnSticker.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                            'TrnSticker.modified'    => "'" . $now . "'"
                                                            )
                                                      ,
                                                      array(
                                                          'TrnSticker.id'  => $o[0]['trn_sticker_id'],
                                                          )
                                                      ,
                                                      -1
                                                      )
                        ) {
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('旧シールの復活に失敗しました', 'growl', array('type'=>'error'));
                        $this->request->data['IsSearch'] =  '0';
                        $this->index();
                        return;
                    }
                    
                    //旧シール在庫増
                    if (!$this->TrnStock->updateAll(array('TrnStock.stock_count' => 'TrnStock.stock_count + 1 ',
                                                          'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                          'TrnStock.modified'    => "'" . $now . "'"
                                                          )
                                                    ,
                                                    array(
                                                        'TrnStock.id'  => $o[0]['trn_stock_id'],
                                                        )
                                                    ,
                                                    -1
                                                    )
                        ) {
                        $this->TrnMoveHeader->rollback();
                        $this->Session->setFlash('旧シールの在庫増に失敗しました', 'growl', array('type'=>'error'));
                        $this->request->data['IsSearch'] =  '0';
                        $this->index();
                        return;
                    }
                }
            }
            
            //コミット前に更新チェック
            
            //コミット
            $this->TrnMoveHeader->commit();
            
            //表示データを取得
            //新しいシール
            $_NewStickerDetail = $this->TrnMoveHeader->query($this->SearchNewStickerDetailSQL( ' a.id in (' .join(',',$moveheader_ids). ')' ));
            //古いシール
            $_OldStickerDetail = $this->TrnMoveHeader->query($this->SearchOldStickerDetailSQL( ' a.id in (' .join(',',$moveheader_ids). ')' ));
            
            $res = array();
            foreach($_NewStickerDetail as $_row){
                $r = $_row[0];
                $r['packing_name'] = $this->getPackingName($r['unit_name'],$r['per_unit'],$r['per_unit_name']);
                $res['new'][$r['id']][] = $r;
            }
            
            foreach($_OldStickerDetail as $_row){
                $res['old'][$_row[0]['id']]['sticker_no'][] = $_row[0]['facility_sticker_no'];
                $res['old'][$_row[0]['id']]['packing_name'][] = $this->getPackingName($_row[0]['unit_name'],$_row[0]['per_unit'],$_row[0]['per_unit_name']);
            }
            $this->set('Res',$res);
            $this->Session->write('Recombin.result' , $res);
            
        }else{
            $this->set('Res' , $this->Session->read('Recombin.result'));
            
        }
    }
        
    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'seal';
    }

    public function seal($dummy = null){
        $_fno = array();
        foreach($this->request->data['TrnSticker']['Rows'] as $_key => $_val){
            if(false !== strstr($_val['selected'], '1')){
                $_fno[] = $_key;
            }
        }
        
        $fid = $this->request->data['MstFacility']['id'];
        $fname = $this->getFacilityName($fid);
        $stickers = $this->Stickers->getFacilityStickers($_fno, $fname, $fid);
        
        $this->layout = 'xml/default';
        $this->set('stickers', $stickers);
        $this->render('/storages/seal');
        



        
    }

    private function getStickers($cond){
        $sql =
            ' select ' .
                ' MstFacilityItem.internal_code'.
                ',TrnSticker.facility_sticker_no'.
                ',MstFacilityItem.item_name'.
                ',MstFacilityItem.standard'.
                ',MstFacilityItem.item_code'.
                ',MstFacilityItem.insurance_claim_code'.
                ',round((MstFacilityItem.unit_price * MstItemUnit.per_unit * MstFacilityItem.converted_num),2) as price'.
                ',MstItemUnit.per_unit'.
                ',MstUnitName.unit_name'.
                ',MstPerUnitName.unit_name as per_unit_name'.
                ',TrnSticker.lot_no'.
                ',MstFacility.facility_name as owner_name'.
                ',MstDealer.dealer_name'.
                ',MstFacilityItem.biogenous_type'.
                ',MstShelfName.code as shelf_code'.
                ',TrnSticker.validated_date'.
            ' from ' .
                ' trn_stickers as TrnSticker' .
                ' inner join mst_item_units as MstItemUnit on TrnSticker.mst_item_unit_id = MstItemUnit.id'.
                ' inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id'.
                ' left  join mst_dealers as MstDealer on MstFacilityItem.mst_dealer_id = MstDealer.id' .
                ' inner join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id'.
                ' inner join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id'.
                ' left join mst_fixed_counts as MstFixedCount on MstFixedCount.mst_department_id = TrnSticker.mst_department_id and MstFixedCount.mst_item_unit_id = TrnSticker.mst_item_unit_id'.
                ' left join mst_shelf_names as MstShelfName on MstFixedCount.mst_shelf_name_id = MstShelfName.id'.
                ' left join mst_facilities as MstFacility on MstFacilityItem.mst_owner_id = MstFacility.id'.
            ' where ' .
                ' TrnSticker.id in (' . join(',', $cond) . ')' .
            ' order by ' .
                ' TrnSticker.id asc';
        $res = $this->TrnSticker->query($sql);
        $result = array();
        $biogenous_types = Configure::read('FacilityItems.biogenous_types');
        foreach($res as $_row){
            $r = $_row[0];
            $r['packing_name'] = $this->getPackingName($r['unit_name'], $r['per_unit'], $r['per_unit_name']);
            $result[] = array(
                '商品ID'         => $r['internal_code'],
                'シール番号'     => $r['facility_sticker_no'],
                '商品名'         => $r['item_name'],
                '規格'           => $r['standard'],
                '製品番号'     => $r['item_code'],
                'バーコード'     => $r['facility_sticker_no'],
                '保険請求'       => $r['insurance_claim_code'],
                '包装単位'       => $r['packing_name'],
                'ロット番号'     => $r['lot_no'],
                'センター情報'   => $this->request->data['facility_name'],
                '販売元'         => $r['dealer_name'],
                '生物由来'       => ($this->isNNs($r['biogenous_type']) || !isset($biogenous_types[$r['biogenous_type']]) ? '' : $biogenous_types[$r['biogenous_type']]),
                'センター棚番号' => $r['shelf_code'],
                '定価'           => $r['price'],
                '有効期限'       => ($this->isNNs($r['validated_date']) ? '' : date('Y/m/d', strtotime($r['validated_date']))),
            );
        }

        return $result;
    }

    private function SearchOldStickerDetailSQL($condition){
        $sql =
            ' select ' .
                ' a.*'.
                ',b.work_type'.
                ',b.recital'.
                ',g.facility_sticker_no'.
                ',c.per_unit'.
                ',d.unit_name'.
                ',e.unit_name as per_unit_name'.
            ' from '.
                ' trn_move_headers a'.
                    ' inner join trn_shippings b on'.
                        ' a.id = b.trn_move_header_id'.
                    ' inner join mst_item_units c on'.
                        ' b.mst_item_unit_id = c.id'.
                    ' inner join mst_unit_names d on'.
                        ' c.mst_unit_name_id = d.id'.
                    ' inner join mst_unit_names e on'.
                        ' c.per_unit_name_id = e.id'.
                    ' inner join trn_stickers g on'.
                        ' b.trn_sticker_id = g.id'.
            ' where '.
                  ' 1 = 1' .
                ' and a.move_type = '.Configure::read('MoveType.recombination').
                ' and '.$condition;
        return $sql;
    }

    private function SearchNewStickerDetailSQL($condition){
        $sql =
            ' select ' .
                ' a.*'.
                ',b.recital'.
                ',b.work_type'.
                ',g.id as sticker_id'.
                ',g.facility_sticker_no'.
                ',g.lot_no'.
                ',g.validated_date'.
                ',e.user_name'.
                ',f.name'.
                ',h.unit_name'.
                ',i.unit_name as per_unit_name'.
                ',c.per_unit'.
            ' from '.
                ' trn_move_headers a'.
                    ' inner join trn_receivings b on'.
                        ' a.id = b.trn_move_header_id'.
                    ' inner join mst_item_units c on'.
                        ' b.mst_item_unit_id = c.id'.
                    ' inner join mst_users e on'.
                        ' a.modifier = e.id'.
                    ' inner join trn_stickers g on'.
                        ' b.trn_sticker_id = g.id'.
                    ' left join mst_classes f on'.
                        ' b.work_type = f.id'.
                    ' inner join mst_unit_names h on'.
                        ' c.mst_unit_name_id = h.id'.
                    ' inner join mst_unit_names i on'.
                        ' c.per_unit_name_id = i.id'.
            ' where '.
                  ' 1 = 1' .
                ' and a.move_type = '.Configure::read('MoveType.recombination').
                ' and '.$condition;
        return $sql;
    }

    /**
     * 包装単位名を取得
     * @param string $unitName
     * @param string $parUnit
     * @param string $parUnitName
     * @access private
     * @return string
     */
    private function getPackingName($unitName,$parUnit,$parUnitName){
        if((integer)$parUnit === 1){
            return $unitName;
        }else{
            return sprintf('%s（%s%s）',$unitName,$parUnit,$parUnitName);
        }
    }
}
?>
