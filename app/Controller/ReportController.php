<?php
class ReportController extends AppController {
    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'export';
        $this->Auth->allowedActions[] = 'report';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    var $name = 'Report';
    var $uses = array(
        'MstUser',
        'MstAnalyze',
        'MstFacility',
        'MstDepartment',
        'MstReport'
    );

    var $helpers = array('Form', 'Html', 'Time');

    var $components = array('RequestHandler','CsvWriteUtils');

    public function index(){
        $item = array();
        $joins=array();
        $joins[]=array('type'=>'INNER'
                      ,'table'=>'mst_facility_relations'
                      ,'alias'=>'MstFacilityRelation'
                      ,'conditions'=>array('MstFacilityRelation.partner_facility_id = MstFacility.id'
                                          ,'MstFacilityRelation.is_deleted = false'
                                          ,'MstFacilityRelation.mst_facility_id' => $this->Session->read('Auth.facility_id_selected')));
        
        $sql  = ' select ';
        $sql .= '       a.id                    as "MstReport__id"';
        $sql .= '     , a.title                 as "MstReport__title" ';
        $sql .= '     , a.donor_selectable      as "MstReport__donor_selectable" ';
        $sql .= '     , a.facility_selectable   as "MstReport__facility_selectable" ';
        $sql .= '     , a.department_selectable as "MstReport__department_selectable" ';
        $sql .= '     , a.dates_selectable      as "MstReport__dates_selectable" ';
        $sql .= '     , a.daynumber_selectable  as "MstReport__daynumber_selectable" ';
        $sql .= '     , a.free_word             as "MstReport__free_word" ';
        $sql .= '   from ';
        $sql .= '     mst_reports as a ';
        $sql .= '   order by a.id ';
        $item = Set::Combine($this->MstAnalyze->query($sql),'{n}.MstReport.id','{n}.MstReport.title');
        
        $this->set('items' , $item);

        //施主
        $donor = Set::Combine($this->MstFacility->find('all',array(
            'recursive' => -1,
            'conditions' => array(
                'MstFacility.is_deleted' => false,
                'MstFacility.facility_type' => Configure::read('FacilityType.donor'),
            ),
            'joins' =>$joins,
            'order'=>'MstFacility.facility_code'
        )),'{n}.MstFacility.id','{n}.MstFacility.facility_name');

        //センター、病院はユーザに関連づいているものを表示
        $joins[]=array('type'=>'INNER'
                      ,'table'=>'mst_user_belongings'
                      ,'alias'=>'MstUserBelonging'
                      ,'conditions'=>array('MstUserBelonging.mst_facility_id = MstFacilityRelation.partner_facility_id'
                                          ,'MstUserBelonging.is_deleted = false'
                                          ,'MstUserBelonging.mst_user_id' => $this->Session->read('Auth.MstUser.id')));


        //施設
        $selected_facility = $this->MstFacility->find('all',array(
            'recursive' => -1,
            'conditions' => array(
                'MstFacility.is_deleted' => false,
                'MstFacility.id' => $this->Session->read('Auth.facility_id_selected'),
            ),
            'order'=>'MstFacility.facility_type desc , MstFacility.facility_code'
        ));
        $facility_list = $this->MstFacility->find('all',array(
            'recursive' => -1,
            'conditions' => array(
                'MstFacility.is_deleted' => false,
                'MstFacility.facility_type' => array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')),
            ),
            'joins' =>$joins,
            'order' =>'MstFacility.facility_type desc , MstFacility.facility_code',
        ));
        $facility_list = array_merge($selected_facility,$facility_list);
        $facilities = Set::Combine($facility_list,'{n}.MstFacility.id','{n}.MstFacility.facility_name');

        $this->set('items', $item);
        $this->set('donors', $donor);
        $this->set('facilities', $facilities);
        
    }
    
    public function report(){
        $sql  = ' select ';
        $sql .= '      a.sql_data    as "MstReport__sql_data" ';
        $sql .= '    , a.report_file as "MstReport__report_file" ';
        $sql .= '    , a.title       as "MstReport__title"';
        $sql .= '    , a.id          as "MstReport__id"';
        $sql .= '   from ';
        $sql .= '     mst_reports as a ';
        $sql .= '   where a.id = ' . $this->request->data['MstReport']['id'];
        $report_data = $this->MstReport->query($sql);
        $sql   = $report_data[0]['MstReport']['sql_data'];
        $file  = $report_data[0]['MstReport']['report_file'];
        $title = $report_data[0]['MstReport']['title'];
        $id    = $report_data[0]['MstReport']['id'];

        //検索条件置換
        $params = $this->request->data['Report'];
        //操作施設
        if(isset($params['f']) && $params['f'] != ''){
            $sql = preg_replace('@(/\*操作[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*操作\*/)@i', ' ', $sql);
            $sql = preg_replace('/@操作id/i', "'{$params['f']}'", $sql);
        }

        //操作ユーザ
        if(isset($params['u']) && $params['u'] != ''){
            $sql = preg_replace('@(/\*ユーザ[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*ユーザ\*/)@i', ' ', $sql);
            $sql = preg_replace('/@ユーザid/i', "'{$params['u']}'", $sql);
        }

        //施主
        if(isset($params['donor_id']) && $params['donor_id'] != ''){
            $sql = preg_replace('@(/\*施主[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*施主\*/)@i', ' ', $sql);
            $sql = preg_replace('/@施主id/i', "'{$params['donor_id']}'", $sql);
        }
        //施設
        if(isset($params['facility_id']) && $params['facility_id'] != ''){
            $sql = preg_replace('@(/\*施設[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*施設\*/)@i', ' ', $sql);
            $sql = preg_replace('/@施設id/i', "'{$params['facility_id']}'", $sql);
        }
        //部署
        if(isset($params['department_id']) && $params['department_id'] != ''){
            $sql = preg_replace('@(/\*部署[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*部署\*/)@i', ' ', $sql);
            $sql = preg_replace('/@部署id/i', "'{$params['department_id']}'", $sql);
        }
        //日付From
        if(isset($params['day_from']) && $params['day_from'] != ''){
            $sql = preg_replace('@(/\*日付from[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*日付from\*/)@i', ' ', $sql);
            $sql = preg_replace('/@日付from/i', "'{$params['day_from']}'", $sql);
        }else{
            $params['day_from']='';
        }
        
        //日付To
        if(isset($params['day_to']) && $params['day_to'] != ''){
            $sql = preg_replace('@(/\*日付to[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*日付to\*/)@i', ' ', $sql);
            $sql = preg_replace('/@日付to/i', "'{$params['day_to']}'", $sql);
        }else{
            $params['day_to'] = '';
        }
        //日付数値
        if(isset($params['day_num']) && $params['day_num'] != ''){
            $sql = preg_replace('@(/\*日数[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*日数\*/)@i', ' ', $sql);
            $sql = preg_replace('/@日数day/i', "'{$params['day_num']}'", $sql);
        }
        
        /* 推移表対応ココから */
        if($id == 4 || $id == 5 ){ // 商品別消費実績推移表
            // 日付From、日付To両方が空だったら
            if(isset($params['day_from']) && $params['day_from'] == '' && isset($params['day_to']) && $params['day_to'] == ''){
                // Toは月末日
                $month = date('Y-m');
                $params['day_to'] = date('Y/m/d', strtotime('last day of ' . $month));
                // Fromは-6ヶ月分
                $params['day_from'] = date('Y/m/d', strtotime(date('Y-m-1').' -5 month'));
            }
            // 日付Toだけ空の場合
            if(isset($params['day_to']) && $params['day_to'] == ''){
                // Fromの6ヶ月後
                $params['day_to'] = date('Y/m/d' , strtotime( $params['day_from'] . ' +5 month'));
            }
            // 日付Fromだけ空の場合
            if(isset($params['day_from']) && $params['day_from'] == ''){
                // Toの6ヵ月前
                $params['day_from'] = date('Y/m/d' , strtotime( $params['day_to'] . ' -5 month'));
            }
            // 置換処理
            $sql = preg_replace('@(/\*日付from[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*日付from\*/)@i', ' ', $sql);
            $sql = preg_replace('/@日付from/i', "'{$params['day_from']}'", $sql);
            $sql = preg_replace('@(/\*日付to[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*日付to\*/)@i', ' ', $sql);
            $sql = preg_replace('/@日付to/i', "'{$params['day_to']}'", $sql);
            
            // D1～D6のラベル対応
            $D1 = date('Y/m' , strtotime( $params['day_from']));
            $D2 = date('Y/m' , strtotime( $params['day_from'] . ' +1 month'));
            $D3 = date('Y/m' , strtotime( $params['day_from'] . ' +2 month'));
            $D4 = date('Y/m' , strtotime( $params['day_from'] . ' +3 month'));
            $D5 = date('Y/m' , strtotime( $params['day_from'] . ' +4 month'));
            $D6 = date('Y/m' , strtotime( $params['day_from'] . ' +5 month'));
            
            $sql = preg_replace('@(/\*D1ラベル[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*D1ラベル\*/)@i', ' ', $sql);
            $sql = preg_replace('/@D1ラベル/i', "'{$D1}'", $sql);
            $sql = preg_replace('@(/\*D2ラベル[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*D2ラベル\*/)@i', ' ', $sql);
            $sql = preg_replace('/@D2ラベル/i', "'{$D2}'", $sql);
            $sql = preg_replace('@(/\*D3ラベル[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*D3ラベル\*/)@i', ' ', $sql);
            $sql = preg_replace('/@D3ラベル/i', "'{$D3}'", $sql);
            $sql = preg_replace('@(/\*D4ラベル[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*D4ラベル\*/)@i', ' ', $sql);
            $sql = preg_replace('/@D4ラベル/i', "'{$D4}'", $sql);
            $sql = preg_replace('@(/\*D5ラベル[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*D5ラベル\*/)@i', ' ', $sql);
            $sql = preg_replace('/@D5ラベル/i', "'{$D5}'", $sql);
            $sql = preg_replace('@(/\*D6ラベル[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*D6ラベル\*/)@i', ' ', $sql);
            $sql = preg_replace('/@D6ラベル/i', "'{$D6}'", $sql);
            
            // 平均抽出の為、From/Toの間の月数を取得
            $date1  = strtotime($params['day_from']);
            $date2  = strtotime($params['day_to']);
            $month1 = date("Y",$date1)*12+date("m",$date1);
            $month2 = date("Y",$date2)*12+date("m",$date2);
            
            $AVE = ($month2 - $month1) + 1;
            
            $sql = preg_replace('@(/\*平均[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*平均\*/)@i', ' ', $sql);
            $sql = preg_replace('/@平均/i', "{$AVE}", $sql);
            
            // 検索フリーワード
            if(isset($params['free_word']) && $params['free_word'] != ''){
                $sql = preg_replace('@(/\*フリーワード[^a-zA-Z0-9 ]*)|([^a-zA-Z0-9 ]*フリーワード\*/)@i', ' ', $sql);
                $sql = preg_replace('/@フリーワード/i', "{$params['free_word']}", $sql);
            }
        }
        /* 推移表対応ココまで */

        $result = $this->MstReport->query($sql);
        $columns = array_keys($result[0][0]);
        
        foreach($result as $r){
            $tmp = array();
            foreach($columns as $c){
                $tmp[$c] = $r[0][$c];
            }
            $data[] = $tmp;
        }

        
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'   => $title,
                           '印刷年月日' => date('Y/m/d'),
                           '開始日'     => $params['day_from'],
                           '終了日'     => $params['day_to'],
                           );
        $this->xml_output($data,join("\t",$columns),$layout_name[$file],$fix_value);
    }

    public function getReportMethod($id){
        Configure::write('debug', 0);
        if(false === $this->RequestHandler->isAjax()){
            return;
        }
        $this->layout = 'ajax';
        $res = $this->MstReport->find('first', array(
            'fields' => array(
                'donor_selectable',
                'facility_selectable',
                'department_selectable',
                'dates_selectable',
                'daynumber_selectable',
                'free_word'
                ),
            'recursive' => -1,
            'conditions' => array(
                'id' => $id,
            ),
            'order'=>'MstReport.id'
        ));
        $this->set('value', json_encode($res));
        $this->render('/storages/ajax');
    }

    public function getDepartments($id){
        Configure::write('debug', 0);
        if(false === $this->RequestHandler->isAjax()){
            return;
        }
        $this->layout = 'ajax';
        $res = $this->MstDepartment->find('all', array(
            'fields' => array(
                'id',
                'department_name',
            ),
            'recursive' => -1,
            'conditions' => array(
                'mst_facility_id' => $id,
            ),
            'order'=>'MstDepartment.department_code'
        ));
        $this->set('value', json_encode($res));
        $this->render('/storages/ajax');
    }
    
}
?>
