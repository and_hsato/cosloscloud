<?php
/**
 * SetItemsController
 *　セット品
 */
class SetItemsController extends AppController {

  /**
   * @var $name
   */
  var $name = 'SetItems';

  /**
   * @var array $uses
   */
  var $uses = array('MstClass'
                    ,'MstDepartment'
                    ,'MstFacility'
                    ,'MstFacilityItem'
                    ,'MstUser'
                    ,'TrnSticker'
                    ,'TrnStock'
                    ,'TrnSetStock'
                    ,'TrnSetStockHeader'
                    );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    public function  beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'seal';
        $this->Auth->allowedActions[] = 'report';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        //CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }

    }


    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker' );

    /**
     * @var array $components
     */
    var $components = array('xml','Common','Stickers','RequestHandler','utils' , 'Barcode','CsvWriteUtils','CsvReadUtils');

    /**
     *
     */
    function index (){
        $this->render('/login/home');
    }


    /**
     * セット品登録初期画面
     */
    function set_items(){
        App::import('Sanitize');
        $this->setRoleFunction(100); //セット品
        $result = array();
        if(isset($this->request->data['search']['is_search'])){
            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $where = "";

            $where .= "  and a.is_deleted = false";
            $where .= "  and a.mst_facility_id = " .$this->Session->read('Auth.facility_id_selected');
            if(isset($this->request->data['MstSetItems']['internal_code']) && $this->request->data['MstSetItems']['internal_code'] !== ''){
                $where .=" and a.internal_code ilike '%" .$this->request->data['MstSetItems']['internal_code']."%'";
            }
            if(isset($this->request->data['MstSetItems']['standard']) && $this->request->data['MstSetItems']['standard'] !== '' ){
                $where .=" and a.standard ilike '%" .$this->request->data['MstSetItems']['standard']."%'";
            }
            if(isset($this->request->data['MstSetItems']['item_name']) && $this->request->data['MstSetItems']['item_name'] !== ''){
                $where .=" and a.item_name ilike '%" .$this->request->data['MstSetItems']['item_name']."%'";
            }
            if(isset($this->request->data['MstSetItems']['item_code']) && $this->request->data['MstSetItems']['item_code'] !== ''){
                $where .=" and a.item_code ilike '%" .$this->request->data['MstSetItems']['item_code']."%'";
            }
            //表示データ
            $result = $this->_getHeaderData($where , $this->request->data['limit'] );
            $this->request->data = $data;
        }
        $this->set('result' , $result);
        $this->set('data',$this->request->data );
    }

    /**
     * セット品登録シール読込
     */
    function read_stickers(){
        $where = ' and a.id = ' . $this->request->data['MstSetItem']['id'];
        $result = $this->_getHeaderData($where);
        $this->request->data = $result[0];

        /* セット品構成要素情報 */
        $this->set('setDetails' , $this->_getSetDetails());
    }

    /**
     * セット品登録在庫選択
     */
    function select_stock(){

        if(isset($this->request->data['add_search']['is_search'])){

            $where = '';
            $where .= '  and b.is_lowlevel = false ';       //低レベル品は対象外
            $where .= '  and a.is_deleted  = false ';       //削除済みは対象外
            $where .= '  and a.quantity > 0 ';              //数量が0のものは対象外
            $where .= '  and a.has_reserved = false ';      //予約済みのものは対象外
            $where .= "  and a.lot_no != ' ' ";             //入荷済み未入庫は出さない
            $where .= "  and b.item_type = " . Configure::read('Items.item_types.normalitem');//通常品のシールのみ（セット品のシールは出さない）

            $where .= '  and g.mst_facility_id = '. $this->Session->read('Auth.facility_id_selected');

            //商品ID
            if(isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] !== ''){
                $where .= "  and b.internal_code = '" . $this->request->data['MstFacilityItem']['internal_code'] . "'";
            }
            //製品番号
            if(isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] !== ''){
                $where .= "  and b.item_code like '%" . $this->request->data['MstFacilityItem']['item_code'] . "%'";
            }
            //商品名
            if(isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] !== ''){
                $where .= "  and b.item_name like '%" . $this->request->data['MstFacilityItem']['item_name'] . "%'";
            }
            //規格
            if(isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] !== ''){
                $where .= "  and b.standard like '%" . $this->request->data['MstFacilityItem']['standard'] . "%'";
            }

            //販売元
            if(isset($this->request->data['MstFacilityItem']['dealer_name']) && $this->request->data['MstFacilityItem']['dealer_name'] !== ''){
                $where .= "  and d.dealer_name like '%" . $this->request->data['MstFacilityItem']['dealer_name'] . "%'";
            }

            //JANコード(前方一致）
            if(isset($this->request->data['MstFacilityItem']['jan_code']) && $this->request->data['MstFacilityItem']['jan_code'] !== ''){
                $where .= "  and b.jan_code like '" . $this->request->data['MstFacilityItem']['jan_code'] . "%'";
            }

           if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and a.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 = " and a.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 = " and 0 = 1 ";
            }

            

            $CartSearchResult = array();
            $CartReSearchResult = array();
            $CartSearchResult = $this->_getData($where2 , null );
            //選択商品のＩＤを取得
            $aryCart = explode(",",$this->request->data['cart']['tmpid']);
            $ArrayValue = array();
            $ArrayValue = array_count_values($aryCart);
            //選択商品を再表示（カート）
            foreach($CartSearchResult as $key => $item){
                //選択中の商品数
                $count = $ArrayValue[$item['MstSetItem']['id']];
                if (isset($count) && $count > 0){
                    for($j = 0; $j< $count; $j++){
                        $CartReSearchResult[] = array(
                            'MstSetItem'=> array(
                                'id'                    => $item['MstSetItem']['id']
                                ,'internal_code'        => $item['MstSetItem']['internal_code']
                                ,'item_name'            => $item['MstSetItem']['item_name']
                                ,'item_code'            => $item['MstSetItem']['item_code']
                                ,'lot_no'               => $item['MstSetItem']['lot_no']
                                ,'facility_sticker_no'  => $item['MstSetItem']['facility_sticker_no']
                                ,'standard'             => $item['MstSetItem']['standard']
                                ,'dealer_name'          => $item['MstSetItem']['dealer_name']
                                ,'transaction_price'    => $item['MstSetItem']['transaction_price']
                                ,'original_price'       => $item['MstSetItem']['original_price']
                                ,'sales_price'          => $item['MstSetItem']['sales_price']
                                ,'validated_date'       => $item['MstSetItem']['validated_date']
                                ,'item_unit_id'         => $item['MstSetItem']['item_unit_id']
                                ,'unit_name'            => $item['MstSetItem']['unit_name']
                                ,'quantity'             => $item['MstSetItem']['quantity']
                                ,'stock_count'          => $item['MstSetItem']['stock_count']
                                ,'stock_id'             => $item['MstSetItem']['stock_id']
                                )
                            );
                    }
                }
            }
            $SearchResult = array();
            $ReSearchResult = array();
            $SearchResult = $this->_getData($where  , $this->request->data['limit'] );
            $blnExit = false; //件数リミットでループ終了フラグ
            foreach($SearchResult as $key => $item){
                if (array_key_exists($item['MstSetItem']['id'], $ArrayValue)){
                    //選択中の商品数
                    $count = $ArrayValue[$item['MstSetItem']['id']];
                }else{
                    $count = 0;
                }
                if (isset($count) && $count > 0){
                    //シール数 - 選択商品数 = ループ数
                    $quantity = $item['MstSetItem']['quantity'] - $count;
                }else{
                    $quantity = $item['MstSetItem']['quantity'];
                }
                for($i = 0; $i< $quantity; $i++){
                    $ReSearchResult[] = array(
                        'MstSetItem'=> array(
                            'id'                    => $item['MstSetItem']['id']
                            ,'internal_code'        => $item['MstSetItem']['internal_code']
                            ,'item_name'            => $item['MstSetItem']['item_name']
                            ,'item_code'            => $item['MstSetItem']['item_code']
                            ,'lot_no'               => $item['MstSetItem']['lot_no']
                            ,'facility_sticker_no'  => $item['MstSetItem']['facility_sticker_no']
                            ,'standard'             => $item['MstSetItem']['standard']
                            ,'dealer_name'          => $item['MstSetItem']['dealer_name']
                            ,'transaction_price'    => $item['MstSetItem']['transaction_price']
                            ,'original_price'       => $item['MstSetItem']['original_price']
                            ,'sales_price'          => $item['MstSetItem']['sales_price']
                            ,'validated_date'       => $item['MstSetItem']['validated_date']
                            ,'item_unit_id'         => $item['MstSetItem']['item_unit_id']
                            ,'unit_name'            => $item['MstSetItem']['unit_name']
                            ,'quantity'             => $item['MstSetItem']['quantity']
                            ,'stock_count'          => $item['MstSetItem']['stock_count']
                            ,'stock_id'             => $item['MstSetItem']['stock_id']
                            )
                        );
                    if (count($ReSearchResult) >= (integer)$this->request->data['limit']){
                        $blnExit = TRUE;
                        break;
                    }
                }
                if ($blnExit == TRUE){
                    break;
                }
            }
            $this->set('SearchResult'     , $ReSearchResult);
            $this->set('CartSearchResult' , $CartReSearchResult);
        } else {
            // 初回のみヘッダ情報の取得を行う
            $where  = ' and a.id = ' . $this->request->data['MstSetItem']['id'];

            $result = $this->_getHeaderData($where);
            $this->request->data = $result[0];
        }
        /* セット品構成要素情報 */
        $this->set('setDetails' , $this->_getSetDetails());
    }

    /**
     * セット品登録確認
      */
    function set_item_confirm(){
        // 作業区分を取得
        $this->set('work_type' , $this->getClassesList($this->Session->read('Auth.facility_id_selected'),
                                                       11
                                                       ));

        /* セット品構成要素情報 */
        $this->set('setDetails' , $this->_getSetDetails());

        $kawatetu_quantity = array();
        $kawatetu_result = array();
        $reserveCheck = array();  //引当予約チェック用

        // シール読込から来た場合
        if(isset($this->request->data['codez'])){
            $codez = explode(',',substr($this->request->data['codez'] , 0 , -1));
            foreach($codez as $code){
                $_barcode = $this->Barcode->readEan128($code);

                if($_barcode != false){
                    //川鉄シールの場合
                    $item = $this->kawatetu($_barcode);
                    if($item != false ){
                        if(!isset($kawatetu_quantity[$item[0]['MstSetItem']['id']])){
                            $kawatetu_quantity[$item[0]['MstSetItem']['id']] = $item[0]['MstSetItem']['quantity'];
                            $item[0]['MstSetItem']['quantity'] = 1;
                        }else{
                            if($kawatetu_quantity[$item[0]['MstSetItem']['id']] == 0){
                                $item[0]['MstSetItem']['status'] = '数量なし';
                            }else{
                                $kawatetu_quantity[$item[0]['MstSetItem']['id']]--;
                                $item[0]['MstSetItem']['quantity'] = 1;
                            }
                        }
                        $kawatetu_result[] = $item[0];
                    }
                }else{
                    //通常シールの場合
                    $where = "    and a.facility_sticker_no in ('" . $code ."') ";
                    $ret = $this->_getData($where);
                    if(empty($ret)){
                        //該当シールなしの場合ダミーデータを入れておく
                        $ret[0]['MstSetItem']['id']                  = '';
                        $ret[0]['MstSetItem']['internal_code']       = '';
                        $ret[0]['MstSetItem']['item_name']           = '';
                        $ret[0]['MstSetItem']['item_code']           = '';
                        $ret[0]['MstSetItem']['lot_no']              = '';
                        $ret[0]['MstSetItem']['facility_sticker_no'] = $code;
                        $ret[0]['MstSetItem']['standard']            = '';
                        $ret[0]['MstSetItem']['dealer_name']         = '';
                        $ret[0]['MstSetItem']['transaction_price']   = 0;
                        $ret[0]['MstSetItem']['original_price']      = 0;
                        $ret[0]['MstSetItem']['sales_price']         = 0;
                        $ret[0]['MstSetItem']['validated_date']      = '';
                        $ret[0]['MstSetItem']['item_unit_id']        = '';
                        $ret[0]['MstSetItem']['unit_name']           = '';
                        $ret[0]['MstSetItem']['quantity']            = 0;
                        $ret[0]['MstSetItem']['stock_count']         = 0;
                        $ret[0]['MstSetItem']['stock_id']            = '';
                        $ret[0]['MstSetItem']['department_id']       = '';
                        $ret[0]['MstSetItem']['status']              = '無効シール';
                    }
                    $result[] = $ret[0];
                }
            }
        }

        // 在庫選択から来た場合
        if(isset($this->request->data['cart']['tmpid'])){
            $where ='     and a.id in ('. $this->request->data['cart']['tmpid'] .") and a.facility_sticker_no != '' ";
            $result = $this->_getData($where);

            $aryCart = explode(",",$this->request->data['cart']['tmpid']);
            $ArrayValue = array_count_values($aryCart);
            $i = 0;
            foreach( $ArrayValue as $sticker_id => $count ){
                $tmp = $this->_getData(' and a.id = ' . $sticker_id . " and a.facility_sticker_no = '' ");
                if(!empty($tmp)){
                    for($j = 0 ; $j < $count ; $j++){
                        $kawatetu_result[$i] = $tmp[0];
                        $kawatetu_result[$i]['MstSetItem']['quantity'] = 1;
                        $i++;
                    }
                }
            }
        }

        //通常シールと川鉄シールをマージ
        if(!empty($result) && !empty($kawatetu_result)){
            //両方が空でない場合マージ
            $result = array_merge($result ,$kawatetu_result);
        }else if(!empty($kawatetu_result)){
            //川鉄だけを読み込んだ場合はマージしない。
            $result = $kawatetu_result;
        }

        //引当予約済みシール
        //予約数チェック
        for($i=0;$i < count($result);$i++){
            if($result[$i]['MstSetItem']['item_unit_id']!=''){
                if(!isset($reserveCheck[$result[$i]['MstSetItem']['item_unit_id']])) {
                    $sql  = ' select ';
                    $sql .= '       x.mst_item_unit_id ';
                    $sql .= '     , x.stock_count - (coalesce(x.count, 0) - coalesce(y.count, 0)) as stock_count ';
                    $sql .= '   from ';
                    $sql .= '     (  ';
                    $sql .= '       select ';
                    $sql .= '             a.mst_item_unit_id ';
                    $sql .= '           , a.mst_department_id ';
                    $sql .= '           , b.stock_count ';
                    $sql .= '           , (b.reserve_count) as count  ';
                    $sql .= '         from ';
                    $sql .= '           trn_stickers as a  ';
                    $sql .= '           left join trn_stocks as b  ';
                    $sql .= '             on a.mst_item_unit_id = b.mst_item_unit_id  ';
                    $sql .= '             and a.mst_department_id = b.mst_department_id  ';
                    $sql .= '         where ';
                    $sql .= '           a.id = ' . $result[$i]['MstSetItem']['id'];
                    $sql .= '     ) as x  ';
                    $sql .= '     left join (  ';
                    $sql .= '       select ';
                    $sql .= '             count(*)   as count ';
                    $sql .= '           , a.mst_item_unit_id ';
                    $sql .= '           , a.mst_department_id  ';
                    $sql .= '         from ';
                    $sql .= '           trn_stickers as a  ';
                    $sql .= '         where ';
                    $sql .= '           a.is_deleted = false  ';
                    $sql .= '           and a.has_reserved = true  ';
                    $sql .= '         group by ';
                    $sql .= '           a.mst_item_unit_id ';
                    $sql .= '           , a.mst_department_id ';
                    $sql .= '     ) as y  ';
                    $sql .= '       on x.mst_item_unit_id = y.mst_item_unit_id  ';
                    $sql .= '       and x.mst_department_id = y.mst_department_id ';

                    $ret = $this->TrnSticker->query($sql);

                    $reserveCheck[$ret[0][0]['mst_item_unit_id']] = $ret[0][0]['stock_count'];
                }

                if($reserveCheck[$result[$i]['MstSetItem']['item_unit_id']] > 0){
                    //使える在庫がある。
                    $reserveCheck[$result[$i]['MstSetItem']['item_unit_id']]--;
                }else{
                    //使える在庫がない。
                    $result[$i]['MstSetItem']['status'] = '引当予約済み';
                }
            }
        }
        $this->set('result' , $result);

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('SetItem.token' , $token);
        $this->request->data['SetItem']['token'] = $token;
    }

    /**
      * セット品登録結果
      */
    function set_item_result(){
        // 作業区分を取得
        $this->set('work_type' , $this->getClassesList($this->Session->read('Auth.facility_id_selected'),
                                                       11
                                                       ));

        if($this->request->data['SetItem']['token'] === $this->Session->read('SetItem.token')) {
            $this->Session->delete('SetItem.token');
            $where='';
            //登録処理メイン
            $ret = $this->setItem_regist();
            if($ret === false){
                //登録失敗
                $this->Session->setFlash('登録が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
                $where  =' and 1 = 0 ' ;
            }else{
                $this->request->data['MstSetItem']['id'] = $ret;
                $where = ' and i.trn_set_stock_header_id = ' .$ret;
            }

            //明細を表示
            $result = $this->_getData($where);

            $this->set('result' , $result);
            $this->Session->write('SetItem.result' , $result);
            $this->Session->write('SetItem.sticker_no' , $this->request->data['MstSetItem']['sticker_no']);
            $this->Session->write('SetItem.id' , $this->request->data['MstSetItem']['id']);
            $this->request->data['MstSetItem']['mst_facility_id']   = $this->Session->read('Auth.facility_id_selected');
            $this->request->data['MstSetItem']['mst_facility_name'] = $this->getFacilityName($this->Session->read('Auth.facility_id_selected'));
        }else{
            $this->set('result' , $this->Session->read('SetItem.result'));
            $this->request->data['MstSetItem']['id']                = $this->Session->read('SetItem.id');
            $this->request->data['MstSetItem']['sticker_no']        = $this->Session->read('SetItem.sticker_no');
            $this->request->data['MstSetItem']['mst_facility_id']   = $this->Session->read('Auth.facility_id_selected');
            $this->request->data['MstSetItem']['mst_facility_name'] = $this->getFacilityName($this->Session->read('Auth.facility_id_selected'));
        }
    }


    /**
     * セット品登録処理
     **/
    function setItem_regist(){
        //トランザクション開始
        $this->TrnSticker->begin();

        //行ロック
        $this->TrnSticker->query("select * from trn_stickers as a where a.id in (" . $this->request->data['select']['stickers'] . ") for update ");

        //更新チェックを行う
        $ret = $this->TrnSticker->query("select count(*) as count from trn_stickers as a where a.id in (" . $this->request->data['select']['stickers'] . ") and a.modified > '"  . $this->request->data['MstSetItem']['time'] . "'" );
        if($ret[0][0]['count'] > 0 ){
            $this->TrnSticker->rollback();
            return false;
        }

        $now = date('Y/m/d H:i:s.u');
        // 選択したシールで関連情報を取得
        $wk_no         = $this->setWorkNo4Header( date('Ymd') , 11);
        $department_id = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'), Configure::read('DepartmentType.warehouse'));

        //シールに紐付く金額
        $total_original_price    = 0;
        $total_transaction_price = 0;
        $total_sales_price       = 0;

        $ResultData=array();
        foreach(explode(',' , $this->request->data['select']['stickers']) as $sticker_id){
            $tmp = $this->_getData( ' and a.id = ' . $sticker_id );
            $ResultData[] = $tmp[0];
        }
        // それぞれの価格のトータル計算
        foreach($ResultData as $data ){
            $total_original_price    +=  $data['MstSetItem']['original_price'];
            $total_transaction_price +=  $data['MstSetItem']['transaction_price'];
            $total_sales_price       +=  $data['MstSetItem']['sales_price'];
        }

        $validate = array();

        // 構成要素の在庫数を減らす
        foreach($ResultData as $data) {
            if(!empty($data['MstSetItem']['validated_date'])){
                $validate[] = $data['MstSetItem']['validated_date'];
            }

            if(!empty($data['MstSetItem']['facility_sticker_no'])){
                $this->TrnStock->create();
                //SQL実行
                if (!$this->TrnStock->updateAll(array(  'TrnStock.stock_count' => 'TrnStock.stock_count - ' . $data['MstSetItem']['quantity'],
                                                        'TrnStock.is_deleted'  => 'false',
                                                        'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                        'TrnStock.modified'    => "'" . $now . "'"
                                                        )
                                                ,
                                                array(
                                                    'TrnStock.id'  => $data['MstSetItem']['stock_id'],
                                                    )
                                                ,
                                                -1
                                                )
                    ) {
                    $this->TrnSticker->rollback();
                    return false;
                }
            }else{
                //川鉄の場合、センターシール番号は空
                //川鉄用のシールレコードの数量を減算する。
                $this->TrnSticker->create();
                //SQL実行
                if (!$this->TrnSticker->updateAll(array(  'TrnSticker.quantity'   => 'TrnSticker.quantity - 1',
                                                    'TrnSticker.is_deleted'  => 'false',
                                                    'TrnSticker.modifier'    => $this->Session->read('Auth.MstUser.id'),
                                                    'TrnSticker.modified'    => "'" . $now . "'"
                                                    )
                                                ,
                                                array(
                                                    'TrnSticker.id'  => $data['MstSetItem']['id'],
                                                    )
                                             ,
                                              -1
                                             )
                    ) {
                    $this->TrnSticker->rollback();
                    return false;
                }
            }
        }

        $min_validate = null;
        //構成要素のうち、最小値の有効期限を取得
        if (!empty($validate)) {
            $min_validate = min($validate);
        }

        // 構成要素のシールを無効にする。
        foreach($ResultData as &$data ) {
            if(!empty($data['MstSetItem']['facility_sticker_no'])){
                //通常の場合
                $stickers_data[] = array(
                    'TrnSticker' => array(
                        'id'          => $data['MstSetItem']['id'],
                        'quantity'    => 0,
                        'is_deleted'  => true,
                        'modifier'    => $this->Session->read('Auth.MstUser.id'),
                        'modified'    => $now
                        )
                    );
            }else{
                //川鉄の場合、センターシール番号は空

                //削除状態のシールを新規に発版する
                $kawatetu_stickers_data = array(
                    'TrnSticker' => array(
                        'id'                  => false,
                        'facility_sticker_no' => $this->getCenterStickerNo(date('Y/m/d') ,$this->getFacilityCode($this->Session->read('Auth.facility_id_selected'))),
                        'hospital_sticker_no' => '',
                        'mst_item_unit_id'    => $data['MstSetItem']['item_unit_id'],
                        'mst_department_id'   => $data['MstSetItem']['department_id'],
                        'trade_type'          => Configure::read('ClassesType.Constant'),
                        'lot_no'              => $data['MstSetItem']['lot_no'],
                        'validated_date'      => $data['MstSetItem']['validated_date'],
                        'original_price'      => $data['MstSetItem']['original_price'],
                        'transaction_price'   => $data['MstSetItem']['transaction_price'],
                        'quantity'            => 0,
                        'has_reserved'        => false,
                        'is_deleted'          => true,
                        'modifier'            => $this->Session->read('Auth.MstUser.id'),
                        'modified'            => $now,
                        'creater'             => $this->Session->read('Auth.MstUser.id'),
                        'created'             => $now
                        )
                    );
                $this->TrnSticker->create();
                if(!$this->TrnSticker->save($kawatetu_stickers_data)){
                    $this->TrnSticker->rollback();
                    return false;
                }
                $data['MstSetItem']['id'] = $this->TrnSticker->getLastInsertID();
            }
        }

        //foreach の参照渡しなので後始末をしておく
        unset($data);

        $this->TrnSticker->create();
        //SQL実行
        if(isset($stickers_data)){
            //すべて川鉄の場合値がNULLのままなので登録はスキップ
            if (!$this->TrnSticker->saveAll($stickers_data , array('validates' => true,'atomic' => false))) {
                $this->TrnSticker->rollback();
                return false;
            }
        }

        // セット品履歴を作る
        $TrnSetStockHeader = array(
            'TrnSetStockHeader'=> array(
                'work_no'      => $wk_no ,
                'work_date'    => date('Y/m/d') ,
                'recital'      => $this->request->data['MstSetItem']['recital'],
                'detail_count' => count($ResultData),
                'is_deleted'   => false ,
                'creater'      => $this->Session->read('Auth.MstUser.id'),
                'modifier'     => $this->Session->read('Auth.MstUser.id'),
                'created'      => $now,
                'modified'     => $now,
                )
            );


        $this->TrnSetStockHeader->create();
        // SQL実行
        if (!$this->TrnSetStockHeader->save($TrnSetStockHeader ,  array('validates' => true,'atomic' => false))) {
            $this->TrnSticker->rollback();
            return false;
        }

        $setStockId = $this->TrnSetStockHeader->getLastInsertID();

        // セット品明細
        $seq = 1;
        foreach($ResultData as $data) {
            $setstock_data[] = array(
                'TrnSetStock' => array(
                    'work_no'                 => $wk_no ,
                    'work_seq'                => $seq,
                    'work_date'               => date('Y/m/d'),
                    'work_type'               => '',
                    'recital'                 => '',
                    'mst_item_unit_id'        => $data['MstSetItem']['item_unit_id'],
                    'trn_sticker_id'          => $data['MstSetItem']['id'],
                    'trn_set_stock_header_id' => $setStockId,
                    'is_deleted'              => false,
                    'creater'                 => $this->Session->read('Auth.MstUser.id'),
                    'modifier'                => $this->Session->read('Auth.MstUser.id'),
                    'created'                 => $now,
                    'modified'                => $now,
                    )
                );
            $seq++;
        }

        $this->TrnSetStock->create();

        //SQL実行
        $ret = $this->TrnSetStock->saveAll($setstock_data , array('validates' => true,'atomic' => false));
        if (!$ret) {
            $this->TrnSticker->rollback();
            return false;
        }

        //セット品のシールを発版
        $stickerNo = $this->getCenterStickerNo(date('Y/m/d') ,$this->getFacilityCode($this->Session->read('Auth.facility_id_selected')));

        // セットのシールを作る
        $sticker_data = array(
            //シール
            'TrnSticker' => array(
                'facility_sticker_no' => $stickerNo,
                'mst_item_unit_id'    => $this->request->data['MstSetItem']['item_unit_id'],
                'mst_department_id'   => $department_id ,
                'trade_type'          => Configure::read('ClassesType.Constant'),
                'trn_set_stock_id'    => $setStockId,
                'quantity'            => 1,
                'lot_no'              => '',
                'validated_date'      => $min_validate,
                'original_price'      => $total_original_price,
                'sales_price'         => $total_sales_price,
                'transaction_price'   => $total_transaction_price,
                'is_deleted'          => false,
                'has_reserved'        => false,
                'creater'             => $this->Session->read('Auth.MstUser.id'),
                'modifier'            => $this->Session->read('Auth.MstUser.id'),
                'created'             => $now,
                'modified'            => $now,
                )
            );

        $this->request->data['MstSetItem']['sticker_no'] = $stickerNo;

        $this->TrnSticker->create();

        //SQL実行
        if (!$this->TrnSticker->save($sticker_data,  array('validates' => true,'atomic' => false))) {
            $this->TrnSticker->rollback();
            return false;
        }

        $this->request->data['MstSetItem']['sticker_id'] = $this->TrnSticker->getLastInsertID();

        // セットの在庫を増やす。（在庫レコードがなければ作る。）
        $search = array();
        $search['conditions']['TrnStock.mst_item_unit_id']  = $this->request->data['MstSetItem']['item_unit_id'];
        $search['conditions']['TrnStock.mst_department_id'] = $department_id;
        $search['conditions']['TrnStock.is_deleted']        = false;
        $stock_data = $this->TrnStock->find('first',$search);
        if(count($stock_data) > 0 && is_array($stock_data)){
            //在庫レコードがあった場合
            $stock_data['TrnStock']['stock_count'] +=  1;
            $stock_data['TrnStock']['modifier']     =  $this->Session->read('Auth.MstUser.id');
            $stock_data['TrnStock']['modified']     =  $now;

        }else{
            //在庫レコードがなかった場合
            $stock_data = array(
                //在庫
                'TrnStock' => array(
                    'mst_item_unit_id'  => $this->request->data['MstSetItem']['item_unit_id'],
                    'mst_department_id' => $department_id,
                    'stock_count'       => 1,
                    'reserve_count'     => 0,
                    'promise_count'     => 0,
                    'requested_count'   => 0,
                    'receipted_count'   => 0,
                    'is_deleted'        => false,
                    'creater'           => $this->Session->read('Auth.MstUser.id'),
                    'modifier'          => $this->Session->read('Auth.MstUser.id'),
                    'created'           => $now,
                    'modified'          => $now,
                    )
                );
        }

        $this->TrnStock->create();
        // SQL実行
        if (!$this->TrnStock->save($stock_data ,  array('validates' => true,'atomic' => false))) {
            //在庫の更新失敗
            $this->TrnSticker->rollback();
            return false;
        }

        //コミット直前に更新チェックを行う
        $ret = $this->TrnSticker->query("select count(*) as count from trn_stickers as a where a.id in (" . $this->request->data['select']['stickers'] . ") and a.modified > '"  . $this->request->data['MstSetItem']['time'] . "' and a.modified <> '" . $now ."'" );
        if($ret[0][0]['count'] > 0 ){
            $this->TrnSticker->rollback();
            return false;
        }
        $this->TrnSticker->commit();
        return $setStockId;
    }

    /**
      * セット品履歴
      */
    function set_item_history(){
        $this->setRoleFunction(101); //セット品履歴

        // シール印字 hidden 用
        $this->request->data['MstSetItem']['mst_facility_id']   = $this->Session->read('Auth.facility_id_selected');
        $this->request->data['MstSetItem']['mst_facility_name'] = $this->getFacilityName($this->Session->read('Auth.facility_id_selected'));

        $result = array();
        if(isset($this->request->data['search']['is_search'])){
            $ret = $this->TrnSetStockHeader->query($this->_historySQL($this->request->data , false , true ));
            $this->set('max' , $ret[0][0]['count']);
            $result = $this->TrnSetStockHeader->query($this->_historySQL($this->request->data));
        } else {
            $this->request->data['MstSetItem']['work_date_from']      = date('Y/m/d',strtotime("-7 day"));
            $this->request->data['MstSetItem']['work_date_to']        = date('Y/m/d');
            $this->request->data['MstSetItem']['is_deleted'] = true;
        }

        $this->set('result' , $result);
        $this->set('data' , $this->request->data);
    }

    /**
      * セット品履歴明細
      */
    function set_item_edit(){
        $result = $this->TrnSetStockHeader->query($this->_historySQL($this->request->data , true));
        $this->set('result' , $result);
    }


    /**
      * セット品取消結果
      */
    function set_item_edit_result(){
        //取消処理メイン
        if(!$this->setItem_chancel_regist()){
            //取消失敗
            $this->Session->setFlash('取消が行えませんでした。システム管理者へ連絡してください。', 'growl', array('type'=>'error') );
            $where  =' and 1 = 0 ' ;
            $result = array();
        }else{
            $result = $this->TrnSetStockHeader->query($this->_historySQL($this->request->data , true));
        }
        $this->set('result' , $result);

    }

    function setItem_chancel_regist(){
        $now = date('Y/m/d H:i:s');
        // 選択したシールで関連情報を取得
        $HeaderData = $this->_getCancelData( ' and a.id in (' .join(',',$this->request->data['MstSetItem']['id']).')' );
        $PartData   = $this->_getCancelData( ' and a.id in (' .join(',',$this->request->data['MstSetItem']['id']).')' , true );

        //トランザクション開始
        $this->TrnSticker->begin();

        //行ロック
        $this->TrnSticker->query( 'select * from trn_stickers as a where a.id = ' . $HeaderData[0]['MstSetItem']['sticker_id'] . ' for update ' );

        //構成要素ループ
        foreach($PartData as $data ) {
            $this->TrnSticker->create();
            $this->TrnStock->create();
            $this->TrnSetStock->create();

            // 構成要素の在庫数を増やす
            $ret = $this->TrnStock->updateAll(
                array(
                    'TrnStock.stock_count'=> 'TrnStock.stock_count + 1' ,
                    'TrnStock.is_deleted' => 'false',
                    'TrnStock.modifier'   => $this->Session->read('Auth.MstUser.id'),
                    'TrnStock.modified'   => "'" . $now . "'"
                    ),
                array(
                    'TrnStock.id' => $data['MstSetItemPart']['stock_id']
                    ),
                -1
                );
            if(!$ret){
                $this->TrnSticker->rollback();
                return false;
            }

            // 構成要素のシールを有効にする。
            $ret = $this->TrnSticker->updateAll(
                array(
                    'TrnSticker.quantity'   => 1,
                    'TrnSticker.is_deleted' => 'false',
                    'TrnSticker.modifier'   => $this->Session->read('Auth.MstUser.id'),
                    'TrnSticker.modified'   => "'" . $now . "'"
                    )
                ,
                array(
                    'TrnSticker.id' => $data['MstSetItemPart']['sticker_id']
                    ),
                -1);

              if(!$ret){
                $this->TrnSticker->rollback();
                return false;
            }

            // セット品明細削除
            $ret = $this->TrnSetStock->updateAll(
                array(
                    'TrnSetStock.is_deleted' => 'true',
                    'TrnSetStock.modifier'   => $this->Session->read('Auth.MstUser.id'),
                    'TrnSetStock.modified'   => "'" . $now . "'",
                    ),
                array(
                    'TrnSetStock.id' => $data['MstSetItemPart']['set_stock_id']
                    ),
                -1
                );
          if(!$ret){
                $this->TrnSticker->rollback();
                return false;
            }
        }

        //ヘッダループ
        foreach($HeaderData as $data){
            $this->TrnSetStockHeader->create();
            $this->TrnSticker->create();
            $this->TrnStock->create();
            // セット品履歴を削除
            $ret = $this->TrnSetStockHeader->updateAll(
                array(
                    'TrnSetStockHeader.is_deleted' => 'true' ,
                    'TrnSetStockHeader.modifier'   => $this->Session->read('Auth.MstUser.id'),
                    'TrnSetStockHeader.modified'   => "'" . $now . "'"
                    ) ,
                array(
                    'TrnSetStockHeader.id' => $data['MstSetItem']['set_stock_header_id']
                    ),
                -1
                );
            if(!$ret){
                $this->TrnSticker->rollback();
                return false;
            }

            // セットのシールを削除
            $ret = $this->TrnSticker->updateAll(
                array(
                    'TrnSticker.quantity'            => 0,
                    'TrnSticker.is_deleted'          => 'true',
                    'TrnSticker.modifier'            => $this->Session->read('Auth.MstUser.id'),
                    'TrnSticker.modified'            => "'" . $now . "'"
                    ),
                array(
                    'TrnSticker.id'=>$data['MstSetItem']['sticker_id']
                    ),
                -1
                );

            if(!$ret){
                $this->TrnSticker->rollback();
                return false;
            }

            // セットの在庫を減らす
            $ret = $this->TrnStock->updateAll(
                array(
                    'TrnStock.stock_count'       => 'TrnStock.stock_count - 1 ',
                    'TrnStock.is_deleted'        => 'false',
                    'TrnStock.modifier'          => $this->Session->read('Auth.MstUser.id'),
                    'TrnStock.modified'          => "'" . $now . "'",
                    ),
                array(
                    'TrnStock.id'                => $data['MstSetItem']['stock_id'],
                    ),
                -1
                );
                if(!$ret){
                //在庫の更新失敗
                $this->TrnSticker->rollback();
                return false;
            }
        }

        //コミット前に更新チェック
        $ret = $this->TrnSticker->query(' select count(*) as count from trn_stickers as a where a.id = ' .$data['MstSetItem']['sticker_id'] . " and a.modified <> '" . $now . "'");
        if($ret[0][0]['count'] > 0){
            $this->TrnSticker->rollback();
            return false;
        }


        $this->TrnSticker->commit();
        return true;
    }

    private function _historySQL($data , $detail = false , $count = false){
        App::import('Sanitize');
        $data = Sanitize::clean($data);

        $where = '';
        //ヘッダーID
        if(isset($data['MstSetItem']['id']) && !empty($data['MstSetItem']['id']) && $detail){
            if(is_array($data['MstSetItem']['id'])){
                $where .= " and  a.id in (  " . join(' , ' , $data['MstSetItem']['id']) . ")";
            }else{
                $where .= " and  a.id = " . $data['MstSetItem']['id'];
            }
        }

        //作成番号
        if(isset($data['MstSetItem']['work_no']) && $data['MstSetItem']['work_no'] !== ""){
            $where .= " and  a.work_no =  '" . $data['MstSetItem']['work_no'] . "'";
        }
        //作成日From
        if(isset($data['MstSetItem']['work_date_from']) && $data['MstSetItem']['work_date_from'] !== ""){
            $where .= " and  a.work_date >=  '" . date('Y/m/d' , strtotime($data['MstSetItem']['work_date_from'])) . "'";
        }
        //作成日To
        if(isset($data['MstSetItem']['work_date_to']) && $data['MstSetItem']['work_date_to'] !== ""){
            $where .= " and  a.work_date <=  '" . date('Y/m/d' , strtotime($data['MstSetItem']['work_date_to'])) . "'";
        }
        //セットID
        if(isset($data['MstSetItem']['set_id']) && $data['MstSetItem']['set_id'] !== ""){
            $where .= " and  e.internal_code =  '" . $data['MstSetItem']['set_id'] . "'";
        }
        //商品ID
        if(isset($data['MstSetItem']['internal_code']) && $data['MstSetItem']['internal_code'] !== ""){
            $where .= " and  i.internal_code =  '" . $data['MstSetItem']['internal_code'] . "'";
        }
        //製品番号
        if(isset($data['MstSetItem']['item_code']) && $data['MstSetItem']['item_code'] !== ""){
            $where .= " and  e.item_code =  '" . $data['MstSetItem']['item_code'] . "'";
        }
        //セット名
        if(isset($data['MstSetItem']['set_name']) && $data['MstSetItem']['set_name'] !== ""){
            $where .= " and  e.item_name like  '%" . $data['MstSetItem']['set_name'] . "%'";
        }
        //商品名
        if(isset($data['MstSetItem']['item_name']) && $data['MstSetItem']['item_name'] !== ""){
            $where .= " and  i.item_name like  '%" . $data['MstSetItem']['item_name'] . "%'";
        }
        //販売元
        if(isset($data['MstSetItem']['dealer_name']) && $data['MstSetItem']['dealer_name'] !== ""){
            $where .= " and  j.dealer_name like  '%" . $data['MstSetItem']['dealer_name'] . "%'";
        }
        //規格
        if(isset($data['MstSetItem']['standard']) && $data['MstSetItem']['standard'] !== ""){
            $where .= " and  e.standard like  '%" . $data['MstSetItem']['standard'] . "%'";
        }
        //シール番号
        if(isset($data['MstSetItem']['facility_sticker_no']) && $data['MstSetItem']['facility_sticker_no'] !== ""){
            $where .= " and  c.facility_sticker_no =  '" . $data['MstSetItem']['facility_sticker_no'] . "'";
        }
            //取消は表示しない-------------------------------------------------------
        if((isset($this->request->data['MstSetItem']['is_deleted'])) and ($this->request->data['MstSetItem']['is_deleted'] == "1")){
            $where .= ' and a.is_deleted = FALSE ';
            $where .= ' and f.is_deleted = FALSE ';
        } else {
            $where .= ' and (a.is_deleted = FALSE  or a.is_deleted = TRUE) ';
            $where .= ' and (f.is_deleted = FALSE  or f.is_deleted = TRUE) ';
        }

        // 自センタの商品のみ
        $where .= " and e.mst_facility_id = " . $this->request->data['MstSetItem']['mst_facility_id'];

        $sql  = ' select ';
        if(!$detail){
            $sql .= '       a.id                       as "MstSetItem__id" ';
            $sql .= '     , a.work_no                  as "MstSetItem__work_no" ';
            $sql .= '     , e.internal_code            as "MstSetItem__set_id" ';
            $sql .= '     , e.item_name                as "MstSetItem__item_name" ';
            $sql .= '     , b.user_name                as "MstSetItem__user_name" ';
            $sql .= '     , c.facility_sticker_no      as "MstSetItem__sticker_no" ';
            $sql .= '     , a.created                  as "MstSetItem__created" ';
            $sql .= '     , a.recital                  as "MstSetItem__recital" ';
            $sql .= '     , count(a.id)                as "MstSetItem__count" ';
            $sql .= '     , a.detail_count             as "MstSetItem__detail_count"  ';
        }else{
            //ヘッダ要素
            $sql .= '       a.id                       as "MstSetItem__id" ';
            $sql .= '     , a.work_no                  as "MstSetItem__work_no" ';
            $sql .= '     , a.work_date                as "MstSetItem__work_date" ';
            $sql .= '     , c.facility_sticker_no      as "MstSetItem__sticker_no" ';
            $sql .= '     , e.internal_code            as "MstSetItem__internal_code" ';
            $sql .= '     , e.item_name                as "MstSetItem__item_name" ';
            $sql .= '     , e.standard                 as "MstSetItem__standard" ';
            $sql .= '     , a.detail_count             as "MstSetItem__detail_count"  ';

            //パーツ要素
            $sql .= '     , i.internal_code            as "MstSetItemPart__internal_code" ';
            $sql .= '     , i.item_name                as "MstSetItemPart__item_name" ';
            $sql .= '     , i.item_code                as "MstSetItemPart__item_code" ';
            $sql .= '     , i.standard                 as "MstSetItemPart__standard" ';
            $sql .= '     , j.dealer_name              as "MstSetItemPart__dealer_name" ';
            $sql .= '     , g.facility_sticker_no      as "MstSetItemPart__sticker_no" ';
            $sql .= '     , g.lot_no                   as "MstSetItemPart__lot_no" ';
            $sql .= "     , to_char(g.validated_date,'YYYY/mm/dd') ";
            $sql .= '                                  as "MstSetItemPart__validated_date" ';
            $sql .= ' ,(case when  h.per_unit = 1 then  k.unit_name ';
            $sql .= "  else k.unit_name || '(' || h.per_unit || l.unit_name || ')' ";
            $sql .= '  end )                           as "MstSetItemPart__unit_name" ';

            $sql .= ' ,(case ';
            $sql .= ' when  c.quantity     = 0    then  1 ';
            $sql .= ' when  c.has_reserved = true then  1 ';
            $sql .= ' when  c.is_deleted   = true then  1 ';
            $sql .= ' when  a.is_deleted   = true then  1 ';
            $sql .= "  else 0";
            $sql .= '  end )                           as "MstSetItem__status" ';

        }
        $sql .= '   from ';
        $sql .= '     trn_set_stock_headers as a  ';
        $sql .= '     left join mst_users as b  ';
        $sql .= '       on b.id = a.creater  ';
        $sql .= '     left join trn_stickers as c  ';
        $sql .= '       on c.trn_set_stock_id = a.id  ';
        $sql .= '     left join mst_item_units as d  ';
        $sql .= '       on c.mst_item_unit_id = d.id  ';
        $sql .= '     left join mst_facility_items as e  ';
        $sql .= '       on e.id = d.mst_facility_item_id  ';
        $sql .= '     left join trn_set_stocks as f  ';
        $sql .= '       on f.trn_set_stock_header_id = a.id  ';
        $sql .= '     left join trn_stickers as g  ';
        $sql .= '       on g.id = f.trn_sticker_id  ';
        $sql .= '     left join mst_item_units as h  ';
        $sql .= '       on h.id = g.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as i  ';
        $sql .= '       on i.id = h.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as j  ';
        $sql .= '       on j.id = i.mst_dealer_id  ';
        $sql .= '     left join mst_unit_names as k  ';
        $sql .= '       on k.id = h.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as l  ';
        $sql .= '       on l.id = h.per_unit_name_id  ';
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .=$where;
        if(!$detail){
            $sql .= '   group by ';
            $sql .= '     a.id ';
            $sql .= '     , a.work_no ';
            $sql .= '     , e.internal_code ';
            $sql .= '     , e.item_name ';
            $sql .= '     , b.user_name ';
            $sql .= '     , a.created ';
            $sql .= '     , a.recital ';
            $sql .= '     , a.detail_count  ';
            $sql .= '     , c.facility_sticker_no  ';

        }
        $sql .= '   order by ';
        $sql .= '     a.work_no desc , e.internal_code , c.facility_sticker_no';

        if($count){
            return 'select count(*) as count from ( ' . $sql . ' ) as x';
        }

        if(!$detail){
            $sql .=' limit ' . $data['limit'];
        }else{
            $sql .=' , i.internal_code , g.facility_sticker_no';
        }

        return $sql;
    }


    // セット品ヘッダ情報を取得
    private function _getHeaderData($where = null , $limit = null ){
        if(is_null($where)){
            return array();
        }

        $sql  = 'select ';
        $sql .= 'a.id            as "MstSetItem__id", ';
        $sql .= 'a.internal_code as "MstSetItem__internal_code", ';
        $sql .= 'a.item_name     as "MstSetItem__item_name", ';
        $sql .= 'a.standard      as "MstSetItem__standard", ';
        $sql .= 'a.item_code     as "MstSetItem__item_code", ';
        $sql .= 'b.id            as "MstSetItem__item_unit_id" ';
        $sql .= ' from  ';
        $sql .= ' mst_facility_items as a ';
        $sql .= ' left join ';
        $sql .= ' mst_item_units as b';
        $sql .= ' on a.id = b.mst_facility_item_id ';
        $sql .= ' where  ';
        $sql .= ' a.item_type = ' . Configure::read('Items.item_types.setitem');
        $sql .= $where;
        $sql .= ' order by a.item_name, a.internal_code, a.id ';

        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'MstFacilityItem'));
            $sql .= ' limit ' . $limit;
        }
        return $this->MstFacilityItem->query($sql);
    }

    //商品情報を取得
    private function _getData($where = '' , $limit = null){
        $sql  = 'select ';
        $sql .= '      a.id                                       as "MstSetItem__id" ';
        $sql .= '    , b.internal_code                            as "MstSetItem__internal_code" ';
        $sql .= '    , b.item_name                                as "MstSetItem__item_name"';
        $sql .= '    , b.item_code                                as "MstSetItem__item_code"';
        $sql .= '    , a.lot_no                                   as "MstSetItem__lot_no"';
        $sql .= '    , a.facility_sticker_no                      as "MstSetItem__facility_sticker_no"';
        $sql .= '    , b.standard                                 as "MstSetItem__standard"';
        $sql .= '    , d.dealer_name                              as "MstSetItem__dealer_name"';
        $sql .= '    , coalesce(a.transaction_price , 0)          as "MstSetItem__transaction_price"';
        $sql .= '    , coalesce(a.original_price , 0)             as "MstSetItem__original_price"';
        $sql .= '    , coalesce(a.sales_price ,0)                 as "MstSetItem__sales_price"';
        $sql .= '    , to_char(a.validated_date , \'YYYY/mm/dd\') as "MstSetItem__validated_date"';
        $sql .= '    , c.id                                       as "MstSetItem__item_unit_id"';
        $sql .= '    , (case when c.per_unit = 1 then e.unit_name   ';
        $sql .= "       else e.unit_name || '('  || c.per_unit || f.unit_name || ')'";
        $sql .= '      end )                                      as "MstSetItem__unit_name"';
        $sql .= '    , a.quantity                                 as "MstSetItem__quantity"';
        $sql .= '    , h.stock_count                              as "MstSetItem__stock_count"';
        $sql .= '    , h.id                                       as "MstSetItem__stock_id"';
        $sql .= '    , a.mst_department_id                        as "MstSetItem__department_id"';
        $sql .= "    , ( case when a.quantity = 0 then '残数なし' when a.is_deleted = true then '取消済み' when a.has_reserved then '予約済み' end ) " ;
        $sql .= '                                                 as "MstSetItem__status"';
        $sql .= '  from ';
        $sql .= '    trn_stickers as a  ';
        $sql .= '    left join mst_item_units as c  ';
        $sql .= '      on c.id = a.mst_item_unit_id  ';
        $sql .= '    left join mst_facility_items as b  ';
        $sql .= '      on b.id = c.mst_facility_item_id  ';
        $sql .= '      and b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '      and b.item_type = ' . Configure::read('Items.item_types.normalitem');
        $sql .= '    left join mst_dealers as d  ';
        $sql .= '      on d.id = b.mst_dealer_id  ';
        $sql .= '    left join mst_unit_names as e  ';
        $sql .= '      on e.id = c.mst_unit_name_id  ';
        $sql .= '    left join mst_unit_names as f  ';
        $sql .= '      on f.id = c.per_unit_name_id  ';
        $sql .= '    left join mst_departments as g  ';
        $sql .= '      on g.id = a.mst_department_id  ';
        $sql .= '      and g.mst_facility_id = '. $this->Session->read('Auth.facility_id_selected');
        $sql .= '    left join trn_stocks as h  ';
        $sql .= '      on h.mst_department_id = a.mst_department_id ';
        $sql .= '      and h.mst_item_unit_id = a.mst_item_unit_id ';
        $sql .= '    left join trn_set_stocks as i  ';
        $sql .= '      on i.trn_sticker_id = a.id ';

        $sql .= ' where 1 = 1 ' ;
        $sql .= $where ;
        $sql . ' order by a.facility_sticker_no';
        if(!is_null($limit)){
            $this->set('max' , $this->getMaxCount($sql , 'TrnSticker'));
            $sql .= ' limit ' . $limit;
        }

        return $this->TrnSticker->query($sql);

    }

    /* 帳票印刷 */
    function report(){
        $data['MstSetItem']['id']              = $this->request->data['MstSetItem']['id'];
        $data['MstSetItem']['mst_facility_id'] = $this->request->data['MstSetItem']['mst_facility_id'];
        $result = $this->TrnSetStockHeader->query($this->_historySQL($data , true));
        $output = array();
        $i = 1;
        $preStickerNo = '';
        foreach($result as $data){
            if($data['MstSetItem']['sticker_no'] !== $preStickerNo){
                //ヘッダのシール番号が違ったらシーケンス番号を振りなおす。
                $i = 1;
            }
            $output[] = array(
                $data['MstSetItem']['sticker_no'],         //セットコード
                $data['MstSetItem']['item_name'],          //セット名
                $i,                                        //行番号
                $data['MstSetItemPart']['internal_code'],  //商品ID
                $data['MstSetItemPart']['item_name'],      //商品名
                $data['MstSetItemPart']['item_code'],      //製品番号
                $data['MstSetItemPart']['unit_name'],      //包装単位
                $data['MstSetItemPart']['lot_no'],         //ロット番号
                $data['MstSetItemPart']['standard'],       //規格
                $data['MstSetItemPart']['dealer_name'],    //販売元
                $data['MstSetItemPart']['sticker_no'],     //センターシール
                $data['MstSetItemPart']['validated_date'], //有効期限
                date('Y/m/d')                              //印刷年月日
                );
            $preStickerNo = $data['MstSetItem']['sticker_no'];
            $i++;
        }

        $columns = array("セットコード",
                         "セット名",
                         "行番号",
                         "商品ID",
                         "商品名",
                         "製品番号",
                         "包装単位",
                         "ロット番号",
                         "規格",
                         "販売元",
                         "センターシール",
                         "有効期限",
                         "印刷年月日"
                         );
        $layout_name = Configure::read('layoutnameModels');
        $layout_file = "xml_output";
        $search_facter = "XXX";
        $fix_value = array('タイトル'=>'セット品明細票','検索条件'=>$search_facter);
        $this->xml_output($output,join("\t",$columns),$layout_name[30],$fix_value,$layout_file);
    }

    /**
     * xml出力テスト用
     * @param
     * @todo 各コントローラの共通関数化
     */
    function xml_output($data,$columns,$layout_name,$fix_value,$layout_file){
        $this->layout = 'xml/default';
        $this->set('columns',$columns);
        $this->set('posted',$data);
        $this->set('layout_name',$layout_name);
        $this->set('fix_value',$fix_value); //連想配列のキーにname属性を指定
        $this->render($layout_file);
    }

    /**
      * シール印字
      */
    function seal(){
        $this->request->data['MstSetItem']['sticker_no'] = (is_array($this->request->data['MstSetItem']['sticker_no'])? array_filter( $this->request->data['MstSetItem']['sticker_no'] ):$this->request->data['MstSetItem']['sticker_no']);

        $stickers = $this->Stickers->getFacilityStickers($this->request->data['MstSetItem']['sticker_no'],
                                                         $this->request->data['MstSetItem']['mst_facility_name'],
                                                         $this->request->data['MstSetItem']['mst_facility_id']
                                                         );

        $this->layout = 'xml/default';
        $this->set('stickers', $stickers);
        $this->render('/storages/seal');
    }


    private function _getCancelData($where , $detail= false){
        $sql ='';

        if(!$detail){
            $sql .=' select ';
            $sql .='       a.id                 as "MstSetItem__set_stock_header_id" ';
            $sql .='     , b.id                 as "MstSetItem__sticker_id" ';
            $sql .='     , b.quantity           as "MstSetItem__sticker_quantiry" ';
            $sql .='     , c.id                 as "MstSetItem__stock_id" ';
            $sql .='     , c.stock_count        as "MstSetItem__stock_count"  ';
            $sql .='   from ';
            $sql .='     trn_set_stock_headers as a  ';
            $sql .='     left join trn_stickers as b  ';
            $sql .='       on a.id = b.trn_set_stock_id  ';
            $sql .='     left join trn_stocks as c  ';
            $sql .='       on c.mst_item_unit_id = b.mst_item_unit_id  ';
            $sql .='       and b.mst_department_id = c.mst_department_id  ';
            $sql .='   where 1 = 1 ';
            $sql .=$where;
        }else{
            $sql .=' select ';
            $sql .='       b.id                   as "MstSetItemPart__set_stock_id" ';
            $sql .='     , c.id                   as "MstSetItemPart__sticker_id" ';
            $sql .='     , d.id                   as "MstSetItemPart__stock_id" ';
            $sql .='     , d.stock_count          as "MstSetItemPart__stock_count"  ';
            $sql .='   from ';
            $sql .='     trn_set_stock_headers as a  ';
            $sql .='     left join trn_set_stocks as b  ';
            $sql .='       on a.id = b.trn_set_stock_header_id  ';
            $sql .='     left join trn_stickers as c  ';
            $sql .='       on c.id = b.trn_sticker_id  ';
            $sql .='     left join trn_stocks as d  ';
            $sql .='       on d.mst_item_unit_id = c.mst_item_unit_id  ';
            $sql .='       and d.mst_department_id = c.mst_department_id  ';
            $sql .='   where 1 = 1 ';
            $sql .=$where;

        }

        return $this->TrnSetStockHeader->query($sql);

    }

    private function _getSetDetails(){
        //ヘッダに紐付く構成要素情報を取得
        $sql  =' select ';
        $sql .='       a.mst_item_unit_id  as "MstSetDetails__id"';
        $sql .='     , c.internal_code     as "MstSetDetails__internal_code"';
        $sql .='     , c.item_name         as "MstSetDetails__item_name"';
        $sql .='     , c.standard          as "MstSetDetails__standard"';
        $sql .='     , c.item_code         as "MstSetDetails__item_code"';
        $sql .='     , f.dealer_name       as "MstSetDetails__dealer_name"';
        $sql .='     , a.quantity          as "MstSetDetails__quantity"';
        $sql .='     , (  ';
        $sql .='       case  ';
        $sql .='         when b.per_unit = 1  ';
        $sql .='         then d.unit_name  ';
        $sql .="         else d.unit_name || '(' || b.per_unit || e.unit_name || ')'  ";
        $sql .='         end ';
        $sql .='     )                            as "MstSetDetails__unit_name" ';
        $sql .='   from ';
        $sql .='     mst_set_details as a  ';
        $sql .='     left join mst_item_units as b  ';
        $sql .='       on b.id = a.mst_item_unit_id  ';
        $sql .='     left join mst_facility_items as c  ';
        $sql .='       on c.id = b.mst_facility_item_id  ';
        $sql .='     left join mst_unit_names as d  ';
        $sql .='       on d.id = b.mst_unit_name_id  ';
        $sql .='     left join mst_unit_names as e  ';
        $sql .='       on e.id = b.per_unit_name_id  ';
        $sql .='     left join mst_dealers as f  ';
        $sql .='       on f.id = c.mst_dealer_id  ';
        $sql .='   where ';
        $sql .='     a.set_item_unit_id = ' . $this->request->data['MstSetItem']['id'];

        return $this->TrnSticker->query($sql);

    }

    /**
     * 川鉄シールレコードを取得
     **/
    private function kawatetu($barcode){
        $sql  = ' select ';
        $sql .= '       a.id                                        as "MstSetItem__id" ';
        $sql .= '     , b.id                                        as "MstSetItem__item_unit_id" ';
        $sql .= '     , c.internal_code                             as "MstSetItem__internal_code"';
        $sql .= '     , c.item_name                                 as "MstSetItem__item_name"';
        $sql .= '     , c.item_code                                 as "MstSetItem__item_code"';
        $sql .= '     , c.standard                                  as "MstSetItem__standard"';
        $sql .= '     , d.dealer_name                               as "MstSetItem__dealer_name"';
        $sql .= '     , a.quantity                                  as "MstSetItem__quantity" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then e.unit_name  ';
        $sql .= "         else e.unit_name || '(' || b.per_unit || f.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                             as "MstSetItem__unit_name" ';
        $sql .= '     , coalesce(a.transaction_price , 0)           as "MstSetItem__transaction_price"';
        $sql .= '     , a.facility_sticker_no                       as "MstSetItem__facility_sticker_no"';
        $sql .= '     , a.lot_no                                    as "MstSetItem__lot_no"';
        $sql .= '     , to_char(a.validated_date , \'YYYY/mm/dd\')  as "MstSetItem__validated_date"';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when a.quantity = 0  ';
        $sql .= "         then '数量なし'  ";
        $sql .= '         end ';
        $sql .= '     )                                             as "MstSetItem__status"  ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as d  ';
        $sql .= '       on d.id = c.mst_dealer_id  ';
        $sql .= '     left join mst_unit_names as e  ';
        $sql .= '       on e.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as f  ';
        $sql .= '       on f.id = b.per_unit_name_id  ';
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .= '     and c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');

        if($barcode['quantity'] == null ){
            $sql .= '     and b.per_unit = 1  ';
        }else{
            $sql .= '     and b.per_unit = ' . $barcode['quantity'];
        }

        if($barcode['lot_no'] != ''){
            $sql .= "     and a.lot_no = '" .$barcode['lot_no']. "'";
        }else{
            $sql .= "     and ( a.lot_no = '' or a.lot_no is null ) ";
        }

        if($barcode['validated_date'] != '' ){
            $sql .= "     and a.validated_date = '" . $this->getFormatedValidateDate($barcode['validated_date']) . "'";
        }else{
            $sql .= "     and a.validated_date is null ";
        }

        $sql .= "     and c.jan_code like '" . $barcode['pure_jan_code'] ."%'";
        $sql .= "     and a.facility_sticker_no = ''  ";
        $sql .= "     and a.hospital_sticker_no = ''  ";

        return $this->TrnSticker->query($sql);
    }

    /**
     * セット組履歴CSV出力
     */
    function export_csv() {
        $where = '';
        $data = $this->request->data;

        //作成番号
        if(isset($data['MstSetItem']['work_no']) && $data['MstSetItem']['work_no'] !== ""){
            $where .= " and  a.work_no =  '" . $data['MstSetItem']['work_no'] . "'";
        }
        //作成日From
        if(isset($data['MstSetItem']['work_date_from']) && $data['MstSetItem']['work_date_from'] !== ""){
            $where .= " and  a.work_date >=  '" . date('Y/m/d' , strtotime($data['MstSetItem']['work_date_from'])) . "'";
        }
        //作成日To
        if(isset($data['MstSetItem']['work_date_to']) && $data['MstSetItem']['work_date_to'] !== ""){
            $where .= " and  a.work_date <=  '" . date('Y/m/d' , strtotime($data['MstSetItem']['work_date_to'])) . "'";
        }
        //セットID
        if(isset($data['MstSetItem']['set_id']) && $data['MstSetItem']['set_id'] !== ""){
            $where .= " and  e.internal_code =  '" . $data['MstSetItem']['set_id'] . "'";
        }
        //商品ID
        if(isset($data['MstSetItem']['internal_code']) && $data['MstSetItem']['internal_code'] !== ""){
            $where .= " and  i.internal_code =  '" . $data['MstSetItem']['internal_code'] . "'";
        }
        //製品番号
        if(isset($data['MstSetItem']['item_code']) && $data['MstSetItem']['item_code'] !== ""){
            $where .= " and  e.item_code =  '" . $data['MstSetItem']['item_code'] . "'";
        }
        //セット名
        if(isset($data['MstSetItem']['set_name']) && $data['MstSetItem']['set_name'] !== ""){
            $where .= " and  e.item_name like  '%" . $data['MstSetItem']['set_name'] . "%'";
        }
        //商品名
        if(isset($data['MstSetItem']['item_name']) && $data['MstSetItem']['item_name'] !== ""){
            $where .= " and  i.item_name like  '%" . $data['MstSetItem']['item_name'] . "%'";
        }
        //販売元
        if(isset($data['MstSetItem']['dealer_name']) && $data['MstSetItem']['dealer_name'] !== ""){
            $where .= " and  j.dealer_name like  '%" . $data['MstSetItem']['dealer_name'] . "%'";
        }
        //規格
        if(isset($data['MstSetItem']['standard']) && $data['MstSetItem']['standard'] !== ""){
            $where .= " and  e.standard like  '" . $data['MstSetItem']['standard'] . "'";
        }
        //シール番号
        if(isset($data['MstSetItem']['facility_sticker_no']) && $data['MstSetItem']['facility_sticker_no'] !== ""){
            $where .= " and  c.facility_sticker_no =  '" . $data['MstSetItem']['facility_sticker_no'] . "'";
        }
        //取消は表示しない-------------------------------------------------------
        if((isset($this->request->data['MstSetItem']['is_deleted'])) && ($this->request->data['MstSetItem']['is_deleted'] == "1")){
            $where .= ' and a.is_deleted = FALSE ';
            $where .= ' and f.is_deleted = FALSE ';
        } else {
            $where .= ' and (a.is_deleted = FALSE  or a.is_deleted = TRUE) ';
            $where .= ' and (f.is_deleted = FALSE  or f.is_deleted = TRUE) ';
        }

        // 自センタの商品のみ
        $where .= " and e.mst_facility_id = " . $this->request->data['MstSetItem']['mst_facility_id'];

        $sql  = ' select ';
        //ヘッダ要素
        $sql .= '       a.work_no                            as 作成番号 ';
        $sql .= "     , to_char(a.work_date,'YYYY/MM/DD')    as 作業日 ";
        $sql .= '     , c.facility_sticker_no                as シール番号 ';
        $sql .= '     , e.internal_code                      as "セットID" ';
        $sql .= '     , e.item_name                          as セット名 ';
        $sql .= '     , e.standard                           as セット規格 ';
        //パーツ要素
        $sql .= '     , i.internal_code                      as "商品ID" ';
        $sql .= '     , i.item_name                          as 商品名 ';
        $sql .= '     , i.standard                           as 規格 ';
        $sql .= '     , i.item_code                          as 製品番号 ';
        $sql .= '     , j.dealer_name                        as 販売元名 ';
        $sql .= '     ,(case when  h.per_unit = 1 then  k.unit_name ';
        $sql .= "           else k.unit_name || '(' || h.per_unit || l.unit_name || ')' ";
        $sql .= '       end )                                as 包装単位名 ';
        $sql .= '     , g.facility_sticker_no                as センターシール ';

        $sql .= '   from ';
        $sql .= '     trn_set_stock_headers as a  ';
        $sql .= '     left join mst_users as b  ';
        $sql .= '       on b.id = a.creater  ';
        $sql .= '     left join trn_stickers as c  ';
        $sql .= '       on c.trn_set_stock_id = a.id  ';
        $sql .= '     left join mst_item_units as d  ';
        $sql .= '       on c.mst_item_unit_id = d.id  ';
        $sql .= '     left join mst_facility_items as e  ';
        $sql .= '       on e.id = d.mst_facility_item_id  ';
        $sql .= '     left join trn_set_stocks as f  ';
        $sql .= '       on f.trn_set_stock_header_id = a.id  ';
        $sql .= '     left join trn_stickers as g  ';
        $sql .= '       on g.id = f.trn_sticker_id  ';
        $sql .= '     left join mst_item_units as h  ';
        $sql .= '       on h.id = g.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as i  ';
        $sql .= '       on i.id = h.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as j  ';
        $sql .= '       on j.id = i.mst_dealer_id  ';
        $sql .= '     left join mst_unit_names as k  ';
        $sql .= '       on k.id = h.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as l  ';
        $sql .= '       on l.id = h.per_unit_name_id  ';
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .=$where;
        $sql .= '   order by ';
        $sql .= '     a.work_no desc , e.internal_code , c.facility_sticker_no';

        $sql .=' , i.internal_code , g.facility_sticker_no';

        $this->db_export_csv($sql , "セット組作業履歴", 'set_item_history');
    }
}
?>
