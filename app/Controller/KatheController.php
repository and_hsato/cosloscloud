<?php
/**
 * KatheController
 *　カテ版
 * @version 1.0.0
 */
class KatheController extends AppController {

    /**
     * @var $name
     */
    var $name = 'Kathe';

    /**
     * @var array $uses
     */
    var $uses = array(
        'MstFacilityItem',
        'MstItemUnit',
        'MstKatheAnesthesia',
        'MstKatheCheckup',
        'MstKatheDepartment',
        'MstKatheDisease',
        'MstKatheMethod',
        'MstKathePerson',
        'MstKathePosition',
        'MstKatheRoom',
        'MstKatheWard',
        'TrnKatheCheckup',
        'TrnKatheDisease',
        'TrnKatheHeader',
        'TrnKatheItem',
        'TrnKatheMethod',
        );
    
    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'Common');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Common','Facilityitem','utils','Stickers','CsvWriteUtils','CsvReadUtils');

    /**
     *
     * @var array $errors
     */
    var $errors;

    /**
     *
     * @var array $successes
     */
    var $successes;


    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        $this->Auth->allowedActions[] = 'seal';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        //CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }
    }
    
    /**
     * 検索画面
     */
    function index (){
        $this->setRoleFunction(120); //カテ実施入力
        // プルダウン作成
        $this->getPullDown();
        $result = array();
        if(isset($this->request->data['search']['is_search'])){
            $sql  = ' select ';
            $sql .= '       a.id                     as "TrnKatheHeader__id" ';
            $sql .= "     , to_char(a.work_date, 'YYYY/MM/DD') ";
            $sql .= '                                as "TrnKatheHeader__work_date" ';
            $sql .= '     , a.work_no                as "TrnKatheHeader__work_no" ';
            $sql .= '     , b.kathe_department_name  as "TrnKatheHeader__department_name" ';
            $sql .= '     , c.kathe_ward_name        as "TrnKatheHeader__ward_name" ';
            $sql .= '     , a.name                   as "TrnKatheHeader__name" ';
            $sql .= '     , d.kathe_person_name      as "TrnKatheHeader__person_name" ';
            $sql .= "     , to_char(a.created, 'YYYY/mm/dd hh24:mi:ss') ";
            $sql .= '                                as "TrnKatheHeader__created" ';
            $sql .= '     , e.user_name              as "TrnKatheHeader__user_name"  ';
            $sql .= '   from ';
            $sql .= '     trn_kathe_headers as a  ';
            $sql .= '     left join mst_kathe_departments as b  ';
            $sql .= '       on b.id = a.mst_kathe_department_id  ';
            $sql .= '     left join mst_kathe_wards as c  ';
            $sql .= '       on c.id = a.mst_kathe_ward_id  ';
            $sql .= '     left join mst_kathe_people as d  ';
            $sql .= '       on d.id = a.mst_kathe_person_id  ';
            $sql .= '     left join mst_users as e  ';
            $sql .= '       on e.id = a.creater  ';
            $sql .= '   where ';
            $sql .= '     a.is_deleted = false  ';
            if(isset($this->request->data['TrnKatheHeader']['search_code']) && $this->request->data['TrnKatheHeader']['search_code'] != ''){
                $sql .= " and a.work_no = '" . $this->request->data['TrnKatheHeader']['search_code'] . "'";
            }
            if(isset($this->request->data['TrnKatheHeader']['search_date']) && $this->request->data['TrnKatheHeader']['search_date'] != ''){
                $sql .= " and a.work_date = '" . $this->request->data['TrnKatheHeader']['search_date'] . "'";
            }
            if(isset($this->request->data['TrnKatheHeader']['search_department']) && $this->request->data['TrnKatheHeader']['search_department'] != ''){
                $sql .= " and a.mst_kathe_department_id = '" . $this->request->data['TrnKatheHeader']['search_department'] . "'";
            }
            if(isset($this->request->data['TrnKatheHeader']['search_ward']) && $this->request->data['TrnKatheHeader']['search_ward'] != ''){
                $sql .= " and a.mst_kathe_ward_id = '" . $this->request->data['TrnKatheHeader']['search_ward'] . "'";
            }
            $sql .= ' and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            
            $sql .= '   order by ';
            $sql .= '     a.work_date , a.work_no';
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnKatheHeader'));

            $sql .= " LIMIT ".$this->request->data['limit'];

            $result = $this->TrnKatheHeader->query($sql , false);
        }
        
        $this->set('result' , $result);
    }

    /**
     * 登録画面
     */
    function edit($id = 0){
        // プルダウン作成
        $this->getPullDown();

        $methods = array();// 術式
        $checkup = array();// 検査
        $disease = array();// 病名
        $items   = array();// 材料
        
        if($id == 0 ){
            // 新規
            
        } else {
            // 編集
            // データを取得
            // ヘッダ
            $header = $this->getKatheHeaderData($id);
            $this->request->data['TrnKathe'] = $header[0][0];
            // 術式
            $methods = $this->getKatheMethodData($id);
            // 材料
            $items = $this->getKatheItemData($id);
            // 病名
            $disease = $this->getKatheDiseaseData($id);
            // 検査
            $checkup = $this->getKatheCheckupData($id);
        }

        $this->set('methods' , $methods); //術式
        $this->set('checkup' , $checkup); //検査
        $this->set('disease' , $disease); //病名
        $this->set('items'   , $items);   //材料

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnKathe.token' , $token);
        $this->request->data['TrnKathe']['token'] = $token;
    }

    /**
     * 登録結果画面
     */
    function result(){
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if(isset($this->request->data['TrnKathe']['token']) &&
           $this->request->data['TrnKathe']['token'] === $this->Session->read('TrnKathe.token')) {
            $this->Session->delete('TrnKathe.token');
            $now = date('Y/m/d H:i:s.u');
            
            if(!empty($this->request->data['TrnKathe']['id'])){
                // 更新のときは行ロックする。
                $this->TrnKatheHeader->query(' select * from trn_kathe_headers as a where a.id = ' . $this->request->data['TrnKathe']['id'] );
                $trn_kathe_header_id = $this->request->data['TrnKathe']['id'];
            }else{
                $trn_kathe_header_id = null;
            }
            
            $this->TrnKatheHeader->begin();
            /*
            if(empty($this->request->data['TrnKathe']['birth'])){
                $birth = null;
            }else{
                $birth = $this->request->data['TrnKathe']['birth'];
            }
            
            if(empty($this->request->data['TrnKathe']['age'])){
                $age = null;
            }else{
                $age = $this->request->data['TrnKathe']['age'];
            }
            */
            // カテヘッダ作成
            if(empty($trn_kathe_header_id)){
                // 新規の場合
                // 作業番号を取得
                $ret = $this->TrnKatheHeader->query(" select lpad((max(a.id) + 1) ::varchar, 7, '0') as work_no from trn_kathe_headers as a ");
                $work_no = $ret[0][0]['work_no'];
                if(empty($work_no)){
                    $work_no = '0000001';
                }
                
                $TrnKatheHeader['TrnKatheHeader']['work_no']                 = $work_no;
                $TrnKatheHeader['TrnKatheHeader']['is_deleted']              = false;
                $TrnKatheHeader['TrnKatheHeader']['creater']                 = $this->Session->read('Auth.MstUser.id');
                $TrnKatheHeader['TrnKatheHeader']['created']                 = $now;
            }else{
                // 更新の場合
                $TrnKatheHeader['TrnKatheHeader']['id']                      = $trn_kathe_header_id;
            }
            $TrnKatheHeader['TrnKatheHeader']['work_date']               = $this->request->data['TrnKathe']['work_date'];
            $TrnKatheHeader['TrnKatheHeader']['kathe_type']              = $this->request->data['TrnKathe']['type']; 
            $TrnKatheHeader['TrnKatheHeader']['mst_kathe_room_id']       = $this->request->data['TrnKathe']['room'];
            $TrnKatheHeader['TrnKatheHeader']['mst_kathe_department_id'] = $this->request->data['TrnKathe']['departmentCode'];
            $TrnKatheHeader['TrnKatheHeader']['mst_kathe_ward_id']       = $this->request->data['TrnKathe']['wardCode'];
            //$TrnKatheHeader['TrnKatheHeader']['mst_kathe_person_id']     = $this->request->data['TrnKathe']['person'];
            $TrnKatheHeader['TrnKatheHeader']['mst_kathe_anesthesia_id'] = $this->request->data['TrnKathe']['anesthesia'];
            $TrnKatheHeader['TrnKatheHeader']['mst_kathe_position_id']   = $this->request->data['TrnKathe']['position'];
            //$TrnKatheHeader['TrnKatheHeader']['number']                  = $this->request->data['TrnKathe']['number'];
            //$TrnKatheHeader['TrnKatheHeader']['name']                    = $this->request->data['TrnKathe']['name'];
            //$TrnKatheHeader['TrnKatheHeader']['birth']                   = $birth;
            //$TrnKatheHeader['TrnKatheHeader']['age']                     = $age;
            //$TrnKatheHeader['TrnKatheHeader']['sex']                     = $this->request->data['TrnKathe']['sex'];
            //$TrnKatheHeader['TrnKatheHeader']['blood_type']              = $this->request->data['TrnKathe']['blood'];
            $TrnKatheHeader['TrnKatheHeader']['in_room_time']            = $this->request->data['TrnKathe']['inroomtime'];
            $TrnKatheHeader['TrnKatheHeader']['out_room_time']           = $this->request->data['TrnKathe']['outroomtime'];
            $TrnKatheHeader['TrnKatheHeader']['modifier']                = $this->Session->read('Auth.MstUser.id');
            $TrnKatheHeader['TrnKatheHeader']['modified']                = $now;
            $TrnKatheHeader['TrnKatheHeader']['mst_facility_id']         = $this->Session->read('Auth.facility_id_selected');

            $this->TrnKatheHeader->create();
            if(!$this->TrnKatheHeader->save($TrnKatheHeader)){
                $this->TrnKatheHeader->rollback();
                $this->Session->setFlash('カテ情報登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('index');
            }
            if(empty($this->request->data['TrnKathe']['id'])){
                $trn_kathe_header_id = $this->TrnKatheHeader->getLastInsertID();
            }else{
                $trn_kathe_header_id = $this->request->data['TrnKathe']['id'];
            }
            
            // 検査
            // いったん削除フラグを立てる。
            $res = $this->TrnKatheCheckup->updateAll(array(
                'TrnKatheCheckup.is_deleted' => 'true',
                'TrnKatheCheckup.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnKatheCheckup.modified'   => "'". $now ."'",
            ), array(
                'TrnKatheCheckup.trn_kathe_header_id' => $trn_kathe_header_id,
            ), -1);

            if(false === $res){
                $this->TrnKatheHeader->rollback();
                $this->Session->setFlash('カテ情報検査登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('index');
            }
            if(isset($this->request->data['TrnKathe']['checkup']) && is_array($this->request->data['TrnKathe']['checkup'])){
                $seq = 1;
                foreach($this->request->data['TrnKathe']['checkup'] as $line => $id){
                    $TrnKatheCheckup = array();
                    // 新規
                    $TrnKatheCheckup = array(
                        'TrnKatheCheckup' => array(
                            'trn_kathe_header_id'  => $trn_kathe_header_id,
                            'work_seq'             => $seq,
                            'mst_kathe_checkup_id' => $id,
                            'is_deleted'           => false,
                            'creater'              => $this->Session->read('Auth.MstUser.id'),
                            'created'              => $now,
                            'modifier'             => $this->Session->read('Auth.MstUser.id'),
                            'modified'             => $now,
                            )
                        );
                    $seq++;
                    $this->TrnKatheCheckup->create();
                    if(!$this->TrnKatheCheckup->save($TrnKatheCheckup)){
                        $this->TrnKatheHeader->rollback();
                        $this->Session->setFlash('カテ情報検査登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('index');
                    }
                }
            }

            
            // 病名
            // いったん削除フラグを立てる。
            $res = $this->TrnKatheDisease->updateAll(array(
                'TrnKatheDisease.is_deleted' => 'true',
                'TrnKatheDisease.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnKatheDisease.modified'   => "'". $now ."'",
            ), array(
                'TrnKatheDisease.trn_kathe_header_id' => $trn_kathe_header_id,
            ), -1);

            if(false === $res){
                $this->TrnKatheHeader->rollback();
                $this->Session->setFlash('カテ情報病名登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('index');
            }

            if(isset($this->request->data['TrnKathe']['disease']) && is_array($this->request->data['TrnKathe']['disease'])){
                $seq = 1;
                foreach($this->request->data['TrnKathe']['disease'] as $line => $id){
                    $TrnKatheDisease = array();
                    // 新規
                    $TrnKatheDisease = array(
                        'TrnKatheDisease' => array(
                            'trn_kathe_header_id'  => $trn_kathe_header_id,
                            'work_seq'             => $seq,
                            'mst_kathe_disease_id' => $id,
                            'is_deleted'           => false,
                            'creater'              => $this->Session->read('Auth.MstUser.id'),
                            'created'              => $now,
                            'modifier'             => $this->Session->read('Auth.MstUser.id'),
                            'modified'             => $now,
                            )
                        );
                    $seq++;
                    $this->TrnKatheDisease->create();
                    if(!$this->TrnKatheDisease->save($TrnKatheDisease)){
                        $this->TrnKatheHeader->rollback();
                        $this->Session->setFlash('カテ情報病名登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('index');
                    }
                }
            }

            
            
            // 材料
            // いったん削除フラグを立てる。
            $res = $this->TrnKatheItem->updateAll(array(
                'TrnKatheItem.is_deleted' => 'true',
                'TrnKatheItem.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnKatheItem.modified'   => "'". $now ."'",
            ), array(
                'TrnKatheItem.trn_kathe_header_id' => $trn_kathe_header_id,
            ), -1);

            if(false === $res){
                $this->TrnKatheHeader->rollback();
                $this->Session->setFlash('カテ情報材料登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('index');
            }
            if(isset($this->request->data['TrnKathe']['item_mst_id'])){
                $seq = 1;
                foreach($this->request->data['TrnKathe']['item_mst_id'] as $line => $mst_id){
                    $TrnKatheItem = array();
                    if(isset($this->request->data['TrnKathe']['item_id'][$line])){
                        // 更新
                        $TrnKatheItem = array(
                            'TrnKatheItem' => array(
                                'id'                   => $this->request->data['TrnKathe']['item_id'][$line],
                                'trn_kathe_header_id'  => $trn_kathe_header_id,
                                'work_seq'             => $seq,
                                'mst_item_unit_id'     => $mst_id,
                                'quantity'             => $this->request->data['TrnKathe']['quantity'][$line],
                                'use_type'             => $this->request->data['TrnKathe']['use_type'][$line],
                                'is_deleted'           => false,
                                'modifier'             => $this->Session->read('Auth.MstUser.id'),
                                'modified'             => $now,
                                )
                            );
                    }else{
                        // 新規
                        $TrnKatheItem = array(
                            'TrnKatheItem' => array(
                                'trn_kathe_header_id'  => $trn_kathe_header_id,
                                'work_seq'             => $seq,
                                'mst_item_unit_id'     => $mst_id,
                                'quantity'             => $this->request->data['TrnKathe']['quantity'][$line],
                                'use_type'             => $this->request->data['TrnKathe']['use_type'][$line],
                                'is_deleted'           => false,
                                'creater'              => $this->Session->read('Auth.MstUser.id'),
                                'created'              => $now,
                                'modifier'             => $this->Session->read('Auth.MstUser.id'),
                                'modified'             => $now,
                                )
                            );
                    }
                    $seq++;
                    $this->TrnKatheItem->create();
                    if(!$this->TrnKatheItem->save($TrnKatheItem)){
                        $this->TrnKatheHeader->rollback();
                        $this->Session->setFlash('カテ情報術式登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('index');
                    }
                }
            }
            
            // 術式
            // いったん削除フラグを立てる。
            $res = $this->TrnKatheMethod->updateAll(array(
                'TrnKatheMethod.is_deleted' => 'true',
                'TrnKatheMethod.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnKatheMethod.modified'   => "'". $now ."'",
            ), array(
                'TrnKatheMethod.trn_kathe_header_id' => $trn_kathe_header_id,
            ), -1);

            if(false === $res){
                $this->TrnKatheHeader->rollback();
                $this->Session->setFlash('カテ情報術式登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect('index');
            }
            if(isset($this->request->data['TrnKathe']['methods_mst_id'])){
                $seq = 1;
                foreach($this->request->data['TrnKathe']['methods_mst_id'] as $line => $mst_id){
                    $TrnKatheMethod = array();
                    if(isset($this->request->data['TrnKathe']['methods_id'][$line])){
                        // 更新
                        $TrnKatheMethod = array(
                            'TrnKatheMethod' => array(
                                'id'                   => $this->request->data['TrnKathe']['methods_id'][$line],
                                'trn_kathe_header_id'  => $trn_kathe_header_id,
                                'work_seq'             => $seq,
                                'mst_kathe_methods_id' => $mst_id,
                                'is_deleted'           => false,
                                'modifier'             => $this->Session->read('Auth.MstUser.id'),
                                'modified'             => $now,
                                )
                            );
                    }else{
                        // 新規
                        $TrnKatheMethod = array(
                            'TrnKatheMethod' => array(
                                'trn_kathe_header_id'  => $trn_kathe_header_id,
                                'work_seq'             => $seq,
                                'mst_kathe_methods_id' => $mst_id,
                                'is_deleted'           => false,
                                'creater'              => $this->Session->read('Auth.MstUser.id'),
                                'created'              => $now,
                                'modifier'             => $this->Session->read('Auth.MstUser.id'),
                                'modified'             => $now,
                                )
                            );
                    }
                    $seq++;
                    $this->TrnKatheMethod->create();
                    if(!$this->TrnKatheMethod->save($TrnKatheMethod)){
                        $this->TrnKatheHeader->rollback();
                        $this->Session->setFlash('カテ情報術式登録処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('index');
                    }
                }
            }
            
            $this->TrnKatheHeader->commit();
            $this->request->data['TrnKatheHeader']['id'] = $trn_kathe_header_id;
            
        }else{
            
        }
    }
    
    function del($id){
        $this->TrnKatheHeader->begin();
        $ret = $this->TrnKatheHeader->del(array('id'=>$id));
        if(!$ret) {
            $this->TrnKatheHeader->rollback();
            $this->Session->setFlash('カテ情報取消処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('index');
        }
        
        $this->TrnKatheHeader->commit();
        $this->Session->setFlash('カテ情報登録を削除しました。', 'growl', array('type'=>'star') );
        $this->redirect('index');
    }

    /**
     * 帳票出力
     */
    function report(){
        $id = $this->request->data['TrnKatheHeader']['id'];

        // 帳票用データを取得
        $result = $this->getReportData($id);

        // 病名
        $sql  = ' select ';
        $sql .= '       b.kathe_disease_name  ';
        $sql .= '   from ';
        $sql .= '     trn_kathe_diseases as a  ';
        $sql .= '     left join mst_kathe_diseases as b  ';
        $sql .= '       on b.id = a.mst_kathe_disease_id  ';
        $sql .= '   where ';
        $sql .= '     a.trn_kathe_header_id = ' . $id ;
        $sql .= '     and a.is_deleted = false  ';
        $sql .= '   order by ';
        $sql .= '     a.work_seq ';
        
        $disease = $this->TrnKatheDisease->query($sql);
        $disease_name = '';
        foreach($disease as $d){
            if(empty($disease_name)){
                $disease_name = $d[0]['kathe_disease_name'];
            }else{
                $disease_name = $disease_name . ' ' . $d[0]['kathe_disease_name'];
            }
        }
        
        // 検査名
        $sql  = ' select ';
        $sql .= '       b.kathe_checkup_name  ';
        $sql .= '   from ';
        $sql .= '     trn_kathe_checkups as a  ';
        $sql .= '     left join mst_kathe_checkups as b  ';
        $sql .= '       on b.id = a.mst_kathe_checkup_id  ';
        $sql .= '   where ';
        $sql .= '     a.trn_kathe_header_id = ' . $id ;
        $sql .= '     and a.is_deleted = false  ';
        $sql .= '   order by ';
        $sql .= '     a.work_seq ';

        $checkup = $this->TrnKatheCheckup->query($sql);
        $checkup_name = '';
        foreach($checkup as $c){
            if(empty($checkup_name)){
                $checkup_name = $c[0]['kathe_checkup_name'];
            }else{
                $checkup_name = $checkup_name . ' ' . $c[0]['kathe_checkup_name'];
            }
        }

        // 点数周り取得
        $sql  = ' select ';
        $sql .= '       sum(d.refund_price * ( case when b.use_type = 1 then b.quantity else 0 end ) ) - sum(b.quantity * e.sales_price)';
        $sql .= '                                       as diff ';
        $sql .= '     , trunc(sum(d.refund_price * ( case when b.use_type = 1 then b.quantity else 0 end ) ) / 10, 0) ';
        $sql .= '                                       as item_total_point ';
        $sql .= '     , sum(b.quantity * e.sales_price) as total_price  ';
        $sql .= '   from ';
        $sql .= '     trn_kathe_headers as a  ';
        $sql .= '     left join trn_kathe_items as b  ';
        $sql .= '       on b.trn_kathe_header_id = a.id  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on c.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as d  ';
        $sql .= '       on d.id = c.mst_facility_item_id  ';
        $sql .= '     left join mst_sales_configs as e  ';
        $sql .= '       on e.mst_item_unit_id = c.id  ';
        $sql .= '       and e.start_date <= a.work_date  ';
        $sql .= '       and e.end_date > a.work_date  ';
        $sql .= '       and e.is_deleted = false  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $id ;
        $sql .= '     and b.is_deleted = false ';
        
        $point_data =  $this->TrnKatheHeader->query($sql);
        
        $columns = array(
            "検査コード",
            "検査日",
            "検査室番号",
            "入室時間",
            "帰室時間",
            "登録番号",
            "患者名",
            "性別",
            "生年月日",
            "実施区分",
            "病名",
            "検査名",
            "麻酔医",
            "麻酔種類",
            "担当科",
            "術者",
            "体位",
            "術式保険点数",
            "材料保険点数",
            "材料購入価",
            "差額",
            "区分",
            "単位",
            "数量",
            "購入価",
            "保険点数",
            "規格",
            "材料名",
            "略称",
            "医事コード",
            "製品番号"
            );
        $data = array();
        
        foreach($result as $r){
            $data[] = array(
                $r[0]['work_no'] ,  //検査コード
                $r[0]['work_date'] , //検査日
                $r[0]['room'] , //検査室番号
                $r[0]['in_room_time'] , //入室時間
                $r[0]['out_room_time'] , //帰室時間
                $r[0]['number'] , //登録番号
                $r[0]['name'] , //患者名
                $r[0]['sex'] , //性別
                $r[0]['birth'] , //生年月日
                $r[0]['type'] , //実施区分
                $disease_name , //病名
                $checkup_name , //検査名
                '' , //麻酔医
                $r[0]['kathe_anesthesia_name'] , //麻酔種類
                $r[0]['kathe_department_name'] , //担当科
                $r[0]['kathe_person_name'] , //術者
                $r[0]['kathe_position_name'] , //体位
                '0' , //術式保険点数
                $point_data[0][0]['item_total_point'] , //材料保険点数
                $point_data[0][0]['total_price'] , //材料購入価
                $point_data[0][0]['diff'] , //差額
                $r[0]['use_type'] , //区分
                $r[0]['unit_name'] , //単位
                $r[0]['quantity'] , //数量
                $r[0]['price'] , //購入価
                $r[0]['point'] , //保険点数
                $r[0]['standard'] , //規格
                $r[0]['item_name'] , //材料名
                $r[0]['code'] , //略称
                $r[0]['medical_code'] , //医事コード
                $r[0]['item_code'] , //製品番号
                );
        }

        
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'使用材料保険点数・購入価比較リスト');
        $this->xml_output($data,join("\t",$columns),$layout_name['34'],$fix_value);
    }


    function getReportData($id) {
        $sql  =' select ';
        $sql .='       a.work_no                          as work_no ';
        $sql .="     , to_char(a.work_date, 'YYYY/MM/DD') as work_date ";
        $sql .='     , e.kathe_room_code                  as room ';
        $sql .='     , a.in_room_time ';
        $sql .='     , a.out_room_time ';
        $sql .='     , (  ';
        $sql .='       case a.kathe_type  ';
        $sql .="         when '1' then '定時' ";
        $sql .="         when '2' then '臨時' ";
        $sql .="         when '3' then '時間外' ";
        $sql .='         end ';
        $sql .='     )                                    as type ';
        $sql .='     , a.number ';
        $sql .='     , a.name ';
        $sql .='     , (  ';
        $sql .='       case a.sex  ';
        $sql .="         when '1' then 'M'  ";
        $sql .="         when '2' then 'F'  ";
        $sql .='         end ';
        $sql .='     )                                    as sex ';
        $sql .="     , to_char(a.birth, 'YYYY年MM月DD日') as birth ";
        $sql .='     , f.kathe_anesthesia_name ';
        $sql .='     , g.kathe_department_name ';
        $sql .='     , h.kathe_person_name ';
        $sql .='     , i.kathe_position_name ';
        $sql .='     , d.internal_code                    as code ';
        $sql .='     , d.medical_code ';
        $sql .='     , d.item_name ';
        $sql .='     , d.standard ';
        $sql .='     , d.item_code ';
        $sql .='     , trunc(d.refund_price / 10, 0)      as point ';
        $sql .='     , j.sales_price                      as price ';
        $sql .='     , b.quantity ';
        $sql .='     , c1.unit_name ';
        $sql .='     , (  ';
        $sql .='       case b.use_type  ';
        $sql .="         when '1' then '' ";
        $sql .="         when '2' then '失敗' ";
        $sql .="         when '3' then 'ｻﾝﾌﾟﾙ' ";
        $sql .='         end ';
        $sql .='     )                                    as use_type  ';
        $sql .='   from ';
        $sql .='     trn_kathe_headers as a  ';
        $sql .='     left join trn_kathe_items as b  ';
        $sql .='       on b.trn_kathe_header_id = a.id  ';
        $sql .='     left join mst_item_units as c  ';
        $sql .='       on c.id = b.mst_item_unit_id  ';
        $sql .='     left join mst_unit_names as c1  ';
        $sql .='       on c1.id = c.mst_unit_name_id  ';
        $sql .='     left join mst_facility_items as d  ';
        $sql .='       on d.id = c.mst_facility_item_id  ';
        $sql .='     left join mst_kathe_rooms as e  ';
        $sql .='       on e.id = a.mst_kathe_room_id  ';
        $sql .='     left join mst_kathe_anesthesias as f  ';
        $sql .='       on f.id = a.mst_kathe_anesthesia_id  ';
        $sql .='     left join mst_kathe_departments as g  ';
        $sql .='       on g.id = a.mst_kathe_department_id  ';
        $sql .='     left join mst_kathe_people as h  ';
        $sql .='       on h.id = a.mst_kathe_person_id  ';
        $sql .='     left join mst_kathe_positions as i  ';
        $sql .='       on i.id = a.mst_kathe_position_id  ';
        $sql .='     left join mst_sales_configs as j  ';
        $sql .='       on j.mst_item_unit_id = b.mst_item_unit_id  ';
        $sql .='       and j.start_date <= now() ::date  ';
        $sql .='       and j.end_date > now() ::date  ';
        $sql .='       and j.is_deleted = false  ';
        $sql .='   where ';
        $sql .='     a.id = ' . $id;
        $sql .='     and b.is_deleted = false ';
        
        return $this->TrnKatheHeader->query($sql);
    }
    
    /**
     * プルダウンデータ作成
     */
    private function getPullDown(){
        // 部屋
        $sql  = 'select ';
        $sql .= '      a.id              as "MstKatheRoom__id"';
        $sql .= '    , a.kathe_room_code as "MstKatheRoom__kathe_room_code"';
        $sql .= '  from';
        $sql .= '    mst_kathe_rooms as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.kathe_room_code ';
        
        $this->set('room' , Set::Combine(
            $this->MstKatheRoom->query($sql),
            "{n}.MstKatheRoom.id",
            "{n}.MstKatheRoom.kathe_room_code"
            ));

        //術者
        $sql  = 'select ';
        $sql .= '      a.id                as "MstKathePerson__id"';
        $sql .= '    , a.kathe_person_name as "MstKathePerson__kathe_person_name"';
        $sql .= '  from';
        $sql .= '    mst_kathe_people as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.id ';
        $this->set('person' , Set::Combine(
            $this->MstKathePerson->query($sql),
            "{n}.MstKathePerson.id",
            "{n}.MstKathePerson.kathe_person_name"
            ));
        
        // 麻酔
        $sql  = 'select ';
        $sql .= '      a.id                    as "MstKatheAnesthesia__id"';
        $sql .= '    , a.kathe_anesthesia_name as "MstKatheAnesthesia__kathe_anesthesia_name"';
        $sql .= '  from';
        $sql .= '    mst_kathe_anesthesias as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.kathe_anesthesia_code';
        $this->set('anesthesia' , Set::Combine(
            $this->MstKatheAnesthesia->query($sql),
            "{n}.MstKatheAnesthesia.id",
            "{n}.MstKatheAnesthesia.kathe_anesthesia_name"
            ));
        
        // 体位
        $sql  = 'select ';
        $sql .= '      a.id                  as "MstKathePosition__id"';
        $sql .= '    , a.kathe_position_name as "MstKathePosition__kathe_position_name"';
        $sql .= '  from';
        $sql .= '    mst_kathe_positions as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.kathe_position_code ';
        $this->set('position', Set::Combine(
            $this->MstKathePosition->query($sql),
            "{n}.MstKathePosition.id",
            "{n}.MstKathePosition.kathe_position_name"
            ));

        //病棟
        $sql  = 'select ';
        $sql .= '      a.id              as "MstKatheWard__id"';
        $sql .= '    , a.kathe_ward_name as "MstKatheWard__kathe_ward_name"';
        $sql .= '  from';
        $sql .= '    mst_kathe_wards as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.kathe_ward_code ';
        $this->set('ward' , Set::Combine(
            $this->MstKatheWard->query($sql),
            "{n}.MstKatheWard.id",
            "{n}.MstKatheWard.kathe_ward_name"
            ));

        //科
        $sql  = 'select ';
        $sql .= '      a.id                    as "MstKatheDepartment__id"';
        $sql .= '    , a.kathe_department_name as "MstKatheDepartment__kathe_department_name"';
        $sql .= '  from';
        $sql .= '    mst_kathe_departments as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.kathe_department_code ';
        $this->set('department' , Set::Combine(
            $this->MstKatheDepartment->query($sql),
            "{n}.MstKatheDepartment.id",
            "{n}.MstKatheDepartment.kathe_department_name"
            ));
        // 病名
        $sql  = 'select ';
        $sql .= '      a.id                 as "MstKatheDisease__id"';
        $sql .= '    , a.kathe_disease_name as "MstKatheDisease__kathe_disease_name"';
        $sql .= '  from';
        $sql .= '    mst_kathe_diseases as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.id ';
        $this->set('mst_disease' , Set::Combine(
            $this->MstKatheDisease->query($sql),
            "{n}.MstKatheDisease.id",
            "{n}.MstKatheDisease.kathe_disease_name"
            ));
        
        // 検査
        $sql  = 'select ';
        $sql .= '      a.id                 as "MstKatheCheckup__id"';
        $sql .= '    , a.kathe_checkup_name as "MstKatheCheckup__kathe_checkup_name"';
        $sql .= '  from';
        $sql .= '    mst_kathe_checkups as a ';
        $sql .= '  where a.is_deleted = false ';
        $sql .= '    and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '  order by a.id ';
        $this->set('mst_checkup' , Set::Combine(
            $this->MstKatheCheckup->query($sql),
            "{n}.MstKatheCheckup.id",
            "{n}.MstKatheCheckup.kathe_checkup_name"
            ));
        
    }

    /**
     * カテヘッダ情報取得
     */
    private function getKatheHeaderData($id){
        $sql  = ' select ';
        $sql .= '       a.id ';
        $sql .= '     , a.work_no                         as code';
        $sql .= "     , to_char(a.work_date,'YYYY/MM/DD') as work_date ";
        $sql .= '     , a.kathe_type                      as type';
        $sql .= '     , a.mst_kathe_room_id               as room';
        $sql .= '     , a.mst_kathe_department_id         as "departmentCode"';
        $sql .= '     , a.mst_kathe_ward_id               as "wardCode"';
        $sql .= '     , a.mst_kathe_person_id             as person';
        $sql .= '     , a.mst_kathe_anesthesia_id         as anesthesia';
        $sql .= '     , a.mst_kathe_position_id           as position';
        $sql .= '     , a.number ';
        $sql .= '     , a.name ';
        $sql .= "     , to_char(a.birth,'YYYY/MM/DD')     as birth ";
        $sql .= '     , a.age ';
        $sql .= '     , a.sex ';
        $sql .= '     , a.blood_type                      as blood';
        $sql .= '     , a.in_room_time                    as inroomtime';
        $sql .= '     , a.out_room_time                   as outroomtime';
        $sql .= '   from ';
        $sql .= '     trn_kathe_headers as a  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $id;
        
        return $this->TrnKatheHeader->query($sql);
    }

    /**
     * カテ術式情報取得
     */
    private function getKatheMethodData($id){
        $sql  = ' select ';
        $sql .= '       a.id                   as "Method__id"';
        $sql .= '     , b.id                   as "Method__mst_id"';
        $sql .= '     , b.kathe_method_code    as "Method__code"';
        $sql .= '     , b.kathe_method_name    as "Method__name"';
        $sql .= '     , b.kathe_method_comment as "Method__comment"';
        $sql .= '     , b.kathe_method_point   as "Method__point"';
        $sql .= '   from ';
        $sql .= '     trn_kathe_methods as a  ';
        $sql .= '     left join mst_kathe_methods as b  ';
        $sql .= '       on b.id = a.mst_kathe_methods_id  ';
        $sql .= '   where ';
        $sql .= '     a.trn_kathe_header_id = ' . $id;
        $sql .= '     and a.is_deleted = false  ';
        $sql .= '   order by ';
        $sql .= '     a.work_seq ';
        
        return $this->TrnKatheMethod->query($sql);
    }

    /**
     * カテ検査情報取得
     */
    private function getKatheCheckupData($id){
        $sql  = ' select ';
        $sql .= '       a.mst_kathe_checkup_id as "Checkup__id"';
        $sql .= '   from ';
        $sql .= '     trn_kathe_checkups as a  ';
        $sql .= '   where ';
        $sql .= '     a.trn_kathe_header_id = ' . $id;
        $sql .= '     and a.is_deleted = false  ';
        $sql .= '   order by ';
        $sql .= '     a.work_seq ';
        
        $result = $this->TrnKatheCheckup->query($sql);
        $ret = array();
        foreach($result as $r){
            $ret[] = $r['Checkup']['id'];
        }
        return $ret;
    }

    /**
     * カテ病名情報取得
     */
    private function getKatheDiseaseData($id){
        $sql  = ' select ';
        $sql .= '       a.mst_kathe_disease_id as "Disease__id"';
        $sql .= '   from ';
        $sql .= '     trn_kathe_diseases as a  ';
        $sql .= '   where ';
        $sql .= '     a.trn_kathe_header_id = ' . $id;
        $sql .= '     and a.is_deleted = false  ';
        $sql .= '   order by ';
        $sql .= '     a.work_seq ';
        
        $result = $this->TrnKatheDisease->query($sql);
        $ret = array();
        foreach($result as $r){
            $ret[] = $r['Disease']['id'];
        }
        return $ret;
    }

    /**
     * カテ材料情報取得
     */
    private function getKatheItemData($id){
        $sql  = ' select ';
        $sql .= '       a.id                              as "Item__id"';
        $sql .= '     , b.id                              as "Item__mst_id" ';
        $sql .= '     , c.internal_code                   as "Item__code"';
        $sql .= '     , c.item_name                       as "Item__item_name"';
        $sql .= '     , c.standard                        as "Item__standard"';
        $sql .= '     , c.item_code                       as "Item__item_code"';
        $sql .= '     , a.quantity                        as "Item__quantity"';
        $sql .= '     , b1.unit_name                      as "Item__unit_name"';
        $sql .= '     , coalesce(trunc(c.refund_price / 10 ,0) , 0) as "Item__point" ';
        $sql .= '     , e.sales_price                     as "Item__price"';
        $sql .= '     , a.use_type                        as "Item__use_type"';
        $sql .= '     , c.medical_code                    as "Item__medical_code"';
        $sql .= '   from ';
        $sql .= '     trn_kathe_items as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as b1 ';
        $sql .= '       on b1.id = b.mst_unit_name_id ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join trn_kathe_headers as d  ';
        $sql .= '       on d.id = a.trn_kathe_header_id  ';
        $sql .= '     left join mst_sales_configs as e  ';
        $sql .= '       on e.mst_item_unit_id = b.id  ';
        $sql .= '       and e.start_date <= d.work_date  ';
        $sql .= '       and e.end_date > d.work_date '; 
        $sql .= '       and e.is_deleted = false  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false  ';
        $sql .= '     and a.trn_kathe_header_id = ' . $id;
        $sql .= '   order by a.work_seq';
        return $this->TrnKatheItem->query($sql);
    }


    /**
     * カテ術式取得 Ajax用
     * @access public
     * @param string $field
     * @param string $value
     * @return string json formatted.
     */
    public function search_kathe_method($field = null, $value = null) {
        App::import('Sanitize');
        Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($field) && isset($value)) {
            $_code = ($_POST['value']!='')? $_POST['value']:$value;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;

                $sql .= ' select ';
                $sql .= '       a.id ';
                $sql .= '     , a.kathe_method_code ';
                $sql .= '     , a.kathe_method_name ';
                $sql .= "     , coalesce(kathe_method_comment,'') as kathe_method_comment ";
                $sql .= '     , kathe_method_point ';
                $sql .= '   from ';
                $sql .= '     mst_kathe_methods as a ';
                $sql .= "   where a.kathe_method_code = '".$_code."'";
                $result = $this->TrnKatheMethod->query($sql);
                $_method = array();
                for ($_i = 0;$_i < count($result);$_i++) {
                    $_method[$_i]['id']                   = $result[$_i][0]['id'];
                    $_method[$_i]['kathe_method_code']    = $result[$_i][0]['kathe_method_code'];
                    $_method[$_i]['kathe_method_name']    = $result[$_i][0]['kathe_method_name'];
                    $_method[$_i]['kathe_method_comment'] = $result[$_i][0]['kathe_method_comment'];
                    $_method[$_i]['kathe_method_point']   = $result[$_i][0]['kathe_method_point'];
                }
                return (json_encode($_method));
            } else {
                throw new Exception('error');
            }
        }
    }

    /**
     * カテ材料取得 Ajax用
     * @access public
     * @param string $field
     * @param string $value
     * @return string json formatted.
     */
    public function search_kathe_item($field = null, $value = null) {
        App::import('Sanitize');
        //Configure::write('debug', 0);
        if (isset($_POST['field']) && isset($_POST['value']) || isset($field) && isset($value)) {
            $_code = ($_POST['value']!='')? $_POST['value']:$value;
            if($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $sql  = ' select ';
                $sql .= '       b.id ';
                $sql .= '     , a.internal_code ';
                $sql .= '     , a.item_name ';
                $sql .= '     , a.standard ';
                $sql .= '     , a.item_code ';
                $sql .= '     , coalesce(trunc(a.refund_price / 10, 0), 0) as point ';
                $sql .= '     , c.unit_name ';
                $sql .= '     , d.sales_price                              as price  ';
                $sql .= '   from ';
                $sql .= '     mst_facility_items as a  ';
                $sql .= '     left join mst_item_units as b  ';
                $sql .= '       on b.mst_facility_item_id = a.id  ';
                $sql .= '       and b.per_unit = 1  ';
                $sql .= '     left join mst_unit_names as c  ';
                $sql .= '       on c.id = b.mst_unit_name_id  ';
                $sql .= '     left join mst_sales_configs as d  ';
                $sql .= '       on d.mst_item_unit_id = b.id  ';
                $sql .= '       and d.start_date <= now()  ';
                $sql .= '       and d.end_date > now()  ';
                $sql .= '       and d.is_deleted = false  ';
                $sql .= '   where ';
                $sql .= '     a.is_deleted = false  ';
                $sql .= '     and (a.end_date is null or a.end_date > now()) ';
                $sql .= "     and a.internal_code = '"  . $_code  ."'";
                $result = $this->MstFacilityItem->query($sql);
                $_item = array();
                for ($_i = 0;$_i < count($result);$_i++) {
                    $_item[$_i]['id']        = $result[$_i][0]['id'];
                    $_item[$_i]['code']      = $result[$_i][0]['internal_code'];
                    $_item[$_i]['item_name'] = $result[$_i][0]['item_name'];
                    $_item[$_i]['standard']  = $result[$_i][0]['standard'];
                    $_item[$_i]['item_code'] = $result[$_i][0]['item_code'];
                    $_item[$_i]['point']     = $result[$_i][0]['point'];
                    $_item[$_i]['unit_name'] = $result[$_i][0]['unit_name'];
                    $_item[$_i]['price']     = $result[$_i][0]['price'];
                }
                return (json_encode($_item));
            } else {
                throw new Exception('error');
            }
        }
    }

    /**
     * 検査名検索
     */
    function checkupSearch(){
        $this->setRoleFunction(137); //検査名マスタ
        $result = array();
        if(isset($this->request->data['KatheCheckup']['is_search'])){
            $sql  = ' select ';
            $sql .= '       a.id                 as "CheckUp__id" ';
            $sql .= '     , a.kathe_checkup_name as "CheckUp__name"  ';
            $sql .= '   from ';
            $sql .= '     mst_kathe_checkups as a  ';
            $sql .= '   where ';
            $sql .= '     mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '   order by ';
            $sql .= '     a.id ';

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnKatheHeader'));

            $sql .= " LIMIT ".$this->request->data['limit'];
            
            $result = $this->MstKatheCheckup->query($sql);
        }
        $this->set('result',$result);
    }
    
    /**
     * 検査名編集
     */
    function checkupMod($id=null){
        if(!is_null($id)){
            $sql  = ' select ';
            $sql .= '       a.id                 as "CheckUp__id"  ';
            $sql .= '     , a.kathe_checkup_name as "CheckUp__name"  ';
            $sql .= '   from ';
            $sql .= '     mst_kathe_checkups as a  ';
            $sql .= '   where  ';
            $sql .= '     a.id = ' . $id;
            $ret = $this->MstKatheCheckup->query($sql);
            
            $this->request->data = $ret[0];
        }
    }
    
    /**
     * 検査名結果
     */
    function checkupResult(){
        $now = date('Y/m/d H:i:s');
        $this->MstKatheCheckup->begin();
        
        if(empty($this->request->data['CheckUp']['id'])){
            // 新規作成
            $MstKatheCheckup['MstKatheCheckup']['kathe_department_code'] = '0000';
            $MstKatheCheckup['MstKatheCheckup']['is_deleted']            = false;
            $MstKatheCheckup['MstKatheCheckup']['creater']               = $this->Session->read('Auth.MstUser.id');
            $MstKatheCheckup['MstKatheCheckup']['created']               = $now;
            $MstKatheCheckup['MstKatheCheckup']['mst_facility_id']       = $this->Session->read('Auth.facility_id_selected');
        }else{
            // 更新
            $MstKatheCheckup['MstKatheCheckup']['id'] = $this->request->data['CheckUp']['id'];
        }
        
        $MstKatheCheckup['MstKatheCheckup']['kathe_checkup_name'] = $this->request->data['CheckUp']['name']; 
        $MstKatheCheckup['MstKatheCheckup']['modifier']           = $this->Session->read('Auth.MstUser.id');
        $MstKatheCheckup['MstKatheCheckup']['modified']           = $now;
        
        $this->MstKatheCheckup->create();
        if(!$this->MstKatheCheckup->save($MstKatheCheckup)){
            $this->MstKatheCheckup->rollback();
            $this->Session->setFlash('検査名作成処理中にエラーが発生しました', 'growl', array('type'=>'error') );
            $this->redirect('checkupSearch');
        }
        $this->MstKatheCheckup->commit();
    }
}
