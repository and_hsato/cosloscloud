<?php
/**
 * InquiresController
 *　在庫照会
 * @version 1.0.0
 * @since 2010/09/10
 */
class SearchInquiriesController extends AppController {

    /**
     * @var $name
     */
    var $name = 'SearchInquiries';

    /**
     * @var array $uses
     */
    var $uses = array(
        'MstDepartment',
        'MstFacility',
        'MstUserBelonging',
        'TrnSticker',
        'TrnStock',
        'MstFacilityRelation'
        );

    /**
     * @var bool $scaffold
     */
    //var $scaffold;

    /**
     * @var array $helpers
     */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    /**
     * @var array $components
     */
    var $components = array('RequestHandler','Utils','CsvWriteUtils');

    /**
     *
     * 前処理
     */
    function beforeFilter() {
        //印刷セット
        $this->Auth->allowedActions[] = 'report';
        // 管理区分の取得
        $this->set('teisu' , Configure::read('TeisuType.List'));
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }

    /**
     * 在庫検索
     */
    function stock_list2() {
        App::import('Sanitize');
        $this->set( 'facility_list' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                          array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')) ,
                                                          true));
        $this->setRoleFunction(62); //在庫検索
        //検索結果格納変数
        $department_list = array();
        $Result = array();
        if( isset($this->request->data['isSearch']) ) {
            //SQL生成
            $sql = $this->createSQL_stock($this->request->data);

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnStock'));

            //リミットは画面から受け取り
            $sql .= " LIMIT ".$this->_getLimitCount();

            //検索実行
            $Result = $this->TrnStock->query($sql);
            // 施設が選択されていたら部署リストを取得
            if($this->request->data['facilityCode'] != '' ){
                $department_list = $this->getDepartmentsList($this->request->data['facilityCode']);
            }
        }

        $this->set( 'owners' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                      array(Configure::read('FacilityType.donor')) ,
                                                      true));

        $this->set('Result',$Result);
        $this->set('department_list' , $department_list);
        $this->set('data',$this->request->data);

        $this->render('stock_list2');
    }


    /**
     * 在庫検索CSV出力
     */
    function output_csv() {
        $sql = $this->_getStockCSV($this->request->data);
        $this->db_export_csv($sql , "在庫一覧", '/search_inquiries/stock_list');
    }

    /**
     * 在庫検索(病院部署向け)
     */
    function stock_list(){
        App::import('Sanitize');
        $department_list = $this->getHospitalDepartmentList($this->Session->read('Auth.facility_id_selected'));
        $this->set('department_list' , $department_list);
        
        $this->setRoleFunction(136); //在庫検索（病院）
        //検索結果格納変数
        $department_list = array();
        $Result = array();
        if( isset($this->request->data['isSearch']) ) {
            //SQL生成
            $sql = $this->createSQL_stock($this->request->data,2);

            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnStock'));

            //リミットは画面から受け取り
            $sql .= " LIMIT ".$this->_getLimitCount();

            //検索実行
            $Result = $this->TrnStock->query($sql);
        }

        $this->set( 'owners' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                      array(Configure::read('FacilityType.donor')) ,
                                                      true));

        $this->set('Result',$Result);

        $this->set('data',$this->request->data);

        $this->render('stock_list');
    }

    /**
     * 在庫検索CSV出力
     */
    function output_csv2() {
        $sql = $this->_getStockCSV($this->request->data,2);
        $this->db_export_csv($sql , "在庫一覧", '/search_inquiries/stock_list');
    }

    
    private function _getStockCSV($data,$type=null){
        $data = Sanitize::clean($data);
        $sql  = ' select ';
        $sql .= '       c.facility_name                    as 施設名';
        $sql .= '     , b.department_name                  as 部署名';
        $sql .= '     , e.internal_code                    as "商品ID" ';
        $sql .= '     , e.item_name                        as 商品名';
        $sql .= '     , e.item_code                        as 製品番号';
        $sql .= '     , e.standard                         as 規格';
        $sql .= '     , f.dealer_name                      as 販売元';
        $sql .= '     , d1.unit_name                       as 包装単位名';
        $sql .= '     , d.per_unit                         as 包装単位入数';
        $sql .= '     , d2.unit_name                       as 基本単位名';
        $sql .= '     , a.stock_count                      as 在庫数';
        $sql .= '     , coalesce(x.fixed_stock_count, 0)   as 定数在庫 ';
        $sql .= '     , coalesce(x.ex_stock_count, 0)      as 臨時在庫 ';
        $sql .= '     , coalesce(y.count, 0)               as シール無数  ';
        $sql .= '     , a.requested_count                  as 入荷予定数';
        $sql .= '     , coalesce(g.order_point, 0)         as 発注点 ';
        $sql .= '     , coalesce(g.fixed_count, 0)         as 定数 ';
        $sql .= '     , coalesce(g.holiday_fixed_count, 0) as 休日定数 ';
        $sql .= '     , coalesce(g.spare_fixed_count, 0)   as 予備定数 ';
        $sql .= '     , h.name                             as 棚番 ';
        $sql .= '     , ( a.stock_count - a.reserve_count - a.promise_count ) ';
        $sql .= '                                          as 引当可能数 ';
        $sql .= '     , ( case ';
        foreach(Configure::read('TeisuType.List') as $k => $v ){
            $sql .= "         when g.trade_type = '" . $k . "' then '" . $v . "'";
        }
        $sql .= '       end )                              as 定数区分';
        
        $sql .= '   from ';
        $sql .= '     trn_stocks as a  ';
        $sql .= '     left join mst_departments as b  ';
        $sql .= '       on b.id = a.mst_department_id  ';
        $sql .= '     left join mst_facilities as c  ';
        $sql .= '       on c.id = b.mst_facility_id  ';
        $sql .= '     left join mst_item_units as d  ';
        $sql .= '       on d.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as d1  ';
        $sql .= '       on d1.id = d.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as d2  ';
        $sql .= '       on d2.id = d.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as e  ';
        $sql .= '       on e.id = d.mst_facility_item_id  ';
        $sql .= '     left join mst_facilities as e2 ';
        $sql .= '       on e2.id = e.mst_owner_id ';
        $sql .= '     left join mst_dealers as f  ';
        $sql .= '       on f.id = e.mst_dealer_id  ';
        $sql .= '     left join mst_fixed_counts as g  ';
        $sql .= '       on g.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '       and g.mst_department_id = a.mst_department_id  ';
        $sql .= '       and g.is_deleted = false  ';
        $sql .= '     left join mst_shelf_names as h  ';
        $sql .= '       on h.id = g.mst_shelf_name_id  ';
        $sql .= '     left join (  ';
        $sql .= '       select ';
        $sql .= '             a.mst_department_id ';
        $sql .= '           , a.mst_item_unit_id ';
        $sql .= '           , sum(  ';
        $sql .= '             case  ';
        $sql .= '               when a.trade_type = ' . Configure::read('ClassesType.Temporary');
        $sql .= '               or a.trade_type = ' . Configure::read('ClassesType.ExShipping');
        $sql .= '               or a.trade_type = ' . Configure::read('ClassesType.ExNoStock');
        $sql .= '               then a.quantity  ';
        $sql .= '               else 0  ';
        $sql .= '               end ';
        $sql .= '           )            as ex_stock_count ';
        $sql .= '           , sum(  ';
        $sql .= '             case  ';
        $sql .= '               when a.trade_type <> ' . Configure::read('ClassesType.Temporary');
        $sql .= '               and a.trade_type <> ' . Configure::read('ClassesType.ExShipping');
        $sql .= '               and a.trade_type <> ' . Configure::read('ClassesType.ExNoStock');
        $sql .= '               then a.quantity  ';
        $sql .= '               else 0  ';
        $sql .= '               end ';
        $sql .= '           )            as fixed_stock_count  ';
        $sql .= '         from ';
        $sql .= '           trn_stickers as a  ';
        $sql .= '         where ';
        $sql .= '           a.is_deleted = false  ';
        $sql .= '           and a.quantity > 0  ';
        $sql .= "           and a.lot_no != ' ' ";
        $sql .= '         group by ';
        $sql .= '           a.mst_department_id ';
        $sql .= '           , a.mst_item_unit_id ';
        $sql .= '     ) as x  ';
        $sql .= '       on x.mst_department_id = a.mst_department_id  ';
        $sql .= '       and x.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '     left join (  ';
        $sql .= '       select ';
        $sql .= '             a.mst_department_id ';
        $sql .= '           , a.mst_item_unit_id ';
        $sql .= '           , sum(a.quantity) as count  ';
        $sql .= '         from ';
        $sql .= '           trn_stickers as a  ';
        $sql .= '         where ';
        $sql .= '           a.is_deleted = false  ';
        $sql .= '           and a.quantity > 0  ';
        $sql .= "           and a.facility_sticker_no = '' ";
        $sql .= "           and a.hospital_sticker_no = '' ";
        $sql .= '         group by ';
        $sql .= '           a.mst_department_id ';
        $sql .= '           , a.mst_item_unit_id ';
        $sql .= '     ) as y  ';
        $sql .= '       on y.mst_department_id = a.mst_department_id  ';
        $sql .= '       and y.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false ';
        $sql .= '     and e.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        
        /*  画面からの検索条件追加  */
        //施主(完全一致)
        if(!empty($data['ownerCode'])){
            $sql .= " and e2.facility_code = '".$data['ownerCode']."'";
        }
        //施設(完全一致)
        if(!empty($data['facilityCode'])){
            $sql .= " and c.facility_code = '".$data['facilityCode']."'";
        }
        //部署(完全一致)
        if(!empty($data['departmentCode'])){
            $sql .= " and b.department_code = '".$data['departmentCode']."'";
        }
        //商品ID(前方一致)
        if(!empty($data['internal_code'])){
            $sql .= " and e.internal_code ILIKE '".$data['internal_code']."%'";
        }
        //商品名(部分一致)
        if(!empty($data['item_name'])){
            $sql .= " and e.item_name ILIKE '%".$data['item_name']."%'";
        }
        //製品番号(前方一致)
        if(!empty($data['item_code'])){
            $sql .= " and e.item_code ILIKE '".$data['item_code']."%'";
        }
        //規格(部分一致)
        if(!empty($data['standard'])){
            $sql .= " and e.standard ILIKE '%".$data['standard']."%'";
        }
        //販売元(部分一致)
        if(!empty($data['dealer_name'])){
            $sql .= " and f.dealer_name ILIKE '%".$data['dealer_name']."%'";
        }
        //管理区分
        if(!empty($data['trade_type'])){
            $sql .= " and g.trade_type = ".$data['trade_type'] ;
        }
        
        //予備キー1
        if(!empty($data['spare_key1'])){
            $sql .= " and e.spare_key1 ILIKE '%".$data['spare_key1']."%'";
        }
        //予備キー2
        if(!empty($data['spare_key2'])){
            $sql .= " and e.spare_key2 ILIKE '%".$data['spare_key2']."%'";
        }
        //予備キー3
        if(!empty($data['spare_key3'])){
            $sql .= " and e.spare_key3 ILIKE '%".$data['spare_key3']."%'";
        }
        // 在庫なしは除く
        if(isset($data['all_zero'])){
            // TrnStockが全部０ AND 定数が無い。
            $sql .= ' and ( a.stock_count != 0 ';
            $sql .= ' or a.reserve_count != 0 ';
            $sql .= ' or a.promise_count != 0 ';
            $sql .= ' or a.requested_count != 0 ';
            $sql .= ' or a.receipted_count != 0 ';
            $sql .= ' or a.lowlevel_stock_count != 0 ';
            $sql .= ' or g.id is not null )';
        }
        
        //フリー検索
        if(!empty($data['Free'])){
            $sql .= " and coalesce( e.internal_code , '') ";
            $sql .= " ||  coalesce( e.item_name , '')";
            $sql .= " ||  coalesce( e.item_code  , '') ";
            $sql .= " ||  coalesce( e.standard  , '') ";
            $sql .= " ||  coalesce( e.spare_key1  , '') ";
            $sql .= " ||  coalesce( e.spare_key2  , '') ";
            $sql .= " ||  coalesce( e.spare_key3  , '') ";
            $sql .= " ilike '%" . $data['Free'] . "%'";
        }
        
        if(!is_null($type)){
            $sql .= ' and c.facility_type != ' . Configure::read('FacilityType.center');
        }
        $sql .= ' order by';
        $sql .= '       e.internal_code';
        $sql .= '      ,d.per_unit';
        $sql .= '      ,c.facility_code';
        $sql .= '      ,b.department_code';
        
        return $sql;
    }



    /**
     * 期限切れ検索
     */
    function expiration_list() {
        App::import('Sanitize');
        $this->set( 'facility_list' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                             array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')) ,
                                                             true));
        $department_list = array();
        //検索結果格納変数
        $Result = array();
        if( isset($this->request->data['isSearch'])) {
            $sql = $this->createSQL($this->request->data,false);

            //where追加
            $sql .= " AND STCK.validated_date IS NOT NULL";

            //検索タイプでシールの有効期限を検索する
            if($this->request->data["expiration_type"] == "1"){
                //$sql .= " AND date_part('day',(STCK.validated_date - current_timestamp)) > 0";
                $sql .= " AND ( ";
                $sql .= "   CASE ";
                $sql .= "   WHEN FCIT.expired_date IS NOT NULL THEN FCIT.expired_date";
                $sql .= "   WHEN FCIT.expired_date IS NULL THEN FCLT.days_before_expiration";
                $sql .= "    END ) >= date_part('day',(STCK.validated_date - current_timestamp))";
            }else if($this->request->data["expiration_type"] == "2"){
                $sql .= " AND STCK.validated_date <= current_timestamp + '".$this->request->data['expiration_after']." days'";
            }else if($this->request->data["expiration_type"] == "3"){
                $sql .= " AND STCK.validated_date <= '".$this->request->data['expiration_date']."'";
            }
            //ORDER BY
            $sql .= " ORDER BY FCLT.id,FCIT.internal_code,STCK.facility_sticker_no,STCK.hospital_sticker_no";
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnSticker'));

            //リミットは画面から受け取り
            $sql .= " LIMIT ".$this->_getLimitCount();

            $Result = $this->TrnSticker->query($sql);
            // 施設が選択されていたら部署リストを取得
            if($this->request->data['facilityCode'] != '' ){
                $department_list = $this->getDepartmentsList($this->request->data['facilityCode']);
            }
            
        }
        $this->set('department_list' , $department_list);
        $this->set('data',$this->request->data);
        $this->set('Result',$Result);
        $this->render('expiration_list');
    }

    /**
     * 期限切れ一覧帳票出力
     */
    function report_expiration() {
        //検索結果格納変数
        $Result = array();
        //sql
        $sql = $this->createSQL($this->request->data,true);

        //where追加
        $sql .= " AND STCK.validated_date IS NOT NULL";

        //検索タイプでシールの有効期限を検索する
        if($this->request->data["expiration_type"] == "1"){
            //$sql .= " AND date_part('day',(STCK.validated_date - current_timestamp)) > 0";
            $sql .= " AND ( ";
            $sql .= "   CASE ";
            $sql .= "   WHEN FCIT.expired_date IS NOT NULL THEN FCIT.expired_date";
            $sql .= "   WHEN FCIT.expired_date IS NULL THEN FCLT.days_before_expiration";
            $sql .= "    END ) >= date_part('day',(STCK.validated_date - current_timestamp))";

        } else if($this->request->data["expiration_type"] == "2"){
            $sql .= " AND STCK.validated_date <= current_timestamp + '".$this->request->data['expiration_after']." days'";
        } else if($this->request->data["expiration_type"] == "3"){
            $sql .= " AND STCK.validated_date <= '".$this->request->data['expiration_date']."'";
        }

        //ORDER BY
        $sql .= " ORDER BY FCLT.id,FCIT.internal_code,STCK.facility_sticker_no,STCK.hospital_sticker_no";
        //印刷内容取得
        $this->TrnSticker->unbindModel(
            array('belongsTo'=>array('TrnReceiving','TrnShipping','TrnStorage','MstDepartment','MstItemUnit')),false);
        $Result = $this->TrnSticker->query($sql);

        /*  帳票の内容設定  */
        //帳票の種類を格納した配列取得
        $layout_name = Configure::read('layoutnameModels');

        //帳票のヘッダ項目設定
        $columns = array("行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "センターシール",
                         "包装単位",
                         "課税区分",
                         "単価",
                         "仕入先",
                         "部署シール",
                         "ロット番号",
                         "有効期限",
                         "棚番",
                         "商品ID",
                         "配置場所",
                         "検索条件",
                         "印刷年月日"
                         );

        //検索条件行を作成
        $search_condition = "";

        //施設
        if(!empty($this->request->data["facility"])){
            $search_condition .= "施設 = ".$this->request->data["facility_name"]."　";
        }
        //部署
        if(!empty($this->request->data["department"])){
            $search_condition .= "部署 = ".$this->request->data["department_name"]."　";
        }
        //商品ID
        if(!empty($this->request->data["internal_code"])){
            $search_condition .= "商品ID = ".$this->request->data["internal_code"]."　";
        }
        //商品名
        if(!empty($this->request->data["item_name"])){
            $search_condition .= "商品名 = ".$this->request->data["item_name"]."　";
        }
        //製品番号
        if(!empty($this->request->data["item_code"])){
            $search_condition .= "製品番号 = ".$this->request->data["item_code"]."　";
        }
        //規格
        if(!empty($this->request->data["standard"])){
            $search_condition .= "規格 = ".$this->request->data["standard"]."　";
        }
        //販売元
        if(!empty($this->request->data["dealer_name"])){
            $search_condition .= "販売元 = ".$this->request->data["dealer_name"]."　";
        }
        //ロットNO(完全一致)
        if(!empty($this->request->data["lot_no"])){
            $search_condition .= "ロットNo = ".$this->request->data["lot_no"]."　";
        }
        //シール番号(センターシール番号または部署シール番号に完全一致)
        if(!empty($this->request->data["sticker_no"])){
            $search_condition .= "シール番号 = ".$this->request->data["sticker_no"]."　";
        }
        //管理区分(完全一致)
        if(!empty($this->request->data["trade_type"])){
            if($this->request->data["trade_type"] == Configure::read('TeisuType.Constant')){
                $search_condition .= "管理区分 = 定数品　";
            }else if($this->request->data["trade_type"] == Configure::read('TeisuType.SemiConstant')){
                $search_condition .= "管理区分 = 準定数品　";
            }else if($this->request->data["trade_type"] == Configure::read('TeisuType.Deposit')){
                $search_condition .= "管理区分 = 預託品　";
            }
        }

        //検索タイプ
        if($this->request->data["expiration_type"] == "1"){
            $search_condition .= "　期限切れ警告期間のもの";
        } else if($this->request->data["expiration_type"] == "2"){
            $search_condition .= "　指定日数の間に切れるもの";
            $search_condition .= "：".$this->request->data["expiration_after"]."日後";
        } else if($this->request->data["expiration_type"] == "3"){
            $search_condition .= "　指定日までに切れるもの";
            $search_condition .= "：".$this->request->data["expiration_date"]."まで";
        }

        //帳票のタイトル
        $fix_value = array('タイトル'=>'期限切れ一覧');


        //一行分のデータを格納
        $row_data = array();
        foreach($Result as $index => $row){
            $row_data[] = array(
                $index + 1
                ,$row[0]['item_name']
                ,$row[0]['standard']
                ,$row[0]['item_code']
                ,$row[0]['dealer_name']
                ,$row[0]['facility_sticker_no']
                ,$row[0]['item_unit_name']
                ,$row[0]['tax_type']
                ,number_format($row[0]['stocking_price'],2)
                ,$row[0]['supplier_name']
                ,$row[0]['hospital_sticker_no']
                ,$row[0]['lot_no']
                ,date("Y/m/d",strtotime($row[0]['validated_date']))
                ,$row[0]['code']
                ,$row[0]['internal_code']
                ,$row[0]['facility_name']."／".$row[0]['dept_name1']
                ,$search_condition
                ,date("Y/m/d")
                );
        }
        //XML出力関数呼び出し
        $this->xml_output($row_data,join("\t",$columns),$layout_name['17'],$fix_value);

    }


    /**
     * 期限切れ一覧CSV出力
     */
    function output_expiration_list_csv() {
        $this->setRoleFunction(63); //期限切れ警告一覧

        $sql = $this->_getExpirationCSV($this->request->data);
        //where追加
        $sql .= " AND STCK.validated_date IS NOT NULL";

        //検索タイプでシールの有効期限を検索する
        if($this->request->data["expiration_type"] == "1"){
            //$sql .= " AND date_part('day',(STCK.validated_date - current_timestamp)) > 0";
            $sql .= " AND date_part('day',(STCK.validated_date - current_timestamp)) <= FCIT.expired_date";
        } else if($this->request->data["expiration_type"] == "2"){
            $sql .= " AND STCK.validated_date <= current_timestamp + '".$this->request->data['expiration_after']." days'";
        } else if($this->request->data["expiration_type"] == "3"){
            $sql .= " AND STCK.validated_date <= '".$this->request->data['expiration_date']."'";
        }
       //ORDER BY
        $sql .= " ORDER BY FCLT.id,FCIT.internal_code,STCK.facility_sticker_no,STCK.hospital_sticker_no";


        $this->db_export_csv($sql , "期限切れ一覧", '/search_inquiries/expiration_list');
    }


    private function _getExpirationCSV($data){
        App::import('Sanitize');
        $data = Sanitize::clean($data);
        $sql = "";
        $sql .= 'SELECT FCIT.internal_code       as "商品ID"';
        $sql .= "      ,SLNM.code                as 棚番";
        $sql .= "      ,FCIT.item_name           as 商品名";
        $sql .= "      ,FCIT.item_code           as 製品番号";
        $sql .= "      ,FCIT.standard            as 規格";
        $sql .= "      ,DELR.dealer_name         as 販売元";
        $sql .= "      ,UNNM1.unit_name          as 包装単位名";
        $sql .= "      ,ITUT.per_unit            as 入数";
        $sql .= "      ,UNNM2.unit_name          as 入数単位名";
        $sql .= "      ,RCVG.stocking_price      as 仕入単価";
        $sql .= "      ,STCK.facility_sticker_no as センターシール番号";
        $sql .= "      ,STCK.hospital_sticker_no as 部署シール番号";
        $sql .= "      ,STCK.lot_no              as ロット番号";
        $sql .= "      ,TO_CHAR(STCK.validated_date,'YYYY/MM/DD') as 有効期限";
        $sql .= "      ,FCLT.facility_name as 配置施設 ";
        $sql .= "      ,DPMT1.department_name AS 配置部署";
        $sql .= "      ,FCLTY.facility_name AS 仕入先";
        $sql .= "      ,STCK.quantity as 数量";
        $sql .= "  FROM trn_stickers STCK";
        //包装単位
        $sql .= " INNER JOIN mst_item_units ITUT";
        $sql .= "    ON STCK.mst_item_unit_id = ITUT.id";
        $sql .= "   AND ITUT.mst_facility_id = ".$data["facilityId"];
        $sql .= "   AND ITUT.is_deleted = false";
        //施設採用品
        $sql .= " INNER JOIN mst_facility_items FCIT";
        $sql .= "    ON ITUT.mst_facility_item_id = FCIT.id";
        $sql .= "   AND ITUT.is_deleted = false";
        //定数設定
        $sql .= "  LEFT JOIN mst_fixed_counts FXCT";
        $sql .= "    ON ITUT.id = FXCT.mst_item_unit_id";
        $sql .= "   AND STCK.mst_department_id = FXCT.mst_department_id";
        //棚名称
        $sql .= "  LEFT JOIN mst_shelf_names SLNM";
        $sql .= "    ON FXCT.mst_shelf_name_id = SLNM.id";
        $sql .= "   AND SLNM.is_deleted = false";
        //販売元
        $sql .= "  LEFT JOIN mst_dealers DELR";
        $sql .= "    ON FCIT.mst_dealer_id = DELR.id";
        $sql .= "   AND DELR.is_deleted = false";
        //包装単位名
        $sql .= " INNER JOIN mst_unit_names UNNM1";
        $sql .= "    ON ITUT.mst_unit_name_id = UNNM1.id";
        $sql .= "   AND UNNM1.is_deleted = false";
        //入数単位名
        $sql .= " INNER JOIN mst_unit_names UNNM2";
        $sql .= "    ON ITUT.per_unit_name_id = UNNM2.id";
        $sql .= "   AND UNNM2.is_deleted = false";
        //入荷
        $sql .= "  LEFT JOIN trn_receivings RCVG";
        $sql .= "    ON STCK.trn_receiving_id = RCVG.id";
        $sql .= "   AND RCVG.is_deleted = false";
        $sql .= "   AND RCVG.receiving_type = " . Configure::read('ReceivingType.arrival');
        //部署(配置場所)
        $sql .= "  LEFT JOIN mst_departments DPMT1";
        $sql .= "    ON STCK.mst_department_id = DPMT1.id";
        $sql .= "   AND DPMT1.is_deleted = false";
        //施設
        $sql .= "  LEFT JOIN mst_facilities FCLT";
        $sql .= "    ON DPMT1.mst_facility_id = FCLT.id";
        $sql .= "   AND FCLT.is_deleted = false";
        //ユーザ操作権限
        $sql .= '  inner join mst_user_belongings as MUB';
        $sql .= '    on MUB.mst_facility_id = FCLT.id';
        $sql .= '    and MUB.mst_user_id = ' . $this->request->data['userId'];
        $sql .= '    and MUB.is_deleted = false ';
        //部署(仕入先)
        $sql .= "  LEFT JOIN mst_departments DPMT2";
        $sql .= "    ON RCVG.department_id_from = DPMT2.id";
        $sql .= "   AND DPMT2.is_deleted = false";
        //施設(仕入先)
        $sql .= "  LEFT JOIN mst_facilities FCLTY";
        $sql .= "    ON DPMT2.mst_facility_id = FCLTY.id";
        $sql .= "   AND FCLTY.is_deleted = false";
        //WHERE句
        $sql .= " WHERE STCK.is_deleted = false";
        $sql .= "   AND STCK.quantity > 0 ";
        /*  画面からの検索条件追加  */
        //施設(完全一致)
        if(!empty($data["facility"])){
            $sql .= " AND FCLT.facility_code = '".$data["facility"]."'";
        }
        //部署(完全一致)
        if(!empty($data["department"])){
            $sql .= " AND DPMT1.department_code = '".$data["department"]."'";
        }
        //商品ID(前方一致)
        if(!empty($data["internal_code"])){
            $sql .= " AND FCIT.internal_code ILIKE '".$data["internal_code"]."%'";
        }
        //商品名(部分一致)
        if(!empty($data["item_name"])){
            $sql .= " AND FCIT.item_name ILIKE '%".$data["item_name"]."%'";
        }
        //製品番号(前方一致)
        if(!empty($data["item_code"])){
            $sql .= " AND FCIT.item_code ILIKE '".$data["item_code"]."%'";
        }
        //規格(部分一致)
        if(!empty($data["standard"])){
            $sql .= " AND FCIT.standard ILIKE '%".$data["standard"]."%'";
        }
        //販売元(部分一致)
        if(!empty($data["dealer_name"])){
            $sql .= " AND DELR.dealer_name ILIKE '%".$data["dealer_name"]."%'";
        }
        //ロットNO(完全一致)
        if(!empty($data["lot_no"])){
            $sql .= " AND STCK.lot_no = '".$data["lot_no"]."'";
        }
        //シール番号(センターシール番号または部署シール番号に完全一致)
        if(!empty($data["sticker_no"])){
            $sql .= " AND (STCK.facility_sticker_no ILIKE '".$data["sticker_no"]."'";
            $sql .= "  OR  STCK.hospital_sticker_no ILIKE '".$data["sticker_no"]."')";
        }
        //管理区分(完全一致)
        if(!empty($data["trade_type"])){
            $sql .= " AND STCK.trade_type = ".$data["trade_type"];
        }

        return $sql;
    }


    /**
     * 不動在庫検索
     */
    function immovable_list() {

        $this->set( 'facility_list' , $this->getFacilityList($this->Session->read('Auth.facility_id_selected'),
                                                             array(Configure::read('FacilityType.center'),Configure::read('FacilityType.hospital')) ,
                                                             true));
        $department_list = array();
        $this->setRoleFunction(64); //不動在庫一覧

        //検索結果格納変数
        $Result = array();

        if( isset($this->request->data['isSearch']) ) {
            $sql  = $this->createSQL($this->request->data,false);
            //where追加
            $sql .= " AND CASE ";
            $sql .= "       WHEN STCK.last_move_date IS NULL";
            $sql .= "        AND STCK.lot_no = ' ' ";
            $sql .= "       THEN 0 ";
            $sql .= "       ELSE date_part('day',current_timestamp - COALESCE(STCK.last_move_date,RCVG.work_date,STCK.created))";
            $sql .= "     END >= ".$this->request->data['immovable_date']." ";
            $sql .= " AND STCK.lot_no <> ' '";

            //ORDER BY
            $sql .= " ORDER BY FCLT.id,FCIT.internal_code,STCK.facility_sticker_no,STCK.hospital_sticker_no";
            //全件取得
            $this->set('max' , $this->getMaxCount($sql , 'TrnSticker'));

            //リミットは画面から受け取り
            $sql .= " LIMIT ".$this->_getLimitCount();
            $Result = $this->TrnSticker->query($sql);
            // 施設が選択されていたら部署リストを取得
            if($this->request->data['facilityCode'] != '' ){
                $department_list = $this->getDepartmentsList($this->request->data['facilityCode']);
            }
        }
        $this->set('department_list' , $department_list);
        $this->set('data',$this->request->data);
        $this->set('Result',$Result);
        $this->render('immovable_list');
    }

    /**
     * 不動在庫一覧帳票出力
     */
    function report_immovable() {
        //検索結果格納変数
        $Result = array();

        $sql  = $this->createSQL($this->request->data,true);
        //where追加
        $sql .= " AND CASE ";
        $sql .= "       WHEN STCK.last_move_date IS NULL";
        $sql .= "        AND STCK.lot_no = ' ' ";
        $sql .= "       THEN 0 ";
        $sql .= "       ELSE date_part('day',current_timestamp - COALESCE(STCK.last_move_date,RCVG.work_date,STCK.created))";
        $sql .= "     END >= ".$this->request->data['immovable_date']." ";
        $sql .= " AND STCK.lot_no <> ' '";

        //ORDER BY
        $sql .= " ORDER BY FCLT.id,FCIT.internal_code,STCK.facility_sticker_no,STCK.hospital_sticker_no";
        //印刷内容取得
        $this->TrnSticker->unbindModel(
            array('belongsTo'=>array('TrnReceiving','TrnShipping','TrnStorage','MstDepartment','MstItemUnit')),false);
        $Result = $this->TrnSticker->query($sql);

        /*  帳票の内容設定  */
        //帳票の種類を格納した配列取得
        $layout_name = Configure::read('layoutnameModels');

        //帳票のヘッダ項目設定
        $columns = array("行番号",
                         "商品ID",
                         "商品名",
                         "製品番号",
                         "包装単位",
                         "センターシール",
                         "ロット番号",
                         "定数",
                         "配置場所",
                         "棚番",
                         "規格",
                         "販売元",
                         "課税区分",
                         "単価",
                         "部署シール",
                         "有効期限",
                         "不動日数",
                         "仕入先",
                         "検索条件",
                         "印刷年月日"
                         );

        //検索条件行を作成
        $search_condition = "";

        //施設
        if(!empty($this->request->data["facility"])){
            $search_condition .= "施設 = ".$this->request->data["facility_name"]."　";
        }
        //部署
        if(!empty($this->request->data["department"])){
            $search_condition .= "部署 = ".$this->request->data["department_name"]."　";
        }
        //商品ID
        if(!empty($this->request->data["internal_code"])){
            $search_condition .= "商品ID = ".$this->request->data["internal_code"]."　";
        }
        //商品名
        if(!empty($this->request->data["item_name"])){
            $search_condition .= "商品名 = ".$this->request->data["item_name"]."　";
        }
        //製品番号
        if(!empty($this->request->data["item_code"])){
            $search_condition .= "製品番号 = ".$this->request->data["item_code"]."　";
        }
        //規格
        if(!empty($this->request->data["standard"])){
            $search_condition .= "規格 = ".$this->request->data["standard"]."　";
        }
        //販売元
        if(!empty($this->request->data["dealer_name"])){
            $search_condition .= "販売元 = ".$this->request->data["dealer_name"]."　";
        }
        //ロットNO
        if(!empty($this->request->data["lot_no"])){
            $search_condition .= "ロットNo = ".$this->request->data["lot_no"]."　";
        }
        //シール番号
        if(!empty($this->request->data["sticker_no"])){
            $search_condition .= "シール番号 = ".$this->request->data["sticker_no"]."　";
        }
        //管理区分
        if(!empty($this->request->data["trade_type"])){
            if($this->request->data["trade_type"] == Configure::read('TeisuType.Constant')){
                $search_condition .= "管理区分 = 定数品　";
            } else if($this->request->data["trade_type"] == Configure::read('TeisuType.SemiConstant')){
                $search_condition .= "管理区分 = 準定数品　";
            } else if($this->request->data["trade_type"] == Configure::read('TeisuType.Deposit')){
                $search_condition .= "管理区分 = 預託品　";
            }
        }
        //不動日数
        $search_condition .= $this->request->data["immovable_date"]."日以上動いていないもの";

        //帳票のタイトル
        $fix_value = array('タイトル'=>'不動在庫一覧');

        //一行分のデータを格納
        $row_data = array();
        foreach($Result as $index => $row){
            if($row[0]['tax_type'] == 0){
                $tax = "課";
            } else {
                $tax = "非";
            }
            if(empty($row[0]['validated_date'])){
                $validated_date = "";
            } else {
                $validated_date = date("Y/m/d",strtotime($row[0]['validated_date']));
            }

            $row_data[] = array(
                $index + 1
                ,$row[0]['internal_code']
                ,$row[0]['item_name']
                ,$row[0]['item_code']
                ,$row[0]['item_unit_name']
                ,$row[0]['facility_sticker_no']
                ,$row[0]['lot_no']
                ,$row[0]['fixed_count']
                ,$row[0]['facility_name']."／".$row[0]['dept_name1']
                ,$row[0]['code']
                ,$row[0]['standard']
                ,$row[0]['dealer_name']
                ,$tax
                ,number_format($row[0]['stocking_price'],2)
                ,$row[0]['hospital_sticker_no']
                ,$validated_date
                ,$row[0]['immovable']
                ,$row[0]['supplier_name']
                ,$search_condition
                ,date("Y/m/d")
                );
        }
        //XML出力関数呼び出し
        $this->xml_output($row_data,join("\t",$columns),$layout_name['18'],$fix_value);

    }


    /**
     * 不動在庫CSV出力
     */
    function output_immovable_csv() {
        //検索結果格納変数
        $Result = array();
        //sql
        $sql = $this->_getImmovableCSV($this->request->data, true);
        //where追加
        $sql .= " AND CASE ";
        $sql .= "       WHEN STCK.last_move_date IS NULL";
        $sql .= "        AND STCK.lot_no = ' ' ";
        $sql .= "       THEN 0 ";
        $sql .= "       ELSE date_part('day',current_timestamp - COALESCE(STCK.last_move_date,RCVG.work_date,STCK.created))";
        $sql .= "     END >= ".$this->request->data['immovable_date']." ";
        $sql .= " AND STCK.lot_no <> ' '";

        //ORDER BY
        $sql .= " ORDER BY FCLT.id,FCIT.internal_code,STCK.facility_sticker_no,STCK.hospital_sticker_no";

        $this->db_export_csv($sql , "不動在庫一覧", '/search_inquiries/immovable_list');


    }

    function _getImmovableCSV($data){
        App::import('Sanitize');
        $data = Sanitize::clean($data);
        $sql = "";
        $sql .= 'SELECT FCIT.internal_code       as "商品ID"';
        $sql .= "      ,SLNM.code                as 棚番";
        $sql .= "      ,FCIT.item_name           as 商品名";
        $sql .= "      ,FCIT.item_code           as 製品番号";
        $sql .= "      ,FCIT.standard            as 規格";
        $sql .= "      ,DELR.dealer_name         as 販売元";
        $sql .= "      ,UNNM1.unit_name          as 包装単位名";
        $sql .= "      ,ITUT.per_unit            as 入数";
        $sql .= "      ,UNNM2.unit_name          as 入数単位名";
        $sql .= "    ,( case FCIT.tax_type";
        foreach(Configure::read('PrintFacilityItems.taxes') as $k=>$v ){
            $sql .= "  when '${k}' then '${v}' ";
        }
        $sql .= '       end )                    as 課税区分';

        $sql .= "      ,RCVG.stocking_price      as 仕入単価";
        $sql .= "      ,STCK.facility_sticker_no as センターシール";
        $sql .= "      ,STCK.hospital_sticker_no as 部署シール";
        $sql .= "      ,STCK.lot_no              as ロット番号";
        $sql .= "      ,TO_CHAR(STCK.validated_date,'YYYY/MM/DD') AS 有効期限";
        $sql .= "      ,FCLT.facility_name       as 配置場所";
        $sql .= "      ,( CASE COALESCE(FXCT.is_deleted,TRUE) WHEN TRUE THEN 0 ELSE FXCT.fixed_count END ) as 定数";
        $sql .= "      ,( CASE ";
        $sql .= "         WHEN STCK.last_move_date IS NULL";
        $sql .= "          AND STCK.lot_no = ' ' ";
        $sql .= "         THEN 0 ";
        $sql .= "         ELSE date_part('day',current_timestamp - COALESCE(STCK.last_move_date,RCVG.work_date,STCK.created))";
        $sql .= "       END )                    as 不動日数";

        $sql .= "      ,DPMT1.department_name    as 配置部署";
        $sql .= "      ,FCLTY.facility_name      as 仕入先";
        $sql .= "      ,STCK.quantity            as 数量";
        $sql .= "  FROM trn_stickers STCK";
        //包装単位
        $sql .= " INNER JOIN mst_item_units ITUT";
        $sql .= "    ON STCK.mst_item_unit_id = ITUT.id";
        $sql .= "   AND ITUT.mst_facility_id = ".$data["facilityId"];
        $sql .= "   AND ITUT.is_deleted = false";
        //施設採用品
        $sql .= " INNER JOIN mst_facility_items FCIT";
        $sql .= "    ON ITUT.mst_facility_item_id = FCIT.id";
        $sql .= "   AND ITUT.is_deleted = false";
        //定数設定
        $sql .= "  LEFT JOIN mst_fixed_counts FXCT";
        $sql .= "    ON ITUT.id = FXCT.mst_item_unit_id";
        $sql .= "   AND STCK.mst_department_id = FXCT.mst_department_id";
        //棚名称
        $sql .= "  LEFT JOIN mst_shelf_names SLNM";
        $sql .= "    ON FXCT.mst_shelf_name_id = SLNM.id";
        $sql .= "   AND SLNM.is_deleted = false";
        //販売元
        $sql .= "  LEFT JOIN mst_dealers DELR";
        $sql .= "    ON FCIT.mst_dealer_id = DELR.id";
        $sql .= "   AND DELR.is_deleted = false";
        //包装単位名
        $sql .= " INNER JOIN mst_unit_names UNNM1";
        $sql .= "    ON ITUT.mst_unit_name_id = UNNM1.id";
        $sql .= "   AND UNNM1.is_deleted = false";
        //入数単位名
        $sql .= " INNER JOIN mst_unit_names UNNM2";
        $sql .= "    ON ITUT.per_unit_name_id = UNNM2.id";
        $sql .= "   AND UNNM2.is_deleted = false";
        //入荷
        $sql .= "  LEFT JOIN trn_receivings RCVG";
        $sql .= "    ON STCK.trn_receiving_id = RCVG.id";
        $sql .= "   AND RCVG.is_deleted = false";
        $sql .= "   AND RCVG.receiving_type = " . Configure::read('ReceivingType.arrival');
        //部署(配置場所)
        $sql .= "  LEFT JOIN mst_departments DPMT1";
        $sql .= "    ON STCK.mst_department_id = DPMT1.id";
        $sql .= "   AND DPMT1.is_deleted = false";
        //施設
        $sql .= "  LEFT JOIN mst_facilities FCLT";
        $sql .= "    ON DPMT1.mst_facility_id = FCLT.id";
        $sql .= "   AND FCLT.is_deleted = false";
        //ユーザ操作権限
        $sql .= '  inner join mst_user_belongings as MUB';
        $sql .= '    on MUB.mst_facility_id = FCLT.id';
        $sql .= '    and MUB.mst_user_id = ' . $this->request->data['userId'];
        $sql .= '    and MUB.is_deleted = false ';

        //部署(仕入先)
        $sql .= "  LEFT JOIN mst_departments DPMT2";
        $sql .= "    ON RCVG.department_id_from = DPMT2.id";
        $sql .= "   AND DPMT2.is_deleted = false";
        //施設(仕入先)
        $sql .= "  LEFT JOIN mst_facilities FCLTY";
        $sql .= "    ON DPMT2.mst_facility_id = FCLTY.id";
        $sql .= "   AND FCLTY.is_deleted = false";
        //WHERE句
        $sql .= " WHERE STCK.is_deleted = false";
        $sql .= "   AND STCK.quantity > 0 ";
        /*  画面からの検索条件追加  */
        //施設(完全一致)
        if(!empty($data["facility"])){
            $sql .= " AND FCLT.facility_code = '".$data["facility"]."'";
        }
        //部署(完全一致)
        if(!empty($data["department"])){
            $sql .= " AND DPMT1.department_code = '".$data["department"]."'";
        }
        //商品ID(前方一致)
        if(!empty($data["internal_code"])){
            $sql .= " AND FCIT.internal_code ILIKE '".$data["internal_code"]."%'";
        }
        //商品名(部分一致)
        if(!empty($data["item_name"])){
            $sql .= " AND FCIT.item_name ILIKE '%".$data["item_name"]."%'";
        }
        //製品番号(前方一致)
        if(!empty($data["item_code"])){
            $sql .= " AND FCIT.item_code ILIKE '".$data["item_code"]."%'";
        }
        //規格(部分一致)
        if(!empty($data["standard"])){
            $sql .= " AND FCIT.standard ILIKE '%".$data["standard"]."%'";
        }
        //販売元(部分一致)
        if(!empty($data["dealer_name"])){
            $sql .= " AND DELR.dealer_name ILIKE '%".$data["dealer_name"]."%'";
        }
        //ロットNO(完全一致)
        if(!empty($data["lot_no"])){
            $sql .= " AND STCK.lot_no = '".$data["lot_no"]."'";
        }
        //シール番号(センターシール番号または部署シール番号に完全一致)
        if(!empty($data["sticker_no"])){
            $sql .= " AND (STCK.facility_sticker_no ILIKE '".$data["sticker_no"]."'";
            $sql .= "  OR  STCK.hospital_sticker_no ILIKE '".$data["sticker_no"]."')";
        }
        //管理区分(完全一致)
        if(!empty($data["trade_type"])){
            $sql .= " AND STCK.trade_type = ".$data["trade_type"];
        }
        return $sql;
    }

    /**
     * 在庫検索用SQL生成
     */
    function createSQL_stock($data,$type=null){
        $data = Sanitize::clean($data);
        $sql  = ' select ';
        $sql .= '       c.facility_name ';
        $sql .= '     , b.department_name ';
        $sql .= '     , e.internal_code ';
        $sql .= '     , e.item_name ';
        $sql .= '     , e.item_code ';
        $sql .= '     , e.standard ';
        $sql .= '     , f.dealer_name ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when d.per_unit = 1  ';
        $sql .= '         then d1.unit_name  ';
        $sql .= "         else d1.unit_name || '(' || d.per_unit || d2.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                    as unit_name ';
        $sql .= '     , a.stock_count ';
        $sql .= '     , a.requested_count ';
        $sql .= '     , coalesce(g.order_point, 0)         as order_point ';
        $sql .= '     , coalesce(g.fixed_count, 0)         as fixed_count ';
        $sql .= '     , coalesce(g.holiday_fixed_count, 0) as holiday_fixed_count ';
        $sql .= '     , coalesce(g.spare_fixed_count, 0)   as spare_fixed_count ';
        $sql .= '     , h.name                             as shelf_name ';
        $sql .= '     , coalesce(x.ex_stock_count, 0)      as ex_stock_count ';
        $sql .= '     , coalesce(x.fixed_stock_count, 0)   as fixed_stock_count ';
        $sql .= '     ,  a.receipted_count + a.lowlevel_stock_count as no_sticker_count  ';
        $sql .= '     , ( a.stock_count - a.reserve_count - a.promise_count ) ';
        $sql .= '                                          as enable_promise_count ';
        $sql .= '     , ( case ';
        foreach(Configure::read('TeisuType.List') as $k => $v ){
            $sql .= "         when g.trade_type = '" . $k . "' then '" . $v . "'";
        }
        $sql .= '       end )                              as fixed_type_name';
        
        $sql .= '   from ';
        $sql .= '     trn_stocks as a  ';
        $sql .= '     left join mst_departments as b  ';
        $sql .= '       on b.id = a.mst_department_id  ';
        $sql .= '     left join mst_facilities as c  ';
        $sql .= '       on c.id = b.mst_facility_id  ';
        $sql .= '     left join mst_item_units as d  ';
        $sql .= '       on d.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_unit_names as d1  ';
        $sql .= '       on d1.id = d.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as d2  ';
        $sql .= '       on d2.id = d.per_unit_name_id  ';
        $sql .= '     left join mst_facility_items as e  ';
        $sql .= '       on e.id = d.mst_facility_item_id  ';
        $sql .= '     left join mst_facilities as e2 ';
        $sql .= '       on e2.id = e.mst_owner_id ';
        $sql .= '     left join mst_dealers as f  ';
        $sql .= '       on f.id = e.mst_dealer_id  ';
        $sql .= '     left join mst_fixed_counts as g  ';
        $sql .= '       on g.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '       and g.mst_department_id = a.mst_department_id  ';
        $sql .= '       and g.is_deleted = false  ';
        $sql .= '     left join mst_shelf_names as h  ';
        $sql .= '       on h.id = g.mst_shelf_name_id  ';
        $sql .= '     left join (  ';
        $sql .= '       select ';
        $sql .= '             a.mst_department_id ';
        $sql .= '           , a.mst_item_unit_id ';
        $sql .= '           , sum(  ';
        $sql .= '             case  ';
        $sql .= '               when a.trade_type = ' . Configure::read('ClassesType.Temporary');
        $sql .= '               or a.trade_type = ' . Configure::read('ClassesType.ExShipping');
        $sql .= '               or a.trade_type = ' . Configure::read('ClassesType.ExNoStock');
        $sql .= '               then a.quantity  ';
        $sql .= '               else 0  ';
        $sql .= '               end ';
        $sql .= '           )            as ex_stock_count ';
        $sql .= '           , sum(  ';
        $sql .= '             case  ';
        $sql .= '               when a.trade_type <> ' . Configure::read('ClassesType.Temporary');
        $sql .= '               and a.trade_type <> ' . Configure::read('ClassesType.ExShipping');
        $sql .= '               and a.trade_type <> ' . Configure::read('ClassesType.ExNoStock');
        $sql .= '               then a.quantity  ';
        $sql .= '               else 0  ';
        $sql .= '               end ';
        $sql .= '           )            as fixed_stock_count  ';
        $sql .= '         from ';
        $sql .= '           trn_stickers as a  ';
        $sql .= '         where ';
        $sql .= '           a.is_deleted = false  ';
        $sql .= '           and a.quantity > 0  ';
        $sql .= "           and a.lot_no != ' ' ";
        $sql .= '         group by ';
        $sql .= '           a.mst_department_id ';
        $sql .= '           , a.mst_item_unit_id ';
        $sql .= '     ) as x  ';
        $sql .= '       on x.mst_department_id = a.mst_department_id  ';
        $sql .= '       and x.mst_item_unit_id = a.mst_item_unit_id  ';
        $sql .= '   where ';
        $sql .= '     a.is_deleted = false ';
        $sql .= '     and e.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        
        /*  画面からの検索条件追加  */
        //施主(完全一致)
        if(!empty($data['ownerCode'])){
            $sql .= " and e2.facility_code = '".$data['ownerCode']."'";
        }
        //施設(完全一致)
        if(!empty($data['facilityCode'])){
            $sql .= " and c.facility_code = '".$data['facilityCode']."'";
        }
        //部署(完全一致)
        if(!empty($data['departmentCode'])){
            $sql .= " and b.department_code = '".$data['departmentCode']."'";
        }
        //商品ID(前方一致)
        if(!empty($data['internal_code'])){
            $sql .= " and e.internal_code ILIKE '".$data['internal_code']."%'";
        }
        //商品名(部分一致)
        if(!empty($data['item_name'])){
            $sql .= " and e.item_name ILIKE '%".$data['item_name']."%'";
        }
        //製品番号(前方一致)
        if(!empty($data['item_code'])){
            $sql .= " and e.item_code ILIKE '".$data['item_code']."%'";
        }
        //規格(部分一致)
        if(!empty($data['standard'])){
            $sql .= " and e.standard ILIKE '%".$data['standard']."%'";
        }
        //販売元(部分一致)
        if(!empty($data['dealer_name'])){
            $sql .= " and f.dealer_name ILIKE '%".$data['dealer_name']."%'";
        }
        //管理区分
        if(!empty($data['trade_type'])){
            $sql .= " and g.trade_type = ".$data['trade_type'] ;
        }
        //予備キー1
        if(!empty($data['spare_key1'])){
            $sql .= " and e.spare_key1 ILIKE '%".$data['spare_key1']."%'";
        }
        //予備キー2
        if(!empty($data['spare_key2'])){
            $sql .= " and e.spare_key2 ILIKE '%".$data['spare_key2']."%'";
        }
        //予備キー3
        if(!empty($data['spare_key3'])){
            $sql .= " and e.spare_key3 ILIKE '%".$data['spare_key3']."%'";
        }
        // 在庫なしは除く
        if(isset($data['all_zero'])){
            // TrnStockが全部０ AND 定数が無い。
            $sql .= ' and ( a.stock_count != 0 ';
            $sql .= ' or a.reserve_count != 0 ';
            $sql .= ' or a.promise_count != 0 ';
            $sql .= ' or a.requested_count != 0 ';
            $sql .= ' or a.receipted_count != 0 ';
            $sql .= ' or a.lowlevel_stock_count != 0 ';
            $sql .= ' or g.id is not null )';
        }
        
        //フリー検索
        if(!empty($data['Free'])){
            $sql .= " and coalesce( e.internal_code , '') ";
            $sql .= " ||  coalesce( e.item_name , '')";
            $sql .= " ||  coalesce( e.item_code  , '') ";
            $sql .= " ||  coalesce( e.standard  , '') ";
            $sql .= " ||  coalesce( e.spare_key1  , '') ";
            $sql .= " ||  coalesce( e.spare_key2  , '') ";
            $sql .= " ||  coalesce( e.spare_key3  , '') ";
            $sql .= " ilike '%" . $data['Free'] . "%'";
        }
        
        // 病院画面からの場合
        if(!is_null($type)){
            $sql .= ' and c.facility_type != ' . Configure::read('FacilityType.center');
        }
        $sql .= ' order by';
        $sql .= '       e.internal_code';
        $sql .= '      ,d.per_unit';
        $sql .= '      ,c.facility_code';
        $sql .= '      ,b.department_code';
        
        return $sql;
    }

    /**
     * 期限切れ検索&不動在庫検索用SQL生成
     * TODO CSV出力まで見据えてSQL書いたのでもしかすると修正
     * 2011/01/11 配置場所は自センター＋取引関係のある施設という検索条件を追加
     */
    function createSQL($data,$AddFieldFlg = false){
        App::import('Sanitize');
        $data = Sanitize::clean($data);
        $sql = "";
        $sql .= "SELECT FCIT.internal_code";
        $sql .= "      ,SLNM.code";
        $sql .= "      ,FCIT.item_name";
        $sql .= "      ,FCIT.item_code";
        $sql .= "      ,FCIT.standard";
        $sql .= "      ,DELR.dealer_name";
        $sql .= "      ,STCK.quantity ";
        $sql .= "      , ( case when STCK.quantity > 1 then ";
        $sql .= "          ( case when ITUT.per_unit = 1 then ";
        $sql .= "            STCK.quantity || UNNM1.unit_name ";
        $sql .= "            else ";
        $sql .= "            STCK.quantity || UNNM1.unit_name || '(' || ITUT.per_unit || UNNM2.unit_name ||  ')' ";
        $sql .= "            end )";
        $sql .= "          else ";
        $sql .= "          ( case when ITUT.per_unit = 1 then ";
        $sql .= "            UNNM1.unit_name ";
        $sql .= "            else ";
        $sql .= "            UNNM1.unit_name || '(' || ITUT.per_unit || UNNM2.unit_name || ')' ";
        $sql .= "            end )";
        $sql .= "           end ";
        $sql .= "         ) as item_unit_name";
        $sql .= "      ,UNNM1.unit_name AS unit_name";
        $sql .= "      ,ITUT.per_unit";
        $sql .= "      ,UNNM2.unit_name AS per_unit_name";
        if($AddFieldFlg){
            $sql .= "    ,( case FCIT.tax_type";
            foreach(Configure::read('PrintFacilityItems.taxes') as $k=>$v ){
                $sql .= "  when '${k}' then '${v}' ";
            }
            $sql .= ' end ) as tax_type';
        }
        $sql .= "      ,RCVG.stocking_price";
        $sql .= "      ,STCK.facility_sticker_no";
        $sql .= "      ,STCK.hospital_sticker_no";
        $sql .= "      ,STCK.lot_no";
        $sql .= "      ,TO_CHAR(STCK.validated_date,'YYYY/MM/DD') AS validated_date";
        $sql .= "      ,FCLT.facility_name";
        if(isset($data["immovable_date"]) && $data["immovable_date"] != null){
            $sql .= "      ,CASE COALESCE(FXCT.is_deleted,TRUE) WHEN TRUE THEN 0 ELSE FXCT.fixed_count END AS fixed_count";
            $sql .= "    ,CASE ";
            $sql .= "       WHEN STCK.last_move_date IS NULL";
            $sql .= "        AND STCK.lot_no = ' ' ";
            $sql .= "       THEN 0 ";
            $sql .= "       ELSE date_part('day',current_timestamp - COALESCE(STCK.last_move_date,RCVG.work_date,STCK.created))";
            $sql .= "     END AS immovable";
        }
        $sql .= "      ,DPMT1.department_name AS dept_name1";
        $sql .= "      ,FCLTY.facility_name AS supplier_name";
        $sql .= ' ,( case ';
        foreach(Configure::read('Classes') as $k => $v){
            $sql .= "  when STCK.trade_type = '" . $k . "' then '" . $v . "'";
        }
        $sql .= '   end ) as trade_type_name';
        $sql .= "  FROM trn_stickers STCK";
        //包装単位
        $sql .= " INNER JOIN mst_item_units ITUT";
        $sql .= "    ON STCK.mst_item_unit_id = ITUT.id";
        $sql .= "   AND ITUT.mst_facility_id = ".$data["facilityId"];
        $sql .= "   AND ITUT.is_deleted = false";
        //施設採用品
        $sql .= " INNER JOIN mst_facility_items FCIT";
        $sql .= "    ON ITUT.mst_facility_item_id = FCIT.id";
        $sql .= "   AND ITUT.is_deleted = false";
        //定数設定
        $sql .= "  LEFT JOIN mst_fixed_counts FXCT";
        $sql .= "    ON ITUT.id = FXCT.mst_item_unit_id";
        $sql .= "   AND STCK.mst_department_id = FXCT.mst_department_id";
        //棚名称
        $sql .= "  LEFT JOIN mst_shelf_names SLNM";
        $sql .= "    ON FXCT.mst_shelf_name_id = SLNM.id";
        $sql .= "   AND SLNM.is_deleted = false";
        //販売元
        $sql .= "  LEFT JOIN mst_dealers DELR";
        $sql .= "    ON FCIT.mst_dealer_id = DELR.id";
        $sql .= "   AND DELR.is_deleted = false";
        //包装単位名
        $sql .= " INNER JOIN mst_unit_names UNNM1";
        $sql .= "    ON ITUT.mst_unit_name_id = UNNM1.id";
        $sql .= "   AND UNNM1.is_deleted = false";
        //入数単位名
        $sql .= " INNER JOIN mst_unit_names UNNM2";
        $sql .= "    ON ITUT.per_unit_name_id = UNNM2.id";
        $sql .= "   AND UNNM2.is_deleted = false";
        //入荷
        $sql .= "  LEFT JOIN trn_receivings RCVG";
        $sql .= "    ON STCK.trn_receiving_id = RCVG.id";
        $sql .= "   AND RCVG.is_deleted = false";
        $sql .= "   AND RCVG.receiving_type = " . Configure::read('ReceivingType.arrival');
        //部署(配置場所)
        $sql .= "  LEFT JOIN mst_departments DPMT1";
        $sql .= "    ON STCK.mst_department_id = DPMT1.id";
        $sql .= "   AND DPMT1.is_deleted = false";
        //施設
        $sql .= "  LEFT JOIN mst_facilities FCLT";
        $sql .= "    ON DPMT1.mst_facility_id = FCLT.id";
        $sql .= "   AND FCLT.is_deleted = false";
        //ユーザ操作権限
        $sql .= '  inner join mst_user_belongings as MUB';
        $sql .= '    on MUB.mst_facility_id = FCLT.id';
        $sql .= '    and MUB.mst_user_id = ' . $this->request->data['userId'];
        $sql .= '    and MUB.is_deleted = false ';
        //部署(仕入先)
        $sql .= "  LEFT JOIN mst_departments DPMT2";
        $sql .= "    ON RCVG.department_id_from = DPMT2.id";
        $sql .= "   AND DPMT2.is_deleted = false";
        //施設(仕入先)
        $sql .= "  LEFT JOIN mst_facilities FCLTY";
        $sql .= "    ON DPMT2.mst_facility_id = FCLTY.id";
        $sql .= "   AND FCLTY.is_deleted = false";
        //WHERE句
        $sql .= " WHERE STCK.is_deleted = false";
        $sql .= "   AND STCK.quantity > 0 ";
        /*  画面からの検索条件追加  */
        //施設(完全一致)
        if(!empty($data['facilityCode'])){
            $sql .= " AND FCLT.facility_code = '".$data['facilityCode']."'";
        }
        //部署(完全一致)
        if(!empty($data['departmentCode'])){
            $sql .= " AND DPMT1.department_code = '".$data['departmentCode']."'";
        }
        //商品ID(前方一致)
        if(!empty($data['internal_code'])){
            $sql .= " AND FCIT.internal_code ILIKE '".$data['internal_code']."%'";
        }
        //商品名(部分一致)
        if(!empty($data['item_name'])){
            $sql .= " AND FCIT.item_name ILIKE '%".$data['item_name']."%'";
        }
        //製品番号(前方一致)
        if(!empty($data['item_code'])){
            $sql .= " AND FCIT.item_code ILIKE '".$data['item_code']."%'";
        }
        //規格(部分一致)
        if(!empty($data['standard'])){
            $sql .= " AND FCIT.standard ILIKE '%".$data['standard']."%'";
        }
        //販売元(部分一致)
        if(!empty($data['dealer_name'])){
            $sql .= " AND DELR.dealer_name ILIKE '%".$data['dealer_name']."%'";
        }
        //ロットNO(完全一致)
        if(!empty($data['lot_no'])){
            $sql .= " AND STCK.lot_no = '".$data['lot_no']."'";
        }
        //シール番号(センターシール番号または部署シール番号に完全一致)
        if(!empty($data['sticker_no'])){
            $sql .= " AND (STCK.facility_sticker_no ILIKE '".$data['sticker_no']."'";
            $sql .= "  OR  STCK.hospital_sticker_no ILIKE '".$data['sticker_no']."')";
        }
        //管理区分(完全一致)
        if(!empty($data['trade_type'])){
            $sql .= " AND STCK.trade_type = ".$data['trade_type'];
        }
        return $sql;
    }

    /**
     * 帳票出力
     */
    function report() {
        //印刷タイプで処理分岐
        if($this->request->data["print_type"] == "expiration"){
            $this->report_expiration();
        }else if($this->request->data["print_type"] == "immovable"){
            $this->report_immovable();
        }
    }


    /**
     * 帳票出力
     */
    function xml_output($data,$columns,$layout_name,$fix_value){
        $this->layout = 'xml/default';
        $this->set('columns',$columns);
        $this->set('posted',$data);
        $this->set('layout_name',$layout_name);
        $this->set('fix_value',$fix_value);

        $this->render('/orders/xml_output');
    }
}
