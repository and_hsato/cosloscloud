<?php
/**
 * StoragesHistoryController
 * 入庫履歴
 * @since 2010/09/06
 */
class StoragesHistoryController extends AppController {
    /**
    * @var $name
    */
    var $name = 'StoragesHistory';

    /**
    * @var array $uses
    */
    var $uses = array(
        'MstFacility',
        'MstClass',
        'MstTransactionConfig',
        'MstDepartment',
        'TrnReceivingHeader',
        'TrnReceiving',
        'TrnSticker',
        'TrnStickerRecord',
        'TrnStock',
        'TrnStorage',
        'TrnStorageHeader',
        'TrnStickerIssue',
    );

    /**
    * @var array $helpers
    */
    var $helpers = array('Form', 'Html', 'Time', 'DatePicker');

    /**
     * @var array
     */
    var $components = array('RequestHandler','Storage','Stickers','CsvWriteUtils');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'seal';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
    }
    
    public function index(){
        $this->setRoleFunction(25); //入庫履歴
        $this->redirect('search');
    }

    public function search(){
        if(false === $this->isSortCall){
            $this->deleteSortInfo();
        }
        //施設取得
        $this->set('facilities',
                   $this->getFacilityList(
                       $this->Session->read('Auth.facility_id_selected'),
                       array(Configure::read('FacilityType.supplier')),
                       false,
                       array('facility_code' , 'facility_name'),
                       'facility_name, facility_code'
                       )
                   );

        //発注区分
        $_OrderType = Configure::read('OrderTypes.orderTypeName');
        $this->set('orderTypes', $_OrderType);
        $this->set('classes',  $this->getClassesList($this->Session->read('Auth.facility_id_selected'),'10'));
        $_SearchResult = array();

        App::import("sanitize");
        if(isset($this->request->data['IsSearch']) && $this->request->data['IsSearch'] == '1'){
            $_conditions = array('header' => '', 'detail' => '');

            if($this->request->data['showDeleted'] == '0'){
                $_conditions['detail'] .= " and a.is_deleted = false";
                $_conditions['detail'] .= " and b.is_deleted = false";
            }

            //conditions for TrnOrder
            if($this->request->data['TrnStorageHeader']['work_no'] != ''){
                $_conditions['header'] .= " and a.work_no LIKE '%".Sanitize::escape($this->request->data['TrnStorageHeader']['work_no'])."%'";
            }
            if($this->request->data['TrnStorageHeader']['work_date_from'] != ''){
                $_conditions['header'] .= " and a.work_date >= '".Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnStorageHeader']['work_date_from'])))."'";
            }
            if($this->request->data['TrnStorageHeader']['work_date_to'] != ''){
                $_conditions['header'] .= " and a.work_date < '".Sanitize::escape(date('Y/m/d', strtotime('1 day',strtotime($this->request->data['TrnStorageHeader']['work_date_to']))))."'";
            }
            if($this->request->data['MstDepartment']['department_code'] != ''){
                $_conditions['detail'] .= " and e.department_code = '".Sanitize::escape($this->request->data['MstDepartment']['department_code'])."'";
            }
            if($this->request->data['TrnOrder']['order_type'] != ''){
                $_conditions['detail'] .= " and h.order_type = '".Sanitize::escape($this->request->data['TrnOrder']['order_type'])."'";
            }
            //conditions for TrnReceiving
            if($this->request->data['TrnReceiving']['work_type'] != ''){
                $_conditions['detail'] .= " and b.work_type = '".Sanitize::escape($this->request->data['TrnReceiving']['work_type'])."'";
            }
            //conditions for MstFacilityItem
            if($this->request->data['MstFacilityItem']['internal_code'] != ''){
                $_conditions['detail'] .= " and k.internal_code = '".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."'";
            }
            if($this->request->data['MstFacilityItem']['item_code'] != ''){
                $_conditions['detail'] .= " and k.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
            }
            if($this->request->data['MstFacilityItem']['item_name'] != ''){
                $_conditions['detail'] .= " and k.item_name LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
            }
            if($this->request->data['MstFacilityItem']['standard'] != ''){
                $_conditions['detail'] .= " and k.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
            }
            //conditions for MstDealer
            if($this->request->data['MstDealer']['dealer_name'] != '') {
                $_conditions['detail'] .= " and l.dealer_name LIKE '%".Sanitize::escape($this->request->data['MstDealer']['dealer_name'])."%'";
            }
            //facility
            if($this->request->data['MstFacility']['facility_code'] != ''){
                $_conditions['detail'] .= " and f.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['facility_code'])."'";
            }
            if($this->request->data['TrnSticker']['lot_no'] != ''){
                $_conditions['detail'] .= " and i.lot_no LIKE '%".Sanitize::escape($this->request->data['TrnSticker']['lot_no'])."%'";
            }
            if($this->request->data['TrnSticker']['facility_sticker_no'] != ''){
                $_conditions['detail'] .= " and (i.facility_sticker_no ILIKE '%".Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."%'";
                $_conditions['detail'] .= "  or  b.facility_sticker_no ILIKE '%".Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."%')";
            }
            $_limit = $this->_getLimitCount();
            //表示データ取得
            $_SearchResult = $this->TrnStorage->query($this->getSearchStorageHeaderSQL($_conditions,$_limit));
        }else{
            $this->request->data['TrnStorageHeader']['work_date_from'] = date('Y/m/d', strtotime('-7 day', time()));
            $this->request->data['TrnStorageHeader']['work_date_to'] = date('Y/m/d', time());
        }
        $_SearchResult = AppController::outputFilter($_SearchResult);
        $this->set('data', $this->request->data);
        $this->set('SearchResult', $_SearchResult);
    }

    public function confirm(){
        //表示対象取得
        $_conditions = array();
        foreach($this->request->data['TrnStorageHeader']['Rows'] as $_key => $_val){
            if($_val['selected'] == '1'){
                $_conditions['TrnStorageHeader'][] = $_key;
            }
        }

        App::import("sanitize");
        $where = "";
        if($this->request->data['TrnStorageHeader']['work_no'] != ''){
            $where .= " AND a.work_no ILIKE '%".Sanitize::escape($this->request->data['TrnStorageHeader']['work_no'])."%'";
        }
        if($this->request->data['TrnStorageHeader']['work_date_from'] != ''){
            $where .= " AND a.work_date >= '".Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnStorageHeader']['work_date_from'])))."'";
        }
        if($this->request->data['TrnStorageHeader']['work_date_to'] != ''){
                $where .= " AND a.work_date < '".Sanitize::escape(date('Y/m/d', strtotime('1 day',strtotime($this->request->data['TrnStorageHeader']['work_date_to']))))."'";
        }
        if($this->request->data['MstDepartment']['department_code'] != ''){
            $where .= " AND i.department_code = '".Sanitize::escape($this->request->data['MstDepartment']['department_code'])."'";
        }
        if($this->request->data['TrnOrder']['order_type'] != ''){
            $where .= " AND c.order_type = '".Sanitize::escape($this->request->data['TrnOrder']['order_type'])."'";
        }
        if($this->request->data['TrnReceiving']['work_type'] != ''){
            $where .= " AND a.work_type = '".Sanitize::escape($this->request->data['TrnReceiving']['work_type'])."'";
        }
        if($this->request->data['MstFacilityItem']['internal_code'] != ''){
            $where .= " AND f.internal_code = '".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."'";
        }
        if($this->request->data['MstFacilityItem']['item_code'] != ''){
            $where .= " AND f.item_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        if($this->request->data['MstFacilityItem']['item_name'] != ''){
            $where .= " AND f.item_name LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
        }
        if($this->request->data['MstFacilityItem']['standard'] != ''){
            $where .= " AND f.standard ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }
        if($this->request->data['MstDealer']['dealer_name'] != '') {
            $where .= " AND g.dealer_name ILIKE '%".Sanitize::escape($this->request->data['MstDealer']['dealer_name'])."%'";
        }
        if($this->request->data['MstFacility']['facility_code'] != ''){
            $where .= " AND j.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['facility_code'])."'";
        }
        if($this->request->data['TrnSticker']['lot_no'] != ''){
            $where .= " AND d.lot_no ILIKE '%".Sanitize::escape($this->request->data['TrnSticker']['lot_no'])."%'";
        }
        if($this->request->data['TrnSticker']['facility_sticker_no'] != ''){
            $where .= " and (d.facility_sticker_no ILIKE '%".Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."%'";
            $where .= "  or  a.facility_sticker_no ILIKE '%".Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."%')";
        }

        $_res = $this->Storage->getStorages($_conditions, true, $where);
        $this->set('SearchResult', $this->outputFilter($_res));
        $fid = $this->Session->read('Auth.facility_id_selected');
        $selects = $this->Session->read('Auth.facilities');
        //倉庫部署id取得
        $_params = array();
        $_params['conditions'] = array('MstDepartment.mst_facility_id'=>$fid,
                                       'MstDepartment.is_deleted'=>FALSE);
        $_params['recursive'] = -1;
        $_params['fields'] = array('MstDepartment.id');
        $dep = $this->MstDepartment->find('first',$_params);
        $this->set('facility_id', $fid);
        $this->set('department_id', $dep['MstDepartment']['id']);
        $this->set('facility_name', $selects[$fid]);
    }



    public function seal($dummy = null){
        //印刷対象選択
        $_fno = array();
        foreach($this->request->data['TrnSticker']['Rows'] as $_key => $_val){
            if(false !== strstr($_val['selected'], '1')){
                $_fno[] = $_key;
            }
        }

        $fid = $this->request->data['facility_id'];
        $fname = $this->getFacilityName($fid);
        $stickers = $this->Stickers->getFacilityStickers($_fno, $fname, $fid);

        $this->layout = 'xml/default';
        $this->set('stickers', $stickers);
        $this->render('/storages/seal');
    }

    public function cancel(){
        $now = date('Y/m/d H:i:s');
        //選択対象取得
        $cond = array();
        $cancelQuantity = array();
        $hasError = false;
        foreach($this->request->data['TrnStorageHeader']['Rows'] as $_key => $_val){
            if($_val['selected'] == '1' && false === $hasError){
                //削除できるか確認
                //明細の数量*入数の合計が入荷の包装単位の入数の倍数と等しいか？
                if(false === $this->canCancel($_key)){
                    $hasError = true;
                }else{
                    $cancelQuantity[$_key] = $this->getCancelQuqntity($_key);
                }
                //取消対象取得
                $cond['TrnStorageHeader'][] = $_key;
            }
        }

        if(true === $hasError){
            $this->Session->setFlash("入庫取消処理に失敗しました。", 'growl', array('type'=>'error'));
            $this->set('IsSearch', '1');
            $this->index();
            return;
        }

        //明細取得
        $storages = $this->Storage->getStorages($cond);

        //トランザクション
        $this->TrnStorageHeader->begin();

        //行ロック
        $sql  = ' select ';
        $sql .= '       *  ';
        $sql .= '   from ';
        $sql .= '     trn_storage_headers as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',' ,  $cond['TrnStorageHeader'] ). ') for update ';
        $ret = $this->TrnStorage->query($sql , false);

        //更新時間チェック
        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     trn_storage_headers as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',' ,  $cond['TrnStorageHeader'] ). ')';
        $sql .= "     and a.modified > '" . $this->request->data['TrnStorage']['time'] ."'";
        $ret = $this->TrnStorage->query($sql , false);

        if($ret[0][0]['count']> 0 ){
            $this->TrnStorage->rollback();
            throw new Exception('更新時間チェックエラー');
            exit;
        }

        foreach($cond['TrnStorageHeader'] as $_val){
            //入庫情報取得
            $this->TrnStorage->create();
            $this->TrnStorage->unbindModel(
                array(
                    "belongsTo"=>array("TrnStorageHeader",'MstItemUnit','MstUser','TrnSticker','MstClass','MstDepartment')
                )
                ,false
            );
            $options = array();
            $options['fields'] = array('DISTINCT TrnStorage.trn_receiving_id',"TrnStorage.trn_storage_header_id");
            $options['conditions'][] = array("TrnStorage.trn_storage_header_id"=>$_val);
            $TrnStorageRec = $this->TrnStorage->find('all',$options);

            //取得した入庫情報を基に入荷明細情報更新
            foreach ($TrnStorageRec as $_rec){
                $this->TrnReceiving->create();
                $this->TrnReceiving->unbindModel(
                  array(
                    "hasOne"=>array('TrnClaim'),
                    "belongsTo"=>array('TrnOrder','MstItemUnit','MstClass','MstDepartment','MstUser')
                  )
                  ,false
                );
                $options = array();
                $options["conditions"][] = array("TrnReceiving.id"=>$_rec["TrnStorage"]["trn_receiving_id"]);
                $TrnReceivingRec = $this->TrnReceiving->find("all",$options);
                $this->TrnReceiving->updateAll(array(
                    'TrnReceiving.remain_count'     => (int)$TrnReceivingRec[0]["TrnReceiving"]["remain_count"] + (int)$cancelQuantity[ $_rec["TrnStorage"]["trn_storage_header_id"] ],
                    'TrnReceiving.receiving_status' => sprintf('(case when TrnReceiving.remain_count = TrnReceiving.quantity - %s then %s else %s end)',$cancelQuantity[$_rec["TrnStorage"]["trn_storage_header_id"]], Configure::read('ReceivingStatus.arrival'),Configure::read('ReceivingStatus.partStocked')),
                    'TrnReceiving.modifier'         => $this->Session->read('Auth.MstUser.id'),
                    'TrnReceiving.modified'         => "'".$now."'"
                ),array(
                    'TrnReceiving.id' => $_rec['TrnStorage']["trn_receiving_id"]
                ),-1);
            }

            //入庫ヘッダ削除
            $this->TrnStorageHeader->create();
            $this->TrnStorageHeader->updateAll(array(
                'TrnStorageHeader.is_deleted' => 'true',
                'TrnStorageHeader.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnStorageHeader.modified'   => "'".$now."'",
            ),array(
                'TrnStorageHeader.id' => $_val,
            ),-1);

            //在庫テーブル更新
            //未入庫数△
            $sql  = ' update trn_stocks xx  ';
            $sql .= '   set ';
            $sql .= '     receipted_count = receipted_count + yy.count  ';
            $sql .= '   , modifier        = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= "   , modified        = '" . $now . "'";
            $sql .= '   from ';
            $sql .= '     (  ';
            $sql .= '       select ';
            $sql .= '             c.id ';
            $sql .= '           , (x.quantity / b.per_unit) as count  ';
            $sql .= '         from ';
            $sql .= '           (  ';
            $sql .= '             select ';
            $sql .= '                   a.trn_receiving_id ';
            $sql .= '                 , sum(a.quantity * b.per_unit) as quantity  ';
            $sql .= '               from ';
            $sql .= '                 trn_storages as a  ';
            $sql .= '                 left join mst_item_units as b  ';
            $sql .= '                   on b.id = a.mst_item_unit_id  ';
            $sql .= '               where ';
            $sql .= '                 a.trn_storage_header_id = ' . $_val;
            $sql .= '               group by ';
            $sql .= '                 a.trn_receiving_id ';
            $sql .= '           ) as x  ';
            $sql .= '           left join trn_receivings as a  ';
            $sql .= '             on a.id = x.trn_receiving_id  ';
            $sql .= '           left join mst_item_units as b  ';
            $sql .= '             on b.id = a.mst_item_unit_id  ';
            $sql .= '           left join trn_stocks as c  ';
            $sql .= '             on c.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '             and c.mst_department_id = a.department_id_to ';
            $sql .= '     ) as yy  ';
            $sql .= '   where ';
            $sql .= '     xx.id = yy.id ';

            $this->TrnStock->query($sql);

        }

        foreach($storages as $_row){
            //入庫明細削除
            $this->TrnStorage->updateAll(array(
                'TrnStorage.is_deleted' => 'true',
                'TrnStorage.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnStorage.modified'   => "'".$now."'",
            ),array(
                'TrnStorage.id' => $_row['id'],
            ),-1);

            //シール削除
            $this->TrnSticker->updateAll(array(
                'TrnSticker.is_deleted' => 'true',
                'TrnSticker.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnSticker.modified'   => "'".$now."'",
            ),array(
                'TrnStorage.id' => $_row['sticker_id'],
            ),-1);

            //シール移動履歴削除
            $c = array(
                Configure::read('RecordType.order'),
                Configure::read('RecordType.receiving'),
                Configure::read('RecordType.storage'),
            );
            $this->TrnStickerRecord->updateAll(array(
                'TrnStickerRecord.is_deleted' => 'true',
                'TrnStickerRecord.modifier'   => $this->Session->read('Auth.MstUser.id'),
                'TrnStickerRecord.modified'   => "'".$now."'",
            ),array(
                'TrnStickerRecord.record_id' => $_row['id'],
                'TrnStickerRecord.record_type in ('. join(',',$c).')',
            ),-1);

            //在庫テーブル更新
            //在庫数▽
            $this->TrnStock->updateAll(array(
                'TrnStock.stock_count' => 'TrnStock.stock_count - 1',
                'TrnStock.modifier'    => $this->Session->read('Auth.MstUser.id'),
                'TrnStock.modified'    => "'".$now."'",
            ),array(
                'TrnStock.mst_item_unit_id'  => $_row['mst_item_unit_id'],
                'TrnStock.mst_department_id' => $_row['mst_department_id'],
            ),-1);

        }

        //入荷済み未入庫のシール更新
        $sql  = ' update trn_stickers x ';
        $sql .= '   set';
        $sql .= '    quantity = quantity + y.cancel_count ';
        $sql .= '   ,modifier = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= "   ,modified = '" . $now . "'";
        $sql .= '  from';
        $sql .= '    ( ';
        $sql .= '      select';
        $sql .= '            d.department_id_to';
        $sql .= '          , d.mst_item_unit_id';
        $sql .= '          , sum(c.per_unit * b.quantity) / e.per_unit as cancel_count ';
        $sql .= '        from';
        $sql .= '          trn_storage_headers as a ';
        $sql .= '          left join trn_storages as b ';
        $sql .= '            on b.trn_storage_header_id = a.id ';
        $sql .= '          left join mst_item_units as c ';
        $sql .= '            on c.id = b.mst_item_unit_id ';
        $sql .= '          left join trn_receivings as d ';
        $sql .= '            on d.id = b.trn_receiving_id ';
        $sql .= '          left join mst_item_units as e ';
        $sql .= '            on e.id = d.mst_item_unit_id ';
        $sql .= '        where';
        $sql .= '          a.id in (' . join(',' , $cond['TrnStorageHeader'] ) . ') ';
        $sql .= '        group by';
        $sql .= '          d.mst_item_unit_id';
        $sql .= '          , e.per_unit';
        $sql .= '          , d.department_id_to';
        $sql .= '    ) y ';
        $sql .= '  where';
        $sql .= '    x.mst_item_unit_id = y.mst_item_unit_id ';
        $sql .= '    and x.mst_department_id = y.department_id_to ';
        $sql .= "    and x.lot_no = ' ' ";
        $sql .= '    and x.is_deleted = false ';

        $this->TrnSticker->query($sql);

        //入荷ヘッダ更新
        $cnt = $this->TrnReceiving->find('count', array(
            'recursive' => -1,
            'conditions' => array(
                'TrnReceiving.receiving_status' => Configure::read('ReceivingStatus.partStocked'),
                'TrnReceiving.trn_receiving_header_id' => $storages[0]['trn_receiving_header_id'],
            ),
        ));

        if($cnt === 0){
            $this->TrnReceivingHeader->updateAll(array(
                'TrnReceivingHeader.receiving_status' => Configure::read('ReceivingStatus.arrival'),
                'TrnReceivingHeader.modifier' => $this->Session->read('Auth.MstUser.id'),
                'TrnReceivingHeader.modified' => "'".$now."'",
            ),array(
                'TrnReceivingHeader.id' => $storages[0]['trn_receiving_header_id'],
            ),-1);
        }else{
            $this->TrnReceivingHeader->updateAll(array(
                'TrnReceivingHeader.receiving_status' => Configure::read('ReceivingStatus.partStocked'),
                'TrnReceivingHeader.modifier' => $this->Session->read('Auth.MstUser.id'),
                'TrnReceivingHeader.modified' => "'".$now."'",
            ),array(
                'TrnReceivingHeader.id' => $storages[0]['trn_receiving_header_id'],
                'TrnReceivingHeader.receiving_status <> ' => Configure::read('ReceivingStatus.partStocked'),
            ),-1);
        }

        $errors = array();
        $errors = array_merge(
                $errors,
                is_array($this->validateErrors($this->TrnReceiving)) ? $this->validateErrors($this->TrnReceiving) : array(),
                is_array($this->validateErrors($this->TrnReceivingHeader)) ? $this->validateErrors($this->TrnReceivingHeader) : array(),
                is_array($this->validateErrors($this->TrnStorage)) ? $this->validateErrors($this->TrnStorage) : array(),
                is_array($this->validateErrors($this->TrnStorageHeader)) ? $this->validateErrors($this->TrnStorageHeader) : array(),
                is_array($this->validateErrors($this->TrnSticker)) ? $this->validateErrors($this->TrnSticker) : array(),
                is_array($this->validateErrors($this->TrnStickerRecord)) ? $this->validateErrors($this->TrnStickerRecord) : array(),
                is_array($this->validateErrors($this->TrnStock)) ? $this->validateErrors($this->TrnStock) : array()
        );

        //コミット直前に更新チェック
        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     trn_storage_headers as a  ';
        $sql .= '   where ';
        $sql .= '     a.id in (' . join(',' ,  $cond['TrnStorageHeader'] ). ')';
        $sql .= "     and a.modified > '" . $this->request->data['TrnStorage']['time'] ."'";
        $sql .= "     and a.modified <> '" . $now ."'";
        $ret = $this->TrnStorage->query($sql , false);

        if($ret[0][0]['count']> 0 ){
            $this->TrnStorage->rollback();
            throw new Exception('更新時間チェックエラー');
            exit;
        }

        if(count($errors) > 0){
            $this->TrnStorageHeader->rollback();
            throw new Exception(print_r($errors,true));
            exit;
        }else{
            $this->TrnStorageHeader->commit();
        }
        $this->Session->setFlash("入庫取消を行いました。", 'growl', array('type'=>'star'));
        $this->set('SearchResult', $this->outputFilter($storages));
        $this->render('results');
    }

    /**
     * 入庫が取消可能かどうか
     * @param string $id 入庫ヘッダのkey
     * @access private
     * @return boolean
     */
    private function canCancel($id){
        $sql =
            'select ' .
                ' a.trn_storage_header_id' .
                ',a.mst_item_unit_id' .
                ',a.storage_type'.
                ',c.per_unit' .
                ',a.is_deleted'.
                ',coalesce(d.total,0) as total' .
                ',coalesce(d.detail_count,0) as detail_count'.
                ',coalesce(d.rec_count,0) as rec_count'.
            ' from ' .
                ' trn_storages a ' .
                ' inner join trn_receivings b on a.trn_receiving_id = b.id' .
                ' inner join mst_item_units c on b.mst_item_unit_id = c.id' .
                ' left join (' .
                    ' select ' .
                        ' sum(b.quantity * c.per_unit) as total' .
                        ',a.trn_storage_header_id' .
                        ',d.detail_count '.
                        ',count(*) as rec_count'.
                    ' from ' .
                        ' trn_storages a' .
                        ' inner join trn_stickers b on a.trn_sticker_id = b.id' .
                        ' inner join mst_item_units c on b.mst_item_unit_id = c.id' .
                        ' inner join trn_storage_headers d on a.trn_storage_header_id = d.id' .
                    ' where ' .
                            ' b.is_deleted = false ' .
                        ' and coalesce(b.has_reserved,false) = false ' .
                        " and b.hospital_sticker_no = ''" .
                        " and b.quantity <> 0" .
                    ' group by ' .
                        'a.trn_storage_header_id' .
                        ',d.detail_count '.
                ') d on a.trn_storage_header_id = d.trn_storage_header_id' .
            ' where ' .
                " a.trn_storage_header_id = {$id} " .
            ' group by ' .
                 'a.trn_storage_header_id' .
                ',c.per_unit' .
                ',d.total' .
                ',a.is_deleted'.
                ',a.storage_type'.
                ',a.mst_item_unit_id'.
                ',detail_count '.
                ',rec_count';
        $res = $this->TrnStorageHeader->query($sql);
        foreach($res as $_row){
            if((integer)$_row[0]['total'] === 0){
                return false;
            }
            if($_row[0]['is_deleted'] == true){
                return false;
            }

            if((integer)$_row[0]['total'] % (integer)$_row[0]['per_unit'] !== 0){
                return false;
            }
            //直納品はダメ
            if($_row[0]['storage_type'] != Configure::read('StorageType.normal')){
                return false;
            }
            //ヘッダの明細数とSQLで取得できたレコード数が違う場合、取り消せない
            if($_row[0]["detail_count"] != $_row[0]["rec_count"]){
                return false;
            }
        }
        return true;
    }

    private function getCancelQuqntity($id){
        $sql =
            'select ' .
                ' a.trn_storage_header_id' .
                ',a.mst_item_unit_id' .
                ',c.per_unit' .
                ',coalesce(d.total,0) as total' .
            ' from ' .
                ' trn_storages a ' .
                ' inner join trn_receivings b on a.trn_receiving_id = b.id' .
                ' inner join mst_item_units c on b.mst_item_unit_id = c.id' .
                ' left join (' .
                    ' select ' .
                        ' sum(b.quantity * c.per_unit) as total' .
                        ',a.trn_storage_header_id' .
                    ' from ' .
                        ' trn_storages a' .
                        ' inner join trn_stickers b on a.trn_sticker_id = b.id' .
                        ' inner join mst_item_units c on b.mst_item_unit_id = c.id' .
                    ' where ' .
                            ' b.is_deleted = false ' .
                        ' and coalesce(b.has_reserved,false) = false ' .
                        " and b.hospital_sticker_no = ''" .
                    ' group by ' .
                        'a.trn_storage_header_id' .
                ') d on a.trn_storage_header_id = d.trn_storage_header_id' .
            ' where ' .
                " a.trn_storage_header_id = {$id} " .
            ' group by ' .
                'a.trn_storage_header_id' .
                ',c.per_unit' .
                ',d.total' .
                ',a.mst_item_unit_id';
        $res = $this->TrnStorageHeader->query($sql);
        foreach($res as $_row){
            return ((integer)$_row[0]['total'] / (integer)$_row[0]['per_unit']);
        }
    }

    private function getSearchStorageHeaderSQL($conditions, $limit){
        $sql =
            ' select ' .
                'a.id'.
                ',a.work_no'.
                ',a.storage_type'.
                ",to_char(a.work_date , 'YYYY/mm/dd') as work_date".
                ',a.detail_count'.
                ',a.recital'.
                ",to_char(a.created , 'YYYY/mm/dd HH24:MI:ss') as created".
                ',c.user_name as creater_name'.
                ',f.facility_name'.
                ',count(*) as real_detail_count'.
            ' from '.
                ' trn_storage_headers a'.
                    ' inner join trn_storages b on'.
                        ' a.id = b.trn_storage_header_id '.
                    ' inner join mst_users c on'.
                        ' a.creater = c.id'.
                    ' inner join trn_receivings g on'.
                        ' b.trn_receiving_id = g.id'.
                    ' inner join mst_departments e on'.
                        ' g.department_id_from = e.id'.
                    ' inner join mst_facilities f on'.
                        ' e.mst_facility_id = f.id'.
                    ' left join trn_orders h on'.
                        ' g.trn_order_id = h.id'.
                    ' inner join trn_stickers i on'.
                        ' b.trn_sticker_id = i.id'.
                    ' inner join mst_item_units j on'.
                        ' b.mst_item_unit_id = j.id'.
                    ' inner join mst_facility_items k on'.
                        ' j.mst_facility_item_id = k.id'.
                    ' left join mst_dealers l on'.
                        ' k.mst_dealer_id = l.id'.
                    ' inner join mst_unit_names m on'.
                        ' j.per_unit_name_id = m.id'.
                    ' inner join mst_departments o on'.
                        ' b.mst_department_id = o.id'.
                    ' inner join mst_facilities p on'.
                        ' o.mst_facility_id = p.id'.
                    ' inner join mst_users q on'.
                        ' b.modifier = q.id'.
                    ' left join mst_classes s on'.
                        ' b.work_type = s.id'.
            ' where ' .
                    ' 1 = 1 ' .
                    ' and p.id = ' . $this->Session->read('Auth.facility_id_selected') .
                    " and i.trn_return_receiving_id IS NULL".
                    " and a.storage_type <> ". Configure::read('StorageType.direct') . // 直納品を除く
                    " and i.trade_type <> 3 ".    // 預託品を除く
                ($this->isNNs($conditions['header']) ? '' : $conditions['header']) .
                ($this->isNNs($conditions['detail']) ? '' : $conditions['detail']) .
            ' group by a.id,a.work_no,a.work_date,a.storage_type,a.detail_count,a.recital,a.created,c.user_name,f.facility_name,a.detail_count ';
        if($this->getSortOrder() === null){
            $sql .= ' order by a.id ASC';
        }else{
            $arrayOrder = $this->getSortOrder();
            $sql .= ' order by '.$arrayOrder[0];
        }
        //全件取得
        $this->set('max' , $this->getMaxCount($sql , 'TrnStorageHeader'));

        if((integer)$limit > 0){
            $sql .= " limit {$limit} ";
        }
        return $sql;
    }

    /**
     * 入庫明細条件取得
     */
    private function _getStoragesDetailWhere(){

        $where = "";
        if($this->request->data['TrnStorageHeader']['work_no'] != ''){
            $where .= " AND a.work_no ILIKE '%".Sanitize::escape($this->request->data['TrnStorageHeader']['work_no'])."%'";
        }
        if($this->request->data['TrnStorageHeader']['work_date_from'] != ''){
            $where .= " AND a.work_date >= '".Sanitize::escape(date('Y/m/d', strtotime($this->request->data['TrnStorageHeader']['work_date_from'])))."'";
        }
        if($this->request->data['TrnStorageHeader']['work_date_to'] != ''){
            $where .= " AND a.work_date < '".Sanitize::escape(date('Y/m/d', strtotime('1 day',strtotime($this->request->data['TrnStorageHeader']['work_date_to']))))."'";
        }
        if($this->request->data['MstDepartment']['department_code'] != ''){
            $where .= " AND i.department_code = '".Sanitize::escape($this->request->data['MstDepartment']['department_code'])."'";
        }
        if($this->request->data['TrnOrder']['order_type'] != ''){
            $where .= " AND c.order_type = '".Sanitize::escape($this->request->data['TrnOrder']['order_type'])."'";
        }
        if($this->request->data['TrnReceiving']['work_type'] != ''){
            $where .= " AND a.work_type = '".Sanitize::escape($this->request->data['TrnReceiving']['work_type'])."'";
        }
        if($this->request->data['MstFacilityItem']['internal_code'] != ''){
            $where .= " AND f.internal_code = '".Sanitize::escape($this->request->data['MstFacilityItem']['internal_code'])."'";
        }
        if($this->request->data['MstFacilityItem']['item_code'] != ''){
            $where .= " AND f.item_code ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        if($this->request->data['MstFacilityItem']['item_name'] != ''){
            $where .= " AND f.item_name LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_name'])."%'";
        }
        if($this->request->data['MstFacilityItem']['standard'] != ''){
            $where .= " AND f.standard ILIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }
        if($this->request->data['MstDealer']['dealer_name'] != '') {
            $where .= " AND g.dealer_name ILIKE '%".Sanitize::escape($this->request->data['MstDealer']['dealer_name'])."%'";
        }
        if($this->request->data['MstFacility']['facility_code'] != ''){
            $where .= " AND j.facility_code = '".Sanitize::escape($this->request->data['MstFacility']['facility_code'])."'";
        }
        if($this->request->data['TrnSticker']['lot_no'] != ''){
            $where .= " AND d.lot_no ILIKE '%".Sanitize::escape($this->request->data['TrnSticker']['lot_no'])."%'";
        }
        if($this->request->data['TrnSticker']['facility_sticker_no'] != ''){
            $where .= " and (d.facility_sticker_no ILIKE '%".Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."%'";
            $where .= "  or  a.facility_sticker_no ILIKE '%".Sanitize::escape($this->request->data['TrnSticker']['facility_sticker_no'])."%')";
        }

        return $where;
    }


    /**
     * 入庫履歴CSV出力
     */
    function export_csv() {

        $where = $this->_getStoragesDetailWhere();

        //一覧固有で指定している条件を追加
        $where .= ' and r.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $where .= " and d.trn_return_receiving_id IS NULL";

        $sql = $this->_getStoragesCSV(array(), $this->request->data['showDeleted'], $where);

        $this->db_export_csv($sql , "入庫履歴", 'search');
    }

    /**
     * 検索処理
     *
     * @param $conditions
     * @access private
     * @return array
     */
    private function _getStoragesCSV($conditions, $showDeleted = false, $where = ""){
        $sql  = ' select ';
        $sql .= '     a.work_no           as 入庫番号';
        $sql .= "    ,to_char(a.work_date, 'YYYY/mm/dd') as 入庫日";
        $sql .= '    ,j.facility_name     as 仕入先名';
        $sql .= '    ,f.internal_code     as 商品ID';
        $sql .= '    ,f.item_name         as 商品名';
        $sql .= '    ,f.standard          as 規格';
        $sql .= '    ,f.item_code         as 製品番号';
        $sql .= '    ,g.dealer_name       as 販売元名';

        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when e.per_unit = 1  ';
        $sql .= '         then h.unit_name  ';
        $sql .= "         else h.unit_name || '(' || e.per_unit || l.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                   as 包装単位名 ';
        
        $sql .= '    ,d.transaction_price as 仕入単価';
        $sql .= '    ,d.lot_no            as ロット番号';
        $sql .= "    ,to_char(d.validated_date,'YYYY/mm/dd') as 有効期限";
        $sql .= '    ,m.name              as 作業区分';
        $sql .= '    ,COALESCE(a.facility_sticker_no,d.facility_sticker_no) ';
        $sql .= '                         as センターシール';
        $sql .= '    ,k.user_name         as 更新者名';
        $sql .= '    ,a.recital           as 備考';
        
        $sql .= ' from ';
        $sql .= '     trn_storage_headers as q ';
        $sql .= '     inner join trn_storages a on';
        $sql .= '     q.id = a.trn_storage_header_id ';
        $sql .= '     inner join trn_receivings b on';
        $sql .= '     a.trn_receiving_id = b.id';
        $sql .= '     left join trn_orders c on';
        $sql .= '     b.trn_order_id = c.id';
        $sql .= '     inner join trn_stickers d on';
        $sql .= '     a.trn_sticker_id = d.id';
        $sql .= '     inner join mst_item_units e on';
        $sql .= '     a.mst_item_unit_id = e.id';
        $sql .= '     inner join mst_facility_items f on';
        $sql .= '     e.mst_facility_item_id = f.id';
        $sql .= '     left join mst_dealers g on';
        $sql .= '     f.mst_dealer_id = g.id';
        $sql .= '     inner join mst_unit_names h on';
        $sql .= '     e.mst_unit_name_id = h.id';
        $sql .= '     inner join mst_departments i on';
        $sql .= '     b.department_id_from = i.id';
        $sql .= '     inner join mst_facilities j on';
        $sql .= '     i.mst_facility_id = j.id';
        $sql .= '     inner join mst_users k on';
        $sql .= '     a.modifier = k.id';
        $sql .= '     inner join mst_unit_names l on';
        $sql .= '     e.per_unit_name_id = l.id ';
        $sql .= '     left join mst_classes m on ';
        $sql .= '     a.work_type = m.id ';
        $sql .= '     left join mst_fixed_counts n on';
        $sql .= '     a.mst_item_unit_id = n.mst_item_unit_id and a.mst_department_id = n.mst_department_id ';
        $sql .= '     left join mst_shelf_names o on';
        $sql .= '     n.mst_shelf_name_id = o.id ';
        $sql .= '     left join mst_consts p on';
        $sql .= '     f.insurance_claim_type::varchar = p.const_cd ';
        $sql .= "     and p.const_group_cd='" . Configure::read('ConstGroupType.insuranceClaimType') . "'";
        $sql .= '     inner join mst_departments r on';
        $sql .= '     a.mst_department_id = r.id ';
        $sql .= ' where ';
        $sql .= '     1 = 1 ';
        $sql .= '     and d.trn_return_receiving_id IS NULL ';
        $sql .= "     and a.storage_type <> " . Configure::read('StorageType.direct');

        $sql .= $where;

        if ($showDeleted == '0') {
            $sql .= "     and a.is_deleted = false ";
            $sql .= "     and q.is_deleted = false ";
        }

        $sql .= ' order by a.id asc ';
        return $sql;
    }
}
?>
