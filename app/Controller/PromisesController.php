<?php
/**
 * PromisesController
 *　引当
 * @version 1.0.0
 * @since 2010/05/18
 */
class PromisesController extends AppController {

    /**
     * @var $name
     */
    var $name = 'Promises';

    /**
     * @var array $uses
     */
    var $uses = array(
        'MstUnitName',
        'MstItemUnit',
        'MstFacilityRelation',
        'MstFacilityItem',
        'TrnPromise',
        'TrnPromiseHeader',
        'TrnStock',
        'MstUser',
        'TrnConsume',
        'MstFacility',
        'MstDepartment',
        'MstFixedCount',
        'TrnFixedPromise',
        'TrnFixedPromiseHeader',
        'MstClass',
        'MstUserBelonging',
        'MstDealer'
        );
    
    /**
     * @var array $helpers
    */
    var $helpers = array('Form', 'Html', 'Time','common');

    /**
     * @var array $components
    */
    var $components = array('xml','Common','RequestHandler','CsvWriteUtils','CsvReadUtils');

    /**
     * @var array _fixedNum
     * 使用定数
    */
    var $_fixedNum = array();

    /**
     * @var array _work_type
     * 作業区分
    */
    var $_work_type = array();

    /**
     *
     * @var array errors
    */
    var $errors;

    //ログインなしでの印刷を許可
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allowedActions[] = 'report';
        //CSV出力方式の切替
        if($this->Session->read('Auth.Config.ExportCsv')=='0'){
            $this->CsvWriteUtils->setDelimiter("\t");
            $this->CsvWriteUtils->setQuote("");
        }
        //CSV入力方式の切替
        if($this->Session->read('Auth.Config.ImportCsv')=='0'){
            $this->CsvReadUtils->setDelimiter("\t");
            $this->CsvReadUtils->setQuote("");
        }
    }

    /**
     *
     */
    function index (){
        $this->add();
    }

    /**
     * 要求作成条件指定
     * @access public
     */
    function add() {
        $this->setRoleFunction(4); //引当要求作成
        $this->set('workType',$this->getClassesList($this->Session->read('Auth.facility_id_selected'), 3)); //作業区分
        $this->set('fixedNum',Configure::read('FixedType.List'));
        $this->request->data['promise']['work_date']  = date('Y/m/d');

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnPromise.token' , $token);
        $this->request->data['TrnPromise']['token'] = $token;
    }

    /**
     * 施設/部署選択画面
     * @access public
     */
    function select () {
        $searchResult = array();

        $workType = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),3);
        $fixedNum = Configure::read('FixedType.List');
        //初期値
        if(empty($this->request->data['promise']['work_type']) ){
            $this->request->data['promise']['work_type_name'] = NULL;
        }else{
            $this->request->data['promise']['work_type_name'] = $workType[$this->request->data['promise']['work_type']];
        }
        $this->request->data['promise']['fixed_num_name'] = $fixedNum[$this->request->data['promise']['fixed_num']];
        //得意先取得
        $this->set('selectIds',
                $this->getFacilityList(
                        $this->Session->read('Auth.facility_id_selected'),
                        array(Configure::read('FacilityType.hospital')),
                        true,
                        array('id' , 'facility_name')
                )
        );

        //検索
        if(isset($this->request->data['promise']['is_search'])) {
            $sql  = ' select ';
            $sql .= '       c.id                                            as "TrnPromise__id"';
            $sql .= '     , b.facility_name || \' / \' || c.department_name as "TrnPromise__department"';
            $sql .= '     , to_char(d.last_date ,\'yyyy/mm/dd HH24:MI:SS\') as "TrnPromise__last_date" ';
            $sql .= '   from ';
            $sql .= '     mst_facility_relations as a  ';
            $sql .= '     left join mst_facilities as b  ';
            $sql .= '       on a.partner_facility_id = b.id  ';
            $sql .= '     left join mst_departments as c  ';
            $sql .= '       on b.id = c.mst_facility_id  ';
            $sql .= '     left join (  ';
            $sql .= '       select ';
            $sql .= '             x.department_id_from ';
            $sql .= '           , max(x.created)    as last_date  ';
            $sql .= '         from trn_promise_headers as x  ';
            $sql .= '         where x.is_deleted = false ';
            $sql .= '         group by x.department_id_from ';
            $sql .= '     ) as d  ';
            $sql .= '       on d.department_id_from = c.id  ';
            $sql .= '     inner join mst_user_belongings as z';
            $sql .= '       on z.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= '       and z.mst_facility_id = c.mst_facility_id ';
            $sql .= '       and z.is_deleted = false ';
            $sql .= '   where ';
            $sql .= '     c.department_type = ' . Configure::read('DepartmentType.hospital');
            $sql .= '     and a.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');

            if($this->request->data['promise']['facilities'] !== '' ){
                $sql .= '   and b.id = ' . $this->request->data['promise']['facilities'];
            }

            $sql .= ' order by b.facility_code , c.department_code';

            $searchResult = $this->TrnPromiseHeader->query($sql);
        }

        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnPromise.token' , $token);
        $this->request->data['TrnPromise']['token'] = $token;

        $this->set('searchResult',$searchResult);
    }

    /**
     * 引当要求作成
     * @access public
     */
    function add_result () {
        if($this->request->data['TrnPromise']['token'] === $this->Session->read('TrnPromise.token')) {
            $this->Session->delete('TrnPromise.token');
            $workType = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),3);
            $fixedNum = Configure::read('FixedType.List');
            //初期値
            if(empty($this->request->data['promise']['work_type']) ){
                $this->request->data['promise']['work_type_name'] = NULL;
            }else{
                $this->request->data['promise']['work_type_name'] = $workType[$this->request->data['promise']['work_type']];
            }
            $this->request->data['promise']['fixed_num_name'] = $fixedNum[$this->request->data['promise']['fixed_num']];
            $result = $this->_add_regist();
            if($result===false){
                //失敗
                $this->Session->setFlash('要求データの作成に失敗しました。', 'growl', array('type'=>'error') );
                $this->redirect('add');
            }
            /* 表示用後処理 */
            $this->set('result' , $result);
            $this->Session->write('TrnPromise.result', $result);
            $this->Session->write('TrnPromise.data', $this->request->data);
        }else{
            $this->set('result' , $this->Session->read('TrnPromise.result'));
            $this->request->data = $this->Session->read('TrnPromise.data');
        }
    }

    /**
     *
     *　@param string $keyword
     */
    function history () {
        $this->setRoleFunction(6); //臨時引当要求履歴、要求作成履歴
        //作業区分取得
        $this->set('workType',$this->getClassesList($this->Session->read('Auth.facility_id_selected') , array(2,3) ));

        //施設取得
        $this->set('facility_List',$this->getFacilityList(
                $this->Session->read('Auth.facility_id_selected'),
                array(Configure::read('FacilityType.hospital'))
                )
        );
        //部署コンボ
        $department_List=array();
        if(isset($this->request->data['TrnPromise']['facilityCode']) &&
           $this->request->data['TrnPromise']['facilityCode'] != ""){
            $department_List = $this->getDepartmentsList(
                    $this->request->data['TrnPromise']['facilityCode']
                );
        }
        $this->set('department_List' , $department_List);

        //ログイン中の施設
        $this->set('login_fas_id',$this->Session->read('Auth.facility_id_selected'));

        //選択施設名
        $this->set('fasName',$this->getFacilityName($this->Session->read('Auth.facility_id_selected')));

        //初期値
        $displayData = array();

        if(isset($this->request->data['selected']['is_search'])) {
            //検索ボタンが押されたら

            //全件取得
            $this->_PromisehistorySQL($this->request->data , $this->_getLimitCount() , false , true );
            //表示データ取得
            $displayData = $this->TrnPromise->query($this->_PromisehistorySQL($this->request->data , $this->_getLimitCount() , false));
        }else{
            //初期設定
            $this->request->data['TrnPromise']['work_date1'] = date('Y/m/d',strtotime("-7 day"));
            $this->request->data['TrnPromise']['work_date2'] = date('Y/m/d');
            $this->request->data['TrnPromise']['opt']        = 1; // 通常表示
            $this->request->data['selected']['mst_user_id'] = $this->Session->read('Auth.MstUser.id');
            
            $this->deleteSortInfo();
        }

        $this->set('displayData',$displayData);
        $this->render('history');
    }

    /*
     * 未引当一覧
    * department_id:部署ID
    */
    function print_stockOut($departmentCode = null){
        $result = $this->_StockOutList($departmentCode);

        //レコードがないときは終了
        if(empty($result)){
            exit;
        }

        $seq = 0;
        if(is_null($departmentCode)){
            //センター名
            $fasName = $this->getFacilityName($this->request->data['selected']['headFas']);
        }else{
            //部署名
            $fasName = $this->request->data['TrnPromise']['facilityName'] .' / '. $this->request->data['TrnPromise']['departmentName'];
        }

        $before = null;

        foreach($result as $r){
            if($before !== $r[0]['internal_code']){
                $seq++;
            }
            if($r[0]['center_stock_flag']=="1") {
                $internal_code = "★ " .$r[0]['internal_code'];
            }else{
                $internal_code = $r[0]['internal_code'];
            }
            $output[] = array(
                    $fasName
                    ,$internal_code           //商品ID
                    ,$r[0]['item_name']       //商品名
                    ,$r[0]['standard']        //規格
                    ,$r[0]['item_code']       //製品番号
                    ,$r[0]['dealer_name']     //販売元
                    ,$seq
                    ,$r[0]['name']            //棚番号
                    ,$r[0]['facility_name']   //仕入先
                    ,$r[0]['unit_name']       //包装単位
                    ,$r[0]['fixed_count']     //定数
                    ,$r[0]['order_point']     //発注点
                    ,$r[0]['requested_count'] //入荷予定
                    ,$r[0]['stock_count']     //在庫数
                    ,$r[0]['reserve_count']   //予約数
                    ,$r[0]['quantity']        //要求数
                    ,$r[0]['fixed_quantity']  //引当済
                    ,$r[0]['remain_count']    //未引当
                    ,date('Y/m/d')            //印刷年月日
            );
            $before = $r[0]['internal_code'];
        }

        $columns = array('センター名',
                         '商品ID',
                         '商品名',
                         '規格',
                         '製品番号',
                         '販売元',
                         '行番号',
                         '棚番号',
                         '仕入先',
                         '包装単位',
                         '定数',
                         '発注点',
                         '入荷予定',
                         '在庫数',
                         '予約数',
                         '要求数',
                         '引当済',
                         '未引当',
                         '印刷年月日'
                         );
        $layout_name = Configure::read('layoutnameModels');
        $layout_file = "/promises/xml_output";
        $fix_value = array('タイトル'=>'未引当一覧');
        $this->xml_output($output,join("\t",$columns),$layout_name['07'],$fix_value,$layout_file);
    }
    /*
     * 未引当一覧 病院
    * facility_id : 病院施設ID
    */
    function print_stockOutHp($facility_id = null){
        $result = $this->_StockOutListHp($facility_id,1);

        //レコードがないときは終了
        if(empty($result)){
            exit;
        }

        $seq = 0;

        $before = null;
        $before2 = null;

        foreach($result as $r){
            if($before2 !== $r[0]['facility_department_name']){
                $seq = 0;
                $before = null;
            }
            if($before !== $r[0]['internal_code']){
                $seq++;
            }
            if($r[0]['center_stock_flag']=="1") {
                $internal_code = "★ " .$r[0]['internal_code'];
            }else{
                $internal_code = $r[0]['internal_code'];
            }
            $output[] = array(
                    $r[0]['facility_department_name']
                    ,$internal_code           //商品ID
                    ,$r[0]['item_name']       //商品名
                    ,$r[0]['standard']        //規格
                    ,$r[0]['item_code']       //製品番号
                    ,$r[0]['dealer_name']     //販売元
                    ,$seq
                    ,$r[0]['name']            //棚番号
                    ,$r[0]['facility_name']   //仕入先
                    ,$r[0]['unit_name']       //包装単位
                    ,$r[0]['fixed_count']     //定数
                    ,$r[0]['order_point']     //発注点
                    ,$r[0]['requested_count'] //入荷予定
                    ,$r[0]['stock_count']     //在庫数
                    ,$r[0]['reserve_count']   //予約数
                    ,$r[0]['quantity']        //要求数
                    ,$r[0]['fixed_quantity']  //引当済
                    ,$r[0]['remain_count']    //未引当
                    ,date('Y/m/d')            //印刷年月日
            );
            $before = $r[0]['internal_code'];
            $before2 = $r[0]['facility_department_name'];
        }

        $columns = array('センター名',
                         '商品ID',
                         '商品名',
                         '規格',
                         '製品番号',
                         '販売元',
                         '行番号',
                         '棚番号',
                         '仕入先',
                         '包装単位',
                         '定数',
                         '発注点',
                         '入荷予定',
                         '在庫数',
                         '予約数',
                         '要求数',
                         '引当済',
                         '未引当',
                         '印刷年月日'
                         );
        $layout_name = Configure::read('layoutnameModels');
        $layout_file = "/promises/xml_output";
        $fix_value = array('タイトル'=>'未引当一覧');
        $this->xml_output($output,join("\t",$columns),$layout_name['07'],$fix_value,$layout_file);
    }

    /*
     * 未引当一覧CSV
    * facility_id : 病院施設ID
    */
    function output_csv($facility_id = null){
        if(isset($this->request->data['selected']['mode']) ){
            $sql = "";
            if( $this->request->data['selected']['mode'] == "5" ){
                $sql = $this->_StockOutListCSV();
            }
            if( $this->request->data['selected']['mode'] == "6" ){
                $sql = $this->_StockOutListCSV($this->request->data['TrnPromise']['departmentCode']);
            }
            if( $this->request->data['selected']['mode'] == "7" ){
                $sql = $this->_StockOutListHpCSV($this->request->data['TrnPromise']['facilityCode'],1);
            }
        }

        $this->db_export_csv($sql , "未引当一覧", 'decision');
    }

    function report($mode){
        switch($mode){
          case 'deci':
            //未引当一覧
            $this->print_stockOut();
            break;
          case 'deci2':
            //未引当一覧(部署別)
            $this->print_stockOut($this->request->data['TrnPromise']['departmentCode']);
            break;
          case 'deci3':
            //未引当一覧(病院)
            $this->print_stockOutHp($this->request->data['TrnPromise']['facilityCode']);
            break;
          case 'history':
            //要求履歴
            $this->history_report();
            break;
          case 'stockout':
            //欠品リスト
            $this->history_report(true);
            break;
          case 'promise_consume':
            //要求＋消費
            $this->special_report();
            break;
          case 'picking_list':
            //ピッキングリスト
            $this->picking_list();
            break;
          case 'xxx_list':
            // 補充リスト
            $this->xxx_list();
            break;
        }
    }

    function xxx_list(){
        $sql  = ' select ';
        $sql .= '       *  ';
        $sql .= '   from ';
        $sql .= '     (  ';
        /* 欠品分 */
        $sql .= '       select ';
        $sql .= '             f.facility_code ';
        $sql .= '           , f.facility_name ';
        $sql .= '           , e.department_code ';
        $sql .= '           , e.department_name ';
        $sql .= "           , to_char(a.work_date,'YYYY/mm/dd') as work_date ";
        $sql .= '           , c.internal_code ';
        $sql .= '           , c.item_name ';
        $sql .= '           , c.item_code ';
        $sql .= '           , c.standard ';
        $sql .= '           , (  ';
        $sql .= '             case  ';
        $sql .= '               when b.per_unit = 1  ';
        $sql .= '               then b1.unit_name  ';
        $sql .= "               else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '               end ';
        $sql .= '           )                            as unit_name ';
        $sql .= '           , d.dealer_name ';
        $sql .= '           , a.remain_count as quantity ';
        $sql .= '           , (  ';
        $sql .= '             case  ';
        $sql .= '               when c.is_lowlevel = true  ';
        $sql .= "               then '一般消耗品'  ";
        $sql .= '               when a.promise_type = ' . Configure::read('PromiseType.Constant');
        $sql .= "               then '定数 : ' || a.fixed_count ";
        $sql .= "               else '臨時' ";
        $sql .= '               end ';
        $sql .= '           )                            as comment ';
        $sql .= '           , j.name                     as shelf_name ';
        $sql .= '           , 0                          as list_type  ';
        $sql .= '           , a.promise_type ';
        $sql .= '           , ( case when is_lowlevel = true then 0 else 1 end ) as lowlevel ';
        $sql .= '         from ';
        $sql .= '           trn_promises as a ';
        $sql .= '           left join mst_item_units as b ';
        $sql .= '             on b.id = a.mst_item_unit_id ';
        $sql .= '           left join mst_unit_names as b1 ';
        $sql .= '             on b1.id = b.mst_unit_name_id ';
        $sql .= '           left join mst_unit_names as b2 ';
        $sql .= '             on b2.id = b.per_unit_name_id ';
        $sql .= '           left join mst_facility_items as c ';
        $sql .= '             on c.id = b.mst_facility_item_id ';
        $sql .= '           left join mst_dealers as d ';
        $sql .= '             on d.id = c.mst_dealer_id ';
        $sql .= '           left join mst_departments as e ';
        $sql .= '             on e.id = a.department_id_from ';
        $sql .= '           left join mst_facilities as f ';
        $sql .= '             on f.id = e.mst_facility_id ';
        $sql .= '           left join mst_facilities as g ';
        $sql .= '             on g.id = c.mst_facility_id ';
        $sql .= '           left join mst_departments as h ';
        $sql .= '             on h.mst_facility_id = g.id';
        $sql .= '            and h.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '           left join mst_fixed_counts as i ';
        $sql .= '             on i.mst_department_id = h.id';
        $sql .= '            and i.mst_item_unit_id = b.id';
        $sql .= '            and b.is_deleted = false ';
        $sql .= '           left join mst_shelf_names as j';
        $sql .= '             on j.id = i.mst_shelf_name_id ';
        $sql .= '         where ';
        $sql .= '           a.remain_count > 0 ';
        $sql .= '           and a.is_deleted = false ';
        $sql .= '           and c.mst_facility_id = ' . $this->request->data['MstFacility']['id'];
        $sql .= '       union all ';
        /* 払出分 */ 
        $sql .= '       select ';
        $sql .= '             f.facility_code ';
        $sql .= '           , f.facility_name ';
        $sql .= '           , e.department_code ';
        $sql .= '           , e.department_name ';
        $sql .= "           , to_char(a.work_date,'YYYY/mm/dd') as work_date ";
        $sql .= '           , c.internal_code ';
        $sql .= '           , c.item_name ';
        $sql .= '           , c.item_code ';
        $sql .= '           , c.standard ';
        $sql .= '           , (  ';
        $sql .= '             case  ';
        $sql .= '               when b.per_unit = 1 ';
        $sql .= '               then b1.unit_name ';
        $sql .= "               else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .= '               end ';
        $sql .= '           )                            as unit_name ';
        $sql .= '           , d.dealer_name ';
        $sql .= '           , x.remain_count as quantity ';
        $sql .= '           , (  ';
        $sql .= '             case  ';
        $sql .= '               when c.is_lowlevel = true  ';
        $sql .= "               then '一般消耗品' ";
        $sql .= '               when a.promise_type = ' . Configure::read('PromiseType.Constant');
        $sql .= "               then '定数 : ' || a.fixed_count ";
        $sql .= "               else '臨時' ";
        $sql .= '               end ';
        $sql .= '           )                            as comment ';
        $sql .= '           , j.name                     as shelf_name ';
        $sql .= '           , 1                          as list_type  ';
        $sql .= '           , a.promise_type ';
        $sql .= '           , ( case when c.is_lowlevel = true then 0 else 1 end ) as lowlevel';
        $sql .= '         from ';
        $sql .= '           trn_fixed_promises as x ';
        $sql .= '           left join trn_promises as a ';
        $sql .= '             on x.trn_promise_id = a.id ';
        $sql .= '           left join mst_item_units as b ';
        $sql .= '             on b.id = a.mst_item_unit_id ';
        $sql .= '           left join mst_unit_names as b1 ';
        $sql .= '             on b1.id = b.mst_unit_name_id ';
        $sql .= '           left join mst_unit_names as b2 ';
        $sql .= '             on b2.id = b.per_unit_name_id ';
        $sql .= '           left join mst_facility_items as c ';
        $sql .= '             on c.id = b.mst_facility_item_id ';
        $sql .= '           left join mst_dealers as d ';
        $sql .= '             on d.id = c.mst_dealer_id ';
        $sql .= '           left join mst_departments as e ';
        $sql .= '             on e.id = a.department_id_from ';
        $sql .= '           left join mst_facilities as f ';
        $sql .= '             on f.id = e.mst_facility_id ';
        $sql .= '           left join mst_facilities as g ';
        $sql .= '             on g.id = c.mst_facility_id ';
        $sql .= '           left join mst_departments as h ';
        $sql .= '             on h.mst_facility_id = g.id';
        $sql .= '            and h.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '           left join mst_fixed_counts as i ';
        $sql .= '             on i.mst_department_id = h.id';
        $sql .= '            and i.mst_item_unit_id = b.id';
        $sql .= '            and b.is_deleted = false ';
        $sql .= '           left join mst_shelf_names as j';
        $sql .= '             on j.id = i.mst_shelf_name_id ';
        $sql .= '         where ';
        $sql .= '           x.is_deleted = false  ';
        $sql .= '           and x.remain_count > 0 ';
        $sql .= '           and a.is_deleted = false  ';
        $sql .= '           and c.mst_facility_id = ' . $this->request->data['MstFacility']['id'];
        $sql .= '       union all ';
        /* 欠品分: 預託、非在庫発注 */
        $sql .= '       select ';
        $sql .= '             f.facility_code ';
        $sql .= '           , f.facility_name ';
        $sql .= '           , e.department_code ';
        $sql .= '           , e.department_name ';
        $sql .= "           , to_char(a.work_date,'YYYY/mm/dd') as work_date ";
        $sql .= '           , c.internal_code ';
        $sql .= '           , c.item_name ';
        $sql .= '           , c.item_code ';
        $sql .= '           , c.standard ';
        $sql .= '           , (  ';
        $sql .= '             case  ';
        $sql .= '               when b.per_unit = 1 ';
        $sql .= '               then b1.unit_name ';
        $sql .= "               else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .= '               end ';
        $sql .= '           )                            as unit_name ';
        $sql .= '           , d.dealer_name ';
        $sql .= '           , a.remain_count as quantity ';
        $sql .= '           , (  ';
        $sql .= '             case  ';
        $sql .= '               when c.is_lowlevel = true  ';
        $sql .= "               then '一般消耗品' ";
        $sql .= '               when a.order_type = ' . Configure::read('OrderTypes.ExNostock');
        $sql .= "               then '臨時' ";
        $sql .= "               else '非在庫 : ' || i.fixed_count ";
        $sql .= '               end ';
        $sql .= '           )                            as comment ';
        $sql .= '           , m.name                     as shelf_name ';
        $sql .= '           , 0                          as list_type ';
        $sql .= '           , ( case when c.is_lowlevel = true ';
        $sql .= '                    then ' . Configure::read('PromiseType.Temporary');
        $sql .= '                    when a.order_type = '  . Configure::read('OrderTypes.ExNostock');
        $sql .= '                    then ' . Configure::read('PromiseType.Temporary');
        $sql .= '                    else ' . Configure::read('PromiseType.Constant');
        $sql .= '               end ) as promise_type';
        $sql .= '           , ( case when c.is_lowlevel = true then 0 else 1 end ) as lowlevel';
        $sql .= '         from ';
        $sql .= '           trn_orders as a ';
        $sql .= '           left join mst_item_units as b ';
        $sql .= '             on b.id = a.mst_item_unit_id ';
        $sql .= '           left join mst_unit_names as b1 ';
        $sql .= '             on b1.id = b.mst_unit_name_id ';
        $sql .= '           left join mst_unit_names as b2 ';
        $sql .= '             on b2.id = b.per_unit_name_id ';
        $sql .= '           left join mst_facility_items as c ';
        $sql .= '             on c.id = b.mst_facility_item_id ';
        $sql .= '           left join mst_dealers as d ';
        $sql .= '             on d.id = c.mst_dealer_id ';
        $sql .= '           left join mst_departments as e ';
        $sql .= '             on e.id = a.department_id_from ';
        $sql .= '           left join mst_facilities as f ';
        $sql .= '             on f.id = e.mst_facility_id ';
        $sql .= '           left join mst_fixed_counts as i ';
        $sql .= '             on i.mst_item_unit_id = b.id ';
        $sql .= '            and i.mst_department_id = e.id ';
        $sql .= '            and i.is_deleted = false ';

        $sql .= '           left join mst_facilities as j ';
        $sql .= '             on j.id = c.mst_facility_id ';
        $sql .= '           left join mst_departments as k ';
        $sql .= '             on k.mst_facility_id = j.id';
        $sql .= '            and k.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '           left join mst_fixed_counts as l ';
        $sql .= '             on l.mst_department_id = k.id';
        $sql .= '            and l.mst_item_unit_id = b.id';
        $sql .= '            and b.is_deleted = false ';
        $sql .= '           left join mst_shelf_names as m';
        $sql .= '             on m.id = l.mst_shelf_name_id ';
        
        $sql .= '         where ';
        $sql .= '           a.remain_count > 0  ';
        $sql .= '           and a.is_deleted = false ';
        $sql .= '           and c.mst_facility_id = ' . $this->request->data['MstFacility']['id'];
        $sql .= '           and a.order_type in (' . Configure::read('OrderTypes.replenish') . ', ' . Configure::read('OrderTypes.nostock') . ', ' . Configure::read('OrderTypes.ExNostock') . ') ';
        $sql .= '     ) as xxx  ';
        $sql .= '   where 1 = 1 ';

        //発行施設
        if(!empty($this->request->data['TrnFixedPromiseHeader']['facilityCode'])){
            $sql .= " and facility_code = '". $this->request->data['TrnFixedPromiseHeader']['facilityCode'] ."'";
            //発行部署
            if(!empty($this->request->data['TrnFixedPromiseHeader']['departmentCode'])){
                $sql .= " and department_code = '" . $this->request->data['TrnFixedPromiseHeader']['departmentCode']."'";
            }
        }
        //商品ID
        if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
            $sql .= " and internal_code = '" . Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']) . "'";
        }
        //製品番号
        if(!empty($this->request->data['MstFacilityItem']['item_code'])){
            $sql .= " and item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        //規格
        if(!empty($this->request->data['MstFacilityItem']['standard'])){
            $sql .= " and standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }
        //商品名
        if(!empty($this->request->data['MstFacilityItem']['item_name'])){
            $sql .= " and item_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%'";
        }
        //販売元
        if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
            $sql .= " and dealer_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name']) . "%'";
        }
        //管理区分
        if(!empty($this->request->data['TrnFixedPromiseHeader']['promise_type'])){
            $sql .= " and promise_type = " . $this->request->data['TrnFixedPromiseHeader']['promise_type'];
        }
        $sql .= '   order by ';
        $sql .= '     facility_code ';
        $sql .= '     , department_code ';
        $sql .= '     , lowlevel ';
        $sql .= '     , list_type desc ';
        $sql .= '     , shelf_name ';
        $sql .= '     , comment ';
        $sql .= '     , item_code ';
        $sql .= '     , standard ';

        $ret = $this->TrnPromise->query($sql);
        $columns = array("補充依頼番号",
                         "仕入先名",
                         "補充先名",
                         "施主名",
                         "補充依頼日",
                         "納品先住所",
                         "施主住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "明細備考",
                         "包装単位",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $tmpHeader = "";
        $i = 1;
        $tmp_page_no = '';
        foreach($ret as $r){
            $page_no = $r[0]['facility_name'].$r[0]['department_name'].'_'.$r[0]['lowlevel'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }

            if($r[0]['quantity'] == 0){
                $r[0]['quantity'] = '';
            }
            $r[0]['comment'] = '要求日 : ' . $r[0]['work_date'] . ' : ' . $r[0]['comment'];
            if($r[0]['list_type'] == 0 ){
                $r[0]['comment'] = $r[0]['comment'] . ' : 欠品';
            }
            if($r[0]['shelf_name'] != ''){
                $r[0]['comment'] = $r[0]['comment'] . ' : 棚番 ' .$r[0]['shelf_name'];
            }
            
            $data[] = array(  ''                                                      //補充依頼番号
                             ,''                                                      //仕入先名
                             ,$r[0]['facility_name'] . ' ' . $r[0]['department_name'] //補充先名
                             ,''                                                      //施主名
                             ,''                                                      //補充依頼日
                             ,''                                                      //納品先住所
                             ,''                                                      //施主住所
                             ,$i                                                      //行番号
                             ,$r[0]['item_name']                                      //商品名
                             ,$r[0]['standard']                                       //規格
                             ,$r[0]['item_code']                                      //製品番号
                             ,$r[0]['dealer_name']                                    //販売元
                             ,$r[0]['internal_code']                                  //商品ID
                             ,$r[0]['comment']                                        //明細備考
                             ,$r[0]['unit_name']                                      //包装単位
                             ,$r[0]['quantity']                                       //数量
                             ,''                                                      //ヘッダ備考
                             ,''                                                      //印刷年月日
                             ,''                                                      //丸め区分
                             ,''                                                      //削除フラグ
                             ,''                                                      //変更フラグ
                             ,$page_no                                                //改ページ番号
                             );
            $i++;
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'補充リスト');
        $this->xml_output($data,join("\t",$columns),$layout_name['11'],$fix_value);
    }
    

    function picking_list(){
        /* 絞り込み条件 */
        $where = '';
        //商品ID
        if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
            $where .= " and c.internal_code = '" . Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']) . "'";
        }
        //製品番号
        if(!empty($this->request->data['MstFacilityItem']['item_code'])){
            $where .= " and c.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        //規格
        if(!empty($this->request->data['MstFacilityItem']['standard'])){
            $where .= " and standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }

        //発行施設
        if(!empty($this->request->data['TrnFixedPromiseHeader']['facilityCode'])){
            $where .= " and f.facility_code = '". $this->request->data['TrnFixedPromiseHeader']['facilityCode'] ."'";
            //発行部署
            if(!empty($this->request->data['TrnFixedPromiseHeader']['departmentCode'])){
                $where .= " and e.department_code = '" . $this->request->data['TrnFixedPromiseHeader']['departmentCode']."'";
            }

        }
        //商品名
        if(!empty($this->request->data['MstFacilityItem']['item_name'])){
            $where .= " and c.item_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%'";
        }
        //販売元
        if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
            $where .= " and d.dealer_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name']) . "%'";
        }
        //管理区分
        if(!empty($this->request->data['TrnFixedPromiseHeader']['promise_type'])){
            $where .= " and g.promise_type = " . $this->request->data['TrnFixedPromiseHeader']['promise_type'];
        }
        
        $sql  = ' select ';
        $sql .= '       f.facility_name ';
        $sql .= '       , e.department_name ';
        $sql .= '       , f.facility_code ';
        $sql .= '       , e.department_code ';
        $sql .= '       , c.internal_code ';
        $sql .= '       , c.item_name ';
        $sql .= '       , c.item_code ';
        $sql .= '       , c.standard ';
        $sql .= '       , (  ';
        $sql .= '         case  ';
        $sql .= '           when b.per_unit = 1  ';
        $sql .= '           then b1.unit_name  ';
        $sql .= "           else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')'  ";
        $sql .= '           end ';
        $sql .= '       ) as unit_name ';
        $sql .= '       , d.dealer_name ';
        $sql .= '       , sum(a.remain_count) as count ';
        $sql .= '       , (  ';
        $sql .= '         case  ';
        $sql .= '           when c.is_lowlevel = true ';
        $sql .= "           then '一般消耗品' ";
        $sql .= '           when g.promise_type = 1  ';
        $sql .= "           then '定数 : ' || g.fixed_count  ";
        $sql .= "           else '臨時'  ";
        $sql .= '           end ';
        $sql .= '       ) as fixed_count  ';
        $sql .= '       , g.promise_type';
        $sql .= '     from ';
        $sql .= '       trn_fixed_promises as a  ';
        $sql .= '       left join mst_item_units as b  ';
        $sql .= '         on b.id = a.mst_item_unit_id  ';
        $sql .= '       left join mst_unit_names as b1  ';
        $sql .= '         on b1.id = b.mst_unit_name_id  ';
        $sql .= '       left join mst_unit_names as b2  ';
        $sql .= '         on b2.id = b.per_unit_name_id  ';
        $sql .= '       left join mst_facility_items as c  ';
        $sql .= '         on c.id = b.mst_facility_item_id  ';
        $sql .= '       left join mst_dealers as d  ';
        $sql .= '         on d.id = c.mst_dealer_id  ';
        $sql .= '       left join mst_departments as e  ';
        $sql .= '         on e.id = a.department_id_from  ';
        $sql .= '       left join mst_facilities as f  ';
        $sql .= '         on f.id = e.mst_facility_id  ';
        $sql .= '       left join trn_promises as g  ';
        $sql .= '         on g.id = a.trn_promise_id  ';
        $sql .= '     where ';
        $sql .= '       a.remain_count > 0  ';
        $sql .= '       and a.is_deleted = false  ';
        $sql .= '       and c.mst_facility_id = ' . $this->request->data['MstFacility']['id'];
        $sql .= $where;
        $sql .= ' group by ';
        $sql .= '     f.facility_name ';
        $sql .= '     , e.department_name ';
        $sql .= '     , f.facility_code ';
        $sql .= '     , e.department_code ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , c.item_name ';
        $sql .= '     , c.item_code ';
        $sql .= '     , c.standard ';
        $sql .= '     , b.per_unit ';
        $sql .= '     , b1.unit_name ';
        $sql .= '     , b2.unit_name ';
        $sql .= '     , d.dealer_name ';
        $sql .= '     , c.is_lowlevel ';
        $sql .= '     , g.promise_type ';
        $sql .= '     , g.fixed_count ';
        $sql .= ' order by ';
        $sql .= '   f.facility_name ';
        $sql .= '   , e.department_name ';
        $sql .= '   , c.is_lowlevel ';
        $sql .= '   , g.promise_type ';
        $sql .= '   , d.dealer_name ';
        $sql .= '   , c.item_name ';
        $sql .= '   , c.item_code ';
        $sql .= '   , b.per_unit ';
        
        $ret = $this->TrnFixedPromise->query($sql);
        
        $columns = array("補充依頼番号",
                         "仕入先名",
                         "補充先名",
                         "施主名",
                         "補充依頼日",
                         "納品先住所",
                         "施主住所",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "明細備考",
                         "包装単位",
                         "数量",
                         "ヘッダ備考",
                         "印刷年月日",
                         "丸め区分",
                         "削除フラグ",
                         "変更フラグ",
                         "改ページ番号"
                         );
        $data = array();
        $tmpHeader = "";
        $i = 1;
        $tmp_page_no = '';
        foreach($ret as $r){
            $page_no = $r[0]['facility_name'].$r[0]['department_name'];
            if($tmp_page_no !== $page_no){
                $tmp_page_no = $page_no;
                $i = 1;
            }

            $data[] = array(  ''                                                      //補充依頼番号
                             ,''                                                      //仕入先名
                             ,$r[0]['facility_name'] . ' ' . $r[0]['department_name'] //補充先名
                             ,''                                                      //施主名
                             ,''                                                      //補充依頼日
                             ,''                                                      //納品先住所
                             ,''                                                      //施主住所
                             ,$i                                                      //行番号
                             ,$r[0]['item_name']                                      //商品名
                             ,$r[0]['standard']                                       //規格
                             ,$r[0]['item_code']                                      //製品番号
                             ,$r[0]['dealer_name']                                    //販売元
                             ,$r[0]['internal_code']                                  //商品ID
                             ,$r[0]['fixed_count']                                    //明細備考
                             ,$r[0]['unit_name']                                      //包装単位
                             ,$r[0]['count']                                          //数量
                             ,''                                                      //ヘッダ備考
                             ,''                                                      //印刷年月日
                             ,''                                                      //丸め区分
                             ,''                                                      //削除フラグ
                             ,''                                                      //変更フラグ
                             ,$page_no                                                //改ページ番号
                             );
            $i++;
        }
        $layout_name = Configure::read('layoutnameModels');
        $fix_value = array('タイトル'=>'ピッキングリスト');
        $this->xml_output($data,join("\t",$columns),$layout_name['11'],$fix_value);
    }

    
    function export_excel(){
        $where = '';
        $work_date = '';
        // 日付From
        if($this->request->data['TrnPromise']['work_date1'] != ''){
            $where .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "' ";
        }

        // 日付To
        if($this->request->data['TrnPromise']['work_date2'] != ''){
            $where .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "' ";
        }

        // 部署コード
        if($this->request->data['TrnPromise']['departmentCode'] != ''){
            $where .= " and e.department_code = '" . $this->request->data['TrnPromise']['departmentCode'] . "'";
        }
        // 表示用データ取得
        $ret = $this->getSpecialReportData($where);
        
        //テンプレートファイルフルパス
        $template = realpath(TMP);
        $template .= DS . 'excel' . DS;
        $template_path = $template . "template34.xls";
        
        // Excelオブジェクト作成
        $PHPExcel = $this->createExcelObj($template_path);

        //表紙への入力
        //シートの設定
        $PHPExcel->setActiveSheetIndex(0);  //一番左のシート
        $sheet = $PHPExcel->getActiveSheet();
        // Excel書込処理
        foreach($ret as $num => $r){
            $cell =  (string)($num+3);
            
            $sheet->setCellValue("A" . $cell, $num + 1);                 // No
            $sheet->setCellValue("B" . $cell, $r[0]['department_name']); // 部署名
            $sheet->setCellValue("C" . $cell, $r[0]['internal_code']);   // 商品ID
            $sheet->setCellValue("D" . $cell, $r[0]['item_name']);       // 商品名
            $sheet->setCellValue("E" . $cell, $r[0]['item_code']);       // 製品番号
            $sheet->setCellValue("F" . $cell, $r[0]['standard']);        // 規格
            $sheet->setCellValue("G" . $cell, $r[0]['dealer_name']);     // 販売元
            $sheet->setCellValue("H" . $cell, $r[0]['unit_name']);       // 包装単位
            $sheet->setCellValue("I" . $cell, $r[0]['consume_count']);   // 定数使用数
            $sheet->setCellValue("J" . $cell, $r[0]['promise_count']);   // 臨時請求数
            $sheet->setCellValue("K" . $cell, $r[0]['fixed_count']);     // 定数
            $sheet->setCellValue("L" . $cell, $r[0]['total_count']);     // 数量合計
        }
        
        //保存ファイル名
        $filename = "保険医療材料_"  . date('YmdHis')  . ".xls";

        $filename = mb_convert_encoding($filename, 'sjis', 'utf-8');
        
        // 保存ファイルパス
        $upload = realpath( TMP );
        $upload .= DS . '..' . DS . 'webroot' . DS . 'rfp' . DS;
        $path = $upload . $filename;
        
        $objWriter = new PHPExcel_Writer_Excel5( $PHPExcel );   //2003形式で保存
        $objWriter->save( $path );
        // 保存したExcelをダウンロード
        $result = file_get_contents(WWW_ROOT . DS . 'rfp' . DS . $filename);
        
        Configure::write('debug',0);
        header("Content-disposition: attachment; filename=" . $filename);
        header("Content-type: application/octet-stream; name=" . $filename);
        print($result);
        return;
    }
    

    function special_report(){
        $where = '';
        $work_date = '';
        // 日付From
        if($this->request->data['TrnPromise']['work_date1'] != ''){
            $where .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "' ";
            $work_date = $this->request->data['TrnPromise']['work_date1'] . ' ～ ';
        }

        // 日付To
        if($this->request->data['TrnPromise']['work_date2'] != ''){
            $where .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "' ";
            if($work_date == ''){
                $work_date = ' ～ ' .  $this->request->data['TrnPromise']['work_date2'];
            }else{
                $work_date .= $this->request->data['TrnPromise']['work_date2'];
            }
        }

        // 部署コード
        if($this->request->data['TrnPromise']['departmentCode'] != ''){
            $where .= " and e.department_code = '" . $this->request->data['TrnPromise']['departmentCode'] . "'";
        }
        
        $ret = $this->getSpecialReportData($where);
        
        $output = array();
        $rownum = 1;
        $per_department_name = null;

        foreach($ret as $r) {
            if($per_department_name != $r[0]['department_name']){
                $rownum = 1;
            }

            $output[] = array(
                     $r[0]['department_name']    //部署名
                    ,$rownum                     //行番号
                    ,$r[0]['item_name']          //商品名
                    ,$r[0]['standard']           //規格
                    ,$r[0]['item_code']          //製品番号
                    ,$r[0]['dealer_name']        //販売元
                    ,$r[0]['internal_code']      //商品ID
                    ,$r[0]['unit_name']          //包装単位
                    ,$r[0]['total_count']        //数量合計
                    ,$r[0]['promise_count']      //臨時請求数
                    ,$r[0]['consume_count']      //定数使用数
                    ,$r[0]['fixed_count']        //定数
                    ,$work_date                  //請求日
                    ,date('Y/m/d')               //印刷日
                    ,$r[0]['department_name']        //改ページ番号
            );
            $rownum++;
            $per_department_name = $r[0]['department_name'];
        }

        $columns = array("部署名",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "商品ID",
                         "包装単位",
                         "数量合計",
                         "臨時請求数",
                         "定数使用数",
                         "定数",
                         "請求日",
                         "作成日時",
                         "改ページ番号"
                         );
        $Search_condition = array();
        $layout_name = Configure::read('layoutnameModels');
        $layout_file = "/promises/xml_output";
        $fix_value = array('タイトル'=>'医療材料請求伝票','検索条件'=>'');
        $useLayout = $layout_name['33'];
        $this->xml_output($output,join("\t",$columns),$useLayout,$fix_value,$layout_file);
    }
    
    
    function getSpecialReportData($where){
        // 臨時要求 + 消費
        $sql  = ' select ';
        $sql .= '       x.department_name ';
        $sql .= '     , x.internal_code ';
        $sql .= '     , x.item_name ';
        $sql .= '     , x.item_code ';
        $sql .= '     , x.standard ';
        $sql .= '     , x.dealer_name ';
        $sql .= '     , x.fixed_count';
        $sql .= '     , x.unit_name ';
        $sql .= '     , sum(case when x.type = 1 then x.quantity else 0 end) as consume_count ';
        $sql .= '     , sum(case when x.type = 2 then x.quantity else 0 end) as promise_count ';
        $sql .= '     , sum(x.quantity)                                      as total_count  ';
        $sql .= '   from ';
        $sql .= '     (  ';
        $sql .= '       select ';
        $sql .= '             e.department_name ';
        $sql .= '           , c.internal_code ';
        $sql .= '           , c.item_name ';
        $sql .= '           , c.item_code ';
        $sql .= '           , c.standard ';
        $sql .= '           , d.dealer_name ';
        $sql .= '           , f.fixed_count';
        $sql .= '           , ( case when b.per_unit = 1 then b1.unit_name ';
        $sql .= "               else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .= '               end )                    as unit_name';
        $sql .= '           , 1                          as type ';
        $sql .= '           , sum(a.quantity)            as quantity  ';
        $sql .= '         from ';
        $sql .= '           trn_consumes as a ';
        $sql .= '           left join mst_item_units as b ';
        $sql .= '             on b.id = a.mst_item_unit_id ';
        $sql .= '           left join mst_unit_names as b1 ';
        $sql .= '             on b.mst_unit_name_id = b1.id ';
        $sql .= '           left join mst_unit_names as b2  ';
        $sql .= '             on b.per_unit_name_id = b2.id ';
        $sql .= '           left join mst_facility_items as c  ';
        $sql .= '             on c.id = b.mst_facility_item_id  ';
        $sql .= '           left join mst_dealers as d  ';
        $sql .= '             on d.id = c.mst_dealer_id  ';
        $sql .= '           left join mst_departments as e  ';
        $sql .= '             on e.id = a.mst_department_id  ';
        $sql .= '           inner join mst_fixed_counts as f '; // 定数設定のない、消費は無視
        $sql .= '             on f.mst_department_id = e.id ';
        $sql .= '            and f.mst_item_unit_id = b.id ';
        $sql .= '            and f.is_deleted = false ';
        $sql .= '         where ';
        $sql .= '           a.is_deleted = false  ';
        $sql .= '           and a.use_type in (1, 2, 3, 7)  ';
        $sql .= $where;
        $sql .= '         group by ';
        $sql .= '           e.department_name ';
        $sql .= '           , c.internal_code ';
        $sql .= '           , c.item_name ';
        $sql .= '           , c.item_code ';
        $sql .= '           , c.standard ';
        $sql .= '           , d.dealer_name  ';
        $sql .= '           , f.fixed_count ';
        $sql .= '           , b.per_unit ';
        $sql .= '           , b1.unit_name ';
        $sql .= '           , b2.unit_name ';
        $sql .= '       union all  ';
        $sql .= '       select ';
        $sql .= '             e.department_name ';
        $sql .= '           , c.internal_code ';
        $sql .= '           , c.item_name ';
        $sql .= '           , c.item_code ';
        $sql .= '           , c.standard ';
        $sql .= '           , d.dealer_name ';
        $sql .= '           , f.fixed_count';
        $sql .= '           , ( case when b.per_unit = 1 then b1.unit_name ';
        $sql .= "               else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .= '               end )                    as unit_name';
        $sql .= '           , 2                          as type ';
        $sql .= '           , sum(a.quantity)            as quantity  ';
        $sql .= '         from ';
        $sql .= '           trn_promises as a  ';
        $sql .= '           left join mst_item_units as b  ';
        $sql .= '             on b.id = a.mst_item_unit_id  ';
        $sql .= '           left join mst_unit_names as b1 ';
        $sql .= '             on b.mst_unit_name_id = b1.id ';
        $sql .= '           left join mst_unit_names as b2  ';
        $sql .= '             on b.per_unit_name_id = b2.id ';
        $sql .= '           left join mst_facility_items as c  ';
        $sql .= '             on c.id = b.mst_facility_item_id  ';
        $sql .= '           left join mst_dealers as d  ';
        $sql .= '             on d.id = c.mst_dealer_id  ';
        $sql .= '           left join mst_departments as e  ';
        $sql .= '             on e.id = a.department_id_from  ';
        $sql .= '           left join mst_fixed_counts as f ';
        $sql .= '             on f.mst_department_id = e.id ';
        $sql .= '            and f.mst_item_unit_id = b.id ';
        $sql .= '            and f.is_deleted = false ';
        $sql .= '         where ';
        $sql .= '           a.is_deleted = false  ';
        $sql .= '           and a.promise_type = ' . Configure::read('PromiseType.Temporary');
        $sql .= $where;
        $sql .= '         group by ';
        $sql .= '           e.department_name ';
        $sql .= '           , c.internal_code ';
        $sql .= '           , c.item_name ';
        $sql .= '           , c.item_code ';
        $sql .= '           , c.standard ';
        $sql .= '           , d.dealer_name ';
        $sql .= '           , f.fixed_count';
        $sql .= '           , b.per_unit ';
        $sql .= '           , b1.unit_name ';
        $sql .= '           , b2.unit_name ';
        $sql .= '      union all ';
        $sql .= '       select ';
        $sql .= '             e.department_name ';
        $sql .= '           , c.internal_code ';
        $sql .= '           , c.item_name ';
        $sql .= '           , c.item_code ';
        $sql .= '           , c.standard ';
        $sql .= '           , d.dealer_name ';
        $sql .= '           , f.fixed_count';
        $sql .= '           , ( case when b.per_unit = 1 then b1.unit_name ';
        $sql .= "               else b1.unit_name || '(' || b.per_unit || b2.unit_name || ')' ";
        $sql .= '               end )                    as unit_name';
        $sql .= '           , 2                          as type ';
        $sql .= '           , sum(a.quantity)            as quantity  ';
        $sql .= '         from ';
        $sql .= '           trn_orders as a  ';
        $sql .= '           left join mst_item_units as b  ';
        $sql .= '             on b.id = a.mst_item_unit_id  ';
        $sql .= '           left join mst_unit_names as b1 ';
        $sql .= '             on b.mst_unit_name_id = b1.id ';
        $sql .= '           left join mst_unit_names as b2  ';
        $sql .= '             on b.per_unit_name_id = b2.id ';
        $sql .= '           left join mst_facility_items as c  ';
        $sql .= '             on c.id = b.mst_facility_item_id  ';
        $sql .= '           left join mst_dealers as d  ';
        $sql .= '             on d.id = c.mst_dealer_id  ';
        $sql .= '           left join mst_departments as e  ';
        $sql .= '             on e.id = a.department_id_from  ';
        $sql .= '           left join mst_fixed_counts as f ';
        $sql .= '             on f.mst_department_id = e.id ';
        $sql .= '            and f.mst_item_unit_id = b.id ';
        $sql .= '            and f.is_deleted = false ';
        $sql .= '         where ';
        $sql .= '           a.is_deleted = false  ';
        $sql .= '           and a.order_type = ' . Configure::read('OrderTypes.ExNostock');
        $sql .= $where;
        $sql .= '         group by ';
        $sql .= '           e.department_name ';
        $sql .= '           , c.internal_code ';
        $sql .= '           , c.item_name ';
        $sql .= '           , c.item_code ';
        $sql .= '           , c.standard ';
        $sql .= '           , d.dealer_name ';
        $sql .= '           , f.fixed_count';
        $sql .= '           , b.per_unit ';
        $sql .= '           , b1.unit_name ';
        $sql .= '           , b2.unit_name ';
        $sql .= '     ) as x  ';
        $sql .= '   group by ';
        $sql .= '     x.department_name ';
        $sql .= '     , x.internal_code ';
        $sql .= '     , x.item_name ';
        $sql .= '     , x.item_code ';
        $sql .= '     , x.standard ';
        $sql .= '     , x.dealer_name  ';
        $sql .= '     , x.fixed_count  ';
        $sql .= '     , x.unit_name ';
        $sql .= '   order by ';
        $sql .= '     x.department_name ';
        $sql .= '     , x.internal_code ';

        $ret = $this->TrnPromise->query($sql);
        return $ret;
    }
    
    
    function history_report($stockout = false){
        $search_facter = ""; //検索条件（帳票表示用）
        $where = ""; //検索条件（SQL用）

        // 施設コードから施設IDを取得
        if($this->request->data['TrnPromise']['facilityCode'] != ''){
            $this->request->data['TrnPromise']['facilityId'] = $this->getFacilityId($this->request->data['TrnPromise']['facilityCode'] ,
                                                                                    Configure::read('FacilityType.hospital'));
        }
        // 部署コードから部署IDを取得
        if($this->request->data['TrnPromise']['departmentCode']!=''){
            $this->request->data['TrnPromise']['departmentId'] = $this->getDepartmentId($this->request->data['TrnPromise']['facilityId'],
                                                                                        Configure::read('DepartmentType.hospital'),
                                                                                        $this->request->data['TrnPromise']['departmentCode']
                                                                                        );
        }
        
        
        if(!isset($this->request->data['TrnPromise']['promise_type'])) $this->request->data['TrnPromise']['promise_type'] = "";
        //管理区分
        $promiseTypeArray = Configure::read('PromiseType.list');
        if(isset($promiseTypeArray[$this->request->data['TrnPromise']['promise_type']])){
            $search_facter .= "・管理区分 = ".$promiseTypeArray[$this->request->data['TrnPromise']['promise_type']];
            $where .= " and a.promise_type = " . $this->request->data['TrnPromise']['promise_type'];
        }

        //取消しも表示
        if($this->request->data['TrnPromise']['delete_opt'] == 0){
            //チェックされてない
            $where .= " and a.is_deleted = false";
        }else{
            //チェックされている
            $search_facter .= "・取消も表示する = ON";
        }

        //欠品のみ表示
        if( ($this->request->data['TrnPromise']['opt'] == "0") || ($stockout) ){
            $where .= " and a.is_deleted = false";
            $where .= " and a.remain_count > 0 ";

            if($this->request->data['TrnPromise']['opt'] == "0"){
                $search_facter .= "・欠品のみ表示 ";
            }
        }

        //要求施設
        if(isset($this->request->data['TrnPromise']['facilityId']) && $this->request->data['TrnPromise']['facilityId'] != ''){
            $where .= " and g.id = " . $this->request->data['TrnPromise']['facilityId'];
            $search_facter .= "・施設 = ".$this->getFacilityName($this->request->data['TrnPromise']['facilityId']);
        }

        //要求部署
        if(isset($this->request->data['TrnPromise']['departmentId']) && $this->request->data['TrnPromise']['departmentId'] != ''){
            $where .= " and f.id = " . $this->request->data['TrnPromise']['departmentId'];
            $search_facter .= "・部署 = ".$this->getDepartmentName($this->request->data['TrnPromise']['departmentId']);
        }

        //引当番号
        if($this->request->data['TrnPromise']['work_no'] != ''){
            $where .= " and a.work_no like '%{$this->request->data['TrnPromise']['work_no']}%'";
            $search_facter .= "・要求番号 = ".$this->request->data['TrnPromise']['work_no'];
        }
        //引当日1
        if($this->request->data['TrnPromise']['work_date1'] != ''){
            $where .= " and  a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] ."'";
            $search_facter .= "・要求日 = ".$this->request->data['TrnPromise']['work_date1']. "～";
        }
        //引当日2
        if($this->request->data['TrnPromise']['work_date2'] != ''){
            $where .= " and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] ."'";
            if($this->request->data['TrnPromise']['work_date1'] == ''){
                $search_facter .= "・要求日 = ～";
            }
            $search_facter .= $this->request->data['TrnPromise']['work_date2'];
        }
        //引当区分
        if($this->request->data['TrnPromise']['promise_type'] != ''){
            $where .= " and a.promise_type = " . $this->request->data['TrnPromise']['promise_type'];
        }
        //商品ID
        if($this->request->data['MstFacilityItem']['internal_code'] != ''){
            $where .= " and c.internal_code = '" . $this->request->data['MstFacilityItem']['internal_code'] . "'";
            $search_facter .= "・商品ID = ".$this->request->data['MstFacilityItem']['internal_code'];
        }
        //製品番号
        if($this->request->data['MstFacilityItem']['item_code'] != ''){
            $where .= " and c.item_code like '%{$this->request->data['MstFacilityItem']['item_code']}%'";
            $search_facter .= "・製品番号 = ".$this->request->data['MstFacilityItem']['item_code'];
        }
        //商品名
        if($this->request->data['MstFacilityItem']['item_name'] != ''){
            $where .= " and c.item_name like '%{$this->request->data['MstFacilityItem']['item_name']}%'";
            $search_facter .= "・商品名 = ".$this->request->data['MstFacilityItem']['item_name'];
        }
        //販売元名
        if($this->request->data['MstFacilityItem']['dealer_name'] != ''){
            $where .= " and j.dealer_name like '%{$this->request->data['MstFacilityItem']['dealer_name']}%'";
            $search_facter .= "・販売元名 = ".$this->request->data['MstFacilityItem']['dealer_name'];
        }
        //規格
        if($this->request->data['MstFacilityItem']['standard'] != ''){
            $where .= " and c.standard like '%{$this->request->data['MstFacilityItem']['standard']}%'";
            $search_facter .= "・規格 = ".$this->request->data['MstFacilityItem']['standard'];
        }
        //jan(前方一致)
        if($this->request->data['MstFacilityItem']['jan_code'] != ''){
            $where .= " and c.jan_code like '%{$this->request->data['MstFacilityItem']['jan_code']}'";
            $search_facter .= "・JANコード = ".$this->request->data['MstFacilityItem']['jan_code'];
        }
        //作業区分
        if($this->request->data['TrnPromise']['work_type'] !='' ){
            $where .= " and a.work_type = " . $this->request->data['TrnPromise']['work_type'];
            $search_facter .= "・作業区分 = ".$this->getWorkTypeName($this->request->data['TrnPromise']['work_type']);
        }

        $sql  = ' select ';
        $sql .= '       i.facility_name ';
        $sql .= '     , f.department_code ';
        $sql .= "     , g.facility_name || ' ' || f.department_name as hospital_name";
        $sql .= '     , c.item_name ';
        $sql .= '     , c.standard ';
        $sql .= '     , c.item_code ';
        $sql .= '     , j.dealer_name ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        foreach(Configure::read('PromiseType.list') as $key => $name){
            $sql .= "         when a.promise_type = {$key} then '$name'  ";
        }
        $sql .= '         end ';
        $sql .= '     )                                 as promise_type_name ';
        $sql .= "     , to_char(a.work_date , 'YYYY/mm/dd') as work_date ";
        $sql .= '     , a.recital ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then d.unit_name  ';
        $sql .= "         else d.unit_name || '(' || b.per_unit || e.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                 as unit_name ';
        $sql .= '     , a.work_no ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , a.stock_count ';
        $sql .= '     , a.fixed_count ';
        $sql .= '     , a.requested_count ';
        $sql .= '     , a.consume_count ';
        $sql .= '     , a.quantity ';
        $sql .= '     , a.unreceipt_count  ';
        $sql .= '   from ';
        $sql .= '     trn_promises as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on d.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as e  ';
        $sql .= '       on e.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_departments as f  ';
        $sql .= '       on f.id = a.department_id_from  ';
        $sql .= '     left join mst_facilities as g  ';
        $sql .= '       on g.id = f.mst_facility_id  ';
        $sql .= '     left join mst_departments as h  ';
        $sql .= '       on h.id = a.department_id_to  ';
        $sql .= '     left join mst_facilities as i  ';
        $sql .= '       on i.id = h.mst_facility_id  ';
        $sql .= '     left join mst_dealers as j  ';
        $sql .= '       on j.id = c.mst_dealer_id  ';
        $sql .= '     inner join mst_facility_relations as k  ';
        $sql .= '       on k.mst_facility_id = ' .$this->request->data['selected']['login_fas_id'];
        $sql .= '       and k.is_deleted = false  ';
        $sql .= '       and k.partner_facility_id = g.id  ';
        $sql .= '     left join mst_classes as l  ';
        $sql .= '       on l.id = a.work_type  ';

        $sql .= '     inner join mst_user_belongings as m  ';
        $sql .= '       on m.mst_user_id = ' .$this->request->data['selected']['mst_user_id'];
        $sql .= '       and m.mst_facility_id = g.id  ';
        $sql .= '   where ';
        $sql .= '     1 = 1 ';
        $sql .= $where;
        $sql .= '   order by ';
        $sql .= '     g.id ';
        $sql .= '     , f.department_code ';
        $sql .= '     , a.work_no ';
        $sql .= '     , c.internal_code ';
        $sql .= '     , b.per_unit ';

        $ret = $this->TrnPromise->query($sql);

        $output = array();
        $rownum = 1;
        $per_department_code = null;

        foreach($ret as $r) {

            if($per_department_code != $r[0]['department_code']){
                $rownum = 1;
            }

            $output[] = array(
                    $r[0]['facility_name']      //センター名
                    ,$r[0]['department_code']    //部署コード
                    ,$r[0]['hospital_name']      //病院 部署
                    ,$rownum                     //行番号
                    ,$r[0]['item_name']          //商品名
                    ,$r[0]['standard']           //規格
                    ,$r[0]['item_code']          //製品番号
                    ,$r[0]['dealer_name']        //販売元
                    ,$r[0]['promise_type_name']  //管理区分
                    ,$r[0]['work_date']          //要求日
                    ,$r[0]['recital']            //明細備考
                    ,$r[0]['unit_name']          //包装単位名
                    ,$r[0]['work_no']            //要求番号
                    ,$r[0]['internal_code']      //商品ID
                    ,$r[0]['stock_count']        //在庫数
                    ,$r[0]['fixed_count']        //定数
                    ,$r[0]['requested_count']    //要求済
                    ,$r[0]['consume_count']      //消費数
                    ,$r[0]['quantity']           //作成数
                    ,$r[0]['unreceipt_count']    //未受領
                    ,date('Y/m/d')               //印刷日
            );
            $rownum++;
            $per_department_code = $r[0]['department_code'];
        }

        $columns = array("センター名",
                         "施設部署コード",
                         "施設部署名",
                         "行番号",
                         "商品名",
                         "規格",
                         "製品番号",
                         "販売元",
                         "管理区分",
                         "要求日",
                         "明細備考",
                         "包装単位",
                         "要求番号",
                         "商品ID",
                         "在庫数",
                         "定数",
                         "要求済",
                         "消費数",
                         "作成数",
                         "未受領",
                         "作成日時"
                         );
        $Search_condition = array();
        $layout_name = Configure::read('layoutnameModels');
        $layout_file = "/promises/xml_output";
        $fix_value = array('タイトル'=>'要求作成履歴','検索条件'=>$search_facter);
        //欠品リスト
        if( ($stockout) ){
            $useLayout = $layout_name['06'];
        }else{
            $useLayout = $layout_name['05'];
        }
        $this->xml_output($output,join("\t",$columns),$useLayout,$fix_value,$layout_file);
    }

    /**
     *
     *　@param string $keyword
     */
    function cancel_confirm () {
        //limit削除
        $this->_deleteLimitCount();
        //ソート削除
        $this->deleteSortInfo();
        //更新時間チェック
        $this->Session->write('PromiseCancel.readTime',date('Y-m-d H:i:s'));

        // 検索処理
        $sql = $this->_PromisehistorySQL($this->request->data , $this->_getLimitCount() , true);
        $displayData = $this->TrnPromise->query($sql);
        
        
        
        $butDisFlg = TRUE;
        $this->set('butDisFlg',$butDisFlg);
        $this->set('displayData',$displayData);
        unset($proms,$displayData);
        
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Promise.token' , $token);
        $this->request->data['Promise']['token'] = $token;
    }

    /**
     *
     *　@param string $keyword
     */
    function cancel_result () {
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['Promise']['token'] === $this->Session->read('Promise.token')) {
            $this->Session->delete('Promise.token');
            $userId = $this->Session->read('Auth.MstUser.id');
            $now    = date('Y/m/d H:i:s');
            //トランザクション開始
            $this->TrnPromise->begin();

            $ids = null;

            $ids = join(',' , $this->request->data['TrnPromise']['id']);

            //行ロック
            $this->TrnPromise->query("select * from trn_promises as a where a.id in ( $ids ) for update  ");

            //病院在庫。未入荷数▽
            $sql  = ' update trn_stocks x ';
            $sql .= '   set ';
            $sql .= '     requested_count = x.requested_count - remain_count  ';
            $sql .= '   , modifier = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= "   , modified = '" . $now . "'";
            $sql .= '   from ';
            $sql .= '     (  ';
            $sql .= '       select ';
            $sql .= '             a.mst_item_unit_id ';
            $sql .= '           , a.department_id_from ';
            $sql .= '           , sum(a.remain_count) as remain_count  ';
            $sql .= '         from ';
            $sql .= '           trn_promises as a  ';
            $sql .= '         where ';
            $sql .= '           a.id in (' . $ids . ')  ';
            $sql .= '           and a.promise_type = ' . Configure::read('PromiseType.Constant') ;
            $sql .= '         group by ';
            $sql .= '           a.mst_item_unit_id ';
            $sql .= '           , a.department_id_from ';
            $sql .= '     ) as y  ';
            $sql .= '   where ';
            $sql .= '     x.mst_item_unit_id = y.mst_item_unit_id  ';
            $sql .= '     and x.mst_department_id = y.department_id_from ';
            $this->TrnPromise->query($sql);

            //センター在庫。要求数▽
            $sql  = ' update trn_stocks x ';
            $sql .= '   set ';
            $sql .= '     promise_count = x.promise_count - remain_count  ';
            $sql .= '   , modifier = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= "   , modified = '" . $now . "'";
            $sql .= '   from ';
            $sql .= '     (  ';
            $sql .= '       select ';
            $sql .= '             a.mst_item_unit_id ';
            $sql .= '           , a.department_id_to ';
            $sql .= '           , sum(a.remain_count) as remain_count  ';
            $sql .= '         from ';
            $sql .= '           trn_promises as a  ';
            $sql .= '         where ';
            $sql .= '           a.id in (' . $ids . ')  ';
            $sql .= '         group by ';
            $sql .= '           a.mst_item_unit_id ';
            $sql .= '           , a.department_id_to ';
            $sql .= '     ) as y  ';
            $sql .= '   where ';
            $sql .= '     x.mst_item_unit_id = y.mst_item_unit_id  ';
            $sql .= '     and x.mst_department_id = y.department_id_to ';
            $this->TrnPromise->query($sql);

            //明細取り消し
            foreach($this->request->data['TrnPromise']['id'] as $key => $id){

                $this->TrnPromise->create();
                if (!$this->TrnPromise->updateAll(array(  'TrnPromise.remain_count'    => 0,
                                                          'TrnPromise.quantity'        => 'TrnPromise.quantity - ' .$this->request->data['TrnPromise']['remain_count'][$key] ,
                                                          'TrnPromise.unreceipt_count' => 'TrnPromise.unreceipt_count - ' .$this->request->data['TrnPromise']['remain_count'][$key],
                                                          'TrnPromise.recital'         => "'" . $this->request->data['TrnPromise']['recital'][$key] . "'",
                                                          'TrnPromise.is_deleted'      => (($this->request->data['TrnPromise']['remain_count'][$key]==$this->request->data['TrnPromise']['quantity'][$key])?'true':'false'),
                                                          'TrnPromise.promise_status'  => (($this->request->data['TrnPromise']['remain_count'][$key]==$this->request->data['TrnPromise']['quantity'][$key])?'5':'3'),
                                                          'TrnPromise.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                                          'TrnPromise.modified'        => "'" . $now . "'"
                                                          )
                                                  ,
                                                  array(
                                                      'TrnPromise.id'  => $id,
                                                      )
                                                  ,
                                                  -1
                                                  )
                    ) {
                    $this->TrnPromise->rollback();
                    return false;
                }
            }

            //ヘッダ取り消し
            $sql  = ' update trn_promise_headers xx  ';
            $sql .= '   set ';
            $sql .= '     is_deleted = yy.is_deleted ';
            $sql .= '     , promise_status = yy.status  ';
            $sql .= '     , modifier = ' . $this->Session->read('Auth.MstUser.id');
            $sql .= "     , modified = '" . $now . "'";
            $sql .= '   from ';
            $sql .= '     (  ';
            $sql .= '       select ';
            $sql .= '             x.id ';
            $sql .= '           , (  ';
            $sql .= '             case  ';
            $sql .= '               when x.detail_count = count(y.is_deleted)  ';
            $sql .= '               then true  ';
            $sql .= '               else false  ';
            $sql .= '               end ';
            $sql .= '           )                      as is_deleted ';
            $sql .= '           , (  ';
            $sql .= '             case  ';
            $sql .= '               when x.detail_count = count(y.is_deleted) ';
            $sql .= '               then ' . Configure::read('PromiseStatus.Delete');
            $sql .= '               when sum(y.remain_count) = sum(y.quantity) ';
            $sql .= '               then ' . Configure::read('PromiseStatus.Init');
            $sql .= '               else ' . Configure::read('PromiseStatus.PartMatching');
            $sql .= '               end ';
            $sql .= '           )                      as status  ';
            $sql .= '         from ';
            $sql .= '           (  ';
            $sql .= '             select ';
            $sql .= '                   b.id ';
            $sql .= '                 , b.detail_count  ';
            $sql .= '                 , count(b.id) as count ';
            $sql .= '               from ';
            $sql .= '                 trn_promises as a  ';
            $sql .= '                 left join trn_promise_headers as b  ';
            $sql .= '                   on a.trn_promise_header_id = b.id  ';
            $sql .= '               where ';
            $sql .= '                 a.id in ('. $ids .')  ';
            $sql .= '               group by ';
            $sql .= '                 b.id ';
            $sql .= '                 , b.detail_count ';
            $sql .= '           ) as x  ';
            $sql .= '           left join trn_promises as y  ';
            $sql .= '             on x.id = y.trn_promise_header_id  ';
            $sql .= '         where ';
            $sql .= '           y.is_deleted = true  ';
            $sql .= '         group by ';
            $sql .= '           x.id ';
            $sql .= '           , x.detail_count ';
            $sql .= '           , x.count ';
            $sql .= '     ) as yy  ';
            $sql .= '   where ';
            $sql .= '     xx.id = yy.id ';
            $this->TrnPromise->query($sql);
            //コミット
            $this->TrnPromise->commit();
            // 検索処理
            $sql = $this->_PromisehistorySQL($this->request->data , $this->_getLimitCount() , true);

            $result = $this->TrnPromise->query($sql);
            $this->Session->write('Promise.result' , $result);

            $this->Session->delete('PromiseCancel.readTime');
            $this->Session->setFlash('要求の取消が完了しました','growl',array('type'=>'star'));
        }else{
            $result = $this->Session->read('Promise.result');
        }
        $this->set('result' , $result);
    }

    /**
     *
     *　@param string $keyword
     */
    function extraordinary ($keyword = null) {
        $this->setRoleFunction(5); //臨時引当要求登録

        //作業区分
        $this->set('workType',$this->getClassesList($this->Session->read('Auth.facility_id_selected'),2));
        //取引先
        $this->set('facility_List',$this->getFacilityList(
                $this->Session->read('Auth.facility_id_selected'),
                array(Configure::read('FacilityType.hospital')),
                true
        )
        );

        //作業日デフォルト
        $this->request->data['TrnPromise']['work_date'] = date('Y/m/d');
    }

    /**
     *
     *　@param string $keyword
     */
    function ex_select () {
        App::import('Sanitize');
        $this->setRoleFunction(5); //臨時引当要求登録

        if(isset($this->request->data['add_search']['is_search'])){
            $CartSearchResult = array();

            $data = $this->request->data;
            $this->request->data = Sanitize::clean($this->request->data);

            $where  = '  and a.mst_facility_id = '. $this->Session->read('Auth.facility_id_selected');

            //商品ID
            if(isset($this->request->data['MstFacilityItem']['internal_code']) && $this->request->data['MstFacilityItem']['internal_code'] !== ''){
                $where .= "  and a.internal_code = '" . $this->request->data['MstFacilityItem']['internal_code'] . "'";
            }
            //製品番号
            if(isset($this->request->data['MstFacilityItem']['item_code']) && $this->request->data['MstFacilityItem']['item_code'] !== ''){
                $where .= "  and a.item_code like '%" . $this->request->data['MstFacilityItem']['item_code'] . "%'";
            }
            //商品名
            if(isset($this->request->data['MstFacilityItem']['item_name']) && $this->request->data['MstFacilityItem']['item_name'] !== ''){
                $where .= "  and a.item_name like '%" . $this->request->data['MstFacilityItem']['item_name'] . "%'";
            }
            //規格
            if(isset($this->request->data['MstFacilityItem']['standard']) && $this->request->data['MstFacilityItem']['standard'] !== ''){
                $where .= "  and a.standard like '%" . $this->request->data['MstFacilityItem']['standard'] . "%'";
            }

            //販売元
            if(isset($this->request->data['MstFacilityItem']['dealer_name']) && $this->request->data['MstFacilityItem']['dealer_name'] !== ''){
                $where .= "  and c.dealer_name like '%" . $this->request->data['MstFacilityItem']['dealer_name'] . "%'";
            }
            //JANコード
            if(isset($this->request->data['MstFacilityItem']['jan_code']) && $this->request->data['MstFacilityItem']['jan_code'] !== ''){
                $where .= "  and a.jan_code like '" . $this->request->data['MstFacilityItem']['jan_code'] . "%'";
            }
            //登録可能のみ表示
            if(isset($this->request->data['MstFacilityItem']['canChecked']) && $this->request->data['MstFacilityItem']['canChecked']==1){
                
                $where .= " and a.start_date < '" .$this->request->data['TrnPromise']['work_date'] ."' "; //施設採用品の適用開始日以前
                $where .= " and ( a.end_date > '" .$this->request->data['TrnPromise']['work_date'] ."' or a.end_date is null )";   //施設採用品の適用終了日以降
                $where .= " and b.start_date < '" .$this->request->data['TrnPromise']['work_date'] ."' "; //包装単位の適用開始日以前
                $where .= " and ( b.end_date > '" .$this->request->data['TrnPromise']['work_date'] ."' or b.end_date is null )";   //包装単位の適用中止日以降
                
                $where .= " and a.is_deleted = false "; //施設採用品の適用フラグOFF
                $where .= " and b.is_deleted = false "; //包装単位の適用フラグOFF
                $where .= " and f.id is not null ";     //売上設定なし

            }
            if(!empty($this->request->data['cart']['tmpid'])){
                $where  .= " and b.id not in (" . $this->request->data['cart']['tmpid'] . ")";
                $where2 = " and b.id in (" . $this->request->data['cart']['tmpid'] . ")";
            }else{
                $where2 = " and 0 = 1 ";
            }

            $this->set('SearchResult' , $this->_getData($where  , $this->request->data['limit'] ));
            //全件数取得
            $this->_getData($where  , $this->request->data['limit'] , true);

            $CartSearchResultTmp  =  $this->_getData($where2 , null );
            $this->request->data = $data;

            if(!empty($this->request->data['cart']['tmpid'])){
                //カートに追加した順に並べなおす。
                foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
                    foreach($CartSearchResultTmp as $tmp){
                        if($tmp['TrnPromise']['id'] == $id){
                            $CartSearchResult[] = $tmp;
                            break;
                        }
                    }
                }
            }

            $this->set('CartSearchResult' , $CartSearchResult);
        }else{
            $this->request->data['TrnPromise']['facility_id'] = $this->getFacilityId($this->request->data['TrnPromise']['facility_code']  , array(Configure::read('FacilityType.hospital')));
        }
    }

    /**
     * 臨時要求CSV登録
     */
    function csv_register (){
        $this->setRoleFunction(5); //臨時引当要求登録
        $this->request->data['TrnPromise']['facility_id'] = $this->getFacilityId($this->request->data['TrnPromise']['facility_code'] , array(Configure::read('FacilityType.hospital')));
        //作業区分
        $workType = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),2);
        if(isset($workType[$this->request->data['TrnPromise']['work_type']])){
            $this->request->data['TrnPromise']['work_name']     = $workType[$this->request->data['TrnPromise']['work_type']];
        }else{
            $this->request->data['TrnPromise']['work_name']     = "";
        }
    }
    /**
     *
     * TSVマッピング
     */
    private function _tsvMapping($tsv_array, $map){
        $title_row = $tsv_array[0];
        $mapped_title = array();
        $mapped_tsv = array();
        foreach($title_row as $key => $title){
            if(array_key_exists ($title,$map)){
                $mapped_title[$key] = $map[$title];
            } else {
                return FALSE;
            }
        }
        foreach($tsv_array as $row_key => $row_data){
            // 不正なデータを排除(データ不足、空行)
            if($row_key != 0 && is_array($row_data) && count($row_data) == count($mapped_title)){
                $mapped_tsv[$row_key] = array_combine($mapped_title,$row_data);
            }
        }
        return $mapped_tsv;
    }

    /**
     *
     *　@param string $keyword
     */
    function ex_add_confirm () {
        $this->setRoleFunction(5); //臨時引当要求登録
        $params = array();
        $params['conditions'] = array('MstDepartment.mst_facility_id' => $this->request->data['TrnPromise']['facility_id'],
                                      'MstDepartment.department_code' => $this->request->data['TrnPromise']['department_id'],
                                      'MstDepartment.is_deleted' => FALSE);
        $params['recursive'] = -1;
        $tmp = $this->MstDepartment->find('first',$params);
        $this->request->data['TrnPromise']['department_id'] = $tmp['MstDepartment']['id'];

        //作業区分
        $workType = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),2);
        $this->set('workType',$workType);
        //CSV登録から
        if(isset($this->request->data['selected']['from_csv'])){
            //ファイルアップ
            $_upfile = $this->request->data['selected']['csv_file']['tmp_name'];
            if (is_uploaded_file($_upfile)){
                // アップロードされたファイル名を設定
                $this->CsvReadUtils->setFileName( $this->request->data['selected']['csv_file']['tmp_name'] );
            }else{
                $this->request->data['selected']['selectFas'] = $this->request->data['selected']['facility_code'];
                $this->Session->setFlash('ファイルが読み込めませんでした。CSVファイルを確認してください。', 'growl', array('type'=>'error') );
                $this->set('selectedData',$this->request->data['selected']);
                $this->render('csv_register');
                return;
            }

            // CsvReadUtils初期設定
            $this->CsvReadUtils->setLineMaxLength( 2000 );

            // CSVオープン
            $this->CsvReadUtils->open();

            // ハッシュキー設定
            $this->CsvReadUtils->hashKeyClear();
            $this->CsvReadUtils->hashKeyAdd( "facility_code" );
            $this->CsvReadUtils->hashKeyAdd( "department_code" );
            $this->CsvReadUtils->hashKeyAdd( "internal_code" );
            $this->CsvReadUtils->hashKeyAdd( "per_unit" );
            $this->CsvReadUtils->hashKeyAdd( "quantity" );
            $this->CsvReadUtils->hashKeyAdd( "recital" );

            $i = 0;
            $result = array();

            while ( ! $this->CsvReadUtils->eof() ) {
                $i++; //continueでスキップすると先頭に戻ってインクリメントされないので。
                $err = false;

                // CSV一行読込
                $line_data = $this->CsvReadUtils->readCsvLine();

                //ヘッダ行があるのでスキップ
                if( $i == 1 ) {
                    continue;
                }

                // EOFだけの行を読み込んでしまうため
                if ( $line_data == "" ) {
                    continue;
                }

                //ダミーデータ
                $tmp['TrnPromise']['id']            = null;
                $tmp['TrnPromise']['internal_code'] = null;
                $tmp['TrnPromise']['item_name']     = null;
                $tmp['TrnPromise']['item_code']     = null;
                $tmp['TrnPromise']['standard']      = null;
                $tmp['TrnPromise']['dealer_name']   = null;
                $tmp['TrnPromise']['per_unit']      = null;
                $tmp['TrnPromise']['unit_name']     = null;
                $tmp['TrnPromise']['per_unit_name'] = null;
                $tmp['TrnPromise']['stock_count']   = null;
                $tmp['TrnPromise']['alert']         = null;

                //読み取りデータチェック 商品ID
                if( $line_data['internal_code'] == "" ){
                    $result[$i] = $tmp;
                    $result[$i]['TrnPromise']['recital'] = '商品IDに間違いがあります';
                    $result[$i]['TrnPromise']['id']      = null;
                    $err = true;
                }

                //読み取りデータチェック 入り数
                if( $line_data['per_unit'] == "" ){
                    $result[$i] = $tmp;
                    $result[$i]['TrnPromise']['recital'] = '包装単位入数に間違いがあります';
                    $result[$i]['TrnPromise']['id']      = null;
                    $err = true;
                }

                //読み取りデータチェック 数量
                if( $line_data['quantity'] == "" ){
                    $result[$i] = $tmp;
                    $result[$i]['TrnPromise']['recital'] = "数量に間違いがあります";
                    $result[$i]['TrnPromise']['id']      = null;
                    $err = true;
                }

                //商品情報取得
                $check = array();
                if(!$err){
                    $check = $this->_getCsvPromiseData($line_data);
                }

                if(!empty($check)){
                    $tmp = $check;
                    $tmp['TrnPromise']['recital'] = $line_data['recital'];
                }

                $tmp['TrnPromise']['quantity'] = $line_data['quantity'];
                //施設コード部署コード
                if( ($line_data['facility_code'] !== $this->request->data['TrnPromise']['facility_code'])
                        || ($line_data['department_code'] !== $this->request->data['TrnPromise']['department_code'])){
                    $result[$i] = $tmp;
                    $result[$i]['TrnPromise']['recital'] = '要求元の病院コードまたは部署コードに間違いがあります';
                    $result[$i]['TrnPromise']['id']      = null;
                    $err = true;
                }

                //商品IDチェック
                if(!isset($check['TrnPromise']['internal_code']) && (!$err)){
                    $result[$i] = $tmp;
                    $result[$i]['TrnPromise']['recital'] = '商品IDに間違いがあります';
                    $result[$i]['TrnPromise']['id']      = null;
                    $err = true;
                }

                //包装単位チェック
                if(!isset($check['TrnPromise']['id']) && (!$err)){
                    $result[$i] = $tmp;
                    $result[$i]['TrnPromise']['recital'] = '包装単位入数に間違いがあります';
                    $result[$i]['TrnPromise']['id']      = null;
                    $err = true;
                }

                //数量チェック
                if(preg_match("/^[0-9]{1,9}$/", $line_data['quantity']) != 1 && (!$err)){
                    $result[$i] = $tmp;
                    $result[$i]['TrnPromise']['recital'] = "数量に間違いがあります";
                    $result[$i]['TrnPromise']['id']      = null;
                    $err = true;
                }

                //適用日、適用フラグチェック
                if(!$err && $check['TrnPromise']['err']){
                    $err = true;
                    $result[$i] = $tmp;
                    $result[$i]['TrnPromise']['recital'] = $check['TrnPromise']['recital'];
                    $result[$i]['TrnPromise']['id']      = null;
                }

                if(!$err){
                    //読込済みでエラーがないものと比較して、既にあるIDだったら数量を足しこみ
                    foreach($result as &$r){
                        if($r['TrnPromise']['id'] == $tmp['TrnPromise']['id']) {
                            $r['TrnPromise']['quantity'] = $r['TrnPromise']['quantity'] + $tmp['TrnPromise']['quantity'];
                            $tmp = array();
                            break;
                        }
                    }
                    unset($r);

                    //読込済みのレコードにないので、新規レコードで表示させる
                    if(!empty($tmp)) {
                        $result[] = $tmp;
                    }
                }
            }
            if(isset($workType[$this->request->data['TrnPromise']['work_type']] ) ) {
                $this->request->data['TrnPromise']['work_name'] = $workType[$this->request->data['TrnPromise']['work_type']];
            } else {
                $this->request->data['TrnPromise']['work_name'] = "";
            }
        } else {
            //商品検索から
            $resultTmp = $this->_getExAddConfirm();
            if(!empty($this->request->data['cart']['tmpid'])){
                //カートに追加した順に並べなおす。
                foreach( explode( ',', $this->request->data['cart']['tmpid']) as $id){
                    foreach($resultTmp as $tmp){
                        if($tmp['TrnPromise']['id'] == $id){
                            $result[] = $tmp;
                            break;
                        }
                    }
                }
            }

        }
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('TrnPromise.token' , $token);
        $this->request->data['TrnPromise']['token'] = $token;

        $this->set('result',$result);
    }

    /**
     *
     *　@param string $keyword
     */
    function ex_add_result () {
        $now = date('Y/m/d H:i:s');
        if($this->request->data['TrnPromise']['token'] === $this->Session->read('TrnPromise.token')) {
            $this->Session->delete('TrnPromise.token');
            $this->request->data['TrnPromise']['workType'] = $this->getClassesList($this->Session->read('Auth.facility_id_selected'),2);
            //要求先倉庫取得
            $CenterDepartmentID = $this->getDepartmentId($this->Session->read('Auth.facility_id_selected'),
                                                         array(Configure::read('DepartmentType.warehouse')));

            $HospitalDepartmentID = $this->request->data['TrnPromise']['department_id'];

            //トランザクション開始
            $this->TrnPromise->begin();
            //行ロック
            $this->TrnStock->query(' select * from trn_stocks as a where a.mst_department_id = ' . $CenterDepartmentID . ' for update');

            //作業番号取得
            $workNo = $this->setWorkNo4Header($this->request->data['TrnPromise']['work_date'],'03');
            //ヘッダー作成
            $promise['TrnPromiseHeader']['work_no']            = $workNo;
            $promise['TrnPromiseHeader']['work_date']          = $this->request->data['TrnPromise']['work_date'];
            $promise['TrnPromiseHeader']['recital']            = $this->request->data['TrnPromise']['recital'];
            $promise['TrnPromiseHeader']['promise_type']       = Configure::read('PromiseType.Temporary');
            $promise['TrnPromiseHeader']['promise_status']     = Configure::read('PromiseStatus.Init');
            $promise['TrnPromiseHeader']['department_id_from'] = $HospitalDepartmentID;
            $promise['TrnPromiseHeader']['department_id_to']   = $CenterDepartmentID;
            $promise['TrnPromiseHeader']['detail_count']       = count($this->request->data['TrnPromise']['id']);
            $promise['TrnPromiseHeader']['is_deleted']         = false;
            $promise['TrnPromiseHeader']['creater']            = $this->Session->read('Auth.MstUser.id');
            $promise['TrnPromiseHeader']['created']            = $now;
            $promise['TrnPromiseHeader']['modifier']           = $this->Session->read('Auth.MstUser.id');
            $promise['TrnPromiseHeader']['modified']           = $now;

            $this->TrnPromiseHeader->create();
            $res = $this->TrnPromiseHeader->save($promise['TrnPromiseHeader'],array('validates' => true,'atomic' => false));
            if(empty($res)){
                $this->TrnPromise->rollback();
                $this->Session->setFlash('臨時要求作成中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect("extraordinary");
            }

            $promsHeadId = $this->TrnPromiseHeader->getLastInsertID();
            $i = 1;
            foreach($this->request->data['TrnPromise']['id'] as $id){
                
                // $id // 包装単位ID
                // 低レベル品の場合、いり数1の単位に換算して要求を作成する。
                $sql  = ' select ';
                $sql .= '       a.per_unit ';
                $sql .= '     , b.id  ';
                $sql .= '     , c.is_lowlevel ';
                $sql .= '   from ';
                $sql .= '     mst_item_units as a  ';
                $sql .= '     left join mst_item_units as b  ';
                $sql .= '       on b.mst_facility_item_id = a.mst_facility_item_id  ';
                $sql .= '       and b.per_unit = 1  ';
                $sql .= '     left join mst_facility_items as c ';
                $sql .= '       on c.id = a.mst_facility_item_id ';
                $sql .= '   where ';
                $sql .= '     a.id = ' . $id;
                $ret = $this->MstFacilityItem->query($sql);
                if($ret[0][0]['is_lowlevel']){
                    // 低レベル
                    $target_id   = $ret[0][0]['id'];
                    $taget_count = $ret[0][0]['per_unit'];
                }else{
                    // 通常品
                    $target_id   = $id;
                    $taget_count = 1;
                }
                $promise['TrnPromise'][$i]['work_no']               = $workNo;
                $promise['TrnPromise'][$i]['work_seq']              = $i;
                $promise['TrnPromise'][$i]['work_date']             = $this->request->data['TrnPromise']['work_date'];
                $promise['TrnPromise'][$i]['work_type']             = $this->request->data['TrnPromise']['work_type_'][$id];
                $promise['TrnPromise'][$i]['recital']               = $this->request->data['TrnPromise']['recital_'][$id];
                $promise['TrnPromise'][$i]['promise_type']          = Configure::read('PromiseType.Temporary');
                $promise['TrnPromise'][$i]['promise_status']        = Configure::read('PromiseStatus.Init');
                $promise['TrnPromise'][$i]['mst_item_unit_id']      = $target_id;
                $promise['TrnPromise'][$i]['department_id_from']    = $HospitalDepartmentID;
                $promise['TrnPromise'][$i]['department_id_to']      = $CenterDepartmentID;
                $promise['TrnPromise'][$i]['quantity']              = $this->request->data['TrnPromise']['quantity'][$id] * $taget_count;
                $promise['TrnPromise'][$i]['before_quantity']       = $this->request->data['TrnPromise']['quantity'][$id] * $taget_count;
                $promise['TrnPromise'][$i]['remain_count']          = $this->request->data['TrnPromise']['quantity'][$id] * $taget_count;
                $promise['TrnPromise'][$i]['unreceipt_count']       = $this->request->data['TrnPromise']['quantity'][$id] * $taget_count;
                $promise['TrnPromise'][$i]['trn_promise_header_id'] = $promsHeadId;
                $promise['TrnPromise'][$i]['is_deleted']            = FALSE;
                $promise['TrnPromise'][$i]['creater']               = $this->Session->read('Auth.MstUser.id');
                $promise['TrnPromise'][$i]['created']               = $now;
                $promise['TrnPromise'][$i]['modifier']              = $this->Session->read('Auth.MstUser.id');
                $promise['TrnPromise'][$i]['modified']              = $now;

                $params = array();
                $params['conditions'] = array('TrnStock.mst_department_id' => $CenterDepartmentID,
                        'TrnStock.mst_item_unit_id'  => $id,
                        'TrnStock.is_deleted'        => false);
                $stockRes = $this->TrnStock->find('first',$params);
                //センター在庫
                if (!$this->TrnStock->updateAll(array(  'TrnStock.promise_count' => 'TrnStock.promise_count + ' . $this->request->data['TrnPromise']['quantity'][$id] * $taget_count ,
                                                        'TrnStock.is_deleted'    => 'false',
                                                        'TrnStock.modifier'      =>  $this->Session->read('Auth.MstUser.id'),
                                                        'TrnStock.modified'      =>  "'" . $now . "'"
                                                        )
                                                ,
                                                array('TrnStock.id'  => $this->getStockRecode($target_id ,$CenterDepartmentID))
                                                ,
                                                -1
                                                )
                    ) {
                    $this->TrnPromise->rollback();
                    $this->Session->setFlash('臨時要求作成中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect("extraordinary");
                }
                $i++;
            }
            $this->TrnPromise->create();
            $res2 = $this->TrnPromise->saveAll($promise['TrnPromise'],array('validates' => true,'atomic' => false));
            if( (empty($res2) ) ){
                $this->TrnPromise->rollback();
                $this->Session->setFlash('臨時要求作成中にエラーが発生しました', 'growl', array('type'=>'error') );
                $this->redirect("extraordinary");
            }
            $this->TrnStock->create();
            if(!empty($stock)){
                $res3 = $this->TrnStock->saveAll($stock,array('validates' => true,'atomic' => false));
                if(empty($res3) ){
                    $this->TrnPromise->rollback();
                    $this->Session->setFlash('臨時要求作成中にエラーが発生しました', 'growl', array('type'=>'error') );
                    $this->redirect("extraordinary");
                }
            }
            $this->TrnPromise->commit();
            $this->request->data['TrnPromise']['work_no'] = $workNo;

            //結果表示用
            $result = $this->_getExAddResult();

            $this->set('result' , $result);
            $this->Session->write('TrnPromise.result', $result);
            $this->Session->write('TrnPromise.data', $this->request->data);

        }else{
            $this->set('result' , $this->Session->read('TrnPromise.result'));
            $this->request->data = $this->Session->read('TrnPromise.data');
        }
    }

    /**
     * 引当確定
     * @param
     */
    function decision () {
        $this->setRoleFunction(7); //引当確定
        
        $this->request->data['TrnPromise']['afterPromise'] = '';
        $this->request->data['TrnPromise']['alertMsg']     = '';
        
        $now    = date('Y/m/d H:i:s');
        $userId = $this->Session->read('Auth.MstUser.id');

        $this->set('headFas',$this->Session->read('Auth.facility_id_selected'));
        $this->set('user_id',$this->Session->read('Auth.MstUser.id'));

        //施設取得
        $this->set('facility_List',$this->getFacilityList(
                $this->Session->read('Auth.facility_id_selected'),
                array(Configure::read('FacilityType.hospital')),
                true
               )
        );
        //部署コンボ
        $department_List = array();
        if(isset($this->request->data['TrnPromise']['facilityCode']) && $this->request->data['TrnPromise']['facilityCode'] != ""){
            $department_List = $this->getDepartmentsList(
                    $this->request->data['TrnPromise']['facilityCode']
                    , true
                );
        }
        $this->set('department_List' , $department_List);

        //初期値
        if(!isset($this->request->data['TrnPromise']['work_date2'] ) ) $this->request->data['TrnPromise']['work_date2'] = date('Y/m/d');
        //検索
        if(isset($this->request->data['selected']['mode']) && $this->request->data['selected']['mode'] == "1"){

            // トランザクション開始
            $this->TrnFixedPromise->begin();
            
            // 要求データ確認
            $sql  = ' select ';
            $sql .= '       a.mst_item_unit_id ';
            $sql .= '   from ';
            $sql .= '     trn_promises as a  ';
            $sql .= '   where ';
            $sql .= '     a.is_deleted = false ';
            $sql .= '     and a.remain_count > 0 ';
            $sql .= '   group by ';
            $sql .= '     a.mst_item_unit_id ';
            
            $ret = $this->TrnPromise->query($sql);
            $ids = '';
            foreach($ret as $r){
                if($ids != ''){
                    $ids = $ids . ' , ';
                }
                $ids = $ids . $r[0]['mst_item_unit_id'];
            }
            
            // 行ロック
            $this->TrnStock->query(' select * from trn_stocks as a where a.mst_item_unit_id in (' . $ids . ') and a.mst_department_id = ' . $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') , Configure::read('DepartmentType.warehouse') ) . ' for update ');

            /* 更新チェック */
            $ret = $this->TrnStock->query(' select count(*) from trn_stocks as a where a.mst_department_id = ' . $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') , Configure::read('DepartmentType.warehouse') ) . " and a.modified > '" .$this->request->data['promise']['time'] . "'");
            if($ret[0][0]['count'] > 0){
                $this->Session->setFlash('ほかユーザによって更新されています。最初からやり直してください。', 'growl', array('type'=>'error') );
                $this->redirect('decision');
            }

            /* 処理前の残数を取得 */
            $preRemainCount = $this->_getRemainCount($this->request->data );

            //検索
            $where = '';
            
            //施設が選択されている場合
            if(!empty($this->request->data['TrnPromise']['facilityCode'])){
                $where .= " and MstFacility.facility_code = '" . $this->request->data['TrnPromise']['facilityCode'] . "'"; 
            }
            
            //部署
            if(!empty($this->request->data['TrnPromise']['departmentCode']) ){
                $where .= " and MstDepartment.department_code = '" . $this->request->data['TrnPromise']['departmentCode'] . "'";
            }

            //要求区分
            if($this->request->data['TrnPromise']['promise_type'] != ""){
                $where .= ' and TrnPromise.promise_type = ' . $this->request->data['TrnPromise']['promise_type'];
            }
            
            //要求日
            if(!empty($this->request->data['TrnPromise']['work_date1']) ){
                $where .= " and TrnPromise.work_date >= '" .  $this->request->data['TrnPromise']['work_date1'] . "'";
            }
            if(!empty($this->request->data['TrnPromise']['work_date2']) ){
                $where .= " and TrnPromise.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
            }
            
            //要求データ一覧を取得
            $sql  = ' select ';
            $sql .= '       TrnPromise.id                        as "TrnPromise__id" ';
            $sql .= '     , TrnPromise.work_no                   as "TrnPromise__work_no" ';
            $sql .= '     , TrnPromise.work_seq                  as "TrnPromise__work_seq" ';
            $sql .= '     , TrnPromise.work_date                 as "TrnPromise__work_date" ';
            $sql .= '     , TrnPromise.work_type                 as "TrnPromise__work_type" ';
            $sql .= '     , TrnPromise.recital                   as "TrnPromise__recital" ';
            $sql .= '     , TrnPromise.promise_type              as "TrnPromise__promise_type" ';
            $sql .= '     , TrnPromise.promise_status            as "TrnPromise__promise_status" ';
            $sql .= '     , TrnPromise.mst_item_unit_id          as "TrnPromise__mst_item_unit_id" ';
            $sql .= '     , TrnPromise.department_id_from        as "TrnPromise__department_id_from" ';
            $sql .= '     , TrnPromise.department_id_to          as "TrnPromise__department_id_to" ';
            $sql .= '     , TrnPromise.before_quantity           as "TrnPromise__before_quantity" ';
            $sql .= '     , TrnPromise.quantity                  as "TrnPromise__quantity" ';
            $sql .= '     , TrnPromise.remain_count              as "TrnPromise__remain_count" ';
            $sql .= '     , TrnPromise.fixed_count               as "TrnPromise__fixed_count" ';
            $sql .= '     , TrnPromise.stock_count               as "TrnPromise__stock_count" ';
            $sql .= '     , TrnPromise.reserver_count            as "TrnPromise__reserver_count" ';
            $sql .= '     , TrnPromise.requested_count           as "TrnPromise__requested_count" ';
            $sql .= '     , TrnPromise.is_deleted                as "TrnPromise__is_deleted" ';
            $sql .= '     , TrnPromise.creater                   as "TrnPromise__creater" ';
            $sql .= '     , TrnPromise.created                   as "TrnPromise__created" ';
            $sql .= '     , TrnPromise.modifier                  as "TrnPromise__modifier" ';
            $sql .= '     , TrnPromise.modified                  as "TrnPromise__modified" ';
            $sql .= '     , TrnPromise.receipt_count             as "TrnPromise__receipt_count" ';
            $sql .= '     , TrnPromise.trn_promise_header_id     as "TrnPromise__trn_promise_header_id" ';
            $sql .= '     , TrnPromise.consume_count             as "TrnPromise__consume_count" ';
            $sql .= '     , TrnPromise.unreceipt_count           as "TrnPromise__unreceipt_count" ';
            $sql .= '     , TrnPromise.fixed_num_used            as "TrnPromise__fixed_num_used" ';
            $sql .= '     , TrnPromise.center_facility_id        as "TrnPromise__center_facility_id" ';
            $sql .= '     , TrnStock.id                          as "TrnStock__id" ';
            $sql .= '     , TrnStock.stock_count                 as "TrnStock__stock_count" ';
            $sql .= '     , TrnStock.reserve_count               as "TrnStock__reserve_count" ';
            $sql .= '     , TrnStock.mst_item_unit_id            as "TrnStock__mst_item_unit_id" ';
            $sql .= '     , MstDepartment.priority               as "MstDepartment__priority" ';
            $sql .= '     , TrnPromiseHeader.id                  as "TrnPromiseHeader__id"  ';
            $sql .= '   from ';
            $sql .= '     trn_promises as TrnPromise  ';
            $sql .= '     left join trn_promise_headers as TrnPromiseHeader  ';
            $sql .= '       on TrnPromiseHeader.id = TrnPromise.trn_promise_header_id ';
            $sql .= '     left join mst_departments as MstDepartment  ';
            $sql .= '       on TrnPromiseHeader.department_id_from = MstDepartment.id ';
            $sql .= '     left join mst_facilities as MstFacility  ';
            $sql .= '       on MstFacility.id = MstDepartment.mst_facility_id ';
            $sql .= '     left join trn_stocks as TrnStock  ';
            $sql .= '       on TrnPromise.department_id_to = TrnStock.mst_department_id  ';
            $sql .= '         and TrnPromise.mst_item_unit_id = TrnStock.mst_item_unit_id  ';
            $sql .= '         and TrnStock.is_deleted = false ';
            $sql .= '     inner join mst_item_units as MstItemUnit  ';
            $sql .= '       on MstItemUnit.id = TrnStock.mst_item_unit_id  ';
            $sql .= '         and MstItemUnit.is_deleted = false ';
            $sql .= '     left join mst_facility_items as MstFacilityItem';
            $sql .= '       on MstFacilityItem.id = MstItemUnit.mst_facility_item_id';
            $sql .= '   where ';
            $sql .= '     MstFacilityItem.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
            $sql .= '     and TrnPromise.promise_status in (' . Configure::read('PromiseStatus.Init') . ',' . Configure::read('PromiseStatus.PartMatching') . ')  ';
            $sql .= '     and TrnPromise.is_deleted = false  ';
            $sql .= '     and TrnPromise.remain_count >= 1  ';
            $sql .= '     and TrnStock.stock_count - TrnStock.reserve_count >= 1 ';
            $sql .= $where;
            $sql .= '   order by MstDepartment.priority desc ,TrnPromise.work_date asc , TrnPromise.promise_type desc , TrnPromise.department_id_from ';
            
            $proms = $this->TrnPromise->query($sql);
            
            $today = date('Y/m/d');

            //一度取得したものを一次配列へ
            $stockCount = $targetProms = $targetPromsHead = array();

            $countsOfProms = count($proms);
            for ($_i = 0; $_i < $countsOfProms;$_i++) {
                $stockCount[$proms[$_i]['TrnStock']['id']]  = (int)$proms[$_i]['TrnStock']['stock_count'] - (int)$proms[$_i]['TrnStock']['reserve_count'];
                $targetPromsHead[]                        = $proms[$_i]['TrnPromiseHeader']['id'];
                $targetProms[$_i]                         = $proms[$_i]['TrnPromise'];
                $targetProms[$_i]['trn_stock_id']         = $proms[$_i]['TrnStock']['id'];
                $targetProms[$_i]['trn_stock_miu_id']     = $proms[$_i]['TrnStock']['mst_item_unit_id'];
            }
            unset($proms);

            //ソート処理
            if(!empty($targetProms) ){
                //要求が1件以上あれば引当処理を行う。
                $fpC = 0;
                //引当確定処理
                $fixedProms = $fixedPromsH = array();

                //対象要求
                $countsOfTargets  = count($targetProms);
                for ($i = 0; $i < $countsOfTargets;$i++) {
                    if( (int)$stockCount[$targetProms[$i]['trn_stock_id']] >= (int)$targetProms[$i]['remain_count'] ){
                        //要求数より在庫が多い場合。
                        $stockCount[$targetProms[$i]['trn_stock_id']] = (int)$stockCount[$targetProms[$i]['trn_stock_id']] - (int)$targetProms[$i]['remain_count'];
                        //要求確定データ
                        $fixedPromsQuantity = $targetProms[$i]['remain_count'];
                        //要求データ
                        $targetProms[$i]['promise_status'] = Configure::read('PromiseStatus.Matching'); //引当済み
                        $targetProms[$i]['remain_count']   = 0;
                        $targetProms[$i]['modifier']       = $userId;
                        $targetProms[$i]['modified']       = $now;
                    }else{
                        //要求数が在庫より多い場合

                        //要求確定データ
                        $fixedPromsQuantity = $stockCount[$targetProms[$i]['trn_stock_id']];

                        if ($fixedPromsQuantity > 0) {
                            $stockCount[$targetProms[$i]['trn_stock_id']] = 0; //在庫数0に
                            //要求データ変更
                            $targetProms[$i]['promise_status'] = configure::read('PromiseStatus.PartMatching'); //一部引当
                            $targetProms[$i]['remain_count']   = (int)$targetProms[$i]['remain_count'] - (int)$fixedPromsQuantity; //要求の残数減らす
                            $targetProms[$i]['modifier']       = $userId;
                            $targetProms[$i]['modified']       = $now;
                        }
                    }
                    //要求確定のデータ作成
                    if( $fixedPromsQuantity != 0 ){ //引当数が0になる場合処理をしない。
                        $fixedProms[$fpC]['work_date']                   = $today;
                        $fixedProms[$fpC]['work_type']                   = "";
                        $fixedProms[$fpC]['recital']                     = "";
                        $fixedProms[$fpC]['fixed_promise_status']        = Configure::read('PromiseStatus.Init');
                        $fixedProms[$fpC]['mst_item_unit_id']            = $targetProms[$i]['mst_item_unit_id'];
                        $fixedProms[$fpC]['department_id_from']          = $targetProms[$i]['department_id_from'];
                        $fixedProms[$fpC]['department_id_to']            = $targetProms[$i]['department_id_to'];
                        $fixedProms[$fpC]['quantity']                    = $fixedPromsQuantity;
                        $fixedProms[$fpC]['trn_promise_id']              = $targetProms[$i]['id'];
                        $fixedProms[$fpC]['remain_count']                = $fixedPromsQuantity;
                        $fixedProms[$fpC]['promise_type']                = $targetProms[$i]['promise_type'];
                        $fixedProms[$fpC]['is_deleted']                  = FALSE;
                        $fixedProms[$fpC]['created']                     = $now;
                        $fixedProms[$fpC]['creater']                     = $userId;
                        $fixedProms[$fpC]['modified']                    = $now;
                        $fixedProms[$fpC]['modifier']                    = $userId;
                        $fpC++;
                    }
                }
                //更新データがある場合
                if( (count($fixedProms) !== 0)){
                    //ヘッダ作成
                    $beforeDepartFrom = null;
                    $beforeDepartTo   = null;
                    $workNo = '';
                    $countsOfFp = count($fixedProms);
                    $fphC = 0;
                    $seq = 1;
                    $stockRes = TRUE;
                    $workNoArray = array();
                    for ($i = 0; $i < $countsOfFp;$i++) {
                        if( $beforeDepartFrom != $fixedProms[$i]['department_id_from'] ) {
                            if($fphC!=0){
                                $fixedPromsH[($fphC-1)]['detail_count']         = $seq;
                            }
                            //作業番号
                            $workNo = $this->setWorkNo4Header($today,'04');
                            $workNoArray[]                              = $workNo;
                            $fixedPromsH[$fphC]['work_no']              = $workNo;
                            $fixedPromsH[$fphC]['work_date']            = $today;
                            $fixedPromsH[$fphC]['recital']              = "";
                            $fixedPromsH[$fphC]['department_id_from']   = $fixedProms[$i]['department_id_from'];
                            $fixedPromsH[$fphC]['department_id_to']     = $fixedProms[$i]['department_id_to'];
                            $fixedPromsH[$fphC]['fixed_promise_status'] = Configure::read('PromiseStatus.Init');
                            $fixedPromsH[$fphC]['is_deleted']           = FALSE;
                            $fixedPromsH[$fphC]['created']              = $now;
                            $fixedPromsH[$fphC]['creater']              = $userId;
                            $fixedPromsH[$fphC]['modified']             = $now;
                            $fixedPromsH[$fphC]['modifier']             = $userId;
                            //次の情報をセット
                            $beforeDepartFrom = $fixedProms[$i]['department_id_from'];
                            $beforeDepartTo   = $fixedProms[$i]['department_id_to'];
                            $detailC = $seq   = 0;
                            $fphC++;
                        }
                        //明細作成
                        $fixedProms[$i]['work_no']                     = $workNo;
                        $fixedProms[$i]['work_seq']                    = (int)$seq + 1;
                        //在庫更新
                        $params = array();
                        $params['recursive'] = -1;
                        $params['conditions'] = array('TrnStock.mst_department_id'=> $fixedProms[$i]['department_id_to'],
                                'TrnStock.mst_item_unit_id' => $fixedProms[$i]['mst_item_unit_id'],
                                'TrnStock.is_deleted'       => FALSE);
                        $tStock = $this->TrnStock->find('first',$params);

                        $tStock['TrnStock']['promise_count'] = (int)$tStock['TrnStock']['promise_count'] - (int)$fixedProms[$i]['quantity'];
                        $tStock['TrnStock']['reserve_count'] = (int)$tStock['TrnStock']['reserve_count'] + (int)$fixedProms[$i]['quantity'];
                        $tStock['TrnStock']['modifier']      = $userId;
                        $tStock['TrnStock']['modified']      = $now;
                        $this->TrnStock->create();
                        $preRes = $this->TrnStock->save($tStock);

                        if($preRes === FALSE){
                            $stockRes = FALSE;
                        }
                        $seq++;
                    }

                    //最終行は、ループ内で処理されないので。
                    $fixedPromsH[($fphC-1)]['detail_count'] = $seq;

                    $this->TrnPromise->create();
                    $this->TrnPromise->recursive = -1;
                    $res  = $this->TrnPromise->saveAll($targetProms,array('validate' => true,'atomic' => false));
                    $this->TrnFixedPromiseHeader->recursive = -1;
                    $res3 = $this->TrnFixedPromiseHeader->saveAll($fixedPromsH,array('validate' => true,'atomic' => false));

                    //確定ヘッダのID取得
                    $params = array();
                    $params['conditions'] = array('TrnFixedPromiseHeader.work_no' => $workNoArray);
                    $params['fields'] = array('TrnFixedPromiseHeader.work_no','TrnFixedPromiseHeader.id');
                    $workAndId = $this->TrnFixedPromiseHeader->find('list',$params);
                    for ($i = 0; $i < $countsOfFp;$i++) {
                        $fixedProms[$i]['trn_fixed_promise_header_id'] = $workAndId[$fixedProms[$i]['work_no']];
                    }
                    $this->TrnFixedPromise->create();
                    $res2 = $this->TrnFixedPromise->saveAll($fixedProms,array('validates' => true,'atomic' => false));
                    //要求ヘッダ更新
                    $targetPromsHead = array_unique($targetPromsHead);
                    $sql  = ' update trn_promise_headers xx  ';
                    $sql .= '   set ';
                    $sql .= '     promise_status = yy.status  ';
                    $sql .= '    ,modifier       = ' . $userId;
                    $sql .= "    ,modified       = '" . $now . "'";
                    $sql .= '   from ';
                    $sql .= '     (  ';
                    $sql .= '       select ';
                    $sql .= '             id ';
                    $sql .= '           , (  ';
                    $sql .= '             case  ';
                    $sql .= '               when detail_count = status5  ';
                    $sql .= '               then ' . Configure::read('PromiseStatus.Delete');
                    $sql .= '               when detail_count - status5 = status1  ';
                    $sql .= '               then ' . Configure::read('PromiseStatus.Init');
                    $sql .= '               when detail_count - status5 = status3  ';
                    $sql .= '               then ' . Configure::read('PromiseStatus.Matching');
                    $sql .= '               else ' . Configure::read('PromiseStatus.PartMatching');
                    $sql .= '               end ';
                    $sql .= '           ) as status  ';
                    $sql .= '         from ';
                    $sql .= '           (  ';
                    $sql .= '             select ';
                    $sql .= '                   a.id ';
                    $sql .= '                 , a.detail_count::int8 ';
                    $sql .= '                 , sum(  ';
                    $sql .= '                   case  ';
                    $sql .= '                     when b.promise_status = ' . Configure::read('PromiseStatus.Init');
                    $sql .= '                     then 1  ';
                    $sql .= '                     else 0  ';
                    $sql .= '                     end ';
                    $sql .= '                 )                      as status1 ';
                    $sql .= '                 , sum(  ';
                    $sql .= '                   case  ';
                    $sql .= '                     when b.promise_status = ' . Configure::read('PromiseStatus.PartMatching');
                    $sql .= '                     then 1  ';
                    $sql .= '                     else 0  ';
                    $sql .= '                     end ';
                    $sql .= '                 )                      as status2 ';
                    $sql .= '                 , sum(  ';
                    $sql .= '                   case  ';
                    $sql .= '                     when b.promise_status = ' . Configure::read('PromiseStatus.Matching');
                    $sql .= '                     then 1  ';
                    $sql .= '                     else 0  ';
                    $sql .= '                     end ';
                    $sql .= '                 )                      as status3 ';
                    $sql .= '                 , sum(  ';
                    $sql .= '                   case  ';
                    $sql .= '                     when b.promise_status = ' . Configure::read('PromiseStatus.Delete');
                    $sql .= '                     then 1  ';
                    $sql .= '                     else 0  ';
                    $sql .= '                     end ';
                    $sql .= '                 )                      as status5  ';
                    $sql .= '               from ';
                    $sql .= '                 trn_promise_headers as a  ';
                    $sql .= '                 left join trn_promises as b  ';
                    $sql .= '                   on b.trn_promise_header_id = a.id  ';
                    $sql .= '               where ';
                    $sql .= '                 a.id in ( '. join(',' , $targetPromsHead) . ' ) ';
                    $sql .= '               group by ';
                    $sql .= '                 a.id ';
                    $sql .= '                 , a.detail_count ';
                    $sql .= '           ) as x ';
                    $sql .= '     ) as yy  ';
                    $sql .= '   where ';
                    $sql .= '     xx.id = yy.id ';
                    $this->TrnPromiseHeader->query($sql);

                    // コミット前に登録前画面を開いた時間以降関係する在庫レコードが更新されていないか確認
                    // => 更新されていたら、他の人が更新 or 2重登録なので rollback する。
                    $checkResult = $this->checkStockRecode($this->request->data , $now);
                    if(!$checkResult){
                        $this->Session->setFlash('処理中のデータに更新がかけられました。', 'growl', array('type'=>'error') );
                        $this->TrnFixedPromise->rollback();
                        $this->redirect('decision');
                    }

                    if( (!empty($res) ) && (!empty($res2) ) && (!empty($res3) ) && ($stockRes === TRUE) ){
                        $this->TrnFixedPromise->commit();
                    }else{
                        $this->TrnFixedPromise->rollback();
                        $this->Session->setFlash('要求確定処理中にエラーが発生しました', 'growl', array('type'=>'error') );
                        $this->redirect('decision');
                    }

                }

            }else{
                $this->TrnFixedPromise->rollback();
            }

            /* 処理後の残数を取得 */
            $afterRemainCount = $this->_getRemainCount($this->request->data);

            $this->request->data['TrnPromise']['afterPromise'] = '要求シール：' . $preRemainCount.'枚中'. $afterRemainCount.'枚の未引当が発生しました';
            if($this->checkAlert($this->request->data)){
                $this->request->data['TrnPromise']['alertMsg'] = '組換後、引当可能な物品が存在します。';
            }
        }

        //未引当一覧
        if(isset($this->request->data['selected']['mode']) ){
            $result = array();
            if( $this->request->data['selected']['mode'] == "2" ){
                $result = $this->_StockOutList();
            }
            if( $this->request->data['selected']['mode'] == "3" ){
                $result = $this->_StockOutList($this->request->data['TrnPromise']['departmentCode']);
            }
            if( $this->request->data['selected']['mode'] == "4" ){
                $result = $this->_StockOutListHp($this->request->data['TrnPromise']['facilityCode'],1);
            }
            $this->set('result' , $result );
        }
    }
    /**
     * 引当確定履歴
     * @param
     */
    function decision_history(){
        $this->setRoleFunction(8); //引当確定履歴

        //作業区分
        $this->set('workType',$this->getClassesList($this->Session->read('Auth.facility_id_selected'),4));
        $department_List = array();
        //施設取得
        $this->set('facility_List', $this->getFacilityList(
            $this->Session->read('Auth.facility_id_selected'),
            array(Configure::read('FacilityType.hospital')),
            true
            
            )
         );
        //部署コンボ
        if(isset($this->request->data['TrnFixedPromiseHeader']['facilityCode']) && $this->request->data['TrnFixedPromiseHeader']['facilityCode'] != ""){
            $department_List = $this->getDepartmentsList(
                    $this->request->data['TrnFixedPromiseHeader']['facilityCode']
                    , true
                   );
        }
        $this->set('department_List' , $department_List);
        //初期化
        $result = array();
        //検索
        if(isset($this->request->data['TrnFixedPromise']['is_search'])){
            //検索ボタンが押されたら

            //全件取得
            $this->_FixedPromisehistorySQL($this->request->data , $this->request->data['TrnFixedPromise']['limit'] , null , true );
            //表示データ取得
            $result = $this->TrnFixedPromise->query($this->_FixedPromisehistorySQL($this->request->data , $this->request->data['TrnFixedPromise']['limit'] ));
        }else{
            //初期画面
            //開始日
            $this->request->data['TrnFixedPromiseHeader']['work_date1'] = date('Y/m/d' , strtotime("-7 day"));
            //終了日
            $this->request->data['TrnFixedPromiseHeader']['work_date2'] = date('Y/m/d');
            $this->deleteSortInfo();
        }
        $this->set('result',$result);
        $this->render('/promises/decision_history/');
    }

    /**
     * 引当取消確認
     * @param
     */
    function decision_cancel_confirm(){
        //検索
        $result = $this->TrnFixedPromise->query($this->_FixedPromisehistorySQL($this->request->data , $this->request->data['TrnFixedPromise']['limit'] , true));

        $this->set('result',$result);
        // 2度押し対策用にトランザクショントークンを作る
        mt_srand((double)microtime()*1000000);
        $token = md5((string)mt_rand());
        $this->Session->write('Promise.token' , $token);
        $this->request->data['Promise']['token'] = $token;
    }

    /**
     * 引当取消処理
     * @param
     */
    function decision_cancel_result(){
        // POSTデータと、Session内のトランザクショントークンを比較し、同じだったら登録処理を行う。
        if($this->request->data['Promise']['token'] === $this->Session->read('Promise.token')) {
            $this->Session->delete('Promise.token');
            $readTime = $this->Session->read('PromiseCancel.readTime');
            $now      = date('Y/m/d H:i:s');

            //引当明細IDから、必要情報を取得
            $sql  = ' select ';
            $sql .= '       a.id                 as "TrnFixedPromise__id" ';
            $sql .= '     , a.remain_count       as "TrnFixedPromise__remain_count" ';
            $sql .= '     , a.quantity           as "TrnFixedPromise__quantity" ';
            $sql .= '     , b.id                 as "TrnPromise__id" ';
            $sql .= '     , b.remain_count       as "TrnPromise__remain_count" ';
            $sql .= '     , b.quantity           as "TrnPromise__quantity" ';
            $sql .= '     , c.id                 as "TrnStock__id" ';
            $sql .= '     , c.promise_count      as "TrnStock__promise_count" ';
            $sql .= '     , c.reserve_count      as "TrnStock__reserve_count"  ';
            $sql .= '   from ';
            $sql .= '     trn_fixed_promises as a  ';
            $sql .= '     left join trn_promises as b  ';
            $sql .= '       on b.id = a.trn_promise_id  ';
            $sql .= '     left join trn_stocks as c  ';
            $sql .= '       on c.mst_item_unit_id = a.mst_item_unit_id  ';
            $sql .= '       and a.department_id_to = c.mst_department_id  ';
            $sql .= '   where ';
            $sql .= '     a.id in (' .join(',',$this->request->data['TrnFixedPromise']['id']). ')';

            $result_data = $this->TrnFixedPromise->query($sql);

            //トランザクション開始
            $this->TrnFixedPromise->begin();
            // 行ロック
            $this->TrnFixedPromise->query(' select * from trn_fixed_promises as a where a.id in (' .join(',',$this->request->data['TrnFixedPromise']['id']). ') for update ');

            foreach( $result_data as $data ){
                //在庫更新
                if (!$this->TrnStock->updateAll(array(  'TrnStock.promise_count' => 'TrnStock.promise_count + ' . $data['TrnFixedPromise']['remain_count'],
                                                        'TrnStock.reserve_count' => 'TrnStock.reserve_count - ' . $data['TrnFixedPromise']['remain_count'],
                                                        'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                                                        'TrnStock.modified'      => "'" .$now . "'"
                                                        )
                                                , array('TrnStock.id'  => $data['TrnStock']['id'],)
                                                ,-1
                                                )
                    ) {
                    $this->TrnFixedPromise->rollback();
                    return false;
                }

                //要求明細更新
                if (!$this->TrnPromise->updateAll(array(  'TrnPromise.remain_count'    => 'TrnPromise.remain_count + ' . $data['TrnFixedPromise']['remain_count'],
                                                          'TrnPromise.promise_status'  => (($data['TrnPromise']['quantity'] == $data['TrnPromise']['remain_count']+$data['TrnFixedPromise']['remain_count'])?Configure::read('PromiseStatus.Init'):Configure::read('PromiseStatus.PartMatching')) ,
                                                          'TrnPromise.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                                          'TrnPromise.modified'        => "'" .$now . "'"
                                                          )
                                                  , array('TrnPromise.id'  => $data['TrnPromise']['id'],)
                                                  ,-1
                                                  )
                    ) {
                    $this->TrnFixedPromise->rollback();
                    return false;
                }

                //引当明細更新
                if (!$this->TrnFixedPromise->updateAll(array(  'TrnFixedPromise.remain_count'         => 0,
                                                               'TrnFixedPromise.quantity'             => 'TrnFixedPromise.quantity - ' . $data['TrnFixedPromise']['remain_count'],
                                                               'TrnFixedPromise.fixed_promise_status' => (($data['TrnFixedPromise']['remain_count'] == $data['TrnFixedPromise']['quantity'])?Configure::read('PromiseStatus.Delete'):Configure::read('PromiseStatus.SealIssue')),
                                                               'TrnFixedPromise.is_deleted'           => (($data['TrnFixedPromise']['remain_count'] == $data['TrnFixedPromise']['quantity'])?'true':'false'),
                                                               'TrnFixedPromise.modifier'             => $this->Session->read('Auth.MstUser.id'),
                                                               'TrnFixedPromise.modified'             => "'" .$now . "'"
                                                               )
                                                       , array('TrnFixedPromise.id'  => $data['TrnFixedPromise']['id'],)
                                                       ,-1
                                                       )
                    ) {
                    $this->TrnFixedPromise->rollback();
                    return false;
                }

            }
            //引当ヘッダ更新
            $sql  = ' update trn_fixed_promise_headers x  ';
            $sql .= '   set ';
            $sql .= '       is_deleted           = y.is_deleted ';
            $sql .= '     , fixed_promise_status = y.status ';
            $sql .= '     , modifier             = ' .  $this->Session->read('Auth.MstUser.id');
            $sql .= "     , modified             = '" .$now . "'" ;
            $sql .= '   from ';
            $sql .= '     (  ';
            $sql .= '       select ';
            $sql .= '             b.id ';
            $sql .= '           , (  ';
            $sql .= '             case  ';
            $sql .= '               when b.detail_count = count(a.*)  ';
            $sql .= '               then ' . Configure::read('PromiseStatus.Delete');
            $sql .= '               else ' . Configure::read('PromiseStatus.Matching');
            $sql .= '               end ';
            $sql .= '           )                                   as status ';
            $sql .= '           , (  ';
            $sql .= '             case  ';
            $sql .= '               when b.detail_count = count(a.*)  ';
            $sql .= '               then true  ';
            $sql .= '               else false  ';
            $sql .= '               end ';
            $sql .= '           )                                   as is_deleted  ';
            $sql .= '         from ';
            $sql .= '           trn_fixed_promises as a  ';
            $sql .= '           left join trn_fixed_promise_headers as b  ';
            $sql .= '             on b.id = a.trn_fixed_promise_header_id  ';
            $sql .= '         where ';
            $sql .= '           a.id in (' . join(',' , $this->request->data['TrnFixedPromise']['id']) . ') ';
            $sql .= '         group by ';
            $sql .= '           b.id ';
            $sql .= '           , b.detail_count ';
            $sql .= '     ) as y  ';
            $sql .= '   where ';
            $sql .= '     x.id = y.id ';
            $this->TrnFixedPromiseHeader->query($sql);

            //要求ヘッダ更新
            $sql  = ' update trn_promise_headers x ';
            $sql .= '   set ';
            $sql .= '       promise_status = y.status ';
            $sql .= '     , modifier             = ' .  $this->Session->read('Auth.MstUser.id');
            $sql .= "     , modified             = '" .$now . "'" ;
            $sql .= '   from ';
            $sql .= '     (  ';
            $sql .= '       select ';
            $sql .= '             c.id ';
            $sql .= '           , (  ';
            $sql .= '             case  ';
            $sql .= '               when c.detail_count = count(b.*)  ';
            $sql .= '               then ' . Configure::read('PromiseStatus.Init');
            $sql .= '               else ' . Configure::read('PromiseStatus.PartMatching');
            $sql .= '               end ';
            $sql .= '           )                             as status  ';
            $sql .= '         from ';
            $sql .= '           trn_fixed_promises as a  ';
            $sql .= '           left join trn_promises as b  ';
            $sql .= '             on b.id = a.trn_promise_id  ';
            $sql .= '           left join trn_promise_headers as c  ';
            $sql .= '             on c.id = b.trn_promise_header_id  ';
            $sql .= '         where ';
            $sql .= '           a.id in ('.join(',' , $this->request->data['TrnFixedPromise']['id'] ).')  ';
            $sql .= '         group by ';
            $sql .= '           c.id ';
            $sql .= '           , c.detail_count ';
            $sql .= '     ) as y  ';
            $sql .= '   where ';
            $sql .= '     x.id = y.id ';
            $this->TrnPromiseHeader->query($sql);

            //コミット直前に更新チェック
            $checkResult = $this->checkStockRecode($this->request->data , $now);
            if(!$checkResult){
                $this->Session->setFlash('処理中のデータに更新がかけられました。', 'growl', array('type'=>'error') );
                $this->TrnPromise->rollback();
                $this->redirect('decision_history');
            }

            //表示用データ取得
            $this->TrnPromise->commit();
            $result = $this->TrnFixedPromise->query($this->_FixedPromisehistorySQL($this->request->data , 0 , true));
            $this->Session->write('Ptomise.result', $result);

            $this->Session->setFlash('要求確定の取消が完了しました','growl',array('type'=>'star'));
        }else {
            $result = $this->Session->read('Ptomise.result');
        }
        $this->set('result' ,$result);
    }

    /**
     * 引当取消結果画面
     * @param
     */
    function __decision_cancel_result(){
        $displayData = $this->Session->read('fixedPromiseCancel');
        if(empty($displayData)){
            //TODO エラー処理
            $this->redirect('decision_history');
        }
        $this->Session->delete('fixedPromiseCancel');
        $this->set('displayData',$displayData);
        unset($displayData);
    }

    private function _PromisehistorySQL($data , $limit , $detail = null , $count = false ) {
        $this->request->data = $data;
        App::import('Sanitize');
        // 検索処理

        $where = "";
        //ヘッダID
        if( (!empty($this->request->data['TrnPromiseHeaders']['id']) && $detail ) ){
            $where .=' and TrnPromise.trn_promise_header_id in (' . implode ( ',' , $this->request->data['TrnPromiseHeaders']['id'] ) .')';
        }
        //明細ID
        if( (!empty($this->request->data['TrnPromise']['id']) ) ){
            $where .=" and TrnPromise.id in (" . implode ( ',' , $this->request->data['TrnPromise']['id'] ) .')';
        }

        //要求番号
        if( (!empty($this->request->data['TrnPromise']['work_no']) ) ){
            $where .=" and TrnPromiseHeaders.work_no LIKE '%".Sanitize::escape($this->request->data['TrnPromise']['work_no'])."%'";
        }
        //要求日FROM
        if( (!empty($this->request->data['TrnPromise']['work_date1']) ) ){
            $where .= " and TrnPromise.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] ."'";
        }
        //要求日TO
        if( (!empty($this->request->data['TrnPromise']['work_date2']) ) ){
            $where .= " and TrnPromise.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] ."'";
        }

        //商品ID
        if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
            $where .= " and MstFacilityItem.internal_code = '" . Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']) . "'";
        }
        //製品番号
        if(!empty($this->request->data['MstFacilityItem']['item_code'])){
            $where .= " and MstFacilityItem.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        //JAN(前方一致）
        if(!empty($this->request->data['MstFacilityItem']['jan_code'])){
            $where .= " and MstFacilityItem.jan_code LIKE  '" . Sanitize::escape($this->request->data['MstFacilityItem']['jan_code']) . "%'";
        }
        //規格
        if(!empty($this->request->data['MstFacilityItem']['standard'])){
            $where .= " and MstFacilityItem.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }


        //取消も表示
        if(isset($this->request->data['TrnPromise']['delete_opt']) && $this->request->data['TrnPromise']['delete_opt'] != "1"){
            $where .= ' and TrnPromise.is_deleted = false ';
        }

        //要求施設
        if(!empty($this->request->data['TrnPromise']['facilityCode'])){
            $where .= " and MstFacility.facility_code = '". $this->request->data['TrnPromise']['facilityCode'] ."'";

            //要求部署
            if(!empty($this->request->data['TrnPromise']['departmentCode'])){
                $where .= " and MstDepartment.department_code = '" . $this->request->data['TrnPromise']['departmentCode']."'";
            }
        }
        //管理区分
        if(!empty($this->request->data['TrnPromise']['promise_type'])){
            $where .= ' and TrnPromise.promise_type = ' . $this->request->data['TrnPromise']['promise_type'];
        }


        //商品名
        if(!empty($this->request->data['MstFacilityItem']['item_name'])){
            $where .= " and MstFacilityItem.item_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%'";
        }

        //販売元
        if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
            $where .= " and MstDealer.dealer_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name']) . "%'";
        }

        //作業区分
        if(!empty($this->request->data['TrnPromise']['work_type'])) {
            $where .= " and TrnPromise.work_type = " . $this->request->data['TrnPromise']['work_type'];
        }


        //欠品
        if(isset($this->request->data['TrnPromise']['opt']) && $this->request->data['TrnPromise']['opt'] == "0"){
            $where .= ' and TrnPromise.is_deleted = FALSE and TrnPromise.remain_count >= 1 ';
        }

        if(!empty($limit) && !$detail){
            $limit = " limit " . $limit;
        }else{
            $limit = "";
        };

        $sql = 'select';
        $sql.= ' TrnPromiseHeaders.id        as "TrnPromiseHeaders__id"';
        $sql.= ' , TrnPromiseHeaders.work_no as "TrnPromiseHeaders__work_no"';
        $sql.= " , to_char(TrnPromiseHeaders.work_date,'YYYY/mm/dd')";
        $sql .= '                            as "TrnPromiseHeaders__work_date"';
        $sql.= " , (  MstFacility.facility_name || ' / ' || MstDepartment.department_name ) ";
        $sql .= '                            as "TrnPromiseHeaders__facility_name"';
        $sql.= ' , MstUser.user_name         as "MstUser__user_name"';
        $sql.= " , to_char(TrnPromiseHeaders.created , 'YYYY/mm/dd hh24:mi:ss')";
        $sql .= '                            as "TrnPromiseHeaders__created"';
        $sql.= ' , TrnPromiseHeaders.recital as "TrnPromiseHeaders__recital"';
        if(!$detail){
            $sql .=  ' , sum(TrnPromise.quantity) as "TrnPromiseHeaders__quantity"';
            $sql .=  ' , (count(TrnPromise.work_no) || \' / \' || TrnPromiseHeaders.detail_count) as "TrnPromiseHeaders__count"';
        }else{
            $sql .=   ', MstFacilityItem.internal_code as "MstFacilityItem__internal_code"';
            $sql .=   ', MstFacilityItem.item_code     as "MstFacilityItem__item_code"';
            $sql .=   ', MstFacilityItem.item_name     as "MstFacilityItem__item_name"';
            $sql .=   ', MstFacilityItem.standard      as "MstFacilityItem__standard"';
            $sql .=   ', (case when MstItemUnit.per_unit = 1 then MstUnitNames.unit_name ';
            $sql .=   "    else MstUnitNames.unit_name || '(' || MstItemUnit.per_unit || PerUnitNames.unit_name || ')' ";
            $sql .=   '  end )                         as "MstUnitNames__unit_name"';
            $sql .=   ', MstDealer.dealer_name         as "MstDealer__dealer_name"';
            $sql .=   ', MstClass.name                 as "TrnPromise__work_name"';
            $sql .=   ', TrnPromise.quantity           as "TrnPromise__quantity"';
            $sql .=   ', TrnPromise.before_quantity    as "TrnPromise__before_quantity"';
            $sql .=   ', TrnPromise.remain_count       as "TrnPromise__remain_count"';
            $sql .=   ', TrnPromise.stock_count        as "TrnPromise__stock_count"';
            $sql .=   ', TrnPromise.reserver_count     as "TrnPromise__reserver_count"';
            $sql .=   ', TrnPromise.requested_count    as "TrnPromise__requested_count"';
            $sql .=   ', TrnPromise.consume_count      as "TrnPromise__consume_count"';
            $sql .=   ', TrnPromise.unreceipt_count    as "TrnPromise__unreceipt_count"';
            $sql .=   ', TrnPromise.fixed_count        as "TrnPromise__fixed_count"';
            $sql .=   ', TrnPromise.work_type          as "TrnPromise__work_type"';
            $sql .=   ', ( case TrnPromise.promise_type ';
            foreach(Configure::read('PromiseType.list') as $k=>$v){
                $sql .= " when $k then '$v' ";
            }
            $sql .= ' end )                            as "TrnPromise__promise_type_name"';
            $sql .=   ', TrnPromise.promise_type       as "TrnPromise__promise_type"';
            $sql .=   ', ( case TrnPromise.promise_status ';
            foreach(Configure::read('PromiseStatus.list') as $k=>$v){
                $sql .= " when $k then '$v' ";
            }
            $sql .= ' end ) as "TrnPromise__promise_status_name"';
            $sql .=   ', TrnPromise.promise_status     as "TrnPromise__promise_status"';

            $sql .=   ', TrnPromise.id                 as "TrnPromise__id"';
            $sql .=   ', ( CASE ';
            $sql .=   "         WHEN TrnPromise.is_deleted = TRUE THEN '取消済みです' ";
            $sql .=   '         WHEN TrnPromise.promise_status = '. Configure::read('PromiseStatus.Matching') . ' THEN \'引当確定済みです\' ';
            $sql .=   "         WHEN TrnPromise.remain_count = 0 AND TrnPromise.quantity = 0 THEN '定数≦在庫の為、要求は作成されませんでした' ";
            $sql .=   "         WHEN TrnPromise.remain_count = 0 THEN '引当確定済みです' ";
            $sql .=   '         ELSE TrnPromise.recital' ;
            $sql .=   ' END ) as "TrnPromise__recital"';
            $sql .=   ', ( CASE ';
            $sql .=   '         WHEN TrnPromise.promise_status = '.Configure::read('PromiseStatus.Matching').' THEN 0 ';
            $sql .=   '         WHEN TrnPromise.remain_count = 0 THEN 0 ';
            $sql .=   '         WHEN TrnPromise.is_deleted = TRUE THEN 0 ';
            $sql .=   ' ELSE 1' ;
            $sql .=   ' END ) as "TrnPromise__statusJudgment"';
            $sql .=   ', TrnPromise.modifier as "TrnPromise__modifier"';
        }
        $sql.= ' from';
        $sql.= ' trn_promise_headers as TrnPromiseHeaders ';
        $sql.= '    left join mst_departments as MstDepartment ';
        $sql.= '      on MstDepartment.id = TrnPromiseHeaders.department_id_from ';
        $sql.= '    left join mst_facilities as MstFacility ';
        $sql.= '      on MstFacility.id = MstDepartment.mst_facility_id';
        $sql.= '    left join trn_promises as TrnPromise';
        $sql.= '      on TrnPromiseHeaders.id = TrnPromise.trn_promise_header_id ';
        $sql.= '    left join mst_item_units as MstItemUnit ';
        $sql.= '      on MstItemUnit.id = TrnPromise.mst_item_unit_id ';
        $sql.= '    left join mst_facility_items as MstFacilityItem';
        $sql.= '      on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ';
        $sql.= '    left join mst_dealers as MstDealer';
        $sql.= '      on MstFacilityItem.mst_dealer_id= MstDealer.id ';
        $sql.= '    left join mst_unit_names as MstUnitNames';
        $sql.= '      on MstItemUnit.mst_unit_name_id = MstUnitNames.id ';
        $sql.= '    left join mst_unit_names as PerUnitNames';
        $sql.= '      on MstItemUnit.per_unit_name_id = PerUnitNames.id ';
        $sql.= '    left join mst_classes as MstClass';
        $sql.= '      on TrnPromise.work_type = MstClass.id ';
        $sql.= '    inner join mst_facility_relations as MstfacilityRelation ';
        $sql.= '      on MstfacilityRelation.mst_facility_id =  ' . $this->Session->read('Auth.facility_id_selected') ;
        $sql.= '      and MstfacilityRelation.partner_facility_id = MstFacility.id ';
        $sql.= '      and MstFacilityItem.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
        $sql.= '    inner join mst_user_belongings as MstUserBelonging ';
        $sql.= '      on MstUserBelonging.mst_user_id =  ' . $this->Session->read('Auth.MstUser.id');
        $sql.= '      and MstUserBelonging.mst_facility_id = MstFacility.id ';
        $sql.= '      and MstUserBelonging.is_deleted = false';

        if(!$detail){
            $sql.=  '    left join mst_users as MstUser';
            $sql.=  '      on MstUser.id = TrnPromiseHeaders.creater';
        }else{
            $sql.=  '    left join mst_users as MstUser';
            $sql.=  '      on MstUser.id = TrnPromise.modifier';
        }
        $sql.=  '  where';
        $sql.=  '    1 = 1 ';
        $sql.=  $where ;
        if(!$detail){
            $sql.=   '  group by';
            $sql.=   '    TrnPromiseHeaders.id';
            $sql.=   '    , TrnPromise.work_no';
            $sql.=   '    , TrnPromiseHeaders.work_no';
            $sql.=   '    , TrnPromiseHeaders.work_date';
            $sql.=   '    , MstFacility.facility_name';
            $sql.=   '    , MstDepartment.department_name';
            $sql.=   '    , MstUser.user_name';
            $sql.=   '    , TrnPromiseHeaders.created';
            $sql.=   '    , TrnPromiseHeaders.recital';
            $sql.=   '    , TrnPromiseHeaders.detail_count';
        }
       $sql.=  '  order by TrnPromiseHeaders.work_no ' ;
        if($count){
            $this->set('max' , $this->getMaxCount($sql , 'TrnPromise'));
            return;
        }
        if(!$detail){
            $sql .=  $limit;
        }
        return $sql;
    }

    /**
     * 未引当件数を取得
     **/

    private function _getRemainCount($data){
        $this->request->data = $data ;

        $where = " and a.is_deleted = false ";
        //要求区分
        if($this->request->data['TrnPromise']['promise_type'] != ""){
            $where .= " and a.promise_type = " . $this->request->data['TrnPromise']['promise_type'];
        }
        //要求日
        if(!empty($this->request->data['TrnPromise']['work_date1']) ){
            $where .= " and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2']) ){
            $where .= " and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        //施設
        if(!empty($this->request->data['TrnPromise']['facilityCode']) ){
            $where .= " and c.facility_code = '" . $this->request->data['TrnPromise']['facilityCode'] . "'";
        }

        //部署
        if(!empty($this->request->data['TrnPromise']['departmentCode']) ){
            $where .= " and d.department_code = '" . $this->request->data['TrnPromise']['departmentCode'] . "'";
        }


        $sql  =' select ';
        $sql .=    ' sum(a.remain_count)  ';
        $sql .=' from ';
        $sql .=    ' trn_promises as a  ';
        $sql .=    ' inner join mst_facility_relations as b  ';
        $sql .=    ' on b.mst_facility_id = '. $this->Session->read('Auth.facility_id_selected') ;
        $sql .=    ' inner join mst_facilities as c  ';
        $sql .=    ' on c.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .=    ' and c.id = b.partner_facility_id  ';
        $sql .=    ' inner join mst_departments as d  ';
        $sql .=    ' on d.id = a.department_id_from  ';
        $sql .=    ' and d.mst_facility_id = c.id ';
        //操作権限
        $sql .=    ' inner join mst_user_belongings as e  ';
        $sql .=    ' on e.mst_facility_id = c.id  ';
        $sql .=    ' and e.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .=    ' and e.is_deleted = false ';

        $sql .=    ' where  ';
        $sql .=    ' a.promise_type in ( ' . Configure::read('PromiseType.Constant') . ', ' . Configure::read('PromiseType.Temporary') . ') ';
        $sql .=    $where ;

        $res = $this->TrnPromise->query($sql , false);
        return (is_null($res[0][0]['sum']))?'0':$res[0][0]['sum'];

    }

    /**
     * 要求リストを取得
     * @access private
     * @param array   $data    POSTデータ
     * @param boolean $header  部署ごとに纏めるか
     * @param boolean $view    要求/消費が0件も対象にするか
     * @return $array
     */
    private function _getPromisesList($data , $header = false , $view = false , $target_department = ''){
        switch ($data['promise']['fixed_num']){
          case 1;
            $fix = 'fixed_count';
            break;
          case 2;
            $fix = 'holiday_fixed_count';
            break;
          case 3;
            $fix = 'spare_fixed_count';
            break;
            
        }
        
        $sql  = ' select ';
        if($header){
            $sql .= "   '' as work_no,";
            $sql .= '     xxx.facility_name, ';
            $sql .= '     xxx.department_name, ';
            $sql .= '     xxx.hospital_department_id, ';
            $sql .= '     xxx.center_department_id, ';
            $sql .= '     count(xxx.hospital_department_id) as detail_count, ';
            $sql .= '     coalesce(sum(xxx.consume_count) ,0) as consume_count, ';
            $sql .= '     coalesce(sum(xxx.promise_request_count) ,0) as promise_request_count, ';
            $sql .= '     coalesce(sum(xxx.requested_count) ,0) as promise_count';
        } else {
            $sql .= '     * ';
        }

        $sql .= '   from ';
        $sql .= '     (  ';
        $sql .= '       select ';
        $sql .= '            h.internal_code';
        $sql .= '          , h.item_name';
        $sql .= '          , coalesce(b.facility_name, g.facility_name)       as facility_name';
        $sql .= '          , coalesce(b.id, g.partner_facility_id)            as mst_facility_id';
        $sql .= '          , coalesce(c.department_name, g.department_name)   as department_name';
        $sql .= '          , coalesce(c.id, g.mst_department_id)              as hospital_department_id';
        $sql .= '          , coalesce(e.id, g.id)                             as hospital_stock_id';
        $sql .= '          , coalesce(e.stock_count, g.stock_count)           as stock_count';
        $sql .= '          , coalesce(e.requested_count, g.requested_count)   as requested_count';
        $sql .= '          , coalesce(e.reserve_count, g.reserve_count)       as reserve_count';
        $sql .= '          , i.id                                             as center_department_id';
        $sql .= '          , j.id                                             as center_stock_id';
        $sql .= '          , coalesce(e.mst_item_unit_id, g.mst_item_unit_id) as mst_item_unit_id';
        $sql .= '          , coalesce(g.consume_count, 0)                     as consume_count';

        $sql .= '          , ( ';
        $sql .= '            case ';
        $sql .= "              when coalesce(d.{$fix}, 0) ";
        $sql .= '                    - ( coalesce(e2.stock_count, g.stock_count ,0) ';
        $sql .= '                    - coalesce(e.reserve_count, g.reserve_count,0) ';
        $sql .= '                    - coalesce(e.promise_count, g.promise_count,0) ';
        $sql .= '                    + coalesce(e.requested_count, g.requested_count,0) ';
        $sql .= '                    )   < 0 ';
        $sql .= '              then 0 ';
        $sql .= "              else coalesce(d.{$fix}, 0) ";
        $sql .= '                 - ( coalesce(e2.stock_count, g.stock_count ,0) ';
        $sql .= '                 -   coalesce(e.reserve_count, g.reserve_count,0) ';
        $sql .= '                 -   coalesce(e.promise_count, g.promise_count,0) ';
        $sql .= '                 +   coalesce(e.requested_count, g.requested_count,0) ';
        $sql .= '                    ) ';
        $sql .= '              end';
        $sql .= '          )                              as promise_request_count';
        $sql .= "           , ( coalesce(d.{$fix},0) )    as use_fixed_count ";
        $sql .= '           , g.last_date ';
        $sql .= '           , d.trade_type ';
        $sql .= '         from ';
        $sql .= '           mst_facility_relations as a ';
        $sql .= '           left join mst_facilities as b ';
        $sql .= '             on a.partner_facility_id = b.id ';
        $sql .= '            and a.is_deleted = false ';
        $sql .= '           inner join mst_departments as c ';
        $sql .= '             on c.mst_facility_id = b.id ';
        $sql .= '            and c.is_deleted = false ';
        $sql .= '           left join mst_fixed_counts as d ';
        $sql .= '             on d.mst_department_id = c.id ';
        $sql .= '             and d.is_deleted = false ';
        $sql .= "             and d.start_date <= '" . $data['promise']['work_date'] ."'";
        $sql .= '             and d.trade_type in (' . Configure::read('TeisuType.Constant') . ', ' . Configure::read('TeisuType.SemiConstant') . ')  ';
        $sql .= '           left join trn_stocks as e ';
        $sql .= '             on e.id = d.trn_stock_id ';
        if($this->Session->read('Auth.Config.StockType') == 0 ){
            // 臨時在庫を判定する
            $sql .= '           left join ( ';
            $sql .= '             select a.mst_department_id , a.mst_item_unit_id , sum(a.quantity) as stock_count from trn_stickers as a ';
            $sql .= '             where a.is_deleted = false ';
            $sql .= '                   and a.quantity > 0 ';
            $sql .= "                   and ( a.facility_sticker_no != '' or a.hospital_sticker_no != '' ) ";
            $sql .= '                   and a.trade_type not in ( ' . Configure::read('ClassesType.Temporary') . ' , ' . Configure::read('ClassesType.ExShipping') . ' , ' . Configure::read('ClassesType.ExNoStock') . ' )';
            $sql .= '             group by a.mst_department_id , a.mst_item_unit_id ';
            $sql .= '             ) as e2 ';
            $sql .= '             on e2.mst_item_unit_id = e.mst_item_unit_id';
            $sql .= '             and e2.mst_department_id = e.mst_department_id';
        }else{
            // 臨時在庫を判定しない
            $sql .= '           left join trn_stocks as e2 ';
            $sql .= '             on e2.mst_item_unit_id = e.mst_item_unit_id';
            $sql .= '             and e2.mst_department_id = e.mst_department_id';
        }
        
        $sql .= '          full outer join (  ';
        $sql .= '            select ';
        $sql .= '                  z.* ';
        $sql .= '                , z1.department_name ';
        $sql .= '                , z2.facility_name ';
        $sql .= '                , z2.id                          as partner_facility_id ';
        $sql .= '                , z2.facility_type ';
        $sql .= '                , z3.id ';
        $sql .= '                , z5.id                          as mst_facility_id ';
        $sql .= '                , z4.is_deleted ';
        $sql .= '                , z6.mst_facility_item_id ';
        $sql .= '                , coalesce(z31.stock_count,0) as stock_count  ';
        $sql .= '                , z3.reserve_count ';
        $sql .= '                , z3.promise_count ';
        $sql .= '                , z3.requested_count ';
        $sql .= '                , z3.receipted_count  ';
        $sql .= '              from ';
        $sql .= '                (  ';
        $sql .= '                  select ';
        $sql .= '                        y.mst_item_unit_id ';
        $sql .= '                      , y.mst_department_id ';
        $sql .= '                      , count(y.quantity)     as consume_count ';
        $sql .= '                      , x.last_date  ';
        $sql .= '                    from ';
        $sql .= '                      (  ';
        $sql .= '                        select ';
        $sql .= '                              x.department_id_from ';
        $sql .= '                            , x.mst_item_unit_id ';
        $sql .= '                            , max(x.created)                 as last_date  ';
        $sql .= '                          from ';
        $sql .= '                            trn_promises as x  ';
        $sql .= '                            left join mst_departments as xx  ';
        $sql .= '                              on x.department_id_from = xx.id  ';
        $sql .= '                             and xx.is_deleted = false ';
        $sql .= '                            left join mst_facility_relations as xxx  ';
        $sql .= '                              on xxx.partner_facility_id = xx.mst_facility_id  ';
        $sql .= '                              and xxx.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '                          where ';
        $sql .= '                            x.is_deleted = false';
        if(isset($data['TrnPromise']['id'])){
            $sql .= '                       and x.department_id_from in (' . join(',' , $data['TrnPromise']['id']). ')';
        }
        $sql .= '                          group by ';
        $sql .= '                            x.department_id_from ';
        $sql .= '                            , x.mst_item_unit_id ';
        $sql .= '                      ) as x  ';
        $sql .= '                      right join trn_consumes as y  ';
        $sql .= '                        on y.mst_department_id = x.department_id_from  ';
        $sql .= '                        and y.mst_item_unit_id = x.mst_item_unit_id  ';
        $sql .= '                      inner join trn_stickers as yy  ';
        $sql .= '                        on y.trn_sticker_id = yy.id  ';
        $sql .= '                    where ';
        $sql .= "                      y.created between coalesce(x.last_date , '1970/01/01') and  '". date('Y/m/d', strtotime('+1 day', strtotime($data['promise']['work_date'])))."'";
        $sql .= '                      and y.is_deleted = false ';
        if(isset($data['TrnPromise']['id'])){
            $sql .= '                       and y.mst_department_id in (' . join(',' , $data['TrnPromise']['id']). ')';
        }
        $sql .= '                      and y.use_type in (' . Configure::read('UseType.use') . ',' . Configure::read('UseType.moveuse') . ' )';
        $sql .= '                      and yy.trade_type in ( ' . Configure::read('ClassesType.Constant') . ',' . Configure::read('ClassesType.SemiConstant') .' )';
        $sql .= '                    group by ';
        $sql .= '                      y.mst_item_unit_id ';
        $sql .= '                      , y.mst_department_id ';
        $sql .= '                      , x.last_date ';
        $sql .= '                ) as z  ';
        $sql .= '                left join mst_departments as z1  ';
        $sql .= '                  on z.mst_department_id = z1.id  ';
        $sql .= '                left join mst_facilities as z2  ';
        $sql .= '                  on z2.id = z1.mst_facility_id  ';
        $sql .= '                left join trn_stocks as z3  ';
        $sql .= '                  on z3.mst_item_unit_id = z.mst_item_unit_id  ';
        $sql .= '                  and z3.mst_department_id = z.mst_department_id  ';
        if($this->Session->read('Auth.Config.StockType') == 0 ){
            // 臨時在庫を判定する
            $sql .= '                left join ( ';
            $sql .= '                  select a.mst_department_id , a.mst_item_unit_id , sum(a.quantity) as stock_count from trn_stickers as a ';
            $sql .= '                  where a.is_deleted = false ';
            $sql .= '                      and a.quantity > 0 ';
            $sql .= "                      and ( a.facility_sticker_no != '' or a.hospital_sticker_no != '' ) ";
            $sql .= '                      and a.trade_type not in ( ' . Configure::read('ClassesType.Temporary') . ' , ' . Configure::read('ClassesType.ExShipping') . ' , ' . Configure::read('ClassesType.ExNoStock') . ' )';
            $sql .= '                  group by a.mst_department_id , a.mst_item_unit_id ';
            $sql .= '                ) as z31';
            $sql .= '                 on z31.mst_department_id = z3.mst_department_id ';
            $sql .= '                 and z31.mst_item_unit_id = z3.mst_item_unit_id ';
        }else{
            // 臨時在庫を判定しない
            $sql .= '                left join trn_stocks as z31';
            $sql .= '                 on z31.mst_department_id = z3.mst_department_id ';
            $sql .= '                 and z31.mst_item_unit_id = z3.mst_item_unit_id ';
        }
        
        $sql .= '                left join mst_facility_relations as z4  ';
        $sql .= '                  on z4.partner_facility_id = z2.id  ';
        $sql .= '                left join mst_facilities as z5  ';
        $sql .= '                  on z5.id = z4.mst_facility_id  ';
        $sql .= '                left join mst_item_units as z6  ';
        $sql .= '                  on z6.id = z.mst_item_unit_id ';

        $sql .= '           ) as g ';
        $sql .= '             on g.mst_department_id = c.id ';
        $sql .= '             and g.mst_item_unit_id = d.mst_item_unit_id ';
        $sql .= '           left join mst_facility_items as h ';
        $sql .= '             on h.id = coalesce(d.mst_facility_item_id, g.mst_facility_item_id)';
        $sql .= '           left join mst_departments as i ';
        $sql .= '             on i.mst_facility_id = coalesce(a.mst_facility_id, g.mst_facility_id) ';
        $sql .= '            and i.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '            and i.is_deleted = false ';
        $sql .= '           left join trn_stocks as j ';
        $sql .= '             on j.mst_item_unit_id = coalesce(e.mst_item_unit_id, g.mst_item_unit_id) ';
        $sql .= '             and j.mst_department_id = i.id  ';

        $sql .= '           inner join mst_user_belongings as k ';
        $sql .= '             on k.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '             and k.mst_facility_id = coalesce(b.id, g.partner_facility_id) ';
        $sql .= '             and k.is_deleted = false';
        $sql .= '         where ';
        $sql .= '           coalesce(a.mst_facility_id, g.mst_facility_id) = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '           and coalesce(b.facility_type, g.facility_type)  = '.Configure::read('FacilityType.hospital');
        $sql .= '           and coalesce(a.is_deleted, g.is_deleted) = false ';

        if($target_department!=''){
            $sql .='            and ( c.id = ' . $target_department .' or g.mst_department_id = ' .$target_department. ')';
        }
        if(isset($data['TrnPromise']['id'])){
            $sql .= '           and  ( c.id in (' . join(',' , $data['TrnPromise']['id']). ')';
            $sql .= '           or g.mst_department_id in (' . join(',' , $data['TrnPromise']['id']). ') )';
        }

        $sql .= '     ) as xxx ';

        if(!$view){
            $sql .= ' where ';
            $sql .= '   ( coalesce(xxx.consume_count, 0) > 0  or coalesce(xxx.promise_request_count, 0) > 0 ) ';
        }

        if($header){
            $sql .= '     group by  ';
            $sql .= '       xxx.facility_name, ';
            $sql .= '       xxx.mst_facility_id, ';
            $sql .= '       xxx.department_name, ';
            $sql .= '       xxx.hospital_department_id,  ';
            $sql .= '       xxx.center_department_id  ';
            if(!$view){
                $sql .= '  having ';
                $sql .= '    ( coalesce(sum(xxx.consume_count) ,0) > 0 or coalesce(sum(xxx.promise_request_count) ,0) > 0 )'; //要求数と消費が０のものは対象外
            }
        }

        $sql .= '     order by  ';
        $sql .= '       xxx.mst_facility_id, ';
        $sql .= '       xxx.hospital_department_id  ';

        return $this->TrnPromise->query($sql , false);
    }

    function checkStockRecode($data , $time){
        $sql  = ' select ';
        $sql .= '       count(*)  ';
        $sql .= '   from ';
        $sql .= '     trn_stocks as a  ';
        $sql .= '     left join mst_departments as b  ';
        $sql .= '       on b.id = a.mst_department_id  ';
        $sql .= '   where ';
        $sql .= '     b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
        if(isset($data['TrnPromise']['id'])){
            $sql .= '     and b.id in (' . join(',' , $data['TrnPromise']['id']). ')';
        }
        $sql .= "     and a.modified > '" . $data['promise']['time'] ."'";
        $sql .= "     and a.modified <> '" . $time ."'";
        $ret = $this->TrnStock->query($sql);

        return (($ret[0][0]['count']> 0 )? false : true);
    }


    private function _add_regist(){
        $now = date('Y/m/d H:i:s');
        /* 結果表示用を取得 */
        $result = $this->_getPromisesList($this->request->data , true , true);
        /* 対象部署一覧を取得 */
        $target_department = $this->_getPromisesList($this->request->data , true );
        /* 対象部署ループ */
        $this->TrnPromiseHeader->begin();
        // 行ロック
        $this->TrnStock->query(' select * from trn_stocks as a where a.mst_department_id = ' . $this->getDepartmentId($this->Session->read('Auth.facility_id_selected') , Configure::read('DepartmentType.warehouse') ) . ' for update');

        foreach($target_department as $target_d){
            /* work_no を作成 */
            $work_no = $this->setWorkNo4Header($this->request->data['promise']['work_date'],'03');

            /* ヘッダレコード作成 */
            $promiseheader = array(
                'TrnPromiseHeader' => array(
                    'work_no'            => $work_no,
                    'work_date'          => $this->request->data['promise']['work_date'],   //作業日（入力情報）
                    'recital'            => $this->request->data['promise']['recital'],     //備考（入力情報）
                    'promise_type'       => Configure::read('PromiseType.Constant'),        //定数
                    'promise_status'     => Configure::read('PromiseStatus.Init'),          //初期
                    'department_id_from' => $target_d[0]['hospital_department_id'],
                    'department_id_to'   => $target_d[0]['center_department_id'],
                    'detail_count'       => $target_d[0]['detail_count'],
                    'is_deleted'         => false,
                    'creater'            => $this->Session->read('Auth.MstUser.id'),
                    'created'            => $now,
                    'modifier'           => $this->Session->read('Auth.MstUser.id'),
                    'modified'           => $now
                    )
                );

            //ヘッダオブジェクトをcreate
            $this->TrnPromiseHeader->create();
            if(!$this->TrnPromiseHeader->save($promiseheader)){
                // 保存失敗
                $this->TrnPromiseHeader->rollback();
                return false;
            }

            /* ヘッダのIDを取得 */
            $header_id = $this->TrnPromiseHeader->getLastInsertID();

            /* 対象明細を取得 */
            $target_item = $this->_getPromisesList($this->request->data , false , false , $target_d[0]['hospital_department_id']);
            $seqNo = 1;
            $promise = array();
            foreach($target_item as $target_i){
                /* 要求明細を作成 */
                $TrnPromise = array(
                    'TrnPromise' => array(
                        'work_no'               => $work_no,
                        'work_seq'              => $seqNo,
                        'work_date'             => $this->request->data['promise']['work_date'],
                        'work_type'             => $this->request->data['promise']['work_type'],
                        'recital'               => $this->request->data['promise']['recital'],
                        'trade_type'            => $target_i[0]['trade_type'],
                        'promise_type'          => Configure::read('PromiseType.Constant'),
                        'promise_status'        => Configure::read('PromiseStatus.Init'),
                        'mst_item_unit_id'      => $target_i[0]['mst_item_unit_id'],
                        'department_id_from'    => $target_i[0]['hospital_department_id'],
                        'department_id_to'      => $target_i[0]['center_department_id'],
                        'before_quantity'       => $target_i[0]['promise_request_count'],
                        'quantity'              => $target_i[0]['promise_request_count'],
                        'remain_count'          => $target_i[0]['promise_request_count'],
                        'fixed_count'           => $target_i[0]['use_fixed_count'],
                        'stock_count'           => $target_i[0]['stock_count'],
                        'reserver_count'        => $target_i[0]['reserve_count'],
                        'requested_count'       => $target_i[0]['requested_count'],
                        'is_deleted'            => false,
                        'creater'               => $this->Session->read('Auth.MstUser.id'),
                        'created'               => $now,
                        'modifier'              => $this->Session->read('Auth.MstUser.id'),
                        'modified'              => $now,
                        'trn_promise_header_id' => $header_id,
                        'consume_count'         => $target_i[0]['consume_count'],
                        'unreceipt_count'       => $target_i[0]['promise_request_count'],
                        'fixed_num_used'        => $this->request->data['promise']['fixed_num']
                        )
                    );
                $this->TrnPromise->create();
                if(!$this->TrnPromise->save($TrnPromise)){
                    // 保存失敗
                    $this->TrnPromiseHeader->rollback();
                    return false;
                }


                
                $seqNo++;

                /* 部署在庫を更新 入荷予定数△ */
                if (!$this->TrnStock->updateAll(array(  'TrnStock.requested_count' => 'TrnStock.requested_count + ' . $target_i[0]['promise_request_count'],
                                                        'TrnStock.modifier'        => $this->Session->read('Auth.MstUser.id'),
                                                        'TrnStock.modified'        => "'" .$now . "'"
                                                        )
                                                , array('TrnStock.id'  => $this->getStockRecode($target_i[0]['mst_item_unit_id'],
                                                                                                $target_i[0]['hospital_department_id']))
                                                ,-1
                                                )
                    ) {
                    $this->TrnPromiseHeader->rollback();
                    return false;
                }
                
                /* センター在庫を更新 被要求数△*/
                if (!$this->TrnStock->updateAll(array(  'TrnStock.promise_count' => 'TrnStock.promise_count + ' . $target_i[0]['promise_request_count'],
                                                        'TrnStock.modifier'      => $this->Session->read('Auth.MstUser.id'),
                                                        'TrnStock.modified'      => "'" . $now ."'"
                                                        )
                                                , array('TrnStock.id'  => $this->getStockRecode($target_i[0]['mst_item_unit_id'],
                                                                                                $target_i[0]['center_department_id']))
                                                ,-1
                                                )
                    ) {
                    $this->TrnPromiseHeader->rollback();
                    return false;
                }
            }
            
            /* work_no を表示用に足す */
            foreach($result as &$wk_result){
                if($wk_result[0]['hospital_department_id'] == $target_d[0]['hospital_department_id']){
                    $wk_result[0]['work_no'] = $work_no;
                    break;
                }
            }
            unset($wk_result);
            /* ループ終わり*/
        }

        $checkResult = $this->checkStockRecode($this->request->data , $now);
        if(!$checkResult){
            $this->Session->setFlash('他端末にて更新された為、ロールバックしました。', 'growl', array('type'=>'error') );
            $this->TrnPromiseHeader->rollback();
            $this->redirect('add');
        }
        $this->TrnPromiseHeader->commit();

        return $result;
    }


    //商品情報を取得
    private function _getData($where = '' , $limit = null , $count=false){
        $sql  = 'select' ;
        $sql .= '      b.id            as "TrnPromise__id"' ;
        $sql .= '    , a.internal_code as "TrnPromise__internal_code"' ;
        $sql .= '    , a.item_name     as "TrnPromise__item_name"' ;
        $sql .= '    , a.standard      as "TrnPromise__standard"' ;
        $sql .= '    , a.item_code     as "TrnPromise__item_code"' ;
        $sql .= '    , c.dealer_name   as "TrnPromise__dealer_name"' ;
        $sql .= '    , ( case ';
        $sql .= '        when b.per_unit = 1 then d.unit_name  ';
        $sql .= "        else d.unit_name || '(' || b.per_unit || e.unit_name || ')'";
        $sql .= '      end )           as "TrnPromise__unit_name"';
        $sql .= '    , ( case ';
        $sql .= "        when a.start_date > '" .$this->request->data['TrnPromise']['work_date'] ."' then false"; //施設採用品の適用開始日以前
        $sql .= "        when a.end_date < '" .$this->request->data['TrnPromise']['work_date'] ."' then false";   //施設採用品の適用終了日以降
        $sql .= "        when b.start_date > '" .$this->request->data['TrnPromise']['work_date'] ."' then false"; //包装単位の適用開始日以前
        $sql .= "        when b.end_date < '" .$this->request->data['TrnPromise']['work_date'] ."' then false";   //包装単位の適用中止日以降
        $sql .= "        when a.is_deleted = true then false";                                           //施設採用品の適用フラグOFF
        $sql .= "        when b.is_deleted = true then false";                                           //包装単位の適用フラグOFF
        $sql .= "        when f.id is null  then false";                                                 //売上設定なし
        $sql .= "        else true";
        $sql .= '       end )          as "TrnPromise__status"';
        $sql .= '  from' ;
        $sql .= '    mst_facility_items as a ' ;
        $sql .= '    left join mst_item_units as b ' ;
        $sql .= '      on a.id = b.mst_facility_item_id ' ;
        $sql .= '    left join mst_dealers as c ' ;
        $sql .= '      on c.id = a.mst_dealer_id ' ;
        $sql .= '    left join mst_unit_names as d ' ;
        $sql .= '      on d.id = b.mst_unit_name_id ' ;
        $sql .= '    left join mst_unit_names as e ' ;
        $sql .= '      on e.id = b.per_unit_name_id' ;

        $sql .= '    left join mst_sales_configs as f ' ;
        $sql .= '      on f.mst_item_unit_id = b.id ';
        $sql .= '     and f.is_deleted = false ';
        $sql .= '     and f.partner_facility_id = ' . $this->request->data['TrnPromise']['facility_id'];
        $sql .= "     and f.start_date <= '" . $this->request->data['TrnPromise']['work_date'] . "'";
        $sql .= "     and f.end_date >= '" . $this->request->data['TrnPromise']['work_date'] . "'";
        $sql .= ' where 1 = 1 ' ;
        $sql .= $where ;

        $sql .= " order by a.internal_code , b.per_unit";
        if($count){
            $sql = 'select count(*) as count from ( ' . $sql . ' ) as x ';
            $ret = $this->MstFacilityItem->query($sql);
            $this->set('max' , $ret[0][0]['count']);
        }
        if(!is_null($limit)){
            $sql .= ' limit ' . $limit;
        }

        return $this->MstFacilityItem->query($sql);

    }


    private function _getExAddConfirm(){
        $sql  = ' select ';
        $sql .= '       a.id             as "TrnPromise__id" ';
        $sql .= '     , b.internal_code  as "TrnPromise__internal_code"';
        $sql .= '     , b.item_name      as "TrnPromise__item_name"';
        $sql .= '     , b.item_code      as "TrnPromise__item_code" ';
        $sql .= '     , b.standard       as "TrnPromise__standard"';
        $sql .= '     , e.dealer_name    as "TrnPromise__dealer_name"';
        $sql .= '     , ( case ';
        $sql .= '         when a.per_unit = 1 then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || a.per_unit || d.unit_name || ')'";
        $sql .= '       end )            as "TrnPromise__unit_name"';
        $sql .= '     , coalesce( (f.stock_count - f.reserve_count - f.promise_count), 0)  as "TrnPromise__stock_count" ';
        $sql .= '     , ( case  ';
        $sql .= '         when x.mst_item_unit_id is null  '; // 病院定数設定が無い
        $sql .= '          and h.id is null  ';               // 仕入設定の代表フラグが立っていない
        $sql .= '          and coalesce( (f.stock_count - f.reserve_count - f.promise_count), 0) <= 0 '; // センター在庫の引当可能数が0以下
        $sql .= "         then '病院定数未設定商品'";
        $sql .= "         else '' ";
        $sql .= '       end )            as "TrnPromise__alert" ';
        $sql .= '   from ';
        $sql .= '     mst_item_units as a  ';
        $sql .= '     left join mst_facility_items as b  ';
        $sql .= '       on b.id = a.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on c.id = a.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on d.id = a.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on e.id = b.mst_dealer_id  ';
        $sql .= '     left join trn_stocks as f ';
        $sql .= '       on f.mst_item_unit_id = a.id ';
        $sql .= '     left join mst_departments as g ';
        $sql .= '       on f.mst_department_id = g.id ';
        $sql .= '    left join (  ';
        $sql .= '      select ';
        $sql .= '            a.mst_item_unit_id  ';
        $sql .= '        from ';
        $sql .= '          mst_fixed_counts as a  ';
        $sql .= '          left join mst_departments as b  ';
        $sql .= '            on b.id = a.mst_department_id  ';
        $sql .= '        where ';
        $sql .= '          a.is_deleted = false  ';
        $sql .= '          and a.fixed_count > 0  ';
        $sql .= '          and b.department_type = ' . Configure::read('DepartmentType.hospital');
        $sql .= '        group by ';
        $sql .= '          a.mst_item_unit_id ';
        $sql .= '    ) as x  ';
        $sql .= '      on x.mst_item_unit_id = a.id  ';
        $sql .= '    left join mst_transaction_configs as h ';
        $sql .= '      on h.mst_item_unit_id = a.id ';
        $sql .= '    left join mst_transaction_mains as i ';
        $sql .= '      on h.mst_item_unit_id = i.mst_item_unit_id ';
        $sql .= '      and h.partner_facility_id = i.mst_facility_id ';
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .= '     and g.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '     and b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and a.id in (' .$this->request->data['cart']['tmpid']. ') ';
        $sql .= '   order by ';
        $sql .= '     b.internal_code , a.per_unit';

        return $this->MstFacilityItem->query($sql);
    }


    private function _getExAddResult(){
        $sql  = ' select ';
        $sql .= '       a.id             as "TrnPromise__id" ';
        $sql .= '     , b.internal_code  as "TrnPromise__internal_code"';
        $sql .= '     , b.item_name      as "TrnPromise__item_name"';
        $sql .= '     , b.item_code      as "TrnPromise__item_code" ';
        $sql .= '     , b.standard       as "TrnPromise__standard"';
        $sql .= '     , e.dealer_name    as "TrnPromise__dealer_name"';
        $sql .= '     , ( case ';
        $sql .= '         when a.per_unit = 1 then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || a.per_unit || d.unit_name || ')'";
        $sql .= '       end )            as "TrnPromise__unit_name"';
        $sql .= '     , coalesce( (f.stock_count - f.reserve_count - f.promise_count), 0)  as "TrnPromise__stock_count" ';
        $sql .= '   from ';
        $sql .= '     mst_item_units as a  ';
        $sql .= '     left join mst_facility_items as b  ';
        $sql .= '       on b.id = a.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on c.id = a.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on d.id = a.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on e.id = b.mst_dealer_id  ';
        $sql .= '     left join trn_stocks as f ';
        $sql .= '       on f.mst_item_unit_id = a.id ';
        $sql .= '     left join mst_departments as g ';
        $sql .= '       on f.mst_department_id = g.id ';
        $sql .= '   where ';
        $sql .= '     1 = 1  ';
        $sql .= '     and g.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '     and b.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and a.id in (' . join(',' , $this->request->data['TrnPromise']['id']). ') ';
        $sql .= '   order by ';
        $sql .= '     b.internal_code , a.per_unit';

        return $this->MstFacilityItem->query($sql);
    }

    private function _getCsvPromiseData($value){
        $sql  = ' select ';
        $sql .= '       c.id            as "TrnPromise__id"';
        $sql .= '     , b.internal_code as "TrnPromise__internal_code"';
        $sql .= '     , b.item_name     as "TrnPromise__item_name"';
        $sql .= '     , b.item_code     as "TrnPromise__item_code"';
        $sql .= '     , b.standard      as "TrnPromise__standard"';
        $sql .= '     , d.dealer_name   as "TrnPromise__dealer_name"';
        $sql .= '     , ( case ';
        $sql .= '         when c.per_unit = 1 then g.unit_name  ';
        $sql .= "         else g.unit_name || '(' || c.per_unit || h.unit_name || ')'";
        $sql .= '       end )           as "TrnPromise__unit_name"';
        $sql .= '     , coalesce(( j.stock_count - j.reserve_count - j.promise_count), 0 ) as "TrnPromise__stock_count"';
        $sql .= '     , ( case ';
        $sql .= '             when b.is_deleted = true then true ';
        $sql .= "             when b.start_date > '".$this->request->data['TrnPromise']['work_date']."' then true ";
        $sql .= "             when b.end_date < '".$this->request->data['TrnPromise']['work_date']."' then true ";
        $sql .= '             when c.is_deleted = true then true ';
        $sql .= "             when c.start_date > '".$this->request->data['TrnPromise']['work_date']."' then true ";
        $sql .= "             when c.end_date < '".$this->request->data['TrnPromise']['work_date']."' then true ";
        $sql .= '             else false ';
        $sql .= '        end ) as "TrnPromise__err" ';
        $sql .= '     , ( case ';
        $sql .= "             when b.is_deleted = true                                         then '適用されていない施設採用品です' ";
        $sql .= "             when b.start_date > '".$this->request->data['TrnPromise']['work_date']."' then '施設採用品の適用開始日前です' ";
        $sql .= "             when b.end_date < '".$this->request->data['TrnPromise']['work_date']."'   then '施設採用品の適用終了日後です' ";
        $sql .= "             when c.is_deleted = true                                         then '適用されていない包装単位です' ";
        $sql .= "             when c.start_date > '".$this->request->data['TrnPromise']['work_date']."' then '包装単位の適用開始日前です' ";
        $sql .= "             when c.end_date < '".$this->request->data['TrnPromise']['work_date']."'   then '包装単位の適用中止日後です' ";
        $sql .= "             else '' ";
        $sql .= '        end ) as "TrnPromise__recital" ';
        $sql .= '     , ( case  ';
        $sql .= '         when x.mst_item_unit_id is null  ';
        $sql .= "         then '病院定数未設定商品'";
        $sql .= "         else '' ";
        $sql .= '       end )            as "TrnPromise__alert" ';

        $sql .= '   from ';
        $sql .= '     mst_facilities as a  ';
        $sql .= '     left join mst_facility_items as b  ';
        $sql .= '       on a.id = b.mst_facility_id  ';
        $sql .= '     left join mst_item_units as c  ';
        $sql .= '       on c.mst_facility_item_id = b.id  ';
        $sql .= '       and c.per_unit = ' . $value['per_unit'];
        $sql .= '     left join mst_dealers as d  ';
        $sql .= '       on d.id = b.mst_dealer_id  ';
        $sql .= '     left join mst_unit_names as g  ';
        $sql .= '       on g.id = c.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as h  ';
        $sql .= '       on h.id = c.per_unit_name_id  ';
        $sql .= '    left join mst_departments as i ';
        $sql .= '      on i.mst_facility_id = a.id ';
        $sql .= '      and i.department_type = ' .Configure::read('DepartmentType.warehouse');
        $sql .= '    left join trn_stocks as j ';
        $sql .= '      on j.mst_item_unit_id = c.id ';
        $sql .= '      and j.mst_department_id = i.id ';
        $sql .= '    left join (  ';
        $sql .= '      select ';
        $sql .= '            a.mst_item_unit_id  ';
        $sql .= '        from ';
        $sql .= '          mst_fixed_counts as a  ';
        $sql .= '          left join mst_departments as b  ';
        $sql .= '            on b.id = a.mst_department_id  ';
        $sql .= '        where ';
        $sql .= '          a.is_deleted = false  ';
        $sql .= '          and a.fixed_count > 0  ';
        $sql .= '          and b.department_type = ' . Configure::read('DepartmentType.hospital');
        $sql .= '        group by ';
        $sql .= '          a.mst_item_unit_id ';
        $sql .= '    ) as x  ';
        $sql .= '      on x.mst_item_unit_id = c.id  ';
        $sql .= '   where ';
        $sql .= '     a.id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= "     and b.internal_code = '" . $value['internal_code'] . "'";
        $sql .= '     and a.facility_type =' . Configure::read('FacilityType.center');

        $tmp = $this->TrnPromise->query($sql);
        return (isset($tmp[0])?$tmp[0]:array());
    }


    /**
     * 引当履歴
     **/
    private function _FixedPromisehistorySQL($data , $limit , $detail = null , $count = false ) {
        $this->request->data = $data;
        App::import('Sanitize');
        // 検索処理

        $where = "";
        //引当ヘッダID
        if( (!empty($this->request->data['TrnFixedPromiseHeader']['id']) ) && $detail ){
            $where .=' and a.id in (' . implode ( ',' , $this->request->data['TrnFixedPromiseHeader']['id'] ) .')';
        }

        //引当明細ID
        if( (!empty($this->request->data['TrnFixedPromise']['id']) ) ){
            $where .=' and b.id in (' . implode ( ',' , $this->request->data['TrnFixedPromise']['id'] ) .')';
        }

        //引当番号
        if( (!empty($this->request->data['TrnFixedPromiseHeader']['work_no']) ) ){
            $where .=" and a.work_no LIKE '%".Sanitize::escape($this->request->data['TrnFixedPromiseHeader']['work_no'])."%'";
        }
        //引当日FROM
        if( (!empty($this->request->data['TrnFixedPromiseHeader']['work_date1']) ) ){
            $where .= " and b.work_date >= '" . $this->request->data['TrnFixedPromiseHeader']['work_date1'] ."'";
        }
        //引当日TO
        if( (!empty($this->request->data['TrnFixedPromiseHeader']['work_date2']) ) ){
            $where .= " and b.work_date <= '" . $this->request->data['TrnFixedPromiseHeader']['work_date2'] ."'";
        }

        //商品ID
        if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
            $where .= " and g.internal_code = '" . Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']) . "'";
        }
        //製品番号
        if(!empty($this->request->data['MstFacilityItem']['item_code'])){
            $where .= " and g.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        //JAN(前方一致）
        if(!empty($this->request->data['MstFacilityItem']['jan_code'])){
            $where .= " and g.jan_code LIKE  '" . Sanitize::escape($this->request->data['MstFacilityItem']['jan_code']) . "%'";
        }
        //規格
        if(!empty($this->request->data['MstFacilityItem']['standard'])){
            $where .= " and g.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }

        //取消も表示
        if(isset($this->request->data['TrnFixedPromiseHeader']['delete_opt'])){
            $where .= ' and b.is_deleted = false ';
        }
        
        //未処理のみ表示
        if(isset($this->request->data['TrnFixedPromiseHeader']['remain_count'])){
            $where .= ' and b.remain_count > 0 ';
        }

        //要求施設
        if(!empty($this->request->data['TrnFixedPromiseHeader']['facilityCode'])){
            $where .= " and d.facility_code = '". $this->request->data['TrnFixedPromiseHeader']['facilityCode'] . "'";

            //要求部署
            if(!empty($this->request->data['TrnFixedPromiseHeader']['departmentCode'])){
                $where .= " and c.department_code = '" . $this->request->data['TrnFixedPromiseHeader']['departmentCode'] . "'";
            }
        }
        //管理区分
        if(!empty($this->request->data['TrnFixedPromiseHeader']['promise_type'])){
            $where .= ' and b.promise_type = ' . $this->request->data['TrnFixedPromiseHeader']['promise_type'];
        }


        //商品名
        if(!empty($this->request->data['MstFacilityItem']['item_name'])){
            $where .= " and g.item_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%'";
        }

        //販売元
        if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
            $where .= " and h.dealer_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name']) . "%'";
        }

        //作業区分
        if(!empty($this->request->data['TrnFixedPromiseHeader']['work_type'])) {
            $where .= ' and b.work_type = ' . $this->request->data['TrnFixedPromiseHeader']['work_type'];
        }

        //削除済みの表示
        if(isset($this->request->data['TrnFixedPromiseHeader']['delete_opt']) && $this->request->data['TrnFixedPromiseHeader']['delete_opt']!=1){
            $where .=' and b.is_deleted = false';
        }

        if(!empty($limit) && !$detail){
            $limit = " limit " . $limit;
        }else{
            $limit = "";
        };

        $sql = ' select ';
        $sql .= '       a.id                as "TrnFixedPromiseHeader__id"';
        $sql .= '     , a.work_no           as "TrnFixedPromise__work_no"';
        $sql .= "     , to_char(a.work_date ,'yyyy/mm/dd' )  ";
        $sql .= '                           as "TrnFixedPromise__work_date"';
        $sql .= '     , d.facility_name     as "TrnFixedPromise__facility_name" ';
        $sql .= '     , c.department_name   as "TrnFixedPromise__department_name"';
        $sql .= "     , to_char(a.created ,'yyyy/mm/dd HH24:MI:SS' )  ";
        $sql .= '                           as "TrnFixedPromise__created" ';
        $sql .= '     , a.recital           as "TrnFixedPromiseHeader__recital"';
        if(!$detail){
            $sql .= '     , e.user_name         as "TrnFixedPromiseHeader__user_name"';
            $sql .= '     , count(a.*)          as "TrnFixedPromise__count"';
            $sql .= '     , a.detail_count      as "TrnFixedPromise__detail_count"';
        }else{
            $sql .= '     , b.id                as "TrnFixedPromise__id"';
            $sql .= '     , b.quantity          as "TrnFixedPromise__quantity"';
            $sql .= '     , b.remain_count      as "TrnFixedPromise__remain_count"';

            $sql .= '     , g.internal_code     as "TrnFixedPromise__internal_code"';
            $sql .= '     , g.item_name         as "TrnFixedPromise__item_name"';
            $sql .= '     , g.item_code         as "TrnFixedPromise__item_code"';
            $sql .= '     , h.dealer_name       as "TrnFixedPromise__dealer_name"';
            $sql .= '     , g.standard          as "TrnFixedPromise__standard"';
            $sql .= '     , ( case when f.per_unit = 1 then i.unit_name ';
            $sql .= "              else i.unit_name || '(' || f.per_unit || j.unit_name || ')' ";
            $sql .= '        end )              as "TrnFixedPromise__unit_name"';
            $sql .= '     , k.user_name         as "TrnFixedPromise__user_name"';
            $sql .= '     , l.name              as "TrnFixedPromise__work_name"';
            $sql .= '      , ( case b.fixed_promise_status ';
            foreach(Configure::read('PromiseStatus.list') as $key => $val){
                $sql .= "           when " . $key . " then '" . $val ."' ";
            }
            $sql .= "        else '' ";
            $sql .= '        end )              as "TrnFixedPromise__fixed_promise_status"';
            $sql .= '      , ( case when b.is_deleted = true then false';
            $sql .= '               when b.remain_count = 0 then false ';
            $sql .= '               else true ';
            $sql .= '          end )            as "TrnFixedPromise__check"';
            $sql .= "      , ( case when b.is_deleted = true then '削除済み'" ;
            $sql .= "               when b.remain_count = 0 then '出荷済み' ";
            $sql .= '               else b.recital';
            $sql .= '          end )            as "TrnFixedPromise__recital"';

        }
        $sql .= '   from ';
        $sql .= '     trn_fixed_promise_headers as a  ';
        $sql .= '     left join trn_fixed_promises as b  ';
        $sql .= '       on b.trn_fixed_promise_header_id = a.id  ';
        $sql .= '     left join mst_departments as c  ';
        $sql .= '       on c.id = a.department_id_from  ';
        $sql .= '     left join mst_facilities as d  ';
        $sql .= '       on d.id = c.mst_facility_id  ';
        $sql .= '     left join mst_users as e  ';
        $sql .= '       on e.id = a.creater  ';
        $sql .= '     left join mst_item_units as f  ';
        $sql .= '       on f.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as g  ';
        $sql .= '       on g.id = f.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as h  ';
        $sql .= '       on h.id = g.mst_dealer_id  ';
        $sql .= '     left join mst_unit_names as i  ';
        $sql .= '       on i.id = f.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as j  ';
        $sql .= '       on j.id = f.per_unit_name_id  ';
        $sql .= '     left join mst_users as k  ';
        $sql .= '       on k.id = b.modifier ';
        $sql .= '     left join mst_classes as l  ';
        $sql .= '       on l.id = b.work_type ';
        $sql .= '     inner join mst_user_belongings as m';
        $sql .= '       on m.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '       and m.mst_facility_id = d.id';
        $sql .= '       and m.is_deleted = false';
        $sql .= '   where ';
        $sql .= '     g.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
        $sql .= $where;
        if(!$detail){
            $sql .= '   group by ';
            $sql .= '     a.id ';
            $sql .= '     , a.work_no ';
            $sql .= '     , a.work_date ';
            $sql .= '     , d.facility_name ';
            $sql .= '     , c.department_name ';
            $sql .= '     , a.department_id_from ';
            $sql .= '     , e.user_name ';
            $sql .= '     , a.created ';
            $sql .= '     , a.creater ';
            $sql .= '     , a.recital ';
            $sql .= '     , a.detail_count ';
        }
        $order = $this->getSortOrder();
        if(isset($order[0])){
            $tmp = preg_split ( "/[\s\.]+/" , $order[0] );
            $sql.='  order by "'. $tmp[0] . '"."'.$tmp[1].'" ' . $tmp[2] . ' ';
        }else{
            $sql.='  order by a.work_no ' ;
        }
        if($count){
            $this->set('max' , $this->getMaxCount($sql , 'TrnFixedPromise'));
            return;
        }
        if(!$detail){
            $sql .=  $limit;
        }
        return $sql;
    }


    /**
     * 未引当（欠品）リスト
     **/
    private function _StockOutList($departmentCode = null){
        $_sql  = '       select ';
        $_sql .= '             c.id                    as mst_item_unit_id ';
        $_sql .= '           , c.mst_facility_item_id ';
        $_sql .= '           , a.department_id_to      as mst_department_id ';
        $_sql .= '           , sum(a.remain_count)     as remain_count  ';
        $_sql .= '           , sum(a.quantity)         as quantity  ';
        $_sql .= '         from ';
        $_sql .= '           trn_promises as a  ';
        $_sql .= '           left join mst_departments as b  ';
        $_sql .= '             on b.id = a.department_id_from  ';
        $_sql .= '           left join mst_facilities as e ';
        $_sql .= '             on e.id = b.mst_facility_id';
        $_sql .= '           left join mst_item_units as c  ';
        $_sql .= '             on c.id = a.mst_item_unit_id  ';
        $_sql .= '           left join mst_facility_items as d  ';
        $_sql .= '             on d.id = c.mst_facility_item_id  ';
        $_sql .= '         where ';
        $_sql .= '           a.remain_count > 0  ';
        $_sql .= '           and a.is_deleted = false  ';
        $_sql .= '           and d.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        if((!empty($this->request->data['TrnPromise']['facilityCode'])) && (!is_null($departmentCode))){
            $_sql .= "           and e.facility_code = '" . $this->request->data['TrnPromise']['facilityCode'] . "'";
        }
        if(!is_null($departmentCode)){
            $_sql .= "           and b.department_code = '" . $departmentCode . "'";
        }
        if(!empty($this->request->data['TrnPromise']['promise_type'])){
            $_sql .= '           and a.promise_type = ' . $this->request->data['TrnPromise']['promise_type'];
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql .= '         group by ';
        $_sql .= '           c.id ';
        $_sql .= '           , c.mst_facility_item_id ';
        $_sql .= '           , a.department_id_to ';


        $_sql_x  = '       select ';
        $_sql_x .= '             c.mst_facility_item_id ';
        $_sql_x .= '           , a.department_id_to      as mst_department_id ';
        $_sql_x .= '           , sum(a.remain_count)     as remain_count  ';
        $_sql_x .= '           , sum(a.quantity)         as quantity  ';
        $_sql_x .= '         from ';
        $_sql_x .= '           trn_promises as a  ';
        $_sql_x .= '           left join mst_departments as b  ';
        $_sql_x .= '             on b.id = a.department_id_from  ';
        $_sql_x .= '           left join mst_facilities as e ';
        $_sql_x .= '             on e.id = b.mst_facility_id';
        $_sql_x .= '           left join mst_item_units as c  ';
        $_sql_x .= '             on c.id = a.mst_item_unit_id  ';
        $_sql_x .= '           left join mst_facility_items as d  ';
        $_sql_x .= '             on d.id = c.mst_facility_item_id  ';
        $_sql_x .= '         where ';
        $_sql_x .= '           a.remain_count > 0  ';
        $_sql_x .= '           and a.is_deleted = false  ';
        $_sql_x .= '           and d.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        if((!empty($this->request->data['TrnPromise']['facilityCode'])) && (!is_null($departmentCode))){
            $_sql_x .= "           and e.facility_code = '" . $this->request->data['TrnPromise']['facilityCode'] . "'";
        }
        if(!is_null($departmentCode)){
            $_sql_x .= "           and b.department_code = '" . $departmentCode. "'"; 
        }
        if(!empty($this->request->data['TrnPromise']['promise_type'])){
            $_sql_x .= '           and a.promise_type = ' . $this->request->data['TrnPromise']['promise_type'];
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql_x .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql_x .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql_x .= '         group by ';
        $_sql_x .= '           c.mst_facility_item_id ';
        $_sql_x .= '           , a.department_id_to ';

        $_sql_stock  = "        select c.mst_facility_item_id";
        $_sql_stock .= '              ,a.department_id_to as mst_department_id_to ';
        $_sql_stock .= "              ,sum(a.remain_count * c.per_unit) as remain_count_per";
        $_sql_stock .= "              ,e.center_stock";
        $_sql_stock .= "          from trn_promises as a";
        $_sql_stock .= "                    left join mst_departments as b";
        $_sql_stock .= "                           on b.id = a.department_id_from";
        $_sql_stock .= "                    left join mst_facilities as b2";
        $_sql_stock .= "                           on b.mst_facility_id = b2.id";
        $_sql_stock .= "                    left join mst_item_units as c";
        $_sql_stock .= "                           on c.id = a.mst_item_unit_id";
        $_sql_stock .= "                    left join mst_facility_items as d";
        $_sql_stock .= "                           on d.id = c.mst_facility_item_id";
        $_sql_stock .= "                    left join (";
        $_sql_stock .= "                               select b.mst_facility_item_id";
        $_sql_stock .= "                                     ,sum((a.stock_count - a.reserve_count) * b.per_unit) as center_stock";
        $_sql_stock .= "                                 from trn_stocks a";
        $_sql_stock .= "                                        inner join mst_item_units b";
        $_sql_stock .= "                                                on a.mst_item_unit_id=b.id";
        $_sql_stock .= "                                        inner join mst_departments c";
        $_sql_stock .= "                                                on a.mst_department_id=c.id";
        $_sql_stock .= "                                where c.mst_facility_id = " . $this->request->data['selected']['headFas'];
        $_sql_stock .= "                                group by b.mst_facility_item_id";
        $_sql_stock .= "                              ) e";
        $_sql_stock .= "                           on d.id=e.mst_facility_item_id";
        $_sql_stock .= "         where a.remain_count > 0";
        $_sql_stock .= "           and a.is_deleted = false";
        $_sql_stock .= "           and d.mst_facility_id = " . $this->request->data['selected']['headFas'];
        if(!is_null($departmentCode)){
            $_sql_stock .= "           and b.department_code = '" . $departmentCode . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql_stock .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql_stock .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql_stock .= "         group by";
        $_sql_stock .= "               c.mst_facility_item_id";
        $_sql_stock .= "              ,e.center_stock";
        //        $_sql_stock .= '              ,a.department_id_from';
        $_sql_stock .= '              ,a.department_id_to';
        $_sql_stock .= "        having sum(a.remain_count * c.per_unit) <= e.center_stock";


        $sql  = ' select ';
        $sql .= '       a.internal_code ';
        $sql .= '     , a.item_name ';
        $sql .= '     , a.standard ';
        $sql .= '     , a.item_code ';
        $sql .= '     , e.dealer_name ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                            as unit_name ';
        $sql .= '     , g.stock_count ';
        $sql .= '     , g.reserve_count ';
        $sql .= '     , g.requested_count ';
        $sql .= '     , coalesce(y.quantity, 0) as quantity';
        $sql .= '     , coalesce(y.quantity, 0) - coalesce(y.remain_count, 0) as fixed_quantity';
        $sql .= '     , coalesce(y.remain_count, 0)  as remain_count';
        $sql .= '     , ( g.stock_count - g.reserve_count /*- g.promise_count*/ ) as stock  ';
        $sql .= '     , z.count  ';
        $sql .= '     , h.fixed_count  ';
        $sql .= '     , h.order_point  ';
        $sql .= '     , j.facility_name  ';
        $sql .= '     , k.name  ';
        $sql .= "     , ( case when b.end_date = '3000/01/01' then '' else  to_char(b.end_date ,'yyyy/mm/dd') end ) as end_date ";
        $sql .= '     , case coalesce(xxx.mst_facility_item_id,0)';
        $sql .= '          when 0 then 0';
        $sql .= '          else 1';
        $sql .= '       end as center_stock_flag';
        $sql .= '   from ';
        $sql .= '     (  ';
        $sql .= $_sql_x;
        $sql .= '     ) as x  ';
        $sql .= '     left join mst_facility_items as a  ';
        $sql .= '       on a.id = x.mst_facility_item_id  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on a.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on c.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on d.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on e.id = a.mst_dealer_id  ';
        $sql .= '     left join mst_departments as f  ';
        $sql .= '       on f.mst_facility_id = a.mst_facility_id  ';
        $sql .= '      and f.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '     left join trn_stocks as g  ';
        $sql .= '       on g.mst_item_unit_id = b.id  ';
        $sql .= '       and g.mst_department_id = f.id  ';
        $sql .= '     left join (  ';
        $sql .= $_sql;
        $sql .= '     ) as y  ';
        $sql .= '       on y.mst_item_unit_id = g.mst_item_unit_id  ';
        $sql .= '    left join ( ';
        $sql .= '      select ';
        $sql .= '            b.mst_facility_item_id ';
        $sql .= '          , count(b.id)            as count  ';
        $sql .= '        from ';
        $sql .= '          mst_facility_items as a  ';
        $sql .= '          left join mst_item_units as b ';
        $sql .= '            on b.mst_facility_item_id = a.id ';
        $sql .= '        where ';
        $sql .= '          a.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        $sql .= '        group by ';
        $sql .= '          b.mst_facility_item_id ';
        $sql .= '    ) as z  ';
        $sql .= '      on z.mst_facility_item_id = x.mst_facility_item_id  ';

        $sql .= '    left join mst_fixed_counts as h   ';
        $sql .= '      on h.trn_stock_id = g.id   ';
        $sql .= '    left join mst_shelf_names as k   ';
        $sql .= '      on k.id = h.mst_shelf_name_id   ';
        $sql .= '    left join mst_transaction_configs as i   ';
        $sql .= '      on i.mst_item_unit_id = b.id   ';
        $sql .= '      and i.is_deleted = false   ';
        $sql .= '    left join mst_transaction_mains as l ';
        $sql .= '      on l.mst_item_unit_id = i.mst_item_unit_id ';
        $sql .= '      and l.mst_facility_id = i.partner_facility_id ';
        $sql .= '    left join mst_facilities as j   ';
        $sql .= '      on j.id = i.mst_facility_id   ';
        $sql .= '    left join (';
        $sql .= $_sql_stock;
        $sql .= '              ) as xxx';
        $sql .= '      on x.mst_facility_item_id=xxx.mst_facility_item_id';
        $sql .= "      and x.mst_department_id=xxx.mst_department_id_to ";
        $sql .= ' group by';
        $sql .= '       a.internal_code ';
        $sql .= '     , a.item_name ';
        $sql .= '     , a.standard ';
        $sql .= '     , a.item_code ';
        $sql .= '     , e.dealer_name ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     ) ,b.per_unit ';
        $sql .= '     , g.stock_count ';
        $sql .= '     , g.reserve_count ';
        $sql .= '     , g.requested_count ';
        $sql .= '     , coalesce(y.quantity, 0) ';
        $sql .= '     , coalesce(y.quantity, 0) - coalesce(y.remain_count, 0) ';
        $sql .= '     , coalesce(y.remain_count, 0) ';
        $sql .= '     , ( g.stock_count - g.reserve_count - g.promise_count )  ';
        $sql .= '     , z.count  ';
        $sql .= '     , h.fixed_count  ';
        $sql .= '     , h.order_point  ';
        $sql .= '     , j.facility_name  ';
        $sql .= '     , k.name  ';
        $sql .= "     , ( case when b.end_date = '3000/01/01' then '' else  to_char(b.end_date ,'yyyy/mm/dd') end ) ";
        $sql .= '     , case coalesce(xxx.mst_facility_item_id,0)';
        $sql .= '          when 0 then 0';
        $sql .= '          else 1';
        $sql .= '       end ';

        $sql .= '   order by ';
        $sql .= '     a.internal_code ';
        $sql .= '     , b.per_unit ';

        return  $this->TrnPromise->query($sql);

    }


    /**
     * 未引当（欠品）リスト
     **/
    private function _StockOutListCSV($departmentCode = null){
        $_sql  = '       select ';
        $_sql .= '             c.id                    as mst_item_unit_id ';
        $_sql .= '           , c.mst_facility_item_id ';
        $_sql .= '           , a.department_id_to      as mst_department_id ';
        $_sql .= '           , sum(a.remain_count)     as remain_count  ';
        $_sql .= '           , sum(a.quantity)         as quantity  ';
        $_sql .= '         from ';
        $_sql .= '           trn_promises as a  ';
        $_sql .= '           left join mst_departments as b  ';
        $_sql .= '             on b.id = a.department_id_from  ';
        $_sql .= '           left join mst_facilities as e  ';
        $_sql .= '             on e.id = b.mst_facility_id  ';
        $_sql .= '           left join mst_item_units as c  ';
        $_sql .= '             on c.id = a.mst_item_unit_id  ';
        $_sql .= '           left join mst_facility_items as d  ';
        $_sql .= '             on d.id = c.mst_facility_item_id  ';
        $_sql .= '         where ';
        $_sql .= '           a.remain_count > 0  ';
        $_sql .= '           and a.is_deleted = false  ';
        $_sql .= '           and d.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        if((!empty($this->request->data['TrnPromise']['facilityCode'])) && (!is_null($departmentCode))){
            $_sql .= "           and e.facility_code = '" . $this->request->data['TrnPromise']['facilityCode'] . "'";
        }
        if(!is_null($departmentCode)){
            $_sql .= "           and b.department_code = '" . $departmentCode . "'";
        }
        if(!empty($this->request->data['TrnPromise']['promise_type'])){
            $_sql .= '           and a.promise_type = ' . $this->request->data['TrnPromise']['promise_type'];
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql .= '         group by ';
        $_sql .= '           c.id ';
        $_sql .= '           , c.mst_facility_item_id ';
        $_sql .= '           , a.department_id_to ';


        $_sql_x  = '       select ';
        $_sql_x .= '             c.mst_facility_item_id ';
        $_sql_x .= '           , a.department_id_to      as mst_department_id ';
        $_sql_x .= '           , sum(a.remain_count)     as remain_count  ';
        $_sql_x .= '           , sum(a.quantity)         as quantity  ';
        $_sql_x .= '         from ';
        $_sql_x .= '           trn_promises as a  ';
        $_sql_x .= '           left join mst_departments as b  ';
        $_sql_x .= '             on b.id = a.department_id_from  ';
        $_sql_x .= '           left join mst_facilities as e  ';
        $_sql_x .= '             on e.id = b.mst_facility_id  ';
        $_sql_x .= '           left join mst_item_units as c  ';
        $_sql_x .= '             on c.id = a.mst_item_unit_id  ';
        $_sql_x .= '           left join mst_facility_items as d  ';
        $_sql_x .= '             on d.id = c.mst_facility_item_id  ';
        $_sql_x .= '         where ';
        $_sql_x .= '           a.remain_count > 0  ';
        $_sql_x .= '           and a.is_deleted = false  ';
        $_sql_x .= '           and d.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        if((!empty($this->request->data['TrnPromise']['facilityCode'])) && (!is_null($departmentCode))){
            $_sql_x .= "           and e.facility_code = '" . $this->request->data['TrnPromise']['facilityCode'] . "'";
        }
        if(!is_null($departmentCode)){
            $_sql_x .= "           and b.department_code = '" . $departmentCode . "'";
        }
        if(!empty($this->request->data['TrnPromise']['promise_type'])){
            $_sql_x .= '           and a.promise_type = ' . $this->request->data['TrnPromise']['promise_type'];
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql_x .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql_x .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql_x .= '         group by ';
        $_sql_x .= '           c.mst_facility_item_id ';
        $_sql_x .= '           , a.department_id_to ';

        $_sql_stock  = "        select c.mst_facility_item_id";
        //        $_sql_stock .= '              ,a.department_id_from as mst_department_id_from ';
        $_sql_stock .= '              ,a.department_id_to as mst_department_id_to ';
        $_sql_stock .= "              ,sum(a.remain_count * c.per_unit) as remain_count_per";
        $_sql_stock .= "              ,e.center_stock";
        $_sql_stock .= "          from trn_promises as a";
        $_sql_stock .= "                    left join mst_departments as b";
        $_sql_stock .= "                           on b.id = a.department_id_from";
        $_sql_stock .= "                    left join mst_facilities as b2";
        $_sql_stock .= "                           on b.mst_facility_id = b2.id";
        $_sql_stock .= "                    left join mst_item_units as c";
        $_sql_stock .= "                           on c.id = a.mst_item_unit_id";
        $_sql_stock .= "                    left join mst_facility_items as d";
        $_sql_stock .= "                           on d.id = c.mst_facility_item_id";
        $_sql_stock .= "                    left join (";
        $_sql_stock .= "                               select b.mst_facility_item_id";
        $_sql_stock .= "                                     ,sum((a.stock_count - a.reserve_count) * b.per_unit) as center_stock";
        $_sql_stock .= "                                 from trn_stocks a";
        $_sql_stock .= "                                        inner join mst_item_units b";
        $_sql_stock .= "                                                on a.mst_item_unit_id=b.id";
        $_sql_stock .= "                                        inner join mst_departments c";
        $_sql_stock .= "                                                on a.mst_department_id=c.id";
        $_sql_stock .= "                                where c.mst_facility_id = " . $this->request->data['selected']['headFas'];
        $_sql_stock .= "                                group by b.mst_facility_item_id";
        $_sql_stock .= "                              ) e";
        $_sql_stock .= "                           on d.id=e.mst_facility_item_id";
        $_sql_stock .= "         where a.remain_count > 0";
        $_sql_stock .= "           and a.is_deleted = false";
        $_sql_stock .= "           and d.mst_facility_id = " . $this->request->data['selected']['headFas'];
        if(!is_null($departmentCode)){
            $_sql_stock .= "           and b.department_code = '" . $departmentCode . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql_stock .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql_stock .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql_stock .= "         group by";
        $_sql_stock .= "               c.mst_facility_item_id";
        $_sql_stock .= "              ,e.center_stock";
        //        $_sql_stock .= '              ,a.department_id_from';
        $_sql_stock .= '              ,a.department_id_to';
        $_sql_stock .= "        having sum(a.remain_count * c.per_unit) <= e.center_stock";

        $sql  = ' select ';
        if($this->request->data['selected']['mode'] == "5"){
            //センター名
            $fasName = $this->getFacilityName($this->request->data['selected']['headFas']);
            $sql .= "'". $fasName . "' as 施設名 ";
        }
        if($this->request->data['selected']['mode'] == "6"){
            //部署名
            $fasName = $this->request->data['TrnPromise']['facilityName'] .' / '. $this->request->data['TrnPromise']['departmentName'];
            $sql .= "'". $fasName . "' as 施設名 ";
                    }

        $sql .= "     , case coalesce(xxx.mst_facility_item_id,0)";
        $sql .= "          when 0 then ''";
        $sql .= "          else '★'";
        $sql .= '       end as 組換可';
        $sql .= '     , a.internal_code                                        as 商品ID';
        $sql .= '     , a.item_name                                            as 商品名';
        $sql .= '     , a.standard                                             as 規格';
        $sql .= '     , a.item_code                                            as 製品番号';
        $sql .= '     , e.dealer_name                                          as 販売元';
        $sql .= '     , k.name                                                 as 棚番号';
        $sql .= '     , j.facility_name                                        as 仕入先';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                                        as 包装単位';
        $sql .= '     , h.fixed_count                                          as 定数';
        $sql .= '     , h.order_point                                          as 発注点';
        $sql .= '     , g.requested_count                                      as 入荷予定';
        $sql .= '     , g.stock_count                                          as 在庫数';
        $sql .= '     , g.reserve_count                                        as 予約数';
        $sql .= '     , coalesce(y.quantity, 0)                                as 要求数';
        $sql .= '     , coalesce(y.quantity, 0) - coalesce(y.remain_count, 0)  as 引当済';
        $sql .= '     , coalesce(y.remain_count, 0)                            as 未引当';
        $sql .= '   from ';
        $sql .= '     (  ';
        $sql .= $_sql_x;
        $sql .= '     ) as x  ';
        $sql .= '     left join mst_facility_items as a  ';
        $sql .= '       on a.id = x.mst_facility_item_id  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on a.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on c.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on d.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on e.id = a.mst_dealer_id  ';
        $sql .= '     left join mst_departments as f  ';
        $sql .= '       on f.mst_facility_id = a.mst_facility_id  ';
        $sql .= '      and f.department_type = ' . Configure::read('DepartmentType.warehouse');
        $sql .= '     left join trn_stocks as g  ';
        $sql .= '       on g.mst_item_unit_id = b.id  ';
        $sql .= '       and g.mst_department_id = f.id  ';
        $sql .= '     left join (  ';
        $sql .= $_sql;
        $sql .= '     ) as y  ';
        $sql .= '       on y.mst_item_unit_id = g.mst_item_unit_id  ';
        $sql .= '    left join ( ';
        $sql .= '      select ';
        $sql .= '            b.mst_facility_item_id ';
        $sql .= '          , count(b.id)            as count  ';
        $sql .= '        from ';
        $sql .= '          mst_facility_items as a  ';
        $sql .= '          left join mst_item_units as b ';
        $sql .= '            on b.mst_facility_item_id = a.id ';
        $sql .= '        where ';
        $sql .= '          a.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        $sql .= '        group by ';
        $sql .= '          b.mst_facility_item_id ';
        $sql .= '    ) as z  ';
        $sql .= '      on z.mst_facility_item_id = x.mst_facility_item_id  ';

        $sql .= '    left join mst_fixed_counts as h   ';
        $sql .= '      on h.trn_stock_id = g.id   ';
        $sql .= '    left join mst_shelf_names as k   ';
        $sql .= '      on k.id = h.mst_shelf_name_id   ';
        $sql .= '    left join mst_transaction_configs as i   ';
        $sql .= '      on i.mst_item_unit_id = b.id   ';
        $sql .= '      and i.is_deleted = false   ';
        $sql .= '    left join mst_transaction_mains as l ';
        $sql .= '      on l.mst_item_unit_id = i.mst_item_unit_id ';
        $sql .= '      and l.mst_facility_id = i.partner_facility_id ';
        $sql .= '    left join mst_facilities as j   ';
        $sql .= '      on j.id = l.mst_facility_id   ';
        $sql .= '    left join (';
        $sql .= $_sql_stock;
        $sql .= '              ) as xxx';
        $sql .= '      on x.mst_facility_item_id=xxx.mst_facility_item_id';
        $sql .= "      and x.mst_department_id=xxx.mst_department_id_to ";
        $sql .= '   order by ';
        $sql .= '     a.internal_code ';
        $sql .= '     , b.per_unit ';

        return  $sql;

    }


    private function _StockOutListHp($facility_id = null , $type = null){
        $_sql  = '       select ';
        $_sql .= '             c.id                    as mst_item_unit_id ';
        $_sql .= '           , c.mst_facility_item_id ';
        $_sql .= '           , a.department_id_from      as mst_department_id_from ';
        $_sql .= '           , a.department_id_to      as mst_department_id_to ';
        $_sql .= '           , sum(a.remain_count)     as remain_count  ';
        $_sql .= '           , sum(a.quantity)         as quantity  ';
        $_sql .= '           , b.mst_facility_id ';
        if($type==1){
            $_sql .= "           , (b2.facility_name || ' / ' || b.department_name) as facility_department_name  ";
            $_sql .= "           , (b.department_name) as department_name  ";

        }
        $_sql .= '         from ';
        $_sql .= '           trn_promises as a  ';
        $_sql .= '           left join mst_departments as b  ';
        $_sql .= '             on b.id = a.department_id_from  ';
        $_sql .= '           left join mst_facilities as b2  ';
        $_sql .= '             on b.mst_facility_id = b2.id';
        $_sql .= '           left join mst_item_units as c  ';
        $_sql .= '             on c.id = a.mst_item_unit_id  ';
        $_sql .= '           left join mst_facility_items as d  ';
        $_sql .= '             on d.id = c.mst_facility_item_id  ';
        $_sql .= '         where ';
        $_sql .= '           a.remain_count > 0  ';
        $_sql .= '           and a.is_deleted = false  ';
        $_sql .= '           and d.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        if(!empty($facility_id)){
            $_sql .= '           and b2.facility_code = ' . $facility_code ;
        }
        if(!empty($this->request->data['TrnPromise']['promise_type'])){
            $_sql .= '           and a.promise_type = ' . $this->request->data['TrnPromise']['promise_type'];
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql .= '         group by ';
        $_sql .= '           c.id ';
        $_sql .= '           , c.mst_facility_item_id ';
        $_sql .= '           , a.department_id_from ';
        $_sql .= '           , a.department_id_to ';
        $_sql .= '           , b.mst_facility_id ';
        if($type==1){
            $_sql .= "           , (b2.facility_name || ' / ' || b.department_name)  ";
            $_sql .= "           , (b.department_name)  ";
        }

        $_sql_x  = '       select ';
        $_sql_x .= '             c.mst_facility_item_id ';
        $_sql_x .= '           , a.department_id_from      as mst_department_id_from ';
        $_sql_x .= '           , a.department_id_to      as mst_department_id_to ';
        $_sql_x .= '           , sum(a.remain_count)     as remain_count  ';
        $_sql_x .= '           , sum(a.quantity)         as quantity  ';
        $_sql_x .= '           , b.mst_facility_id ';
        if($type==1){
            $_sql_x .= "           , (b2.facility_name || ' / ' || b.department_name) as facility_department_name  ";
            $_sql_x .= "           , (b.department_name) as department_name  ";

        }
        $_sql_x .= '         from ';
        $_sql_x .= '           trn_promises as a  ';
        $_sql_x .= '           left join mst_departments as b  ';
        $_sql_x .= '             on b.id = a.department_id_from  ';
        $_sql_x .= '           left join mst_facilities as b2  ';
        $_sql_x .= '             on b.mst_facility_id = b2.id';
        $_sql_x .= '           left join mst_item_units as c  ';
        $_sql_x .= '             on c.id = a.mst_item_unit_id  ';
        $_sql_x .= '           left join mst_facility_items as d  ';
        $_sql_x .= '             on d.id = c.mst_facility_item_id  ';
        $_sql_x .= '         where ';
        $_sql_x .= '           a.remain_count > 0  ';
        $_sql_x .= '           and a.is_deleted = false  ';
        $_sql_x .= '           and d.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        if(!empty($facility_id)){
            $_sql_x .= '           and b.mst_facility_id = ' . $facility_id ;
        }
        if(!empty($this->request->data['TrnPromise']['promise_type'])){
            $_sql_x .= '           and a.promise_type = ' . $this->request->data['TrnPromise']['promise_type'];
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql_x .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql_x .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql_x .= '         group by ';
        $_sql_x .= '            c.mst_facility_item_id ';
        $_sql_x .= '           , a.department_id_from ';
        $_sql_x .= '           , a.department_id_to ';
        $_sql_x .= '           , b.mst_facility_id ';
        if($type==1){
            $_sql_x .= "           , (b2.facility_name || ' / ' || b.department_name)  ";
            $_sql_x .= "           , (b.department_name)  ";
        }

        $_sql_stock  = "        select c.mst_facility_item_id";
        $_sql_stock .= '              ,a.department_id_from as mst_department_id_from ';
        $_sql_stock .= '              ,a.department_id_to as mst_department_id_to ';
        $_sql_stock .= "              ,sum(a.remain_count * c.per_unit) as remain_count_per";
        $_sql_stock .= "              ,e.center_stock";
        $_sql_stock .= "          from trn_promises as a";
        $_sql_stock .= "                    left join mst_departments as b";
        $_sql_stock .= "                           on b.id = a.department_id_from";
        $_sql_stock .= "                    left join mst_facilities as b2";
        $_sql_stock .= "                           on b.mst_facility_id = b2.id";
        $_sql_stock .= "                    left join mst_item_units as c";
        $_sql_stock .= "                           on c.id = a.mst_item_unit_id";
        $_sql_stock .= "                    left join mst_facility_items as d";
        $_sql_stock .= "                           on d.id = c.mst_facility_item_id";
        $_sql_stock .= "                    left join (";
        $_sql_stock .= "                               select b.mst_facility_item_id";
        $_sql_stock .= "                                     ,sum((a.stock_count - a.reserve_count) * b.per_unit) as center_stock";
        $_sql_stock .= "                                 from trn_stocks a";
        $_sql_stock .= "                                        inner join mst_item_units b";
        $_sql_stock .= "                                                on a.mst_item_unit_id=b.id";
        $_sql_stock .= "                                        inner join mst_departments c";
        $_sql_stock .= "                                                on a.mst_department_id=c.id";
        $_sql_stock .= "                                where c.mst_facility_id = " . $this->request->data['selected']['headFas'];
        $_sql_stock .= "                                group by b.mst_facility_item_id";
        $_sql_stock .= "                              ) e";
        $_sql_stock .= "                           on d.id=e.mst_facility_item_id";
        $_sql_stock .= "         where a.remain_count > 0";
        $_sql_stock .= "           and a.is_deleted = false";
        $_sql_stock .= "           and d.mst_facility_id = " . $this->request->data['selected']['headFas'];
        if(!empty($facility_id)){
            $_sql_stock .= '           and b.mst_facility_id = ' . $facility_id ;
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql_stock .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql_stock .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql_stock .= "         group by";
        $_sql_stock .= "               c.mst_facility_item_id";
        $_sql_stock .= "              ,e.center_stock";
        $_sql_stock .= '              ,a.department_id_from';
        $_sql_stock .= '              ,a.department_id_to';
        $_sql_stock .= "        having sum(a.remain_count * c.per_unit) <= e.center_stock";

        $sql  = ' select ';
        $sql .= '       a.internal_code ';
        $sql .= '     , a.item_name ';
        $sql .= '     , a.standard ';
        $sql .= '     , a.item_code ';
        $sql .= '     , e.dealer_name ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                            as unit_name ';
        $sql .= '     , g.stock_count ';
        $sql .= '     , g.reserve_count ';
        $sql .= '     , g.requested_count ';
        $sql .= '     , coalesce(y.quantity, 0) as quantity';
        $sql .= '     , coalesce(y.quantity, 0) - coalesce(y.remain_count, 0) as fixed_quantity';
        $sql .= '     , coalesce(y.remain_count, 0)  as remain_count';
        $sql .= '     , ( g.stock_count - g.reserve_count /*- g.promise_count*/ ) as stock  ';
        $sql .= '     , z.count  ';
        $sql .= '     , h.fixed_count  ';
        $sql .= '     , h.order_point  ';
        $sql .= '     , j.facility_name  ';
        $sql .= '     , k.name  ';
        $sql .= "     , ( case when b.end_date = '3000/01/01' then '' else  to_char(b.end_date ,'yyyy/mm/dd') end ) as end_date ";
        if($type==1){
            $sql .= "     , x.facility_department_name  ";
            $sql .= "     , x.department_name  ";
        }
        $sql .= '     , case coalesce(xxx.mst_facility_item_id,0)';
        $sql .= '          when 0 then 0';
        $sql .= '          else 1';
        $sql .= '       end as center_stock_flag';
        $sql .= '   from ';
        $sql .= '     (  ';
        $sql .= $_sql_x;
        $sql .= '     ) as x  ';
        $sql .= '     left join mst_facility_items as a  ';
        $sql .= '       on a.id = x.mst_facility_item_id  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on a.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on c.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on d.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on e.id = a.mst_dealer_id  ';
        $sql .= '     left join mst_departments as f  ';
        $sql .= '       on f.mst_facility_id = a.mst_facility_id  ';
        $sql .= '      and f.id = x.mst_department_id_to  ';
        $sql .= '     left join trn_stocks as g  ';
        $sql .= '       on g.mst_item_unit_id = b.id  ';
        $sql .= '       and g.mst_department_id = f.id  ';
        $sql .= '     left join (  ';
        $sql .= $_sql;
        $sql .= '     ) as y  ';
        $sql .= '       on y.mst_item_unit_id = g.mst_item_unit_id  ';
        if($type==1){
            $sql .= "      and x.mst_department_id_from=y.mst_department_id_from  ";
        }
        $sql .= '    left join ( ';
        $sql .= '      select ';
        $sql .= '            b.mst_facility_item_id ';
        $sql .= '          , count(b.id)            as count  ';
        $sql .= '        from ';
        $sql .= '          mst_facility_items as a  ';
        $sql .= '          left join mst_item_units as b ';
        $sql .= '            on b.mst_facility_item_id = a.id ';
        $sql .= '        where ';
        $sql .= '          a.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        $sql .= '        group by ';
        $sql .= '          b.mst_facility_item_id ';
        $sql .= '    ) as z  ';
        $sql .= '      on z.mst_facility_item_id = x.mst_facility_item_id  ';

        $sql .= '    left join mst_fixed_counts as h   ';
        $sql .= '      on h.trn_stock_id = g.id   ';
        $sql .= '    left join mst_shelf_names as k   ';
        $sql .= '      on k.id = h.mst_shelf_name_id   ';
        $sql .= '    left join mst_transaction_configs as i   ';
        $sql .= '      on i.mst_item_unit_id = b.id   ';
        $sql .= '      and i.is_deleted = false   ';
        $sql .= '    left join mst_transaction_mains as l ';
        $sql .= '      on l.mst_facility_id = i.partner_facility_id ';
        $sql .= '      and l.mst_item_unit_id = i.mst_item_unit_id';
        $sql .= '    left join mst_facilities as j   ';
        $sql .= '      on j.id = l.mst_facility_id   ';

        $sql .= '    left join (';
        $sql .= $_sql_stock;
        $sql .= '              ) as xxx';
        $sql .= '      on x.mst_facility_item_id=xxx.mst_facility_item_id';
        $sql .= "      and x.mst_department_id_from=xxx.mst_department_id_from  ";

        $sql .= ' group by';
        $sql .= '       a.internal_code ';
        $sql .= '     , a.item_name ';
        $sql .= '     , a.standard ';
        $sql .= '     , a.item_code ';
        $sql .= '     , e.dealer_name ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     ),b.per_unit        ';
        $sql .= '     , g.stock_count ';
        $sql .= '     , g.reserve_count ';
        $sql .= '     , g.requested_count ';
        $sql .= '     , coalesce(y.quantity, 0) ';
        $sql .= '     , coalesce(y.quantity, 0) - coalesce(y.remain_count, 0) ';
        $sql .= '     , coalesce(y.remain_count, 0) ';
        $sql .= '     , ( g.stock_count - g.reserve_count - g.promise_count )  ';
        $sql .= '     , z.count  ';
        $sql .= '     , h.fixed_count  ';
        $sql .= '     , h.order_point  ';
        $sql .= '     , j.facility_name  ';
        $sql .= '     , k.name  ';
        $sql .= "     , ( case when b.end_date = '3000/01/01' then '' else  to_char(b.end_date ,'yyyy/mm/dd') end )  ";
        if($type==1){
            $sql .= "     , x.facility_department_name  ";
            $sql .= "     , x.department_name  ";
        }
        $sql .= '     , case coalesce(xxx.mst_facility_item_id,0)';
        $sql .= '          when 0 then 0';
        $sql .= '          else 1';
        $sql .= '       end ';

        $sql .= '   order by ';
        if($type==1){
            $sql .= "      x.facility_department_name ,  ";
            $sql .= "      x.department_name ,  ";
        }
        $sql .= '       a.internal_code ';
        $sql .= '     , b.per_unit ';

        return  $this->TrnPromise->query($sql);

    }


    private function _StockOutListHpCSV($facility_id = null , $type = null){
        $_sql  = '       select ';
        $_sql .= '             c.id                    as mst_item_unit_id ';
        $_sql .= '           , c.mst_facility_item_id ';
        $_sql .= '           , a.department_id_from      as mst_department_id_from ';
        $_sql .= '           , a.department_id_to      as mst_department_id_to ';
        $_sql .= '           , sum(a.remain_count)     as remain_count  ';
        $_sql .= '           , sum(a.quantity)         as quantity  ';
        $_sql .= '           , b.mst_facility_id ';
        if($type==1){
            $_sql .= "           , (b2.facility_name || ' / ' || b.department_name) as facility_department_name  ";
            $_sql .= "           , (b.department_name) as department_name  ";

        }
        $_sql .= '         from ';
        $_sql .= '           trn_promises as a  ';
        $_sql .= '           left join mst_departments as b  ';
        $_sql .= '             on b.id = a.department_id_from  ';
        $_sql .= '           left join mst_facilities as b2  ';
        $_sql .= '             on b.mst_facility_id = b2.id';
        $_sql .= '           left join mst_item_units as c  ';
        $_sql .= '             on c.id = a.mst_item_unit_id  ';
        $_sql .= '           left join mst_facility_items as d  ';
        $_sql .= '             on d.id = c.mst_facility_item_id  ';
        $_sql .= '         where ';
        $_sql .= '           a.remain_count > 0  ';
        $_sql .= '           and a.is_deleted = false  ';
        $_sql .= '           and d.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        if(!empty($facility_id)){
            $_sql .= '           and b.mst_facility_id = ' . $facility_id ;
        }
        if(!empty($this->request->data['TrnPromise']['promise_type'])){
            $_sql .= '           and a.promise_type = ' . $this->request->data['TrnPromise']['promise_type'];
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql .= '         group by ';
        $_sql .= '           c.id ';
        $_sql .= '           , c.mst_facility_item_id ';
        $_sql .= '           , a.department_id_from ';
        $_sql .= '           , a.department_id_to ';
        $_sql .= '           , b.mst_facility_id ';
        if($type==1){
            $_sql .= "           , (b2.facility_name || ' / ' || b.department_name)  ";
            $_sql .= "           , (b.department_name)  ";
        }

        $_sql_x  = '       select ';
        $_sql_x .= '             c.mst_facility_item_id ';
        $_sql_x .= '           , a.department_id_from      as mst_department_id_from ';
        $_sql_x .= '           , a.department_id_to      as mst_department_id_to ';
        $_sql_x .= '           , sum(a.remain_count)     as remain_count  ';
        $_sql_x .= '           , sum(a.quantity)         as quantity  ';
        $_sql_x .= '           , b.mst_facility_id ';
        if($type==1){
            $_sql_x .= "           , (b2.facility_name || ' / ' || b.department_name) as facility_department_name  ";
            $_sql_x .= "           , (b.department_name) as department_name  ";

        }
        $_sql_x .= '         from ';
        $_sql_x .= '           trn_promises as a  ';
        $_sql_x .= '           left join mst_departments as b  ';
        $_sql_x .= '             on b.id = a.department_id_from  ';
        $_sql_x .= '           left join mst_facilities as b2  ';
        $_sql_x .= '             on b.mst_facility_id = b2.id';
        $_sql_x .= '           left join mst_item_units as c  ';
        $_sql_x .= '             on c.id = a.mst_item_unit_id  ';
        $_sql_x .= '           left join mst_facility_items as d  ';
        $_sql_x .= '             on d.id = c.mst_facility_item_id  ';
        $_sql_x .= '         where ';
        $_sql_x .= '           a.remain_count > 0  ';
        $_sql_x .= '           and a.is_deleted = false  ';
        $_sql_x .= '           and d.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        if(!empty($facility_id)){
            $_sql_x .= '           and b.mst_facility_id = ' . $facility_id ;
        }
        if(!empty($this->request->data['TrnPromise']['promise_type'])){
            $_sql_x .= '           and a.promise_type = ' . $this->request->data['TrnPromise']['promise_type'];
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql_x .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql_x .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql_x .= '         group by ';
        $_sql_x .= '            c.mst_facility_item_id ';
        $_sql_x .= '           , a.department_id_from ';
        $_sql_x .= '           , a.department_id_to ';
        $_sql_x .= '           , b.mst_facility_id ';
        if($type==1){
            $_sql_x .= "           , (b2.facility_name || ' / ' || b.department_name)  ";
            $_sql_x .= "           , (b.department_name)  ";
        }

        $_sql_stock  = "        select c.mst_facility_item_id";
        $_sql_stock .= '              ,a.department_id_from as mst_department_id_from ';
        $_sql_stock .= '              ,a.department_id_to as mst_department_id_to ';
        $_sql_stock .= "              ,sum(a.remain_count * c.per_unit) as remain_count_per";
        $_sql_stock .= "              ,e.center_stock";
        $_sql_stock .= "          from trn_promises as a";
        $_sql_stock .= "                    left join mst_departments as b";
        $_sql_stock .= "                           on b.id = a.department_id_from";
        $_sql_stock .= "                    left join mst_facilities as b2";
        $_sql_stock .= "                           on b.mst_facility_id = b2.id";
        $_sql_stock .= "                    left join mst_item_units as c";
        $_sql_stock .= "                           on c.id = a.mst_item_unit_id";
        $_sql_stock .= "                    left join mst_facility_items as d";
        $_sql_stock .= "                           on d.id = c.mst_facility_item_id";
        $_sql_stock .= "                    left join (";
        $_sql_stock .= "                               select b.mst_facility_item_id";
        $_sql_stock .= "                                     ,sum((a.stock_count - a.reserve_count) * b.per_unit) as center_stock";
        $_sql_stock .= "                                 from trn_stocks a";
        $_sql_stock .= "                                        inner join mst_item_units b";
        $_sql_stock .= "                                                on a.mst_item_unit_id=b.id";
        $_sql_stock .= "                                        inner join mst_departments c";
        $_sql_stock .= "                                                on a.mst_department_id=c.id";
        $_sql_stock .= "                                where c.mst_facility_id = " . $this->request->data['selected']['headFas'];
        $_sql_stock .= "                                group by b.mst_facility_item_id";
        $_sql_stock .= "                              ) e";
        $_sql_stock .= "                           on d.id=e.mst_facility_item_id";
        $_sql_stock .= "         where a.remain_count > 0";
        $_sql_stock .= "           and a.is_deleted = false";
        $_sql_stock .= "           and d.mst_facility_id = " . $this->request->data['selected']['headFas'];
        if(!empty($facility_id)){
            $_sql_stock .= '           and b.mst_facility_id = ' . $facility_id ;
        }
        if(!empty($this->request->data['TrnPromise']['work_date1'])){
            $_sql_stock .= "           and a.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] . "'";
        }
        if(!empty($this->request->data['TrnPromise']['work_date2'])){
            $_sql_stock .= "           and a.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] . "'";
        }
        $_sql_stock .= "         group by";
        $_sql_stock .= "               c.mst_facility_item_id";
        $_sql_stock .= "              ,e.center_stock";
        $_sql_stock .= '              ,a.department_id_from';
        $_sql_stock .= '              ,a.department_id_to';
        $_sql_stock .= "        having sum(a.remain_count * c.per_unit) <= e.center_stock";

        $sql  = ' select ';
        $sql .= '       x.facility_department_name as 施設名';
        $sql .= "     , case coalesce(xxx.mst_facility_item_id,0)";
        $sql .= "          when 0 then ''";
        $sql .= "          else '★'";
        $sql .= '       end                        as 組換可';

        $sql .= '     , a.internal_code  as 商品ID';
        $sql .= '     , a.item_name      as 商品名';
        $sql .= '     , a.standard  as 規格';
        $sql .= '     , a.item_code as 製品番号 ';
        $sql .= '     , e.dealer_name  as 販売元';
        $sql .= '     , k.name         as 棚番号';
        $sql .= '     , j.facility_name  as 仕入先';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                            as 包装単位 ';
        $sql .= '     , h.fixed_count  as 定数';
        $sql .= '     , h.order_point                                          as 発注点';
        $sql .= '     , g.requested_count                                      as 入荷予定';
        $sql .= '     , g.stock_count                                          as 在庫数';
        $sql .= '     , g.reserve_count                                        as 予約数';
        $sql .= '     , coalesce(y.quantity, 0)                                as 要求数';
        $sql .= '     , coalesce(y.quantity, 0) - coalesce(y.remain_count, 0)  as 引当済';
        $sql .= '     , coalesce(y.remain_count, 0)                            as 未引当';
        $sql .= '   from ';
        $sql .= '     (  ';
        $sql .= $_sql_x;
        $sql .= '     ) as x  ';
        $sql .= '     left join mst_facility_items as a  ';
        $sql .= '       on a.id = x.mst_facility_item_id  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on a.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on c.id = b.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on d.id = b.per_unit_name_id  ';
        $sql .= '     left join mst_dealers as e  ';
        $sql .= '       on e.id = a.mst_dealer_id  ';
        $sql .= '     left join mst_departments as f  ';
        $sql .= '       on f.mst_facility_id = a.mst_facility_id  ';
        $sql .= '      and f.id = x.mst_department_id_to  ';
        $sql .= '     left join trn_stocks as g  ';
        $sql .= '       on g.mst_item_unit_id = b.id  ';
        $sql .= '       and g.mst_department_id = f.id  ';
        $sql .= '     left join (  ';
        $sql .= $_sql;
        $sql .= '     ) as y  ';
        $sql .= '       on y.mst_item_unit_id = g.mst_item_unit_id  ';
        if($type==1){
            $sql .= "      and x.mst_department_id_from=y.mst_department_id_from  ";
        }
        $sql .= '    left join ( ';
        $sql .= '      select ';
        $sql .= '            b.mst_facility_item_id ';
        $sql .= '          , count(b.id)            as count  ';
        $sql .= '        from ';
        $sql .= '          mst_facility_items as a  ';
        $sql .= '          left join mst_item_units as b ';
        $sql .= '            on b.mst_facility_item_id = a.id ';
        $sql .= '        where ';
        $sql .= '          a.mst_facility_id = ' . $this->request->data['selected']['headFas'];
        $sql .= '        group by ';
        $sql .= '          b.mst_facility_item_id ';
        $sql .= '    ) as z  ';
        $sql .= '      on z.mst_facility_item_id = x.mst_facility_item_id  ';

        $sql .= '    left join mst_fixed_counts as h   ';
        $sql .= '      on h.trn_stock_id = g.id   ';
        $sql .= '    left join mst_shelf_names as k   ';
        $sql .= '      on k.id = h.mst_shelf_name_id   ';
        $sql .= '    left join mst_transaction_configs as i   ';
        $sql .= '      on i.mst_item_unit_id = b.id   ';
        $sql .= '      and i.is_deleted = false   ';
        $sql .= '    left join mst_transaction_main as l ';
        $sql .= '      on l.mst_item_unit_id = i.mst_item_unit_id ';
        $sql .= '     and l.mst_facility_id = i.partner_facility_id ';
        $sql .= '    left join mst_facilities as j   ';
        $sql .= '      on j.id = l.mst_facility_id   ';

        $sql .= '    left join (';
        $sql .= $_sql_stock;
        $sql .= '              ) as xxx';
        $sql .= '      on x.mst_facility_item_id=xxx.mst_facility_item_id';
        $sql .= "      and x.mst_department_id_from=xxx.mst_department_id_from  ";

        $sql .= '   order by ';
        if($type==1){
            $sql .= "      x.facility_department_name ,  ";
            $sql .= "      x.department_name ,  ";
        }
        $sql .= '       a.internal_code ';
        $sql .= '     , b.per_unit ';

        return  $sql;

    }


    /**
     * 要求IDを受けてステータスを返す
     */
    public function get_status () {
        Configure::write('debug', 0);
        if($this->RequestHandler->isAjax()) {
            $this->autoRender = false;

            $sql  = ' select ';
            $sql .= '       a.quantity                                          as quantity ';
            $sql .= '     , coalesce(to_char(a.work_date,\'YYYY/mm/dd\') ,\'\') as promise_date ';
            $sql .= '     , coalesce(to_char(b.work_date,\'YYYY/mm/dd\') ,\'\') as fix_promise_date ';
            $sql .= '     , coalesce(to_char(c.work_date,\'YYYY/mm/dd\') ,\'\') as sticker_issue_date ';
            $sql .= '     , coalesce(d.facility_sticker_no,\'\')                as facility_sticker_no ';
            $sql .= '     , coalesce(c.hospital_sticker_no,\'\')                as hospital_sticker_no ';
            $sql .= '     , coalesce(to_char(e.work_date,\'YYYY/mm/dd\') ,\'\') as shipping_date ';
            $sql .= '     , coalesce(to_char(f.work_date,\'YYYY/mm/dd\') ,\'\') as receiving_date  ';
            $sql .= '   from ';
            $sql .= '     trn_promises as a  ';
            $sql .= '     left join trn_fixed_promises as b  ';
            $sql .= '       on b.trn_promise_id = a.id  ';
            $sql .= '      and b.is_deleted = false  ';
            $sql .= '     left join trn_sticker_issues as c  ';
            $sql .= '       on c.trn_fixed_promise_id = b.id  ';
            $sql .=  '     and c.is_deleted = false  ';
            $sql .= '     left join trn_stickers as d  ';
            $sql .= '       on d.hospital_sticker_no = c.hospital_sticker_no  ';
            $sql .= '      and d.is_deleted = false  ';
            $sql .= '     left join trn_shippings as e  ';
            $sql .= '       on e.id = d.trn_shipping_id  ';
            $sql .= '      and e.is_deleted = false  ';
            $sql .= '     left join trn_receivings as f  ';
            $sql .= '       on f.id = d.receipt_id  ';
            $sql .= '      and f.is_deleted = false  ';
            $sql .= '   where ';
            $sql .= '     a.id = ' . $_POST['id'];
            $sql .= '     and a.is_deleted = false  ';
            $sql .= '   order by ';
            $sql .= '     c.hospital_sticker_no ';
            $res = $this->TrnPromise->query($sql);

            return (json_encode($res));
        } else {
            $this->cakeError('error404');
        }
    }

    /**
     * 要求履歴CSV出力
     */
    function export_csv() {

        $sql = $this->_getPromiseCSV($this->request->data);
        $this->db_export_csv($sql , "要求作成履歴", '/promises/history');
    }

    private function _getPromiseCSV($data) {
        $this->request->data = $data;
        App::import('Sanitize');
        // 検索処理

        $where = "";
        //要求番号
        if( (!empty($this->request->data['TrnPromise']['work_no']) ) ){
            $where .=" and TrnPromiseHeaders.work_no LIKE '%".Sanitize::escape($this->request->data['TrnPromise']['work_no'])."\'";
        }
        //要求日FROM
        if( (!empty($this->request->data['TrnPromise']['work_date1']) ) ){
            $where .= " and TrnPromise.work_date >= '" . $this->request->data['TrnPromise']['work_date1'] ."'";
        }
        //要求日TO
        if( (!empty($this->request->data['TrnPromise']['work_date2']) ) ){
            $where .= " and TrnPromise.work_date <= '" . $this->request->data['TrnPromise']['work_date2'] ."'";
        }

        //商品ID
        if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
            $where .= " and MstFacilityItem.internal_code = '" . Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']) . "'";
        }
        //製品番号
        if(!empty($this->request->data['MstFacilityItem']['item_code'])){
            $where .= ' and MstFacilityItem.item_code LIKE \'%'.Sanitize::escape($this->request->data['MstFacilityItem']['item_code']).'%\'';
        }
        //JAN(前方一致）
        if(!empty($this->request->data['MstFacilityItem']['jan_code'])){
            $where .= " and MstFacilityItem.jan_code LIKE " . Sanitize::escape($this->request->data['MstFacilityItem']['jan_code']) . "%'";
        }
        //規格
        if(!empty($this->request->data['MstFacilityItem']['standard'])){
            $where .= " and MstFacilityItem.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }


        //取消も表示
        if(isset($this->request->data['TrnPromise']['delete_opt']) && $this->request->data['TrnPromise']['delete_opt'] != "1"){
            $where .= ' and TrnPromise.is_deleted = false ';
        }

        //要求施設
        if(!empty($this->request->data['TrnPromise']['facility_id'])){
            $where .= " and MstFacility.id = '". $this->request->data['TrnPromise']['facility_id'] ."'";

            //要求部署
            if(!empty($this->request->data['TrnPromise']['department_id'])){
                $where .= " and MstDepartment.id = '" . $this->request->data['TrnPromise']['department_id']."'";
            }
        }
        //管理区分
        if(!empty($this->request->data['TrnPromise']['promise_type'])){
            $where .= ' and TrnPromise.promise_type = ' . $this->request->data['TrnPromise']['promise_type'];
        }


        //商品名
        if(!empty($this->request->data['MstFacilityItem']['item_name'])){
            $where .= " and MstFacilityItem.item_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . "%'";
        }

        //販売元
        if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
            $where .= " and MstDealer.dealer_name LIKE '%" . Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name']) . "%'";
        }

        //作業区分
        if(!empty($this->request->data['TrnPromise']['work_type'])) {
            $where .= ' and TrnPromise.work_type = ' . $this->request->data['TrnPromise']['work_type'];
        }


        //欠品
        if(isset($this->request->data['TrnPromise']['opt']) && $this->request->data['TrnPromise']['opt'] == "0"){
            $where .= ' and TrnPromise.is_deleted = FALSE ';
            $where .= ' and TrnPromise.remain_count >= 1 ';
        }

        $sql  = 'select';
        $sql .= '   TrnPromiseHeaders.work_no                         as 要求番号';
        $sql .= " , to_char(TrnPromiseHeaders.work_date,'YYYY/mm/dd') as 要求日";
        $sql .= ' , MstFacility.facility_name                         as 施設名';
        $sql .= ' , MstDepartment.department_name                     as 部署名';
        $sql .= ',  MstFacilityItem.internal_code                     as "商品ID"';
        $sql .= ', ( case TrnPromise.promise_status ';
        foreach(Configure::read('PromiseStatus.list') as $k=>$v){
            $sql .= " when $k then '$v' ";
        }
        $sql .= ' end )                                               as 状態';
        $sql .= ', MstFacilityItem.item_name                          as 商品名';
        $sql .= ', MstFacilityItem.standard                           as 規格';
        $sql .= ', MstFacilityItem.item_code                          as 製品番号';
        $sql .= ', MstDealer.dealer_name                              as 販売元名';

        $sql .= ', (case when MstItemUnit.per_unit = 1 then MstUnitNames.unit_name ';
        $sql .= "    else MstUnitNames.unit_name || '(' || MstItemUnit.per_unit || PerUnitNames.unit_name || ')' ";
        $sql .= '  end )                                              as 包装単位名';
        $sql .= ', ( case TrnPromise.promise_type ';
        foreach(Configure::read('PromiseType.list') as $k=>$v){
            $sql .= " when $k then '$v' ";
        }
        $sql .= ' end )                                               as 管理区分';
        $sql .= ', TrnPromise.stock_count                             as 在庫数';
        $sql .= ', TrnPromise.fixed_count                             as 定数';
        $sql .= ', TrnPromise.requested_count                         as 要求済数';
        $sql .= ', TrnPromise.consume_count                           as 消費数';
        $sql .= ', TrnPromise.quantity                                as 作成数';
        $sql .= ', TrnPromise.unreceipt_count                         as 未受領数';
        $sql .= ', MstClass.name                                      as 作業区分';
        $sql .= ', MstUser.user_name                                  as 更新者名';
        $sql .= ', ( CASE ';
        $sql .= "         WHEN TrnPromise.is_deleted = TRUE THEN '取消済みです' ";
        $sql .= "         WHEN TrnPromise.promise_status = ".Configure::read('PromiseStatus.Matching')." THEN '引当確定済みです' ";
        $sql .= "         WHEN TrnPromise.remain_count = 0 AND TrnPromise.quantity = 0 THEN '定数≦在庫の為、要求は作成されませんでした' ";
        $sql .= "         WHEN TrnPromise.remain_count = 0 THEN '引当確定済みです' ";
        $sql .= '         ELSE TrnPromise.recital' ;
        $sql .= ' END )                                               as 備考';

        $sql.= ' from';
        $sql.= ' trn_promise_headers as TrnPromiseHeaders ';
        $sql.= '    left join mst_departments as MstDepartment ';
        $sql.= '      on MstDepartment.id = TrnPromiseHeaders.department_id_from ';
        $sql.= '    left join mst_facilities as MstFacility ';
        $sql.= '      on MstFacility.id = MstDepartment.mst_facility_id';
        $sql.= '    left join trn_promises as TrnPromise';
        $sql.= '      on TrnPromiseHeaders.id = TrnPromise.trn_promise_header_id';
        $sql.= '    left join mst_item_units as MstItemUnit ';
        $sql.= '      on MstItemUnit.id = TrnPromise.mst_item_unit_id ';
        $sql.= '    left join mst_facility_items as MstFacilityItem';
        $sql.= '      on MstItemUnit.mst_facility_item_id = MstFacilityItem.id ';
        $sql.= '    left join mst_dealers as MstDealer';
        $sql.= '      on MstFacilityItem.mst_dealer_id = MstDealer.id ';
        $sql.= '    left join mst_unit_names as MstUnitNames';
        $sql.= '      on MstItemUnit.mst_unit_name_id = MstUnitNames.id ';
        $sql.= '    left join mst_unit_names as PerUnitNames';
        $sql.= '      on MstItemUnit.per_unit_name_id = PerUnitNames.id ';
        $sql.= '    left join mst_classes as MstClass';
        $sql.= '      on TrnPromise.work_type = MstClass.id ';
        $sql.= '    inner join mst_facility_relations as MstfacilityRelation';
        $sql.= '      on MstfacilityRelation.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected') ;
        $sql.= '      and MstfacilityRelation.partner_facility_id = MstFacility.id ';
        $sql.= '      and MstFacilityItem.mst_facility_id = '.$this->Session->read('Auth.facility_id_selected');
        $sql.= '    inner join mst_user_belongings as MstUserBelonging ';
        $sql.= '      on MstUserBelonging.mst_user_id =  ' . $this->Session->read('Auth.MstUser.id');
        $sql.= '      and MstUserBelonging.mst_facility_id = MstFacility.id ';
        $sql.= '      and MstUserBelonging.is_deleted = false';
        $sql.= '    left join mst_users as MstUser';
        $sql.= '      on MstUser.id = TrnPromise.modifier';
        $sql.= '  where';
        $sql.= '    1 = 1 ';
        $sql.= $where ;
        $sql.= '  order by TrnPromiseHeaders.work_no , TrnPromise.work_seq desc ' ;

        return $sql;
    }

    /**
     * 引当作成履歴CSV出力
     */
    function decision_export_csv() {
        $sql = $this->_getFixedPromiseCSV($this->request->data);
        $this->db_export_csv($sql , "引当作成履歴", '/promises/decision_history/');
    }


    private function _getFixedPromiseCSV($data) {
        $this->request->data = $data;
        App::import('Sanitize');
        // 検索処理
        $where = "";
        //引当番号
        if( (!empty($this->request->data['TrnFixedPromiseHeader']['work_no']) ) ){
            $where .=' and a.work_no LIKE \'%'.Sanitize::escape($this->request->data['TrnFixedPromiseHeader']['work_no']).'%\'';
        }
        //引当日FROM
        if( (!empty($this->request->data['TrnFixedPromiseHeader']['work_date1']) ) ){
            $where .= ' and b.work_date >= \'' . $this->request->data['TrnFixedPromiseHeader']['work_date1'] .'\'';
        }
        //引当日TO
        if( (!empty($this->request->data['TrnFixedPromiseHeader']['work_date2']) ) ){
            $where .= " and b.work_date <= '" . $this->request->data['TrnFixedPromiseHeader']['work_date2'] ."'";
        }

        //商品ID
        if(!empty($this->request->data['MstFacilityItem']['internal_code'])){
            $where .= " and g.internal_code = '" . Sanitize::escape($this->request->data['MstFacilityItem']['internal_code']) . "'";
        }
        //製品番号
        if(!empty($this->request->data['MstFacilityItem']['item_code'])){
            $where .= " and g.item_code LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['item_code'])."%'";
        }
        //JAN(前方一致）
        if(!empty($this->request->data['MstFacilityItem']['jan_code'])){
            $where .= " and g.jan_code LIKE  '" . Sanitize::escape($this->request->data['MstFacilityItem']['jan_code']) . "%'";
        }
        //規格
        if(!empty($this->request->data['MstFacilityItem']['standard'])){
            $where .= " and g.standard LIKE '%".Sanitize::escape($this->request->data['MstFacilityItem']['standard'])."%'";
        }

        //取消も表示
        if(isset($this->request->data['TrnFixedPromiseHeader']['delete_opt'])){
            $where .= ' and b.is_deleted = false ';
        }
        //未処理のみ
        if(isset($this->request->data['TrnFixedPromiseHeader']['remain_count'])){
            $where .= ' and b.remain_count > 0 ';
        }

        //要求施設
        if(!empty($this->request->data['TrnFixedPromiseHeader']['facility_id'])){
            $where .= ' and d.id = '. $this->request->data['TrnFixedPromiseHeader']['facility_id'];

            //要求部署
            if(!empty($this->request->data['TrnFixedPromiseHeader']['department_id'])){
                $where .= ' and c.id = ' . $this->request->data['TrnFixedPromiseHeader']['department_id'];
            }
        }
        //管理区分
        if(!empty($this->request->data['TrnFixedPromiseHeader']['promise_type'])){
            $where .= ' and b.promise_type = ' . $this->request->data['TrnFixedPromiseHeader']['promise_type'];
        }


        //商品名
        if(!empty($this->request->data['MstFacilityItem']['item_name'])){
            $where .= ' and g.item_name LIKE \'%' . Sanitize::escape($this->request->data['MstFacilityItem']['item_name']) . '%\'';
        }

        //販売元
        if(!empty($this->request->data['MstFacilityItem']['dealer_name'])){
            $where .= ' and h.dealer_name LIKE \'%' . Sanitize::escape($this->request->data['MstFacilityItem']['dealer_name']) . '%\'';
        }

        //作業区分
        if(!empty($this->request->data['TrnFixedPromiseHeader']['work_type'])) {
            $where .= ' and b.work_type = ' . $this->request->data['TrnFixedPromiseHeader']['work_type'];
        }

        //削除済みの表示
        if(isset($this->request->data['TrnFixedPromiseHeader']['delete_opt'])){
            if($this->request->data['TrnFixedPromiseHeader']['delete_opt']!=1){
                $where .=' and b.is_deleted = false';
            }

        }

        $sql = ' select ';
        $sql .= '       a.work_no           as 引当番号';
        $sql .= "     , to_char(a.work_date ,'yyyy/mm/dd' )  ";
        $sql .= '                           as 引当日';
        $sql .= '     , d.facility_name     as 施設名';
        $sql .= '     , c.department_name   as 部署名';
        $sql .= '     , g.internal_code     as "商品ID"';

        $sql .= '      , ( case b.fixed_promise_status ';
        foreach(Configure::read('PromiseStatus.list') as $key => $val){
            $sql .= "           when " . $key . " then '" . $val ."' ";
        }
        $sql .= "        else '' ";
        $sql .= '        end )              as 状態';
        $sql .= '     , g.item_name         as 商品名';
        $sql .= '     , g.standard          as 規格';
        $sql .= '     , g.item_code         as 製品番号';
        $sql .= '     , h.dealer_name       as 販売元名';
        $sql .= '     , ( case when f.per_unit = 1 then i.unit_name ';
        $sql .= "              else i.unit_name || '(' || f.per_unit || j.unit_name || ')' ";
        $sql .= '        end )              as 包装単位名';
        $sql .= '     , b.quantity          as 数量';
        $sql .= '     , b.remain_count      as 残数';
        $sql .= '     , l.name              as 作業区分';
        $sql .= '     , k.user_name         as 更新者名';
        $sql .= "      , ( case when b.is_deleted = true then '削除済み'" ;
        $sql .= "               when b.fixed_promise_status = ".Configure::read('PromiseStatus.SealIssue')." then '既にシールが発行されています。'";
        $sql .= '               else b.recital';
        $sql .= '          end )            as 備考';

        $sql .= '   from ';
        $sql .= '     trn_fixed_promise_headers as a  ';
        $sql .= '     left join trn_fixed_promises as b  ';
        $sql .= '       on b.trn_fixed_promise_header_id = a.id  ';
        $sql .= '     left join mst_departments as c  ';
        $sql .= '       on c.id = a.department_id_from  ';
        $sql .= '     left join mst_facilities as d  ';
        $sql .= '       on d.id = c.mst_facility_id  ';
        $sql .= '     left join mst_users as e  ';
        $sql .= '       on e.id = a.creater  ';
        $sql .= '     left join mst_item_units as f  ';
        $sql .= '       on f.id = b.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as g  ';
        $sql .= '       on g.id = f.mst_facility_item_id  ';
        $sql .= '     left join mst_dealers as h  ';
        $sql .= '       on h.id = g.mst_dealer_id  ';
        $sql .= '     left join mst_unit_names as i  ';
        $sql .= '       on i.id = f.mst_unit_name_id  ';
        $sql .= '     left join mst_unit_names as j  ';
        $sql .= '       on j.id = f.per_unit_name_id  ';
        $sql .= '     left join mst_users as k  ';
        $sql .= '       on k.id = b.modifier ';
        $sql .= '     left join mst_classes as l  ';
        $sql .= '       on l.id = b.work_type ';
        $sql .= '     inner join mst_user_belongings as m';
        $sql .= '       on m.mst_user_id = ' . $this->Session->read('Auth.MstUser.id');
        $sql .= '       and m.mst_facility_id = d.id';
        $sql .= '       and m.is_deleted = false';
        $sql .= '   where ';
        $sql .= '     g.mst_facility_id = ' .  $this->Session->read('Auth.facility_id_selected');
        $sql .= $where;

        $sql.='  order by a.work_no ' ;
        return $sql;
    }
    
    /* 引当完了後、組み換え払い出し可能か判定 */
    function checkAlert($data){
        $sql  = ' select ';
        $sql .= '       c.internal_code ';
        $sql .= '     , b.per_unit ';
        $sql .= '     , sum(  ';
        $sql .= '       (  ';
        $sql .= '         case  ';
        $sql .= '           when f.stock_count - f.reserve_count > 0  ';
        $sql .= '           then f.stock_count - f.reserve_count  ';
        $sql .= '           else 0  ';
        $sql .= '           end ';
        $sql .= '       ) * d.per_unit ';
        $sql .= '     )                            as stock_count  ';
        $sql .= '   from ';
        $sql .= '     (  ';
        $sql .= '       select ';
        $sql .= '             x1.mst_item_unit_id ';
        $sql .= '           , sum(x1.remain_count)  ';
        $sql .= '         from ';
        $sql .= '           trn_promises as x1';
        $sql .= '         left join mst_departments as x2 ';
        $sql .= '           on x2.id = x1.department_id_from ';
        $sql .= '         left join mst_facilities as x3 ';
        $sql .= '           on x3.id = x2.mst_facility_id ';
        $sql .= '         where ';
        $sql .= '           x1.remain_count > 0  ';
        $sql .= '           and x1.is_deleted = false  ';
        if($data['TrnPromise']['facilityCode'] != ''){
            $sql .= " and x3.facility_code = '" . $data['TrnPromise']['facilityCode'] . "'";
        }
        if($data['TrnPromise']['departmentCode'] != ''){
            $sql .= " and x2.department_code = '" . $data['TrnPromise']['departmentCode'] . "'";
        }
        if($data['TrnPromise']['work_date1'] != ''){
            $sql .= " and x1.work_date >= '" . $data['TrnPromise']['work_date1'] . "'";
        }
        if($data['TrnPromise']['work_date2'] != ''){
            $sql .= " and x1.work_date <= '" . $data['TrnPromise']['work_date2'] . "'";
        }
        if($data['TrnPromise']['promise_type'] != ''){
            $sql .= " and x1.promise_type = '" . $data['TrnPromise']['promise_type'] . "'";
        }
        $sql .= '         group by ';
        $sql .= '           mst_item_unit_id ';
        $sql .= '     ) as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on b.id = a.mst_item_unit_id  ';
        $sql .= '     left join mst_facility_items as c  ';
        $sql .= '       on c.id = b.mst_facility_item_id  ';
        $sql .= '     left join mst_item_units as d  ';
        $sql .= '       on d.mst_facility_item_id = c.id  ';
        $sql .= '     left join mst_departments as e  ';
        $sql .= '       on e.mst_facility_id = c.mst_facility_id  ';
        $sql .= '     left join trn_stocks as f  ';
        $sql .= '       on f.mst_item_unit_id = d.id  ';
        $sql .= '       and f.mst_department_id = e.id  ';
        $sql .= '   where ';
        $sql .= '     c.mst_facility_id = ' . $this->Session->read('Auth.facility_id_selected');
        $sql .= '     and c.item_type = ' . Configure::read('Items.item_types.normalitem');
        $sql .= '   group by ';
        $sql .= '     c.internal_code ';
        $sql .= '     , b.per_unit ';
        $sql .= '   having  ';
        $sql .= '      b.per_unit <  ';
        $sql .= '      sum(  ';
        $sql .= '       (  ';
        $sql .= '         case  ';
        $sql .= '           when f.stock_count - f.reserve_count > 0  ';
        $sql .= '           then f.stock_count - f.reserve_count  ';
        $sql .= '           else 0  ';
        $sql .= '           end ';
        $sql .= '       ) * d.per_unit ';
        $sql .= '     ) ';
        
        $ret = $this->TrnPromise->query($sql);
        if(count($ret)){
            return true;
        }
        return false;
    }


}

