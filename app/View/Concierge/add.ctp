<script>
$(function($){
    // 送信ボタン押下
    $("#btn_Submit").click(function(){
        $("#input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result/').submit();
    });
});
</script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/concierge.css" />
<div class="wrapper">
    <div id="content-wrapper">
        <div id="TopicPath">
            <ul>
                <li><a href="<?php echo $this->webroot; ?>login/home">ＴＯＰ</a></li>
                <li class="pankuzu">マスタコンシェルジュデスク</li>
                <li>新規相談</li>
            </ul>
        </div>
        <nav class="concierge-tab">
            <ul>
                <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">相談内容一覧</a></li>
                <li><a class="current">新規相談</a></li>
            </ul>
        </nav>
        <h2 class="HeaddingLarge"><span>新規相談</span></h2>
        <h2 class="HeaddingMid">マスタコンシェルジュへの新規相談窓口です。診療材料の物品管理のことから、細かなお悩みなど無料でご相談いただけます。<a href="http://cloud.coslos.jp/master-concierge" target="_blank">【マスタコンシェルジュとは？】</a></h2>
        <form id="input_form" action="<?php echo $this->webroot; ?>Concierge" class="input_form validate_form" method="post">
            <div class="textarea_box">
                <h3>タイトル</h3>
                <?php echo $this->form->input('Ticket.title', array('type'=>'text','class'=>'concierge_text_3 validate[required]')); ?>
                <h3>相談内容</h3>
                <?php echo $this->form->input('Ticket.inquiry', array('type'=>'textarea','class'=>'concierge_textarea validate[required]')); ?>
                <input type="button" class="common-button-2 ml-5" id="btn_Submit" value="送信"/>
            </div>
        </form>
    </div><!--#content-wrapper-->
</div>