<script>
$(function($){
    // 送信ボタン押下
    $("#btn_Submit").click(function(){
        $("#input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result/<?php echo $result[0][0]['id'];?>').submit();
    });
    // 完了ボタン押下
    $("#btn_Finish").click(function(){
        validateDetachSubmit($('#input_form') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ticketFinish/<?php echo $result[0][0]['id'];?>' );
    });
});
</script>

<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/concierge.css" />

<div id="content-wrapper">
    <div id="TopicPath">
        <ul>
            <li><a href="<?php echo $this->webroot?>login/home">ＴＯＰ</a></li>
            <li><a href="<?php echo $this->webroot?>Concierge/index">マスタコンシェルジュデスク</a></li>
            <li><a href="<?php echo $this->webroot?>Concierge/index">相談内容一覧</a></li>
            <li>相談内容詳細</li>
        </ul>
    </div>

    <nav class="concierge-tab">
        <ul>
            <li><a class="current"  href="<?php echo $this->webroot?>Concierge/index">相談内容一覧</a></li>
            <li><a href="<?php echo $this->webroot; ?>Concierge/add">新規相談</a></li>
        </ul>
    </nav>

    <h2 class="HeaddingLarge"><span>相談内容詳細</span></h2>
    <h2 class="HeaddingMid">相談内容の詳細ページです。マスタコンシェルジュへの返答、ステータス変更が行えます。</h2>

    <div class="conth3">
        <table class="FormStyleTable">
            <tr>
                <th>相談番号</th>
                <td><?php echo h_out($result[0][0]['id']);?></td>
                <th>相談日</th>
                <td><?php echo h_out($result[0][0]['created'] , 'center')?></td>
                <th>ステータス</th>
                <td><?php echo h_out($result[0][0]['status_name'] , 'center status'.$result[0][0]['status']); ?></td>
            </tr>
            <tr>
                <th>タイトル</th>
                <td coalspan="5" ><?php echo h_out($result[0][0]['title']); ?></td>
            </tr>
        </table>
    </div>
    <div class="results">
        <div class="TableScroll">
            <table class="conciergetable table-even">
                <colgroup>
                    <col />
                    <col />
                    <col />
                </colgroup>
                <thead>
                <tr>
                    <th>送受信日</th>
                    <th>送信者</th>
                    <th width="80%">内容</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($result as $r){ ?>
                <tr>
                    <td><?php echo h_out($r[0]['send_date'],'center'); ?></td>
                    <td>
                        <?php if($r[0]['addusertype'] == 0 ){ // 利用者 ?>
                        <?php echo h_out($r[0]['user_name']. '様')  ?>
                        <?php } else { // マスタコンシェルジュ  ?>
                        <?php echo h_out('マスタコンシェルジュ');  ?>
                        <?php } ?>
                    </td>
                    <td><pre><?php echo $r[0]['inquiry'];?></pre></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <form id="input_form" class="validate_form" method="post">
    <div class="textarea_box">
        <?php echo $this->form->input('Ticket.inquiry', array('type'=>'textarea','class'=>'concierge_textarea validate[required]','placeholder'=>'マスタコンシェルジュへの返答内容を入力してください。')); ?>
        <input type="button" class="common-button-2 m-0" id="btn_Submit" value="送信"/>
        <?php if($result[0][0]['status'] != '完了'){ ?>
        <input type="button" class="common-button-2 ml-5" id="btn_Finish" value="ステータスを完了にする"/>
        <?php } ?>
        <a href="<?php echo $this->webroot?><?php echo $this->name; ?>" class="text-link ml-5">相談一覧へ戻る>></a>
    </div>
    </form>
</div><!--#content-wrapper-->
