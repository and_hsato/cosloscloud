<script>
$(function($){
    // 検索ボタン押下
    $("#btn_Search").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/index/#result').submit();
    });
    // 新規ボタン押下
    $("#btn_New").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add/').submit();
    });
    // 返信アイコンクリック
    $(".res_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/detail/'+$(this).parent(':eq(0)').parent(':eq(0)').parent(':eq(0)').attr('id')).submit();
    });

  /*
    // マウスオーバーで色変え
    $(".TableStyle-desk tbody tr").hover(
        function(){
            $(this).addClass('tr-hover');
        },
        function(){
            $(this).removeClass('tr-hover');
        }
    );
  */

    // 初期で子要素を非表示
    $(window).load(function () {
        $('.child').hide();
    });
  
    // 行クリック
    $(".TableStyle-desk tr.ticket_header").click(function(event){
        tag = $(event.target).attr("tagName");
		 if (tag === "IMG") {
        }
       else{
            $('.child' + $(this).attr('id')).toggle(0);
            $(this).find('.ticket').eq(0).toggleClass('d-icon');
            $(this).find('.ticket').eq(0).toggleClass('u-icon');
        }
    });
});
</script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/concierge.css" />
<div class="wrapper">
    <div id="content-wrapper">
        <div id="TopicPath">
            <ul>
                <li><a href="<?php echo $this->webroot?>login/home">ＴＯＰ</a></li>
                <li><a href="<?php echo $this->webroot?>Concierge/index">マスタコンシェルジュデスク</a></li>
                <li>相談内容一覧</li>
            </ul>
        </div>
        <nav class="concierge-tab">
        <ul>
            <li><a class="current">相談内容一覧</a></li>
            <li><a href="<?php echo $this->webroot?>Concierge/add">新規相談</a></li>
        </ul>
        </nav>
        <h2 class="HeaddingLarge"><span>相談内容一覧</span></h2>
        <h2 class="HeaddingMid">マスタコンシェルジュへの過去の相談内容を検索できます。</h2>
        <h3 class="conth3">相談内容を検索</h3>

        <form id="search_form" class="validate_from search_form" method="post">
            <?php echo $this->form->input('Ticket.is_search' , array('type'=>'hidden'));?>
            <div class="SearchBox">
                <table class="FormStyleTable">
                    <tr class="">
                        <th colspan="2">
                            <?php echo $this->form->input('Ticket.keyword', array('class'=>'txt','style'=>'width: 400px;',)); ?>
                        </th> 
                        <td><input type="button" class="common-button-2 ml-5" id="btn_Search" value="検索"/></td>
                        <td><input type="button" class="common-button-2 ml-5" id="btn_New" value="新規相談"/></td>
                    </tr>
                    <tr>
                        <th>ステータス</th>
                        <td>
                            <?php echo $this->form->radio('Ticket.status' , array('' => 'すべて') , array('hiddenField'=>false , 'label'=>false , 'class'=>'' , 'checked'=>true)); ?>
                            <?php foreach( Configure::read('Concierge.status') as $k => $v ){ ?>
                            <?php echo $this->form->radio('Ticket.status' , array($k => $v) , array('hiddenField'=>false , 'label'=>false , 'class'=>'')); ?>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <th>相談日</th>
                        <td>
                            <?php echo $this->form->input('Ticket.work_date_from', array('id' => 'datepicker1', 'class' => 'txt validate[optional,custom[date]] date')); ?>
                            &nbsp;&nbsp;～&nbsp;&nbsp;
                            <?php echo $this->form->input('Ticket.work_date_to', array('id' => 'datepicker2', 'class' => 'txt validate[optional,custom[date]] date')); ?>
                        <td>
                    </tr>
                </table>
            </div>
            <a name="result"></a>
            <div class="results">
                <div class="TableScroll2">
                    <table class="TableStyle-desk table-even">
                        <colgroup>
                            <col />
                            <col width="150px" />
                            <col width="120px" />
                            <col width="100px" />
                        </colgroup>
                        <thead>
                            <tr>
                                <th>タイトル</th>
                                <th>更新日</th>
                                <th>相談番号</th>
                                <th>ステータス</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($result as $r){?>
                            <tr id="<?php echo $r[0]['ticketid']; ?>" class="ticket_header">
                                <td class="tiket-title">
                                    <p class="ticket d-icon" style="float:left;"><?php echo $r[0]['title']; ?></p>
                                    <p class="res-wrap"><img src="<?php echo $this->webroot; ?>img/r-icon.png" class="res_btn" />返答する</p>
                                </td>
                                <td><?php echo h_out($r[0]['modified'],'center'); ?></td>
                                <td><?php echo h_out($r[0]['ticketid'],'center'); ?></td>
                                <td><?php echo h_out($r[0]['status_name'],'center status'.$r[0]['status']); ?></td>
                            </tr>
                            <?php foreach($r['childtickets'] as $c){ ?>
                            <tr class='child<?php echo $r[0]['ticketid']; ?> child child_header'>
                                <td>
                                    <?php if($c[0]['addusertype'] == 0 ){ // 利用者 ?>
                                    <?php echo h_out($r[0]['user_name']. '様' , 'user-icon')  ?>
                                    <?php } else { // マスタコンシェルジュ  ?>
                                    <?php echo h_out('マスタコンシェルジュ' , 'master-icon');  ?>
                                    <?php } ?>
                                </td>
                                <td><?php echo h_out($c[0]['modified'],'center'); ?></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class='child<?php echo $r[0]['ticketid']; ?> child'>
                                <td>
                                    <pre><?php echo $c[0]['inquiry']; ?></pre>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php } ?>
                            <?php } ?>
                            <?php if(count($result) == 0 && isset($this->request->data['Ticket']['is_search'])){?>
                            <tr><td colspan="4" class="center">該当するデータがありませんでした</td></tr>
                            <?php } ?>
                        </tbody>
                    </table>
               </div>
           </div>
       </form>
   </div><!--#content-wrapper-->
</div>