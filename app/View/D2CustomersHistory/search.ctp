<script type="text/javascript">
    $(document).ready(function(){
        //検索ボタン押下
        $('#search').click(function(){
            var errorMsg1 = dateChecker($("#DirectWorkDateFrom").val());
            var errorMsg2 = dateChecker($("#DirectWorkDateTo").val());

            if(errorMsg1 == "" && errorMsg2 == ""){
              $('#form_edit').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').submit();
            } else {
              alert('入力された日付が不正です');
            }
        });

        //CSVボタン押下
        $('#btn_Csv').click(function(){
            var errorMsg1 = dateChecker($("#DirectWorkDateFrom").val());
            var errorMsg2 = dateChecker($("#DirectWorkDateTo").val());

            if(errorMsg1 == "" && errorMsg2 == ""){
              $('#form_edit').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
              $('#form_edit').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
            } else {
              alert('入力された日付が不正です');
            }
        });

        //チェックボックス制御
        $('.checkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
        });

        //編集ボタン押下
        $('#btn_edit').click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length === 0){
                alert('変更を行う明細を選択してください');
                return false;
            }else{
                $('#form_edit').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm').submit();
            }
        });


        $('#printShipping').click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length === 0){
                alert('納品書を発行する明細を選択してください');
            }else{
                $('input[type=checkbox].checkAllTarget').each(function(i){
                    if(this.checked){
                        var v = $('input[type=hidden].invoice:eq(' + i + ')').val();
                        $('<input type="hidden" name="data[TrnShippingHeader][id][]" />').val(v).appendTo('#form_edit');
                    }
                });
                $('#form_edit').attr('action', '<?php echo $this->webroot ?>d2customers/report').submit();
            }
        });
        $('#printOrder').click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length === 0){
                alert('発注書を印刷する明細を選択してください');
            }else{
                var orderId = [];
                $('input[type=checkbox].checkAllTarget').each(function(i){
                    if(this.checked){
                        orderId.push($('input[type=hidden].order:eq(' + i + ')').val());
                    }
                });
                $('#printOrderId').val(orderId.join(','));
                $('#form_edit').attr('action', '<?php echo $this->webroot ?>orders/report/order').submit();
            }
        });
        $('#customer_select').change(function(){
            var params = {};
            params['field'] = '';
            params['value'] = $(this).val();
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->webroot; ?>d2customers_history/get_departments/",
                data: params,
                success: function(data, dataType){
                    $('#department_select').children().remove();
                    var obj = $.parseJSON(data);
                    $.each(obj, function(idx, o){
                        if(idx === 0 && obj.length > 1){
                            $('<option value=""></option>').appendTo('#department_select');
                        }
                        if(o.department_code == $('#department_input').val()){
                            $('<option></option>').text(o.department_name).attr('value', o.department_code).attr('selected','selected').appendTo('#department_select');
                        }else{
                            $('<option></option>').text(o.department_name).attr('value', o.department_code).appendTo('#department_select');
                        }

                    });
                    $('#customer_input').val($('#customer_select').val());
                },
                error: function(){
                    alert('部署データの取得に失敗しました');
                }
            });
            return false;
        });

        $('#department_select').change(function(){
            $('#department_input').val($(this).val());
        });
        $('#department_input').blur(function(){
            if($('#department_select').find('[value=' + $(this).val() + ']').length === 1){
                $('#department_select').val($(this).val());
            }else{
                $('#department_select').val('');
            }
        });

        $('#customer_input').val($('#customer_select').val());
        $('#customer_select').change();
    });

//日付形式チェック
function dateChecker(date){
    var msg = "";
    //YYYY/MM/DD形式かどうか
    if(date.length != 0){
      if(!date.match(/^\d{4}\/\d{2}\/\d{2}$/)){
          msg = "日付はYYYY/MM/DD形式で入力してください";
          return msg;
      }
    } else {return msg;}

    //入力された日付の妥当性チェック
    var tempYear = date.substr(0, 4) - 0;
    var tempMonth = date.substr(5, 2) - 1;
    var tempDay = date.substr(8, 2) - 0;

    if(tempYear >= 1970 && tempMonth >= 0 && tempMonth <= 11 && tempDay >= 1 && tempDay <= 31){
        var checkDate = new Date(tempYear, tempMonth, tempDay);
        if(isNaN(checkDate)){
            msg = "入力された日付が不正です";
        }else if(checkDate.getFullYear() == tempYear && checkDate.getMonth() == tempMonth && checkDate.getDate() == tempDay){
            //
        }else{
            msg = "入力された日付が不正です";
        }
    }else{
        msg = "入力された日付が不正です";
    }
    return msg;
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>直納品受領履歴</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>直納品受領履歴</span></h2>
<h2 class="HeaddingMid">変更を行いたい直納品受領履歴を検索してください</h2>

<form class="validate_form search_form" id="form_edit" action="<?php echo $this->webroot; ?>d2customers_history/search" method="post">
    <input type="hidden" name="data[isSearch]" value="1" />
    <input type="hidden" name="data[TrnOrder][selected_facility_id]" value="<?php echo $this->Session->read('Auth.facility_id_selected'); ?>" />
    <input type="hidden" id="printOrderId" name="data[TrnOrderHeader][id]" value="" />
    <input type="hidden" name="data[facility_from_id]" value="<?php echo $this->Session->read('Auth.facility_id_selected'); ?>" />
    <input type="hidden" name="data[User][user_id]" value="<?php echo $this->Session->read('Auth.MstUser.id'); ?>" />
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="60" align="right"/>
            </colgroup>
            <tr>
                <th>直納番号</th>
                <td><?php echo($this->form->input('Direct.work_no', array('class'=>'txt', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>直納日</th>
                <td>
                    <?php echo($this->form->input('Direct.work_date_from', array('class'=>'date', 'maxlength'=>'10', 'label'=>''))); ?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo($this->form->input('Direct.work_date_to', array('class'=>'date', 'maxlength'=>'10', 'label'=>''))); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo($this->form->input('Direct.customer_input', array('class'=>'txt', 'id'=>'customer_input', 'maxlength'=>'10', 'label'=>'', 'style'=>'width:60px;'))); ?>
                    <?php echo($this->form->input('Direct.customer_id', array('options'=>$customers,'id'=>'customer_select','class'=>'txt','empty' => (count($customers) > 1),))); ?>
                </td>
                <td></td>
                <th>得意先部署</th>
                <td>
                    <?php echo($this->form->input('Direct.department_input', array('class'=>'txt', 'id'=>'department_input', 'maxlength'=>'10', 'label'=>'', 'style'=>'width:60px;'))); ?>
                    <select id="department_select" name="data[department_select]" class="txt" />
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo($this->form->input('Direct.internal_code', array('class'=>'txt search_internal_code', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo($this->form->input('Direct.item_code', array('class'=>'txt search_upper', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo($this->form->input('Direct.lot_no', array('class'=>'txt', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo($this->form->input('Direct.item_name', array('class'=>'txt search_canna', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo($this->form->input('Direct.dealer_name', array('class'=>'txt search_canna', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('Direct.work_type', array('options'=>$classes, 'empty'=>true,'class'=>'txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo($this->form->input('Direct.standard', array('class'=>'txt search_canna', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo($this->form->input('Direct.sticker_no', array('class'=>'txt search_canna', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <td colspan="2"><?php echo $this->form->checkbox('Direct.showDeleted',array('label'=>false)); ?>取消も表示する</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn1" id="search" />
            <input type="button" id="btn_Csv" class="btn btn5"/>
        </p>
    </div>
    <hr class="Clear" />
    <div id="hideer">
        <h2 class="HeaddingSmall">変更を行いたい受領履歴を選択、クリックしてください。</h2>
        <?php echo $this->element('limit_combobox',array('result'=>count($searchResult))); ?>
        <div class="TableScroll">
            <table class="TableStyle01" style="margin:0;padding:0;">
                <tr>
                    <th style="width:20px;"><input type="checkbox" class="checkAll"/></th>
                    <th width="135">直納番号</th>
                    <th width="95">直納日</th>
                    <th class="col15">仕入先</th>
                    <th class="col15">施設／部署</th>
                    <th class="col15">登録者</th>
                    <th class="col15">登録日時</th>
                    <th class="col10">備考</th>
                    <th class="col10">件数</th>
                </tr>
                <?php if(!empty($searchResult)){ ?>
                <?php $i=0;foreach($searchResult as $row): ?>
                <tr class="<?php echo ($i%2===0?'':'odd'); ?>">
                    <td align="center" style="width:20px;">
                        <?php echo $this->form->checkbox("TrnReceivingHeader.Rows.{$row[0]['id']}", array('class'=>'center checkAllTarget')); ?>
                        <input type="hidden" class="invoice" value="<?php echo $row[0]['trn_shipping_header_id']; ?>" />
                        <input type="hidden" class="order" value="<?php echo $row[0]['trn_order_header_id']; ?>" />
                    </td>
                    <td width="135"><?php echo h_out($row[0]['work_no'],'center'); ?></td>
                    <td width="95"><?php echo h_out($row[0]['work_date'],'center'); ?></td>
                    <td class="col15"><?php echo h_out($row[0]['supplier_name']); ?></td>
                    <td class="col15"><?php echo h_out($row[0]['customer_name'].' / '.$row[0]['department_name']); ?></td>
                    <td class="col15"><?php echo h_out($row[0]['user_name']); ?></td>
                    <td class="col15"><?php echo h_out($row[0]['created'],'center'); ?></td>
                    <td class="col10"><?php echo h_out($row[0]['recital']); ?></td>
                    <td class="col10"><?php echo h_out($row[0]['total_count'].'/'.$row[0]['detail_count'],'right'); ?></td>
                </tr>
                <?php $i++;endforeach; ?>
            </table>
        </div>
        <?php if(count($searchResult) > 0){ ?>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn7" id="btn_edit" />
                <input type="button" class="btn btn21" id="printOrder" />
                <input type="button" class="btn btn19" id="printShipping" />
            </p>
        </div>
        <?php } } else { ?>
        <?php if((isset($this->request->data['isSearch'])) && ($this->request->data['isSearch'] == '1')){ ?>
        <div class="TableScroll">
            <table class="TableStyle01"  style="margin:0;padding:0;">
                <tr>
                    <td colspan="9" class="center">該当するデータがありませんでした</td>
                </tr>
            </table>
        </div>
        <?php } ?>
        <?php } ?>
    </div>
</form>
