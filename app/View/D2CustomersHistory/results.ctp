<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">直納品受領履歴</a></li>
        <li class="pankuzu">直納品受領履歴編集</li>
        <li>直納品受領履歴編集結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>直納品受領履歴編集結果</span></h2>
<div class="Mes01">以下の受領の取消を行いました。</div>
<div class="TableScroll2">
    <table class="TableStyle01 table-odd2">
        <colgroup>
            <col width="135"/>
            <col width="95"/>
            <col />
            <col />
            <col width="80"/>
            <col width="80"/>
            <col width="100"/>
            <col width="150"/>
            <col width="70"/>
            <col width="70"/>
        </colgroup>
        <tr>
            <th>消費番号</th>
            <th>消費日</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th>包装単位</th>
            <th>仕入価格</th>
            <th>ロット番号</th>
            <th rowspan="2">部署シール</th>
            <th>作業区分</th>
            <th>更新者</th>
        </tr>
        <tr>
            <th colspan="2">施設　／　部署</th>
            <th>規格</th>
            <th>販売元</th>
            <th>商品ID</th>
            <th>売上価格</th>
            <th>有効期限</th>
            <th colspan="2">備考</th>
        </tr>
        <?php $i=0;foreach($results as $key => $val): ?>
        <tr>
            <td><?php echo h_out($val['work_no'],'center'); ?></td>
            <td><?php echo h_out($val['work_date'],'center'); ?></td>
            <td><?php echo h_out($val['item_name']); ?></td>
            <td><?php echo h_out($val['item_code']); ?></td>
            <td><?php echo h_out($val['packing_name']); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($val['transaction_price']),'right'); ?></td>
            <td><?php echo h_out($val['lot_no']); ?></td>
            <td align="center" rowspan="2"><?php echo join('<br>', $val['sticker_no']); ?></td>
            <td><?php echo h_out($val['work_type_name']); ?></td>
            <td><?php echo h_out($val['user_name']); ?></td>
        </tr>
        <tr>
            <td colspan="2"><?php echo h_out($val['customer_name'] . ' / ' . $val['department_name']); ?></td>
            <td><?php echo h_out($val['standard']); ?></td>
            <td><?php echo h_out($val['dealer_name']); ?></td>
            <td><?php echo h_out($val['internal_code'],'center'); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($val['sales_price']),'right'); ?></td>
            <td><?php echo h_out($val['validated_date'],'center'); ?></td>
            <td colspan="2"><?php echo h_out($val['recital']); ?></td>
        </tr>
        <?php $i++;endforeach; ?>
    </table>
</div>
