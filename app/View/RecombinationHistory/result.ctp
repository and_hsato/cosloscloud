<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>recombination_history">組替履歴</a></li>
        <li>組替履歴編集</li>
        <li>組替履歴取消</li>
    </ul>
</div>

    <h2 class="HeaddingLarge"><span>組替履歴取消</span></h2>
    <div class="Mes01">組替登録を取り消しました</div>
    <div id="results">
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
                <tr>
                    <th style="width:20px;"></th>
                    <th width="145">組替番号<br/>組替日</th>
                    <th width="145">新シール</th>
                    <th class="col10">包装単位</th>
                    <th class="col10">作業区分</th>
                    <th class="col10">備考</th>
                    <th style="width:2px;"></th>
                    <th width="145">旧シール</th>
                    <th class="col10">包装単位</th>
                    <th class="col10">ロット番号<BR>有効期限</th>
                    <th class="col10">更新者</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01">
                <?php $i=0;foreach($Res['new'] as $_key => $_val): ?>
                    <?php $j=0;foreach($_val as $_row): ?>
                    <tr class="<?php echo($i%2===0?'':'odd'); ?> center">
                        <td style="width:20px;"></td>
                        <?php if($j === 0): ?>
                        <td rowspan="<?php echo count($_val); ?>" width="145"><label title="<?php echo $_row['work_no']."&#13;".date('Y/m/d', strtotime($_row['created'])); ?>"><?php echo $_row['work_no']."<br>".date('Y/m/d', strtotime($_row['created'])); ?></label></td>
                        <?php endif; ?>
                        <td align="center" width="145"><label title="<?php echo $_row['facility_sticker_no']; ?>"><p align="center"><?php echo $_row['facility_sticker_no']; ?></p></label></td>
                        <td class="col10"><label title="<?php echo $_row['packing_name']; ?>"><?php echo $_row['packing_name']; ?></label></td>
                        <td class="col10"><label title="<?php echo $_row['name']; ?>"><?php echo $_row['name']; ?></label></td>
                        <td class="col10"><label title="<?php echo $_row['recital']; ?>"><?php echo $_row['recital']; ?></label></td>
                        <?php if($j === 0): ?>
                        <td width="145" align="center" rowspan="<?php echo count($_val); ?>"><label title="<?php echo join('&#13;',$Res['old'][$_key]['sticker_no']); ?>"><p align="center"><?php echo join('<br />',$Res['old'][$_key]['sticker_no']); ?></p></label></td>
                        <td rowspan="<?php echo count($_val); ?>" class="col10"><label title="<?php echo join('&#13;',$Res['old'][$_key]['packing_name']); ?>"><?php echo join('<br />',$Res['old'][$_key]['packing_name']); ?></label></td>
                        <td rowspan="<?php echo count($_val); ?>" class="col10"><label title="<?php echo $_row['lot_no']."&#13;". ($_row['validated_date'] == '' ? '' : date('Y/m/d', strtotime($_row['validated_date']))); ?>"><?php echo $_row['lot_no']."<br>". ($_row['validated_date'] == '' ? '' : date('Y/m/d', strtotime($_row['validated_date']))); ?></label></td>
                        <td rowspan="<?php echo count($_val); ?>" class="col10"><label title="<?php echo $_row['user_name']; ?>"><?php echo $_row['user_name']; ?></label></td>
                        <?php endif; ?>
                    </tr>
                    <?php $j++;endforeach; ?>
                <?php $i++;endforeach; ?>
            </table>
        </div>
    </div>
</form>
