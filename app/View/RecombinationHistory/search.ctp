<script type="text/javascript">
$(document).ready(function(){
    //検索ボタン押下
    $('#btn_search').click(function(){
        $('#form_search').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').submit();
    });
    //全件チェック
    $('.checkAll').click(function(){
        $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
    });
    //明細表示ボタン押下
    $('#btn_detail').click(function(){
        if($('input[type=checkbox].checkAllTarget:checked').length == 0 ) { 
            alert('変更を行う入庫履歴を選択してください');
            return false;
        }else{
            $('#form_search').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit').submit();
        }
    });
})
</script>
  <div id="TopicPath">
    <ul>
      <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
      <li>組替履歴</li>
    </ul>
  </div>
  <h2 class="HeaddingLarge"><span>組替履歴（シール再発行）</span></h2>
  <!--<h2 class="HeaddingMid"><div class="Mes01">組替の場合、取り消しは行えません。組替の取り消しを行う場合は、再度、組替を行ってください。</div></h2>-->
    <form id="form_search" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search" method="post" class="validate_form search_form">
    <input type="hidden" name="data[IsSearch]" value="1" />
    <div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
            <col width="60" align="right" />
        </colgroup>
    <tr>
        <th>組替番号</th>
        <td><?php echo($this->form->text('TrnMoveHeader.work_no', array('class'=>'txt', 'maxlength'=>'50', 'label'=>''))); ?></td>
        <td></td>
        <th>組替日</th>
            <td>
                <?php echo($this->form->text('TrnMoveHeader.work_date_from', array('class' => 'date txt validate[optional,custom[date]]', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                <span>&nbsp;～&nbsp;</span>
                <?php echo($this->form->text('TrnMoveHeader.work_date_to', array('class' => 'date txt validate[optional,custom[date]]', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
            </td>
        <td></td>
    </tr>
     <tr>
        <th>商品ID</th>
        <td><?php echo($this->form->text('MstFacilityItem.internal_code', array('class' => 'txt search_internal_code', 'maxlength' => '50', 'label' => ''))); ?></td>
        <td></td>
        <th>製品番号</th>
        <td><?php echo($this->form->text('MstFacilityItem.item_code', array('class' => 'txt search_upper', 'maxlength' => '50', 'label' => ''))); ?></td>
        <td></td>
        <th>ロット番号</th>
        <td><?php echo($this->form->text('TrnSticker.lot_no', array('class' => 'txt', 'maxlength' => '50', 'label' => ''))); ?></td>
        <td></td>
    </tr>
    <tr>
        <th>商品名</th>
        <td><?php echo($this->form->text('MstFacilityItem.item_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
        <td></td>
        <th>販売元</th>
        <td><?php echo($this->form->text('MstDealer.dealer_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
        <td></td>
        <th>作業区分</th>
        <td><?php echo $this->form->input('TrnShipping.work_type', array('options'=>$classes, 'empty'=>true, 'class' => 'txt', 'id' => 'classTypeSel')); ?></td>
        <td></td>
    </tr>
    <tr>
        <th>規格</th>
        <td><?php echo($this->form->text('MstFacilityItem.standard', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
        <td></td>
        <th>シール番号</th>
        <td><?php echo($this->form->text('TrnSticker.sticker_no', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="9" style="height:10px;"></td>
    </tr>
    </table>
    </div>

    <div class="ButtonBox">
        <p class="center">
            <input type="button" value="" class="btn btn1" id="btn_search" />

        </p>
    </div>
            <div id="results">
                <h2 class="HeaddingSmall">明細を表示したい組替を選択してください。</h2>
                <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
                <div class="TableScroll">
                    <table class="TableStyle01">
                        <tr>
                            <th style="width:20px;"><input type="checkbox" class="checkAll"/></th>
                            <th style="width:135px;">組替番号</th>
                            <th>作業者</th>
                            <th style="width:150px;">組替日時</th>
                            <th style="width:80px;">商品ID</th>
                            <th>商品名</th>
                            <th>規格</th>
                            <th style="width:100px;">製品番号</th>
                            <th style="width:100px;">販売元</th>
                        </tr>

<?php
                    if(count($SearchResult) ===0 && isset($data["IsSearch"]) && $data["IsSearch"] === "1"){
?>
                        <tr><td colspan="9" class="center"><span>該当データが存在しません。</span></td></tr>
<?php
                    }
                        $i = 0;
                        foreach($SearchResult as $_row):?>
                        <tr class="<?php echo($i % 2 == 0 ? '' : 'odd'); ?>">
                            <td class="center"><?php echo $this->form->checkbox("TrnMoveHeader.Rows.{$_row[0]['id']}.selected", array('class' => 'center checkAllTarget',)); ?></td>
                            <td align="center"><label title="<?php echo($_row[0]['work_no']); ?>"><p align="center"><?php echo($_row[0]['work_no']); ?></p></label></td>
                            <td><label title="<?php echo($_row[0]['user_name']); ?>"><?php echo($_row[0]['user_name']); ?></label></td>
                            <td align="center"><label title="<?php echo(date('Y/m/d H:i:s', strtotime($_row[0]['created']))); ?>"><p align="center"><?php echo(date('Y/m/d H:i:s', strtotime($_row[0]['created']))); ?></p></label></td>
                            <td><label title="<?php echo($_row[0]['internal_code']); ?>"><p align="center"><?php echo($_row[0]['internal_code']); ?></p></label></td>
                            <td><label title="<?php echo($_row[0]['item_name']); ?>"><?php echo($_row[0]['item_name']); ?></label></td>
                            <td><label title="<?php echo($_row[0]['standard']); ?>"><?php echo($_row[0]['standard']); ?></label></td>
                            <td><label title="<?php echo($_row[0]['item_code']); ?>"><?php echo($_row[0]['item_code']); ?></label></td>
                            <td><label title="<?php echo($_row[0]['dealer_name']); ?>"><?php echo($_row[0]['dealer_name']); ?></label></td>
                        </tr>
                        <?php $i++;
                        endforeach;
                   ?>
                   </table>
                </div>
                <?php if(count($SearchResult) > 0): ?>
                <div class="ButtonBox">
                    <p class="center">
                        <input type="button" value="" class="btn btn7" id="btn_detail" />
                    </p>
                </div>
                <?php endif; ?>
    </div>
</form>
