<script type="text/javascript">
$(document).ready(function(){
    $('.checkAll').click(function(){
        $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
    });
    $('#printSeal').click(function(){
        var cnt = 0;
        $('input[type=checkbox].checkAllTarget').each(function(i){
            if(this.checked){
                cnt++;
            }
        });
        if(cnt === 0){
            alert('印刷対象を選択してください');
            return false;
        }else{
            $('#recombination_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/dummy').submit();
        }
    });

    $('#cancel').click(function(){
        var cnt = 0;
        $('input[type=checkbox].checkAllTarget').each(function(i){
            if(this.checked){
                cnt++;
            }
        });
        if(cnt === 0){
            alert('取消対象を選択してください');
            return false;
        }else{
            if(window.confirm("取消を実行します。よろしいですか？")){
                $('#recombination_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result/').submit();
            }
        }
    });

  
})
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>recombination_history">組替履歴</a></li>
        <li>組替履歴編集</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>組替履歴編集</span></h2>
<form id="recombination_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/dummy" method="POST">
    <input type="hidden" name="data[facility_name]" value="<?php echo $facility_name; ?>" />
    <?php echo $this->form->input('MstFacility.id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected'))); ?>
    <?php echo $this->form->input('Recombin.token' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div id="results">
        <div class="TableScroll">
            <table class="TableStyle01">
                <tr>
                    <th style="width:20px;"><input type="checkbox" checked class="checkAll"/></th>
                    <th width="145">組替番号<br/>組替日</th>
                    <th width="145">新シール</th>
                    <th class="col10">包装単位</th>
                    <th class="col10">作業区分</th>
                    <th class="col10">備考</th>
                    <th width="145">旧シール</th>
                    <th class="col10">包装単位</th>
                    <th class="col10">ロット番号<BR>有効期限</th>
                    <th class="col10">更新者</th>
                </tr>
                <?php $i=0;foreach($Res['new'] as $_key => $_val): ?>
                    <?php $j=0;foreach($_val as $_row): ?>
                    <tr class="<?php echo($i%2===0?'':'odd'); ?> center">
                        <td><?php echo $this->form->checkbox("TrnSticker.Rows.{$_row['facility_sticker_no']}.selected", array('class' => 'center checkAllTarget', 'checked' => 'checked')); ?></td>
                        <?php if($j === 0): ?>
                        <td rowspan="<?php echo count($_val); ?>" width="145"><label title="<?php echo $_row['work_no']."&#13;".date('Y/m/d', strtotime($_row['created'])); ?>"><?php echo $_row['work_no']."<br>".date('Y/m/d', strtotime($_row['created'])); ?></label></td>
                        <?php endif; ?>
                        <td><?php echo h_out($_row['facility_sticker_no'], 'center'); ?></td>
                        <td><?php echo h_out($_row['packing_name']); ?></td>
                        <td><?php echo h_out($_row['name']); ?></td>
                        <td><?php echo h_out($_row['recital']); ?></td>
                        <?php if($j === 0): ?>
                        <td align="center" rowspan="<?php echo count($_val); ?>"><label title="<?php echo join('&#13;',$Res['old'][$_key]['sticker_no']); ?>"><p align="center"><?php echo join('<br />',$Res['old'][$_key]['sticker_no']); ?></p></label></td>
                        <td rowspan="<?php echo count($_val); ?>"><label title="<?php echo join('&#13;',$Res['old'][$_key]['packing_name']); ?>"><?php echo join('<br />',$Res['old'][$_key]['packing_name']); ?></label></td>
                        <td rowspan="<?php echo count($_val); ?>"><label title="<?php echo $_row['lot_no']."&#13;". ($_row['validated_date'] == '' ? '' : date('Y/m/d', strtotime($_row['validated_date']))); ?>"><?php echo $_row['lot_no']."<br>". ($_row['validated_date'] == '' ? '' : date('Y/m/d', strtotime($_row['validated_date']))); ?></label></td>
                        <td rowspan="<?php echo count($_val); ?>"><label title="<?php echo $_row['user_name']; ?>"><?php echo $_row['user_name']; ?></label></td>
                        <?php endif; ?>
                    </tr>
                    <?php $j++;endforeach; ?>
                <?php $i++;endforeach; ?>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="btn btn6 [p2]" id="cancel" />
            <input type="button" class="btn btn18" id="printSeal" />
        </div>
    </div>
</form>
