<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/receive_from_outside">管理外移入</a></li>
        <li class="pankuzu">管理外移入確認</li>
        <li>管理外移入結果</li>
   </ul>
</div>
<h2 class="HeaddingLarge"><span>管理外移入結果</span></h2>
<h2 class="HeaddingMid">結果を確認してください</h2>
<div class="Mes01">以下の商品を移動しました</div>
<form>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>移動番号</th>
                <td><?php echo $this->form->input('TrnMoveHeader.work_no',array('type'=>'text','class'=>'lbl','readonly'=>'readonly','readonly'=>'readonly')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>移動日</th>
                <td><?php echo $this->form->input('TrnMoveHeader.work_date',array('type'=>'text','class'=>'lbl','readonly'=>'readonly')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnMoveHeader.work_class_txt',array('type'=>'hidden','id'=>'work_class_txt')); ?>
                    <?php echo $this->form->input('TrnMoveHeader.work_class',array('options'=>$work_classes,'empty'=>true,'class'=>'txt','id'=>'work_class')); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo Configure::read('facility_subcode'); ?></th>
                <td><?php echo $this->form->input('TrnMoveHeader.subcode',array('class'=>'lbl','readonly'=>'readonly')); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnMoveHeader.recital',array('maxlength'=>50,'class'=>'lbl','readonly'=>'readonly')); ?></td>
            </tr>
        </table>
    </div>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle01">
            <tr>
                <th class="col10">商品ID</th>
                <th class="col15">商品名</th>
                <th class="col15">規格</th>
                <th class="col15">製品番号</th>
                <th class="col15">販売元</th>
                <th class="col10">包装単位</th>
                <th class="col10">数量</th>
                <th class="col10">仕入単価</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even">
            <?php $i = 0; foreach ($result as $r): ?>
            <tr>
                <td style="width: 20px;"></td>
                <td class="col10"><?php echo h_out($r['TrnShipping']['internal_code']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnShipping']['item_name']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnShipping']['standard']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnShipping']['item_code']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnShipping']['dealer_name']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnShipping']['unit_name']); ?></td>
                <td class="col10">
                <?php echo $this->form->text("Receive.Quantity.{$i}", array('class'=>'lbl','style'=>'width:80px; text-align:right;',"value"=>$r['TrnShipping']['quantity'],"readonly"=>"readonly")); ?>
                </td>
                <td class="col10">
                <?php echo $this->form->text("Receive.Price.{$i}", array('class'=>'lbl','style'=>'width:80px; text-align:right;',"value"=>$r['TrnShipping']['price'],"readonly"=>"readonly")); ?>
                </td>
            </tr>
            <?php $i++; endforeach;?>
        </table>
    </div>
</form>
