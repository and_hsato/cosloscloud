<script type="text/javascript">
$(function(){
    //検索ボタン押下
    $("#search_btn").click(function(){
        $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history').submit();
    });
    //CSVボタン押下
    $("#csv_btn").click(function(){
        $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
    });
    //明細表示ボタン押下
    $("#history_btn").click(function(){
        if($('input[type="checkbox"].chk:checked').length > 0 ){
            $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/view_history').submit();
        } else {
            alert('明細は１件以上選択して下さい。');
            exit;
        }
    });
  
    // チェックボックス制御
    $('.checkAll').click(function(){
        $('#history_form input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>移動履歴</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>移動履歴</span></h2>
<form class="validate_form search_form" id="history_form" method="POST" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history">
    <?php echo $this->form->input('isSearch',array('type'=>'hidden','value'=>'1')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>移動番号</th>
                <td><?php echo $this->form->input('Search.work_no',array('class'=>'txt')); ?></td>
                <td width="20"></td>
                <th>移動日</th>
                <td>
                    <?php echo $this->form->input('Search.work_date_from',array('class'=>'txt date validate[optional,custom[date]]','id'=>'datepicker1','style'=>'width: 70px;',"maxlength"=>10));?>
                    ~
                    <?php echo $this->form->input('Search.work_date_to',array('class'=>'txt date validate[optional,custom[date]]','id'=>'datepicker2','style'=>'width: 70px;',"maxlength"=>10));?>
                </td>
                <td width="20"></td>
                <th colspan="2">
                <?php if(Configure::read('SplitTable.flag') == 1){ ?>
                <?php echo $this->form->input('Search.center_move',array('type'=>'checkbox','hiddenField'=>false)); ?>センター間移動を表示
                <?php } ?>
                </th>
            </tr>
            <tr>
                <th>移動元施設</th>
                <td>
                    <?php echo $this->form->input('Search.from_facility_txt',array('class'=>'txt','style'=>'width:60px;' , 'id'=>'centerText')); ?>
                    <?php echo $this->form->input('Search.from_facility_code',array('options'=>$facility_list,'class'=>'txt' , 'empty'=>true , 'id'=>'centerCode')); ?>
                    <?php echo $this->form->input('Search.from_facility_name',array('type'=>'hidden','id'=>'centerName')); ?>
                </td>
                <td></td>
                <th>移動先施設</th>
                <td>
                    <?php echo $this->form->input('Search.to_facility_txt',array('class'=>'txt','style'=>'width:60px;' , 'id'=>'hospitalText')); ?>
                    <?php echo $this->form->input('Search.to_facility_code',array('options'=>$facility_list,'class'=>'txt' , 'empty'=>true , 'id'=>'hospitalCode')); ?>
                    <?php echo $this->form->input('Search.to_facility_name',array('type'=>'hidden','id'=>'hospitalName')); ?>
                </td>
                <td></td>
                <th>移動種別</th>
                <td><?php echo $this->form->input('Search.move_type',array('options'=>$move_types,'class'=>'txt' ,'empty'=>true)); ?></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('Search.internal_code',array('class'=>'txt search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('Search.item_code',array('class'=>'txt search_upper')); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->input('Search.lot_no',array('class'=>'txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('Search.item_name',array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('Search.dealer_name',array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>管理区分</th>
                <td><?php echo $this->form->input('Search.trade_type',array('options'=>$teisu,'class'=>'txt','empty'=>true)); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('Search.standard',array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->input('Search.sticker_no',array('class'=>'txt')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('Search.work_type',array('options'=>$work_classes,'class'=>'txt','empty'=>true)); ?></td>
                <td></td>
            </tr>

        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="search_btn" />
                <input type="button" class="btn btn5" id="csv_btn"/>
            </p>
        </div>
    </div>
    <div id="results">
        <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th style="width:20px;"><input type="checkbox" class="checkAll"/></th>
                    <th style="width:135px;" >移動番号</th>
                    <th>移動日</th>
                    <th>移動種別</th>
                    <th class="col20">移動元</th>
                    <th class="col20" >移動先</th>
                    <th>登録者</th>
                    <th>登録日</th>
                    <th>備考</th>
                    <th>件数</th>
                </tr>
                <?php $i = 0; foreach ($result as $r): ?>
                <tr>
                    <td align="center"><?php echo $this->form->input('TrnMoveHeader.id.'.$i,array('type'=>'checkbox','value'=>$r['TrnMoveHeader']['id'],'class'=>'center chk' , 'hiddenField'=>false)); ?></td>
                    <td ><?php echo h_out($r['TrnMoveHeader']['work_no'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnMoveHeader']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($move_types[ $r['TrnMoveHeader']['move_type'] ],'center'); ?></td>
                    <td><?php echo h_out($r['TrnMoveHeader']['facility_name_from']."／".$r['TrnMoveHeader']['department_name_from']); ?></td>
                    <td><?php echo h_out($r['TrnMoveHeader']['facility_name_to']."／".$r['TrnMoveHeader']['department_name_to']); ?></td>
                    <td><?php echo h_out($r['TrnMoveHeader']['user_name']); ?></td>
                    <td><?php echo h_out($r['TrnMoveHeader']['created'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnMoveHeader']['recital']); ?></td>
                    <td><?php echo h_out($r['TrnMoveHeader']['count'] ."/".  $r['TrnMoveHeader']['detail_count'] , 'right') ?></td>
                </tr>
                <?php $i++; endforeach;?>
                <?php if(count($result) == 0 && isset($this->request->data['isSearch'])): ?>
                <tr>
                    <td colspan="10" class="center"><span>該当データが存在しません</span></td>
                </tr>
                <?php endif; ?>
            </table>
        </div>
        <?php if(count($result) > 0 ){ ?>
        <div class="ButtonBox">
            <p class="center"><input type="button" class="btn btn7" id="history_btn" /></p>
        </div>
        <?php } ?>
    </div>
</form>
