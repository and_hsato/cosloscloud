<script type="text/javascript">
$(function(){
    $('.checkAll_1').click(function(){
        $('#searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#cartForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
  
    //検索ボタン押下
    $("#search_btn").click(function(){
        var tempId = '';
        $("#cartForm input[type=checkbox].chk").each(function(){
            if($(this).attr("checked") == true){
                //hidden項目にID追加
                if(tempId==''){
                    tempId = $(this).val();
                }else{
                    tempId += "," + $(this).val();
                }
            }
        });
        $("#tempId").val(tempId);

        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2outer_search').submit();
    });

    // 確認ボタン押下
    $("#confirm_btn").click(function(){
        var cartId = ''
        if($("#cartForm input[type=checkbox].chk").length > 0) {
            $("#cartForm input[type=checkbox].chk").each(function(){
                if($(this).attr("checked") == true){
                    if(cartId==''){
                        cartId = $(this).val();
                    }else{
                        cartId += "," + $(this).val();
                    }
                }
            });
            $("#cartId").val(cartId);
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2outer_confirm').submit();
        }else{
            alert("商品が選択されていません");
        }
    });

    //選択ボタン押下
    $("#cmdSelect").click(function(){
        var tempId = $("#tempId").val();

        $("#searchForm input[type=checkbox].chk").each(function(){
            if($(this).attr("checked") == true){
                //全選択用にチェックボックスのクラス変更
                //選択行を追加
  /*
                if($('#addTable tr').length/2%2 === 0){
                    $(this).parents('tr:eq(0)').removeClass('odd').appendTo('#addTable');
                    $(this).parent().next('tr:eq(0)').removeClass('odd').appendTo('#addTable');
                }else{
                    $(this).parents('tr:eq(0)').removeClass('odd').addClass('odd').appendTo('#addTable');
                    $(this).parent().next('tr:eq(0)').removeClass('odd').appendTo('#addTable');
                }
                // $('#searchTable').find('tr').removeClass('odd').end().find('tr:odd').addClass('odd');
*/
  
                if($("#addTable tr").length == 0){
                    obj = $("#addTable colgroup:last");
                } else {
                    obj = $("#addTable tr:last");
                }
                obj.after($(this).parents('tr:eq(0)').next());
                obj.after($(this).parents('tr:eq(0)'));

                //hidden項目にID追加
                if(tempId==''){
                    tempId = $(this).val();
                }else{
                    tempId += "," + $(this).val();
                }
            }
        });

        $("#tempId").val(tempId);
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2outer">施設外移動</a></li>
        <li>在庫検索</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>在庫選択</span></h2>
<h2 class="HeaddingMid">データを検索してください</h2>
<form class="validate_form search_form" id="searchForm" method="post">
    <?php echo $this->form->input('TrnMoveHeader.isSearch',array('type'=>'hidden','id'=>'isSearch','value'=>'1'));?>
    <?php echo $this->form->input('TrnMoveHeader.tempId',array('type'=>'hidden','id' =>'tempId'));?>
    <?php echo $this->form->input('TrnMoveHeader.cartId',array('type'=>'hidden','id' =>'cartId'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>移動日</th>
                <td>
                    <?php echo $this->form->input('TrnMoveHeader.work_date',array('type'=>'text','class'=>'lbl','readonly'=>'readonly')); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnMoveHeader.work_class',array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnMoveHeader.work_class_txt',array('class'=>'lbl','readonly'=>'readonly')); ?>
                </td>
            </tr>
            <tr>
                <th>移動元施設</th>
                <td>
                    <?php echo $this->form->input('TrnMoveHeader.facility_code',array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnMoveHeader.facility_name',array('type'=>'text','class'=>'lbl','readonly'=>'readonly')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('TrnMoveHeader.recital',array('type'=>'text','maxlength'=>50,'class'=>'lbl','readonly'=>'readonly')); ?>
                </td>
            </tr>
        </table>
    </div>

    <h2 class="HeaddingLarge2">検索条件</h2>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>在庫部署</th>
                <td>
                    <?php echo $this->form->input('TrnMoveHeader.from_department_text',array('class' => 'txt','style'=>'width: 60px;','id'=>'departmentText')); ?>
                    <?php echo $this->form->input('TrnMoveHeader.from_department_code',array('options'=>$from_departments,'empty'=>true,'class'=>'txt','style'=>'width:150px;','id'=>'departmentCode')); ?>
                    <?php echo $this->form->input('TrnMoveHeader.from_department_name',array('type'=>'hidden','id'=>'departmentName')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code',array('class'=>'txt search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_code',array('class'=>'txt search_upper')); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->input('TrnSticker.lot_no',array('class'=>'txt')); ?></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_name',array('type'=>'text','class'=>'txt search_canna','size'=>10)); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstDealer.dealer_name',array('class'=>'txt search_canna')); ?></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('MstFacilityItem.standard',array('type'=>'text','class'=>'txt search_canna','size'=>10)); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->input('TrnSticker.facility_sticker_no',array('type'=>'text','class'=>'txt','size'=>10)); ?></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn1" id="search_btn" /></p>
    </div>

        <div id="results">
            <hr class="Clear" />
            <div class="DisplaySelect">
                <div align='right' style='margin-right:25px; float:right;'>
                </div>
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
                <span class="BikouCopy"></span>
            </div>
            <div class="">
                <div class="TableHeaderAdjustment01">
                    <table class="TableHeaderStyle01">
                        <tr>
                            <th style="width: 20px;" rowspan="2"><input type="checkbox" class="checkAll_1"></th>
                            <th class="col10">商品ID</th>
                            <th >商品名</th>
                            <th >製品番号</th>
                            <th class="col10">包装単位</th>
                            <th class="col10">ロット番号</th>
                            <th class="col15">センターシール</th>
                            <th class="col15">施設</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>規格</th>
                            <th>販売元</th>
                            <th></th>
                            <th>有効期限</th>
                            <th>部署シール</th>
                            <th>部署</th>
                        </tr>
                    </table>
                </div>
                <table class="TableStyle01 table-even2" id="searchTable">
                    <colgroup>
                        <col>
                        <col align="center">
                        <col>
                        <col>
                        <col>
                        <col>
                    </colgroup>
                    <?php $i = 0; foreach ($result as $row): ?>
                    <tr>
                        <td style="width:20px; padding:0px;" rowspan="2" class="center">
                            <?php if(!$row['TrnMove']['has_reserved']){ ?>
                                 <?php echo $this->form->checkbox("stickerId.{$i}", array('class'=>'center chk','value'=>$row['TrnMove']['id']) ); ?>
                            <?php } ?>
                        </td>
                        <td class="col10"><?php echo h_out($row['TrnMove']['internal_code'] , 'center')?></td>
                        <td><?php echo h_out($row['TrnMove']['item_name']); ?></td>
                        <td><?php echo h_out($row['TrnMove']['item_code']); ?></td>
                        <td class="col10"><?php echo h_out($row['TrnMove']['unit_name']); ?></td>
                        <td class="col10"><?php echo h_out($row['TrnMove']['lot_no']); ?></td>
                        <td class="col15"><?php echo h_out($row['TrnMove']['facility_sticker_no']); ?></td>
                        <td class="col15"><?php echo h_out($row['TrnMove']['facility_name']); ?></td>
                    </tr>
                    <tr>
                        <td>
                        <?php if($row['TrnMove']['has_reserved']){ ?>
                            <span style='color:#FF0000;'>
                                <label title="予約中"><p align='center'>予約中</p></label>
                            </span>
                        <?php } ?>
                        </td>
                        <td><?php echo h_out($row['TrnMove']['standard']); ?></td>
                        <td><?php echo h_out($row['TrnMove']['dealer_name']); ?></td>
                        <td></td>
                        <td><?php echo h_out($row['TrnMove']['validated_date'],'center'); ?></td>
                        <td><?php echo h_out($row['TrnMove']['hospital_sticker_no']); ?></td>
                        <td><?php echo h_out($row['TrnMove']['department_name']);?></td>
                    </tr>
                    <?php $i++; endforeach; ?>
                    <input type='hidden' id='cnt' value='<?php print $i; ?>'>
                </table>
            </div>
            <div class="ButtonBox">
                <p class="center"><input type="button" class="btn btn4" id="cmdSelect" /></p>
            </div>
            <div id="cartForm">
                <h2 class="HeaddingSmall">選択商品</h2>

                <div class="">
                    <div class="TableHeaderAdjustment01">
                    <table class="TableHeaderStyle01">
                        <tr>
                            <th style="width: 20px;" rowspan="2"><input type="checkbox" class="checkAll_2"></th>
                            <th class="col10">商品ID</th>
                            <th >商品名</th>
                            <th >製品番号</th>
                            <th class="col10">包装単位</th>
                            <th class="col10">ロット番号</th>
                            <th class="col15">センターシール</th>
                            <th class="col15">施設</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>規格</th>
                            <th>販売元</th>
                            <th></th>
                            <th>有効期限</th>
                            <th>部署シール</th>
                            <th>部署</th>
                        </tr>
                    </table>

                    </div>
                    <div class="TableStyle01 itemSelectedTable" id="itemSelectedTable">
                        <table class="TableStyle01" id="addTable" style="margin:0;padding:0;">
                            <colgroup>
                                <col>
                                <col align="center">
                                <col>
                                <col>
                                <col>
                                <col>
                            </colgroup>
                        
                            <?php $i = 0; foreach ($cartResult as $row): ?>
                            <tr class="<?php echo ($i%2 ===1)? 'odd':'';?>">
                                <td style="width:20px; padding:0px;" rowspan="2" class="center">
                                <?php echo $this->form->checkbox("stickerId.{$i}", array('class'=>'center chk','value'=>$row['TrnMove']['id'], 'checked'=>true )); ?>
                                </td>
                                <td class="col10"><?php echo h_out($row['TrnMove']['internal_code'] , 'center')?></td>
                                <td><?php echo h_out($row['TrnMove']['item_name']); ?></td>
                                <td><?php echo h_out($row['TrnMove']['item_code']); ?></td>
                                <td class="col10"><?php echo h_out($row['TrnMove']['unit_name']); ?></td>
                                <td class="col10"><?php echo h_out($row['TrnMove']['lot_no']); ?></td>
                                <td class="col15"><?php echo h_out($row['TrnMove']['facility_sticker_no']); ?></td>
                                <td class="col15"><?php echo h_out($row['TrnMove']['facility_name']); ?></td>
                            </tr>
                            <tr class="<?php echo ($i%2 ===1)? 'odd':'';?>">
                                <td></td>
                                <td><?php echo h_out($row['TrnMove']['standard']); ?></td>
                                <td><?php echo h_out($row['TrnMove']['dealer_name']); ?></td>
                                <td></td>
                                <td><?php echo h_out($row['TrnMove']['validated_date'],'center'); ?></td>
                                <td><?php echo h_out($row['TrnMove']['hospital_sticker_no']); ?></td>
                                <td><?php echo h_out($row['TrnMove']['department_name']);?></td>
                            </tr>
                            <?php $i++; endforeach; ?>
                        </table>
                    </div>
                </div>

                <div class="ButtonBox">
                    <input type="button" id="confirm_btn" class="btn btn3" />
                </div>
                <div align='right' style='margin-right:25px; margin-top:10px;'>
                </div>
            </div>
        </div>
    </div>

<h3 id='foot'></h3>
<?php echo $this->form->end(); ?>
