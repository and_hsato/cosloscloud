<script type="text/javascript">
$(document).ready(function(){
    $('input[type=text].lbl').attr('readonly', 'readonly');
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2other">センター間移動</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2other_search">センター間移動在庫選択</a></li>
        <li class="pankuzu">センター間移動確認</li>
        <li>センター間移動結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>センター間移動結果</span></h2>
<h2 class="HeaddingMid">結果を確認してください</h2>
<div class="Mes01">以下の商品の移動予約をしました</div>
<div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col>
            <col>
            <col width="20">
            <col>
            <col>
        </colgroup>
        <tr>
            <th>移動番号</th>
            <td><input type="text" class="lbl" value="<?php echo $params['work_no']; ?>" readonly></td>
            <td></td>
        </tr>
        <tr>
            <th>移動日</th>
            <td><input type="text" class="lbl" value="<?php echo $params['work_date']; ?>" readonly/></td>
            <td></td>
            <th>作業区分</th>
            <td><input type="text" class="lbl" value="<?php echo $params['work_type_name']; ?>" readonly/></td>
        </tr>
        <tr>
            <th>移動先施設</th>
            <td><input type="text" class="lbl" value="<?php echo $params['facility_name']; ?>" readonly/></td>
            <td></td>
            <th>備考</th>
            <td><input type="text" class="lbl" value="<?php echo $params['recital']; ?>" readonly/></td>
        </tr>
    </table>

</div>
<br>
<div class="TableHeaderAdjustment01">
    <table class="TableHeaderStyle01">
        <tr>
            <th width="95">商品ID</th>
            <th width="50%">商品名</th>
            <th width="50%">製品番号</th>
            <th width="150">包装単位</th>
            <th width="95">ロット番号</th>
            <th width="100">仕入単価</th>
            <th width="120">数量</th>
            <th width="120">作業区分</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>センターシール</th>
            <th>有効期限</th>
            <th>評価単価</th>
            <th>単価</th>
            <th>備考</th>
        </tr>
    </table>
</div>
<div class="TableScroll">
    <table class="TableStyle02">
        <?php $i=0;foreach($results as $row): ?>
        <tr class="<?php echo ($i%2===0?'':'odd'); ?>">
            <td width="95"><?php echo h_out($row[0]['internal_code'], 'center'); ?></td>
            <td width="50%"><?php echo h_out($row[0]['item_name']); ?></td>
            <td width="50%"><?php echo h_out($row[0]['item_code']); ?></td>
            <td width="150"><?php echo h_out($row[0]['packing_name']); ?></td>
            <td width="95"><?php echo h_out($row[0]['lot_no']); ?></td>
            <td width="100"><?php echo h_out($this->Common->toCommaStr($row[0]['transaction_price']),'right'); ?></td>
            <td width="120"><?php echo h_out($row[0]['quantity'],'right'); ?></td>
            <td width="120">
              <input type="text" class="lbl" value="<?php echo $classes[$this->request->data['TrnShipping']['work_type'][$row[0]['trn_sticker_id']]]; ?>" />
            </td>
        </tr>
        <tr class="<?php echo ($i%2===0?'':'odd'); ?>">
            <td></td>
            <td><?php echo h_out($row[0]['standard']); ?></td>
            <td><?php echo h_out($row[0]['dealer_name']); ?></td>
            <td><?php echo h_out($row[0]['facility_sticker_no']); ?></td>
            <td><?php echo h_out($row[0]['validated_date'],'center'); ?></td>
            <td>
              <label title="<?php echo ($row[0]['price'] == '' ? '' : $this->Common->toCommaStr($row[0]['price'])); ?>">
                <p align ="right"><?php echo ($row[0]['price'] == '' ? '' : $this->Common->toCommaStr($row[0]['price'])); ?></p>
              </label>
            </td>
            <td>
              <input type="text" class="num lbl" value="<?php echo $this->request->data['TrnShipping']['unit_price'][$row[0]['trn_sticker_id']]; ?>" />
            </td>
            <td>
              <input type="text" class="num lbl" value="<?php echo $this->request->data['TrnShipping']['recital'][$row[0]['trn_sticker_id']]; ?>" />
            </td>
        </tr>
        <?php $i++;endforeach; ?>
    </table>
</div>
