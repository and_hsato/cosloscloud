<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('#confirmForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    //確定ボタン押下時の処理
    $('#btn_regist').click(function(){
        if($("input[type=checkbox].chk:checked").length == 0 ){
            alert('明細を選択してください。');
        } else {
            $("input[type=checkbox].chk").each(function(){
                if($(this).attr("checked") == true){
                    var price = $(this).parents('tr:eq(0)').next().find('input[type=text].price');
                    // 単価入力チェック
                    if(price.val() == ''){
                        alert('単価を入力してください。');
                        price.focus();
                        exit;
                    }
                    // 単価フォーマットチェック
                    if(!numCheck(price.val() , 10 , 2 , false) ){
                        alert('単価の入力に誤りがあります。');
                        price.focus();
                        exit;
                    }
                }
            });
            $("#confirmForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2outside_result').submit();
        }
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/move2outside">管理外移出</a></li>
        <li class="pankuzu">在庫選択</li>
        <li>管理外移出確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>管理外移出確認</span></h2>
<h2 class="HeaddingMid">対象を確認してください</h2>

<?php echo $this->form->create( 'Move',array('type'=>'post','action' =>'','id'=>'confirmForm','class'=>'validate_form') );?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>移動日</th>
                <td>
                    <?php echo $this->form->text('TrnMoveHeader.work_date',array('class'=>'lbl','readonly'=>'readonly')); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->hidden('TrnMoveHeader.work_class'); ?>
                    <?php echo $this->form->text('TrnMoveHeader.work_class_txt',array('class'=>'lbl','readonly'=>'readonly')); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo Configure::read('facility_subcode'); ?></th>
                <td>
                    <?php echo $this->form->text('TrnMoveHeader.subcode',array('class'=>'lbl','readonly'=>'readonly')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->text('TrnMoveHeader.recital',array('class'=>'lbl','readonly'=>'readonly')); ?>
                </td>
            </tr>
        </table>
    </div>

    <?php echo $this->form->input('TrnMoveHeader.token',array('type'=>'hidden'));?>
    <?php echo $this->form->input('TrnMoveHeader.time',array('type'=>'hidden','value' =>date('Y/m/d H:i:s.u')));?>

    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle01">
            <tr>
                <th style="width:20px;" rowspan="2"><input type="checkbox" checked class="checkAll" /></th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col15">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col10">仕入単価</th>
                <th class="col10">数量</th>
                <th class="col10">作業区分</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>センターシール</th>
                <th>有効期限</th>
                <th>評価単価</th>
                <th>単価</th>
                <th>備考</th>
            </tr>
        </table>
    </div>

    <div class="TableScroll">
        <table class="TableStyle02 table-even2" style="margin:0px; padding:0px;">
            <?php $i = 0; foreach ($result as $row): ?>
            <tr>
                <td style="width:20px; padding:0px;" rowspan="2" class="center">
                    <?php echo $this->form->checkbox("stickerId.{$i}", array('class'=>'center chk','value'=>$row['TrnMove']['id'],'checked'=>true , 'hiddenField'=>false) );?>
                    <?php echo $this->form->hidden("Moves.modified.{$row['TrnMove']['id']}",array('value' => $row['TrnMove']['modified']));?>
                </td>
                <td class="col10"><?php echo h_out( $row['TrnMove']['internal_code'] , 'center'); ?></td>
                <td><?php echo h_out($row['TrnMove']['item_name']); ?></td>
                <td><?php echo h_out($row['TrnMove']['item_code']); ?></td>
                <td class="col15"><?php echo h_out( $row['TrnMove']['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($row['TrnMove']['lot_no']); ?></td>
                <td class="col10"><?php echo h_out($this->Common->toCommaStr($row['TrnMove']['transaction_price']),'right'); ?></td>
                <td class="col10"><?php echo $this->form->text("Moves.quantity.{$row['TrnMove']['id']}", array('class'=>'txt','style'=>'width:85px; text-align:right;','readonly'=>'readonly',"value"=>1)); ?></td>
                <td class="col10">
                    <?php echo $this->form->input("Moves.work_class.{$row['TrnMove']['id']}",array('options'=>$work_classes,'class'=>'txt','empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($row['TrnMove']['standard']); ?></td>
                <td><?php echo h_out($row['TrnMove']['dealer_name']); ?></td>
                <td><?php echo h_out($row['TrnMove']['facility_sticker_no']); ?></td>
                <td><?php echo h_out($row['TrnMove']['validated_date'] , 'center')?></td>
                <td><?php echo h_out($this->Common->toCommaStr($row['TrnMove']['price']), 'right')?></td>
                <td>
                    <?php echo $this->form->text("Moves.price.{$row['TrnMove']['id']}", array('class'=>'r price num txt ',"maxlength"=>13));?>
                </td>
                <td>
                    <?php echo $this->form->text("Moves.recital.{$row['TrnMove']['id']}", array('class'=>'txt','style'=>'width:85px;',"maxlength"=>200));?>
                </td>
            </tr>
            <?php $i++; endforeach; ?>
        </table>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
        <p class="center"><input type="button"  class="btn btn2" id="btn_regist" /></p>
    </div>
<?php echo $this->form->end();?>
