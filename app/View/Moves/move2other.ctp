<script type="text/javascript">
$(document).ready(function(){
    $('#moveDate').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});

    $('#facilityInput').blur(function(){
        if($('#facilitySelect').find('option[value=' + $(this).val() + ']').length === 0){
            $('#facilitySelect').val('');
        }else{
            $('#facilitySelect').val($(this).val());
        }
    });

    //施設のプルダウンを変更
    $('#facilitySelect').change(function(){
        $('#facilityInput').val($(this).val());
        $('#facilityName').val($('#facilitySelect option:selected').text());
    });

    //在庫選択押下
    $('#inventorySelect').click(function(){
        $('#move2other_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2other_search').submit();
    });

    //作業区分のプルダウンを変更
    $('#work_type').change(function(){
        $('#work_name').val($('#work_type option:selected').text());
    });


});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>センター間移動</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>センター間移動</span></h2>
<h2 class="HeaddingMid">データを入力してください</h2>

<form id="move2other_form" method="POST" action="<?php echo $this->webroot; ?>moves/move2other" class="validate_form">
    <input type="hidden" name="data[isSelectIventory]" value="1" />
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>移動日</th>
                <td><?php echo $this->form->input('Move2Other.work_date', array(
                            'class'=>'validate[required,custom[date] date r',
                            'id'=>'moveDate',
                            "maxlength"=>10
                          )); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('Move2Other.work_type', array('options'=>$classes, 'class'=>'txt' , 'id'=>'work_type' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('Move2Other.work_type_name', array('type'=>'hidden' , 'id'=>'work_name')); ?>
                </td>
            </tr>
            <tr>
                <th>移動先施設</th>
                <td>
                    <?php echo $this->form->input('Move2Other.facility_input', array('class'=>'r txt validate[required]', 'style'=>'width:60px;', 'id'=>'facilityInput')); ?>
                    <?php echo $this->form->input('Move2Other.facility_select', array('options'=>$facilities, 'class'=>'r txt validate[required]', 'id'=>'facilitySelect' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('Move2Other.facility_name' , array('type'=>'hidden' , 'id'=>'facilityName'))?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('Move2Other.recital', array('class'=>'txt',"maxlength"=>200)); ?></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn15 submit" id="inventorySelect"/>
        </p>
    </div>
</form>
