<script type="text/javascript">
//jquery calendar
$(function($){
    //チェックボックス一括制御
    $('.checkAll').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
  
   // 確定ボタン押下
    $("#receive_confirm_form_btn").click(function(){
        if($('input[type=checkbox].chk:checked').length === 0){  
            alert( "少なくとも1つの商品にチェックをつけてください");
        }else{
            $("#receive_confirm_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/receive_result').submit();
        }
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/search_item_moved">移動受領</a></li>
        <li>移動受領確認</li>
   </ul>
</div>
<h2 class="HeaddingLarge"><span>移動受領確認</span></h2>
<h2 class="HeaddingMid">受領する商品を選択してください</h2>
<form class="validate_form" id="receive_confirm_form" method="post">
    <?php echo $this->form->input('Search.center_move' , array('type'=>'hidden'));?>
    <?php echo $this->form->input('TrnMoveHeader.token' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('TrnMoveHeader.time' , array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u')))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>受領日</th>
                <td><?php echo $this->form->input('MoveHeader.work_date',array('maxlength'=>10,'type'=>'text','class'=>'r date validate[required,custom[date]]','id'=>'datepicker1')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>備考</th>
                <td><?php echo $this->form->input('MoveHeader.recital',array('maxlength'=>200,'class'=>'txt')); ?></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="results">
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="25"/>
                    <col width="80"/>
                    <col />
                    <col />
                    <col width="180"/>
                    <col />
                    <col width="350"/>
                </colgroup>
                <tr>
                    <th rowspan="2"><input type="checkbox" checked class="checkAll" /></th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>数量</th>
                    <th>移動元</th>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>センターシール</th>
                    <th>受入単価</th>
                    <th>移動先</th>
                </tr>

                <?php $cnt=0;foreach($result as $r) { ?>
                <tr>
                    <td rowspan="2" class="center">
                        <input checked type="checkbox" class="chk" id="" name="data[TrnMove][id][]" value="<?php echo $r['TrnMove']['id']; ?>"/>
                        <?php echo $this->form->input('TrnMove.mst_item_unit_id.'.$r['TrnMove']['id'] , array('type'=>'hidden' , 'value'=>$r['TrnMove']['mst_item_unit_id'])); ?>
                        <?php echo $this->form->input('TrnMove.modified.'.$r['TrnMove']['id'] , array('type'=>'hidden' , 'value'=>$r['TrnMove']['modified'])); ?>
                    </td>
                    <td><?php echo h_out($r['TrnMove']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnMove']['item_name']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['item_code']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['unit_name']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['quantity'],'right'); ?></td>
                    <td><?php echo h_out($r['TrnMove']['facility_name_from']."／".$r['TrnMove']['department_name_from']); ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><?php echo h_out($r['TrnMove']['standard']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['facility_sticker_no'],'center'); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($r['TrnMove']['transaction_price']),'right'); ?></td>
                    <td><?php echo h_out($r['TrnMove']['facility_name_to']."／".$r['TrnMove']['department_name_to']); ?></td>
                </tr>
                <?php $cnt++; } ?>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn2 confirm_btn [p2]" id="receive_confirm_form_btn" /></p>
    </div>
</form>
