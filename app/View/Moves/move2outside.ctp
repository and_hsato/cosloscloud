<script type="text/javascript">
$(function(){
    $("#move2outside_form_btn").click(function(){
        $("#move2outside_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2outside_search');
        $("#move2outside_form").attr('method', 'post');
        $("#move2outside_form").submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>管理外移出</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>管理外移出</span></h2>
<h2 class="HeaddingMid">データを入力してください</h2>

<form id="move2outside_form" class="validate_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>移動日</th>
                <td>
                    <?php echo $this->form->text('TrnMoveHeader.work_date',array('maxlength'=>10,'class'=>'r date validate[required,custom[date]] ','id'=>'datepicker1')); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->hidden('TrnMoveHeader.work_class_txt'); ?>
                    <?php echo $this->form->input('TrnMoveHeader.work_class',array('options'=>$work_classes,'empty'=>true,'class'=>'txt','id'=>'work_class')); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo Configure::read('facility_subcode'); ?></th>
                <td><?php echo $this->form->text('TrnMoveHeader.subcode',array('maxlength'=>20,'class'=>'r validate[required]')); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->text('TrnMoveHeader.recital',array('maxlength'=>200,'class'=>'txt')); ?></td>
            </tr>
        </table>
    </div>

    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn15 submit" id="move2outside_form_btn" /></p>
    </div>
</form>