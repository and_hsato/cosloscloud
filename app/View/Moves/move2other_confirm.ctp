<script type="text/javascript">
$(document).ready(function(){
    $('.checkAll').click(function(){
        $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
    });

    $('#commit').click(function(){
        if($('input[type=checkbox].checkAllTarget:checked').length === 0){
            alert('移動データを選択してください');
            return false;
        }else{
            var hasError = false;
            $('input[type=checkbox].checkAllTarget').each(function(i){
                if(this.checked){
                    var val = $('input[type=text].unit_price:eq('+i+')').val();
                    if(val == ''){
                        alert('単価を入力してください');
                        hasError = true;
                        return false;
                    }else if(!val.match(/^(0|[1-9]\d*|[0-9]\.[0-9]{1,2}|[1-9]\d+\.[0-9]{1,2})$/)){
                        alert('単価を正しく入力してください');
                        hasError = true;
                        return false;
                    }
                }
            });
            if(hasError === true){
                return false;
            }else{
                $('#move2other_form').attr('action','<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2other_results').submit();
            }
        }
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2other">センター間移動</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2other_search">センター間移動在庫選択</a></li>
        <li>センター間移動確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>センター間移動確認</span></h2>
<h2 class="HeaddingMid">移動データを確認してください</h2>
<form class="validate_form" id="move2other_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2other_results" method="post">
    <input type="hidden" name="data[Move2Other][viewtime]" value="<?php echo time(); ?>" />
    <?php echo $this->form->input('TrnMoveHeader.time' , array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
    <?php echo $this->form->input('TrnMoveHeader.token' , array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>移動日</th>
                <td>
                    <?php echo $this->form->input('Move2Other.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly')); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('Move2Other.work_type_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly')); ?>
                    <?php echo $this->form->input('Move2Other.work_type' , array('type'=>'hidden')); ?>
                </td>
            </tr>
            <tr>
                <th>移動先施設</th>
                <td>
                    <?php echo $this->form->input('Move2Other.facility_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly')); ?>
                    <?php echo $this->form->input('Move2Other.facility_input' , array('type'=>'hidden')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Move2Other.recital' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly')); ?>
                </td>
            </tr>
        </table>

    </div>
    <br>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle01">
            <colgroup>
                <col width="20" />
                <col width="95" />
                <col width="50%" />
                <col width="50%" />
                <col width="150" />
                <col width="95" />
                <col width="100" />
                <col width="120" />
                <col width="120" />
            </colgroup>

            <tr>
                <th rowspan="2" class="center"><input type="checkbox" checked class="checkAll center" /></th>
                <th>商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th>包装単位</th>
                <th>ロット番号</th>
                <th>仕入単価</th>
                <th>数量</th>
                <th>作業区分</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>センターシール</th>
                <th>有効期限</th>
                <th>評価単価</th>
                <th>単価</th>
                <th>備考</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02">
            <?php $i=0;foreach($SearchResult as $row): ?>
            <tr class="<?php echo ($i%2===0?'':'odd'); ?>">
                <td style="width:20px;" rowspan="2" class="center">
                    <?php echo $this->form->checkbox("Move2Other.id.{$i}", array('value'=>$row[0]['trn_sticker_id'] , 'class'=>'center checkAllTarget','checked'=>true , 'hiddenField'=>false)); ?>
                </td>
                <td width="95"><?php echo h_out($row[0]['internal_code'],'center'); ?></td>
                <td width="50%"><?php echo h_out($row[0]['item_name']); ?></td>
                <td width="50%"><?php echo h_out($row[0]['item_code']); ?></td>
                <td width="150"><?php echo h_out($row[0]['packing_name']); ?></td>
                <td width="95"><?php echo h_out($row[0]['lot_no']); ?></td>
                <td width="100"><?php echo h_out($this->Common->toCommaStr($row[0]['transaction_price']), 'right'); ?></td>
                <td width="120"><?php echo h_out($row[0]['quantity'],'right'); ?></td>
                <td width="120">
                  <?php echo $this->form->input("TrnShipping.work_type.{$row[0]['trn_sticker_id']}", array('options'=>$classes, 'class'=>'txt' ,'empty'=>true)); ?>
                </td>
            </tr>
            <tr class="<?php echo ($i%2===0?'':'odd'); ?>">
                <td></td>
                <td><?php echo h_out($row[0]['standard']); ?></td>
                <td><?php echo h_out($row[0]['dealer_name']); ?></td>
                <td><?php echo h_out($row[0]['facility_sticker_no']); ?></td>
                <td><?php echo h_out($row[0]['validated_date'],'center'); ?></td>
                <td align="right">
                  <label title="<?php echo ($row[0]['price'] == '' ? '' : $this->Common->toCommaStr($row[0]['price'])); ?>">
                    <p align ="right"><?php echo ($row[0]['price'] == '' ? '' : $this->Common->toCommaStr($row[0]['price'])); ?></p>
                  </label>
                </td>
                <?php
                  //評価額がある場合は評価額、ない場合は仕入単価をデフォルト表示
                  $default_price = "";
                  if($row[0]['price'] !== ''){
                    $default_price = $row[0]['price'];
                  }else{
                    $default_price = $row[0]['transaction_price'];
                  }
                ?>
                <td>
                  <input type="text" class="num r unit_price" name="data[TrnShipping][unit_price][<?php echo $row[0]['trn_sticker_id']; ?>]" value="<?php echo $default_price; ?>" maxlength="12" />
                </td>
                <td>
                  <input type="text" class="txt" name="data[TrnShipping][recital][<?php echo $row[0]['trn_sticker_id']; ?>]" value="" />
                </td>
            </tr>
            <?php $i++;endforeach; ?>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn2 fix_btn" value="" id="commit" /></p>
    </div>
</form>
