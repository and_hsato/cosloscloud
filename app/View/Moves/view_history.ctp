<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
  
    $("#delete_btn").click(function(){
         if($('input[type="checkbox"].chk:checked').length > 0 ){
             if(window.confirm("取消を実行します。よろしいですか？")){
                 $("#view_history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/delete_history').submit();
             }
         } else {
             alert('明細を選択してください。');
         }
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/history">移動履歴</a></li>
        <li>移動履歴明細</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>移動履歴明細</span></h2>
<form class="validate_form" id="view_history_form" method="POST">
    <?php echo $this->form->input('TrnMoveHeader.time' , array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))) ?>
    <?php echo $this->form->input('TrnMoveHeader.token' , array('type'=>'hidden')) ?>
  
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th style="width: 20px;" rowspan="2"><input type="checkbox" class="checkAll" checked></th>
                <th class="col15">移動番号</th>
                <th>移動元</th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col10">数量</th>
                <th class="col10">作業区分</th>
            </tr>
            <tr>
                <th>受領番号</th>
                <th>移動先</th>
                <th>種別</th>
                <th>規格</th>
                <th>販売元</th>
                <th>センターシール</th>
                <th>有効期限</th>
                <th>単価</th>
                <th>備考</th>
            </tr>
            <?php $i=0; foreach($result as $row ):?>
            <tr>
                <td rowspan="2" class="center">
                    <?php
                    if($row['TrnShipping']['check'] == true ){
                        echo $this->form->checkbox("TrnShipping.id.{$i}",array('value'=>$row['TrnShipping']['id'],"checked"=>true,"class"=>"chk" , 'hiddenField'=>false));
                    }
                    ?>
                </td>
                <td><?php echo h_out($row['TrnMoveHeader']['work_no']); ?></td>
                <td><?php echo h_out($row['FromFacility']['facility_name']."／".$row['FromDepartment']['department_name']); ?></td>
                <td><?php echo h_out($row['MstFacilityItem']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($row['MstFacilityItem']['item_name']); ?></td>
                <td><?php echo h_out($row['MstFacilityItem']['item_code']); ?></td>
                <td><?php echo h_out($row['TrnShipping']['unit_name']); ?></td>
                <td><?php echo h_out($row['TrnSticker']['lot_no']); ?></td>
                <td><?php echo h_out(number_format($row['TrnShipping']['quantity']),'right'); ?></td>
                <td><?php echo h_out($row['TrnShipping']['work_type_name']); ?></td>
            </tr>
            <tr>
                <td><?php echo h_out($row['MstReceiving']['work_no']); ?></td>
                <td><?php echo h_out($row['ToFacility']['facility_name']."／".$row['ToDepartment']['department_name']); ?></td>
                <td><?php echo h_out($move_types[ $row['TrnMoveHeader']['move_type'] ],'center') ?></td>
                <td><?php echo h_out($row['MstFacilityItem']['standard']); ?></td>
                <td><?php echo h_out($row['MstDealer']['dealer_name']); ?></td>
                <td><?php echo h_out($row['TrnSticker']['facility_sticker_no']); ?></td>
                <td><?php echo h_out($row['TrnSticker']['validated_date']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($row['TrnSticker']['transaction_price']),'right'); ?></td>
                <td><?php echo h_out($row['TrnShipping']['recital']); ?></td>
            </tr>
            <?php $i++; endforeach;?>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn6 submit [p2]" id="delete_btn" />
        </p>
    </div>
</form>
