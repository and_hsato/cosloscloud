<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/history">移動履歴</a></li>
        <li class="pankuzu">移動履歴明細</li>
        <li>移動取消</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>移動取消</span></h2>
<div class="Mes01">移動登録を取り消しました</div>
<form id="view_history_form">
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th style="width: 20px;" rowspan="2"></th>
                <th class="col15">移動番号</th>
                <th>移動元</th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col10">数量</th>
                <th class="col10">作業区分</th>
            </tr>
            <tr>
                <th>受領番号</th>
                <th>移動先</th>
                <th>種別</th>
                <th>規格</th>
                <th>販売元</th>
                <th>センターシール</th>
                <th>有効期限</th>
                <th>単価</th>
                <th>備考</th>
            </tr>
            <?php $i=0; foreach($result as $row ):?>
            <tr>
                <td rowspan="2" style="padding:0px;">&nbsp;</td>
                <td><?php echo h_out($row['TrnMoveHeader']['work_no'],'center'); ?></td>
                <td><?php echo h_out($row['FromFacility']['facility_name']); ?></td>
                <td><?php echo h_out($row['MstFacilityItem']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($row['MstFacilityItem']['item_name']); ?></td>
                <td><?php echo h_out($row['MstFacilityItem']['item_code']); ?></td>
                <td><?php echo h_out($row['MstFacilityItem']['unit_name']); ?></td>
                <td><?php echo h_out($row['TrnSticker']['lot_no']); ?></td>
                <td><?php echo h_out($row['TrnSticker']['quantity'],'right'); ?></td>
                <td><?php echo h_out($row['TrnShipping']['work_type']); ?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($row['ToFacility']['facility_name']); ?></td>
                <td><?php echo h_out($move_types[$row['TrnMoveHeader']['move_type']],'center') ?></td>
                <td><?php echo h_out($row['MstFacilityItem']['standard']); ?></td>
                <td><?php echo h_out($row['MstDealer']['dealer_name']); ?></td>
                <td><?php echo h_out($row['TrnSticker']['facility_sticker_no']); ?></td>
                <td><?php echo h_out($row['TrnSticker']['validated_date']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($row['TrnSticker']['transaction_price'])); ?></td>
                <td><?php echo h_out($row['TrnShipping']['recital']); ?></td>
            </tr>
            <?php $i++; endforeach;?>
        </table>
    </div>
</form>
