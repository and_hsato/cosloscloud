<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('#confirmForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    //確定ボタン押下時の処理
    $('#btn_regist').click(function(){
        if(!dateCheck($('#datepicker1').val())){
            alert("移動日が不正です");
            exit;
        }
        if($('#confirmForm input[type=checkbox].chk:checked').length > 0 ){ 
            $('#confirmForm input[type=checkbox].chk').each(function(){
                if($(this).attr('checked') == true){
                    var price = $(this).parents('tr:eq(0)').find('input[type=text].price');
                    var quantity = $(this).parents('tr:eq(0)').find('input[type=text].quantity');
                    // 数量入力チェック
                    if(quantity.val() == ''){
                        alert('数量を入力してください。');
                        quantity.focus();
                        exit;
                    }
                    // 数量フォーマットチェック
                    if(!numCheck(quantity.val() , 10 , 0 , true) ){
                        alert('数量の入力に誤りがあります。');
                        quantity.focus();
                        exit;
                    }
                    // 単価入力チェック
                    if(price.val() == ''){
                        alert('単価を入力してください。');
                        price.focus();
                        exit;
                    }
                    // 単価フォーマットチェック
                    if(!numCheck(price.val() , 10 , 2 , false) ){
                        alert('単価の入力に誤りがあります。');
                        price.focus();
                        exit;
                    }
                }
            });

            $("#confirmForm").attr('action','<?php echo $this->webroot; ?><?php echo $this->name; ?>/receive_from_outside_result').submit();
        } else {
            alert('明細を選択してください');
        }
    });
});


</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/receive_from_outside">管理外移入</a></li>
        <li>管理外移入確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>管理外移入確認</span></h2>
<h2 class="HeaddingMid">移入データを確認してください</h2>

<form class="validate_form" id="confirmForm" method="post">
    <?php echo $this->form->input('TrnMoveHeader.time' , array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u')));?>
    <?php echo $this->form->input('TrnMoveHeader.token' , array('type'=>'hidden'));?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>移動日</th>
                <td><?php echo $this->form->input('TrnMoveHeader.work_date',array('maxlength'=>10,'type'=>'text','class'=>'r date validate[required,date]','id'=>'datepicker1')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnMoveHeader.work_class_txt',array('type'=>'hidden','id'=>'work_class_txt')); ?>
                    <?php echo $this->form->input('TrnMoveHeader.work_class',array('options'=>$work_classes,'empty'=>true,'class'=>'txt','id'=>'work_class')); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo Configure::read('facility_subcode'); ?></th>
                <td><?php echo $this->form->input('TrnMoveHeader.subcode',array('maxlength'=>20,'class'=>'r validate[required]')); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnMoveHeader.recital',array('maxlength'=>200,'class'=>'txt')); ?></td>
            </tr>
        </table>
    </div>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle01">
            <tr>
                <th style="width: 20px;"><input type="checkbox" class="checkAll" CHECKED></th>
                <th class="col10">商品ID</th>
                <th class="col15">商品名</th>
                <th class="col15">規格</th>
                <th class="col15">製品番号</th>
                <th class="col15">販売元</th>
                <th class="col10">包装単位</th>
                <th class="col10">数量</th>
                <th class="col10">単価</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even">
            <?php $i = 0; foreach ($result as $r): ?>
            <tr>
                <td style="width: 20px; padding:0px;" class="center">
                    <?php echo $this->form->checkbox("Receive.ItemId.{$i}",array('value'=>$r['TrnMove']['id'],'class'=>'center chk','checked'=>true , 'hiddenField'=>false)); ?>
                </td>
                <td class="col10"><?php echo h_out($r['TrnMove']['internal_code'],'center'); ?></td>
                <td class="col15"><?php echo h_out($r['TrnMove']['item_name']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnMove']['standard']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnMove']['item_code']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnMove']['dealer_name']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnMove']['unit_name']); ?></td>
                <td class="col10">
                    <?php echo $this->form->text("Receive.Quantity.{$r['TrnMove']['id']}", array('class'=>'r num txt quantity','maxlength'=>10)); ?>
                </td>
                <td class="col10">
                    <?php echo $this->form->text("Receive.Price.{$r['TrnMove']['id']}", array('class'=>'r num txt price','maxlength'=>13)); ?>
                </td>
            </tr>
            <?php $i++; endforeach;?>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn2 confirm_btn" id="btn_regist" /></p>
    </div>
</form>
