<script type="text/javascript">
$(document).ready(function(){
    //在庫選択ボタン押下
    $("#move2outer_form_btn").click(function(){
        $("#move2inner_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2outer_search');
        $("#move2inner_form").submit();
    });
});

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>施設外移動</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>施設外移動</span></h2>
<h2 class="HeaddingMid">条件を指定してください</h2>

<?php echo $this->form->create( 'Moves',array('type'=>'post','action' =>'post','class'=>'validate_form input_form',"id"=>"move2inner_form") );?>
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
        </colgroup>
        <tr>
            <th>移動日</th>
            <td><?php echo $this->form->input('TrnMoveHeader.work_date',array('maxlength'=>10,'type'=>'text','class'=>'r validate[required,custom[date],funcCall2[date2]] date','id'=>'datepicker1')); ?></td>
            <td></td>
            <th>作業区分</th>
            <td>
                <?php echo $this->form->hidden('TrnMoveHeader.work_class_txt',array('id'=>'work_class_txt')); ?>
                <?php echo $this->form->input('TrnMoveHeader.work_class',array('options'=>$work_classes,'empty'=>true,'class'=>'txt','id'=>'work_class')); ?>
            </td>
        </tr>
        <tr>
            <th>移動元施設</th>
            <td>
                <?php echo $this->form->input('TrnMoveHeader.facility_name',array('type'=>'hidden','id'=>'hospitalName')); ?>
                <?php echo $this->form->input('TrnMoveHeader.facility_text',array('class'=>'r ','style'=>'width: 60px;','id'=>'hospitalText')); ?>
                <?php echo $this->form->input('TrnMoveHeader.facility_code',array('options'=>$from_facilities,'empty'=>true,'class'=>'r validate[required]','style'=>'width:150px;','id'=>'hospitalCode')); ?>
            </td>
            <td></td>
            <th>備考</th>
            <td>
                <?php echo $this->form->input('TrnMoveHeader.recital',array('type'=>'text','class'=>'txt')); ?>
            </td>
        </tr>
    </table>

    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn15 submit" id="move2outer_form_btn" />
        </p>
    </div>
<?php echo $this->form->end();?>
