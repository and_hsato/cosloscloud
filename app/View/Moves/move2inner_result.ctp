<script type="text/javascript">
$(function(){
    // 部署シール印字ボタン押下
    $("#hospital_sticker_print_btn").click(function(){
        if(confirm("部署シール印刷を実行します。よろしいですか？")){
            $("#resultForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/print_hospital_sticker').submit();
        }
        return;
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/move2inner">施設内移動</a></li>
        <li class="pankuzu">在庫選択</li>
        <li class="pankuzu">施設内移動確認</li>
        <li>施設内移動結果</li>
    </ul>
</div>
<?php echo $this->form->create( 'Move',array('type'=>'post','action' =>'','id'=>'resultForm','class'=>'validate_form') ); ?>
<?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
<h2 class="HeaddingLarge"><span>施設内移動結果</span></h2>
<h2 class="HeaddingMid">結果を確認してください</h2>
<div class="Mes01">以下の商品を移動しました</div>
<div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
        </colgroup>
        <tr>
            <th>移動日</th>
            <td>
                <?php echo $this->form->text('TrnMoveHeader.work_date',array('class'=>'lbl','readonly'=>'readonly')); ?>
            </td>
            <td></td>
            <th>作業区分</th>
            <td>
                <?php echo $this->form->hidden('TrnMoveHeader.work_class'); ?>
                <?php echo $this->form->text('TrnMoveHeader.work_class_txt',array('class'=>'lbl','readonly'=>'readonly')); ?>
            </td>
        </tr>
        <tr>
            <th>移動先施設</th>
            <td>
                <?php echo $this->form->hidden('TrnMoveHeader.facility_code'); ?>
                <?php echo $this->form->text('TrnMoveHeader.facility_name',array('class'=>'lbl','readonly'=>'readonly')); ?>
            </td>
            <td></td>
            <th>備考</th>
            <td>
                <?php echo $this->form->text('TrnMoveHeader.recital',array('maxlength'=>50,'class'=>'lbl')); ?>
            </td>
        </tr>
        <tr>
            <th>移動先部署</th>
            <td>
                <?php echo $this->form->hidden('TrnMoveHeader.department_code'); ?>
                <?php echo $this->form->text('TrnMoveHeader.department_name',array('class'=>'lbl','readonly'=>'readonly')); ?>
            </td>
        </tr>
    </table>
</div>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th style="width:20px;" rowspan="2"></th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col5">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col15">センターシール</th>
                <th class="col15">施設</th>
                <th class="col10">作業区分</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>数量</th>
                <th>有効期限</th>
                <th>部署シール</th>
                <th>部署</th>
                <th>備考</th>
            </tr>
            <?php $i = 0; foreach ($resultView as $row): ?>
            <tr>
                <td rowspan="2"><?php echo $this->form->input('TrnMove.id.'.$row['TrnMove']['id'] , array('type'=>'hidden','value'=>$row['TrnMove']['id'])); ?></td>
                <td><?php echo h_out($row['TrnMove']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($row['TrnMove']['item_name']); ?></td>
                <td><?php echo h_out($row['TrnMove']['item_code']);?></td>
                <td><?php echo h_out($row['TrnMove']['unit_name']); ?></td>
                <td><?php echo h_out($row['TrnMove']['lot_no']); ?></td>
                <td><?php echo h_out($row['TrnMove']['facility_sticker_no']); ?></td>
                <td><?php echo h_out($row['TrnMove']['facility_name']); ?></td>
                <td>
                <?php echo $this->form->input("Moves.work_class.{$row['TrnMove']['id']}",array('options'=>$work_classes,'class'=>'txt','empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($row['TrnMove']['standard']); ?></td>
                <td><?php echo h_out($row['TrnMove']['dealer_name']); ?></td>
                <td>
                    <?php echo $this->form->text("Moves.quantity.{$row['TrnMove']['id']}",array('class'=>'lbl','style'=>'width:85px; text-align:right;','readonly'=>'readonly','value'=>$this->request->data['Moves']['quantity'][$row['TrnMove']['id']]));?>
                </td>
                <td><?php echo h_out($row['TrnMove']['validated_date'] , 'center'); ?></td>
                <td><?php echo h_out($row['TrnMove']['hospital_sticker_no']); ?></td>
                <td><?php echo h_out($row['TrnMove']['department_name']); ?></td>
                <td>
                    <?php echo $this->form->text("Moves.recital.{$row['TrnMove']['id']}", array('class'=>'lbl','style'=>'width:85px;','value'=>$this->request->data['Moves']['recital'][$row['TrnMove']['id']]));?>
                </td>
            </tr>
            <?php $i++; endforeach; ?>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn16 submit print_btn" id="hospital_sticker_print_btn" />
        </p>
    </div>
<?php echo $this->form->end(); ?>