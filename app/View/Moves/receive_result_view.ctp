<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/search_item_moved">移動受領</a></li>
        <li>移動受領結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>移動受領結果</span></h2>
<div class="Mes01">移動受領登録を行いました</div>
<form>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>受領日</th>
                <td><?php echo $this->form->input('MoveHeader.work_date',array('type'=>'text','class'=>'lbl','readonly'=>'readonly')); ?></td>
            </tr>
            <tr>
                <th>備考</th>
                <td><?php echo $this->form->input('MoveHeader.recital',array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
        </table>
    </div>
    <div class="results">
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="80"/>
                    <col />
                    <col />
                    <col width="180"/>
                    <col />
                    <col width="350"/>
                </colgroup>
                <tr>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>数量</th>
                    <th>移動元</th>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>センターシール</th>
                    <th>受入単価</th>
                    <th>移動先</th>
                </tr>
                <?php $cnt=0; foreach($result as $r) { ?>
                <tr>
                    <td><?php echo h_out($r['TrnMove']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnMove']['item_name']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['item_code']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['unit_name']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['quantity'],'right'); ?></td>
                    <td><?php echo h_out($r['TrnMove']['facility_name_from']."／".$r['TrnMove']['department_name_from']); ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><?php echo h_out($r['TrnMove']['standard']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['facility_sticker_no'],'center'); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($r['TrnMove']['transaction_price']),'right'); ?></td>
                    <td><?php echo h_out($r['TrnMove']['facility_name_to']."／".$r['TrnMove']['department_name_to']); ?></td>
                </tr>
                <?php $cnt++; } ?>
            </table>
        </div>
    </div>
</form>
