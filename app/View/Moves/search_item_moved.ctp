<script type="text/javascript">
/**
 * page transfer.
 */

var item_data = eval(<?php echo $json_data ?>);
  
$(function(){
    //検索ボタン押下
    $("#search_item_moved_form_btn").click(function(){
        $("#search_item_moved_form").attr('action', '<?php echo $this->webroot; ?>moves/search_item_moved').submit();
    });
    //確認ボタン押下
    $("#search_item_moved_selected_form_btn").click(function(){
        if($('input[type="checkbox"].chk:checked').length == 0 ){  
            alert( "少なくとも1つの商品にチェックをつけてください");
            return false;
        }
        $("#search_item_moved_form").attr('action', '<?php echo $this->webroot; ?>moves/receive_confirm').submit();
    });
    //移動元プルダウン変更
    $("#moves_from_facility_selected").change(function () {
        $('#search_from_facility_txt').val($('#moves_from_facility_selected option:selected').val());
    });
    //センター間プルダウン変更
    $('.center_move').change(function(){
        shipping_id  = $(this).attr('id');
        item_unit_id = $(this).val();

        $('#item_name'+shipping_id).html('<label title="'+item_data[shipping_id][item_unit_id].TrnMove.item_name+'">' + item_data[shipping_id][item_unit_id].TrnMove.item_name + '</label>');
        $('#item_code'+shipping_id).html('<label title="'+item_data[shipping_id][item_unit_id].TrnMove.item_code+'">' + item_data[shipping_id][item_unit_id].TrnMove.item_code + '</label>');
        $('#unit_name'+shipping_id).html('<label title="'+item_data[shipping_id][item_unit_id].TrnMove.unit_name+'">' + item_data[shipping_id][item_unit_id].TrnMove.unit_name + '</label>');
        $('#standard'+shipping_id).html('<label title="'+item_data[shipping_id][item_unit_id].TrnMove.standard+'">' + item_data[shipping_id][item_unit_id].TrnMove.standard + '</label>');
        $('#dealer_name'+shipping_id).html('<label title="'+item_data[shipping_id][item_unit_id].TrnMove.dealer_name+'">' + item_data[shipping_id][item_unit_id].TrnMove.dealer_name + '</label>');

    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>移動受領</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>移動受領</span></h2>
<h2 class="HeaddingMid">受領する明細を選択してください</h2>
<form class="validate_form search_form" id="search_item_moved_form" method="post">
    <?php echo $this->form->input('isSearch',array('type'=>'hidden','value'=>'1')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>移動元</th>
                <td colspan="4">
                    <?php echo $this->form->input('Search.from_facility_txt',array('id' => 'search_from_facility_txt', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('Search.from_facility',array('options'=>$movefrom_facilities,'class'=>'txt facility search_canna','style'=>'width:150px;','id' => 'moves_from_facility_selected','empty'=>true)); ?>
                <?php if(Configure::read('SplitTable.flag') == 1){ ?>
                <?php echo $this->form->input('Search.center_move',array('type'=>'checkbox','hiddenField'=>false)); ?>センター間移動を表示
                <?php } ?>
                </td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('Search.MstFacilityItem.internal_code',array('class'=>'txt search_internal_code')); ?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('Search.MstFacilityItem.item_code',array('class'=>'txt search_upper')); ?></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('Search.MstFacilityItem.item_name',array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('Search.MstDealer.dealer_name',array('class'=>'txt search_canna')); ?></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('Search.MstFacilityItem.standard',array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('Search.MstFacilityItem.jan_code',array('class'=>'txt')); ?></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" value="" class="btn btn1" id="search_item_moved_form_btn" /></p>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="25"/>
                    <col width="80"/>
                    <col />
                    <col />
                    <col width="180"/>
                    <col />
                    <col width="350"/>
                </colgroup>
                <tr>
                    <th rowspan="2"><input type="checkbox" class="" onClick="listAllCheck(this);" /></th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>数量</th>
                    <th>移動元</th>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>センターシール</th>
                    <th>受入単価</th>
                    <th>移動先</th>
                </tr>

                <?php $cnt=0; foreach($result as $r) { ?>
                <tr>
                    <td rowspan="2" class="center">
                        <input type="checkbox" class="chk" id="" name="data[TrnMove][id][]" value="<?php echo $r['TrnMove']['id']; ?>"/>
                    </td>
                    <td>
                        <?php if($r['TrnMove']['move_type'] == 3){ ?>
                        <?php
                            $internal_code = array();
                            foreach($move3_array[$r['TrnMove']['id']] as $m){
                                $internal_code[$m['TrnMove']['mst_item_unit_id']] = $m['TrnMove']['internal_code'];
                            }
                        ?>
                        <?php echo $this->form->input('TrnMove.mst_item_unit_id.'.$r['TrnMove']['id'] , array('options'=> $internal_code ,'class'=>'center_move', 'id'=>$r['TrnMove']['id']) ); ?>
                        <?php }else{?>
                        <?php echo h_out($r['TrnMove']['internal_code'],'center'); ?>
                        <?php } ?>
                    </td>
                    <td id="item_name<?php echo $r['TrnMove']['id']; ?>">
                        <?php echo h_out($r['TrnMove']['item_name']); ?>
                    </td>
                    <td id="item_code<?php echo $r['TrnMove']['id']; ?>">
                        <?php echo h_out($r['TrnMove']['item_code']); ?>
                    </td>
                    <td id="unit_name<?php echo $r['TrnMove']['id']; ?>">
                        <?php echo h_out($r['TrnMove']['unit_name']); ?>
                    </td>
                    <td>
                        <?php echo h_out($r['TrnMove']['quantity'],'right'); ?>
                    </td>
                    <td>
                        <?php echo h_out($r['TrnMove']['facility_name_from']."／".$r['TrnMove']['department_name_from']); ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td id="standard<?php echo $r['TrnMove']['id']; ?>">
                        <?php echo h_out($r['TrnMove']['standard']); ?>
                    </td>
                    <td id="dealer_name<?php echo $r['TrnMove']['id']; ?>">
                        <?php echo h_out($r['TrnMove']['dealer_name']); ?>
                    </td>
                    <td>
                        <?php echo h_out($r['TrnMove']['facility_sticker_no'],'center'); ?>
                    </td>
                    <td>
                        <?php echo h_out($this->Common->toCommaStr($r['TrnMove']['transaction_price']),'right'); ?>
                    </td>
                    <td>
                        <?php echo h_out($r['TrnMove']['facility_name_to']."／".$r['TrnMove']['department_name_to']); ?>
                    </td>
                </tr>
                <?php $cnt++; } ?>
                <?php if((isset($this->request->data['isSearch'])) && count($result)==0){ ?>
                <tr><td colspan="7" class="center">該当するデータがありませんでした</td></tr>
                <?php } ?>
            </table>
        </div>
    </div>
</form>
<?php if(count($result) > 0){ ?>
<div class="ButtonBox">
    <p class="center"><input type="button" value="" class="btn btn3" id="search_item_moved_selected_form_btn" /></p>
</div>
<?php } ?>

  