<script type="text/javascript">
$(document).ready(function(){
    //在庫選択ボタン押下
    $("#move2inner_form_btn").click(function(){
        //日付チェック
        var errorMsg = dateChecker($('#datepicker1').val());
        if(errorMsg != ""){
            alert(errorMsg);
            $("#datepicker1").focus();
            return false;
        }
        $("#move2inner_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2inner_search').submit();
    });

});

//日付形式チェック
function dateChecker(date){
    var msg = "";
    //YYYY/MM/DD形式かどうか
    if(!date.match(/^\d{4}\/\d{2}\/\d{2}$/)){
        msg = "日付はYYYY/MM/DD形式で入力してください";
        return msg;
    }

    //入力された日付の妥当性チェック
    var tempYear = date.substr(0, 4) - 0;
    var tempMonth = date.substr(5, 2) - 1;
    var tempDay = date.substr(8, 2) - 0;

    if(tempYear >= 1970 && tempMonth >= 0 && tempMonth <= 11 && tempDay >= 1 && tempDay <= 31){
        var checkDate = new Date(tempYear, tempMonth, tempDay);
        if(isNaN(checkDate)){
            msg = "入力された日付が不正です";
        }else if(checkDate.getFullYear() == tempYear && checkDate.getMonth() == tempMonth && checkDate.getDate() == tempDay){
            //
        }else{
            msg = "入力された日付が不正です";
        }
    }else{
        msg = "入力された日付が不正です";
    }
    return msg;
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>施設内移動</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>施設内移動</span></h2>
<h2 class="HeaddingMid">条件を指定してください</h2>
 
<?php echo $this->form->create( 'Moves',array('type'=>'post','action' =>'post','class'=>'validate_form input_form',"id"=>"move2inner_form") );?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
             </colgroup>
             <tr>
                 <th>移動日</th>
                 <td><?php echo $this->form->text('TrnMoveHeader.work_date',array('maxlength'=>10,'type'=>'text','class'=>'r validate[required,custom[date],funcCall2[date2]] date','id'=>'datepicker1',)); ?></td>
                 <td></td>
                 <th>作業区分</th>
                 <td>
                     <?php echo $this->form->input('TrnMoveHeader.work_class_txt',array('type'=>'hidden','id'=>'className')); ?>
                     <?php echo $this->form->input('TrnMoveHeader.work_class',array('options'=>$work_classes,'empty'=>true,'class'=>'txt','id'=>'classId')); ?>
                 </td>
             </tr>
             <tr>
                 <th>移動先施設</th>
                 <td>
                     <?php echo $this->form->input('TrnMoveHeader.facility_txt',array('id'=>'facilityText','class'=>'r facility_txt','style'=>'width: 60px;',"readonly"=>"readonly")); ?>
                     <?php echo $this->form->input('TrnMoveHeader.facility_code',array('options'=>$to_facilities,'empty'=>true,'class'=>'txt r validate[required] facility','style'=>'width:150px;','id'=>'facilityCode')); ?>
                     <?php echo $this->form->input('TrnMoveHeader.facility_name',array('type'=>'hidden' , 'id'=>'facilityName')); ?>
                 </td>
                 <td></td>
                 <th>備考</th>
                 <td>
                     <?php echo $this->form->input('TrnMoveHeader.recital',array('maxlength'=>50,'class'=>'txt')); ?>
                 </td>
             </tr>
             <tr>
                 <th>移動先部署</th>
                 <td>
                     <?php echo $this->form->input('TrnMoveHeader.department_txt',array('id'=>'departmentText','class' => 'r','style'=>'width: 60px;')); ?>
                     <?php echo $this->form->input('TrnMoveHeader.department_code',array('options'=>$department_list,'empty'=>true,'class'=>'txt r validate[required]','style'=>'width:150px;','id'=>'departmentCode')); ?>
                     <?php echo $this->form->input('TrnMoveHeader.department_name',array('type'=>'hidden','id'=>'departmentName')); ?>
                 </td>
             </tr>
         </table>
     </div>

     <div class="ButtonBox">
         <p class="center"><input type="button" class="btn btn15 submit" id="move2inner_form_btn" /></p>
     </div>
<?php echo $this->form->end(); ?>
