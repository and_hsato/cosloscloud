<!-- breadcrums (start) -->
<div id="TopicPath">
<ul>
  <li><a href="<?php echo $this->webroot; ?>login/home">TOP</a></li>
  <li><a href="<?php echo $this->webroot; ?>moves/receive_from_outside">管理外移入</a></li>
  <li class="pankuzu">管理外移入確認</li>
  <li>管理外移入結果</li>
</ul>
</div>
<!-- breadcrums (end) -->
<h2 class="HeaddingLarge"><span>管理外移入結果</span></h2>
<h2 class="HeaddingMid">結果を確認してください</h2>
<div style="color: red; font-size: 15px;">以下の商品を移動しました</div>

<form>
<div class="SearchBox">
<table class="FormStyleTable">
  <colgroup>
    <col />
    <col />
    <col width="20" />
    <col />
    <col />
  </colgroup>
  <tr>
    <th>移動番号</th>
    <td><?php echo $this->form->input('TrnMoveHeader.work_no',array('type'=>'text','class'=>'lbl','value'=>$this->request->data['TrnMoveHeader']['work_no'],'readonly'=>'readonly','readonly'=>'readonly')); ?></td>
    <td></td>
  </tr>
  <tr>
    <th>移動日</th>
    <td><?php echo $this->form->input('TrnMoveHeader.work_date',array('type'=>'text','class'=>'lbl','value'=>$this->request->data['TrnMoveHeader']['work_date'],'readonly'=>'readonly')); ?></td>
    <td></td>
    <th>作業区分</th>
    <td>
    <?php
      $_work_class = (isset($this->request->data['TrnMoveHeader']['work_class']) && $this->request->data['TrnMoveHeader']['work_class'] != '') ? $work_classes[$this->request->data['TrnMoveHeader']['work_class']]:'';
      echo $this->form->input('TrnMoveHeader.work_class',array('type'=>'text','class'=>'lbl','id'=>'work_class_txt','value'=>$_work_class,'readonly'=>'readonly'));
    ?>
    </td>

  </tr>
  <tr>
    <th><?php echo Configure::read('facility_subcode'); ?></th>
    <td><?php echo $this->form->input('TrnMoveHeader.assist_code',array('class'=>'lbl','readonly'=>'readonly')); ?></td>
    <td></td>
    <th>備考</th>
    <td><?php echo $this->form->input('TrnMoveHeader.recital',array('maxlength'=>50,'class'=>'lbl','readonly'=>'readonly')); ?></td>
  </tr>
</table>
</div>
<br>
<div class="TableHeaderAdjustment01">
<table class="TableHeaderStyle01">
  <tr>
    <th class="col10">商品ID</th>
    <th class="col15">商品名</th>
    <th class="col15">規格</th>
    <th class="col15">製品番号</th>
    <th class="col15">販売元</th>
    <th class="col10">包装単位</th>
    <th class="col10">数量</th>
    <th class="col10">仕入単価</th>
  </tr>
</table>
</div>
<div class="TableScroll">
<table class="TableStyle02">
<?php $i = 0; foreach ($result as $r): ?>
  <tr class="<?php echo ($i%2 ===1)? 'odd':'';?>">
  <?php echo $this->form->input('Receive.'.$i.'.MstFacilityItem.id', array('type'=>'hidden','value'=>$r['MstFacilityItem']['id'])); ?>
    <td style="width: 20px;"></td>
    <td class="col10">
      <label title="<?php echo $r['MstFacilityItem']['internal_code']; ?>">
        <?php echo $r['MstFacilityItem']['internal_code']; ?>
      </label>
    </td>
    <td class="col15">
      <label title="<?php echo $r['MstFacilityItem']['item_name']; ?>">
        <?php echo $r['MstFacilityItem']['item_name']; ?>
      </label>
    </td>
    <td class="col15">
      <label title="<?php echo $r['MstFacilityItem']['standard']; ?>">
        <?php echo $r['MstFacilityItem']['standard']; ?>
      </label>
    </td>
    <td class="col15">
      <label title="<?php echo $r['MstFacilityItem']['item_code']; ?>">
        <?php echo $r['MstFacilityItem']['item_code']; ?>
      </label>
    </td>
    <td class="col15">
      <label title="<?php echo $r['MstDealer']['dealer_name']; ?>">
        <?php echo $r['MstDealer']['dealer_name']; ?>
      </label>
    </td>
    <td class="col10">
      <label title="<?php echo $r['MstUnitName']['unit_name']."(".$r['MstItemUnit']['per_unit'].$r['MstPerUnitName']['unit_name'].")"; ?>">
        <?php echo $r['MstUnitName']['unit_name']."(".$r['MstItemUnit']['per_unit'].$r['MstPerUnitName']['unit_name'].")"; ?>
      </label>
    </td>
    <td class="col10">
      <?php echo $this->form->text("Receive.Quantity.{$i}", array('class'=>'lbl','style'=>'width:80px; text-align:right;',"value"=>$this->request->data['Receive']['Quantity'][$i],"readonly"=>"readonly")); ?>
    </td>
    <td class="col10">
      <?php echo $this->form->text("Receive.Price.{$i}", array('class'=>'lbl','style'=>'width:80px; text-align:right;',"value"=>$this->request->data['Receive']['Price'][$i],"readonly"=>"readonly")); ?>
    </td>
  </tr>
<?php $i++; endforeach;?>
</table>
</div>
</form>
