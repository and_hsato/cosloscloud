<script type="text/javascript">
/**
 * page transfer for moves action.
 */
$(function(){
    $("#search_item_form_btn").click(function(){
        // カート内にあるすべての商品のIDを取得
        var tempId = "";
        $("#moves_cart_form input[type=checkbox].chk").each(function(){
            tempId += $(this).val() + ",";
        });
        $("#cart_ids").val(tempId);
    
        $("#search_item_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/select_item/<?php echo $page; ?>');
        $("#search_item_form").attr('method', 'post');
        $("#search_item_form").submit();
    });
    $("#moves_cart_form_btn").click(function(){
        if( $("#itemSelectedTable > table").length == 0 ) {
            alert( "少なくとも1つの商品にチェックをつけてください");
            return false;
        }
        $("#moves_cart_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/<?php echo $nextPage; ?>');
        $("#moves_cart_form").attr('method', 'post');
        $("#moves_cart_form").submit();
     });
});

/**
 * Set department's display name, selecting department from selectbox.
 */
$(function () {
  $('#search_depts').change(function () {
    $('#depts_txt').val($('#search_depts option:selected').val());
  }); 
});

</script>
<div id="TopicPath">
<ul>
  <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
  <?php if( $page == Configure::read('MoveType.move2inner.reserved')): ?>
  <li><a href="<?php echo $this->webroot; ?>moves/move2inner">施設内移動</a></li>
  <?php elseif( $page == Configure::read('MoveType.move2outer.reserved') ): ?>
  <li><a href="<?php echo $this->webroot; ?>moves/move2outer">施設外移動</a></li>  
  <?php elseif( $page == Configure::read('MoveType.move2other.reserved') ): ?>
  <li><a href="<?php echo $this->webroot; ?>moves/move2other">センター間移動</a></li>  
  <?php elseif( $page == Configure::read('MoveType.move2outside') ): ?>
  <li><a href="<?php echo $this->webroot; ?>moves/move2outside">管理外移出</a></li>  
  <?php endif; ?>
  <li>在庫選択</li>
</ul>
</div>
<h2 class="HeaddingLarge"><span>在庫選択</span></h2>
<h2 class="HeaddingMid">データを検索してください</h2>
<form id="search_item_form" class="search_form">
<?php echo $this->form->input('isSearch',array('type'=>'hidden','value'=>'1')); ?>
<?php echo $this->element('moves' . DS . 'move_header' , array('input' => false , 'page' => $page)); ?>

<h2 class="HeaddingLarge2">検索条件</h2>
<div class="SearchBox">
<table class="FormStyleTable">
  <colgroup>
    <col />
    <col />
    <col c/>
    <col />
    <col />
    <col width="20" />
    <col />
    <col />
  </colgroup>
  <tr>
    <th>在庫部署</th>
    <td>
    <?php
    //debug($department_selects);
      $_key = array_keys($department_selects);
      $_dep_code = (count($department_selects) > 1) ? '':$_key[0]; 
      echo $this->form->input('TrnMoveHeader.department_code_txt',array('id'=>'depts_txt', 'class'=>'txt','style'=>'width:60px;','value'=>$_dep_code));
    ?>
    <?php
      $_hasEmptyDep = (count($department_selects) > 1) ? true:false; 
      echo $this->form->input('TrnMoveHeader.department_code',array('options'=>$department_selects,'value'=>true,'id'=>'search_depts','class'=>'txt','style'=>'width:120px;','empty'=>$_hasEmptyDep)); 
    ?>
    </td>
    <td></td>
  </tr>
  <tr>
    <th>商品ID</th>
    <td><?php echo $this->form->input('MstFacilityItem.internal_code',array('class'=>'txt')); ?></td>
    <td></td>
    <th>製品番号</th>
    <td><?php echo $this->form->input('MstFacilityItem.item_code',array('class'=>'txt search_upper')); ?></td>
    <td></td>
    <th>ロット番号</th>
    <td><?php echo $this->form->input('TrnSticker.lot_no',array('class'=>'txt')); ?></td>
  </tr>
  <tr>
    <th>商品名</th>
    <td><?php echo $this->form->input('MstFacilityItem.item_name',array('type'=>'text','class'=>'txt search_canna','size'=>10)); ?></td>
    <td></td>
    <th>販売元</th>
    <td><?php echo $this->form->input('MstDealer.dealer_name',array('class'=>'txt search_canna')); ?></td>
  </tr>
  <tr>
    <th>規格</th>
    <td><?php echo $this->form->input('MstFacilityItem.standard',array('type'=>'text','class'=>'txt search_canna','size'=>10)); ?></td>
    <td></td>
    <th>シール番号</th>
    <td><?php echo $this->form->input('TrnSticker.facility_sticker_no',array('type'=>'text','class'=>'txt','size'=>10)); ?></td>
  </tr>
</table>
</div>
<div class="ButtonBox">
<p class="center"><input type="button" value="" class="btn btn1" id="search_item_form_btn" /></p>
</div>

<?php if(isset($this->request->data['isSearch']) && $this->request->data['isSearch'] == '1'):?>
<?php echo $this->element('limit_combobox',array('result'=>(isset($stickers) ? count($stickers) : 0))); ?>
<?php endif; ?>
  <?php echo $this->form->input('cart_ids',array('type'=>'hidden','id' =>'cart_ids'));?>
</form>
<?php if(isset($this->request->data['isSearch']) && $this->request->data['isSearch'] == '1'):?>
<?php if(isset($stickers) && count($stickers) !== 0): ?>
<?php echo $this->element('moves' . DS . 'movetable' , array('results' => $stickers , 'view_type' => 'default' , 'selected_stickers'=>$selected_stickers)); ?>

<div class="ButtonBox">
</div>
<h2 class="HeaddingLarge2">選択商品</h2>
<div class="TableHeaderAdjustment01">
<table class="TableHeaderStyle01">
  <tr>
    <th style="width: 20px;" rowspan="2">&nbsp;</th>
    <th class="col10">商品ID</th>
    <th class="col20">商品名</th>
    <th class="col15">製品番号</th>
    <th class="col10">包装単位</th>
    <th class="col10">ロット番号</th>
    <th class="col15">センターシール</th>
    <th class="col15">施設</th>
  </tr>
  <tr>
    <th></th>
    <th>規格</th>
    <th>販売元</th>
    <th>数量</th>
    <th>有効期限</th>
    <th>部署シール</th>
    <th>部署</th>
  </tr>
</table>
</div>
<form id="moves_cart_form">
<div class="TableScroll">
<div class="TableStyle02 itemSelectedTable" id="itemSelectedTable">

<?php $i = 0; foreach ($selected_stickers as $s): ?>
<table class="TableStyle02" id="cloneRow<?php echo $s['TrnSticker']['id'];?>" style="margin:0;padding:0;">
  <tr class="<?php echo ($i%2 ===1)? 'odd':'';?>">
    <td class="col5" style="width: 20px;text-align:center;" rowspan="2">
      <?php echo $this->form->input('Moves.'.$i.'.TrnSticker.id', array('type'=>'hidden','value'=>$s['TrnSticker']['id'])); ?>
      <?php echo $this->form->input('Moves.'.$i.'.checked',array('type'=>'checkbox','class'=>'center chk','id'=>'editable-checkbox'.$i,'disabled'=>true,'value'=>$s['TrnSticker']['id'])); ?>
    </td>
    <td class="col10">
    <?php echo h( $s['MstFacilityItem']['internal_code']); ?>
    </td>
    <td class="col20">
    <?php echo h( $s['MstFacilityItem']['item_name']); ?>
    </td>
    <td class="col15">
    <?php echo h( $s['MstFacilityItem']['item_code']); ?>
    </td>
    <td class="col10">
    <?php echo h( $s['MstUnitName']['unit_name']); ?><?php if( isset($s['MstItemUnit']['per_unit']) && $s['MstItemUnit']['per_unit'] > 1 ): ?>（<?php echo $s['MstItemUnit']['per_unit']; ?><?php echo isset($s['MstParUnitName']['unit_name']) ? h( $s['MstParUnitName']['unit_name'] ) : ''; ?>）<?php endif; ?>
    </td>
    <td class="col10">
    <?php echo $s['TrnSticker']['lot_no']; ?>
    </td>
    <td class="col15">
    <?php echo $s['TrnSticker']['facility_sticker_no']; ?>
    </td>
    <td class="col15">
    <?php echo h( $s['MstFacility']['facility_name']); ?>
    </td>
  </tr>
  <tr class="<?php echo ($i%2 ===1)? 'odd':'';?>">  
    <td>
    <?php echo ($s['TrnSticker']['has_reserved'] ? '移動予約済':'') ;?>    <?php echo $this->form->input('Moves.'.$i.'.has_reserved', array('type'=>'hidden','value'=>$s['TrnSticker']['has_reserved'])); ?>
    </td>
    <td>
    <?php echo h( $s['MstFacilityItem']['standard']); ?>
    </td>
    <td>
    <?php echo isset($s['MstDealer']['dealer_name']) ? h( $s['MstDealer']['dealer_name']) : ''; ?>
    </td>
    <td>
    <?php echo h( $s['TrnSticker']['quantity']); ?>
    </td>
    <td>
    <?php echo $s['TrnSticker']['validated_date']; ?>
    </td>
    <td>
    <?php echo $s['TrnSticker']['hospital_sticker_no']; ?>
    </td>
    <td>
    <?php echo h( $s['MstDepartment']['department_name']); ?>
    </td>
  </tr>
</table>
<?php $i++; endforeach; ?>
</div>
</div>
<div class="ButtonBox">
<input type="button" class="submit" value="" class="btn btn3" id="moves_cart_form_btn" />
</div>
<?php echo $this->form->input('TrnMoveHeader.work_date',array('type' => 'hidden','value'=>$this->request->data['TrnMoveHeader']['work_date'])); ?>
<?php echo $this->form->input('TrnMoveHeader.work_class',array('type' => 'hidden','value'=>$this->request->data['TrnMoveHeader']['work_class'])); ?>
<?php echo $this->form->input('TrnMoveHeader.work_class_txt',array('type' => 'hidden')); ?>
<?php echo $this->form->input('TrnMoveHeader.user_facility_name', array('type'=>'hidden' , 'id' => 'user_facility_name', 'label' => false, 'div' => false, 'class' => 'r', 'style' => 'width: 60px;')); ?>
<?php echo $this->form->input('TrnMoveHeader.user_facility_txt',array('type' =>'hidden')); ?>
<?php echo $this->form->input('TrnMoveHeader.user_facility', array('type'=>'hidden' , 'id' => 'user_facility', 'label' => false, 'div' => false, 'class' => 'r', 'style' => 'width: 60px;')); ?>
<?php echo $this->form->input('TrnMoveHeader.recital',array('type'=>'hidden')); ?>
<?php echo $this->form->input('TrnMoveHeader.user_department',array('type'=>'hidden')); ?>
<?php echo $this->form->input('TrnMoveHeader.user_department_name',array('type'=>'hidden')); ?>
<?php echo $this->form->input('TrnMoveHeader.asist_department',array('type'=>'hidden')); ?>
<?php echo $this->form->input('TrnMoveHeader.facility_from',array('type'=>'hidden')); ?>
<?php echo $this->form->input('TrnMoveHeader.facility_from_name',array('type'=>'hidden')); ?>
<?php echo $this->form->input('TrnMoveHeader.facility_from_txt',array('type'=>'hidden')); ?>
</form>
<?php endif; ?>

<?php endif; ?>