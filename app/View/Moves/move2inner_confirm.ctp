<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('#confirmForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    //確定ボタン押下時の処理
    $('#btn_regist').click(function(){
        if( $("#confirmForm input[type=checkbox].checkAllTarget:checked").length == 0) { 
            alert('明細を選択してください');
            return;
        }else{
            $("#confirmForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2inner_result').submit();
        }
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/move2inner">施設内移動</a></li>
        <li class='pankuzu'>在庫選択</li>
        <li>施設内移動確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>施設内移動確認</span></h2>
<h2 class="HeaddingMid">移動内容を確認してください</h2>
<?php echo $this->form->create('Move',array('type'=>'post','action' =>'','id'=>'confirmForm','class'=>'validate_form') ); ?>
<?php echo $this->form->input('TrnMoveHeader.token',array('type'=>'hidden'));?>
<?php echo $this->form->input('TrnMoveHeader.time',array('type'=>'hidden',"value"=>date('Y/m/d H:i:s.u')));?>
<div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
         </colgroup>
         <tr>
             <th>移動日</th>
             <td>
                 <?php echo $this->form->text('TrnMoveHeader.work_date',array('class'=>'lbl','readonly'=>'readonly')); ?>
             </td>
             <td></td>
             <th>作業区分</th>
             <td>
                 <?php echo $this->form->hidden('TrnMoveHeader.work_class'); ?>
                 <?php echo $this->form->text('TrnMoveHeader.work_class_txt',array('class'=>'lbl','readonly'=>'readonly')); ?>
             </td>
         </tr>
         <tr>
             <th>移動先施設</th>
             <td>
                 <?php echo $this->form->hidden('TrnMoveHeader.facility_code'); ?>
                 <?php echo $this->form->text('TrnMoveHeader.facility_name',array('class'=>'lbl','readonly'=>'readonly')); ?>
             </td>
             <td></td>
             <th>備考</th>
             <td>
                 <?php echo $this->form->text('TrnMoveHeader.recital',array('maxlength'=>50,'class'=>'lbl')); ?>
             </td>
         </tr>
         <tr>
             <th>移動先部署</th>
             <td>
                 <?php echo $this->form->hidden('TrnMoveHeader.department_code'); ?>
                 <?php echo $this->form->text('TrnMoveHeader.department_name',array('class'=>'lbl','readonly'=>'readonly')); ?>
             </td>
        </tr>
    </table>
</div>

<div class="TableScroll2">
    <table class="TableStyle01 table-odd2">
        <tr>
            <th style="width:20px;" rowspan="2"><input type="checkbox" class="checkAll" CHECKED></th>
            <th class="col10">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th class="col5">包装単位</th>
            <th class="col10">ロット番号</th>
            <th class="col15">センターシール</th>
            <th class="col15">施設</th>
            <th class="col10">作業区分</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>数量</th>
            <th>有効期限</th>
            <th>部署シール</th>
            <th>部署</th>
            <th>備考</th>
        </tr>
        <?php $i = 0; foreach ($result as $row): ?>
        <tr>
            <td rowspan="2" class="center">
                <?php echo $this->form->checkbox("Moves.stickerId.{$i}", array('class'=>'center checkAllTarget','value'=>$row['TrnMove']['id'],'checked'=>true) ); ?>
            </td>
            <td><?php echo h_out($row['TrnMove']['internal_code'] , 'center'); ?></td>
            <td><?php echo h_out($row['TrnMove']['item_name']); ?></td>
            <td><?php echo h_out($row['TrnMove']['item_code']); ?></td>
            <td><?php echo h_out($row['TrnMove']['unit_name']); ?></td>
            <td><?php echo h_out($row['TrnMove']['lot_no']); ?></td>
            <td><?php echo h_out($row['TrnMove']['facility_sticker_no']); ?></td>
            <td><?php echo h_out($row['TrnMove']['facility_name']); ?></td>
            <td>
                <?php echo $this->form->input("Moves.work_class.{$row['TrnMove']['id']}",array('options'=>$work_classes,'class'=>'txt','empty'=>true)); ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo h_out($row['TrnMove']['standard']); ?></td>
            <td><?php echo h_out($row['TrnMove']['dealer_name']); ?></td>
            <td>
                <?php echo $this->form->text("Moves.quantity.{$row['TrnMove']['id']}", array('class'=>'txt','style'=>'width:85px; text-align:right;','readonly'=>'readonly','value'=>1)); ?>
            </td>
            <td><?php echo h_out($row['TrnMove']['validated_date']); ?></td>
            <td><?php echo h_out($row['TrnMove']['hospital_sticker_no']); ?></td>
            <td><?php echo h_out($row['TrnMove']['department_name']); ?></td>
            <td>
                <?php echo $this->form->text("Moves.recital.{$row['TrnMove']['id']}", array('class'=>'txt','style'=>'width:85px;'));?>
            </td>
        </tr>
        <?php $i++; endforeach; ?>
    </table>
</div>

<div class="ButtonBox" style="margin-top:5px;">
    <p class="center"><input type="button" class="btn btn2" id="btn_regist" /></p>
</div>
<?php echo $this->form->end(); ?>
