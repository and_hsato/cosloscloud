<script type="text/javascript">

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#MoveForm #searchForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#MoveForm #cartForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });

    //検索ボタン押下
    $("#btn_Search").click(function(){
        var tmpid = '';
        $("#MoveForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);

        $("#MoveForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2other_search').submit();
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        $('#itemSelectedTable').empty();
        $("#MoveForm #searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
                cnt++;
            }
        });
        if(cnt === 0 ){
            alert('商品を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        //選択チェック
        var cnt = 0;
        var tmpid = '';
        $("#MoveForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);
        if(cnt === 0){
            alert('商品を選択してください');
        }else{
            $("#MoveForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/move2other_confirm').submit();
        }
    });

});

function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot;?><?php echo $this->name; ?>/move2other">センター間移動</a></li>
        <li>在庫検索</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>在庫検索</span></h2>
<h2 class="HeaddingMid">直納品登録を行う商品を選択してください</h2>
<?php echo $this->form->create( 'Move2Other',array('type' => 'post','action' => '' ,'id' =>'MoveForm','class' => 'validate_form search_form') ); ?>
    <?php echo $this->form->input('add_search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('cart.tmpid',array('type'=>'hidden','id' =>'tmpid'));?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>移動日</th>
                <td>
                    <?php echo $this->form->input('Move2Other.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly')); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('Move2Other.work_type_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly')); ?>
                    <?php echo $this->form->input('Move2Other.work_type' , array('type'=>'hidden')); ?>
                </td>
            </tr>
            <tr>
                <th>移動先施設</th>
                <td>
                    <?php echo $this->form->input('Move2Other.facility_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly')); ?>
                    <?php echo $this->form->input('Move2Other.facility_input' , array('type'=>'hidden')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Move2Other.recital' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly')); ?>
                </td>
            </tr>
        </table>
    </div>

    <h2 class="HeaddingSmall">検索条件</h2>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('Move2Other.internal_code', array('class'=>'txt search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('Move2Other.item_code', array('class'=>'txt search_upper')); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->input('Move2Other.lot_no', array('class'=>'txt')); ?></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('Move2Other.item_name', array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('Move2Other.dealer_name', array('class'=>'txt search_canna')); ?></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('Move2Other.standard', array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->input('Move2Other.sticker_no', array('class'=>'txt search_canna')); ?></td>
            </tr>
        </table>
    </div>

        <div class="ButtonBox">
           <p class="center">
             <input type="button" class="btn btn1" id="btn_Search"/>
           </p>
        </div>

    <div align="right" id="pageTop" ></div>


    <div id="results">
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle01">
            <tr>
                <th width="20" rowspan="2" class="center"><input type="checkbox" class="checkAll_1" /></th>
                <th width="95">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th width="150">包装単位</th>
                <th width="95">ロット番号</th>
                <th width="100">仕入単価</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>センターシール</th>
                <th>有効期限</th>
                <th>評価単価</th>
            </tr>
        </table>
      </div>

<?php
    if(isset($this->request->data['add_search']['is_search'])){
      if(count($SearchResult) === 0){
?>
      <span>該当するデータはありません。</span>
<?php
      }else{
?>

      <div class="TableScroll" style="" id="searchForm">
        <?php $_cnt=0;foreach($SearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row[0]['trn_sticker_id'];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <colgroup>
            <col width="20" />
            <col width="95" />
            <col />
            <col />
            <col width="150" />
            <col width="95"/>
            <col width="100"/>
          </colgroup>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" >
            <td class="center" rowspan="2">
              <?php
                  if($_row[0]['check']==true){
                      echo $this->form->checkbox("SelectedID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row[0]['trn_sticker_id'] ) );
                  }
              ?>
            </td>
            <td class="center"><?php echo h_out($_row[0]['internal_code'], 'center'); ?></td>
            <td><?php echo h_out($_row[0]['item_name']); ?></td>
            <td><?php echo h_out($_row[0]['item_code']); ?></td>
            <td><?php echo h_out($_row[0]['packing_name']); ?></td>
            <td><?php echo h_out($_row[0]['lot_no']); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($_row[0]['transaction_price']));?></td>
          </tr>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>">
            <td></td>
            <td><?php echo h_out($_row[0]['standard']);?></td>
            <td><?php echo h_out($_row[0]['dealer_name']); ?></td>
            <td><?php echo h_out($_row[0]['facility_sticker_no']); ?></td>
            <td><?php echo h_out($_row[0]['validated_date']); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($_row[0]['price'])); ?></td>
          </tr>
        </table>
        <?php $_cnt++;endforeach;?>
      </div>

      <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
          <input type="button" value="" class="btn btn4" id="cmdSelect" />
        </p>
      </div>
    </div>
<?php
      }
?>

    <?php if(isset($this->request->data['add_search']['is_search'])){?>
    <h2 class="HeaddingSmall">選択商品</h2>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle01">
            <tr>
                <th style="width:20px;" rowspan="2" class="center"><input type="checkbox" class="checkAll_2" checked /></th>
                <th width="95">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th width="150">包装単位</th>
                <th width="95">ロット番号</th>
                <th width="100">仕入単価</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>センターシール</th>
                <th>有効期限</th>
                <th>評価単価</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll" style="" id="cartForm">
      <?php if(isset($CartSearchResult)){ ?>
      <?php $_cnt=0;foreach($CartSearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row[0]['trn_sticker_id'];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <colgroup>
            <col style="width:20px;" />
            <col width="95" />
            <col />
            <col />
            <col width="150" />
            <col width="95"/>
            <col width="100"/>
          </colgroup>

          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" >
            <td class="center" rowspan="2" style="width:20px;">
              <?php echo $this->form->checkbox("SelectedID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row[0]['trn_sticker_id'] ,'checked'=>true) ); ?>
            </td>
            <td class="center"><?php echo h_out($_row[0]['internal_code']); ?></td>
            <td><?php echo h_out($_row[0]['item_name']); ?></td>
            <td><?php echo h_out($_row[0]['item_code']);?></td>
            <td><?php echo h_out($_row[0]['packing_name']);?></td>
            <td><?php echo h_out($_row[0]['lot_no']);?></td>
            <td><?php echo h_out($this->Common->toCommaStr($_row[0]['transaction_price']))?></td>
          </tr>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>">
            <td></td>
            <td><?php echo h_out($_row[0]['standard']);?></td>
            <td><?php echo h_out($_row[0]['dealer_name']); ?></td>
            <td><?php echo h_out($_row[0]['facility_sticker_no']); ?></td>
            <td><?php echo h_out($_row[0]['validated_date']); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($_row[0]['price'])); ?></td>
          </tr>

        </table>
      <?php $_cnt++;endforeach;?>
      <?php }?>
      <div id="itemSelectedTable"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" class="btn btn3"/>
      </p>
    </div>
    <?php }?>
<?php
    }
    echo $this->form->end();

?>
<div align="right" id="pageDow" ></div>
