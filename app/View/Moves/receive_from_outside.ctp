<script type="text/javascript">

$(function(){
    $('.checkAll_1').click(function(){
        $('#searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#cartForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
  
    //検索ボタン押下
    $("#search_btn").click(function(){
        var tempId = '';
        $("#cartForm input[type=checkbox].chk").each(function(){
            if($(this).attr("checked") == true){
                //hidden項目にID追加
                if(tempId==''){
                    tempId = $(this).val();
                }else{
                    tempId += "," + $(this).val();
                }
            }
        });
        $("#tempId").val(tempId);

        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/receive_from_outside').submit();
    });

    //確認ボタン押下
    $("#confirm_btn").click(function(){
        var selectId = ''
        if($("#cartForm input[type=checkbox].chk").length > 0) {
            $("#cartForm input[type=checkbox].chk").each(function(){
                if($(this).attr("checked") == true){
                    if(selectId==''){
                        selectId = $(this).val();
                    }else{
                        selectId += "," + $(this).val();
                    }
                }
            });
            $("#selectId").val(selectId);
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/receive_from_outside_confirm').submit();
        }else{
            alert("商品が選択されていません");
        }
    });

    //選択ボタン押下
    $("#cmdSelect").click(function(){
        var tempId = $("#tempId").val();

        $("#searchForm input[type=checkbox].chk").each(function(){
            if($(this).attr("checked") == true){
                //全選択用にチェックボックスのクラス変更
                //選択行を追加
                if($('#addTable tr').length%2 === 0){
                    $(this).parents('tr:eq(0)').removeClass('odd').appendTo('#addTable');
                }else{
                    $(this).parents('tr:eq(0)').removeClass('odd').addClass('odd').appendTo('#addTable');
                }
                $('#searchTable').find('tr').removeClass('odd').end().find('tr:odd').addClass('odd');

                //hidden項目にID追加
                if(tempId==''){
                    tempId = $(this).val();
                }else{
                    tempId += "," + $(this).val();
                }
            }
        });

        $("#tempId").val(tempId);
    });
});
</script>


<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>管理外移入</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>管理外移入</span></h2>
<h2 class="HeaddingMid">商品を選択してください。</h2>

<form class="validate_form search_form input_form" id="searchForm" method="post" action="receive_from_outside">
    <?php echo $this->form->input('select.tempId',array('type'=>'hidden','id' =>'tempId')); ?>
    <?php echo $this->form->input('select.Id',array('type'=>'hidden','id' =>'selectId')); ?>

    <?php echo $this->form->input('search.is_search',array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <h2 class="HeaddingLarge2">検索条件</h2>
        <table class="FormStyleTable">
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code', array('type'=>'text', 'class'=>'txt search_upper search_internal_code')); ?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_code', array('type'=>'text', 'class'=>'txt search_upper')); ?></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_name', array('type'=>'text','class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFacilityItem.dealer_name', array('type'=>'text','class'=>'txt search_canna')); ?></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFacilityItem.standard', array('type'=>'text', 'class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('MstFacilityItem.jan_code', array('type'=>'text','class'=>'txt')); ?></td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center"><input type="button" class="btn btn1" id="search_btn"/></p>
        </div>

        <div id="results">
            <hr class="Clear" />
            <div class="DisplaySelect">
                <div align='right' style='margin-right:25px; float:right;'>
                </div>
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
                <span class="BikouCopy"></span>
            </div>
            <div class="TableScroll">
                <div class="TableHeaderAdjustment01">
                    <table class="TableHeaderStyle01">
                        <tr>
                            <th style="width:20px;"><input type="checkbox" class="checkAll_1" /></th>
                            <th class="col10">商品ID</th>
                            <th>商品名</th>
                            <th>規格</th>
                            <th>製品番号</th>
                            <th>販売元</th>
                            <th>包装単位</th>
                        </tr>
                    </table>
                </div>
                <table class="TableStyle01" id="searchTable">
                    <colgroup>
                        <col>
                        <col align="center">
                        <col>
                        <col>
                        <col>
                        <col>
                    </colgroup>
                    <?php $i = 0; foreach ($result as $r): ?>
                        <tr class="<?php echo ($i%2 == 0)?'':'odd';?>" id="<?php echo $i; ?>">
                            <td style="width:20px;" class="center"><?php echo $this->form->checkbox('TrnMove.id.'.$i, array('class'=>'center chk', 'value'=>$r['TrnMove']['id'])); ?></td>
                            <td class="col10"><?php echo h_out($r['TrnMove']['internal_code'],'center'); ?></td>
                            <td><?php echo h_out($r['TrnMove']['item_name']); ?></td>
                            <td><?php echo h_out($r['TrnMove']['standard']); ?></td>
                            <td><?php echo h_out($r['TrnMove']['item_code']); ?></td>
                            <td><?php echo h_out($r['TrnMove']['dealer_name']); ?></td>
                            <td><?php echo h_out($r['TrnMove']['unit_name']); ?></td>
                        </tr>
                    <?php $i++; endforeach; ?>
                    <input type='hidden' id='cnt' value='<?php print $i; ?>'>
                </table>
            </div>
            <div class="ButtonBox">
                <p class="center"><input type="button" class="btn btn4" id="cmdSelect" /></p>
            </div>
            <div id="cartForm">

                <h2 class="HeaddingSmall">選択商品</h2>

                <div class="TableScroll">
                    <div class="TableHeaderAdjustment01">
                        <table class="TableHeaderStyle01">
                            <tr>
                                <th style="width:20px;"><input type="checkbox" class="checkAll_2" checked /></th>
                                <th class="col10">商品ID</th>
                                <th>商品名</th>
                                <th>規格</th>
                                <th>製品番号</th>
                                <th>販売元</th>
                                <th>包装単位</th>
                            </tr>
                        </table>
                    </div>
                    <div class="TableStyle01 itemSelectedTable" id="itemSelectedTable">
                        <table class="TableStyle01" id="addTable" style="margin:0;padding:0;">
                            <colgroup>
                                <col>
                                <col align="center">
                                <col>
                                <col>
                                <col>
                                <col>
                            </colgroup>
                        
                            <?php $i = 0; foreach ($cartResult as $c): ?>
                            <tr class="<?php echo ($i%2 == 0)?'':'odd';?>" id="<?php echo $i; ?>">
                                <td style="width:20px;" class="center">
                                    <?php echo $this->form->checkbox('TrnMove.id.'.$i, array('class'=>'center chk', 'value'=>$c['TrnMove']['id'], 'checked'=>true)); ?>
                                </td>
                                <td class="col10"><?php echo h_out($c['TrnMove']['internal_code'],'center'); ?></td>
                                <td><?php echo h_out($c['TrnMove']['item_name']); ?></td>
                                <td><?php echo h_out($c['TrnMove']['standard']); ?></td>
                                <td><?php echo h_out($c['TrnMove']['item_code']); ?></td>
                                <td><?php echo h_out($c['TrnMove']['dealer_name']); ?></td>
                                <td><?php echo h_out($c['TrnMove']['unit_name']); ?></td>
                            </tr>
                            <?php $i++; endforeach; ?>
                        </table>
                    </div>
                </div>

                <div class="ButtonBox">
                    <input type="button" id="confirm_btn" class="btn btn3 confirm_btn">
                </div>
                <div align='right' style='margin-right:25px; margin-top:10px;'>
                </div>
            </div>
        </div>
    </div>
<h3 id='foot'></h3>
<?php echo $this->form->end(); ?>
