<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/move2outer">施設外移動</a></li>
        <li class="pankuzu">在庫選択</li>
        <li class="pankuzu">施設外移動確認</li>
        <li>施設外移動結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>施設外移動結果</span></h2>
<h2 class="HeaddingMid">結果を確認してください</h2>
<div class="Mes01">以下の商品の移動予約をしました</div>
<div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
        </colgroup>
        <tr>
            <th>移動日</th>
            <td>
                <?php echo $this->form->input('TrnMoveHeader.work_date',array('type'=>'text','class'=>'lbl','readonly'=>'readonly')); ?>
            </td>
            <td></td>
            <th>作業区分</th>
            <td>
                <?php echo $this->form->input('TrnMoveHeader.work_class',array('type'=>'hidden')); ?>
                <?php echo $this->form->input('TrnMoveHeader.work_class_txt',array('type'>'text','class'=>'lbl','readonly'=>'readonly')); ?>
            </td>
        </tr>
        <tr>
            <th>移動元施設</th>
            <td>
                <?php echo $this->form->input('TrnMoveHeader.facility_code',array('type'=>'hidden')); ?>
                <?php echo $this->form->input('TrnMoveHeader.facility_name',array('type'=>'text','class'=>'lbl','readonly'=>'readonly')); ?>
            </td>
            <td></td>
            <th>備考</th>
            <td>
                <?php echo $this->form->input('TrnMoveHeader.recital',array('type'=>'text','maxlength'=>50,'class'=>'lbl','readonly'=>'readonly')); ?>
            </td>
        </tr>
    </table>
</div>
<?php echo $this->form->create( 'Move',array('type'=>'post','action' =>'','id'=>'resultForm','class'=>'validate_form') ); ?>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th style="width:20px;" rowspan="2"></th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col10">センターシール</th>
                <th class="col15">施設</th>
                <th class="col10">作業区分</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>数量</th>
                <th>有効期限</th>
                <th>部署シール</th>
                <th>部署</th>
                <th>備考</th>
            </tr>
            <?php $i = 0; foreach ($resultView as $row): ?>
            <tr>
                <td rowspan="2">&nbsp;</td>
                <td><?php echo h_out( $row['TrnMove']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($row['TrnMove']['item_name']); ?></td>
                <td><?php echo h_out( $row['TrnMove']['item_code']); ?></td>
                <td><?php echo h_out($row['TrnMove']['unit_name']); ?></td>
                <td><?php echo h_out($row['TrnMove']['lot_no']); ?></td>
                <td><?php echo h_out($row['TrnMove']['facility_sticker_no']); ?></td>
                <td><?php echo h_out($row['TrnMove']['facility_name']); ?></td>
                <td>
                    <?php echo $this->form->input("Moves.work_class.{$row['TrnMove']['id']}",array('options'=>$work_classes,'empty'=>true,'class'=>'txt')); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($row['TrnMove']['standard']); ?></td>
                <td><?php echo h_out($row['TrnMove']['dealer_name']); ?></td>
                <td>
                    <?php echo $this->form->text("Moves.quantity.{$row['TrnMove']['id']}",array('class'=>'lbl','style'=>'width:85px; text-align:right;','readonly'=>'readonly',"value"=>$this->request->data['Moves']['quantity'][$row['TrnMove']['id']]));?>
                </td>
                <td><?php echo h_out($row["TrnMove"]['validated_date'],'center'); ?></td>
                <td><?php echo h_out($row['TrnMove']['hospital_sticker_no']); ?></td>
                <td><?php echo h_out($row['TrnMove']['department_name']); ?></td>
                <td>
                    <?php echo $this->form->text("Moves.recital.{$row['TrnMove']['id']}", array('class'=>'lbl','style'=>'width:85px;',"value"=>$this->request->data['Moves']['recital'][$row['TrnMove']['id']]));?>
                </td>
            </tr>
            <?php $i++; endforeach; ?>
        </table>
    </div>
<?php echo $this->form->end(); ?>