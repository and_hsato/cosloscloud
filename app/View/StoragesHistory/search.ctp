<script type="text/javascript">
    $(document).ready(function(){
        //検索ボタン押下
        $('#btn_Search').click(function(){
            $('#search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').submit();
        });
        //CSVボタン押下
        $("#btn_Csv").click(function(){
            $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/').submit();
            $('#search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
        });

        //全件チェック
        $('.checkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
        });
        //明細表示ボタン押下
        $('#cmdConfirm').click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length == 0){
                alert('変更を行う入庫履歴を選択してください');
                return false;
            }else{
                $('#search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm').submit();
            }
        });

        $('#cmdCancel').click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length  == 0){
                alert('取消を行う入庫履歴を選択してください');
                return false;
            }else{
                if(window.confirm("取消を実行します。よろしいですか？")){
                $('#search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/cancel').submit();
                }
            }
        });

    });

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>入庫履歴</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入庫履歴</span></h2>

<form class="validate_form search_form" id="search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search" method="post">
    <?php echo $this->form->input('TrnStorage.time' , array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
    <input type="hidden" name="data[IsSearch]" value="1" />
    <input type="hidden" name="data[MstDepartment][department_code]" value="" />
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="60" align="right"/>
            </colgroup>
            <tr>
                <th>入庫番号</th>
                <td><?php echo($this->form->text('TrnStorageHeader.work_no', array('class'=>'txt', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>入庫日</th>
                <td>
                    <?php echo($this->form->text('TrnStorageHeader.work_date_from', array('class' => 'date txt validate[optional,custom[date]]', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo($this->form->text('TrnStorageHeader.work_date_to', array('class' => 'date txt validate[optional,custom[date]]', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                </td>
                <td></td>
                <td><?php echo($this->form->checkbox('showDeleted', array('label' => false))); ?>取消も表示する</td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo($this->form->input('MstFacility.facility_code', array('type'=>'hidden','id'=>'facilityName'))); ?>
                    <?php echo($this->form->input('MstFacility.facility_code', array('type'=>'text','id'=>'facilityText','class'=>'txt', 'maxlength'=>'50','label'=>'', 'style'=>'width:60px;'))); ?>
                    <?php echo($this->form->input('MstFacility.facility_code_select', array('options'=>$facilities, 'id'=>'facilityCode','empty'=>true, 'class' => 'txt','empty' => ''))); ?>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo($this->form->text('MstFacilityItem.internal_code', array('class' => 'txt search_internal_code', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo($this->form->text('MstFacilityItem.item_code', array('class' => 'txt search_upper', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo($this->form->text('TrnSticker.lot_no', array('class' => 'txt', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo($this->form->text('MstFacilityItem.item_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo($this->form->text('MstDealer.dealer_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>発注区分</th>
                <td><?php echo $this->form->input('TrnOrder.order_type', array('options'=>$orderTypes, 'empty'=>true, 'class' => 'txt', 'id' => 'orderTypeSel')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo($this->form->text('MstFacilityItem.standard', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo($this->form->text('TrnSticker.facility_sticker_no', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('TrnReceiving.work_type', array('options'=>$classes, 'empty'=>true, 'class' => 'txt', 'id' => 'classTypeSel')); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>
    </div>

    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn1" id="btn_Search" />&nbsp;&nbsp;
            <input type="button" class="btn btn5" id="btn_Csv"/>
        </p>
    </div>
    <div id="results">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th style="width:20px;"><input type="checkbox" class="checkAll"/></th>
                    <th style="width:135px;">入庫番号</th>
                    <th style="width:100px;">入庫日</th>
                    <th>仕入先</th>
                    <th>登録者</th>
                    <th style="width:140px;">登録日時</th>
                    <th>備考</th>
                    <th>該当件数</th>
                </tr>
<?php if(count($SearchResult) === 0  && isset($data["IsSearch"]) && $data["IsSearch"] === "1"){ ?>
                <tr><td colspan="8" class="center"><span>該当するデータが存在しません。</span></td></tr>
<?php
                }
                $i = 0;
                foreach ($SearchResult as $_row):
?>
                <tr>
                    <td align="center">
                        <?php echo $this->form->checkbox("TrnStorageHeader.Rows.{$_row[0]['id']}.selected", array('class' => 'center checkAllTarget',)); ?>
                    </td>
                    <td ><?php echo h_out($_row[0]['work_no'] , 'center'); ?></td>
                    <td ><?php echo h_out($_row[0]['work_date'] ,'center'); ?></td>
                    <td><?php echo h_out($_row[0]['facility_name']); ?></td>
                    <td><?php echo h_out($_row[0]['creater_name']); ?></td>
                    <td><?php echo h_out($_row[0]['created'] , 'center'); ?></td>
                    <td><?php echo h_out($_row[0]['recital']); ?></td>
                    <td><?php echo h_out($_row[0]['real_detail_count']."/".$_row[0]['detail_count'] , 'right'); ?></td>
                </tr>
<?php
                $i++;
                endforeach;
?>
            </table>
        </div>
    <?php if (count($SearchResult) > 0): ?>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn6 submit [p2]" id="cmdCancel" />
            <input type="button" id="cmdConfirm" class="btn btn7"/>
        </p>
    </div>
    <?php endif; ?>
</div>
</form>
