<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">入庫履歴</a></li>
        <li>入庫履歴明細結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入庫履歴明細結果</span></h2>
<div class="Mes01">入庫登録を取り消しました</div>
<div class="TableScroll2">
    <table class="TableStyle01 table-even2">
        <thead>
        <tr>
            <th class="col10">入庫番号</th>
            <th class="col10">入庫日</th>
            <th class="col10">商品ID</th>
            <th class="col15">商品名</th>
            <th class="col10">製品番号</th>
            <th class="col10">包装単位</th>
            <th class="col10">ロット番号</th>
            <th class="col15">作業区分</th>
            <th class="col10">更新者</th>
        </tr>
        <tr>
            <th colspan="2">仕入先</th>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>仕入単価</th>
            <th>有効期限</th>
            <th>センターシール</th>
            <th>備考</th>
        </tr>
        </thead>
        <tbody>
        <?php $i=0;foreach($SearchResult as $_row): ?>
        <tr>
            <td><?php echo h_out($_row['work_no'],'center'); ?></td>
            <td><?php echo h_out($_row['work_date'],'center'); ?></td>
            <td><?php echo h_out($_row['internal_code'],'center'); ?></td>
            <td><?php echo h_out($_row['item_name']); ?></td>
            <td><?php echo h_out($_row['item_code']); ?></td>
            <td><?php echo h_out($_row['packing_name']); ?></td>
            <td><?php echo h_out($_row['lot_no']); ?></td>
            <td><?php echo h_out($_row['work_type_name']); ?></td>
            <td><?php echo h_out($_row['modifier_name']); ?></td>
        </tr>
        <tr>
            <td colspan="2"><?php echo h_out($_row['facility_name']); ?></td>
            <td></td>
            <td><?php echo h_out($_row['standard']); ?></td>
            <td><?php echo h_out($_row['dealer_name']); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($_row['transaction_price']),'right'); ?></td>
            <td><?php echo h_out($_row['validated_date'],'center'); ?></td>
            <td><?php echo h_out($_row['facility_sticker_no']); ?></td>
            <td><input type="text" value="<?php echo $_row['recital']; ?>" class="lbl" readonly style="width:80px;" /></td>
        </tr>
        <?php $i++;endforeach; ?>
        </tbody>
    </table>
</div>
