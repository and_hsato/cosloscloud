<script type="text/javascript">
    $(document).ready(function(){
        $('#cmdSeal').click(function(){
            var _c = 0;
            $('input[type=checkbox].chk').each(function(i){
                if(this.checked){
                    _c++;
                }
            });
            if(_c===0){
                alert('シールを印字する対象を選択してください');
                return false;
            }
            $('#storages_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/dummy').submit();
        });
    });
</script>
<form id="storages_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/results" method="POST">
    <input type="hidden" name="data[facility_name]" value="<?php echo $facility_name; ?>" />
    <input type="hidden" name="data[facility_id]" value="<?php echo $facility_id; ?>" />
    <input type="hidden" name="data[department_id]" value="<?php echo $department_id; ?>" />
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div id="TopicPath">
        <ul>
            <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
            <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">入庫履歴</a></li>
            <li>入庫履歴明細</li>
        </ul>
    </div>
    <h2 class="HeaddingLarge"><span>入庫履歴明細</span></h2>
    <div class="TableScroll2">
        <table class="TableStyle01 table-even2">
            <thead>
            <tr>
                <th style="width:20px;" rowspan="2"><input type="checkbox" checked onClick="listAllCheck(this);" /></th>
                <th style="width:135px;">入庫番号</th>
                <th style="width:95px;">入庫日</th>
                <th class="col10">商品ID</th>
                <th class="col20">商品名</th>
                <th class="col10">製品番号</th>
                <th style="width:8%;">包装単位</th>
                <th class="col10">ロット番号</th>
                <th style="width:12%;">作業区分</th>
                <th class="col10">更新者</th>
            </tr>
            <tr>
                <th class="col20" colspan="2">仕入先</th>
                <th class="col10"></th>
                <th class="col15">規格</th>
                <th class="col15">販売元</th>
                <th style="width:8%;">仕入単価</th>
                <th class="col10">有効期限</th>
                <th style="width:12%;">センターシール</th>
                <th class="col10">備考</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=0;foreach($SearchResult as $_row): ?>
            <tr>
                <td rowspan="2" align="center"><?php echo $this->form->checkbox("TrnSticker.Rows.{$_row['facility_sticker_no']}.selected",array('class'=>'center chk', 'checked'=>'checked')); ?></td>
                <td><?php echo h_out($_row['work_no'],'center'); ?></td>
                <td><?php echo h_out($_row['work_date'],'center'); ?></td>
                <td><?php echo h_out($_row['internal_code'],'center'); ?></td>
                <td><?php echo h_out($_row['item_name']); ?></td>
                <td><?php echo h_out($_row['item_code']); ?></td>
                <td><?php echo h_out($_row['packing_name']); ?></td>
                <td><?php echo h_out($_row['lot_no']); ?></td>
                <td><?php echo h_out($_row['work_type_name']); ?></td>
                <td><?php echo h_out($_row['modifier_name']); ?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo h_out($_row['facility_name']); ?></td>
                <td></td>
                <td><?php echo h_out($_row['standard']); ?></td>
                <td><?php echo h_out($_row['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($_row['transaction_price']),'right'); ?></td>
                <td><?php echo h_out($_row['validated_date'],'center'); ?></td>
                <td><?php echo h_out($_row['facility_sticker_no'],'center'); ?></td>
                <td><input type="text" value="<?php echo $_row['recital']; ?>" class="lbl" readonly style="width:80px;" /></td>
            </tr>
            <?php $i++;endforeach; ?>
            </tbody>
        </table>
    </div>

    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn28 submit" id="cmdSeal" />
        </p>
    </div>
</form>