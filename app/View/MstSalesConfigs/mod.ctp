<script>
$(function($) {
    $('.display').attr('style' , 'display:none;');
    $('.date').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
 
    //radioボタン変更時の処理
    $('#display0').change(function () {
        if(confirm("過去適用分も表示:表示を切り替えますと現在編集中の内容が破棄されますがよろしいでしょうか？")){
            //該当行を表示に
            $('.display').attr('style' , '');
        }
    });

    $('#display1').change(function () {
        if(confirm("適用中分のみ表示:表示を切り替えますと現在編集中の内容が破棄されますがよろしいでしょうか？")){
            //該当行を非表示に
            $('.display').attr('style' , 'display:none;');
        }
    });

    $('#add').click(function(){
        var tr_count = $('#table1 > tbody > tr').size(); //TR要素をカウント
        $('#table1').append(
            $('<tr>').append(
                $('<td>').append('<input type="text" name="data[MstSalesConfig][start_date]['+tr_count+']" class="r date" id="start_date'+tr_count+'">')
                         .append('<input type="hidden" name="data[MstSalesConfig][end_date]['+tr_count+']" value="3000/01/01">')
            ).append(
                $('<td>').append(
                    $('<select name="data[MstSalesConfig][mst_item_unit_id]['+tr_count+']" class="r">')
                     .append('<option value="">選択してください</option>')
                     <?php foreach($UnitNameList as $k => $v){ ?>
                     .append('<option value="<?php echo $k ?>"><?php echo $v?></option>')
                     <?php } ?>
                )
            ).append(
                $('<td>').append('<input type="text" name="data[MstSalesConfig][sales_price]['+tr_count+']" class="txt r right price" maxlength="13">')
            ).append(
                $('<td class="center">').append('<input type="checkbox" name="data[MstSalesConfig][is_deleted]['+tr_count+']">')
                         .append('<input type="hidden" name="data[MstSalesConfig][id]['+tr_count+']">')
                         .append('<input type="hidden" name="data[MstSalesConfig][status]['+tr_count+']" value="1">')
            )
        )

        //追加要素にカレンダーを追加
        $('#start_date'+tr_count).datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
    });

    $("#btn_Mod").click(function(){
        submitFlg = true;
        //適用開始日チェック
        $("#modForm input[type=text].date").each(function(){
          if(!dateCheck($(this).val())){
            alert('入力された日付が不正です');
            submitFlg = false;
            return false;
          }
        });
        
        //包装単位チェック
        $("#modForm select").each(function(){
          if($(this).val() == '' ){
            alert('包装単位を選択してください');
            submitFlg = false;
            return false;
          }
        });
        
        //小数点フォーマットチェック
        $("#modForm input[type=text].price").each(function(){
          if(!numCheck($(this).val() , 10 , 2 , false)){
            alert("取引単価が不正です");
            submitFlg = false;
            return false;
          }
        });

        if(submitFlg){
            $("#modForm").attr('action', '<?php echo $this->webroot; ?>mst_sales_configs/result');
            $("#modForm").attr('method', 'post');
            $("#modForm").submit();
        }
    });
});

</script>

    <div id="TopicPath">
        <ul>
              <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
              <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
              <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_configs_list">採用品選択</a></li>
              <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_configs_list">得意先選択</a></li>
              <li>売上設定</li>
        </ul>
    </div>
    <h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
    <h2 class="HeaddingMid">売上設定編集</h2>

    <form class="validate_form" id="modForm">
        <?php echo $this->form->input('MstFacilityItem.id' , array('type'=>'hidden'));?>
        <?php echo $this->form->input('MstFacility.id' , array('type'=>'hidden'))?>
           <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>得意先</th>
                    <td><?php echo $this->form->input('MstFacility.facility_name' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->input('MstFacilityItem.item_code' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品ID</th>
                    <td><?php echo $this->form->input('MstFacilityItem.internal_code' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->input('MstFacilityItem.dealer_name' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->input('MstFacilityItem.item_name' , array('class'=>'lbl' , 'readonly'=>'readonly')) ?></td>
                    <td></td>
                    <th>JANコード</th>
                    <td><?php echo $this->form->input('MstFacilityItem.jan_code' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                    <td></td>
                </tr>
                <tr>
                     <th>規格</th>
                     <td><?php echo $this->form->input('MstFacilityItem.standard' , array('class'=>'lbl' , 'readonly' => 'readonly'))?></td>
                     <td></td>
                     <th>保険請求区分</th>
                     <td><?php echo $this->form->input('MstFacilityItem.insurance_claim_department_name' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                     <td></td>
                 </tr>
                 <tr>
                     <th>償還価格</th>
                     <td><?php echo $this->form->input('MstFacilityItem.refund_price' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                     <td></td>
                     <th>保険請求名称</th>
                     <td><?php echo $this->form->input('MstFacilityItem.insurance_claim_name_s' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                     <td></td>
                 </tr>
                 <tr>
                 <tr>
                     <td><input type="radio" id="display1" name="display" value="1" checked/>適用中分のみ表示</td>
                     <td><input type="radio" id="display0" name="display" value="0" />過去適用分も表示</td>
                     <td></td>
                     <th></th>
                     <td></td>
                     <td></td>
                  </tr>
              </table>
              <div class="results">
                  <div class="SelectBikou_Area">
                      <span class="DisplaySelect">
                          <input type="button" id="add" class="btn btn44 [p2]" value="" />
                      </span>
                     <span class="BikouCopy"></span>
                  </div>
              <div class="TableScroll">
              <table class="TableStyle01" id="table1">
                  <colgroup>
                      <col />
                      <col />
                      <col />
                      <col width="35" />
                  </colgroup>
                  <thead>
                  <tr>
                      <th>適用開始日</th>
                      <th>包装単位</th>
                      <th>売上単価</th>
                      <th>適用</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $cnt = 0 ; foreach($result as $r){ ?>
                  <tr class="<?php echo ((!$r['MstSalesConfig']['status'])?'display':'') ?>">
                      <td>
                           <?php echo $this->form->input('MstSalesConfig.start_date.'.$cnt , array('type'=>'text' , 'class' => 'r date' , 'value'=>$r['MstSalesConfig']['start_date']))?>
                           <?php echo $this->form->input('MstSalesConfig.end_date.'.$cnt , array('type'=>'hidden' , 'value'=>'3000/01/01' ))?>
                      </td>
                      <td>
                          <?php echo $this->form->input('MstSalesConfig.mst_item_unit_id.'.$cnt , array('options'=>$UnitNameList , 'value'=>$r['MstSalesConfig']['mst_item_unit_id'] , 'empty'=>'選択してください' , 'class'=>'r') ) ?>
                      </td>
                      <td><?php echo $this->form->input('MstSalesConfig.sales_price.'.$cnt , array('class'=>'txt r right price' , 'maxlength'=>'13' , 'value'=>$r['MstSalesConfig']['sales_price']) )?></td>
                      <td class="center">
                          <?php echo $this->form->input('MstSalesConfig.is_deleted.'.$cnt , array('type'=>'checkbox' , 'checked'=>($r['MstSalesConfig']['is_deleted']) , 'hiddenField'=>false) )?>
                          <?php echo $this->form->input('MstSalesConfig.id.'.$cnt , array('type'=>'hidden' , 'value'=>$r['MstSalesConfig']['id']))?>
                          <?php echo $this->form->input('MstSalesConfig.status.'.$cnt , array('type'=>'hidden' , 'value'=>$r['MstSalesConfig']['status']))?>
                      </td>
                  </tr>
                  <?php $cnt++; } ?>
                  </tbody>
              </table>
          </div>
      </div>
  </div>
  <div class="ButtonBox">
      <input type="button" class="btn btn2 [p2]" value=""  id="btn_Mod" />
  </div>
</form>