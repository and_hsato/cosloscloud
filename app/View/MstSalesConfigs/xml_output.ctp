<printdata>
<setting>
    <layoutname><?php echo($layout_name); ?></layoutname>
    <fixvalues><?php
  foreach($fix_value as $name => $data) {
    echo('<value name="'.$name.'">'.$data.'</value>');
  }
?></fixvalues>
</setting>
<datatable>
    <columns><?php echo $columns; ?></columns>
    <rows>
        <?php foreach($posted as $r): ?>
        <row><?php echo join("\t", h($r)); ?></row>
        <?php endforeach; ?>
    </rows>
</datatable>
</printdata>