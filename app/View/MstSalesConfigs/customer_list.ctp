<script>
$(function(){
    $("#btn_Mod").click(function(){
        if($('input[type=radio]:checked').length > 0){
            $("#SalesConfigList1").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
        } else {
            alert('売上設定を行いたい得意先を選択してください');
        }
    });

    $("#btn_Search").click(function(){
        $("#SalesConfigList1").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/customer_list').submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
          <li>売上設定一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">売上設定一覧</h2>
<h2 class="HeaddingMid">検索条件</h2>
<form class="validate_form" id="SalesConfigList1" method="post">
    <?php echo $this->form->input('MstFacilityItem.id' , array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>得意先</th>
                <td><?php echo $this->form->input('MstFacilityItem.customer', array('options'=>$facilityList , 'class'=>'txt' ,'empty'=>true))?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_code' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstFacilityItem.dealer_name' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_name' , array('class'=>'lbl' , 'readonly'=>'readonly')) ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('MstFacilityItem.jan_code' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                <td></td>
            </tr>
            <tr>
                 <th>規格</th>
                 <td><?php echo $this->form->input('MstFacilityItem.standard' , array('class'=>'lbl' , 'readonly' => 'readonly'))?></td>
                 <td></td>
                 <th>保険請求区分</th>
                 <td><?php echo $this->form->input('MstFacilityItem.insurance_claim_department_name' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                 <td></td>
             </tr>
             <tr>
                 <th>償還価格</th>
                 <td><?php echo $this->form->input('MstFacilityItem.refund_price' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                 <td></td>
                 <th>保険請求名称</th>
                 <td><?php echo $this->form->input('MstFacilityItem.insurance_claim_name_s' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                 <td></td>
             </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" value="" class="btn btn1" id="btn_Search"/>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($result))) ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th></th>
                    <th>得意先</th>
                    <th>適用開始日</th>
                    <th>包装単位</th>
                    <th>売上単価</th>
                </tr>
          <?php 
             if(count($result) > 0){
                 foreach ($result as $r) {
          ?>
                <tr>
                    <td class="center"><?php echo $this->form->radio('MstFacility.id' ,array($r['MstFacility']['id'] => '' ) , array('label'=>false , 'div'=>false , 'hiddenField'=>false , 'class'=>'check'))?></td>
                    <td><?php echo h_out($r['MstSalesConfig']['facility_name']); ?></td>
                    <td><?php echo h_out($r['MstSalesConfig']['start_date'],'center'); ?></td>
                    <td><?php echo h_out($r['MstSalesConfig']['unit_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($r['MstSalesConfig']['sales_price']),'right'); ?></td>
                </tr>
          <?php }} ?>

            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" value="" class="btn btn4" id="btn_Mod"/>
    </div>
</form>
