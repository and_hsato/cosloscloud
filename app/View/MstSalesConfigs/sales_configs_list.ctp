<script>
    $(function(){
        //選択ボタン押下
        $("#btn_CustomerList").click(function(){
            $("#SalesConfigList2").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/customer_list').submit();
        });

        //検索ボタン押下
        $("#btn_Search").click(function(){
            validateDetachSubmit($("#SalesConfigList1") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_configs_list');
        });
        
        $("#btn_Print").click(function(){
            if(confirm('売上設定印刷を行ってよろしいでしょうか？')){
                $("#SalesConfigList1").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report').submit();
            }
        });
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>売上設定一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">売上設定一覧</h2>

<form class="validate_form search_form" method="post" accept-charset="utf-8" id="SalesConfigList1">
    <?php echo $this->form->input('MstSalesConfig.is_search' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('MstSalesConfig.mst_facility_id' , array('type'=>'hidden' , 'value'=> $this->Session->read('Auth.facility_id_selected'))) ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.facilityText',array('class'=>'txt require','style'=>'width:60px;','id'=>'centerText')); ?>
                    <?php echo $this->form->input('MstFacilityItem.facilityCode',array('options'=>$facilities,'class'=>'txt','style'=>'width:120px;','id'=>'centerCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('MstFacilityItem.facilityName',array('type'=>'hidden','id'=>'centerName')); ?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code' ,array('class'=>'txt search_internal_code') ) ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_code' , array('class'=>'txt search_canna'))?></td>
                <td></td>
                <th>適用開始日</th>
                <td>
                    <?php echo $this->form->input('MstSalesConfig.start_date' , array('type'=>'text', 'class'=>'date validate[optional,custom[date],funcCall2[date2]]' , 'id'=>'datepicker1'))?>
                    ～
                    <?php echo $this->form->input('MstSalesConfig.end_date' , array('type'=>'text', 'class'=>'date validate[optional,custom[date],funcCall2[date2]]', 'id'=>'datepicker2'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_name' , array('class'=>'txt search_canna'));?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstFacilityItem.dealer_name' , array('class'=>'txt search_canna'))?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('MstFacilityItem.standard' , array('class'=>'txt search_canna'))?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('MstFacilityItem.jan_code' , array('class'=>'txt'))?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>
        <input type="button" class="btn btn41" id="btn_Print"/>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($SalesConfig_Info))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
    </div>
</form>

<form class="validate_form search_form" method="post" action="" accept-charset="utf-8" id="SalesConfigList2">
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25" />
                <col width="140"/>
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th></th>
                <th>商品ID</th>
                <th>商品名</th>
                <th>規格</th>
                <th>製品番号</th>
                <th>販売元</th>
            </tr>
            <?php foreach ($SalesConfig_Info as $sales) { ?>
            <tr>
                <td class="center"><input class="validate[required]" id="MstFacilityItemId" name="data[MstFacilityItem][id]" type="radio" value="<?php echo $sales['MstFacilityItem']['id'] ?>"/></td>
                <td><?php echo h_out($sales['MstFacilityItem']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($sales['MstFacilityItem']['item_name']); ?></td>
                <td><?php echo h_out($sales['MstFacilityItem']['standard']); ?></td>
                <td><?php echo h_out($sales['MstFacilityItem']['item_code']); ?></td>
                <td><?php echo h_out($sales['MstDealer']['dealer_name']); ?></td>
            </tr>
            <?php } ?>
            <?php if(isset($this->request->data['MstSalesConfig']['is_search']) && count($SalesConfig_Info) == 0 ){ ?>
            <tr><td colspan="6" class="center">該当するデータがありませんでした。</td></tr>
            <?php } ?>
        </table>
    </div>
    <?php if(count($SalesConfig_Info) > 0){ ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn4" id="btn_CustomerList"/>
    </div>
    <?php } ?>
</form>
