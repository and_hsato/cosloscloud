<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_configs_list">採用品選択</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_configs_list">得意先選択</a></li>
        <li class="pankuzu">売上設定編集</li>
        <li>売上設定結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">売上設定編集結果</h2>

<div class="Mes01">情報を設定しました</div>
    <form>
    <div class="SearchBox">
         <table class="FormStyleTable">
             <colgroup>
                 <col />
                 <col />
                 <col width="20" />
                 <col />
                 <col />
                 <col width="20" />
             </colgroup>
             <tr>
                 <th>得意先</th>
                 <td><?php echo $this->form->input('MstFacility.facility_name' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                 <td></td>
                 <th>製品番号</th>
                 <td><?php echo $this->form->input('MstFacilityItem.item_code' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                 <td></td>
             </tr>
             <tr>
                 <th>商品ID</th>
                 <td><?php echo $this->form->input('MstFacilityItem.internal_code' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                 <td></td>
                 <th>販売元</th>
                 <td><?php echo $this->form->input('MstFacilityItem.dealer_name' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                 <td></td>
             </tr>
             <tr>
                 <th>商品名</th>
                 <td><?php echo $this->form->input('MstFacilityItem.item_name' , array('class'=>'lbl' , 'readonly'=>'readonly')) ?></td>
                 <td></td>
                 <th>JANコード</th>
                 <td><?php echo $this->form->input('MstFacilityItem.jan_code' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                 <td></td>
             </tr>
             <tr>
                  <th>規格</th>
                  <td><?php echo $this->form->input('MstFacilityItem.standard' , array('class'=>'lbl' , 'readonly' => 'readonly'))?></td>
                  <td></td>
                  <th>保険請求区分</th>
                  <td><?php echo $this->form->input('MstFacilityItem.insurance_claim_department_name' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                  <td></td>
             </tr>
             <tr>
                  <th>償還価格</th>
                  <td><?php echo $this->form->input('MstFacilityItem.refund_price' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                  <td></td>
                  <th>保険請求名称</th>
                  <td><?php echo $this->form->input('MstFacilityItem.insurance_claim_name_s' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                  <td></td>
             </tr>
         </table>
         <div class="results">
             <div class="TableScroll">
                  <table class="TableStyle01 table-odd">
                      <colgroup>
                          <col />
                          <col />
                          <col />
                          <col width="25" />
                      </colgroup>
                      <tr>
                          <th>適用開始日</th>
                          <th>包装単位</th>
                          <th>売上単価</th>
                          <th>適用</th>
                      </tr>
                      <?php foreach($result as $r){  ?>
                      <tr>
                          <td><?php echo h_out($r['MstSalesConfig']['start_date'] ,'center'); ?></td>
                          <td><?php echo h_out($UnitNameList[$r['MstSalesConfig']['mst_item_unit_id']],'center'); ?></td>
                          <td><?php echo h_out($this->Common->toCommaStr($r['MstSalesConfig']['sales_price']),'center'); ?></td>
                          <td><?php echo h_out((($r['MstSalesConfig']['is_deleted'])?'×':'○'),'center');  ?></td>
                      </tr>
                      <?php } ?>
                  </table>
              </div>
          </div>
      </div>
</form>
