<script type="text/javascript">

$(function(){
    // 検索ボタン押下
    $('#search_btn').click(function(){
        validateDetachSubmit($('#ItemForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/list_by_item#search_result' );
    });
    // 編集ボタン押下
    $('#edit_btn').click(function(){
        $("#ItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_by_item').submit();
    });
});

</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home">TOP</a></li>
        <li class="pankuzu">運用設定</li>
        <li>定数設定</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>定数設定</span></h2>
<h2 class="HeaddingMid">部署の定数設定が行えます。</h2>
<?php echo $this->form->create('MstFixedCount',array('class'=>'validate_form search_form','id' =>'ItemForm' , 'method'=>'post')); ?>
    <?php echo $this->form->input('search.is_search',array('type' => 'hidden')); ?>
    <h3 class="conth3">定数を検索</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFixedCount.internal_code',array('class' => 'txt search_internal_code','maxlength' => '50' ) ); ?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFixedCount.item_code',array('class' => 'txt search_upper','maxlength' => '50' ) ); ?></td>
                <td width="20"></td>
                <th></th>
                <td></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFixedCount.item_name',array('class' => 'search_canna txt','maxlength' => '50' ) ); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFixedCount.dealer_name',array('class' => 'search_canna txt','maxlength' => '50' ) ); ?></td>
                <td></td>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('MstFixedCount.mst_department_id',array('options'=>$department_list,'id' =>'depts', 'class' =>'txt' , 'empty'=>true)); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFixedCount.standard',array('class' => 'search_canna txt','maxlength' => '50' ) ); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('MstFixedCount.jan_code',array('class' => 'txt','maxlength' => '50' ) ); ?></td>
                <td></td>
                <th><!--管理区分--></th>
                <td><?php // echo $this->form->input('MstFixedCount.trade_type',array('options'=>$setPromsType,'id' => 'facilities','class'=>'txt' , 'empty'=>true) ); ?></td>
                <td></td>
            </tr>
        </table>
      </div>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="common-button" id="search_btn" value="検索"/>
            </p>
        </div>
    <div class="results">
      <a name="search_result"></a>
        <h2 class="HeaddingSmall">検索結果</h2>	
                <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult) ) ); ?>
            <div class="TableScroll">
                <table class="TableStyle01 table-odd">
                    <tr>
                        <th width="20"></th>
                        <th class="col10">商品ID</th>
                        <th class="col20">商品名</th>
                        <th class="col20">規格</th>
                        <th class="col20">製品番号</th>
                        <th class="col15">販売元</th>
                        <th class="col15">JANコード</th>
                    </tr>
                    <?php foreach($SearchResult as $item){ ?>
                    <tr>
                        <td class="center">
                            <input type="radio" name="data[MstFacilityItem][id]" id="check<?php echo $item['MstFixedCount']['id']; ?>" class="validate[required]" value="<?php echo $item['MstFixedCount']['id']; ?>" />
                        </td>
                        <td><?php echo h_out($item['MstFixedCount']['internal_code'],'center'); ?></td>
                        <td><?php echo h_out($item['MstFixedCount']['item_name']); ?></td>
                        <td><?php echo h_out($item['MstFixedCount']['standard']); ?></td>
                        <td><?php echo h_out($item['MstFixedCount']['item_code']); ?></td>
                        <td><?php echo h_out($item['MstFixedCount']['dealer_name']); ?></td>
                        <td><?php echo h_out($item['MstFixedCount']['jan_code']); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if(count($SearchResult) == 0 && isset($this->request->data['search']['is_search'])){  ?>
                    <tr>
                        <td colspan="7" class="center">該当するデータがありませんでした</td>
                    </tr>
                    <?php } ?>
                </table>
            </div>

        </div>
        <?php if(count($SearchResult) !== 0 ){  ?>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="common-button" id="edit_btn" value="編集"/>
            </p>
        </div>
        <?php } ?>
<?php echo $this->form->end(); ?>
