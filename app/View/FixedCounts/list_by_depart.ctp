<script type="text/javascript">
$(function($) {
    $("#regist_btn").click(function(){
        if($("input[type=radio]:checked").length > 0){
            $("#inputForm").attr('action' , "<?php echo $this->webroot ?><?php echo $this->name; ?>/select_by_depart").submit();
        }else{
            alert("部署を選択してください");
            exit;
        }
    });

    $("#edit_btn").click(function(){
        if($("input[type=radio]:checked").length > 0){
            $("#inputForm").attr('action' , "<?php echo $this->webroot ?><?php echo $this->name; ?>/edit_by_depart").submit();
        }else{
            alert("部署を選択してください");
            exit;
        }
    });
    // 検索ボタン押下
    $("#revealResult").click(function(){
        $("#inputForm").attr('action' ,"<?php echo $this->webroot ?><?php echo $this->name; ?>/list_by_depart").submit();
    });

    //印刷ボタン押下
    $("#printOut").click(function(){
        if($("input[type=radio]:checked").length > 0){
            $("#inputForm").attr('action' , "<?php echo $this->webroot ?><?php echo $this->name; ?>/report").submit();
        }else{
            alert("部署を選択してください");
            return false;
        }
    });
});
function anc(id){
   obj = $("#"+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}
function anc2(){
   obj = $("#pageDow")[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}  
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters/">マスタメンテ</a></li>
        <li class ="pankuzu">定数設定(部署別)</li>
        <li>部署選択</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>部署選択</span></h2>
<h2 class="HeaddingMid">設定済の商品を検索する条件を指定できます</h2>
<form class="validate_form search_form" id="inputForm" method="post" action="list_by_depart">
    <?php echo $this->form->input('MstFixedCount.is_search',array('type' => 'hidden')); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('MstFixedCount.mst_facility_id',array('options'=>$FacilityList,'class'=>'txt' , 'empty'=>true) ); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFixedCount.internal_code',array('maxlength' => '50','class'=>'txt search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFixedCount.item_code',array('maxlength' => '50','class'=>'txt search_upper')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFixedCount.item_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFixedCount.dealer_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFixedCount.standard',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
                <th>ＪＡＮコード</th>
                <td><?php echo $this->form->text('MstFixedCount.jan_code',array('maxlength' => '50','class'=>'txt')); ?></td>
                <td></td>
            </tr>
       </table>
       <p><font color="red">※新規に定数を追加する場合は条件を指定せずに検索してください</font></p>
           <div class="ButtonBox">
               <p class="center">
                   <input type="button" class="btn btn1" id="revealResult" />
               </p>
           </div>
           <hr class="Clear" />
           <div class="DisplaySelect">
           <div align="right" id="pageTop" style="padding-right: 10px;"></div>
           <?php echo $this->element('limit_combobox',array('result'=>count($result) ) ); ?>
           </div>
            <div class="TableScroll">
                <table class="TableStyle01 table-odd">
                   <tr>
                       <th width="20"></th>
                       <th>施設　／　部署</th>
                   </tr>
                <?php if(isset($this->request->data['MstFixedCount']['is_search'])){ ?>
                <?php
                $i = 0;
                if(count($result) !== 0){
                  foreach($result as $contents){
                ?>
                <tr>
                    <td width="20" class="center"><input type="radio" name='data[MstFixedCount][mst_department_id]' value="<?php echo h($contents['MstFixedCount']['mst_department_id']); ?>"  ></td>
                    <td><?php echo h_out($contents['MstFixedCount']['facility_name'] . '/' . $contents['MstFixedCount']['department_name']); ?>
                    </td>
                </tr>
                <?php $i++; } ?>
                </table>
            </div>
        </div>
        <div class="ButtonBox" id="pageDow">
            <input type="button" class="btn btn11 [p2]" id="regist_btn" />
            <input type="button" class="btn btn9" id="edit_btn" />
            <input type="button" class="btn btn83" id="printOut"  />
        </div>
                <?php } else { ?>
                  <tr>
                  <td class="center" colspan="2">該当するデータがありませんでした</td>
                  </tr>
                <?php }  ?>
                </table>
            </div>
        </div>
        <div align="right" ></div>
          <?php } ?>
<?php echo $this->form->end(); ?>
