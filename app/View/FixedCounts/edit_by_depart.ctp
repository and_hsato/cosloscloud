<script type="text/javascript">
$(function($) {
    $("#regist_btn").click(function(){

        submitFlg = true;
        isChecked = false;
        $("input[type=checkbox].chk").each(function() {
            if($(this).attr("checked") == true){
                isChecked = true;
                //チェックが入っている行の必須項目チェック
                fixed_count         = $(this).parents('tr:eq(0)').find('input[type=text].fixed_count');
                holiday_fixed_count = $(this).parents('tr:eq(0)').find('input[type=text].holiday_fixed_count');
                spare_fixed_count   = $(this).parents('tr:eq(0)').find('input[type=text].spare_fixed_count');
                start_date          = $(this).parents('tr:eq(0)').find('input[type=text].start_date');
          
                //定数チェック
                if(fixed_count.val() == ""){
                    alert("定数を入力してください");
                    fixed_count.focus();
                    exit;
                }
                if( fixed_count.val().match( /[\D]/g ) ){
                    alert("正の整数を入力してください");
                    fixed_count.focus();
                    exit;
                }
                //休日チェック
                if(holiday_fixed_count.val() == ""){
                    alert("休日定数を入力してください");
                    holiday_fixed_count.focus();
                    exit;
                }
                if(holiday_fixed_count.val().match( /[\D]/g ) ){
                    alert("正の整数を入力してください");
                    holiday_fixed_count.focus();
                    exit;
                }
                //予備チェック
                if(spare_fixed_count.val() == ""){
                    alert("予備定数を入力してください");
                    spare_fixed_count.focus();
                    exit;
                }
                if(spare_fixed_count.val().match( /[\D]/g ) ){
                    alert("正の整数を入力してください");
                    spare_fixed_count.focus();
                    exit;
                }
                //開始日チェック
                if(start_date.val() == ""){
                    alert("開始日を入力してください");
                    start_date.focus();
                    exit;
                }
                //開始日チェック
                if(!dateCheck(start_date.val())){
                    alert("開始日が不正です");
                    start_date.focus();
                    exit;
                }
            }
      });
      //部署選択チェック
      if(!isChecked){
          alert('登録する商品を選択してください');
          submitFlg = false;
          exit;
      }
      //サブミット可否確認
      if(submitFlg){
            $("#inputForm").attr('action'  , '<?php echo $this->webroot ?><?php echo $this->name; ?>/result_by_depart').submit();
      }
    });

    // 検索ボタン押下
    $("#revealResult").click(function(){
        $("#inputForm").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/edit_by_depart').submit();
    });
    //チェックボタン一括制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/list_by_depart">定数設定(部署別)</a></li>
        <li>定数編集</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>定数部署別</span></h2>
<h2 class="HeaddingMid">検索条件</h2>
<form class="validate_form search_form" id="inputForm" method="post">
    <?php echo $this->form->input('MstFixedCount.is_search',array('type' => 'hidden')); ?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstFacilityItem.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->text('MstFixedCount.facility_name',array('class' => 'lbl','readOnly' => 'readOnly' ) ); ?>
                    <?php echo $this->form->text('MstFixedCount.mst_facility_id',array('type' =>'hidden' ) ); ?>
                </td>
                <td></td>
                <th>部署</th>
                <td>
                    <?php echo $this->form->text('MstFixedCount.department_name',array('class' => 'lbl','readOnly' => 'readOnly' ) ); ?>
                    <?php echo $this->form->text('MstFixedCount.mst_department_id',array('type' =>'hidden' ) ); ?>
                </td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFixedCount.internal_code',array('maxlength' => '50','class'=>'txt search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFixedCount.item_code',array('maxlength' => '50','class'=>'txt search_upper')); ?></td>
                <td></td>
                <?php if(!isset($this->request->data['MstFacilityItem']['id'])){ ?>
                <th>管理区分</th>
                <td>
                    <?php echo $this->form->input('MstFixedCount.type',array('options'=>$setPromsType ,'class'=>'txt' , 'empty'=>true) ); ?>
                </td>
                <?php } ?>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFixedCount.item_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFixedCount.dealer_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
                <td colspan="2"><?php echo $this->form->checkbox('selected.onlyValidItems'); ?>有効な定数設定のみ表示</td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFixedCount.standard',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('MstFixedCount.jan_code',array('maxlength' => '50','class'=>'txt')); ?></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <?php if(!isset($this->request->data['MstFacilityItem']['id'])){ ?>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="revealResult" />
            </p>
        </div>
        <?php } ?>
        <hr class="Clear" />
        <div class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($result) ) );  ?>
        </div>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th style="width:20px;" rowspan="2"><input type="checkbox" checked class="checkAll" /></th>
                    <th class="col10">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col20">製品番号</th>
                    <th style="width:80px;">包装単位</th>
                    <th style="width:80px;" rowspan="2">印刷区分</th>
                    <th style="width:60px;" rowspan="2">定数</th>
                    <th style="width:60px;" rowspan="2">休日</th>
                    <th style="width:60px;" rowspan="2">予備</th>
                    <th style="width:80px;" rowspan="2">適用開始日</th>
                    <th style="width:30px;" rowspan="2">適用</th>
                </tr>
                <tr>
                    <th>棚番号</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>管理区分</th>
                </tr>
            <?php
            if(count($result) !== 0){
            $i = 0;
            foreach($result as $r) {
            ?>
                <tr>
                    <td rowspan="2" class="center">
                    <?php
                        if($r['MstFixedCount']['check']){
                            echo $this->form->checkbox('MstFixedCount.id.'.$i,array('checked' =>1, 'id'=> $i,  'class' =>'center chk' , 'value'=>(is_null($r['MstFixedCount']['id'])?'a':$r['MstFixedCount']['id']), 'hiddenField'=>false) );
                            echo $this->form->text('MstFixedCount.mst_facility_item_id.'.$i,array('type'=>'hidden', 'value' =>h($r['MstFixedCount']['mst_facility_item_id'])) );
                            echo $this->form->text('MstFixedCount.trn_stock_id.'.$i,array('type'=>'hidden', 'value' =>h($r['MstFixedCount']['trn_stock_id'])) );
                            foreach($r['MstFixedCount']['salesConfig'] as $line => $sales){
                                echo $this->form->input('MstSalesConfig.'.$i.'.'.$line , array('type'=>'hidden' , 'class'=>'sales' , 'value'=>$sales['MstSalesConfig']['mst_item_unit_id']));
                            }
                        }
                    ?>
                    </td>
                    <td class="center"><?php echo h_out($r['MstFixedCount']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['MstFixedCount']['item_name']);?></td>
                    <td><?php echo h_out($r['MstFixedCount']['item_code']);?></td>
                    <td>
                        <?php echo $this->form->input('MstFixedCount.mst_item_unit_id.'.$i , array('options'=>$r['MstFixedCount']['unitList'] , 'value'=> $r['MstFixedCount']['mst_item_unit_id'] , 'class'=>'r num','style'=>'height:22px','empty' => false)); ?>
                    </td>
                    <td rowspan="2"><?php echo $this->form->input('MstFixedCount.print_type.'.$i, array('options'=>$print_type,'value'=>$r['MstFixedCount']['print_type'],'class'=>'txt','style'=>'width:90%','empty' =>false)); ?></td>
                    <td rowspan="2"><?php echo $this->form->text('MstFixedCount.fixed_count.'.$i, array('value'=>$r['MstFixedCount']['fixed_count'],'class'=>'r num fixed_count','style'=>'width:90%','maxlength' =>'4')); ?></td>
                    <td rowspan="2"><?php echo $this->form->text('MstFixedCount.holiday_fixed_count.'.$i, array('value'=>$r['MstFixedCount']['holiday_fixed_count'],'style'=>'width:90%','class'=>'r num holiday_fixed_count','maxlength' =>'4')); ?></td>
                    <td rowspan="2"><?php echo $this->form->text('MstFixedCount.spare_fixed_count.'.$i, array('value'=>$r['MstFixedCount']['spare_fixed_count'],'style'=>'width:90%','class'=>'r num spare_fixed_count','maxlength' =>'4')); ?></td>
                    <td rowspan="2"><?php echo $this->form->text('MstFixedCount.start_date.'.$i, array('value'=>$r['MstFixedCount']['start_date'],'class'=>'r date center start_date')); ?></td>
                    <td rowspan="2" class="center">
                        <input type='checkbox' name='data[MstFixedCount][is_deleted][<?php echo $i?>]' <?php echo (($r['MstFixedCount']['is_deleted'])?'checked':'')?> class="center del_check">
                        <?php echo $this->form->input('MstFixedCount.err.'.$i, array('type'=>'hidden' , 'class'=>'err' , 'value'=>$r['MstFixedCount']['err']))?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $this->form->input('MstFixedCount.mst_shelf_name_id.'.$i, array('options'=> $shelf , 'value' => $r['MstFixedCount']['mst_shelf_name_id'],'style'=>'height:22px','class'=>'txt' , 'empty' => true));  ?>
                    </td>
                    <td><?php echo h_out($r['MstFixedCount']['standard']); ?></td>
                    <td><?php echo h_out($r['MstFixedCount']['dealer_name']);?></td>
                    <td>
                    <?php
                            echo $this->form->input('MstFixedCount.trade_type.'.$i, array('options'=>$setPromsType, 'value'=> h($r['MstFixedCount']['trade_type']),'class'=>'r','style'=>'height:22px','empty' => false) );
                            echo $this->form->text('MstFixedCount.is_lowlevel.'.$i,array('type'=>'hidden','value'=>'0' ) );
                    ?>
                    </td>
                </tr>
                <?php $i++; } ?>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="btn btn2 fix_btn [p2]" id="regist_btn" />
        </div>
        <?php
            }else{
               if(isset($this->request->data['MstFixedCount']['is_search'])){
                   echo '<tr><td colspan="10" class="center">該当するデータがありませんでした</td></tr>';
               }
        ?>
    </table>
</div>
    <?php
        }
    ?>
</div>
</form>
