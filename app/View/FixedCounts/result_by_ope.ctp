<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters/">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/list_by_ope">定数設定（オペ）</a></li>
        <li class="pankuzu">定数編集</li>
        <li>定数編集結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>定数編集結果</span></h2>
<div class="Mes01">以下の内容の定数を設定しました</div>
<form>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->text('MstFixedCount.facility_name',array('class' =>'lbl','readOnly' => 'readOnly')); ?></td>
                <td></td>
            </tr>
        </table>
        <hr class="Clear" />
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th class="col10">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col20">製品番号</th>
                    <th style="width:100px;">包装単位</th>
                    <th style="width:70px;" rowspan="2">発注点</th>
                    <th style="width:70px;" rowspan="2">定数</th>
                    <th style="width:70px;" rowspan="2">休日</th>
                    <th style="width:70px;" rowspan="2">予備</th>
                    <th style="width:40px;" rowspan="2">適用</th>
                </tr>
                <tr>
                    <th class="col10">棚番号</th>
                    <th class="col15">規格</th>
                    <th class="col10">販売元</th>
                    <th class="col15">適用開始日</th>
                </tr>
             <?php
                  if(count($result) !== 0){
                  $i = 0;
                  foreach($result as $r) {
             ?>
                <tr>
                    <td class="center"><?php echo h_out($r['MstFacilityItem']['internal_code'],'center');?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_name']);?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_code']);?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['unit_name']);?></td>
                    <td rowspan="2"><?php echo $this->form->text('displayData.order_point',array('value' =>h($r['MstFixedCount']['order_point']),'class' => 'lbl num','readOnly' => 'readOnly'));?></td>
                    <td rowspan="2"><?php echo $this->form->text('displayData.fixed_count',array('value' =>h($r['MstFixedCount']['fixed_count']),'class' => 'lbl num','readOnly' => 'readOnly'));?></td>
                    <td rowspan="2"><?php echo $this->form->text('displayData.holiday_fixed_count',array('value' =>h($r['MstFixedCount']['holiday_fixed_count']),'class' => 'lbl num','readOnly' => 'readOnly'));?></td>
                    <td rowspan="2"><?php echo $this->form->text('displayData.spare_fixed_count',array('value' =>h($r['MstFixedCount']['spare_fixed_count']),'class' => 'lbl num','readOnly' => 'readOnly'));?></td>
                    <td rowspan="2" class="center"><?php echo h_out($r['MstFixedCount']['is_deleted_view'],'center');?></td>
                </tr>
                <tr>
                    <td><?php echo $this->form->text('displayData.mst_shelf_name_id',array('value' =>h($r['MstFixedCount']['mst_shelf_name']),'class' => 'lbl','readOnly' => 'readOnly','style'=>'width:90%;'));?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['standard']);?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['dealer_name']);?></td>
                    <td><?php echo $this->form->text('displayData.start_date',array('value' =>h($r['MstFixedCount']['start_date']),'class' => 'lbl center','readOnly' => 'readOnly','style'=>'width:90%;'));?></td>
                </tr>
                <?php  $i++; } } ?>
            </table>
        </div>
    </div>
</form>
