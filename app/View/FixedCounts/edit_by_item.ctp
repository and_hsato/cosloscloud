<script type="text/javascript">
$(function($) {
    // 検索ボタン押下
    $("#revealResult").click(function(){
        $("#itemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_by_item#search_result').submit();
    });
    // 確定ボタン押下
    $("#regist").click(function(){
        submitFlg = true;
        isChecked = false;
        $("#itemForm input[type=checkbox].chk").each(function(){
            if($(this).attr("checked") == true){
                cnt = $(this).attr('id');
                isChecked = true;
                //チェックが入っている行の必須項目チェック
                fixed_count         = $(this).parents('tr:eq(0)').find('input[type=text].fixed_count');
                //定数チェック
                if(fixed_count.val() == ""){
                    alert("定数を入力してください");
                    fixed_count.focus();
                    exit;
                }
                if( fixed_count.val().match( /[\D]/g ) ){
                    alert("正の整数を入力してください");
                    fixed_count.focus();
                    exit; 
                }
            }
        });
        //部署選択チェック
        if(!isChecked){
            alert('登録する施設/部署を選択してください');
            submitFlg = false;
            return false;
        }
        //サブミット可否確認
        if(submitFlg){
            $("#itemForm").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/result_by_item').submit();
        }
    });
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home">TOP</a></li>
        <li class="pankuzu">運用設定</li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/list_by_item">定数設定</a></li>
        <li>定数編集</li>
    </ul>
</div>

<h2 class="HeaddingLarge">定数設定</span></h2>
<h2 class="HeaddingMid">部署の定数設定が行えます。</h2>
<?php echo $this->form->create('MstFixedCount',array('class' => 'validate_form','id' =>'itemForm' ,'method'=>'post')); ?>
    <?php echo $this->form->text('MstFixedCount.is_search',array('type' =>'hidden')); ?>
    <?php echo $this->form->text('MstFacilityItem.id',array('type' =>'hidden')); ?>
    <?php echo $this->form->text('MstFacilityItem.is_lowlevel',array('type' => 'hidden')); ?>
    <?php echo $this->form->text('MstFacilityItem.facility_id',array('type' =>'hidden')); ?>
    <?php echo $this->form->text('MstFacilityItem.item_type',array('type' =>'hidden')); ?>
    <?php echo $this->form->text('MstFacilityItem.readTime',array('value'=> date('Y-m-d H:i:s'), 'type' => 'hidden')); ?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstFacilityItem.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
    <h3 class="conth3">定数を検索</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td>
                  <?php echo $this->form->text('MstFacilityItem.internal_code',array('class' => 'lbl' ,'readonly' => 'readonly' ));  ?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                  <?php echo $this->form->text('MstFacilityItem.item_code',array('class' => 'lbl search_upper' ,'readonly' => 'readonly' ));  ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td>
                  <?php echo $this->form->text('MstFacilityItem.item_name',array('class' => 'lbl' ,'readonly' => 'readonly' ));  ?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                  <?php echo $this->form->text('MstFacilityItem.dealer_name',array('class' => 'lbl' ,'readonly' => 'readonly' ));  ?>
                </td>
                <td></td>
                <td colspan="2"><?php echo $this->form->checkbox('selected.onlyValidDepts'); ?>有効な定数設定のみ表示</td>
            </tr>
            <tr>
                <th>規格</th>
                <td>
                  <?php echo $this->form->text('MstFacilityItem.standard',array('class' => 'lbl' ,'readonly' => 'readonly' ));  ?>
                </td>
                <td></td>
                <th>JANコード</th>
                <td>
                  <?php echo $this->form->text('MstFacilityItem.jan_code',array('class' => 'lbl' ,'readonly' => 'readonly' ));  ?>
                </td>
                <td></td>
                <td colspan="2"><?php echo $this->form->checkbox('selected.onlySetDepts'); ?>定数設定部署のみ表示</td>
            </tr>
        </table>
      </div>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="common-button" value="検索" id="revealResult" />
            </p>
        </div>
    <hr class="Clear" />
    <div class="results">
    <div class="DisplaySelect">
      <a name="search_result"></a>
        <h2 class="HeaddingSmall">検索結果</h2>	
        <?php echo $this->element('limit_combobox',array('result'=> count($displayData))); ?>
    </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
            <colgroup>
                <col style="width:20px;"/>
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th><input type="checkbox"  onClick="listAllCheck(this);" /></th>
                <th>部署</th>
                <th>包装単位</th>
                <th>定数</th>
                <!--<th class="col10">休日</th>-->
                <!--<th class="col10">予備</th>-->
                <!--<th class="col10">管理区分</th>-->
                <!--<th class="col10">印刷区分</th>-->
                <!--<th class="col15">棚番号</th>-->
                <!--<th class="col15">適用開始日</th>-->
                <!--<th width="40">適用</th>-->
            </tr>
            <?php if(count($displayData) === 0 && isset($this->request->data['MstFixedCount']['is_search'])){ ?>
                <td class="center" colspan="4">該当するデータがありませんでした</td>
            <?php }else{  $i = 0;
                foreach($displayData as $contents){
             ?>
               <tr>
                <td class="center" >
                  <?php if($contents['MstFixedCount']['check'] == 1 ){ ?>
                      <?php echo $this->form->input('MstFixedCount.id.'.$i, array('type'=>'checkbox' , 'id' =>$i , 'class' =>'center chk','checked'=>false , 'value'=>(is_null($contents['MstFixedCount']['id'])?'a':$contents['MstFixedCount']['id']), 'hiddenField'=>false) ); ?>
                      <?php echo $this->form->input('MstFixedCount.mst_department_id.'.$i, array('value'=>$contents['MstFixedCount']['department_id'],'type'=>'hidden')) ; ?>
                      <?php echo $this->form->input('MstFixedCount.mst_facility_id.'.$i, array('value'=>$contents['MstFixedCount']['mst_facility_id'],'type'=>'hidden')) ; ?>
                      <?php foreach($contents['MstFixedCount']['salesConfig'] as $line => $sales){ ?>
                          <?php echo $this->form->input('MstSalesConfig.'.$i.'.'.$line , array('type'=>'hidden' , 'class'=>'sales' , 'value'=>$sales['MstSalesConfig']['mst_item_unit_id'])); ?>
                      <?php } ?>
                  <?php } ?>
                </td>
                <td><?php echo h_out($contents['MstFacility']['Facility_Name']); ?></td>
                <td>
                    <?php
                    echo $this->form->input('MstFixedCount.mst_item_unit_id.'.$i,array('options'=>$unitSelecter , 'value'=> h($contents['MstFixedCount']['mst_item_unit_id']),'class'=>'r num','style'=>'height:22px','empty' => false));
                    ?>
                </td>
                <td><?php echo $this->form->text('MstFixedCount.fixed_count.'.$i,array('class' => 'r num txt fixed_count','value' => h($contents['MstFixedCount']['fixed_count']),'maxlength' =>'4'  ) ); ?>
                <?php echo $this->form->input('MstFixedCount.holiday_fixed_count.'.$i,array('type' => 'hidden','value' => 0 ) ); ?></td>
                <?php echo $this->form->input('MstFixedCount.spare_fixed_count.'.$i,array('type' => 'hidden','value' => 0) ); ?></td>
                <?php echo $this->form->input('MstFixedCount.trade_type.'.$i,array('type'=> 'hidden', 'value'=> '5')); ?>
                <?php echo $this->form->input('MstFixedCount.start_date.'.$i,array('type'=>'hidden' , 'value' =>date('Y/m/d'))); ?>

                </td>
                <!--<td><?php echo $this->form->text('MstFixedCount.holiday_fixed_count.'.$i,array('class' => 'r num txt holiday_fixed_count','value' => h($contents['MstFixedCount']['holiday_fixed_count']),'maxlength' =>'4' ) ); ?></td>-->
                <!--<td><?php echo $this->form->text('MstFixedCount.spare_fixed_count.'.$i,array('class' => 'r num txt spare_fixed_count','value' => h($contents['MstFixedCount']['spare_fixed_count']),'maxlength' =>'4' ) ); ?></td>-->
                <!--<td>
                    <?php if($this->request->data['MstFacilityItem']['is_lowlevel']){ ?>
                        <p class='center'>低レベル品</p>
                     <?php echo $this->form->text('MstFixedCount.trade_type.'.$i,array('type'=>'hidden','value'=>Configure::read('TeisuType.Constant') ) ); ?>
                     <?php }else if($this->request->data['MstFacilityItem']['item_type'] == Configure::read('Items.item_types.setitem')) { // セット品 ?>
                     <?php echo $this->form->input('MstFixedCount.trade_type.'.$i,array('options'=> array('1'=>'定数品','2'=>'準定数品') , 'value'=> $contents['MstFixedCount']['trade_type'],'class'=>'r','style'=>'height:22px','empty' => false)); ?>
                     <?php  }else{ ?>
                     <?php echo $this->form->input('MstFixedCount.trade_type.'.$i,array('options'=>$setPromsType, 'value'=>$contents['MstFixedCount']['trade_type'],'class'=>'r','style'=>'height:22px','empty' => false)); ?>
                     <?php } ?>
                </td>-->
                <!--<td>
                    <?php echo $this->form->input('MstFixedCount.print_type.'.$i, array('options'=>$print_type, 'value' => $contents['MstFixedCount']['print_type'], 'class'=>'txt', 'style'=>'height:22px','empty' => false));  ?>
                </td>-->
                <!--<td>
                    <?php
                    $val = (isset($contents['MstFixedCount']['mst_shelf_name_id'])) ? $contents['MstFixedCount']['mst_shelf_name_id'] :"";
                    echo $this->form->input('MstFixedCount.mst_shelf_name_id.'.$i, array('options'=>$contents['MstFixedCount']['selecter'], 'value' => $val, 'class'=>'txt', 'style'=>'height:22px','empty' => true)); 
                    ?>
                </td>-->
                <!--<td class="center">
                    <?php echo $this->form->text('MstFixedCount.start_date.'.$i,array('value' =>h((($contents['MstFixedCount']['start_date'])?date('Y/m/d' , strtotime($contents['MstFixedCount']['start_date'])):'')), 'class'=>'r date center start_date','empty' => false , 'maxlength' => 10)); ?>
                </td>-->
                <!--<td class="center">
                    <input type='checkbox' name='data[MstFixedCount][is_deleted][<?php echo $i?>]' <?php echo ((!$contents['MstFixedCount']['is_deleted'])?'checked':'')?> class="del_check">
                    <?php echo $this->form->input('MstFixedCount.err.'.$i , array('type'=>'hidden' , 'class'=>'err' , 'value'=>$contents['MstFixedCount']['err']))?>
                </td>-->
            </tr>
            <?php
                $i++;
                }
              }
            ?>

            </table>
        </div>
      </div>
        <div class="ButtonBox">
            <input type="button" class="common-button [p2]" id="regist" value="登録" />
        </div>
</div>
</form>
</div>