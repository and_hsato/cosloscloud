<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li class="pankuzu">運用設定</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/list_by_item">定数設定</a></li>
        <li>定数編集</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>定数設定</span></h2>
<h2 class="HeaddingMid">以下の定数を設定しました。</h2>
<div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col/>
            <col/>
            <col width="20"/>
            <col/>
            <col/>
            <col width="20"/>
            <col/>
            <col/>
        </colgroup>
        <tr>
            <th>商品ID</th>
            <td><?php echo $this->form->text('MstFacilityItem.internal_code',array('class' => 'lbl' ,'maxlength' =>'50','readonly' => 'readonly' ));  ?></td>
            <td></td>
            <th>製品番号</th>
            <td><?php echo $this->form->text('MstFacilityItem.item_code',array('class' => 'lbl search_upper' ,'maxlength' =>'50','readonly' => 'readonly' ));  ?></td>
            <td></td>
        </tr>
        <tr>
            <th>商品名</th>
            <td><?php echo $this->form->text('MstFacilityItem.item_name',array('class' => 'lbl' ,'maxlength' =>'50','readonly' => 'readonly' ));  ?></td>
            <td></td>
            <th>販売元</th>
            <td><?php echo $this->form->text('MstFacilityItem.dealer_name',array('class' => 'lbl' ,'maxlength' =>'50','readonly' => 'readonly' ));  ?></td>
        </tr>
        <tr>
            <th>規格</th>
            <td><?php echo $this->form->text('MstFacilityItem.standard',array('class' => 'lbl' ,'maxlength' =>'50','readonly' => 'readonly' ));  ?></td>
            <td></td>
            <th>JANコード</th>
            <td><?php echo $this->form->text('MstFacilityItem.jan_code',array('class' => 'lbl' ,'maxlength' =>'50','readonly' => 'readonly' ));  ?></td>
            <td></td>
            <td colspan="2"></td>
        </tr>
    </table>
  </div>
    <hr class="Clear" />
<div class="results">
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
        <tr>
            <th class="col20">部署</th>
            <th class="col10">包装単位</th>
            <th class="col10">定数</th>
            <!--<th class="col10">休日</th>
            <th class="col10">予備</th>
            <th class="col10">管理区分</th>
            <th class="col10">印刷区分</th>
            <th class="col15">棚番号</th>
            <th class="col15">適用開始日</th>
            <th width="40">適用</th>-->
        </tr>
        <?php
        $i = 0;
        foreach($result as $r ){
        ?>
            <tr>
                <td><?php echo h_out($r['MstFixedCount']['fasAndDept']); ?></td>
                <td><input type="text" value="<?php echo h($r['MstFixedCount']['unit_name']); ?>" class="lbl num" readOnly style="width:90%;" /></td>
                <td><input type="text" value="<?php echo h($r['MstFixedCount']['fixed_count']); ?>" class="lbl num" readOnly style="width:90%;" /></td>
                <!--<td><input type="text" value="<?php echo h($r['MstFixedCount']['holiday_fixed_count']); ?>" class="lbl num" readOnly style="width:90%;" /></td>
                <td><input type="text" value="<?php echo h($r['MstFixedCount']['spare_fixed_count']); ?>" class="lbl num" readOnly style="width:90%;" /></td>
                <td><input type="text" value="<?php echo h($r['MstFixedCount']['trade_type']); ?>" class="lbl" readOnly style="width:90%;" /></td>
                <td><input type="text" value="<?php echo h($r['MstFixedCount']['print_type']); ?>" class="lbl" readOnly style="width:90%;" /></td>
                <td><input type="text" value="<?php echo h($r['MstFixedCount']['shelf_name']); ?>" class="lbl" readOnly style="width:90%;" /></td>
                <td class="center"><input type="text" class="lbl center" value="<?php echo h($r['MstFixedCount']['start_date']); ?>" readOnly style="width:90%;" ></td>
                <td><?php echo h_out($r['MstFixedCount']['is_deleted'],'center'); ?></td>-->
            </tr>
        <?php $i++; } ?>
        </table>
    </div>
</div>
</div>