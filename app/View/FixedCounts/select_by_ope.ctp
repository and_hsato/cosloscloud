<script type="text/javascript">

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#fixedCountForm #searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#fixedCountForm #cartForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );  

    //検索ボタン押下
    $("#btn_Search").click(function(){
        //カート欄にある全IDを取得
        var tmpId = "";
        $("#cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tmpId === '' ){
                    tmpId = $(this).val();
                }else{
                    tmpId +="," +  $(this).val();
                }
            }
        });
        $("#tmpId").val(tmpId);

        $("#fixedCountForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/select_by_ope').submit();
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        $('#itemSelectedTable').empty();
        $("#fixedCountForm #searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
            cnt++;
            }
        });
        if(cnt === 0 ){
            alert('商品を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        //選択チェック
        var cnt = 0;
        var tmpId = '';
        $("#fixedCountForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
                if(tmpId === ''){
                    tmpId = $(this).val();
                }else{
                    tmpId = tmpId + ','+$(this).val();
                }
            }
        });
        $('#tmpId').val(tmpId);
        if(cnt === 0){
            alert('商品を選択してください');
        }else{
            $("#fixedCountForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_by_ope').submit();
        }
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/list_by_center">定数設定(オペ)</a></li>
        <li>採用品選択</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>採用品選択</span></h2>
<h2 class="HeaddingMid">検索条件</h2>
<form class="validate_form search_form" id="fixedCountForm" method="post" action="select_by_ope">
    <?php echo $this->form->input('MstFixedCount.itemSearch',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('cart.tmpid',array('type'=>'hidden','id'=>'tmpId'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('MstFixedCount.facility_name',array('class' => 'lbl','readOnly' => 'readOnly' ) ); ?>
                    <?php echo $this->form->input('MstFixedCount.mst_facility_id',array('type' =>'hidden') ); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFixedCount.internal_code',array('maxlength' => '50','class'=>'txt search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFixedCount.item_code',array('maxlength' => '50','class'=>'txt search_upper')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFixedCount.item_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFixedCount.dealer_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFixedCount.standard',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
                <th>ＪＡＮコード</th>
                <td><?php echo $this->form->text('MstFixedCount.jan_code',array('maxlength' => '50','class'=>'txt')); ?></td>
                <td></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="btn_Search"/>
            </p>
        </div>
    </div>
    <div align="right" id="pageTop" ></div>

<?php
    if(isset($this->request->data['MstFixedCount']['itemSearch'])){
      if(count($SearchResult) === 0){
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
                <table class="TableHeaderStyle02" >
                <tr>
                    <th style="width:20px;" rowspan="2">
                    <input type="checkbox" onClick="listAllCheck(this)"; />
                    </th>
                    <th class="col20">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col15">規格</th>
                    <th class="col15">製品番号</th>
                    <th class="col20">販売元</th>
                    <th class="col10">包装単位</th>
                </tr>
                </table>
      </div>
      <span>該当するデータはありません。</span>
<?php
      }else{
?>
    <div id="results">
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
                <table class="TableHeaderStyle02" >
                <tr>
                    <th style="width:20px;" rowspan="2">
                    <input type="checkbox" checked="checked" class='checkAll_1'  />
                    </th>
                    <th class="col20">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col15">規格</th>
                    <th class="col15">製品番号</th>
                    <th class="col20">販売元</th>
                    <th class="col10">包装単位</th>
                </tr>
                </table>
    </div>

    <div class="" id="searchForm">
<?php
  $i = 0;
  foreach($SearchResult as $item){
  $class = ($i % 2 == 0)?'':'odd';
  ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$item["MstFixedCount"]["mst_item_unit_id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <tr class="<?php echo $class ; ?>" id=<?php echo "itemSelectedRow".$item["MstFixedCount"]['mst_item_unit_id'];?> >
            <td class="center" rowspan="2" style="width:20px;">
              <?php echo $this->form->checkbox("SelectedID.{$i}", array('class'=>'center checkAllTarget','value'=>$item["MstFixedCount"]["mst_item_unit_id"] , 'style'=>'width:20px;text-align:center;' , 'checked'=>'checked' , 'hiddenField'=>false) ); ?>
            </td>
                <td class="col20"><?php echo $item['MstFacilityItem']['internal_code'] ?></td>
                <td class="col20"><?php echo $item['MstFacilityItem']['item_name'] ?></td>
                <td class="col15"><?php echo $item['MstFacilityItem']['standard'] ?></td>
                <td class="col15"><?php echo $item['MstFacilityItem']['item_code'] ?></td>
                <td class="col20"><?php echo $item['MstFacilityItem']['dealer_name'] ?></td>
                <td class="col10"><?php echo $item['MstFacilityItem']['unit_name'] ?></td>
            </tr>
      </table>
      <?php $i++; } ?>
    </div>
      <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
          <input type="button" value="" class="btn btn4" id="cmdSelect" />
        </p>
      </div>
    </div>
    <?php } ?>

    <?php if(isset($this->request->data['MstFixedCount']['itemSearch'])){?>
    <h2 class="HeaddingSmall">選択商品</h2>
    <div class="TableHeaderAdjustment01">
                <table class="TableHeaderStyle02" >
                <tr>
                    <th style="width:20px;" rowspan="2">
                    <input type="checkbox" checked="checked" class='checkAll_2' />
                    </th>
                    <th class="col20">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col15">規格</th>
                    <th class="col15">製品番号</th>
                    <th class="col20">販売元</th>
                    <th class="col10">包装単位</th>
                </tr>
                </table>
    </div>

    <div class="" broder='0'  id="cartForm" >
      <?php if(isset($CartSearchResult)){ ?>
<?php
  $i = 0;
  foreach($CartSearchResult as $item){
  $class = ($i % 2 == 0)?'':'odd';
  ?>
      <table class="TableStyle02" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
            <tr class=<?php echo $class ?>>
                <td class="center" rowspan="2" style="width:20px;">
                <?php echo $this->form->checkbox('MstItemUnitt.selectid.'.$i , array('checked' => 1 , 'class'=>'center checkAllTarget' , 'value'=>$item['MstFixedCount']['mst_item_unit_id'] , 'hiddenField'=>false) ); ?>
                </td>
                <td class="col20"><?php echo $item['MstFacilityItem']['internal_code'] ?></td>
                <td class="col20"><?php echo $item['MstFacilityItem']['item_name'] ?></td>
                <td class="col15"><?php echo $item['MstFacilityItem']['standard'] ?></td>
                <td class="col15"><?php echo $item['MstFacilityItem']['item_code'] ?></td>
                <td class="col20"><?php echo $item['MstFacilityItem']['dealer_name'] ?></td>
                <td class="col10"><?php echo $item['MstFacilityItem']['unit_name'] ?></td>
            </tr>
      </table>
            <?php
              $i++;
              }
            ?>
      <?php }?>
      <div id="itemSelectedTable" border="0" style="margin-top:-1px; margin-bottom:-1px; padding:0px;"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" value="" class="btn btn3" />
      </p>
    </div>
    <?php }?>
<?php
    }else{
      //検索実行フラグがfalse
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>0)); ?>
      </div>

      <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02" >
                <tr>
                    <th style="width:20px;" rowspan="2">
                    <input type="checkbox"  />
                    </th>
                    <th class="col20">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col15">規格</th>
                    <th class="col15">製品番号</th>
                    <th class="col20">販売元</th>
                    <th class="col10">包装単位</th>
                </tr>
            </table>
      </div>
<?php
    }
    echo $this->form->end();
?>
<div align="right" id="pageDow" ></div>
