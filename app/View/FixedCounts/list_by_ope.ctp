<script type="text/javascript">
$(function($) {
    // 新規ボタン押下
    $("#new").click(function(){
        $("#search_form").attr("action" , "<?php echo $this->webroot ?><?php echo $this->name; ?>/select_by_ope").submit();
    })
    // 編集ボタン押下
    $("#edit").click(function(){
        $("#search_form").attr("action" , "<?php echo $this->webroot ?><?php echo $this->name; ?>/edit_by_ope" ).submit();
    })
});
  
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot;?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot;?>masters/">マスタメンテ</a></li>
        <li class="pankuzu">定数設定（オペ）</li>
        <li>センター指定</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>センター指定</span></h2>
<h2 class="HeaddingMid">定数を設定するセンターを指定してください</h2>
<?php echo $this->form->create('MstfixedCount',array('class'=>'validate_form','type' => 'post','action' => '.' ,'id' =>'search_form')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('MstFixedCount.mst_facility_id',array('options'=>$facilities,'empty' => FALSE, 'class' =>'txt','style'=>'width:230px;')); ?></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn11 [p2]" id ="new" />
        <input type="button" class="btn btn9" id ="edit"/>
    </div>
<?php echo $this->form->end(); ?>
  