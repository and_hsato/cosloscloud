<script type="text/javascript">
$(function($) {
    $("#regist_btn").click(function(){
        submitFlg = true;
        isChecked = false;
        $("#inputForm input[type=checkbox].chk").each(function(){
            if($(this).attr("checked") == true){
                isChecked = true;
                //チェックが入っている行の必須項目チェック
                order_point         = $(this).parents('tr:eq(0)').find('input[type=text].order_point');
                fixed_count         = $(this).parents('tr:eq(0)').find('input[type=text].fixed_count');
                holiday_fixed_count = $(this).parents('tr:eq(0)').find('input[type=text].holiday_fixed_count');
                spare_fixed_count   = $(this).parents('tr:eq(0)').find('input[type=text].spare_fixed_count');
                start_date          = $(this).parents('tr:eq(0)').next().find('input[type=text].start_date');

                //発注点チェック
                if(order_point.val() == ""){
                    alert("発注点を入力してください");
                    order_point.focus();
                    exit;
                }
                if(order_point.val().match( /[\D]/g ) ){
                    alert("正の整数を入力してください");
                    order_point.focus();
                    exit;
                }
                //定数チェック
                if(fixed_count.val() == ""){
                    alert("定数を入力してください");
                    fixed_count.focus();
                    exit;
                }
                if(fixed_count.val().match( /[\D]/g ) ){
                    alert("正の整数を入力してください");
                    fixed_count.focus();
                    exit;
                }
                //休日チェック
                if(holiday_fixed_count.val() == ""){
                    alert("休日定数を入力してください");
                    holiday_fixed_count.focus();
                    exit;
                }
                if(holiday_fixed_count.val().match( /[\D]/g ) ){
                    alert("正の整数を入力してください");
                    holiday_fixed_count.focus();
                    exit;
                }
                //予備チェック
                if(spare_fixed_count.val() == ""){
                    alert("予備定数を入力してください");
                    spare_fixed_count.focus();
                    exit;
                }
                if(spare_fixed_count.val().match( /[\D]/g ) ){
                    alert("正の整数を入力してください");
                    spare_fixed_count.focus();
                    exit;
                }
                //開始日チェック
               if(start_date.val() == ""){
                   alert("開始日を入力してください");
                   start_date.focus();
                   exit;
               }
               //開始日チェック
               if(!dateCheck(start_date.val())){
                   alert("開始日が不正です");
                   start_date.focus();
                   exit;
               }
           }
       });
       //部署選択チェック
       if(!isChecked){
           alert('登録する商品を選択してください');
           exit;
        }
        //サブミット可否確認
        if(submitFlg){
            $("#inputForm").attr('action' , "<?php echo $this->webroot ?><?php echo $this->name; ?>/result_by_ope").submit();
        }
    });

    $("#revealResult").click(function(){
        $("#inputForm").attr('action' , "<?php echo $this->webroot ?><?php echo $this->name; ?>/edit_by_ope").submit();
    });
});
function dateCheck( checkStr ) {

  var year = checkStr.substr( 0 , 4 );
  var month = checkStr.substr( 5 , 2 );
  var day = checkStr.substr( 8 , 2 );

  if( month > 0 && month < 13 && day > 0 && day < 32 ) {
    var date = new Date( year ,month -1 , day );
    if( isNaN(date) ) {
      return false;
    } else if( date.getFullYear() == year && date.getMonth() == month - 1  ,date.getDate() == day ){
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters/">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/list_by_ope">定数設定（オペ）</a></li>
        <li>定数編集</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>定数設定</span></h2>
<h2 class="HeaddingMid">検索条件</h2>

<form class="validate_form search_form" action="edit_by_ope" id="inputForm" method="post">
<?php
  echo $this->form->input('cart.tmpid',array('type' => 'hidden'));
  echo $this->form->input('MstFixedCount.readTime',array('value'=> date('Y-m-d H:i:s'), 'type' => 'hidden'));
  echo $this->form->input('MstFixedCount.mst_department_id',array('type' => 'hidden'));
?>
<?php echo $this->form->input('MstFixedCount.is_search' , array('type'=>'hidden'))?>
<?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
<?php echo $this->form->input('MstFacilityItem.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col/>
                    <col/>
                    <col width="20"/>
                    <col/>
                    <col/>
                    <col width="20"/>
                    <col/>
                    <col/>
                </colgroup>
                <tr>
                    <th>施設</th>
                    <td>
                    <?php
                      echo $this->form->text('MstFixedCount.facility_name',array('class' => 'lbl','readOnly' => 'readOnly' ) );
                      echo $this->form->text('MstFixedCount.mst_facility_id',array('type' =>'hidden') );
                    ?>
                    </td>
                    <td></td>
                </tr>
                <?php if(isset($this->request->data['MstFixedCount']['edit']) && $this->request->data['MstFixedCount']['edit'] == "1"){ ?>
                <tr>
                    <th>商品ID</th>
                    <td><?php echo $this->form->text('MstFixedCount.internal_code',array('maxlength' => '50','class'=>'txt search_internal_code')); ?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->text('MstFixedCount.item_code',array('maxlength' => '50','class'=>'txt search_upper')); ?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->text('MstFixedCount.item_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->text('MstFixedCount.dealer_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->text('MstFixedCount.standard',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                    <td></td>
                    <th>ＪＡＮコード</th>
                    <td><?php echo $this->form->text('MstFixedCount.jan_code',array('maxlength' => '50','class'=>'txt')); ?></td>
                    <td></td>
                </tr>
                <?php } ?>
            </table>
            <?php if(isset($this->request->data['MstFixedCount']['edit']) && $this->request->data['MstFixedCount']['edit'] == "1"){ ?>
            <div class="ButtonBox">
                <p class="center">
                  <input type="button" value="" class="btn btn1" id="revealResult" />
                </p>
            </div>
            <?php } ?>
            <hr class="Clear" />
            <div class="DisplaySelect">
            <?php
                echo $this->element('limit_combobox',array('result'=>count($result) ) );
            ?>
            </div>
            <div class="TableScroll2">
                <table class="TableStyle01 table-odd2">
                <tr>
                    <th style="width:20px;" rowspan="2"><input type="checkbox" checked onClick="listAllCheck(this);" /></th>
                    <th class="col10">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col20">製品番号</th>
                    <th style="width:100px;">包装単位</th>
                    <th style="width:70px;" rowspan="2">発注点</th>
                    <th style="width:70px;" rowspan="2">定数</th>
                    <th style="width:70px;" rowspan="2">休日</th>
                    <th style="width:70px;" rowspan="2">予備</th>
                    <th style="width:40px;" rowspan="2">適用</th>
                </tr>
                <tr>
                    <th>棚番号</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>適用開始日</th>
                </tr>
            <?php if(true) { ?>
                <?php
                    if(count($result) !== 0){
                        $i = 0;
                        foreach($result as $r) {
               ?>
                <tr>
                    <td rowspan="2" class="center">
        <?php
        echo $this->form->checkbox('MstFixedCount.id.'.$i,array('checked' =>'checked', 'value'=>(is_null($r['MstFixedCount']['id'])?'a':$r['MstFixedCount']['id']), 'class' =>'center chk' , 'hiddenField' => false) );
        echo $this->form->input('MstFixedCount.mst_item_unit_id.'.$i,array('type'=>'hidden' , 'value'=>$r['MstFixedCount']['mst_item_unit_id']) );
        echo $this->form->input('MstFacilityItem.id.'.$i,array('type'=>'hidden' , 'value'=>$r['MstFacilityItem']['id']) );
        ?>
                    </td>
                    <td><?php echo h_out($r['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_code']);?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['unit_name']); ?></td>
                    <td rowspan="2"><?php echo $this->form->text('MstFixedCount.order_point.'.$i, array('value'=>$r['MstFixedCount']['order_point'],'class'=>'r txt num order_point','maxlength' =>'4')); ?></td>
                    <td rowspan="2"><?php echo $this->form->text('MstFixedCount.fixed_count.'.$i, array('value'=>$r['MstFixedCount']['fixed_count'],'class'=>'r txt num fixed_count','maxlength' =>'4')); ?></td>
                    <td rowspan="2"><?php echo $this->form->text('MstFixedCount.holiday_fixed_count.'.$i, array('value'=>$r['MstFixedCount']['holiday_fixed_count'],'class'=>'r txt num holiday_fixed_count','maxlength' =>'4')); ?></td>
                    <td rowspan="2"><?php echo $this->form->text('MstFixedCount.spare_fixed_count.'.$i, array('value'=>$r['MstFixedCount']['spare_fixed_count'],'class'=>'r txt num spare_fixed_count','maxlength' =>'4')); ?></td>
                    <td rowspan="2" class="center">
                        <input type='checkbox' name='data[MstFixedCount][is_deleted][<?php echo $i?>]' <?php echo (($r['MstFixedCount']['is_deleted'])?'checked':'')?> class="center">
                    </td>
                </tr>
                <tr>
                    <td class="col10"> 
                    <?php
      $val = (isset($r['MstFixedCount']['mst_shelf_name_id'])) ? $r['MstFixedCount']['mst_shelf_name_id'] :"";
      echo $this->form->input('MstFixedCount.mst_shelf_name_id.'.$i, array('options'=>$ShelfList ,'value' => $val,'empty' => true)); 
      ?>
                    </td>
                    <td><?php echo h_out($r['MstFacilityItem']['standard']);?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['dealer_name']);?></td>
                    <td>
                        <?php echo $this->form->text('MstFixedCount.start_date.'.$i, array('value'=>$r['MstFixedCount']['start_date'],'class'=>'r date center start_date')); ?>
                    </td>
                </tr>
      <?php
            $i++;
          }
      ?>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="btn btn2 fix_btn [p2]" id="regist_btn" />
        </div>
      <?php
        }else{
          if($this->request->data['MstFixedCount']['edit'] == "1" && isset($this->request->data['MstFixedCount']['is_search'])){
           echo '<tr><td colspan="10" class="center">該当するデータがありませんでした</td></tr>';
          }
        ?>
    </table>
</div>
    <?php
        }
    }
    ?>
</div>
<?php echo $this->form->end(); ?>
