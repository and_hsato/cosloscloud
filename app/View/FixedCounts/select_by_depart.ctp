<script type="text/javascript">

function anc(id){
   obj = $("#"+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}
function anc2(){
   obj = $("#pageTop")[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#confirmForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );
  
    //検索ボタン押下
    $("#btn_Search").click(function(){
        //カート欄にある全IDを取得
        var tempId = "";
        $("#confirmForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tempId == ''){
                    tempId += $(this).val();
                }else{
                    tempId += ',' + $(this).val();
                }
            }
        });
        $("#tempId").val(tempId);

        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/select_by_depart');
        $("#searchForm").submit();
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        var objArray = new Array();
        var isChecked = false;
        $('#itemSelectedTable').empty();
        $("#searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
                isChecked = true;
            }else{
            }
            cnt++;
        });
        if(!isChecked){
            alert('明細を選択してください');
        }else{
            $(".checkAll_2").attr("checked",true)
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        var tempId = "";
        $("#confirmForm input[type=checkbox].checkAllTarget").each(function(){
          tempId += $(this).val() + ",";
        });
        $("#temp_id").val(tempId);
        //選択チェック
        var cnt = 0;
        var objArray = new Array();
        $("#confirmForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
            }
        });
        if(cnt === 0){
            alert('確認する明細を選択してください');
        }else{
            $("#confirmForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_by_depart');
            $("#confirmForm").submit();
        }
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot;?>masters/">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/list_by_depart">定数設定(部署別)</a></li>
        <li>採用品選択</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>採用品選択</span></h2>
<h2 class="HeaddingMid">検索条件</h2>
<form class="validate_form search_form" id="searchForm" method="post" action="select_by_depart">
    <?php echo $this->form->input('MstFixedCount.add_search',array('type' => 'hidden')); ?>
    <?php echo $this->form->input('MstFixedCount.tempId',array('type'=>'hidden','id' =>'tempId'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('MstFixedCount.facility_name',array('class' => 'lbl','readOnly' => 'readOnly' ) ); ?>
                    <?php echo $this->form->input('MstFixedCount.mst_facility_id',array('type' =>'hidden' ) ); ?>
                </td>
                <td></td>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('MstFixedCount.department_name',array('class' => 'lbl','readOnly' => 'readOnly' ) ); ?>
                    <?php echo $this->form->input('MstFixedCount.mst_department_id',array('type' =>'hidden',) ); ?>
                </td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFixedCount.internal_code',array('maxlength' => '50','class'=>'txt search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFixedCount.item_code',array('maxlength' => '50','class'=>'txt search_upper')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFixedCount.item_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFixedCount.dealer_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFixedCount.standard',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                <td></td>
                <th>ＪＡＮコード</th>
                <td><?php echo $this->form->text('MstFixedCount.jan_code',array('maxlength' => '50','class'=>'txt')); ?></td>
                <td></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" value="" class="btn btn1" id="btn_Search" />
            </p>
        </div>
        <hr class="Clear" />
        <div class="DisplaySelect" id ="pageDow">
            <div align="right" style="padding-right: 10px;"></div>
            <?php
                //件数表示
                echo $this->element('limit_combobox',array('result'=>count($result) ) );
            ?>
        </div>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02" >
                <tr>
                    <th style="width:20px;" rowspan="2">
                        <input type="checkbox" class="checkAll_1" />
                    </th>
                    <th class="col10">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col20">規格</th>
                    <th class="col15">製品番号</th>
                    <th class="col20">販売元</th>
                    <th class="col15">JANコード</th>
                </tr>
            </table>
        </div>
        <?php if(isset($this->request->data['MstFixedCount']['add_search']) ){ ?>
        <div class="" style="padding-bottom:5px;">
                <?php
                if(count($result) === 0 ){ ?>
           <table class="TableStyle02"><tr><td colspan="7" class="center" >該当するデータがありませんでした</tr></table>
           <?php }else{
                  $i = 0;
                  foreach($result as $r){
                ?>
            <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$r['MstFixedCount']['id'];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
                <tr class="<?php echo ($i%2==0)?'':'odd'; ?>" id="itemSelectedRow<?php echo $r['MstFixedCount']['id'] ?>">
                    <td style="width:20px;" class="center">
                        <?php echo $this->form->checkbox('MstFacilityItem.id.'.$r['MstFixedCount']['id'], array('id'=>"MstFacilityItemId{$r['MstFixedCount']['id']}", 'class'=>'center checkAllTarget','value'=>$r['MstFixedCount']["id"] , 'hiddenField'=>false) ); ?>
                    </td>
                    <td class="col10"><?php echo h_out($r['MstFixedCount']['internal_code'],'center'); ?></td>
                    <td class="col20"><?php echo h_out($r['MstFixedCount']['item_name']); ?></td>
                    <td class="col20"><?php echo h_out($r['MstFixedCount']['standard']); ?></td>
                    <td class="col15"><?php echo h_out($r['MstFixedCount']['item_code']); ?></td>
                    <td class="col20"><?php echo h_out($r['MstFixedCount']['dealer_name']); ?></td>
                    <td class="col15"><?php echo h_out($r['MstFixedCount']['jan_code']); ?></td>
                </tr>
            </table>
                <?php
                    $i++;
                  }
                }
            ?>

            </div>
            <div class="ButtonBox">
                <input type="button" class="btn btn4 fix_btn" id="cmdSelect" />
            </div>
        <?php } ?>
    </div>
<?php echo $this->form->end(); ?>
<?php echo $this->form->create( 'fixedCounts',array('type' => 'post','action' => '' ,'id' =>'confirmForm','class' => 'validate_form') ); ?>

<?php echo $this->form->input('search.temp_id',array('type'=>'hidden','id' =>'temp_id'));?>
<?php echo $this->form->input('MstFixedCount.facility_name',array('type' => 'hidden') ); ?>
<?php echo $this->form->input('MstFixedCount.mst_facility_id',array('type' =>'hidden' ) ); ?>
<?php echo $this->form->input('MstFixedCount.department_name',array('type' => 'hidden') ); ?>
<?php echo $this->form->input('MstFixedCount.mst_department_id',array('type' =>'hidden') ); ?>
            <h2 class="HeaddingLarge2">選択商品</h2>
            <div class="TableHeaderAdjustment01">
                <table class="TableHeaderStyle02">
                <tr>
                  <th style="width:20px;" rowspan="2">
                  <input type="checkbox" class="checkAll_2" checked />
                  </th>
                  <th class="col10">商品ID</th>
                  <th class="col20">商品名</th>
                  <th class="col20">規格</th>
                  <th class="col15">製品番号</th>
                  <th class="col20">販売元</th>
                  <th class="col15">JANコード</th>
                </tr>
                </table>
            </div>
            <div class="" id="pageTop" style="padding-bottom: 5px;">
                <?php
                  $i = 0;
                  foreach($cart as $r){
                 ?>
                  <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$r['MstFixedCount']['id'];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
                <?php
                    $idN = 'itemSelectedRow'.$r['MstFixedCount']['id'];
                    if($i % 2){
                      echo "<tr class=\"odd\" id=\"{$idN}\"  >";
                    }else{
                      echo "<tr id=\"{$idN}\">";
                    }
                ?>
                    <td style="width:20px;" class="center">
                      <?php echo $this->form->checkbox('MstFacilityItem.id.'.$r['MstFixedCount']['id'] , array('id'=>"MstFacilityItemId{$r['MstFixedCount']["id"]}", 'class'=>'center checkAllTarget','value'=>$r['MstFixedCount']["id"],'checked'=>'checked' , 'hiddenField'=>false) ); ?>
                    </td>
                    <td class="col10"><?php echo h_out($r['MstFixedCount']['internal_code'],'center'); ?></td>
                    <td class="col20"><?php echo h_out($r['MstFixedCount']['item_name']); ?></td>
                    <td class="col20"><?php echo h_out($r['MstFixedCount']['standard']); ?></td>
                    <td class="col15"><?php echo h_out($r['MstFixedCount']['item_code']); ?></td>
                    <td class="col20"><?php echo h_out($r['MstFixedCount']['dealer_name']); ?></td>
                    <td class="col15"><?php echo h_out($r['MstFixedCount']['jan_code']); ?></td>
                </tr>
                <?php $i++; } ?>
                </table>
            </table>
            <div id="itemSelectedTable"></div>
            </div>
            <div class="ButtonBox">
              <input type="button" class="btn btn3 fix_btn" id="btn_Confirm" />
            </div>
            <div align="right"></div>
    </div>
<?php echo $this->form->end(); ?>
