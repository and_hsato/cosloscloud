<script type="text/javascript">
$(function(){
    $(document).ready(function(){
        //チェックボックス一括変更
        $('.checkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
            getTotalPrice();
        });
        $('.checkAllTarget').click(function(){
            //合計金額
            getTotalPrice();
        });
        
        $('#cmdSetRemainCount').click(function(){
            $('input[type=text].quantity').each(function(){
                $(this).val($(this).parent(':eq(0)').find('input[type=hidden]:eq(0)').val());
                var index = $('input[type=text].quantity').index(this);
                q = $('input[type=text].quantity:eq(' + index + ')').val();
                p = $('input[type=text].stocking_price:eq(' + index + ')').val();
                if(q.match(/[0-9]+/) && p.match(/^(0|[1-9]\d*|[0-9]\.[0-9]{1,2}|[1-9]\d+\.[0-9]{1,2})$/)){
                    // 計算
                    $('p.calc_price:eq(' + index + ')').text( numberFormat(p * q) );
                }else{
                    $('p.calc_price:eq(' + index + ')').text('');
                }
            });
            //合計金額
            getTotalPrice();
        });
        $('#cmdSetUnitPrice').click(function(){
            $('input[type=text].stocking_price').each(function(){
                $(this).val($(this).parent(':eq(0)').find('input[type=hidden]:eq(0)').val());
                var index = $('input[type=text].stocking_price').index(this);
                q = $('input[type=text].quantity:eq(' + index + ')').val();
                p = $('input[type=text].stocking_price:eq(' + index + ')').val();
                if(q.match(/[0-9]+/) && p.match(/^(0|[1-9]\d*|[0-9]\.[0-9]{1,2}|[1-9]\d+\.[0-9]{1,2})$/)){
                    // 計算
                    $('p.calc_price:eq(' + index + ')').text( numberFormat(p * q) );
                }else{
                    $('p.calc_price:eq(' + index + ')').text('');
                }
            })
            //合計金額
            getTotalPrice();
        });

        // フォーカスが外れたら参考価格を計算する。
        $('input[type=text].r').blur( function () {
            if($(this).hasClass('quantity')){
                var index = $('input[type=text].quantity').index(this);
            }
            if($(this).hasClass('stocking_price')){
                var index = $('input[type=text].stocking_price').index(this);
            }
            if($(this).hasClass('calc_price')){
                var index = $('input[type=text].calc_price').index(this);
            }
            q = $('input[type=text].quantity:eq(' + index + ')').val();
            p = $('input[type=text].stocking_price:eq(' + index + ')').val();
 
            if(q.match(/[0-9]+/) && p.match(/^(0|[1-9]\d*|[0-9]\.[0-9]{1,2}|[1-9]\d+\.[0-9]{1,2})$/)){
                // 計算
                $('p.calc_price:eq(' + index + ')').text( numberFormat(p * q) );
            }else{
                $('p.calc_price:eq(' + index + ')').text('');
            }
            //合計金額を取得
            getTotalPrice();
        });
  
        $('#cmdConfirm').click(function(){
            var canCommit = true;
            var cnt = 0;

            $('input[type=checkbox].checkAllTarget').each(function(i){
                if(true === this.checked){
                    cnt++;

                    //数量
                    var q = $('input[type=text].quantity:eq(' + i + ')');
                    if(q.val == ''){
                        alert('数量は必須入力です');
                        canCommit = false;
                        return false;
                    }else if(!q.val().match(/[1-9][0-9]*/)){
                        alert('数量を正しく入力してください');
                        canCommit = false;
                        return false;
                    }else if(q.val().match(/[^0-9]/g)){
                        alert('数量を正しく入力してください');
                        canCommit = false;
                        return false;
                    }else if(parseInt(q.find('~ input[type=hidden]').val()) < parseInt(q.val())){
                        alert('残数を超えた仕入数量が入力されています');
                        canCommit = false;
                        return false;
                    }

                    //単価
                    q = $('input[type=text].stocking_price:eq(' + i + ')').val();
                    if(q == ''){
                        alert('仕入単価は必須入力です');
                        canCommit = false;
                        return false;
                    }else if(!q.match(/^(0|[1-9]\d*|[0-9]\.[0-9]{1,2}|[1-9]\d+\.[0-9]{1,2})$/)){
                        alert('仕入単価を正しく入力してください');
                        canCommit = false;
                        return false;
                    }
                }
            });

            if(cnt === 0){
                alert('入荷登録を行う明細を選択してください');
                return false;
            }

            if(canCommit === true){
                $(window).unbind('beforeunload');
                $('#trn_receiving_search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/results').submit();
            }else{
                return false;
            }
        });
    });
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });

});
    //合計金額を取得
    function getTotalPrice(){
        var total_price = 0;
        $('input[type=checkbox].checkAllTarget').each(function(i){
            if(true === this.checked){
                tmp = $(this).parent(':eq(0)').parent(':eq(0)').next(':eq(0)').find('.calc_price').text();
                if(tmp != ''){
                    total_price = total_price + parseInt(tmp.replaceAll(",", ""));
                }
            }
        });
        $('#total_price').text(numberFormat(total_price));
    }
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">未入荷一覧</a></li>
        <li>入荷確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入荷確認</span></h2>
<h2 class="HeaddingSmall">入荷登録を行う明細を選択してください</h2>
<?php echo $this->form->create('TrnReceiving',array('action' =>'/receivings/results','type'=>'post','id'=>'trn_receiving_search_form','class'=>'validate_form')); ?>
    <?php echo $this->form->input('TrnReceiving.token' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('TrnReceiving.time' , array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s')))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>入荷日</th>
                <td><?php echo $this->form->text('TrnReceivingHeader.work_date',array('class'=>'date validate[required,custom[date]]','maxlength'=>'10','id'=>'work_date','label'=>'')); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->text('TrnReceivingHeader.recital',array('class'=>'txt','label'=>'',"maxlength"=>200)); ?></td>
            </tr>
        </table>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect">
        </span>
        <span class="BikouCopy">
            <input type="button" class="btn btn47 [p2]" id="cmdSetUnitPrice"/>
            <input type="button" class="btn btn61 [p2]" id="cmdSetRemainCount"/>
        </span>
    </div>
    <div class="SearchBox">
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2">
                <thead>
                <tr>
                    <th style="width: 20px;" rowspan="2" align="center"><input type="checkbox" class="checkAll" /></th>
                    <th style="width:130px;">発注番号</th>
                    <th style="width:90px;">発注日</th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col10"></th>
                    <th class="col10">遡及除外</th>
                    <th class="col10">作業区分</th>
                </tr>
                <tr>
                    <th colspan="2">仕入先</th>
                    <th>区分</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>数量</th>
                    <th>合計金額</th>
                    <th>備考</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=0;foreach($SearchResult as $_row): ?>
                <tr>
                    <td rowspan="2" style="width:20px;" align="center"><?php echo $this->form->checkbox("TrnReceiving.id.{$_row['TrnOrder']['id']}", array('class'=>'center checkAllTarget','value'=>$_row['TrnOrder']['id'], 'hiddenField'=>false)); ?></td>
                    <td align="center" style="width:130px;"><?php echo h_out($_row['TrnOrder']['work_no'], 'center'); ?></td>
                    <td align="center" style="width:90px;"><?php echo h_out($_row['TrnOrder']['work_date'] , 'center'); ?></p></label></td>
                    <td class="col10" align="center"><?php echo h_out($_row['TrnOrder']['internal_code'], 'center'); ?></td>
                    <td><?php echo h_out($_row['TrnOrder']['item_name']); ?></td>
                    <td><?php echo h_out($_row['TrnOrder']['item_code']); ?></td>
                    <td class="col10"><?php echo h_out($_row['TrnOrder']['unit_name']); ?></td>
                    <td class="col10"></td>
                    <td class="col10" align="center"><?php echo $this->form->checkbox("TrnReceiving.IsExclude.{$_row['TrnOrder']['id']}", array('class'=>'center')); ?></td>
                    <td class="col10"><?php echo $this->form->input("TrnReceiving.work_type.{$_row['TrnOrder']['id']}",array('type'=>'select' ,'options'=>$classes, 'empty'=>true, 'class'=>'txt')); ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($_row['TrnOrder']['facility_name']); ?></td>
                    <td><?php echo h_out($_row['TrnOrder']['order_type_name'],'center'); ?></td>
                    <td><?php echo h_out($_row['TrnOrder']['standard']); ?></td>
                    <td><?php echo h_out($_row['TrnOrder']['dealer_name']); ?></td>
                    <td>
                        <?php echo($this->form->input("TrnReceiving.stocking_price.{$_row['TrnOrder']['id']}", array('class'=>'r txt stocking_price','style' => 'text-align:right;width:90%;','label'=>'', "maxlength"=>12 )));  ?>
                        <?php echo($this->form->input("TrnReceiving.transaction_price.{$_row['TrnOrder']['id']}", array('type'=>'hidden','value'=>$_row['TrnOrder']['transaction_price']))); ?>
                        <?php echo($this->form->input("TrnReceiving.facility_id.{$_row['TrnOrder']['id']}", array('type'=>'hidden','value'=>$_row['TrnOrder']['facility_id']))); ?>
                        <?php echo($this->form->input("TrnReceiving.trn_order_header_id.{$_row['TrnOrder']['id']}", array('type'=>'hidden','value'=>$_row['TrnOrder']['trn_order_header_id']))); ?>
                        <?php echo($this->form->input("TrnReceiving.mst_item_unit_id.{$_row['TrnOrder']['id']}", array('type'=>'hidden','value'=>$_row['TrnOrder']['mst_item_unit_id']))); ?>
                        <?php echo($this->form->input("TrnReceiving.is_lowlevel.{$_row['TrnOrder']['id']}", array('type'=>'hidden','value'=>$_row['TrnOrder']['is_lowlevel']))); ?>
                        <?php echo($this->form->input("TrnReceiving.order_type.{$_row['TrnOrder']['id']}", array('type'=>'hidden','value'=>$_row['TrnOrder']['order_type']))); ?>
                    </td>
                    <td>
                        <?php echo($this->form->input("TrnReceiving.quantity.{$_row['TrnOrder']['id']}", array('class'=>'r txt quantity', 'style' => 'text-align:right;width:90%;', 'label'=>'', "maxlength"=>10 )));  ?>
                        <?php echo($this->form->input("TrnReceiving.remain_count.{$_row['TrnOrder']['id']}", array('type'=>'hidden','value'=>$_row['TrnOrder']['remain_count']))); ?>
                    </td>
                    <td>
                       <p style="text-align:right;" class='calc_price'></p>
                    </td>
                    <td><?php echo($this->form->input("TrnReceiving.recital.{$_row['TrnOrder']['id']}", array('class'=>'txt', 'style'=>'width:90%;','label'=>'', "maxlength"=>200))); ?></td>
                </tr>
                <?php $i++;endforeach; ?>
                </tbody>
            </table>
        </div>
        <table class="TableStyle01 table-even2">
                <tr>
                    <td colspan="7"></td>
                    <td class="center" style="font-weight:bold;" >合計金額</td>
                    <td><p style="text-align:right;" id="total_price"></p></td>
                    <td></td>
                </tr>
        </table>
        <?php if(true === $IsShowFinal): ?>
        <div class="ButtonBox">
            <p class="center"><input type="button" class="btn btn2 submit [p2]" id="cmdConfirm"/></p>
        </div>
        <?php endif; ?>
    </div>
<?php echo $this->form->end(); ?>
