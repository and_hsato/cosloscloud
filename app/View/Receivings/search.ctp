<script type="text/javascript">
    $(document).ready(function(){
        $('#shippingNo').val('').focus();
  
        $('#search').click(function(){
            $('#order_search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').submit();
        });

        // チェックボックス制御
        $('.checkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
        });
        $('#cmdConfirm').click(function(){
            var _cnt = 0;
            if($('input[type=checkbox].checkAllTarget:checked').length == 0 ){
                alert('変更を行う明細を選択してください');
            }else{
                $('#order_search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm').submit();
            }
        });
        $('#orderNo').keyup(function(e){
            if(e.which === 13){
                $('#search').click();
            }
        });
  
        $('#shippingNo').keyup(function(e){
            if(e.which === 13){
                if($('#shippingNo').val().length > 0){
                    $('#order_search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm/2').submit();
                }
            }
        });

  
    });
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>入荷登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>未入荷一覧</span></h2>
<h2 class="HeaddingMid">未入荷一覧</h2>
    <form class="validate_form search_form" id="order_search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search" method="POST">
        <input type="hidden" name="data[IsSearch]" value="1" />
        <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>業者出荷番号</th>
                <td><?php echo($this->form->text('TrnEdiShipping.work_no', array('class'=>'txt', 'id' => 'shippingNo','maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th></th>
                <td colspan="3"></td>
            </tr>
            <tr>
                <th>発注番号</th>
                <td><?php echo($this->form->text('TrnOrderHeader.work_no', array('class'=>'txt', 'id' => 'orderNo','maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>発注日</th>
                <td colspan="3">
                    <?php echo $this->form->input('TrnOrderHeader.order_date_from', array('class'=>'date txt validate[custom[date]]', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('TrnOrderHeader.order_date_to', array('class'=>'date txt validate[custom[date]]', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                </td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo($this->form->input('MstFacility.facility_name', array('type'=>'hidden' , 'id'=>'facilityName'))); ?>
                    <?php echo($this->form->input('MstFacility.facility_text', array('type'=>'text','class'=>'txt require', 'style'=>'width:60px;text-align:right;', 'label'=>'','id'=>'facilityText'))); ?>
                    <?php echo($this->form->input('MstFacility.facility_code', array('type'=>'select' , 'options'=>$facilities,'class' => 'txt','empty' => true , 'id'=>'facilityCode'))); ?>
                </td>
                <td></td>
                <th>APコード</th>
                <td><?php echo($this->form->input('MstFacilityItem.aptage_code', array('type'=>'text','class'=>'txt', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo($this->form->input('MstFacilityItem.internal_code', array('type'=>'text','class'=>'txt search_internal_code', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo($this->form->input('MstFacilityItem.item_code', array('type'=>'text','class'=>'txt search_upper', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td width="20"></td>
                <th>発注区分</th>
                <td><?php echo $this->form->input('TrnOrderHeader.order_type',array('type'=>'select' , 'options'=>$orderTypes, 'empty'=>true, 'class'=>'txt','id'=>'orderTypeSel')); ?></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo($this->form->input('MstFacilityItem.item_name', array('type'=>'text','class'=>'txt search_canna', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo($this->form->input('MstDealer.dealer_name', array('type'=>'text','class'=>'txt search_canna', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('MstClass.id',array('type'=>'select' , 'options'=>$classes, 'empty'=>true, 'class'=>'txt','id'=>'classTypeSel')); ?></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo($this->form->input('MstFacilityItem.standard', array('type'=>'text','class'=>'txt search_canna', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th></th>
                <td><?php echo($this->form->checkbox('TrnEdiShipping.isEdiShipping', array('label' => false , 'hiddenField'=>false))); ?>業者出荷済みのみ表示</td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
        </table>

                <div class="ButtonBox">
                    <p class="center">
                        <input type="button" class="btn btn1" id="search" />
                    </p>
                </div>

                <hr class="Clear" />
        <div id="results">
                <h2 class="HeaddingSmall">変更を行いたい発注履歴を選択、クリックしてください。</h2>
                <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
                <div class="TableScroll">
                    <table class="TableStyle01 table-odd">
                        <tr>
                            <th style="width:20px;"><input type="checkbox" class="checkAll" checked="checked"/></th>
                            <th style="width: 140px;">発注番号</th>
                            <th style="width: 80px;">発注日</th>
                            <th class="col20">仕入先</th>
                            <th class="col10">登録者</th>
                            <th style="width: 150px;">登録日時</th>
                            <th>備考</th>
                            <th class="col10">発注件数</th>
                        </tr>
                        <?php $_cnt=0;foreach($SearchResult as $_row): ?>
                        <tr>
                            <td class="center"><?php echo $this->form->checkbox("TrnOrderHeader.Rows.{$_row['trn_order_header_id']}", array('class'=>'center checkAllTarget','checked' => 'checked')); ?></td>
                            <td><?php echo h_out($_row['work_no'] , 'center'); ?></td>
                            <td><?php echo h_out($_row['work_date'] , 'left'); ?></td>
                            <td><?php echo h_out($_row['facility_name']); ?></td>
                            <td><?php echo h_out($_row['user_name']);?></td>
                            <td><?php echo h_out($_row['created'] , 'center');?></td>
                            <td><?php echo h_out($_row['recital']);?></td>
                            <td><?php echo h_out($_row['receivable_detail_count'] . " / " . $_row['detail_count'] , 'right');?></td>
                        </tr>
                        <?php $_cnt++;endforeach;?>
                        <?php if(count($SearchResult) === 0 && isset($this->request->data['IsSearch'])){ ?>
                        <tr><td colspan="8" class="center"><span>該当データがありません。</span></td></tr>
                        <?php } ?>
                    </table>
                </div>

                <?php if(count($SearchResult) > 0){ ?>
                <div class="ButtonBox">
                    <p class="center">
                        <input type="button" class="btn btn3 confirm_btn" id="cmdConfirm" />
                    </p>
                </div>
                <?php } ?>

        </div>
    </div>
</form>
