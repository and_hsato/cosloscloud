<script type="text/javascript">
    $(document).ready(function(){
        $('input[type=text].readonly, input[type=text].lbl').attr('readonly','readonly');
        $('#costStickerPrint').click(function(){
            $('#resultForm').attr('action', '<?php echo $this->webroot ?><?php echo $this->name; ?>/seal').submit();
        });
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">未入荷一覧</a></li>
        <li class="pankuzu">入荷確認</li>
        <li>入荷結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入荷結果</span></h2>
<div class="Mes01">以下の入荷を行いました</div>

<form class="validate_form" id="resultForm" action="#" method="POST">
    <?php foreach($TrnReceivingIds as $i => $id){ ?>
    <?php echo $this->form->input('TrnReceivingHeader.id.'.$i , array('type' => 'hidden' , 'value' => $id )); ?>
    <?php } ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
</form>
<div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
        </colgroup>
        <tr>
            <th>入荷日</th>
            <td><?php echo $this->form->text('TrnReceivingHeader.work_date',array('class'=>'lbl')); ?></td>
            <td></td>
            <th>備考</th>
            <td><?php echo $this->form->text('TrnReceivingHeader.recital',array('class'=>'lbl')); ?></td>
        </tr>
    </table>
</div>
<div class="TableScroll2">
    <table class="TableStyle01 table-even2">
      <thead>
        <tr>
            <th style="width:135px;">発注番号</th>
            <th style="width:95px;">発注日</th>
            <th class="col10">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th class="col10">包装単位</th>
            <th class="col10">遡及除外</th>
            <th class="col10">作業区分</th>
        </tr>
        <tr>
            <th colspan="2">仕入先</th>
            <th>区分</th>
            <th>規格</th>
            <th>販売元</th>
            <th>仕入単価</th>
            <th>数量</th>
            <th>備考</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 0;foreach ($this->request->data['results'] as $_row): ?>
            <tr>
                <td><?php echo h_out($_row['TrnOrder']['work_no'],'center'); ?></td>
                <td><?php echo h_out($_row['TrnOrder']['work_date'],'center'); ?></td>
                <td><?php echo h_out($_row['TrnOrder']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($_row['TrnOrder']['item_name']); ?></td>
                <td><?php echo h_out($_row['TrnOrder']['item_code']); ?></td>
                <td><?php echo h_out($_row['TrnOrder']['unit_name']); ?></td>
                <td><?php echo h_out((($this->request->data['TrnReceiving']['IsExclude'][$_row['TrnOrder']['id']]) == '1' ? '○' : ''),'center'); ?></td>
                <td><?php echo h_out($this->request->data['TrnReceiving']['work_type'][$_row['TrnOrder']['id']]);?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo h_out($_row['TrnOrder']['facility_name']); ?></td>
                <td><?php echo h_out($_row['TrnOrder']['order_type_name'] , 'center'); ?></td>
                <td><?php echo h_out($_row['TrnOrder']['standard']); ?></td>
                <td><?php echo h_out($_row['TrnOrder']['dealer_name']); ?></td>
                <td><?php echo h_out($this->request->data['TrnReceiving']['stocking_price'][$_row['TrnOrder']['id']] , 'right');?></td>
                <td><?php echo h_out($this->request->data['TrnReceiving']['quantity'][$_row['TrnOrder']['id']],'right'); ?></td>
                <td><?php echo h_out($this->request->data['TrnReceiving']['recital'][$_row['TrnOrder']['id']]); ?></td>
            </tr>
        <?php $i++;endforeach; ?>
   </tbody>
    </table>
</div>
<?php if(count($TrnReceivingIds) > 0){ ?>
<div class="ButtonBox">
    <input type="button" class="btn btn12 fix_btn" id="costStickerPrint" />
</div>
<?php } ?>
