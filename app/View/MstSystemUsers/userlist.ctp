<script>
$(document).ready(function(){
    // 検索ボタン押下
    $("#btn_Search").click(function(){
        validateDetachSubmit($('#UserList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist/' );
    });

    //新規ボタン押下
    $("#btn_Add").click(function(){
        validateDetachSubmit($('#UserList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add' );
    });

    //編集ボタン押下
    $("#btn_Mod").click(function(){
        $("#UserList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
    });

});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>ユーザ一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">ユーザ一覧</h2>

<form class="validate_form search_form" action='<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist/' method="post" id="UserList">
    <?php echo $this->form->input('MstUser.is_search' , array('type'=>'hidden'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>ユーザID</th>
                <td><?php echo $this->form->input('MstUser.search_login_id' , array('type'=>'text' , 'class'=>'txt'))?></td>
                <td></td>
                <th>所属施設</th>
                <td>
                    <?php echo $this->form->input('MstUser.search_facility_nm',array('type'=>'hidden','id' => 'facilityName')); ?>
                    <?php echo $this->form->input('MstUser.search_facility_cd',array('id' => 'facilityText', 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('MstUser.search_facility_id',array('options'=>$facility_enabled,'id' => 'facilityCode', 'class' => 'txt' , 'empty'=>'')); ?>
                </td>
                <th>&nbsp;</th>
                <td><?php echo $this->form->input('MstUser.search_is_deleted',array('type'=>'checkbox', 'hiddenField'=>false)); ?>削除も表示する</td>
            </tr>
            <tr>
                <th>ユーザ名</th>
                <td><?php echo $this->form->input('MstUser.search_user_name' , array('type'=>'text' , 'class'=>'txt search_canna'))?></td>
                <td></td>
                <th>ロール</th>
                <td><?php echo $this->form->input('MstUser.search_role_id' , array('options'=>$Role_List , 'class'=>'txt' , 'empty'=>''))?></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>
        <input type="button" class="btn btn11 [p2]" id="btn_Add"/>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <?php echo $this->element('limit_combobox',array('result'=>count($User_List))); ?>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even" id="userlistTable">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col width="50"/>
                </colgroup>
                <thead>
                <tr>
                    <th></th>
                    <th>ユーザID</th>
                    <th>ユーザ名</th>
                    <th>所属</th>
                    <th>ロール</th>
                    <th>削除</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($User_List as $user) { ?>
                <tr>
                    <td class="center"><input name="data[MstUser][id]" type="radio" class="validate[required]" id="user<?php echo $user['MstUser']['id']; ?>" value="<?php echo $user['MstUser']['id']; ?>" /></td>
                    <td><?php echo h_out($user['MstUser']['login_id']); ?></td>
                    <td><?php echo h_out($user['MstUser']['user_name']); ?></td>
                    <td><?php echo h_out($user['MstFacility']['facility_name']); ?></td>
                    <td><?php echo h_out($user['MstRole']['role_name']); ?></td>
                    <td><?php echo h_out($user['MstUser']['is_deleted'],'center');  ?></td>
                </tr>
                <?php } ?>
                </tbody>
                <?php if(isset($this->request->data['MstUser']['is_search']) && count($User_List) == 0){ ?>
                <tr>
                    <td colspan="5" class="center">該当するデータがありませんでした。</td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
    <?php if(count($User_List) > 0 ){ ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn9" id="btn_Mod"/>
    </div>
    <?php } ?>
</form>
