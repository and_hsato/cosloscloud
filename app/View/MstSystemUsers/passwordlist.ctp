<script>
$(document).ready(function(){
    $('table.TableStyle01 > tbody > tr:odd').addClass('odd');
  
    // 検索ボタン押下
    $("#btn_Search").click(function(){
        validateDetachSubmit($('#PasswordList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/passwordlist/' );
    });

    //CSVボタン押下
    $("#btn_Csv").click(function(){
        //バリデート無効化 && ぐるぐる表示 && submit!!
        $('#PasswordList').validationEngine('detach');
        $("#PasswordList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        $('#msg').text('CSV出力中です。しばらくお待ちください。');
        $(this).attr('disabled' , 'disabled');
        $('.tooltip').hide();
        $("#PasswordList").validationEngine({
            onValidationComplete : function( form , status ){
                if(status == true ) {
                    if(!form.attr('action').match(/csv|export|report|seal|beep|download/ig)){
                        dclick_flg = true;
                        setTimeout(function(){$('#waitingPanel').click()},100);
                        $('input[type=button]').attr('disabled' , 'disabled');
                    }
                } else {
                    tb_remove();
                }
            }
        });
        $("#PasswordList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/passwordlist/');
    });
  
    //施設のプルダウンを変更したら、施設コードも変更する
    $("#SearchUserFacilityId").change(function () {
        $('#SearchUserFacilityCd').val($('#SearchUserFacilityId option:selected').val());
    });

    //施設コードを入力したら、施設のプルダウンも変更する。
    $('#SearchUserFacilityCd').change(function(){
        $('#SearchUserFacilityId').selectOptions($(this).val());
        $('#SearchUserFacilityId').select();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>パスワード更新状況一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">パスワード更新状況一覧</h2>
<div id="msg" class="err" style="font-size:20px"></div>
<form class="validate_form search_form" action='<?php echo $this->webroot; ?><?php echo $this->name; ?>/passwordlist/' method="post" id="PasswordList">
    <?php echo $this->form->input('MstUser.is_search' , array('type'=>'hidden'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>ユーザID</th>
                <td><?php echo $this->form->input('MstUser.search_login_id' , array('type'=>'text' , 'class'=>'txt'))?></td>
                <td></td>
                <th>所属施設</th>
                <td>
                    <?php echo $this->form->input('MstUser.search_facility_cd',array('id' => 'SearchUserFacilityCd', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('MstUser.search_facility_id',array('options'=>$facility_enabled,'id' => 'SearchUserFacilityId', 'class' => 'txt' , 'empty'=>'')); ?>
                </td>
                <td></td>
                <th>&nbsp;</th>
                <td><?php echo $this->form->input('MstUser.search_is_deleted',array('type'=>'checkbox', 'hiddenField'=>false)); ?>削除も表示する</td>
                <td></td>
                <th>現在の更新周期</th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>ユーザ名</th>
                <td><?php echo $this->form->input('MstUser.search_user_name' , array('type'=>'text' , 'class'=>'txt search_canna'))?></td>
                <td></td>
                <th>ロール</th>
                <td><?php echo $this->form->input('MstUser.search_role_id' , array('options'=>$Role_List , 'class'=>'txt' , 'empty'=>''))?></td>
                <td></td>
                <th>&nbsp;</th>
                <td><?php echo $this->form->input('MstUser.search_is_update',array('type'=>'checkbox', 'hiddenField'=>false)); ?>更新済みユーザも表示する</td>
                <td></td>
                <th>毎年 <?php echo $Update_Cycle?></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th></th>
                <td></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
                <th>&nbsp;</th>
                <td><?php echo $this->form->input('MstUser.search_is_temp',array('type'=>'checkbox', 'hiddenField'=>false)); ?>仮パスワード発行中ユーザも表示する</td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <?php echo $this->element('limit_combobox',array('result'=>count($User_List) )); ?>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 userlistTable" id="userlistTable">
                <thead>
                <tr>
                    <th rowspan="2">ユーザID</th>
                    <th rowspan="2">ユーザ名</th>
                    <th rowspan="2">所属</th>
                    <th rowspan="2">ロール</th>
                    <th colspan="3">パスワード</th>
                    <th rowspan="2" class="col5">削除</th>
                </tr>
                <tr>
                    <th class="col5">仮</th>
                    <th class="col5">更新</th>
                    <th class="col10">更新日</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($User_List as $user) { ?>
                <tr class="<?php echo ($user['MstUser']['update_check']==''&&$user['MstUser']['is_deleted']=='')?'alert':''  ?>" >
                    <td><?php echo h_out($user['MstUser']['login_id']); ?></td>
                    <td><?php echo h_out($user['MstUser']['user_name'],'center'); ?></td>
                    <td><?php echo h_out($user['MstFacility']['facility_name']); ?></td>
                    <td><?php echo h_out($user['MstRole']['role_name']); ?></td>
                    <td><?php echo h_out($user['MstUser']['is_temp_issue'],'center'); ?></td>
                    <td><?php echo h_out($user['MstUser']['update_check'],'center'); ?></td>
                    <td><?php echo h_out($user['MstUser']['user_name'],'center'); ?></td>
                    <td><?php echo h_out($user['MstUser']['is_deleted'],'center');  ?></td>
                </tr>
                <?php } ?>
                </tbody>
                <?php if(isset($this->request->data['MstUser']['is_search']) && count($User_List) == 0){ ?>
                <tr>
                    <td colspan="8" class="center">該当するデータがありませんでした。</td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
    <?php if(count($User_List) > 0 ){ ?>
    <div class="ButtonBox">
        <input type="button" value="　ユーザ一覧CSV出力　" id="btn_Csv"/>
    </div>
    <?php } ?>
</form>
