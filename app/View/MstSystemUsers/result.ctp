<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist">ユーザ一覧</a></li>
        <li class="pankuzu">ユーザ編集</li>
        <li>ユーザ編集結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">ユーザ編集結果</h2>

<?php if(isset($this->request->data['MstUser']['is_deleted'])){ ?>
<div class="Mes01">データを削除しました</div>
<?php }else{ ?>
<div class="Mes01">以下の内容で設定しました</div>
<?php } ?>

<form method="post" id="">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>ユーザID</th>
                <td>
                    <?php echo $this->form->input('MstUser.login_id' , array('type'=>'text' , 'class'=>'lbl' ,'readonly'=>'readonly')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>ユーザ名</th>
                <td>
                    <?php echo $this->form->input('MstUser.user_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>所属施設デフォルト設定</th>
                <td>
                    <?php echo $this->form->input('MstFacility.centerName' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>カタログ施設設定</th>
                <td>
                    <?php echo $this->form->input('MstFacility.facilityName' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>カタログ部署設定</th>
                <td>
                    <?php echo $this->form->input('MstDepartment.departmentName' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>ロール</th>
                <td>
                    <?php echo $this->form->input('MstRole.role_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <h2 class="HeaddingSmall">利用可能施設</h2>
    <div class="TableScroll">
        <table class="TableStyle01 table-even">
            <thead>
            <tr>
                <th>施設名</th>
                <th>区分</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($Facility_List as $facility){ ?>
            <tr>
                <td><?php echo h_out($facility['MstFacility']['facility_name']); ?></td>
                <td><?php echo h_out($facility['MstFacility']['facility_type_name']); ?></td>
            </tr>
            <?php }  ?>
            </tbody>
        </table>
    </div>
</form>
