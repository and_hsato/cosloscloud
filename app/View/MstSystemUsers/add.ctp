<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //病院が1つ以上チェックされていないとダメ
        if($('input[type=checkbox].chk2:checked').length === 0){
           alert('病院を1つ以上、選択してください。');
           return
        }
        //センターが1つ以上チェックされていないとダメ
        if($('input[type=checkbox].chk1:checked').length === 0){
            alert('センターを1つ以上、選択してください。');
            return;
        }
        var chk_flg = false;
        $('input[type=checkbox].chk1:checked').each(function(){
            code = $(this).parent(':eq(0)').find('input[type=hidden]:eq(0)').val();
            if(code == $('#centerCode').val()){
                chk_flg = true;
            }
        });
        if( chk_flg == false){
            alert('所属施設デフォルトはチェックしたセンターの中から選択してください。');
            return;
        }
        //チェックOKだったら、送信する
        validateDetachSubmit($('#AddForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result/' );
    }
}

$(document).ready(function(){
    <?php if(Configure::read('Password.Security') == 1 ) { ?>
    $('#temp_password').addClass('r validate[required,custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]');
    $('#temp_password2').addClass('r validate[required,custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]');
    <?php }else{ ?>
    $('#temp_password').addClass('r validate[required]');
    $('#temp_password2').addClass('r validate[required,equals[temp_password]]');
    <?php } ?>
    $("#AddForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
    $('table.TableStyle01 > tbody > tr:odd').addClass('odd');
    //チェックボックス一括変更
    $("#allCheck").click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    //新規なので仮パスワード発行フラグは強制ON
    $('#is_temp_issue').change(function(){
        $(this).attr('checked' , 'checked');
    });


});

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist">ユーザ一覧</a></li>
        <li>ユーザ編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">ユーザ編集</h2>

<form class="search_form input_form"  action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckUserId" id="AddForm">
    <input type="hidden" name="mode" value="add" />
    <?php echo $this->form->input('MstUser.is_update' , array('type'=>'hidden' , 'value'=>'1'));?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <div style="float:left;">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>ユーザID</th>
                    <td><?php echo $this->form->input('MstUser.login_id' , array('type'=>'text' , 'class'=>'validate[required,ajax[ajaxUserId]] search_canna r' , 'maxlength'=>100))?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>ユーザ名</th>
                    <td><?php echo $this->form->input('MstUser.user_name' , array('type'=>'text' , 'class'=>'r validate[required] search_canna' , 'maxlength'=>100))?></td>
                    <td></td>
                </tr>
                 <tr>
                     <th>所属施設デフォルト設定</th>
                     <td>
                         <?php echo $this->form->input('MstFacility.centerName' , array('id' => 'centerName' , 'type'=>'hidden'))?>
                         <?php echo $this->form->input('MstFacility.centerText' , array('id' => 'centerText' , 'label' => false, 'div' => false, 'class' => 'r', 'style'=>'width: 60px;')); ?>
                         <?php echo $this->form->input('MstFacility.centerCode' , array('id' => 'centerCode' , 'options'=>$center_list, 'class' => 'r validate[required]' , 'empty'=>'')); ?>
                    </td>
                    <td></td>
                </tr>
                 <tr>
                     <th>カタログ施設設定</th>
                     <td>
                         <?php echo $this->form->input('MstFacility.facilityName' , array('id' => 'facilityName' , 'type'=>'hidden'))?>
                         <?php echo $this->form->input('MstFacility.facilityText' , array('id' => 'facilityText' , 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                         <?php echo $this->form->input('MstFacility.facilityCode' , array('id' => 'facilityCode' , 'options'=>$hospital_list, 'class' => 'txt' , 'empty'=>'')); ?>
                    </td>
                    <td></td>
                </tr>
                 <tr>
                     <th>カタログ部署設定</th>
                     <td>
                         <?php echo $this->form->input('MstDepartment.departmentName' , array('id'=>'departmentName' , 'type'=>'hidden'))?>
                         <?php echo $this->form->input('MstDepartment.departmentText' , array('id'=>'departmentText' , 'type'=>'text' , 'class'=>'txt' , 'style'=>'width: 60px;'))?>
                         <?php echo $this->form->input('MstDepartment.departmentCode' , array('id'=>'departmentCode' , 'options'=>array() ,'class' => 'txt' , 'empty'=>'')); ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>ロール</th>
                    <td><?php echo $this->form->input('MstUser.mst_role_id' , array('options'=>$Role_List , 'class'=>'r txt validate[required]' , 'empty'=>'選択してください'))?></td>
                    <td></td>
                </tr>
                <?php if(Configure::read('Password.Security') == 1 ){ ?>
                <tr>
                    <th>仮発行フラグ</th>
                    <td>
                        <?php echo $this->form->input('MstUser.is_temp_issue_dummy' , array('type'=>'checkbox' ,'hiddenField'=>false ,'checked'=>'checked' , 'disabled'=>'disabled'))?>
                        <?php echo $this->form->input('MstUser.is_temp_issue' , array('type'=>'hidden'))?>
                        <input type="button" value="仮パスワード再設定" id="password_btn" style="width:150px;" disabled="disabled">
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <th><?php if(Configure::read('Password.Security') == 1 ){ ?>仮<?php } ?>パスワード</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password' , array('type'=>'password' , 'class'=>'' , 'id'=>'temp_password'))?>
                    </td>
                </tr>
                <tr>
                    <th><?php if(Configure::read('Password.Security') == 1 ){ ?>仮<?php } ?>パスワード(確認用)</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password2' , array('type'=>'password' , 'class'=>'' , 'id'=>'temp_password2'))?>
                    </td>
                </tr>
            </table>
        </div>
        <?php if(Configure::read('Password.Security') == 1 ){ ?>
        <div style="border: 1px dotted black; width:400px;margin: 5px;float:left;margin-top:100px;">
            <p style="text-align:left;">【更新パスワード入力ルール】</p>
            <ol style="list-style-type: decimal !important;margin-left:25px;">
                <li style="list-style-type: decimal !important; text-align:left;">半角数字、半角英小文字、_（アンダーバー）、-（ハイフン）が使用出来ます。ただし<span class="ColumnRed">半角数字の0（ゼロ）、1（イチ）および  半角英小文字のo（オー）、l（エル）</span>は使用禁止です。</li>
                <li style="list-style-type: decimal !important; text-align:left;">パスワードは8文字とし、半角数字と半角英小文字が  それぞれ１文字以上含まれる値を設定してください。</li>
                <li style="list-style-type: decimal !important; text-align:left;">現在のパスワードとは異なる値を設定してください。</li>
            </ol>
        </div>
        <?php } ?>
    </div>
    <h2 class="HeaddingSmall">利用可能施設</h2>
    <div class="TableScroll">
        <table class="TableStyle01">
            <colgroup>
                <col width="25" />
                <col />
            </colgroup>
            <thead>
            <tr>
                <th><input type="checkbox" id="allCheck" /></th>
                <th>施設名</th>
                <th>区分</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($Facility_List as $f) { ?>
            <tr>
                <td class="center">
                    <input type="checkbox" class="chk chk<?php echo $f['MstFacility']['facility_type']?>" name="data[MstUser][Usable_Facility][]" value="<?php echo $f['MstFacility']['id'] ?>" />
                    <?php echo $this->form->input('MstFacility.facility_code.'.$f['MstFacility']['id'], array('type'=>'hidden' , 'value'=>$f['MstFacility']['facility_code'])); ?>
                </td>
                <td><?php echo h_out($f['MstFacility']['facility_name']); ?></td>
                <td><?php echo h_out($f['MstFacility']['facility_type_name']); ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="submit" class="btn btn2 [p2]" value="" />
    </div>
</form>
