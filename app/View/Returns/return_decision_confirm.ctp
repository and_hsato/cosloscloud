<script type="text/javascript">
$(document).ready(function(){
    //登録ボタン押下
    $("#btn_Select").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_decision_result').submit();
        }else{
            alert("登録する明細を選択してください。");
            return false;
        }
    });

    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
    });
});

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/return_decision">仕入先選択</a></li>
        <li>返品確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>返品確認</span></h2>
<h2 class="HeaddingMid">以下の内容で返品を行います。</h2>
<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'searchForm','class' => 'validate_form') ); ?>
    <?php echo $this->form->input('TrnReturn.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnReturn.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>返品日</th>
                <td>
                    <?php echo $this->form->input('TrnReturn.date',array('id' => 'datepicker', 'class' => 'text r date validate[required]', 'style'=>'width: 80px;','value'=>date("Y/m/d"),'maxlength'=>10)); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
               　検索結果：<?php echo count($result); ?>件
            </span>
            <span class="BikouCopy"></span>
        </div>
    <div class="TableScroll">
    <table class="TableStyle01 table-odd2">
        <colgroup>
            <col width="25" />
            <col />
            <col />
        </colgroup>
        <tr>
            <th rowspan='2'><input type="checkbox" class="checkAll" checked="checked" /></th>
            <th>商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th>包装単位</th>
            <th>ロット番号</th>
            <th>シール番号</th>
            <th>マスタ単価</th>
            <th>返品単価</th>
            <th>作業区分</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>仕入単位</th>
            <th>有効期限</th>
            <th>仕入先</th>
            <th>仕入単価</th>
            <th>数量</th>
            <th>備考</th>
        </tr>
        <?php foreach($result as $r){ ?>
        <tr>
            <td rowspan=2 class="center">
                <input name="data[TrnReturn][id][]" checked type="checkbox" class="chk" value="<?php echo $r['TrnReturn']['id'] ?>"/>
            </td>
            <td><?php echo h_out($r['TrnReturn']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($r['TrnReturn']['item_name']); ?></td>
            <td><?php echo h_out($r['TrnReturn']['item_code']); ?></td>
            <td><?php echo h_out($r['TrnReturn']['unit_name']); ?></td>
            <td><?php echo $this->form->input('TrnReturn.lot_no.'.$r['TrnReturn']['id'], array('type'=>'text', 'value'=>trim($r['TrnReturn']['lot_no']), 'class'=>'txt', 'style'=>'width:90%;')) ?></td>
            <td><?php echo h_out($r['TrnReturn']['sticker_no']); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($r['TrnReturn']['transaction_price']),'right'); ?></td>
            <td><?php echo $this->form->input('TrnReturn.stocking_price.'.$r['TrnReturn']['id'], array('type'=>'text', 'value'=>$r['TrnReturn']['return_price'], 'class'=>'r validate[required]', 'style'=>'width:95%; text-align:right;')) ?></td>
            <td><?php echo $this->form->input('TrnReturn.work_type.'.$r['TrnReturn']['id'], array('options'=>$classes, 'value'=>$r['TrnReturn']['work_type'], 'class'=>'txt' , 'empty'=>true)) ?></td>
        </tr>
        <tr>
            <td><?php h_out($r['TrnReturn']['receiving_type_name'],'center'); ?></td>
            <td><?php echo h_out($r['TrnReturn']['standard']); ?></td>
            <td><?php echo h_out($r['TrnReturn']['dealer_name']); ?></td>
            <td><?php echo h_out($r['TrnReturn']['receiving_unit_name']); ?></td>
            <td><?php echo $this->form->input('TrnReturn.validated_date.'.$r['TrnReturn']['id'], array('type'=>'text', 'value'=>$r['TrnReturn']['validated_date'], 'class'=>'txt', 'style'=>'width:90%;',"maxlength"=>10)) ?></td>
            <td><?php echo h_out($r['TrnReturn']['facility_name_to']); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($r['TrnReturn']['stoking_price']),'right') ?></td>
            <td><?php echo h_out($r['TrnReturn']['quantity'],'right') ?></td>
            <td><?php echo $this->form->input('TrnReturn.recital.'.$r['TrnReturn']['id'], array('type'=>'text', 'value'=>$r['TrnReturn']['recital'], 'class'=>'txt', 'style'=>'width:90%;',"maxlength"=>200)) ?></td>
        </tr>
        <?php } ?>
        </table>
      </div>
      <div class="ButtonBox">
          <p class="center">
              <input type="button" class="btn btn29" id="btn_Select"/>
          </p>
      </div>
    </div>
<?php echo $this->form->end();?>
