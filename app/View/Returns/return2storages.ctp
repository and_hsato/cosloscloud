<script type="text/javascript">

$(document).ready(function(){

    //シール読込ボタン押下
    $('#btn_reader').click(function(){
        $("#settingForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return2storages_reader').submit();
    });

    //在庫選択ボタン押下
    $('#btn_search').click(function(){
        $("#settingForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return2storages_search').submit();
    });

});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>入庫済返品登録</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>入庫済返品登録</span></h2>
<h2 class="HeaddingMid">読み込み方法を選択してください</h2>
<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'settingForm','class' => 'validate_form input_form') ); ?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('setting.classId',array('options' => $classes,'class' => 'txt' ,'id'=>'classId', 'empty'=>true)); ?>
                    <?php echo $this->form->input('setting.className',array('type' => 'hidden','id' => 'className' , 'empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <th>備考</th>
                <td>
                    <?php echo $this->form->text('setting.remarks',array('class' => 'txt')); ?>
                </td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center">
                <input type="button" id="btn_reader" class="btn btn17"/>
            </p>
        </div>
    </div>
<?php echo $this->form->end(); ?>