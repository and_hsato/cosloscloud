<script type="text/javascript">
/*
 * バーコードリスト、削除処理
 * @param {event} event
 * @return {bool}
 */

String.prototype.trim = function() {return this.replace(/^\s+|\s+$/g,"");}

function editTextArea(event){
    var KeyChar   = event.keyCode;
    var codeInput = $('#codeinput').val().trim().toUpperCase();

    if (KeyChar == 13){ //Enterキー押下
        var _hasDup = false;
        if (codeInput === '') {
            $('#err_msg').text('空文字は入力できません');
            $('#codeinput').val('');
        } else {
            $('#barcodelist > li').each(function () {
                if( codeInput == this.innerText) {
                    $('#err_msg').text("番号が重複しています");
                    _hasDup = true;
                    $('#codeinput').val('');
                }
            });
            if (_hasDup === false) {
                //要素追加
                $('#barcodelist').append('<li>'+codeInput+'</li>');
                $('#barcodelist > li').click(function(){
                    $('#barcodelist > li').removeClass('selected');
                    $(this).addClass('selected');
                });
                //スクロール
                $('#barcodelist').scrollTop($('#barcodelist > li').length * 14);
                $('#err_msg').text('');
                $('#codeinput').val('');
                $('#codez').attr('value', $('#codez').val() + codeInput + ',');
                $('#barcode_count').text('読込件数：'+ $('#barcodelist > li').length +'件');
            }
        }
        $('#codeinput').focus();
        return false;
    }
}

$(document).ready(function(){
    // current codez.
    var _currentCodez = null;

    //初期フォーカスをバーコード入力欄にあわせる
    $('#codeinput').focus();

    //DELETEキー押下
    $(document).keyup(function(e){
        if(e.which === 46){ //DELETEキー
            obj = jQuery(document.activeElement);
            if(obj.attr('id') != 'codeinput'){ //バーコード入力上でDELが押された場合は無視
                $('#barcodelist li.selected').remove();
                $('#codez').val(''); //いったん全部消す
                $('#barcodelist li').each(function(){
                    $('#codez').val($('#codez').val() + this.innerText + ',');
                });
                $('#barcode_count').text('読込件数：'+ $('#barcodelist > li').length +'件');
            }
        }
        $('#codeinput').focus();
    });
  
    //確認ボタン押下
    $("#confirmBtn").click(function(){
        if ($('#barcodelist li').length > 0 && $('#codez').val() != '') {
             $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_deposits_confirm');
             $("#searchForm").attr('method', 'post');
             $("#searchForm").submit();
        } else {
             alert('バーコードが入力されていません');
             $('#codeinput').focus();
             return false;
        }
    });
    //クリアボタン押下
    $("#claerBtn").click(function(){
        $('#codeinput').val('');
        $('#err_msg').text('');
        $('#codeinput').focus();
    });

    //全クリアボタン押下
    $('#allClaerBtn').click(function(){
        $('#barcodelist > li').remove();
        $('#codeinput').val('');
        $('#codez').val('');
        $('#err_msg').text('');
        $('#barcode_count').text('読込件数：0件');
        $('#codeinput').focus();
    });
});
</script>
<style>
#barcodelist{
  width: 512px;
  height:280px;
  overflow: auto;
  margin:5px 0px 5px;
  padding:5px;
  display: block;
  border: groove 1px #111111;
}
#barcodelist > li{
  width: 500px;
  cursor: pointer;
  display: block;
}
  
.selected{
  background:#0A246A;
  color:#ffffff;
}
</style>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_deposits">預託品返品登録</a></li>
        <li>シール読込</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>シール読込</span></h2>
<h2 class="HeaddingMid">預託品返品登録を行いたい商品のシールを読込してください</h2>
<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'searchForm','class' => 'validate_form') ); ?>
    <?php echo $this->form->hidden("Search.Url",array('value'=>'return_deposits_sticker')); ?>
    <?php echo $this->form->hidden('Return.codez', array('id'=>'codez',)); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col/>
                <col/>
                <col width='400'/>
            </colgroup>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo  $this->form->input('MstFacility.facility_name',array('class' => 'lbl', 'style'=>'width: 110px;', 'readonly'=>'readonly')); ?>
                </td>
                <td width="20px"></td>
                <th>作業区分</th>
                <td>
                <?php echo $this->form->input('Returns.work_type_name',array('class' => 'lbl', 'style'=>'width: 110px;', 'readonly'=>'readonly')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo  $this->form->input('MstDepartment.department_name',array('class' => 'lbl', 'style'=>'width: 110px;', 'readonly'=>'readonly')); ?>
                    <?php echo  $this->form->input('MstDepartment.department',array( 'type' => 'hidden')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->text('search.recital',array('style' => 'width:110px', 'class'=>'lbl','readonly'=>'readonly'));?></td>
                <td></td>
            </tr>
            <tr>
                <th>バーコード</th>
                <td colspan="4">
                    <div>
                        <input type="text" class="txt15 r" id="codeinput" onkeyDown="return editTextArea(event)" />
                        <input type="button" id='claerBtn' value="" class="btn btn14" />
                    </div>
                    <div id="err_msg" class="RedBold"></div>
                </td>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <td colspan="4" valign="bottom">
                    <div style="float:left;"><ul class="barcodez" id="barcodelist" name="barcodelist"></ul></div>
                    <div style="float:right;"><span class="barcodez_count" id="barcode_count">読込件数：0件</span></div>
                    <br clear="all" />
                    <input type="button" class="btn btn13" id="allClaerBtn" />
                </td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" id="confirmBtn" class="btn btn3"/>
            </p>
        </div>
    </div>
<?php echo $this->form->end(); ?>
