<script type="text/javascript">
$(document).ready(function(){
    //検索ボタン押下
    $('#btn_search').click(function(){
        $("#searchForm").attr('action','<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_history').submit();
    });
    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $("#searchForm").attr('action','<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
    });

    //明細表示ボタン押下
    $('#btn_Select').click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#searchForm").attr('action','<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_history_detail').submit();
        }else{
            alert("返品情報を選択してください");
        }
    });
    //返品明細書印刷ボタン押下
    $('#btn_print_return').click(function(){
        if($('input[type=checkbox].header_chk:checked').length > 0 ){
            $("#selected_facility_id_in_header").val($('#usersFacilities option:selected').val());
            $("#searchForm").attr('action','<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/return').submit();
        }else{
            alert("返品明細書の印刷を行いたい返品を選択してください");
        }
    });
    //仕入先プルダウン変更したらコードを入力
    $('#facility_to_id').change(function(){
         $('#facility_to_text').val($(this).val());
    });
    //仕入先テキストを入力したらプルダウンを変更
    $('#facility_to_text').blur(function(){
         if($('#facility_to_id').find('[value=' + $(this).val() + ']').length === 1){
             $('#facility_to_id').val($(this).val());
         }else{
             $('#facility_to_id').val('');
         }
    });

    //施設プルダウン変更したらコードを入力
    $('#facility_from_id').change(function(){
         $('#facility_from_text').val($(this).val());
    });

    //施設テキストを入力したらプルダウンを変更
    $('#facility_from_text').blur(function(){
         if($('#facility_from_id').find('[value=' + $(this).val() + ']').length === 1){
             $('#facility_from_id').val($(this).val());
         }else{
             $('#facility_from_id').val('');
         }
    });

    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>返品履歴</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>返品履歴</span></h2>
<h2 class="HeaddingMid">返品履歴</h2>
<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'searchForm','class' => 'validate_form search_form') ); ?>
    <input type="hidden" name="data[user][selected_facility_id_in_header]" id="selected_facility_id_in_header" value="" />
    <?php echo $this->form->input('User.user_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.MstUser.id')))?>
    <?php echo $this->form->input('search.is_search' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>返品番号</th>
                <td>
                    <?php echo  $this->form->input('TrnReturnHeader.work_no',array('class' => 'txt', 'style'=>'width: 110px;')); ?>
                </td>
                <td width=20px;></td>
                <th>返品日</th>
                <td>
                <?php echo $this->form->input('TrnReceiving.date_select_start',array('id'=>'datepicker1','class'=>'date validate[custom[date]]','style'=>'width:80px;',"maxlength"=>10)); ?>
                &nbsp;～&nbsp;
                <?php echo $this->form->input('TrnReceiving.date_select_end',array('id'=>'datepicker2','class'=>'date validate[custom[date]]','style'=>'width:80px;',"maxlength"=>10)); ?>
                </td>
                <td width=20px;></td>
                <td colspan=2>
                <?php echo $this->form->input('TrnReceiving.yet' , array('type'=>'checkbox', 'hiddenField'=>false)); ?>
                未確定も表示する
                </td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('MstFacility.facility_to' , array('type'=>'text' , 'class'=>'txt' , 'style'=>'width:60px;' , 'id'=>'facility_to_text'))?>
                    <?php echo $this->form->input('MstFacility.facility_to_id',array('options'=> $facility_select ,'id' => 'facility_to_id','class' => 'txt' , 'empty'=>true)); ?>
                </td>
                <td width=20px;></td>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('MstFacility.facility_from' , array('type'=>'text' , 'class'=>'txt' , 'style'=>'width:60px;' , 'id'=>'facility_from_text'));?>
                    <?php echo $this->form->input('MstFacility.facility_from_id', array('options'=>$facility_from_select ,'id' => 'facility_from_id' ,'class' => 'txt' , 'empty'=>true)); ?>
                </td>
                <td width=20px;></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td>
                    <?php echo $this->form->text('MstFacilityItem.internal_code',array('class' => 'txt search_internal_code','style'=>'width:150px; ime-modo:disabled;')); ?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->text('MstFacilityItem.item_code',array('class' => 'txt search_upper','style'=>'width:150px; ime-modo:disabled;')); ?>
                </td>
                <td></td>
                <th>ロット番号</th>
                <td>
                    <?php echo $this->form->text('TrnSticker.lot_no',array('class' => 'txt','style'=>'width:150px; ime-modo:disabled;')); ?>
                </td>
            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->text('MstFacilityItem.item_name',array('class' => 'txt search_canna','style'=>'width:150px; ime-modo:disabled;')); ?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                    <?php echo $this->form->text('MstDealer.dealer_name',array('class' => 'txt search_canna','style'=>'width:150px; ime-modo:disabled;')); ?>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td>
                    <?php echo $this->form->text('MstFacilityItem.standard',array('class' => 'txt search_canna','style'=>'width:150px; ime-modo:disabled;')); ?>
                </td>
                <td></td>
                <th>センターシール番号</th>
                <td>
                    <?php echo $this->form->text('TrnSticker.facility_sticker_no',array('class' => 'txt','style'=>'width:150px; ime-modo:disabled;')); ?>
                </td>
                <td></td>
                <th>部署シール番号</th>
                <td>
                    <?php echo $this->form->text('TrnSticker.hospital_sticker_no',array('class' => 'txt','style'=>'width:150px; ime-modo:disabled;')); ?>
                </td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center">
                <input type="button" id="btn_search" class="btn btn1"/>
                <input type="button" class="btn btn5" id="btn_Csv"/>
            </p>
        </div>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <colgroup>
                    <col width="25" />
                    <col width='140'/>
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th style="width:20px;"><input type="checkbox" checked class="checkAll"/></th>
                    <th style="width:135px;">返品番号</th>
                    <th style="width:85px;">返品日</th>
                    <th>仕入先</th>
                    <th>施設</th>
                    <th>登録者</th>
                    <th style="width:150px;">登録日時</th>
                    <th>備考</th>
                    <th style="width:80px;">件数</th>
                </tr>
                <?php
                    $cnt=0;
                    foreach ($result as $r) {
                ?>
                <tr>
                    <td style="width:20px;" class="center">
                        <?php if($r['Returns']['id'] == '' ){ ?>
                        <?php echo $this->form->input('Returns.facility.'.$cnt , array('type'=>'checkbox' , 'value'=>$r['Returns']['facility_id_from'] ."_" .$r['Returns']['facility_id_to'], 'hiddenField'=>false ,'class'=>'chk', 'checked'=>'checked'));?>
                        <?php }else{ ?>
                        <?php echo $this->form->input('Returns.id.'.$cnt , array('type'=>'checkbox' , 'value'=>$r['Returns']['id'] , 'hiddenField'=>false ,'class'=>'chk header_chk' ,  'checked'=>'checked'));?>
                        <?php } ?>
                    </td>
                    <td style="width:135px;"><?php echo h_out($r['Returns']['work_no'],'center'); ?></td>
                    <td style="width:85px;"><?php echo h_out($r['Returns']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($r['Returns']['facility_name_to']); ?></td>
                    <td><?php echo h_out($r['Returns']['facility_name_from']); ?></td>
                    <td><?php echo h_out($r['Returns']['user_name']); ?></td>
                    <td style="width:140px;"><?php echo h_out($r['Returns']['created'],'center'); ?></td>
                    <td><?php echo h_out($r['Returns']['recital']); ?></td>
                    <td><?php echo h_out($r['Returns']['count']."/".$r['Returns']['detail_count'],'right'); ?></td>
                </tr>
                <?php $cnt++;  } ?>
                <?php if(count($result) == 0 && isset($this->request->data['search']['is_search']) ){ ?>
                <tr>
                    <td align="center" colspan=9>返品データがありません</td>
                </tr>
                <?php } ?>
            </table>
        </div>
        <?php if(count($result) > 0){ ?>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn7" id="btn_Select"/>&nbsp;&nbsp;
                <input type="button" class="btn btn42" id="btn_print_return"/>
            </p>
        </div>
        <?php } ?>
    </div>

<?php echo $this->form->end(); ?>
