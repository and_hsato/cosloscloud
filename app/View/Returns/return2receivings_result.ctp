<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/return2receivings">入荷済返品登録</a></li>
        <li class="pankuzu">入荷済返品確認</li>
        <li>入荷済返品結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>入荷済返品結果</span></h2>
<div class="Mes01">以下の内容で登録しました</div>
<hr class="Clear" />

<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'resultForm','class' => 'validate_form') );?>
    <div class="SearchBox">
        <table style="width:100%;">
            <tr>
                <td></td>
                <td align="right"></td>
            </tr>
            <tr>
                <td>表示件数：<?php echo count($RegistResultView['result']['id']); ?>件</td>
            </tr>
        </table>

        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <colgroup>
                    <col width="20" />
                    <col width="100" />
                    <col width="100" />
                    <col width="80" />
                    <col />
                    <col />
                    <col width="100"/>
                    <col width="100"/>
                    <col width="80" />
                    <col width="80" />
                    <col width="60" />
                </colgroup>
                <tr>
                    <th rowspan="2"></th>
                    <th>発注番号</th>
                    <th>入荷番号</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>入荷数</th>
                    <th>ロット番号</th>
                    <th>返品数</th>
                    <th>作業区分</th>
                    <th>遡及<br>不可</th>
                </tr>
                <tr>
                    <th colspan="2">仕入先</th>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>有効期限</th>
                    <th colspan="3">備考</th>
                </tr>
            </table>
        </div>
    </div>

    <div class="TableScroll">
        <table class="TableStyle02 tabel-even2" border="0">
            <colgroup>
                <col width="20" />
                <col width="100" />
                <col width="100" />
                <col width="80" />
                <col />
                <col />
                <col width="100" />
                <col width="100" />
                <col width="80" />
                <col width="80" />
                <col width="60" />
            </colgroup>
            <?php for($_cnt=0; $_cnt<count($RegistResultView['result']['id']); $_cnt++){ ?>
            <tr>
                <td rowspan="2" style="padding:0px;"></td>
                <td><?php echo h_out($RegistResultView['result']['OrderWorkNo'][$_cnt]); ?></td>
                <td><?php echo h_out($RegistResultView['result']['TrnReceivingWorkNo'][$_cnt]); ?></td>
                <td><?php echo h_out($RegistResultView['result']['InternalCode'][$_cnt],'center'); ?></td>
                <td><?php echo h_out($RegistResultView['result']['ItemName'][$_cnt]); ?></td>
                <td><?php echo h_out($RegistResultView['result']['ItemCode'][$_cnt]); ?></td>
                <td><?php echo h_out(number_format($RegistResultView['result']['RemainCount'][$_cnt]),'right'); ?></td>
                <td>
                <?php echo $this->form->text("RegistTrnReceiving.Lot_no.{$_cnt}",array('class'=>'lbl','value'=>$RegistResultView['result']['LotNo'][$_cnt],'style'=>'width:80px;','readonly'=>'readonly')); ?>
                </td>
                <td>
                <?php echo $this->form->text("RegistTrnReceiving.Return_counts.{$_cnt}",array('class'=>'lbl','value'=>$RegistResultView['result']['ReturnCounts'][$_cnt],'style'=>'width:65px; text-align:right;','readonly'=>'readonly')); ?>
                </td>
                <td>
                <?php echo $this->form->text("RegistTrnReceiving.work_type.{$_cnt}",array('class'=>'lbl',"value"=>$RegistResultView['result']['work_type'][$_cnt],"style"=>"width:65px;","readonly"=>"readonly")); ?>
                </td>
                <td><?php echo h_out($RegistResultView['result']['is_retroactable'][$_cnt]); ?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo h_out($RegistResultView['result']['FacilityName'][$_cnt]); ?></td>
                <td></td>
                <td><?php echo h_out($RegistResultView['result']['Standard'][$_cnt]); ?></td>
                <td><?php echo h_out($RegistResultView['result']['DealerName'][$_cnt]); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($RegistResultView['result']['StockingPrice'][$_cnt]),'right'); ?></td>
                <td>
<?php
            if(empty($RegistResultView['result']['ExpiryDate'][$_cnt])){
              echo "";
            }else{
              echo $this->form->text("RegistTrnReceiving.Expiry_date.{$_cnt}",array('class'=>'lbl','value'=>$RegistResultView['result']['ExpiryDate'][$_cnt],'style'=>'width:80px','readonly'=>'readonly'));
            }
?>
                </td>
                <td colspan="3">
                <?php echo $this->form->text("RegistTrnReceiving.Remarks.{$_cnt}",array('class'=>'lbl','value'=>$RegistResultView['result']['Remarks'][$_cnt],'style'=>'width:200px','readonly'=>'readonly')); ?>
                </td>
            </tr>
            <?php }?>
       </table>
    </div>

<?php echo $this->form->end() ?>
    