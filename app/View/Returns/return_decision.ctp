<script type="text/javascript">
$(document).ready(function(){
    //検索ボタン押下
    $("#btn_Search").click(function(){
        validateDetachSubmit($("#searchForm") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_decision');
    });

    //選択ボタン押下
    $('#btn_Select').click(function(){
        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_decision_confirm').submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>返品確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>仕入先選択</span></h2>
<h2 class="HeaddingMid">返品を行う仕入先を選択してください</h2>
<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'searchForm','class' => 'validate_form') ); ?>
    <?php echo $this->form->input('Return.is_search' , array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('MstFacility.id', array('options'=>$facility_select , 'id' => 'facility_selected','class' => 'txt' , 'empty'=>true)); ?>
                </td>
                <td></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" value="" class="btn btn1" id="btn_Search"/>
            </p>
        </div>
    </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th></th>
                    <th>仕入先</th>
                    <th>施設</th>
                </tr>
                <?php $cnt=0; ?>
                <?php foreach ($result as $r) { ?>
                <tr>
                    <td class="center">
                        <input name="data[TrnReturn][id][]" type="checkbox"  class="center chk " value="<?php echo $r['MstDepartmentFrom']['id'].'_'.$r['MstDepartmentTo']['id'] ?>"/>
                    </td>
                    <td><?php echo h_out($r['MstFacilityTo']['facility_name']); ?>
                    </td>
                    <td>
                        <?php echo h_out($r['MstFacilityFrom']['facility_name'] . '／' . $r['MstDepartmentFrom']['department_name']) ?>
                    </td>
                </tr>
                <?php  $cnt++; } ?>
                <?php if(count($result) == 0 && isset($this->request->data['Return']['is_search'])) { ?>
                <tr>
                    <td colspan='3' class="center">該当データはありません。</td>
                </tr>
                <?php } ?>
            </table>
        </div>
        <?php if(count($result) > 0) { ?>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn4" id="btn_Select"/>
            </p>
        </div>
        <?php } ?>
    </div>
<?php echo $this->form->end(); ?>
