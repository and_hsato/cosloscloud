<script type="text/javascript">

$(document).ready(function(){
    //返品明細書印刷ボタン押下
    $("#btn_print").click(function(){
        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/return').submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/return_decision">仕入先選択</a></li>
        <li>返品完了</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>返品完了</span></h2>
<h2 class="HeaddingMid">返品登録が完了しました。</h2>

<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'searchForm','class' => 'validate_form') ); ?>
    <input type="hidden" id="uid" name="data[User][user_id]" value="<?php print $this->Session->read('Auth.MstUser.id'); ?>">
    <?php $cnt = 0 ; foreach($header_ids as $id){ ?>
    <?php echo $this->form->input('Returns.id.' . $cnt , array('type'=>'hidden' , 'value'=>$id));?>
    <?php $cnt++ ; } ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>返品日</th>
                <td>
                    <?php echo $this->form->input('TrnReturn.date',array('type'=>'text','class'=>'lbl','readonry'=>'readonry','style'=>'width:80px;')); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="results">
        <div class="SelectBikou_Area">
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd2" >
                <colgroup>
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>ロット番号</th>
                    <th>シール番号</th>
                    <th>マスタ単価</th>
                    <th>返品単価</th>
                    <th>作業区分</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単位</th>
                    <th>有効期限</th>
                    <th>仕入先</th>
                    <th>仕入単価</th>
                    <th>数量</th>
                    <th>備考</th>
                </tr>
                <?php $cnt=0; foreach($result as $r ){ ?>
                <tr>
                    <?php echo $this->form->input('TrnSticker.id.'.$cnt, array('type'=>'hidden', 'value'=>$r['TrnReturn']['id'], 'style'=>'width:90%')); ?>
                    <td><?php echo h_out($r['TrnReturn']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnReturn']['item_name']); ?></td>
                    <td><?php echo h_out($r['TrnReturn']['item_code']); ?></td>
                    <td><?php echo h_out($r['TrnReturn']['unit_name']); ?></td>
                    <td><?php echo $this->form->input('TrnReturn.lot_no', array('type'=>'text','readonly'=>'readonly', 'value'=>$r['TrnReturn']['lot_no'], 'class'=>'lbl', 'style'=>'width:90%')); ?></td>
                    <td><?php echo h_out($r['TrnReturn']['sticker_no']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($r['TrnReturn']['transaction_price']),'right'); ?></td>
                    <td align='right'><?php echo $this->form->input('TrnReturn.return_price', array('type'=>'text','readonly'=>'readonly', 'value'=>$r['TrnReturn']['return_price'] , 'class'=>'lbl', 'style'=>'width:90%; text-align:right;')); ?></td>
                    <td><?php echo h_out($r['TrnReturn']['work_type_name']); ?></td>
                </tr>
                <tr >
                    <td></td>
                    <td><?php echo h_out($r['TrnReturn']['standard']); ?></td>
                    <td><?php echo h_out($r['TrnReturn']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['TrnReturn']['receiving_unit_name']); ?></td>
                    <td><?php echo $this->form->input('TrnReturn.validated_date',array('type'=>'text','value'=>$r['TrnReturn']['validated_date'],'readonly'=>'readonly','class'=>'lbl', 'style'=>'width:90%')) ?></td>
                    <td><?php echo h_out($r['TrnReturn']['facility_name_to']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($r['TrnReturn']['stoking_price']),'right'); ?></td>
                    <td><?php echo h_out($r['TrnReturn']['quantity'],'right'); ?></td>
                    <td><?php echo $this->form->input('TrnReturn.recital', array('type'=>'text','readonly'=>'readonly', 'value'=>$r['TrnReturn']['recital'], 'class'=>'lbl', 'style'=>'width:90%')) ?></td>
                </tr>
                <?php $cnt++; } ?>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn42" id="btn_print"/>
    </div>
<?php echo $this->form->end(); ?>
