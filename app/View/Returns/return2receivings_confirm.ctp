<script type="text/javascript">

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('#registForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    //備考一括設定ボタン押下
    $('#but_CollectiveSetting').click(function(){
        $("#registForm input[type=text].remarks").each(function(){
            $(this).val($("#ReturnCollectiveSetting").val());
        });
    });

    //確定ボタン押下
    $('#btn_Regist').click(function(){
        //errorMsg = "必須入力項目です";
        cnt = 0;
        submitFlg = true;
        isCheck = false;
        $("#registForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                isCheck = true;
                validated_date = $("#RegistTrnReceivingExpiryDate" + cnt).val();
                //チェックが入っている行の必須項目チェック
                fieldName = "#RegistTrnReceivingReturnCounts" + cnt;
                if($(fieldName).val() == ""){
                    alert("返品数を入力してください");
                    $(fieldName).focus();
                    submitFlg = false;
                    return false;
                }else if($(fieldName).val() == "0"){
                    alert("0以外の数字を入力してください");
                    $(fieldName).focus();
                    submitFlg = false;
                    return false;
                }else if( $(fieldName).val().match( /[\D]/g ) ){
                    alert("正の整数を入力してください");
                    $(fieldName).focus();
                    submitFlg = false;
                    return false;
                }else if(parseInt($("#RegistTrnReceivingRemainCount" + cnt).val()) < parseInt($(fieldName).val())){
                    alert("返品数が入荷数を超えています");
                    $(fieldName).focus();
                    submitFlg = false;
                    return false;
                }

                //有効期限が入力されている場合、妥当性チェック
                if(validated_date != ""){
                    if( !dateCheck(validated_date) ){
                        //有効期限6桁入力取りやめに対応
                        alert('入力日付が正しくありません');
                        $("#RegistTrnReceivingExpiry_date" + cnt).focus();
                        submitFlg = false;
                        return false;
                    }
                }

                //ロット番号の長さチェック
                var lot = $("#RegistTrnReceivingLotNo" + cnt).val();
                if(lot.length > 20){
                    alert("ロット番号は20文字以内で入力してください");
                    submitFlg = false;
                    return false;
                }

                //備考の長さチェック
                var remarks = $("#RegistTrnReceivingRemarks" + cnt).val();
                if(remarks.length > 200){
                    alert("備考は200文字以内で入力してください");
                    submitFlg = false;
                    return false;
                }

                //仕入先選択チェック
                var trader = $("#RegistTrnReceivingDepartmentId" + cnt).val();
                if(trader == ""){
                    alert("返品する仕入先を選択してください");
                    submitFlg = false;
                    return false;
                }else{
                    var traderName = $("#RegistTrnReceivingDepartmentId" + cnt + " option:selected").text();
                    $("#RegistTrnReceivingFacilityName" + cnt).val(traderName);
                }

                //金額入力チェック
                var price = $("#RegistTrnReceivingStockingPrice" + cnt).val();
                if(price == ""){
                    alert("返品単価を入力してください");
                    submitFlg = false;
                    return false;
                }else{
                    //数字チェック
                    if(!numCheck(price ,10 ,2 , false )){
                        alert("単価が不正です");
                        submitFlg = false;
                        return false;
                    }
                }
            }
            cnt++;
        });

        if(!isCheck){
            alert("明細を選択してください");
            submitFlg = false;
            return false;
        }

        if(submitFlg){
            $("#registForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return2receivings_regist');
            $("#registForm").submit();
        }
    });

});

//パンくずクリック
function return1(){
    $("#registForm").attr('action',"<?php echo $this->webroot; ?><?php echo $this->name; ?>/return2receivings");
    $("#registForm").submit();
    return false;
}


</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home">TOP</a></li>
        <li><a href="#" id="return1" onclick="return1();">入荷済返品登録</a></li>
        <li>入荷済返品確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>入荷済返品確認</span></h2>
<h2 class="HeaddingMid">以下の商品を返品します。</h2>

<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'registForm','class' => 'validate_form') ); ?>
    <?php echo $this->form->input('TrnReturn.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnReturn.time', array('type'=>'hidden' , 'value'=> date('Y/m/d H:i:s.u'))); ?>
    <?php echo $this->form->hidden('search.work_no1');?>
    <?php echo $this->form->hidden('search.work_date1'); ?>
    <?php echo $this->form->hidden('search.work_date2'); ?>
    <?php echo $this->form->hidden('search.work_no2');?>
    <?php echo $this->form->hidden('search.work_date3'); ?>
    <?php echo $this->form->hidden('search.work_date4'); ?>
    <?php echo $this->form->hidden('search.item_id'); ?>
    <?php echo $this->form->hidden('search.item_code'); ?>
    <?php echo $this->form->hidden('search.management_class'); ?>
    <?php echo $this->form->hidden('search.item_name'); ?>
    <?php echo $this->form->hidden('search.dealer_name'); ?>
    <?php echo $this->form->hidden('search.work_type'); ?>
    <?php echo $this->form->hidden('search.standard'); ?>
    <?php echo $this->form->hidden('search.jan_code'); ?>

    <?php echo $this->form->input('search.tempId',array('type'=>'hidden','id'=>'tempId','value'=>$data['search']['temp_id']));?>
    <div class="SearchBox">
        <table style="width:100%;">
            <tr>
                <td></td>
                <td align="right"></td>
            </tr>
            <tr>
                <td>表示件数：<?php echo count($SearchResult); ?>件</td>
                <td align="right">
                    <?php echo $this->form->text('CollectiveSetting',array('class' => 'txt','style' => 'width:150px'));?>
                    <input type="button" value="" class="btn btn8 [p2]" id="but_CollectiveSetting">
                </td>
            </tr>
        </table>

        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <colgroup>
                    <col width="20" />
                    <col width="100" />
                    <col width="100" />
                    <col width="80" />
                    <col />
                    <col />
                    <col width="100"/>
                    <col width="100"/>
                    <col width="80" />
                    <col width="80" />
                    <col width="60" />
                </colgroup>
                <tr>
                    <th rowspan="2"><input type="checkbox" class="checkAll" checked></th>
                    <th>発注番号</th>
                    <th>入荷番号</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>入荷数</th>
                    <th>ロット番号</th>
                    <th>返品数</th>
                    <th>作業区分</th>
                    <th>遡及<br>不可</th>
                </tr>
                <tr>
                    <th colspan="2">仕入先</th>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>有効期限</th>
                    <th colspan="3">備考</th>
                </tr>
            </table>
        </div>
    </div>

    <div class="TableScroll" style="">
        <table class="TableStyle02" border="0">
            <colgroup>
                <col width="20" />
                <col width="100" />
                <col width="100" />
                <col width="80" />
                <col />
                <col />
                <col width="100" />
                <col width="100" />
                <col width="80" />
                <col width="80" />
                <col width="60" />
            </colgroup>
            <?php $_cnt=0;foreach($SearchResult as $_row):
                if($_row['FIT']['is_lowlevel']){
                    $readonly = "";
                    $value = "";
                }else{
                    $readonly = "readonly";
                    $value = 1;
                }
            ?>
            <tr>
                <td rowspan="2">
                    <?php echo $this->form->checkbox("RegistTrnReceiving.id.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row['TrnReceiving']['id'],'checked'=>'checked') ); ?>
                </td>
                <td>
                <?php echo $this->form->hidden("RegistTrnReceiving.OrderWorkNo.{$_cnt}", array('value'=>$_row['ODR']['work_no'])); ?>
                <?php echo h_out($_row['ODR']['work_no']); ?>
                </td>
          <td>
            <?php echo $this->form->hidden("RegistTrnReceiving.TrnReceivingWorkNo.{$_cnt}", array('value'=>$_row['TrnReceiving']['work_no'])); ?>
            <?php echo h_out($_row['TrnReceiving']['work_no']); ?>
          </td>
          <td>
            <?php echo $this->form->hidden("RegistTrnReceiving.InternalCode.{$_cnt}", array('value'=>$_row['FIT']['internal_code'])); ?>
            <?php echo h_out($_row['FIT']['internal_code']); ?>
          </td>
          <td>
            <?php echo $this->form->hidden("RegistTrnReceiving.ItemName.{$_cnt}", array('value'=>$_row['FIT']['item_name'])); ?>
            <?php echo h_out($_row['FIT']['item_name']); ?>
          </td>
          <td>
            <?php echo $this->form->hidden("RegistTrnReceiving.ItemCode.{$_cnt}", array('value'=>$_row['FIT']['item_code'])); ?>
            <?php echo h_out($_row['FIT']['item_code']); ?>
          </td>
          <td>
            <?php echo $this->form->hidden("RegistTrnReceiving.RemainCount.{$_cnt}", array('value'=>$_row['TrnReceiving']['remain_count'])); ?>
            <?php echo h_out(number_format($_row['TrnReceiving']['remain_count']),'right'); ?>
          </td>
          <td>
            <?php echo $this->form->text("RegistTrnReceiving.Lot_no.{$_cnt}",array('class' => 'txt','style'=>'width:80px; ime-mode:disabled;',"maxlength"=>20)); ?>
          </td>
          <td>
            <?php
              echo $this->form->text("RegistTrnReceiving.Return_counts.{$_cnt}",array(
                  'class'=>'r',
                  'value'=>$value,
                  'style'=>'width:70px; ime-mode:disabled; text-align:right; width:90%;'
              ));
            ?>
          </td>
          <td>
            <?php echo $this->form->input("RegistTrnReceiving.work_type.{$_cnt}",array('options'=>$classes,'empty'=>true,'class'=>'txt')); ?>
          </td>
          <td align="center">
<?php
    $checked = "";
    $val = "0";
    if(empty($_row['TrnReceiving']['is_retroactable'])){
        $checked = "checked";
        $val = "1";
    }
    if(empty($_row['TrnReceiving']['retroact_execution_flag'])){
        echo $this->form->checkbox("RegistTrnReceiving.is_retroactable.{$_cnt}",array("checked"=>$checked) );
    }else{
        echo $this->form->checkbox("RegistTrnReceiving.is_retroactable.{$_cnt}",array("disabled"=>"disabled","checked"=>$checked) );
        echo $this->form->hidden("RegistTrnReceiving.is_retroactable.{$_cnt}", array('value'=>$val));
    }
?>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <?php
              $list = Set::Combine($traderList,'{n}.MstDepartment.id','{n}.MstFacilitie.name');
              echo $this->form->input("RegistTrnReceiving.DepartmentId.{$_cnt}",
                                array('options'=>$list
                                ,'value'=>$_row['TrnReceiving']["department_id_from"]
                                ,'class'=>'r txt')
                                );
            ?>
            <?php echo $this->form->hidden("RegistTrnReceiving.FacilityName.{$_cnt}"); ?>
          </td>
          <td></td>
          <td><?php echo $this->form->hidden("RegistTrnReceiving.Standard.{$_cnt}", array('value'=>$_row['FIT']['standard'])); ?>
              <?php echo h_out($_row['FIT']['standard']); ?>
          </td>
          <td>
            <?php echo $this->form->hidden("RegistTrnReceiving.DealerName.{$_cnt}", array('value'=>$_row['DLR']['dealer_name'])); ?>
            <?php echo h_out($_row['DLR']['dealer_name']); ?>
          </td>
          <td>
            <?php
              echo $this->form->text("RegistTrnReceiving.StockingPrice.{$_cnt}"
                              ,array('value'=>$_row['TrnReceiving']['stocking_price']
                                    ,"class"=>"r txt"
                                    ,"style"=>"text-align:right; width:90%;"
                                    ,"maxlength"=>12));
            ?>
          </td>
          <td>
            <?php echo $this->form->text("RegistTrnReceiving.Expiry_date.{$_cnt}",array('class'=>'txt','style'=>'width:80px',"maxlength"=>10)); ?>
          </td>
          <td colspan="3">
            <?php echo $this->form->text("RegistTrnReceiving.Remarks.{$_cnt}",array('class'=>'txt remarks','style'=>'width:200px',"maxlength"=>200)); ?>
          </td>
        </tr>
        <?php echo $this->form->hidden("RegistTrnReceiving.Modified.{$_cnt}", array('value'=>$_row['TrnReceiving']['modified'])); ?>
        <?php echo $this->form->hidden("RegistTrnReceiving.StickerModified.{$_cnt}", array('value'=>$_row['STC']['modified'])); ?>
        <?php $_cnt++;endforeach;?>
      </table>
    </div>

    <table style="width:100%;">
      <tr>
        <td></td>
        <td align="right"></td>
      </tr>
    </table>

    <div class="ButtonBox">
       <p class="center">
         <input type="button" class="btn btn2 [p2]" id="btn_Regist"/>
       </p>
    </div>
<?php echo $this->form->end(); ?>
