<script type="text/javascript">

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#ReturnForm #searchForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#ReturnForm #cartForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });

    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );
  
    //検索ボタン押下
    $("#btn_Search").click(function(){
        var tmpid = '';
        $("#ReturnForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);
        $("#ReturnForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_deposits_stock').submit();
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        $('#itemSelectedTable').empty();
        $("#ReturnForm #searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
            }
            cnt++;
        });
        if(cnt === 0 ){
            alert('商品を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        //選択チェック
        var cnt = 0;
        var tmpid = '';
        $("#ReturnForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);
        if(cnt === 0){
            alert('追加する商品を選択してください');
        }else{
            $("#ReturnForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_deposits_confirm').submit();
        }
    });

});

function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_deposits">預託品返品登録</a>    
        <li>在庫検索</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>在庫検索</span></h2>
<h2 class="HeaddingMid">預託品返品登録を行いたい商品を検索してください</h2>
<form class="validate_form search_form" id="ReturnForm" method="post" action="return_deposits_stock">
    <?php echo $this->form->input('add_search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('cart.tmpid',array('type'=>'hidden','id' =>'tmpid'));?>
    <?php echo $this->form->hidden("Search.Url",array("value"=>"return_deposits_stock"));?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>得意先</th>
                <td>
                <?php echo  $this->form->input('MstFacility.facility_name',array('class' => 'lbl', 'style'=>'width: 110px;', 'readonly'=>'readonly')); ?>
                </td>
                <td width="20px"></td>
                <th>作業区分</th>
                <td>
                <?php echo $this->form->input('Returns.work_type_name',array('class' => 'lbl', 'style'=>'width: 110px;', 'readonly'=>'readonly')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                <?php echo  $this->form->input('MstDepartment.department_name',array( 'class' => 'lbl', 'style'=>'width: 110px;', 'readonly'=>'readonly')); ?>
                <?php echo  $this->form->input('MstDepartment.department',array( 'type' => 'hidden')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->text('search.recital',array('class' => 'lbl', 'style' => 'width:110px', 'readonly'=>'readonly'));?></td>
                <td></td>
            </tr>
        </table>
    </div>

    <h2 class="HeaddingSmall">検索条件</h2>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFacilityItems.internal_code',array('class'=>'txt search_internal_code','style'=>'width:120px')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFacilityItems.item_code',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->text('MstFacilityItems.lot_no',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFacilityItems.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFacilityItems.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFacilityItems.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->text('MstFacilityItems.hospital_sticker_no',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>

        <div class="ButtonBox">
           <p class="center">
             <input type="button" class="btn btn1" id="btn_Search"/>
           </p>
        </div>
    </div>
    <div align="right" id="pageTop" ></div>

<?php
    if(isset($this->request->data['add_search']['is_search'])){
      if(count($SearchResult) === 0){
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th rowspan="2" class="col5"><input type="checkbox" /></th>
            <th>商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th>包装単位</th>
            <th>ロット番号</th>
            <th>部署シール</th>
          </tr>
          <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th></th>
            <th>有効期限</th>
            <th>仕入先</th>
          </tr>
        </table>
      </div>
      <span>該当するデータはありません。</span>
<?php
      }else{
?>
    <div id="results">
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th rowspan="2" class="col5"><input type="checkbox" class="checkAll_1" style="width:20px;text-align:center;"/></th>
            <th width="80px;">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th>包装単位</th>
            <th>ロット番号</th>
            <th>部署シール</th>
          </tr>
          <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th></th>
            <th>有効期限</th>
            <th>仕入先</th>
          </tr>
        </table>
      </div>

      <div class="TableScroll" id="searchForm">
        <?php $_cnt=0;foreach($SearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row['TrnSticker']['id'];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php echo "itemSelectedRow".$_row['TrnSticker']['id'];?> >
            <td class="col5 center" rowspan="2">
              <?php echo $this->form->checkbox("TrnSticker.id.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row['TrnSticker']['id'] , 'style'=>'width:20px;text-align:center;') ); ?>
            </td>
            <td width="80px"><?php echo h_out($_row['MstFacilityItem']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($_row['MstFacilityItem']['item_name']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItem']['item_code']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItem']['unit_name']); ?></td>
            <td><?php echo h_out($_row['TrnSticker']['lot_no']); ?></td>
            <td><?php echo h_out($_row['TrnSticker']['hospital_sticker_no']); ?></td>
          </tr>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>">
            <td></td>
            <td><?php echo h_out($_row['MstFacilityItem']['standard']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItem']['dealer_name']); ?></td>
            <td></td>
            <td><?php echo h_out($_row['TrnSticker']['validated_date']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItem']['facility_name']); ?></td>
          </tr>

        </table>
        <?php $_cnt++;endforeach;?>
      </div>

      <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
          <input type="button" value="" class="btn btn4" id="cmdSelect" />
        </p>
      </div>
    </div>
<?php
      }
?>

    <?php if(isset($this->request->data['add_search']['is_search'])){?>
    <h2 class="HeaddingSmall">選択商品</h2>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th rowspan="2" class="col5"><input type="checkbox" checked class="checkAll_2"/></th>
            <th width="80px;">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th>包装単位</th>
            <th>ロット番号</th>
            <th>部署シール</th>
          </tr>
          <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th></th>
            <th>有効期限</th>
            <th>仕入先</th>
          </tr>
        </table>
    </div>
    <div class="TableScroll" id="cartForm">
      <?php if(isset($CartSearchResult)){ ?>
      <?php $_cnt=0;foreach($CartSearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row['TrnSticker']['id'];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <tr id=<?php echo "itemSelectedRow".$_row['TrnSticker']['id'];?> >
            <td class="col5 center" rowspan="2">
              <?php echo $this->form->checkbox("TrnSticker.id.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row['TrnSticker']['id'] , 'style'=>'width:20px;text-align:center;' , 'checked'=>'checked') ); ?>
            </td>
            <td width="80px;"><?php echo h_out($_row['MstFacilityItem']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($_row['MstFacilityItem']['item_name']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItem']['item_code']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItem']['unit_name']); ?></td>
            <td><?php echo h_out($_row['TrnSticker']['lot_no']); ?></td>
            <td><?php echo h_out($_row['TrnSticker']['hospital_sticker_no']); ?></td>
          </tr>
          <tr>
            <td class="center"></td>
            <td><?php echo h_out($_row['MstFacilityItem']['standard']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItem']['dealer_name']); ?></td>
            <td></td>
            <td><?php echo h_out($_row['TrnSticker']['validated_date']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItem']['facility_name']); ?></td>
          </tr>

        </table>
      <?php $_cnt++;endforeach;?>
      <?php }?>
      <div id="itemSelectedTable"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" class="btn btn3"/>
      </p>
    </div>
    <?php }?>
<?php
    }else{
      //検索実行フラグがfalse
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>0)); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th rowspan="2" class="col5"><input type="checkbox" /></th>
            <th>商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th>包装単位</th>
            <th>ロット番号</th>
            <th>部署シール</th>
          </tr>
          <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th></th>
            <th>有効期限</th>
            <th>仕入先</th>
          </tr>
        </table>

      </div>
<?php
    }
    echo $this->form->end();

?>
<div align="right" id="pageDow" ></div>
