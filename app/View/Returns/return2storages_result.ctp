<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/return2storages">入庫済返品登録</a></li>
        <li class="pankuzu">入庫済返品確認</li>
        <li>入庫済返品結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>入庫済返品登録結果</span></h2>
<div class="Mes01">入庫済返品登録が完了しました</div>
<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'resultForm','class' => 'validate_form') ); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->text("setting.className",array('class' => 'lbl','style'=>'width:150px',"readonly"=>"readonly" )); ?>
                    <?php echo $this->form->hidden("setting.classId"); ?>
                </td>
            </tr>
            <tr>
                <th>備考</th>
                <td><?php echo $this->form->text("setting.remarks",array('class' => 'lbl','style'=>'width:150px',"readonly"=>"readonly" )); ?></td>
            </tr>
        </table>
    </div>

    <hr class="Clear" />
    <div class="SearchBox">
        <table style="width:100%;">
            <tr>
                <td>表示件数：<?php echo count($result); ?>件</td>
            </tr>
        </table>

        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th rowspan="2" width="20"></th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col10">ロット番号</th>
                    <th class="col15">センターシール</th>
                    <th class="col10">単価</th>
                    <th class="col10">返品単価</th>
                    <th class="col10">作業区分</th>
                    <th style="width:60px;">遡及<br>不可</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単位</th>
                    <th>有効期限</th>
                    <th>仕入先</th>
                    <th>仕入単価</th>
                    <th>数量</th>
                    <th colspan="2">備考</th>
                </tr>
            </table>
        </div>
    </div>

    <div class="TableScroll">
        <table class="TableStyle02 table-even2">
        <?php $_cnt=0;foreach($result as $r): ?>
            <tr>
                <td rowspan="2" width="20" class="center"></td>
                <td class="col10"><?php echo h_out($r['TrnSticker']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($r['TrnSticker']['item_name']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['item_code']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnSticker']['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnSticker']['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnSticker']['facility_sticker_no']); ?></td>
                <td class="col10"><?php echo h_out($this->Common->toCommaStr($r['TrnSticker']['price']),'right'); ?></td>
                <td class="col10"><?php echo $this->form->input("Regist.price.{$r['TrnSticker']['id']}",array('type' => 'text' , 'class'=>'lbl' ,'readonly'=>true, 'value'=>$this->request->data['Regist']['price'][$r['TrnSticker']['id']])); ?></td>
                <td class="col10">
                    <?php echo $this->form->input("Regist.work_type.{$r['TrnSticker']['id']}",array('options'=>$classes,'empty'=>true,'class' => 'txt', 'disabled' => true)); ?>
                </td>
                <td style="width:60px; text-align:center;">
                <?php echo $this->form->checkbox("Regist.is_retroactable.{$r['TrnSticker']['id']}",array('class'=>'center checkAllTarget','value'=>$r['TrnSticker']['id'],'hiddenField'=>false) ); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($r['TrnSticker']['standard']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['dealer_name']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['receiving_unit_name']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['validated_date']); ?></td>
                <td><?php echo $this->form->input("Regist.DepartmentId.{$r['TrnSticker']['id']}",array('options' => $traderList,'class' => 'txt' ,'value'=>$this->request->data['Regist']['DepartmentId'][$r['TrnSticker']['id']] , 'disabled' => true )); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($r['TrnSticker']['receiving_price']),'right'); ?></td>
                <td><?php echo h_out($r['TrnSticker']['quantity'],'right'); ?></td>
                <td colspan="2"><?php echo $this->form->input("Regist.recital.{$r['TrnSticker']['id']}",array('type' => 'text' , 'class'=>'lbl' , 'readonly'=>true)); ?></td>
            </tr>
            <?php $_cnt++;endforeach;?>
        </table>
    </div>
<?php echo $this->form->end(); ?>
