<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('#registForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });

    //備考一括設定ボタン押下
    $('#but_CollectiveSetting').click(function(){
        $("#registForm input[type=text].remarks").each(function(){
            $(this).val($("#ReturnCollectiveSetting").val());
        });
    });

    //確定ボタン押下
    $('#btn_Regist').click(function(){
        //errorMsg = "必須入力項目です";
        cnt = 0;
        submitFlg = true;
        isChecked = false;
        $("#registForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                isChecked = true;
            }
            cnt++;
        });
        if(!isChecked){
            alert("明細を選択してください");
            submitFlg = false;
            return false;
        }

        if(submitFlg){
            $("#registForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return2storages_regist').submit();
        }
    });

});

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/return2storages">入庫済返品登録</a></li>
        <li>入庫済返品確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入庫済返品確認</span></h2>
<h2 class="HeaddingMid">以下の内容で更新します</h2>

<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'registForm','class' => 'validate_form') ); ?>
    <?php echo $this->form->input('TrnReturn.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnReturn.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->text("setting.className",array('class' => 'lbl','style'=>'width:150px',"readonly"=>"readonly")); ?>
                    <?php echo $this->form->hidden("setting.classId"); ?>
                </td>
            </tr>
            <tr>
                <th>備考</th>
                <td>
                    <?php echo $this->form->text("setting.remarks",array('class' => 'lbl','style'=>'width:150px',"readonly"=>"readonly")); ?>
                </td>
            </tr>
        </table>
    </div>

    <hr class="Clear" />
    <div class="SearchBox">
        <table style="width:100%;">
            <tr>
                <td></td>
                <td align="right"></td>
            </tr>
            <tr>
                <td>表示件数：<?php echo count($result); ?>件</td>
                <td align="right">
                    <?php echo $this->form->text('CollectiveSetting',array('class' => 'txt','style' => 'width:150px'));?>
                    <input type="button" class="btn btn8 [p2]" id="but_CollectiveSetting">
                </td>
            </tr>
        </table>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th rowspan="2" width="20"><input type="checkbox" class="checkAll" checked></th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col10">ロット番号</th>
                    <th class="col15">センターシール</th>
                    <th class="col10">単価</th>
                    <th class="col10">返品単価</th>
                    <th class="col10">作業区分</th>
                    <th style="width:60px;">遡及<br>不可</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単位</th>
                    <th>有効期限</th>
                    <th>仕入先</th>
                    <th>仕入単価</th>
                    <th>数量</th>
                    <th colspan="2">備考</th>
                </tr>
            </table>
        </div>
    </div>

    <div class="TableScroll">
        <table class="TableStyle02 table-even2">
        <?php $_cnt=0;foreach($result as $r): ?>
            <tr>
                <td rowspan="2" width="20" class="center">
                    <?php if(empty($r['TrnSticker']['message'])){?>
                    <?php echo $this->form->checkbox("Regist.id.{$_cnt}",array('class'=>'center checkAllTarget','value'=>$r['TrnSticker']['id'],'checked'=>'checked','hiddenField'=>false) ); ?>
                    <?php }?>
                </td>
                <td class="col10"><?php echo h_out($r['TrnSticker']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($r['TrnSticker']['item_name']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['item_code']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnSticker']['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnSticker']['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnSticker']['facility_sticker_no']); ?></td>
                <td class="col10"><?php echo h_out($this->Common->toCommaStr($r['TrnSticker']['price']),'right'); ?></td>
                <td class="col10">
                <?php if(empty($r['TrnSticker']['message'])){ ?>
                <?php echo $this->form->input("Regist.price.{$r['TrnSticker']['id']}",array('type' => 'text' , 'class'=>'r txt' , 'value'=>$r['TrnSticker']['price'])); ?>
                <?php } ?>
                </td>
                <td class="col10">
                <?php if(empty($r['TrnSticker']['message'])){ ?>
                    <?php echo $this->form->input("Regist.work_type.{$r['TrnSticker']['id']}",array('options'=>$classes,'empty'=>true,'class' => 'txt')); ?>
                <?php } ?>
                </td>
                <td style="width:60px; text-align:center;">
                <?php if(empty($r['TrnSticker']['message'])){ ?>
                <?php echo $this->form->checkbox("Regist.is_retroactable.{$r['TrnSticker']['id']}",array('class'=>'center checkAllTarget','value'=>$r['TrnSticker']['id'],'hiddenField'=>false) ); ?>
                <?php } ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($r['TrnSticker']['standard']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['dealer_name']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['receiving_unit_name']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['validated_date']); ?></td>
                <td>
                <?php if(empty($r['TrnSticker']['message'])){ ?>
                <?php echo $this->form->input("Regist.DepartmentId.{$r['TrnSticker']['id']}",array('options' => $traderList,'class' => 'txt' ,'value'=>$r['TrnSticker']['facility_id'])); ?>
                <?php } ?>
                </td>
                <td><?php echo h_out($this->Common->toCommaStr($r['TrnSticker']['receiving_price']),'right'); ?></td>
                <td><?php echo h_out($r['TrnSticker']['quantity'],'right'); ?></td>
                <td colspan="2">
                <?php if(empty($r['TrnSticker']['message'])){ ?>
                    <?php echo $this->form->input("Regist.recital.{$r['TrnSticker']['id']}",array('type' => 'text' , 'class'=>'txt' , 'value'=>'')); ?>
                <?php } else { ?>
                    <span style="color:#FF0000;"><?php echo $r['TrnSticker']['message'] ?></span>
                <?php } ?>
                </td>
            </tr>
            <?php $_cnt++;endforeach;?>
        </table>
    </div>

    <table style="width:100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>

    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn2 [p2]" id="btn_Regist"/>
        </p>
    </div>
<?php echo $this->form->end(); ?>
