<script type="text/javascript">
$(document).ready(function(){
    //在庫選択ボタン押下
    $("#btn_Search").click(function(){
        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_deposits_stock');
        $("#searchForm").submit();
    });

    //シール読込ボタン押下
    $('#btn_Sticker').click(function(){
        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_deposits_sticker');
        $("#searchForm").submit();
    });

    //
    $("#work_type").change(function () {
        $('#work_type_name').val($('#work_type option:selected').text());
    });
    $("#department_selected").change(function () {
    // 部署プルダウン変更でコードを取得
        $('#department_selected_id').selectOptions($('#department_selected option:selected').val());
        $('#department_selected_code').selectOptions($('#department_selected_id option:selected').text());
        $('#department_txt').val($('#department_selected_code option:selected').val());
        $('#select_department_name').val($('#department_selected option:selected').text());
    });
  
    $('#department_txt').change(function(){
    // 部署コード変更でプルダウン変更
        $('#department_selected_code').selectOptions($('#department_selected_code').val());
        $('#department_selected_id').selectOptions($('#department_selected_code option:selected').text());
        $('#department_selected').selectOptions($('#department_selected_id option:selected').val());
        $('#select_department_name').val($('#department_selected option:selected').text());
    });

    $("#facility_selected").change(function (){
    // 施設プルダウン変更でコード取得
        $('#facility_selected_id').selectOptions($('#facility_selected option:selected').val());
        $('#facility_selected_code').selectOptions($('#facility_selected_id option:selected').text());
        $('#facility_txt').val($('#facility_selected_code option:selected').val());
        $('#select_facility_name').val($('#facility_selected option:selected').text());
        //施設変更で部署をいったん空にする
        $('#department_txt').val("");
        fieldName = $(this).attr('id');
        fieldValue = $('#facility_selected_code').val();
        $.post("<?php echo $this->webroot . $this->name; ?>/get_departments/",
        {
            field: fieldName,
            value: fieldValue
        },
        function (data) {
            var options = '';
            var options_id = '';
            var options_code = '';
            var obj = jQuery.parseJSON(data);
            options += '<option></option>';
            options_id += '<option></option>';
            options_code += '<option></option>';
            $.each(obj, function(index, o) {
                options += '<option value="' + o.id + '">' + o.department_name + '</option>';
                options_id += '<option value="' + o.id + '">' + o.department_code + '</option>';
                options_code += '<option value="' + o.department_code + '">' + o.id + '</option>';
            });
            $('#department_selected').html(options);
            $('#department_selected').show();
            $('#department_selected_id').html(options_id);
            $('#department_selected_code').html(options_code);
        },
        function (success) {
            // What do I do?
        },
        function(error) {
            if(error.length != 0) {
                $('#facility_selected').after('<div class="error-message" id="'+ fieldName +'-exists">' + error + '</div>');
            } else {
                $('#' + fieldName + '-exists').remove();
            }
        });
        return false;
    });
  
    $('#facility_txt').change(function(){
    //施設コード変更でプルダウン変更
        $('#select_facility_code').val($('#facility_txt').val());
        $('#facility_selected_code').selectOptions($('#select_facility_code').val());
        $('#facility_selected_id').selectOptions($('#facility_selected_code option:selected').text());
        $('#facility_selected').selectOptions($('#facility_selected_id option:selected').val());
        $('#select_facility_name').val($('#facility_selected option:selected').text());
        $('#department_txt').val("");
        fieldName = $(this).attr('id');
        fieldValue = $('#facility_selected_code').val();
        $.post("<?php echo $this->webroot . $this->name ; ?>/get_departments/",
        {
            field: fieldName,
            value: fieldValue
        },
        function (data) {
            var options = '';
            var options_id = '';
            var options_code = '';
            var obj = jQuery.parseJSON(data);
            options += '<option></option>';
            options_id += '<option></option>';
            options_code += '<option></option>';
            $.each(obj, function(index, o) {
                options += '<option value="' + o.id + '">' + o.department_name + '</option>';
                options_id += '<option value="' + o.id + '">' + o.department_code + '</option>';
                options_code += '<option value="' + o.department_code + '">' + o.id + '</option>';
            });

            $('#department_selected').html(options);
            $('#department_selected').show();
            $('#department_selected_id').html(options_id);
            $('#department_selected_code').html(options_code);
        },
        function (success) {
            // What do I do?
        },
        function(error) {
            if(error.length != 0) {
                $('#facility_selected').after('<div class="error-message" id="'+ fieldName +'-exists">' + error + '</div>');
            } else {
                $('#' + fieldName + '-exists').remove();
            }
        });
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>預託品返品登録</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>預託品返品登録</span></h2>
<h2 class="HeaddingMid">読み込み方法を選択してください</h2>
<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'searchForm','class' => 'validate_form') ); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col width="220"/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('MstFacility.facility_name', array('type'=>'hidden' , 'id'=>'select_facility_name'))?>
                    <?php echo $this->form->input('MstFacility.facility_txt',array('id' => 'facility_txt', 'class' => 'r', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('MstFacility.facility',array('options'=>$facility_enabled , 'id' => 'facility_selected' , 'class' => 'txt r validate[required]' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('MstFacility.facility_id',array('options'=>$facility_enabled_id ,'id' => 'facility_selected_id','style'=>'display:none;','empty'=>true)); ?>
                    <?php echo $this->form->input('MstFacility.facility_code',array('options'=>$facility_enabled_code , 'id' => 'facility_selected_code','style'=>'display:none;','empty'=>true)); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('Returns.work_type', array('options'=>$classes_List, 'class' => 'txt' ,'empty'=>true , 'id'=>'work_type')); ?>
                    <?php echo $this->form->input('Returns.work_type_name', array('type'=>'hidden' , 'id'=>'work_type_name')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('MstDepartment.department_name' , array('type'=>'hidden' , 'id'=>'select_department_name'))?>
                    <?php echo $this->form->input('MstDepartment.department_txt',array('id' => 'department_txt', 'class' => 'r', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('MstDepartment.department',array('options'=>array() , 'id' => 'department_selected','class' => 'txt r validate[required]','empty'=>true)); ?>
                    <?php echo $this->form->input('MstDepartment.department_id',array('options'=>array() , 'id' => 'department_selected_id','style'=>'display:none;','empty'=>true)); ?>
                    <?php echo $this->form->input('MstDepartment.department_code',array('options'=>array() , 'id' => 'department_selected_code','style'=>'display:none;','empty'=>true)); ?>
               </td>
               <td></td>
               <th>備考</th>
               <td><?php echo $this->form->text('search.recital',array('class' => 'txt','style' => 'width:110px'));?></td>
               <td></td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn17" id="btn_Sticker"/>&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" class="btn btn15" id="btn_Search"/>
            </p>
        </div>
    </div>
<?php echo $this->form->end(); ?>
