<script type="text/javascript">
$(document).ready(function(){

    //検索ボタン押下
    $("#btn_Search").click(function searchItem(){
        //カート欄にある全IDを取得
        var tempId = "";
        $("#cartForm input[type=checkbox].checkAllTarget").each(function(){
          tempId += $(this).val() + ",";
        });
        $("#tempId").val(tempId);

        $("#searchForm").attr('action', '<?php echo $this->webroot; ?>returns/return2storages_search').submit();
    });

    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#cartFrom input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        var objArray = new Array();
        var isChecked = false;
        //$('#itemSelectedTable').empty();
        $("#searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#SelectedItemId' + cnt).val( $(this).val() );
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
                isChecked = true;
            }else{
                $('#SelectedItemId' + cnt).val("0");
            }
            cnt++;
        });
        if(!isChecked){
            alert('明細を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        var tempId = "";
        $("#cartForm input[type=checkbox].checkAllTarget").each(function(){
          if($(this).attr("checked") == true){
              tempId += $(this).val() + ",";
          }
        });
        $("#temp_id").val(tempId);

        var cnt = 0;
        var objArray = new Array();
        $("#cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
            }
        });
        if(cnt === 0){
            alert('確認する明細を選択してください');
        }else{
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return2storages_confirm');
            $("#searchForm").submit();
        }
    });

});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/return2storages">入庫済返品登録</a></li>
        <li>在庫検索</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>在庫検索</span></h2>
<h2 class="HeaddingMid">返品を行いたい商品を検索してください。</h2>

<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'searchForm','class' => 'validate_form search_form') );?>
    <?php echo $this->form->input('search.is_search',array('type' => 'hidden','id' => 'is_search'));?>
    <?php echo $this->form->input('search.tempId',array('type'=>'hidden','id' =>'tempId'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->text("setting.className",array('class' => 'lbl','style'=>'width:150px',"readonly"=>"readonly")); ?>
                    <?php echo $this->form->hidden("setting.classId"); ?>
                </td>
            </tr>
            <tr>
                <th>備考</th>
                <td>
                    <?php echo $this->form->text("setting.remarks",array('class' => 'lbl' ,'style'=>'width:150px',"readonly"=>"readonly")); ?>
                </td>
            </tr>
        </table>

        <h2 class="HeaddingLarge2">検索条件</h2>

        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="60" align="right" />
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('search.item_id',array('class'=>'txt search_internal_code','style'=>'width:120px; ime-mode:disabled;')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('search.item_code',array('class'=>'txt search_upper','style'=>'width:120px; ime-mode:disabled;')); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->text('search.lot_no',array('class'=>'txt','style'=>'width:120px; ime-mode:disabled;')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('search.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('search.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('search.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->text('search.trn_stickers_id',array('class'=>'txt','style'=>'width:120px; ime-mode:disabled;')); ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>

    <div class="ButtonBox">
      <p class="center">
        <input type="button" class="btn btn1" id="btn_Search"/>
      </p>
    </div>
  </div>

  <hr class="Clear" />

<?php
    if($SearchFlg){
?>

  <div id="results">
    <div class="DisplaySelect">
      <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
    </div>

    <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
          <th rowspan="2" width="20"><input type="checkbox" class="checkAll_1"></th>
          <th class="col10">商品ID</th>
          <th>商品名</th>
          <th>製品番号</th>
          <th class="col15">包装単位</th>
          <th class="col15">ロット番号</th>
          <th class="col15">センターシール</th>
          <th class="col10">単価</th>
        </tr>
        <tr>
          <th></th>
          <th>規格</th>
          <th>販売元</th>
          <th>仕入単位</th>
          <th>有効期限</th>
          <th>仕入先</th>
          <th>仕入単価</th>
        </tr>
      </table>
    </div>

    <div class="TableScroll">
      <?php $_cnt=0;foreach($SearchResult as $_row): ?>
      <table class="TableStyle02" style="margin-top:-1px; margin-bottom:0px; padding:0px;" id="<?php echo 'containTables'.$_row['TrnSticker']['id'];?>">
        <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php "itemSelectedRow".$_row["TrnSticker"]['id'];?> >
          <td rowspan="2" width="20" class="center">
            <?php echo $this->form->checkbox("SelectedTrnStickerId.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row['TrnSticker']['id']) ); ?>
          </td>
          <td class="col10"><?php echo h_out($_row['FIT']['internal_code'],'center'); ?></td>
          <td><?php echo h_out($_row['FIT']['item_name']); ?></td>
          <td><?php echo h_out($_row['FIT']['item_code']); ?></td>
          <td class="col15">
            <label title="<?php
            if($_row["ITU"]['per_unit'] == 1){
              echo $_row["UTN1"]['unit_name'];
            }else{
              echo $_row["UTN1"]['unit_name']."(".$_row["ITU"]['per_unit'].$_row["UTN2"]['unit_name'].")";
            }
            ?>">
              <?php
              if($_row["ITU"]['per_unit'] == 1){
                echo $_row["UTN1"]['unit_name'];
              }else{
                echo $_row["UTN1"]['unit_name']."(".$_row["ITU"]['per_unit'].$_row["UTN2"]['unit_name'].")";
              }
              ?>
            </label>
          </td>
          <td class="col15"><?php echo h_out($_row['TrnSticker']['lot_no']); ?></td>
          <td class="col15"><?php echo h_out($_row['TrnSticker']['facility_sticker_no']); ?></td>
          <td class="col10"><?php echo h_out($this->Common->toCommaStr($_row['calculate']['unit_price']),'right'); ?></td>
        </tr>
        <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>">
          <td></td>
          <td><?php echo h_out($_row['FIT']['standard']);?></td>
          <td><?php echo h_out($_row['DLR']['dealer_name']);?></td>
          <td>
            <label title="<?php
            if($_row["IUN"]['per_unit'] == 1){
              echo $_row["UNA1"]['unit_name'];
            }else{
              echo $_row["UNA1"]['unit_name']."(".$_row["IUN"]['per_unit'].$_row["UNA2"]['unit_name'].")";
            }
            ?>">
              <?php
              if($_row["IUN"]['per_unit'] == 1){
                echo $_row["UNA1"]['unit_name'];
              }else{
                echo $_row["UNA1"]['unit_name']."(".$_row["IUN"]['per_unit'].$_row["UNA2"]['unit_name'].")";
              }
              ?>
            </label>
          </td>
          <td><?php echo h_out($_row['TrnSticker']['validated_date'] , 'center'); ?></td>
          <td><?php echo h_out($_row['FCTF']['facility_name']);?></td>
          <td><?php echo h_out($this->Common->toCommaStr($_row["RCV"]['stocking_price']),'right');?></td>
        </tr>
      </table>
      <?php $_cnt++;endforeach;?>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" value="" class="btn btn4" id="cmdSelect" />
      </p>
    </div>
  </div>


  <div id="cartForm">
    <?php echo $this->form->hidden("Search.Url",array("value"=>"return2storages_search")); ?>

    <?php echo $this->form->input('search.temp_id',array('type'=>'hidden','id' =>'temp_id'));?>

    <h2 class="HeaddingSmall">選択商品</h2>

    <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
          <th rowspan="2" width="20"><input type="checkbox" class="checkAll_2" checked></th>
            <th class="col10">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th class="col15">包装単位</th>
            <th class="col15">ロット番号</th>
            <th class="col15">センターシール</th>
            <th class="col10">単価</th>
          </tr>
        <tr>
          <th></th>
          <th>規格</th>
          <th>販売元</th>
          <th>仕入単位</th>
          <th align="center">有効期限</th>
          <th>仕入先</th>
          <th>仕入単価</th>
        </tr>
      </table>
    </div>
    <?php $_cnt=0;foreach($SearchResult as $_row): ?>
      <?php echo $this->form->hidden("SelectedItem.Id.{$_cnt}"); ?>
    <?php $_cnt++;endforeach;?>
    <div class="TableScroll" style="">

      <?php if(isset($CartSearchResult)){?>
      <?php $_cnt=0;foreach($CartSearchResult as $_row): ?>
      <table class="TableStyle02" style="margin-top:-1px; margin-bottom:0px; padding:0px;" id="<?php echo 'containTables'.$_row["TrnSticker"]["id"];?>">
        <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php "itemSelectedRow".$_row["TrnSticker"]['id'];?> >
          <td rowspan="2" width="20" class="center">
            <?php echo $this->form->checkbox("SelectedTrnStickerId.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row["TrnSticker"]["id"] , 'checked'=>true) ); ?>
          </td>
          <td class="col10"><?php echo h_out($_row['FIT']['internal_code'],'center'); ?></td>
          <td><?php echo h_out($_row['FIT']['item_name']); ?></td>
          <td><?php echo h_out($_row['FIT']['item_code']); ?></td>
          <td class="col15">
            <label title="<?php
            if($_row["ITU"]['per_unit'] == 1){
              echo $_row["UTN1"]['unit_name'];
            }else{
              echo $_row["UTN1"]['unit_name']."(".$_row["ITU"]['per_unit'].$_row["UTN2"]['unit_name'].")";
            }
            ?>">
              <?php
              if($_row["ITU"]['per_unit'] == 1){
                echo $_row["UTN1"]['unit_name'];
              }else{
                echo $_row["UTN1"]['unit_name']."(".$_row["ITU"]['per_unit'].$_row["UTN2"]['unit_name'].")";
              }
              ?>
            </label>
          </td>
          <td class="col15"><?php echo h_out($_row['TrnSticker']['lot_no']); ?></td>
          <td class="col15"><?php echo h_out($_row['TrnSticker']['facility_sticker_no']); ?></td>
          <td class="col10"><?php echo h_out($this->Common->toCommaStr($_row['calculate']['unit_price']),'right'); ?></td>
        </tr>
        <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>">
          <td></td>
          <td><?php echo h_out($_row['FIT']['standard']);?></td>
          <td><?php echo $_row['DLR']['dealer_name'];?></td>
          <td>
            <label title="<?php echo $_row["UNA1"]['unit_name']; ?>(<?php echo $_row["IUN"]['per_unit'].$_row["UNA2"]['unit_name']; ?>)">
              <?php echo $_row["UNA1"]['unit_name']; ?>(<?php echo $_row["IUN"]['per_unit'].$_row["UNA2"]['unit_name']; ?>)
            </label>
          </td>
          <td><?php echo h_out($_row['TrnSticker']['validated_date'],'center'); ?></td>
          <td><?php echo h_out($_row['FCTF']['facility_name']); ?></td>
          <td><?php echo h_out($this->Common->toCommaStr($_row['RCV']['stocking_price']),'right');?></td>
        </tr>
      </table>
      <?php $_cnt++;endforeach;?>
      <?php }?>
      <div id="itemSelectedTable"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" class="btn btn3"/>
      </p>
    </div>
    <?php }?>

  </div>
<?php echo $this->form->end(); ?>