<script type="text/javascript">
$(document).ready(function(){
    //取消ボタン押下
    $('#btn_delete').click(function(){
        if(window.confirm("取消を実行します。よろしいですか？")){
        $("#searchForm").attr('action','<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_history_result').submit();
        }
    });

    //備考一括設定ボタン押下
    $('#btn_recital').click(function(){
        $('.Recital').val($('#recital_txt').val());
    });

    //チェックボックス一括
    $('#all_check').click(function(){
        $('.check').attr('checked' , $(this).attr('checked'));
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/return_history">返品履歴</a></li>
        <li>返品履歴明細</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>返品履歴明細</span></h2>
<h2 class="HeaddingMid">返品履歴明細</h2>

<form id="searchForm" class="validate_form" method="post">
    <?php echo $this->form->input('TrnReturn.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnReturn.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
    <div class="results">
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
            　検索結果：<?php echo count($result); ?>件
            </span>
            <span class="BikouCopy">
                <?php print $this->form->input('all_details_recital', array('id'=>'allDetailsRecital','type'=>'text', 'class'=>'txt' , 'id'=>'recital_txt')); ?>
                <input type="button" class="btn btn8 [p2]" value="" id="btn_recital"/>
            </span>
        </div>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="25" />
                    <col width="130"/>
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col width="150"/>
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th rowspan=2><input type="checkbox" checked id='all_check' /></th>
                    <th>返品番号</th>
                    <th>仕入先</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>数量</th>
                    <th>ロット番号</th>
                    <th>部署シール番号</th>
                    <th>作業区分</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th>返品日</th>
                    <th>施設</th>
                    <th>区分</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>有効期限</th>
                    <th>センターシール</th>
                    <th colspan=2>備考</th>
                </tr>
                <?php $cnt=0; $id_list = "";
                    foreach ($result as $r) { ?>
                <tr>
                    <td rowspan=2 class="center">
                        <!--締め済みの場合は、チェックボックスを表示しない -->
                        <?php if($r['Returns']['status'] == ""){ ?>
                        <input name="data[TrnReceiving][id][]" checked id="receiving_id_<?php echo $cnt ?>" type="checkbox" class="validate[minCheckbox[1]] check" value="<?php echo $r['Returns']['id'] ?>"/>
                        <?php  } ?>
                    </td>
                    <td><?php echo h_out($r['Returns']['work_no']); ?></td>
                    <td><?php echo h_out($r['Returns']['facility_name_to']); ?></td>
                    <td><?php echo h_out($r['Returns']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Returns']['item_name']); ?></td>
                    <td><?php echo h_out($r['Returns']['item_code']); ?></td>
                    <td><?php echo h_out($r['Returns']['quantity'],'right'); ?></td>
                    <td><?php echo h_out($r['Returns']['lot_no']); ?></td>
                    <td><?php echo h_out($r['Returns']['hospital_sticker_no']); ?></td>
                    <td><?php echo h_out($r['Returns']['class_name']); ?></td>
                    <td><?php echo h_out($r['Returns']['user_name']); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($r['Returns']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($r['Returns']['facility_name_from']); ?></td>
                    <td><?php echo h_out($r['Returns']['receiving_type_name'],'center'); ?></td>
                    <td><?php echo h_out($r['Returns']['standard']); ?></td>
                    <td><?php echo h_out($r['Returns']['dealer_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($r['Returns']['stocking_price']),'right'); ?></td>
                    <td><?php echo h_out($r['Returns']['validated_date'],'center'); ?></td>
                    <td><?php echo h_out($r['Returns']['facility_sticker_no']); ?></td>
                    <td colspan=2>
                        <?php echo $this->form->input('TrnReceiving.recital.'.$r['Returns']['id'], array('type'=>'text', 'value'=>$r['Returns']['recital'], 'class'=>'txt Recital', 'style'=>'width:90%;')) ?>
                        <?php echo $this->form->input('TrnSticker.modified.'.$r['Returns']['sticker_id'] , array('type'=>'hidden' , 'value'=>$r['Returns']['modified'] )); ?>
                    </td>
               </tr>
      <?php $cnt++;
      $id_list .= $r['Returns']['id']. ',';
    }
    $id_list_array = rtrim($id_list,',');
?>
               <input type="hidden" id="cnt" value="<?php print $cnt; ?>"/>
               <input type="hidden" id="id_list" value="<?php print $id_list_array; ?>"/>
           </table>
      </div>
      <div class="ButtonBox">
          <p class="center">
              <input type="button" class="btn btn6 [p2]" id="btn_delete"/>
          </p>
      </div>
    </div>
  </form>
