<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/return_history">返品履歴</a></li>
        <li>返品取消</li>
     </ul>
</div>
<h2 class="HeaddingLarge"><span>返品取消</span></h2>
<h2 class="HeaddingMid"><?php print $msg; ?></h2>
<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'searchForm','class' => 'validate_form') ); ?>
    <div class="results">
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="120"/>
                    <col />
                    <col />
                    <col width="120"/>
                    <col />
                    <col />
                    <col />
                    <col width="120"/>
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>返品番号</th>
                    <th>仕入先</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>数量</th>
                    <th>ロット番号</th>
                    <th>部署シール</th>
                    <th>作業区分</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th>返品日</th>
                    <th>施設</th>
                    <th>区分</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>有効期限</th>
                    <th>センターシール</th>
                    <th colspan=2>備考</th>
                </tr>
<?php
  $cnt=0;
    foreach ($ReturnList as $return) {
      if(isset($classes[$return['TrnReceiving']['work_type']])){
        $class_name = $classes[$return['TrnReceiving']['work_type']];
      }else{
        $class_name = '';
      } ?>
                <tr>
                    <td><?php echo h_out($return['TrnReturnHeader']['work_no']); ?></td>
                    <td><?php echo h_out($return['MstFacilityTo']['facility_name']); ?></td>
                    <td><?php echo h_out($return['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($return['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($return['MstFacilityItem']['item_code']); ?></td>
                    <td><?php echo h_out($return['TrnReceiving']['remain_count'],'right'); ?></td>
                    <td><?php echo h_out($return['TrnSticker']['lot_no']); ?></td>
                    <td><?php echo h_out($return['TrnSticker']['hospital_sticker_no']); ?></td>
                    <td><?php echo h_out($class_name); ?></td>
                    <td><?php echo h_out($return['MstUser']['user_name']); ?></td>
                </tr>
                <tr>
                    <?php if(empty($return['TrnReturnHeader']['work_date'])){ ?>
                    <td align='center'></td>
                    <?php }else{ ?>
                    <td><?php echo h_out($return['TrnReturnHeader']['work_date'],'center'); ?></td>
                    <?php } ?>
                    <td><?php echo h_out($return['MstFacilityFrom']['facility_name']); ?></td>
                    <td></td>
                    <td><?php echo h_out($return['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($return['MstDealer']['dealer_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($return['TrnReceiving']['stocking_price']),'right') ?></td>
                    <td><?php echo h_out($return['TrnSticker']['validated_date'],'center'); ?></td>
                    <td><?php echo h_out($return['TrnSticker']['facility_sticker_no']); ?></td>
                    <td colspan=2>
                        <?php echo $this->form->input('TrnReceiving.recital.'.$cnt, array('type'=>'text','value'=>$return['TrnReceiving']['recital'], 'class'=>'lbl' ,'readonly'=>'readonly')); ?>
                    </td>
                </tr>
                <?php $cnt++; } ?>
            </table>
        </div>
    </div>
<?php echo $this->form->end(); ?>
