<script type="text/javascript">

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#confirmForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );
  
    //検索ボタン押下
    $("#btn_Search").click(function(){
        //発注日FROM
        if($("#datepicker1").val() != ""){
            var errorMsg1 = dateChecker($("#datepicker1").val());
            if(errorMsg1 != ""){
                alert(errorMsg1);
                $("#datepicker1").focus();
                return false;
            }
        }
        //発注日TO
        if($("#datepicker2").val() != ""){
            var errorMsg2 =dateChecker($("#datepicker2").val());
            if(errorMsg2 != ""){
                alert(errorMsg2);
                $("#datepicker2").focus();
                return false;
            }
        }
        //入荷日FROM
        if($("#datepicker3").val() != ""){
            var errorMsg3 =dateChecker($("#datepicker3").val());
            if(errorMsg3 != ""){
                alert(errorMsg3);
                $("#datepicker3").focus();
                return false;
            }
        }
        //入荷日TO
        if($("#datepicker4").val() != ""){
            var errorMsg4 =dateChecker($("#datepicker4").val());
            if(errorMsg4 != ""){
                alert(errorMsg4);
                $("#datepicker4").focus();
                return false;
            }
        }
        //カート欄にある全IDを取得
        var tempId = "";
        $("#confirmForm input[type=checkbox].checkAllTarget").each(function(){
          tempId += $(this).val() + ",";
        });
        $("#tempId").val(tempId);

        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return2receivings').submit();
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        var objArray = new Array();
        var isChecked = false;
        $('#itemSelectedTable').empty();
        $("#searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#SelectedTrnReceivingId' + cnt).val( $(this).val() );
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
                isChecked = true;
            }else{
                $('#SelectedTrnReceivingId' + cnt).val("0");
            }
            cnt++;
        });
        if(!isChecked){
            alert('明細を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        var tempId = "";
        $("#confirmForm input[type=checkbox].checkAllTarget:checked").each(function(){
          if($(this).attr("checked") == true){
              tempId += $(this).val() + ",";
          }
        });
        $("#temp_id").val(tempId);

        //選択チェック
        var cnt = 0;
        var objArray = new Array();
        $("#confirmForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
            }
        });
        if(cnt === 0){
            alert('確認する明細を選択してください');
        }else{
            $("#confirmForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/return2receivings_confirm').submit();
        }
    });

});

//日付形式チェック
function dateChecker(date){
    var msg = "";
    //YYYY/MM/DD形式かどうか
    if(!date.match(/^\d{4}\/\d{2}\/\d{2}$/)){
        msg = "日付はYYYY/MM/DD形式で入力してください";
        return msg;
    }

    //入力された日付の妥当性チェック
    var tempYear = date.substr(0, 4) - 0;
    var tempMonth = date.substr(5, 2) - 1;
    var tempDay = date.substr(8, 2) - 0;

    if(tempYear >= 1970 && tempMonth >= 0 && tempMonth <= 11 && tempDay >= 1 && tempDay <= 31){
        var checkDate = new Date(tempYear, tempMonth, tempDay);
        if(isNaN(checkDate)){
            msg = "入力された日付が不正です";
        }else if(checkDate.getFullYear() == tempYear && checkDate.getMonth() == tempMonth && checkDate.getDate() == tempDay){
            //
        }else{
            msg = "入力された日付が不正です";
        }
    }else{
        msg = "入力された日付が不正です";
    }
    return msg;
}

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>入荷済返品登録</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>入荷済返品登録</span></h2>
<h2 class="HeaddingMid">返品を行いたい商品を検索してください。</h2>
<?php echo $this->form->create( 'Return',array('type' => 'post','action' => 'return2receivings' ,'id' =>'searchForm','class' => 'validate_form search_form') );?>
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('search.tempId',array('type'=>'hidden','id' =>'tempId'));?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="60" align="right"/>
            </colgroup>
            <tr>
                <th>発注番号</th>
                <td><?php echo $this->form->text('search.work_no1',array('class'=>'txt','style'=>'width:110px'));?></td>
                <td></td>
                <th>発注日</th>
                <td>
                    <?php echo $this->form->text('search.work_date1',array('class'=>'txt date','maxlength'=>'10','id'=>'datepicker1'));?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo $this->form->text('search.work_date2',array('class'=>'txt date','maxlength'=>'10', 'id'=>'datepicker2')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>入荷番号</th>
                <td><?php echo $this->form->text('search.work_no2',array('class'=>'txt','style'=>'width:110px'));?></td>
                <td></td>
                <th>入荷日</th>
                <td>
                    <?php echo $this->form->text('search.work_date3',array('class'=>'txt date','maxvalue'=>'10', 'id'=>'datepicker3'));?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo $this->form->text('search.work_date4',array( 'class'=>'txt date', 'maxvalue'=>'10', 'id'=>'datepicker4') );?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('search.item_id',array('class'=>'txt search_internal_code','style'=>'width:120px')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('search.item_code',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
                <th>管理区分</th>
                <td><?php echo $this->form->input('search.management_class',array('type'=>'select','options'=>$managementClass,'empty'=>true , 'class'=>'txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('search.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('search.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('search.work_type',array('type'=>'select' , 'options'=>$classes,'empty'=>true,'class'=>'txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('search.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('search.jan_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>

        <div class="ButtonBox">
           <p class="center">
             <input type="button" class="btn btn1" id="btn_Search"/>
           </p>
        </div>
    </div>

<?php
    if($SearchFlg){
      if(count($SearchResult) === 0){
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <colgroup>
            <col width="20" />
            <col width="100" />
            <col width="90" />
            <col width="100" />
            <col width="100" />
            <col />
            <col />
            <col width="100"/>
          </colgroup>
          <tr>
            <th rowspan="2"><input type="checkbox" class="checkAll_1"></th>
            <th>発注番号</th>
            <th>発注日</th>
            <th>入荷番号</th>
            <th>商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th>入荷数</th>
          </tr>
          <tr>
            <th colspan="2">仕入先</th>
            <th>入荷日</th>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>仕入単価</th>
          </tr>
        </table>
      </div>
      <table class="TableStyle02" border=0><tr><td colspan="8" class="center"><span class="center">該当するデータはありません。</span></td></tr></table>
<?php
      }else{
?>
    <div id="results">
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <colgroup>
            <col width="20" />
            <col width="100" />
            <col width="90" />
            <col width="100" />
            <col width="100" />
            <col />
            <col />
            <col width="100"/>
          </colgroup>
          <tr>
            <th rowspan="2" style="padding:0px;"><input type="checkbox" class="checkAll_1"></th>
            <th>発注番号</th>
            <th>発注日</th>
            <th>入荷番号</th>
            <th>商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th>入荷数</th>
          </tr>
          <tr>
            <th colspan="2">仕入先</th>
            <th>入荷日</th>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>仕入単価</th>
          </tr>
        </table>
      </div>

      <div class="TableScroll" style="">
        <?php $_cnt=0;foreach($SearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row['TrnReceiving']['id'];?>" style="margin-top:-1px; margin-bottom:0px; padding:0px;">
          <colgroup>
            <col width="20" />
            <col width="100" />
            <col width="90"  align="center" />
            <col width="100" align="center" />
            <col width="100" align="center" />
            <col />
            <col />
            <col width="100" align="right" />
          </colgroup>
          <tr id=<?php "itemSelectedRow".$_row['TrnReceiving']['id'];?> >
            <td rowspan="2" style="padding:0px;" class="center">
              <?php echo $this->form->checkbox("SelectedTrnReceivingID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row['TrnReceiving']['id']) ); ?>
            </td>
            <td><?php echo h_out($_row['ODR']['work_no']); ?></td>
            <td><?php echo h_out(date('Y/m/d',strtotime($_row['ODR']['work_date'])),'center'); ?></td>
            <td><?php echo h_out($_row['TrnReceiving']['work_no'],'center'); ?></td>
            <td><?php echo h_out($_row['FIT']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($_row['FIT']['item_name']); ?></td>
            <td><?php echo h_out($_row['FIT']['item_code']); ?></td>
            <td><?php echo h_out(number_format($_row['TrnReceiving']['remain_count']),'right'); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo h_out($_row['FCT']['facility_name']); ?></td>
            <td><?php echo h_out(date('Y/m/d',strtotime($_row['TrnReceiving']['work_date'])),'center'); ?></td>
            <td></td>
            <td><?php echo h_out($_row['FIT']['standard']); ?></td>
            <td><?php echo h_out($_row['DLR']['dealer_name']); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($_row['TrnReceiving']['stocking_price']),'right'); ?></td>
          </tr>
        </table>
        <?php $_cnt++;endforeach;?>
      </div>

      <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
          <input type="button" class="btn btn4" id="cmdSelect" />
        </p>
      </div>
    </div>
<?php
      }
  echo $this->form->end();
?>

<?php
    echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'confirmForm','class' => 'validate_form') );
?>

  <?php echo $this->form->hidden('search.work_no1',array('value'=>$data["search"]["work_no1"]));?>
  <?php echo $this->form->hidden('search.work_date1',array("value"=>$data["search"]["work_date1"])); ?>
  <?php echo $this->form->hidden('search.work_date2',array("value"=>$data["search"]["work_date2"])); ?>
  <?php echo $this->form->hidden('search.work_no2',array('value'=>$data["search"]["work_no2"]));?>
  <?php echo $this->form->hidden('search.work_date3',array("value"=>$data["search"]["work_date3"])); ?>
  <?php echo $this->form->hidden('search.work_date4',array("value"=>$data["search"]["work_date4"])); ?>
  <?php echo $this->form->hidden('search.item_id',array('value'=>$data["search"]["item_id"])); ?>
  <?php echo $this->form->hidden('search.item_code',array('value'=>$data["search"]["item_code"])); ?>
  <?php echo $this->form->hidden('search.management_class',array('value'=>$data["search"]["management_class"])); ?>
  <?php echo $this->form->hidden('search.item_name',array('value'=>$data['search']['item_name'])); ?>
  <?php echo $this->form->hidden('search.dealer_name',array('value'=>$data['search']['dealer_name'])); ?>
  <?php echo $this->form->hidden('search.work_type',array('value'=>$data['search']['work_type'])); ?>
  <?php echo $this->form->hidden('search.standard',array('value'=>$data['search']['standard'])); ?>
  <?php echo $this->form->hidden('search.jan_code',array('value'=>$data['search']['jan_code'])); ?>

  <?php echo $this->form->input('search.temp_id',array('type'=>'hidden','id' =>'temp_id'));?>

    <?php if($SearchFlg){?>
    <h2 class="HeaddingSmall">選択商品</h2>
    <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <colgroup>
          <col width="20" />
          <col width="100" />
          <col width="90" />
          <col width="100" />
          <col width="100" />
          <col />
          <col />
          <col width="100" />
        </colgroup>
        <tr>
          <th rowspan="2" style="padding:0px;"><input type="checkbox" class="checkAll_2"></th>
          <th>発注番号</th>
          <th>発注日</th>
          <th>入荷番号</th>
          <th>商品ID</th>
          <th>商品名</th>
          <th>製品番号</th>
          <th>入荷数</th>
        </tr>
        <tr>
          <th colspan="2">仕入先</th>
          <th>入荷日</th>
          <th></th>
          <th>規格</th>
          <th>販売元</th>
          <th>仕入単価</th>
        </tr>
      </table>
    </div>
    <?php $_cnt=0;foreach($SearchResult as $_row): ?>
      <?php echo $this->form->hidden("SelectedTrnReceiving.Id.{$_cnt}"); ?>
    <?php $_cnt++;endforeach;?>
    <div class="TableScroll" style="">
      <?php if(isset($CartSearchResult)){ ?>
      <?php $_cnt=0;foreach($CartSearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row['TrnReceiving']['id'];?>" style="margin-top:-1px; margin-bottom:0px; padding:0px;">
          <colgroup>
            <col width="20" />
            <col width="100" />
            <col width="90"  align="center" />
            <col width="100" align="center" />
            <col width="100" align="center" />
            <col />
            <col />
            <col width="100" align="right" />
          </colgroup>
          <tr id=<?php "itemSelectedRow".$_row['TrnReceiving']['id'];?> >
            <td rowspan="2" style="padding:0px;" class="center">
              <?php echo $this->form->checkbox("SelectedTrnReceivingID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row['TrnReceiving']['id'] , 'checked'=>'checked') ); ?>
            </td>
            <td><?php echo h_out($_row['ODR']['work_no']); ?></td>
            <td>
              <label title="<?php echo date('Y/m/d',strtotime($_row["ODR"]['work_date'])); ?>">
                <p align ="center"><?php echo date('Y/m/d',strtotime($_row["ODR"]['work_date'])); ?></p>
              </label>
            </td>
            <td><?php echo h_out($_row['TrnReceiving']['work_no'],'center'); ?></td>
            <td><?php echo h_out($_row['FIT']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($_row['FIT']['item_name']); ?></td>
            <td><?php echo h_out($_row['FIT']['item_code']); ?></td>
            <td><?php echo h_out(number_format($_row['TrnReceiving']['remain_count']),'right'); ?></td>
          </tr>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>">
            <td colspan="2"><?php echo h_out($_row['FCT']['facility_name']); ?></td>
            <td>
              <label title="<?php echo date('Y/m/d',strtotime($_row["TrnReceiving"]['work_date'])); ?>">
                <p align ="center"><?php echo date('Y/m/d',strtotime($_row["TrnReceiving"]['work_date'])); ?></p>
              </label>
            </td>
            <td></td>
            <td><?php echo h_out($_row['FIT']['standard']); ?></td>
            <td><?php echo h_out($_row['DLR']['dealer_name']); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($_row['TrnReceiving']['stocking_price']),'right'); ?></td>
          </tr>
        </table>
      <?php $_cnt++;endforeach;?>
      <?php }?>
      <div id="itemSelectedTable"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm"  class="btn btn3" />
      </p>
    </div>
    <?php }?>
<?php
    }else{
      //検索実行フラグがfalse
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>0)); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <colgroup>
            <col width="20" />
            <col width="100" />
            <col width="90" />
            <col width="100" />
            <col width="100" />
            <col />
            <col />
            <col width="100"/>
          </colgroup>
          <tr>
            <th rowspan="2"><input type="checkbox" class="checkAll_1"></th>
            <th>発注番号</th>
            <th>発注日</th>
            <th>入荷番号</th>
            <th>商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th>入荷数</th>
          </tr>
          <tr>
            <th colspan="2">仕入先</th>
            <th>入荷日</th>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>仕入単価</th>
          </tr>
        </table>
      </div>
<?php
    }
    echo $this->form->end();
?>

