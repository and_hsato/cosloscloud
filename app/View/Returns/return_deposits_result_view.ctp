<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/return_deposits">預託品返品登録</a></li>
        <li class="pankuzu">預託品返品確認</li>
        <li>預託品返品登録結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>預託品返品登録結果</span></h2>
<h2 class="HeaddingMid"><?php print $msg; ?></h2>
<?php echo $this->form->create( 'Return',array('type' => 'post','action' => '' ,'id' =>'searchForm','class' => 'validate_form') ); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo  $this->form->input('MstFacility.facility_name',array('class' => 'lbl', 'style'=>'width: 110px;', 'readonly'=>'readonly')); ?>
                </td>
                <td width="20px"></td>
                <th>作業区分</th>
                <td>
                <?php echo $this->form->input('Returns.work_type_name',array('class' => 'lbl', 'style'=>'width: 110px;', 'readonly'=>'readonly')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                <?php echo  $this->form->input('MstDepartment.department_name',array('class' => 'lbl', 'style'=>'width: 110px;', 'readonly'=>'readonly')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                <?php echo $this->form->text('search.recital',array('class' => 'lbl','style' => 'width:110px', 'readonly'=>'readonly'));?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
             <span class="DisplaySelect">
             　表示件数：<?php echo count($result); ?>件
             </span>
             <span class="BikouCopy"></span>
         </div>
         <div class="TableScroll">
             <table class="TableStyle01">
                 <colgroup>
                     <col />
                     <col />
                 </colgroup>
                 <tr>
                     <th>商品ID</th>
                     <th>商品名</th>
                     <th>製品番号</th>
                     <th>包装単位</th>
                     <th>ロット番号</th>
                     <th>部署シール</th>
                     <th>作業区分</th>
                     <th>遡及<br>不可</th>
                 </tr>
                 <tr>
                     <th></th>
                     <th>規格</th>
                     <th>販売元</th>
                     <th></th>
                     <th>有効期限</th>
                     <th>仕入先</th>
                     <th colspan=2>備考</th>
                 </tr>
                 <?php
                     $cnt=0;
                     foreach ($result as $r) {
                 ?>
                 <tr>
                     <td><?php echo h_out($r['Returns']['internal_code'],'center'); ?></td>
                     <td><?php echo h_out($r['Returns']['item_name']); ?></td>
                     <td><?php echo h_out($r['Returns']['item_code']); ?></td>
                     <td><?php echo h_out($r['Returns']['unit_name']); ?></td>
                     <td><?php echo h_out($r['Returns']['lot_no']); ?></td>
                     <td><?php echo h_out($r['Returns']['hospital_sticker_no']); ?></td>
                     <td><?php echo $this->form->input('RCV.work_typde', array('type'=>'text','value'=>$r['Returns']['class_name'], 'readonly'=>'readonly', 'class'=>'lbl','style'=>'width:105px;')) ?></td>
                     <td><?php echo h_out($r['Returns']['is_retroactable'],'center'); ?></td>
                 </tr>
                 <tr>
                     <td></td>
                     <td><?php echo h_out($r['Returns']['standard']); ?></td>
                     <td><?php echo h_out($r['Returns']['dealer_name']); ?></td>
                     <td></td>
                     <td><?php echo h_out($r['Returns']['validated_date'],'center'); ?></td>
                     <td><?php echo h_out($r['Returns']['facility_name']); ?></td>
                     <td colspan=2><?php echo $this->form->input('RCV.recital', array('value'=>$r['Returns']['recital'], 'type'=>'text', 'readonly'=>'readonly', 'class'=>'lbl','style'=>'width:230px;')) ?></td>
                 </tr>
                 <?php $cnt++; } ?>
             </table>
         </div>
     </div>
<?php echo $this->form->end(); ?>
