<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
          <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/favorite_group_list">お気に入りグループ一覧</a></li>
          <li>お気に入りグループ編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">お気に入りグループ編集</h2>

<form class="validate_form search_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/result"  id="aAddForm">
    <input type="hidden" name="mode" value="add" />
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>お気に入りグループコード</th>
                <td>
                    <?php echo $this->form->input('MstFavoriteGroup.group_code' , array('type'=>'text' , 'class'=>'txt validate[required] search_canna' , 'maxlength'=>'20')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>お気に入りグループ名称</th>
                <td>
                    <?php echo $this->form->input('MstFavoriteGroup.group_name' , array('type'=>'text' , 'class'=>'txt validate[required] search_canna' , 'maxlength'=>'20')); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="ButtonBox">
        <input type="submit" class="btn btn2 [p2]" value="" />
    </div>
</form>
