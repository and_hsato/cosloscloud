<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
          <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/favorite_group_list">お気に入りグループ一覧</a></li>
          <li class="pankuzu">お気に入りグループ編集</li>
          <li>お気に入りグループ結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">お気に入りグループ編集結果</h2>
<form method="post" accept-charset="utf-8" class="validate_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>お気に入りグループコード</th>
                <td>
                    <?php echo $this->form->input('MstFavoriteGroup.group_code' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>お気に入りグループ名称</th>
                <td>
                    <?php echo $this->form->input('MstFavoriteGroup.group_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'));?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</form>

