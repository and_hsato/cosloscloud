<script>
$(document).ready(function() {
  //新規ボタン押下
    $("#btn_Add").click(function(){
        validateDetachSubmit($('#FavoriteGroupList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add' );
    });
    //編集ボタン押下
    $("#btn_Mod").click(function(){
        $("#FavoriteGroupList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
    });
    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $('#FavoriteGroupList').validationEngine('detach');  
        $("#FavoriteGroupList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        $("#FavoriteGroupList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/favorite_group_list');
    });
  
    //検索ボタン押下
    $("#btn_Search").click(function(){
        validateDetachSubmit($('#FavoriteGroupList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/favorite_group_list' );
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>お気に入りグループ一覧</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">お気に入りグループ一覧</h2>

<form class="validate_form search_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/favorite_group_list" accept-charset="utf-8" id="FavoriteGroupList">
    <?php echo $this->form->input('MstFavoriteGroup.is_search' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstFavoriteGroup.mst_facility_id' , array('type'=>'hidden' , 'value'=> $this->Session->read('Auth.facility_id_selected'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>お気に入りグループコード</th>
                <td>
                    <?php echo $this->form->input('MstFavoriteGroup.search_code' , array('type'=>'txt' , 'class'=>'txt search_canna'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>お気に入りグループ名称</th>
                <td>
                    <?php echo $this->form->input('MstFavoriteGroup.search_name' , array('type'=>'txt' , 'class'=>'txt search_canna'));?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>
        <input type="button" class="btn btn11 [p2]" id="btn_Add"/>
        <input type="button" class="btn btn5" id="btn_Csv"/>
    </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <?php echo $this->element('limit_combobox',array('result'=>count($FavoriteGroup_List))); ?>
        </div>
        
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col />
                </colgroup>
                <thead>
                    <tr>
                        <th></th>
                        <th>お気に入りグループコード</th>
                        <th>お気に入りグループ名称</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0 ;foreach($FavoriteGroup_List as $item) { ?>
                    <tr>
                        <td class="center"><input name="data[MstFavoriteGroup][id]" id="radio<?php echo $i; ?>" class="validate[required]" type="radio" value="<?php echo $item['MstFavoriteGroup']['id'] ?>" /></td>
                        <td><?php echo h_out($item['MstFavoriteGroup']['group_code']); ?></td>
                        <td><?php echo h_out($item['MstFavoriteGroup']['group_name']); ?></td>
                    </tr>
                    <?php $i++; } ?>
                    <?php if(count($FavoriteGroup_List)==0 && isset($this->request->data['MstFavoriteGroup']['is_search'])){ ?>
                    <tr><td colspan="3" class="center">該当するデータがありませんでした。</td></tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php if(count($FavoriteGroup_List)>0){ ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn9" id="btn_Mod"/>
    </div>
    <?php } ?>
</form>

