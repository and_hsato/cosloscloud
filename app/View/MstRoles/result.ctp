<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/roles_list">ロール一覧</a></li>
        <li>ロール編集結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>ロール一覧</span></h2>
<?php if(isset($this->request->data['MstRole']['is_deleted'])){ ?>
<div class="Mes01">データを削除しました</div>
<?php }else{ ?>
<div class="Mes01">以下の内容で設定しました</div>
<?php }  ?>
<form method="post" action="" accept-charset="utf-8">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>ロール名</th>
                <td><?php echo $this->form->input('MstRole.role_name' , array('class'=>'lbl' , 'readonly' => 'readonly'))?></td>
                <td></td>
            </tr>
            <tr>
                <th>ロール略称</th>
                <td><?php echo $this->form->input('MstRole.role_name_s' , array('class'=>'lbl' , 'readonly' => 'readonly'))?></td>
                <td></td>
            </tr>
            <tr>
                <th>ロールタイプ</th>
                <td>
                    <?php echo $this->Form->input('MstRole.mst_role_type_id', array('disabled'=>'disabled')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>エディション</th>
                <td>
                    <?php echo $this->Form->input('MstRole.mst_edition_id', array('disabled'=>'disabled')); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <h2 class="HeaddingSmall">権限</h2>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col />
                <col />
                <col width="120" />
            </colgroup>
            <tr>
                <th colspan="2">機能名</th>
                <th>権限</th>
            </tr>
            <?php foreach($result as $data){ ?>
            <tr>
                <td><?php echo h_out($data['MstRole']['Headername']); ?></td>
                <td><?php echo h_out($data['MstRole']['name']); ?></td>
                <td><?php echo $this->form->input('MstRole.Authority.'.$data['MstRole']['view_id'] , array('options'=>array( 0 => '不可' , 1=> '参照' , 2=> '更新', 3=>'仮締後更新') , 'value'=>$data['MstRole']['Authority'] ,'disabled'=>'disabled'))?> </td>
            </tr>
            <?php } ?>
        </table>
    </div>
</form>
