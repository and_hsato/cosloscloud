<script>
$(function(){
    $("#btn_Add").click(function(){
        $("#RolesList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add').submit();
    });
    $("#btn_Mod").click(function(){
       if($('input.chk:checked').length > 0){
            $("#RolesList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
        }else{
           alert('編集するロールを選択してください。');
        }
    });
    $("#btn_Search").click(function(){
        $("#RolesList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/roles_list').submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>ロール一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>ロール一覧</span></h2>
<h2 class="HeaddingMid">検索条件</h2>
<form class="validate_form search_form" method="post" action="" accept-charset="utf-8" id="RolesList" >
    <?php echo $this->form->input('search.is_search' , array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>ロール名</th>
                <td><?php echo $this->form->input('MstRole.search_role_name' , array('class'=>'txt search_canna')) ?></td>
                <td><?php echo $this->form->input('MstRole.search_is_deleted' , array('type'=>'checkbox' , 'hiddenField'=>false )) ?></td>
                <td>削除済みも表示する</td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>
        <input type="button" id="btn_Add" class="btn btn11 [p2]"/>
    </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            </span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <colgroup>
                    <col width="25px" />
                    <col />
                    <col class="col10"/>
                </colgroup>
                <tr>
                    <th></th>
                    <th>ロール名</th>
                    <th>削除</th>
                </tr>
                <?php foreach ($result as $data) { ?>
                <tr>
                    <td class="center" width="25px"><?php echo $this->form->radio('MstRole.id' , array($data['MstRole']['id'] => '') , array('hiddenField'=>false , 'label'=>false , 'class'=>'chk')); ?></td>
                    <td><?php echo h_out($data['MstRole']['role_name']); ?></td>
                    <td><?php echo h_out(($data['MstRole']['is_deleted'])?'○':'','center'); ?></td>
                </tr>
                <?php } ?>
                <?php if(isset($this->request->data['search']['is_search']) && empty($result)){ ?>
                <tr>
                    <td colspan="3" class="center">対象データがありません。</td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
    <?php if(isset($this->request->data['search']['is_search']) && count($result)>0){ ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn9" id="btn_Mod"/>
    </div>
    <?php } ?>
</form>
