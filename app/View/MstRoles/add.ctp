<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/roles_list">ロール一覧</a></li>
        <li>ロール編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>ロール一覧</span></h2>
<form method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/result" accept-charset="utf-8" id="AddForm" class="validate_form search_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>ロール名</th>
                <td>
                    <?php echo $this->form->input('MstRole.role_name' , array('class'=>'r txt validate[required] search_canna' , 'maxlength'=>100));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>ロール略称</th>
                <td>
                    <?php echo $this->form->input('MstRole.role_name_s' , array('class'=>'r txt validate[required] search_canna' , 'maxlength'=>10));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>ロールタイプ</th>
                <td>
                    <?php echo $this->Form->input('MstRole.mst_role_type_id', array('class' => 'r validate[required] txt')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>エディション</th>
                <td>
                    <?php echo $this->Form->input('MstRole.mst_edition_id', array('class' => 'r validate[required] txt')); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col />
                <col />
                <col width="120" />
            </colgroup>
            <tr>
                <th colspan="2">機能名</th>
                <th>権限</th>
            </tr>
            <?php foreach($result as $data){ ?>
            <tr>
                <td><?php echo h_out($data['MstRole']['Headername']);?></td>
                <td><?php echo h_out($data['MstRole']['name']);?></td>
                <td><?php echo $this->form->input('MstRole.Authority.'.$data['MstRole']['view_id'] , array('options'=>array( 0 => '不可' , 1=> '参照' , 2=> '更新', 3=>'仮締後更新') ))?></td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="submit" class="btn btn2 [p2]" value=''/>
    </div>
</form>
