<script>
    $(function(){
        //新規ボタン押下
        $("#btn_Add").click(function(){
           //バリデート無効化 && ぐるぐる表示 && submit!!
            validateDetachSubmit($('#FacilitiesList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add' );
        });
        //検索ボタン押下
        $("#btn_Search").click(function(){
           //バリデート無効化 && ぐるぐる表示 && submit!!
            validateDetachSubmit($('#FacilitiesList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/facilities_list#search_result' );
        });
        //CSVボタン押下
        $("#btn_Csv").click(function(){
           //バリデート無効化 && ぐるぐる表示 && submit!!
            $('#FacilitiesList').validationEngine('detach');
            $("#FacilitiesList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        });
        //編集ボタン押下
        $("#btn_Mod").click(function(){
            $("#FacilitiesList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
        });
    });
</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li class="pankuzu">運用設定</li>
          <li>仕入先登録・編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>仕入登録・編集</span></h2>
<h2 class="HeaddingMid">登録済み仕入先の検索、仕入先の新規登録・編集が行えます。</h2>

<form class="validate_form search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/facilities_list/" method="post" id="FacilitiesList">
    <?php echo $this->form->input('MstFacility.is_search' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstFacility.mst_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected'))); ?>
    <?php echo $this->form->input('MstFacility.type' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstFacility.title' , array('type'=>'hidden')); ?>
    
    <h3 class="conth3">仕入先を検索</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>仕入先コード</th>
                <td><?php echo $this->form->input('MstFacility.search_facility_code' , array('class'=>'txt'));?></td>
                <th>&nbsp;</th>
                <td><?php echo $this->form->input('MstFacility.search_is_deleted',array('type'=>'checkbox' , 'hiddenField'=>false)); ?>削除も表示する</td>
            </tr>
            <tr>
                <th>仕入先名称</th>
                <td><?php echo $this->form->input('MstFacility.search_facility_name' , array('class'=>'txt search_canna' , 'id'=>'SearchFacilityName'))?></td>
         <!--       <th>&nbsp;</th>
                <td>
                <?php if($this->request->data['MstFacility']['type']=="mode1"){ ?>
                    <?php //echo $this->form->input('MstFacility.search_relation',array('type'=>'checkbox' , 'hiddenField'=>false)); ?>関連する施設のみ表示する
                <?php } ?>
                </td>
            </tr> -->
            <!--
            <?php if($this->request->data['MstFacility']['type']=="mode1"){ ?>
            <tr>
               <th>種別</th>
                <td>
                    <?php echo $this->form->input('MstFacility.search_facility_type' , array('options'=>$facility_type , 'class'=>'txt' , 'id'=>'SearchFacilityType' , 'empty'=>''))?>
                </td>
                <th></th>
                <td></td> 
            </tr>
            <?php } ?>
            -->
        </table>
    </div>

    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Search" value="検索"/>
        <input type="button" class="common-button [p2]" id="btn_Add" value="新規"/>
        <!--<input type="button" class="common-button" id="btn_Csv" value="CSV"/>-->
    </div>

    <div class="results">
      <a name="search_result"></a>
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($Facilities_List))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col class="col5"/>
                    <col class="col10"/>
                    <col />
                 <!--   <col class="col10"/> -->
                    <col class="col5"/>
                </colgroup>
                <thead>
                <tr>
                    <th></th>
                    <th>施設コード</th>
                    <th>施設名</th>
                    <!-- <th>種別</th> -->
                    <th>削除済</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($Facilities_List as $facility) { ?>
                <tr>
                    <td class="center"><input name="data[MstFacility][id]" type="radio" id="radio<?php echo $facility['MstFacility']['id']; ?>" class="validate[required]" value="<?php echo $facility['MstFacility']['id']; ?>" /></td>
                    <td><?php echo h_out($facility['MstFacility']['facility_code'],'center') ?></td>
                    <td><?php echo h_out($facility['MstFacility']['facility_name']); ?></td>
              <!--      <td><?php echo h_out($facility['MstFacility']['facility_type'],'center'); ?></td> -->
                    <td><?php echo h_out((($facility['MstFacility']['is_deleted'] == TRUE) ? "○" : ""),'center')  ?></td>
                </tr>
                <?php } ?>
                <?php if(isset($this->request->data['MstFacility']['is_search']) && count($Facilities_List) == 0){ ?>
                <tr><td colspan="4" class="center">該当するデータがありませんでした</td></tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php if(count($Facilities_List)>0){ ?>
    <div class="ButtonBox">
        <input type="button" class="common-button" value="編集" id="btn_Mod"/>
</div>
    <?php } ?>
</form>
</div><!--#content-wrapper-->