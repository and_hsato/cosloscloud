<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //チェックOKだったら、送信する
        validateDetachSubmit($('#ModForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result/' );
    }
}

$(document).ready(function(){
    $("#ModForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
    //チェックボックス一括変更
    $("#allCheck").click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $("#optionArea").hide();
    $("#optionTitle").click(function(){
        $('#optionArea').slideToggle('slow');
    });
  
    $("#spareArea").hide();
    $("#spareTitle").click(function(){
        $('#spareArea').slideToggle('slow');
    });

});
</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li class="pankuzu">運用設定</li>
          <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/facilities_list/">仕入先一覧</a></li>
          <li>仕入先編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>仕入先編集</span></h2>
<h2 class="HeaddingMid">登録済みの仕入先情報を編集できます。</h2>
<form class="input_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckFacilityCode" id="ModForm" method="post" onsubmit="return false;">
    <input type="hidden" name="mode" value="mod" />
    <?php echo $this->form->input('MstFacility.type' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstFacility.title' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstFacility.id' , array('type'=>'hidden' , 'id'=>'facility_id'))?>
    <?php echo $this->form->input('MstFacility.facility_type' , array('type'=>'hidden' , 'id'=>'facility_type'))?>

<h3 class="conth3">仕入先情報の編集</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>仕入先名</th>
                <td><?php echo $this->form->input('MstFacility.facility_name' , array('type'=>'text' , 'style'=>'width:280px;' , 'class'=>'validate[required] search_canna txt r' , 'maxlength'=>100))?></td>
                <td></td>
                <th>削除フラグ</th>
                <td>
                    <?php echo $this->form->input('MstFacility.is_deleted' , array('type'=>'checkbox' , 'hiddenField'=>false)) ?>
                    削除する
                </td>
                <td></td>
            </tr>
            <tr>
                <th>仕入先正式名称</th>
                <td><?php echo $this->form->input('MstFacility.facility_formal_name' , array('type'=>'text' , 'style'=>'width:280px;' , 'class'=>'validate[required] search_canna txt r' , 'maxlength'=>'100') )?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>郵便番号</th>
                <td><?php echo $this->form->input('MstFacility.zip' , array('type'=>'text' , 'class'=>'txt' , 'maxlength'=>8))?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>住所</th>
                <td><?php echo $this->form->input('MstFacility.address' , array('type'=>'text' , 'class'=>'txt' , 'style'=>'width:280px;' , 'maxlength'=>'100'))?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>TEL</th>
                <td><?php echo $this->form->input('MstFacility.tel' , array('class'=>'txt' , 'maxlength'=>100)) ?></td>
                <td></td>
                <th>FAX</th>
                <td><?php echo $this->form->input('MstFacility.fax' , array('class'=>'txt' , 'maxlength'=>100)) ?></td>
                <td></td>
            </tr>

            <tr>
                   <th>メールアドレス</th>
                <td><?php echo $this->form->input('MstFacility.email' , array('type'=>'text','class'=>'txt' , 'maxlength'=>100))?></td>
                <td></td>         
            </tr>
            <tr>
                <th>仕入先コード</th>
                <td><?php echo $this->form->input('MstFacility.facility_code' , array('type'=>'text' ,'readonly'=>'readonly', 'class'=>'r validate[required,ajax[ajaxFacilityCode]]' , 'maxlength'=>5))?></td>
                <td></td>

                <th>消費税</th>
                <td>
                <?php echo $this->form->input('MstFacility.round', array('options'=> Configure::read('Round'),'class' => 'r txt','empty'=>false)); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_back" value="戻る"/>
        <input type="submit" class="common-button [p2]" value="登録" id="btn_Conf"/>
    </div>
</form>
</div><!--#content-wrapper-->