<script>
$(document).ready(function(){
    $('select').attr('readonly','readonly').attr('disabled' , 'disabled');
    $('.txt').attr('readonly','readonly').attr('disabled' , 'disabled');
  
    $("#btn_list").click(function(){
        $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/facilities_list#search_result').submit();
    });

    $('td').addClass('border-none');
  
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li class="pankuzu">運用設定</li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/facilities_list/">仕入先一覧</a></li>
        <li class="pankuzu">仕入先編集</li>
        <li>仕入先編集完了</li>
    </ul>
</div>
    <h2 class="HeaddingLarge"><span>仕入先編集完了</span></h2>

<?php if(isset($this->request->data['MstFacility']['is_deleted'])) { ?>
<h2 class="HeaddingMid">データを削除しました</h2>
<?php }else{ ?>
<h2 class="HeaddingMid">以下の内容で設定しました</h2>
<?php } ?>
<form method="post" id="result_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>仕入先名</th>
                <td>
                    <?php echo $this->form->input('MstFacility.facility_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly' ))?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>仕入先正式名称</th>
                <td>
                    <?php echo $this->form->input('MstFacility.facility_formal_name' ,array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>郵便番号</th>
                <td>
                    <?php echo $this->form->input('MstFacility.zip' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>住所</th>
                <td>
                    <?php echo $this->form->input('MstFacility.address' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>TEL</th>
                <td>
                    <?php echo $this->form->input('MstFacility.tel' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>FAX</th>
                <td>
                    <?php echo $this->form->input('MstFacility.fax' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>メールアドレス</th>
                <td><?php echo $this->form->input('MstFacility.email',array('type'=>'text','class'=>'lbl','readonly'=>'readonly')); ?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>仕入先コード</th>
                <td>
                    <?php echo $this->form->input('MstFacility.facility_code' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly' ))?>
                </td>
                <td></td>
                <th>消費税</th>
                <td>
                    <?php echo $this->form->input('MstFacility.round', array('options'=> Configure::read('Round'),'class' => 'lbl','empty'=>false , 'readonly'=>'readonly' , 'disabled'=>'disabled')); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <?php echo $this->form->input('MstFacility.is_search', array('type'=>'hidden','value'=>1)); ?>
            <input type="button" class="common-button" id="btn_list" value="一覧表示"/>
        </p>
    </div>
    
</form>
</div><!--#content-wrapper-->
