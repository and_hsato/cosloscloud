<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //チェックOKだったら、送信する
        validateDetachSubmit($('#AddForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result/' );
    }
}

$(document).ready(function(){
    $("#AddForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
    //チェックボックス一括変更
    $("#allCheck").click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $("#optionArea").hide();
    $("#optionTitle").click(function(){
        $('#optionArea').slideToggle('slow');
    });
  
    $("#spareArea").hide();
    $("#spareTitle").click(function(){
        $('#spareArea').slideToggle('slow');
    });
});
</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li class="pankuzu">運用設定</li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/facilities_list/<?php echo $this->request->data['MstFacility']['type'] ?>">仕入先一覧</a></li>
        <li>仕入先新規登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>仕入先新規登録</span></h2>
<h2 class="HeaddingMid">商品の仕入先情報を新規登録できます。</h2>
<?php if(isset($Facility_Info)){$info = $Facility_Info[0];} ?>
<form class="input_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckFacilityCode" id="AddForm" onsubmit="return false;">
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <input type="hidden" name="mode" value="add" />
    <?php echo $this->form->input('MstFacility.type' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstFacility.title' , array('type'=>'hidden')); ?>

<h3 class="conth3">仕入先情報の入力</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>仕入先名</th>
                <td><?php echo $this->form->input('MstFacility.facility_name' , array('type'=>'text' , 'style'=>'width:280px;' , 'class'=>'validate[required] search_canna txt r' , 'maxlength'=>100))?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>仕入先正式名称</th>
                <td><?php echo $this->form->input('MstFacility.facility_formal_name' , array('type'=>'text' , 'style'=>'width:280px;' , 'class'=>'validate[required] search_canna txt r' , 'maxlength'=>'100') )?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>郵便番号</th>
                <td><?php echo $this->form->input('MstFacility.zip' , array('type'=>'text' , 'class'=>'txt' , 'maxlength'=>8))?></td>
                <td></td>
            </tr>
            <tr>
                <th>住所</th>
                <td><?php echo $this->form->input('MstFacility.address' , array('type'=>'text' , 'class'=>'txt' , 'style'=>'width:280px;' , 'maxlength'=>'100'))?></td>
                <td></td>
                <th></th>
                <td><?php echo $this->form->input('MstFacility.facility_type' , array('type'=>'hidden' , 'id'=>'facility_type' , 'value'=> 3))?></td>
                <td></td>
                <td></td>
            </tr>
            <tr>

                <th>TEL</th>
                <td><?php echo $this->form->input('MstFacility.tel' , array('class'=>'txt' , 'maxlength'=>100)) ?></td>
                <td></td>
                <th>FAX</th>
                <td><?php echo $this->form->input('MstFacility.fax' , array('class'=>'txt' , 'maxlength'=>100)) ?></td>
                <td></td>
            </tr>
            <tr>
                <th>メールアドレス</th>
                <td><?php echo $this->form->input('MstFacility.email' , array('type'=>'text' , 'class'=>'txt', 'maxlength'=>100)); ?></td>
                <td></td>

            </tr>
            <tr>
                <th>仕入先コード</th>
                <td><?php echo $this->form->input('MstFacility.facility_code' , array('type'=>'text' ,'readonly'=>'readonly', 'class'=>'r validate[required,ajax[ajaxFacilityCode],custom[onlyNumber]]' , 'maxlength'=>5))?></td>
                <td></td>
                <th>消費税</th>
                <td>
                    <?php echo $this->form->input('MstFacility.round', array('options'=> Configure::read('Round'),'class' => 'r txt','empty'=>false)); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_back" value="戻る"/>
        <input type="submit" class="common-button [p2]"  value="登録" id="btn_Conf"/>
    </div>
</form>
</div><!--#content-wrapper-->