<script>
$(document).ready(function() {
  //新規ボタン押下
    $("#btn_Add").click(function(){
        validateDetachSubmit($('#ClassList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add' );
    });
    //編集ボタン押下
    $("#btn_Mod").click(function(){
        $("#ClassList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
    });
    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $('#ClassList').validationEngine('detach');  
        $("#ClassList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        $("#ClassList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/classes_list');
    });
  
    //検索ボタン押下
    $("#btn_Search").click(function(){
        validateDetachSubmit($('#ClassList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/classes_list' );
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>作業区分一覧</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">作業区分一覧</h2>

<form class="validate_form search_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/classes_list" accept-charset="utf-8" id="ClassList">
    <?php echo $this->form->input('MstClass.is_search' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstClass.mst_facility_id' , array('type'=>'hidden' , 'value'=> $this->Session->read('Auth.facility_id_selected'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('MstClass.search_mst_facility_id' , array('options'=>$Facility_List , 'class'=>'txt' , 'empty'=>true)  );?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>表示箇所</th>
                <td>
                    <?php echo $this->form->input('MstClass.search_mst_menu_id' , array('options'=>$Menu_List , 'class'=>'txt' , 'empty'=>true)  );?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>コード</th>
                <td>
                    <?php echo $this->form->input('MstClass.search_code' , array('type'=>'txt' , 'class'=>'txt search_canna'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>名称</th>
                <td>
                    <?php echo $this->form->input('MstClass.search_name' , array('type'=>'txt' , 'class'=>'txt search_canna'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('MstClass.search_comment' , array('type'=>'txt' , 'class'=>'txt'));?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>
        <input type="button" class="btn btn11 [p2]" id="btn_Add"/>
        <input type="button" class="btn btn5" id="btn_Csv"/>
    </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <?php echo $this->element('limit_combobox',array('result'=>count($Claasses_List))); ?>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col width="100"/>
                    <col />
                    <col />
                    <col />
                </colgroup>
                <thead>
                    <tr>
                        <th></th>
                        <th>施設</th>
                        <th>表示箇所</th>
                        <th>コード</th>
                        <th>名称</th>
                        <th>備考</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0 ;foreach($Claasses_List as $classes) { ?>
                    <tr>
                        <td class="center"><input name="data[MstClass][id]" id="radio<?php echo $i; ?>" class="validate[required]" type="radio" value="<?php echo $classes['MstClass']['id'] ?>" /></td>
                        <td><?php echo h_out($classes['MstClass']['facility_name']); ?></td>
                        <td><?php echo h_out($classes['MstClass']['category1']); ?></td>
                        <td><?php echo h_out($classes['MstClass']['code']); ?></td>
                        <td><?php echo h_out($classes['MstClass']['name']); ?></td>
                        <td><?php echo h_out($classes['MstClass']['comment']); ?></td>
                    </tr>
                    <?php $i++; } ?>
                    <?php if(count($Claasses_List)==0 && isset($this->request->data['MstClass']['is_search'])){ ?>
                    <tr><td colspan="6" class="center">該当するデータがありませんでした。</td></tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php if(count($Claasses_List)>0){ ?>
    <div class="ButtonBox">
        <input type="button"  class="btn btn9" id="btn_Mod"/>
    </div>
    <?php } ?>
</form>
