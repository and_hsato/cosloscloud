<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
          <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/classes_list">作業区分一覧</a></li>
          <li class="pankuzu">作業区分編集</li>
          <li>作業区分編集結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">作業区分編集結果</h2>
<?php if($this->Session->check('Del')){ ?>
<div class="Mes01">データを削除しました</div>
<?php }else{ ?>
<div class="Mes01">以下の内容で設定しました</div>
<?php } ?>
<form method="post" accept-charset="utf-8" class="validate_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>施設名</th>
                <td>
                    <?php echo $this->form->input('MstClass.facility_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>表示箇所</th>
                <td>
                    <?php echo $this->form->input('MstClass.catgory1' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>コード</th>
                <td>
                    <?php echo $this->form->input('MstClass.code' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>名称</th>
                <td>
                    <?php echo $this->form->input('MstClass.name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('MstClass.comment' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'));?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</form>
