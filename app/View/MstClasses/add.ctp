<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
          <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/classes_list">作業区分一覧</a></li>
          <li>作業区分編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">作業区分編集</h2>

<form class="validate_form search_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/result"  id="aAddForm">
    <input type="hidden" name="mode" value="add" />
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('MstClass.mst_facility_id' , array('options'=>$Facility_List , 'class'=>'txt validate[required]' )  );?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>表示箇所</th>
                <td>
                    <?php echo $this->form->input('MstClass.mst_menu_id' , array('options'=>$Menu_List , 'class'=>'txt validate[required]')  );?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>コード</th>
                <td>
                    <?php echo $this->form->input('MstClass.code' , array('type'=>'text' , 'class'=>'txt validate[required] search_canna' , 'maxlength'=>'20')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>名称</th>
                <td>
                    <?php echo $this->form->input('MstClass.name' , array('type'=>'text' , 'class'=>'txt validate[required] search_canna' , 'maxlength'=>'20')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('MstClass.comment' , array('type'=>'text' , 'class'=>'txt' , 'maxlength'=>'200')); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="submit" class="btn btn2 [p2]" value="" />
    </div>
</form>
