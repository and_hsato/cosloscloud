<script type="text/javascript">
    $(function(){
        //印刷ボタン押下
        $("#btn_Print").click(function(){
            tmp = confirm('仕入設定印刷を行ってよろしいでしょうか？');
            $('#printLimit').val($('#limit').val());
            if(tmp == true){
                $("#TransactionConfigList1").validationEngine('detach');
                $("#TransactionConfigList1").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report').submit();
                $("#TransactionConfigList1").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
            }
        });

        //選択ボタン押下
        $("#btn_Mod").click(function(){
            $("#TransactionConfigList1").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
        });

        //検索ボタン押下
        $("#btn_Search").click(function(){
            validateDetachSubmit($("#TransactionConfigList1") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search#search_result');
        });
   });

</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a>Myマスタ</a></li>
        <li>仕入情報設定</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>仕入情報設定</span></h2>
<h2 class="HeaddingMid">Myマスタ登録商品に仕入先と仕入金額の設定が行えます。</h2>
<form class="validate_form search_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search" accept-charset="utf-8" id="TransactionConfigList1">
    <?php echo $this->form->input('MstTransactionConfig.is_search' , array('type'=>'hidden'))?>
    <input type="hidden" id="printLimit" name="data[printLimit]" value="" />
    <input type="hidden" name="data[selected_facility_id]" value="<?php echo $this->Session->read('Auth.facility_id_selected'); ?>" />
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    
    <h3 class="conth3">Myマスタ登録商品を検索</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>仕入先</th>
                <td colspan="2">
                    <?php echo $this->form->input('MstFacilityItem.supplierName',array('id' => 'supplierName', 'type' => 'hidden')); ?>
                    <?php echo $this->form->input('MstFacilityItem.supplierText',array('id' => 'supplierText', 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('MstFacilityItem.supplierCode', array('options'=>$suppliers_enabled , 'id' => 'supplierCode', 'class' => 'txt' , 'empty'=>true)); ?>
                </td>
                <th>&nbsp;</th>
                <td> <?php echo $this->form->input('MstTransactionConfig.is_setting' , array('type'=>'checkbox' , 'hiddenField'=>false , 'label'=>'raberl'))?>
                    仕入未設定のみ表示する
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_internal_code' , array('type'=>'text' , 'class'=>'txt search_internal_code'))?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_item_code' , array('type'=>'text' , 'class'=>'txt search_upper'))?>
                </td>
                <td></td>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_standard' , array('type'=>'text' , 'class'=>'txt search_canna'))?>
                </td>
                <td></td>

            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_item_name' , array('type'=>'text' , 'class'=>'txt search_canna'))?>
                </td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstFacilityItem.search_dealer_name' , array('type'=>'text' , 'class'=>'txt search_canna') ) ?></td>
                <td></td>
                <th>JANコード</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_jan_code' , array('type'=>'text' , 'class'=>'txt' ));?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Search" value="検索"/>
       <!-- <input type="button" class="btn btn62" id="btn_Print"/> -->
    </div>
    
    <div class="results">
      <a name="search_result"></a>
        <h2 class="HeaddingSmall">検索結果</h2>
        
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($TransactionConfig_Info))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
    
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25" />
                <col width="90"/>
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th></th>
                <th>商品ID</th>
                <th>商品名</th>
                <th>規格</th>
                <th>製品番号</th>
                <th>販売元</th>
                <th>代表仕入先</th>
           </tr>
           <?php foreach ($TransactionConfig_Info as $tran) { ?>
           <tr>
               <td class="center"><input class="validate[required] rd" id="MstFacilityItemId" name="data[MstFacilityItem][id]" type="radio" value="<?php echo $tran['MstFacilityItem']['id'] ?>"/></td>
               <td><?php echo h_out($tran['MstFacilityItem']['internal_code'],'center'); ?></td>
               <td><?php echo h_out($tran['MstFacilityItem']['item_name']); ?></td>
               <td><?php echo h_out($tran['MstFacilityItem']['standard']); ?></td>
               <td><?php echo h_out($tran['MstFacilityItem']['item_code']); ?></td>
               <td><?php echo h_out($tran['MstFacilityItem']['dealer_name']); ?></td>
               <td><?php echo h_out($tran['MstFacilityItem']['facility_name']); ?></td>
           </tr>
           <?php } ?>
           <?php if(count($TransactionConfig_Info) == 0 && isset($this->request->data['MstTransactionConfig']['is_search'])){?>
           <tr><td colspan="7" class="center">該当するデータがありませんでした。</td></tr>
           <?php } ?>
       </table>
    </div>
</div>

    <?php if(count($TransactionConfig_Info) > 0 ){?>
    <div class="ButtonBox">
        <input type="button" value="選択" class="common-button" id="btn_Mod"/>
    </div>
    <?php } ?>
</form>
</div><!--#content-wrapper-->