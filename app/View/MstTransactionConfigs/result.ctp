<script>
$(document).ready(function(){
    $("#btn_list").click(function(){
        $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search#search_result').submit();
    });

    $('td').addClass('border-none');
  
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li style=""><a href="<?php echo $this->webroot; ?>masters">Myマスタ</a></li>
        <li style=""><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">仕入情報設定</a></li>
        <li class="pankuzu">仕入情報設定編集</li>
        <li>仕入情報設定完了</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>仕入情報設定完了</span></h2>
<h2 class="HeaddingMid">仕入情報の設定が完了しました。</h2>

<h3 class="conth3">仕入情報</h3>
<form method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/result" accept-charset="utf-8" id="result_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.internal_code' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('MstFacilityItem.id' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_code' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;'  , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_name' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                   <?php echo $this->form->input('MstFacilityItem.dealer_name' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.standard' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>JANコード</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.jan_code' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>償還価格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.refund_price' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>保険請求区分</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.insurance_claim_department_name' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th></th>
                <td></td>
                <td></td>
                <th>保険請求名称</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.insurance_claim_name_s' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
        </table>
        <div class="results">
            <div class="TableScroll">
                <table class="TableStyle01 table-odd" id="table1">
                    <tr>
                        <th class="col15">適用開始日</th>
                        <th class="col15">仕入先</th>
                        <!--<th class="col15">仕入先2</th>-->
                        <th>仕入単位</th>
                        <th>仕入単価</th>
<?php if($this->Session->read('Auth.Config.MSCorporate') == '1' ) { ?>
                        <th>MS仕入単価</th>
<?php } ?>
                        <th class="col5">適用</th>
                    </tr>
                    <?php foreach($result as $r){  ?>
                    <tr>
                        <td><?php echo h_out($r['MstTransactionConfig']['start_date'],'center'); ?></td>
                        <td><?php echo h_out($Suppliers_List[$r['MstTransactionConfig']['partner_facility_id']],'center'); ?></td>
                        <!--<td><?php echo h_out($Suppliers_List[$r['MstTransactionConfig']['partner_facility_id2']],'center'); ?></td>-->
                        <td><?php echo h_out($UnitName_List[$r['MstTransactionConfig']['mst_item_unit_id']],'center'); ?></td>
                        <td><?php echo h_out($this->Common->toCommaStr($r['MstTransactionConfig']['transaction_price']),'right'); ?></td>
<?php if($this->Session->read('Auth.Config.MSCorporate') == '1' ) { ?>
                        <td><?php echo h_out($this->Common->toCommaStr($r['MstTransactionConfig']['ms_transaction_price']), 'right'); ?></td>
<?php } ?>
                        <td><?php echo h_out((($r['MstTransactionConfig']['is_deleted'])?'':'○'),'center'); ?></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        <!--
        <h2 class="HeaddingSmall">代表仕入</h2>
        <div class="TableScroll">
            <table class="TableStyle01" id="table3">
                <colgroup>
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>代表仕入先</th>
                    <th>代表包装単位</th>
                </tr>
                <tr>
                    <td><?php echo h_out($Suppliers_List[$main_result['MstTransactionMain']['mst_facility_id']],'center'); ?></td>
                    <td><?php echo h_out($UnitName_List[$main_result['MstTransactionMain']['mst_item_unit_id']],'center'); ?></td>
                </tr>
            </table>
        </div>
        -->
        </div>
    </div>

    <div class="ButtonBox">
        <p class="center">
            <?php echo $this->form->input('MstTransactionConfig.is_search', array('type'=>'hidden','value'=>1)); ?>
            <input type="button" class="common-button" id="btn_list" value="一覧表示"/>
        </p>
    </div>
</form>
</div><!--#content-wrapper-->
