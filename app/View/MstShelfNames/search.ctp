<script>
$(function(){
    //検索ボタン押下
    $('#search').click(function(){
        validateDetachSubmit($("#searchForm") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
    });

    //印刷ボタン押下
    $('#print').click(function(){
        validateDetachSubmit($("#searchForm") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report');
    });
    //CSV取込ボタン押下
    $('#csv').click(function(){
        validateDetachSubmit($("#searchForm") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/csvimport');
    });

    //CSVボタン押下
    $('#download').click(function(){
        validateDetachSubmit($("#searchForm") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv');
    });
    
    //新規ボタン押下
    $("#add").click(function(){
        validateDetachSubmit($("#searchForm") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add');
    });

    //更新ボタン押下
    $("#regist").click(function(){
        $('#searchForm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/regist').submit();
    });

});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>棚番号一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>棚番号一覧</span></h2>
<h2 class="HeaddingMid">検索条件</h2>
<form id ="searchForm" class="validate_form search_form input_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">
    <?php echo $this->form->input('MstUser.id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.MstUser.id')) );?>  
    <?php echo $this->form->input('MstFacility.id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')) );?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <?php echo $this->form->hidden('searchFlg');?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('MstShelfName.facilityName',array('id' => 'facilityName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('MstShelfName.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('MstShelfName.facilityCode',array('options'=>$facility_list ,'id' => 'facilityCode','class'=>'txt','empty'=>true) ); ?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('MstShelfName.departmentName',array('id' => 'departmentName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('MstShelfName.departmentText',array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('MstShelfName.departmentCode', array('options'=>$department_list, 'id' =>'departmentCode', 'class' =>'txt','empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <th>棚番号</th>
                <td>
                    <?php echo $this->form->input('MstShelfName.code', array('id'=>'code','class'=>'txt','style'=>'width: 200px;')); ?>
                </td>
            </tr>
            <tr>
                <th>棚名</th>
                <td>
                    <?php echo $this->form->input('MstShelfName.name', array('id'=>'name','class'=>'txt','style'=>'width: 200px;')); ?>
                </td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" id="search" class="btn btn1" />
                <input type="button" id="print" class="btn btn80" />
                <input type="button" id="csv" class="btn btn79" />
                <input type="button" id="download" class="btn btn78" />
            </p>
        </div>
        <hr class="Clear" />

        <div class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($result)));?>
        </div>

        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th style="width: 20px;"></th>
                    <th class="col20">施設名</th>
                    <th class="col30">部署名</th>
                    <th class="col20">棚番号</th>
                    <th class="col30">棚名</th>
                </tr>
                <?php $i = 0; foreach($result as $row){ ?>
                <tr>
                    <td style="width: 20px;" class="center">
                        <?php echo $this->form->radio('MstShelfName.id' , array($row['MstShelfName']['id']=>'') , array('label' => '','legend'=>'' ,'class'=>'validate[required]','hiddenField'=>false));?>
                    </td>
                    <td class="col20"><?php echo h_out($row['MstFacility']['facility_name']);?></td>
                    <td class="col30"><?php echo h_out($row['MstDepartment']['department_name']);?></td>
                    <td class="col20"><?php echo h_out($row['MstShelfName']['code']);?></td>
                    <td class="col30"><?php echo h_out($row['MstShelfName']['name']);?></td>
                </tr>
                <?php $i++; } ?>
                <?php if(isset($this->request->data['searchFlg']) && count($result) == 0){ ?>
                <tr>
                    <td colspan="5" class="center">対象データがありません</td>
                </tr>
                <?php } ?>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" id="add" class="btn btn11" />
            <?php if(count($result) > 0){ ?>
            <input type="button" id="regist" class="btn btn24" />
            <?php } ?>
        </div>
    </div>
</form>
