<script>
$(function(){
    //確定ボタン押下
    $("#result").click(function(){
        $("#registForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result').submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">棚番号一覧</a></li>
        <li>棚番号編集</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>棚番号編集</span></h2>
<h2 class="HeaddingMid">棚の情報を設定してください</h2>
<form id ="registForm" class="validate_form" method="post" action="" accept-charset="utf-8">
    <?php echo $this->form->hidden('MstShelfName.id'); ?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->hidden('MstShelfName.facilityId'); ?>
                    <?php echo $this->form->hidden('MstShelfName.facilityCode'); ?>
                    <?php echo $this->form->input('MstShelfName.facilityName',array('class'=>'lbl','style'=>'width:200px;','readonly'=>'readonly'));?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->hidden('MstShelfName.departmentId'); ?>
                    <?php echo $this->form->hidden('MstShelfName.departmentCode'); ?>
                    <?php echo $this->form->input('MstShelfName.departmentName',array('class'=>'lbl','style'=>'width:200px;','readonly'=>'readonly'));?>
                </td>
            </tr>
            <tr>
                <th>棚番号</th>
                <td><?php echo $this->form->input('MstShelfName.code',array('class'=>'r txt validate[required]','style'=>'width:200px;','maxlength'=>20));?></td>
            </tr>
        </table>

        <hr class="Clear" />

        <div class="ButtonBox">
            <input type="button" class="btn btn2" id="result" />
        </div>
    </div>
</form>
