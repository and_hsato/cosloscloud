<printdata>
    <setting>
        <layoutname>31_棚卸バーコード.rpx</layoutname>
        <fixvalues>
            <value name="printer"><?php echo Configure::read('PrinterType.normal'); ?></value>
        </fixvalues>
    </setting>
    <datatable>
        <columns><?php echo join("\t", $head); ?></columns>
        <rows>
            <?php foreach($result as $_row): ?>
            <row><?php echo h(join("\t", array_values($_row))); ?></row>
            <?php endforeach; ?>
        </rows>
    </datatable>
</printdata>