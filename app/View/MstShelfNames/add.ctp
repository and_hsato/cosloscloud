<script>
$(function(){
    //確定ボタン押下
    $("#result").click(function(){
        //必須チェック
        if($('#facility').val() === ""){
            alert("施設が選択されていません");
            return false;
        }
        if($('#department').val() === ""){
            alert("部署が選択されていません");
            return false;
        }else{
            $('#department_name').val($('#department option:selected').text());
        }
        if($('#code').val() === ""){
            alert("棚番号が入力されていません");
            return false;
        }
        $("#addForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result');
        $("#addForm").submit();
    });
});

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">棚番号一覧</a></li>
        <li>棚番号登録</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>棚番号登録</span></h2>
<h2 class="HeaddingMid">棚の情報を設定してください</h2>
<form id ="addForm" class="validate_form input_form" method="post" action="" accept-charset="utf-8">
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden')); ?>    
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('MstShelfName.facilityName',array('id' => 'facilityName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('MstShelfName.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'r txt validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('MstShelfName.facilityCode',array('options'=>$facility_list ,'id' => 'facilityCode','class'=>'r txt validate[required]','empty'=>true) ); ?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('MstShelfName.departmentName',array('id' => 'departmentName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('MstShelfName.departmentText',array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'r txt validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('MstShelfName.departmentCode', array('options'=>array(), 'id' =>'departmentCode', 'class' =>'r txt validate[required]','empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <th>棚番号</th>
                <td><?php echo $this->form->input('MstShelfName.code', array('id'=>'code','class'=>'r txt','style'=>'width:200px;',"maxlength"=>20)); ?></td>
            </tr>
        </table>

        <hr class="Clear" />

        <div class="ButtonBox">
            <input type="button" class="btn btn2" id="result" />
        </div>
    </div>
</form>