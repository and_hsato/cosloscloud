<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">棚番号一覧</a></li>
        <li class="pankuzu"><?php echo $this->request->data['url'];?></li>
        <li ><?php echo $this->request->data['url']."結果";?></li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span><?php echo $this->request->data['url']."結果";?></span></h2>
<h2 class="HeaddingMid">結果を確認してください</h2>
<div class="Mes01">以下の内容で棚番号データを更新しました</div>
<form id ="resultForm" class="validate_form" method="post" action="" accept-charset="utf-8">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('MstShelfName.facilityName',array('class'=>'lbl','style'=>'width:200px;','readonly'=>'readonly'));?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td><?php echo $this->form->input('MstShelfName.departmentName',array('class'=>'lbl','style'=>'width:200px;','readonly'=>'readonly'));?></td>
            </tr>
            <tr>
                <th>棚番号</th>
                <td><?php echo $this->form->input('MstShelfName.code',array('class'=>'lbl','style'=>'width:200px;','readonly'=>'readonly')); ?></td>
            </tr>
        </table>
    </div>
</form>
