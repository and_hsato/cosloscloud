<script type="text/javascript">
$(function(){
    $("#btn_execution_add_result").click(function(){
        if(!$('#facility').val()){
             alert('施設が選択されていません');
             return;
        }
        if(!$('#csv_file').val()){
            alert('ファイルが選択されていません');
            return;
        } else if(!$('#csv_file').val().match(/\.csv$|\.tsv$|\.vsv$|\.txt$/)){
            alert('CSVファイルを選択してください');
            return;
        }
        $("#form_execution_add_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_import_result');
        $("#form_execution_add_result").attr('method', 'post');
        $("#form_execution_add_result").submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">棚番号一覧</a></li>
        <li>棚番CSV取込</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>棚番CSV取込確認</span></h2>
<h2 class="HeaddingMid">棚番ファイルを選択してください。</h2>
<form id="form_execution_add_result" action="#" method="post" class="validate_form" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="data[selected][from_csv]" value="1" />
    <div class="SearchBox">
        <hr class="Clear" />
        <h2 class="HeaddingSmall">棚番ファイルを選択し、確定ボタンをクリックしてください。</h2>
        <br>
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td>
                <?php echo $this->form->input('facility', array('options'=>$facility_list,'class'=>'txt','style'=>'width:220px;') ); ?>
                </td>
            </tr>
            <tr>
                <th>棚番ファイル</th>
                <td>
                    <input type="file" name="data[selected][csv_file]" id="csv_file" style="width:250px;"/>
                </td>
            </tr>
        </table>

        <div>
        <?php echo (isset($data['errmsg']))?$data['errmsg']:""; ?>
        </div>
        <br>
        <br>
        <br>
        <div class="ButtonBox">
            <input type="button" class="btn btn2 fix_btn [p2]" id="btn_execution_add_result" />
        </div>
        <?php if (isset($csvresult)){ ?>  
        <div class="Mes01"><?php echo $read_count; ?>件取り込みました。</div>
        <div class="Mes01"><?php echo $err_count; ?>件エラーが発生しました。</div>
        <?php } ?>
    </div>
</form>

