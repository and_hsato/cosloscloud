<printdata>
    <setting>
        <layoutname>03_コストシール.rpx</layoutname>
        <fixvalues>
            <value name="printer"><?php echo Configure::read('PrinterType.cost'); ?></value>
        </fixvalues>
    </setting>
    <datatable>
        <?php if(count($records)>0){ ?>
        <columns><?php echo join("\t", array_keys($records[0])); ?></columns>
        <rows>
        <?php foreach ($records as $row): ?>
            <row><?php echo h(join("\t", $row)); ?></row>
        <?php endforeach; ?>
        </rows>
        <?php } ?>
    </datatable>
</printdata>
