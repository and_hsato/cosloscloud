<script type="text/javascript">
$(function(){
    $("#stickers_edit_confirm_form_btn").click(function(){
        var _cnt = 0;
        var _i = 0;
        var _falsecnt = 0;
        $('input[type=checkbox].checkAllTarget').each(function(){
            if(this.checked){
               flg = $('#TrnStickerIssueCheckFlg'+_i ).val();
               _cnt++;
               if(flg == 1){
                  _falsecnt++;
               }
            }
            _i++;
        });
        if(_cnt === 0){
            alert('取消を行う明細を選択してください');
            return false;
        }else if(_falsecnt != 0){
            alert('取消不可シールを '+_falsecnt+' 件選択しています\n確認して下さい');
            return false;
        }else{
            ans = confirm("シールの取消しを行います\nよろしいですか？");
            if(ans){
                $("#stickers_edit_confirm_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_result').submit();
            }
        }
    });
    $("#republish_hospital_stikers_btn").click(function(){
        if($('input[type=checkbox].checkAllTarget:checked').length === 0){
            alert('再発行するシールを選択してください');
            return false;
        }else{
            if(confirm("部署シールを印字します\nよろしいですか？")){
                $("#stickers_edit_confirm_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/0').submit();
            }
        }
    });
    $('#cmdCostStickerPrint').click(function(){
          if($('input[type=checkbox].checkAllTarget:checked').length === 0){
              alert('発行するコストシールを選択してください');
              return false;
          }else{
              if(confirm("コストシールを印字します\nよろしいですか？")){
                  $('#stickers_edit_confirm_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/2').submit();
              }
          }
    });

    $('.checkAll').click(function(){
        $('input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });
});
function anc2(){
   obj = $('#pageDow')[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

function anc(id){
   obj = $('#'+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history">部署シール発行履歴</a></li>
        <li>部署シール発行履歴明細</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>部署シール発行履歴明細</span></h2>

<div align="right" id="pageTop" ></div>
<form class="validate_form" id="stickers_edit_confirm_form" method="post" action="#">
    <?php echo $this->form->input('operationDate', array('type'=>'hidden','value'=>date("Y-m-d H:i:s"))); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>発行順序</th>
                <td>
                    <?php echo $this->form->input('TrnStickerIssue.print_type' , array('options'=>array('1'=>'部署、棚番号' , '0'=> '棚番号、部署','2'=>'棚番号、商品ID') , 'style'=>'height: 20px; width: 120px;' , 'class'=>'r' , 'id'=>'print_type' )); ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <colgroup>
                <col width="20" />
                <col width="140" />
                <col width="80" />
                <col />
                <col />
                <col width="100" />
                <col width="150" />
                <col width="80" />
                <col width="80" />
                <col width="80" />
            </colgroup>
            <tr>
                <th rowspan="2"><input type="checkbox" class="checkAll" /></th>
                <th>発行番号</th>
                <th>発行日</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th>包装単位</th>
                <th>医事コード</th>
                <th>ロット番号</th>
                <th>作業区分</th>
                <th>更新者</th>
            </tr>
            <tr>
                <th colspan="2">施設 ／ 部署</th>
                <th>規格</th>
                <th>販売元</th>
                <th>商品ID</th>
                <th>部署シール</th>
                <th>有効期限</th>
                <th>管理区分</th>
                <th>備考</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even2">
            <colgroup>
                <col width="20" />
                <col width="140" />
                <col width="80" />
                <col />
                <col />
                <col width="100" />
                <col width="150" />
                <col width="80" />
                <col width="80" />
                <col width="80" />
            </colgroup>
            <?php $_i = 0; foreach ($result as $data): ?>
            <tr>
                <td rowspan="2" style="width: 20px;">
                    <input name ="data[TrnStickerIssue][id][<?php echo $_i; ?>]" type='checkbox' class='center checkAllTarget chk' value="<?php echo $data['TrnStickerIssue']['id']; ?>" ?>
                    <?php echo $this->form->input('TrnStickerIssue.checkFlg.'.$_i , array('type'=>'hidden','value'=>$data['TrnStickerIssue']['checkFlg'])); ?>
                </td>
                <td><?php echo h_out($data['TrnStickerIssue']['work_no'] , 'center'); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['work_date'] , 'center'); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['item_name']); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['item_code']); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['unit_name']); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['medical_code']); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['lot_no']); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['work_type']); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['user_name']); ?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo h_out($data['TrnStickerIssue']['facility_name'] . '/' . $data['TrnStickerIssue']['department_name'] ); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['standard']); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['dealer_name']); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['internal_code']); ?></td>
                <td>
                    <label title="<?php echo h($data['TrnStickerIssue']['hospital_sticker_no']); ?>">
                        <input type="hidden" name="data[TrnStickerIssue][hospital_sticker_no][<?php echo $_i;?>]" value="<?php echo h($data['TrnStickerIssue']['hospital_sticker_no']); ?>">
                        <?php echo h($data['TrnStickerIssue']['hospital_sticker_no']); ?>
                    </label>
                </td>
                <td><?php echo h_out($data['TrnStickerIssue']['validated_date']); ?></td>
                <td><?php echo h_out($data['TrnStickerIssue']['promise_type'],'center'); ?></td>
                <td><?php echo $this->form->input('TrnStickerIssue.recital.'.$_i ,array('class'=>'txt','value'=>$data['TrnStickerIssue']['recital'],'style'=>'width: 140px;')); ?></td>
            </tr>
            <?php $_i++; endforeach; ?>
        </table>
    </div>
    <div align="right" id ="pageDow"></div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn6 submit [p2]" value="" id="stickers_edit_confirm_form_btn" />&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" class="btn btn18 submit" value="" id="republish_hospital_stikers_btn" />&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" class="btn btn12 submit" value="" id="cmdCostStickerPrint" />
        </p>
   </div>
</form>
