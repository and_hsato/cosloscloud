<script type="text/javascript">
$(document).ready(function(){
    $('.checkAll').click(function(){
        $('input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });

    // 検索ボタン押下
    $("#stickerissues_history_search_btn").click(function(){
        $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history').submit();
    });
        
    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history');
    });
  
    //印刷ボタン押下
    $("#btn_Print").click(function(){
        $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report').submit();
        $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history');
    });
        
    //明細表示ボタン押下
    $('#cmdConfirm').click(function(){
        if($('input[type=checkbox].checkAllTarget:checked').length > 0 ){
            $('#history_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit').submit();
        }else{
            alert('明細表示を行う項目を選択してください');
        }
    });
});

function anc2(){
   obj = $('#pageDow')[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

function anc(id){
   obj = $('#'+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>部署シール発行履歴</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>部署シール発行履歴</span></h2>
<form class="validate_form search_form input_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history" id="history_form">
    <?php echo $this->form->input('selected.is_search',array('type' => 'hidden','id' => 'is_search')); ?>
    <?php echo $this->form->input('MstFacility.id',array('type' => 'hidden','value'=>$this->Session->read('Auth.facility_id_selected'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="60" align="right" />
            </colgroup>
            <tr>
                <th>発行番号</th>
                <td><?php echo $this->form->text('TrnStickerIssueHeader.work_no',array('class' => 'txt','style' => 'width:110px'));?></td>
                <td></td>
                <th>発行日</th>
                <td>
                    <span>
                        <?php echo $this->form->input('TrnStickerIssueHeader.work_date_start',array('class'=>'txt validate[optional,custom[date],funcCall2[date2]] date', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' ));?>
                        &nbsp;～&nbsp;
                        <?php echo $this->form->input('TrnStickerIssueHeader.work_date_end',array('class'=>'txt validate[optional,custom[date],funcCall2[date2]] date ', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' ));?>
                    </span>
                </td>
                <td></td>
                <td>
                    <?php echo $this->form->checkbox('TrnStickerIssueHeader.delete_opt',array('class' =>'center')); ?>取消も表示する
                </td>
                <td>
                    <?php echo $this->form->checkbox('TrnStickerIssueHeader.no_shipping',array('class' =>'center')); ?>未出荷を表示
                </td>
            </tr>
            <tr>
                <th>発行施設</th>
                <td>
                    <?php echo $this->form->input('TrnStickerIssueHeader.facilityText',array('class'=>'txt require','style'=>'width:60px;','id'=>'facilityText')); ?>
                    <?php echo $this->form->input('TrnStickerIssueHeader.facilityCode',array('options'=>$facility_List,'class'=>'txt','style'=>'width:120px;','id'=>'facilityCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('TrnStickerIssueHeader.facilityName',array('type'=>'hidden','id'=>'facilityName')); ?>
                </td>
                <td></td>
                <th>発行部署</th>
                <td>
                    <?php echo $this->form->input('TrnStickerIssueHeader.departmentText',array('class'=>'txt','style'=>'width:60px;','id'=>'departmentText')); ?>
                    <?php echo $this->form->input('TrnStickerIssueHeader.departmentCode',array('options'=>$department_List, 'class'=>'txt','style'=>'width:120px;','id'=>'departmentCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('TrnStickerIssueHeader.departmentName',array('type'=>'hidden','id'=>'departmentName')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFacilityItem.internal_code',array('class' => 'txt search_internal_code','style' => 'width:120px',)); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_code',array('class' => 'txt search_upper','style' => 'width:120px',)); ?></td>
                <td></td>
                <th>管理区分</th>
                <td>
                    <?php echo $this->form->input('TrnFixedPromise.promise_type',array('type'=>'select' , 'options'=> Configure::read('PromiseType.list'),'empty'=>true , 'style' => 'width:120px','class' => 'txt')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_name',array('class' => 'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFacilityItem.dealer_name',array('class' => 'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnStickerIssueHeader.work_class_txt',array('type'=>'hidden','id'=>'work_class_txt')); ?>
                    <?php echo $this->form->input('TrnStickerIssueHeader.work_class',array('type'=>'select' , 'options'=>$work_classes, 'empty'=>true ,'class'=>'txt','id'=>'work_class')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFacilityItem.standard',array('class' => 'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->text('TrnStickerIssue.hospital_sticker_no',array('class' => 'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>定数区分</th>
                <td>
                    <?php echo $this->form->input('MstFixedCount.trade_type', array('options'=>array(1=>'定数品', 2=>'準定数品') , 'style' => 'width:120px','class' => 'txt','empty'=>true )); ?>
                </td>
            </tr>
            <tr>
                <td colspan="9" style="height: 10px;"></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn1" id="stickerissues_history_search_btn" />
            <input type="button" class="btn btn5" id="btn_Csv"/>
            <!--<input type="button" class="btn btn10" id="btn_Print"/>-->
        </p>
    </div>
    <hr class="Clear" />
    <?php if(isset($result) && count($result) !== 0){ ?>
    <div align="right" id="pageTop" ></div>
    <?php } ?>
    <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($result) ) ); ?>
    </div>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle01">
            <tr>
                <th style="width:20px;"><input type="checkbox" class="checkAll" /></th>
                <th style="width:135px;">発行番号</th>
                <th style="width:85px;">発行日</th>
                <th>施設 ／ 部署</th>
                <th class="col10">登録者</th>
                <th style="width:140px;">登録日時</th>
                <th class="col15">備考</th>
                <th class="col10">件数</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle01 table-even">
        <?php $i = 0; foreach($result as $contents) {?>
            <tr>
                <td style="width:20px;" align="center">
                    <input type="checkbox" name="data[TrnStickerIssueHeader][id][<?php echo $i?>]" value ="<?php echo h($contents['TrnStickerIssueHeader']['id']) ?>" class="chk center checkAllTarget" >
                </td>
                <td style="width:135px;"><?php echo h_out($contents['TrnStickerIssue']['work_no'],'center'); ?></td>
                <td style="width:85px;"><?php echo h_out($contents['TrnStickerIssue']['work_date'],'center'); ?></td>
                <td><?php echo h_out($contents['TrnStickerIssue']['facility_name'].' / '.$contents['TrnStickerIssue']['department_name']); ?></td>
                <td class="col10"><?php echo h_out($contents['TrnStickerIssue']['user_name']); ?></td>
                <td style="width:140px;" align="center"><?php echo h_out($contents['TrnStickerIssueHeader']['created'],'center'); ?></td>
                <td class="col15"><?php echo h_out($contents['TrnStickerIssueHeader']['recital']); ?></td>
                <td class="col10" align="right"><?php echo h_out($contents['TrnStickerIssueHeader']['count'] . '/' . $contents['TrnStickerIssueHeader']['detail_count'] ,'right'); ?></td>
            </tr>
            <?php $i++;} ?>
            <?php if(isset($this->request->data['selected']['is_search']) && count($result) == 0 ){ ?>
            <tr><td colspan="8" class="center">該当するデータがありませんでした</td></tr>
            <?php } ?>
        </table>
    </div>
    <div align="right" id ="pageDow"></div>
    <?php if( count($result) > 0){ ?>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn7 submit" value="" id="cmdConfirm"/>
        </p>
    </div>
    <?php } ?>
</form>
