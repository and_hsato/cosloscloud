<script type="text/javascript">
//jquery calendar
$(document).ready(function(){

    $(function(){
        $('#cmdStickerPrint').click(function(){
              ans=confirm("シール印刷を行います。\nよろしいですか？");
              if(ans === true){
                  $('#stickers_add_result_form').attr('action', '<?php echo $this->webroot; ?>stickerissues/seal/1').submit();
                  $('#cmdStickerPrint').attr("disabled","disabed");
              }
        });
        $('#cmdCostStickerPrint').click(function(){
              ans=confirm("シール印刷を行います。\nよろしいですか？");
              if(ans === true){
                  $('#stickers_add_result_form').attr('action', '<?php echo $this->webroot; ?>stickerissues/seal/3').submit();
              }
        });
    });

 });
</script>
        <div id="TopicPath">
            <ul>
                <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
                <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_search">部署シール未発行一覧</a></li>
                <li class="pankuzu">部署シール発行確認</li>
                <li>発行結果</li>
            </ul>
        </div>
        <h2 class="HeaddingLarge"><span>部署シール発行結果</span></h2>
        <div class="Mes01">以下の部署を対象に部署シールを発行します</div>
<form class="validate_form" id="stickers_add_result_form" method="POST" action="<?php echo $this->webroot; ?>stickerissues/add_result">
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <?php $i=0;foreach($stickerNos as $_no): ?>
        <input type="hidden" name="data[Sticker][StickerNo][<?php echo $i?>]" value = "<?php echo $_no; ?>" />
    <?php $i++; endforeach; ?>
        <input type="hidden" name="data[Sticker][StickerOrder]" value = "<?php echo $printOrder; ?>" />
        <div class="SearchBox">
        <hr class="Clear" />
        <table class="FormStyleTable">
        <tr>
            <th>発行順序</th>
            <td>
              <?php
                  if ($printType == 1) {
                      $_print_type="部署、棚番号";
                  } else if ($printType == 0) {
                      $_print_type="棚番号、部署";
                  } else if ($printType == 2) {
                      $_print_type="棚番号、商品ID";
                  }
              ?>
              <?php echo $this->form->input('print_type',array('class'=>'lbl','value'=>$_print_type,'readonly'=>'readonly')); ?>
              <?php echo $this->form->input('TrnStickerIssue.print_type',array('type'=>'hidden','id'=>'work_class_txt')); ?>
            </td>
            <td width="20"></td>
            <th>作業区分</th>
            <td>
                <?php echo $this->form->input('TrnStickerIssueHeader.work_class_txt',array('class'=>'lbl','readonly'=>'readonly')); ?>
                <?php echo $this->form->input('TrnStickerIssueHeader.work_class',array('type'=>'hidden','id'=>'work_class')); ?>

            </td>
            <td width="20"></td>
            <th>備考</th>
            <td><?php echo $this->form->input('TrnStickerIssueHeader.recital',array('class'=>'lbl','readonly'=>'readonly')); ?></td>
        </tr>
        <tr>
            <th>施設</th>
            <td>
            <?php echo $this->form->input('MstFacility.facility_name',array('label' => false, 'div' => false, 'class' => 'lbl', 'readonly'=>'readonly')); ?>
            </td>
            <td width="20"></td>
            <th>引当日</th>
            <td>
            <?php echo  $this->form->input('TrnFixedPromise.work_date_start',array('class'=>'lbl', 'div' => 'false', 'label' => 'false' , 'readonly'=>'readonly' )); ?>
             ～
            <?php echo  $this->form->input('TrnFixedPromise.work_date_end',array('class'=>'lbl', 'div' => 'false', 'label' => 'false', 'readonly'=>'readonly' )); ?>
            </td>
        </tr>
        <tr>
            <th>商品ID</th>
            <td><?php echo $this->form->text('MstFacilityItem.internal_code', array('label' => false, 'class' => 'lbl', 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>製品番号</th>
            <td><?php echo $this->form->text('MstFacilityItem.item_code', array('label' => false, 'class' => 'lbl', 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>管理区分</th>
            <td>
            <?php echo $this->form->input('TrnFixedPromise.promise_type_name',array('type' => 'text','class' => 'lbl','readonly'=>'readonly')); ?>
            </td>
        </tr>
        <tr>
            <th>商品名</th>
            <td><?php echo $this->form->text('MstFacilityItem.item_name', array('label' => false, 'class' => 'lbl', 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>販売元</th>
            <td><?php echo $this->form->text('MstDealer.dealer_name', array('label' => false, 'class' => 'lbl' , 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>定数区分</th>
            <td>
            <?php echo $this->form->input('MstFixedCount.trade_type_name', array('type'=>'text','class' => 'lbl', 'readonly'=>'readonly' )); ?>
            </td>
        </tr>
        <tr>
            <th>規格</th>
            <td><?php echo $this->form->text('MstFacilityItem.standard', array('label' => false, 'class' => 'lbl', 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>JANコード</th>
            <td><?php echo $this->form->text('MstFacilityItem.jan_code', array('label' => false, 'class' => 'lbl' , 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>要求作業区分</th>
            <td>
            <?php echo $this->form->input('TrnPromise.work_type_name', array('type'=>'text','class' => 'lbl', 'readonly'=>'readonly' )); ?>
            </td>
        </tr>
        </table>
        <hr class="Clear" />

<?php if($ErrorFlg): ?>
    <span>
        <div class="Mes01">シール発行登録時にエラーが発生しました。<BR>再度検索を行ってください。</div>
        <div>
            エラー内容：<?php echo $Msg; ?>
        </div>
    </span>
<?php else: ?>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
            <tr>
                <th style="width:20px;"></th>
                <th class="col15" style="width:140px;">作業番号</th>
                <th class="col45">施設/部署</th>
                <th class="col10" style="width:80px;">引当日</th>
                <th class="col10">シール枚数</th>
                <th class="col10">作業区分</th>
                <th class="col20">備考</th>
            </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col/>
                    <col align="center"/>
                    <col/>
                    <col align="center"/>
                    <col align="right"/>
                    <col/>
                </colgroup>
                <?php
                  $_cnt=0;foreach($SearchResult as $_row):
                  $search_data = '';
                  $search_data = $_row['id'].",".$_row['department_id_from'].",".date('Y/m/d',strtotime($_row['work_date']));
                  $work_type = "";
                  if(isset($_row['work_class'])){
                   $work_type = $_row['work_class'];
                  }
                ?>
                <tr>
                    <td style="width: 20px;">
                        <?php echo $this->form->input("TrnStickerIssue.Rows.{$_row['id']}.row_search_data",array('type'=>'hidden','id'=>'row_search_data','value'=>$search_data)); ?>
                    </td>
                    <td class="col15" style="width:140px;">
                          <label title="<?php echo($_row['work_no']);?>"><?php echo($_row['work_no']);?></label>
                          <?php echo $this->form->input("TrnStickerIssue.Rows.{$_cnt}.work_no",array('type'=>'hidden','id'=>'work_no'.$_cnt,'value'=>$_row['work_no'])); ?>
                    </td>
                    <td class="col45"><?php echo h_out($_row['facility_name'].'/'.$_row['department_name']);?></td>
                    <td class="col10 center" style="width:80px;"><?php echo h_out(date('Y/m/d',strtotime($_row['work_date'])), 'center'); ?></td>
                    <td class="col10 right"><?php echo h_out($_row['sum_remains'],'right');?></td>
                    <td class="col10">
                        <?php echo $this->form->input("TrnStickerIssue.Rows.{$_cnt}.work_class",array('type'=>'hidden','id'=>'work_class'.$_cnt,'value'=>$work_type)); ?>
                        <?php echo $this->form->text("TrnStickerIssue.Rows.{$_cnt}.work_classs_txt",array('style'=>'width: 85px;','class'=>'lbl','id'=>'work_classs_txt'.$_cnt,'value'=>$_row['work_class_txt'],'readonly'=>'readonly')); ?>
                    </td>
                    <td class="col20"><?php echo $this->form->text("TrnStickerIssue.Rows.{$_cnt}.recital", array('style'=>'width: 185px;','id'=>'recital'.$_cnt, 'class' => 'lbl','value'=>$_row['recital'],'readonly'=>'readonly')); ?></td>
                    </tr>
                <?php $_cnt++;endforeach;?>
            </table>
        </div>
    </div>
            <div class="ButtonBox">
                <input type="button" class="btn btn16 result_btn" value="" id="cmdStickerPrint" />
                <input type="button" class="btn btn12 result_btn" value="" id="cmdCostStickerPrint" />
            </div>
</form>
<?php endif; ?>
