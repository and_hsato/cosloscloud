<script type="text/javascript">
$(document).ready(function(){
    $(function(){
        $('#cmdResultview').click(function(){
            var _cnt = 0;
            var _i = 0;
            $('input[type=checkbox].checkAllTarget').each(function(){
                $('#work_class_txt'._i).val($('#work_class'+_i+' option:selected').text());
                if($(this).attr('checked') == true){
                    _cnt++;
                }
                _i++;
            });
            $('#stickers_add_result_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_result').submit();
        });
    });
});

function setChangeClass(id) {
    $('#work_class_txt'+id).val($('#work_class'+id+' option:selected').text());
}
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_search">部署シール未発行一覧</a></li>
        <li>発行確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>部署シール発行確認</span></h2>
<h2 class="HeaddingMid">以下の部署を対象に部署シールを発行します。</h2>
<p><font color="red">※確定を押下した時点でシール発行履歴に登録されます。</font></p>
<form class="validate_form" id="stickers_add_result_form" method="POST" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_result_view">
<?php echo $this->form->input('TrnStickerIssue.time' , array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s')))?>
<?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden')); ?>
    <table class="FormStyleTable">
        <tr>
            <th>発行順序</th>
            <td>
                <?php
                    if ($_POST['data']['TrnStickerIssue']['print_type'] == 1) {
                        $_print_type="部署、棚番号";
                    } else if ($_POST['data']['TrnStickerIssue']['print_type'] == 0) {
                        $_print_type="棚番号、部署";
                    } else if ($_POST['data']['TrnStickerIssue']['print_type'] == 2) {
                        $_print_type="棚番号、商品ID";
                    }
                ?>
                <?php echo $this->form->input('print_type',array('class'=>'lbl','value'=>$_print_type,'readonly'=>'readonly')); ?>
                <?php echo $this->form->input('TrnStickerIssue.print_type',array('type'=>'hidden','id'=>'work_class_txt','value'=>$this->request->data['TrnStickerIssue']['print_type'])); ?>
            </td>
            <td width="20"></td>
            <th>作業区分</th>
            <td>
                <?php echo $this->form->input('TrnStickerIssueHeader.work_class_txt',array('class'=>'lbl','readonly'=>'readonly')); ?>
                <?php echo $this->form->input('TrnStickerIssueHeader.work_class',array('type'=>'hidden','id'=>'work_class')); ?>
            </td>
            <td width="20"></td>
            <th>備考</th>
            <td>
                <?php echo $this->form->text('TrnStickerIssueHeader.recital',array('class'=>'lbl','readonly'=>'readonly')); ?>
            </td>
        </tr>
        <tr>
            <th>施設</th>
            <td>
            <?php echo $this->form->input('MstFacility.facility_name',array('label' => false, 'div' => false, 'class' => 'lbl', 'readonly'=>'readonly')); ?>
            </td>
            <td width="20"></td>
            <th>引当日</th>
            <td>
            <?php echo  $this->form->input('TrnFixedPromise.work_date_start',array('class'=>'lbl', 'div' => 'false', 'label' => 'false' , 'readonly'=>'readonly' )); ?>
             ～
            <?php echo  $this->form->input('TrnFixedPromise.work_date_end',array('class'=>'lbl', 'div' => 'false', 'label' => 'false', 'readonly'=>'readonly' )); ?>
            </td>
        </tr>
        <tr>
            <th>商品ID</th>
            <td><?php echo $this->form->text('MstFacilityItem.internal_code', array('label' => false, 'class' => 'lbl', 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>製品番号</th>
            <td><?php echo $this->form->text('MstFacilityItem.item_code', array('label' => false, 'class' => 'lbl', 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>管理区分</th>
            <td>
            <?php echo $this->form->input('TrnFixedPromise.promise_type_name',array('type' => 'text','class' => 'lbl','readonly'=>'readonly')); ?>
            </td>
        </tr>
        <tr>
            <th>商品名</th>
            <td><?php echo $this->form->text('MstFacilityItem.item_name', array('label' => false, 'class' => 'lbl', 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>販売元</th>
            <td><?php echo $this->form->text('MstDealer.dealer_name', array('label' => false, 'class' => 'lbl' , 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>定数区分</th>
            <td>
            <?php echo $this->form->input('MstFixedCount.trade_type_name', array('type'=>'text','class' => 'lbl', 'readonly'=>'readonly' )); ?>
            </td>
        </tr>
        <tr>
            <th>規格</th>
            <td><?php echo $this->form->text('MstFacilityItem.standard', array('label' => false, 'class' => 'lbl', 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>JANコード</th>
            <td><?php echo $this->form->text('MstFacilityItem.jan_code', array('label' => false, 'class' => 'lbl' , 'readonly'=>'readonly')); ?></td>
            <td width="20"></td>
            <th>要求作業区分</th>
            <td>
            <?php echo $this->form->input('TrnPromise.work_type_name', array('type'=>'text','class' => 'lbl', 'readonly'=>'readonly' )); ?>
            </td>
        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
    </table>

    <div class="SearchBox">
        <hr class="Clear" />
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
            <tr>
                <th style="width:20px;"></th>
                <th class="col50">施設/部署</th>
                <th class="col10" style="width:80px;">引当日</th>
                <th class="col10">シール枚数</th>
                <th class="col10">作業区分</th>
                <th class="col20">備考</th>
            </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col/>
                    <col/>
                    <col align="center"/>
                    <col align="right"/>
                    <col/>
                    <col/>
                </colgroup>
                <?php $_cnt=0;foreach($SearchResult as $_row):
                   $search_data = '';
                   $search_data = $_row['id'].",".$_row['department_id_from'].",".date('Y/m/d',strtotime($_row['work_date']));
                ?>
                <tr>
                    <td style="width: 20px;"></td>
                    <td class="col50"><?php echo h_out($_row['facility_name'].'/'.$_row['department_name']);?></td>
                    <td class="col10 center" style="width:80px;"><?php echo h_out(date('Y/m/d',strtotime($_row['work_date'])),'center'); ?></td>
                    <td class="col10 right"><?php echo h_out($_row['sum_remains'] , 'right');?></td>
                    <td class="col10">
                        <?php echo $this->form->input("TrnStickerIssue.Rows.{$_cnt}.work_class_txt",array('type'=>'hidden','id'=>'work_class_txt'.$_cnt,'value' => '')); ?>
                        <?php echo $this->form->input("TrnStickerIssue.Rows.{$_cnt}.work_class",array('options'=>$work_classes,'empty'=>true,'class'=>'txt','id'=>'work_class'.$_cnt ,'onChange'=>'setChangeClass('.$_cnt.');')); ?>
                    </td>
                    <td class="col20">
                        <?php echo $this->form->input("TrnStickerIssue.Rows.{$_cnt}.recital", array('label'=>false,'class'=>'txt','style'=>'width:185px;',"maxlength"=>200)); ?>
                    </td>
                </tr>
                <?php $_cnt++;endforeach;?>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="btn btn2 resultview_btn [p2]" id="cmdResultview" />
        </div>
    </div>
</form>
