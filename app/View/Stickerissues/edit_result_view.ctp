<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history">部署シール発行履歴</a></li>
        <li class="pankuzu">部署シール発行履歴明細</li>
        <li>部署シール発行取消</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>部署シール発行取消</span></h2>
<div class="Mes01">部署シール発行を取り消しました</div>
<form>
    <div class="SearchBox">
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <colgroup>
                    <col width="20" />
                    <col width="140" />
                    <col width="80" />
                    <col width="70" />
                    <col />
                    <col />
                    <col width="140" />
                    <col width="80" />
                    <col width="100" />
                    <col width="100" />
                </colgroup>
                <tr>
                    <th rowspan="2"></th>
                    <th>発行番号</th>
                    <th>発行日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>ロット番号</th>
                    <th>作業区分</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th colspan="2">施設 ／ 部署</th>
                    <th>状態</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>部署シール</th>
                    <th>有効期限</th>
                    <th colspan="2">備考</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02 table-even2">
                <colgroup>
                    <col width="20" />
                    <col width="140" />
                    <col width="80" />
                    <col width="70" />
                    <col />
                    <col />
                    <col width="140" />
                    <col width="80" />
                    <col width="100" />
                    <col width="100" />
                </colgroup>
                <?php $_i = 0; foreach ($result as $data): ?>
                <tr>
                    <td rowspan="2" style="width: 20px;"></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['work_no'],'center'); ?></td>
                    <td><?php echo h_out(date("Y/m/d",strtotime($data['TrnStickerIssue']['work_date'])),'center'); ?></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['item_name']); ?></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['item_code']); ?></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['unit_name']); ?></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['lot_no']); ?></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['work_type']); ?></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['user_name']); ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($data['TrnStickerIssue']['facility_name'] . '/' . $data['TrnStickerIssue']['department_name']); ?></td>
                    <td></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['standard']); ?></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['dealer_name']); ?></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['hospital_sticker_no']); ?></td>
                    <td><?php echo h_out($data['TrnStickerIssue']['validated_date']); ?></td>
                    <td colspan="2"><?php echo $this->form->input('TrnStickerIssue.'.$_i.'.recital',array('class'=>'txt','value'=>h($data['TrnStickerIssue']['recital']) , 'readonly'=>'readonly' ) ); ?></td>
                </tr>
                <?php $_i++; endforeach; ?>
            </table>
        </div>
    </div>
</form>
