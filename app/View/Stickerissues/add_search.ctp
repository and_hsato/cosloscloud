<script type="text/javascript">
$(document).ready(function(){
    // 検索ボタン押下
    $("#stickers_add_search_form_btn").click(function(){
        $("#stickers_add_search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_search').submit();
    });

    $('#cmdConfirm').click(function(){
        if($('input[type=checkbox].checkAllTarget:checked').length === 0){
            alert('発行を行う部署を選択してください');
        }else{
            $('#stickers_add_search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_confirm').submit();
        }
    });

    $('.checkAll').click(function(){
        $('input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });

    $('#work_class').change(function () {
        $('#work_class_txt').val($('#work_class option:selected').text());
    });

    $('#promise_type').change(function () {
        $('#promise_type_name').val($('#promise_type option:selected').text());
    });

    $('#trade_type').change(function () {
        $('#trade_type_name').val($('#trade_type option:selected').text());
    });

    $('#promise_class').change(function () {
        $('#promise_class_name').val($('#promise_class option:selected').text());
    });

});
function anc2(){
   obj = $('#pageDow')[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

function anc(id){
   obj = $('#'+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>部署シール発行</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>部署シール未発行一覧</span></h2>
<h2 class="HeaddingMid">発行を行う部署の条件を指定してください</h2>
<form class="validate_form search_form input_form" id="stickers_add_search_form" method="POST" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_search">
    <input type="hidden" name="data[IsSearch]" value="1" />
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>発行順序</th>
                <td>
                    <input type="hidden" name="data[TrnStickerIssue][print_type]" value="data[TrnStickerIssue][print_type]" />
                    <?php echo $this->form->input('TrnStickerIssue.print_type' , array('options'=>array('1'=>'部署、棚番号' , '0'=> '棚番号、部署','2'=>'棚番号、商品ID') , 'style'=>'height: 20px; width: 120px;' , 'class'=>'r' , 'id'=>'print_type' )); ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnStickerIssueHeader.work_class_txt',array('type'=>'hidden','id'=>'work_class_txt')); ?>
                    <?php echo $this->form->input('TrnStickerIssueHeader.work_class',array('type'=>'select' , 'options'=>$work_classes,'empty'=>true ,'class'=>'txt','id'=>'work_class')); ?>
                </td>
                <td width="20"></td>
                <th>備考</th>
                <td><?php echo $this->form->text('TrnStickerIssueHeader.recital', array('label' => false, 'class' => 'txt')); ?></td>
            </tr>
        </table>
        <h2 class="HeaddingSmall">検索条件</h2>
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('MstFacility.facilityText',array('class'=>'txt require','style'=>'width:60px;','id'=>'facilityText')); ?>
                    <?php echo $this->form->input('MstFacility.facilityCode',array('options'=>$facility_List,'class'=>'txt','style'=>'width:120px;','id'=>'facilityCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('MstFacility.facilityName',array('type'=>'hidden','id'=>'facilityName')); ?>
                </td>
                <td width="20"></td>
                <th>引当日</th>
                <td><?php echo  $this->form->input('TrnFixedPromise.work_date_start',array('class'=>'txt date validate[optional,custom[date]]', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                ～
                    <?php echo  $this->form->input('TrnFixedPromise.work_date_end',array('class'=>'txt date validate[optional,custom[date]]', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                </td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFacilityItem.internal_code', array('label' => false, 'class' => 'txt search_internal_code')); ?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_code', array('label' => false, 'class' => 'txt search_upper')); ?></td>
                <td width="20"></td>
                <th>管理区分</th>
                <td>
                <?php echo $this->form->input('TrnFixedPromise.promise_type',array('type'=>'select' , 'options'=>Configure::read('PromiseType.list'),'empty'=>true , 'style' => 'width:120px','class' => 'txt' , 'id'=>'promise_type')); ?>
                <?php echo $this->form->input('TrnFixedPromise.promise_type_name',array('type' => 'hidden' , 'id'=>'promise_type_name')); ?>
                </td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_name', array('label' => false, 'class' => 'txt search_canna')); ?></td>
                <td width="20"></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstDealer.dealer_name', array('label' => false, 'class' => 'txt search_canna')); ?></td>
                <td width="20"></td>
                <th>定数区分</th>
                <td>
                <?php echo $this->form->input('MstFixedCount.trade_type', array('options'=>array(1=>'定数品', 2=>'準定数品') , 'style' => 'width:120px','class' => 'txt','empty'=>true , 'id'=>'trade_type' )); ?>
                <?php echo $this->form->input('MstFixedCount.trade_type_name', array('type' => 'hidden' , 'id'=>'trade_type_name')); ?>
                </td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFacilityItem.standard', array('label' => false, 'class' => 'txt search_canna')); ?></td>
                <td width="20"></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('MstFacilityItem.jan_code', array('label' => false, 'class' => 'txt')); ?></td>
                <td width="20"></td>
                <th>要求作業区分</th>
                <td>
                <?php echo $this->form->input('TrnPromise.work_type', array('options'=>$promise_class , 'style' => 'width:120px','class' => 'txt','empty'=>true , 'id'=>'promise_class' )); ?>
                <?php echo $this->form->input('TrnPromise.work_type_name', array('type' => 'hidden' , 'id'=>'promise_class_name')); ?>
                </td>
            </tr>
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox"><input type="button" value="" class="btn btn1" id="stickers_add_search_form_btn" /></div>

    <div id="results">
        <hr class="Clear" />
        <div align="right" id="pageTop" ></div>
        <div class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
        </div>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
                <tr>
                    <th style="width: 20px;" align="center"><input type="checkbox" class="checkAll" checked="checked"/></th>
                    <th class="col80">施設/部署</th>
                    <th style="width:80px;" class="col10">引当日</th>
                    <th class="col10">シール枚数</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
            <?php foreach($SearchResult as $_row){
                 $search_data = '';
                 $search_data = $_row['TrnStickerIssue']['id'].",".$_row['TrnStickerIssue']['department_id'].",".$_row['TrnStickerIssue']['work_date'];
            ?>
                <tr>
                    <td style="width: 20px;" align="center"><?php echo $this->form->checkbox("TrnStickerIssue.Rows.{$search_data}", array('class'=>'center checkAllTarget','checked' => 'checked')); ?></td>
                    <td class="col80"><?php echo h_out($_row['TrnStickerIssue']['facility_name'].'/'.$_row['TrnStickerIssue']['department_name']); ?></td>
                    <td style="width:80px;" class="col10 center"><?php echo h_out($_row['TrnStickerIssue']['work_date'] , 'center'); ?></td>
                    <td class="col10 right"><?php echo h_out($_row['TrnStickerIssue']['count'],'right');?></td>
                </tr>
                <?php } ?>
                <?php if(isset($this->request->data['IsSearch']) && count($SearchResult) == 0 ){ ?>
                <tr><td colspan="4" class="center">該当するデータはありません。</td></tr>
                <?php } ?>

            </table>
            <div align="right" id ="pageDow"></div>
            <?php if(count($SearchResult)>0){ ?>
            <div class="ButtonBox">
                <input type="button" class="btn btn3 confirm_btn" value="" id="cmdConfirm" />
            </div>
            <?php } ?>
        </div>
    </div>
</form>
