<stickers>
<?php if(count($Sticker)>0){ ?>
<?php foreach($Sticker as $p_num => $data ){ ?>
<printdata>
    <setting>
        <?php if($p_num == Configure::read('PrinterType.hospital.extraordinary') && $data[0]['レイアウト'] == '1' ){ ?>
        <layoutname>02_部署シール_1.rpx</layoutname>
        <?php } else { ?>
        <layoutname>02_部署シール.rpx</layoutname>
        <?php } ?>
        <fixvalues>
            <value name="printer"><?php echo $p_num; ?></value>
            <value name="sortkey"><?php echo $sortkey ?></value>
        </fixvalues>
    </setting>
    <datatable>
        <columns><?php echo join("\t", array_keys($data[0])); ?></columns>
        <rows>
            <?php foreach($data as $v){ ?>
            <row><?php echo h(join("\t", array_values($v))); ?></row>
            <?php } ?>
        </rows>
    </datatable>
</printdata>
<?php } ?>
<?php }else{ ?>
<printdata>
    <setting>
        <layoutname>02_部署シール.rpx</layoutname>
    </setting>
    <datatable>
    </datatable>
</printdata>
<?php } ?>
</stickers>