<script>
$(function($){
    //見積依頼ボタン押下
    $("#bid_btn").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#same_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/same_order/').submit();
        } else {
            alert('必ず一つは選択してください');
        }
    });

    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
    });

    $(".img").error(function(){
        $(this).attr('src', '<?php echo $this->webroot; ?>/images/item/no-image.jpg');
    });
    
});
</script>

<div id="content-wrapper">
    <div id="TopicPath">
        <ul>
            <li><a href="<?php echo $this->webroot?>">ＴＯＰ</a></li>
            <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/edit_search">Myマスタ一覧</a></li>
            <li class="pankuzu">Myマスタ編集</li>
            <li>同種同効品一覧</li>
        </ul>
    </div>

    <h2 class="HeaddingLarge"><span>同種同効品一覧 【<?php echo $baseItemName ?>】</span></h2>
    <h2 class="HeaddingMid">（<?php echo $baseItemName ?>）の同種同効品一覧です。お好みの製品を選択し仕入先へ見積依頼が行えます。</h2>

  <form id="same_form" method="post">
    <?php echo $this->element('masters/grandmaster/rfp_item_table', array('chkAllClass' => 'checkAll', 'chkClass' => 'chk')) ?>
    <br />
    <div class="ButtonBox">
        <input type="button" class="common-button black" id="btn_back" value="戻る"/>
        <?php if (!empty($items)) { ?> <input type="button" class="common-button [p2]" id="bid_btn"  value="見積依頼へ進む" /> <?php } ?>
    </div>
  </form>

</div><!--#content-wrapper-->
