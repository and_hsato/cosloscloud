<script type="text/javascript">
$(document).ready(function(){
    $('.date').datepicker('destroy');
    $('select , input[type=text]').attr("readonly","readonly").removeClass('r txt date' ).addClass("lbl");
    $('input[type=button]').css('display','none');
    $('input[type=checkbox] , select').each(function(){
        $(this).after('<input type=hidden name=' + $(this).attr('name') + ' value=' + $(this).val() + ' >');
        $(this).attr('disabled' , 'disabled');
    });
    $('#btn_list').css('display','inline');
    $('#btn_new').css('display','inline');
    $('td').addClass('border-none');
  
    $('#floating-menu').css('display','none');
  
    $('#btn_new').click(function(){
        $('#result_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search#search_result').submit();
    });
  
    $('#btn_list').click(function(){
        $('#result_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_search#search_result').submit();
    });
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">Myマスタ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">Myマスタ新規登録</a></li>
        <li class="pankuzu">Myマスタ新規登録情報入力</li>
        <li class="pankuzu">Myマスタ新規登録情報確認</li>
        <li>Myマスタ新規登録完了</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>Myマスタ新規登録完了</span></h2>
<h2 class="Mes01 HeaddingMid">Myマスタへ新規商品の登録が完了しました。</h2>

 <h3 class="conth3">登録完了商品情報</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <?php echo $this->element('masters/facility_items/item_Table');?>
        </table>
    </div>
    <h3 class="conth3">仕入設定</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>
                    <table>
                        <colgroup>
                            <col width="150"/>
                            <col width="150"/>
                            <col width="150"/>
                            <col width="150"/>
                        </colgroup>
                        <tr>
                           <td>仕入先</td>
                           <td>仕入価格</td>
                           <td>仕入単位</td>
                        </tr>
                        <tr>
                            <td><?php echo $this->form->input('MstTransactionConfig.partner_facility_id', array('options'=>$suppliers, 'empty'=>true, 'class' => 'txt r validate[required]','style'=>'width: 120px; height: 23px;','div'=>false,'label'=>false)); ?></td>
                            <td><?php echo $this->form->input('MstTransactionConfig.transaction_price',array('type'=>'text' , 'class'=>'r validate[required] right')); ?></td>
                            <td><?php echo $this->form->input('MstFacilityItem.mst_unit_name_id', array('options'=>$item_units, 'empty'=>true ,'class' => 'r validate[required]', 'style'=>'width: 80px;')); ?>
                                <?php echo $this->form->input('MstFacilityItem.converted_num', array('type'=>'hidden','value'=>1)); ?>
                            </td>
                        </tr>
                    </table>
                </th>
            </tr>
        </table>
    </div>

<form class="validate_form" id="result_form" method="post">
    <?php echo $this->form->input('search.is_search', array('type'=>'hidden','value'=>1)); ?>
    <?php echo $this->form->input('hide_action', array('type'=>'hidden','value'=>1)); ?>
    <?php echo $this->form->input('MstGrandmaster.isAccepted', array('type'=>'hidden')); ?>  
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="common-button" id="btn_new" value="続けて登録"/>
            <input type="button" class="common-button" id="btn_list" value="Myマスタ一覧"/>
        </p>
    </div>
</form>
</div><!--#content-wrapper-->