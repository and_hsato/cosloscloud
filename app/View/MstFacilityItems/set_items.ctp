<script type="text/javascript">
$(function(){
    $("#search_btn").click(function(){
        validateDetachSubmit($('#search_form') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_items' );
     });
    $("#add_btn").click(function(){
        validateDetachSubmit($('#search_form') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_set_items' );
    });
    $("#edit_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_set_items').submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>セット品一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>セット品一覧</span></h2>
<h2 class="HeaddingMid">セット品検索条件</h2>
<form class="validate_form search_form" id="search_form" method="post">
    <input type="hidden" name="data[search][is_search]"/>
    <div class="SearchBox">
        <div class="ColumnLeft">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>セットID</th>
                    <td><?php echo $this->form->text('MstFacilityItems.internal_code' ,array('class' => 'txt search_internal_code','maxlength' => '50' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th>規格</th>
                    <td><?php echo $this->form->text('MstFacilityItems.standard',array('class' => 'txt search_canna','maxlength' => '50','label' =>'' ));?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>セット名</th>
                    <td><?php echo $this->form->text('MstFacilityItems.item_name',array('class' => 'txt search_canna' ,'maxlength' => '50', 'label' =>'') );?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->text('MstFacilityItems.item_code',array('class' =>'txt search_canna','maxlength' => '50', 'label' =>'' )); ?></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="search_btn"/>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>

    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th style="width: 20px;"></th>
                <th style="width: 135px;">セットID</th>
                <th>セット名</th>
                <th>規格</th>
                <th>製品番号</th>
            </tr>
            <?php $i = 0; foreach ($result as $item){ ?>
            <tr>
                <td class="center">
                    <input type="radio" name="data[MstFacilityItem][id]" class="validate[required]" id="item<?php echo $item['MstFacilityItem']['id']; ?>" value="<?php echo $item['MstFacilityItem']['id']; ?>" />
                </td>
                <td><?php echo h_out($item['MstFacilityItem']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($item['MstFacilityItem']['item_name']); ?></td>
                <td><?php echo h_out($item['MstFacilityItem']['standard']); ?></td>
                <td><?php echo h_out($item['MstFacilityItem']['item_code']); ?></td>
            </tr>
            <?php $i++; } ?>
            <?php if(isset($data['search']['is_search']) && count($result) == 0 ){ ?>
            <tr><td colspan="5" class="center">該当するデータはありません。</td></tr>
            <?php } ?>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="ButtonBox">
        <input type="button" class="btn btn11" id="add_btn"/>
        <?php if( count($result) > 0 ){ ?>
        <input type="button" class="btn btn9" id="edit_btn"/>
        <?php } ?>
    </div>
</form>