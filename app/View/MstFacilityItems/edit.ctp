<script type="text/javascript">
var webroot = "<?php echo $this->webroot; ?>";
    $(document).ready(function(){
        $('#floating-menu').css('display','none');
        
        $('#change_masterId').show();
  
        // 付替ボタン押下
        $('#change_masterId').click(function () {
            if( $('#master_id').val() == ''){
                alert('コスロスCDを入力してください');
                $("#master_id").focus();
                return false;
            }
            $("#item_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit/'+$('#master_id').val()).submit();
        });

        // 確定ボタン押下
        $("#edit_btn").click(function(){

            var SubmitFlg = true;
            if(!dateCheck($('#datepicker1').val())){
                alert('適用開始日が不正です');
                $('#datepicker1').focus();
                SubmitFlg = false;
            }
            if($('#datepicker2').val() != '' ){
                if(!dateCheck($('#datepicker2').val())){
                    alert('適用終了日が不正です');
                    $('#datepicker2').focus();
                    SubmitFlg = false;
                }
            }
            // 採用開始日 型チェック
            $('input[type=text].start_date').each(function(){
                if(!dateCheck($(this).val())){
                    alert('採用開始日が不正です');
                    $(this).focus();
                    SubmitFlg = false;
                }
            });
  
            // 採用中止日 型チェック
            $('input[type=text].end_date').each(function(){
                if($(this).val() != ''){
                    if(!dateCheck($(this).val())){
                        alert('採用中止日が不正です');
                        $(this).focus();
                        SubmitFlg = false;
                    }
                }
            });

            if(SubmitFlg){
                $("#item_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_result').submit();
            }
       });
       // 同種同効品ボタン押下
       $("#same_btn").click(function(){
           $("#item_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/same_index').submit();
       })
});

</script>

<script type="text/javascript" src="<?php echo $this->webroot; ?>js/facility_item.js"></script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a>Myマスタ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_search">Myマスタ一覧</a></li>
        <li>Myマスタ編集</li>
    </ul>
</div>

<?php echo $this->element('masters/facility_items/item_hiddenAjaxForm'); ?>
<div style="float:left;">
<h2 class="HeaddingLarge"><span>Myマスタ編集</span></h2>
<h2 class="HeaddingMid">登録済みMyマスタの商品情報を編集できます。</h2>
</div>
<div style="float:right;margin-top: 50px;margin-right:50px;">

<?php if(@$sameItemCount > 0){ ?>
<input type="button" class="same-button [p2]" id="same_btn" value="同種同効品有り"/>
<?php } ?>
  
</div>
<form class="validate_form" id="item_form"  method="post" style="clear:both;">
    <?php echo $this->form->input('MstFacilityItem.id',array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstFacilityItem.mst_facility_id',array('type'=>'hidden')); ?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <?php echo $this->form->input('MstFacilityItem.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
    
    <h3 class="conth3">Myマスタ登録商品の情報編集</h3>
    <div class="SearchBox">
            <?php echo $this->element('masters/facility_items/item_Table');?>
        </div>

    <h3 class="conth3">仕入設定</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>
                    <table>
                        <colgroup>
                            <col width="150"/>
                            <col width="150"/>
                            <col width="150"/>
                            <col width="150"/>
                        </colgroup>
                        <tr>
                           <td>仕入先</td>
                           <td>仕入価格</td>
                           <td>仕入単位</td>
                        </tr>
                        <tr>
                            <td><?php echo $this->form->input('MstTransactionConfig.partner_facility_id', array('options'=>$suppliers, 'empty'=>true, 'class' => 'txt r validate[required]','style'=>'width: 120px; height: 23px;')); ?></td>
                            <td><?php echo $this->form->input('MstTransactionConfig.transaction_price',array('type'=>'text' , 'class'=>'r validate[required] right')); ?></td>
                            <td><?php echo $this->form->input('MstTransactionConfig.mst_unit_name_id', array('options'=>$item_units, 'empty'=>true, 'class' => 'txt r validate[required]','style'=>'width: 120px; height: 23px;')); ?>
                            </td>
                        </tr>
                    </table>
                </th>
            </tr>
        </table>
    </div>
         <div class="ButtonBox">
             <p class="center">
                 <input type="button" class="common-button" id="btn_back" value="戻る"/>
                 <input type="button" class="common-button [p2]" id="edit_btn" value="登録"/>
             </p>
         </div>
     </div>
</form>
</div><!--#content-wrapper-->