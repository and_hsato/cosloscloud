<script type="text/javascript">
$(function(){
    //登録ボタン押下
    $("#btn_Result").click(function(){
        if($("#result").val() === ''){
            alert('ファイルが選択されてません');
            return false;
        }else{
            if(confirm('CSV登録を行います。\nよろしいですか？')){
                $("#CsvImport").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/import_csv_result');
                $("#CsvImport").attr('method', 'post');
                //登録中メッセージ追加
                $('#msg').text('登録処理中です。しばらくお待ちください。');
                //ボタン無効化
                $('#btn_Result').attr('disabled' , 'disabled');
                $('#download_btn').attr('disabled' , 'disabled');
                //日付項目のチェック削除
                $('#datepicker1').removeClass('validate[optional,custom[date]]');
                $('#datepicker2').removeClass('validate[optional,custom[date]]');
                //送信
                $("#CsvImport").submit();
            }
        }
    });
    //ダウンロードボタン押下
    $("#download_btn").click(function(){
        //必須を消す
        $('#result').removeClass('validate[required]');
        $('#CsvImport').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        //必須を戻す
        $('#result').addClass('validate[required]');
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>マスタＣＳＶ登録</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>マスタＣＳＶ登録</span></h2>
<h2 class="HeaddingMid">登録するマスタを選択してください</h2>
<div id="msg" class="err" style="font-size:20px"></div>

<form class="validate_form" method="post" action="" enctype="multipart/form-data" accept-charset="utf-8" id="CsvImport">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>登録マスタ</th>
                <td><?php echo $this->form->radio('csv.type' , array(1=>'') , array('checked'=>true , 'hiddenField'=>false)); ?>施設採用品</td>
            </tr>
            <tr style="height:25px">
                <th></th>
                <td><?php echo $this->form->radio('csv.type' , array(2=>'') , array('hiddenField'=>false)); ?>センター定数設定</td>
            </tr>
            <tr style="height:25px">
                <th></th>
                <td><?php echo $this->form->radio('csv.type' , array(3=>'') , array('hiddenField'=>false)); ?>病院定数設定</td>
            </tr>
            <tr style="height:25px">
                <th></th>
                <td><?php echo $this->form->radio('csv.type' , array(4=>'') , array('hiddenField'=>false)); ?>仕入設定</td>
            </tr>
            <tr style="height:25px">
                <th></th>
                <td><?php echo $this->form->radio('csv.type' , array(5=>'') , array('hiddenField'=>false)); ?>売上設定</td>
            </tr>
            <tr>
                <th>CSVファイル</th>
                <td colspan="2">
                    <?php echo $this->form->file('result',array('label'=>false,'div'=>false,'class'=>'r text validate[required]','style'=>'width:250px;','value'=>'')); ?>
                </td>
            </tr>
        </table>
    </div>

    <h2 class="HeaddingMid">ダウンロード条件</h2>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('download.id' , array('options'=>$FacilityList , 'class'=>'txt' , 'id'=>'facility_id','empty'=>true )); ?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('download.internal_code' ,array('class'=>'txt') ) ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('download.item_code' , array('class'=>'txt'))?></td>
                <td></td>
                <th>開始日</th>
                <td>
                    <?php echo $this->form->input('download.start_date' , array('type'=>'text', 'class'=>'date validate[optional,custom[date]]' , 'id'=>'datepicker1'))?>
                    ～
                    <?php echo $this->form->input('download.end_date' , array('type'=>'text', 'class'=>'date validate[optional,custom[date]]', 'id'=>'datepicker2'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('download.item_name' , array('class'=>'txt search_canna'));?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('download.dealer_name' , array('class'=>'txt search_canna'))?></td>
                <td></td>
                <th>有効なデータのみ</th>
                <td><?php echo $this->form->checkbox('download.flg' , array('hiddenField'=>false)); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('download.standard' , array('class'=>'txt search_canna'))?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('download.jan_code' , array('class'=>'txt'))?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn29 [p2]" id="btn_Result" />
        <input type="button" class="btn btn34" id="download_btn" />
    </div>
</form>
