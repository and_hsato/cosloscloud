<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_items">セット品一覧</a></li>
        <li class="pankuzu">セット品編集確認</li>
        <li>セット品編集結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>セット品編集結果</span></h2>
<?php if(count($result)>0){ ?>
<div class="Mes01">以下の内容を設定しました</div>
<?php } else { ?>
<div class="Mes01">セット品の登録に失敗しました。</div>
<?php } ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>セットID</th>
                <td><?php echo $this->form->text('MstSetItems.set_id',array('class'=>'lbl','style'=>'width:120px' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>規格</th>
                <td><?php echo $this->form->text('MstSetItems.standard',array('class'=>'lbl','style'=>'width:120px', 'readonly'=>'readonly')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>セット名</th>
                <td><?php echo $this->form->text('MstSetItems.set_name',array('class'=>'lbl','style'=>'width:120px', 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstSetItems.item_code',array('class'=>'lbl','style'=>'width:120px', 'readonly'=>'readonly')); ?></td>
                <td></td>
            </tr>
        </table>

    </div>
    <div align="right" id="pageTop" ></div>

        <h2 class="HeaddingSmall">構成品一覧</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th class="col20">商品ID</th>
                    <th class="col30">商品名</th>
                    <th class="col30">製品番号</th>
                    <th class="col20">包装単位</th>
                </tr>
                <tr>
                    <th class="col20"></th>
                    <th class="col30">規格</th>
                    <th class="col30">販売元</th>
                    <th class="col20">数量</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02">
            <?php
              $i = 0;
              foreach($result as $item){
                  $class =($i % 2 == 0)?'':'odd';
              ?>
                <tr class="<?php echo $class?>">
                    <td class="col20"><?php echo h_out($item['MstSetItems']['internal_code'],'center'); ?></td>
                    <td class="col30 rowspan-border1"><?php echo h_out($item['MstSetItems']['item_name']); ?></td>
                    <td class="col30 rowspan-border1"><?php echo h_out($item['MstSetItems']['item_code']); ?></td>
                    <td class="col20 rowspan-border1"><?php echo h_out($item['MstSetItems']['unit_name']); ?></td>
                </tr>
                <tr class="<?php echo $class?>">
                    <td class="col20 rowspan-border2"></td>
                    <td class="col30 rowspan-border2"><?php echo h_out($item['MstSetItems']['standard']); ?></td>
                    <td class="col30 rowspan-border2"><?php echo h_out($item['MstSetItems']['dealer_name']); ?></td>
                    <td class="col20 rowspan-border2">
                      <?php echo $this->form->text('MstSetItems.quantity.'.$i ,array('class'=>'lbl num','readonly'=>'readonly','style'=>'width:90%')); ?>
                    </td>
                </tr>
            <?php
                $i++;
            } ?>
            </table>
        </div>
        <div align="right" id="pageDow" ></div>
