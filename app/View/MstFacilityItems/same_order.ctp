<script>
$(function($){
    //見積りボタン押下
    $("#bid_btn").click(function(){
        if($('input[type=checkbox].chkEdi:checked').length > 0){
            $("#same_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/same_result/').submit();
        } else {
            alert('仕入れ先は必ず一つは選択してください');
        }
    });
    
    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
    });

    //チェックボックス制御 (業者)
    $('.checkAllEdi').click(function(){
        $('input[type=checkbox].chkEdi').attr('checked', $(this).attr('checked'));
    });

    $(".img").error(function(){
        $(this).attr('src', '<?php echo $this->webroot; ?>/images/item/no-image.jpg');
    });
    
});
</script>

<div id="content-wrapper">

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">ＴＯＰ</a></li>
        <li><a href="">Myマスタ一覧</a></li>
        <li class="pankuzu">Myマスタ編集</li>
        <li class="pankuzu">同種同効品一覧</li>
        <li>見積内容確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>見積依頼内容確認</span></h2>
<h2 class="HeaddingMid">登録済みの仕入先へ、選択した同種同効品の一括見積が行えます。</h2>

  <form class="validate_form" id="same_form" method="post">
    <?php echo $this->element('masters/grandmaster/rfp_item_table', array('hiddenCheckBox' => true, 'chkAllClass' => 'checkAll', 'chkClass' => 'chk')) ?>
    <br/>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>締切日</th>
                <td><?php echo($this->form->text('limit_date', array('class' => 'r date validate[required,custom[date]]', 'maxlength' => '10', 'id' => 'limit_date', 'label' => ''))); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo($this->form->text('recital', array('class' => 'txt', 'label' => '', 'maxlength'  => 200))); ?></td>
            </tr>
        </table>
    </div>
    <div class="results">
        <table class="conciergetable table-even">
            <colgroup>
                <col width="20px"/>
                <col />
                <col />
            </colgroup>
            <tr style="background:#f7f7f7">
                <td colspan="3" style="font-size:14px">仕入先一覧 ※以下仕入先へ一括見積依頼を行います。</td>
            </tr>
            <tr>
                <td align="center"><input type="checkbox" class="checkAllEdi" ></td>
                <th>施設コード</th>
                <th>施設名</th>
            </tr>
         <?php foreach ($edis as $d) { $edi = $d['MstFacility']; ?>
            <tr>
                <td align="center"><input type="checkbox" class="chkEdi" name="data[edi_ids][]" value="<?php echo $edi['id'] ?>" ></td>
                <td><?php echo $edi['facility_code'] ?></td>
                <td><?php echo $edi['facility_name'] ?></td>
            </tr>
         <?php } ?>
        </table>
    </div>
    <br />
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_back" value="戻る"/>
        <input type="button" id="bid_btn" class="common-button [p2]" value="見積依頼をする" />
    </div>
  </form>
</div><!--#content-wrapper-->
