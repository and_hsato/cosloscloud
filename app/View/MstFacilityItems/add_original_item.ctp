<script type="text/javascript">
  var webroot  = "<?php echo $this->webroot; ?>";
  var prefix   = "<?php echo Configure::read('ItemType.originalItem');?>";
  var lowlevel = "<?php echo Configure::read('ItemType.lowlevelItem');?>";
  
$(function(){
    $('#spare_table').hide();
    $("#spare_header").click(function () {
        $('#spare_table').slideToggle('slow');
    });

  
    $("#add_btn").click(function(){
        var SubmitFlg = true;
        if(!dateCheck($('#datepicker1').val())){
            alert('適用開始日が不正です');
            $('#datepicker1').focus();
            SubmitFlg = false;
        }
        if($('#datepicker2').val() != '' ){ 
            if(!dateCheck($('#datepicker2').val())){
                alert('適用終了日が不正です');
                $('#datepicker2').focus();
                SubmitFlg = false;
            }
        }

        // 予備キーチェック
        // class に spare_key と r を持っている場合、空の登録はNGにする。
        $('.sparekey').each(function(){
            if($(this).hasClass('r')){
                if($(this).val() == '' ){
                    $('#spare_table').show();
                    alert('必須項目です。');
                    $(this).focus();
                    SubmitFlg = false;
                    return false;
                }
            }
        });
    
        if(SubmitFlg){
            if( $('#MstFacilityItemSealissueMedicalcode').val() == 0 &&
              ( $('#insurance_claim_type_sel').val() != ''  &&
                $('#insurance_claim_type_sel').val() != 6 ) ) {
                if(window.confirm("医事シールが印字されない設定ですがよろしいですか？")){
                    $("#add_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_confirm').submit();
                }
            }else{
                $("#add_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_confirm').submit();
            }
        }
    });

});

</script>

<script type="text/javascript" src="<?php echo $this->webroot; ?>js/facility_item.js"></script>

<style>
<!--
div#search_dealer_form {
  z-index: 9999;
  background-color: #808080;
  position: absolute;
  height: auto;
  width: auto;
  padding: 4px;
  color: #fff;
  left: 400px;
  top: 200px;
  width: 350px;
}

div#search_dealer_form ul li {
  margin-top: 2px;
  margin-left: 0px;
}

div#search_jmdn_form {
  z-index: 9999;
  background-color: black;
  position: absolute;
  height: auto;
  width: auto;
  padding: 4px;
  color: #fff;
  left: 400px;
  top: 200px;
  width: 350px;
}

div#search_jmdn_form ul li {
  margin-top: 2px;
  margin-left: 0px;
}

div#search_icc_form {
  z-index: 9999;
  background-color: red;
  position: absolute;
  height: auto;
  width: auto;
  padding: 4px;
  color: #fff;
  left: 400px;
  top: 200px;
  width: 350px;
}

div#search_icc_form ul li {
  margin-top: 2px;
  margin-left: 0px;
}

.floatClose {
  margin-left: 10px;
}
-->
</style>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">Myマスタ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">Myマスタ新規登録</a></li>
        <li>独自品新規登録</li>
    </ul>
</div>
<?php echo $this->element('masters/facility_items/item_hiddenAjaxForm'); ?>

<h2 class="HeaddingLarge"><span>独自品新規登録</span></h2>
<h2 class="HeaddingMid">施設の独自商品をMyマスタへ新規登録できます。</h2>
<form class="validate_form" id="add_form"  method="post">
    <?php echo $this->form->input('MstFacilityItem.isOriginal', array('type'=>'hidden', 'value'=>1)); ?>
    
    <h3 class="conth3">独自品の情報入力</h3>
    <div class="SearchBox">
            <?php echo $this->element('masters/facility_items/item_Table'); ?>
        </div>
        
    <h3 class="conth3">仕入設定</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>
                    <table>
                        <colgroup>
                            <col width="150"/>
                            <col width="150"/>
                            <col width="150"/>
                            <col width="150"/>
                        </colgroup>
                        <tr>
                           <td>仕入先</td>
                           <td>仕入価格</td>
                           <td>仕入単位</td>
                        </tr>
                        <tr>
                            <td><?php echo $this->form->input('MstTransactionConfig.partner_facility_id', array('options'=>$suppliers, 'empty'=>true, 'class' => 'txt r validate[required]','style'=>'width: 120px; height: 23px;','div'=>false,'label'=>false)); ?></td>
                            <td><?php echo $this->form->input('MstTransactionConfig.transaction_price',array('type'=>'text' , 'class'=>'r validate[required,custom[onlyDecimal]] right', 'id'=>'TransactionPrice')); ?></td>
                            <td><?php echo $this->form->input('MstFacilityItem.mst_unit_name_id', array('options'=>$item_units, 'empty'=>true ,'class' => 'r validate[required]', 'style'=>'width: 80px;')); ?></td>
                        </tr>
                    </table>
                </th>
            </tr>
        </table>
    </div>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="common-button" id="btn_back" value="戻る"/>
                <input type="button" class="common-button [p2]" id="add_btn" value="確認"/>
            </p>
        </div>
    </div>
</form>
</div><!--#content-wrapper-->