<script type="text/javascript">
$(document).ready(function(){
    $('#floating-menu').css('display','none');
    
    $('#btn_new').click(function(){
        $('#result_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search#search_result').submit();
    });
  
    $('#btn_list').click(function(){
        $('#result_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_search#search_result').submit();
    });
});

</script>

<div id="content-wrapper">
    <div id="TopicPath">
        <ul>
            <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
            <li><a href="<?php echo $this->webroot; ?>masters">Myマスタ</a></li>
            <li>Myマスタ新規登録(一括採用)</li>
        </ul>
    </div>
    <form class="validate_form" id="result_form" method="post">
        <div class="results">
        <h2 class="HeaddingLarge"><span>Myマスタ新規登録(一括採用)</span></h2>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <colgroup>
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th style="width: 135px;" >コスロスCD</th>
                    <th style="width: 135px;" >JANコード</th>
                    <th>商品名</th>
                    <th>規格</th>
                    <th>製品番号</th>
                    <th>販売元</th>
                    <th>単位</th>
                </tr>
                <?php foreach ($tmp as $tp){ ?>
                <tr>
                    <td><?php echo h_out($tp['MstFacilityItem']['master_id'],'center'); ?></td>
                    <td><?php echo h_out($tp['MstFacilityItem']['jan_code'],'center'); ?></td>
                    <td><?php echo h_out($tp['MstFacilityItem']['item_name'],'center'); ?></td>
                    <td><?php echo h_out($tp['MstFacilityItem']['standard'],'center'); ?></td>
                    <td><?php echo h_out($tp['MstFacilityItem']['item_code'],'center'); ?></td>
                    <td><?php echo h_out($tp['MstDealer']['dealer_name'],'center'); ?></td>
                    <td><?php echo h_out($tp['MstGrandmaster']['unit_name'],'center'); ?></td>
                </tr>
                <?php } ?>
            </table>
        </div>
      </div>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="common-button" id="btn_new" value="続けて登録"/>
                <input type="button" class="common-button" id="btn_list" value="Myマスタ一覧"/>
            </p>
        </div>
    </form>
</div><!--.results -->
