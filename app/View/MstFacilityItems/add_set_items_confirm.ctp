<script type="text/javascript">

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    //確定ボタン押下
    $('#add_set_items_result').click(function(){
        //選択チェック
        var cnt = 0;
        var err = 0;
        $("#confirmForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                // 次のテキストボックス要素を検索
                var index = $('input[type=checkbox].checkAllTarget').index(this);
                var quantity = $('input[type=text].quantity:eq(' + index + ')');

                // 入力内容をチェック
                if(!quantity.val().match(/[0-9]+/)){
                    alert('数量を正しく入力してください。');
                    quantity.focus();
                    err = 1;
                    return false;
                }
                cnt++; // チェックの入っている件数をカウントアップ
            }
        });
        if(err === 1){
           return false;
        }
        if(cnt === 0){
            alert('セットに追加する商品を選択してください');
            return false;
        }
        if($('#set_id').val() == ''){
            alert('セットIDを入力してください');
            $('#set_id').focus();
            return false;
        }
        if($('#set_name').val() == ''){
            alert('セット名を入力してください');
            $('#set_name').focus();
            return false;
        }
        $("#confirmForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_set_items_result').submit();
    });

});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_items">セット品一覧</a></li>
        <li class="pankuzu">セット品編集</li>
        <li>セット品編集確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>採用品選択</span></h2>
<h2 class="HeaddingMid">検索条件</h2>

<form class="validate_form" id="confirmForm" method="post">
<?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>セットID</th>
                <td><?php echo $this->form->text('MstSetItems.set_id',array('class'=>'txt r','style'=>'width:120px' , 'id' => 'set_id' , 'maxlength'=>'20')); ?></td>
                <td></td>
                <th>規格</th>
                <td><?php echo $this->form->text('MstSetItems.standard',array('class'=>'txt','style'=>'width:120px' , 'maxlength'=>'100')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>セット名</th>
                <td><?php echo $this->form->text('MstSetItems.set_name',array('class'=>'txt r','style'=>'width:120px' , 'id'=>'set_name' , 'maxlength'=>'100')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstSetItems.item_code',array('class'=>'txt','style'=>'width:120px' , 'maxlength'=>'100')); ?></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div align="right" id="pageTop" ></div>
        <h2 class="HeaddingSmall">構成品一覧</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th width="20" rowspan="2"><input type="checkbox" class="checkAll" checked/></th>
                    <th class="col20" >商品ID</th>
                    <th class="col30">商品名</th>
                    <th class="col30">製品番号</th>
                    <th class="col20">包装単位</th>
                </tr>
                <tr>
                    <th></th>
                    <th class="col30">規格</th>
                    <th class="col30">販売元</th>
                    <th class="col20">数量</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02">

            <?php
              $i = 0;
              foreach($result as $item){
              $class =($i % 2 == 0)?'':'odd';
            ?>
                <tr class='<?php echo $class?>'>
                    <td width="20" rowspan="2" class="center"><input type="checkbox" name="data[MstSetItems][id][<?php echo $i ?>]"  class="chk checkAllTarget" checked value="<?php echo $item['MstSetItems']['id']; ?>"/></td>
                    <td class="col20" ><?php echo h_out($item['MstSetItems']['internal_code'],'center'); ?></td>
                    <td class="col30 rowspan-border1"><?php echo h_out($item['MstSetItems']['item_name']); ?></td>
                    <td class="col30 rowspan-border1"><?php echo h_out($item['MstSetItems']['item_code']); ?></td>
                    <td class="col20 rowspan-border1"><?php echo h_out($item['MstSetItems']['unit_name']) ; ?></td>
                </tr>
                <tr class='<?php echo $class?>'>
                    <td></td>
                    <td class="col30 rowspan-border2"><?php echo h_out($item['MstSetItems']['standard']); ?></td>
                    <td class="col30 rowspan-border2"><?php echo h_out($item['MstSetItems']['dealer_name']); ?></td>
                    <td class="col20 rowspan-border2">
                      <?php echo $this->form->text('MstSetItems.quantity.'.$i ,array('class'=>'txt r quantity num','maxlength'=>'32')); ?>
                    </td>

                </tr>
            <?php
            $i++;
            } ?>
            </table>
        </div>

        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn2 fix_btn" id="add_set_items_result" />
            </p>
        </div>
</form>
<div align="right" id="pageDow" ></div>
