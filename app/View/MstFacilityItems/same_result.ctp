<script type="text/javascript">
$(function(){
    $("#top_btn").click(function(){
         $("#same_form").attr('action', '<?php echo $this->webroot; ?>').submit();
    });
});
</script>
<div id="content-wrapper">
<form id="same_form" method="post">
<h2 class="confilm_message">
  同種同効品の見積依頼を仕入先へ送信しました。<br />
  見積依頼の状況は
  <?php echo $this->Html->link( '「見積確認」', array('controller' => 'Bid', 'action' => 'decision_search')) ?>
  からご確認いただけます。
</h2>
<br />
<div class="ButtonBox">
        <input type="button" class="common-button" id="top_btn" value="TOPページへ戻る"/>
</div>
</form>

</div><!--#content-wrapper-->
