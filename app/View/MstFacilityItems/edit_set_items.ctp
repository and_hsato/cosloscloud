<script type="text/javascript">

$(document).ready(function(){
    //確定ボタン押下
    $('#edit_set_items_result').click(function(){
        if($('#setid').val() == ''){
            alert('セットIDを入力してください');
            return false;
        }
        if($('#setname').val() == ''){
            alert('セット名を入力してください');
            return false;
        }
        $("#confirmForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_set_items_result').submit();
    });

});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_items">セット品一覧</a></li>
        <li>セット品編集確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>セット品確認</span></h2>
<h2 class="HeaddingMid">セット品の構成品を確認します。</h2>

<form id="confirmForm" class="validate_form" method="post">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>セットID</th>
                <td><?php echo $this->form->text('MstSetItems.set_id',array('class'=>'txt r','style'=>'width:120px' ,'value'=>$result[0]['MstSetItemHeader']['internal_code'] ,'id' => 'set_id' , 'maxlength'=>'20')); ?></td>
                <td></td>
                <th>規格</th>
                <td><?php echo $this->form->text('MstSetItems.standard',array('class'=>'txt','style'=>'width:120px','value'=>$result[0]['MstSetItemHeader']['standard'] , 'maxlength'=>'100')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>セット名</th>
                <td><?php echo $this->form->text('MstSetItems.set_name',array('class'=>'txt r','style'=>'width:120px' ,'value'=>$result[0]['MstSetItemHeader']['item_name'], 'id'=>'set_name' , 'maxlength'=>'100')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstSetItems.item_code',array('class'=>'txt','style'=>'width:120px','value'=>$result[0]['MstSetItemHeader']['item_code'] , 'maxlength'=>'100')); ?></td>
                <td></td>
            </tr>
        </table>

    </div>
    <div align="right" id="pageTop" ></div>

        <h2 class="HeaddingSmall">構成品一覧</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th width="20" rowspan="2"></th>
                    <th class="col20" >商品ID</th>
                    <th class="col30">商品名</th>
                    <th class="col30">製品番号</th>
                    <th class="col20">包装単位</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02 table-even2">
            <?php foreach($result as $item){?>
                <tr>
                    <td width="20" rowspan="2"></td>
                    <td class="col20" ><?php echo h_out($item['MstSetItems']['internal_code'],'center'); ?></td>
                    <td class="col30 rowspan-border1"><?php echo h_out($item['MstSetItems']['item_name']); ?></td>
                    <td class="col30 rowspan-border1"><?php echo h_out($item['MstSetItems']['item_code']); ?></td>
                    <td class="col20 rowspan-border1"><?php echo h_out($item['MstSetItems']['unit_name']); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="rowspan-border2"><?php echo h_out($item['MstSetItems']['standard']); ?></td>
                    <td class="rowspan-border2"><?php echo h_out($item['MstSetItems']['dealer_name']); ?></td>
                    <td class="rowspan-border2"><?php echo h_out($item['MstSetItems']['quantity']); ?></td>
                </tr>
            <?php } ?>
            </table>
        </div>

        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn2 fix_btn" value="" id="edit_set_items_result" />
            </p>
        </div>

<input type='hidden' value=<?php echo $this->request->data['MstFacilityItem']['id'] ?> name="data[MstFacilityItem][id]" >
</form>
<div align="right" id="pageDow" ></div>
