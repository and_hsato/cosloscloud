<script type="text/javascript">
$(function(){
    //検索ボタン押下
    $("#search_btn").click(function(){
        validateDetachSubmit($('#search_form') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_search#search_result' );
    });
    //編集ボタン押下
    $("#edit_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit').submit();
    });
});

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">Myマスタ</a></li>
        <li>Myマスタ一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>Myマスタ一覧</span></h2>
<h2 class="HeaddingMid">Myマスタ一覧から編集対象の商品を検索できます。</h2>
<form class="validate_form search_form" id="search_form" method="post">
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <input type="hidden" name="data[hide_action]" id="hide_action" value="1" />
    <h3 class="conth3">Myマスタ登録商品を検索</h3>
    <div class="SearchBox">
        <div class="ColumnLeft">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>商品ID</th>
                    <td><?php echo $this->form->text('MstFacilityItem.internal_code' ,array('class' => 'txt search_internal_code','maxlength' => '50' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->text('MstFacilityItem.item_code',array('class' => 'txt search_upper','maxlength' => '50','label' =>'' ));?></td>
                    <td></td>
                    <th>大分類</th>
                    <td><?php echo $this->form->input('MstFacilityItem.item_category1', array('options'=>$item_category1 , 'class'=>'txt' , 'id'=>'item_category1' , 'empty'=>true)); ?></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->text('MstFacilityItem.item_name',array('class' => 'txt','maxlength' => '50', 'label' =>'') );?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->text('MstFacilityItem.dealer_code',array('class' =>'txt','maxlength' => '50', 'label' =>'' )); ?></td>
                    <td></td>
                    <th>中分類</th>
                    <td><?php echo $this->form->input('MstFacilityItem.item_category2', array('options'=>$item_category2 , 'class'=>'txt' , 'id'=>'item_category2' , 'empty'=>true)); ?></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->text('MstFacilityItem.standard',array('class' => 'txt search_canna','maxlength' => '50' ,'label' =>'') ); ?></td>
                    <td></td>
                    <th>JANコード</th>
                    <td><?php echo $this->form->text('MstFacilityItem.jan_code' ,array('class' => 'txt','maxlength' => '50' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th>小分類</th>
                    <td><?php echo $this->form->input('MstFacilityItem.item_category3', array('options'=>$item_category3 , 'class'=>'txt' , 'id'=>'item_category3' , 'empty'=>true)); ?></td>
                </tr>
                <tr>
                    <th></th>
                    <td colspan="7">
                        <?php echo $this->form->input('MstFacilityItem.is_deleted',array('label'=>'削除は除外' , 'hiddenField'=>false)); ?>削除は除外
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="search_btn" value="検索"/>
    </div>
    
    <div class="results">
        <a name="search_result"></a>
        <h2 class="HeaddingSmall">検索結果</h2>

        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>(isset($fitems) && count($fitems) != 0) ? count($fitems):0)); ?>
             </span>
             <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th style="width:25px;"></th>
                    <th style="width:100px;">商品ID</th>
                    <th>商品名</th>
                    <th>規格</th>
                    <th>製品番号</th>
                    <th>販売元</th>
                    <th style="width:130px;">ＪＡＮコード</th>
                    <th style="width: 50px;">画像</th>
                </tr>
                <?php $i = 0; foreach ($fitems as $item){ ?>
                <?php if($item['MstFacilityItem']['is_deleted'] == ''){$class='';}else{$class='tr-deleted';};?>
                <tr class="<?php echo $class; ?>">
                    <td>
                        <input type="radio" name="data[MstFacilityItem][id]" style="width:25px;" class="validate[required]" id="item<?php echo $item['MstFacilityItem']['id']; ?>" value="<?php echo $item['MstFacilityItem']['id']; ?>" />
                    </td>
                    <td style="width:100px;"><?php echo h_out($item['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($item['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($item['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($item['MstFacilityItem']['item_code']); ?></td>
                    <td><?php echo h_out($item['MstDealer']['dealer_name']); ?></td>
                    <td style="width:130px;"><?php echo h_out($item['MstFacilityItem']['jan_code']); ?></td>
                    <td style="width: 50px;" align="center"><?php echo $this->Common->toImagePopupHtmlForMasterItem($item['MstFacilityItem'], '表示') ?></td>
                </tr>
                <?php $i++; } ?>
                <?php if(isset($data['search']['is_search']) && count($fitems) == 0 ) { ?>
                <tr><td colspan="8" class="center">該当するデータはありません。</td></tr>
                <?php } ?>
            </table>
        </div>
    </div>
    
    <div class="ButtonBox">
        <?php if(count($fitems) > 0){ ?>
        <input type="button" class="common-button" id="edit_btn" value="選択"/>
        <?php } ?>
    </div>
</form>
</div><!--#content-wrapper-->