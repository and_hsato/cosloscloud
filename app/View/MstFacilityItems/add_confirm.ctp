<script type="text/javascript">
$(document).ready(function(){
    $('.date').datepicker('destroy');
    $('select , input[type=text]').attr("readonly","readonly").removeClass('r txt date' ).addClass("lbl");
    $('input[type=button]').css("display","none");
    $('#submit_btn').css("display","inline");
    $('#btn_back').css("display","inline");

    $('td').addClass('border-none');
    $('#floating-menu').css('display','none');

    $('select').each(function(){
        $(this).after('<input type=hidden name=' + $(this).attr('name') + ' value=' + $(this).val() + ' >');
        $(this).attr('disabled' , 'disabled');
    });
    $('input[type=checkbox]').each(function(){
        if($(this).attr('checked')){
            $(this).after('<input type=hidden name=' + $(this).attr('name') + ' value=' + $(this).val() + ' >');
        }
        $(this).attr('disabled' , 'disabled');
    });
    $("#submit_btn").click(function(){
        $(window).unbind('beforeunload');
        $("#confirm_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_result').submit();
    });
  
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
  
});
</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">Myマスタ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">Myマスタ新規登録</a></li>
        <li class="pankuzu">Myマスタ新規登録情報入力</li>
        <li>Myマスタ新規登録情報確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>Myマスタ新規登録情報確認</span></h2>
<h2 class="HeaddingMid">Myマスタへ新規登録する内容を確認してください</h2>

<form class="validate_form" id="confirm_form" method="post">

    <h3 class="conth3">登録商品情報</h3>
    <div class="SearchBox">
        <?php echo $this->form->input('MstFacilityItem.isOriginal', array('type'=>'hidden')); ?>
        <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
            <?php echo $this->element('masters/facility_items/item_Table');?>
    </div>
    <h3 class="conth3">仕入設定</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>
                    <table>
                        <colgroup>
                            <col width="150"/>
                            <col width="150"/>
                            <col width="150"/>
                        </colgroup>
                        <tr>
                           <td>仕入先</td>
                           <td>仕入価格</td>
                           <td>仕入単位</td>
                        </tr>
                        <tr>
                            <td><?php echo $this->form->input('MstTransactionConfig.partner_facility_id', array('options'=>$suppliers, 'empty'=>true, 'class' => 'txt r validate[required]','style'=>'width: 120px; height: 23px;','div'=>false,'label'=>false)); ?></td>
                            <td><?php echo $this->form->input('MstTransactionConfig.transaction_price',array('type'=>'text' , 'class'=>'r validate[required] right')); ?></td>
                            <td><?php echo $this->form->input('MstFacilityItem.mst_unit_name_id', array('options'=>$item_units, 'empty'=>true ,'class' => 'r validate[required]', 'style'=>'width: 80px;')); ?>
                                <?php echo $this->form->input('MstFacilityItem.unit_name' , array('type'=>'hidden')); ?>
                                <?php echo $this->form->input('MstFacilityItem.per_unit', array('type'=>'hidden','value'=>1)); ?>
                                <?php echo $this->form->input('MstFacilityItem.converted_num', array('type'=>'hidden','value'=>1)); ?>
                            </td>
                        </tr>
                    </table>
                </th>
            </tr>
        </table>
    </div>
      
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="common-button" id="btn_back" value="戻る"/>
            <input type="button" class="common-button [p2]"  id="submit_btn" value="登録"/>
        </p>
    </div>
</form>
</div><!--#content-wrapper-->