<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/mc_search">医事コードメンテ&nbsp;施設採用品一覧</a></li>
        <li class="pankuzu">医事コード編集</li>
        <li>医事コード編集結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>医事コード編集結果</span></h2>
<div class="Mes01">医事コードの設定を行いました</div>
<h2 class="HeaddingSmall">施設採用品情報</h2>
<form>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>得意先</th>
                <td><?php echo $this->form->input('MstFacilityItem.customer_name' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_code' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_name' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstDealer.dealer_name' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('MstFacilityItem.standard' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('MstFacilityItem.jan_code' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
            </tr>
            <tr>
                <th>保険請求</th>
                <td><?php echo $this->form->input('MstFacilityItem.insurance_claim_department_name' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
                <td></td>
                <th>生物由来</th>
                <td><?php echo $this->form->input('MstFacilityItem.biogenous_type_name' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
                <td></td>
                <th>償還価格</th>
                <td><?php echo $this->form->input('MstFacilityItem.refund_price' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
            </tr>
            <tr>
                <th>保険請求名</th>
                <td colspan="7"><?php echo $this->form->input('MstFacilityItem.insurance_claim_name_s' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
            </tr>
            <tr>
                <th>医事コード</th>
                <td><?php echo $this->form->input('MstFacilityItem.medical_code' , array('type'=>'text' , 'readonly'=>true , 'class'=>'lbl'));?></td>
            </tr>
        </table>
    </div>
</form>
