<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_items">セット品一覧</a></li>
        <li class="pankuzu">セット品編集確認</li>
        <li>セット品編集結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>セット品編集結果</span></h2>
<h2 class="HeaddingMid">以下の構成品でセット品を構成しました。</h2>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>セットID</th>
                <td><?php echo $this->form->text('MstSetItems.set_id',array('class'=>'lbl','style'=>'width:120px' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>規格</th>
                <td><?php echo $this->form->text('MstSetItems.standard',array('class'=>'lbl','style'=>'width:120px', 'readonly'=>'readonly')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>セット名</th>
                <td><?php echo $this->form->text('MstSetItems.set_name',array('class'=>'lbl','style'=>'width:120px', 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstSetItems.item_code',array('class'=>'lbl','style'=>'width:120px', 'readonly'=>'readonly')); ?></td>
                <td></td>
            </tr>
        </table>

    </div>
    <div align="right" id="pageTop" ></div>

        <h2 class="HeaddingSmall">構成品一覧</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th class="col20">商品ID</th>
                    <th class="col30">商品名</th>
                    <th class="col30">製品番号</th>
                    <th class="col20">包装単位</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02 table-even2" >
            <?php  $i = 0; foreach($result as $item){ ?>
                <tr>
                    <td class="col20" ><?php echo h_out($item['MstSetItems']['internal_code'],'center'); ?></td>
                    <td class="col30 rowspan-border1"><?php echo h_out($item['MstSetItems']['item_name']); ?></td>
                    <td class="col30 rowspan-border1"><?php echo h_out($item['MstSetItems']['item_code']); ?></td>
                    <td class="col20 rowspan-border1"><?php echo h_out($item['MstSetItems']['unit_name']); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="rowspan-border2"><?php echo h_out($item['MstSetItems']['standard']); ?></td>
                    <td class="rowspan-border2"><?php echo h_out($item['MstSetItems']['dealer_name']); ?></td>
                    <td class="rowspan-border2"><?php echo h_out($item['MstSetItems']['quantity']); ?></td>
                </tr>
            <?php  } ?>
            </table>
        </div>
<div align="right" id="pageDow" ></div>
