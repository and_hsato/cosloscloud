<script type="text/javascript">
$(function(){
    // 検索ボタン押下
    $("#search_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search#search_result').submit();
    });
  
    // 独自採用品登録ボタン押下
    $("#add_own_items_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_original_item').submit();
    });
    
    //「Myマスタへ登録」ボタン押下
    $("#add_btn").click(function(){    
        if( $('input.chk:checked').length > 1 ) {
              alert('複数選択されています。');
        } else if( $('input.chk:checked').length > 0 ) {
            if($('input.chk:checked').next().val() != ''){
                if(window.confirm("すでに登録されています。\n処理を続行してもよろしいですか？")){
                    $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add').submit();
                }else{
                    return false;
                }
            }else{
                $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add').submit();
            }
        }else{
            alert('採用品が選択されていません。');
        }
    });
    
    //「複数選択」ボタン押下
    $("#adds_btn").click(function(){
        if( $('input.chk:checked').length > 0 ) {
            if($('input.chk:checked').next().val() != ''){
                if(window.confirm("すでに登録されています。\n処理を続行してもよろしいですか？")){
                    $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/adds_result').submit();
                }else{
                    return false;
                }
            }else{
                $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/adds_result').submit();
            }
        }else{
            alert('選択されていません。');
        }
    });
    
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">Myマスタ</a></li>
        <li>Myマスタ新規登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>Myマスタ新規登録</span></h2>
<h2 class="HeaddingMid">Myマスタへ登録する商品をコスロスマスタ上で検索してください</h2>
<form class="validate_form search_form" id="search_form" method="post">
    <?php echo $this->form->input('search.is_search' , array('type'=>'hidden')); ?>
        <h3 class="conth3">登録する商品を検索</h3>
    <div class="SearchBox">
        <div class="ColumnLeft">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col /> 
                    <col width="20" />
                    <col />
                    <col /> 
                </colgroup>
                <tr>
                    <th>コスロスCD</th>
                    <td><?php echo $this->form->text('MstGrandmaster.master_id' ,array('class' => 'txt','maxlength' => '50' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th>JANコード</th>
                    <td><?php echo $this->form->text('MstGrandmaster.jan_code' ,array('class' => 'txt','maxlength' => '50' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th></th>
                    <!-- <td><?php echo $this->form->input('MstGrandmaster.item_category1', array('options'=>$item_category1 , 'class'=>'txt' , 'id'=>'item_category1' , 'empty'=>true)); ?></td> -->
                    <td></td>
                    <th></th>
                    <td><?php echo $this->form->input('MstGrandmaster.isAccepted' , array('type'=>'checkbox' , 'hiddenField'=>false))?>採用済は表示しない</td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->text('MstGrandmaster.item_name',array('class' => 'txt','maxlength' => '50', 'label' =>''));?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->text('MstGrandmaster.item_code',array('class' => 'txt search_upper ','maxlength' => '50','label' =>'' ));?></td>
                    <td></td>
                    <th></th>
                    <td></td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->text('MstGrandmaster.standard',array('class' => 'txt','maxlength' => '50' ,'label' =>'') ); ?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->text('MstGrandmaster.dealer_name',array('class' =>'txt','maxlength' => '50', 'label' =>'' )); ?></td>
                    <td></td>
                    <th></th>
                    <td></td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <p>※コスロスマスタ上にない商品は「独自登録」から登録していただけます。</p>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="search_btn" value="検索"/>
        <input type="button" id="add_own_items_btn" class="common-button [p2]" value="独自登録" />
    </div>
    <div class="results">
      <a name="search_result"></a>
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($gm_items))); ?>
             </span>
             <span class="BikouCopy"></span>
        </div>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th style="width: 20px;"></th>
                <th style="width: 135px;" >コスロスCD</th>
                <th style="width: 135px;" >JANコード</th>
                <th>商品名</th>
                <th>規格</th>
                <th>製品番号</th>
                <th>販売元</th>
                <th style="width: 50px;">単位</th>
                <th style="width: 50px;">採用済</th>
                <th style="width: 50px;">画像</th>
            </tr>
            <?php $i = 0; foreach ($gm_items as $gmi){ ?>
            <?php if($gmi['MstGrandmaster']['accepted'] == ''){$class='';}else{$class='tr-deleted';};?>
            <tr class="<?php echo $class; ?>">
                <td class="center">
                    <input name="data[MstGrandmaster][opt][<?php echo $i; ?>]"  type="checkbox" class="chk" value="<?php echo $gmi['MstGrandmaster']['master_id']; ?>"/>
                    <?php echo $this->form->text('MstGrandmaster.Accepted' , array('type'=>'hidden' , 'class'=>'hasAccepted' , 'value'=>$gmi['MstGrandmaster']['accepted'] )); ?>
                </td>                
                <td><?php echo h_out($gmi['MstGrandmaster']['master_id'],'center'); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['jan_code'],'center'); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['item_name']); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['standard']); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['item_code']); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['dealer_name']); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['unit_name'],'center'); ?></td>
                <td style="width: 50px;"><?php echo h_out($gmi['MstGrandmaster']['hasAccepted'],'center'); ?></td>
                <td style="width: 50px;" align="center"><?php echo $this->Common->toImagePopupHtmlForMasterItem($gmi['MstGrandmaster'], '表示') ?></td>
            </tr>
            <?php $i++; } ?>
            <?php if(isset($this->request->data['search']['is_search']) && count($gm_items) == 0){ ?>
            <tr><td colspan="8" class="center">該当するデータはありません。</td></tr>
            <?php } ?>
        </table>
        </div>
    </div><!-- results -->
    <?php if (count($gm_items) != 0){ ?>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="add_btn"  value="Myマスタへ登録"/>
        <input type="button" class="common-button" id="adds_btn" value="一括採用"/>
    </div>
<?php } ?>
</form>
</div><!--#content-wrapper-->
