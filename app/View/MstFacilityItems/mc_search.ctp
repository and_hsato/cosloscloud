<script type="text/javascript">
$(function(){
    //検索ボタン押下
    $("#mc_search_btn").click(function(){
        $("#mc_search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mc_search').submit();
    });
  
    //編集ボタン押下
    $("#edit_mc_btn").click(function(){
        if( $('input.check:checked').length > 0 ) { 
            $("#mc_search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mc_edit').submit();
        }else{
            alert('採用品が選択されていません。');
        }
    });
});
//-->
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>医事コードメンテ　施設採用品一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>医事コードメンテ　施設採用品一覧</span></h2>
<h2 class="HeaddingMid">医事コードを編集する施設採用品を選択してください</h2>
<form class="validate_form search_form" id="mc_search_form" method="post">
    <?php echo $this->form->input('search.is_search' , array('type'=>'hidden'));?>
    <div class="SearchBox">
        <div class="ColumnLeft">
            <table class="FormStyleTable">
                <tr>
                    <th>得意先</th>
                    <td colspan="4">
                        <?php echo $this->form->input('MstFacilityItem.hospitalName',array('id'=>'hospitalName','type'=>'hidden')); ?>
                        <?php echo $this->form->input('MstFacilityItem.hospitalText',array('id'=>'hospitalText','class'=>'txt','style'=>'width:60px;')); ?>
                        <?php echo $this->form->input('MstFacilityItem.hospitalCode',array('options'=>$customers,'empty'=>true,'class'=>'txt','style'=>'width:120px;','id'=>'hospitalCode')); ?>
                    </td>
                </tr>
                <tr>
                    <th>商品ID</th>
                    <td><?php echo $this->form->input('MstFacilityItem.internal_code', array('maxlength'=>50,'class'=>'txt search_internal_code')); ?></td>
                    <td width="20"></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->input('MstFacilityItem.item_code', array('maxlength'=>50,'class'=>'txt search_upper')); ?></td>
                    <td width="20"></td>
                    <th><?php echo $this->Session->read('Auth.Config.SpareKey1Title'); ?></th>
                    <td><?php echo $this->form->input('MstFacilityItem.spare_key1', array('maxlength'=>50,'class'=>'txt')); ?></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->input('MstFacilityItem.item_name', array('type'=>'text','maxlength'=>50,'class'=>'txt search_canna')); ?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->input('MstFacilityItem.dealer_code', array('type'=>'text','maxlength'=>50,'class'=>'txt search_canna')); ?></td>
                    <td></td>
                    <th><?php echo $this->Session->read('Auth.Config.SpareKey2Title'); ?></th>
                    <td><?php echo $this->form->input('MstFacilityItem.spare_key2', array('maxlength'=>50,'class'=>'txt search_canna')); ?></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->input('MstFacilityItem.standard', array('type'=>'text','maxlength'=>50,'class'=>'txt search_canna')); ?></td>
                    <td></td>
                    <th>JANコード</th>
                    <td><?php echo $this->form->input('MstFacilityItem.jan_code', array('maxlength'=>50,'class'=>'txt')); ?></td>
                    <td></td>
                    <th><?php echo $this->Session->read('Auth.Config.SpareKey3Title'); ?></th>
                    <td><?php echo $this->form->input('MstFacilityItem.spare_key3', array('maxlength'=>50,'class'=>'txt search_canna')); ?></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="mc_search_btn"/>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <?php echo $this->element('limit_combobox',array('result'=>count($fitems))); ?>
    <div id="results">
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th width="20px" rowspan="2"></th>
                    <th class="col20">商品ID</th>
                    <th class="col30">商品名</th>
                    <th class="col20">製品番号</th>
                    <th class="col30">得意先</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>医事コード</th>
                </tr>
                <?php $i = 0; foreach ($fitems as $item){ ?>
                <tr>
                    <td rowspan="2" class="center">
                        <?php echo $this->form->radio('MstFacilityItem.opt' , array($item['MstFacilityItem']['id'] . "_" . $item['MstFacility']['id'] => '') , array('hiddenField'=>false , 'label'=>false , 'class'=>'check')); ?></td>
                    <td><?php echo h_out($item['MstFacilityItem']['internal_code']);?></td>
                    <td><?php echo h_out($item['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($item['MstFacilityItem']['item_code']); ?></td>
                    <td><?php echo h_out($item['MstFacilityItem']['partner_facility_name']); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo h_out($item['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($item['MstDealer']['dealer_name']); ?></td>
                    <td><?php echo h_out($item['MstFacilityItemMc']['medical_code']); ?></td>
                </tr>
                <?php $i++; } ?>
                <?php if(isset($data['search']['is_search']) && count($fitems)==0){ ?>
                <tr><td colspan="4" class="center">該当するデータはありません。</td></tr>
                <?php } ?>
            </table>
        </div>
        <table style="width: 100%;">
            <tr>
                <td></td>
                <td align="right"></td>
            </tr>
        </table>
        <?php if(count($fitems)>0){ ?>
        <div class="ButtonBox"><input type="button" class="btn btn9" id="edit_mc_btn" /></div>
        <?php } ?>
    </div>
</form>
