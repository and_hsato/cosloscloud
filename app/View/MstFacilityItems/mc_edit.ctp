<script type="text/javascript">
<!--
function dateCheck( checkStr ) {
    if( !checkStr.match(/^\d{4}\/\d{2}\/\d{2}$/)) {
        return false;
    }

    var year = checkStr.substr( 0 , 4 );
    var month = checkStr.substr( 5 , 2 );
    var day = checkStr.substr( 8 , 2 );

    if( month > 0 && month < 13 && day > 0 && day < 32 ) {
        var date = new Date( year ,month -1 , day );
        if( isNaN(date) ) {
            return false;
        } else if( date.getFullYear() == year && date.getMonth() == month - 1 && date.getDate() == day ){
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
  
$(function(){
    //確定ボタン押下
    $("#mc_edit_btn").click(function(){
        if($("#medical_code").val()==""){
            alert("医事コードを入力してください");
            return false;
        }
        $("#edit_mc_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mc_edit_result').submit();
    });
    //コストシール印字ボタン押下
    $("#print_cost_sticker_btn").click(function(){
       if($("#medical_code").val()==""){
            alert("医事コードを入力してください");
            $("#medical_code").focus();
            return false;
       }

       if($("#sheets").val()==""){
            alert("枚数を入力してください");
            $("#sheets").focus();
            return false;
       }

       if(!$("#sheets").val().match(/^[0-9]+$/i)){
            alert("枚数は数字で入力してください");
            $("#sheets").focus();
            return false;
       }
  
      if( !dateCheck($("#validated_date").val()) && ""!=$("#validated_date").val() ) {
            alert("有効期限が正しくありません");
            $("#validated_date").focus();
            return false;
      }
      if(confirm("医事シールを"+$("#sheets").val()+"枚印字します\nよろしいですか？")){
          $("#edit_mc_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal').submit();
      }
    });
});

//-->
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/mc_search">医事コードメンテ　施設採用品一覧</a></li>
        <li>医事コード編集</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>医事コード編集</span></h2>
<h2 class="HeaddingSmall">施設採用品情報</h2>
<form id="edit_mc_form" class="validate_form" method="post">
    <?php echo $this->form->hidden('MstFacilityItem.id',array('id'=>'item_id','value'=>$data['MstFacilityItem']['id'])); ?>
    <?php echo $this->form->hidden('MstFacilityItem.medical_id',array('id'=>'medical_id','value'=>$data['MstFacilityItem']['medical_id'])); ?>
    <?php echo $this->form->hidden('MstFacilityItem.facility_id',array('id'=>'facility_id','value'=>$data['MstFacilityItem']['facility_id'])); ?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>得意先</th>
                <td><?php echo $this->form->input('MstFacilityItem.customer_name',array('type'=>'text', 'class' => 'lbl' , 'id'=>'customer_name','value'=>$data['MstFacilityItem']['customer_name'] , 'readonly'=>'readonly') ); ?></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code',array('class'=>'lbl','value'=>$data['MstFacilityItem']['internal_code'], 'readonly'=>'readonly')); ?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_code',array('class'=>'lbl search_upper','value'=>$data['MstFacilityItem']['item_code'],'readonly'=>'readonly')); ?></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_name',array('type'=>'text','class'=>'lbl','value'=>$data['MstFacilityItem']['item_name'],'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstDealer.dealer_name',array('class'=>'lbl','readonly'=>'readonly','value'=>$data['MstDealer']['dealer_name'])); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('MstFacilityItem.standard',array('type'=>'text','class'=>'lbl', 'value'=>$data['MstFacilityItem']['standard'] ,'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('MstFacilityItem.jan_code',array('type'=>'text','class'=>'lbl','value'=>$data['MstFacilityItem']['jan_code'],'readonly'=>'readonly')); ?></td>
            </tr>
            <tr>
                <th>保険請求</th>
                <td><?php echo $this->form->input('MstFacilityItem.insurance_claim_department_name',array('type'=>'text','class'=>'lbl','value'=>$data['MstFacilityItem']['insurance_claim_department_name'] ,'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>生物由来</th>
                <td><?php echo $this->form->input('MstFacilityItem.biogenous_type_name',array('type'=>'text','class'=>'lbl', 'value'=>$data['MstFacilityItem']['biogenous_type_name'],'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>償還価格</th>
                <td><?php echo $this->form->input('MstFacilityItem.refund_price',array('type'=>'text','class'=>'num lbl','value'=>$data['MstFacilityItem']['refund_price'], 'readonly'=>'readonly')); ?></td>
            </tr>
            <tr>
                <th>保険請求名</th>
                <td colspan="7"><?php echo $this->form->input('MstFacilityItem.insurance_claim_name_s',array('type'=>'text','class'=>'lbl','readonly'=>'readonly','value'=>$data['MstFacilityItem']['insurance_claim_name_s'])); ?></td>
            </tr>
            <tr>
                <th>医事コード</th>
                <td><?php echo $this->form->input('MstFacilityItem.medical_code',array('class'=>'r','maxlength' => '20' , 'value'=>$data['MstFacilityItem']['medical_code'] , 'id'=>'medical_code' )); ?></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn2 [p2]" id="mc_edit_btn" /></p>
    </div>
    <h2 class="HeaddingSmall">一括印字</h2>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>ロット番号</th>
                <td><?php echo $this->form->input('TrnSticker.lot_no',array('class'=>'txt')); ?></td>
            </tr>
            <tr>
                <th>有効期限</th>
                <td><?php echo $this->form->input('TrnSticker.validated_date',array('type'=>'text','class'=>'date','id'=>'validated_date')); ?></td>
            </tr>
            <tr>
                <th>枚数</th>
                <td><?php echo $this->form->input('TrnSticker.sheets',array('class'=>'num r','style'=>'width:60px;' , 'id'=>'sheets')); ?></td>
            </tr>
        </table>
     </div>
     <div class="ButtonBox">
         <p class="center">
             <input type="button" class="btn btn12 [p2]" id="print_cost_sticker_btn" />
         </p>
     </div>
</form>
