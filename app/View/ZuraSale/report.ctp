<style>
SELECT#year {
    width: 70px
}
SELECT#month {
    width: 60px
}
</style>
<script type="text/javascript">
    $(document).ready(function(){
        // 帳票出力ボタン押下
        $('#btn_print').click(function() {
            tmp = $('#selected_id').val();
            if (tmp === null || tmp === "")
            {
              window.alert("対象月が選択されていません");
              return false;
            }
            $("#zura_form").attr('action', '<?php echo $this->webroot; ?>PdfReport/claim').attr('target','_blank').submit();
        });
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>請求書発行</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>請求書発行</span></h2>
<?php if (false) { ?>
<h2 class="HeaddingMid">印刷対象のデータがありません。</h2>
<?php } else { ?>
<h2 class="HeaddingMid">AND宛ての請求書を発行します。<br />対象月を選択し、発行して下さい。</h2>
<form id="zura_form" id="form_sales" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/report" method="post">
    <div class="SearchBox">
      <div class="box-float">
        <div>
          <span>対象月</span>
          <?php echo $this->form->input('selected_id', array('options'=>$selectedDateList, 'class' => 'txt', 'style'=> 'height:40px; width:120px; font-size:20px;', 'id' => 'selected_id')); ?>
        </div>
      </div>
      <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_print" value="表示" />
      </div>
    </div>
</form>
<?php } ?>
