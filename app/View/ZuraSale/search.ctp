<script type="text/javascript">
    var defaultZuraDate = {};
    $(document).ready(function(){
        // お支払明細表示ボタン押下
        $('#btn_ｓpecification').click(function() {
          $("#dialog").dialog("open");
          return false;
        });
        // 詳細画面へ進むボタン押下
        $('#btn_confirm').click(function() {
            amount_available = parseInt($('#amount_available').text().replaceAll(",", ""));
            if (isFinite(amount_available) === false || amount_available === 0){
              alert("延伸可能な品目がありません");
              return;
            }
            upper = $('#extensionHopingPrice').val().replaceAll(",", "");
            if (isFinite(upper) === false && upper === ""){
              alert("支払いサイト延伸希望金額を入力してください");
              return;
            }

            $('#form_zura').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm').attr('method', 'post').submit();
        });
        // 延伸希望期間を選択
        $('#selectedExtensionHopingMonth').change(function() {
          tmp = parseInt($('#extensionHopingPrice').val().replaceAll(",", ""));
          if (isFinite(tmp) === false || tmp.length === 0) { return false; }

          setCalculatedItems();
        });
        //延伸希望金額を入力
        $('#extensionHopingPrice').keypress( function ( e ) {
          if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)){ //Enterキー押下
          tmp = parseInt(this.value.replaceAll(",", ""));
          if (isFinite(tmp) === false || tmp.length === 0 || $('#selectedExtensionHopingMonth').val() === 0) { return false; }

          setCalculatedItems();

          return false;
          }
        });

        $("#dialog").dialog({
          autoOpen: false,
          width: "auto",
          modal: true,
        });

        function setCalculatedItems(){
          default_price_0 = parseInt($('#default_price_0').text().replaceAll(",", ""));
          default_price_1 = parseInt($('#default_price_1').text().replaceAll(",", ""));
          default_price_2 = parseInt($('#default_price_2').text().replaceAll(",", ""));
          default_price_3 = parseInt($('#default_price_3').text().replaceAll(",", ""));
          default_price_4 = parseInt($('#default_price_4').text().replaceAll(",", ""));
          default_price_5 = parseInt($('#default_price_5').text().replaceAll(",", ""));
          default_price_6 = parseInt($('#default_price_6').text().replaceAll(",", ""));
          default_commission_0 = parseInt($('#default_commission_0').text().replaceAll(",", ""));
          default_commission_1 = parseInt($('#default_commission_1').text().replaceAll(",", ""));
          default_commission_2 = parseInt($('#default_commission_2').text().replaceAll(",", ""));
          default_commission_3 = parseInt($('#default_commission_3').text().replaceAll(",", ""));
          default_commission_4 = parseInt($('#default_commission_4').text().replaceAll(",", ""));
          default_commission_5 = parseInt($('#default_commission_5').text().replaceAll(",", ""));
          default_commission_6 = parseInt($('#default_commission_6').text().replaceAll(",", ""));
          default_sumPrice_0 = parseInt($('#default_sumPrice_0').text().replaceAll(",", ""));
          default_sumPrice_1 = parseInt($('#default_sumPrice_1').text().replaceAll(",", ""));
          default_sumPrice_2 = parseInt($('#default_sumPrice_2').text().replaceAll(",", ""));
          default_sumPrice_3 = parseInt($('#default_sumPrice_3').text().replaceAll(",", ""));
          default_sumPrice_4 = parseInt($('#default_sumPrice_4').text().replaceAll(",", ""));
          default_sumPrice_5 = parseInt($('#default_sumPrice_5').text().replaceAll(",", ""));
          default_sumPrice_6 = parseInt($('#default_sumPrice_6').text().replaceAll(",", ""));

          upper = 0;
          if (default_price_0 < parseInt($('#extensionPrice').text().replaceAll(",", ""))){
            upper = default_price_0;
          }else{
            upper = parseInt($('#extensionPrice').text().replaceAll(",", ""));
          }
          if (parseInt($('#extensionHopingPrice').val().replaceAll(",", "")) > upper){
            add = upper;
            $('#extensionHopingPrice').val(upper);
          }else{
            add = parseInt($('#extensionHopingPrice').val().replaceAll(",", ""));
          }
          month = parseInt($('#selectedExtensionHopingMonth').val());
          commission_rate = parseFloat($('#commission_rate').text());
          commission = Math.floor(add * month * commission_rate);
          switch(month){
            case 1:
              default_price_0 -= add;
              default_commission_0 += commission;
              default_sumPrice_0 -= add;
              default_sumPrice_0 += commission;


              default_price_1 += add;
              default_sumPrice_1 += add;
              break;
            case 2:
              default_price_0 -= add;
              default_commission_0 += commission;
              default_sumPrice_0 -= add;
              default_sumPrice_0 += commission;

              default_price_2 += add;
              default_sumPrice_2 += add;
              break;
            case 3:
              default_price_0 -= add;
              default_commission_0 += commission;
              default_sumPrice_0 -= add;
              default_sumPrice_0 += commission;

              default_price_3 += add;
              default_sumPrice_3 += add;
              break;
            case 4:
              default_price_0 -= add;
              default_commission_0 += commission;
              default_sumPrice_0 -= add;
              default_sumPrice_0 += commission;

              default_price_4 += add;
              default_sumPrice_4 += add;
              break;
            case 5:
              default_price_0 -= add;
              default_commission_0 += commission;
              default_sumPrice_0 -= add;
              default_sumPrice_0 += commission;

              default_price_5 += add;
              default_sumPrice_5 += add;
              break;
            case 6:
              default_price_0 -= add;
              default_commission_0 += commission;
              default_sumPrice_0 -= add;
              default_sumPrice_0 += commission;

              default_price_6 += add;
              default_sumPrice_6 += add;
              break;
            default:
              break;
          }

          $('#price_0').text(numberFormat(default_price_0));
          $('#price_1').text(numberFormat(default_price_1));
          $('#price_2').text(numberFormat(default_price_2));
          $('#price_3').text(numberFormat(default_price_3));
          $('#price_4').text(numberFormat(default_price_4));
          $('#price_5').text(numberFormat(default_price_5));
          $('#price_6').text(numberFormat(default_price_6));
          $('#commission_0').text(numberFormat(default_commission_0));
          $('#commission_1').text(numberFormat(default_commission_1));
          $('#commission_2').text(numberFormat(default_commission_2));
          $('#commission_3').text(numberFormat(default_commission_3));
          $('#commission_4').text(numberFormat(default_commission_4));
          $('#commission_5').text(numberFormat(default_commission_5));
          $('#commission_6').text(numberFormat(default_commission_6));
          $('#sumPrice_0').text(numberFormat(default_sumPrice_0));
          $('#sumPrice_1').text(numberFormat(default_sumPrice_1));
          $('#sumPrice_2').text(numberFormat(default_sumPrice_2));
          $('#sumPrice_3').text(numberFormat(default_sumPrice_3));
          $('#sumPrice_4').text(numberFormat(default_sumPrice_4));
          $('#sumPrice_5').text(numberFormat(default_sumPrice_5));
          $('#sumPrice_6').text(numberFormat(default_sumPrice_6));
        }

    });
</script>

<p id="default_price_0" style="display:none"><?php echo $present_month_price?></p>
<p id="default_price_1" style="display:none"><?php echo $_1month_later_price?></p>
<p id="default_price_2" style="display:none"><?php echo $_2month_later_price?></p>
<p id="default_price_3" style="display:none"><?php echo $_3month_later_price?></p>
<p id="default_price_4" style="display:none"><?php echo $_4month_later_price?></p>
<p id="default_price_5" style="display:none"><?php echo $_5month_later_price?></p>
<p id="default_price_6" style="display:none"><?php echo $_6month_later_price?></p>
<p id="default_commission_0" style="display:none"><?php echo $present_month_commission?></p>
<p id="default_commission_1" style="display:none"><?php echo $_1month_later_commission?></p>
<p id="default_commission_2" style="display:none"><?php echo $_2month_later_commission?></p>
<p id="default_commission_3" style="display:none"><?php echo $_3month_later_commission?></p>
<p id="default_commission_4" style="display:none"><?php echo $_4month_later_commission?></p>
<p id="default_commission_5" style="display:none"><?php echo $_5month_later_commission?></p>
<p id="default_commission_6" style="display:none"><?php echo $_6month_later_commission?></p>
<p id="default_sumPrice_0" style="display:none"><?php echo $present_month_sum_price?></p>
<p id="default_sumPrice_1" style="display:none"><?php echo $_1month_later_sum_price?></p>
<p id="default_sumPrice_2" style="display:none"><?php echo $_2month_later_sum_price?></p>
<p id="default_sumPrice_3" style="display:none"><?php echo $_3month_later_sum_price?></p>
<p id="default_sumPrice_4" style="display:none"><?php echo $_4month_later_sum_price?></p>
<p id="default_sumPrice_5" style="display:none"><?php echo $_5month_later_sum_price?></p>
<p id="default_sumPrice_6" style="display:none"><?php echo $_6month_later_sum_price?></p>
<p id="commission_rate" style="display:none"><?php echo $commission_rate?></p>
<p id="amount_available" style="display:none"><?php echo $amount_available?></p>

<div id="content-wrapper">

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>支払いサイト延伸［ZuraSale］</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>支払いサイト延伸［ZuraSale］</span></h2>
<h2 class="HeaddingMid">ご購入いただいた商品のお支払い月を最大６ヶ月延伸することができます。<br />
支払いサイトの延伸を希望したい金額と期間を指定し、「支払いサイト延伸詳細画面へ進む」ボタンを押すと、延伸希望金額に近くなる様、請求明細が自動選択されます。<br />
※<?php echo $purchase_month ?>月のお支払い分の延伸は、<?php echo $present_year ?>年<?php echo $present_month ?>月10日24:00までに入力を完了して下さい。</h2>
<form id="form_zura">
<div id="zura-table">
  <table class="state">
    <tr>
      <td>現在の支払サイト延伸金額：<?php echo substr($this->Common->toCommaStr($extension_price), 0 , strpos($this->Common->toCommaStr($extension_price), '.')) ?>円</td>
      <td>当月購入金額：<?php echo substr($this->Common->toCommaStr($present_month_totalPrice), 0 , strpos($this->Common->toCommaStr($present_month_totalPrice), '.')) ?>円</td>
    </tr>
    <tr>
      <td colspan="2"><?php echo $present_year ?>年<?php echo $present_month ?>月<?php echo $present_day ?>日お支払い予定金額(税抜)：<?php echo substr($this->Common->toCommaStr($present_month_sum_price), 0 , strpos($this->Common->toCommaStr($present_month_sum_price), '.')) ?>円
        うち、延伸可能金額：<?php echo substr($this->Common->toCommaStr($amount_available), 0 , strpos($this->Common->toCommaStr($amount_available), '.')) ?>円
        <input type="button" class="common-button-2" id="btn_ｓpecification" value="お支払明細表示" />
      </td>
    </tr>
  </table>
  <h2 class="HeaddingSmall mb10 mt30">支払いサイトを延伸する</h2>
  <table class="extension">
    <tbody>
    <tr>
      <th><?php echo $present_year ?>年<?php echo $present_month ?>月<?php echo $present_day ?>日までの支払いサイト延伸上限金額</th>
			<td style="text-align:right;"><p style="display:inline" id="extensionPrice"><?php echo substr($this->Common->toCommaStr($extensionUpperLimit), 0 , strpos($this->Common->toCommaStr($extensionUpperLimit), '.')) ?> </p><p style="display:inline" >円</p></td>
    </tr>
    <tr>
      <th>支払いサイト延伸希望金額を入力</th>
      <td><?php echo $this->form->input('extensionHopingPrice', array('type'=>'text', 'class' => 'txt', 'style'=>'width:160px;', 'id' => 'extensionHopingPrice')); ?>円</td>
    </tr>
    <tr>
      <th>支払いサイト延伸希望期間を選択 ※最大６ヶ月</th>
      <td style="width:60px;"><?php echo $this->form->input('selectedExtensionHopingMonth', array('options'=>$extensionHopingMonth, 'class' => 'txt','style'=> 'width:60px;', 'id' => 'selectedExtensionHopingMonth', 'maxlength'=>1)); ?>ヶ月</td>
    </tr>
    </tbody>
  </table>
 <h2 class="HeaddingSmall mb10 mt30">月別 お支払い予定金額</h2>
<table class="zura-simulator">
  <tbody>
		<tr class="zura-date">
			<td class="hedden"></td>
			<td class="zura-date"><?php echo $present_year ?>年<?php echo $present_month ?>月</td>
			<td class="zura-date"><?php echo $_1month_later_year ?>年<?php echo $_1month_later_month ?>月</td>
			<td class="zura-date"><?php echo $_2month_later_year ?>年<?php echo $_2month_later_month ?>月</td>
			<td class="zura-date"><?php echo $_3month_later_year ?>年<?php echo $_3month_later_month ?>月</td>
			<td class="zura-date"><?php echo $_4month_later_year ?>年<?php echo $_4month_later_month ?>月</td>
			<td class="zura-date"><?php echo $_5month_later_year ?>年<?php echo $_5month_later_month ?>月</td>
			<td class="zura-date"><?php echo $_6month_later_year ?>年<?php echo $_6month_later_month ?>月</td>
		</tr>
		<tr class="alinright">
			<th>お支払予定金額（元本）</th>
			<td style="text-align:right;"><p style="display:inline" id="price_0"><?php echo substr($this->Common->toCommaStr($present_month_price), 0 , strpos($this->Common->toCommaStr($present_month_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="price_1"><?php echo substr($this->Common->toCommaStr($_1month_later_price), 0 , strpos($this->Common->toCommaStr($_1month_later_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="price_2"><?php echo substr($this->Common->toCommaStr($_2month_later_price), 0 , strpos($this->Common->toCommaStr($_2month_later_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="price_3"><?php echo substr($this->Common->toCommaStr($_3month_later_price), 0 , strpos($this->Common->toCommaStr($_3month_later_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="price_4"><?php echo substr($this->Common->toCommaStr($_4month_later_price), 0 , strpos($this->Common->toCommaStr($_4month_later_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="price_5"><?php echo substr($this->Common->toCommaStr($_5month_later_price), 0 , strpos($this->Common->toCommaStr($_5month_later_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="price_6"><?php echo substr($this->Common->toCommaStr($_6month_later_price), 0 , strpos($this->Common->toCommaStr($_6month_later_price), '.')) ?></p><p style="display:inline" >円</p></td>
		</tr>
		<tr class="alinright">
			<th>延伸手数料</th>
      <td style="text-align:right;"><p style="display:inline" id="commission_0"><?php echo substr($this->Common->toCommaStr($present_month_commission), 0 , strpos($this->Common->toCommaStr($present_month_commission), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="commission_1"><?php echo substr($this->Common->toCommaStr($_1month_later_commission), 0 , strpos($this->Common->toCommaStr($_1month_later_commission), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="commission_2"><?php echo substr($this->Common->toCommaStr($_2month_later_commission), 0 , strpos($this->Common->toCommaStr($_2month_later_commission), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="commission_3"><?php echo substr($this->Common->toCommaStr($_3month_later_commission), 0 , strpos($this->Common->toCommaStr($_3month_later_commission), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="commission_4"><?php echo substr($this->Common->toCommaStr($_4month_later_commission), 0 , strpos($this->Common->toCommaStr($_4month_later_commission), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="commission_5"><?php echo substr($this->Common->toCommaStr($_5month_later_commission), 0 , strpos($this->Common->toCommaStr($_5month_later_commission), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="commission_6"><?php echo substr($this->Common->toCommaStr($_6month_later_commission), 0 , strpos($this->Common->toCommaStr($_6month_later_commission), '.')) ?></p><p style="display:inline" >円</p></td>
		</tr>
		<tr class="alinright">
			<th>お支払予定金額合計</th>
      <td style="text-align:right;"><p style="display:inline" id="sumPrice_0"><?php echo substr($this->Common->toCommaStr($present_month_sum_price), 0 , strpos($this->Common->toCommaStr($present_month_sum_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="sumPrice_1"><?php echo substr($this->Common->toCommaStr($_1month_later_sum_price), 0 , strpos($this->Common->toCommaStr($_1month_later_sum_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="sumPrice_2"><?php echo substr($this->Common->toCommaStr($_2month_later_sum_price), 0 , strpos($this->Common->toCommaStr($_2month_later_sum_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="sumPrice_3"><?php echo substr($this->Common->toCommaStr($_3month_later_sum_price), 0 , strpos($this->Common->toCommaStr($_3month_later_sum_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="sumPrice_4"><?php echo substr($this->Common->toCommaStr($_4month_later_sum_price), 0 , strpos($this->Common->toCommaStr($_4month_later_sum_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="sumPrice_5"><?php echo substr($this->Common->toCommaStr($_5month_later_sum_price), 0 , strpos($this->Common->toCommaStr($_5month_later_sum_price), '.')) ?></p><p style="display:inline" >円</p></td>
			<td style="text-align:right;"><p style="display:inline" id="sumPrice_6"><?php echo substr($this->Common->toCommaStr($_6month_later_sum_price), 0 , strpos($this->Common->toCommaStr($_6month_later_sum_price), '.')) ?></p><p style="display:inline" >円</p></td>
		</tr>
	</tbody>
</table>

</div>
   <div class="ButtonBox">
        <p class="center">
            <input type="button" class="common-button" id="btn_confirm" value="支払いサイト延伸詳細画面へ進む" />
        </p>
    </div>
</form>

<div id="dialog" title=<?php echo $present_year ?>年<?php echo $present_month ?>月<?php echo $present_day ?>日　お支払い予定明細　※色付の明細は既に支払いサイトが延伸された項目です。 >
  <div id="zura-table">
    <div id="extension-detail">
      <table class="itemTable" style="font-size:14px">
        <tr style="background-color:#E7E8E2">
          <th width="80">購入日</th>
          <th width="320">品名</th>
          <th width="320">規格</th>
          <th width="320">製品番号</th>
          <th width="80">購入数量</th>
          <th width="120">購入金額</th>
        </tr>
        <?php $cnt=0; foreach ($purchase_items as $item) { ; ?>
        <?php if($item['TrnClaim']['extension_possible']) {  ?>
        <tr>
          <td><?php echo date("n月j日", strtotime($item['TrnClaim']['claim_date'])) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['item_name']) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['standard']) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['item_code']) ?></td>
          <td style="text-align:right; padding-right:8px;"><p style="display:inline" class="unitCount" ><?php echo $item['TrnClaim']['count'] ?></p><p style="display:inline"><?php echo $item['MstUnitName']['unit_name'] ?></p></td>
          <td style="text-align:right; padding-right:8px;"><p style="display:inline" class="unitPrice" ><?php echo substr($this->Common->toCommaStr($item['TrnClaim']['unit_price']), 0 , strpos($this->Common->toCommaStr($item['TrnClaim']['unit_price']), '.')) ?></p><p style="display:inline" >円</p></td>
        </tr>
        <?php } else{ ?>
        <tr bgcolor="#74A9D6">　<!--　延伸分 -->
          <td><?php echo date("n月j日", strtotime($item['TrnClaim']['claim_date'])) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['item_name']) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['standard']) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['item_code']) ?></td>
          <td style="text-align:right; padding-right:8px;"><p style="display:inline" class="unitCount" ><?php echo $item['TrnClaim']['count'] ?></p><p style="display:inline"><?php echo $item['MstUnitName']['unit_name'] ?></p></td>
          <td style="text-align:right; padding-right:8px;"><p style="display:inline" class="unitPrice" ><?php echo substr($this->Common->toCommaStr($item['TrnClaim']['unit_price']), 0 , strpos($this->Common->toCommaStr($item['TrnClaim']['unit_price']), '.')) ?></p><p style="display:inline" >円</p></td>
        <tr>
        <?php } ?>
        <?php $cnt++;} ?>
      </table>
    </div>
  </div>
</div>
