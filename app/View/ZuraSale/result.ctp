<script type="text/javascript">
    $(document).ready(function(){
        // トップページへボタン押下
        $('#btn_top').click(function() {
            window.location.href = '<?php echo $this->webroot; ?>';
        });
    });
</script>
<div id="content-wrapper">

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>">支払いサイト延伸［ZuraSale］</a></li>
        <li>支払いサイト延伸内容詳細</a></li>
        <li>支払いサイト延伸内容の確認</li>
        <li>完了画面</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>支払いサイト延伸完了</span></h2>
<h2 class="HeaddingMid">以下の内容で支払いサイト延伸を受け付けました。</h2>

<div id="zura-table">
 <h2 class="HeaddingSmall mb10">2016年5月31日　お支払い予定金額  </h2>
</div>

<div id="fix-summary" class="back-white">
       <table>

		<tr>
			<th>支払いサイトを延伸する合計金額</th>
      <td><p style="display:inline" id="totalExtensionPrice" ><?php echo substr($this->Common->toCommaStr($totalExtensionPrice), 0 , strpos($this->Common->toCommaStr($totalExtensionPrice), '.')) ?></p><p style="display:inline" >円</p></td>
		</tr>
 		<tr>
			<th>支払いサイト延伸手数料</th>
      <td><p style="display:inline" id="totalCommission" ><?php echo substr($this->Common->toCommaStr($totalCommission), 0 , strpos($this->Common->toCommaStr($totalCommission), '.')) ?></p><p style="display:inline" >円</p></td>
		</tr>
       </table>
       <table class="zura-result">
		<tr>
			<th>延伸後　<?php echo $present_year ?>年<?php echo $present_month ?>月<?php echo $present_day ?>日お支払い予定金額</th>
      <td><p style="display:inline" id="totalPrice" ><?php echo substr($this->Common->toCommaStr($totalPrice), 0 , strpos($this->Common->toCommaStr($totalPrice), '.')) ?><p style="display:inline" >円</p></td>
		</tr>
       </table>

</div>


   <div class="ButtonBox">
        <p class="center">
            <input type="button" class="common-button" id="btn_top" value="トップページへ" />
        </p>
    </div>

</form>
