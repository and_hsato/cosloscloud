<script type="text/javascript">
    $(document).ready(function(){
        // 確定ボタン押下
        $('#btn_finish').click(function() {
            resutl = checkExtensionSet();
            if (resutl == false){
              alert("延伸する商品が選択されていません。");
              return false;
            }
            if(!confirm('支払いサイト延伸を確定します。\n一度確定した内容は変更できませんがよろしいですか？')){
              return false;
            }

            $('#zura_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result').submit();
        });

        $('input.extensionMonth').change(function(){
          value = parseInt(this.value);
          if(value > 0){
            //延伸ON
            $(this).parents('.itemRow').find('input[type=checkbox]').attr('checked', 'checked');
          }else{
            //延伸OFF
            $(this).parents('.itemRow').find('input[type=checkbox]').val([]);
          }
          setCalculatedItems();
          setExtentionBgColor();
        });

        function setCalculatedItems(){
          default_extensionUpperLimit = parseInt($('#default_extensionUpperLimit').text().replaceAll(",", ""));
          default_totalExtensionPrice = parseInt($('#default_totalExtensionPrice').text().replaceAll(",", ""));
          default_totalCommission = parseInt($('#default_totalCommission').text().replaceAll(",", ""));
          default_totalPrice = parseInt($('#default_totalPrice').text().replaceAll(",", ""));
          commission_rate = parseFloat($('#commission_rate').text());

          totalExtensionPrice = 0;
          totalCommission = 0;

          $('input[type=checkbox].extensionSet').each(function(i){
            if(true === this.checked){
              month = $(this).parents('.itemRow').find('input[type=radio]').filter(':checked').val();
              unitCount = parseInt($(this).parents('.itemRow').find('.unitCount').text());
              unitPrice = parseInt($(this).parents('.itemRow').find('.unitPrice').text().replaceAll(",", ""));

              if (isFinite(month) == false){ return true; }
              if (isFinite(unitCount) == false){ return true; }
              if (isFinite(unitPrice) == false){ return true; }

              totalExtensionPrice += parseInt(unitPrice * unitCount);
              totalCommission += Math.floor(parseInt(unitPrice * unitCount * commission_rate * month));
            }
          });

          set_upperLimit = default_extensionUpperLimit - totalExtensionPrice;
          set_totalExtension = default_totalExtensionPrice + totalExtensionPrice;
          set_commission = default_totalCommission + totalCommission;
          set_totalPrice = default_totalPrice - totalExtensionPrice + totalCommission;

          $('#extensionUpperLimit').text(numberFormat(set_upperLimit));
          $('#totalExtensionPrice').text(numberFormat(set_totalExtension));
          $('#totalCommission').text(numberFormat(set_commission));
          $('#totalPrice').text(numberFormat(set_totalPrice));
        }

        function setExtentionBgColor(){
          $('input[type=checkbox].extensionSet').each(function(i){
            month = $(this).parents('.itemRow').find('input[type=radio]').filter(':checked').val();
            tr = $(this).parents('.itemRow');
            if(true === this.checked
              && isFinite(month)
              && parseInt(month) > 0){
              tr.css("background-color", "#FFC34C");
            }else{
              tr.css("background-color", "#FFFFFF");
            }
          });
        }

        function checkExtensionSet(){
            result = $('input[type=checkbox].extensionSet').filter(':checked').val();
            if (isFinite(result)){
              return true;
            }else{
              return false;
            }
        }

    });
    $(window).load(function(){
      $('input[type=checkbox].extensionSet').each(function(i){
        index = $(this).parents('.itemRow')[0].sectionRowIndex;
        month = $(this).parents('.itemRow').find('input[type=radio]').filter(':checked').val();
        tr = $(this).parents('.itemRow');
        if(true === this.checked
          && isFinite(month)
          && parseInt(month) > 0){
          tr.css("background-color", "#FFC34C");
        }else{
          tr.css("background-color", "#FFFFFF");
        }
      });
    });
</script>

<p id="default_extensionUpperLimit" style="display:none"><?php echo $default_extensionUpperLimit?></p>
<p id="default_totalExtensionPrice" style="display:none"><?php echo $default_totalExtensionPrice?></p>
<p id="default_totalCommission" style="display:none"><?php echo $default_totalCommission?></p>
<p id="default_totalPrice" style="display:none"><?php echo $default_totalPrice?></p>
<p id="commission_rate" style="display:none"><?php echo $commission_rate?></p>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>">支払いサイト延伸［ZuraSale］</a></li>
        <li>支払いサイト延伸内容詳細</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>支払いサイト延伸内容詳細</span></h2>
<h2 class="HeaddingMid">お支払いを延伸する詳細画面をご確認ください、<br />
                        支払いサイト延伸希望金額に合わせた請求明細が自動選択されています。この画面で変更可能です。</h2>
 <div id="zura-table">
   <table class="extension">
     <tbody>
      <tr>
        <th><?php echo $present_year ?>年<?php echo $present_month ?>月<?php echo $present_day ?>日までの支払いサイト延伸上限金額</th>
        <td><p style="display:inline" id="extensionUpperLimit"><?php echo substr($this->Common->toCommaStr($extensionUpperLimit), 0 , strpos($this->Common->toCommaStr($extensionUpperLimit), '.')) ?></p> <p style="display:inline">円</p> </td>
      </tr>
    </tbody>
   </table>
   <h2 class="HeaddingSmall mt30"><?php echo $present_year ?>年<?php echo $present_month ?>月<?php echo $present_day ?>日　お支払い予定明細  </h2>
   <p>※色付の明細は既に支払いサイトが延伸された項目です。チェックボックスをクリックして延伸明細品目を変更可能です。</p>
  <div id="extension-detail">
   <table class="itemTable" id ="itemTable">
    <thead>
      <tr>
        <th colspan="7" class="hedden"></th>
        <th colspan="7">延伸希望期間</th>
      </tr>
      <tr>
        <th>延伸</th>
        <th>購入日</th>
        <th>品名</th>
        <th>規格</th>
        <th>製品番号</th>
        <th>購入数量</th>
        <th>購入金額</th>
        <th>延伸なし</th>
        <th>1ヶ月</th>
        <th>2ヶ月</th>
        <th>3ヶ月</th>
        <th>4ヶ月</th>
        <th>5ヶ月</th>
        <th>6ヶ月</th>
      </tr>
    </thead>
    <?php echo $this->form->create('postClaim',array('type'=>'post','id'=>'zura_form','class'=>'validate_form')); ?>
      <tbody>
        <?php foreach ($purchase_items as $item) { ; ?>
        <?php if($item['TrnClaim']['extension_possible']) {  ?>
        <tr class='itemRow'>
          <?php ($item['TrnClaim']['extension_set'] == true) ? $chk="checked" : $chk=""; ?>
          <td><?php echo $this->form->checkbox("id.{$item['TrnClaim']['id']}", array('class'=>'extensionSet', 'value'=>$item['TrnClaim']['id'], 'checked'=>$chk, 'hiddenField'=>false)); ?></td>
          <td ><?php echo date("n月j日", strtotime($item['TrnClaim']['claim_date'])) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['item_name']) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['standard']) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['item_code']) ?></td>
          <td style="text-align:right; padding-right:8px;"><p style="display:inline" class="unitCount" ><?php echo $item['TrnClaim']['count'] ?></p> <p style="display:inline"><?php echo $item['MstUnitName']['unit_name'] ?></p></td>
          <td style="text-align:right; padding-right:8px;"><p style="display:inline" class="unitPrice" ><?php echo substr($this->Common->toCommaStr($item['TrnClaim']['unit_price']), 0 , strpos($this->Common->toCommaStr($item['TrnClaim']['unit_price']), '.')) ?></p><p style="display:inline" >円</p></td>
          <?php $month = -1; ?>
          <?php if ($item['TrnClaim']['present_month'] == true){ $month=0; }?>
          <?php if ($item['TrnClaim']['1_month_later'] == true){ $month=1; }?>
          <?php if ($item['TrnClaim']['2_month_later'] == true){ $month=2; }?>
          <?php if ($item['TrnClaim']['3_month_later'] == true){ $month=3; }?>
          <?php if ($item['TrnClaim']['4_month_later'] == true){ $month=4; }?>
          <?php if ($item['TrnClaim']['5_month_later'] == true){ $month=5; }?>
          <?php if ($item['TrnClaim']['6_month_later'] == true){ $month=6; }?>
          <?php $options = array(0=>'', 1=>'', 2=>'', 3=>'', 4=>'', 5=>'', 6=>''); ?>
          <?php $attributes = array('class'=>'extensionMonth', 'id'=>$item['TrnClaim']['id'], 'value'=>$month, 'legend'=>false, 'separator'=>'<td>'); ?>
          <td><?php echo $this->form->radio("month.{$item['TrnClaim']['id']}", $options, $attributes); ?></td>
        </tr>
        <?php } else{ ?>
        <tr bgcolor="#74A9D6">　<!--　延伸分 -->
          <td>×</td>
          <td><?php echo date("n月j日", strtotime($item['TrnClaim']['claim_date'])) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['item_name']) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['standard']) ?></td>
          <td style="padding-left:8px;"><?php echo h_out($item['MstFacilityItem']['item_code']) ?></td>
          <td style="text-align:right; padding-right:8px;"><p style="display:inline" class="unitCount" ><?php echo $item['TrnClaim']['count'] ?></p><p style="display:inline"><?php echo $item['MstUnitName']['unit_name'] ?></p></td>
          <td style="text-align:right; padding-right:8px;"><p style="display:inline" class="unitPrice" ><?php echo substr($this->Common->toCommaStr($item['TrnClaim']['unit_price']), 0 , strpos($this->Common->toCommaStr($item['TrnClaim']['unit_price']), '.')) ?></p><p style="display:inline" >円</p></td>
          <td colspan="7">延伸分（再延伸不可）</td>
        </tr>
        <?php } } ?>
      </tbody>
    <?php echo $this->form->end(); ?>
   </table>
  </div>
 </div>
 <div id="fix-summary">
  <table>
    <tr>
      <th><?php echo $present_year ?>年<?php echo $present_month ?>月<?php echo $present_day ?>日お支払い予定金額</th>
      <td><p style="display:inline" id="paymentAmout" ><?php echo substr($this->Common->toCommaStr($paymentAmout), 0 , strpos($this->Common->toCommaStr($paymentAmout), '.')) ?></p><p style="display:inline" >円</p></td>
    </tr>
    <tr>
      <th>支払いサイトを延伸する合計金額</th>
      <td><p style="display:inline" id="totalExtensionPrice" ><?php echo substr($this->Common->toCommaStr($totalExtensionPrice), 0 , strpos($this->Common->toCommaStr($totalExtensionPrice), '.')) ?></p><p style="display:inline" >円</p></td>
    </tr>
    <tr>
      <th>支払いサイト延伸手数料</th>
      <td><p style="display:inline" id="totalCommission" ><?php echo substr($this->Common->toCommaStr($totalCommission), 0 , strpos($this->Common->toCommaStr($totalCommission), '.')) ?></p><p style="display:inline" >円</p></td>
    </tr>
  </table>

  <table class="zura-result">
  <tr>
    <th>延伸後　<?php echo $present_year ?>年<?php echo $present_month ?>月<?php echo $present_day ?>日お支払い予定金額</th>
    <td><p style="display:inline" id="totalPrice" ><?php echo substr($this->Common->toCommaStr($totalPrice), 0 , strpos($this->Common->toCommaStr($totalPrice), '.')) ?><p style="display:inline" >円</p></td>
  </tr>
  </table>

</div>
<div class="ButtonBox">
  <p class="center">
    <input type="button" class="common-button" id="btn_finish" value="延伸サイト延伸を申し込む" />
  </p>
</div>

<div class="dialog">
  <span class="button cancel">延伸を申し込む</span>
  <span class="button cancel">戻る</span>
</div>
