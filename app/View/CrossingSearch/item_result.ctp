<script type="text/javascript">
$(document).ready(function(){
  $('select , input[type=text]').attr("readonly","readonly").removeClass('r txt date' ).addClass("lbl");
  $('input[type=button]').css("display","none");
  $('input[type=checkbox] , select').each(function(){
      $(this).after('<input type=hidden name=' + $(this).attr('name') + ' value=' + $(this).val() + ' >');
      $(this).attr('disabled' , 'disabled');
  });
});
  
var webroot = "<?php echo $this->webroot; ?>";

 $(function($) {
   $('select.basic_item_unit:disabled');
   /**
    * Changing item's basic unit name, and synchronize item's each item unit's unit name.
    */
   $('#basic_item_unit_name').change(function() {
   $('.basic_item_unit').val($('#basic_item_unit_name option:selected').val());
   });
 });

</script>

<script type="text/javascript" src="<?php echo $this->webroot; ?>js/facility_item.js"></script>

<style>
<!--
div#search_dealer_form {
    z-index: 9999;
    background-color: #808080;
    position: absolute;
    height: auto;
    width: auto;
    padding: 4px;
    color: #fff;
    left: 400px;
    top: 200px;
        width: 350px;
}

div#search_dealer_form ul li {
    margin-top: 2px;
    margin-left: 0px;
}

div#search_jmdn_form {
    z-index: 9999;
    background-color: black;
    position: absolute;
    height: auto;
    width: auto;
    padding: 4px;
    color: #fff;
    left: 400px;
    top: 200px;
        width: 350px;
}

div#search_jmdn_form ul li {
    margin-top: 2px;
    margin-left: 0px;
}

div#search_icc_form {
    z-index: 9999;
    background-color: red;
    position: absolute;
    height: auto;
    width: auto;
    padding: 4px;
    color: #fff;
    left: 400px;
    top: 200px;
        width: 350px;
}

div#search_icc_form ul li {
    margin-top: 2px;
    margin-left: 0px;
}

div#search_item_category_form {
    z-index: 9999;
    background-color: blue;
    position: absolute;
    height: auto;
    width: auto;
    padding: 4px;
    color: #fff;
    left: 400px;
    top: 200px;
        width: 350px;
}

div#search_item_category_form ul li {
    margin-top: 2px;
    margin-left: 0px;
}

/* root element for scrollable */
.scrollable { /* required settings */

    position: relative;
    overflow: auto;
        width: 350px; 
}

/* root element for scrollable items */
.scrollable .items {
    display: none; 
}
.floatClose{
    margin-left: 10px;
}

-->
</style>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/item_search">GMメンテ</a></li>
        <li>GM編集</li>
    </ul>
</div>

<?php echo $this->element('masters/facility_items/item_hiddenAjaxForm'); ?>

<h2 class="HeaddingLarge"><span>GM登録確認</span></h2>
<div class="Mes01"><?php echo $msg ?></div>
<h2 class="HeaddingSmall">GM情報</h2>
<form class="validate_form search_form" id="edit_facility_item_form">
    <?php echo $this->form->input('MstItem.id',array('type'=>'hidden')); ?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <?php echo $this->form->input('MstItem.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
    <div class="SearchBox">
        <div class="item_scrollable">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>ID</th>
                    <td><?php echo $this->form->input('MstItem.internal_code', array('label'=>false,'div'=>false, 'maxlength'=>'20','class'=>'r validate[required] search_upper','id'=>'internal_code'));?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->input('MstItem.item_code', array('label'=>false,'div'=>false, 'maxlength'=>'100','class'=>'txt search_upper'));?></td>
                    <td></td>
                    <th>予備キー１</th>
                    <td><?php echo $this->form->input('MstItem.spare_key1', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->input('MstItem.item_name', array('type'=>'text','label'=>false,'div'=>false, 'maxlength'=>'100','class'=>'r validate[required] search_canna','width'=>'150px'));?></td>
                    <td></td>
                    <th>販売元</th>
                    <td>
                        <?php echo $this->form->input('MstItem.mst_dealer_id', array('type'=>'hidden','id'=>'mst_dealer_id_hidden_txt')); ?>
                        <?php echo $this->form->input('MstDealer.dealer_code', array('type'=>'hidden','id'=>'mst_dealer_code_hidden_txt')); ?>
                        <?php echo $this->form->input('MstDealer.dealer_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'dealer_name_txt','readonly' => true));?>
                        <input type="button" value="選択" id="call_search_dealer_form_btn" /></td>
                    <td></td>
                    <th>予備キー２</th>
                    <td><?php echo $this->form->input('MstItem.spare_key2', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->input('MstItem.standard', array('type'=>'text','label'=>false,'div'=>false, 'maxlength'=>'100','class'=>'txt'));?></td>
                    <td></td>
                    <th>JANコード</th>
                    <td><?php echo $this->form->input('MstItem.jan_code', array('label'=>false,'div'=>false, 'maxlength'=>'14','class'=>'txt validate[optional,custom[onlyNumber]]'));?></td>
                    <td></td>
                    <th>予備キー３</th>
                    <td><?php echo $this->form->input('MstItem.spare_key3', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
                </tr>
                <tr>
                    <th>保険請求区分</th>
                    <td>
                        <?php echo $this->form->hidden('MstItem.insurance_claim_type_hidden',array('id'=>'insurance_claim_type_txt')); ?>
                        <?php echo $this->form->input('MstItem.insurance_claim_type_txt' , array('id'=>'insurance_claim_type_txt_p' , 'style'=>'width: 40px;','class'=>'txt','maxlength'=>'50')) ?>
                        <?php echo $this->form->input('MstItem.insurance_claim_type', array('options'=>$insuranceClaimDepartments, 'class' => 'txt', 'style'=>'width: 120px; height: 23px;','id'=>'insurance_claim_type_sel','empty'=>true)); ?>
                    </td>
                    <td></td>
                    <th>JMDNコード</th>
                    <td><?php echo $this->form->input('MstItem.jmdn_code', array('type'=>'hidden','id'=>'mst_jmdn_name_hidden_txt')); ?> <?php echo $this->form->input('MstItem.jmdn_code', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'jmdn_code_txt','readonly' => true));?>
                        <input type="button" id="call_search_jmdn_form_btn" value="選択" />
                    </td>
                    <td></td>
                    <th>予備キー４</th>
                    <td><?php echo $this->form->input('MstItem.spare_key4', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
                </tr>
                <tr>
                    <th>保険請求コード</th>
                    <td>
                        <?php echo $this->form->input('MstItem.insurance_claim_code_hidden', array('type'=>'hidden','id'=>'mst_icc_name_hidden_txt')); ?>
                        <?php echo $this->form->input('MstItem.insurance_claim_code', array('label'=>false,'readonly'=>true,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'icc_code_txt'));?>
                        <input type="button" id="call_search_icc_form_btn" value="選択" />
                    </td>
                    <td></td>
                    <th>一般名称</th>
                    <td><?php echo $this->form->input('MstItem.jmdn_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'mst_jmdn_name_txt','readonly' => true));?></td>
                    <td></td>
                    <th>予備キー５</th>
                    <td><?php echo $this->form->input('MstItem.spare_key5', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
                </tr>
                <tr>
                    <th>保険請求略称</th>
                    <td><?php echo $this->form->input('MstItem.insurance_claim_short_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','readonly'=>true,'id'=>'mst_icc_name_s_txt'));?></td>
                    <td></td>
                    <th>クラス分類</th>
                    <td>
                        <?php echo $this->form->input('MstItem.class_separation_hidden', array('type'=>'hidden','id'=>'class_separation_txt')); ?>
                        <?php echo $this->form->input('MstItem.class_separation', array('options'=>$classseparations, 'label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'class_separation_sel' ,'style'=>'height: 23px;', 'empty'=>false)); ?>
                    </td>
                    <td></td>
                    <th>予備キー６</th>
                    <td><?php echo $this->form->input('MstItem.spare_key6', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
                </tr>
                <tr>
                    <th>保険請求名</th>
                    <td><?php echo $this->form->input('MstItem.insurance_claim_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'readonly'=>true,'class'=>'txt','id'=>'mst_icc_name_txt'));?></td>
                    <td></td>
                    <th>生物由来区分</th>
                    <td><?php echo $this->form->input('MstItem.biogenous_type', array('options'=>$biogenous_types, 'class'=>'txt','style'=>'width: 120px; height: 23px;','empty'=>true)); ?></td>
                    <td></td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>償還価格</th>
                    <td><?php echo $this->form->input('MstItem.refund_price', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt num r validate[required] refund_price'));?></td>
                    <td></td>
                    <th>定価単価</th>
                    <td><?php echo $this->form->input('MstItem.unit_price', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt num'));?></td>
                    <td></td>
                    <th>定価</th>
                    <td><?php echo $this->form->input('MstItem.price', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt num'));?></td>
                </tr>
                <tr>
                    <th>課税区分</th>
                    <td><?php echo $this->form->input('MstItem.tax_type',array('options'=> $taxes, 'class' => 'txt','style'=>'width: 120px; height: 23px;','div'=>false,'label'=>false , 'empty'=>true)); ?></td>
                    <td></td>
                    <th>適用開始日</th>
                    <td>
                        <?php echo $this->form->input('MstItem.start_date', array('type'=>'text','div'=>false,'label'=>false,'class'=>'r validate[required,custom[date]]','id'=>'datepicker1')); ?>
                    </td>
                    <td></td>
                    <th>適用終了日</th>
                    <td>
                        <?php echo $this->form->input('MstItem.end_date', array('type'=>'text' ,'div'=>false,'label'=>false,'class'=>'txt validate[custom[date]]','id'=>'datepicker2')); ?>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td></td>
                    <th>商品カテゴリ</th>
                    <td>
                        <?php echo $this->form->input('MstItem.category_code', array('type'=>'hidden','id'=>'item_category_code_hidden_txt')); ?>
                        <?php echo $this->form->input('MstItem.item_category_txt', array('label'=>false,'div'=>false,'maxlength'=>50,'class'=>'txt','id'=>'item_category_name_txt')); ?>
                        <input type="button" id="call_search_item_category_form_btn" value="選択" />
                    </td>
                    <td></td>
                    <th>備考</th>
                    <td><?php echo $this->form->input('MstItem.recital', array('label'=>false,'div'=>false,'maxlength'=>200,'class'=>'txt')); ?></td>
                </tr>
            </table>
        </div>
    </div>
</form>