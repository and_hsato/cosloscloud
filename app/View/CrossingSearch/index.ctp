<script type="text/javascript">
$(document).ready(function(){
    //結果テーブルスクロール設定
    $('#crossing_search_table').tablefix({width: 1400,  fixCols: 1 });
  
     //種別が在庫のときは初期から日付項目を無効化
    if($("#type").val() == 3 || $("#type").val() == 6){
            $("#datepicker1").attr('disabled', 'disabled');
            $("#datepicker2").attr('disabled', 'disabled');
            $("#datepicker1").val('');
            $("#datepicker2").val('');
            $("#datepicker1").addClass("r_lbl");
            $("#datepicker2").addClass("r_lbl");
            $("#datepicker1").removeClass("txt validate[custom[date]]");
            $("#datepicker2").removeClass("txt validate[custom[date]]");
    }
    //チェックボックス一括処理
    $('.checkAll').click(function(){
        $('#search_form input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
    //商品検索ボタン押下
    $("#item_search").click(function(){
        $("#is_cross_search").val(0);
        $("#is_search").val(1);
        $("#search_form").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/');
        $("#search_form").attr('method', 'post');
        $("#search_form").submit();
    });
    //検索ボタン押下
    $("#search").click(function(){
        if($('input[type=checkbox].chk:checked').length > 1 ){
            alert("画面表示する場合、1商品のみ選択できます。");
            return false;
        }else if($('input[type=checkbox].chk:checked').length == 0 ){
            alert("商品を選択してください。");
            return false;
        }else if($('input[type=checkbox].chk:checked').length == 1 ){
            $("#is_cross_search").val(1);
            $("#is_search").val(0);
            $("#search_form").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/#crossingSearchList');
            $("#search_form").attr('method', 'post');
            $("#search_form").submit();
        }
    });
    //CSVボタン押下
    $("#csv").click(function(){
        if($('input[type=checkbox].chk:checked').length != 0 ){
            $("#search_form").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/');
            $("#search_form").attr('method', 'post');
            $("#search_form").submit();
        }else{
            alert("商品を選択してください。");
            return false;
        } 
    });
    //種別変更
    $("#type").change(function(){
        if($(this).val() == 3 || $(this).val() == 6){
            //在庫は日付を見ないので日付項目を無効化
            $("#datepicker1").attr('disabled', 'disabled');
            $("#datepicker2").attr('disabled', 'disabled');
            $("#datepicker1").val('');
            $("#datepicker2").val('');
            $("#datepicker1").addClass("r_lbl");
            $("#datepicker2").addClass("r_lbl");
            $("#datepicker1").removeClass("txt validate[custom[date]]");
            $("#datepicker2").removeClass("txt validate[custom[date]]");
            
        }else{
            //日付項目を有効化
            $("#datepicker1").removeAttr("disabled");
            $("#datepicker2").removeAttr("disabled");
            $("#datepicker1").removeClass("r_lbl");
            $("#datepicker2").removeClass("r_lbl");
            $("#datepicker1").addClass("txt validate[custom[date]]");
            $("#datepicker2").addClass("txt validate[custom[date]]");

        }
    });
});
</script>
<style>
<!--
div.TableHeaderAdjustment02 {
/*  overflow: scroll; */
/*  width: auto !important; */
}

div.TableHeaderAdjustment02 table{
  width:auto !important;
  margin: 0 0 10px;
  color: #000;
  font-size: 13px;
  border-style: solid;
  border-color:#005522;
  border-width: 1px;
  border-collapse: collapse;
}

div.TableHeaderAdjustment02 table > thead > tr > td {
  padding: 2px;
  white-space: nowrap;
  border-color: #059D9D;
  border-style:solid;
  border-width: 1px;
  color: #fff;
  background-color: #07A2A2;
  background-image: none;
  background-repeat: repeat-x;
  background-position: bottom;
  font-weight: bold;
  text-align: center;
}

div.TableHeaderAdjustment02 table > tbody > tr > td {
  font-size: 12px;
  white-space: nowrap;
  border-style:solid;
  border-width: 1px;
  border-color: #059D9D;
  padding:2px;
}  
-->
</style>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>横断検索</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>横断検索</span></h2>
<form class="validate_form search_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>" id="search_form">
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('search.is_cross_search',array('type'=>'hidden','id' =>'is_cross_search' , 'value'=>0));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>ID</th>
                <td><?php echo $this->form->text('search.awid',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
                <th>マスタID</th>
                <td><?php echo $this->form->text('search.master_id',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>商品ID</th>
                <td><?php echo $this->form->text('search.internal_code',array('class'=>'txt search_upper search_internal_code','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JMDNコード</th>
                <td><?php echo $this->form->input('search.jmdn_code',array('options'=>$JMDN_List,'class'=>'txt','style'=>'width:150px' , 'empty'=>'')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>製品番号</th>
                <td><?php echo $this->form->text('search.item_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>商品名</th>
                <td><?php echo $this->form->text('search.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('search.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>商品カテゴリ</th>
                <td><?php echo $this->form->input('search.itemcategory',array('options'=>$ItemCategory_List , 'class'=>'txt','style'=>'width:150px' , 'empty'=>'')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('search.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('search.jan_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>種別</th>
                <td><?php echo $this->form->input('search.type',array('options'=>array('1'=>'仕入(実績)', '2'=>'売上(実績)' ,'4'=>'仕入(マスタ)' , '5'=>'売上(マスタ)' , '3'=>'在庫(センターのみ)' , '6'=>'在庫(センター+病院)') , 'id'=>'type' , 'class'=>'txt','style'=>'width:150px')); ?></td>
                <td></td>
                <th>日付</th>
                <td>
                    <span>
                    <?php echo $this->form->input('search.work_date_from', array('label' => false, 'div' => 'false', 'id' => 'datepicker1', 'class' => 'date txt validate[custom[date]]')); ?>
                    &nbsp;&nbsp;～&nbsp;&nbsp;
                    <?php echo $this->form->input('search.work_date_to', array('label' => false, 'div' => 'false', 'id' => 'datepicker2', 'class' => 'date txt validate[custom[date]]')); ?>
                    </span>
                </td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center">
                <input type="button" id="item_search" class="btn btn1">
            </p>
        </div>
        <table style="width: 100%;">
            <tr>
                <td></td>
                <td align="right"></td>
            </tr>
        </table>
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult) )) ?>
        <div class="TableScroll">
            <table class="TableStyle01">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th style="width: 20px;"><input type ="checkbox" class="checkAll"></th>
                    <th style="width: 135px;">ID</th>
                    <th>商品名</th>
                    <th>規格</th>
                    <th>製品番号</th>
                    <th>販売元</th>
                </tr>

                <?php $i=0;foreach($SearchResult as $r){ ?>
                <tr class="<?php echo ($i%2 == 0)?'':'odd';?>">
                    <td style="width: 20px;" class="center">
                    <?php echo $this->form->input('aw.awid.'.$i , array('type' => 'checkbox' , 'class'=>'chk' , 'value' => $r['aw']['awid'] ,  'hiddenField'=>false ))?>
                    </td>
                    <td style="width: 135px;"><?php echo h_out($r['aw']['awid'],'center'); ?></td>
                    <td><?php echo h_out($r['aw']['item_name'],'center'); ?></td>
                    <td><?php echo h_out($r['aw']['standard']); ?></td>
                    <td><?php echo h_out($r['aw']['item_code']); ?></td>
                    <td><?php echo h_out($r['aw']['dealer_name']); ?></td>
                </tr>
                <?php $i++;} ?>
                <?php if(count($SearchResult)==0 && isset($this->request->data['search']['is_search'])){ ?>
                <tr><td colspan = '6' class="center"> 該当データはありません </td></tr>
                <?php } ?>
            </table>
        </div>

        <?php if (count($SearchResult) > 0){ ?>
        <table style="width: 100%;">
            <tr>
                <td></td>
                <td align="right"></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="search"/>
                <input type="button" class="btn btn5" id="csv"/>
            </p>
        </div>
        <?php } ?>
    </div>
</form>

<?php if(isset($CrossingSearchResult)){?>
<a name='crossingSearchList'>
    <h2 class="HeaddingLarge"><span>横断検索結果</span></h2>
</a>
<div class="TableHeaderAdjustment02">
    <div>
        <table>
            <thead>
                <tr>
                    <td>マスタID</td>
                    <td>商品名</td>
                    <td>規格</td>
                    <td>製品番号</td>
                    <td>販売元</td>
                    <td>JANコード</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $CrossingSearchResult[0][0]['マスタID']; ?></td>
                    <td><?php echo $CrossingSearchResult[0][0]['商品名']; ?></td>
                    <td><?php echo $CrossingSearchResult[0][0]['規格']; ?></td>
                    <td><?php echo $CrossingSearchResult[0][0]['製品番号']; ?></td>
                    <td><?php echo $CrossingSearchResult[0][0]['販売元']; ?></td>
                    <td><?php echo $CrossingSearchResult[0][0]['JANコード']; ?></td>
                </tr>
            </tbody>
        </table>
    </div>

    <table id="crossing_search_table">
        <thead>
        <tr>
        <?php $flg=false ; foreach($header as $h){ ?>
            <?php if($flg || $h == '包装単位'){$flg=true;} ?>
            <?php if($flg){ ?>
            <?php $tmp = explode( '_' , $h ); ?>
            <?php if(isset($tmp[1])){ ?>
            <?php if($tmp[1] == '数量' ){ ?>
            <?php if($this->request->data['search']['type']%3==0){ ?>
            <td colspan="3">
            <?php }else{ ?>
            <td colspan="4">
            <?php } ?>
                <?php echo $tmp[0] ?>
            <?php } ?>
            <?php }else{ ?>
            <td rowspan="2">
                <?php echo $h ?>
            <?php } ?>
            </td>
            <?php } ?>
        <?php } ?>
        </tr>

        <tr>
        <?php foreach($header as $h){ ?>
            <?php $tmp = explode( '_' , $h ); ?>
            <?php if(isset($tmp[1])){ ?>
            <td>
                <?php echo $tmp[1] ?>
            </td>
            <?php } ?>
        <?php } ?>
        </tr>
        </thead>

        <tbody>
        <?php $i=0;foreach($CrossingSearchResult as $cr){ ?>
        <tr style="<?php echo ($i%2 == 0)?'':'background: #DEE3DB;';?>">
        <?php $flg=false ;foreach($header as $h ){ ?>
            <?php if($flg || $h=='包装単位'){$flg=true;} ?>
            <?php if($flg){ ?>
            <td>
                <?php echo $cr[0][$h] ?>
            </td>
            <?php } ?>
        <?php } ?>
        </tr>
        <?php $i++;} ?>
        </tbody>
    </table>
</div>
<?php } ?>
