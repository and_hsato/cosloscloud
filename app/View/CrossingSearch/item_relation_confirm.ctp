<script type="text/javascript">
    $(document).ready(function(){
        $('.checkAll').click(function(){
            $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
        });
        $("#btn_add_result").click(function(){
            if($('input[type=checkbox].chk:checked').length === 0){
                alert('登録を行う商品を選択してください');
                return false;
            }else{
                $("#form_add_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/item_relation_result').submit();
            }
        });
    });

</script>
<div id="TopicPath">
    <ul>
        <li style=""><a href="<?php echo $this->webroot ?>login/home" style="" tabindex=-1>TOP</a></li>
        <li>GMメンテ</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>GMメンテ</span></h2>
<h2 class="HeaddingMid">以下の登録を行います。</h2>

<form class="validate_form" id="form_add_result" action="#" method="post">
    <hr class="Clear" />
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
        <span class="DisplaySelect">
            　表示件数：<?php echo count($FacilityItemList);  ?>件
        </span>

    </div>
    <div class="TableScroll" id="containTables">
        <table class="TableStyle01" id="inputTable" border=0 style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
            <thead>
                <tr>
                    <th rowspan="2" class="center" ><input type="checkbox" class="checkAll"></th>
                    <th class="col15">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col20">規格</th>
                    <th class="col20">製品番号</th>
                    <th class="col20">販売元</th>
                </tr>
                <tr>
                    <th></th>
                    <th>センター</th>
                    <th>ID</th>
                    <th>マスタID</th>
                    <th>JANコード</th>
                </tr>
            </thead>
        </table>
        <table class="TableStyle01 table-even2">
            <?php $cnt=0; foreach($FacilityItemList as $_row) { ?>
            <tr>
                <td rowspan='2' class="center">
                    <?php echo $this->form->checkbox("id.{$_row['aw']['id']}", array('class'=>'center chk','value'=>$_row["aw"]["item_id"] , 'style'=>'width:20px;text-align:center;' , 'hiddenField'=>false) ); ?>
                </td>
                <td class="col15"><?php echo h_out($_row['aw']['internal_code'],'center'); ?></td>
                <td class="col20"><?php echo h_out($_row['aw']['item_name']) ; ?></td>
                <td class="col20"><?php echo h_out($_row['aw']['standard']); ?></td>
                <td class="col20"><?php echo h_out($_row['aw']['item_code']); ?></td>
                <td class="col20"><?php echo h_out($_row['aw']['dealer_name']); ?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($_row['aw']['facility_name'] ); ?></td>
                <td>
                    <?php echo $this->form->input('awid.'.$_row['aw']['id'] , array('class'=>'txt r' , 'value'=>$_row['aw']['awid'])); ?>
                </td>
                <td><?php echo h_out($_row['aw']['master_id']); ?></td>
                <td><?php echo h_out($_row['aw']['jan_code']); ?></td>
             </tr>
        <?php $cnt++; } ?>
        </table>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
        <input type="button" class="btn btn2 [p2]" id="btn_add_result" name="btn_add_result"/>
    </div>
</form>
