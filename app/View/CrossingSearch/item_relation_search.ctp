<script type="text/javascript">

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#facilityItemForm #searchForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#facilityItemForm #cartForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });

    //検索ボタン押下
    $("#btn_Search").click(function(){
        var tmpid = '';
        $("#facilityItemForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);

        $("#facilityItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/item_relation_search/');
        $("#facilityItemForm").submit();
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        $('#itemSelectedTable').empty();
        $("#facilityItemForm #searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
                cnt++;
            }
        });
        if(cnt === 0 ){
            alert('商品を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        //選択チェック
        var cnt = 0;
        var tmpid = '';
        $("#facilityItemForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);
        if(cnt === 0){
            alert('追加する商品を選択してください');
        }else{
            $("#facilityItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/item_relation_confirm');
            $("#facilityItemForm").submit();
        }
    });

});

function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

</script>

<div id="TopicPath">
    <ul>
        <li style=""><a href="<?php echo $this->webroot ?>login/home" style="">TOP</a></li>
        <li>GMメンテ</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>GMメンテ</span></h2>
<h2 class="HeaddingMid">メンテナンスを行う商品を選択してください</h2>
<?php echo $this->form->create( 'search',array('type' => 'post','action' => '' ,'id' =>'facilityItemForm','class' => 'validate_form search_form') ); ?>
    <?php echo $this->form->input('add_search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('cart.tmpid',array('type'=>'hidden','id' =>'tmpid'));?>
    <h2 class="HeaddingSmall">検索条件</h2>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>ID</th>
                <td><?php echo $this->form->text('search.awid',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
                <th>マスタID</th>
                <td><?php echo $this->form->text('search.master_id',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>商品ID</th>
                <td><?php echo $this->form->text('search.internal_code',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JMDNコード</th>
                <td><?php echo $this->form->input('search.jmdn_code',array('options'=>$JMDN_List,'class'=>'txt','style'=>'width:150px' , 'empty'=>'')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('search.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('search.item_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('search.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>商品カテゴリ</th>
                <td><?php echo $this->form->input('search.itemcategory',array('options'=>$ItemCategory_List , 'class'=>'txt','style'=>'width:150px' , 'empty'=>'')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('search.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('search.jan_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>センター</th>
                <td> <?php echo $this->form->input('search.mst_facility_id',array('options'=> $Facility_List, 'class'=>'txt','style'=>'width:150px;' , 'empty'=>'')); ?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>

        <div class="ButtonBox">
           <p class="center">
             <input type="button" value="" class="btn btn1" id="btn_Search"/>
           </p>
        </div>
    </div>
    <div align="right" id="pageTop" ></div>

<?php
    if(isset($this->request->data['add_search']['is_search'])){
      if(count($SearchResult) === 0){
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th rowspan="2" class="center" ><input type="checkbox" class="checkAll_1"></th>
            <th class="col15">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">規格</th>
            <th class="col20">製品番号</th>
            <th class="col20">販売元</th>
          </tr>
          <tr>
            <th></th>
            <th>センター</th>
            <th>ID</th>
            <th>マスタID</th>
            <th>JANコード</th>
          </tr>
        </table>
      </div>
      <span>該当するデータはありません。</span>
<?php
      }else{
?>
    <div id="results">
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th rowspan="2" class="center" ><input type="checkbox" class="checkAll_1"></th>
            <th class="col15">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">規格</th>
            <th class="col20">製品番号</th>
            <th class="col20">販売元</th>
          </tr>
          <tr>
            <th></th>
            <th>センター</th>
            <th>ID</th>
            <th>マスタID</th>
            <th>JANコード</th>
          </tr>
        </table>
      </div>

      <div class="TableScroll" style="" id="searchForm">
        <?php $_cnt=0;foreach($SearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row["aw"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php echo "itemSelectedRow".$_row["aw"]['id'];?> >
            <td rowspan='2' class="center">
              <?php echo $this->form->checkbox("SelectedID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row["aw"]["id"] , 'style'=>'width:20px;text-align:center;' , 'hiddenField'=>false) ); ?>
            </td>
            <td class="col15"><?php echo h_out($_row['aw']['internal_code'],'center'); ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['item_name']) ; ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['standard']); ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['item_code']); ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['dealer_name']); ?></td>
          </tr>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" >
            <td></td>
            <td><?php echo h_out($_row['aw']['facility_name']); ?></td>
            <td><?php echo h_out($_row['aw']['awid']); ?></td>
            <td><?php echo h_out($_row['aw']['master_id']); ?></td>
            <td><?php echo h_out($_row['aw']['jan_code']); ?></td>
          </tr>
        </table>
        <?php $_cnt++;endforeach;?>
      </div>

      <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
          <input type="button" value="" class="btn btn4" id="cmdSelect" />
        </p>
      </div>
    </div>
<?php
      }
?>

    <?php if(isset($this->request->data['add_search']['is_search'])){?>
    <h2 class="HeaddingSmall">選択商品</h2>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th rowspan="2"><input type="checkbox" class="checkAll_2" checked></th>
          
            <th class="col15">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">規格</th>
            <th class="col20">製品番号</th>
            <th class="col20">販売元</th>
          </tr>
          <tr>
            <th></th>
            <th>センター</th>
            <th>ID</th>
            <th>マスタID</th>
            <th>JANコード</th>
          </tr>
        </table>
    </div>
    <div class="TableScroll" style="" id="cartForm">
      <?php if(isset($CartSearchResult)){ ?>
      <?php $_cnt=0;foreach($CartSearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row["aw"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php echo "itemSelectedRow".$_row["aw"]['id'];?> >
            <td rowspan='2' class="center">
              <?php echo $this->form->checkbox("SelectedID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row["aw"]["id"] , 'style'=>'width:20px;text-align:center;' ,'checked'=>true , 'hiddenField'=>false) ); ?>
            </td>
            <td class="col15"><?php echo h_out($_row['aw']['internal_code'],'center'); ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['item_name']) ; ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['standard']); ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['item_code']); ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['dealer_name']); ?></td>
          </tr>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" >
            <td></td>
            <td><?php echo h_out($_row['aw']['facility_name']) ; ?></td>
            <td><?php echo h_out($_row['aw']['awid']); ?></td>
            <td><?php echo h_out($_row['aw']['master_id']); ?></td>
            <td><?php echo h_out($_row['aw']['jan_code']); ?></td>
          </tr>
        </table>
      <?php $_cnt++;endforeach;?>
      <?php }?>
      <div id="itemSelectedTable"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" class="btn btn3"/>
      </p>
    </div>
    <?php }?>
<?php
    }else{
      //検索実行フラグがfalse
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>0)); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th rowspan="2"><input type="checkbox" class="checkAll_1"></th>
          
            <th class="col15">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">規格</th>
            <th class="col20">製品番号</th>
            <th class="col20">販売元</th>
          </tr>
          <tr>
            <th></th>
            <th>センター</th>
            <th>ID</th>
            <th>マスタID</th>
            <th>JANコード</th>
          </tr>
        </table>

      </div>
<?php
    }
    echo $this->form->end();

?>
<div align="right" id="pageDow" ></div>
