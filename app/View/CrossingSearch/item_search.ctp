<script type="text/javascript">
<!--
$(function(){
    $("#search_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/item_search');
        $("#search_form").attr('method', 'post');
        $("#search_form").submit();
    });

    $("#items_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/item_confirm');
        $("#search_form").attr('method', 'post');
        $("#search_form").submit();
    });
});

//-->
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>GMメンテ</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>GM一覧</span></h2>
<h2 class="HeaddingMid">商品を選択してください</h2>
<form class="validate_form search_form" id="search_form">
    <?php echo $this->form->input('search.is_search' , array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <div class="ColumnLeft">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>ID</th>
                    <td><?php echo $this->form->text('search.awid',array('class' => 'txt search_canna','maxlength' => '50' ,'label' =>'') ); ?></td>
                    <td></td>
                    <th>JANコード</th>
                    <td><?php echo $this->form->text('search.jan_code' ,array('class' => 'txt','maxlength' => '50' ,'label' => '')) ; ?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->text('search.item_name',array('class' => 'txt search_canna','maxlength' => '50', 'label' =>'') );?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->text('search.item_code',array('class' => 'txt search_upper','maxlength' => '50','label' =>'' ));?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->text('search.standard',array('class' => 'txt search_canna','maxlength' => '50' ,'label' =>'') ); ?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->text('search.dealer_name',array('class' =>'txt','maxlength' => '50', 'label' =>'' )); ?></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" value="" class="btn btn1" id="search_btn" />
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <?php echo $this->element('limit_combobox',array('result'=>count($result))) ; ?>
    <div class="TableScroll">
        <table class="TableStyle01">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th style="width: 20px;"></th>
                <th style="width: 135px;">ID</th>
                <th>商品名</th>
                <th>規格</th>
                <th>製品番号</th>
                <th>販売元</th>
                <th>JANコード</th>
            </tr>
            <?php $i = 0; foreach ($result as $r){ ?>
            <tr class="<?php echo ($i%2 == 0)?'':'odd';?>">
                <td style="width: 20px;" class="center">
                    <input type="radio" name="data[MstItem][id]" value="<?php echo $r['aw']['id']; ?>" />
                </td>
                <td style="width: 135px;"><?php echo h_out($r['aw']['awid'],'center'); ?></td>
                <td><?php echo h_out($r['aw']['item_name']); ?></td>
                <td><?php echo h_out($r['aw']['standard']); ?></td>
                <td><?php echo h_out($r['aw']['item_code']); ?></td>
                <td><?php echo h_out($r['aw']['dealer_name']); ?></td>
                <td><?php echo h_out($r['aw']['jan_code'],'center'); ?></td>
            </tr>
            <?php $i++; } ?>
            <?php if(isset($this->request->data['search']['is_search']) && count($result) == 0){ ?>
            <tr><td colspan="7" class="center">該当するデータはありません。</td></tr>
            <?php } ?>
        </table>
    </div>
    <?php if (count($result) != 0){ ?>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="ButtonBox">
        <input type="button" class="btn btn4" id="items_btn" />
    </div>
    <?php } ?>
</form>
