<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>">TOP</a></li>
        <li>GMメンテ</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>GMメンテ登録確認</span></h2>
<div class="Mes01"><?php echo $msg ?></div>
<hr class="Clear" />
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td>表示件数：<?php echo count($result); ?>件</td>
        <td align="right">
        </td>
    </tr>
</table>
<div class="TableHeaderAdjustment01">
    <table class="TableHeaderStyle01">
        <tr>
            <th rowspan="2" class="center" ></th>
            <th class="col15">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">規格</th>
            <th class="col20">製品番号</th>
            <th class="col20">販売元</th>
        </tr>
        <tr>
            <th></th>
            <th>センター</th>
            <th>ID</th>
            <th>マスタID</th>
            <th>JANコード</th>
        </tr>
    </table>
</div>
<div class="TableScroll">
    <table class="TableStyle01 table-even2">
        <?php $cnt=0; foreach($result as $_row) { ?>
        <tr>
            <td rowspan='2' class="center"></td>
            <td class="col15"><?php echo h_out($_row['aw']['internal_code'],'center'); ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['item_name']) ; ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['standard']); ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['item_code']); ?></td>
            <td class="col20"><?php echo h_out($_row['aw']['dealer_name']); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo h_out($_row['aw']['facility_name']) ; ?></td>
            <td><?php echo h_out($_row['aw']['awid']); ?></td>
            <td><?php echo h_out($_row['aw']['master_id']); ?></td>
            <td><?php echo h_out($_row['aw']['jan_code']); ?></td>
        </tr>
        <?php $cnt++;} ?>
    </table>
</div>
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
