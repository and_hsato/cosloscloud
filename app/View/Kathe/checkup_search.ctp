<script>
$(document).ready(function(){
    //検索ボタン押下
    $("#btn_Search").click(function(){
        validateDetachSubmit($("#CheckUpList") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/checkupSearch');
    });
    
    //新規ボタン押下
    $("#btn_Add").click(function(){
        validateDetachSubmit($('#CheckUpList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/checkupMod' );
    });
    
    //編集ボタン押下
    $("#btn_Mod").click(function(){
        var id = $("input[name='data[CheckUp][id]']:checked").val();
        $("#CheckUpList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/checkupMod/'+id).submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>検査名一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">検査名一覧</h2>

<form class="validate_form search_form" method="post" action="" accept-charset="utf-8" id="CheckUpList">
    <?php echo $this->form->input('KatheCheckup.is_search' , array('type'=>'hidden'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>検査名</th>
                <td><?php echo $this->form->input('CheckUp.search_name' , array('type'=>'text' , 'class'=>'txt'))?></td>
                <td></td>
            </tr>

        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search" />
        <input type="button" class="btn btn11 [p2]" id="btn_Add" />
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col width="25" />
                    <col width="50" />
                    <col />
                </colgroup>
                <thead>
                <tr>
                    <th></th>
                    <th>ID</th>
                    <th>検査名</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($result as $r) { ?>
                    <tr>
                        <td class="center">
                            <input name="data[CheckUp][id]" type="radio" class="validate[required]" id="checkUp<?php echo $r['CheckUp']['id']; ?>" value="<?php echo $r['CheckUp']['id']; ?>"/>
                        </td>
                        <td><?php echo h_out($r['CheckUp']['id'],'right'); ?></td>
                        <td><?php echo h_out($r['CheckUp']['name']); ?></td>
                    </tr>
                <?php } ?>
                </tbody>
                <?php if(count($result) == 0 && isset($this->request->data['KatheCheckup']['is_search'])){ ?>
                <tr><td colspan="3" class="center">該当するデータがありませんでした。</td></tr>
                <?php } ?>
            </table>
        </div>
    </div>
    <?php if(count($result)>0){ ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn9" id="btn_Mod" />
    </div>
    <?php } ?>
</form>
