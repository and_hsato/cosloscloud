<script type="text/javascript">
$(document).ready(function() {
    $("select#KatheDisease").multiselect({
        selectedList: 100,
        checkAllText: "全選択",
        uncheckAllText: "全選択解除",
        noneSelectedText: "未選択です",
        selectedText: "# 個選択",
        multiple:true
    });
  
    $("select#KatheCheckup").multiselect({
        selectedList: 100,
        checkAllText: "全選択",
        uncheckAllText: "全選択解除",
        noneSelectedText: "未選択です",
        selectedText: "# 個選択",
        multiple:true
    });
  
    var method_count = <?php echo count($methods); ?>;
    var item_count = <?php echo count($items); ?>;
  
    // 術式入力ENTER
    $('#input_method_code').keyup(function(e){
        if(e.which === 13) { //エンターキー押下
            fieldName = 'method_name';
            fieldValue = $('#input_method_code').val();
            fieldValue = FHConvert.ftoh(FHConvert.fkktohkk(fieldValue),{jaCode:true});
            $('#input_method_code').val('');
            $.post(basepath+"/search_kathe_method/",
            {
                field: fieldName,
                value: fieldValue
            },
            function (data) {
                var obj = jQuery.parseJSON(data);
                $.each(obj, function(index, o) {
                    $('#input_method_line').append(
                         $('<tr>').append(
                             $('<td>').append('<input type="hidden" name="data[TrnKathe][methods_mst_id]['+ method_count +'] " value="' + o.id + '">')
                         ).append(
                             $('<td>').append(
                                 $('<label>').text(o.kathe_method_code).attr('title' , o.kathe_method_code)
                             )
                         ).append(
                             $('<td>').append(
                                 $('<label>').text(o.kathe_method_name +' / '+ o.kathe_method_comment).attr('title' , o.kathe_method_name +' / '+ o.kathe_method_comment)
                             )
                         ).append(
                             $('<td class="right">').append(
                                 $('<label >').attr('title' , o.kathe_method_point).append(
                                     $('<p class="right method_point">').text(o.kathe_method_point)
                                 )
                             )
                         ).append(
                             $('<td class="center">').append(
                                 $('<input type="button" value="DEL" class="method_del_btn">')
                             )
                         )
                    )
                    method_count = method_count + 1;
                    getMethodPoint();
                    //追加したボタンにイベントを振りなおす
                    $('.method_del_btn').click(function(){
                        $(this).parent().parent().remove();
                        getMethodPoint();
                    });
                });
            },
            function (success) {
            },
            function(error) {
            });
            return false;
        }
    });

    // 材料入力ENTER
    $('#input_item_code').keyup(function(e){
        if(e.which === 13) { //エンターキー押下
            fieldName = 'code';
            fieldValue = $('#input_item_code').val();
            fieldValue = FHConvert.ftoh(FHConvert.fkktohkk(fieldValue),{jaCode:true});
            $('#input_item_code').val('');
            $.post(basepath+"/search_kathe_item/",
            {
                field: fieldName,
                value: fieldValue
            },
            function (data) {
                var obj = jQuery.parseJSON(data);
                $.each(obj, function(index, o) {
                if($('#input_item_line tr.item_count').length){
                    $('#input_item_line tr.item_count:first').before(
                        $('<tr class="item_count">').append(
                            $('<td>').append('<input type="hidden" name="data[TrnKathe][item_mst_id]['+ item_count +'] " value="' + o.id + '">')
                        ).append(
                            $('<td>').append(
                                $('<label>').attr('title' , o.code).append(
                                    $('<p class="internal_code">').text(o.code)
                                )
                            )
                        ).append(
                            $('<td>').append(
                                $('<label>').text(o.item_name + ' ' +o.standard + ' ' + o.item_code).attr('title' , o.item_name + ' ' +o.standard + ' ' + o.item_code)
                            )
                        ).append(
                            $('<td>').append(
                                $('<input type="button" value="+" class="item_plus_btn" style="width:25px;">')
                            ).append(
                                $('<input type="button" value="-" class="item_minus_btn" style="width:25px;">')
                            ).append(
                                $('<input type="text" name="data[TrnKathe][quantity]['+ item_count +']"  value="1" class="right quantity" style="width:30px;" readonly="readonly">')
                            ).append(
                                $('<span>').text(o.unit_name)
                            )
                        ).append(
                            $('<td class="center">').append(
                                $('<input type="button" value="DEL" class="item_del_btn">')
                            )
                        ).append(
                            $('<td class="right">').append(
                                $('<label >').attr('title' , o.point).append(
                                    $('<p class="right item_point">').text(o.point)
                                )
                            )
                        ).append(
                            $('<td class="right">').append(
                                $('<label >').attr('title' , o.price).append(
                                    $('<p class="right">').text(o.price)
                                )
                            )
                        ).append(
                            $('<td class="center">').append(
                                $('<select name="data[TrnKathe][use_type]['+ item_count +']" class="txt use_type">').append(
                                    $('<option value="1" selected="selected">').text("使用")
                                ).append(
                                    $('<option value="2">').text("失敗")
                                ).append(
                                    $('<option value="3">').text("サンプル")
                                )
                            )
                        )
                    )

                    }else{
                    $('#input_item_line').append(
                        $('<tr class="item_count">').append(
                            $('<td>').append('<input type="hidden" name="data[TrnKathe][item_mst_id]['+ item_count +'] " value="' + o.id + '">')
                        ).append(
                            $('<td>').append(
                                $('<label>').attr('title' , o.code).append(
                                    $('<p class="internal_code">').text(o.code)
                                )
                            )
                        ).append(
                            $('<td>').append(
                                $('<label>').text(o.item_name + ' ' +o.standard + ' ' + o.item_code).attr('title' , o.item_name + ' ' +o.standard + ' ' + o.item_code)
                            )
                        ).append(
                            $('<td>').append(
                                $('<input type="button" value="+" class="item_plus_btn" style="width:25px;">')
                            ).append(
                                $('<input type="button" value="-" class="item_minus_btn" style="width:25px;">')
                            ).append(
                                $('<input type="text" name="data[TrnKathe][quantity]['+ item_count +']"  value="1" class="right quantity" style="width:30px;" readonly="readonly">')
                            ).append(
                                $('<span>').text(o.unit_name)
                            )
                        ).append(
                            $('<td class="center">').append(
                                $('<input type="button" value="DEL" class="item_del_btn">')
                            )
                        ).append(
                            $('<td class="right">').append(
                                $('<label >').attr('title' , o.point).append(
                                    $('<p class="right item_point">').text(o.point)
                                )
                            )
                        ).append(
                            $('<td class="right">').append(
                                $('<label >').attr('title' , o.price).append(
                                    $('<p class="right">').text(o.price)
                                )
                            )
                        ).append(
                            $('<td class="center">').append(
                                $('<select name="data[TrnKathe][use_type]['+ item_count +']" class="txt use_type">').append(
                                    $('<option value="1" selected="selected">').text("使用")
                                ).append(
                                    $('<option value="2">').text("失敗")
                                ).append(
                                    $('<option value="3">').text("サンプル")
                                )
                            )
                        )
                    )
                    }
                    item_count = item_count + 1;
                    //追加したボタンにイベントを振りなおす
                    $('.item_del_btn').click(function(){
                        $(this).parent().parent().remove();
                        getItemPoint();
                        setItemCount();
                    });
                    // 加算処理が2重に定義されるのでいったんイベントを削除する
                    $(".item_plus_btn").unbind();
                    $('.item_plus_btn').click(function(){
                        var quantity = $(this).parent().find('.quantity');
                        quantity.val(parseInt(quantity.val()) + 1);
                        getItemPoint();
                    });
                    // 減算処理が2重に定義されるのでいったんイベントを削除する
                    $(".item_minus_btn").unbind();
                    $('.item_minus_btn').click(function(){
                        var quantity = $(this).parent().find('.quantity');
                        quantity.val(parseInt(quantity.val()) - 1);
                        if(parseInt(quantity.val()) < 1 ){
                            $(this).parent().parent().remove();
                            setItemCount();
                        }
                        getItemPoint();
                    });
                    $('.use_type').change(function(){
                        getItemPoint();
                    });
                    getItemPoint();
                    setItemCount();
                });
            },
            function (success) {
            },
            function(error) {
            });
            return false;
        }
    });


    // 術式削除
    $('.method_del_btn').click(function(){
        $(this).parent().parent().remove();
        getMethodPoint();
    });

    // 材料削除
    $('.item_del_btn').click(function(){
        $(this).parent().parent().remove();
        getItemPoint();
        setItemCount();
    });

    // 材料加算
    $('.item_plus_btn').click(function(){
        var quantity = $(this).parent().find('.quantity');
        quantity.val(parseInt(quantity.val()) + 1);
        getItemPoint();
    });

    // 材料減算
    $('.item_minus_btn').click(function(){
        var quantity = $(this).parent().find('.quantity');
        quantity.val(parseInt(quantity.val()) - 1);
        if(parseInt(quantity.val()) < 1 ){
            $(this).parent().parent().remove();
            setItemCount();
        }
        getItemPoint();
    });

    // 使用区分変更
    $('.use_type').change(function(){
        getItemPoint();
    });

    //削除ボタン押下
    $('#delete_btn').click(function(){
        if(window.confirm("取消を実行します。よろしいですか？")){
            var id = $('#kathe_id').val();
            $("#kathe_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/del/' + id).submit();
        }
    });

    //登録ボタン押下
    $('#submit_btn').click(function(){
        //登録前チェック
        // 検査日必須
        if($('#KatheDate').val() == "" ){
            alert('検査日が未入力です。');
            $('#KatheDate').focus();
            return false ;
        }
        // 検査日日付フォーマット
        // 区分必須
        if($('#KatheType').val() == "" ){
            alert('区分が未選択です。');
            $('#KatheType').focus();
            return false ;
        }
        
        // 検査室必須
        if($('#KatheRoom').val() == "" ){
            alert('検査室が未選択です。');
            $('#KatheRoom').focus();
            return false ;
        }
        // 担当科必須
        if($('#departmentCode').val() == "" ){
            alert('担当科が未選択です。');
            $('#departmentCode').focus();
            return false ;
        }
        // 病棟必須
        if($('#wardCode').val() == "" ){
            alert('病棟が未選択です。');
            $('#wardCode').focus();
            return false ;
        }
        $("#kathe_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result').submit();
    });
    // 材料入力を非表示
    $("#Item_Area").hide();

    // 材料入力をトグル表示
    $("#item_edit_btn").toggle(
        function () {
            $('#Item_Area').show();
            $('#Input_Area').hide();
            $("#item_edit_btn").val('材料編集完了');
            $('#input_item_code').focus();
        },
        function () {
            $('#Item_Area').hide();
            $('#Input_Area').show();
            $("#item_edit_btn").val('材料編集');
        }
    );

    $(window).load(function () {
        getMethodPoint();
        getItemPoint();
        prettyPrint();
        setItemCount();
    });
});

    //術式合計点数を取得
    function getMethodPoint(){
        var total_method_point = 0;
        $('#Method_Area').find('.method_point').each(function(){
              total_method_point = total_method_point + parseInt($(this).text());
        });
        $('#method_point').val(numberFormat(total_method_point));
    }

    //材料合計点数を取得
    function getItemPoint(){
        var total_item_point = 0;
        $('#Item_Area').find('.item_point').each(function(){
            var quantity = $(this).parent().parent().parent().find('.quantity');
            var type = $(this).parent().parent().parent().find('.use_type');
            if(type.val() == 1){
                total_item_point = total_item_point + ( parseInt($(this).text()) * parseInt(quantity.val()) );
            }
        });
        $('#item_point').val(numberFormat(total_item_point));
    }
    //読み込み件数を設定
    function setItemCount(){
        $('#count').text('読込件数：' + $('#Item_Area tr.item_count').length + '件');
    }

</script>

<script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.multiselect.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/prettify.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/prettify.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/jquery.multiselect.css" />

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">カテ実施一覧</a></li>
        <li>カテ実施登録</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>カテ実施登録</span></h2>
<form class="validate_form" id="kathe_form"  method="post">
    <?php echo $this->form->input('TrnKathe.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnKathe.id' , array('type'=>'hidden' , 'id'=>'kathe_id')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <td><?php if(isset($this->request->data['TrnKathe']['id'])){ ?><input type="button" class="btn btn6 [p2]" id="delete_btn" /><?php } ?></td>
                <td>処置簿編集</td>
                <td><?php if(isset($this->request->data['TrnKathe']['id'])){ echo "編集" ; } else { echo "新規" ;}  ?></td>
                <td><input type="button" value="材料編集" id="item_edit_btn" /></td>
            </tr>
        </table>
        <div id="Input_Area">
        <table class="FormStyleTable">
            <tr>
                <th>検査日</th>
                <td><?php echo($this->form->text('TrnKathe.work_date', array('class' => 'date validate[required,custom[date]]', 'readonly'=>'readonly','maxlength' => '10' , 'id'=> 'KatheDate'))); ?></td>
                <th>区分</th>
                <td><?php echo $this->form->input('TrnKathe.type', array('options'=>array('1'=>'定時','2'=>'臨時','3'=>'時間外'), 'empty'=>false , 'style'=>'width:80px;' , 'class' => 'txt', 'id' => 'KatheType')); ?></td>
                <th>検査室番号</th>
                <td><?php echo $this->form->input('TrnKathe.room', array('options'=>$room, 'empty'=>false , 'class' => 'txt', 'style'=> 'width:60px;' , 'id' => 'KatheRoom')); ?></td>
                <th>検査コード</th>
                <td><?php echo $this->form->input('TrnKathe.code', array('type'=>'text','readonly'=>'readonly' , 'class' => 'txt', 'style'=>'width:80px;', 'id' => 'KatheCode')); ?></td>
            </tr>
        </table>
        <table class="FormStyleTable">
            <tr>
                <td colspan="5">
                    <table>
                        <tr>
                            <th>担当科</th>
                            <td>
                                <?php echo $this->form->input('TrnKathe.departmentCode',array('options'=>$department,'class'=>'txt','style'=>'width:180px;','id'=>'departmentCode' , 'empty'=>true)); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>病棟</th>
                            <td>
                                <?php echo $this->form->input('TrnKathe.wardCode',array('options'=>$ward,'class'=>'txt','style'=>'width:180px;','id'=>'wardCode' , 'empty'=>true)); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>患者番号</th>
                            <td><?php echo($this->form->text('TrnKathe.number', array('class' => 'txt', 'maxlength' => '10' , 'id'=>'KatheNumber'))); ?></td>
                        </tr>
                        <tr>
                            <th>患者氏名</th>
                            <td><?php echo($this->form->text('TrnKathe.name', array('class' => 'txt', 'maxlength' => '50','id'=>'KatheName'))); ?></td>
                        </tr>
                        <tr>
                            <th>生年月日</th>
                            <td><?php echo $this->form->input('TrnKathe.birth', array('type'=>'text', 'class' => 'date','readonly'=>'readonly', 'id' => 'KatheBirth')); ?></td>
                            <th>年齢</th>
                            <td><?php echo $this->form->input('TrnKathe.age', array('type'=>'text', 'class' => 'txt num validate[optional,custom[onlyNumber]]','style'=>'width:50px;', 'maxlength'=>'3','id' => 'KatheAge')); ?></td>
                        </tr>
                        <tr>
                            <th>性別</th>
                            <td><?php echo $this->form->input('TrnKathe.sex', array('options'=>array('1'=>'M','2'=>'F'), 'empty'=>true , 'style'=>'width:50px;', 'class' => 'txt', 'id' => 'KatheSex')); ?></td>
                            <th>血液型</th>
                            <td><?php echo $this->form->input('TrnKathe.blood', array('options'=>array('1'=>'A','2'=>'B','3'=>'O','4'=>'AB'), 'empty'=>true , 'class' => 'txt', 'style'=>'width:50px;', 'id' => 'KatheBlood')); ?></td>
                        </tr>
                    </table>
                </td>
                <td colspan="5" style="width:600px;"> 
                    <table class="FormStyleTable">
                        <tr>
                            <th>病名</th>
                            <td><?php echo $this->form->input('TrnKathe.disease', array('options'=>$mst_disease , 'value'=>$disease ,'multiple'=>'multiple','id' => 'KatheDisease')); ?></td>
                        </tr>
                        <tr>
                            <th>検査名</th>
                            <td><?php echo $this->form->input('TrnKathe.checkup', array('options'=>$mst_checkup , 'value'=>$checkup , 'multiple'=>'multiple','id' => 'KatheCheckup')); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th>術者</th>
                <td><?php echo $this->form->input('TrnKathe.person', array('options'=>$person, 'empty'=>true , 'class' => 'txt', 'id' => 'KathePerson')); ?></td>
                <th>入室</th>
                <td><?php echo $this->form->input('TrnKathe.inroomtime', array('type'=>'text' , 'class' => 'txt', 'id' => 'inroomtime','maxlength'=>10)); ?></td>
                <th>帰室</th>
                <td><?php echo $this->form->input('TrnKathe.outroomtime', array('type'=>'text', 'class' => 'txt', 'id' => 'outroomtime','maxlength'=>10)); ?></td>
                <th>麻酔種類</th>
                <td><?php echo $this->form->input('TrnKathe.anesthesia', array('options'=>$anesthesia, 'empty'=>false , 'class' => 'txt', 'id' => 'anesthesia')); ?></td>
                <th>体位</th>
                <td><?php echo $this->form->input('TrnKathe.position', array('options'=>$position, 'empty'=>false , 'class' => 'txt', 'id' => 'position')); ?></td>
            </tr>
            <tr>
                <th>術式点数計</th>
                <td><?php echo $this->form->input('TrnKathe.method_point', array('type'=>'text' , 'class' => 'lbl right', 'readonly'=>'readonly','id' => 'method_point')); ?></td>
                <th>材料点数計</th>
                <td><?php echo $this->form->input('TrnKathe.item_point', array('type'=>'text' , 'class' => 'lbl right', 'readonly'=>'readonly' , 'id' => 'item_point')); ?></td>
            </tr>
        </table>
        <div class="TableScroll" id="Method_Area">
            <table class="TableStyle01" id="input_method_line" border=0 style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
                <colgroup>
                    <col width="20px;"/>
                    <col width="100px;"/>
                    <col />
                    <col width="60px;"/>
                    <col width="50px"/>
                </colgroup>
                <tr>
                    <th></th>
                    <th>術式コード</th>
                    <th class="center">術式名 / 準用</th>
                    <th>保険点数</th>
                    <th>削除</th>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo $this->form->input('TrnKathe.method_code' , array('type'=>'text' ,'id'=>'input_method_code','class'=>'txt' , 'maxlength'=>'200'))?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <table class="TableStyle01" border=0 style="margin-top:-1px; padding:0px;">
                <colgroup>
                    <col width="20px;"/>
                    <col width="100px;"/>
                    <col />
                    <col width="60px;"/>
                    <col width="50px"/>
                </colgroup>
                <?php foreach($methods as $i => $m){ ?>
                <tr>
                    <td>
                        <?php echo $this->form->input('TrnKathe.methods_id.'.$i , array('type'=>'hidden' , 'value'=>$m['Method']['id'])); ?>
                        <?php echo $this->form->input('TrnKathe.methods_mst_id.'.$i , array('type'=>'hidden' , 'value'=>$m['Method']['mst_id'])); ?>
                    </td>
                    <td><?php echo h_out($m['Method']['code']); ?></td>
                    <td><?php echo h_out($m['Method']['name'] .' / '. $m['Method']['comment']); ?></td>
                    <td><?php echo h_out($m['Method']['point'] , 'right method_point'); ?></td>
                    <td class="center"><input type="button" value="DEL" class="method_del_btn" /></td>
                </tr>
                <?php } ?>
            </table>
        </div>
        </div>
        <div class="TableScroll" id="Item_Area">
            <div id="count"></div>
            <table class="TableStyle01" id="input_item_line" border=0 style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
                <colgroup>
                    <col width="20px;"/>
                    <col width="100px;"/>
                    <col />
                    <col width="130px;"/>
                    <col width="50px"/>
                    <col width="60px"/>
                    <col width="100px"/>
                    <col width="100px"/>
                </colgroup>
                <tr>
                    <th></th>
                    <th>材料コード</th>
                    <th class="center">材料名 / 規格サイズ / 製品番号</th>
                    <th>数量</th>
                    <th>削除</th>
                    <th>保険点数</th>
                    <th>金額</th>
                    <th>使用区分</th>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo $this->form->input('TrnKathe.item_code' , array('type'=>'text' ,'id'=>'input_item_code','class'=>'txt' , 'maxlength'=>'200'))?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <table class="TableStyle01" border=0 style="margin-top:-1px; padding:0px;">
                <colgroup>
                    <col width="20px;"/>
                    <col width="100px;"/>
                    <col />
                    <col width="130px;"/>
                    <col width="50px"/>
                    <col width="60px"/>
                    <col width="100px"/>
                    <col width="100px"/>
                </colgroup>
                <?php foreach($items as $l => $i){ ?>
                <tr class="item_count">
                    <td>
                        <?php echo $this->form->input('TrnKathe.item_id.'.$l , array('type'=>'hidden' , 'value'=>$i['Item']['id'])); ?>
                        <?php echo $this->form->input('TrnKathe.item_mst_id.'.$l , array('type'=>'hidden' , 'value'=>$i['Item']['mst_id'])); ?>
                    </td>
                    <td><?php echo h_out($i['Item']['code'],'internal_code'); ?></td>
                    <td><?php echo h_out($i['Item']['item_name'] .' '. $i['Item']['standard'] .' '. $i['Item']['item_code']); ?></td>
                    <td>
                        <input type="button" value="+" class="item_plus_btn" style="width:25px;"/>
                        <input type="button" value="-" class="item_minus_btn" style="width:25px;" />
                        <input type="text" name="data[TrnKathe][quantity][<?php echo $l ; ?>]"  value="<?php echo $i['Item']['quantity']; ?>" class="right quantity" style="width:30px;" readonly="readonly">
                        <?php echo $i['Item']['unit_name'] ?>
                    </td>
                    <td class="center"><input type="button" value="DEL" class="item_del_btn" /></td>
                    <td><?php echo h_out($i['Item']['point'] , 'right item_point'); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($i['Item']['price']), 'right'); ?></td>
                    <td><?php echo $this->form->input('TrnKathe.use_type.'.$l , array('options' => array('1'=>'使用' , '2'=> '失敗' , '3'=>'サンプル') , 'value'=> $i['Item']['use_type'] , 'empty'=>false , 'class'=>'txt use_type')); ?></td>
                </tr>
                <?php } ?>
            </table>
        </div>

        <div class="ButtonBox">
            <p class="center">
                <input type="button" id="submit_btn" class="btn btn29 submit [p2]" />
            </p>
        </div>
    </div>
</form>

