<script type="text/javascript">
$(function(){
    // 検索ボタン押下
    $("#search_btn").click(function(){
        validateDetachSubmit($('#search_form') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>' );
    });

    // 新規ボタン押下
    $("#add_btn").click(function(){
        validateDetachSubmit($('#search_form') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit/' );
    });
    // 選択ボタン押下
    $("#edit_btn").click(function(){
        if( $('.chk:checked').length > 0 ) {
            var id = $('input[name="data[TrnKatheHeader][id]"]:checked').val();
            $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit/'+id).submit();
        }else{
            alert('カテ実施が選択されていません。');
        }
    });
    $("#report_btn").click(function(){
        if( $('.chk:checked').length > 0 ) {
            $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/').submit();
        }else{
            alert('カテ実施が選択されていません。');
        }
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>カテ実施一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>カテ実施一覧</span></h2>
<form class="validate_form search_form" id="search_form" method="post">
    <?php echo $this->form->input('search.is_search' , array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <div class="ColumnLeft">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>検査コード</th>
                    <td><?php echo $this->form->input('TrnKatheHeader.search_code' ,array('type'=>'text' , 'class' => 'txt','maxlength' => '10' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th>検査日</th>
                    <td><?php echo $this->form->input('TrnKatheHeader.search_date',array('type'=>'text','class' => 'date','maxlength' => '10','label' =>'' ));?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>担当科</th>
                    <td><?php echo $this->form->input('TrnKatheHeader.search_department',array('options'=>$department,'class' => 'txt','empty' => true) );?></td>
                    <td></td>
                    <th>病棟</th>
                    <td><?php echo $this->form->input('TrnKatheHeader.search_ward',array('options'=>$ward ,'class' =>'txt','empty' => true)); ?></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="search_btn" />
        <input type="button" class="btn btn11 [p2]" id="add_btn" />
    </div>

    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <?php echo $this->element('limit_combobox',array('result'=>count($result))) ; ?>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25" />
                <col width="90px;"/>
                <col width="80px;"/>
                <col />
                <col />
                <col width="150px;"/>
                <col />
            </colgroup>
            <tr>
                <th></th>
                <th>検査日</th>
                <th>検査コード</th>
                <th>担当科</th>
                <th>病棟</th>
                <th>登録日時</th>
                <th>登録者</th>
            </tr>
            <?php $i = 0; foreach ($result as $r){ ?>
            <tr>
                <td class="center">
                    <input type="radio" name="data[TrnKatheHeader][id]" style="width:25px;" class="validate[required] chk center" id="id_<?php echo $r['TrnKatheHeader']['id']?>" value="<?php echo $r['TrnKatheHeader']['id']; ?>" />
                </td>
                <td><?php echo h_out($r['TrnKatheHeader']['work_date'],'center'); ?></td>
                <td><?php echo h_out($r['TrnKatheHeader']['work_no'],'center'); ?></td>
                <td><?php echo h_out($r['TrnKatheHeader']['department_name']); ?></td>
                <td><?php echo h_out($r['TrnKatheHeader']['ward_name']); ?></td>
                <td><?php echo h_out($r['TrnKatheHeader']['created'],'center'); ?></td>
                <td><?php echo h_out($r['TrnKatheHeader']['user_name']); ?></td>
            </tr>
            <?php $i++; } ?>
            <?php if(isset($this->request->data['search']['is_search']) && count($result) == 0){ ?>
            <tr><td colspan="7" class="center">該当するデータはありません。</td></tr>
            <?php } ?>
        </table>
    </div>

    <?php if (count($result) != 0){ ?>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="ButtonBox">
      <input type="button" class="btn btn4" id="edit_btn"/>
      <input type="button" id="report_btn" class="btn btn10 submit [p2]" />
    </div>
    <?php } ?>
</form>