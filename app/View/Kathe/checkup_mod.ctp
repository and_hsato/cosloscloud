<script>
$(document).ready(function(){
    $('#btn_result').click(function(){
        $('#ModForm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name?>/checkupResult').submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/checkupSearch">検査名一覧</a></li>
        <li>検査名編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">検査名編集</h2>

<form class="input_form validate_form" method="post" id="ModForm">
    <?php echo $this->form->input('CheckUp.id' , array('type'=>'hidden' , 'id'=>'checkup_id'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>検査名</th>
                <td>
                    <?php echo $this->form->input('CheckUp.name' , array('type'=>'text' , 'class'=>'txt r validate[required]' , 'maxlength'=>80)); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn2 [p2]" id="btn_result" />
    </div>
</form>
