<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/checkupSearch">検査名一覧</a></li>
        <li class="pankuzu">検査名編集</li>
        <li>検査名編集結果</li>
   </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">検査名編集</h2>
<div class="Mes01">以下の内容で設定しました</div>

<form method="post" action="" accept-charset="utf-8" >
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>検査名</th>
                <td>
                    <?php echo $this->form->input('CheckUp.name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</form>
