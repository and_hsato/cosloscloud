<script  type="text/javascript">

$(document).ready(function() { 

    $("#btn_Select").click(function(){
         $("#MstRepaymentInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/RepaymentList');
         $("#MstRepaymentInfo").submit();
    });

    $("#btn_Read").click(function(){
         $("#MstRepaymentInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/read');
         $("#MstRepaymentInfo").submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>返却登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>返却登録</span></h2>
<h2 class="HeaddingMid">読み込み方法を選択してください</h2>
    <form class="validate_form input_form" method="post" action="" accept-charset="utf-8" id="MstRepaymentInfo">
        <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col width="250" />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>返却日</th>
                    <td><?php echo $this->form->input('MstRepayment.work_date' , array('type'=>'text' , 'class'=>'validate[required,custom[date],funcCall2[date2]] date r' , 'id'=>'datepicker' ));?></td>
                    <td></td>
                    <th>作業区分</th>
                    <td>
                        <?php echo $this->form->input('MstRepayment.classId' , array('options'=>$class_list , 'class'=>'txt' , 'empty'=>true))?>
                        <?php echo $this->form->input('MstRepayment.className' , array('type'=>'hidden'))?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>得意先</th>
                    <td>
                        <?php echo $this->form->input('MstRepayment.facilityName', array('type' => 'hidden' , 'id'=>'facilityName')); ?>
                        <?php echo $this->form->input('MstRepayment.facilityText', array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'r', 'style' => 'width: 60px;')); ?>
                        <?php echo $this->form->input('MstRepayment.facilityCode' ,array('options'=>$facility_list , 'class'=>'r txt validate[required]' , 'id'=>'facilityCode' , 'empty'=>true))?>
                    </td>
                    <td></td>
                    <th>備考</th>
                    <td><?php echo $this->form->input('MstRepayment.recital' , array('class'=>'txt' , 'maxlength'=>'200')); ?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>部署</th>
                    <td>
                        <?php echo $this->form->input('MstRepayment.departmentName', array('type' => 'hidden' , 'id'=>'departmentName')); ?>
                        <?php echo $this->form->input('MstRepayment.departmentText', array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'r', 'style' => 'width: 60px;')); ?>
                        <?php echo $this->form->input('MstRepayment.departmentCode' ,array('options'=>array() , 'class'=>'r txt validate[required]' , 'id'=>'departmentCode'))?>
                    </td>
                    <td></td>
                    <th></th>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="ButtonBox">
            <span><input type="button" value="" class="btn btn92" id="btn_Select"/></span>
        </div>
    </form>
