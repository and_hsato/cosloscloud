<script>
  $(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    //備考一括コピー
    $('#recital_btn').click(function(){
        $('input[type=text].recital').val($('#recital_txt').val());
    });

    //確定ボタン
    $("#btn_Conf").click(function(){
        cnt=0;
        err=0;
        //チェックされている行をヒップアップ
        $('input[type=checkbox].chk').each(function(){
            if($(this).attr('checked') == true ){
                cnt++;
                var r_id = $(this).val();
                //入力数量
                var quantity = $('#quantity_'+r_id ).val();
                //最大数量
                var max_quantity = $(this).parent(':eq(0)').find('input[type=hidden].max_quantity:eq(0)').val();
                //低レベルフラグ
                var is_lowlevel = $(this).parent(':eq(0)').find('input[type=hidden].is_lowlevel:eq(0)').val();

                //数量フォーマットチェック
                if(!numCheck(quantity , 5 , 0 , true)){
                    alert('数量は1以上の整数で入力してください');
                    err++;
                    return false;
                }

                if(!quantity.match(/[1-9][0-9]*/)){
                    alert('数量を正しく入力してください');
                    err++;
                    return false;
                }else if(quantity.match(/[^0-9]/g)){
                    alert('数量を正しく入力してください');
                    err++;
                    return false;
                }

                if(is_lowlevel){
                    //単価
                    var unit_price = $('#unit_price_'+r_id).val();

                    //単価フォーマットチェック
                    if(!numCheck(unit_price , 5 , 2 , false)){
                        alert('売上単価は小数点以下2位までの数値で入力してください');
                        err++;
                        return false;
                    }
                }

                // 数量がmax 以下のチェック
                if(parseInt(quantity) > parseInt(max_quantity)){
                   alert('数量が入力できる最大値を超えています');
                   err++;
                   return false;
                }
            }
        });
        if(cnt != 0){
            if(err == 0){
                if(confirm('返却登録を行ってよろしいでしょうか？')){
                    $(window).unbind('beforeunload');
                    $("#RepaymentList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result').submit();
                }
            }
        } else {
            alert('必ず一つは選択してください');
        }
    });
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">返却</a></li>
        <li class="pankuzu">返却登録（検索）</a></li>
        <li>返却登録確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>返却登録確認</span></h2>
<h2 class="HeaddingMid">返却を行う商品を確認してください。</h2>
<form class="validate_form" method="post" action="" accept-charset="utf-8" id="RepaymentList">
    <?php echo $this->form->input('TrnRepayments.token', array('type'=>'hidden'));?>
    <?php echo $this->form->input('TrnRepayments.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>返却日</th>
                <td><?php echo $this->form->input('MstRepayment.work_date' , array('type'=>'text','class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                <td></td>
                <th>作業区分</th>
                <td>
                     <?php echo $this->form->input('MstRepayment.className' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                     <?php echo $this->form->input('MstRepayment.classId' , array('type'=>'hidden'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('MstRepayment.facilityName' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('MstRepayment.facilityId' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('MstRepayment.recital' , array('type'=>'text','class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('MstRepayment.departmentName' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('MstRepayment.departmentId' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
            <div class="SelectBikou_Area">
                <span class="DisplaySelect"></span>
                <span class="BikouCopy">
                </span>
            </div>
            <div class="SelectBikou_Area" id="page_top">
                <span class="DisplaySelect">
              　表示件数：<?php echo count($result);  ?>件
                </span>
                <span class="BikouCopy">
                    <?php echo $this->form->input('MstRepayment.recital_txt' , array('type'=>'text' , 'class'=>'txt' , 'id'=>'recital_txt' , 'maxlength' => '200'))?>
                    <input type="button" id="recital_btn" class="btn btn8 [p2]" />
                </span>
            </div>
            <div class="TableScroll2">
                <table class="TableStyle01 table-odd2">
                    <colgroup>
                        <col width="25" />
                        <col width="150" />
                        <col width="90" />
                        <col width="90" />
                        <col />
                        <col />
                        <col />
                        <col />
                        <col width="120" />
                        <col />
                    </colgroup>
                    <tr>
                        <th rowspan="2"><input type="checkbox" checked class="checkAll" /></th>
                        <th>作業番号</th>
                        <th>売上日</th>
                        <th>商品ID</th>
                        <th>商品名</th>
                        <th>製品番号</th>
                        <th>包装単位</th>
                        <th>ロット番号</th>
                        <th>数量</th>
                        <th>作業区分</th>
                    </tr>
                    <tr>
                        <th colspan="2">仕入先</th>
                        <th>管理区分</th>
                        <th>規格</th>
                        <th>販売元</th>
                        <th>部署シール</th>
                        <th>有効期限</th>
                        <th>売上単価</th>
                        <th>備考</th>
                    </tr>
                    <?php if(!empty($result)){ ?>
                    <?php $cnt=0; foreach($result as $r) {  ?>
                    <tr>
                        <td rowspan="2" class="center">
                            <?php echo $this->form->checkbox('MstRepayment.id.'.$r['MstRepayment']['id'] , array('value'=>$r['MstRepayment']['id'] , 'class' => 'center chk' , 'hiddenField'=>false , 'checked'=>true)) ?>
                        </td>
                        <td></td>
                        <td><?php echo h_out($r['MstRepayment']['claim_date'],'center'); ?></td>
                        <td><?php echo h_out($r['MstRepayment']['internal_code'],'center'); ?></td>
                        <td><?php echo h_out($r['MstRepayment']['item_name']); ?></td>
                        <td><?php echo h_out($r['MstRepayment']['item_code']); ?></td>
                        <td><?php echo h_out($r['MstRepayment']['unit_name']); ?></td>
                        <td><?php echo h_out($r['MstRepayment']['lot_no']); ?></td>
                        <td>
                            <?php echo $this->form->input('MstRepayment.quantity.'.$r['MstRepayment']['id'] , array('type'=>'text' ,'id'=>'quantity_' .$r['MstRepayment']['id']  ,  'class'=>'r txt right quantity' , 'value'=>$r['MstRepayment']['quantity']))?>
                        </td>
                        <td>
                            <?php echo $this->form->input('MstRepayment.work_type.'.$r['MstRepayment']['id'] , array('options'=>$class_list , 'class'=>'txt' ,'value'=>$this->request->data['MstRepayment']['classId'] , 'empty'=>true))?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td><?php echo h_out($r['MstRepayment']['trade_type_name'],'center'); ?></td>
                        <td><?php echo h_out($r['MstRepayment']['standard']); ?></td>
                        <td><?php echo h_out($r['MstRepayment']['dealer_name']); ?></td>
                        <td><?php echo h_out($r['MstRepayment']['hospital_sticker_no']); ?></td>
                        <td><?php echo h_out($r['MstRepayment']['validated_date'],'center'); ?></td>
                        <td class="right"><?php echo h_out($this->Common->toCommaStr($r['MstRepayment']['unit_price']),'right'); ?></td>
                        <td><?php echo $this->form->input('MstRepayment.detail_recital.'.$r['MstRepayment']['id'] , array('class'=>'txt recital' , 'maxlength'=>'200' ))?></td>
                    </tr>
                    <?php  $cnt++; } ?>
                </table>
            </div>
        </div>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="ButtonBox" id="page_footer">
            <input type="button" class="btn btn2" id="btn_Conf" />
        </div>
   </form>
<?php } ?>
