<script>
$(function(){
   $("#btn_Print_Invoice").click(function(){
       if(confirm('納品書印刷を行ってよろしいでしょうか？')){
           $("#RepaymentList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/invoice').submit();
       }
   });

   $("#btn_Print_Requestsales").click(function(){
       if(confirm('売上依頼票印刷を行ってよろしいでしょうか？')){
           $("#RepaymentList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/requestsales').submit();
       }
   });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">返却</a></li>
        <li class="pankuzu">返却登録（検索）</li>
        <li class="pankuzu">返却登録確認</li>
        <li>&nbsp;返却登録結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>返却登録結果</span></h2>
<div class="Mes01">以下の内容で更新しました。</div>
<form method="post" action="" accept-charset="utf-8" id="RepaymentList">
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>返却日</th>
                <td><?php echo $this->form->input('MstRepayment.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly')) ?></td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('MstRepayment.className' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('MstRepayment.classId' , array('type'=>'hidden'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('MstRepayment.facilityName' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('MstRepayment.facilityId' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('MstRepayment.recital' , array('type'=>'text','class'=>'lbl' , 'readonly'=>'readonly'))?></td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('MstRepayment.departmentName' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('MstRepayment.departmentId' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="results">
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>

        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="150" />
                    <col width="100" />
                    <col width="90" />
                    <col />
                    <col />
                    <col width="150" />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>作業番号</th>
                    <th>売上日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>ロット番号</th>
                    <th>数量</th>
                    <th>作業区分</th>
                </tr>
                <tr>
                    <th colspan="2">仕入先</th>
                    <th>管理区分</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>シール番号</th>
                    <th>有効期限</th>
                    <th>売上単価</th>
                    <th>備考</th>
                </tr>

                <?php foreach($result as $r){ ?>
                <tr>
                    <td><?php echo h_out($r['MstRepayment']['work_no'],'center'); ?></td>
                    <td><?php echo h_out($r['MstRepayment']['claim_date'],'center'); ?></td>
                    <td><?php echo h_out($r['MstRepayment']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['MstRepayment']['item_name']); ?></td>
                    <td><?php echo h_out($r['MstRepayment']['item_code']); ?></td>
                    <td><?php echo h_out($r['MstRepayment']['unit_name']); ?></td>
                    <td><?php echo h_out($r['MstRepayment']['lot_no']); ?></td>
                    <td>
                        <?php echo $this->form->input('MstRepayment.quantity' , array('class' => 'right lbl' , 'value'=>$r['MstRepayment']['quantity'] , 'readonly'=>'readonly') )?>
                    </td>
                    <td>
                        <?php echo $this->form->input('MstRepayment.work_name' , array('class' => 'lbl' , 'value'=>$r['MstRepayment']['work_name'] , 'readonly'=>'readonly') )?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td><?php echo h_out($r['MstRepayment']['trade_type_name'],'center'); ?></td>
                    <td><?php echo h_out($r['MstRepayment']['standard']); ?></td>
                    <td><?php echo h_out($r['MstRepayment']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['MstRepayment']['sticker_no']); ?></td>
                    <td><?php echo h_out($r['MstRepayment']['validated_date'],'center'); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($r['MstRepayment']['unit_price']),'right'); ?></td>
                    <td>
                        <?php echo $this->form->input('MstRepayment.recital' , array('class' => 'lbl' , 'value'=>$r['MstRepayment']['recital'] , 'readonly'=>'readonly') )?>
                    </td>
                </tr>
                <?php } ?>
                </table>
            </div>
        </div>
    </div>
    <div class="SelectBikou_Area" id="page_footer" style="margin-top:15px;">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
        <input type="button" class="btn btn19" id="btn_Print_Invoice" name="btn_Print_Invoice" />
        <input type="button" class="btn btn20" id="btn_Print_Requestsales" name="btn_Print_Requestsales" />
    </div>
</form>
