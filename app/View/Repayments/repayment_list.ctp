<script>
    $(function(){
        //全選択・解除
        $('.checkAll_1').click(function(){
            $('#containTables input[type=checkbox]').attr('checked',$(this).attr('checked'));
        });
        $('.checkAll_2').click(function(){
            $('#confirmForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
        });

        // ENTERで送信
        $( 'input[type=text]').keypress( function ( e ) {
            form = $(this).parents('form');
            if ( e.which == 13 ) { // ENTER で送信
                if(form.hasClass('search_form')){ // 検索画面だったら
                    $("#btn_Search").click();
                }
            }
        } );
  
        //確認ボタン
        $("#btn_Conf").click(function(){
            //選択チェック
            var cnt = 0;
            var tmpId = "";
            $("#confirmForm input[type=checkbox].checkAllTarget").each(function(){
                if($(this).attr("checked") == true){
                    cnt++;
                    if(tmpId === '' ){
                        tmpId = $(this).val();
                    }else{
                        tmpId +="," +  $(this).val();
                    }
                }
            });
            if( cnt === 0 ) {
                alert('返却登録を行う商品を、1つ以上選択してください');
            } else {
                $("#selected_id").val(tmpId);
                $("#confirmForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/conf').submit();
            }
        });

        //検索ボタン
        $("#btn_Search").click(function(){
            var tmpId = "";
            $("#confirmForm input[type=checkbox].checkAllTarget").each(function(){
                if($(this).attr("checked") == true){
                    if(tmpId === '' ){
                        tmpId = $(this).val();
                    }else{
                        tmpId +="," +  $(this).val();
                    }
                }
            });
            $("#tmpId").val(tmpId);
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/RepaymentList').submit();
        });

        //選択ボタン押下
        $('#cmdSelect').click(function(){
            var cnt = 0;
            var isChecked = false;
            $('#itemSelectedTable').empty();
            $("#containTables input[type=checkbox].checkAllTarget").each(function(){
                if($(this).attr("checked") == true){
                    $('#SelectedTrnRepaymentID' + cnt).val( $(this).val() );
                    $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
                    isChecked = true;
                }else{
                    $('#SelectedTrnRepaymentID' + cnt).val("0");
                }
                cnt++;
            });
            if(!isChecked){
                alert('明細を選択してください');
            }
        });
    });

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li style=""><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">返却</a></li>
        <li>返却登録（検索）</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>返却登録（検索）</span></h2>
<h2 class="HeaddingMid">返却登録を行いたい売上を検索してください。</h2>
<form class="validate_form search_form" id="searchForm" method="post" action="RepaymentList">
     <?php echo $this->form->input('search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
     <?php echo $this->form->input('search.tmpId',array('type'=>'hidden','id' =>'tmpId'));?>

<div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
            <col width="20" />
        </colgroup>
        <tr>
            <th>返却日</th>
            <td><?php echo $this->form->input('MstRepayment.work_date' , array('class'=>'lbl','readonly'=>'readonly'))?></td>
            <td></td>
            <th>作業区分</th>
            <td>
                <?php echo $this->form->input('MstRepayment.className' , array('class'=>'lbl','readonly'=>'readonly'))?>
                <?php echo $this->form->input('MstRepayment.classId' , array('type'=>'hidden'))?>
            </td>
            <td></td>
        </tr>
        <tr>
            <th>得意先</th>
            <td>
                <?php echo $this->form->input('MstRepayment.facilityName' , array('class'=>'lbl','readonly'=>'readonly'))?>
                <?php echo $this->form->input('MstRepayment.facilityId' , array('type'=>'hidden'))?>
            </td>
            <td></td>
            <th>備考</th>
            <td><?php echo $this->form->input('MstRepayment.recital' , array('class'=>'lbl','readonly'=>'readonly'))?></td>
            <td></td>
       </tr>
       <tr>
            <th>部署</th>
            <td>
                <?php echo $this->form->input('MstRepayment.departmentName' , array('class'=>'lbl','readonly'=>'readonly'))?>
                <?php echo $this->form->input('MstRepayment.departmentId' , array('type'=>'hidden'))?>
            </td>
            <td></td>
            <th></th>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>
    <h2 class="HeaddingMid">検索条件</h2>
    <div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
            <col width="20" />
        </colgroup>
        <tr>
            <th>売上日</th>
            <td>
                <?php echo $this->form->input('MstRepayment.search_start_date' , array('class'=>'validate[optional,custom[date],funcCall2[date2]] date' , 'id' => 'datepicker1'))?>
                ～
                <?php echo $this->form->input('MstRepayment.search_end_date' , array('class'=>'validate[optional,custom[date],funcCall2[date2]] date' , 'id' => 'datepicker2'))?>
            </td>
            <td></td>
            <th></th>
            <td></td>
            <td></td>
            <th></th>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th>商品ID</th>
            <td><?php echo $this->form->input('MstRepayment.search_internal_code' , array('class'=>'txt search_internal_code'));?></td>
            <td></td>
            <th>製品番号</th>
            <td><?php echo $this->form->input('MstRepayment.search_item_code' , array('class'=>'txt search_upper'));?></td>
            <td></td>
            <th>ロット番号</th>
            <td><?php echo $this->form->input('MstRepayment.search_lot_no' , array('class'=>'txt'));?></td>
            <td></td>
        </tr>
        <tr>
            <th>商品名</th>
            <td><?php echo $this->form->input('MstRepayment.search_item_name' , array('class'=>'txt search_canna'));?></td>
            <td></td>
            <th>販売元</th>
            <td><?php echo $this->form->input('MstRepayment.search_dealer_name' , array('class'=>'txt search_canna'));?></td>
            <td></td>
            <th></th>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th>規格</th>
            <td><?php echo $this->form->input('MstRepayment.search_standard' , array('class'=>'txt'));?></td>
            <td></td>
            <th>シール番号</th>
            <td><?php echo $this->form->input('MstRepayment.search_sticker_no' , array('class'=>'txt'));?></td>
            <td></td>
            <th></th>
            <td></td>
            <td></td>
        </tr>
    </table>
    </div>
    <div class="ButtonBox">
        <input type="button" value="" class="btn btn1" id="btn_Search"/>
    </div>

<div class="results">
    <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
          <span class="DisplaySelect">
          　<?php echo $this->element('limit_combobox',array('result'=>count($SearchList))); ?>
          </span>
        </div>

      <!-- コピー元ヘッダテーブルSTART -->
      <div class="" id="containTables">
          <table class="TableStyle01" style="margin:0;padding:0;">
              <colgroup>
                  <col width="25" />
                  <col width="150" />
                  <col width="80" />
                  <col width="80" />
                  <col />
                  <col />
                  <col width="85" />
                  <col width="100" />
                  <col />
              </colgroup>
              <tr>
                  <th rowspan="2"><input type="checkbox" class="checkAll_1" /></th>
                  <th>作業番号</th>
                  <th>売上日</th>
                  <th>商品ID</th>
                  <th>商品名</th>
                  <th>製品番号</th>
                  <th>包装単位</th>
                  <th>ロット番号</th>
                  <th>部署シール</th>
              </tr>
              <tr>
                  <th colspan="2">仕入先</th>
                  <th>管理区分</th>
                  <th>規格</th>
                  <th>販売元</th>
                  <th>売上単価</th>
                  <th>有効期限</th>
                  <th></th>
              </tr>
        </table><!-- コピー元ヘッダテーブルEND -->
<?php if(!empty($SearchList)){ ?>
<?php
    $cnt=0;
    foreach($SearchList as $r ) {
?>
    <!-- コピー元テーブルボディ部START -->

    <table class="TableStyle02" id="<?php echo 'containTables'.$r['MstRepayment']['id'];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;" >
        <colgroup>
            <col width="25" />
            <col width="150" />
            <col width="80" />
            <col width="80" />
            <col />
            <col />
            <col width="85" />
            <col width="100" />
            <col />
         </colgroup>
         <tr class="<?php echo (($cnt%2==0)?'':'odd'); ?>">
             <td rowspan="2" class="center">
                 <input type="checkbox" name="data[SelectedTrnRepaymentID][<?php echo $cnt; ?>]" class="center checkAllTarget" value="<?php echo $r['MstRepayment']['id']; ?>">
             </td>
             <td></td>
             <td><?php echo h_out($r['MstRepayment']['claim_date'],'center'); ?></td>
             <td><?php echo h_out($r['MstRepayment']['internal_code'],'center'); ?></td>
             <td><?php echo h_out($r['MstRepayment']['item_name']); ?></td>
             <td><?php echo h_out($r['MstRepayment']['item_code']); ?></td>
             <td><?php echo h_out($r['MstRepayment']['unit_name']); ?></td>
             <td><?php echo h_out($r['MstRepayment']['lot_no']); ?></td>
             <td><?php echo h_out($r['MstRepayment']['hospital_sticker_no']); ?></td>
         </tr>
         <tr class="<?php echo (($cnt%2==0)?'':'odd'); ?>">
             <td colspan="2"></td>
             <td><?php echo h_out($r['MstRepayment']['trade_type_name'],'center'); ?></td>
             <td><?php echo h_out($r['MstRepayment']['standard']); ?></td>
             <td><?php echo h_out($r['MstRepayment']['dealer_name']); ?></td>
             <td><?php echo h_out($this->Common->toCommaStr($r['MstRepayment']['unit_price']),'right'); ?></td>
             <td><?php echo h_out($r['MstRepayment']['validated_date'],'center'); ?></td>
             <td></td>
         </tr>
      </table>
  <?php $cnt++;  } //end foreach ?>
  <?php }elseif(isset($this->request->data['search']['is_search'])){ ?>
       <table class="TableStyle02">
           <tr><td colspan="9" class="center">該当するデータがありませんでした。</td></tr>
       </table>
  <?php } ?>

      </div>
      <div class="ButtonBox" style="margin-top:3px;">
        <p class="center">
          <input type="button" class="btn btn4" id="cmdSelect" />
        </p>
      </div>
<?php echo $this->form->end(); ?>
<h2 class="HeaddingSmall">選択商品</h2>
<?php
    echo $this->form->create( 'RepaymentList',array('type' => 'post','action' => '' ,'id' =>'confirmForm','class' => 'validate_form') );
?>

      <?php echo $this->form->input('MstRepayment.work_date' , array('type'=>'hidden'))?>
      <?php echo $this->form->input('MstRepayment.classId' , array('type'=>'hidden'))?>
      <?php echo $this->form->input('MstRepayment.className' , array('type'=>'hidden'))?>
      <?php echo $this->form->input('MstRepayment.facilityName' , array('type'=>'hidden'))?>
      <?php echo $this->form->input('MstRepayment.facilityId' , array('type'=>'hidden'))?>
      <?php echo $this->form->input('MstRepayment.recital' , array('type'=>'hidden'))?>
      <?php echo $this->form->input('MstRepayment.departmentName' , array('type'=>'hidden'))?>
      <?php echo $this->form->input('MstRepayment.departmentId' , array('type'=>'hidden'))?>
      <?php echo $this->form->input('selected_id' , array('type'=>'hidden','id' =>'selected_id'))?>

<div>
      <table class="TableStyle01" style="margin:0;padding:0;">
          <colgroup>
              <col width="25" />
              <col width="150" />
              <col width="80" />
              <col  width="80" />
              <col />
              <col />
              <col width="85" />
              <col width="100" />
              <col />
          </colgroup>
          <tr>
              <th rowspan="2"><input type="checkbox" checked="checked" class="checkAll_2" /></th>
              <th>作業番号</th>
              <th>売上日</th>
              <th>商品ID</th>
              <th>商品名</th>
              <th>製品番号</th>
              <th>包装単位</th>
              <th>ロット番号</th>
              <th>部署シール</th>
          </tr>
          <tr>
              <th colspan="2">仕入先</th>
              <th>管理区分</th>
              <th>規格</th>
              <th>販売元</th>
              <th>売上単価</th>
              <th>有効期限</th>
              <th></th>
          </tr>
      </table><!-- テーブルヘッダ部END --></div>
<?php if(!empty($CartList)){ ?>
<?php
    $cnt=0;
    foreach($CartList as $r ) {
?>
    <table class="TableStyle02" id="<?php echo 'containTables'.$r['MstRepayment']['id'];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;" >
        <colgroup>
            <col width="25" />
            <col width="150" />
            <col width="80" />
            <col width="80" />
            <col />
            <col />
            <col width="85" />
            <col width="100" />
            <col />
         </colgroup>
         <tr class="<?php echo (($cnt%2==0)?'':'odd'); ?>">
             <td rowspan="2" class="center">
                 <input type="checkbox" name="data[SelectedTrnRepaymentID][<?php echo $cnt; ?>]" class="center checkAllTarget" value="<?php echo $r['MstRepayment']['id']; ?>" checked>
             </td>
             <td></td>
             <td><?php echo h_out($r['MstRepayment']['claim_date'],'center'); ?></td>
             <td><?php echo h_out($r['MstRepayment']['internal_code'],'center'); ?></td>
             <td><?php echo h_out($r['MstRepayment']['item_name'],'center'); ?></td>
             <td><?php echo h_out($r['MstRepayment']['item_code']); ?></td>
             <td><?php echo h_out($r['MstRepayment']['unit_name']); ?></td>
             <td><?php echo h_out($r['MstRepayment']['lot_no']); ?></td>
             <td><?php echo h_out($r['MstRepayment']['hospital_sticker_no']); ?></td>
         </tr>
         <tr class="<?php echo (($cnt%2==0)?'':'odd'); ?>">
             <td colspan="2"></td>
             <td><?php echo h_out($r['MstRepayment']['trade_type_name'],'center'); ?></td>
             <td><?php echo h_out($r['MstRepayment']['standard']); ?></td>
             <td><?php echo h_out($r['MstRepayment']['dealer_name']); ?></td>
             <td><?php echo h_out($this->Common->toCommaStr($r['MstRepayment']['unit_price']),'right'); ?></td>
             <td><?php echo h_out($r['MstRepayment']['validated_date'],'center'); ?></td>
             <td></td>
         </tr>
      </table>
  <?php
          $cnt++;
      } //end foreach
  } // end if  ?>
      <div id="itemSelectedTable"></div>
      <div class="ButtonBox" style="margin-top:3px;">
        <input type="button" value="" class="btn btn3" id="btn_Conf"/>
      </div>
</div>
<?php
    echo $this->form->end();
?>
