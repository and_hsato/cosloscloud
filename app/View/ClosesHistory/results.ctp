<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">締め履歴</a></li>
        <li>締め取消結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>締め取消結果</span></h2>
<div class="Mes01">締めを取り消しました</div>
<div class="SearchBox">
    <div id="results">
        <br>
        <div class="TableScroll">
            <div class="TableHeaderAdjustment01">
                <table class="TableHeaderStyle01">
                    <colgroup>
                        <col>
                        <col align="center" width="80">
                        <col>
                        <col align="center" width="180">
                        <col align="center" width="50">
                        <col align="center" width="90">
                        <col>
                    </colgroup>
                    <tr>
                        <th>施設</th>
                        <th>締め年月</th>
                        <th>実行者</th>
                        <th>実行日</th>
                        <th>状態</th>
                        <th>種別</th>
                        <th>備考</th>
                    </tr>
                </table>
            </div>
            <div class="TableScroll">
                <table class="TableStyle01 table-even">
                    <colgroup>
                        <col>
                        <col align="center" width="80">
                        <col>
                        <col align="center" width="180">
                        <col align="center" width="50">
                        <col align="center" width="90">
                        <col>
                    </colgroup>
                    <?php foreach ($results as $row): ?>
                        <tr>
                            <td><?php echo h_out($row['facility_name']); ?></td>
                            <td><?php echo h_out($row['close_month'],'center'); ?></td>
                            <td><?php echo h_out($row['user_name']); ?></td>
                            <td><?php echo h_out($row['work_date'],'center'); ?></td>
                            <td><?php echo h_out($provisional[$row['is_provisional'] === false ? '0' : '1'],'center'); ?></td>
                            <td><?php echo h_out($closeTypes[$row['close_type']],'center'); ?></td>
                            <td><?php echo h_out($row['recital']); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>
