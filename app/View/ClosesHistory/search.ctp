<script type="text/javascript">
    $(document).ready(function(){
        $('#search').click(function(){
            $('#close_history_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').submit();
        });

        $('#btnCancel').click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length === 0){
                alert('取消を行う明細を選択してください');
                return false;
            }else{
                if(!confirm('選択された締めの取消を行います。よろしいですか？')){
                    return false;
                }else{
                    $('#close_history_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/cancel').submit();
                }
            }
        });

        $('.checkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
        });
    });
</script>
<form class="validate_form search_form" id="close_history_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search" method="POST">
    <input type="hidden" name="data[isSearch]" value="1" />
    <div id="TopicPath">
        <ul>
            <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
            <li>締め履歴</li>
        </ul>
    </div>
    <h2 class="HeaddingLarge"><span>締め履歴</span></h2>
    <h2 class="HeaddingMid">検索条件を指定してください</h2>
    <div class="SearchBox">
        <table class="FormStyle01">
            <tr>
                <th>締め種別</th>
                <td>
                    <?php echo $this->form->input('TrnCloseHeader.close_type', array('type'=>'select' , 'options'=>$closeTypes, 'empty'=>true , 'class' => 'txt')); ?>
                </td>
                <td width="20"></td>
                <th>対象年月</th>
                <td><?php echo $this->form->input('TrnCloseHeader.work_date_from', array('class'=>'txt validate[optional,custom[month]]', 'style'=>'width:60px;','maxlength'=>7)); ?>&nbsp;～&nbsp;<?php echo $this->form->input('TrnCloseHeader.work_date_to', array('class'=>'txt', 'style'=>'width:60px;','maxlength'=>7)); ?></td>
            </tr>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('MstFacility.facility_name', array('class'=>'txt search_canna')); ?></td>
                <td width="20"></td>
                <th>状態</th>
                <td>
                    <?php echo $this->form->input('TrnCloseHeader.is_provisional', array('type'=>'select','options'=>$provisional, 'empty'=>true , 'class' => 'txt')); ?>
                </td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center"><input type="button" class="btn btn1" id="search" /></p>
        </div>

        <hr class="Clear" />
        <div id="results">
            <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
            <div class="TableScroll">
                <div class="TableHeaderAdjustment01">
                    <table class="TableHeaderStyle01">
                        <colgroup>
                            <col align="center" width="20">
                            <col>
                            <col align="center" width="80">
                            <col>
                            <col align="center" width="180">
                            <col align="center" width="50">
                            <col align="center" width="90">
                            <col>
                        </colgroup>
                        <tr>
                            <th><input type="checkbox" class="checkAll"></th>
                            <th>施設</th>
                            <th>締め年月</th>
                            <th>実行者</th>
                            <th>実行日</th>
                            <th>状態</th>
                            <th>種別</th>
                            <th>備考</th>
                        </tr>
                    </table>
                </div>
                <?php if(count($SearchResult) > 0) : ?>
                <div class="TableScroll">
                    <table class="TableStyle01 table-even">
                        <colgroup>
                            <col align="center" width="20">
                            <col>
                            <col align="center" width="80">
                            <col>
                            <col align="center" width="180">
                            <col align="center" width="50">
                            <col align="center" width="90">
                            <col>
                        </colgroup>
                        <?php foreach($SearchResult as $row): ?>
                        <tr>
                            <td style="width:20px;">
                                <?php if($row['canCancel'] === true): ?>
                                    <?php echo $this->form->checkbox("TrnCloseHeader.Rows.{$row['id']}.isSelected", array('class'=>'checkAllTarget',)); ?>
                                <?php endif; ?>
                            </td>
                            <td><?php echo h_out($row['facility_name']); ?></td>
                            <td><?php echo h_out($row['close_month'],'center'); ?></td>
                            <td><?php echo h_out($row['user_name']); ?></td>
                            <td><?php echo h_out($row['work_date'],'center'); ?></td>
                            <td><?php echo h_out($provisional[$row['is_provisional'] === false ? '0' : '1'] , 'center'); ?></td>
                            <td><?php echo h_out($closeTypes[$row['close_type']],'center'); ?></td>
                            <td><?php echo h_out($row['recital']); ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php elseif(count($SearchResult) === 0 && isset($this->request->data['isSearch'])): ?>
                <div class="TableScroll">
                    <table class="TableStyle01">
                        <tr><td colspan="8" class="center"><span>該当するデータが存在しません</span></td></tr>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <?php if(count($SearchResult) > 0): ?>
            <div class="ButtonBox">
                <input type="button" id="btnCancel" class="btn btn6 [p2]" />
            </div>
            <?php endif; ?>
        </div>
    </div>
</form>
