<script>
$(function(){
    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    //取消ボタン押下
    $("#btn_Mod").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            if(window.confirm("取消を実行します。よろしいですか？")){
                $("#ShippingsList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result').submit();
            }
        } else {
            alert('必ず一つは選択してください');
        }
    });

    //部署シール印刷
    $('#hospitalStickerPrint').click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $('#ShippingsList').attr('action', '<?php echo $this->webroot ?><?php echo $this->name; ?>/seal/1').submit();
        }
    });

    //コストシール印刷
    $('#costStickerPrint').click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $('#ShippingsList').attr('action', '<?php echo $this->webroot ?><?php echo $this->name; ?>/seal/2').submit();
        }
    });
    
    //備考一括設定
    $('#recitalBtn').click(function(){
        $('.recital').val($('#recitalTxt').val());
    });
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a>入荷処理</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">入荷履歴</a></li>
        <li>入荷履歴詳細</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入荷履歴詳細</span></h2>
<h2 class="HeaddingMid">入荷履歴の確認と取り消しが行えます。</h2>
<form method="post" action="" accept-charset="utf-8" id="ShippingsList" class="validate_form">
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>

    <div class="results">
        <div class="SelectBikou_Area" id="page_top">
            <span class="DisplaySelect">
            　表示件数：<?php echo count($ShippingList);  ?>件
            </span>
            <span class="BikouCopy">
                <input type="text" class="txt" id="recitalTxt" />
                <input type="button" class="common-button-2" id="recitalBtn"  value="備考一括設定" />
            </span>
        </div>
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2">
                <colgroup>
                    <col width="25" />
                    <col width="140" />
                    <col width="90" />
                    <col width="100" />
                    <col />
                    <col width="150"/>
                    <col width="150">
                    <col width="140"/>
                </colgroup>
                <thead>
                <tr>
                    <th rowspan="2"><input type="checkbox" checked class="checkAll"/></th>
                    <th>入荷番号</th>
                    <th>入荷日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>出荷番号</th>
                    <th>備考</th>
                </tr>
                <tr>
                    <th colspan="2">部署</th>
                    <th>包装単位</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>状態</th>
                    <th>更新者</th>
                </tr>
                </thead>
                <tbody>
                <?php $cnt=0; foreach ($ShippingList as $shipp) { ?>
                <tr>
                    <td rowspan="2" class="center">
        <?php
          $disabled = true;
          //消費済み・予約済み・締め済み・削除済みは選択できないようにする
          if(($shipp['TrnReceiving']['status'] != "")){
            $disabled = true;
          } else {
            $disabled = false;
          }
        ?>
                    <?php if(!$disabled){ ?>
                    <?php echo $this->form->input('TrnReceiving.id.'.$cnt , array('type' => 'checkbox' , 'class'=>'chk' , 'value' => $shipp['TrnReceiving']['id'] ,  'hiddenField'=>false , 'checked' => true))?>
                    <?php echo $this->form->input('TrnReceiving.sticker_id.'.$shipp['TrnReceiving']['id'] , array('type' => 'hidden' , 'value' => $shipp['TrnSticker']['id'] ))?>
                    <?php echo $this->form->input('TrnReceiving.hospital_sticker_no.'.$shipp['TrnReceiving']['id'] , array('type' => 'hidden' , 'value' => $shipp['TrnSticker']['hospital_sticker_no'] ))?>
                    <?php } ?>
                    </td>
                    <td><?php echo h_out($shipp['TrnReceiving']['work_no']); ?></td>
                    <td><?php echo h_out($shipp['TrnReceiving']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($shipp['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($shipp['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($shipp['MstFacilityItem']['item_code']); ?></td>
                    <td><?php echo h_out($shipp['TrnShipping']['work_no']); ?></td>
                    <td>
                    <?php if(!$disabled){ ?>
                    <?php echo $this->form->input('TrnReceiving.recital.'.$shipp['TrnReceiving']['id'] , array('type' => 'text' , 'value' => '' , 'class' => 'txt recital' , 'id' => 'recital'.$shipp['TrnReceiving']['id']))?>
                    <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($shipp['TrnReceiving']['department_name']); ?></td>
                    <td><?php echo h_out($shipp['MstItemUnit']['unit_name']); ?></td>
                    <td><?php echo h_out($shipp['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($shipp['MstDealer']['dealer_name']); ?></td>
                    <td><?php echo h_out($shipp['TrnReceiving']['status']); ?></td>
                    <td><?php echo h_out($shipp['MstUser']['user_name']); ?></td>
                </tr>
                <?php $cnt++; } ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="ButtonBox" id="page_footer">
        <input type="button" class="common-button [p2]" id="btn_Mod" value="取消"/>
        <!--<input type="button" class="btn btn16 fix_btn" id="hospitalStickerPrint" />-->
        <!--<input type="button" class="btn btn12 fix_btn" id="costStickerPrint" />-->
    </div>
</form>
</div><!--#content-wrapper-->