<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a>入荷処理</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">入荷履歴</a></li>
        <li class="pankuzu">入荷履歴詳細</li>
        <li>入荷履歴取消完了</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入荷履歴取消完了</span></h2>
<h2 class="HeaddingMid">以下の入荷履歴の取消を行いました。</h2>
<form method="post" action="" id="ReceivingList">
    <div class="results">
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2">
                <colgroup>
                    <col width="140" />
                    <col width="90" />
                    <col width="100" />
                    <col />
                    <col width="150"/>
                    <col width="150">
                    <col width="140"/>
                </colgroup>
                <thead>
                <tr>
                    <th>入荷番号</th>
                    <th>入荷日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>出荷番号</th>
                    <th>備考</th>
                </tr>
                <tr>
                    <th colspan="2">部署</th>
                    <th>包装単位</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>状態</th>
                    <th>更新者</th>
                </tr>
                </thead>
                <tbody>
                <?php $cnt=0; foreach ($ReceivingList as $rec) { ?>
                <tr>
                    <td><?php echo h_out($rec['TrnReceiving']['work_no']); ?></td>
                    <td><?php echo h_out($rec['TrnReceiving']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($rec['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($rec['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($rec['MstFacilityItem']['item_code']); ?></td>
                    <td><?php echo h_out($rec['TrnShipping']['work_no']); ?></td>
                    <td><input type="text" class="tbl_lbl" name="" id="" value="<?php echo $rec['TrnReceiving']['recital'] ?>" readonly="readonly" /></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($rec['TrnReceiving']['department_name']); ?></td>
                    <td><?php echo h_out($rec['TrnReceiving']['unit_name']); ?></td>
                    <td><?php echo h_out($rec['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($rec['MstDealer']['dealer_name']); ?></td>
                    <td></td>
                    <td><?php echo h_out($rec['MstUser']['user_name']); ?></td>
                </tr>
                <?php $cnt++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</form>
</div><!--#content-wrapper-->