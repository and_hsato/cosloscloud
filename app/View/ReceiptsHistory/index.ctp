<script  type="text/javascript">
$(document).ready(function() {
    //検索ボタン押下
     $("#btn_Search").click(function(){
         $("#ReceiptsInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/index#search_result').submit();
     });
    //CSVボタン押下
    $("#btn_Csv").click(function(){
         $("#ReceiptsInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
    });
  
    //Excelボタン押下
    $("#btn_Excel").click(function(){
         $("#ReceiptsInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_excel').submit();
    });

    //明細表示ボタン押下
    $("#btn_Conf").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#ReceiptsInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/conf').submit();
        } else {
            alert('必ず一つは選択してください');
        }
    });
    
    //部署シール印刷ボタン押下
    $("#btn_hospital_sticker_print").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#ReceiptsInfo").attr('action', '<?php echo $this->webroot; ?>PdfReport/sticker').attr('target','_blank').submit();
            $("#ReceiptsInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/index').attr('target' , '_self');
        } else {
            alert('必ず一つは選択してください');
        }
    });

    //納品リスト印刷ボタン押下
    $("#btn_delivery_Print").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#ReceiptsInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/1').submit();
        } else {
            alert('必ず一つは選択してください');
        }
    });

    //欠品リスト印刷ボタン押下
    $("#btn_stockout_Print").click(function(){
        $("#ReceiptsInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/2').submit();
    });
    
    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
});

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a>入荷処理</a></li>
        <li>入荷履歴</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入荷履歴</span></h2>
<h2 class="HeaddingMid">入荷履歴を検索できます。また、入荷処理を間違った場合は詳細画面から取り消しが行えます。</h2>
    <form method="post" action="" accept-charset="utf-8" id="ReceiptsInfo" class="validate_form search_form">
        <?php echo $this->form->input('MstFacility.id' , array('type'=>'hidden','value'=>$this->Session->read('Auth.facility_id_selected')))?>
        <?php echo $this->form->input('TrnReceiving.is_search' , array('type'=>'hidden'))?>
       <h3 class="conth3">入荷履歴を検索</h3>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>入荷番号</th>
                    <td><?php echo $this->form->input('MstReceipts.search_receipts_no' , array('class'=>'txt'))?></td>
                    <td></td>
                    <th>入荷日</th>
                    <td>
                        <?php echo $this->form->input('MstReceipts.search_start_date' , array('type'=>'text' , 'class'=>'date validate[optional,custom[date]]' , 'id'=>'datepicker1'))?>
                      &nbsp;～&nbsp;
                        <?php echo $this->form->input('MstReceipts.search_end_date' , array('type'=>'text' , 'class'=>'date validate[optional,custom[date]]' , 'id'=>'datepicker2'))?>
                    </td>
                    <td></td>
                    <th></th>
                    <td>
                      <?php echo $this->form->input('MstReceipts.search_display_deleted' , array('type'=>'checkbox'))?>取消も表示する
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>仕入先</th>
                    <td>
                        <?php echo $this->form->input('MstReceipts.supplierText',array('id' => 'supplierText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                        <?php echo $this->form->input('MstReceipts.supplierCode',array('options'=>$supplier_list ,'empty'=>true,'id' => 'supplierCode', 'class' => 'txt')); ?>
                        <?php echo $this->form->input('MstReceipts.supplierName',array('type'=>'hidden' ,'id' => 'supplierName')); ?>
                    </td>
                    <td></td>
                    <th>入荷部署</th>
                    <td>
                        <?php echo $this->form->input('MstReceipts.departmentText',array('class'=>'txt','style'=>'width:60px;','id'=>'departmentText')); ?>
                        <?php echo $this->form->input('MstReceipts.departmentCode',array('options'=>$department_list, 'class'=>'txt','style'=>'width:120px;','id'=>'departmentCode' , 'empty'=>true)); ?>
                        <?php echo $this->form->input('MstReceipts.departmentName',array('type'=>'hidden','id'=>'departmentName')); ?>
                    </td>
                    <td></td>
                    <th></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品ID</th>
                    <td><?php echo $this->form->input('MstReceipts.search_internal_code' , array('class'=>'txt search_internal_code'))?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->input('MstReceipts.search_item_code' , array('class'=>'txt search_upper'))?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->input('MstReceipts.search_item_name' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->input('MstReceipts.search_dealer_name' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->input('MstReceipts.search_standard' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="common-button" id="btn_Search" value="検索"/>
            <input type="button" class="common-button" id="btn_Excel" value="Excel"/>
        </div>

    <div class="results">
      <a name="search_result"></a>
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <thead>
                <tr>
                    <th style="width:20px;"><input type="checkbox" class="checkAll"/></th>
                    <th style="width:135px;">入荷番号</th>
                    <th style="width:85px;">入荷日</th>
                    <th>仕入先</th>
                    <th>部署</th>
                    <th>登録者</th>
                    <th style="width:140px;">登録日時</th>
                    <th>備考</th>
                    <th>件数</th>
                </tr>
                </thead>
                <tbody>
                <?php $cnt=0; foreach($result as $r){ ?>
                <tr>
                    <td class="center"><?php echo $this->form->input('MstReceipts.id.'.$cnt , array('type'=>'checkbox' , 'value'=>$r['TrnReceivingHeader']['id'] , 'hiddenField'=>false , 'class'=>'chk'))?></td>
                    
                    <td><?php echo h_out($r['TrnReceivingHeader']['work_no']); ?></td>
                    <td><?php echo h_out($r['TrnReceivingHeader']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnReceivingHeader']['supplier_name']); ?></td>
                    <td><?php echo h_out($r['TrnReceivingHeader']['department_name']); ?></td>
                    <td><?php echo h_out($r['MstUser']['user_name']); ?></td>
                    <td><?php echo h_out($r['TrnReceivingHeader']['created'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnReceivingHeader']['recital']); ?></td>
                    <td><?php echo h_out($r['TrnReceivingHeader']['total_count']." ／ ".$r['TrnReceivingHeader']['detail_count'],'right'); ?></td>
                </tr>
                <?php $cnt++; } ?>
                <?php if( count($result)==0 && isset($this->request->data['TrnReceiving']['is_search']) ){ ?>
                <tr><td colspan="9" class="center">該当するデータがありませんでした</td></tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if(count($result) > 0){ ?>

        <?php } ?>
    </div>
    <?php if(count($result) > 0){ ?>

    <?php
        $mstUser = $this->Session->read('Auth.MstUser');
        $editionId = null;
        if (empty($mstUser['parent_id'])) {
            $mstRole = $this->Session->read('Auth.MstRole');
            $editionId = $mstRole['mst_edition_id'];
        } else {
            $parent = $this->Session->read('Auth.Parent');
            $editionId = $parent['MstRole']['mst_edition_id'];
        }
        $isStdEdition = Configure::read('edition.std') === $editionId;
    ?>
    <div class="ButtonBox">
      <input type="button" class="common-button" id="btn_Conf" value="詳細"/>
      <?php if (!$isStdEdition) echo '<input type="button" class="common-button" id="btn_hospital_sticker_print" value="部署シール印刷"/>' ?>
      <!--<input type="button" class="btn btn120" id="btn_delivery_Print"/>-->
      <!--<input type="button" class="btn btn121" id="btn_stockout_Print"/>-->
    </div>
    <?php } ?>
</form>
    </div><!--#content-wrapper-->
