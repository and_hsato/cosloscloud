<script type="text/javascript">
    $(document).ready(function(){
        //検索ボタン押下
        $('#search').click(function(){
            $('#trn_receiving_header_search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').submit();
        });
        //確認ボタン押下
        $('#cmdConfirm').click(function(){
            if($('input[type=radio][name="data[TrnReceiving][selected]"]:checked').length === 0){
                alert('入庫を行いたい明細を選択してください');
                return false;
            }else{
                $('#trn_receiving_header_search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm').submit();
            }
        });

        <?php if(isset($isPrintSticker) && $isPrintSticker == true): ?>
            $('#trn_receiving_header_search_form').attr('action', '<?php echo $this->webroot; ?>storages/seal/dummy').submit();
            $('#trn_receiving_header_search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
        <?php endif; ?>
    });
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>選択入庫</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>選択入庫</span></h2>
<h2 class="HeaddingMid">入庫を行いたい商品を選択してください。</h2>

<form class="validate_form search_form" id="trn_receiving_header_search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search" method="post">
    <input type="hidden" name="data[IsSearch]" value="1" />
    <input type="hidden" name="data[lastHeaderId]" value="<?php echo((isset($lastHeaderId) ? $lastHeaderId : '')); ?>" />
    <input type="hidden" name="data[facility_name]" value="<?php echo $facility_name; ?>" />
    <?php echo $this->form->input('MstFacility.id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>入荷番号</th>
                <td><?php echo($this->form->text('TrnReceiving.work_no', array('class'=>'txt', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>入荷日</th>
                <td><?php echo($this->form->text('TrnReceiving.work_date_from', array('class' => 'txt date validate[optional,custom[date]]', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' ))); ?>&nbsp;～&nbsp;<?php echo($this->form->text('TrnReceiving.work_date_to', array('class' => 'txt date validate[optional,custom[date]]', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' ))); ?></td>
                <td></td>
                <td>
                <?php if(Configure::read('SplitTable.flag') == 1){ ?>
                <?php echo $this->form->input('Search.center_move',array('type'=>'checkbox','hiddenField'=>false)); ?>センター間移動を表示
                <?php } ?>
                </td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo($this->form->text('MstFacilityItem.internal_code', array('class'=>'txt search_internal_code', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo($this->form->text('MstFacilityItem.item_code', array('class'=>'txt search_upper', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo($this->form->text('MstFacilityItem.item_name', array('class'=>'txt search_canna', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo($this->form->text('MstDealer.dealer_name', array('class'=>'txt search_canna', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo($this->form->text('MstFacilityItem.standard', array('class'=>'txt', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo($this->form->text('MstFacilityItem.jan_code', array('class'=>'txt', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn1" id="search" /></p>
    </div>
    <?php if(count($SearchResult) === 0): ?>
        <?php if(isset($this->request->data['IsSearch'])){ ?>
        <span>該当するデータはありません。</span>
        <?php } ?>
    <?php else: ?>
    <div id="results">
        <h2 class="HeaddingSmall">入庫を行いたい明細を選択してください。</h2>
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2" border=0>
                <tr>
                    <th style="width:20px;" rowspan="2"></th>
                    <th style="width:135px;">入荷番号</th>
                    <th style="width:95px;">入荷日</th>
                    <th class="col10">商品ID</th>
                    <th class="col15">商品名</th>
                    <th class="col15">製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col5">入荷数</th>
                    <th class="col10">作業区分</th>
                    <th class="col10">更新者</th>
                </tr>
                <tr>
                    <th class="col25" colspan="2">仕入先</th>
                    <th class="col10">区分</th>
                    <th class="col15">規格</th>
                    <th class="col15">販売元</th>
                    <th class="col10">仕入単価</th>
                    <th class="col5">残数</th>
                    <th class="col20" colspan="2">備考</th>
                </tr>
                <?php $i=0;foreach($SearchResult as $_row): ?>
                <tr>
                    <td style="width:20px;" rowspan="2" class="center"><input type="radio" name="data[TrnReceiving][selected]" value="<?php echo($_row['id']); ?>" /></td>
                    <td style="width:135px;"><?php echo h_out($_row['work_no'] , 'center'); ?></td>
                    <td style="width:95px;"><?php echo h_out(date('Y/m/d', strtotime($_row['work_date'])),'center'); ?></td>
                    <td class="col10"><?php echo h_out($_row['internal_code'],'center'); ?></td>
                    <td class="col15"><?php echo h_out($_row['item_name']); ?></td>
                    <td class="col15"><?php echo h_out($_row['item_code']); ?></td>
                    <td class="col10"><?php echo h_out($_row['packing_name']); ?></td>
                    <td class="col5"><?php echo h_out($_row['quantity'],'right'); ?></td>
                    <td class="col10"><?php echo h_out($_row['work_type_name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['user_name']); ?></td>
                </tr>
                <tr>
                    <td class="col25" colspan="2"><?php echo h_out($_row['facility_name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['order_type_name'],'center'); ?></td>
                    <td class="col15"><?php echo h_out($_row['standard']); ?></td>
                    <td class="col15"><?php echo h_out($_row['dealer_name']); ?></td>
                    <td class="col10"><?php echo h_out($this->Common->toCommaStr($_row['stocking_price']) ,'right'); ?></td>
                    <td class="col5"><?php echo h_out($_row['remain_count'] , 'right'); ?></td>
                    <td class="col20" colspan="2"><?php echo h_out($_row['recital']); ?></td>
                </tr>
                <?php $i++;endforeach; ?>
            </table>
        </div>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn3" id="cmdConfirm" />
            </p>
        </div>
    </div>
    <?php endif; ?>
</form>
