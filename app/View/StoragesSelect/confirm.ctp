<script type="text/javascript">
$(function(){
    $(document).ready(function(){
        $('#TrnStickerValidatedDate').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
        $('input[type=checkbox].checkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
        });

        $('#commit').click(function(){
<?php if (isset($lotchk) && ($lotchk == true)){ ?>
                if($('#TrnStickerLotNo').val() == ''){
                    if (!confirm('ロット番号が入力されていませんが間違いありませんか？')){
                        return false;
                }
            }
<?php  } ?>
            if($('#TrnStickerValidatedDate').val() != '') {
                $('#TrnStickerValidatedDate').val(convertDate($('#TrnStickerValidatedDate').val()));
                var checkStr = $('#TrnStickerValidatedDate').val();
                if( !checkStr.match(/^\d{4}\/\d{2}\/\d{2}$/)) {
                    alert('有効期限の書式が不正です');
                    return false;
                }
                var year = checkStr.substr( 0 , 4 );
                var month = checkStr.substr( 5 , 2 );
                var day = checkStr.substr( 8 , 2 );

                if( month > 0 && month < 13 && day > 0 && day < 32 ) {
                    var date = new Date( year ,month -1 , day );
                    if( isNaN(date) ) {
                        alert('有効期限の書式が不正です');
                        return false;
                    } else if( date.getFullYear() == year && date.getMonth() == month - 1  ,date.getDate() == day ){

                    } else {
                        alert('有効期限の書式が不正です');
                        return false;
                    }
                } else {
                    alert('有効期限の書式が不正です');
                    return false;
                }
            }
<?php if (isset($datechk) && ($datechk == true)){ ?>
            if ("" == $("#TrnStickerValidatedDate").val()){
                if (!confirm('有効期限が入力されていませんが間違いありませんか？')){
                    return false;
                }
            }else{
                var term = <?php echo $expired_date; ?>;
                var cur = '<?php echo $current_date; ?>';
                if (!chkValidatedDate($('#TrnStickerValidatedDate').val() ,term ,cur)){
                    if (!confirm('有効期限切れ又は有効期限警告期間内です。\n\n\nこのまま登録する場合は    「OK」   を\n中止したい場合は   「キャンセル」   を\n\n選択して下さい。')){
                        return false;
                    }
                }
            }
<?php } ?>
            var cnt = 0;
            var hasError = false;
            var sumStocks = 0;
            $('input[type=checkbox].checkAllTarget').each(function(i){
                if(this.checked){
                    var q = $('input[type=text].inputQuantity:eq(' + i + ')').val();
                    cnt++;
                    if(q == ''){
                        alert('数量を入力してください');
                        hasError = true;
                        return false;
                    }else if(q == "0"){
                        alert('0以外の数字を入力してください');
                        hasError = true;
                        return false;
                    }else if(q.match(/[\D]/g)){
                        alert('数量に正しくない入力があります');
                        hasError = true;
                        return false;
                    }else{
                        sumStocks += parseInt(q) * parseInt($('input[type=hidden][name=stock_per_unit]:eq(' + i + ')').val());
                    }
                }
            });

            if(hasError){
                return false;
            }else if(cnt === 0){
                alert('在庫情報を選択してください');
                return false;
            }

            if((parseInt($('#total_per_unit').val()) * parseInt($('#total_quantity').val())) < parseInt(sumStocks)){
                alert('入力数量を適切に分配してください');
                return false;
            }else if(parseInt(sumStocks) % parseInt($('#total_per_unit').val()) !== 0 ){
                alert('入力数量を適切に分配してください');
                return false;
            }else{
                $('#storage_quantity').val(parseInt(sumStocks) / parseInt($('#total_per_unit').val()));
            }
            $(window).unbind('beforeunload');
            $('#storages_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/commit').submit();
        });
    });

    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
});

function chkValidatedDate(target, term, cur){
    //期限切れ警告日数がなければチェックしない
    if( isNaN(term) ) {
       return true;
    }
    var targetdt = new Date(target);
    var todaydt  = new Date(cur);
    //本日日付(入庫日) + 有効期限日数
    var baseSec = todaydt.getTime();
    var addSec = term * 86400000;
    var targetSec = baseSec + addSec;
    todaydt.setTime(targetSec);

    //[本日日付(入庫日) + 有効期限日数] >= シールの有効期限 の場合
    if(todaydt.getTime() >= targetdt.getTime()) {
        return false;
    }else{
        return true;
    }
}
</script>

<form class="validate_form" id="storages_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm" method="post">
    <?php echo $this->form->input('TrnStorage.time' , array('type'=>'hidden' , 'value'=> date('Y/m/d H:i:s.u')));?>
    <?php echo $this->form->input('TrnStorage.select_mode' , array('type'=>'hidden'));?>
    <?php echo $this->form->input('Search.center_move' , array('type'=>'hidden'));?>
    <input type="hidden" id="total_per_unit" value="<?php echo $res['per_unit']; ?>" />
    <input type="hidden" id="total_quantity" value="<?php echo $res['remain_count']; ?>" />
    <input type="hidden" name="data[TrnStorage][storage_quantity]" id="storage_quantity" value="" />
    <input type="hidden" id="class_separation" value="<?php echo $res['class_separation']; ?>" />
    <div id="TopicPath">
        <ul>
            <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
            <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">入荷選択</a></li>
            <li>入庫詳細</li>
        </ul>
    </div>
    <h2 class="HeaddingLarge"><span>入庫詳細</span></h2>

    <div id="results">
        <h2 class="HeaddingSmall">以下の内容でよろしければ確定ボタンをクリックしてください</h2>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col/>
                    <col/>
                    <col width="20"/>
                    <col/>
                    <col/>
                </colgroup>
                <tr>
                    <th>商品ID</th>
                    <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $res['internal_code']; ?>" readonly /></td>
                    <td></td>
                    <th>入数</th>
                    <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $res['per_unit']; ?>" readonly /></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $res['item_name']; ?>" readonly /></td>
                    <td></td>
                    <th>単位</th>
                    <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $res['unit_name']; ?>" readonly /></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $res['standard']; ?>" readonly /></td>
                    <td></td>
                    <th>ロット番号</th>
                    <td><?php echo $this->form->input('TrnSticker.lot_no', array('class'=>'txt','style'=>'width:150px;')); ?></td>
                </tr>
                <tr>
                    <th>製品番号</th>
                    <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $res['item_code']; ?>" readonly /></td>
                    <td></td>
                    <th>有効期限</th>
                    <td><?php echo $this->form->input('TrnSticker.validated_date', array('class'=>'txt','type'=>'text','style'=>'width:150px;')); ?></td>
                </tr>
                <tr>
                    <th>販売元</th>
                    <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $res['dealer_name']; ?>" readonly /></td>
                    <td></td>
                    <th>備考</th>
                    <td><?php echo $this->form->input('TrnSticker.recital', array('class'=>'txt','style'=>'width:150px;')); ?></td>
                </tr>
            </table>
        </div>

        <h2 class="HeaddingSmall">入荷情報</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
                <tr>
                    <th class="col20">入荷番号</th>
                    <th class="col10">入荷日時</th>
                    <th class="col30">取引先</th>
                    <th class="col10">包装単位</th>
                    <th class="col20">仕入金額</th>
                    <th class="col10">数量</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll" style="">
            <table class="TableStyle01">
                <tr>
                    <td class="col20"><?php echo h_out($res['work_no'],'center'); ?></td>
                    <td class="col10"><?php echo h_out(date('Y/m/d', strtotime($res['work_date'])) , 'center'); ?></td>
                    <td class="col30"><?php echo h_out($res['facility_name']); ?></td>
                    <td class="col10"><?php echo h_out($res['packing_name']); ?></td>
                    <td class="col20"><?php echo h_out($this->Common->toCommaStr($res['stocking_price']), 'right'); ?></td>
                    <td class="col10"><?php echo h_out($res['remain_count'],'right'); ?></td>
                </tr>
            </table>
        </div>

        <h2 class="HeaddingSmall">在庫情報</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
                <colgroup>
                    <col width="20"/>
                    <col/>
                    <col/>
                    <col/>
                    <col/>
                    <col/>
                    <col/>
                    <col/>
                </colgroup>
                <tr>
                    <th style="width:20px;" class="center"><input type="checkbox" checked class="checkAll" /></th>
                    <th>包装単位</th>
                    <th>定数</th>
                    <th>在庫数</th>
                    <th>要求数</th>
                    <th>引当可</th>
                    <th>最大作成可能数</th>
                    <th>数量</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll" style="">
            <table class="TableStyle01">
                <colgroup>
                    <col width="20"/>
                    <col />
                    <col align="right"/>
                    <col align="right"/>
                    <col align="right"/>
                    <col align="right"/>
                    <col align="right"/>
                    <col/>
                </colgroup>
                <?php $i=0;foreach($stocks as $_row): ?>
                <tr class="<?php echo($i%2===0?'':'odd'); ?>">
                    <td style="width:20px;" class="center">
                        <?php
                          if($_row['is_right_term_pack'] == TRUE){

                            if (is_null($_row['fixed_count']) || $_row['fixed_count'] == 0){
                                echo $this->form->checkbox("TrnStock.Rows.{$_row['id']}.selected",array('class'=>'center checkAllTarget'));
                            } else {
                                echo $this->form->checkbox("TrnStock.Rows.{$_row['id']}.selected",array('class'=>'center checkAllTarget','checked'=>'checked'));
                            }

                          }else{
                            echo $this->form->text("TrnStock.Rows.noId.selected",array('type'=>'hidden'));
                          }

                          if($_row['is_right_term_pack'] == TRUE){
                        ?>
                          <input type="hidden" name="stock_per_unit" value="<?php echo $_row['per_unit']; ?>" />
                        <?php
                          }else{
                        ?>
                          <input type="hidden" name="noId" value="<?php echo $_row['per_unit']; ?>" />
                        <?php
                          }
                        ?>
                    </td>
                    <td><?php echo h_out($_row['packing_name'],'right'); ?></td>
                    <td><?php echo h_out($_row['fixed_count'],'right'); ?></td>
                    <td><?php echo h_out($_row['stock_count'], 'right'); ?></td>
                    <td><?php echo h_out($_row['promise_count'], 'right'); ?></td>
                    <td><?php echo h_out($_row['promisable_count'] , 'right'); ?></td>
                    <td>
                      <?php
                        //使用可能な包装単位か
                        if($_row['is_right_term_pack'] == TRUE){
                          echo '<span class="maxCreatableCount">';?>
                          <label title="<?php echo $_row['max_creatable_count']; ?>">
                          <p align="right">
                          <?php
                          echo $_row['max_creatable_count'];
                        }else{
                          echo '<span>';?>
                          <label title="<?php echo $_row['max_creatable_count']; ?>">
                          <p align="right">
                          <?php
                          echo $_row['max_creatable_count'];
                        }
                      ?>
                      </p>
                      </label>
                      </span>
                    </td>
                <?php
                  //使用可能な包装単位か
                  if($_row['is_right_term_pack'] == TRUE){
                    echo "<td>";
                    /*
                    if (isset($_row['input_quantity'])) {
                        echo $this->form->input("TrnStock.Rows.{$_row['id']}.quantity", array('class'=>'r txt inputQuantity', 'style'=>'text-align: right; width:90%;','value'=>$_row['input_quantity']));
                    } else {
                        echo $this->form->input("TrnStock.Rows.{$_row['id']}.quantity", array('class'=>'r txt inputQuantity', 'style'=>'text-align: right; width:90%;'));
                    }
                    */

                    echo $this->form->input("TrnStock.Rows.{$_row['id']}.quantity", array('class'=>'r txt inputQuantity', 'style'=>'text-align: right; width:90%;'));
                    
                    echo "</td>";
                  }else{
                    echo "<td>";
                    echo $this->form->input("TrnStock.Rows.noId.quantity", array('class'=>'lbl', 'style'=>'text-align: right;','readOnly'=>'readOnly'));
                    echo "</td>";
                  }
               ?>
                </tr>
                <?php $i++;endforeach; ?>
            </table>
        </div>

        <div class="ButtonBox">
            <p class="center"><input type="button" class="btn btn2 fix_btn [p2]" id="commit" /></p>
        </div>
    </div>
</form>