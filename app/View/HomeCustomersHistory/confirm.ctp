<script type="text/javascript">
    $(document).ready(function(){

        $('.chkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
        });

        $('#cancel').click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length === 0){
                alert('取消を行う明細を選択してください');
                return false;
            }else{
                if(window.confirm("取消を実行します。よろしいですか？")){
                $('#direct_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/results').submit();
                }
            }
        });

        $('#hospital').click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length === 0){
                alert('印刷を行う明細を選択してください');
                return false;
            }else{
                if(window.confirm("部署シールを印刷します。よろしいですか？")){
                $('#direct_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/1').submit();
                }
            }
        });
        $('#cost').click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length === 0){
                alert('印刷を行う明細を選択してください');
                return false;
            }else{
                if(window.confirm("コストシールを印刷します。よろしいですか？")){
                $('#direct_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/2').submit();
                }
            }
        });
  
    });

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">在宅品消費履歴</a></li>
        <li>在宅品消費履歴編集</li>
    </ul>
</div>
<form class="validate_form" id="direct_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/results" method="POST">
    <input type="hidden" name="data[pre_modified]" value="<?php echo date('Y/m/d H:i:s', time()); ?>" />
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <h2 class="HeaddingLarge"><span>在宅品消費履歴編集</span></h2>
    <h2 class="HeaddingMid">取消を行う在宅品消費明細を選択してください</h2>
    <div class="TableScroll">
        <table class="TableStyle01" style="margin:0;padding:0;">
            <colgroup>
                <col width="25"/>
                <col width="135"/>
                <col width="95"/>
                <col />
                <col />
                <col width="80"/>
                <col width="80"/>
                <col width="100"/>
                <col width="150"/>
                <col width="70"/>
                <col width="70"/>
            </colgroup>
            <tr>
                <th rowspan="2"><input type="checkbox" class="chkAll" /></th>
                <th>消費番号</th>
                <th>消費日</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th>包装単位</th>
                <th>仕入単価</th>
                <th>ロット番号</th>
                <th rowspan="2">部署シール</th>
                <th>作業区分</th>
                <th>更新者</th>
            </tr>
            <tr>
                <th colspan="2">施設　／　部署</th>
                <th>規格</th>
                <th>販売元</th>
                <th>商品ID</th>
                <th>売上単価</th>
                <th>有効期限</th>
                <th colspan="2">備考</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle01 table-even2">
            <colgroup>
                <col width="25"/>
                <col width="135"/>
                <col width="95"/>
                <col />
                <col />
                <col width="80"/>
                <col width="80"/>
                <col width="100"/>
                <col width="150"/>
                <col width="70"/>
                <col width="70"/>
            </colgroup>
            <?php $i=0;foreach($searchResult as $key => $val): ?>
            <tr>
                <td rowspan="2" align="center">
                    <?php // if($val['cancancel'] == true && (isset($reserved[$i]) && $reserved[$i] == false)): ?>
                        <?php echo $this->form->checkbox("TrnReceivingCenter.Rows.{$val['id']}", array('class'=>'center checkAllTarget','hiddenField'=>false)); ?>
                    <?php // endif; ?>
                </td>
                <td><?php echo h_out($val['work_no'],'center'); ?></td>
                <td><?php echo h_out($val['work_date'],'center'); ?></td>
                <td><?php echo h_out($val['item_name']); ?></td>
                <td><?php echo h_out($val['item_code']); ?></td>
                <td><?php echo h_out($val['packing_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($val['transaction_price']),'right'); ?></td>
                <td><?php echo h_out($val['lot_no']); ?></td>
                <td rowspan="2"><?php echo join('<br>', $val['sticker_no']); ?></td>
                <td><?php echo h_out($val['work_type_name']); ?></td>
                <td><?php echo h_out($val['user_name']); ?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo h_out($val['customer_name'] . ' / ' . $val['department_name']); ?></td>
                <td><?php echo h_out($val['standard']); ?></td>
                <td><?php echo h_out($val['dealer_name']); ?></td>
                <td><?php echo h_out($val['internal_code'],'center'); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($val['sales_price']),'right'); ?></td>
                <td><?php echo h_out($val['validated_date'],'center'); ?></td>
                <td colspan="2"><?php echo h_out($val['recital']); ?></td>
            </tr>
            <?php $i++;endforeach; ?>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" id="cancel" class="btn btn6 [p2]" />
            <input type="button" id="hospital" class="btn btn16" />
            <input type="button" id="cost" class="btn btn12" />
        </p>
    </div>
</form>
