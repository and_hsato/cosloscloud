<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
          <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/accounte_list">集計科目一覧</a></li>
          <li class="pankuzu">集計科目編集</li>
          <li>集計科目編集結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">集計科目編集結果</h2>
<form method="post" accept-charset="utf-8" class="validate_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>集計科目コード</th>
                <td>
                    <?php echo $this->form->input('MstAccounte.code' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>集計科目名称</th>
                <td>
                    <?php echo $this->form->input('MstAccounte.name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'));?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</form>

