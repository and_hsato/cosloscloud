<script>
$(document).ready(function() {
  //新規ボタン押下
    $("#btn_Add").click(function(){
        validateDetachSubmit($('#AccounteList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add' );
    });
    //編集ボタン押下
    $("#btn_Mod").click(function(){
        $("#AccounteList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
    });
    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $('#AccounteList').validationEngine('detach');  
        $("#AccounteList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        $("#AccounteList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/accounte_list');
    });
  
    //検索ボタン押下
    $("#btn_Search").click(function(){
        validateDetachSubmit($('#AccounteList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/accounte_list' );
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>集計科目一覧</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">集計科目一覧</h2>

<form class="validate_form search_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/accounte_list" accept-charset="utf-8" id="AccounteList">
    <?php echo $this->form->input('MstAccounte.is_search' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstAccounte.mst_facility_id' , array('type'=>'hidden' , 'value'=> $this->Session->read('Auth.facility_id_selected'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>集計科目コード</th>
                <td>
                    <?php echo $this->form->input('MstAccounte.search_code' , array('type'=>'txt' , 'class'=>'txt search_canna'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>集計科目名称</th>
                <td>
                    <?php echo $this->form->input('MstAccounte.search_name' , array('type'=>'txt' , 'class'=>'txt search_canna'));?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>
        <input type="button" class="btn btn11 [p2]" id="btn_Add"/>
        <input type="button" class="btn btn5" id="btn_Csv"/>
    </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <?php echo $this->element('limit_combobox',array('result'=>count($Accounte_List))); ?>
        </div>
        
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col />
                </colgroup>
                <thead>
                    <tr>
                        <th></th>
                        <th>集計科目コード</th>
                        <th>集計科目名称</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0 ;foreach($Accounte_List as $accounte) { ?>
                    <tr>
                        <td class="center"><input name="data[MstAccounte][id]" id="radio<?php echo $i; ?>" class="validate[required]" type="radio" value="<?php echo $accounte['MstAccounte']['id'] ?>" /></td>
                        <td><?php echo h_out($accounte['MstAccounte']['code']); ?></td>
                        <td><?php echo h_out($accounte['MstAccounte']['name']); ?></td>
                    </tr>
                    <?php $i++; } ?>
                    <?php if(count($Accounte_List)==0 && isset($this->request->data['MstAccounte']['is_search'])){ ?>
                    <tr><td colspan="3" class="center">該当するデータがありませんでした。</td></tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php if(count($Accounte_List)>0){ ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn9" id="btn_Mod"/>
    </div>
    <?php } ?>
</form>

