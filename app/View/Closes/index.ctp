<script type="text/javascript">
    $(document).ready(function(){
        $('#temporary').click(function(){
            if(!confirm('仮締めを実行します。よろしいですか？')){
                return false;
            }else{
                $('#MstClassName').val($('#MstClassId option:selected').text());
                $('#form_stock_close_list').attr('action', '<?php echo $this->webroot; ?>closes/temporary').submit();
            }
        });
        $('#complete').click(function(){
            if(!confirm('本締めを実行します。よろしいですか？')){
                return false;
            }else{
                $('#MstClassName').val($('#MstClassId option:selected').text());
                $('#form_stock_close_list').attr('action', '<?php echo $this->webroot; ?>closes/complete').submit();
            }
        });
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>在庫締め</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>在庫締め</span></h2>

<form class="validate_form" id="form_stock_close_list" action="<?php echo $this->webroot; ?>closes/temporary" method="post">
    <div class="SearchBox">
        <table class="FormStyle01">
            <tr>
                <th>締め年月</th>
                <td><?php echo $this->form->input('TrnClose.close_month', array('class' => 'lbl')); ?></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('MstClass.id', array('type'=>'select' , 'options'=>$classes, 'empty'=>true , 'class' => 'txt' , 'id'=>'MstClassId')); ?>
                    <?php echo $this->form->input('MstClass.name', array('type'=>'hidden' , 'id'=>'MstClassName')); ?>
                </td>
            </tr>
            <tr>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnCloseHeader.recital', array('class' => 'txt', "maxlength"=>200)); ?></td>
            </tr>
        </table>

        <hr class="Clear" />
    </div>
    <div id="results">
        <h2 class="HeaddingSmall">得意先一覧</h2>
        <div class="TableScroll">
            <div class="TableHeaderAdjustment01">
                <table class="TableHeaderStyle01">
                    <tr>
                        <th class="col80">病院</th>
                        <th class="col10">最終締め年月</th>
                        <th class="col10">状態</th>
                    </tr>
                </table>
            </div>
            <div class="TableScroll">
                <table class="TableStyle01">
                    <colgroup>
                        <col>
                        <col align="center">
                        <col align="center">
                    </colgroup>
                    <?php $i=0;foreach($partners as $_row): ?>
                    <tr class="<?php echo ($i%2 == 0 ?'':'odd'); ?>">
                        <td class="col80"><?php echo h_out($_row['MstFacility']['facility_name']); ?></td>
                        <td class="col10"><?php echo h_out(date('Y/m', strtotime($_row['TrnCloseHeader']['end_date'])),'center'); ?></td>
                        <td class="col10"><?php echo h_out($_row['TrnCloseHeader']['is_provisional'] == '1' ? '仮締め' : '本締め','center'); ?></td>
                    </tr>
                    <?php $i++;endforeach; ?>
                </table>
            </div>
            <br>
            <h2 class="HeaddingSmall">仕入一覧</h2>
            <div class="TableScroll">
                <div class="TableHeaderAdjustment01">
                    <table class="TableHeaderStyle01">
                        <tr>
                            <th class="col80">仕入先</th>
                            <th class="col10">最終締め年月</th>
                            <th class="col10">状態</th>
                        </tr>
                    </table>
                </div>
                <div class="TableScroll">
                    <table class="TableStyle01">
                        <colgroup>
                            <col>
                            <col align="center">
                            <col align="center">
                        </colgroup>
                        <?php $i=0;foreach($suppliers as $_row): ?>
                        <tr class="<?php echo ($i%2 == 0 ?'':'odd'); ?>">
                            <td class="col80"><?php echo h_out($_row['MstFacility']['facility_name']); ?></td>
                            <td class="col10"><?php echo h_out(date('Y/m', strtotime($_row['TrnCloseHeader']['end_date'])),'center'); ?></td>
                            <td class="col10"><?php echo h_out($_row['TrnCloseHeader']['is_provisional'] == true ? '仮締め' : '本締め','center'); ?></td>
                        </tr>
                        <?php $i++;endforeach; ?>
                    </table>
                </div>

                <br>
                <div class="ButtonBox">
                    <?php if($isTemporary === true): ?>
                    <input id="temporary" type="button" class="btn btn50 [p2]" />
                    <?php else: ?>
                    <input id="complete" type="button" class="btn btn87 [p2]" />
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</form>
