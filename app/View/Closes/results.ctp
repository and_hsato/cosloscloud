<script type="text/javascript">
    $(document).ready(function(){
        $('input[type=text].lbl').attr('readonly', 'readonly');
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>closes">在庫締め</a></li>
        <li>在庫締め結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>在庫締め結果</span></h2>
<div class="Mes01">在庫締めを行いました</div>

<div class="SearchBox">
    <table class="FormStyle01">
        <tr>
            <th>締め年月</th>
            <td><?php echo $this->form->input('TrnClose.close_month', array('class' => 'lbl')); ?></td>
            <td width="20"></td>
        </tr>
        <tr>
            <th>作業区分</th>
            <td><?php echo $this->form->input('MstClass.name' ,array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true))?></td>
        </tr>
        <tr>
            <th>備考</th>
            <td><?php echo $this->form->input('TrnCloseHeader.recital', array('type'=>'text', 'class' => 'lbl', 'readonly'=>true)); ?></td>
        </tr>
    </table>
    <hr class="Clear" />
</div>
