<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/history">遡及履歴</a></li>
        <li>遡及取消結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>遡及取消結果</span></h2>
<h2 class="HeaddingMid">以下の遡及取消を実施しました</h2>
<form id="result_form">
    <div class="TableHeaderAdjustment01" style="margin-top:40px;">
        <table class="TableHeaderStyle01">
            <colgroup>
                <col width="25" />
                <col width="135" />
                <col width="95" />
                <col width="95" />
                <col />
                <col width="125" />
                <col width="105" />
                <col width="125" />
                <col width="80" />
            </colgroup>
            <tr>
                <th></th>
                <th>遡及番号</th>
                <th>遡及種別</th>
                <th>計上日</th>
                <th>対象施設</th>
                <th>登録者</th>
                <th>登録日</th>
                <th>備考</th>
                <th>件数</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle01">
            <colgroup>
                <col width="20" />
                <col width="130"/>
                <col width="90" />
                <col width="90" />
                <col />
                <col width="120" />
                <col width="100" />
                <col width="120" />
                <col width="80" />
            </colgroup>
            <?php $i = 0; foreach ($result as $row){ ?>
            <tr>
                <td></td>
                <td>
                    <?php echo h_out($row['TrnRetroactHeader']['work_no'],'center'); ?>
                </td>
                <td>
                    <?php echo h_out($row['TrnRetroactHeader']['retroact_type'],'center'); ?>
                </td>
                <td>
                    <?php echo h_out($row['TrnRetroactHeader']['work_date'],'center'); ?>
                </td>
                <td>
                    <?php echo h_out($row['MstFacility']['facility_name']); ?>
                </td>
                <td>
                    <?php echo h_out($row['MstUser']['user_name']); ?>
                </td>
                <td>
                    <?php echo h_out($row['TrnRetroactHeader']['created'],'center'); ?>
                </td>
                <td>
                    <?php echo h_out($row['TrnRetroactHeader']['recital']); ?>
                </td>
                <td>
                    <?php echo h_out($row['TrnRetroactHeader']['count'],'right'); ?>
                </td>
            </tr>
            <?php $i++; } ?>
        </table>
    </div>
</form>
