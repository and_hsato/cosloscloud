<script type="text/javascript">
$(function(){
    $("#regist_btn").click(function(){
        $("#inputForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ms_stock_with_master_result/').submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/ms_stock">MS仕入赤黒遡及変更</a></li>
        <li>MS仕入赤黒遡及確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>MS仕入赤黒遡及確認</span></h2>
<?php echo $this->form->create( 'TrnRetroact',array('type' => 'post','action' => '' ,'id' =>'inputForm','class' => 'validate_form') ); ?>
    <?php echo $this->form->input('Retroact.token', array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.ownerName', array('type'=>'text', 'class'=>'lbl','readonly'=>true)); ?>
                    <?php echo $this->form->input('TrnRetroactHeader.ownerCode',array('type'=>'hidden')); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.work_class_txt',array('type'=>'text', 'class'=>'lbl' , 'readonly' => true)); ?>
                    <?php echo $this->form->input('TrnRetroactHeader.work_class',array('type'=>'hidden')); ?>
                </td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.facilityCode', array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnRetroactHeader.facilityName', array('type'=>'text', 'class'=>'lbl' , 'readonly'=>true)); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnRetroactHeader.recital', array('type'=>'text', 'class'=>'lbl', 'readonly' => true)); ?></td>
            </tr>
            <tr>
                <th>遡及期間</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.start_date',array('type'=>'text','class'=>'lbl' , 'readonly'=>true)); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('TrnRetroactHeader.end_date',array('type'=>'text','class'=>'lbl' , 'readonly'=>true)); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>適用開始日</th>
                <td><?php echo $this->form->input('TrnRetroactHeader.master_start_date', array('type'=>'text', 'class'=>'lbl' ,'readonly'=>true)); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>計上日</th>
                <td><?php echo $this->form->input('TrnRetroactHeader.claim_date', array('type'=>'text', 'class'=>'r date validate[required,custom[date],funcCall2[date2]]')); ?></td>
                <td></td>
            </tr>
        </table>
        <div class="AlertLimitBox">対象件数<?php echo $count ?>件</div>
        <div class="ButtonBox">
            <input type="button" class="btn btn2" id="regist_btn" />
        </div>
    </div>
</form>
