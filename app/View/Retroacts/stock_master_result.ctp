<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_stockprice">仕入価格変更</a></li>
        <li class="pankuzu">仕入価格変更確認</li>
        <li>仕入価格変更結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>仕入価格変更結果</span></h2>
<div class="Mes01">仕入価格変更を登録しました</div>
<?php echo $this->form->create( 'TrnRetroact',array('type' => 'post','action' => '' ,'id' =>'inputForm','class' => 'validate_form') ); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.ownerName', array('type'=>'text', 'class'=>'lbl','readonly'=>true)); ?>
                    <?php echo $this->form->input('TrnRetroactHeader.ownerCode',array('type'=>'hidden')); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.work_class_txt',array('type'=>'text', 'class'=>'lbl' , 'readonly' => true)); ?>
                    <?php echo $this->form->input('TrnRetroactHeader.work_class',array('type'=>'hidden')); ?>
                </td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.facilityCode', array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnRetroactHeader.facilityName', array('type'=>'text', 'class'=>'lbl' , 'readonly'=>true)); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnRetroactHeader.recital', array('type'=>'text', 'class'=>'lbl', 'readonly' => true)); ?></td>
            </tr>
            <tr>
                <th>遡及期間</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.start_date',array('type'=>'text','class'=>'lbl' , 'readonly'=>true)); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('TrnRetroactHeader.end_date',array('type'=>'text','class'=>'lbl' , 'readonly'=>true)); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>適用開始日</th>
                <td><?php echo $this->form->input('TrnRetroactHeader.master_start_date', array('type'=>'text', 'class'=>'lbl' ,'readonly'=>true)); ?></td>
                <td></td>
            </tr>
        </table>
    </div>
</form>
