<script type="text/javascript">
    $(function(){
        //検索ボタン押下
        $("#search_btn").click(function(){
            $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history');
            $("#history_form").attr('method', 'post');
            $("#history_form").submit();
        });
        //CSVボタン押下
        $("#btn_Csv").click(function(){
            $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/');
            $("#history_form").attr('method', 'post');
            $("#history_form").submit();
        });

        //明細表示ボタン押下
        $("#select_btn").click(function(){
            var _count = 0;
            $('input[:radio].retroact_radio').each(function(){
                if (this.checked === true) {
                    _count++;
                }
            });
            if (_count > 0) {
                $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit');
                $("#history_form").attr('method', 'post');
                $("#history_form").submit();
            }else {
                alert('遡及データが選択されていません');
                return false;
            }
        });

        //取消ボタン押下時
        $("#del_btn").click(function(){
            var submit_flag = false;
            $('input[:radio].retroact_radio').each(function(){
                if (this.checked === true) {
                    submit_flag = true;
                }
            });
            if(submit_flag){
                if(window.confirm("取消を実行します。\rよろしいですか？")){
                    $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_result');
                    $("#history_form").attr('method', 'post');
                    $("#history_form").submit();
                }
            }else {
                alert('遡及データが選択されていません');
                return false;
            }
        });
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>遡及履歴</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>遡及履歴</span></h2>
<form class="validate_form search_form" id="history_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history">
    <?php echo $this->form->input('search_options',array('type'=>'hidden','value'=>'1')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="60" align="right" />
            </colgroup>
            <tr>
                <th>遡及番号</th>
                <td><?php echo $this->form->input('TrnRetroactHeader.work_no',array('class'=>'txt','style'=>'width:110px;')); ?></td>
                <td></td>
                <th>計上日</th>
                <td>
                    <span>
                    <?php echo $this->form->input('TrnRetroactHeader.start_date',array('type'=>'text','class'=>'txt date','id'=>'datepicker1' )); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('TrnRetroactHeader.end_date',array('type'=>'text','class'=>'txt date','id'=>'datepicker2')); ?>
                    </span>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactRecord.supplierName',array('type'=>'hidden','id'=>'supplierName')); ?>
                    <?php echo $this->form->input('TrnRetroactRecord.supplierText',array('class'=>'txt require','style'=>'width:60px;','id'=>'supplierText')); ?>
                    <?php echo $this->form->input('TrnRetroactRecord.supplierCode',array('options'=>$suppliers, 'empty'=>true, 'class'=>'txt','style'=>'width:120px;','id'=>'supplierCode')); ?>
                </td>
                <td></td>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactRecord.hospitalName',array('type'=>'hidden','id'=>'hospitalName')); ?>
                    <?php echo $this->form->input('TrnRetroactRecord.hospitalText',array('class'=>'txt require','style'=>'width:60px;','id'=>'hospitalText')); ?>
                    <?php echo $this->form->input('TrnRetroactRecord.hospitalCode',array('options'=>$customers, 'empty'=>true, 'class'=>'txt','style'=>'width:120px;','id'=>'hospitalCode')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code',array('class'=>'txt search_upper search_internal_code','style'=>'width:120px;')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_code',array('class'=>'txt search_upper','style'=>'width:120px;')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('TrnRetroactRecord.work_type',array('options'=>$work_types, 'empty'=>true, 'class'=>'txt','style'=>'width:120px;')); ?></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_name',array('class'=>'txt search_canna','style'=>'width:120px;')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstFacilityItem.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px;')); ?></td>
                <td></td>
                <th>遡及種別</th>
                <td><?php echo $this->form->input('TrnRetroactRecord.retroact_type',array('options'=>$retroact_types, 'empty'=>true, 'class'=>'txt','style'=>'width:120px;')); ?></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('MstFacilityItem.standard',array('class'=>'txt search_canna','style'=>'width:120px;')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('MstFacilityItem.jan_code',array('class'=>'txt','style'=>'width:120px;')); ?></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="search_btn" />
                <input type="button" class="btn btn5" id="btn_Csv"/>
            </p>
        </div>
    </div>

<div id="results">
    <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox', array('param'=>'limit' , 'result' => count($RetroactHeaders)));?>
    </div>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
                <colgroup>
                    <col width="20" />
                    <col width="150" />
                    <col width="90" />
                    <col width="90" />
                    <col />
                    <col width="120" />
                    <col width="100" />
                    <col width="120" />
                    <col width="80" />
                </colgroup>
                <tr>
                    <th></th>
                    <th>遡及番号</th>
                    <th>遡及種別</th>
                    <th>計上日</th>
                    <th>対象施設</th>
                    <th>登録者</th>
                    <th>登録日</th>
                    <th>備考</th>
                    <th>件数</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col width="20" />
                    <col width="150" />
                    <col width="90" />
                    <col width="90" />
                    <col />
                    <col width="120" />
                    <col width="100" />
                    <col width="120" />
                    <col width="80" />
                </colgroup>
                <?php foreach ($RetroactHeaders as $rs){ ?>
                <tr>
                    <td>
                        <input type="radio" name="data[TrnRetroactHeader][id]" class="retroact_radio" value="<?php echo $rs['TrnRetroactHeader']['id']; ?>" />
                    </td>
                    <td><?php echo h_out($rs['TrnRetroactHeader']['work_no'],'center'); ?></td>
                    <td><?php echo h_out($rs['TrnRetroactHeader']['retroact_type'],'center'); ?>
                    <td><?php echo h_out($rs['TrnRetroactHeader']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($rs['MstFacility']['facility_name']); ?></td>
                    <td><?php echo h_out($rs['MstUser']['user_name']); ?></td>
                    <td><?php echo h_out($rs['TrnRetroactHeader']['created'],'center'); ?></td>
                    <td><?php echo h_out($rs['TrnRetroactHeader']['recital']); ?></td>
                    <td><?php echo h_out($rs['TrnRetroactHeader']['count'],'right'); ?></td>
                </tr>
                <?php } ?>
                <?php if(count($RetroactHeaders) == 0 && isset($this->request->data['search_options']) ){ ?>
                <tr><td colspan="9" class="center" >該当データがありません</td></tr>
                <?php } ?>
            </table>
        </div>
        <?php if (isset ($RetroactHeaders) && count($RetroactHeaders)){ ?>
        <?php echo $this->form->input('TrnRetroactHeader.chk_date',array('type'=>'hidden','value'=>date("Y-m-d H:i:s") )); ?>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn7 submit" id="select_btn" />
                <input type="button" class="btn btn6 submit [p2]" id="del_btn" >
            </p>
        </div>
        <?php } ?>
    </form>
</div>