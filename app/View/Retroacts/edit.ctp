<script type="text/javascript">
    $(function() {
        $('#retroacts_edit_form_btn').click(function(){
            var _count = 0;
            if($('input[type=checkbox].chk:checked').length === 0){
                alert('取消を行いたい遡及明細データを選択してください');
                return false;
            }else{
                $("#retroacts_edit_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_result');
                $("#retroacts_edit_form").attr('method', 'post');
                $("#retroacts_edit_form").submit();
            }
        });
    });
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history">遡及履歴</a></li>
        <li>遡及履歴明細</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>遡及履歴明細</span></h2>
<form id="retroacts_edit_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>遡及番号</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.work_no',array('class'=>'lbl','value'=>$retroactHeader['TrnRetroactHeader']['work_no'],'readonly'=>'readonly'));?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
<?php
                    echo $this->form->input('TrnRetroactHeader.work_type',array('type'=>'hidden','value'=>$retroactHeader['TrnRetroactHeader']['work_type']));
                    echo $this->form->input('TrnRetroactHeader.work_type_txt',array('class'=>'lbl','readonly'=>'readonly','value'=>$retroactHeader['MstClass']['name']));
?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>遡及種別</th>
                <td>
<?php
                    $type = (isset($retroact_types[$retroactHeader['TrnRetroactHeader']['retroact_type']])&& $retroact_types[$retroactHeader['TrnRetroactHeader']['retroact_type']] != '')?$retroact_types[$retroactHeader['TrnRetroactHeader']['retroact_type']]:'';
                    echo $this->form->input('TrnRetroactHeader.retroact_type',array('type'=>'hidden','value'=>$type));
                    echo $this->form->input('TrnRetroactHeader.retroact_type_txt',array('class'=>'lbl','value'=>$type,'readonly'=>'readonly'));
?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.recital',array('class'=>'lbl','value'=>$retroactHeader['TrnRetroactHeader']['recital'],'readonly'=>'readonly')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
<?php
                    $retroactHeader['MstFacility']['facility_name'] = '';
                    if(isset($retroactHeader['Supplier']['facility_name']) && $retroactHeader['Supplier']['facility_name']!==''){
                        $retroactHeader['MstFacility']['facility_name'] = $retroactHeader['Supplier']['facility_name'];
                    }else if(isset($retroactHeader['Hospital']['facility_name']) && $retroactHeader['Hospital']['facility_name']!==''){
                        $retroactHeader['MstFacility']['facility_name'] = $retroactHeader['Hospital']['facility_name'];
                    }
                    //$_facility_name = (isset($retroactHeader['MstFacility']['facility_name']) && $retroactHeader['MstFacility']['facility_name'] != '') ? $retroactHeader['Hospital']['facility_name']:$this->request->data['Supplier']['facility_name'];
                    echo $this->form->input('TrnRetroactHeader.facility',array('class'=>'lbl','value'=>$retroactHeader['MstFacility']['facility_name'],'readonly'=>'readonly'));
?>
                </td>
                <td></td>
                <th>計上日</th>
                <td>
                <?php echo $this->form->input('TrnRetroactHeader.work_date',array('type'=>'text','class'=>'lbl','value'=>$retroactHeader['TrnRetroactHeader']['work_date'],'readonly'=>'readonly')); ?>
                </td>
            </tr>
        </table>
    </div>

    <br>

<?php echo $this->Paginator->prev('≪ 前', null, null, array('class' => 'disabled')); ?>&nbsp;&nbsp;
<?php echo $this->Paginator->numbers(); ?>&nbsp;&nbsp;
<?php echo $this->Paginator->next('次 ≫', null, null, array('class' => 'disabled')); ?>&nbsp;&nbsp;
<?php echo $this->Paginator->counter(); ?>

    <br />
    <br />
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <colgroup>
                <col width="100">
                <col>
                <col>
                <col width="140">
                <col width="100">
            </colgroup>
            <tr>
                <th>商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th>包装単位</th>
                <th>適用開始日</th>
            </tr>
            <tr>
                <th>明細種別</th>
                <th>規格</th>
                <th>販売元</th>
                <th>単価</th>
                <th>適用終了日</th>
            </tr>
        </table>
    </div>
<?php
    if(isset($retroacts) && count($retroacts)):
?>
    <div class="TableScroll">
        <table class="TableStyle02">
            <colgroup>
                <col width="100" align="center">
                <col>
                <col>
                <col width="140">
                <col width="100" align="center">
            </colgroup>
            <?php $i = 0; foreach ($retroacts as $r): ?>
            <tr class="<?php echo($i % 2 == 0 ? '' : 'odd'); ?>">
                <?php echo $this->form->input('TrnRetroactRecord.'.$i.'.id', array('type'=>'hidden','value'=>$r['TrnRetroactRecord']['id'],'readonly'=>'readonly')); ?>
                <td>
                    <?php echo $this->form->input('TrnRetroactRecord.'.$i.'.MstFacilityItem.internal_code', array('type'=>'hidden','value'=>$r['MstFacilityItem']['internal_code'],'readonly'=>'readonly')); ?>
                    <?php echo h_out($r['MstFacilityItem']['internal_code'],'center'); ?>
                </td>
                <td>
                    <?php echo $this->form->input('TrnRetroactRecord.'.$i.'.MstFacilityItem.item_name', array('type'=>'hidden','value'=>$r['MstFacilityItem']['internal_code'],'readonly'=>'readonly')); ?>
                    <?php echo h_out($r['MstFacilityItem']['item_name']); ?>
                </td>
                <td>
                    <?php echo $this->form->input('TrnRetroactRecord.'.$i.'.MstFacilityItem.item_code', array('type'=>'hidden','value'=>$r['MstFacilityItem']['item_code'],'readonly'=>'readonly')); ?>
                    <?php echo h_out($r['MstFacilityItem']['item_code']); ?>
                </td>
                <td>
                    <?php $this->form->input('TrnRetroactRecord.'.$i.'.unit_name', array('type'=>'hidden','value'=>$r['MstFacilityItem']['unit_name']));?>
                    <?php echo h_out($r['MstFacilityItem']['unit_name']); ?>
                </td>
                <td>
                    <?php echo $this->form->input('TrnRetroactRecord.'.$i.'.TrnRetroactHeader.start_date', array('type'=>'hidden','value'=>$r['TrnRetroactHeader']['start_date'],'readonly'=>'readonly')); ?>
                    <?php echo h_out($r['TrnRetroactHeader']['start_date'],'center'); ?>
                </td>
            </tr>
            <tr class="<?php echo($i % 2 == 0 ? '' : 'odd'); ?>">
                <td>
                    <?php echo $retroact_types[$r['TrnRetroactRecord']['retroact_type']]; ?>
                </td>
                <td>
                    <?php echo h_out($r['MstFacilityItem']['standard']); ?>
                </td>
                <td>
                    <?php echo $this->form->input('TrnRetroactRecord.'.$i.'.MstDealer.dealer_name', array('type'=>'hidden','value'=>$r['MstDealer']['dealer_name'],'readonly'=>'readonly')); ?>
                    <?php echo h_out($r['MstDealer']['dealer_name']); ?>
                </td>
                <td class="num">
                    <?php echo $this->form->input('TrnRetroactRecord.'.$i.'.retroact_price', array('type'=>'hidden','value'=>$r['TrnRetroactRecord']['retroact_price'],'readonly'=>'readonly')); ?>
                    <?php echo h_out($this->Common->toCommaStr($r['TrnRetroactRecord']['retroact_price']),'right'); ?>
                </td>
                <td>
                    <?php echo $this->form->input('TrnRetroactRecord.'.$i.'.TrnRetroactHeader.end_date', array('type'=>'hidden','value'=>$r['TrnRetroactHeader']['end_date'],'readonly'=>'readonly')); ?>
                    <?php echo h_out($r['TrnRetroactHeader']['end_date'],'center'); ?>
                </td>
            </tr>
            <?php $i++; endforeach; ?>
        </table>
    </div>
    <?php echo $this->Paginator->prev('≪ 前', null, null, array('class' => 'disabled')); ?>&nbsp;&nbsp;
    <?php echo $this->Paginator->numbers(); ?>&nbsp;&nbsp;
    <?php echo $this->Paginator->next('次 ≫', null, null, array('class' => 'disabled')); ?>&nbsp;&nbsp;
    <?php echo $this->Paginator->counter(); ?>
    <?php endif; ?>
</form>