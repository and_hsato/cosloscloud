<script type="text/javascript">
$(function(){
    $("#master_btn").click(function(){
        $("#inputForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_master/').submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>売上価格変更</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>売上価格変更</span></h2>
<h2 class="HeaddingMid">価格変更の条件を指定します。</h2>
<?php echo $this->form->create( 'TrnRetroact',array('type' => 'post','action' => '' ,'id' =>'inputForm','class' => 'validate_form input_form') ); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.ownerText', array('type'=>'text' , 'class'=>'r', 'style'=>'width: 60px;', 'id'=>'ownerText')); ?>
                    <?php echo $this->form->input('TrnRetroactHeader.ownerCode', array('options'=>$owners, 'empty'=>true, 'class'=>'r txt validate[required]', 'id'=>'ownerCode')); ?>
                    <?php echo $this->form->input('TrnRetroactHeader.ownerName', array('type'=>'hidden', 'id'=>'ownerName')); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.work_class',array('options'=>$work_types, 'empty'=>true, 'class'=>'txt' ,'id'=>'work_class')); ?>
                    <?php echo $this->form->input('TrnRetroactHeader.work_class_txt',array('type'=>'hidden', 'id'=>'work_class_txt')); ?>
                </td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.facilityText', array('type'=>'text' , 'class'=>'r', 'style'=>'width: 60px;', 'id'=>'facilityText')); ?>
                    <?php echo $this->form->input('TrnRetroactHeader.facilityCode', array('options'=>$customers, 'empty'=>true, 'class'=>'r txt validate[required]', 'id'=>'facilityCode')); ?>
                    <?php echo $this->form->input('TrnRetroactHeader.facilityName', array('type'=>'hidden', 'id'=>'facilityName')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnRetroactHeader.recital', array('type'=>'text', 'class'=>'txt', 'maxlength' => '200')); ?></td>
            </tr>
            <tr>
                <th>遡及期間</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactHeader.start_date',array('type'=>'text','class'=>'r date validate[required,custom[date],funcCall2[date2]]')); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('TrnRetroactHeader.end_date',array('type'=>'text','class'=>'r date validate[required,custom[date],funcCall2[date2]]')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>適用開始日</th>
                <td><?php echo $this->form->input('TrnRetroactHeader.master_start_date', array('type'=>'text', 'class'=>'r date validate[required,custom[date],funcCall2[date2]]')); ?></td>
                <td></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <input type="button" class="btn btn23" id="master_btn" />
        </div>
    </div>
</form>
