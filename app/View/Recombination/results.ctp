<script type="text/javascript">
$(function(){
    $(document).ready(function(){
        $('#printSeal').click(function(){
            $(window).unbind('beforeunload');
            $('#recombination_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/dummy').submit();
        });
    });
    $(window).bind('beforeunload' ,function(){
        return 'シール印字が完了していません。';
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>recombination">組替商品選択</a></li>
        <li class="pankuzu">組替登録確認</li>
        <li>組替登録結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>組替登録結果</span></h2>
<div class="Mes01">以下の組替登録を行いました</div>

<form class="validate_form" id="recombination_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/dummy" method="post">
    <input type="hidden" name="data[facility_name]" value="<?php echo $facility_name; ?>" />
    <?php echo $this->form->input('MstFacility.id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected'))); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo $master['internal_code']; ?>" readonly />
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo $master['item_code']; ?>" readonly />
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo $master['work_type_name']; ?>" readonly />
                </td>
            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo $master['item_name']; ?>" readonly />
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo $master['dealer_name']; ?>" readonly />
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo $master['recital']; ?>" readonly />
                </td>
            </tr>
            <tr>
                <th>規格</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo $master['standard']; ?>" readonly />
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <h2 class="HeaddingSmall">作成シール</h2>

    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle01">
            <tr>
                <th class="col15">新シール</th>
                <th class="col5">包装単位</th>
                <th class="col10">作業区分</th>
                <th class="col20">備考</th>
                <th style="width:2px;"></th>
                <th class="col15">旧シール</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col10">有効期限</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle01">
            <?php $i=0;foreach($newResult as $_row): ?>
            <tr>
                <td align="center" class="col15"><?php echo h_out($_row['facility_sticker_no'],'center'); ?>
                    <input type="hidden" name="data[Recombination][facility_sticker_no][]" value="<?php echo $_row['facility_sticker_no']; ?>" />
                </td>
                <td class="col5"><?php echo h_out($_row['packing_name']); ?></td>
                <td class="col10"><input type="text" class="lbl" value="<?php echo $_row['work_type_name']; ?>" /></td>
                <td class="col20"><input type="text" class="lbl" value="<?php echo $_row['recital']; ?>" /></td>
                <?php if($i === 0): ?>
                <td rowspan="<?php echo count($newResult); ?>" style="width:2px;"></td>
                <td align="center" rowspan="<?php echo count($newResult); ?>" class="col15"><label title="<?php echo join('<br />',$oldResult['facility_sticker_no']); ?>"><p align="center"><?php echo join('<br />',$oldResult['facility_sticker_no']); ?></p></label></td>
                <td rowspan="<?php echo count($newResult); ?>" class="col10"><label title="<?php echo join('<br />',$oldResult['packing_name']); ?>"><?php echo join('<br />',$oldResult['packing_name']); ?></label></td>
                <td rowspan="<?php echo count($newResult); ?>" class="col10"><label title="<?php echo $master['lot_no']; ?>"><?php echo $master['lot_no']; ?></label></td>
                <td align="center" rowspan="<?php echo count($newResult); ?>" class="col10"><label title="<?php echo ($master['validated_date'] == '' ? '' : date('Y/m/d', strtotime($master['validated_date']))); ?>"><p align="center"><?php echo ($master['validated_date'] == '' ? '' : date('Y/m/d', strtotime($master['validated_date']))); ?></p></label></td>
                <?php endif; ?>
            </tr>
            <?php $i++;endforeach; ?>
        </table>
    </div>

    <div id="results">
        <h2 class="HeaddingSmall">現在の在庫</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
                <tr>
                    <th class="col40">単位</th>
                    <th class="col15">棚番号</th>
                    <th class="col15">在庫数</th>
                    <th class="col15">要求数</th>
                    <th class="col15">引当可</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll" style="">
            <table class="TableStyle01">
                <?php $i=0;foreach($stocks as $_row): ?>
                <tr class="<?php echo($i%2===0?'':'odd'); ?>">
                    <td class="col40"><?php echo h_out($_row['packing_name']); ?></td>
                    <td class="col15"><?php echo h_out($_row['shelf_name']); ?></td>
                    <td class="col15 right"><?php echo h_out($_row['stock_count'],'right'); ?></td>
                    <td class="col15 right"><?php echo h_out($_row['promise_count'],'right'); ?></td>
                    <td class="col15 right"><?php echo h_out($_row['promisable_count'],'right'); ?></td>
                </tr>
                <?php $i++;endforeach; ?>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" value="" class="btn btn18" id="printSeal" />
        </div>
    </div>
</form>
