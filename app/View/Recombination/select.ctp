<script type="text/javascript">
    $(document).ready(function(){
        $('#read_seal').keyup(function(e){
            var sticker = FHConvert.ftoh($(this).val().trim());
            if(e.which === 13 && sticker !== ''){
                if(sticker.match(/^01[0-9][0-9]{13}/)){
                    $('<option></option>').attr('value',sticker).html(sticker).appendTo('#center_seals');
                    $('<input>').attr('type', 'hidden').attr('name', 'data[Recombination][seals][]').attr('value', sticker).appendTo('#recombination_form');
                }else{
                    if($('#center_seals').find('option[value=' + sticker + ']').length === 0){
                        $('<option></option>').attr('value',sticker).html(sticker).appendTo('#center_seals');
                        $('<input>').attr('type', 'hidden').attr('name', 'data[Recombination][seals][]').attr('value', sticker).appendTo('#recombination_form');
                    }else{
                        alert("すでに読み込まれています");
                        $('#read_seal').focus();
                    }
                }
                $(this).val('');
            }else if(e.which === 13 && sticker == ''){
                $(this).val('');
            }
        });

        // DELキー
        $('#center_seals').keyup(function(e){
            if(e.which === 46){
                var idx = $(this).children().index($(this).find('option:selected:eq(0)'));
                $(this).children(':eq(' + idx + ')').remove();
                $('input[type=hidden][name="data[Recombination][seals][]"]:eq(' + idx + ')').remove();
                $('#read_seal').focus();
            }
        });

        // クリアボタン押下
        $('#clearSeals').click(function(){
            $('#center_seals').children().remove();
            $('input[type=hidden][name="data[Recombination][seals][]"]').remove();
            $('#read_seal').focus();
        });


        $('#confirm').click(function(){
            if($('#center_seals').children().length === 0){
                alert('組換商品を読み込んでください');
                $('#read_seal').focus();
                return false;
            }else{
                $('#recombination_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm').submit();
            }
        });
        $('#read_seal').focus();
    });
  
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>組替登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>シール読込</span></h2>

<form class="validate_form" id="recombination_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm" method="POST">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>シール番号</th>
                <td>
                    <input type="text" class="txt r" style="ime-mode:disabled;" id="read_seal" />
                </td>
            </tr>
            <tr>
                <th>作業区分</th>
                <td><?php echo $this->form->input('MstClass.id',array('options'=>$classes, 'empty'=>true, 'class'=>'txt')); ?></td>
            </tr>
            <tr>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Recombination.recital', array('class' => 'txt')); ?>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <select id="center_seals" size="20" style="width: 400px;" class="clist"></select>
                </td>
                <td valign="bottom"><input id="clearSeals" type="button" class="btn btn13" /></td>
            </tr>
        </table>

        <div class="ButtonBox">
            <input type="button" class="btn btn3 submit" id="confirm" />
        </div>
    </div>
</form>
