<script type="text/javascript">
$(function(){
    $(document).ready(function(){
        $('.checkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
        });

        $('#commit').click(function(){
            var sumStocks = 0;
            var hasError = false;
            var cnt = 0;
            $('input[type=checkbox].checkAllTarget').each(function(i){
                if(this.checked){
                    var q = $('input[type=text].inputQuantity:eq('+ i + ')').val();
                    cnt++;
                    if(q == ''){
                        alert('数量を入力してください');
                        hasError = true;
                        return false;
                    }else if(q.match(/[\D]/g)){
                        hasError = true;
                        alert('数量に正しくない入力があります');
                        return false;
                    }else if(parseInt(q) > parseInt($('span.maxCreatableCount').text())){
                        hasError = true;
                        alert('最大作成可能数以上の入力はできません');
                        return false;
                    }else{
                        sumStocks += parseInt(q) * parseInt($('span.perUnit:eq(' + i + ')').text());
                    }
                }
            });

            if(hasError){
                return false;
            }else if(cnt === 0){
                alert('在庫情報を選択してください');
                return false;
            }

            if(parseInt($('#maxUnits').val()) !== sumStocks){
                alert('入荷数量を適切に分配してください');
                return false;
            }
            $(window).unbind('beforeunload');
            $('#recombination_confirm_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/commit').submit();
        });
    });
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });

  
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">組替商品選択</a></li>
        <li>組替登録確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>組替登録確認</span></h2>

<form class="validate_form" id="recombination_confirm_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/commit" method="POST">
    <input type="hidden" id="maxUnits" value="<?php echo $maxUnits; ?>" />
    <?php echo $this->form->input('Recombination.time' , array('type'=>'hidden' , 'value'=> date('Y/m/d H:i:s.u')))?>
    <input type="hidden" name="data[showTime]" value="<?php echo time(); ?>" />
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo isset($master['internal_code']) ? $master['internal_code'] : ''; ?>" readonly />
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo isset($master['item_code']) ? $master['item_code'] : ''; ?>" readonly />
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo $workType['name']; ?>" readonly />
                    <?php echo $this->form->hidden('Recombination.work_type', array('value' => $workType['id'])); ?>
                </td>
            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo isset($master['item_name']) ? $master['item_name'] : ''; ?>" readonly />
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo isset($master['dealer_name']) ? $master['dealer_name'] : ''; ?>" readonly />
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Recombination.recital', array('readonly' => 'readonly', 'class'=>'lbl', 'value' => $recital)); ?>
                </td>
            </tr>
            <tr>
                <th>規格</th>
                <td>
                    <input type="text" class="lbl" value="<?php echo isset($master['standard']) ? $master['standard'] : ''; ?>" readonly />
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <h2 class="HeaddingSmall">対象商品</h2>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle01">
            <tr>
                <th class="col25">センターシール</th>
                <th class="col15">包装単位</th>
                <th class="col20">ロット番号</th>
                <th class="col15">有効期限</th>
                <th class="col25"></th>
            </tr>
        </table>
    </div>
    <div class="TableScroll" style="">
        <table class="TableStyle01">
            <?php $i=0;foreach($stickers as $_row): ?>
            <tr class="<?php echo($i%2===0?'':'odd'); ?>">
                <td class="col25"><?php echo h_out($_row['facility_sticker_no'] ,'center'); ?></td>
                <td class="col15"><?php echo h_out($_row['packing_name']); ?></td>
                <td class="col20"><?php echo h_out($_row['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($_row['validated_date'],'center'); ?></td>
                <td class="col25"><label title="<?php echo (isset($_row['error']) ? join('<br />',$_row['error']) : ''); ?>"><?php echo (isset($_row['error']) ? '<span style="color:red;">' . join('<br />',$_row['error']) . '</span>' : ''); ?></label></td>
            </tr>
            <?php $i++;endforeach; ?>
        </table>
    </div>
    <?php if($hasError === false) : ?>
    <div id="results">
        <h2 class="HeaddingSmall">作成可能単位</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
                <tr>
                    <th style="width:20px;"><input type="checkbox" class="checkAll" /></th>
                    <th class="col10">包装単位</th>
                    <th class="col10">棚番号</th>
                    <th class="col10">在庫数</th>
                    <th class="col10">要求数</th>
                    <th class="col10">引当可</th>
                    <th class="col10">作成単位</th>
                    <th class="col10">最大作成可能</th>
                    <th class="col10">数量</th>
                    <th class="col10">作業区分</th>
                    <th class="col10">備考</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll" style="">
            <table class="TableStyle01">
                <?php $i=0;foreach($stocks as $_row): ?>
                <tr class="<?php echo ($i%2===0?'':'odd'); ?>">
                <?php if($_row['is_right_term_pack'] == TRUE){ //使用可能な包装単位 ?>
                    <td style="width:20px;" class="center"><?php echo $this->form->checkbox("Recombination.Rows.{$_row['id']}.selected", array('class'=>'checkAllTarget')); ?></td>
                <?php }else{ ?>
                    <td style="width:20px;"><?php echo $this->form->text("Recombination.Rows.noId.selected", array('type'=>'hidden')); ?></td>
                <?php } ?>
                    <td class="col10"><?php echo h_out($_row['packing_name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['shelf_name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['stock_count'],'right'); ?></td>
                    <td class="col10"><?php echo h_out($_row['promise_count'],'right'); ?></td>
                    <td class="col10"><?php echo h_out($_row['promisable_count'],'right'); ?></td>

                <?php if($_row['is_right_term_pack'] == TRUE){ //使用可能な包装単位 ?>

                    <td class="col10"><span class="perUnit"><?php echo h_out($_row['per_unit'],'right'); ?></span></td>
                    <td class="col10"><span class="maxCreatableCount"><?php echo h_out($_row['max_creatable_count'],'right'); ?></td>
                    <td class="col10"><?php echo $this->form->input("Recombination.Rows.{$_row['id']}.quantity", array('class' => 'r num inputQuantity','style'=>'width:90%',"maxlength"=>10,'value'=>$_row['max_creatable_count'])); ?></td>
                    <td class="col10"><?php echo $this->form->input('MstClass.id',array('options'=>$classes,'empty'=>true, 'class'=>'txt', 'name'=>"data[Recombination][Rows][{$_row['id']}][work_type]")); ?></td>
                    <td class="col10"><?php echo $this->form->input("Recombination.Rows.{$_row['id']}.recital", array('class'=>'txt','style'=>'width:90%',"maxlength"=>200)); ?></td>

                <?php }else{ ?>
                    <td class="col10"><span><?php echo h_out($_row['per_unit'],'right'); ?></span></td>
                    <td class="col10"><span><?php echo h_out($_row['max_creatable_count'],'right'); ?></span></td>
                    <td class="col10"><?php echo $this->form->input("Recombination.Rows.noId.quantity", array('class' => 'lbl','readOnly'=>'readOnly')); ?></td>
                    <td class="col10"><?php echo $this->form->input('MstClass.id',array('options'=>$classes, 'empty'=>true, 'class'=>'txt', 'name'=>"data[Recombination][Rows][noId][work_type]")); ?></td>
                    <td class="col10"><?php echo $this->form->input("Recombination.Rows.noId.recital", array('class'=>'txt',"maxlength"=>200)); ?></td>
                <?php } ?>
                </tr>
                <?php $i++;endforeach; ?>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="btn btn2 submit [p2]" id="commit" />
        </div>
    </div>
    <?php endif; ?>
</form>
