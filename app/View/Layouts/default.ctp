<<?php ?>?xml version="1.0" encoding="UTF-8" ?>
<?php echo $this->html->docType(); ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
    <head>
    <?php echo $this->html->charset(); ?>
        <meta http-equiv="Content-Style-Type" content="text/css" />
        <meta http-equiv="Content-Script-Type" content="text/javascript" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Cache-Control" content="no-cache" />
        <meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT" />
        <title><?php echo $title_for_layout; ?></title>
        <?php echo $this->html->css(array('x.generic','jquery-ui','mbExtruder1','round-button','validationEngine.jquery','jquery.jgrowl','jgrowl-theme','datePicker'));?>
        <script type="text/javascript">
            var basepath = "<?php echo $this->webroot; ?><?php echo $this->name; ?>";
            var webroot = "<?php echo $this->webroot; ?>";
        </script>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/menu_black.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/list_menu.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/zurasale.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/bench.css" media="screen" />

        <!-- ダッシュボードレイアウト用「foundation」ここから -->
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/foundation.css" media="screen" />
        <!-- //ダッシュボードレイアウト用「foundation」ここまで -->
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/dashboard.css" media="screen" />

        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript">
            var $jq1_7_2_min = jQuery = $.noConflict(true);
            var $jqueryForLb = $jq1_7_2_min;
        </script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.bgiframe.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/common.js?ver=<?php echo time(); ?>"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.datePicker.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.ui.datepicker-ja.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.validationEngine-ja.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-ui-1.8.1.custom.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.jgrowl.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.selectboxes.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jgrowl-theme.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.event.wheel.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/fhconvert.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.tablefix.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/floater.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.mousewheel.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>inc/jquery.metadata.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>inc/jquery.hoverIntent.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>inc/mbMenu.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/thickbox.js"></script>
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/thickbox.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/lightbox.css" type="text/css" media="screen" />
        <script type="text/javascript">
<?php $pre=""; for($i=0;$i<$this->Session->read('Auth.Config.CodeLength');$i++){ $pre=$pre . "0";}; ?>
var internal_code_prefix = "<?php echo $pre; ?>";
var internal_code_length = "<?php echo $this->Session->read('Auth.Config.CodeLength'); ?>";
dclick_flg = false

//全選択・解除
function listAllCheck(target){
    $(".chk").each(function() { this.checked=target.checked; });
}

$(document).ready(function() {
    simple_tooltip(".btn","tooltip");
    $('#loading').hide().ajaxStart(function() {
        $(this).show();
        $(this).bgiframe();
    }).ajaxStop(function() {
        $(this).hide();
    });

    // ウィンドウサイズの80%でスクロール固定
    //var _h = $(window).height();
    //var h = _h * 0.8;
    // テーブルスクロール対応
    //$('.TableScroll').tablefix({height: h, fixRows: 1});
    // テーブルスクロール対応2行の場合
    //$('.TableScroll2').tablefix({height: h, fixRows: 2});

    /*
    // マウスオーバーで色変え
    $(".TableStyle01 tr").hover(
        function(){
            $(this).addClass('tr-hover');
        },
        function(){
            $(this).removeClass('tr-hover');
        }
    );

    // 行クリックで、チェックボックス制御
    $(".TableStyle01 tr").click(function(){
        $(this).find('input[type=checkbox]:eq(0)').attr('checked' , !$(this).find('input[type=checkbox]:eq(0)').attr('checked'));
        if($(this).find('input[type=checkbox]:eq(0)').attr('checked')){
            $(this).addClass('tr-selected');
        }else{
            $(this).removeClass('tr-selected');
        }
    });
    */

    //2度押し対策
    if ( dclick_flg == true ) {
        event.keyCode = null;
        return false ;
    }

    // 戻るボタン
    $("#btn_back").click(function(){
        history.back();
    });

    // お気に入りメニュー
    $("#fav_menu").hide();
    $("#fav_menu_btn").click(function(){
        $("#fav_menu").toggle();
        $("#main_content").toggleClass("fav");
    });

    //validate engine
    $(".validate_form").validationEngine({
        onValidationComplete : function( form , status ){
            if(status == true ) {
                if(!form.attr('action').match(/csv|export|report|seal|beep/ig)){
                    dclick_flg = true;
                    setTimeout(function(){$('#waitingPanel').click()},100);
                    $('input[type=button]').attr('disabled' , 'disabled');
                }
            } else {
                tb_remove();
            }
        }
    });

    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if($(e.target).attr('id').search( /Text$/ ) == -1 ){ // Textで終わるIDだった場合送信しない。
                if(form.hasClass('search_form')){ // 検索画面だったら
                    validateDetachSubmit(form , form.attr('action')); //submit実行
                }
            }
        }
    } );

    //convert from 全角 to 半角
    $('form.search_form').submit(function(){
        $('form.search_form').find('input[type=text]').each(function(){
            $(this).val($.trim($(this).val()));
        });
        //$('form.search_form').find('input.search_canna').each(function(){
        //    this.value = FHConvert.ftoh(FHConvert.fkktohkk(this.value),{jaCode:true});
        //});
        $('form.search_form').find('input.search_upper').each(function(){
            this.value = FHConvert.ftoh(FHConvert.fkktohkk(this.value),{jaCode:true}).toUpperCase();
        });
        $('form.search_form').find('input.search_internal_code').each(function(){
            if(this.value.length > 0){
                this.value = (internal_code_prefix+this.value).slice('-'+internal_code_length);
            }
        });
    });

    // 施設名などを補完
    $('form.input_form').submit(function(){
        $("#ownerName").val($("#ownerCode option:selected").text());
        $("#facilityName").val($("#facilityCode option:selected").text());
        $("#departmentName").val($("#departmentCode option:selected").text());
        $("#className").val($("#classId option:selected").text());
        $("#supplierName").val($("#supplierCode option:selected").text());
        $("#hospitalName").val($("#hospitalCode option:selected").text());
        $("#centerName").val($("#centerCode option:selected").text());
        $('#work_class_txt').val($('#work_class option:selected').text());
    });

    // 入力フォーム共通
    $('form.input_form').ready(function(){
        // カレンダー
        $(".date").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});

        // 施設プルダウンを変更したら施設コードを入力
        $('#facilityCode').change(function(){
            $('#facilityText').val($('#facilityCode').val());
            getDepartment($(this).val());
        });

        // 施設コードを入力したら施設プルダウンを変更
        $('#facilityText').blur(function(){
            $('#facilityCode').selectOptions($(this).val());
            getDepartment($('#facilityCode').val());
        });

        // 施設コード入力 ENTER で変更
        $( 'input[type=text]#facilityText').keypress( function ( e ) {
            if ( e.which == 13 ) { // ENTER
                $('#facilityCode').selectOptions($(this).val());
                getDepartment($('#facilityCode').val());
            }
        } );

        // 部署プルダウンを変更したら部署コードを入力
        $('#departmentCode').change(function(){
            $('#departmentText').val($(this).val());
        });

        // 部署コードを入力したら部署プルダウンを変更
        $('#departmentText').blur(function(){
            $('#departmentCode').selectOptions($(this).val());
        });

        // 部署コード入力 ENTER で変更
        $( 'input[type=text]#departmentText').keypress( function ( e ) {
            if ( e.which == 13 ) { // ENTER
                $('#departmentCode').selectOptions($(this).val());
            }
        } );

        // 施主プルダウンを変更したら施主コードを入力
        $('#ownerCode').change(function () {
            $('#ownerText').val($(this).val());
        });

        // 施主コードを入力したら施主プルダウンを変更
        $('#ownerText').blur(function(){
            $('#ownerCode').selectOptions($(this).val());
        });

        // 施主コード入力 ENTER で変更
        $( 'input[type=text]#ownerText').keypress( function ( e ) {
            if ( e.which == 13 ) { // ENTER
                $('#ownerCode').selectOptions($(this).val());
            }
        } );

        // 仕入先プルダウンを変更したら仕入先コードを入力
        $('#supplierCode').change(function () {
            $('#supplierText').val($('#supplierCode option:selected').val());
        });

        // 仕入先コードを入力したら仕入先プルダウンを変更
        $('#supplierText').blur(function(){
            $('#supplierCode').selectOptions($(this).val());
        });

        // 仕入先コード入力 ENTER で変更
        $( 'input[type=text]#supplierText').keypress( function ( e ) {
            if ( e.which == 13 ) { // ENTER
                $('#supplierCode').selectOptions($(this).val());
            }
        } );

        // 病院プルダウンを変更したら仕入先コードを入力
        $('#hospitalCode').change(function () {
            $('#hospitalText').val($('#hospitalCode option:selected').val());
        });

        // 病院コードを入力したら仕入先プルダウンを変更
        $('#hospitalText').blur(function(){
            $('#hospitalCode').selectOptions($(this).val());
        });

        // 病院コード入力 ENTER で変更
        $( 'input[type=text]#hospitalText').keypress( function ( e ) {
            if ( e.which == 13 ) { // ENTER
                $('#hospitalCode').selectOptions($(this).val());
            }
        } );

        // センタープルダウンを変更したら仕入先コードを入力
        $('#centerCode').change(function () {
            $('#centerText').val($('#centerCode option:selected').val());
        });

        // センターコードを入力したら仕入先プルダウンを変更
        $('#centerText').blur(function(){
            $('#centerCode').selectOptions($(this).val());
        });

        // センターコード入力 ENTER で変更
        $( 'input[type=text]#centerText').keypress( function ( e ) {
            if ( e.which == 13 ) { // ENTER
                $('#centerCode').selectOptions($(this).val());
            }
        } );

        // 商品カテゴリ1 を変更したらカテゴリ2を取得 、カテゴリ3を空に
        $('#item_category1').change(function(){
            getCategoryList(2 , $(this).val());
            $('#item_category3').children().remove();
        });

        // 商品カテゴリ2 を変更したらカテゴリ2を取得
        $('#item_category2').change(function(){
            getCategoryList(3 , $(this).val());
        });

    });

    $('form').submit(function(){
        $('form').find('input[type=text]').each(function(){
            if( this.value.indexOf("\u00a0") > -1 ){
                 this.value = this.value.replaceAll("\u00a0" , "\u0020");
            }
        });
    });
});

// 自動スクロール制御
$(document).ready(function(){
    var floatingMenu = $('#floating-menu');
    var btnAutoScrollTop = $("#btnAutoScrollTop");
    var btnAutoScrollBottom = $("#btnAutoScrollBottom");

    var topPosition;
    var bottomPosition;

    btnAutoScrollTop.hide();
    btnAutoScrollBottom.hide();
    refreshAutoScrollButton();

    var floatingMenuDisable = null;
    function refreshAutoScrollButton() {
        // 初期表示状態がhiddenの場合は制御しない
        if (floatingMenuDisable == null) {
            floatingMenuDisable = floatingMenu.is(':hidden');
        }
        if (floatingMenuDisable === true) return;

        var autoScrollTop = $("#autoScrollTop");
        var autoScrollBottom = $("#autoScrollBottom");
        var offsetTop = autoScrollTop.offset();
        var offsetBottom = autoScrollBottom.offset();
        topPosition = offsetTop !== null ? offsetTop.top : 0;
        bottomPosition = offsetBottom !== null ? offsetBottom.top : $(document).height();

        var visibleTop = $(window).scrollTop();
        var visibleBottom = visibleTop + $(window).height();

        if((visibleTop <= topPosition && visibleBottom >= bottomPosition)
                || visibleBottom < topPosition || visibleTop > bottomPosition) {
            // 範囲が全て見えている、または完全に範囲外の場合はボタン表示しない
            btnAutoScrollTop.hide();
            btnAutoScrollBottom.hide();
        } else {
            if (visibleTop > topPosition) {
                floatingMenu.show();
                btnAutoScrollTop.show();
            } else {
                btnAutoScrollTop.hide();
            }
            if (visibleBottom < bottomPosition) {
                floatingMenu.show();
                btnAutoScrollBottom.show();
            } else {
                btnAutoScrollBottom.hide();
            }
        }

        if (btnAutoScrollTop.is(':hidden') && btnAutoScrollBottom.is(':hidden')) {
            floatingMenu.hide();
        }
    }

    // 上部へスクロール
    btnAutoScrollTop.click(function(){
        $("html,body").animate({scrollTop: topPosition}, 'slow');
    });

    // 下部へスクロール
    btnAutoScrollBottom.click(function(){
        $("html,body").animate({scrollTop: bottomPosition}, 'slow');
    });

    var resizeTimer = false;
    $(window).resize(function() {
        if (resizeTimer !== false) {
            clearTimeout(resizeTimer);
        }
        resizeTimer = setTimeout(function() {
            console.log($(document).height());
            refreshAutoScrollButton();
        }, 200);
    });

    var scrollTimer = false;
    $(window).scroll(function() {
        if (scrollTimer !== false) {
            clearTimeout(scrollTimer);
        }
        scrollTimer = setTimeout(function() {
            refreshAutoScrollButton();
        }, 200);
    });

});

$(window).load(function () {
    // Windowを読み込み終わったら処理をする。
    // 必須セレクトボックスで要素数が決まっている場合あらかじめ選択しておく。
    $('select.r').each(function(i){
        if($(this).children().length == 2 ){
            if($(this).children(':eq(0)').val() == ''){
                if($(this).val() != $(this).children(':eq(1)').val()){
                    $(this).val(($(this).children(':eq(1)').val())).trigger("change");
                }
            }
        }
    });
});

        </script>
        <!-- 権限の設定 -->
        <?php echo RoleManager::getControlCode($this->Session->read('Auth.MstRole.id'), $this->Session->read('Role.function_id') ); ?>
    </head>
    <body>
        <a id="waitingPanel" href="#TB_inline?height=130&width=280&inlineId=waitingMessage&modal=true" class="thickbox" style="display: none;"></a>
            <div id="waitingMessage" style="text-align: center;padding: 10px 0 0 0;display: none;">
                <div style="text-align:center;width: 100%;padding-top:15px;">
                    <p>しばらくお待ちください．．．</p>
                    <p><img src="<?php echo $this->webroot; ?>img/loadingAnimation.gif" alt="" /></p>
                </div>
            </div>
            <div id="wrapper">

            <?php echo $this->element('header'); ?>

            <div id="main">
                <!-- お気に入りメニュー -->
                <?php echo $this->Session->read('Auth.favorite_menu'); ?>
                <!-- メインコンテンツ -->
                <div id="main_content">
                    <div id="content"><?php echo $content_for_layout; ?></div>
                </div>
            </div>

        </div>
        <?php echo $this->element('footer'); ?>
        <?php echo $this->element('menu');?>

        <!-- カスタマイズ -->
        <?php if ( Configure::read('debug') >= 2 ) { ?>
           <?php echo $this->element('sql_dump'); ?>
        <?php } ?>
        <?php echo $this->session->flash(); ?>
        <div id="loading" style="">Now loading...</div>

        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/lightbox.js">jQuery.noConflict();</script>

    </body>
</html>
