<script>

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/informations_list">お知らせ一覧</a></li>
        <li>お知らせ編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">お知らせ情報</h2>
<form class="validate_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/result" method="post">
    <?php echo $this->form->input('TrnInformation.id' , array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>公開先</th>
                <td>
                    <?php echo $this->form->input('TrnInformation.mst_facility_id' , array('options'=>$CenterList , 'class'=>'r' ,  'empty'=>'すべて'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>公開期間</th>
                <td>
                    <?php echo $this->form->input('TrnInformation.start_date' , array('type'=>'text' , 'class'=>'r date validate[required,custom[date]]' , 'id'=> 'datepicker1')); ?>
                    ～
                    <?php echo $this->form->input('TrnInformation.end_date' , array('type'=>'text' , 'class'=>'date validate[custom[date]]', 'id'=> 'datepicker2')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>タイトル</th>
                <td>
                    <?php echo $this->form->input('TrnInformation.title' , array('type'=>'text' , 'class'=>'r validate[required]')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>内容</th>
                <td>
                    <textarea id="textarea" name="data[TrnInformation][message]" cols="50" rows="20" maxlength="200" class="r validate[required]" style="height:auto !important"><?php echo $this->request->data['TrnInformation']['message'] ?></textarea>
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="ButtonBox">
        <input type="submit" class="btn btn2 [p2]" value=""/>
    </div>
</form>
