<script>
    $(function(){
        $("#Add").click(function(){
            $("#InfoList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add').submit();
        });
        $(".Mod").click(function(){
            id = $(this).parent(':eq(0)').find('input[type=hidden]:eq(0)').val();
            $("#InfoList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod/'+id);
            $("#InfoList").submit();
        });
        $(".Del").click(function(){
            id = $(this).parent(':eq(0)').find('input[type=hidden]:eq(0)').val();
            $("#InfoList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/informations_list/'+id);
            $("#InfoList").submit();
        });
    });
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>お知らせ一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">お知らせ一覧</h2>

<form class="validate_form search_form" action='<?php echo $this->webroot; ?><?php echo $this->name; ?>/informations_list/' method="post" id="InfoList">
    <div class="results">
       <div class="TableScroll">
           <table class="TableStyle01">
               <colgroup>
                   <col />
                   <col />
                   <col />
                   <col />
                   <col width="50" />
                   <col width="50" />
               </colgroup>
               <tr>
                   <th>公開期間</th>
                   <th>公開先</th>
                   <th>タイトル</th>
                   <th>内容</th>
                   <th colspan="2">操作</th>
               </tr>
               <?php $cnt=0;foreach ($Information_List as $info) { ; ?>
               <tr>
                   <td><?php echo $info['TrnInformation']['start_date'] ?>　～　<?php echo $info['TrnInformation']['end_date'] ?></td>
                   <td><?php echo $info['TrnInformation']['facility_name'] ?></td>
                   <td><?php echo $info['TrnInformation']['title'] ?></td>
                   <td><?php echo htmlspecialchars($info['TrnInformation']['message']) ?></td>
                   <td class="center"><input type="button" class="btn btn9 Mod [p2]"/><?php echo $this->form->input('TrnInformation.mod.'.$cnt, array('type'=>'hidden' , 'value'=>$info['TrnInformation']['id'])); ?></td>
                   <td class="center"><input type="button" class="btn btn39 Del [p2]"/><?php echo $this->form->input('TrnInformation.del.'.$cnt, array('type'=>'hidden' , 'value'=>$info['TrnInformation']['id'])); ?></td>
               </tr>
               <?php $cnt++;} ?>
           </table>
       </div>
   </div>
   <div class="ButtonBox">
       <input type="button" id="Add" class="btn btn11 [p2]"/>
   </div>
</form>
