<script>
  $(document).ready(function(){
  $('body').addClass('loginBody');
  $('#content').css({"background-color":"#F9F9F9"});
   $('#MstUserLoginId').focus();
  
    $('#MstUserLoginId').keydown(function(e){
        if(e.which === 13){ //エンターキー押下
            $('#MstUserPassword').focus();
            return false;
        }
    });
    $('#MstUserPassword').keydown(function(e){
        if(e.which === 13){ //エンターキー押下
            $('#LoginForm').submit();
        }
    });
    $('#login_btn').click(function(e){
        $('#LoginForm').submit();
    });
  
});
</script>

<header>
<h1 class="center login-logo"><img src="<?php echo $this->webroot?>img/login/login_logo.png" alt="コンパクトSPDシサービス「CoslosCloud」コスロス・クラウド" width="360px" height="100px" /></h1>
</header>

<article>
<div id="login-page" class="center" style="display:block;">
    <form method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/login" accept-charset="utf-8" id="LoginForm" onsubmit="return false;">
    <?php if ($this->session->check('Message.auth')) { echo $this->session->flash('auth'); } ?>
        <div class="center">
          <label for="user_email">メールアドレス</label>
          <div><input class="user validate[required]" type="text" name="data[MstUser][login_id]" id="MstUserLoginId" /></div>
        </div>
        <div class="center">
          <label for="user_password">パスワード</label>
          <div><input class="pass validate[required]" type="password" name="data[MstUser][password]" id="MstUserPassword" /></div>
        </div>
        <div class="button-wrapper center">
          <input class="btn-primary login-button" data-disable-with="ログインしています..." id="login_btn" name="commit" type="submit" value="ログイン" />
        </div>
    </form> 
    <?php echo $this->Html->link('パスワードを忘れた方はこちらからお問い合わせください。',  array('controller' => 'Password', 'action' => 'lost')) ?>
    <a href="/cocontact/" target="_blank"></a>
</div>
<a class="center" style="display:block;" href="http://cloud.coslos.jp/contact/" target="_blank">まだアカウントをお持ちでない方はこちら</a>
</article>
<footer>
<nav class="navbar">
<ul>
<li><a href="http://cloud.coslos.jp" target="_blank">Coslos Cloud</a></li>
<li><a href="http://www.andinfo.co.jp/company/summary.html" target="_blank">運営会社</a></li>
<li><a href="http://cloud.coslos.jp/terms/" target="_blank">利用規約</a></li>
<li><a href="https://coslos.jp/guide/privacy.php" target="_blank">プライバシーポリシー</a></li>
<li><a href="http://cloud.coslos.jp/contact/" target="_blank">ご相談窓口</a></li>
</ul>
</nav>
<p class="copyRight">Copyright @ AND Inc.</p>
</footer>
