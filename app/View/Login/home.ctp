<script type="text/javascript">
$(document).ready(function() {
    $('#floating-menu').css('display','none');
    // お知らせ本文の非表示
    $(".news_body").hide();

    // お知らせクリック
    $(".infomation").click(function(){
        $(this).next().slideToggle("slow");
    });
});
</script>
<?php echo $this->session->flash(); ?>

<!--ここから-->
<form name="base_form" >
<input name="base_url" type="hidden" value="<?php echo $this->webroot; ?>">
<input name="catalog_url" type="hidden" value="<?php echo Configure::read('catalog.url'); ?>session.mvc/login?uiand=" . $loginUser['login_id'] . "&paand=" . $loginUser['password']  . "'>
<input name="login" type="hidden" value="<?php echo $this->Session->read('Auth.MstUser.login_id'); ?>">
<input name="pass" type="hidden" value="<?php echo $this->Session->read('Auth.MstUser.password')?>">
</form>
<!--ここまで-->

<!--ダッシュボード-->
<div id="dashboard-widget-box">
    <div class="row fullWidth">
        <div class="medium-4 columns">
            <!-- 左カラム内容 -->
            <div class="box dash-claim">
                <h2>部署請求件数</h2>
                <p class="dcont-01"><span><?php echo $CntOrder; ?></span>件</p>
                <ul class="dash-menu">
                    <li>
                        <h3>発注</h3></li>
                    <li><a href="<?php echo Configure::read('catalog.url'); ?>session.mvc/login?uiand=<?php echo $this->Session->read('Auth.MstUser.login_id'); ?>&paand=<?php echo $this->Session->read('Auth.MstUser.password')?>" target="_blank">部署請求</a></li>
                    <li><a href="<?php echo $this->webroot; ?>Orders/order_decision">発注確定</a></li>
                </ul>
            </div>

            <div class="box dash-enter">
                <h2>入荷待ち件数</h2>
                <p class="dcont-01"><span><?php echo $CntReceiving; ?></span>件</p>
                <p class="dcont-02">1週間以上待ち件数<span><?php echo $CntOneWeekReceiving; ?></span>件</p>
                <ul class="dash-menu">
                    <li>
                        <h3>入荷処理</h3></li>
                    <li><a href="<?php echo $this->webroot; ?>Receipts/index">入荷処理</a></li>
                    <li><a href="<?php echo $this->webroot; ?>ReceiptsHistory/index">入荷履歴</a></li>
                </ul>
            </div>
            <?php if($this->Session->read('Auth.Config.ZuraFlg')){ ?>
            <div class="box dash-billing">
                <h2>月別請求予定金額</h2>
                <dl class="bill-table">
                    <dt>4月</dt>
                    <dd><span>400,000</span>円</dd>
                    <dt>5月</dt>
                    <dd><span>6,000</span>円</dd>
                    <dt>6月</dt>
                    <dd><span>106,000</span>円</dd>
                    <dt>7月</dt>
                    <dd><span>0</span>円</dd>
                    <dt>8月</dt>
                    <dd><span>0</span>円</dd>
                    <dt>9月</dt>
                    <dd><span>0</span>円</dd>
                </dl>
                <ul class="dash-menu">
                    <li><a href="<?php echo $this->webroot; ?>ZuraSale/report">請求書発行</a></li>
                    </ul>
            </div>
            <?php } ?>
            <!-- //左カラム内容 -->
        </div>
        <div class="medium-4 columns">
            <!-- 中央カラム内容 -->
            <div class="box dash-order">
                <h2>今月の発注状況</h2>
                <h3>合計発注件数</h3>
                <p class="dcont-01"><span><?php echo $Order['Order']['count']; ?></span>件</p>
                <h3>
                </h3>
                <h3>発注合計金額［<?php echo date("Y.m.01", time());?>〜<?php echo date("Y.m.t", time());?>］</h3>
                <p class="dcont-01"><span><?php echo $Order['Order']['price']; ?></span>円</p>
                <ul class="dash-menu">
                    <li>
                        <h3>発注</h3></li>
                    <li><a href="<?php echo $this->webroot; ?>Orders/order_history">発注履歴</a></li>
                    </ul>
            </div>
            <div class="box dash-master">
                <h2>Myマスタ登録件数</h2>
                <p class="dcont-01"><span><?php echo number_format($masterItemInfo['use_count']); ?></span>件</p>
                <p class="dcont-02">上限まであと<span><?php echo number_format($masterItemInfo['max_count'] - $masterItemInfo['use_count']); ?></span>件</p>
                <ul class="dash-menu">
                    <li>
                        <h3>Myマスタ</h3></li>
                    <li><a href="<?php echo $this->webroot; ?>MstFacilityItems/search">Myマスタ新規登録</a></li>
                    <li><a href="<?php echo $this->webroot; ?>MstFacilityItems/edit_search">Myマスタ一覧</a></li>
                    </ul>
            </div>
            <?php if($this->Session->read('Auth.Config.ZuraFlg')){ ?>
            <div class="box dash-zura">
              <h2>支払いサイト延伸<br>[ZuraSale]</h2>
              <h3>支払いサイト延伸可能金額</h3><p class="dcont-01"><span><?php echo $extensionUpper; ?></span>円</p>
              <h3>支払サイト延伸申込期限まで</h3><p class="dcont-02">あと<span><?php echo $Day; ?></span>日</p>
               <ul class="dash-menu">
                <li><a href="<?php echo $this->webroot; ?>ZuraSale/search">支払いサイト延伸</a></li>
           　　</ul>
            </div>
            <?php } ?>
            <!-- //中央カラム内容 -->
        </div>
        <div class="medium-4 columns">
            <!-- 右カラム内容 -->
            <div class="box dash-info">
              <h2>お知らせ</h2>
            　<ul>
                <?php foreach($info as $i){ ?>
                <li><span><?php echo $i['TrnInformation']['start_date']; ?></span><br><?php echo $i['TrnInformation']['title']; ?></li>
                <?php } ?>
              </ul>
            </div>

            <div class="box dash-coslos">
              <h2>コスロスマスタ総件数</h2>
              <p class="dcont-01"><span><?php echo number_format($masterItemInfo['grand_count']); ?>件</span></p>
            </div>
            <div class="box dash-bench">
              <h2>価格比較<br>メッカル医療材料ベンチマーク</h2>
            　 <ul class="dash-menu">
                <?php if($this->Session->read('Auth.Config.BenchFlg')){ ?>
                <li><a href="https://mecculbi.healthcaresolution.jp/InfoViewApp/logon.jsp" target="_blank">詳細はこちら</a></li>
                <?php } else { ?>
                <li><a href="<?php echo $this->webroot; ?>BenchMarks">詳細はこちら</a></li>
                <?php } ?>
           　　</ul>
            </div>
            <div class="box dash-others">
              <h2>部署利用者数</h2><p class="dcont-02"><span><?php echo $CntUser; ?></span>ユーザ</p>
              <h2>部署数</h2><p class="dcont-02"><span><?php echo $CntDepartment; ?></span>部署</p>
              <h2>仕入先数</h2><p class="dcont-02"><span><?php echo $CntSupplier; ?></span>業者</p>
                 <ul class="dash-menu">
                  <li><h3>運用設定</h3></li>
                  <li><a href="<?php echo $this->webroot; ?>MstDepartments/departments_list">部署設定</a></li>
                  <li><a href="<?php echo $this->webroot; ?>MstUsers/userlist">利用者登録</a></li>
                  <li><a href="<?php echo $this->webroot; ?>MstFacilities/facilities_list">仕入先一覧</a></li>
                  <li><a href="<?php echo $this->webroot; ?>FixedCounts/list_by_item">定数設定</a></li>
             　　</ul>
            </div>
            <!-- //右カラム内容 -->
        </div>
    </div>
</div>
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script>
    $(document).foundation();
</script>
<!--ダッシュボード ここまで-->
<p class="clear"></p>
