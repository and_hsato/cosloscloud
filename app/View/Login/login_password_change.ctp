<script>
function checkPassword(field, rules, i, options){
    if (field.val() == $('#oldpassword').val()) {
        return options.allrules.checkNotEquals.alertText;
    }
}

$(document).ready(function(){
    $("#update_btn").click(function(){
        $("#LoginForm").attr('action' , '<?php echo $this->webroot; ?>login/loginPasswordResult/');
        $("#LoginForm").attr('method', 'post');
        $("#LoginForm").submit();  
    });
    $("#logout_btn").click(function(){
        validateDetachSubmit($("#LoginForm") , '<?php echo $this->webroot; ?>login/logout/');
    });
});
</script>
<form class="validate_form" id="LoginForm">
<?php echo $this->form->input('MstUser.id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.MstUser.id') , 'id'=>'id') );?>
    <hr class="Clear" />
    <div align="center" style="margin-top:100px;">
    <span class="ColumnRed">パスワード更新</span>
    <table class="">
        <tr>
            <td>現在のパスワード</td>
            <td><input class="pass validate[required]" type="password" name="data[MstUser][old_password]" id="oldpassword" /></td>
        </tr>
        <tr>
            <td>新しいパスワード</td>
            <td><input class="pass validate[required,funcCall[checkPassword],custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]" type="password" name="data[MstUser][password]" id="password" /></td>
        </tr>
        <tr>
            <td>（再入力）</td>
            <td><input class="pass validate[required,equals[password],custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]" type="password" name="data[MstUser][password2]" id="password2"/></td>
        <tr>
            <td colspan="2">
                <div>
                    <p><input type="button" value="　更新　" id="update_btn" style="width:200px;" /></p>
                    <p><input type="button" value="　戻る　" id="logout_btn" style="width:200px;" /></p>
                </div>
            </td>
        </tr>
    </table>

    <div style="border: 1px dotted black; width:400px;margin: 5px;">
        <p style="text-align:left;">【更新パスワード入力ルール】</p>
        <ol style="list-style-type: decimal !important;margin-left:25px;">
            <li style="list-style-type: decimal !important; text-align:left;">半角数字、半角英小文字、_（アンダーバー）、-（ハイフン）が使用出来ます。ただし<span class="ColumnRed">半角数字の0（ゼロ）、1（イチ）および  半角英小文字のo（オー）、l（エル）</span>は使用禁止です。</li>
            <li style="list-style-type: decimal !important; text-align:left;">パスワードは8文字とし、半角数字と半角英小文字が  それぞれ１文字以上含まれる値を設定してください。</li>
            <li style="list-style-type: decimal !important; text-align:left;">現在のパスワードとは異なる値を設定してください。</li>
        </ol>
    </div>
  </div>
</form>
