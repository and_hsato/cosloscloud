<script type="text/javascript">
$(document).ready(function() {
    // お知らせ本文の非表示
    $(".news_body").hide();
    
    // お知らせクリック
    $(".infomation").click(function(){
        $(this).next().slideToggle("slow");
    });

    $("#guide-close").click(function(){
        $("#s-guide").hide(500);
    });
  
});
</script>
<?php echo $this->session->flash(); ?>

<!--ここから-->
<aside id="s-guide">
<a href="<?php echo $this->webroot; ?>startguide.html" class="s-guide-link" target="guide">
<img src="img/s_guideicon_00.png" width="22px"/>
<p><span>スタートガイド</span>初めてコスロスクラウドをご利用される方へ</p>
</a>
<a id="guide-close"><img src="img/s_guideicon_close.png" width="22px" class="s-close" /></a>

</aside >
<form name="base_form" >
<input name="base_url" type="hidden" value="<?php echo $this->webroot; ?>">
<input name="catalog_url" type="hidden" value="<?php echo Configure::read('catalog.url'); ?>">
<input name="login" type="hidden" value="<?php echo $this->Session->read('Auth.MstUser.login_id'); ?>">
<input name="pass" type="hidden" value="<?php echo $this->Session->read('Auth.MstUser.password')?>">
</form>
<!--ここまで-->
<div class="MenuBox">
  <h2>メニュー</h2>

<div id="every">  
  <div class="order">
    <div class="order_image"></div>
    <div class="order_menu">
    <h3>発注</h3>
    <ul>
      <li><a href="<?php echo Configure::read('catalog.url'); ?>session.mvc/login?uiand=<?php echo $this->Session->read('Auth.MstUser.login_id'); ?>&paand=<?php echo $this->Session->read('Auth.MstUser.password')?>" target="_blank">部署請求【<?php echo $CntOrder; ?>】</a></li>
      <li><a href="<?php echo $this->webroot; ?>Orders/order_decision">発注確定</a></li>
      <li><a href="<?php echo $this->webroot; ?>Orders/order_history">発注履歴</a></li>
    </ul>
  </div>
  </div>
  
  <div class="receiving">
    <div class="receiving_image"></div>
    <div class="receiving_menu">
      <h3>入荷処理</h3>
      <ul>
        <li><a href="<?php echo $this->webroot; ?>Receipts/index">入荷処理【<?php echo $CntReceiving; ?>】</a></li>
        <li><a href="<?php echo $this->webroot; ?>ReceiptsHistory/index">入荷履歴</a></li>
      </ul>
    </div>
  </div><!--#every-->
  
  <div class="master">
    <div class="master_image"></div>
    <div class="master_menu">
      <h3>Myマスタ</h3>
      <ul>
        <li><a href="<?php echo $this->webroot; ?>MstFacilityItems/edit_search">Myマスタ一覧【<?php echo $this->Session->read('Cnt.Master'); ?>】</a></li>
        <li><a href="<?php echo $this->webroot; ?>MstFacilityItems/search">Myマスタ新規登録</a></li>
      </ul>
    </div>
  </div>
 </div>
  
  
  <div class="setting">
    <div class="setting_image"></div>
    <div class="setting_menu">
      <h3>設定</h3>
      <ul>
        <li><a href="<?php echo $this->webroot; ?>MstDepartments/departments_list">部署設定【<?php echo $CntDepartment; ?>】</a></li>
        <li><a href="<?php echo $this->webroot; ?>MstUsers/userlist">利用者登録【<?php echo  $CntUser; ?>】</a></li>
        <li><a href="<?php echo $this->webroot; ?>MstFacilities/facilities_list">仕入先一覧【<?php echo $CntSupplier; ?>】</a></li>
      </ul>
    </div>
  </div>
  <div class="contact">
    <div class="contact_image"></div>
    <div class="contact_menu">
      <h3>マスタコンシェルジュデスク</h3>
      <ul>
        <li><a href="<?php echo $this->webroot; ?>Concierge">お問い合わせ一覧</a></li>
        <li><a href="<?php echo $this->webroot; ?>Concierge/add">新規お問い合わせ</a></li>
      </ul>
    </div>
  </div>
  
</div>
<hr class="Clear">
<div class="MasterBox" >
  <h2>My マスタ利用状況</h2>
  <p>プラン : <?php echo $master['plan_name']?></p>
  <p>Myマスタ登録件数: <?php echo $this->Session->read('Cnt.Master'); ?> / <?php echo $master['max_count']; ?> 件 </p>

  <p><?php echo date('Y'); ?>年<?php echo date('n'); ?>月の請求金額 : <?php echo $this->Common->toCommaStr($master['claim_price']) ?> 円</p>

</div>

<div class="OrderBox">
  <h2>今月の発注状況</h2>
  <p>発注数量:<?php echo $Order['Order']['count']; ?></p>
  <p>合計金額:<?php echo $this->Common->toCommaStr($Order['Order']['price']); ?> 円</p>
  
</div>

<div class="InfoBox">
   <h2>おしらせ</h2>
   <?php foreach($info as $i){ ?>
   <p><?php echo $i['TrnInformation']['start_date']; ?> <?php echo $i['TrnInformation']['title']?></p>
   <?php } ?>
</div>

<p class="clear"></p>
