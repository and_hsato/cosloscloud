<script>

$(document).ready(function(){
    $("#password_btn").click(function(){
        $("#LoginForm").attr('action' , '<?php echo $this->webroot; ?>login/loginPasswordChange/');
        $("#LoginForm").attr('method', 'post');
        $("#LoginForm").submit();
    });
    $("#login_btn").click(function(){
        $("#LoginForm").attr('action' , '<?php echo $this->webroot; ?>login/login/');
        $("#LoginForm").attr('method', 'post');
        $("#LoginForm").submit();
    });
    $("#logout_btn").click(function(){
        $("#LoginForm").attr('action' , '<?php echo $this->webroot; ?>login/logout/');
        $("#LoginForm").attr('method', 'post');
        $("#LoginForm").submit();
    });
});

</script>
<form method="post" action="<?php echo $this->webroot; ?>login/login" accept-charset="utf-8" id="LoginForm">
  <div style="margin-top:100px;text-align:center;">
  <pre class="ColumnRed">
現在お使いのパスワードの有効期限は　<?php echo $effective_date ; ?>　です。
お早めにパスワードの更新をお願いいたします。
  </pre>
  <p><input type="button" value="　パスワード更新画面へ　" id="password_btn" style="width:200px;"/></p>
  <p><input type="button" value="　続行　" id="login_btn" style="width:200px;"/></p>
  <p><input type="button" value="　戻る　" id="logout_btn" style="width:200px;"/></p>
  <div>
</form>

