<script type="text/javascript">
$(document).ready(function() {
    // お知らせ本文の非表示
    $(".news_body").hide();
    
    // お知らせクリック
    $(".infomation").click(function(){
        $(this).next().slideToggle("slow");
    });

    $("#guide-close").click(function(){
        $("#s-guide").hide(500);
    });
  
});
</script>
<?php echo $this->session->flash(); ?>

<!--ここから-->
<aside id="s-guide">
<a href="<?php echo $this->webroot; ?>startguide.html" class="s-guide-link" target="guide">
<img src="img/s_guideicon_00.png" width="22px"/>
<p><span>スタートガイド</span>初めてコスロスクラウドをご利用される方へ</p>
</a>
<a id="guide-close"><img src="img/s_guideicon_close.png" width="22px" class="s-close" /></a>

</aside >
<div class="InfoBox">
   <h2>おしらせ</h2>
   <?php foreach($info as $i){ ?>
   <p><?php echo $i['TrnInformation']['start_date']; ?> <?php echo $i['TrnInformation']['title']?></p>
   <p><?php echo $i['TrnInformation']['message']; ?></p>
   <?php } ?>
</div>
<div class="RfpBox">
   <h2>見積依頼</h2>
 <?php if (empty($data)) { ?>
   <p>見積依頼はありません。</p>
 <?php } else { ?>
 <table class="TableStyle01 table-odd">
   <tr>
     <th width="20%">回答期限</th>
     <th width="20%">登録日時</th>
     <th width="40%">見積依頼番号</th>
     <th width="20%">見積依頼書</th>
   </tr>
   <?php foreach ($data as $d) { $rfpHeader = $d['TrnRfpHeader']; ?>
   <tr>
     <td><?php echo date('Y/m/d', strtotime($rfpHeader['limit_date'])) ?></td>
     <td><?php echo date('Y/m/d', strtotime($rfpHeader['work_date'])); ?></td>
     <td><?php echo $rfpHeader['work_no'] ?></td>
     <td><?php echo $this->Html->link('ダウンロード', $rfpHeader['report_url']) ?></td>
   </tr>
   <?php } ?>
 <?php } ?>
 </table>
</div>
<p class="clear"></p>
