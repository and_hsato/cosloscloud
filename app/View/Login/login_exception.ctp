<script>
$(document).ready(function(){
    $("#password_btn").click(function(){
        $("#LoginForm").attr('action' , '<?php echo $this->webroot; ?>login/loginPasswordChange/');
        $("#LoginForm").attr('method', 'post');
        $("#LoginForm").submit();
    });
    $("#logout_btn").click(function(){
        $("#LoginForm").attr('action' , '<?php echo $this->webroot; ?>login/logout/');
        $("#LoginForm").attr('method', 'post');
        $("#LoginForm").submit();
    });
});
</script>
<form method="post" action="<?php echo $this->webroot; ?>login/login" accept-charset="utf-8" id="LoginForm">
<hr class="Clear" />
<table class="LoginArea">
  <tr>
    <td align="center">
    <p class="ColumnRed">パスワードの更新が必要です。</p>
    <p><input type="button" value="　パスワード更新画面へ　" id="password_btn" style="width:200px;" /></p>
    <p><input type="button" value="　戻る　" id="logout_btn" style="width:200px;" /></p>
    </div>
    </div>
    </td>
  </tr>
</table>
</form>
