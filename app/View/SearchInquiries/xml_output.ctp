<printdata>
    <setting>
        <layoutname><?php e($layout_name); ?></layoutname>
        <fixvalues>
<?php
  foreach($fix_value as $name => $data) {
    e('<value name="'.$name.'">'.$data.'</value>');
  }
?>
        </fixvalues>
    </setting>
    <datatable>

<?php
  e('<columns>'.$columns.'</columns>');
?>
<?php
  e('<rows>');
  foreach($posted as $data) {
    $tabed = implode("\t", $data);
    e('<row>'.h($tabed).'</row>');
  }
  e('</rows>');
?>
    </datatable>
</printdata>
