<script type="text/javascript">
$(function(){
    //検索ボタン押下時
    $("#btn_search").click(function(){
        if(document.getElementById("ExpirationType2").checked){
            if($("#expiration_after").val() == "" || $("#expiration_after").val() == 0){
                alert("日数を指定してください");
                return;
            }
            if($("#expiration_after").val().match(/[^0-9]/g)){
                alert("日数は数値で指定してください");
                return;
            }
        }

        if(document.getElementById("ExpirationType3").checked){
            if($("#datepicker1").val() == ""){
                alert("日付を指定してください");
                return;
            }
        }

        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/expiration_list').submit();
    });

    //印刷ボタン押下時
    $("#btn_print").click(function(){
        if(document.getElementById("ExpirationType2").checked){
            if($("#expiration_after").val() == "" || $("#expiration_after").val() == 0){
                alert("日数を指定してください");
                return;
            }
            if($("#expiration_after").val().match(/[^0-9]/g)){
                alert("日数は数値で指定してください");
                return;
            }
        }

        if(document.getElementById("ExpirationType3").checked){
            if($("#datepicker1").val() == ""){
                alert("日付を指定してください");
                return;
            }
        }

        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report').submit();
    });

    //CSVボタン押下時
    $("#btn_csv").click(function(){
        if(document.getElementById("ExpirationType2").checked){
            if($("#expiration_after").val() == "" || $("#expiration_after").val() == 0){
                alert("日数を指定してください");
                return;
            }
            if($("#expiration_after").val().match(/[^0-9]/g)){
                alert("日数は数値で指定してください");
                return;
            }
        }

        if(document.getElementById("ExpirationType3").checked){
            if($("#datepicker1").val() == ""){
                alert("日付を指定してください");
                return;
            }
        }

        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/output_expiration_list_csv').submit();
    });
});

// 日付妥当性チェック
function validate2date(date_data){
    var date = date_data.val();
    var date_split;
    var di;
    if(date.match(/^[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}$/)){
        date_split = date.split("/");
        di = new Date(date_split[0],date_split[1]-1,date_split[2]);
        if(di.getFullYear() == date_split[0] && di.getMonth() == date_split[1]-1 && di.getDate() == date_split[2]){
            return true;
        } else {
            return false;
        }
    }
    return true;
}
</script>


<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>期限切れ一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>期限切れ一覧</span></h2>
<h2 class="HeaddingMid">条件を指定して検索してください。</h2>
<form class="validate_form search_form" id="search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/expiration_list" method="post">
    <?php echo $this->form->input('isSearch',array('type'=>'hidden','value'=>'1','class'=>'search_form')); ?>

    <?php echo $this->form->input('print_type',array('type'=>'hidden','id' =>'print_type','value'=>'expiration'));?>
    <?php echo $this->form->input('facility_name',array('type'=>'hidden','id' =>'facility_name'));?>
    <?php echo $this->form->input('department_name',array('type'=>'hidden','id' =>'department_name'));?>
    <?php echo $this->form->input('facilityId',array('type'=>'hidden','value' => $this->Session->read('Auth.facility_id_selected')));?>
    <?php echo $this->form->input('userId',array('type'=>'hidden','value' => $this->Session->read('Auth.MstUser.id') ));?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
  
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('facilityText',array('id' => 'facilityText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                    <?php echo $this->form->input('facilityCode',array('options'=>$facility_list ,'empty'=>true,'id' => 'facilityCode', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('facilityName',array('type'=>'hidden' ,'id' => 'facilityName')); ?>
                </td>
                <td></td>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('departmentText',array('id' => 'departmentText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                    <?php echo $this->form->input('departmentCode',array('options'=>$department_list ,'empty'=>true,'id' => 'departmentCode', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('departmentName',array('type'=>'hidden' ,'id' => 'departmentName')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('internal_code',array('class'=>'txt search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('item_code',array('class'=>'txt search_upper')); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->input('lot_no',array('class'=>'txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('item_name',array('type'=>'text','class'=>'txt search_canna','size'=>10)); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('dealer_name',array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>管理区分</th>
                <td> <?php echo $this->form->input('trade_type',array('type'=>'select','options'=>$teisu,'empty'=>true,'class'=>'txt','style'=>'width:150px;')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('standard',array('type'=>'text','class'=>'txt search_canna','size'=>10)); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->input('sticker_no',array('type'=>'text','class'=>'txt','size'=>10)); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php $expiration_type = isset($this->request->data['expiration_type']) ? $this->request->data['expiration_type'] : '1'; ?>
                    <?php echo $this->form->radio('expiration_type',array(1 => '期限切れ警告期間のもの'),array('value' => $expiration_type,'label' => '','legend'=>'' )); ?>
                </td>
                <td></td>
                <td colspan="2">
                    <?php echo $this->form->radio('expiration_type',array(2 => '指定日数の間に切れるもの'),array('value' => $expiration_type,'label' => '','legend'=>'' )); ?>
                </td>
                <td></td>
                <td colspan="2">
                    <?php echo $this->form->radio('expiration_type',array(3 => '指定日までに切れるもの'),array('value' => $expiration_type,'label' => '','legend'=>'' )); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><?php echo $this->form->input('expiration_after',array('class'=>'txt')); ?>日後</td>
                <td></td>
                <td></td>
                <td><?php echo $this->form->input('expiration_date',array('maxlength'=>50,'type'=>'text','class'=>'validate[optional,custom[date],funcCall2[date2]] date','id'=>'datepicker1')); ?>まで</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn1" id="btn_search" />
            <input type="button" class="btn btn10" id="btn_print" />
            <input type="button" class="btn btn5" id="btn_csv" />
        </p>
    </div>
    <div id="hideer">
        <h2 class="HeaddingSmall">期限切れ警告一覧</h2>
        <div class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($Result))); ?>
        </div>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th style="width:20px;" rowspan="2"></th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col15">センターシール</th>
                    <th class="col10">ロット番号</th>
                    <th class="col5">管理区分</th>
                    <th class="col20">配置場所</th>
                </tr>
                <tr>
                    <th>棚番</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>部署シール</th>
                    <th>有効期限</th>
                    <th></th>
                    <th>仕入先</th>
                </tr>

                <?php foreach($Result as $_index => $row) { ?>
                <tr>
                    <td rowspan="2"></td>
                    <td><?php echo h_out($row[0]['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($row[0]['item_name']); ?></td>
                    <td><?php echo h_out($row[0]['item_code']); ?></td>
                    <td><?php echo h_out($row[0]['item_unit_name'],'right'); ?></td>
                    <td><?php echo h_out($row[0]['facility_sticker_no']); ?></td>
                    <td><?php echo h_out($row[0]['lot_no']); ?></td>
                    <td><?php echo h_out($row[0]['trade_type_name'],'center'); ?></td>
                    <td><?php echo h_out($row[0]['facility_name'] .' / '. $row[0]['dept_name1']); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($row[0]['code'] , 'center') ?></td>
                    <td><?php echo h_out($row[0]['standard']); ?></td>
                    <td><?php echo h_out($row[0]['dealer_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($row[0]['stocking_price']),'right'); ?></td>
                    <td><?php echo h_out($row[0]['hospital_sticker_no']); ?></td>
                    <td><?php echo h_out($row[0]['validated_date'],'center'); ?></td>
                    <td></td>
                    <td><?php echo h_out($row[0]['supplier_name']); ?></td>
                </tr>
                <?php } ?>
                <?php if(isset($data["isSearch"]) && count($Result) == 0){ ?>
                <tr><td colspan="10" class="center">該当データがありません</td></tr>
                <?php } ?>
            </table>
        </div>
    </div>
</form>
