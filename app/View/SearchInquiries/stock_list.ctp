<script type="text/javascript">
$(function(){
    //検索ボタン押下時
    $("#btn_Search").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/stock_list#search_result').submit();
    });
    $('#FreeKeyWord').focus();
});
</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>在庫検索</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>在庫検索</span></h2>
<h2 class="HeaddingMid">在庫を検索してください。</h2>

<form class="validate_form search_form" id="search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/stock_list" method="post">
    <?php echo $this->form->input('isSearch',array('type'=>'hidden','value'=>'1','class'=>'search_form')); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <input type="hidden" name="data['print_type']" value="stock">

    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('departmentText',array('id' => 'departmentText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                    <?php echo $this->form->input('departmentCode',array('options'=>$department_list ,'empty'=>true,'id' => 'departmentCode', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('departmentName',array('type'=>'hidden' ,'id' => 'departmentName')); ?>
                </td>
                <td width="20"></td>
                <th>商品ID</th>
                <td><?php echo $this->form->text('internal_code',array('class'=>'txt search_upper search_internal_code',"style"=>"width:150px; ime-mode:disabled;")); ?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('item_code',array('class'=>'txt',"style"=>"width:150px;")); ?></td>
                <td width="20"></td>
                <td><?php echo $this->form->input('all_zero',array('type'=>'checkbox','hiddenField'=>false)); ?>在庫なしを除く</td>
            </tr>
            <tr>
                <th></th>
                <td>
                </td>
                <td></td>
                <th>商品名</th>
                <td><?php echo $this->form->text('item_name',array('class'=>'txt search_canna',"style"=>"width:150px;")); ?></td>
                <td></td>
                <th>規格</th>
                <td><?php echo $this->form->text('standard',array('class'=>'txt search_canna',"style"=>"width:150px;",'size'=>10)); ?></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>フリー検索</th>
                <td colspan="10"><?php echo $this->form->input('Free', array('type' => 'text', 'id'=>'FreeKeyWord' ,'class' => 'txt', 'style' => 'width:100%;')); ?></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="common-button" id="btn_Search" value="検索"/>
        </p>
    </div>
    <div class="results">
        <a name="search_result"></a>
        <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($Result)));?>
        </div>

    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <colgroup>
                <col style="width:20px;"/>
                <col class="col10" />
                <col />
                <col />
                <col class="col5" />
                <col class="col5" />
            </colgroup>
            <tr>
                <th rowspan="2"></th>
                <th>商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th>包装単位</th>
                <th>定数</th>
            </tr>
            <tr>
                <th>部署</th>
                <th>規格</th>
                <th>販売元</th>
                <th>発注済</th>
                <th>在庫数</th>
            </tr>

            <?php if(isset($data["isSearch"]) && count($Result) == 0){ ?>
            <tr><td class="center" colspan="6">該当データがありません</td></tr>
            <?php } ?>
            <?php foreach( $Result as $index => $row ){ ?>
            <tr>
                <td rowspan="2"></td>
                <td><?php echo h_out($row[0]['internal_code'],'center'); ?></td>
                <td><?php echo h_out($row[0]['item_name']); ?></td>
                <td><?php echo h_out($row[0]['item_code']); ?></td>
                <td><?php echo h_out($row[0]['unit_name'],'right'); ?></td>
                <td><?php echo h_out($row[0]['fixed_count'],'right'); ?></td>
            </tr>
            <tr>
                <td><?php echo h_out($row[0]['department_name'],'center'); ?></td>
                <td><?php echo h_out($row[0]['standard']); ?></td>
                <td><?php echo h_out($row[0]['dealer_name']); ?></td>
                <td><?php echo h_out( number_format($row[0]['requested_count']),'right'); ?></td>
                <td><?php echo h_out( number_format($row[0]['stock_count']),'right'); ?></td>
            </tr>
            <?php } ?>
        </table>
      </div>
  </div>
  </form>
</div>