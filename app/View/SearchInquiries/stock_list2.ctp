<script type="text/javascript">
$(function(){
    //検索ボタン押下時
    $("#btn_search").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/stock_list2').submit();
    });

    //印刷ボタン押下時
    $("#btn_print").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report').submit();
    });

    //CSVボタン押下時
    $("#btn_csv").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/output_csv2').submit();
    });
    $('#FreeKeyWord').focus();
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>在庫検索（病院）</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>在庫検索</span></h2>
<h2 class="HeaddingMid">在庫を検索してください。</h2>

<form class="validate_form search_form" id="search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/stock_list2" method="post">
    <?php echo $this->form->input('isSearch',array('type'=>'hidden','value'=>'1','class'=>'search_form')); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <input type="hidden" name="data['print_type']" value="stock">

    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th >施主</th>
                <td >
                    <?php echo $this->form->input('ownerText',array('id' => 'ownerText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                    <?php echo $this->form->input('ownerCode',array('options'=>$owners ,'empty'=>true,'id' => 'ownerCode', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('ownerName',array('type'=>'hidden' ,'id' => 'ownerName')); ?>
                </td>
                <td width="20"></td>
                <th>商品ID</th>
                <td><?php echo $this->form->text('internal_code',array('class'=>'txt search_upper search_internal_code',"style"=>"width:150px; ime-mode:disabled;")); ?></td>
                <td width="20"></td>
                <th>定数区分</th>
                <td><?php echo $this->form->input('trade_type',array('type'=>'select' , 'options'=>$teisu,'empty'=>true,'class'=>'txt','style'=>'width:150px;')); ?></td>
                <td width="20"></td>
                <td><?php echo $this->form->input('all_zero',array('type'=>'checkbox','hiddenField'=>false)); ?>在庫なしを除く</td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('facilityText',array('id' => 'facilityText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                    <?php echo $this->form->input('facilityCode',array('options'=>$facility_list ,'empty'=>true,'id' => 'facilityCode', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('facilityName',array('type'=>'hidden' ,'id' => 'facilityName')); ?>
                </td>
                <td></td>
                <th>商品名</th>
                <td><?php echo $this->form->text('item_name',array('class'=>'txt search_canna',"style"=>"width:150px;")); ?></td>
                <td></td>
                <th>規格</th>
                <td><?php echo $this->form->text('standard',array('class'=>'txt search_canna',"style"=>"width:150px;",'size'=>10)); ?></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('departmentText',array('id' => 'departmentText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                    <?php echo $this->form->input('departmentCode',array('options'=>$department_list ,'empty'=>true,'id' => 'departmentCode', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('departmentName',array('type'=>'hidden' ,'id' => 'departmentName')); ?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('item_code',array('class'=>'txt',"style"=>"width:150px;")); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('dealer_name',array('class'=>'txt search_canna',"style"=>"width:150px;")); ?></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th><?php echo $this->Session->read('Auth.Config.SpareKey1Title'); ?></th>
                <td><?php echo $this->form->input('spare_key1', array('type' => 'text', 'class' => 'txt')); ?></td>
                <td></td>
                <th><?php echo $this->Session->read('Auth.Config.SpareKey2Title'); ?></th>
                <td><?php echo $this->form->input('spare_key2', array('type' => 'text', 'class' => 'txt')); ?></td>
                <td></td>
                <th><?php echo $this->Session->read('Auth.Config.SpareKey3Title'); ?></th>
                <td><?php echo $this->form->input('spare_key3', array('type' => 'text', 'class' => 'txt')); ?></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>フリー検索</th>
                <td colspan="10"><?php echo $this->form->input('Free', array('type' => 'text', 'id'=>'FreeKeyWord' ,'class' => 'txt', 'style' => 'width:100%;')); ?></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn1" id="btn_search" />
            <input type="button" class="btn btn5" id="btn_csv" />
        </p>
    </div>
    <div id="results">
    <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($Result)));?>
    </div>

    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th style="width:20px;" rowspan="2"></th>
                <th class="col20">施設</th>
                <th class="col10">商品ID</th>
                <th class="col20">商品名</th>
                <th class="col20">製品番号</th>
                <th class="col5">包装単位</th>
                <th class="col5">定数区分</th>
                <th class="col5">発注点</th>
                <th class="col5">定数</th>
                <th class="col5">休日</th>
                <th class="col5">予備</th>
            </tr>
            <tr>
                <th>部署</th>
                <th>棚番</th>
                <th>規格</th>
                <th>販売元</th>
                <th>入荷予定</th>
                <th>定数在庫</th>
                <th>臨時在庫</th>
                <th>ｼｰﾙ無</th>
                <th>在庫数</th>
                <th>引当可</th>
            </tr>
<?php
  if(isset($data["isSearch"])){
    if(count($Result) == 0){
?>
            <tr><td class="center" colspan="10">該当データがありません</td></tr>
<?php
    }else{
      foreach( $Result as $index => $row ): ?>
            <tr>
                <td rowspan="2"></td>
                <td><?php echo h_out($row[0]['facility_name']); ?></td>
                <td><?php echo h_out($row[0]['internal_code'],'center'); ?></td>
                <td><?php echo h_out($row[0]['item_name']); ?></td>
                <td><?php echo h_out($row[0]['item_code']); ?></td>
                <td><?php echo h_out($row[0]['unit_name'],'right'); ?></td>
                <td><?php echo h_out($row[0]['fixed_type_name'],'center'); ?></td>
                <td><?php echo h_out( number_format($row[0]['order_point']) , 'right'); ?></td>
                <td><?php echo h_out( number_format($row[0]['fixed_count']) , 'right'); ?></td>
                <td><?php echo h_out( number_format($row[0]['holiday_fixed_count']) , 'right'); ?></td>
                <td><?php echo h_out( number_format($row[0]['spare_fixed_count']),'right'); ?></td>
            </tr>
            <tr>
                <td><?php echo h_out($row[0]['department_name']); ?></td>
                <td><?php echo h_out($row[0]['shelf_name']); ?></td>
                <td><?php echo h_out($row[0]['standard']); ?></td>
                <td><?php echo h_out($row[0]['dealer_name']); ?></td>
                <td><?php echo h_out( number_format($row[0]['requested_count']), 'right'); ?></td>
                <td><?php echo h_out( number_format($row[0]['fixed_stock_count']), 'right'); ?></td>
                <td><?php echo h_out( number_format($row[0]['ex_stock_count']),'right'); ?></td>
                <td><?php echo h_out( number_format($row[0]['no_sticker_count']),'right'); ?></td>
                <td><?php echo h_out( number_format($row[0]['stock_count']),'right'); ?></td>
                <td><?php echo h_out( number_format($row[0]['enable_promise_count']),'right'); ?></td>
            </tr>
<?php
      endforeach;
    }
  }
?>
        </table>
      </div>
  </div>
</form>
