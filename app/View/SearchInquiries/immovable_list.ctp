<script type="text/javascript">
$(function(){
    //検索ボタン押下時
    $("#btn_search").click(function(){
        if($("#immovable_date").val() == ""){
            $("#immovable_date").val("0");
        }else{
            if( $("#immovable_date").val().match(/[\D]/g) ){
                alert("正の整数を入力してください");
                return false;
            }
        }
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/immovable_list').submit();
    });

    //印刷ボタン押下時
    $("#btn_print").click(function(){
        if($("#immovable_date").val() == ""){
            $("#immovable_date").val("0");
        }else{
            if( $("#immovable_date").val().match(/[\D]/g) ){
                alert("正の整数を入力してください");
                return false;
            }
        }
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report').submit();
    });

    //CSVボタン押下時
    $("#btn_csv").click(function(){
        if($("#immovable_date").val() == ""){
            $("#immovable_date").val("0");
        }else{
            if( $("#immovable_date").val().match(/[\D]/g) ){
                alert("正の整数を入力してください");
                return false;
            }
        }
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/output_immovable_csv').submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>不動在庫一覧</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>不動在庫一覧</span></h2>
<form class="validate_form search_form" id="search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/immovable_list" method="post">

    <?php echo $this->form->input('isSearch',array('type'=>'hidden','value'=>'1','class'=>'search_form')); ?>
    <?php echo $this->form->input('print_type',array('type'=>'hidden','id' =>'print_type',"value"=>"immovable"));?>
    <?php echo $this->form->input('facility_name',array('type'=>'hidden','id' =>'facility_name'));?>
    <?php echo $this->form->input('department_name',array('type'=>'hidden','id' =>'department_name'));?>
    <?php echo $this->form->input('facilityId',array('type'=>'hidden','value' => $this->Session->read('Auth.facility_id_selected')));?>
    <?php echo $this->form->input('userId',array('type'=>'hidden','value' => $this->Session->read('Auth.MstUser.id') ));?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('facilityText',array('id' => 'facilityText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                    <?php echo $this->form->input('facilityCode',array('options'=>$facility_list ,'empty'=>true,'id' => 'facilityCode', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('facilityName',array('type'=>'hidden' ,'id' => 'facilityName')); ?>
                </td>
                <td></td>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('departmentText',array('id' => 'departmentText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                    <?php echo $this->form->input('departmentCode',array('options'=>$department_list ,'empty'=>true,'id' => 'departmentCode', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('departmentName',array('type'=>'hidden' ,'id' => 'departmentName')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('internal_code',array('class'=>'txt search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('item_code',array('class'=>'txt search_upper')); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->input('lot_no',array('class'=>'txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('item_name',array('type'=>'text','class'=>'txt search_canna','size'=>10)); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('dealer_name',array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>管理区分</th>
                <td> <?php echo $this->form->input('trade_type',array('type'=>'select','options'=>$teisu,'empty'=>true,'class'=>'txt','style'=>'width:150px;')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('standard',array('type'=>'text','class'=>'txt search_canna','size'=>10)); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->input('sticker_no',array('type'=>'text','class'=>'txt','size'=>10)); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>不動日数</th>
                <td><?php echo $this->form->input('immovable_date',array('class'=>'txt num','style'=>'width:60px')); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn1" id="btn_search" />
            <input type="button" class="btn btn10" id="btn_print" />
            <input type="button" class="btn btn5" id="btn_csv" />
        </p>
    </div>

    <div id="results">
        <div class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($Result))); ?>
        </div>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th style="width:10px;" rowspan="2"></th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col15">センターシール</th>
                    <th class="col10">ロット番号</th>
                    <th class="col5">管理区分</th>
                    <th class="col5">定数</th>
                    <th class="col15">配置場所</th>
                </tr>
                <tr>
                    <th>棚番</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>部署シール</th>
                    <th>有効期限</th>
                    <th></th>
                    <th>不動日数</th>
                    <th>仕入先</th>
                </tr>
<?php foreach($Result as $_index => $row){ ?>
                <tr>
                    <td rowspan="2"></td>
                    <td><?php echo h_out($row[0]['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($row[0]['item_name']); ?></td>
                    <td><?php echo h_out($row[0]['item_code']); ?></td>
                    <td><?php echo h_out($row[0]['item_unit_name'],'right'); ?></td>
                    <td><?php echo h_out($row[0]['facility_sticker_no']); ?></td>
                    <td><?php echo h_out($row[0]['lot_no']); ?></td>
                    <td><?php echo h_out($row[0]['trade_type_name'],'center'); ?></td>
                    <td><?php echo h_out($row[0]['fixed_count'],'right'); ?></td>
                    <td><?php echo h_out($row[0]['facility_name'] . '／' . $row[0]['dept_name1']); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($row[0]['code']); ?></td>
                    <td><?php echo h_out($row[0]['standard']); ?></td>
                    <td><?php echo h_out($row[0]['dealer_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($row[0]['stocking_price']),'right'); ?></td>
                    <td><?php echo h_out($row[0]['hospital_sticker_no']); ?></td>
                    <td><?php echo h_out($row[0]['validated_date'],'center'); ?></td>
                    <td></td>
                    <td><?php echo h_out($row[0]['immovable'],'right'); ?></td>
                    <td><?php echo h_out($row[0]['supplier_name']); ?></td>
                </tr>
         <?php } ?>
            <?php if(isset($data["isSearch"] ) && count($Result) == 0){ ?>
                <tr><td colspan="10" class="center" >該当データがありません</td></tr>
             <?php } ?>
            </table>
        </div>
    </div>
</form>
