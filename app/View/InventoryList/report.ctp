<printdata>
    <setting>
        <layoutname>16_在庫表.rpx</layoutname>
        <fixvalues>
            <value name="printer"><?php echo Configure::read('PrinterType.center'); ?></value>
        </fixvalues>
    </setting>
    <datatable>
        <columns><?php echo join("\t", $columns); ?></columns>
        <rows>
            <?php foreach($records as $_row): ?>
            <row><?php echo join("\t", array_values($_row)); ?></row>
            <?php endforeach; ?>
        </rows>
    </datatable>
</printdata>