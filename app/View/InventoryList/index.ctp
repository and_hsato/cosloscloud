<script type="text/javascript">
    $(document).ready(function(){
        $('#donorInput').blur(function(){
            if($('#donorSelect').find('option[value=' + $(this).val() + ']').length === 0){
                $('#donorSelect').val('');
            }else{
                $('#donorSelect').val($(this).val());
            }
        });
        $('#facilityInput').blur(function(){
            if($('#facilitySelect').find('option[value=' + $(this).val() + ']').length === 0){
                $('#facilitySelect').val('');
            }else{
                $('#facilitySelect').val($(this).val());
            }
        });
        $('#donorSelect').change(function(){
            $('#donorInput').val($(this).val());
        });
        $('#facilitySelect').change(function(){
            $('#facilityInput').val($(this).val());
        });

        $('#printInventory').click(function(){
            if($('#facilityInput').val() == '' && $('#donorInput').val() == '' && $('#subcode').val() == ''){
                alert('施主、施設、サブコードのいずれかを指定してください');
                return false;
            }

            if(!confirm('在庫表を印刷します。よろしいですか？')){
                return false;
            }else{
                $('#inventoryForm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report').submit();
            }
        });

        $('#exportInventory').click(function(){
            if($('#facilityInput').val() == '' && $('#donorInput').val() == '' && $('#subcode').val() == ''){
                alert('施主、施設、サブコードのいずれかを指定してください');
                return false;
            }
            if(!confirm('在庫受払CSVを出力します。よろしいですか？')){
                return false;
            }else{
                $('#inventoryForm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/exportCsv').submit();
            }
        });

    });
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>在庫表印刷</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>在庫表印刷</span></h2>
<h2 class="HeaddingMid">印刷を行う施設の条件を指定してください</h2>
<form id="inventoryForm" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/report" method="POST">
<?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyle01">
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->input('Inventory.donor_input', array('class'=>'txt', 'id'=>'donorInput', 'style'=>'width:60px')); ?>
                    <?php echo $this->form->input('Inventory.donor', array('options'=>$donor, 'empty'=>true,'class'=>'txt','id'=>'donorSelect')); ?>
                </td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('Inventory.facility_input', array('class'=>'txt', 'id'=>'facilityInput', 'style'=>'width:60px')); ?>
                    <?php echo $this->form->input('Inventory.facility', array('options'=>$facilities, 'empty'=>true, 'class'=>'txt','id'=>'facilitySelect',)); ?>
                    <input type="hidden" name="data[Inventory][f]" value="<?php echo $this->Session->read('Auth.facility_id_selected'); ?>">
                </td>
            </tr>
            <tr>
                <th>サブコード</th>
                <td>
                    <?php echo $this->form->input('Inventory.subcode', array('class'=>'txt', 'id'=>'subcode')); ?>
                </td>
            </tr>
            <tr>
                <th>対象月</th>
                <td>
                    <?php
                    $yymm = date('Y/m', strtotime('-1 month'));
                    list($yy,$mm) = explode("/", $yymm);
                    ?>
                    <?php echo $this->form->input('Inventory.year',array('options'=>$years, 'value'=>$yy, 'class'=>'txt r','style'=>'width:60px;','empty'=>false)); ?>年
                    <?php echo $this->form->input('Inventory.month',array('options'=>$months, 'value'=>$mm, 'class'=>'txt r','style'=>'width:40px;','empty'=>false)); ?>月度
                </td>
            </tr>
        </table>

        <div class="ButtonBox">
            <input type="button" class="btn btn38 print_btn" id="printInventory" />
            <input type="button" class="btn btn37 print_btn" id="exportInventory"  />
        </div>
    </div>
</form>
