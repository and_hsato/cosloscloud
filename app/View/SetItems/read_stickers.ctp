<script type="text/javascript">
/*
 * バーコードリスト、削除処理
 * @param {event} event
 * @return {bool}
 */

String.prototype.trim = function() {return this.replace(/^\s+|\s+$/g,"");}

function editTextArea(event){
    var KeyChar   = event.keyCode;
    var codeInput = $('#codeinput').val().trim().toUpperCase();

    if (KeyChar == 13){ //Enterキー押下
        var _hasDup = false;
        if (codeInput === '') {
            $('#err_msg').text('空文字は入力できません');
            $('#codeinput').val('');
        } else {
            //川鉄の場合重複チェックはしない
            if(!codeInput.match(/^01[0-9][0-9]{12}/)){
                $('#barcodelist > li').each(function () {
                    if( codeInput == this.innerText) {
                        $('#err_msg').text("番号が重複しています");
                        _hasDup = true;
                        $('#codeinput').val('');
                    }
                });
            }
            if (_hasDup === false) {
                //要素追加
                $('#barcodelist').append('<li>'+codeInput+'</li>');
                $('#barcodelist > li').click(function(){
                    $('#barcodelist > li').removeClass('selected');
                    $(this).addClass('selected');
                });
                //スクロール
                $('#barcodelist').scrollTop($('#barcodelist > li').length * 14);
                $('#err_msg').text('');
                $('#codeinput').val('');
                $('#codez').attr('value', $('#codez').val() + codeInput + ',');
                $('#barcode_count').text('読込件数：'+ $('#barcodelist > li').length +'件');
            }
        }
        $('#codeinput').focus();
        return false;
    }
}

$(document).ready(function(){
    // current codez.
    var _currentCodez = null;

    //初期フォーカスをバーコード入力欄にあわせる
    $('#codeinput').focus();

    //DELETEキー押下
    $(document).keyup(function(e){
        if(e.which === 46){ //DELETEキー
            obj = jQuery(document.activeElement);
            if(obj.attr('id') != 'codeinput'){ //バーコード入力上でDELが押された場合は無視
                $('#barcodelist li.selected').remove();
                $('#codez').val(''); //いったん全部消す
                $('#barcodelist li').each(function(){
                    $('#codez').val($('#codez').val() + this.innerText + ',');
                });
                $('#barcode_count').text('読込件数：'+ $('#barcodelist > li').length +'件');
            }
        }
        $('#codeinput').focus();
    });
  
    //確認ボタン押下
    $("#confirmBtn").click(function(){
        if ($('#barcodelist li').length > 0 && $('#codez').val() != '') {
            $("#read_form").attr('action', '<?php echo $this->webroot; ?>set_items/set_item_confirm');
            $("#read_form").attr('method','post');
            $("#read_form").submit();  
        } else {
             alert('バーコードが入力されていません');
             $('#codeinput').focus();
             return false;
        }
    });
    //クリアボタン押下
    $("#claerBtn").click(function(){
        $('#codeinput').val('');
        $('#err_msg').text('');
        $('#codeinput').focus();
    });

    //全クリアボタン押下
    $('#allClaerBtn').click(function(){
        $('#barcodelist > li').remove();
        $('#codeinput').val('');
        $('#codez').val('');
        $('#err_msg').text('');
        $('#barcode_count').text('読込件数：0件');
        $('#codeinput').focus();
    });
});
</script>

<style type="text/css">
#barcodelist{
  width: 512px;
  height:280px;
  overflow: auto;
  margin:5px 0px 5px;
  padding:5px;
  display: block;
  border: groove 1px #111111;
}
#barcodelist > li{
  width: 500px;
  cursor: pointer;
  display: block;
}
  
.selected{
  background:#0A246A;
  color:#ffffff;
}
</style>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_items">セット組作業</a></li>
        <li>セット組作業シール読込</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>セット組作業シール読込</span></h2>
<form class="validate_form" id="read_form" onsubmit="return false;">
    <input type="hidden" id="post_count" value="0" />
    <?php echo $this->form->hidden('codez', array('id'=>'codez',)); ?>
    <?php echo $this->form->input('MstSetItem.id',array('type'=>'hidden'));?>
    <?php echo $this->form->input('MstSetItem.item_unit_id',array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>セットID</th>
                <td>
                    <?php echo $this->form->input('MstSetItem.internal_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
                </td>
                <td width="20"></td>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstSetItem.standard',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
                </td>
            </tr>
            <tr>
                <th>セット名称</th>
                <td>
                    <?php echo $this->form->input('MstSetItem.item_name',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('MstSetItem.item_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
                </td>
            </tr>
        </table>
        <h2 class="HeaddingSmall">構成品一覧</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th width="20" rowspan="2"></th>
                    <th class="col20" >商品ID</th>
                    <th class="col30">商品名</th>
                    <th class="col30">製品番号</th>
                    <th class="col20">包装単位</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02 table-even2">
            <?php
              $i = 0;
              foreach($setDetails as $item){
              ?>
                <tr>
                    <td width="20" rowspan="2"><?php echo $this->form->input('MstSetDetails.id' , array('type'=> 'hidden' , 'value' => $item['MstSetDetails']['quantity'] , 'class' => 'hidden_id', 'id' => $item['MstSetDetails']['id'])) ?></td>
                    <td class="col20"><?php echo h_out($item['MstSetDetails']['internal_code']); ?></td>
                    <td class="col30"><?php echo h_out($item['MstSetDetails']['item_name']); ?></td>
                    <td class="col30"><?php echo h_out($item['MstSetDetails']['item_code']); ?></td>
                    <td class="col20"><?php echo h_out($item['MstSetDetails']['unit_name']); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo h_out($item['MstSetDetails']['standard']); ?></td>
                    <td><?php echo h_out($item['MstSetDetails']['dealer_name']); ?></td>
                    <td><?php echo h_out($item['MstSetDetails']['quantity']); ?></td>
                </tr>
            <?php $i++; } ?>
            </table>
        </div>
        <table>
            <tr>
                <th>バーコード</th>
                <td colspan="4">
                    <div>
                        <input type="text" class="txt15 r" id="codeinput" onkeyDown="return editTextArea(event)" />
                        <input type="button" id='claerBtn' value="" class="btn btn14" />
                    </div>
                    <div id="err_msg" class="RedBold"></div>
                </td>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <td colspan="4" valign="bottom">
                    <div style="float:left;"><ul class="barcodez" id="barcodelist" name="barcodelist"></ul></div>
                    <div style="float:right;"><span class="barcodez_count" id="barcode_count">読込件数：0件</span></div>
                    <br clear="all" />
                    <input type="button" value="" class="btn btn13" id="allClaerBtn" />
                </td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" id="confirmBtn" class="btn btn3 submit [p2]" /></p>
    </div>
</form>
