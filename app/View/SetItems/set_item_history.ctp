<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    $("#search_btn").click(function(){
        $("#SetItemHistory").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_item_history/').submit();
    });
    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $("#SetItemHistory").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/').submit();
    });

    $("#print_btn").click(function(){
        var cnt =  0;
        $('input[type=checkbox].chk').each(function(){
            if($(this).attr('checked') == true ){
                 // チェックの付いている項目の tmpSticker の 値を sticker にコピーする
                 $(this).parent(':eq(0)').find('input[type=hidden].sticker:eq(0)').val($(this).parent(':eq(0)').find('input[type=hidden].tmpsticker:eq(0)').val());
                 cnt++;
            }else{
                 // チェックの付いていない項目は空文字を入れておく
                 $(this).parent(':eq(0)').find('input[type=hidden].sticker:eq(0)').val('');
            }
        });
        if (cnt > 0) {
            $("#SetItemHistory").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/').submit();
        }else{
            alert('シール印字する対象を選択してください');
            return false;
        }
    });

    $("#report_btn").click(function(){
        var cnt =  0;
        $('input[type=checkbox].chk').each(function(){
            if($(this).attr('checked') == true ){
                 // チェックの付いている項目の tmpSticker の 値を sticker にコピーする
                 $(this).parent(':eq(0)').find('input[type=hidden].sticker:eq(0)').val($(this).parent(':eq(0)').find('input[type=hidden].tmpsticker:eq(0)').val());
                 cnt++;
            }else{
                 // チェックの付いていない項目は空文字を入れておく
                 $(this).parent(':eq(0)').find('input[type=hidden].sticker:eq(0)').val('');
            }
        });
        if (cnt > 0) {
            $("#SetItemHistory").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/').submit();
        }else{
            alert('印刷する対象を選択してください');
            return false;
        }
    });

    $("#detail_btn").click(function(){
        if ($('.TableStyle01 input:checked').length > 0) {
            $("#SetItemHistory").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_item_edit/').submit();
        }else{
            alert('閲覧する明細を選択してください');
            return false;
        }
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li>セット品履歴</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>セット組作業履歴</span></h2>
<form class="validate_form search_form"  id="SetItemHistory" method="post">
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstSetItem.mst_facility_id',array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstSetItem.mst_facility_name',array('type'=>'hidden')); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
          <table class="FormStyleTable">
              <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
              </colgroup>
              <tr>
                <th>作成番号</th>
                <td>
                <?php echo $this->form->input('MstSetItem.work_no' , array('type' => 'text' , 'class' => 'txt' , 'maxlength' => '50'));?>
                </td>
                <td></td>
                <th>作成日</th>
                <td>
                  <?php echo $this->form->input('MstSetItem.work_date_from' , array('type' => 'text' , 'class' => 'date validate[custom[date]]' , 'maxlength' => '10'));?> ～
                  <?php echo $this->form->input('MstSetItem.work_date_to' , array('type' => 'text' , 'class' => 'date validate[custom[date]]' , 'maxlength' => '10'));?>
                </td>
                <td></td>
                <td colspan="2">
                    <?php echo $this->form->checkbox('MstSetItem.is_deleted',array('hiddenField'=>false)); ?>取消は表示しない
                </td>
            </tr>
            <tr>
                <th>セットID</th>
                <td>
                 <?php echo $this->form->input('MstSetItem.set_id' , array('type' => 'text' , 'class' => 'txt search_internal_code' , 'maxlength' => '50'));?>
                </td>
                <td></td>
                <th>商品ID</th>
                <td>
                  <?php echo $this->form->input('MstSetItem.internal_code' , array('type' => 'text' , 'class' => 'txt search_internal_code' , 'maxlength' => '50'));?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                  <?php echo $this->form->input('MstSetItem.item_code' , array('type' => 'text' , 'class' => 'txt' , 'maxlength' => '50'));?>
                </td>
            </tr>
            <tr>
                <th>セット名</th>
                <td>
                  <?php echo $this->form->input('MstSetItem.set_name' , array('type' => 'text' , 'class' => 'txt search_canna' , 'maxlength' => '50'));?>
                </td>
                <td></td>
                <th>商品名</th>
                <td>
                  <?php echo $this->form->input('MstSetItem.item_name' , array('type' => 'text' , 'class' => 'txt search_canna' , 'maxlength' => '50'));?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                  <?php echo $this->form->input('MstSetItem.dealer_name' , array('type' => 'text' , 'class' => 'txt search_canna' , 'maxlength' => '50'));?>
                </td>
            </tr>
            <tr>
                <th></th>
                <td></td>
                <td></td>
                <th>規格</th>
                <td>
                  <?php echo $this->form->input('MstSetItem.standard' , array('type' => 'text' , 'class' => 'txt search_canna' , 'maxlength' => '50'));?>
                </td>
                <td></td>
                <th>シール番号</th>
                <td>
                  <?php echo $this->form->input('MstSetItem.facility_sticker_no' , array('type' => 'text' , 'class' => 'txt' , 'maxlength' => '50'));?>
                </td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center">
              <input type="button" class="btn btn1" id="search_btn" />
              &nbsp;&nbsp;
              <input type="button" class="btn btn5" id="btn_Csv"/>
            </p>
        </div>
    </div>
    <hr class="Clear" />
    <div class="DisplaySelect">
      <div align="right" id="pageTop" ></div>
        <?php echo $this->element('limit_combobox',array('result'=>count($result) ) ); ?>
    </div>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle01">
            <tr>
            <th style="width:20px;" class="center">
             <?php echo $this->form->input('MstSetItem.checkAll' , array('class' => 'checkAll' , 'type' => 'checkbox' , 'checked' => true));?>
            </th>
            <th class="col15">作成番号</th>
            <th class="col10">セットID</th>
            <th class="col20">セット名</th>
            <th class="col15">登録者</th>
            <th class="col15">登録日時</th>
            <th class="col15">備考</th>
            <th class="col10">件数</th>
            </tr>
        </table>
    </div>
<?php if(isset($data['search']['is_search'])){ ?>
    <div class="TableScroll">
        <table class="TableStyle01">
        <?php
        if(count($result) !== 0){
          $i = 0;
          foreach($result as $data) {
        ?>
         <tr>
                <td style="width:20px;" class="center">
                <?php echo $this->form->input('MstSetItem.id.'.$i , array('type' => 'checkbox' , 'class'=>'chk' , 'value' => $data['MstSetItem']['id'] ,  'hiddenField'=>false , 'checked' => true))?>
                <?php echo $this->form->input('MstSetItem.tmp_sticker_no.'.$i , array('type' => 'hidden' , 'value' => $data['MstSetItem']['sticker_no'] , 'class'=>'tmpsticker'))?>
                <?php echo $this->form->input('MstSetItem.sticker_no.'.$i , array('type' => 'hidden' ,'class' => 'sticker'))?>
                </td>
                </td>
                <td class='col15'><?php echo h_out($data['MstSetItem']['work_no'],'center'); ?></td>
                <td class="col10"><?php echo h_out($data['MstSetItem']['set_id'],'center'); ?></td>
                <td class="col20"><?php echo h_out($data['MstSetItem']['item_name']); ?></td>
                <td class="col15"><?php echo h_out($data['MstSetItem']['user_name']); ?></td>
                <td class="col15"><?php echo h_out(date('Y/m/d H:i:s', strtotime($data['MstSetItem']['created'])),'center'); ?></td>
                <td class="col15"><?php echo h_out($data['MstSetItem']['recital']); ?></td>
                <td class="col10"><?php echo h_out($data['MstSetItem']['count'] .' / '. $data['MstSetItem']['detail_count'],'right'); ?></td>
            </tr>
        <?php
            $i++;
          }
        }else{
           echo '<td colspan="8" class="center">該当するデータがありませんでした</td>';
        }
        ?>
          </table>
    </div>
<?php if( count($result) > 0 ){ ?>
    <div class="ButtonBox">
       <p class="center">
            <input type="button" class="btn btn7 submit" id="detail_btn"/>
            <input type="button" class="btn btn18 submit" id="print_btn"/>
            <input type="button" class="btn btn33 submit" id="report_btn"/>
        </p>
    </div>
    <div align="right" id ="pageDow"></div>
    <?php }
      }
      ?>
</form>
