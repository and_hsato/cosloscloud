<script type="text/javascript">
$(document).ready(function(){

    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#setItemForm #searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#setItemForm #cartForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );
    //検索ボタン押下
    $("#btn_Search").click(function(){
        //カート欄にある全IDを取得
        var tmpId = "";
        $("#cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tmpId === '' ){
                    tmpId = $(this).val();
                }else{
                    tmpId +="," +  $(this).val();
                }
            }
        });
        $("#tmpId").val(tmpId);
        $('#itemSelectedTable').empty();
        $('#containTables').empty();
        
        $("#setItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/select_stock').submit();
    });

    //選択ボタン押下

    $('#cmdSelect').click(function(){
        var cnt = 0;
        $("#setItemForm #searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().removeClass('odd').appendTo('#itemSelectedTable');
                $('#containTables'+$(this).val()).remove();
                cnt++;
            }
        });
        if(cnt === 0 ){
            alert('商品を選択してください');
            return false;
        }
        $("#searchForm input[type=checkbox].checkAllTarget").attr("checked",false);
        $('#cartForm input[type=checkbox].checkAllTarget').attr('checked',true);
        
        var rowcnt = 0;
        $("#searchForm table tbody").each(function(){
            $(this).find('tr').removeClass('odd');
            if (rowcnt%2 !== 0){
                $(this).find('tr').removeClass('odd').addClass('odd');
            }
            rowcnt++;
        });
        rowcnt = 0;
        //$("#itemSelectedTable table tbody").each(function(){
        $("#cartForm table tbody").each(function(){
            $(this).find('tr').removeClass('odd');
            if (rowcnt%2 !== 0){
                $(this).find('tr').removeClass('odd').addClass('odd');
            }
            rowcnt++;
        });
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        //選択チェック
        var cnt = 0;
        var tmpId = '';
        $("#setItemForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
                if(tmpId === ''){
                    tmpId = $(this).val();
                }else{
                    tmpId = tmpId + ','+$(this).val();
                }
            }
        });
        $('#tmpId').val(tmpId);
        if(cnt === 0){
            alert('セットに追加する商品を選択してください');
        }else{
            $("#setItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_item_confirm').submit();
        }
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_items/">セット組作業</a></li>
        <li class="pankuzu">セット組作業在庫選択</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>セット組作業在庫選択</span></h2>

<form class="validate_form search_form" id="setItemForm" method="post" action="select_stock">
    <?php echo $this->form->input('add_search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('MstSetItem.id',array('type'=>'hidden'));?>
    <?php echo $this->form->input('MstSetItem.item_unit_id',array('type'=>'hidden'));?>
    <?php echo $this->form->input('cart.tmpid',array('type'=>'hidden','id'=>'tmpId'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>セットID</th>
                <td><?php echo $this->form->input('MstSetItem.internal_code',array('class' => 'lbl', 'readonly' => 'readonly'));?></td>
                <td width="20"></td>
                <th>規格</th>
                <td><?php echo $this->form->input('MstSetItem.standard',array('class' => 'lbl', 'readonly' => 'readonly'));?></td>
            </tr>
            <tr>
                <th>セット名称</th>
                <td><?php echo $this->form->input('MstSetItem.item_name',array('class' => 'lbl', 'readonly' => 'readonly'));?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstSetItem.item_code',array('class' => 'lbl', 'readonly' => 'readonly'));?></td>
            </tr>
        </table>
        <h2 class="HeaddingSmall">構成品一覧</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th width="20" rowspan="2"></th>
                    <th class="col20" >商品ID</th>
                    <th class="col30">商品名</th>
                    <th class="col30">製品番号</th>
                    <th class="col20">包装単位</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02 table-even2">
            <?php
              $i = 0;
              foreach($setDetails as $item){
              ?>
                <tr>
                    <td width="20" rowspan="2"><?php echo $this->form->input('MstSetDetails.id' , array('type'=> 'hidden' , 'value' => $item['MstSetDetails']['quantity'] , 'class' => 'hidden_id', 'id' => $item['MstSetDetails']['id'])) ?></td>
                    <td class="col20"><?php echo h_out($item['MstSetDetails']['internal_code']); ?></td>
                    <td class="col30"><?php echo h_out($item['MstSetDetails']['item_name']); ?></td>
                    <td class="col30"><?php echo h_out($item['MstSetDetails']['item_code']); ?></td>
                    <td class="col20"><?php echo h_out($item['MstSetDetails']['unit_name']); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo h_out($item['MstSetDetails']['standard']); ?></td>
                    <td><?php echo h_out($item['MstSetDetails']['dealer_name']); ?></td>
                    <td><?php echo h_out($item['MstSetDetails']['quantity']); ?></td>
                </tr>
            <?php
                $i++;
            } ?>
            </table>
        </div>

<h2 class="HeaddingMid">検索条件</h2>

        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFacilityItem.internal_code',array('class'=>'txt search_internal_code','style'=>'width:120px')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_code',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFacilityItem.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFacilityItem.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('MstFacilityItem.jan_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>

        <div class="ButtonBox">
           <p class="center">
             <input type="button" class="btn btn1" id="btn_Search"/>
           </p>
        </div>
    </div>
    <div align="right" id="pageTop" ></div>

<?php
    if(isset($this->request->data['add_search']['is_search'])){
      if(count($SearchResult) === 0){
?>
      <div class="DisplaySelect">
          <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
          <table class="TableHeaderStyle02">
              <tr>
                  <th rowspan="2" style="width:20px;"><input type="checkbox"/></th>
                  <th class="col10">商品ID</th>
                  <th class="col20">商品名</th>
                  <th class="col20">製品番号</th>
                  <th class="col20">包装単位</th>
                  <th class="col15">ロット番号</th>
                  <th class="col15">センターシール</th>
              </tr>
              <tr>
                  <th></th>
                  <th>規格</th>
                  <th>販売元</th>
                  <th>仕入価格</th>
                  <th>有効期限</th>
                  <th></th>
              </tr>
          </table>
      </div>
      <span>該当するデータはありません。</span>
<?php
      }else{
?>
    <div id="results">
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
            <th rowspan="2" style="width:20px;"><input type="checkbox" class='checkAll_1' /></th>
            <th class="col10">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">製品番号</th>
            <th class="col20">包装単位</th>
            <th class="col15">ロット番号</th>
            <th class="col15">センターシール</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>仕入価格</th>
            <th>有効期限</th>
            <th></th>
        </tr>
      </table>
    </div>

    <div class="TableScroll div-even" id="searchForm">
<?php
  $i = 0;
  foreach($SearchResult as $item){
  $class = ($i % 2 == 0)?'':'odd';
  ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$item["MstSetItem"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
        <tbody>
          <tr id=<?php echo "itemSelectedRow".$item['MstSetItem']['id'];?> >
            <td class="center" rowspan="2" style="width:20px;">
              <?php echo $this->form->checkbox("SelectedID.{$i}", array('class'=>'center checkAllTarget','value'=>$item['MstSetItem']['id'] , 'style'=>'width:20px;text-align:center;' , 'hiddenField'=>false) );?>
            </td>
                <td class="col10"><?php echo h_out($item['MstSetItem']['internal_code']); ?></td>
                <td class="col20"><?php echo h_out($item['MstSetItem']['item_name']); ?></td>
                <td class="col20"><?php echo h_out($item['MstSetItem']['item_code']); ?></td>
                <td class="col20"><?php echo h_out($item['MstSetItem']['unit_name']); ?></td>
                <td class="col15"><?php echo h_out($item['MstSetItem']['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($item['MstSetItem']['facility_sticker_no']); ?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($item['MstSetItem']['standard']); ?></td>
                <td><?php echo h_out($item['MstSetItem']['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($item['MstSetItem']['transaction_price'])); ?></td>
                <td><?php echo h_out($item['MstSetItem']['validated_date']); ?></td>
                <td></td>
            </tr>
           </tbody>
      </table>
      <?php $i++; } ?>
    </div>
      <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
          <input type="button" value="" class="btn btn4" id="cmdSelect" />
        </p>
      </div>
    </div>
    <?php } ?>

    <?php if(isset($this->request->data['add_search']['is_search'])){?>
    <h2 class="HeaddingSmall">選択商品</h2>
    <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
            <th rowspan="2" style="width:20px;"><input type="checkbox" checked class='checkAll_2' /></th>
            <th class="col10">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">製品番号</th>
            <th class="col20">包装単位</th>
            <th class="col15">ロット番号</th>
            <th class="col15">センターシール</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>仕入価格</th>
            <th>有効期限</th>
            <th></th>
        </tr>
      </table>
    </div>

    <div class="TableScroll div-even" broder='0'  id="cartForm" >
      <?php if(isset($CartSearchResult)){ ?>
      
<?php
  $i = 0;
  foreach($CartSearchResult as $item){
  ?>
      <table class="TableStyle02" id="addTable" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <tr>
              <td class="center" rowspan="2" style="width:20px;">
              <?php echo $this->form->checkbox('MstSetItem.selectid.'.$i , array('checked' => 1 , 'class'=>'center checkAllTarget' , 'value'=>$item['MstSetItem']['id'] , 'hiddenField'=>false) ); ?>
              </td>
              <td class="col10"><?php echo h_out($item['MstSetItem']['internal_code']); ?></td>
              <td class="col20"><?php echo h_out($item['MstSetItem']['item_name']); ?></td>
              <td class="col20"><?php echo h_out($item['MstSetItem']['item_code']); ?></td>
              <td class="col20"><?php echo h_out($item['MstSetItem']['unit_name']); ?></td>
              <td class="col15"><?php echo h_out($item['MstSetItem']['lot_no']); ?></td>
              <td class="col15"><?php echo h_out($item['MstSetItem']['facility_sticker_no']); ?></td>
          </tr>
          <tr>
              <td></td>
              <td><?php echo h_out($item['MstSetItem']['standard']); ?></td>
              <td><?php echo h_out($item['MstSetItem']['dealer_name']); ?></td>
              <td><?php echo h_out($this->Common->toCommaStr($item['MstSetItem']['transaction_price'])); ?></td>
              <td><?php echo h_out($item['MstSetItem']['validated_date']); ?></td>
              <td></td>
          </tr>
      </table>
      <?php
              $i++;
              }
            ?>
        
      <?php }?>
      <div id="itemSelectedTable" border="0" style="margin-top:-1px; margin-bottom:-1px; padding:0px;"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" value="" class="btn btn3" />
      </p>
    </div>
    <?php }?>
<?php
    }else{
      //検索実行フラグがfalse
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>0)); ?>
      </div>

      <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
            <th rowspan="2" style="width:20px;"><input type="checkbox" /></th>
            <th class="col10">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">製品番号</th>
            <th class="col20">包装単位</th>
            <th class="col15">ロット番号</th>
            <th class="col15">センターシール</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>仕入価格</th>
            <th>有効期限</th>
            <th></th>
        </tr>
      </table>

      </div>
<?php
    }
    echo $this->form->end();
?>
<div align="right" id="pageDow" ></div>
