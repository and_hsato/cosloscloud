<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_item_history/">セット組作業履歴</a></li>
        <li class="pankuzu">セット組作業履歴編集</li>
        <li>セット組作業履歴編集結果</li>
    </ul>
</div>


<h2 class="HeaddingLarge"><span>セット組作業履歴編集結果</span></h2>
  <div class="Mes01">以下のセットを解除しました</div>

<form id="SetItemEdit">
    <?php echo $this->form->input('MstSetItem.mst_facility_id',array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstSetItem.mst_facility_name',array('type'=>'hidden')); ?>
    <div id="results">
    <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle01">
        <tr>
          <th style="width:20px;" rowspan="2">
          </th>
          <th class="col15" rowspan="2">作業番号<br/>作業日</th>
          <th class="col15" rowspan="2">シール番号</th>
          <th class="col15" rowspan="2">セットID<br/>セット名<br/>規格</th>
          <th class="col10">商品ID</th>
          <th class="col20">商品名</th>
          <th class="col10">製品番号</th>
          <th class="col15">包装単位</th>
        </tr>
        <tr>
          <th class="col10"></th>
          <th class="col20">規格</th>
          <th class="col20">販売元</th>
          <th class="col15">センターシール</th>
        </tr>
      </table>
    </div>
    <div class="TableScroll" style="">
      <table class="TableStyle01">

      <?php
        $count = null;
        $i=1;
        $class='odd';
        foreach($result as $data) { ?>
      <?php
          if(is_null($count)){
              $class=(($class=='')?'odd':'');
              $count = $data['MstSetItem']['detail_count'];
      ?>
      <tr class="<?php echo $class?>">
           <td style="width:20px;"  rowspan="<?php echo ($count * 2 )?>">
              </td>
              <td class="col15 center" rowspan="<?php echo ($count * 2 )?>"><?php echo $data['MstSetItem']['work_no'] ?><br/>
                  <?php echo date('Y/m/d' , strtotime($data['MstSetItem']['work_date'])) ?></td>
              <td class="col15 center" rowspan="<?php echo ($count * 2 )?>"><?php echo $data['MstSetItem']['sticker_no'] ?></td>
              <td class="col15" rowspan="<?php echo ($count * 2 )?>"><?php echo $data['MstSetItem']['internal_code'] ?><br><?php echo $data['MstSetItem']['item_name'] ?><br/><?php echo $data['MstSetItem']['standard'] ?></td>
              <td class="col10"><?php echo $data['MstSetItemPart']['internal_code'] ?></td>
              <td class="col20"><?php echo $data['MstSetItemPart']['item_name'] ?></td>
              <td class="col10"><?php echo $data['MstSetItemPart']['item_code'] ?></td>
              <td class="col15"><?php echo $data['MstSetItemPart']['unit_name'] ?></td>
        </tr>
        <tr class="<?php echo $class?>">
              <td class="col10"></td>
              <td class="col20"><?php echo $data['MstSetItemPart']['standard'] ?></td>
              <td class="col20"><?php echo $data['MstSetItemPart']['dealer_name'] ?></td>
              <td class="col15"><?php echo $data['MstSetItemPart']['sticker_no'] ?></td>
        </tr>

<?php }else{ ?>
        <tr class="<?php echo $class?>">
              <td class="col10"><?php echo $data['MstSetItemPart']['internal_code'] ?></td>
              <td class="col20"><?php echo $data['MstSetItemPart']['item_name'] ?></td>
              <td class="col10"><?php echo $data['MstSetItemPart']['item_code'] ?></td>
              <td class="col15"><?php echo $data['MstSetItemPart']['unit_name'] ?></td>
        </tr>
        <tr class="<?php echo $class?>">
              <td class="col10"></td>
              <td class="col20"><?php echo $data['MstSetItemPart']['standard'] ?></td>
              <td class="col20"><?php echo $data['MstSetItemPart']['dealer_name'] ?></td>
              <td class="col15"><?php echo $data['MstSetItemPart']['sticker_no'] ?></td>
        </tr>
        <?php $i++; }

          if($count ==  $i){
          $i = 1;
          $count = null;
          }

 } ?>
      </table>

    </div>
</form>
