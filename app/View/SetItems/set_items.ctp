<script type="text/javascript">
<!--
$(function(){
    $("#mst_set_items_search_btn").click(function(){
        $("#mst_set_items_search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_items').submit();
    });
    $("#read_stickers_btn").click(function(){
        if( $('input[name="data[MstSetItem][id]"]:checked').length > 0){
            $("#mst_set_items_search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/read_stickers').submit();
        }else{
            alert('編集するセット品を選択してください。');
            return false;
        }
    });
    $("#select_stock_btn").click(function(){
        if( $('input[name="data[MstSetItem][id]"]:checked').length > 0) {
            $("#mst_set_items_search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/select_stock').submit();
        }else{
            alert('編集するセット品を選択してください。');
            return false;
        }
    });
});

//-->
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>セット品登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>セット組作業</span></h2>
<h2 class="HeaddingMid">セット品検索条件</h2>
<form class="validate_form search_form" id="mst_set_items_search_form" method="post" action="set_items">
    <?php echo $this->form->input('search.is_search' , array('type' => 'hidden'));?>
    <div class="SearchBox">
        <div class="ColumnLeft">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>セットID</th>
                    <td><?php echo $this->form->text('MstSetItems.internal_code' ,array('class' => 'txt search_internal_code','maxlength' => '50' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th>規格</th>
                    <td><?php echo $this->form->text('MstSetItems.standard',array('class' => 'txt search_canna','maxlength' => '50','label' =>'' ));?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>セット名</th>
                    <td><?php echo $this->form->text('MstSetItems.item_name',array('class' => 'txt search_canna','maxlength' => '50', 'label' =>'') );?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->text('MstSetItems.item_code',array('class' =>'txt','maxlength' => '50', 'label' =>'' )); ?></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1"id="mst_set_items_search_btn"/>  
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>

    <?php echo $this->element('limit_combobox',array('result'=>(count($result)))); ?>

    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th></th>
                <th>セットID</th>
                <th>セット名</th>
                <th>規格</th>
                <th>製品番号</th>
            </tr>
            <?php if (isset($result) && count($result) != 0){ ?>
            <?php $i = 0; foreach ($result as $item){ ?>
            <tr>
                <td class="center">
                    <input type="radio" name="data[MstSetItem][id]" id="item_select_radio_btn" value="<?php echo $item['MstSetItem']['id']; ?>" />
                </td>
                <td><?php echo h_out($item['MstSetItem']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($item['MstSetItem']['item_name']); ?></td>
                <td><?php echo h_out($item['MstSetItem']['standard']); ?></td>
                <td><?php echo h_out($item['MstSetItem']['item_code']); ?></td>
            </tr>
            <?php $i++; } ?>
            <?php }elseif(isset($data['search']['is_search'])){ ?>
            <tr><td colspan="5" class="center">該当するデータはありません。</td></tr>
            <?php } ?>
        </table>
    </div>

    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <?php if(isset($this->request->data['search']['is_search'])){?>
    <div class="ButtonBox">
        <?php if (isset($result) && count($result) != 0){ ?>
        <input type="button" class="btn btn46" id="read_stickers_btn"/>
        <input type="button" class="btn btn15" id="select_stock_btn"/>
        <?php } ?>
    </div>
    <?php } ?>
</form>
