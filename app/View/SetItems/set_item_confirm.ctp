<script type="text/javascript">
Array.prototype.count = function(){
    var count = 0;
    for (var key in this) {
        if (this.hasOwnProperty(key)) count++;
    }
    return count;
}

$(function() {
    $(document).ready(function(){
        //チェックボックス オールチェック
        $('.checkAll_1').click(function(){
            $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
        });
  
        $('#setItem_add_confirm_btn').click(function(){
            var cnt = 0;
            var errMsg = '';
            var ids = '';
            var t = new Array();
            $('input[type=checkbox].checkAllTarget').each(function(){
                // チェックのついているIDの数量を足しこんで配列に保持する
                if($(this).attr('checked') == true ){
                   //包装単位ID
                   var item_id = $(this).val();
                   //シール数量
                   var quantity = $(this).parent(':eq(0)').find('input[type=hidden].quantity:eq(0)').val();
                   //シールID
                   var id = $(this).parent(':eq(0)').find('input[type=hidden].sticker_id:eq(0)').val();
                   //チェックの付いていたシールIDを保持しておく
                   if(ids == ''){
                       ids = id;
                   }else{
                       ids = ids + ',' + id;
                   }
                   if(t[item_id]){
                       t[item_id] =  parseInt(t[item_id]) + parseInt(quantity);
                   }else{
                       t[item_id] =  parseInt(quantity);
                   }
                   cnt++;
                }
            });

            $('input[type=hidden].hidden_id').each(function(){
                //入力とセットの構成品一覧を比較
                if(t[$(this).attr('id')]){
                   //構成品のIDがチェック要素にあったか？
                   if(t[$(this).attr('id')] == $(this).val()){
                       //チェックした数量は構成品の期待数量と同じか？
                   }else{
                       errMsg = '数量が誤っています。';
                       return false;
                   }
                }else{
                   //構成品の方が多い
                   errMsg = '必要な構成品が選択されていません。';
                   return false;
                }
            });
            if($('input[type=hidden].hidden_id').size() != t.count()){
                errMsg='構成品に不要なものが選択されています。';
            }
            if(cnt === 0){
                alert('登録するデータを選択してください');
                return false;
            } else if(errMsg != ''){
                // エラーメッセージに何か入っていたら出力して終了
                alert(errMsg);
                return false;
            } else {
                // シールIDを格納する
                $('#select_stickers').val(ids);
                $(window).unbind('beforeunload');
                $('#setItem_add_confirm_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_item_result');
                $('#setItem_add_confirm_form').attr('method', 'post');
                $('#setItem_add_confirm_form').submit();
            }
        });
    });
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_items">セット組作業</a></li>
        <li>セット組作業確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>セット組作業確認</span></h2>
<form class="validate_form" id="setItem_add_confirm_form" method="post">
    <?php echo $this->form->input('select.stickers' , array('type' => 'hidden' , 'id'=> 'select_stickers'));?>
    <?php echo $this->form->input('MstSetItem.id' , array('type' => 'hidden' , ));?>
    <?php echo $this->form->input('MstSetItem.item_unit_id' , array('type' => 'hidden' , ));?>
    <?php echo $this->form->input('SetItem.token' , array('type' => 'hidden' , ));?>
    <?php echo $this->form->input('MstSetItem.time' , array('type' => 'hidden' , 'value'=>date('Y/m/d H:i:s.u')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>セットID</th>
                <td>
                    <?php echo $this->form->input('MstSetItem.internal_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
                </td>
                <td width="20"></td>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstSetItem.standard',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
                </td>
            </tr>
            <tr>
                <th>セット名称</th>
                <td>
                    <?php echo $this->form->input('MstSetItem.item_name',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('MstSetItem.item_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
                </td>
            </tr>
            <tr>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('MstSetItem.work_type' , array('options'=>$work_type ,'class' => 'txt' , 'empty'=>true) ) ;?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('MstSetItem.recital' , array('class'=>'txt'))?>
                </td>
            </tr>
        </table>
    </div>
    <h2 class="HeaddingSmall">構成品一覧</h2>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th width="20" rowspan="2"></th>
                <th class="col20">商品ID</th>
                <th class="col30">商品名</th>
                <th class="col30">製品番号</th>
                <th class="col20">包装単位</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>数量</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even2">
        <?php
          $i = 0;
          foreach($setDetails as $item){
          ?>
            <tr>
                <td width="20" rowspan="2"><?php echo $this->form->input('MstSetDetails.id' , array('type'=> 'hidden' , 'value' => $item['MstSetDetails']['quantity'] , 'class' => 'hidden_id', 'id' => $item['MstSetDetails']['id'])) ?></td>
                <td class="col20"><?php echo $item['MstSetDetails']['internal_code']; ?></td>
                <td class="col30"><?php echo $item['MstSetDetails']['item_name']; ?></td>
                <td class="col30"><?php echo $item['MstSetDetails']['item_code']; ?></td>
                <td class="col20"><?php echo $item['MstSetDetails']['unit_name']; ?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo $item['MstSetDetails']['standard']; ?></td>
                <td><?php echo $item['MstSetDetails']['dealer_name']; ?></td>
                <td><?php echo $item['MstSetDetails']['quantity']; ?></td>
            </tr>
        <?php
            $i++;
        } ?>
        </table>
    </div>

    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th rowspan="2" style="width:20px;">
                    <input type="checkbox"  checked="checked" class='checkAll_1'/>
                </th>
                <th class="col10">商品ID</th>
                <th class="col20">商品名</th>
                <th class="col20">製品番号</th>
                <th class="col20">包装単位</th>
                <th class="col15">ロット番号</th>
                <th class="col15">センターシール</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>仕入価格</th>
                <th>有効期限</th>
                <th>状態</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even2">
        <?php
        $i = 0;
            foreach($result as $item){
        ?>
            <tr>
                <td rowspan="2" style="width:20px;" class="center">
                    <?php if(empty($item['MstSetItem']['status'])){?>
                    <?php echo $this->form->checkbox('MstSetItem.item_id.'.$i , array('value'=>$item['MstSetItem']['item_unit_id'] , 'class' => 'checkAllTarget' , 'checked'=>'checked' , 'hiddenField'=>false) ); ?>
                    <?php } ?>
                    <?php echo $this->form->input('MstSetItem.quantity.'.$i , array('type'=>'hidden' ,'class'=>'quantity' , 'value'=>$item['MstSetItem']['quantity']) );?>
                    <?php echo $this->form->input('MstSetItem.sticker_id.'.$i , array('type'=>'hidden' ,'class'=>'sticker_id' , 'value'=>$item['MstSetItem']['id']) );?>
                </td>
                <td class="col10"><?php echo h_out($item['MstSetItem']['internal_code']); ?></td>
                <td class="col20"><?php echo h_out($item['MstSetItem']['item_name']); ?></td>
                <td class="col20"><?php echo h_out($item['MstSetItem']['item_code']); ?></td>
                <td class="col20"><?php echo h_out($item['MstSetItem']['unit_name']); ?></td>
                <td class="col15"><?php echo h_out($item['MstSetItem']['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($item['MstSetItem']['facility_sticker_no']); ?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($item['MstSetItem']['standard']); ?></td>
                <td><?php echo h_out($item['MstSetItem']['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($item['MstSetItem']['transaction_price'])); ?></td>
                <td><?php echo h_out($item['MstSetItem']['validated_date']); ?></td>
                <td><p class="red" style='color:red;'><?php echo $item['MstSetItem']['status'] ?></p></td>
            </tr>
            <?php $i++; } ?>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" id="setItem_add_confirm_btn" class="btn btn2 submit [p2]" />
        </p>
    </div>
</form>
