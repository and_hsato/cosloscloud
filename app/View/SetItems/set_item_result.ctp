<script type="text/javascript">
$(function() {
    $(document).ready(function(){
        $('#seal_print_btn').click(function() {
            $(window).unbind('beforeunload');
            $('#setItem_add_confirm_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal').submit();
        });
        $('#report_print_btn').click(function() {
            $('#setItem_add_confirm_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report').submit();
        });
    });
    $(window).bind('beforeunload' ,function(){
        return 'シール印字が完了していません。';
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_items">セット組作業</a></li>
        <li class="pankuzu">セット組作業確認</li>
        <li>セット組作業結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>セット組作業結果</span></h2>
<div class="Mes01">以下の内容で更新しました</div>
<form class="validate_form" id="setItem_add_confirm_form" method="post">
    <?php echo $this->form->input('select.stickers' , array('type' => 'hidden' , 'id'=> 'select_stickers'));?>
    <?php echo $this->form->input('MstSetItem.sticker_id' , array('type' => 'hidden' ));?>
    <?php echo $this->form->input('MstSetItem.sticker_no' , array('type' => 'hidden' ));?>
    <?php echo $this->form->input('MstSetItem.mst_facility_id' , array('type' => 'hidden' ));?>
    <?php echo $this->form->input('MstSetItem.mst_facility_name' , array('type' => 'hidden' ));?>
    <?php echo $this->form->input('MstSetItem.id' , array('type' => 'hidden' ));?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>セットID</th>
                <td><?php echo $this->form->input('MstSetItem.internal_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
                <td width="20"></td>
                <th>規格</th>
                <td><?php echo $this->form->input('MstSetItem.standard',array('class' => 'lbl', 'readonly' => 'readonly'));?></td>
            </tr>
            <tr>
                <th>セット名称</th>
                <td><?php echo $this->form->input('MstSetItem.item_name',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstSetItem.item_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
            </tr>
            <tr>
                <th>作業区分</th>
                <td><?php echo $this->form->input('MstSetItem.work_type' , array('options'=>$work_type ,'class' => 'lbl' , 'empty'=>true , 'readonly'=>'readonly' , 'disabled' => 'disabled') ) ;?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('MstSetItem.recital' , array('class'=>'lbl' , 'readonly'=>'readonly'))?></td>
            </tr>
        </table>
    </div>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th rowspan="2" style="width:20px;"></th>
                <th class="col10">商品ID</th>
                <th class="col20">商品名</th>
                <th class="col20">製品番号</th>
                <th class="col20">包装単位</th>
                <th class="col15">ロット番号</th>
                <th class="col15">センターシール</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>仕入価格</th>
                <th>有効期限</th>
                <th>状態</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even2">
<?php
  $i = 0;
  foreach($result as $item){
  ?>
            <tr>
                <td rowspan="2" style="width:20px;" class="center"></td>
                <td class="col10"><?php echo h_out($item['MstSetItem']['internal_code']); ?></td>
                <td class="col20"><?php echo h_out($item['MstSetItem']['item_name']); ?></td>
                <td class="col20"><?php echo h_out($item['MstSetItem']['item_code']); ?></td>
                <td class="col20"><?php echo h_out($item['MstSetItem']['unit_name']); ?></td>
                <td class="col15"><?php echo h_out($item['MstSetItem']['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($item['MstSetItem']['facility_sticker_no']); ?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($item['MstSetItem']['standard']); ?></td>
                <td><?php echo h_out($item['MstSetItem']['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($item['MstSetItem']['transaction_price'])); ?></td>
                <td><?php echo h_out($item['MstSetItem']['validated_date']); ?></td>
                <td></td>
            </tr>
            <?php $i++; } ?>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" id="seal_print_btn" class="btn btn18 [p2]" />
            <input type="button" id="report_print_btn" class="btn btn33 [p2]" />
        </p>
    </div>
</form>

