<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
  
    $("#chancel_btn").click(function(){
        var cnt   = 0;
        var ngCnt = 0;
        $('input[type=checkbox].chk').each(function(){
            if($(this).attr('checked') == true ){
                 // チェックの付いている項目の status を確認して取り消し可能な明細か確認する
                 if($(this).parent(':eq(0)').find('input[type=hidden].status:eq(0)').val()==1){
                     ngCnt++;
                 }
                 cnt++;
            }
        });
        if (cnt > 0 && ngCnt === 0) {
            $("#SetItemEdit").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_item_edit_result/').submit();
        }else{
            alert('取消し不可能な明細を'+ngCnt+'件選択しています。');
            return false;
        }
    });
    $("#print_btn").click(function(){
        var cnt =  0;
        $('input[type=checkbox].chk').each(function(){
            if($(this).attr('checked') == true ){
                 // チェックの付いている項目の tmpSticker の 値を sticker にコピーする
                 $(this).parent(':eq(0)').find('input[type=hidden].sticker:eq(0)').val($(this).parent(':eq(0)').find('input[type=hidden].tmpsticker:eq(0)').val());
                 cnt++;
            }else{
                 // チェックの付いていない項目は空文字を入れておく
                 $(this).parent(':eq(0)').find('input[type=hidden].sticker:eq(0)').val('');
            }
        });
        if (cnt > 0) {
            $("#SetItemEdit").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/').submit();
        }else{
            alert('シール印字する明細を選択してください');
            return false;
        }
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/set_item_history/">セット組作業履歴</a></li>
        <li>セット組作業履歴</li>
    </ul>
</div>


<h2 class="HeaddingLarge"><span>セット組作業履歴編集</span></h2>

<form class='validate_form' id="SetItemEdit" method="post">
    <?php echo $this->form->input('MstSetItem.mst_facility_id',array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstSetItem.mst_facility_name',array('type'=>'hidden')); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div id="results">
    <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle01">
        <tr>
          <th style="width:20px;" rowspan="2">
             <?php echo $this->form->input('MstSetItem.checkAll' , array('class' => 'checkAll' , 'type' => 'checkbox' , 'checked' => true));?>
         </th>
          <th class="col15" rowspan="2">作業番号<br/>作業日</th>
          <th class="col15" rowspan="2">シール番号</th>
          <th class="col15" rowspan="2">セットID<br/>セット名<br/>規格</th>

          <th class="col10">商品ID</th>
          <th class="col20">商品名</th>
          <th class="col10">製品番号</th>
          <th class="col15">包装単位</th>
        </tr>
        <tr>
          <th class="col10"></th>
          <th class="col20">規格</th>
          <th class="col20">販売元</th>
          <th class="col15">センターシール</th>
        </tr>
      </table>
    </div>
    <div class="TableScroll">
      <table class="TableStyle01">

      <?php
        $count = null;
        $i=1;
        $j=0;
        $class='odd';
        foreach($result as $data) { ?>
      <?php
          if(is_null($count)){
              $count = $data['MstSetItem']['detail_count'];
              $class=(($class=='')?'odd':'');
      ?>
      <tr class="<?php echo $class ?>">
           <td style="width:20px;"  rowspan="<?php echo ($count * 2 )?>" class="center">
                <?php echo $this->form->input('MstSetItem.id.'.$j , array('type' => 'checkbox' , 'class'=>'chk' , 'value' => $data['MstSetItem']['id'] ,  'hiddenField'=>false , 'checked' => true))?>
                <?php echo $this->form->input('MstSetItem.tmp_sticker_no.'.$j , array('type' => 'hidden' , 'value' => $data['MstSetItem']['sticker_no'] , 'class'=>'tmpsticker'))?>
                <?php echo $this->form->input('MstSetItem.sticker_no.'.$j , array('type' => 'hidden' ,'class' => 'sticker'))?>
                <?php echo $this->form->input('MstSetItem.status.'.$j , array('type' => 'hidden' , 'value' => $data['MstSetItem']['status'] , 'class'=>'status'))?>
              </td>
              <td class="col15 center" rowspan="<?php echo ($count * 2 )?>"><?php echo $data['MstSetItem']['work_no'] ?><br/>
              <?php echo date('Y/m/d' , strtotime($data['MstSetItem']['work_date'])) ?></td>
              <td class="col15 center" rowspan="<?php echo ($count * 2 )?>"><?php echo $data['MstSetItem']['sticker_no'] ?></td>
              <td class="col15" rowspan="<?php echo ($count * 2 )?>"><?php echo $data['MstSetItem']['internal_code'] ?><br><?php echo $data['MstSetItem']['item_name'] ?><br/><?php echo $data['MstSetItem']['standard'] ?></td>

              <td class="col10"><?php echo h_out($data['MstSetItemPart']['internal_code']); ?></td>
              <td class="col20"><?php echo h_out($data['MstSetItemPart']['item_name']); ?></td>
              <td class="col10"><?php echo h_out($data['MstSetItemPart']['item_code']); ?></td>
              <td class="col15"><?php echo h_out($data['MstSetItemPart']['unit_name']); ?></td>
        </tr>
        <tr class="<?php echo $class ?>">
              <td class="col10"></td>
              <td class="col20"><?php echo h_out($data['MstSetItemPart']['standard']); ?></td>
              <td class="col20"><?php echo h_out($data['MstSetItemPart']['dealer_name']); ?></td>
              <td class="col15"><?php echo h_out($data['MstSetItemPart']['sticker_no']); ?></td>
        </tr>

<?php }else{ ?>
        <tr class="<?php echo $class ?>">

              <td class="col10"><?php echo h_out($data['MstSetItemPart']['internal_code']); ?></td>
              <td class="col20"><?php echo h_out($data['MstSetItemPart']['item_name']); ?></td>
              <td class="col10"><?php echo h_out($data['MstSetItemPart']['item_code']); ?></td>
              <td class="col15"><?php echo h_out($data['MstSetItemPart']['unit_name']); ?></td>
        </tr>
        <tr class="<?php echo $class ?>">
              <td class="col10"></td>
              <td class="col20"><?php echo h_out($data['MstSetItemPart']['standard']); ?></td>
              <td class="col20"><?php echo h_out($data['MstSetItemPart']['dealer_name']); ?></td>
              <td class="col15"><?php echo h_out($data['MstSetItemPart']['sticker_no']); ?></td>
        </tr>
        <?php $i++; }
          if($count ==  $i){
              $i = 1;
              $count = null;
              $j++;
          }

 } ?>
        </table>
        <div class="ButtonBox">
            <input type="button" class="btn btn6" id="chancel_btn" />
            <input type="button" class="btn btn18" id="print_btn" />
        </div>
    </div>
</form>
