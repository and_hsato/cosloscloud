<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/ope_method_set_search/">手術術式セット一覧</a></li>
        <li class="pankuzu">材料選択</li>
        <li class="pankuzu">手術術式セット確認</li>
        <li>手術術式セット完了</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>手術術式セット完了</span></h2>
<div class="Mes01">以下の内容を設定しました</div>
<div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col/>
            <col/>
            <col width="20"/>
            <col/>
            <col/>
            <col width="20"/>
        </colgroup>
        <tr>
            <th>術式セットコード</th>
            <td><?php echo $this->form->text('MstOpeMethod.code',array('class'=>'lbl','readonly'=>'readonly','style'=>'width:120px' , 'id' => 'code' , 'maxlength'=>'20')); ?></td>
            <td></td>
            <th>術式セット名</th>
            <td><?php echo $this->form->text('MstOpeMethod.name',array('class'=>'lbl','readonly'=>'readonly','style'=>'width:600px' , 'id' => 'name' , 'maxlength'=>'100')); ?></td>
            <td></td>
        </tr>
        <tr>
            <th>コメント</th>
            <td colspan="5">
                <textarea id="textarea" name="data[MstOpeMethod][comment]" readonly="readonly" cols="120" rows="5" maxlength="200" style="height:auto !important"><?php echo $this->request->data['MstOpeMethod']['comment'] ?></textarea>
            </td>
        </tr>
    </table>

</div>
<div align="right" id="pageTop" ></div>

<h2 class="HeaddingSmall">材料セット一覧</h2>
<div class="TableScroll">
    <table class="TableStyle01 table-odd">
        <tr>
            <th width="20"></th>
            <th class="col30">材料セットコード</th>
            <th class="col70">材料セット名</th>
        </tr>
        <?php  $i = 0; foreach($result as $item){ ?>
        <tr>
            <td width="20" class="center"></td>
            <td class="col30"><?php echo h_out($item['MstOpeItemSet']['code']); ?></td>
            <td class="col70"><?php echo h_out($item['MstOpeItemSet']['name']); ?></td>
        </tr>
        <?php $i++; } ?>
    </table>
</div>
<div align="right" id="pageDow" ></div>
