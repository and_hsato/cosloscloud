<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/ope_item_set_search/">材料セット一覧</a></li>
        <li class="pankuzu">材料選択</li>
        <li class="pankuzu">材料セット確認</li>
        <li>材料セット完了</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>手術材料セット完了</span></h2>
<div class="Mes01">以下の内容を設定しました</div>
<div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col/>
            <col/>
            <col width="20"/>
            <col/>
            <col/>
            <col width="20"/>
        </colgroup>
        <tr>
            <th>材料セットコード</th>
            <td><?php echo $this->form->text('MstOpeItemSetHeader.ope_item_set_code',array('class'=>'lbl','readonly'=>'readonly','style'=>'width:120px' , 'id' => 'code' , 'maxlength'=>'20')); ?></td>
            <td></td>
            <th>材料セット名</th>
            <td><?php echo $this->form->text('MstOpeItemSetHeader.ope_item_set_name',array('class'=>'lbl','readonly'=>'readonly','style'=>'width:600px' , 'id' => 'name' , 'maxlength'=>'100')); ?></td>
            <td></td>
        </tr>
    </table>
</div>
<div align="right" id="pageTop" ></div>
<h2 class="HeaddingSmall">構成品一覧</h2>
<div class="TableScroll2">
    <table class="TableStyle01 table-odd2">
        <tr>
            <th width="20" rowspan="2"></th>
            <th class="col10">商品ID</th>
            <th class="col30">商品名</th>
            <th class="col30">製品番号</th>
            <th class="col30">備考</th>
        </tr>
        <tr>
            <th>包装単位</th>
            <th>規格</th>
            <th>販売元</th>
            <th>数量</th>
        </tr>
    <?php  $i = 0; foreach($result as $item){ ?>
        <tr>
            <td width="20" rowspan="2" class="center"></td>
            <td class="col10"><?php echo h_out($item['MstOpeItemSet']['internal_code'],'center'); ?></td>
            <td class="col30"><?php echo h_out($item['MstOpeItemSet']['item_name']); ?></td>
            <td class="col30"><?php echo h_out($item['MstOpeItemSet']['item_code']); ?></td>
            <td class="col30"><?php echo $this->form->text('MstOpeItemSet.recital.'.$i ,array('value'=>$item['MstOpeItemSet']['recital'],'class'=>'lbl','readonly'=>'readonly','maxlength'=>'200')); ?></td>
        </tr>
        <tr>
            <td><?php echo h_out($item['MstOpeItemSet']['unit_name']) ; ?></td>
            <td><?php echo h_out($item['MstOpeItemSet']['standard']); ?></td>
            <td><?php echo h_out($item['MstOpeItemSet']['dealer_name']); ?></td>
            <td><?php echo $this->form->text('MstOpeItemSet.quantity.'.$i ,array('value'=>$item['MstOpeItemSet']['quantity'],'class'=>'lbl num','readonly'=>'readonly','maxlength'=>'32')); ?></td>
        </tr>
    <?php $i++; } ?>
    </table>
</div>
<div align="right" id="pageDow" ></div>
