<script type="text/javascript">
$(function(){
    $("#report_btn").click(function(){
        $("#report_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/return').submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_search">手術予定一覧</a></li>
        <li class="pankuzu">手術返却登録</li>
        <li>手術返却登録結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>手術返却登録結果</span></h2>
<div class="Mes01">手術返却登録を行いました</div>
<form id="report_form" class="vlidated_form" method="post" >
    <?php echo $this->form->input('TrnOpeHeader.id' , array('type'=>'hidden'))?>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" id="report_btn" class="btn btn10 submit [p2]" />
        </p>
    </div>
</form>
