<script type="text/javascript">
$(document).ready(function(){

    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#setItemForm #searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#setItemForm #cartForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );
    //検索ボタン押下
    $("#btn_Search").click(function(){
        //カート欄にある全IDを取得
        var tmpId = "";
        $("#cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tmpId === '' ){
                    tmpId = $(this).val();
                }else{
                    tmpId +="," +  $(this).val();
                }
            }
        });
        $("#tmpId").val(tmpId);
        $('#itemSelectedTable').empty();
        $('#containTables').empty();
        
        $("#setItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_item_set_cart').submit();
    });

    //選択ボタン押下

    $('#cmdSelect').click(function(){
        var cnt = 0;
        $("#setItemForm #searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().removeClass('odd').appendTo('#itemSelectedTable');
                $('#containTables'+$(this).val()).remove();
                cnt++;
            }
        });
        $("#searchForm input[type=checkbox].checkAllTarget").attr("checked",false);
        $('#cartForm input[type=checkbox].checkAllTarget').attr('checked',true);
        
        var rowcnt = 0;
        $("#searchForm table tbody").each(function(){
            $(this).find('tr').removeClass('odd');
            if (rowcnt%2 !== 0){
                $(this).find('tr').removeClass('odd').addClass('odd');
            }
            rowcnt++;
        });
        rowcnt = 0;
        //$("#itemSelectedTable table tbody").each(function(){
        $("#cartForm table tbody").each(function(){
            $(this).find('tr').removeClass('odd');
            if (rowcnt%2 !== 0){
                $(this).find('tr').removeClass('odd').addClass('odd');
            }
            rowcnt++;
        });
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        //選択チェック
        var cnt = 0;
        var tmpId = '';
        $("#setItemForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
                if(tmpId === ''){
                    tmpId = $(this).val();
                }else{
                    tmpId = tmpId + ','+$(this).val();
                }
            }
        });
        $('#tmpId').val(tmpId);
        $("#setItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_item_set_confirm').submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/ope_item_set_search/">材料セット一覧</a></li>
        <li>材料選択</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>材料選択</span></h2>

<form class="validate_form search_form" id="setItemForm" method="post" action="ope_item_set_cart">
    <?php echo $this->form->input('add_search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('MstOpeItemSetHeader.id',array('type'=>'hidden'));?>
    <?php echo $this->form->input('cart.tmpid',array('type'=>'hidden','id'=>'tmpId'));?>
    <div class="SearchBox">
        <?php if(isset($this->request->data['MstOpeItemSetHeader']['id']) && !empty($this->request->data['MstOpeItemSetHeader']['id'])) { ?>
        <table class="FormStyleTable">
            <tr>
                <th>材料セットコード</th>
                <td><?php echo $this->form->input('MstOpeItemSetHeader.ope_item_set_code',array('class' => 'lbl', 'readonly' => 'readonly'));?></td>
                <td width="20"></td>
                <th>材料セット名</th>
                <td><?php echo $this->form->input('MstOpeItemSetHeader.ope_item_set_name',array('class' => 'lbl','style'=>'width:600px;', 'readonly' => 'readonly'));?></td>
            </tr>
        </table>
        <h2 class="HeaddingSmall">構成品一覧</h2>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th class="col20">商品ID</th>
                    <th class="col30">商品名</th>
                    <th class="col30">製品番号</th>
                    <th class="col20">包装単位</th>
                </tr>
                <tr>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                    <th>備考</th>
                </tr>
            <?php
              $i = 0;
              foreach($items as $i){
              ?>
                <tr>
                    <td class="col20"><?php echo h_out($i['MstOpeItemSet']['internal_code']); ?></td>
                    <td class="col30"><?php echo h_out($i['MstOpeItemSet']['item_name']); ?></td>
                    <td class="col30"><?php echo h_out($i['MstOpeItemSet']['item_code']); ?></td>
                    <td class="col20"><?php echo h_out($i['MstOpeItemSet']['unit_name']); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($i['MstOpeItemSet']['standard']); ?></td>
                    <td><?php echo h_out($i['MstOpeItemSet']['dealer_name']); ?></td>
                    <td><?php echo h_out($i['MstOpeItemSet']['quantity']); ?></td>
                    <td><?php echo h_out($i['MstOpeItemSet']['recital']); ?></td>
                </tr>
            <?php
                $i++;
            } ?>
            </table>
        </div>
        <?php } ?>

<h2 class="HeaddingMid">検索条件</h2>

        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFacilityItem.internal_code',array('class'=>'txt search_internal_code','style'=>'width:120px')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_code',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFacilityItem.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFacilityItem.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('MstFacilityItem.jan_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>

        <div class="ButtonBox">
           <p class="center">
             <input type="button" class="btn btn1" id="btn_Search"/>
           </p>
        </div>
    </div>
    <div align="right" id="pageTop" ></div>

<?php
    if(isset($this->request->data['add_search']['is_search'])){
      if(count($SearchResult) === 0){
?>
      <div class="DisplaySelect">
          <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
          <table class="TableHeaderStyle02">
              <tr>
                  <th rowspan="2" style="width:20px;"><input type="checkbox"/></th>
                  <th class="col10">商品ID</th>
                  <th class="col30">商品名</th>
                  <th class="col30">製品番号</th>
                  <th class="col30">包装単位</th>
              </tr>
              <tr>
                  <th></th>
                  <th>規格</th>
                  <th>販売元</th>
                  <th>JANコード</th>
              </tr>
          </table>
      </div>
      <span>該当するデータはありません。</span>
<?php
      }else{
?>
    <div id="results">
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
            <th rowspan="2" style="width:20px;"><input type="checkbox" class='checkAll_1' /></th>
            <th class="col10">商品ID</th>
            <th class="col30">商品名</th>
            <th class="col30">製品番号</th>
            <th class="col30">包装単位</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>JANコード</th>
        </tr>
      </table>
    </div>

    <div class=" div-even" id="searchForm">
<?php
  $i = 0;
  foreach($SearchResult as $item){
  $class = ($i % 2 == 0)?'':'odd';
  ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$item["MstFacilityItem"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
        <tbody>
          <tr id=<?php echo "itemSelectedRow".$item['MstFacilityItem']['id'];?> >
            <td class="center" rowspan="2" style="width:20px;">
              <?php echo $this->form->checkbox("SelectedID.{$i}", array('class'=>'center checkAllTarget','value'=>$item['MstFacilityItem']['id'] , 'style'=>'width:20px;text-align:center;' , 'hiddenField'=>false) );?>
            </td>
                <td class="col10"><?php echo h_out($item['MstFacilityItem']['internal_code']); ?></td>
                <td class="col30"><?php echo h_out($item['MstFacilityItem']['item_name']); ?></td>
                <td class="col30"><?php echo h_out($item['MstFacilityItem']['item_code']); ?></td>
                <td class="col30"><?php echo h_out($item['MstFacilityItem']['unit_name']); ?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($item['MstFacilityItem']['standard']); ?></td>
                <td><?php echo h_out($item['MstFacilityItem']['dealer_name']); ?></td>
                <td><?php echo h_out($item['MstFacilityItem']['jan_code']); ?></td>
            </tr>
           </tbody>
      </table>
      <?php $i++; } ?>
    </div>
      <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
          <input type="button" value="" class="btn btn4" id="cmdSelect" />
        </p>
      </div>
    </div>
    <?php } ?>

    <?php if(isset($this->request->data['add_search']['is_search'])){?>
    <h2 class="HeaddingSmall">選択商品</h2>
    <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
            <th rowspan="2" style="width:20px;"><input type="checkbox" checked class='checkAll_2' /></th>
            <th class="col10">商品ID</th>
            <th class="col30">商品名</th>
            <th class="col30">製品番号</th>
            <th class="col30">包装単位</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>JANコード</th>
        </tr>
      </table>
    </div>

    <div class=" div-even" broder='0'  id="cartForm" >
      <?php if(isset($CartSearchResult)){ ?>
      
<?php
  $i = 0;
  foreach($CartSearchResult as $item){
  ?>
      <table class="TableStyle02" id="addTable" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <tr>
              <td class="center" rowspan="2" style="width:20px;">
              <?php echo $this->form->checkbox('MstFacilityItem.selectid.'.$i , array('checked' => 1 , 'class'=>'center checkAllTarget' , 'value'=>$item['MstFacilityItem']['id'] , 'hiddenField'=>false) ); ?>
              </td>
              <td class="col10"><?php echo h_out($item['MstFacilityItem']['internal_code']); ?></td>
              <td class="col30"><?php echo h_out($item['MstFacilityItem']['item_name']); ?></td>
              <td class="col30"><?php echo h_out($item['MstFacilityItem']['item_code']); ?></td>
              <td class="col30"><?php echo h_out($item['MstFacilityItem']['unit_name']); ?></td>
          </tr>
          <tr>
              <td></td>
              <td><?php echo h_out($item['MstFacilityItem']['standard']); ?></td>
              <td><?php echo h_out($item['MstFacilityItem']['dealer_name']); ?></td>
              <td><?php echo h_out($item['MstFacilityItem']['jan_code']); ?></td>
          </tr>
      </table>
      <?php
              $i++;
              }
            ?>
        
      <?php }?>
      <div id="itemSelectedTable" border="0" style="margin-top:-1px; margin-bottom:-1px; padding:0px;"></div>
    </div>
    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" class="btn btn3" />
      </p>
    </div>
    <?php }?>
<?php
    }else{
      //検索実行フラグがfalse
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>0)); ?>
      </div>

      <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
            <th rowspan="2" style="width:20px;"><input type="checkbox" /></th>
            <th class="col10">商品ID</th>
            <th class="col30">商品名</th>
            <th class="col30">製品番号</th>
            <th class="col30">包装単位</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>JANコード</th>
        </tr>
      </table>

      </div>
    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" class="btn btn3" />
      </p>
    </div>

      
<?php
    }
    echo $this->form->end();
?>
<div align="right" id="pageDow" ></div>
