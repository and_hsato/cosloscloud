<script type="text/javascript">
$(document).ready(function(){

    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#setMethodForm #searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#setMethodForm #cartForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );
    //検索ボタン押下
    $("#btn_Search").click(function(){
        //カート欄にある全IDを取得
        var tmpId = "";
        $("#cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tmpId === '' ){
                    tmpId = $(this).val();
                }else{
                    tmpId +="," +  $(this).val();
                }
            }
        });
        $("#tmpId").val(tmpId);
        $('#itemSelectedTable').empty();
        $('#containTables').empty();
        
        $("#setMethodForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_method_set_cart').submit();
    });

    //選択ボタン押下

    $('#cmdSelect').click(function(){
        var cnt = 0;
        $("#setMethodForm #searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().removeClass('odd').appendTo('#itemSelectedTable');
                $('#containTables'+$(this).val()).remove();
                cnt++;
            }
        });
        $("#searchForm input[type=checkbox].checkAllTarget").attr("checked",false);
        $('#cartForm input[type=checkbox].checkAllTarget').attr('checked',true);
        
        var rowcnt = 0;
        $("#searchForm table tbody").each(function(){
            $(this).find('tr').removeClass('odd');
            if (rowcnt%2 !== 0){
                $(this).find('tr').removeClass('odd').addClass('odd');
            }
            rowcnt++;
        });
        rowcnt = 0;
        //$("#itemSelectedTable table tbody").each(function(){
        $("#cartForm table tbody").each(function(){
            $(this).find('tr').removeClass('odd');
            if (rowcnt%2 !== 0){
                $(this).find('tr').removeClass('odd').addClass('odd');
            }
            rowcnt++;
        });
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        //選択チェック
        var cnt = 0;
        var tmpId = '';
        $("#setMethodForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
                if(tmpId === ''){
                    tmpId = $(this).val();
                }else{
                    tmpId = tmpId + ','+$(this).val();
                }
            }
        });
        $('#tmpId').val(tmpId);
        $("#setMethodForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_method_set_confirm').submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/ope_method_set_search/">術式一覧</a></li>
        <li>材料セット選択</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>材料セット選択</span></h2>

<form class="validate_form search_form" id="setMethodForm" method="post">
    <?php echo $this->form->input('add_search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('MstOpeMethod.id',array('type'=>'hidden'));?>
    <?php echo $this->form->input('cart.tmpid',array('type'=>'hidden','id'=>'tmpId'));?>
    <div class="SearchBox">
        <?php if(isset($this->request->data['MstOpeMethod']['id']) && !empty($this->request->data['MstOpeMethod']['id'])) { ?>
        <table class="FormStyleTable">
            <tr>
                <th>術式コード</th>
                <td><?php echo $this->form->input('MstOpeMethod.code',array('class' => 'lbl', 'readonly' => 'readonly'));?></td>
                <td width="20"></td>
                <th>術式名</th>
                <td><?php echo $this->form->input('MstOpeMethod.name',array('class' => 'lbl','style'=>'width:600px;', 'readonly' => 'readonly'));?></td>
            </tr>
            <tr>
                <th>コメント</th>
                <td colspan="5">
                    <textarea id="textarea" name="data[MstOpeMethod][comment]" readonly="readonly" cols="120" rows="5" maxlength="200" style="height:auto !important"><?php echo $this->request->data['MstOpeMethod']['comment'] ?></textarea>
                </td>
            </tr>
        </table>
        <h2 class="HeaddingSmall">材料セット一覧</h2>
        <div class="FormStyleTable">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th class="col30">材料セットコード</th>
                    <th class="col70">材料セット名</th>
                </tr>
                <?php foreach($method as $i){ ?>
                <?php if(isset($i['MstOpeItemSet']['code'])){  ?>
                <tr>
                    <td><?php echo h_out($i['MstOpeItemSet']['code']); ?></td>
                    <td><?php echo h_out($i['MstOpeItemSet']['name']); ?></td>
                </tr>
                <?php } ?>
                <?php $i++; } ?>
            </table>
        </div>
        <?php } ?>

<h2 class="HeaddingMid">検索条件</h2>

        <table class="FormStyleTable table-even">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>材料セットコード</th>
                <td><?php echo $this->form->text('MstOpeItemSet.search_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>材料セット名</th>
                <td><?php echo $this->form->text('MstOpeItemSet.search_name',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
            </tr>
        </table>

        <div class="ButtonBox">
           <p class="center">
             <input type="button" class="btn btn1" id="btn_Search"/>
           </p>
        </div>
    </div>
    <div align="right" id="pageTop" ></div>

<?php
    if(isset($this->request->data['add_search']['is_search'])){
      if(count($SearchResult) === 0){
?>
      <div class="DisplaySelect">
          <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
          <table class="TableHeaderStyle02">
              <tr>
                  <th rowspan="2" style="width:20px;"><input type="checkbox"/></th>
                  <th class="col30">材料セットコード</th>
                  <th class="col70">材料セット名</th>
              </tr>
          </table>
      </div>
      <span>該当するデータはありません。</span>
<?php
      }else{
?>
    <div id="results">
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
            <th rowspan="2" style="width:20px;"><input type="checkbox" class='checkAll_1' /></th>
            <th class="col30">材料セットコード</th>
            <th class="col70">材料セット名</th>
        </tr>
      </table>
    </div>

    <div class=" div-even" id="searchForm">
<?php
  $i = 0;
  foreach($SearchResult as $item){
  $class = ($i % 2 == 0)?'':'odd';
  ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$item["MstOpeItemSetHeader"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
        <tbody>
          <tr id=<?php echo "itemSelectedRow".$item['MstOpeItemSetHeader']['id'];?> >
            <td class="center" rowspan="2" style="width:20px;">
              <?php echo $this->form->checkbox("SelectedID.{$i}", array('class'=>'center checkAllTarget','value'=>$item['MstOpeItemSetHeader']['id'] , 'style'=>'width:20px;text-align:center;' , 'hiddenField'=>false) );?>
            </td>
                <td class="col30"><?php echo h_out($item['MstOpeItemSetHeader']['code']); ?></td>
                <td class="col70"><?php echo h_out($item['MstOpeItemSetHeader']['name']); ?></td>
            </tr>
           </tbody>
      </table>
      <?php $i++; } ?>
    </div>
      <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
          <input type="button" value="" class="btn btn4" id="cmdSelect" />
        </p>
      </div>
    </div>
    <?php } ?>

    <?php if(isset($this->request->data['add_search']['is_search'])){?>
    <h2 class="HeaddingSmall">選択商品</h2>
    <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
            <th rowspan="2" style="width:20px;"><input type="checkbox" checked class='checkAll_2' /></th>
            <th class="col30">材料セットコード</th>
            <th class="col70">材料セット名</th>
        </tr>
      </table>
    </div>

    <div class=" div-even" broder='0'  id="cartForm" >
      <?php if(isset($CartSearchResult)){ ?>
      
<?php
  $i = 0;
  foreach($CartSearchResult as $item){
  ?>
      <table class="TableStyle02" id="addTable" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <tr>
              <td class="center" rowspan="2" style="width:20px;">
              <?php echo $this->form->checkbox('MstOpeItemSetHeader.selectid.'.$i , array('checked' => 1 , 'class'=>'center checkAllTarget' , 'value'=>$item['MstOpeItemSetHeader']['id'] , 'hiddenField'=>false) ); ?>
              </td>
              <td class="col30"><?php echo h_out($item['MstOpeItemSetHeader']['code']); ?></td>
              <td class="col70"><?php echo h_out($item['MstOpeItemSetHeader']['name']); ?></td>
          </tr>
      </table>
      <?php
              $i++;
              }
            ?>
        
      <?php }?>
      <div id="itemSelectedTable" border="0" style="margin-top:-1px; margin-bottom:-1px; padding:0px;"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" value="" class="btn btn3" />
      </p>
    </div>
    <?php }?>
<?php
    }else{
      //検索実行フラグがfalse
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>0)); ?>
      </div>

      <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
            <th rowspan="2" style="width:20px;"><input type="checkbox" /></th>
            <th class="col30">材料セットコード</th>
            <th class="col70">材料セット名</th>
        </tr>
      </table>

      </div>
<?php
    }
    echo $this->form->end();
?>
<div align="right" id="pageDow" ></div>
