<script type="text/javascript">
$(function(){
    $(document).ready(function() {
        //確定ボタン押下
        $("#accept_btn").click(function(){
            if($('.chk:checked').length > 0){
                $(window).unbind('beforeunload');
                $("#accept_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_move_result').submit();
            }else{
                alert('明細は1つ以上選択してください。');
                return;
            }
        });
        //チェックボックス一括制御
        $('#all_check').click(function () {
            $('.chk').attr('checked',$(this).attr('checked'));
        });
        //備考一括入力
        $('#recital_input_btn').click( function () {
            $('input.recital').val($('#recital_all_txt').val());
        });
    });
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
  
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_move_read_sticker">オペ倉庫移動_シール読込</a></li>
        <li>オペ倉庫移動確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>オペ倉庫移動確認</span></h2>
<form class="validate_form" id="accept_form" method="post">
    <?php echo $this->form->input('OpeMove.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('OpeMove.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>移動日</th>
                <td><?php echo $this->form->input('OpeMove.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>備考</th>
                <td><?php echo $this->form->input('OpeMove.recital', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?></td>
            </tr>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
        <tr>
            <td>表示件数：<?php echo count($result); ?>件</td>
            <td align="right"><input type="text" id="recital_all_txt" class="txt" style="width: 150px;" maxlength="200" ><input type="button" class="btn btn8 [p2]"  value="" id="recital_input_btn"></td>
        </tr>
    </table>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th rowspan="2" width="20"><?php echo $this->form->input('all_check',array('type'=>'checkbox','id'=>'all_check','checked'=>true)) ?></th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col15">センターシール</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th></th>
                <th>有効期限</th>
                <th>備考</th>
            </tr>
            <?php foreach($result as $i=>$r){ ?>
            <tr>
                <td width="20" rowspan="2" class="center">
                    <?php if($r['TrnSticker']['is_check']){ ?>
                    <?php echo $this->form->checkbox('TrnSticker.id.'.$i , array('value'=>$r['TrnSticker']['id'] , 'class' => 'chk id' , 'checked'=>'checked' , 'hiddenField'=>false) ); ?>
                    <?php } ?>
                </td>
                <td class="col10" align="center"><?php echo h_out($r['TrnSticker']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($r['TrnSticker']['item_name']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['item_code']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnSticker']['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnSticker']['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnSticker']['facility_sticker_no']); ?></td>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($r['TrnSticker']['standard']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['dealer_name']); ?></td>
                <td></td>
                <td><?php echo h_out($r['TrnSticker']['validated_date'],'center'); ?></td>
                <td>
                    <?php if ($r['TrnSticker']['is_check']) { ?>
                    <?php echo $this->form->input('TrnSticker.recital.'.$r['TrnSticker']['id'] , array('type'=>'text','class'=>'txt','value'=>$r['TrnSticker']['alert'])); ?>
                    <?php } else {  ?>
                    <?php echo h_out($r['TrnSticker']['alert']); ?>
                    <?php }  ?>
                </td>
            </tr>
         <?php } ?>
        </table>
    </div>
    <?php if(!isset($header_error)){ ?>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn2 submit" id="accept_btn"/></p>
    </div>
    <?php } ?>
</form>
