<script>
$(function($){
    //検索ボタン押下
    $("#search_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_move_search/').submit();
    });
    //明細表示ボタン押下
    $('#btn_detail').click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $('#search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_move_detail/').submit();
        }else{
            alert('確認を行いたい履歴を選択してください');
            return false;
        }
    });

    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>オペ倉庫移動履歴</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>オペ倉庫移動履歴</span></h2>
<form class="validate_form search_form input_form" method="POST" id="search_form">
    <?php echo $this->form->input('ope_move_search.is_search',array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="60" align="right" />
            </colgroup>
            <tr>
                <th>作業番号</th>
                <td><?php echo($this->form->text('TrnOpeMove.work_no', array('class'=>'txt', 'maxlength'=>'50', 'label'=>''))); ?></td>
                <td></td>
                <th>作業日</th>
                    <td>
                        <?php echo($this->form->text('TrnOpeMove.work_date_from', array('class' => 'date txt validate[optional,custom[date]]', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                        <span>&nbsp;～&nbsp;</span>
                        <?php echo($this->form->text('TrnOpeMove.work_date_to', array('class' => 'date txt validate[optional,custom[date]]', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                    </td>
                <td></td>
            </tr>
             <tr>
                <th>商品ID</th>
                <td><?php echo($this->form->text('TrnOpeMove.internal_code', array('class' => 'txt search_internal_code', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo($this->form->text('TrnOpeMove.item_code', array('class' => 'txt search_upper', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo($this->form->text('TrnOpeMove.lot_no', array('class' => 'txt', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo($this->form->text('TrnOpeMove.item_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo($this->form->text('TrnOpeMove.dealer_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo($this->form->text('TrnOpeMove.standard', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo($this->form->text('TrnOpeMove.facility_sticker_no', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn1" id="search_btn" />
        </p>
    </div>

    <div id="results">
        <h2 class="HeaddingSmall">明細を表示したい行を選択してください。</h2>
        <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th style="width:20px;"><input type="checkbox" class="checkAll"/></th>
                    <th style="width:135px;">作業番号</th>
                    <th style="width:135px;">作業日</th>
                    <th>登録者</th>
                    <th style="width:150px;">登録日時</th>
                    <th>備考</th>
                    <th style="width:50px;">件数</th>
                </tr>
                <?php foreach($result as $c=>$r){?>
                <tr>
                    <td class="center"><?php echo $this->form->checkbox("TrnMove.id.{$c}", array('value'=>$r['TrnMove']['id'],'class' => 'center chk','hiddenField'=>false)); ?></td>
                    <td><?php echo h_out($r['TrnMove']['work_no'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnMove']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnMove']['user_name']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['created'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnMove']['recital']); ?></td>
                    <td><?php echo h_out($r['TrnMove']['detail_count'],'right'); ?></td>
                </tr>
                <?php } ?>
                <?php if(count($result) == 0 && isset($this->request->data['ope_move_search']['is_search'])){ ?>
                <tr><td colspan="7" class="center"><span>該当データが存在しません。</span></td></tr>
                <?php } ?>
            </table>
        </div>
        <?php if(count($result) > 0){ ?>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" value="" class="btn btn7" id="btn_detail" />
            </p>
        </div>
        <?php } ?>
    </div>
</form>
