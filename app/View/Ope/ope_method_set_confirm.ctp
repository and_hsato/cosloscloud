<script type="text/javascript">
function ajaxValidationCallback(status, form, json, options) {
    if (status === true) {
        //チェックOKだったら、送信する
        if( $('input[type=checkbox].chk:checked').length > 0 ) {
             validateDetachSubmit($('#confirmForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_method_set_result' );
        } else {
             alert('材料セットを選択してください');
        }
    }
}

$(document).ready(function(){
    $("#confirmForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });

    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/ope_method_set_search/">術式セット一覧</a></li>
        <li class="pankuzu">材料選択</li>
        <li>術式セット確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>術式セット確認</span></h2>

<form class="search_form" id="confirmForm" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckMethodCode">
    <?php echo $this->form->input('OpeMethod.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('OpeMethod.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s'))); ?>
    <?php echo $this->form->input('MstOpeMethod.id',array('type'=>'hidden','id'=>'method_id')); ?>
    <?php echo $this->form->input('MstOpeMethod.mst_facility_id',array('type'=>'hidden','id'=>'facility_id' , 'value'=> $this->Session->read('Auth.facility_id_selected'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>術式コード</th>
                <td><?php echo $this->form->text('MstOpeMethod.code',array('class'=>'txt r validate[required,ajax[ajaxMethodCode]]','style'=>'width:120px' , 'id' => 'code' , 'maxlength'=>'20')); ?></td>
                <td></td>
                <th>術式名</th>
                <td><?php echo $this->form->text('MstOpeMethod.name',array('class'=>'txt r validate[required]','style'=>'width:600px' , 'id' => 'name' , 'maxlength'=>'100')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>コメント</th>
                <td colspan="5">
                    <textarea id="textarea" name="data[MstOpeMethod][comment]" cols="120" rows="5" maxlength="200" style="height:auto !important"><?php echo $this->request->data['MstOpeMethod']['comment'] ?></textarea>
                </td>
            </tr>
        </table>
    </div>
    <div align="right" id="pageTop" ></div>
        <h2 class="HeaddingSmall">材料セット一覧</h2>

        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th width="20"><input type="checkbox" checked class="checkAll"/></th>
                    <th class="col30">材料セットコード</th>
                    <th class="col70">材料セット名</th>
                </tr>


            <?php
              $i = 0;
              foreach($items as $item){
            ?>
                <tr>
                    <td width="20" class="center"><input type="checkbox" name="data[MstOpeItemSet][id][<?php echo $i ?>]"  class="chk" checked value="<?php echo $item['MstOpeItemSet']['id']; ?>"></td>
                    <td class="col30"><?php echo h_out($item['MstOpeItemSet']['code']); ?></td>
                    <td class="col70"><?php echo h_out($item['MstOpeItemSet']['name']); ?></td>
                </tr>
            <?php $i++; } ?>
            <?php foreach($add_items as $item){ ?>
                <tr>
                    <td width="20" class="center"><input type="checkbox" name="data[MstOpeItemSet][id][<?php echo $i ?>]"  class="chk" checked value="<?php echo $item['MstOpeItemSetHeader']['id']; ?>"></td>
                    <td class="col30"><?php echo h_out($item['MstOpeItemSetHeader']['code']); ?></td>
                    <td class="col70"><?php echo h_out($item['MstOpeItemSetHeader']['name']); ?></td>
                </tr>
            <?php $i++; } ?>
            </table>
        </div>

        <div class="ButtonBox">
            <p class="center">
                <input type="submit" class="btn btn2 [p2]" value="" />
            </p>
        </div>
</form>
<div align="right" id="pageDow" ></div>
