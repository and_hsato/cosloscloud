<script type="text/javascript">
var item_count = <?php echo count($items); ?>;
var webroot  = "<?php echo $this->webroot; ?>";
var opepath = webroot + "/<?php echo $this->name ; ?>";

</script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/ope_edit.js"></script>
<style>
div#search_method_form {
    z-index: 9999;
    background-color: #FF0000;
    position: absolute;
    height: auto;
    width: auto;
    padding: 4px;
    color: #fff;
    left: 400px;
    top: 200px;
    width: 350px;
}

div#search_method_form ul li {
    margin-top: 2px;
    margin-left: 0px;
}

div#search_item_form {
    z-index: 9999;
    background-color: #009900;
    position: absolute;
    height: auto;
    width: auto;
    padding: 4px;
    color: #fff;
    left: 400px;
    top: 200px;
    width: 350px;
}

div#search_item_form ul li {
    margin-top: 2px;
    margin-left: 0px;
}

div#search_item_set_form {
    z-index: 9999;
    background-color: #0000FF;
    position: absolute;
    height: auto;
    width: auto;
    padding: 4px;
    color: #fff;
    left: 400px;
    top: 200px;
    width: 350px;
}

div#search_item_set_form ul li {
    margin-top: 2px;
    margin-left: 0px;
}

</style>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_search">手術予定一覧</a></li>
        <li>手術予定登録</li>
    </ul>
</div>

<div id="search_method_form" class="search_form" >
    <span>術式検索</span>
    <input type="text" name="method_name" id="search_method_name" class="search_canna" />
    <input type="button" id="search_method_btn" name="search_method_btn" value="検索" />
    <img src="<?php echo $this->webroot; ?>img/close2_blk.gif" alt="close" class="floatClose" />
    <div class="scrollable vertical">
        <ul id="search_method_results" class="items"></ul>
    </div>
</div>

<div id="search_item_form" class="search_form" >
  <span>材料検索</span>
    <input type="text" name="item_name" id="search_item_name" class="search_canna" />
    <input type="button" id="search_item_btn" name="search_item_btn" value="検索" />
    <img src="<?php echo $this->webroot; ?>img/close2_blk.gif" alt="close" class="floatClose" />
    <div class="scrollable vertical">
        <ul id="search_item_results" class="items"></ul>
    </div>
</div>

<div id="search_item_set_form" class="search_form" >
  <span>材料セット検索</span>
    <input type="text" name="item_set_name" id="search_item_set_name" class="search_canna" />
    <input type="button" id="search_item_set_btn" name="search_item_set_btn" value="検索" />
    <img src="<?php echo $this->webroot; ?>img/close2_blk.gif" alt="close" class="floatClose" />
    <div class="scrollable vertical">
        <ul id="search_item_set_results" class="items"></ul>
    </div>
</div>


<h2 class="HeaddingLarge"><span>手術予定登録</span></h2>
<form class="validate_form" id="Ope_form"  method="post">
    <?php echo $this->form->input('TrnOpe.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnOpe.id' , array('type'=>'hidden' , 'id'=>'ope_id')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <td><?php if(isset($this->request->data['TrnOpe']['id'])){ ?><input type="button" class="btn btn6 [p2]" id="delete_btn" /><?php } ?></td>
                <td>処置簿編集</td>
                <td><?php if(isset($this->request->data['TrnOpe']['id'])){ echo "編集" ; } else { echo "新規" ;}  ?></td>
                <td><input type="button" value="材料追加" id="call_item_btn" /></td>
                <td><input type="button" value="材料セット追加" id="call_item_set_btn" /></td>
            </tr>
        </table>
        <div id="Input_Area">
        <table class="FormStyleTable">
            <tr>
                <th>予定日</th>
                <td><?php echo($this->form->text('TrnOpe.work_date', array('class' => 'r date validate[required,custom[date]]', 'readonly'=>'readonly','maxlength' => '10' , 'id'=> 'OpeDate'))); ?></td>
                <th>手術室番号</th>
                <td><?php echo $this->form->input('TrnOpe.room', array('options'=>$room, 'empty'=>false , 'class' => 'txt', 'style'=> 'width:60px;' , 'id' => 'OpeRoom')); ?></td>
                <th>手術コード</th>
                <td><?php echo $this->form->input('TrnOpe.ope_code', array('type'=>'text','readonly'=>'readonly' , 'class' => 'txt', 'style'=>'width:80px;', 'id' => 'OpeCode')); ?></td>
            </tr>
        </table>
        <table class="FormStyleTable">
            <tr>
                <td colspan="5">
                    <table>
                        <tr>
                            <th>担当科</th>
                            <td>
                                <?php echo $this->form->input('TrnOpe.opeDepartmentCode',array('options'=>$department,'class'=>'txt','style'=>'width:180px;','id'=>'opeDepartmentCode' , 'empty'=>true)); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>病棟</th>
                            <td>
                                <?php echo $this->form->input('TrnOpe.wardCode',array('options'=>$ward,'class'=>'txt','style'=>'width:180px;','id'=>'wardCode' , 'empty'=>true)); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>患者番号</th>
                            <td><?php echo($this->form->text('TrnOpe.number', array('class' => 'txt', 'maxlength' => '10' , 'id'=>'OpeNumber'))); ?></td>
                        </tr>
                        <tr>
                            <th>患者氏名</th>
                            <td><?php echo($this->form->text('TrnOpe.name', array('class' => 'txt', 'maxlength' => '50','id'=>'OpeName'))); ?></td>
                        </tr>
                        <tr>
                            <th>年齢</th>
                            <td><?php echo $this->form->input('TrnOpe.age', array('type'=>'text', 'class' => 'txt num validate[optional,custom[onlyNumber]]','style'=>'width:50px;', 'maxlength'=>'3','id' => 'OpeAge')); ?></td>
                        </tr>
                        <tr>
                            <th>性別</th>
                            <td><?php echo $this->form->input('TrnOpe.sex', array('options'=>array('1'=>'M','2'=>'F'), 'empty'=>true , 'style'=>'width:50px;', 'class' => 'txt', 'id' => 'OpeSex')); ?></td>
                        </tr>
                    </table>
                </td>
                <td colspan="5" style="width:600px;"> 
                    <table class="FormStyleTable">
                        <tr>
                            <th>術式名</th>
                            <td><?php echo $this->form->input('TrnOpe.method', array('type'=>'text' , 'class'=>'text r validate[required]' , 'style'=> 'width:400px;' ,'readonly'=>true, 'id' => 'OpeMethod')); ?>
                                <?php echo $this->form->input('TrnOpe.methodid', array('type'=>'hidden','id'=>'OpeMethodId')); ?>
                                <input type="button" value="選択" id="call_method_btn" />
                            </td>
                        </tr>
                        <tr>
                            <th>術式セットコード</th>
                            <td><?php echo $this->form->input('TrnOpe.method_set_code', array('type'=>'text' , 'class'=>'txt' , 'style'=> 'width:400px;'  ,'readonly'=>true, 'id' => 'OpeMethodSetCode')); ?>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <div class="TableScroll" id="ItemSet_Area">
                                    <table class="TableStyle01" id="input_item_set_line" border=0 style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
                                        <tr><th>材料セットコード</th><th>材料セット名</th><th class="center">削除</th></tr>
                                        <?php foreach($item_set as $s ){ ?>
                                        <tr>
                                            <td><?php echo h_out($s['Item']['code']); ?></td>
                                            <td><?php echo h_out($s['Item']['name']); ?></td>
                                            <td class="center"><input type="button" value="DEL" id="<?php echo $s['Item']['code']; ?>" class="item_set_del_btn" /></td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>消費施設</th>
                            <td>
                                <?php echo $this->form->input('TrnOpe.facilityName',array('id' => 'facilityName','type'=>'hidden')); ?>
                                <?php echo $this->form->input('TrnOpe.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'txt r validate[required]', 'style'=>'width: 60px;')); ?>
                                <?php echo $this->form->input('TrnOpe.facilityCode',array('options'=>$facility_list,'id' => 'facilityCode', 'class' => 'txt r validate[required]','empty' => true)); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>消費部署</th>
                            <td>
                                <?php echo $this->form->input('TrnOpe.departmentName', array('id' => 'departmentName','type'=>'hidden')); ?>
                                <?php echo $this->form->input('TrnOpe.departmentText', array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'txt r validate[required]', 'style' => 'width: 60px;')); ?>
                                <?php echo $this->form->input('TrnOpe.departmentCode',array('options'=>$department_list,'id' => 'departmentCode', 'class' => 'txt r validate[required]','empty' => true)); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th>術者</th>
                <td><?php echo $this->form->input('TrnOpe.person', array('options'=>$person, 'empty'=>true , 'class' => 'txt', 'id' => 'OpePerson')); ?></td>
                <th>開始時間</th>
                <td><?php echo $this->form->input('TrnOpe.start_time', array('type'=>'text' , 'class' => 'txt', 'id' => 'starttime','maxlength'=>10)); ?></td>
                <th>麻酔種類</th>
                <td><?php echo $this->form->input('TrnOpe.anesthesia', array('options'=>$anesthesia, 'empty'=>false , 'class' => 'txt', 'id' => 'anesthesia')); ?></td>
                <th>体位</th>
                <td><?php echo $this->form->input('TrnOpe.position', array('options'=>$position, 'empty'=>false , 'class' => 'txt', 'id' => 'position')); ?></td>
            </tr>
        </table>
        </div>
        <div class="" id="Item_Area">
            <table class="TableStyle01" id="input_item_line" border=0 style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
                <colgroup>
                    <col width="100px;"/>
                    <col width="100px;"/>
                    <col />
                    <col width="130px;"/>
                    <col width="200px"/>
                    <col width="60px"/>
                </colgroup>
                <tr>
                    <th>材料セット</th>
                    <th>略称</th>
                    <th class="center">材料名 / 規格サイズ / 製品番号</th>
                    <th>数量</th>
                    <th>備考</th>
                    <th>削除</th>
                </tr>
                <?php foreach($items as $l => $i){ ?>
                <tr class="<?php echo $i['Item']['code']; ?>">
                    <td><?php echo h_out($i['Item']['code'],'center'); ?>
                        <?php echo $this->form->input('TrnOpe.item_id.'.$l , array('type'=>'hidden' , 'value'=>$i['Item']['id'])); ?>
                        <?php echo $this->form->input('TrnOpe.item_mst_id.'.$l , array('type'=>'hidden' , 'value'=>$i['Item']['mst_id'])); ?>
                    </td>
                    <td><?php echo h_out($i['Item']['internal_code'],'internal_code'); ?></td>
                    <td><?php echo h_out($i['Item']['item_name'] .' '. $i['Item']['standard'] .' '. $i['Item']['item_code']); ?></td>
                    <td>
                        <input type="button" value="+" class="item_plus_btn" style="width:25px;"/>
                        <input type="button" value="-" class="item_minus_btn" style="width:25px;" />
                        <input type="text" name="data[TrnOpe][quantity][<?php echo $l ; ?>]"  value="<?php echo $i['Item']['quantity']; ?>" class="right quantity" style="width:30px;" readonly="readonly">
                    </td>
                    <td><?php echo h_out($i['Item']['recital']); ?></td>
                    <td class="center"><input type="button" value="DEL" class="item_del_btn" /></td>
                </tr>
                <?php } ?>
            </table>
        </div>

        <div class="ButtonBox">
            <p class="center">
                <input type="button" id="submit_btn" class="btn btn29 submit [p2]" />
            </p>
        </div>
    </div>
</form>
