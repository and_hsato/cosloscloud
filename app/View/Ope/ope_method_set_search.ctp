<script type="text/javascript">
$(function(){
    $("#search_btn").click(function(){
        validateDetachSubmit($('#search_form') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_method_set_search' );
    });
    $("#add_btn").click(function(){
        validateDetachSubmit($('#search_form') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_method_set_cart' );
    });
    $("#edit_btn").click(function(){
        if( $('.chk:checked').length > 0 ) {
            var id = $('input[name="data[MstOpeMethod][id]"]:checked').val();
            $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_method_set_cart/'+id).submit();
        }
    });
    $("#del_btn").click(function(){
        if( $('.chk:checked').length > 0 ) {
            var id = $('input[name="data[MstOpeMethod][id]"]:checked').val();
            if(window.confirm("削除を実行します。よろしいですか？")){
                $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_method_set_del/'+id).submit();
            }
        }
    });
    $("#csv_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/ope_method').submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>術式セット一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>術式セット一覧</span></h2>
<h2 class="HeaddingMid">術式セット検索条件</h2>
<form class="validate_form search_form" id="search_form" method="post">
    <input type="hidden" name="data[search][is_search]"/>
    <div class="SearchBox">
        <div class="ColumnLeft">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>術式コード</th>
                    <td><?php echo $this->form->text('MstOpeMethod.code',array('class' => 'txt','maxlength' => '50','label' => '')) ; ?></td>
                    <td></td>
                    <th>術式名</th>
                    <td><?php echo $this->form->text('MstOpeMethod.name',array('class' => 'txt','maxlength' => '50','label' =>'' ));?></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="search_btn"/>
        <input type="button" class="btn btn11" id="add_btn"/>
        <input type="button" class="btn btn5" id="csv_btn"/>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>

    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25px;" />
                <col width="135px;" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th></th>
                <th>術式コード</th>
                <th>術式名</th>
                <th>コメント</th>
            </tr>
            <?php $i = 0; foreach ($result as $method){ ?>
            <tr>
                <td class="center">
                    <input type="radio" name="data[MstOpeMethod][id]" class="validate[required] chk" id="method<?php echo $method['MstOpeMethod']['id']; ?>" value="<?php echo $method['MstOpeMethod']['id']; ?>" />
                </td>
                <td><?php echo h_out($method['MstOpeMethod']['code'],'center'); ?></td>
                <td><?php echo h_out($method['MstOpeMethod']['name']); ?></td>
                <td><?php echo h_out($method['MstOpeMethod']['comment']); ?></td>
            </tr>
            <?php $i++; } ?>
            <?php if(isset($this->request->data['search']['is_search']) && count($result) == 0 ){ ?>
            <tr><td colspan="4" class="center">該当するデータはありません。</td></tr>
            <?php } ?>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="ButtonBox">
        <?php if( count($result) > 0 ){ ?>
        <input type="button" class="btn btn9" id="edit_btn"/>
        <input type="button" class="btn btn39" id="del_btn"/>
        <?php } ?>
    </div>
</form>