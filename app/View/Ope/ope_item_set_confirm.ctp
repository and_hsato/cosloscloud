<script type="text/javascript">
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //選択チェック
        var cnt = 0;
        var err = 0;
        $("#confirmForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                // 次のテキストボックス要素を検索
                var index = $('input[type=checkbox].checkAllTarget').index(this);
                var quantity = $('input[type=text].quantity:eq(' + index + ')');

                // 入力内容をチェック
                if(!quantity.val().match(/[0-9]+/)){
                    alert('数量を正しく入力してください。');
                    quantity.focus();
                    err = 1;
                    return false;
                }
                cnt++; // チェックの入っている件数をカウントアップ
            }
        });
        if(err === 1){
           return false;
        }
        if(cnt === 0){
            alert('セットに追加する商品を選択してください');
            return false;
        }
        if($('#code').val() == ''){
            alert('セットコードを入力してください');
            $('#code').focus();
            return false;
        }
        if($('#name').val() == ''){
            alert('セット名を入力してください');
            $('#name').focus();
            return false;
        }
        validateDetachSubmit($('#confirmForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_item_set_result' );
    }
}

$(document).ready(function(){
    $("#confirmForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });

    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/ope_item_set_search/">材料セット一覧</a></li>
        <li class="pankuzu">材料選択</li>
        <li>材料セット確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>材料セット確認</span></h2>

<form class="search_form" id="confirmForm" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckItemSetCode">
    <?php echo $this->form->input('OpeItemSet.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('OpeItemSet.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s'))); ?>
    <?php echo $this->form->input('MstOpeItemSetHeader.id',array('type'=>'hidden','id'=>'itemset_id')); ?>
    <?php echo $this->form->input('MstOpeItemSetHeader.mst_facility_id',array('type'=>'hidden','id'=>'facility_id' , 'value'=> $this->Session->read('Auth.facility_id_selected'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>材料セットコード</th>
                <td><?php echo $this->form->text('MstOpeItemSetHeader.ope_item_set_code',array('class'=>'txt r validate[required,ajax[ajaxItemSetCode]]','style'=>'width:120px' , 'id' => 'code' , 'maxlength'=>'20')); ?></td>
                <td></td>
                <th>材料セット名</th>
                <td><?php echo $this->form->text('MstOpeItemSetHeader.ope_item_set_name',array('class'=>'txt r validate[required]','style'=>'width:600px' , 'id' => 'name' , 'maxlength'=>'100')); ?></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div align="right" id="pageTop" ></div>
        <h2 class="HeaddingSmall">構成品一覧</h2>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th width="20" rowspan="2"><input type="checkbox" checked class="checkAll"/></th>
                    <th class="col10">商品ID</th>
                    <th class="col30">商品名</th>
                    <th class="col30">製品番号</th>
                    <th class="col30">備考</th>
                </tr>
                <tr>
                    <th>包装単位</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                </tr>
            <?php
              $i = 0;
              foreach($items as $item){
            ?>
                <tr>
                    <td width="20" rowspan="2" class="center"><input type="checkbox" name="data[MstOpeItemSet][id][<?php echo $i ?>]"  class="chk checkAllTarget" checked value="<?php echo $item['MstOpeItemSet']['item_unit_id']; ?>"></td>
                    <td class="col10"><?php echo h_out($item['MstOpeItemSet']['internal_code'],'center'); ?></td>
                    <td class="col30"><?php echo h_out($item['MstOpeItemSet']['item_name']); ?></td>
                    <td class="col30"><?php echo h_out($item['MstOpeItemSet']['item_code']); ?></td>
                    <td class="col30">
                      <?php echo $this->form->text('MstOpeItemSet.recital.'.$i ,array('value'=>$item['MstOpeItemSet']['recital'],'class'=>'txt','maxlength'=>'200')); ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo h_out($item['MstOpeItemSet']['unit_name']) ; ?></td>
                    <td><?php echo h_out($item['MstOpeItemSet']['standard']); ?></td>
                    <td><?php echo h_out($item['MstOpeItemSet']['dealer_name']); ?></td>
                    <td>
                      <?php echo $this->form->text('MstOpeItemSet.quantity.'.$i ,array('value'=>$item['MstOpeItemSet']['quantity'],'class'=>'txt r quantity num','maxlength'=>'32')); ?>
                    </td>
                </tr>
            <?php $i++; } ?>
            <?php foreach($add_items as $item){ ?>
                <tr>
                    <td width="20" rowspan="2" class="center"><input type="checkbox" name="data[MstOpeItemSet][id][<?php echo $i ?>]"  class="chk checkAllTarget" checked value="<?php echo $item['MstFacilityItem']['id']; ?>"></td>
                    <td class="col10"><?php echo h_out($item['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td class="col30"><?php echo h_out($item['MstFacilityItem']['item_name']); ?></td>
                    <td class="col30"><?php echo h_out($item['MstFacilityItem']['item_code']); ?></td>
                    <td class="col30">
                      <?php echo $this->form->text('MstOpeItemSet.recital.'.$i ,array('class'=>'txt','maxlength'=>'200')); ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo h_out($item['MstFacilityItem']['unit_name']) ; ?></td>
                    <td><?php echo h_out($item['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($item['MstFacilityItem']['dealer_name']); ?></td>
                    <td>
                      <?php echo $this->form->text('MstOpeItemSet.quantity.'.$i ,array('class'=>'txt r quantity num','maxlength'=>'32')); ?>
                    </td>
                </tr>

            <?php $i++; } ?>
            </table>
        </div>

        <div class="ButtonBox">
            <p class="center">
                <input type="submit" class="btn btn2 [p2]" value="" />
            </p>
        </div>
</form>
<div align="right" id="pageDow" ></div>
