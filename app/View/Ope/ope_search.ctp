<script type="text/javascript">
$(function(){
    // 検索ボタン押下
    $("#search_btn").click(function(){
        validateDetachSubmit($('#search_form') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_search/' );
    });

    // 新規ボタン押下
    $("#add_btn").click(function(){
        validateDetachSubmit($('#search_form') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_edit/' );
    });
    // 選択ボタン押下
    $("#edit_btn").click(function(){
        if( $('.chk:checked').length > 0 ) {
            var id = $('input[name="data[TrnOpeHeader][id]"]:checked').val();
            var status = $('input[name="data[TrnOpeHeader][id]"]:checked').parent().parent().find('.status').val();
            switch (status){
                case "<?php echo Configure::read('OpeStatus.init')?>":
                    $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_edit/'+id).submit();
                    break;
                case "<?php echo Configure::read('OpeStatus.shipping'); ?>":
                    alert('払出済みの手術情報です。');
                    break;
                case "<?php echo Configure::read('OpeStatus.return'); ?>":
                    alert('返却中の手術情報です。');
                    break;
                case "<?php echo Configure::read('OpeStatus.complete'); ?>":
                    alert('返却済みの手術情報です。');
                    break;
                case "<?php echo Configure::read('OpeStatus.delete'); ?>":
                    alert('削除済みの手術情報です。');
                    break;
                case "<?php echo Configure::read('OpeStatus.abort'); ?>":
                    alert('中止済みの手術情報です。');
                    break;
            }
  
        }else{
            alert('手術情報が選択されていません。');
        }
    });

    // 払出ボタン押下
    $("#shipping_btn").click(function(){
        if( $('.chk:checked').length > 0 ) {
            var id = $('input[name="data[TrnOpeHeader][id]"]:checked').val();
            var status = $('input[name="data[TrnOpeHeader][id]"]:checked').parent().parent().find('.status').val();
            switch (status){
                case "<?php echo Configure::read('OpeStatus.init')?>":
                    $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_shipping/'+id).submit();
                    break;
                case "<?php echo Configure::read('OpeStatus.shipping'); ?>":
                    alert('払出済みの手術情報です。');
                    break;
                case "<?php echo Configure::read('OpeStatus.return'); ?>":
                    alert('返却中の手術情報です。');
                    break;
                case "<?php echo Configure::read('OpeStatus.complete'); ?>":
                    alert('返却済みの手術情報です。');
                    break;
                case "<?php echo Configure::read('OpeStatus.delete'); ?>":
                    alert('削除済みの手術情報です。');
                    break;
                case "<?php echo Configure::read('OpeStatus.abort'); ?>":
                    alert('中止済みの手術情報です。');
                    break;
            }
        }else{
            alert('手術情報が選択されていません。');
        }
    });
  
    // 返却ボタン押下
    $("#return_btn").click(function(){
        if( $('.chk:checked').length > 0 ) {
            var id = $('input[name="data[TrnOpeHeader][id]"]:checked').val();
            var status = $('input[name="data[TrnOpeHeader][id]"]:checked').parent().parent().find('.status').val();
            switch (status){
                case "<?php echo Configure::read('OpeStatus.init'); ?>":
                    alert('未払出の手術情報です。');
                    break;
                case "<?php echo Configure::read('OpeStatus.shipping'); ?>":
                    $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_return/'+id).submit();
                    break;
                case "<?php echo Configure::read('OpeStatus.return'); ?>":
                    $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_return/'+id).submit();
                    break;
                case "<?php echo Configure::read('OpeStatus.complete'); ?>":
                    alert('返却済みの手術情報です。');
                    break;
                case "<?php echo Configure::read('OpeStatus.delete'); ?>":
                    alert('削除済みの手術情報です。');
                    break;
                case "<?php echo Configure::read('OpeStatus.abort'); ?>":
                    alert('中止済みの手術情報です。');
                    break;
            }
        }else{
            alert('手術情報が選択されていません。');
        }
    });

  
    $("#report_btn").click(function(){
        if( $('.chk:checked').length > 0 ) {
            var status = $('input[name="data[TrnOpeHeader][id]"]:checked').parent().parent().find('.status').val();
            var type = "";
            switch (status){
                case "<?php echo Configure::read('OpeStatus.init'); ?>":
                    type = "picking";
                    break;
                case "<?php echo Configure::read('OpeStatus.shipping'); ?>":
                    type = "shipping";
                    break;
                case "<?php echo Configure::read('OpeStatus.return'); ?>":
                    type = "return";
                    break;
                case "<?php echo Configure::read('OpeStatus.complete'); ?>":
                    type = "return";
                    break;
                case "<?php echo Configure::read('OpeStatus.delete'); ?>":
                    type = "picking";
                    break;
                case "<?php echo Configure::read('OpeStatus.abort'); ?>":
                    type = "picking";
                    break;
            }
            $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/' + type ).submit();
        }else{
            alert('手術情報が選択されていません。');
        }
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>手術予定一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>手術予定一覧</span></h2>
<form class="validate_form search_form" id="search_form" method="post">
    <?php echo $this->form->input('search.is_search' , array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <div class="ColumnLeft">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>手術コード</th>
                    <td><?php echo $this->form->input('TrnOpeHeader.search_code' ,array('type'=>'text' , 'class' => 'txt','maxlength' => '10' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th>手術日</th>
                    <td><?php echo $this->form->input('TrnOpeHeader.search_date',array('type'=>'text','class' => 'date','maxlength' => '10','label' =>'' ));?></td>
                    <td></td>
                    <th>術式名</th>
                    <td><?php echo $this->form->input('TrnOpeHeader.search_method_name' ,array('type'=>'text' , 'class' => 'txt' ,'label' => '')) ; ?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>担当科</th>
                    <td><?php echo $this->form->input('TrnOpeHeader.search_department',array('options'=>$department,'class' => 'txt','empty' => true) );?></td>
                    <td></td>
                    <th>病棟</th>
                    <td><?php echo $this->form->input('TrnOpeHeader.search_ward',array('options'=>$ward ,'class' =>'txt','empty' => true)); ?></td>
                    <td></td>
                    <th>状態</th>
                    <td><?php echo $this->form->input('TrnOpeHeader.search_status',array('options'=>$status,'class' => 'txt','empty' => true) );?></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="search_btn" />
        <input type="button" class="btn btn11 [p2]" id="add_btn" />
    </div>

    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <?php echo $this->element('limit_combobox',array('result'=>count($result))) ; ?>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25" />
                <col width="90px;"/>
                <col width="80px;"/>
                <col />
                <col width="90px;"/>
                <col width="120px;" />
                <col width="100px;"/>
                <col width="150px;"/>
                <col width="60px;"/>
            </colgroup>
            <tr>
                <th></th>
                <th>手術日</th>
                <th>手術コード</th>
                <th>術式名</th>
                <th>担当科</th>
                <th>病棟</th>
                <th>状態</th>
                <th>登録日時</th>
                <th>登録者</th>
            </tr>
            <?php $i = 0; foreach ($result as $r){ ?>
            <tr>
                <td class="center">
                    <input type="radio" name="data[TrnOpeHeader][id]" style="width:25px;" class="validate[required] chk center" id="id_<?php echo $r['TrnOpeHeader']['id']?>" value="<?php echo $r['TrnOpeHeader']['id']; ?>" />
                    <?php echo $this->form->input('TrnOpeHeader.status.'.$i, array('type'=>'hidden' ,'class'=>'status', 'value'=>$r['TrnOpeHeader']['status'])); ?>
                </td>
                <td><?php echo h_out($r['TrnOpeHeader']['work_date'],'center'); ?></td>
                <td><?php echo h_out($r['TrnOpeHeader']['work_no'],'center'); ?></td>
                <td><?php echo h_out($r['TrnOpeHeader']['method_name']); ?></td>
                <td><?php echo h_out($r['TrnOpeHeader']['department_name']); ?></td>
                <td><?php echo h_out($r['TrnOpeHeader']['ward_name']); ?></td>
                <td><?php echo h_out($r['TrnOpeHeader']['status_name'],'center'); ?></td>
                <td><?php echo h_out($r['TrnOpeHeader']['created'],'center'); ?></td>
                <td><?php echo h_out($r['TrnOpeHeader']['user_name']); ?></td>
            </tr>
            <?php $i++; } ?>
            <?php if(isset($this->request->data['search']['is_search']) && count($result) == 0){ ?>
            <tr><td colspan="9" class="center">該当するデータはありません。</td></tr>
            <?php } ?>
        </table>
    </div>

    <?php if (count($result) != 0){ ?>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="ButtonBox">
        <input type="button" class="btn btn4" id="edit_btn"/>
        <input type="button" id="shipping_btn" class="btn btn115 submit [p2]"/>
        <input type="button" id="return_btn" class="btn btn116 submit [p2]"/>
        <input type="button" id="report_btn" class="btn btn10 submit [p2]" />
    </div>
    <?php } ?>
</form>