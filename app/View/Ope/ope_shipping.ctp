<script type="text/javascript">
$(function(){
    $(document).ready(function() {
        //確定ボタン押下
        $("#accept_btn").click(function(){
            if($('.chk:checked').length > 0){
                $(window).unbind('beforeunload');
                if(window.confirm("払出を確定します。よろしいですか？")){
                    $("#ope_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_shipping_result').submit();
                }
            }else{
                alert('明細は1つ以上選択してください。');
                return;
            }
        });
    });
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
    $('.checkAll_1').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
        trColorSet();
    });

    // マウスオーバーで色変え
    $(".TableStyle01 tr").hover(
        function(){
            $(this).addClass('tr-hover');
        },
        function(){
            $(this).removeClass('tr-hover');
        }
    );
  
    // 行クリックで、チェックボックス制御
    $(".TableStyle01 tr").click(function(event){
        tag = $(event.target).attr("tagName");
        if (tag === "INPUT") {
            trColorSet();
            return;
        }
        $(this).find('input[type=checkbox]:eq(0)').attr('checked' , !$(this).find('input[type=checkbox]:eq(0)').attr('checked'));
        trColorSet();
    });
  
    $(window).load(function () {
        // 読み込み終わったらTR要素に色づけする
        trColorSet();
    });
});
  
    function trColorSet(){
        $('input[type=checkbox].chk').each(function(i){
            if(this.checked){
                $(this).closest("tr").addClass('tr-selected');
            } else {
                $(this).closest("tr").removeClass('tr-selected');
            }
        })
    }
  
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_search">手術予定一覧</a></li>
        <li>手術払出登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>手術払出登録</span></h2>
<form class="validate_form" id="ope_form" method="post">
    <?php echo $this->form->input('OpeShipping.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('OpeShipping.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s'))); ?>
    <?php echo $this->form->input('TrnOpeHeader.id', array('type'=>'hidden' , 'value'=>$id)); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>予定日</th>
                <td><?php echo $this->form->input('work_date' , array('type'=>'text', 'value'=> $result[0]['work_date'], 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>手術コード</th>
                <td><?php echo $this->form->input('work_no' , array('type'=>'text' , 'value'=>$result[0]['code'] , 'class'=>'lbl' , 'readonly'=>true));?></td>
            </tr>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
        <tr>
            <td>表示件数：<?php echo count($result); ?>件</td>
        </tr>
    </table>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <tr>
                <th width="20"><?php echo $this->form->input('all_check',array('type'=>'checkbox','class'=>'checkAll_1' ,'checked'=>true)) ?></th>
                <th class="col10">材料セットコード</th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>規格</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col15">バーコード</th>
            </tr>
            <?php foreach($result as $i=>$r){ ?>
            <tr>
                <td class="center">
                    <?php if(!empty($r['trn_sticker_id'])){ ?>
                    <?php echo $this->form->checkbox('ope.id.'.$i , array('value'=>$r['ope_item_id'] . "_" . $r['trn_sticker_id'], 'class' => 'chk' , 'checked'=>'checked' , 'hiddenField'=>false) ); ?>
                    <?php echo $this->form->input('sticker.modified.'.$r['trn_sticker_id'] , array('type'=>'hidden','value'=>$r['modified']) ); ?>
                    <?php } ?>
                </td>
                <td><?php echo h_out($r['set_code'],'center'); ?></td>
                <td><?php echo h_out($r['internal_code'],'center'); ?></td>
                <td><?php echo h_out($r['item_name']); ?></td>
                <td><?php echo h_out($r['standard']); ?></td>
                <td><?php echo h_out($r['item_code']); ?></td>
                <td><?php echo h_out($r['unit_name']); ?></td>
                <td><?php echo h_out($r['facility_sticker_no']); ?></td>
            </tr>
         <?php } ?>
        </table>
    </div>
    <?php if(!isset($header_error)){ ?>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn2 submit" id="accept_btn"/></p>
    </div>
    <?php } ?>
</form>
