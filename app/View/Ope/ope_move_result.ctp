<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_move_read_sticker">オペ倉庫移動_シール読込</a></li>
        <li class="pankuzu">オペ倉庫移動確認</li>
        <li>オペ倉庫移動結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>オペ倉庫移動結果</span></h2>
<div class="Mes01">在庫移動を行いました</div>

<form class="validate_form" id="result_form" method="post">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>移動日</th>
                <td><?php echo $this->form->input('OpeMove.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>備考</th>
                <td><?php echo $this->form->input('OpeMove.recital', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?></td>
            </tr>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
        <tr>
            <td>表示件数：<?php echo count($result); ?>件</td>
            <td align="right"></td>
        </tr>
    </table>
    <br>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th rowspan="2" width="20"></th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col15">センターシール</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th></th>
                <th>有効期限</th>
                <th>備考</th>
            </tr>
            <?php foreach($result as $i=>$r){ ?>
            <tr>
                <td width="20" rowspan="2" class="center"></td>
                <td class="col10" align="center"><?php echo h_out($r['TrnSticker']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($r['TrnSticker']['item_name']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['item_code']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnSticker']['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnSticker']['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnSticker']['facility_sticker_no']); ?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($r['TrnSticker']['standard']); ?></td>
                <td><?php echo h_out($r['TrnSticker']['dealer_name']); ?></td>
                <td></td>
                <td><?php echo h_out($r['TrnSticker']['validated_date'],'center'); ?></td>
                <td>
                    <?php echo $this->form->input('TrnSticker.recital.'.$r['TrnSticker']['id'] , array('type'=>'text','class'=>'lbl','readonly'=>'readonly')); ?>
                </td>
            </tr>
         <?php } ?>
        </table>
    </div>
</form>
