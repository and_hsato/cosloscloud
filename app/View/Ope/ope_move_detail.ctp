<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>ope_move_search">オペ倉庫移動履歴</a></li>
        <li>オペ倉庫移動履歴明細</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>オペ倉庫移動履歴明細</span></h2>
<form id="detail_form" method="POST">
    <div id="results">
        <div class="TableScroll">
            <table class="TableStyle01">
                <tr>
                    <th width="145">作業番号<br/>作業日</th>
                    <th width="145">商品ID<br />商品名</th>
                    <th width="145">規格<br />製品番号</th>
                    <th width="145">新シール</th>
                    <th class="col10">包装単位</th>
                    <th width="145">旧シール</th>
                    <th class="col10">包装単位</th>
                </tr>
                <?php $i=0;foreach($result as $k => $v){ ?>
                <tr class="<?php echo($i%2===0?'':'odd'); ?> center">
                    <td rowspan="<?php echo count($v['new'])?>"><?php echo $v[0]['work_no']; ?><br /><?php echo $v[0]['work_date']; ?></th>
                    <td rowspan="<?php echo count($v['new'])?>"><?php echo $v[0]['internal_code']; ?><br /><?php echo $v[0]['item_name']; ?></th>
                    <td rowspan="<?php echo count($v['new'])?>"><?php echo $v[0]['standard']; ?><br /><?php echo $v[0]['item_code']; ?></th>
                    <?php foreach($v['new'] as $l => $n){ ?>
                    <?php if($l>0){ ?>
                    <tr class="<?php echo($i%2===0?'':'odd'); ?> center">
                    <?php } ?>
                    <td><?php echo $n[0]['facility_sticker_no']; ?></td>
                    <td><?php echo $n[0]['unit_name']; ?></td>
                    <?php if($l>0){ ?>
                    </tr>
                    <?php }else{ ?>
                    <td rowspan="<?php echo count($v['new'])?>"><?php echo $v[0]['facility_sticker_no']; ?></th>
                    <td rowspan="<?php echo count($v['new'])?>"><?php echo $v[0]['unit_name']; ?></th>
                </tr>
                <?php } ?>
                    <?php } ?>
                <?php $i++; }; ?>
            </table>
        </div>
    </div>
</form>
