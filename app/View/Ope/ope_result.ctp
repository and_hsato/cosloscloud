<script type="text/javascript">
$(document).ready(function(){
    $('.date').datepicker('destroy');
    $('select , input[type=text]').attr("readonly","readonly").removeClass('r txt date' ).addClass("lbl");
    $('input[type=checkbox] , select').each(function(){
        $(this).after('<input type=hidden name=' + $(this).attr('name') + ' value=' + $(this).val() + ' >');
        $(this).attr('disabled' , 'disabled');
    });
    $("#report_btn").click(function(){
        $("#report_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/picking').submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_search">手術予定一覧</a></li>
        <li class="pankuzu">手術予定登録</li>
        <li>手術予定登録結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>手術予定登録結果</span></h2>
<div class="Mes01">手術予定登録を行いました</div>
<form id="report_form" class="vlidated_form" method="post" >
    <?php echo $this->form->input('TrnOpeHeader.id' , array('type'=>'hidden'))?>

<form class="validate_form" id="Ope_form"  method="post">
    <?php echo $this->form->input('TrnOpe.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnOpe.id' , array('type'=>'hidden' , 'id'=>'ope_id')); ?>
    <div class="SearchBox">
        <div id="Input_Area">
        <table class="FormStyleTable">
            <tr>
                <th>予定日</th>
                <td><?php echo($this->form->text('TrnOpe.work_date', array('class' => 'lbl', 'readonly'=>'readonly','maxlength' => '10' , 'id'=> 'OpeDate'))); ?></td>
                <th>手術室番号</th>
                <td><?php echo $this->form->input('TrnOpe.room', array('options'=>$room, 'empty'=>false , 'class' => 'lbl', 'style'=> 'width:60px;' , 'id' => 'OpeRoom')); ?></td>
                <th>手術コード</th>
                <td><?php echo $this->form->input('TrnOpe.ope_code', array('type'=>'text','readonly'=>'readonly' , 'class' => 'lbl', 'style'=>'width:80px;', 'id' => 'OpeCode')); ?></td>
            </tr>
        </table>
        <table class="FormStyleTable">
            <tr>
                <td colspan="5">
                    <table>
                        <tr>
                            <th>担当科</th>
                            <td>
                                <?php echo $this->form->input('TrnOpe.opeDepartmentCode',array('options'=>$department,'class'=>'lbl','style'=>'width:180px;','id'=>'opeDepartmentCode' , 'empty'=>true)); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>病棟</th>
                            <td>
                                <?php echo $this->form->input('TrnOpe.wardCode',array('options'=>$ward,'class'=>'lbl','style'=>'width:180px;','id'=>'wardCode' , 'empty'=>true)); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>患者番号</th>
                            <td><?php echo($this->form->text('TrnOpe.number', array('class' => 'lbl', 'maxlength' => '10' , 'id'=>'OpeNumber'))); ?></td>
                        </tr>
                        <tr>
                            <th>患者氏名</th>
                            <td><?php echo($this->form->text('TrnOpe.name', array('class' => 'lbl', 'maxlength' => '50','id'=>'OpeName'))); ?></td>
                        </tr>
                        <tr>
                            <th>年齢</th>
                            <td><?php echo $this->form->input('TrnOpe.age', array('type'=>'lbl', 'class' => 'lbl','style'=>'width:50px;', 'maxlength'=>'3','id' => 'OpeAge')); ?></td>
                        </tr>
                        <tr>
                            <th>性別</th>
                            <td><?php echo $this->form->input('TrnOpe.sex', array('options'=>array('1'=>'M','2'=>'F'), 'empty'=>true , 'style'=>'width:50px;', 'class' => 'lbl', 'id' => 'OpeSex')); ?></td>
                        </tr>
                    </table>
                </td>
                <td colspan="5" style="width:600px;"> 
                    <table class="FormStyleTable">
                        <tr>
                            <th>術式名</th>
                            <td><?php echo $this->form->input('TrnOpe.method', array('type'=>'text' , 'class'=>'lbl' , 'style'=> 'width:400px;' ,'readonly'=>true, 'id' => 'OpeMethod')); ?>
                                <?php echo $this->form->input('TrnOpe.methodid', array('type'=>'hidden','id'=>'OpeMethodId')); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>術式セットコード</th>
                            <td><?php echo $this->form->input('TrnOpe.method_set_code', array('type'=>'text' , 'class'=>'lbl' , 'style'=> 'width:400px;'  ,'readonly'=>true, 'id' => 'OpeMethodSetCode')); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>消費施設</th>
                            <td>
                                <?php echo $this->form->input('TrnOpe.facilityCode',array('options'=>$facility_list,'id' => '', 'class' => 'lbl','empty' => true)); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>消費部署</th>
                            <td>
                                <?php echo $this->form->input('TrnOpe.departmentCode',array('options'=>$department_list,'id' => '', 'class' => 'lbl','empty' => true)); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th>術者</th>
                <td><?php echo $this->form->input('TrnOpe.person', array('options'=>$person, 'empty'=>true , 'class' => 'lbl', 'id' => 'OpePerson')); ?></td>
                <th>開始時間</th>
                <td><?php echo $this->form->input('TrnOpe.start_time', array('type'=>'text' , 'class' => 'lbl', 'id' => 'starttime','maxlength'=>10)); ?></td>
                <th>麻酔種類</th>
                <td><?php echo $this->form->input('TrnOpe.anesthesia', array('options'=>$anesthesia, 'empty'=>false , 'class' => 'lbl', 'id' => 'anesthesia')); ?></td>
                <th>体位</th>
                <td><?php echo $this->form->input('TrnOpe.position', array('options'=>$position, 'empty'=>false , 'class' => 'lbl', 'id' => 'position')); ?></td>
            </tr>
        </table>
        </div>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" id="report_btn" class="btn btn10 submit [p2]" />
        </p>
    </div>
</form>
