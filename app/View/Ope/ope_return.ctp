<script type="text/javascript">
$(function(){
    $(document).ready(function() {
        //確定ボタン押下
        $("#accept_btn").click(function(){
            $(window).unbind('beforeunload');
            if(window.confirm("返却を確定します。よろしいですか？")){
                $("#ope_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_return_result').submit();
            }
        });

        //保存ボタン押下
        $("#save_btn").click(function(){
            $(window).unbind('beforeunload');
            $("#ope_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_return_result/save').submit();
        });

        //中止ボタン押下
        $("#abort_btn").click(function(){
            $(window).unbind('beforeunload');
            if(window.confirm("中止で登録を確定します。よろしいですか？")){
                $("#ope_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_return_result/abort').submit();
            }
        });
  
    });
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
    $('.checkAll_1').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
        trColorSet();
        setReturnCount();
    });

    // マウスオーバーで色変え
    $(".TableStyle01 tr").hover(
        function(){
            $(this).addClass('tr-hover');
        },
        function(){
            $(this).removeClass('tr-hover');
        }
    );
  
    // 行クリックで、チェックボックス制御
    $(".TableStyle01 tr").click(function(event){
        tag = $(event.target).attr("tagName");
        if (tag === "INPUT") {
            trColorSet();
            setReturnCount();
            return;
        }
        $(this).find('input[type=checkbox]:eq(0)').attr('checked' , !$(this).find('input[type=checkbox]:eq(0)').attr('checked'));
        trColorSet();
        setReturnCount();
    });
  
    $(window).load(function () {
        // 読み込み終わったらTR要素に色づけする
        trColorSet();
        setReturnCount();
    });
});

    function trColorSet(){
        $('input[type=checkbox].chk').each(function(i){
            if(this.checked){
                $(this).closest("tr").addClass('tr-selected');
            } else {
                $(this).closest("tr").removeClass('tr-selected');
            }
        })
    }
  
    //返却件数を設定
    function setReturnCount(){
        $('#return_count').text('返却件数：' + $('.TableScroll:last input[type=checkbox].chk:checked').length + '件');
    }
  
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/ope_search">手術予定一覧</a></li>
        <li>手術返却登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>手術返却登録</span></h2>
<form class="validate_form" id="ope_form" method="post">
    <?php echo $this->form->input('OpeReturn.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('OpeReturn.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s'))); ?>
    <?php echo $this->form->input('TrnOpeHeader.id', array('type'=>'hidden' , 'value'=>$id)); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>手術コード</th>
                <td><?php echo $this->form->input('work_no' , array('type'=>'text' , 'value'=>$result[0][0]['work_no'] , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>診療科名</th>
                <td><?php echo $this->form->input('ope_department_name' , array('type'=>'text' , 'value'=>$result[0][0]['ope_department_name'] , 'class'=>'lbl' , 'readonly'=>true));?></td>
            </tr>
            <tr>
                <th>予定日</th>
                <td><?php echo $this->form->input('work_date' , array('type'=>'text', 'value'=> $result[0][0]['work_date'], 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>開始時間</th>
                <td><?php echo $this->form->input('start_time' , array('type'=>'text' , 'value'=>$result[0][0]['start_time'] , 'class'=>'lbl' , 'readonly'=>true));?></td>
            </tr>
            <tr>
                <th>手術名</th>
                <td colspan="4"><?php echo $this->form->input('ope_method_name' , array('type'=>'text', 'value'=> $result[0][0]['ope_method_name'], 'class'=>'lbl' ,'style'=>'width:360px;', 'readonly'=>true));?></td>
            </tr>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
        <tr>
            <td>表示件数：<?php echo count($result); ?>件<div id="return_count">返却件数 : 0 件</div></td>
        </tr>
    </table>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <tr>
                <th width="20"><?php echo $this->form->input('all_check',array('type'=>'checkbox','class'=>'checkAll_1' ,'checked'=>false)) ?></th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>規格</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col15">バーコード</th>
            </tr>
            <?php foreach($result as $i=>$r){ ?>
            <tr>
                <td class="center">
                    <?php echo $this->form->checkbox('ope.id.'.$i , array('value'=>$r[0]['id'], 'class' => 'chk' , 'checked'=>(!empty($r[0]['trn_ope_return_id'])) , 'hiddenField'=>false) ); ?>
                    <?php echo $this->form->input('sticker.id.'.$i , array('type'=>'hidden' , 'value'=>$r[0]['id']) ); ?>
                    <?php echo $this->form->input('TrnOpeItem.id.'.$i , array('type'=>'hidden' , 'value'=>$r[0]['trn_ope_item_id']) ); ?>
                </td>
                <td align="center"><?php echo h_out($r[0]['internal_code'],'center'); ?></td>
                <td><?php echo h_out($r[0]['item_name']); ?></td>
                <td><?php echo h_out($r[0]['standard']); ?></td>
                <td><?php echo h_out($r[0]['item_code']); ?></td>
                <td><?php echo h_out($r[0]['unit_name']); ?></td>
                <td><?php echo h_out($r[0]['facility_sticker_no']); ?></td>
            </tr>
         <?php } ?>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn2 submit" id="accept_btn"/>
            <input type="button" class="btn btn118 submit" id="save_btn"/>
            <input type="button" class="btn btn117 submit" id="abort_btn"/>
        </p>
    </div>
</form>
