<script type="text/javascript">
$(document).ready(function(){
    //シール読込ボタン押下
    $('#btn_reader').click(function(){
        if( $('input.chk:checked').length > 0 ) {
            $("#adjustmentName").val($('input[name="data[setting][adjustment_type]"]:checked').parent().text());
            $("#settingForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/adjustment_reader').submit();
        } else {
            alert('調整種別を選択してください。');
        }
    });

    //在庫選択ボタン押下
    $('#btn_search').click(function(){
        if( $('input.chk:checked').length > 0 ) {
            $("#adjustmentName").val($('input[name="data[setting][adjustment_type]"]:checked').parent().text());
            $("#settingForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/adjustment_search').submit();
        } else {
            alert('調整種別を選択してください。');
        }
    });
});

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>在庫調整</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>在庫調整</span></h2>
<?php echo $this->form->create('Expiration',array('type'=>'post','action'=>'','id'=>'settingForm','class'=>'validate_form input_form')); ?>
    <?php echo $this->form->hidden('Search.BarCode'); ?>
    <?php echo $this->form->hidden('Search.Url',array('value'=>'return2storages_reader')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>調整日</th>
                <td>
                    <?php echo $this->form->text('setting.date',array('class'=>'r date','style'=>'width:80px','maxlength'=>'10','id'=>'datepicker1'));?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('setting.classes',array('options'=>$classes,'class'=>'txt','style'=>'width:100px;' ,'empty'=>true,'id'=>'classId')); ?>
                    <?php echo $this->form->input('setting.classes_name',array('type'=>'hidden','id'=>'className'));?>
                </td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('setting.facility_code',array('class'=>'r validate[required] txt','style'=>'width:60px','id'=>'facilityText'));?>
                    <?php echo $this->form->input('setting.facilities',array('options'=>$facilities,'class'=>'r validate[required] txt','style'=>'width:150px;','empty'=>true, 'id'=>'facilityCode')); ?>
                    <?php echo $this->form->input('setting.facility_name',array('type'=>'hidden' , 'id'=>'facilityName'));?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->text('setting.remarks',array('class'=>'txt','style'=>'width:150px',"amxlength"=>200));?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('setting.department_code',array('class'=>'r validate[required] txt','style'=>'width:60px','id'=>'departmentText'));?>
                    <?php echo $this->form->input('setting.departments',array('options'=>array(),'class'=>'r validate[required] txt','style'=>'width:150px;','empty'=>true,'id'=>'departmentCode')); ?>
                    <?php echo $this->form->input('setting.department_name',array('type'=>'hidden' , 'id'=>'departmentName'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>調整種別</th>
                <td colspan="4">
                <?php foreach(Configure::read('Expiration') as $v=>$name ) { ?>
                    <label><input type="radio" name="data[setting][adjustment_type]" class="chk" value="<?php echo $v?>"><?php echo $name; ?></label>
                <?php } ?>
                    <?php echo $this->form->input('setting.adjustment_name',array('type'=>'hidden' , 'id'=>'adjustmentName'))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="ButtonBox">
        <p class="center">
            <input type="button" id="btn_reader" class="btn btn17"/>
            <input type="button" id="btn_search" class="btn btn15"/>
        </p>
    </div>
<?php echo $this->form->end(); ?>
