<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/adjustment">在庫調整</a></li>
        <li class="pankuzu"><?php echo $this->request->data['Search']['back'];?></li>
        <li class="pankuzu">在庫調整確認</li>
        <li>在庫調整結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>在庫調整結果</span></h2>
<div class="Mes01">在庫調整を行いました</div>
<?php echo $this->form->create('Expiration',array('type'=>'post','action'=>'','id'=>'registForm','class'=>'validate_form')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>調整番号</th>
                <td>
                    <?php echo $this->form->text('regist.work_no',array('class' => 'lbl' ,'style'=>'width:150px' ,'readonly'=>'readonly')); ?>
                </td>
                <td width="20"></td>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>調整日</th>
                <td>
                    <?php echo $this->form->text('setting.date',array('class' => 'lbl','style'=>'width:150px','readonly'=>'readonly')); ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->text('setting.classes_name', array('class' => 'lbl' ,'style'=>'width:150px' ,'readonly'=>'readonly')); ?>
                    <?php echo $this->form->hidden('setting.classes'); ?>
                </td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->text('setting.facility_name', array('class' => 'lbl','style'=>'width:150px;','readonly'=>'readonly')); ?>
                    <?php echo $this->form->hidden('setting.facilities'); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->text('setting.remarks', array('class' => 'lbl' ,'style'=>'width:150px','readonly'=>'readonly')); ?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->text('setting.department_name', array('class' => 'lbl' ,'style'=>'width:150px;' ,'readonly'=>'readonly')); ?>
                    <?php echo $this->form->hidden('setting.departments'); ?>
                </td>
                <td></td>
                <th>調整種別</th>
                <td>
                    <?php echo $this->form->text('setting.adjustment_name',  array('class' => 'lbl'  ,'style'=>'width:150px;' ,'readonly'=>'readonly'));  ?>
                    <?php echo $this->form->hidden('setting.adjustment_type'); ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="SearchBox">
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th rowspan="2" width="20"></th>
                    <th class="col10">商品ID</th>
                    <th class="col15">商品名</th>
                    <th class="col15">製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col10">ロット番号</th>
                    <th class="col15">部署シール</th>
                    <th class="col10">在庫数</th>
                    <th class="col15">作業区分</th>
                </tr>
                <tr>
                    <th>状態</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>評価単価</th>
                    <th>有効期限</th>
                    <th>センターシール</th>
                    <th>調整数</th>
                    <th>備考</th>
                </tr>
            <?php foreach($result as $r){?>
            <tr>
                <td rowspan="2" width="20"></td>
                <td class="col10"><?php echo h_out($r[0]['internal_code'],'center'); ?></td>
                <td class="col15"><?php echo h_out($r[0]['item_name']); ?></td>
                <td class="col15"><?php echo h_out($r[0]['item_code']); ?></td>
                <td class="col10"><?php echo h_out($r[0]['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($r[0]['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($r[0]['hospital_sticker_no']); ?></td>
                <td class="col10"><?php echo h_out($r[0]['quantity'],'right'); ?></td>
                <td class="col15">
                    <?php echo $this->form->text("Regist.work_type.{$r[0]['id']}",array('class' =>'lbl','readonly'=>'readonly' ));?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($r[0]['standard']); ?></td>
                <td><?php echo h_out($r[0]['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($r[0]['price']),'right'); ?></td>
                <td><?php echo h_out($r[0]['validated_date'],'center'); ?></td>
                <td><?php echo h_out($r[0]['facility_sticker_no']); ?></td>
                <td>
                    <?php echo $this->form->text("Regist.AdjustmentCount.{$r[0]['id']}",array('class'=>'lbl num' ,'readonly'=>'readonly'  )); ?>
                </td>
                <td>
                    <?php echo $this->form->text("Regist.Remarks.{$r[0]['id']}",array( 'class' =>'lbl' ,'readonly'=>'readonly'  )); ?>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
<?php echo $this->form->end(); ?>
