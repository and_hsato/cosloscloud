<script type="text/javascript">
$(function(){
    $(document).ready(function(){
        //チェックボックス オールチェック
        $('.checkAll').click(function(){
            $('#registForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
        });

        //備考一括設定ボタン押下
        $('#but_CollectiveSetting').click(function(){
            $("#registForm input[type=text].remarks").each(function(){
                $(this).val($("#ExpirationCollectiveSetting").val());
            });
        });

        //確定ボタン押下
        $('#btn_Regist').click(function(){
            submitFlg = true;
            isChecked = false;
            isAlert = false;

            $("#registForm input[type=checkbox].chk").each(function(){
                if($(this).attr("checked") == true){
                var count = $(this).parents('tr:eq(0)').next().find('input[type=text].count');
                var quantity = $(this).parents('tr:eq(0)').next().find('input[type=hidden].quantity');
  
                isChecked = true;
                //調整数チェック
                if(count.val() == ""){
                    alert("調整数を入力してください");
                    count.focus();
                    submitFlg = false;
                    return false;
                }
                if(count.val() == "0"){
                    alert("0以外の数字を入力してください");
                    count.focus();
                    submitFlg = false;
                    return false;
                }
                if( count.val().match( /[\D]/g ) ){
                    alert("正の整数を入力してください");
                    count.focus();
                    submitFlg = false;
                    return false;
                }
                var type = <?php echo $this->request->data['setting']['adjustment_type']; ?>;
                if(type == 1 || type ==2){
                    if(parseInt(quantity.val()) < parseInt(count.val())){
                        alert("調整数が在庫数を超えています");
                        count.focus();
                        submitFlg = false;
                        return false;
                    }
                }
                if($(this).next(':eq(0)').val() != ''){
                    isAlert = true;
                }
            }
        });

        if(!isChecked){
           alert("明細を選択してください");
           submitFlg = false;
            return false;
        }

             if(submitFlg){
                if(isAlert){
                   if(confirm('定数切替対象商品が含まれますが\nよろしいですか？')){
                        $(window).unbind('beforeunload');
                        $("#registForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/adjustment_regist').submit();
                    }
                }else{
                    $(window).unbind('beforeunload');
                    $("#registForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/adjustment_regist').submit();
                }
            }
        });
    });
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
});

//パンくずクリック
function goBack1(){
    $("#registForm").attr('action', "<?php echo $this->webroot?><?php echo $this->name; ?>/adjustment").submit();
}

function goBack2(){
    $("#registForm").attr('action', "<?php echo $this->webroot; ?><?php echo $this->name; ?>/<?php echo $data['Search']['Url'];?>").submit();
}

function goBack3(){
    $("#registForm").attr('action', "<?php echo $this->webroot.'inventories/difference_list' ?>").submit();
}

function goBack4(){
    $("#registForm").attr('action', "<?php echo $this->webroot.'inventories/difference_detail' ?>").submit();
}

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <?php if(isset($data['TrnInventoryHeader']['id'])){ //棚卸からきた場合?>
        <li><a href="#" onclick="goBack3();" >差異一覧</a></li>
        <li><a href="#" onclick="goBack4();" >差異明細</a></li>
        <?php }else{ //棚卸からきた場合 ?>
        <li><a href="#" onclick="goBack1();">在庫調整</a></li>
        <li><a href="#" onclick="goBack2();"><?php echo $data["Search"]["back"];?> </a></li>
        <?php } ?>
        <li>在庫調整確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>在庫調整確認</span></h2>
<h2 class="HeaddingMid">以下の内容で更新します</h2>
<?php echo $this->form->create('Expiration',array('type'=>'post','action'=>'','id'=>'registForm','class'=>'validate_form')); ?>
    <?php echo $this->form->hidden("Search.Url",array('value'=>$data['Search']['Url'])); ?>
    <?php echo $this->form->hidden("Search.back",array('value'=>$data['Search']['back'])); ?>
    <?php echo $this->form->input('TrnAdjustment.time',array('type' => 'hidden', 'value'=>date('Y/m/d H:i:s.u')));?>
    <?php echo $this->form->input('TrnAdjustment.token',array('type' => 'hidden'));?>

    <?php if(isset($data['TrnInventoryHeader']['id'])){ //棚卸からきた場合 ?>
    <?php $i = 0 ; ?>
    <?php foreach( $data['TrnInventoryHeader']['id'] as $id ){ ?>
    <input type="hidden" name="data[TrnInventoryHeader][id][<?php echo $i ?>]" value="<?php echo $id ?>">
    <?php $i++ ; ?>
    <?php } ?>
    <?php } ?>


    <?php if($data["Search"]["Url"] == "adjustment_search"){?>
    <?php echo $this->form->input('search.tempId',array('type'=>'hidden','id'=>'tempId'));?>
    <?php echo $this->form->hidden("search.item_id");?>
    <?php echo $this->form->hidden("search.item_code");?>
    <?php echo $this->form->hidden("search.lot_no");?>
    <?php echo $this->form->hidden("search.item_name");?>
    <?php echo $this->form->hidden("search.dealer_name");?>
    <?php echo $this->form->hidden("search.standard");?>
    <?php echo $this->form->hidden("search.stickers_id");?>
    <?php }?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>調整日</th>
                <td>
                    <?php echo $this->form->text('setting.date',array('class' => 'lbl','style'=>'width:150px','readonly'=>'readonly')); ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('setting.classes_name',array('class' => 'lbl','style'=>'width:150px','readonly'=>'readonly')); ?>
                    <?php echo $this->form->input('setting.classes',array('type'=>'hidden')); ?>
                </td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('setting.facility_name',array('class' => 'lbl','style'=>'width:150px;','readonly'=>'readonly')); ?>
                    <?php echo $this->form->input('setting.facilities',array('type'=>'hidden')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->text('setting.remarks',array('class' => 'lbl','style'=>'width:150px','readonly'=>'readonly')); ?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('setting.department_name',array('class' => 'lbl','style'=>'width:150px;','readonly'=>'readonly')); ?>
                    <?php echo $this->form->input('setting.departments',array('type'=>'hidden')); ?>
                </td>
                <td></td>
                <th>調整種別</th>
                <td>
                    <?php echo $this->form->input('setting.adjustment_name',array('class' => 'lbl','style'=>'width:150px;','readonly'=>'readonly')); ?>
                    <?php echo $this->form->input('setting.adjustment_type',array('type'=>'hidden')); ?>
                </td>
            </tr>
        </table>
    </div>
    <hr class="Clear" />

    <div class="SearchBox">
        <table style="width:100%;">
            <tr>
                <td></td>
                <td align="right"></td>
            </tr>
            <tr>
                <td>表示件数：<?php echo count($SearchResult); ?>件</td>
                <td align="right">
                    <?php echo $this->form->text('CollectiveSetting',array('class' => 'txt','style' => 'width:150px'));?>
                    <input type="button" class="btn btn8 [p2]" id="but_CollectiveSetting">
                </td>
            </tr>
        </table>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th rowspan="2" width="20"><input type="checkbox" class="checkAll" checked></th>
                    <th class="col10">商品ID</th>
                    <th class="col15">商品名</th>
                    <th class="col15">製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col10">ロット番号</th>
                    <th class="col15">センターシール</th>
                    <th class="col10">在庫数</th>
                    <th class="col15">作業区分</th>
                </tr>
                <tr>
                    <th>状態</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>評価単価</th>
                    <th>有効期限</th>
                    <th>部署シール</th>
                    <th>調整数</th>
                    <th>備考</th>
                </tr>
        <?php if(count($SearchResult) == 0){ ?>
            <tr>
                <td colspan="9"><span>該当するデータがありません</span></td>
            </tr>
        <?php } ?>
        <?php $_cnt=0;foreach($SearchResult as $_row): ?>
            <tr>
                <td rowspan="2" class="center">
                <?php if($_row[0]['status'] == ""){
                    echo $this->form->checkbox("Regist.Id.{$_cnt}",array('class'=>'center chk','value'=>$_row[0]['id'],'checked'=>'checked','hiddenField'=>false) );
                    echo $this->form->input('FixedCount.substitute_unit_id' , array('type'=>'hidden' , 'value'=>$_row[0]['substitute_unit_id']));
                }
                ?>
                </td>
                <td><?php echo h_out($_row[0]['internal_code'] ,'center'); ?></td>
                <td><?php echo h_out($_row[0]['item_name']); ?></td>
                <td><?php echo h_out($_row[0]['item_code']); ?></td>
                <td><?php echo h_out($_row[0]['unit_name']); ?></td>
                <td><?php echo h_out($_row[0]['lot_no']); ?></td>
                <td><?php echo h_out($_row[0]['facility_sticker_no']); ?></td>
                <td><?php echo h_out($_row[0]['quantity'],'right'); ?></td>
                <td>
                <?php echo $this->form->input("Regist.work_type.{$_row[0]['id']}",array('options'=>$classes,'empty'=>true,'class' => 'txt')); ?>
                </td>
            </tr>
            <tr>
                <td style="color:#FF0000;"><?php echo h_out($_row[0]['status'],'center'); ?></td>
                <td><?php echo h_out($_row[0]['standard']); ?></td>
                <td><?php echo h_out($_row[0]['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($_row[0]['appraised_price']),'right'); ?></td>
                <td><?php echo h_out($_row[0]['validated_date'],'center'); ?></td>
                <td><?php echo h_out($_row[0]['hospital_sticker_no']); ?></td>
                <td align="right">
                <?php  if($_row[0]['status'] == "" || $_row[0]['status'] == "予約中"){
                           if($_row[0]['is_lowlevel']){
                               echo $this->form->text("Regist.AdjustmentCount.{$_row[0]['id']}",
                               array('class'=>'r txt num count'));
                           }else{
                               if($this->request->data['setting']['adjustment_type'] == Configure::read('ExpirationType.captiveConsumptionReduction') || $this->request->data['setting']['adjustment_type'] == Configure::read('ExpirationType.miscellaneousLossReduction')){
                                   echo $this->form->text("Regist.AdjustmentCount.{$_row[0]['id']}",
                                   array('class'=>'lbl r txt num count','value'=>$_row[0]['quantity']));
                               }else{
                                   echo $this->form->text("Regist.AdjustmentCount.{$_row[0]['id']}",
                                   array('class'=>'lbl num count','readonly'=>'readonly','value'=>1));
                               }
                          }
                       }
                       $this->form->hidden("Regist.Quantity.{$_row[0]['id']}",array('value'=>$_row[0]['quantity'] , 'quantity'));
                ?>
                </td>
                <td>
                <?php
                    if($_row[0]['status'] == ''){
                        echo $this->form->text("Regist.Remarks.{$_row[0]['id']}",array('class' => 'txt remarks','style'=>'width:163px;','maxlength'=>200));
                    }else{
                        echo $this->form->hidden("Regist.Remarks.{$_row[0]['id']}",array('value'=>'','maxlength'=>200));
                    }
                ?>
                </td>
                <?php echo $this->form->hidden("Regist.Modified.{$_row[0]['id']}",array('value'=>$_row[0]['modified'])); ?>
            </tr>
            <?php $_cnt++;endforeach;?>
        </table>
    </div>

    <table style="width:100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>

    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn2 [p2]" id="btn_Regist"/>
        </p>
    </div>
<?php echo $this->form->end(); ?>
