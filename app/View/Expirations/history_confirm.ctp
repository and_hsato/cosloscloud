<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('#confirmForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    //取消ボタン押下時
    $('#btn_Regist').click(function(){
        if($("#confirmForm input[type=checkbox].checkAllTarget:checked").length == 0 ){
            alert("明細を選択してください");
            return false;
        }else{
            if(window.confirm("取消を実行します。よろしいですか？")){
                 $("#confirmForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history_result');
                 $("#confirmForm").submit();
            }
        }
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/history">在庫調整履歴</a></li>
        <li>調整履歴明細</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>調整履歴明細</span></h2>

<?php echo $this->form->create('Expiration',array('type'=>'post','action'=>'','id'=>'confirmForm','class'=>'validate_form'));?>
    <?php echo $this->form->input('TrnAdjustment.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u')));?>
    <?php echo $this->form->input('TrnAdjustment.token', array('type'=>'hidden'));?>
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden','id'=>'is_search'));?>
    <?php echo $this->form->hidden("search.work_no",array("value"=>$data["search"]["work_no"])); ?>
    <?php echo $this->form->hidden("search.work_date_from",array("value"=>$data["search"]["work_date_from"])); ?>
    <?php echo $this->form->hidden("search.work_date_to",array("value"=>$data["search"]["work_date_to"])); ?>
    <?php echo $this->form->hidden("search.item_id",array("value"=>$data["search"]["item_id"])); ?>
    <?php echo $this->form->hidden("search.item_code",array("value"=>$data["search"]["item_code"])); ?>
    <?php echo $this->form->hidden("search.lot_no",array("value"=>$data["search"]["lot_no"])); ?>
    <?php echo $this->form->hidden("search.item_name",array("value"=>$data["search"]["item_name"])); ?>
    <?php echo $this->form->hidden("search.dealer_name",array("value"=>$data["search"]["dealer_name"])); ?>
    <?php echo $this->form->hidden("search.management",array("value"=>$data["search"]["management"])); ?>
    <?php echo $this->form->hidden("search.standard",array("value"=>$data["search"]["standard"])); ?>
    <?php echo $this->form->hidden("search.sticker_no",array("value"=>$data["search"]["sticker_no"])); ?>
    <?php echo $this->form->hidden("search.deleted",array("value"=>$data["search"]["deleted"])); ?>

    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th style="width: 20px;" rowspan="2"><input type="checkbox" class="checkAll"></th>
                <th class="col10">調整番号</th>
                <th class="col10">調整日</th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col10">センターシール</th>
                <th class="col10">作業区分</th>
                <th class="col10">更新者</th>
            </tr>
            <tr>
                <th colspan="2">施設　／　部署</th>
                <th>種別</th>
                <th>規格</th>
                <th>販売元</th>
                <th>調整単価</th>
                <th>有効期限</th>
                <th>部署シール</th>
                <th colspan="2">備考</th>
            </tr>
        <?php $_cnt=0;foreach($SearchResult as $_row): ?>
        <?php $_id = $_row[0]['id'] ;?>
            <tr>
                <td rowspan="2" style="width:20px;" class="center">
                    <?php if($_row[0]['regist_status'] === ''){?>
                    <?php echo $this->form->checkbox("Regist.Id.{$_cnt}",array('class'=>'center checkAllTarget','hiddenField'=>false ,'value'=>$_row[0]['id']) );?>
                    <?php }?>
                </td>
                <td class="col10">
                    <?php echo h_out($_row[0]['work_no']); ?>
                    <?php echo $this->form->hidden("Regist.WorkNo.{$_id}",array('value'=>$_row[0]['work_no'])); ?>
                </td>
                <td class="col10">
                    <?php echo h_out($_row[0]['work_date'],'center');?>
                    <?php echo $this->form->hidden("Regist.WorkDate.{$_id}",array('value'=>$_row[0]['work_date'])); ?>
                </td>
                <td class="col10">
                    <?php echo h_out($_row[0]['internal_code'],'center');?>
                    <?php echo $this->form->hidden("Regist.InternalCode.{$_id}",array('value'=>$_row[0]['internal_code'])); ?>
                </td>
                <td>
                    <?php echo h_out($_row[0]['item_name']);?>
                    <?php echo $this->form->hidden("Regist.ItemName.{$_id}",array('value'=>$_row[0]['item_name'])); ?>
                </td>
                <td>
                    <?php echo h_out($_row[0]['item_code']);?>
                    <?php echo $this->form->hidden("Regist.ItemCode.{$_id}",array('value'=>$_row[0]['item_code'])); ?>
                </td>
                <td class="col10">
                    <?php echo $_row[0]['unit_name']; ?>
                    <?php echo $this->form->hidden("Regist.PackagingUnit.{$_id}",array("value"=>$_row[0]['unit_name']));?>
                </td>
                <td class="col10">
                    <?php echo h_out($_row[0]['lot_no']);?>
                    <?php echo $this->form->hidden("Regist.LotNo.{$_id}",array('value'=>$_row[0]['lot_no'])); ?>
                </td>
                <td class="col10">
                    <?php echo h_out($_row[0]['facility_sticker_no']);?>
                    <?php echo $this->form->hidden("Regist.FacilityStickerNo.{$_id}",array('value'=>$_row[0]['facility_sticker_no'])); ?>
                </td>
                <td class="col10">
                    <?php echo h_out($_row[0]['name']);?>
                    <?php echo $this->form->hidden("Regist.Name.{$_id}",array('value'=>$_row[0]['name'])); ?>
                </td>
                <td class="col10">
                    <?php echo h_out($_row[0]['user_name']);?>
                    <?php echo $this->form->hidden("Regist.UserName.{$_id}",array('value'=>$_row[0]['user_name'])); ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php echo h_out($_row[0]['facility_name'].'／'.$_row[0]['department_name']);?>
                    <?php echo $this->form->hidden("Regist.FacilityName.{$_id}",array('value'=>$_row[0]['facility_name'].'／'.$_row[0]['department_name'])); ?>
                </td>
                <td>
                    <?php echo h_out($_row[0]['adjust_type'],'center');?>
                    <?php echo $this->form->hidden("Regist.AdjustType.{$_id}",array('value'=>$_row[0]['adjust_type'])); ?>
                </td>
                <td>
                    <?php echo h_out($_row[0]['standard']);?>
                    <?php echo $this->form->hidden("Regist.Standard.{$_id}",array('value'=>$_row[0]['standard'])); ?>
                </td>
                <td>
                    <?php echo h_out($_row[0]['dealer_name']);?>
                    <?php echo $this->form->hidden("Regist.DealerName.{$_id}",array('value'=>$_row[0]['dealer_name'])); ?>
                </td>
                <td>
                    <?php echo h_out($this->Common->toCommaStr($_row[0]['appraised_price']),'right'); ?>
                    <?php echo $this->form->hidden("Regist.AppraisedPrice.{$_id}",array('value'=>$_row[0]['appraised_price'])); ?>
                </td>
                <td>
                    <?php echo h_out($_row[0]['validated_date'],'center') ; ?>
                    <?php echo $this->form->hidden("Regist.ValidatedDate.{$_id}",array('value'=>$_row[0]['validated_date'])); ?>
                </td>
                <td>
                    <?php echo h_out($_row[0]['hospital_sticker_no']);?>
                    <?php echo $this->form->hidden("Regist.HospitalStickerNo.{$_id}",array('value'=>$_row[0]['hospital_sticker_no'])); ?>
                </td>
                <td colspan="2">
                <?php if($_row[0]['regist_status'] === ''){?>
                <?php echo $this->form->text("Regist.Remarks.{$_id}",array('class'=>'txt','style'=>'width:230px')); ?>
                <?php }else if($_row[0]['is_deleted']){?>
                <p style="color:#FF0000;">すでに取消されています</p>
                <?php }else{?>
                <p style="color:#FF0000;"><?php echo $_row[0]['regist_status'];?></p>
                <?php }?>
                <?php echo $this->form->hidden("Regist.Modified.{$_id}",array('value'=>$_row[0]['modified'])); ?>
                </td>
            </tr>
            <?php $_cnt++;endforeach;?>
        </table>
    </div>

    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn6 [p2]" id="btn_Regist" />
        </p>
    </div>

<?php echo $this->form->end(); ?>