<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/history">在庫調整履歴</a></li>
        <li class="pankuzu">調整履歴明細</li>
        <li>在庫調整取消結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>在庫調整取消結果</span></h2>
<div class="Mes01">以下の調整を取り消しました</div>

<?php echo $this->form->create('Expiration',array('type'=>'post','action'=>'','id'=>'resultForm','class'=>'validate_form'));?>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th style="width: 20px;" rowspan="2"></th>
                <th class="col10">調整番号</th>
                <th class="col10">調整日</th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col10">センターシール</th>
                <th class="col10">作業区分</th>
                <th class="col10">更新者</th>
            </tr>
            <tr>
                <th colspan="2">施設　／　部署</th>
                <th>種別</th>
                <th>規格</th>
                <th>販売元</th>
                <th>調整単価</th>
                <th>有効期限</th>
                <th>部署シール</th>
                <th colspan="2">備考</th>
            </tr>
            <?php foreach($data['Regist']['Id'] as $id){ ?>
            <tr>
                <td style="width: 20px;" rowspan="2"></td>
                <td class="col10"><?php echo h_out($data['Regist']['WorkNo'][$id]);?></td>
                <td class="col10"><?php echo h_out($data['Regist']['WorkDate'][$id],'center');?></td>
                <td class="col10"><?php echo h_out($data['Regist']['InternalCode'][$id],'center');?></td>
                <td><?php echo h_out($data['Regist']['ItemName'][$id]);?></td>
                <td><?php echo h_out($data['Regist']['ItemCode'][$id]);?></td>
                <td class="col10"><?php echo h_out($data['Regist']['PackagingUnit'][$id]);?></td>
                <td class="col10"><?php echo h_out($data['Regist']['LotNo'][$id]);?></td>
                <td class="col10"><?php echo h_out($data['Regist']['FacilityStickerNo'][$id]);?></td>
                <td class="col10"><?php echo h_out($data['Regist']['Name'][$id]);?></td>
                <td class="col10"><?php echo h_out($data['Regist']['UserName'][$id]);?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo h_out($data['Regist']['FacilityName'][$id]);?></td>
                <td><?php echo h_out($data['Regist']['AdjustType'][$id],'center');?></td>
                <td><?php echo h_out($data['Regist']['Standard'][$id]);?></td>
                <td><?php echo h_out($data['Regist']['DealerName'][$id]);?></td>
                <td><?php echo h_out($data['Regist']['AppraisedPrice'][$id]);?></td>
                <td><?php echo h_out($data['Regist']['ValidatedDate'][$id]); ?></td>
                <td><?php echo h_out($data['Regist']['HospitalStickerNo'][$id]);?></td>
                <td colspan="2">
                <?php echo $this->form->text("Regist.Remarks.{$id}",array('class'=>'lbl','style'=>'width:230px','readonly'=>'readonly')); ?>
                </td>
            </tr>
<?php } ?>
        </table>
    </div>
<?php echo $this->form->end();?>
