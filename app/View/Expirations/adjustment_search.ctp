<script type="text/javascript">
$(document).ready(function(){
    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );
  
    //検索ボタン押下
    $("#btn_Search").click(function searchItem(){
        //カート欄にある全IDを取得
        var tempId = "";
        $("#confirmForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tempId==""){
                    tempId += $(this).val();
                }else{
                    tempId += "," + $(this).val();
                }
             }
        });
        $("#tempId").val(tempId);

        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/adjustment_search').submit();
    });

    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#confirmForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        var objArray = new Array();
        var isChecked = false;
        $('#itemSelectedTable').empty();
        $("#search input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#SelectedItemId' + cnt).val( $(this).val() );
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
                isChecked = true;
            }
            cnt++;
        });
        if(!isChecked){
            alert('明細を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        var tempId = "";
        $("#confirmForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tempId==""){
                    tempId += $(this).val();
                }else{
                    tempId += "," + $(this).val();
                }
            }
        });
        $("#tempId").val(tempId);
        
        if($("#confirmForm input[type=checkbox].checkAllTarget").length  === 0){
            alert('確認する明細を選択してください');
        }else{
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/adjustment_confirm').submit();
        }
    });

});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/adjustment">在庫調整</a></li>
        <li>在庫選択</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>在庫選択</span></h2>
<?php echo $this->form->create('Expiration',array('type'=>'post','action'=>'','id'=>'searchForm','class'=>'validate_form search_form')); ?>
    <?php echo $this->form->hidden("Search.Url",array("value"=>"adjustment_search")); ?>
    <?php echo $this->form->input('search.is_search',array('type' => 'hidden','id' => 'is_search'));?>
    <?php echo $this->form->input('search.tempId',array('type'=>'hidden','id' =>'tempId'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>調整日</th>
                <td>
                    <?php echo $this->form->input('setting.date',array('type'=>'text','class' => 'lbl','style'=>'width:150px','readonly'=>'readonly' ));?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('setting.classes_name', array('class' => 'lbl','style'=>'width:150px','readonly'=>'readonly'));?>
                    <?php echo $this->form->hidden('setting.classes'); ?>
                </td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('setting.facility_name',array('class' => 'lbl','style'=>'width:150px;','readonly'=>'readonly' ));?>
                    <?php echo $this->form->hidden('setting.facilities'); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('setting.remarks',array('class' => 'lbl','style'=>'width:150px','readonly'=>'readonly'));?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('setting.department_name',array('class' => 'lbl','style'=>'width:150px;','readonly'=>'readonly'));?>
                    <?php echo $this->form->hidden('setting.departments'); ?>
                </td>
                <td></td>
                <th>調整種別</th>
                <td>
                    <?php echo $this->form->input('setting.adjustment_name',array('class' => 'lbl','style'=>'width:150px;','readonly'=>'readonly'));?>
                    <?php echo $this->form->hidden('setting.adjustment_type'); ?>
                </td>
            </tr>
        </table>
        <h2 class="HeaddingLarge2">検索条件</h2>
        <table class="FormStyleTable">
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('search.item_id',array('class'=>'txt search_upper search_internal_code','style'=>'width:120px; ime-mode:disabled;')); ?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('search.item_code', array('class'=>'txt search_upper','style'=>'width:120px; ime-mode:disabled;')); ?></td>
                <td width="20"></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->text('search.lot_no',array('class'=>'txt','style' =>'width:120px;ime-mode:disabled;')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('search.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('search.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('search.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->text('search.stickers_id',array('class'=>'txt','style'=>'width:120px;ime-mode:disabled;')); ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="btn_Search"/>
            </p>
        </div>
    </div>
    <hr class="Clear" />
    <div id="results">
        <div class="DisplaySelect"><?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?></div>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th rowspan="2" width="20"><input type="checkbox" class="checkAll_1"></th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th class="col15">包装単位</th>
                    <th class="col15">ロット番号</th>
                    <th class="col15">センターシール</th>
                </tr>
                <tr>
                    <th>状態</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>評価単価</th>
                    <th>有効期限</th>
                    <th>部署シール</th>
                </tr>
            </table>
        </div>

        <div class="" id="search">
            <?php $_cnt=0;foreach($SearchResult as $_row): ?>
            <table class="TableStyle02" style="margin-top:-1px; margin-bottom:0px; padding:0px;" id="<?php echo 'containTables'.$_row[0]['id'];?>" >
                <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>">
                    <td rowspan="2" width="20" style="padding:0px;" class="center">
                    <?php if(empty($_row[0]['status']) || $_row[0]['status'] == '予約中'){ ?>
                    <?php echo $this->form->checkbox("SelectedId.{$_cnt}",array('class'=>'center checkAllTarget','value'=>$_row[0]['id']) ); ?>
                    <?php }?>
                    </td>
                    <td class="col10"><?php echo h_out($_row[0]['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($_row[0]['item_name']); ?></td>
                    <td><?php echo h_out($_row[0]['item_code']); ?></td>
                    <td class="col15"><?php echo h_out($_row[0]['unit_name']); ?></td>
                    <td class="col15"><?php echo h_out($_row[0]['lot_no']); ?></td>
                    <td class="col15"><?php echo h_out($_row[0]['facility_sticker_no']); ?></td>
                </tr>
                <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>">
                    <td class="red"><?php echo h_out($_row[0]['status'],'center'); ?></td>
                    <td><?php echo h_out($_row[0]['standard']); ?></td>
                    <td><?php echo h_out($_row[0]['dealer_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($_row[0]['appraised_price']),'right'); ?></td>
                    <td><?php echo h_out($_row[0]['validated_date'],'center'); ?></td>
                    <td><?php echo h_out($_row[0]['hospital_sticker_no']); ?></td>
                </tr>
            </table>
            <?php $_cnt++;endforeach;?>
            <?php if( $_cnt==0 && isset($this->request->data['search']['is_search'])){ ?>
            <table class="TableStyle02" style="margin-top:-1px; margin-bottom:0px; padding:0px;">
            <tr><td colspan="7" class="center">該当するデータはありません。</td></tr>
            </table>
            <?php } ?>
        </div>
        <div style="margin-top:5px;" class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn4" id="cmdSelect" />
            </p>
        </div>
    </div>

    <h2 class="HeaddingSmall">選択商品</h2>

    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th rowspan="2" width="20" style="padding:0px;">
                    <input type="checkbox" class="checkAll_2" checked>
                </th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col15">包装単位</th>
                <th class="col15">ロット番号</th>
                <th class="col15">センターシール</th>
            </tr>
            <tr>
                <th>状態</th>
                <th>規格</th>
                <th>販売元</th>
                <th>評価単価</th>
                <th>有効期限</th>
                <th>部署シール</th>
            </tr>
        </table>
    </div>

    <div class="" style="" id="confirmForm">
        <?php $_cnt=0;foreach($CartSearchResult as $_row): ?>
        <table class="TableStyle02" style="margin-top:-1px; margin-bottom:0px; padding:0px;" id="<?php echo 'containTables'.$_row[0]['id'];?>" >
            <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>">
                <td rowspan="2" width="20" style="padding:0px;" class="center">
                    <?php echo $this->form->checkbox("SelectedId.{$_cnt}",array('class'=>'center checkAllTarget','value'=>$_row[0]['id'] , 'checked'=>true) );?>
                </td>
                <td class="col10"><?php echo h_out($_row[0]['internal_code'],'center'); ?></td>
                <td><?php echo h_out($_row[0]['item_name']); ?></td>
                <td><?php echo h_out($_row[0]['item_code']); ?></td>
                <td class="col15"><?php echo h_out($_row[0]['unit_name']) ?></td>
                <td class="col15"><?php echo h_out($_row[0]['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($_row[0]['facility_sticker_no']); ?></td>
            </tr>
            <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>">
                <td class="red"><?php echo h_out($_row[0]['status'],'center'); ?></td>
                <td><?php echo h_out($_row[0]['standard']); ?></td>
                <td><?php echo h_out($_row[0]['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($_row[0]['appraised_price'])); ?></td>
                <td><?php echo h_out($_row[0]['validated_date'],'center'); ?></td>
                <td><?php echo h_out($_row[0]['hospital_sticker_no']); ?></td>
            </tr>
        </table>
        <?php $_cnt++;endforeach;?>
        <div id="itemSelectedTable"></div>
    </div>

    <div style="margin-top:5px;" class="ButtonBox">
        <p class="center">
            <input type="button" id="btn_Confirm" class="btn btn3"/>
        </p>
    </div>
<?php echo $this->form->end();?>
