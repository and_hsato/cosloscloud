<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
    });

    //検索ボタン押下
    $("#btn_Search").click(function(){
        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history').submit();
    });

    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
    });

    //明細表示ボタン押下
    $("#btn_Confirm").click(function(){
        if($("#searchForm input[type=checkbox].checkAllTarget:checked").length === 0){
            alert('確認する明細を選択してください');
        }else{
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history_confirm').submit();
        }
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>在庫調整履歴</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>在庫調整履歴</span></h2>
<?php echo $this->form->create('Expiration',array('type'=>'post','action'=>'','id'=>'searchForm','class'=>'validate_form search_form input_form')); ?>
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>調整番号</th>
                <td><?php echo $this->form->text('search.work_no',array('class'=>'txt','style'=>'width:110px'));?></td>
                <td></td>
                <th>調整日</th>
                <td colspan="4">
                    <?php echo $this->form->text('search.work_date_from',array('class'=>'date validate[custom[date]]','maxlength'=>'10','id'=>'datepicker_from')); ?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo $this->form->text('search.work_date_to',array('class'=>'date validate[custom[date]]','maxlength'=>'10','id'=>'datepicker_to')); ?>
                </td>
            </tr>
            <tr>
                <th>調整施設</th>
                <td>
                    <?php echo $this->form->input('search.facility_code',array('class'=>'txt','style'=>'width:60px','id'=>'facilityText'));?>
                    <?php echo $this->form->input('search.facilities',array('options'=>$facilities,'class'=>'txt','style'=>'width:150px;' , 'empty'=>true,'id'=>'facilityCode')); ?>
                    <?php echo $this->form->input('search.facility_name',array('type'=>'hidden','id'=>'facilityName'));?>
                </td>
                <td></td>
                <th>調整部署</th>
                <td>
                    <?php echo $this->form->input('search.department_code',array('class'=>'txt','style'=>'width:60px','id'=>'departmentText'));?>
                    <?php echo $this->form->input('search.departments',array('options'=>$department,'class'=>'txt','style'=>'width:150px;','empty'=>true,'id'=>'departmentCode')); ?>
                    <?php echo $this->form->input('search.department_name',array('type'=>'hidden','id'=>'departmentName'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('search.item_id',array('class'=>'txt search_upper search_internal_code','style'=>'width:120px')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('search.item_code',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->text('search.lot_no',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td>
                    <?php echo $this->form->checkbox("search.deleted",array());?>取消も表示する
                </td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('search.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('search.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>管理区分</th>
                <td><?php echo $this->form->input('search.management',array('options'=>$management,'class'=>'txt' , 'empty'=>true)); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('search.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->text('search.sticker_no',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('search.work_type',array('options'=>$classes,'class'=>'txt' , 'empty'=>true));?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="btn_Search"/>
                <input type="button" class="btn btn5" id="btn_Csv"/>
            </p>
        </div>
    </div>
    <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
    </div>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <tr>
                <th style="width:20px;"><input type="checkbox" class="checkAll"></th>
                <th class="col15">調整番号</th>
                <th class="col10">調整日</th>
                <th class="col20">施設　／　部署</th>
                <th class="col10">登録者</th>
                <th class="col15">登録日時</th>
                <th class="col20">備考</th>
                <th class="col10">件数</th>
            </tr>
            <?php $_cnt=0;foreach($SearchResult as $_row): ?>
            <tr>
                <td class="center">
                    <?php echo $this->form->checkbox("SelectedId.{$_cnt}",array('class'=>'center checkAllTarget','value'=>$_row['TrnAdjustHeader']['id']) );?>
                </td>
                <td><?php echo h_out($_row['TrnAdjustHeader']['work_no'],'center');?></td>
                <td><?php echo h_out($_row['TrnAdjustHeader']['work_date'],'center');?></td>
                <td><?php echo h_out($_row['TrnAdjustHeader']['facility_name']."／".$_row['TrnAdjustHeader']['department_name']);?></td>
                <td><?php echo h_out($_row['TrnAdjustHeader']['user_name']);?></td>
                <td><?php echo h_out($_row['TrnAdjustHeader']['created'],'center');?></td>
                <td><?php echo h_out($_row['TrnAdjustHeader']['recital']);?></td>
                <td><?php echo h_out($_row['TrnAdjustHeader']['count']."/".$_row['TrnAdjustHeader']['detail_count'],'right');?></td>
            </tr>
            <?php $_cnt++;endforeach;?>
            <?php if(isset($this->request->data['search']['is_search']) && count($SearchResult) == 0 ){ ?>
            <tr><td colspan = '8' class="center"> 該当するデータはありません。</td></tr>
            <?php } ?>
        </table>
    </div>
    <?php if(count($SearchResult) > 0){ ?>
    <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
            <input type="button" class="btn btn7" id="btn_Confirm" />
        </p>
    </div>
    <?php } ?>
<?php echo $this->form->end(); ?>
