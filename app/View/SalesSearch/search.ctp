<script type="text/javascript">
    $(document).ready(function(){
        //検索ボタン押下
        $('#btn_Search').click(function(){
            $('#change_confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').submit();
        });

        //帳票出力ボタン押下
        $('#btn_Print').click(function(){
            $('#change_confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/dummy').submit();
            $('#change_confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
        });

        //CSVボタン押下
        $('#btn_Export').click(function(){
            $('#change_confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
            $('#change_confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
        });
 
        //EXCELボタン押下
        $("#btn_Excel").click(function(){
            $("#change_confirm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_excel').submit();
            $("#change_confirm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
        });

        $('.checkAll').click(function(){
            $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
        });

        $('#btn_confirm').click(function(){
            if($('input[type=checkbox].chk:checked').length > 0 ){  
                $('#change_confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm').submit();
            }else{
                alert('変更を行う売上履歴を選択してください');
                return false;
            }
        });

        $(function (){
          $('img.scroll').click(function(){
           var to_id = $(this).attr('to_id');
           var obj = $('#'+to_id);
           var offset = obj.offset();

           scrollTo(0,offset.top);
           });
        });

    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>売上検索</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>売上検索</span></h2>
<form class="validate_form search_form" id="change_confirm" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search" method="post">
    <input type="hidden" name="data[IsSearch]" value="1" />
    <input type="hidden" name="data[session]" value="<?php print($this->Session->read('Auth.facility_id_selected'));?>">
    <input type="hidden" name="data[user_id]" value="<?php print($this->Session->read('Auth.MstUser.id'));?>">
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
        <tr>
            <th>施主</th>
            <td>
                <?php echo $this->form->input('MstFacility.ownerText',array('class'=>'txt','style'=>'width:60px;' , 'id'=>'ownerText')); ?>
                <?php echo $this->form->input('MstFacility.ownerCode',array('options'=>$donor,'class'=>'txt' , 'empty'=>true , 'id'=>'ownerCode')); ?>
                <?php echo $this->form->input('MstFacility.ownerName',array('type'=>'hidden','id'=>'ownerName')); ?>
            </td>
            <td width="20"></td>
            <th>売上日</th>
            <td>
                <?php echo($this->form->text('TrnClaim.claim_date_from', array('class' => 'date txt validate[custom[date]]', 'id'=> 'datepicker1', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                <span>&nbsp;～&nbsp;</span>
                <?php echo($this->form->text('TrnClaim.claim_date_to', array('class' => 'date txt validate[custom[date]]', 'id'=> 'datepicker2', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
            </td>
            <td width="20"></td>
            <th colspan="2"><?php if(!(isset($check))){$check = true;} echo($this->form->checkbox('TrnClaim.is_deleted', array('checked' => $check))); ?>取消は表示しない</th>
        </tr>
        <tr>
            <th>得意先</th>
            <td>
                <?php echo $this->form->input('MstFacility.facilityText',array('class'=>'txt require','style'=>'width:60px;','id'=>'facilityText')); ?>
                <?php echo $this->form->input('MstFacility.facilityCode',array('options'=>$facilities,'class'=>'txt','style'=>'width:120px;','id'=>'facilityCode' , 'empty'=>true)); ?>
                <?php echo $this->form->input('MstFacility.facilityName',array('type'=>'hidden','id'=>'facilityName')); ?>
            </td>
            <td width="20"></td>
            <th>部署</th>
            <td>
                <?php echo $this->form->input('MstFacility.departmentText',array('class'=>'txt','style'=>'width:60px;','id'=>'departmentText')); ?>
                <?php echo $this->form->input('MstFacility.departmentCode',array('options'=>$departments, 'class'=>'txt','style'=>'width:120px;','id'=>'departmentCode' , 'empty'=>true)); ?>
                <?php echo $this->form->input('MstFacility.departmentName',array('type'=>'hidden','id'=>'departmentName')); ?>
            </td>
        </tr>
        <tr>
            <th>商品ID</th>
            <td><?php echo($this->form->text('MstFacilityItem.internal_code', array('class' => 'txt search_internal_code', 'maxlength' => '20', 'label' => ''))); ?></td>
            <td></td>
            <th>製品番号</th>
            <td><?php echo($this->form->text('MstFacilityItem.item_code', array('class' => 'txt search_upper', 'maxlength' => '50', 'label' => ''))); ?></td>
            <td></td>
            <th>ロット番号</th>
            <td>
                <?php echo($this->form->text('TrnSticker.lot_no', array('class' => 'txt', 'maxlength' => '50', 'label' => ''))); ?>
            </td>
        </tr>
        <tr>
            <th>商品名</th>
            <td><?php echo($this->form->text('MstFacilityItem.item_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
            <td></td>
            <th>販売元</th>
            <td><?php echo($this->form->text('MstDealer.dealer_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
            <td></td>
            <th>シール番号</th>
            <td>
                <?php echo($this->form->text('TrnSticker.hospital_sticker_no', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?>
            </td>
        </tr>
        <tr>
            <th>規格</th>
            <td><?php echo($this->form->text('MstFacilityItem.standard', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
            <td></td>
            <th>JANコード</th>
            <td><?php echo($this->form->text('MstFacilityItem.jan_code', array('class' => 'txt', 'maxlength' => '50', 'label' => ''))); ?></td>
            <td></td>
            <th>勘定科目</th>
            <td>
                <?php echo $this->form->input('MstFacilityItem.mst_accountlist_id',array('options'=>$accountlists, 'class'=>'txt','style'=>'width:120px;','id'=>'account' , 'empty'=>true)); ?>
            </td>
        </tr>
        <tr>
            <td colspan="8" style="height:10px;"></td>
        </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn1" id="btn_Search" />
            <input type="button" class="btn btn10 print_btn" id="btn_Print" />
            <input type="button" class="btn btn5 button" id="btn_Export" />
            <input type="button" class="btn btn109" id="btn_Excel"/>
        </p>
    </div>
<br>
    <div id="results">
        <table style="width:100%;">
        <tr>
            <td><div class="DisplaySelect"><?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?></div></td>
            <td align="right"><div id="pageTop"></div></td>
        </tr>
        </table>

        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="20" rowspan="2"/>
                    <col width="150" colspan="2"/>
                    <col width="100"/>
                    <col>
                    <col>
                    <col width="140"/>
                    <col width="100"/>
                    <col width="100"/>
                    <col width="100"/>
                    <col width="40"/>
                </colgroup>
                <tr>
                    <th rowspan="2"><input type="checkbox" class="checkAll"></th>
                    <th colspan="2">得意先　／　部署</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>ロット番号</th>
                    <th>数量</th>
                    <th>売上単価</th>
                    <th>金額</th>
                    <th rowspan="2">遡及<BR>除外</th>
                </tr>
                <tr>
                    <th>勘定科目</th>
                    <th>売上日</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>部署シール</th>
                    <th>商品ID</th>
                    <th>マスタ単価</th>
                    <th>区分</th>
                </tr>
            <?php if(isset($SearchResult) && count($SearchResult) === 0 && isset($this->request->data["IsSearch"]) && $this->request->data["IsSearch"] === "1"): ?>
                <tr><td colspan="10" class="center"><span>該当するデータが存在しません</span></td></tr>
            <?php endif;?>
                <?php
                $i = 0;
                foreach ($SearchResult as $_row): ?>
                <tr>
                    <td rowspan="2" align="center">
                    <?php echo $this->form->checkbox("TrnClaim.id.{$_row[0]['id']}", array('value'=>$_row[0]['id'],'class' => 'center chk' ,'hiddenField'=>false )); ?>
                    </td>
                <td colspan="2"><?php echo h_out($_row[0]['facility_name']) ;?></td>
                <td><?php echo h_out($_row[0]['item_name']); ?></td>
                <td><?php echo h_out($_row[0]['item_code']); ?></td>
                <td><?php echo h_out($_row[0]['lot_no']); ?></td>
                <td><?php echo h_out($_row[0]['count'].$_row[0]['packing_name'] , 'right'); ?></td>
                <td class="num"><?php echo h_out($this->Common->toCommaStr($_row[0]['unit_price']),'right'); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($_row[0]['claim_price']),'right'); ?></td>
                <td rowspan="2"><?php if(strcmp($_row[0]['is_not_retroactive'],"true")==0) {print "○"; } ?></td>
            </tr>
            <tr>
                <td><?php echo h_out($_row[0]['accountlists_name']); ?></td>
                <td><?php echo h_out($_row[0]['claim_date'],'center'); ?></td>
                <td><?php echo h_out($_row[0]['standard']); ?></td>
                <td><?php echo h_out($_row[0]['dealer_name']); ?></td>
                <td><?php echo h_out($_row[0]['hospital_sticker_no'],'center'); ?></td>
                <td><?php echo h_out($_row[0]['internal_code'],'center'); ?></td>
                <td class="num"><?php echo h_out($this->Common->toCommaStr($_row[0]['sales_price']), 'right');?></td>
                <td><?php echo h_out($_row[0]['type_name'],'center'); ?></td>
            </tr>
            <?php $i++; endforeach; ?>
            </table>
        </div>
<?php if (count($SearchResult) > 0): ?>
        <div align="right" id="pageDow"></div>
        <div class="ButtonBox">
            <p class="center"><input type="button" class="common-button" id="btn_confirm" value="確認" /></p>
        </div>
<?php endif; ?>
    </div>
</form>
