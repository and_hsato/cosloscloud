<script type="text/javascript">
    $(document).ready(function(){
        $(function (){
          $('img.scroll').click(function(){
           var to_id = $(this).attr('to_id');
           var obj = $('#'+to_id);
           var offset = obj.offset();

           scrollTo(0,offset.top);
           });
        });
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">売上検索</a></li>
        <li class="pankuzu">売上変更確認</li>
        <li>売上変更結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>売上変更結果</span></h2>

<div class="Mes01">以下の売上変更を行いました。</div>
<br>
<div id="results">
    <div class="DisplaySelect">
        <table style="width:100%;">
            <tr>
                <td align="right"><div id="pageTop"></div></td>
            </tr>
        </table>
    </div>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <colgroup>
                <col width="170"/>
                <col width="100"/>
                <col>
                <col>
                <col width="160"/>
                <col width="100"/>
                <col width="100"/>
                <col width="100"/>
                <col width="40"/>
            </colgroup>
            <tr>
                <th colspan="2">得意先　／　部署</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th>ロット番号</th>
                <th>数量</th>
                <th>売上単価</th>
                <th>金額</th>
                <th rowspan="2">遡及<BR>除外</th>
            </tr>
            <tr>
                <th>勘定科目</th>
                <th>売上日</th>
                <th>規格</th>
                <th>販売元</th>
                <th>部署シール</th>
                <th>商品ID</th>
                <th>マスタ単価</th>
                <th>区分</th>
            </tr>
            <?php $i = 0;foreach ($SearchResult as $_row): ?>
            <tr>
                <td colspan="2"><?php echo h_out($_row[0]['facility_name']); ?></td>
                <td><?php echo h_out($_row[0]['item_name']); ?></td>
                <td><?php echo h_out($_row[0]['item_code']); ?></td>
                <td><?php echo h_out($_row[0]['lot_no']); ?></td>
                <td><?php echo h_out($_row[0]['count'].$_row[0]['packing_name'],'right'); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($_row[0]['unit_price']), 'right'); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($_row[0]['claim_price']), 'right') ?></td>
                <td rowspan="2">
                    <?php echo $_row[0]['is_not_retroactive'] == 'false' ? '' : '○'; ?>
                </td>
            </tr>
            <tr>
                <td><?php echo h_out($_row[0]['accountlists_name']); ?></td>
                <td><?php echo h_out($_row[0]['claim_date'],'center'); ?></td>
                <td><?php echo h_out($_row[0]['standard']); ?></td>
                <td><?php echo h_out($_row[0]['dealer_name']); ?></td>
                <td><?php echo h_out($_row[0]['hospital_sticker_no']); ?></td>
                <td><?php echo h_out($_row[0]['internal_code'],'center'); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($_row[0]['sales_price']),'right'); ?></td>
                <td><?php echo h_out($_row[0]['type_name'],'center');?></td>
            </tr>
            <?php $i++;endforeach; ?>
        </table>
    </div>
    <div align="right" id="pageDow"></div>
</div>
