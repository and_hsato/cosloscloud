<script type="text/javascript">
$(function(){
    $("#btn_Result").click(function(){
        if($("#result").val() === ''){
            alert('ファイルが選択されてません');
            return false;
        }else{
            if(confirm('CSV登録を行います。\nよろしいですか？')){
                $("#CsvImport").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/import_csv_result');
                $("#CsvImport").attr('method', 'post');
                //登録中メッセージ追加
                $('#msg').text('登録処理中です。しばらくお待ちください。');
                //送信
                $("#CsvImport").submit();
            }
        }
    });
    $('#floating-menu').css('display','none');
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>グランドマスタＣＳＶ登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>グランドマスタＣＳＶ登録</span></h2>
<h2 class="HeaddingMid">登録するマスタを選択してください</h2>
<div id="msg" class="err" style="font-size:20px"></div>

<form class="validate_form" method="post" action="" enctype="multipart/form-data" accept-charset="utf-8" id="CsvImport">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>登録マスタ</th>
                <td width="125px">
                  <?php echo $this->form->radio('csv.type' , array(1=>'') , array('checked'=>true , 'hiddenField'=>false)); ?>MSC商品[KyoMaterial]
                </td>
                <td><small>【最終更新日時：
                   <?php
                     if(isset($Memo1[0]['MstGrandKyoMaterial']['modified'])) {
                     $date1 = h($Memo1[0]['MstGrandKyoMaterial']['modified']);
                     echo date('Y年m月d日 H時i分s秒', strtotime($date1));
                     }else{
                         echo ('※現在0レコードです。');
                       }
                   ?>】</small>
                </td>
                
            </tr>
            
            <tr style="height:25px">
                <th></th>
                <td><?php echo $this->form->radio('csv.type' , array(2=>'') , array('hiddenField'=>false)); ?>保険適用[InsAppr]</td>
            <td><small>【最終更新日時：
                <?php
                    if(isset($Memo2[0]['MstGrandInsAppr']['modified'])) {
                    $date2 = h($Memo2[0]['MstGrandInsAppr']['modified']);
                    echo date('Y年m月d日 H時i分s秒', strtotime($date2));
                    }else{
                         echo ('※現在0レコードです。');
                       }
                ?>】</small>
            </td>
            </tr>
            
            <tr style="height:25px">
                <th></th>
                <td><?php echo $this->form->radio('csv.type' , array(3=>'') , array('hiddenField'=>false)); ?>メーカー[MktCo]</td>
            <td><small>【最終更新日時：
                <?php
                    if(isset($Memo3[0]['MstGrandMktCo']['modified'])) {
                    $date3 = h($Memo3[0]['MstGrandMktCo']['modified']);
                    echo date('Y年m月d日 H時i分s秒', strtotime($date3));
                    }else{
                         echo ('※現在0レコードです。');
                       }
                ?>】</small>
            </td>
            </tr>
            <tr style="height:25px">
                <th></th>
                <td><?php echo $this->form->radio('csv.type' , array(5=>'') , array('hiddenField'=>false)); ?>償還連携[DepClCdRelation]</td>
            <td><small>【最終更新日時：
                <?php
                    if(isset($Memo5[0]['MstGrandDepClCdRelation']['modified'])) {
                    $date5 = h($Memo5[0]['MstGrandDepClCdRelation']['modified']);
                    echo date('Y年m月d日 H時i分s秒', strtotime($date5));
                    }else{
                         echo ('※現在0レコードです。');
                       }
                ?>】</small>
            </td>
            </tr>
            <tr style="height:25px">
                <th></th>
                <td><?php echo $this->form->radio('csv.type' , array(6=>'') , array('hiddenField'=>false)); ?>単位[Unit]</td>
            <td><small>【最終更新日時：
                <?php
                    if(isset($Memo6[0]['MstGrandUnit']['modified'])) {
                    $date6 = h($Memo6[0]['MstGrandUnit']['modified']);
                    echo date('Y年m月d日 H時i分s秒', strtotime($date6));
                    }else{
                         echo ('※現在0レコードです。');
                       }
                ?>】</small>
            </td>
            </tr>
            <tr style="height:25px">
                <th></th>
                <td><?php echo $this->form->radio('csv.type' , array(7=>'') , array('hiddenField'=>false)); ?>定価[MatPrice]</td>
            <td><small>【最終更新日時：
                <?php                    
                    if(isset($Memo7[0]['MstGrandMatPrice']['modified'])) {
                    $date7 = h($Memo7[0]['MstGrandMatPrice']['modified']);
                    echo date('Y年m月d日 H時i分s秒', strtotime($date7));
                    }else{
                         echo ('※現在0レコードです。');
                       }
                ?>】</small>
            </td>
            </tr>
            <tr style="height:25px">
                <th></th>
                <td><?php echo $this->form->radio('csv.type' , array(8=>'') , array('hiddenField'=>false)); ?>MSC分類[MacmapClass]</td>
            <td><small>【最終更新日時：
                <?php
                    if(isset($Memo8[0]['MstGrandMacmapClass']['modified'])) {
                    $date8 = h($Memo8[0]['MstGrandMacmapClass']['modified']);
                    echo date('Y年m月d日 H時i分s秒', strtotime($date8));
                    }else{
                         echo ('※現在0レコードです。');
                       }
                ?>】</small>
            </td>
            </tr>
            <tr style="height:25px">
                <th></th>
                <td><?php echo $this->form->radio('csv.type' , array(9=>'') , array('hiddenField'=>false)); ?>償還[DepClCd]</td>
            <td><small>【最終更新日時：
                <?php
                    if(isset($Memo9[0]['MstGrandDepClCd']['modified'])) {
                    $date9 = h($Memo9[0]['MstGrandDepClCd']['modified']);
                    echo date('Y年m月d日 H時i分s秒', strtotime($date9));
                    }else{
                         echo ('※現在0レコードです。');
                       }
                ?>】</small>
            </td>
            </tr>

            <tr>
                <th>CSVファイル</th>
                <td colspan="2">
                    <?php echo $this->form->file('result',array('label'=>false,'div'=>false,'class'=>'r text validate[required]','style'=>'width:250px;','value'=>'')); ?>
                </td>
            </tr>
        </table>
    </div>

    <div class="ButtonBox">
        <input type="button" class="common-button [p2]" value="登録" id="btn_Result" name="1" />
    </div>
</form>
</div>