<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/import_csv">グランドマスタＣＳＶ登録</a></li>
        <li>グランドマスタＣＳＶ登録結果</li>
   </ul>
</div>
<h2 class="HeaddingLarge"><span>グランドマスタＣＳＶ登録</span></h2>
<div class="SearchBox">
    <?php if(count($err_result) > 0) { ?>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th class="col10">行番号</th>
                <th>エラー内容</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even" border=0>
        <?php $i=0;foreach($err_result as $err ){ ?>
            <?php foreach($err as $rownum => $msg ){ ?>
            <tr>
                <td class="col5"><?php echo h_out($rownum , 'right');?></td>
                <td><?php echo h_out($msg); ?></td>
            </tr>
            <?php } ?>
        <?php $i++; } ?>
        </table>
    </div>
    <?php }else{ ?>
    <div>
         <?php echo $type_name; ?>CSVのレコードを
         <?php echo $count; ?> 件登録しました。
    </div>
    <?php } ?>
</div>
</div>