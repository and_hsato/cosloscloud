<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/add">発注予定作成</a></li>
        <li>発注予定作成結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>発注予定作成結果</span></h2>
<div class="Mes01" style="margin-left:20px:"><?php print $message; ?></div>
<form id="order_input_form" method="post">
    <div class="results">
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <thead>
                    <tr>
                        <th>仕入先</th>
                        <th>品目数</th>
                        <th>合計金額</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($order_List as $o ){ ?>
                    <tr>
                        <td><?php echo h_out($o['name']); ?></td>
                        <td><?php echo h_out($o['count'],'right'); ?></td>
                        <td><?php echo h_out($this->Common->toCommaStr($o['price']),'right'); ?></td>
                    </tr>
                <?php } ?>
                <?php if(count($order_List)==0){ ?>
                    <tr colspan='2'><td colspan='2' align='center'>発注が作成されていません</td></tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</form>
</div>