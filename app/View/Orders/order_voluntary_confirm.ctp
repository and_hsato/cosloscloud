<script type="text/javascript">
$(document).ready(function(){

    $("#result_btn").click(function(){
        if($('.TableScroll2:last input[type=checkbox].chk:checked').length === 0){
            alert("商品を選択してください。");
            return false;
        } else {
            var hasError = false;
            $('.TableScroll2:last input[type=checkbox].chk').each(function(i){
                if(this.checked){
                    var q = $(this).parent(':eq(0)').parent(':eq(0)').find('.quantity:first');
                    //数量
                    if(q.val() == ''){
                        alert('数量を入力してください');
                        hasError = true;
                        q.focus();
                        return false ;
                    }else if(!q.val().match(/^[1-9][0-9]*/)){
                        alert('数量の入力が不正です');
                        hasError = true;
                        q.focus();
                        return false ;
                    }
                }
            });
            if(true === hasError){
                return false;
            }else{
                $("#input_form").attr('action', '<?php echo $this->webroot; ?>orders/order_voluntary_result/1').submit();
            }
        }
    });


    $("#result2_btn").click(function(){
        if($('.TableScroll2:last input[type=checkbox].chk:checked').length === 0){
            alert("商品を選択してください。");
            return false;
        } else {
            var hasError = false;
            $('.TableScroll2:last input[type=checkbox].chk').each(function(i){
                if(this.checked){
                    var q = $(this).parent(':eq(0)').parent(':eq(0)').find('.quantity:first');
                    //数量
                    if(q.val() == ''){
                        alert('数量を入力してください');
                        hasError = true;
                        q.focus();
                        return;
                    }else if(!q.val().match(/^[1-9][0-9]*/)){
                        alert('数量の入力が不正です');
                        hasError = true;
                        q.focus();
                        return;
                    }
                }
            });
            if(true === hasError){
                return false;
            }else{
                $("#input_form").attr('action', '<?php echo $this->webroot; ?>orders/order_voluntary_result/2').submit();
            }
        }
    });

    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
});  
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>orders/order_voluntary_search">任意発注</a></li>
        <li><a href="<?php echo $this->webroot; ?>orders/order_voluntary_search">任意発注(商品検索)</a></li>
        <li>任意発注確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>任意発注確認</span></h2>
<h2 class="HeaddingMid">以下の内容で発注要求の作成を行います。</h2>
<form id="input_form" class="validate_form" method="post">
    <?php echo $this->form->input('calculate_flag', array('type'=>'hidden', 'value'=>''));?>
    <?php echo $this->form->input('TrnOrder.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnOrder.order_mode', array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('Orders.className', array('type'=>'text', 'class'=>'lbl')); ?>
                    <?php echo $this->form->input('Orders.classId', array('type'=>'hidden')); ?>
                </td>
                <td width="20"></td>
                <th>備考</th>
                <td><?php echo $this->form->input('Orders.recital', array('type'=>'text', 'class'=>'lbl')); ?></td>
            </tr>
        </table>
        <hr class="Clear" />
        <h2 class="HeaddingSmall">問題ない場合は確定ボタンをクリックしてください。</h2>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th style="width: 20px;" rowspan="2"><input type="checkbox" class="checkAll" /></th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th class="col10">数量</th>
                    <th class="col15">作業区分</th>
                    <th class="col15">備考</th>
                </tr>
                <tr>
                    <th></th>
                    <th>販売元</th>
                    <th>規格</th>
                    <th colspan="3">仕入先情報</th>
                </tr>
            <?php $i = 0; foreach ($result as $r): ?>
                <tr>
                    <td rowspan="2" class="center">
                    <?php if(!empty($r['MstFacilityItem']['TransactionData'])){ ?>
                        <?php echo $this->form->checkbox('TrnOrder.id.'.$i, array('class'=>'center chk', 'hiddenField'=>false,'value'=>$r['MstFacilityItem']['id'])); ?>
                    <?php } ?>
                    </td>
                    <td><?php echo h_out($r['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_code']); ?></td>
                    <td><?php echo $this->form->input('TrnOrder.quantity.'.$r['MstFacilityItem']['id'], array('type'=>'text','maxlength'=>10,'value'=>$r['MstFacilityItem']['quantity'],'class'=>'quantity num r txt'));?></td>
                    <td><?php echo $this->form->input('TrnOrder.work_type.'.$r['MstFacilityItem']['id'], array('options'=>$order_classes_List,'empty'=>true, 'class'=>'txt')); ?></td>
                    <td><?php echo $this->form->input('TrnOrder.recital.'.$r['MstFacilityItem']['id'], array('type'=>'text','maxlength'=>200,'class'=>'txt'));?></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo h_out($r['MstFacilityItem']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['standard']); ?></td>
                    <?php if(!empty($r['MstFacilityItem']['TransactionData'])){ ?>
                      <td colspan="3"><?php echo $this->form->input('TrnOrder.transaction.'.$r['MstFacilityItem']['id'], array('options'=>$r['MstFacilityItem']['TransactionData'],  'value'=>$r['MstFacilityItem']['TrnsactionData']['is_main'],'class'=>'transaction r txt')); ?></td>
                    <?php } else { ?>
                      <td colspan="3"><?php echo h_out("仕入設定が無い、または仕入設定の仕入先に取引関係がありません"); ?></td>
                    <?php } ?>
                </tr>
                <?php $i++; endforeach; ?>
                <input type='hidden' id='cnt' value='<?php print $i; ?>'>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" id="result_btn" class="btn btn65 confirm_btn">
            <input type="button" id="result2_btn" class="btn btn64 confirm_btn">
        </div>
    </div>
</form>
