<script type="text/javascript">
$(function(){
    //検索ボタン押下
    $("#btn_Search").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_decision#search_result').submit();
    });
    //確認ボタン押下
    $("#btn_confirm").click(function(){
        if($("#order_input_form input[type=checkbox].chk:checked").length > 0 ) {
            $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_decision_confirm').submit();
        }else{
            alert('発注が選択されていません');
            return false;
        }
    });
});

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a>発注</a></li>
        <li>発注状況一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>発注状況一覧</span></h2>
<h2 class="HeaddingMid">カタログ発注の依頼状況の検索と発注確定作業が行えます。</h2>
<form id="order_input_form" class="validate_form input_form" method="post">
    <?php echo $this->form->input('search.is_search', array('type'=>'hidden')); ?>
     <h3 class="conth3">カタログ発注依頼を検索</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
<!--            <tr>
                <th>発注確定日</th>
                <td>
                    <?php echo  $this->form->input('TrnOrder.work_date',array('type'=>'text','class'=>'date r validate[required]','id'=>'datepicker1','maxlength'=>10));?>
                </td>
            </tr>
-->
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('TrnOrder.work_date',array('type'=>'hidden'));?>
                    <?php echo $this->form->input('TrnOrder.supplierName',array('type'=>'hidden' ,'id' => 'supplierName')); ?>
                    <?php echo $this->form->input('TrnOrder.supplierText',array('id' => 'supplierText','style'=>'width:60px;', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('TrnOrder.supplierCode',array('options'=>$supplier_list ,'class'=>'txt','id' => 'supplierCode', 'empty'=>true)); ?>
                </td>
                <td width="20"></td>
<!--                <th>発注種別</th>
                <td><?php echo $this->form->input('TrnOrder.orders_type', array( 'options'=> $order_type_List,'class' => 'txt','empty'=>true)); ?></td>
            </tr>
            <tr>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnOrder.classId', array('options'=>$order_classes_List, 'id'=>'classId','class' => 'txt','empty'=>true)); ?></td>
                    <?php echo $this->form->input('TrnOrder.className', array('type'=>'hidden','id'=>'className')); ?></td>
                <td></td>-->
                <th>備考</th>
                <td><?php echo $this->form->input('TrnOrder.comment', array('label' => false, 'class' => 'txt', 'maxlength'=>200)); ?></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Search" value="検索"/>
    </div>
    <div class="results">
      <a name="search_result"></a>
        <h2 class="HeaddingSmall">検索結果</h2>
        
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($order_List))); ?>
             </span>
             <span class="BikouCopy"></span>
        </div>
        
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th><input type="checkbox"  onClick="listAllCheck(this);"/></th>
                    <th>仕入先</th>
                    <th>部署</th>
                    <!--<th>発注種別</th>-->
                </tr>
                <?php foreach ($order_List as $orders) { ?>
                <tr>
                    <td class="center"><input name="data[TrnOrder][id][]" type="checkbox"  class="center chk " value="<?php echo $orders['0']['to_id'].'_'.$orders['0']['from_id'].'_'.$orders[0]['from_department_id'].'_'.$orders['0']['order_type'] ?>"/></td>
                    <td><?php echo h_out($orders[0]['to_name']); ?></td>
                    <td><?php echo h_out($orders[0]['from_name']); ?></td>
                    <!--<td><?php echo h_out($orders[0]['order_type_name'],'center'); ?></td>-->
                </tr>
                <?php } ?>
                <?php if(isset($this->request->data['search']['is_search']) && count($order_List)==0){ ?>
                <tr class="center">
                    <td colspan=3 class="center">発注データがありません</td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
    
    <?php if(count($order_List)>0){ ?>
    <div class="ButtonBox" id="box">
        <input type="button" class="common-button" id="btn_confirm" value="詳細"/>
    </div>
    <?php } ?>
</form>
    </div><!--#content-wrapper-->
