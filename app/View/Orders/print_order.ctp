<printdata>
<!-- 
    <setting>
        <layoutname>発注書.rpx</layoutname>
        <fixvalues>
            <value name="タイトル">発注書</value>
            <value name="ＸＸＸＸ">ＸＸＸＸＸ</value>
        </fixvalues>
    </setting>
 -->
<datatable>

<!-- ここは各帳票のカラム名が並ぶ。当然、タブ区切り -->
<?php
  e('<columns>'.$columns.'</columns>');
?>
<!-- loop start -->
<?php
  e('<rows>');
  //var_dump($posted);
  //exit();
  foreach($posted as $data) {
    // tab区切りのデータに加工
    $tabed = implode("\t", $data);
    e('<row>'.$tabed.'</row>');
  }
  e('</rows>');
?>
</datatable>
</printdata>
