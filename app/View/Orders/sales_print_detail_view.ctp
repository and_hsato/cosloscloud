<script type="text/javascript">
$(function(){
    //印刷ボタン押下
    $("#btn_print").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/sales').submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/sales_print">売上依頼票印刷(検索)</a></li>
        <li>売上依頼票印刷</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>売上依頼票印刷</span></h2>
<?php 
if(isset($error_message)){
  print '<div class="Mes01">'. $error_message. '</div>';
}
?>

<form id="order_input_form" method="post">
    <input type="hidden" name="data[TrnOrder][selected_facility_id]" value="<?php echo($this->Session->read('Auth.facility_id_selected')); ?>" />
    <input type="hidden" name="data[TrnOrder][user_id]" value="<?php echo($this->Session->read('Auth.MstUser.id')); ?>" />
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <colgroup>
                <col />
                <col width="145"/>
                <col />
                <col />
                <col width="80"/>
                <col width="80"/>
                <col width="80"/>
                <col width="70"/>
                <col width="70"/>
            </colgroup>
            <tr>
                <th>仕入先</th>
                <th>売上依頼番号</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th>ロット番号</th>
                <th>包装単位</th>
                <th>売上単価</th>
                <th>作業区分</th>
                <th>更新者</th>
            </tr>
            <tr>
                <th>施設／部署</th>
                <th>部署シール</th>
                <th>規格</th>
                <th>販売元</th>
                <th>売上依頼日</th>
                <th>商品ID</th>
                <th>金額</th>
                <th colspan='2'>備考</th>
            </tr>
            <?php $cnt=0; foreach ($result as $r) {?>
            <tr>
                <td>
                    <input name="data[TrnConsume][id][]" id="id_<?php echo $cnt ?>" type="hidden" value="<?php echo $r['TrnOrder']['consume_id'] ?>"/>
                    <input name="data[TrnSalesRequestHeader][id][]" id="id_<?php echo $cnt ?>" type="hidden" value="<?php echo $r['TrnOrder']['sale_request_id'] ?>"/>
                    <?php echo h_out($r['TrnOrder']['supplier_name']); ?>
                </td>
                <td><?php echo h_out($r['TrnOrder']['work_no']); ?></td>
                <td><?php echo h_out($r['TrnOrder']['item_name']); ?></td>
                <td><?php echo h_out($r['TrnOrder']['item_code']); ?></td>
                <td><?php echo h_out($r['TrnOrder']['lot_no']); ?></td>
                <td><?php echo h_out($r['TrnOrder']['unit_name']); ?></td>
                <td align='right'><?php echo h_out($this->Common->toCommaStr($r['TrnOrder']['unit_price']),'right') ?></td>
                <td><?php echo h_out($r['TrnOrder']['work_type_name']) ?></td>
                <td><?php echo h_out($r['TrnOrder']['user_name']); ?></td>
            </tr>
            <tr>
                <td><?php echo h_out($r['TrnOrder']['facility_name']); ?></td>
                <td><?php echo h_out($r['TrnOrder']['hospital_sticker_no']); ?></td>
                <td><?php echo h_out($r['TrnOrder']['standard']); ?></td>
                <td><?php echo h_out($r['TrnOrder']['dealer_name']); ?></td>
                <td align='center'><?php echo h_out($r['TrnOrder']['claim_date'],'center'); ?></td>
                <td align='center'><?php echo h_out($r['TrnOrder']['internal_code'],'center'); ?></td>
                <td align='right'><?php echo h_out($this->Common->toCommaStr($r['TrnOrder']['claim_price']),'right'); ?></td>
                <td colspan='2'><?php echo $this->form->input('TrnSalesRequest.recital.'.$r['TrnOrder']['claim_id'], array('label' => false, 'class' => 'txt','style'=>'width:90%', 'maxlength'=>200)) ?></td>
            </tr>
            <?php $cnt++; } ?>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn20" id="btn_print"/>
    </div>
</form>
