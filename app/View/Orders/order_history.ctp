<script type="text/javascript">
$(function(){
    // 検索ボタン押下
    $("#btn_Search").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_history#search_result').submit();
    });
  
    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_history');
    });
  
    //EXCELボタン押下
    $("#btn_Excel").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_excel').submit();
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_history');
    });

    $("#btn_detail").click(function(){
         if(listCheckedCheck()){
             $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_detail_history').submit();
         }else{
             alert("発注を選択してください");
         }
    });
    $("#btn_print_order").click(function(){
        if(listCheckedCheck()){
            if(!targetCheck()){
                alert("完了されていない発注が選択されています");
                return false;
            }
            $("#order_input_form").attr('action', '<?php echo $this->webroot; ?>PdfReport/order').attr('target','_blank').submit();
            $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_history').attr('target' , '_self');
        }else{
            alert("発注を選択してください");
        }
    });

    $("#btn_print_order_summary").click(function(){
        if(listCheckedCheck()){
            if(!targetCheck()){
                alert("完了されていない発注が選択されています");
                return false;
            }
            $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/summary').submit();
        }else{
            alert("発注を選択してください");
        }
    });  
    $("#btn_print_replenish").click(function(){
        if(listCheckedCheck()){
            if(!targetCheck()){
                alert("完了されていない発注が選択されています");
                return false;
            }
            $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/replenish').submit();
        }else{
            alert("発注を選択してください");
        }
    });
    $("#btn_print_sticker").click(function(){
        if(listCheckedCheck()){
            if(!targetCheck()){
                alert("完了されていない発注が選択されています");
                return false;
            }
            $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal').submit();
        }else{
            alert("発注を選択してください");
        }
    });
    $("#btn_print_cost_sticker").click(function(){
        if(listCheckedCheck()){
            if(!targetCheck()){
                alert("完了されていない発注が選択されています");
                return false;
            }
            $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/cost_history').submit();
        }else{
            alert("発注を選択してください");
        }
    });
});

function listCheckedCheck(){
    var targets = $('#cnt').val();
    var result = false;
    var condition = "";
    for(var i=0;i<=targets;i++){
        if($('#id_'+i).attr('checked')){
            if($('#condition_'+i).val() !== null && $('#condition_'+i).val() !== ""){
                condition += $('#condition_'+i).val() + ',';
            }
            result = true;
        }
    }
    $('#target').val(condition);
    return result;
}
function targetCheck(){
    var target = $('#target').val();
    if(target == ""){
        return true;
    }
    return false;
}

function keydown(){
    if(event.keyCode == 13){
        var work_no = $('#TrnOrderWorkNo').val();
        if(work_no === null||work_no === ""){
            return;
        }
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_detail_history/2').submit();
    }
}
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a>発注</a></li>
        <li>発注履歴一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>発注履歴一覧</span></h2>
<h2 class="HeaddingMid">発注履歴の検索と一覧表示、変更、削除が行えます。</h2>
<form class="validate_form search_form" id="order_input_form" method="post" action="<?php echo $this->webroot; ?>orders/order_history">
    <input type="hidden" name="data[TrnOrder][selected_facility_id]" value="<?php echo($this->Session->read('Auth.facility_id_selected')); ?>" />
    <input type="hidden" id="uid" name="data[User][user_id]" value="<?php print $this->Session->read('Auth.MstUser.id'); ?>">
    <input type="hidden" name="data[target]" id="target" value="" />
    <?php echo $this->form->input('Edi.id', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('search.is_search', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    
     <h3 class="conth3">発注履歴を検索</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>発注番号</th>
                <td><?php echo $this->form->input('search.work_no', array('label' => false,'onkeydown'=>'keydown();' , 'class' => 'txt', 'maxlength'=>16)); ?></td>
                <td width="20"></td>
                <th>発注日</th>
                <td><?php echo $this->form->input('search.work_date_start',array('class'=>'date txt validate[custom[date]]','id'=> 'datepicker1','div' => 'false','label' => 'false','maxlength' => '10')); ?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo  $this->form->input('search.work_date_end',array('class'=>'date txt validate[custom[date]]','id'=> 'datepicker2','div' => 'false','label' => 'false','maxlength' => '10')); ?>
                </td>
                <td width="20"></td>
                <td colspan=2>
                    <?php echo $this->form->input('search.is_deleted' , array('type'=>'checkbox' , 'hiddenField'=>false))?>
                    取消は表示しない
                </td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('search.supplierText',array('id' => 'supplierText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                    <?php echo $this->form->input('search.supplierCode',array('options'=>$supplier_list ,'empty'=>true,'id' => 'supplierCode', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('search.supplierName',array('type'=>'hidden' ,'id' => 'supplierName')); ?>
                </td>
                <td width="20"></td>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('search.departmentText',array('id' => 'departmentText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                    <?php echo $this->form->input('search.departmentCode',array('options'=>$department_list ,'empty'=>true,'id' => 'departmentCode', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('search.departmentName',array('type'=>'hidden' ,'id' => 'departmentName')); ?>
                </td>
                <td width="20"></td>
                <td colspan=2>
                    <?php echo $this->form->input('search.yet' , array('type'=>'checkbox' , 'hiddenField'=>false , 'label'=>'raberl'))?>
                    未発注も表示する
                </td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('search.internal_code', array('label' => false, 'class' => 'txt search_canna search_internal_code', 'maxlength'=>20)); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('search.item_code', array('label' => false, 'class' => 'txt search_upper', 'maxlength'=>20)); ?></td>
                <td></td>
                <td colspan=2>
                    <?php echo $this->form->input('search.remain_count' , array('type'=>'checkbox' , 'hiddenField'=>false , 'label'=>'raberl'))?>
                    未入荷のみ表示する
                </td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('search.item_name', array('label' => false,  'class' => 'txt search_canna', 'maxlength'=>20)); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('search.dealer_name', array('label' => false, 'class' => 'txt search_canna', 'maxlength'=>20)); ?></td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('search.standard', array('label' => false, 'class' => 'txt search_canna', 'maxlength'=>20)); ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Search" value="検索"/>
        <!--<input type="button" class="common-button" id="btn_Csv" value="CSV"/>-->
        <input type="button" class="common-button" id="btn_Excel" value="Excelファイルダウンロード"/>
        <!--<input type="button" class="btn btn27" id="btn_Shipping"/>-->
        <!--<input type="button" class="btn btn113" id="btn_Aptage"/>-->
    </div>
    <div class="results">
      <a name="search_result"></a>
        <h2 class="HeaddingSmall">検索結果</h2>

        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
             </span>
             <span class="BikouCopy"></span>
        </div>       
  
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th style="width:20px;"><input type="checkbox"  onClick="listAllCheck(this);"/></th>
                    <th style="width:135px;">発注番号</th>
                    <!--<th style="width:110px;">発注種別</th>-->
                    <th style="width:80px;">発注日</th>
                    <th>仕入先</th>
                    <th>部署</th>
                    <th style="width:85px;">登録者</th>
                    <th style="width:145px;">登録日時</th>
                    <th style="width:70px;">備考</th>
                    <th style="width:60px;">件数</th>
                </tr>

                <?php $cnt=0;foreach ($result as $r) { ?>
                <tr>
                    <td class="center"><input name="data[TrnOrderHeader][id][]" id="id_<?php echo $cnt ?>" type="checkbox" class="center chk" value="<?php echo $r['TrnOrder']['trn_order_header_id'] ?>"/>
                    <?php if(!isset($r['TrnOrder']['trn_order_header_id']) || $r['TrnOrder']['trn_order_header_id']===""){ ?>
                        <input name="data[TrnOrder][condition][]" id="condition_<?php echo $cnt ?>" type="hidden" value="<?php echo $r['TrnOrder']['department_id_to'].'_'.$r['TrnOrder']['department_id_from'].'_'.$r['TrnOrder']['order_type'] ?>"/>
                    <?php }else{ ?>
                        <input name="data[TrnOrder][condition][]" id="condition_<?php echo $cnt ?>" type="hidden" value=""/>
                    <?php } ?>
                    </td>
                    <td><?php echo h_out($r['TrnOrder']['work_no'] ,'center'); ?></td>
                    <!--<td><?php echo h_out($r['TrnOrder']['order_type_name'],'center'); ?></td>-->
                    <td><?php echo h_out($r['TrnOrder']['work_date'],'center'); ?></td>
                    <td>
                        <?php echo h_out($r['TrnOrder']['facility_name_to']); ?>
                    </td>
                    <td><?php echo h_out($r['TrnOrder']['department_name_from']); ?></td>
                    <td><?php echo h_out($r['TrnOrder']['user_name']);?></td>
                    <td><?php echo h_out($r['TrnOrder']['created'] ,'center'); ?></td>
                    <td><?php echo h_out($r['TrnOrder']['recital']);?></td>
                    <td><?php echo h_out($r['TrnOrder']['count'] . ' / ' . $r['TrnOrder']['detail_count'] , 'right'); ?></td>
                </tr>
                <?php $cnt++;  } ?>
                <?php if(count($result)==0 && isset($this->request->data['search']['is_search'])){ ?>
                <tr>
                    <td colspan=9 class="center">発注データがありません</td>
                </tr>
                <?php } ?>
                <input type='hidden' id='cnt' value='<?php print $cnt; ?>'>
            </table>
        
        </div>
    </div>
    <?php if(count($result)>0){ ?>
    <div class="ButtonBox">
        <input type="button" value="詳細" id="btn_detail" class="common-button confirm_btn"/>
        <input type="button" value="発注書印刷" id="btn_print_order" class="common-button confirm_btn"/>
        <!--<input type="button" value="" id="btn_print_order_summary" class="btn btn114 confirm_btn"/>-->
        <!--<input type="button" value="" id="btn_print_sticker" class="btn btn16 confirm_btn"/>-->
        <!--<input type="button" value="" id="btn_print_cost_sticker" class="btn btn12 confirm_btn"/>-->
        <!--<input type="button" value="" id="btn_print_replenish" class="btn btn32 confirm_btn"/>-->
    </div>
    <?php } ?>
  </div>
</form>
</div><!--#content-wrapper-->