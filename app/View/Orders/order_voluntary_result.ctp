 <div id="TopicPath">
     <ul>
         <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
         <li><a href="<?php echo $this->webroot; ?>orders/order_voluntary_search">任意発注</a></li>
         <li><a href="<?php echo $this->webroot; ?>orders/order_voluntary_search">任意発注(商品検索)</a></li>
         <li class="pankuzu">任意発注確認</li>
         <li>任意発注結果</li>
     </ul>
 </div>

 <h2 class="HeaddingLarge"><span>任意発注結果</span></h2>
 <div class="Mes01">以下の内容で発注要求の作成を行いました。</div>
 <form>

     <div class="SearchBox">
         <table class="FormStyleTable">
             <tr>
                 <th>作業区分</th>
                 <td><?php echo $this->form->input('Orders.className', array('type'=>'text',  'readonly'=>'readonly', 'class'=>'lbl')); ?></td>
                 <td width="20"></td>
                 <th>備考</th>
                 <td><?php echo $this->form->input('Orders.recital', array('type'=>'text', 'readonly'=>'readonly', 'class'=>'lbl')); ?></td>
             </tr>
         </table>

         <hr class="Clear" />

         <div class="TableHeaderAdjustment01">
             <table class="TableHeaderStyle02">
                 <tr>
                     <th class="col10">商品ID</th>
                     <th>商品名</th>
                     <th>製品番号</th>
                     <th class="col10">数量</th>
                     <th class="col15">作業区分</th>
                     <th class="col15">備考</th>
                 </tr>
                 <tr>
                     <th></th>
                     <th>販売元</th>
                     <th>規格</th>
                     <th colspan="3">仕入先情報</th>
                 </tr>
             </table>
         </div>

         <div class="TableScroll">
             <table class="TableStyle01 table-even2">
                 <?php $i = 0; foreach ($result as $r): ?>
                 <tr>
                     <td class="col10" align='center'><?php echo h_out($r['TrnOrder']['internal_code'],'center'); ?></td>
                     <td><?php echo h_out($r['TrnOrder']['item_name']); ?></td>
                     <td><?php echo h_out($r['TrnOrder']['item_code']); ?></td>
                     <td class="col10"><?php echo h_out($r['TrnOrder']['quantity'],'right'); ?></td>
                     <td class="col15"><?php echo h_out($r['TrnOrder']['work_type_name']); ?></td>
                     <td class="col15"><?php echo h_out($r['TrnOrder']['recital']);?></td>
                 </tr>
                 <tr>
                     <td></td>
                     <td><?php echo h_out($r['TrnOrder']['dealer_name']); ?></td>
                     <td><?php echo h_out($r['TrnOrder']['standard']); ?></td>
                     <td colspan="3">
                     <?php
                         $display = $r['TrnOrder']['unit_name'] . ' / ' . $this->Common->toCommaStr($r['TrnOrder']['stoking_price']) . ' / ' . $r['TrnOrder']['facility_name'];
                         if($r['TrnOrder']['facility_name'] != $r['TrnOrder']['facility_name2']){
                            $display .=  ' / ' . $r['TrnOrder']['facility_name2'];
                         }
                         echo h_out($display);
                     ?>
                     </td>
                 </tr>
                 <?php $i++; endforeach; ?>
             </table>
         </div>

     </div>
<?php echo $this->form->end(); ?>
