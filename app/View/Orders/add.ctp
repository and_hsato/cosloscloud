<script type="text/javascript">
/**
 * validation engine.
 */
$(document).ready(function(){
    $("#btn_make").click(function(){
        if(selected_check()){
            $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_result').submit();
        }
        return false;
    });

    //通常発注
    $("#order_type1").click(function(){
        getFacility(1);
    });
    //預託品補充
    $("#order_type2").click(function(){
        getFacility(2);
    });
    //非在庫品補充
    $("#order_type3").click(function(){
        getFacility(2);
    });
    //オペ用補充
    $("#order_type4").click(function(){
        getFacility(1);
    });
});


/**
 * 必須項目チェック
 */
function selected_check() {
  var checker = false;
  var tmp_chk = false;
  //使用定数
  for(var i=1;i<=3;i++){
    if($('#fixed_count'+i).attr('checked')){
      checker = true;
    }
  }
  if(!checker){
    alert("使用定数を選択してください");
    $('#fixed_count1').focus();
    return false;
  }
  //発注種別
  checker = false;
  for(var i=1;i<=4;i++){
    if($('#order_type'+i).attr('checked')){
      checker = true;
    }
  }
  if(!checker){
    alert("発注種別を選択してください");
    $('#order_type1').focus();
    return false;
  }

  //施設
  if($('#facilityCode option:selected').val() == "" || $('#facilityCode option:selected').val() == null){
    alert("施設を選択してください");
    $('#facilityCode').focus();
    return false;
  }

  return true;
}

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>発注予定作成</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>発注予定作成</span></h2>
<!--<h2 class="HeaddingMid">作成条件を指定してください</h2>-->
<form id="order_input_form" class="validate_form input_form" method="post">
    <?php echo $this->form->input('TrnOrder.token', array('type'=>'hidden')); ?>
    <input type="hidden" name="mode2" value="2" />
    <div hidden class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>使用定数</th>
                <td>
                    <input type="radio" id='fixed_count1' name='data[TrnOrder][fixed_count]' value="1" checked>定数
                    <input type="radio" id='fixed_count2' name='data[TrnOrder][fixed_count]' value="2" >休日定数
                    <input type="radio" id='fixed_count3' name='data[TrnOrder][fixed_count]' value="3" >予備定数
                </td>
                <td width="20"></td>
                <td></td>
            </tr>
            <tr>
                <th>発注種別</th>
                <td>
                    <input type="radio" id='order_type1' name='data[TrnOrder][order_type]' value="<?php echo Configure::read('OrderTypes.nomal');?>">通常品発注
                    <input type="radio" id='order_type2' name='data[TrnOrder][order_type]' value="<?php echo Configure::read('OrderTypes.replenish');?>">預託品発注
                    <input type="radio" id='order_type3' name='data[TrnOrder][order_type]' value="<?php echo Configure::read('OrderTypes.nostock');?>" checked>非在庫品発注
                    <?php if($this->Session->read('Auth.Config.OpeSetUseType') == '1'){ ?>
                    <input type="radio" id='order_type4' name='data[TrnOrder][order_type]' value="<?php echo Configure::read('OrderTypes.opestock');?>">オペ倉庫品発注
                    <?php } ?>
                </td>
                <td width="20"></td>
                <td></td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('TrnOrder.facilityName',array('id' => 'facilityName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnOrder.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'r txt  validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('TrnOrder.facilityCode',array('options'=>$facility_list ,'id' => 'facilityCode','class'=>'r txt  validate[required]','empty'=>true) ); ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnOrder.work_type', array('options'=>$order_classes_List, 'empty'=>true,'class' => 'txt' , 'id'=>'classId')); ?>
                    <?php echo $this->form->input('TrnOrder.work_type_name', array('type' => 'hidden' , 'id'=>'className')); ?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('TrnOrder.departmentName',array('id' => 'departmentName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnOrder.departmentText',array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('TrnOrder.departmentCode', array('options'=>$department_list, 'id' =>'departmentCode', 'class' =>'txt','empty'=>true)); ?>
                </td>
                <td width="20"></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnOrder.recital', array('label' => false, 'class' => 'txt', 'maxlength'=>200)); ?></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_make" value="予定作成"/>
    </div>
</form>
</div>