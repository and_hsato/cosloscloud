<script type="text/javascript">
$(function(){
    //検索ボタン押下
    $("#btn_Search").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_print').submit();
    });
  
    //明細ボタン押下
    $("#btn_detail").click(function(){
        var objRadioCol = document.getElementsByName("data[TrnConsume][id][]");
        var cnt=0;
        for(var i=0;i<objRadioCol.length;i++){
            if(objRadioCol[i].checked){
                cnt++;
            }
        }
        if(cnt != 0){
            $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_print_detail').submit();
        } else {
            alert('必ず一つは選択してください');
        }
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>売上依頼票印刷</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>売上依頼票印刷(検索)</span></h2>
<form id="order_input_form" method="post" class="validate_form search_form ">
    <?php echo $this->form->input('TrnOrder.user_id' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('search.is_search' , array('type'=>'hidden')) ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>売上依頼番号</th>
                <td><?php echo $this->form->input('TrnSalesRequestHeader.work_no', array('label' => false,'class' => 'txt', 'maxlength'=>20)); ?></td>
                <td width="20"></td>
                <th>売上日</th>
                <td>
                    <?php echo $this->form->input('TrnConsume.work_date_start',array('class'=>'date validate[custom[date]]','id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('TrnConsume.work_date_end',array('class'=>'date validate[custom[date]]','id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                </td>
                <td width="20"></td>
                <td colspan=2>
                    <?php echo $this->form->input('TrnConsume.trn_consume_headers_id' , array('type'=>'checkbox' , 'hiddenField'=>false))?>
                    印刷済は表示しない
                </td>
            </tr>
            <tr>
                <th>売上依頼施設</th>
                <td>
                    <?php echo $this->form->input('MstFacility.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;', 'maxlength'=>20)); ?>
                    <?php echo $this->form->input('MstFacility.facilityCode',array('options'=>$facilityList ,'empty'=>true,'id' => 'facilityCode' , 'class'=>'txt')); ?>
                    <?php echo $this->form->input('MstFacility.facilityName',array('type'=>'hidden','id' => 'facilityName')); ?>
                </td>
                <td width="20"></td>
                <th>売上依頼部署</th>
                <td>
                    <?php echo $this->form->input('MstDepartment.departmentText',array('id'=>'departmentText' , 'class' => 'txt','style'=>'width:60px;')) ?>
                    <?php echo $this->form->input('MstDepartment.departmentCode',array('options'=>$departmentList ,'empty'=>true,'id'=>'departmentCode','class'=>'txt')); ?>
                    <?php echo $this->form->input('MstDepartment.departmentName',array('type'=>'hidden' ,'id' => 'departmentName')); ?>
                </td>
                <td width="20"></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->input('TrnSticker.lot_no',array('label' => false,'class' => 'txt search_canna')); ?></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code', array('label' => false,'class' => 'txt search_canna search_internal_code', 'maxlength'=>20)); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_code', array('label' => false, 'class' => 'txt search_upper', 'maxlength'=>100)); ?></td>
                <td></td>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_name' , array('class'=>'txt search_canna')) ?></td>
            </tr>
            <tr>
                <th>販売元</th>
                <td>
                    <?php echo $this->form->input('MstDealer.dealer_name', array('label' => false, 'class' => 'txt search_canna')); ?>
                </td>
                <td></td>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.stamdard' , array('class'=>'txt search_canna' , 'maxlength'=>100)); ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('TrnConsume.work_type', array('options'=>$order_classes_List, 'class' => 'txt' , 'empty'=>true)); ?></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" value="" class="btn btn1" id="btn_Search"/>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <div align='right' style='float:right;'></div>
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($sales_request_list))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col width="145"/>
                    <col />
                    <col />
                    <col width="80"/>
                    <col width="80"/>
                    <col width="80"/>
                    <col width="70"/>
                    <col width="70"/>
                </colgroup>
                <tr>
                    <th rowspan='2'><input type="checkbox"  onClick="listAllCheck(this);"/></th>
                    <th>仕入先</th>
                    <th>売上依頼番号</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>ロット番号</th>
                    <th>包装単位</th>
                    <th>売上単価</th>
                    <th>作業区分</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th>施設／部署</th>
                    <th>部署シール</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>売上依頼日</th>
                    <th>商品ID</th>
                    <th>金額</th>
                    <th colspan='2'>備考</th>
                </tr>
                <?php $cnt=0; foreach ($sales_request_list as $sales_request) { ?>
                <tr>
                    <td class="center" rowspan='2'><input name="data[TrnConsume][id][]" id="id_<?php echo $cnt ?>" type="checkbox" class="center chk " value="<?php echo $sales_request['TrnConsume']['id'] ?>"/></td>
                    <td><?php echo h_out($sales_request['MstFacilityTo']['facility_name']); ?></td>
                    <td><?php echo h_out($sales_request['TrnSalesRequestHeader']['work_no']); ?>
                        <input name="data[TrnConsume][trn_sales_request_header_id][<?php echo $sales_request['TrnConsume']['id']?>]" id="trn_sales_request_header_id<?php echo $cnt ?>" type="hidden" value="<?php echo $sales_request['TrnSalesRequestHeader']['id'] ?>"/>
                    </td>
                    <td><?php echo h_out($sales_request['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($sales_request['MstFacilityItem']['item_code']); ?></td>
                    <td><?php echo h_out($sales_request['TrnSticker']['lot_no']); ?></td>
                    <td><?php echo h_out($sales_request['MstItemUnit']['unit_name']); ?></td>
                    <td align='right'><?php echo h_out($this->Common->toCommaStr($sales_request['TrnClaim']['unit_price']),'right' ); ?></td>
                    <td><?php echo h_out($sales_request['TrnConsume']['work_type']); ?></td>
                    <td><?php echo h_out($sales_request['MstUser']['user_name']); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($sales_request['MstFacilitySale']['facility_name']."/".$sales_request['MstDepartmentSale']['department_name']); ?></td>
                    <td><?php echo h_out($sales_request['TrnSticker']['hospital_sticker_no']); ?></td>
                    <td><?php echo h_out($sales_request['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($sales_request['MstDealer']['dealer_name']); ?></td>
                    <td align='center'><?php echo h_out($sales_request['TrnClaim']['claim_date'],'center'); ?></td>
                    <td align='center'><?php echo h_out($sales_request['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td align='right'><?php h_out($this->Common->toCommaStr($sales_request['TrnClaim']['claim_price']),'right'); ?></td>
                    <td colspan='2'>
                        <?php echo $this->form->input('TrnSalesRequest.recital.'.$sales_request['TrnClaim']['id'], array('label' => false, 'class' => 'txt','style'=>'width:93%', 'maxlength'=>200)) ?>
                    </td>
                </tr>
                <?php $cnt++;  } ?>
                <?php if(count($sales_request_list)==0 && isset($this->request->data['search']['is_search'])){ ?>
                <tr>
                    <td colspan=10 align='center'>印刷可能な売上依頼票がありません</td>
                </tr>
                <?php } ?>
            </table>
        </div>
        <div align='right' style='margin-right:25px;'></div>
    </div>
    <?php if(count($sales_request_list) >0 ) { ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn2" value="" id="btn_detail"/>
    </div>
    <?php } ?>
</form>
<h3 id='foot'></h3>
