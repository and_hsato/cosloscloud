<script type="text/javascript">
$(function(){
    $("#btn_print").click(function(){
       $("#order_input_form").attr('action', '<?php echo $this->webroot; ?>orders/sales_print_do_print').submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>売上依頼票印刷</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>売上依頼票印刷</span></h2>
<form id="order_input_form" method="post">
			<div class="TableScroll">
				<table class="TableStyle01">
					<colgroup>
						<col />
            <col />
					</colgroup>
					<tr>
						<th>仕入先</th>
						<th>売上依頼番号</th>
						<th>商品名</th>
						<th>製品番号</th>
						<th>ロット番号</th>
						<th>包装単位</th>
						<th>売上単価</th>
						<th>作業区分</th>
						<th>更新者</th>
					</tr>
					<tr>
						<th>施設／部署</th>
						<th>売上依頼日</th>
						<th>規格</th>
						<th>販売元</th>
						<th>部署シール</th>
						<th>商品ID</th>
						<th>金額</th>
						<th colspan='2'>備考</th>
					</tr>
<?php
if(!empty($sales_request_list)){ 
	$cnt=0;
	$total=0;
	$rows_code = "";
	
	foreach ($sales_request_list as $sales_request) {
	  $work_type = "";
	  $per_unit = "";
	  if(isset($sales_request['TrnConsume']['work_type'])){
	    $work_type = $order_classes_List[$sales_request['TrnConsume']['work_type']];
	  }
	  if(isset($sales_request['MstItemUnit']['per_unit'])){
	    $per_unit = "(".$sales_request['MstItemUnit']['per_unit'].$sales_request['MstItemUnit']['MstParUnitName']['unit_name'].")";
	  }
		//セル背景色調整
		if($cnt%2==0){$class=" ";} else {$class=" class=\"odd\"";}
		print "\t\t\t\t\t<tr".$class.">\n";
		print "\t\t\t\t\t\t<input name=\"data[TrnConsume][id][]\" id=\"id_".$cnt."\" type=\"hidden\" value=\"".$sales_request['TrnConsume']['id']."\"/>\n";
		print "\t\t\t\t\t\t<input name=\"data[TrnSalesRequestHeader][id][]\" id=\"id_".$cnt."\" type=\"hidden\" value=\"".$sales_request['TrnClaim']['TrnSalesRequestHeader']['id']."\"/>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['MstDepartment']['MstFacility']['facility_name'])){print $sales_request['MstDepartment']['MstFacility']['facility_name'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['TrnConsume']['TrnSalesRequestHeader']['work_no'])){
		  print $sales_request['TrnConsume']['TrnSalesRequestHeader']['work_no'];
		}
		"</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['MstItemUnit']['MstFacilityItem']['item_name'])){print $sales_request['MstItemUnit']['MstFacilityItem']['item_name'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['MstItemUnit']['MstFacilityItem']['item_code'])){print $sales_request['MstItemUnit']['MstFacilityItem']['item_code'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['TrnConsume']['TrnSticker']['lot_no'])){print $sales_request['TrnConsume']['TrnSticker']['lot_no'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['MstItemUnit']['MstUnitName']['unit_name'])){print $sales_request['MstItemUnit']['MstUnitName']['unit_name'];};
		print $per_unit."</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['MstItemUnit']['MstFacilityItem']['unit_price'])){print $sales_request['MstItemUnit']['MstFacilityItem']['unit_price'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td>".$work_type."</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['MstUser']['user_name'])){print $sales_request['MstUser']['user_name'];}
		print "</td>\n";
		print "\t\t\t\t\t</tr>\n";
		print "\t\t\t\t\t<tr".$class.">\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['TrnConsume']['MstDepartment']['MstFacility']['facility_name'])){print $sales_request['TrnConsume']['MstDepartment']['MstFacility']['facility_name'];}
		print "/";
		if(isset($sales_request['TrnConsume']['MstDepartment']['department_name'])){print $sales_request['TrnConsume']['MstDepartment']['department_name'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['TrnClaim']['claim_date'])){print $sales_request['TrnClaim']['claim_date'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['MstItemUnit']['MstFacilityItem']['standard'])){print $sales_request['MstItemUnit']['MstFacilityItem']['standard'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['MstItemUnit']['MstFacilityItem']['MstDealer']['dealer_name'])){print $sales_request['MstItemUnit']['MstFacilityItem']['MstDealer']['dealer_name'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['TrnConsume']['TrnSticker']['hospital_sticker_no'])){print $sales_request['TrnConsume']['TrnSticker']['hospital_sticker_no'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['MstItemUnit']['MstFacilityItem']['internal_code'])){print $sales_request['MstItemUnit']['MstFacilityItem']['internal_code'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td>";
		if(isset($sales_request['TrnClaim']['claim_price'])){print $sales_request['TrnClaim']['claim_price'];}
		print "</td>\n";
		print "\t\t\t\t\t\t<td colspan='2'>".$this->form->input('TrnSalesRequest.recital.'.$sales_request['TrnClaim']['id'], array('label' => false, 'class' => 'txt','style'=>'width:215px'))."</td>\n";
		print "\t\t\t\t\t</tr>\n";
		$cnt++;
	}
?>
				</table>
			</div>
		</div>
		<div class="ButtonBox">
			<span class="button"><input type="button" value="" class="btn btn20" id="btn_print"/></span>
		</div>
	</form>

<?php
}
?>
