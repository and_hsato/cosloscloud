<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a>発注</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/order_history">発注履歴一覧</a></li>
        <li class="pankuzu">発注履歴詳細</li>
        <li>発注取消完了</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>発注取消完了</span></h2>
<h2 class="HeaddingMid">以下の発注を取り消しました。</h2>
<form id="order_input_form" method="post">
    <div class="results">
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2">
                <colgroup>
                    <col width="135"/>
                    <col />
                    <col width="120"/>
                    <col />
                    <col />
                    <col width="80"/>
                    <col width="50"/>
                    <col width="70"/>
                </colgroup>
                <thead>
                <tr>
                    <th>発注番号</th>
                    <th>仕入先</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>発注数</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th>発注日</th>
                    <th>部署</th>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>残数</th>
                    <th>備考</th>
                </tr>
                </thead>
                <tbody>
<?php
  if(!empty($order_List)){
  $cnt=0;
  foreach ($order_List as $orders) {
    //セル背景色調整
    // 直納発注は消す
    $type_name = Configure::read('OrderTypes.orderTypeName');
    $orders[0]['発注種別名'] =$type_name[$orders[0]['発注種別']];
    ?>
                <tr>
                    <td><?php echo h_out($orders[0]['発注番号']); ?></td>
                    <td>
                        <?php echo h_out($orders[0]['仕入先']);?>
                    </td>
                    <td><?php echo h_out($orders[0]['商品id'],'center'); ?></td>
                    <td><?php echo h_out($orders[0]['商品名']); ?></td>
                    <td><?php echo h_out($orders[0]['製品番号']); ?></td>
                    <td><?php echo h_out($orders[0]['包装単位']); ?></td>
                    <td><?php echo h_out($orders[0]['発注数'],'right'); ?></td>
                    <td><?php echo h_out($orders[0]['更新者']); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($orders[0]['発注日'],'center'); ?></td>
                    <td><?php echo h_out($orders[0]['部署']); ?></td>
                    <td></td>
                    <td><?php echo h_out($orders[0]['規格']); ?></td>
                    <td><?php echo h_out($orders[0]['販売元名']); ?></td>
                    <td><?php echo h_out(number_format($orders[0]['仕入価格'],2),'right'); ?></td>
                    <td><?php echo h_out($orders[0]['残数'],'right'); ?></td>
                    <td><?php echo h_out($orders[0]['備考']); ?></td>
                </tr>
                <?php $cnt++; }?>
                </tbody>
            </table>
        </div>
    </div>
<form>

<?php
  }
?>

</div><!--#content-wrapper-->