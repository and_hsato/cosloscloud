<script type="text/javascript">
$(function(){
    $("#btn_print_order").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?>PdfReport/order').attr('target','_blank').submit();
    });
    $("#btn_print_supply").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/replenish');
        $("#order_input_form").submit();
    });
    $("#btn_print_salse").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_print');
        $("#order_input_form").submit();
    });
    $("#btn_print_hospital").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/dummy');
        $("#order_input_form").submit();
    });
    $("#btn_print_cost").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/cost');
        $("#order_input_form").submit();
    });
    $("#btn_print_order_summary").click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/summary');
        $("#order_input_form").submit();
    });
  
});
</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a>発注</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/order_decision">発注状況一覧</a></li>
        <li class="pankuzu">発注詳細</li>
        <li>発注完了</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>発注完了</span></h2>
<h2 class="HeaddingMid">以下商品の発注データを作成しました。こちらのページから発注書の印刷が行えますので、仕入先へFAXしてください。</h2>

<form id="order_input_form" method="post">
    <input type="hidden" name="data[TrnOrder][selected_facility_id]" value="<?php echo($this->Session->read('Auth.facility_id_selected')); ?>" />
    <input type="hidden" id="uid" name="data[User][user_id]" value="<?php print $this->Session->read('Auth.MstUser.id'); ?>">
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>

    <?php foreach($order_header_ids as $k=>$v){ ?>
    <?php echo $this->form->input("TrnOrderHeader.id.{$k}" , array('type'=>'hidden' , 'value'=>$v));?>
    <?php } ?>
  
    <?php foreach($order_ids as $k=>$v){ ?>
    <?php echo $this->form->input("TrnOrder.id.{$k}" , array('type'=>'hidden' , 'value'=>$v));?>
    <?php } ?>
    
    
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>発注日</th>
                <td class="border-none"><?php echo  $this->form->input('TrnOrder.work_date',array('type'=>'text','id' => 'work_date',  'class' => 'lbl', 'style'=>'width: 100px;', 'readonly'=>'readonly')); ?></td>
            </tr>
        </table>
    </div>
    <?php foreach($result as $r){?>
    <hr class="Clear" />
    
    <h2 class="HeaddingMid">
        <?php echo $r[0]['TrnOrder']['supplier_name']; ?>
    </h2>
    
         <div class="results">  
    <h2 class="HeaddingSmall"><!--<?php echo $r[0]['TrnOrder']['order_type_name']; ?>-->
    <?php if(Configure::read('OrderTypes.replenish') == $r[0]['TrnOrder']['order_type'] || Configure::read('OrderTypes.nostock') == $r[0]['TrnOrder']['order_type'] || Configure::read('OrderTypes.ExNostock') == $r[0]['TrnOrder']['order_type'] ){ ?>
    <?php echo $r[0]['TrnOrder']['department_name'] ?>
    <?php } ?>
    </h2>
    </div>
    
    <div class="TableScroll">
    
    <div class="results">
    
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
               <!--     <th style="width:20px;" rowspan="2"></th> -->
                    <th class="col10">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col20">製品番号</th>
                    <th class="col15">包装単位</th>
                    <th class="col10">発注済</th>
                    <th class="col10">数量</th>
                   <!-- <th class="col15">作業区分</th> -->
                </tr>
                <tr>
                    <th>前回発注日</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>在庫数</th>
                    <th >備考</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even2">
            <?php $i=1;foreach($r as $o){?>
                <tr>
                <!--    <td style="width:20px;" rowspan="2" class="center"></td> -->
                    <td class="col10" align='center'><?php echo h_out($o['TrnOrder']['internal_code'] , 'center')?></td>
                    <td class="col20"><?php echo h_out($o['TrnOrder']['item_name']); ?></td>
                    <td class="col20"><?php echo h_out($o['TrnOrder']['item_code']);?></td>
                    <td class="col15"><?php echo h_out($o['TrnOrder']['unit_name']);?></td>
                    <td class="col10"><?php echo h_out($o['TrnOrder']['remain_count'], 'right');?></td>
                    <td class="col10"><?php echo $this->form->input('TrnOrder.quantity.'.$o['TrnOrder']['id'], array('class' => 'lbl num'));?></td>
              <!--      <td class="col15"><?php echo $this->form->input('TrnOrder.order_details_class.'.$o['TrnOrder']['id'], array('options'=>$order_classes_List,'empty'=>true ,'class' => 'txt'));?></td> -->
                </tr>
                <tr>
                    <td><?php echo h_out($o['TrnOrder']['last_order_date'], 'center');?></td>
                    <td><?php echo h_out($o['TrnOrder']['standard']);?></td>
                    <td><?php echo h_out($o['TrnOrder']['dealer_name']);?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($o['TrnOrder']['transaction_price']).'円','right');?></td>
                    <td><?php echo h_out($o['TrnOrder']['stock_count'],'right');?></td>
              <td>
                        <?php echo $this->form->input('TrnOrder.comment.'.$o['TrnOrder']['id'] , array('type'=>'text' , 'class'=>'lbl'));?> 
                    </td> 
                </tr>
            <?php $i++; } ?>
            </table>
        </div>
                </div>
        <?php } ?>
    </div>

    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_print_order" value="発注書印刷"/>
        <!--<input type="button" class="btn btn114" id="btn_print_order_summary"/>-->
        <?php if($deposit_flg){ ?>
        <input type="button" class="btn btn32" id="btn_print_supply"/>
        <input type="button" class="btn btn20" id="btn_print_salse"/>
        <input type="button" class="btn btn16" id="btn_print_hospital"/>
        <input type="button" class="btn btn12" id="btn_print_cost"/>
        <?php } ?>
    </div>
</form>
</div><!--#content-wrapper-->