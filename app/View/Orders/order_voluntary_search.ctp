<script type="text/javascript">

$(function(){
    $('.checkAll_1').click(function(){
        $('#searchTable input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#cartForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });


    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.attr('id') == 'searchForm'){ // 検索画面だったら
                $("#search_btn").click();
            }
        }
    } );
  
    //検索ボタン押下
    $("#search_btn").click(function(){
        var tempId = '';
        $("#cartForm input[type=checkbox].chk").each(function(){
            if($(this).attr("checked") == true){
                //hidden項目にID追加
                if(tempId==''){
                    tempId = $(this).val();
                }else{
                    tempId += "," + $(this).val();
                }
            }
        });
        $("#tempId").val(tempId);

        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_voluntary_search').submit();
    });

    //数量を指定して登録ボタン押下
    $("#specified_confirm").click(function(){
        var selectId = ''
        if($("#cartForm input[type=checkbox].chk").length > 0) {
            $("#cartForm input[type=checkbox].chk").each(function(){
                if($(this).attr("checked") == true){
                    if(selectId==''){
                        selectId = $(this).val();
                    }else{
                        selectId += "," + $(this).val();
                    }
                }
            });
            $("#selectId").val(selectId);
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_voluntary_confirm/1').submit();
        }else{
            alert("商品が選択されていません");
        }
    });

    //数量を計算して登録ボタン押下
    $("#calculated_confirm").click(function(){
        var selectId = ''
        if($("#cartForm input[type=checkbox].chk").length > 0) {
            $("#cartForm input[type=checkbox].chk").each(function(){
                if($(this).attr("checked") == true){
                    if(selectId==''){
                        selectId = $(this).val();
                    }else{
                        selectId += "," + $(this).val();
                    }
                }
            });
            $("#selectId").val(selectId);
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_voluntary_confirm/2').submit();
        }else{
            alert("商品が選択されていません");
        }
    });

    //選択ボタン押下
    $("#cmdSelect").click(function(){
        var tempId = $("#tempId").val();

        $("#searchForm input[type=checkbox].chk").each(function(){
            if($(this).attr("checked") == true){
                //全選択用にチェックボックスのクラス変更
                //選択行を追加
                if($('#addTable tr').length%2 === 0){
                    $(this).parents('tr:eq(0)').removeClass('odd').appendTo('#addTable');
                }else{
                    $(this).parents('tr:eq(0)').removeClass('odd').addClass('odd').appendTo('#addTable');
                }
                $('#searchTable').find('tr').removeClass('odd').end().find('tr:odd').addClass('odd');

                //hidden項目にID追加
                if(tempId==''){
                    tempId = $(this).val();
                }else{
                    tempId += "," + $(this).val();
                }
            }
        });

        $("#tempId").val(tempId);
    });
});
</script>


<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/order_voluntary_search">任意発注</a></li>
        <li>任意発注(商品検索)</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>任意発注</span></h2>
<h2 class="HeaddingMid">任意発注を行いたい商品を選択してください。</h2>

<form class="validate_form search_form input_form" id="searchForm" method="post" action="order_voluntary_confirm">
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden')); ?>
    <?php echo $this->form->input('select.tempId',array('type'=>'hidden','id' =>'tempId')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('Orders.classId', array('options'=>$order_classes_List, 'empty'=>true,'id'=>'classId', 'class'=>'txt')); ?>
                    <?php echo $this->form->input('Orders.className', array('type'=>'hidden', 'id'=>'className')); ?>
                </td>
                <td width="20"></td>
                <th>備考</th>
                <td><?php echo $this->form->input('Orders.recital', array('type'=>'text', 'class'=>'txt')); ?></td>
            </tr>
        </table>
        <h2 class="HeaddingLarge2">検索条件</h2>
        <table class="FormStyleTable">
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code', array('type'=>'text', 'class'=>'txt search_upper search_internal_code')); ?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_code', array('type'=>'text', 'class'=>'txt search_upper')); ?></td>
                <td width="20"></td>
                <th><?php echo $this->Session->read('Auth.Config.SpareKey1Title'); ?></th>
                <td><?php echo $this->form->text('MstFacilityItem.spare_key1', array('type'=>'text', 'class'=>'txt')); ?></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_name', array('type'=>'text','class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFacilityItem.dealer_name', array('type'=>'text','class'=>'txt search_canna')); ?></td>
                <td></td>
                <th><?php echo $this->Session->read('Auth.Config.SpareKey2Title'); ?></th>
                <td><?php echo $this->form->text('MstFacilityItem.spare_key2', array('type'=>'text', 'class'=>'txt')); ?></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFacilityItem.standard_name', array('type'=>'text', 'class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('MstFacilityItem.jan_code', array('type'=>'text','class'=>'txt')); ?></td>
                <td></td>
                <th><?php echo $this->Session->read('Auth.Config.SpareKey3Title'); ?></th>
                <td><?php echo $this->form->text('MstFacilityItem.spare_key3', array('type'=>'text', 'class'=>'txt')); ?></td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center"><input type="button" class="btn btn1" id="search_btn"/></p>
        </div>

        <div id="results">
            <hr class="Clear" />
            <div class="DisplaySelect">
                <div align='right' style='margin-right:25px; float:right;'>
                </div>
                <?php echo $this->element('limit_combobox',array('result'=>count($facility_item))); ?>
                <span class="BikouCopy"></span>
            </div>
            <div class="">
                <div class="TableHeaderAdjustment01">
                    <table class="TableHeaderStyle01">
                        <tr>
                            <th style="width:20px;"><input type="checkbox" class="checkAll_1" /></th>
                            <th class="col10">商品ID</th>
                            <th>商品名</th>
                            <th>規格</th>
                            <th>製品番号</th>
                            <th>販売元</th>
                        </tr>
                    </table>
                </div>
                <table class="TableStyle01 table-even" id="searchTable">
                    <colgroup>
                        <col>
                        <col align="center">
                        <col>
                        <col>
                        <col>
                        <col>
                    </colgroup>
                    <?php $i = 0; foreach ($facility_item as $item): ?>
                        <tr id="<?php echo $i; ?>">
                            <td style="width:20px;" class="center"><?php echo $this->form->checkbox('MstFacilityItemList.'.$i.'.id', array('class'=>'center chk', 'value'=>$item['MstFacilityItem']['id'])); ?></td>
                            <td class="col10"><?php echo h_out($item['MstFacilityItem']['internal_code'],'center'); ?></td>
                            <td><?php echo h_out($item['MstFacilityItem']['item_name']); ?></td>
                            <td><?php echo h_out($item['MstFacilityItem']['standard']); ?></td>
                            <td><?php echo h_out($item['MstFacilityItem']['item_code']); ?></td>
                            <td><?php echo h_out($item['MstDealer']['dealer_name']); ?></td>
                        </tr>
                    <?php $i++; endforeach; ?>
                    <?php if(count($facility_item) == 0 && isset($this->request->data['search']['is_search'])){ ?>
                        <tr><td colspan="7" class="center"> 該当するデータはありません。 </td></tr>
                    <?php } ?>
                </table>
            </div>
            <div class="ButtonBox">
                <p class="center"><input type="button" class="btn btn4" id="cmdSelect" /></p>
            </div>
            <div id="cartForm">
                <?php echo $this->form->hidden('select.id',array('id' =>'selectId')); ?>

                <h2 class="HeaddingSmall">選択商品</h2>

                <div class="">
                    <div class="TableHeaderAdjustment01">
                        <table class="TableHeaderStyle01">
                            <tr>
                                <th style="width:20px;"><input type="checkbox" class="checkAll_2" checked /></th>
                                <th class="col10">商品ID</th>
                                <th>商品名</th>
                                <th>規格</th>
                                <th>製品番号</th>
                                <th>販売元</th>
                            </tr>
                        </table>
                    </div>
                    <div class="TableStyle01 itemSelectedTable" id="itemSelectedTable">
                        <table class="TableStyle01 table-even" id="addTable" style="margin:0;padding:0;">
                            <colgroup>
                                <col>
                                <col align="center">
                                <col>
                                <col>
                                <col>
                                <col>
                            </colgroup>
                            <?php $i = 0; foreach ($select_item as $row): ?>
                            <tr id="<?php echo $i; ?>">
                                <td style="width:20px;" class="center">
                                    <?php echo $this->form->checkbox('MstFacilityItemList.'.$i.'.id', array('class'=>'center chk', 'value'=>$row['MstFacilityItem']['id'], "checked"=>"checked")); ?>
                                </td>
                                <td class="col10"><?php echo h_out($row['MstFacilityItem']['internal_code'],'center'); ?></td>
                                <td><?php echo h_out($row['MstFacilityItem']['item_name']); ?></td>
                                <td><?php echo h_out($row['MstFacilityItem']['standard']); ?></td>
                                <td><?php echo h_out($row['MstFacilityItem']['item_code']); ?></td>
                                <td><?php echo h_out($row['MstDealer']['dealer_name']); ?></td>
                            </tr>
                            <?php $i++; endforeach; ?>
                        </table>
                    </div>
                </div>

                <div class="ButtonBox">
                    <input type="button" id="specified_confirm" class="btn btn68 confirm_btn">
                    <input type="button" id="calculated_confirm" class="btn btn69 confirm_btn">
                </div>
                <div align='right' style='margin-right:25px; margin-top:10px;'>
                </div>
            </div>
        </div>
    </div>

<h3 id='foot'></h3>
<?php echo $this->form->end(); ?>
