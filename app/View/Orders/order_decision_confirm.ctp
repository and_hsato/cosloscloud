<script type="text/javascript">
$(function(){
    $('#btn_err_chk').click(function(){
        $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_err_csv').submit();
    });
  
    $(".checkAll").click(function(){
        $('input[type=checkbox].' + $(this).attr('id')).attr('checked',$(this).attr('checked'));
    });
    $("#btn_decision").click(function(){
        var ids = "";
        var targets = "";
        var order_num = $('#order_num').val();
        for(var num=0;num<order_num;num++){
            targets = $('#check_target_'+num).val();
            for(var i=0;i<targets;i++){
                if($('#order_check_'+num+'_'+i).attr('checked')){
                    ids += $('#order_check_'+num+'_'+i).val() + ',';
                    // 数量チェック
                    var quantity = $('#order_quantity_'+num+'_'+i).val();
                    var number_checker = true;
                    if(!quantity.match(/^[1-9]+[0-9]*$/)){
                        number_checker = false;
                    }
                    if(quantity == ""){
                        number_checker = false;
                    }
                    if(number_checker === false){
                        alert("数量は1以上の半角数値で入力してください");
                        $('#order_quantity_'+num+'_'+i).focus();
                        return false;
                    }
                }
            }
        }
        if($('input[type=checkbox][aptage_error]:checked').length > 0){
            if(window.confirm("アプテージ連携エラーが発生しています。このまま発注しますか？")){
                $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_decision_result').submit();
            }
            return false;
        }
        if($("input.chk:checked").length > 0){
            $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_decision_result').submit();
        }else{
            alert("発注が選択されていません");
            return false;
        }
    });
    $(window).load(function () {
        if($('.err_chk').length == 0){
           $("#btn_err_chk").hide();
        }
    });
});

</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home">TOP</a></li>
        <li><a>発注</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_decision">発注状況一覧</a></li>
        <li>発注詳細</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>発注詳細</span></h2>
<h2 class="HeaddingMid">カタログ発注詳細の確認と発注確定データの作成、発注数量の変更が行えます。</h2>
<form class="validate_form" id="order_input_form" method="post">
    <?php echo $this->form->input('TrnOrder.token', array('type'=>'hidden')); ?>
    
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>発注確定日</th>
                <td class="border-none">
                    <?php echo  $this->form->input('TrnOrder.work_date',array('type'=>'text','class'=>'lbl','readonly' => true));?>
                </td>
                <th></th>
                <td>
                    <input type="button" class="btn btn10" id="btn_err_chk"/>
                </td>
            </tr>
        </table>
    </div>
   
    <?php $i=1;foreach($result as $r){?>
    <hr class="Clear" />
         
    <h2 class="HeaddingMid">
        <?php echo $r[0]['TrnOrder']['supplier_name'];?>
    </h2>
     
     <div class="results">    
    <h2 class="HeaddingSmall"><!--<?php echo $r[0]['TrnOrder']['order_type_name']; ?>-->
    <?php if(Configure::read('OrderTypes.replenish') == $r[0]['TrnOrder']['order_type'] || Configure::read('OrderTypes.nostock') == $r[0]['TrnOrder']['order_type'] || Configure::read('OrderTypes.ExNostock') == $r[0]['TrnOrder']['order_type'] ){ ?>
    <?php echo $r[0]['TrnOrder']['department_name'] ?>
    <?php } ?>
    </h2>
</div>


    <div class="TableScroll">

<div class="results">    

        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th class="col5" rowspan="2">
                        <input type="checkbox" checked class="checkAll" id="<?php echo $r[0]['TrnOrder']['supplier_id'] . '_' . $r[0]['TrnOrder']['department_id'] . '_' . $r[0]['TrnOrder']['order_type'] ?>"/>
                    </th>
                    <th class="col10">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col20">製品番号</th>
                    <th class="col15">包装単位</th>
                    <th class="col10">発注済</th>
                    <th class="col10">数量</th>
              <!--      <th class="col15">作業区分</th> -->
                </tr>
                <tr>
                    <th>前回発注日</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>在庫数</th>
                    <th>備考</th>
                </tr>
            </table>
        </div>

        <div class="TableScroll">
            <table class="TableStyle01 table-even2">
            <?php foreach($r as $o){?>
                <tr>
                    <td class="col5 center" rowspan="2">
                        <?php if($o['TrnOrder']['alert_message'] == '' || ( isset($o['TrnOrder']['aptage_error']) && $o['TrnOrder']['aptage_error'] == true) ) { ?>
                        <input type="checkbox"
                               name="data[TrnOrder][id][<?php echo $i;?>]"
                               value="<?php echo $o['TrnOrder']['id']; ?>"
                               id="TrnOrderId<?php echo $i; ?>"
                               class="center chk <?php echo $r[0]['TrnOrder']['supplier_id'] . '_' . $r[0]['TrnOrder']['department_id'] . '_' . $r[0]['TrnOrder']['order_type'] ?>"
                               <?php if((isset($o['TrnOrder']['aptage_error']) && $o['TrnOrder']['aptage_error'] == false) || !isset($o['TrnOrder']['aptage_error'])){ echo "checked"; } else { echo "aptage_error"; } ?>
                        />
                        <?php } ?>
                        <?php if(isset($o['TrnOrder']['aptage_error']) && $o['TrnOrder']['aptage_error'] == true ){ ?>
                        <?php echo $this->form->input('TrnOrder.aptage_error_id.'.$i , array('type'=>'hidden' , 'class'=>'err_chk' ,  'value'=>$o['TrnOrder']['id'] ))?>
                        <?php } ?>
                    </td>
                    <td class="col10" align='center'><?php echo h_out($o['TrnOrder']['internal_code'] , 'center')?></td>
                    <td class="col20"><?php echo h_out($o['TrnOrder']['item_name']); ?></td>
                    <td class="col20"><?php echo h_out($o['TrnOrder']['item_code']);?></td>
                    <td class="col15"><?php echo h_out($o['TrnOrder']['unit_name']);?></td>
                    <td class="col10"><?php echo h_out($o['TrnOrder']['remain_count'], 'right');?></td>
                    <td class="col10"><?php echo $this->form->input('TrnOrder.quantity.'.$o['TrnOrder']['id'], array('value'=>$o['TrnOrder']['before_quantity'], 'class' => 'r num txt'));?></td>
             <!--       <td class="col15"><?php echo $this->form->input('TrnOrder.order_details_class.'.$o['TrnOrder']['id'], array('options'=>$order_classes_List,'empty'=>true ,'value'=>$o['TrnOrder']['work_type'], 'class' => 'txt'));?></td> -->
                </tr>
                <tr>
                    <td><?php echo h_out($o['TrnOrder']['last_order_date'], 'center');?></td>
                    <td><?php echo h_out($o['TrnOrder']['standard']);?></td>
                    <td><?php echo h_out($o['TrnOrder']['dealer_name']);?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($o['TrnOrder']['transaction_price']).'円','right');?></td>
                    <td><?php echo h_out($o['TrnOrder']['stock_count'],'right');?></td>
                    <td>
                        <?php if($o['TrnOrder']['alert_message'] == '') { ?>
                        <?php echo $this->form->input('TrnOrder.comment.'.$o['TrnOrder']['id'] , array('type'=>'text' , 'class'=>'txt' , 'value'=>$o['TrnOrder']['recital']));?>
                        <?php } else { ?>
                        <?php echo $this->form->input('TrnOrder.comment.'.$o['TrnOrder']['id'] , array('type'=>'text' , 'class'=>'txt RedBold' ,'readonly'=>true, 'value'=>$o['TrnOrder']['alert_message']));?>
                        <?php } ?>
                    </td>
                </tr>
            <?php $i++; } ?>
            </table>
        </div>
        </div>
        <?php } ?>
    </div>

    
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_back" value="戻る"/>
        <input type="button" class="common-button [p2]" id="btn_decision" value="発注確定"/>
    </div>
</form>
</div><!--#content-wrapper-->