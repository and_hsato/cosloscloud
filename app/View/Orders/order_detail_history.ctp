<script type="text/javascript">
$(function(){
    $("#btn_delete").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0){
            if(window.confirm("取消を実行します。よろしいですか？")){
                $("#order_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/order_delete').submit();
            }
        }else{
            alert("取り消す発注を選択してください");
        }
    });
});

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a>発注</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/order_history">発注履歴一覧</a></li>
        <li>発注履歴詳細</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>発注履歴詳細</span></h2>
<h2 class="HeaddingMid">発注を取り消す場合は、取消ボタンをクリックしてください。※入荷済みの項目は取り消しできません。</h2>
<form class="validate_form" id="order_input_form" method="post">
    <?php echo $this->form->input('Order.token' , array('type'=>'hidden'));?>
    <div class="results">
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2">
                <colgroup>
                    <col width="25" />
                    <col width="135"/>
                    <col />
                    <col width="120"/>
                    <col />
                    <col />
                    <col width="70"/>
                    <col width="50"/>
                    <col width="70"/>
                </colgroup>
                <thead>
                <tr>
                    <th rowspan="2"><input type="checkbox"  onClick="listAllCheck(this);"/></th>
                    <th>発注番号</th>
                    <th>仕入先</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>発注数</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th>発注日</th>
                    <th>部署</th>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>残数</th>
                    <th>備考</th>
                </tr>
                </thead>
                <tbody>
                <?php $cnt=0; foreach ($order_List as $orders) { ?>
                <tr>
                    <td class="center" rowspan="2">
                    <?php if($orders[0]['チェック']){ ?>
                        <input name="data[TrnOrder][id][]" id="id_<?php echo $cnt ?>" type="checkbox" class="center chk" value="<?php echo $orders['0']['id'] ?>"/>
                    <?php } ?>
                    </td>
                    <td><?php echo h_out($orders[0]['発注番号']); ?></td>
                    <td>
                        <?php echo h_out( $orders[0]['仕入先']);?>
                    </td>
                    <td><?php echo h_out($orders[0]['商品id'],'center'); ?></td>
                    <td><?php echo h_out($orders[0]['商品名']); ?></td>
                    <td><?php echo h_out($orders[0]['製品番号']); ?></td>
                    <td><?php echo h_out($orders[0]['包装単位']); ?></td>
                    <td><?php echo h_out($orders[0]['発注数'],'right') ?></td>
                    <td><?php echo h_out($orders[0]['更新者']); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($orders[0]['発注日'],'center'); ?></td>
                    <td><?php echo h_out($orders[0]['部署']); ?></td>
                    <td></td>
                    <td><?php echo h_out($orders[0]['規格']); ?></td>
                    <td><?php echo h_out($orders[0]['販売元名']); ?></td>
                    <td><?php echo h_out(number_format($orders[0]['仕入価格'],2),'right') ?></td>
                    <td><?php echo h_out($orders[0]['残数'] ,'right'); ?>
                        <input type='hidden' name="data[TrnOrder][modified][<?php echo $orders['0']['id'] ?>]" value="<?php echo $orders[0]['更新日時'] ?>">
                        <input type="hidden" name="data[TrnOrder][facility_to][<?php echo $orders['0']['id'] ?>]" value="<?php echo $orders[0]['仕入先id'] ?>"></td>
                    <td><?php echo h_out($orders[0]['備考']); ?></td>
                </tr>
                <?php $cnt++; } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_back" value="戻る"/>
        <input type="button" id="btn_delete" class="common-button confirm_btn [p2]" value="取消"/>
    </div>
</form>
</div><!--#content-wrapper-->