<script type="text/javascript">
    $(document).ready(function(){
        $('.checkAll').click(function(){
            $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
        });
        $('#btn_dicision').click(function(){
            if($('input[type=checkbox].chk:checked').length > 0 ){  
                $('#change_dicision').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result').submit();
            }else{
                alert('変更を行う仕入履歴を選択してください');
                return false;
            }
        });
        $(function (){
          $('img.scroll').click(function(){
           var to_id = $(this).attr('to_id');
           var obj = $('#'+to_id);
           var offset = obj.offset();

           scrollTo(0,offset.top);
           });
        });
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">仕入検索</a></li>
        <li>仕入変更確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>仕入変更確認</span></h2>

<form class="validate_form" id="change_dicision" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/result" method="post">
    <input type="hidden" name="data[operationDate]" value="<?php echo date("Y-m-d H:i:s")?>" />
    <?php echo $this->form->input('TrnClaim.time' , array('type'=>'hidden' , 'value' => date('Y/m/d H:i:s.u')))?>
    <div id="results">
        <div class="DisplaySelect">
            <table style="width:100%;">
            <tr>
                <td align="right"><div id="pageTop"></div></td>
            </tr>
            </table>
        </div>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="20"/>
                    <col width="150"/>
                    <col width="150"/>
                    <col>
                    <col>
                    <col width="100"/>
                    <col width="100"/>
                    <col width="100"/>
                    <col width="120"/>
                </colgroup>
                <tr>
                    <th rowspan="2"><input type="checkbox" class="checkAll"/></th>
                    <th>仕入先</th>
                    <th>仕入日</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>数量</th>
                    <th>区分</th>
                    <th>入荷単価</th>
                    <th>金額</th>
                </tr>
                <tr>
                    <th>施主</th>
                    <th>発注番号</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>商品ID</th>
                    <th></th>
                    <th>マスタ単価</th>
                    <th>遡及除外</th>
                </tr>
                <?php
                $i = 0;
                foreach ($SearchResult as $_row){ ?>
                <tr>
                    <td rowspan="2" align="center" style="width:20px;">
                        <?php if($_row[0]['status']){ ?>
                            <?php echo $this->form->checkbox("TrnClaim.id.{$_row[0]['id']}", array('hiddenField'=>false , 'class'=>'chk' , 'value'=>$_row[0]['id'])); ?>
                        <?php } ?>
                    </td>
                    <td><?php echo h_out($_row[0]['supplier']); ?></td>
                    <?php if($_row[0]['status']){ // 書き換え権限あり  ?>
                    <td><?php echo($this->form->text("TrnClaim.claim_date.{$_row[0]['id']}", array('class' => 'r date', 'maxlength' => '10', 'value'=>$_row[0]['claim_date']))); ?></td>
                    <?php } else { ?>
                    <td><?php echo h_out($_row[0]['claim_date'],'center'); ?></td>
                    <?php } ?>
                    <td><?php echo h_out($_row[0]['item_name']); ?></td>
                    <td><?php echo h_out($_row[0]['item_code']); ?></td>
                    <td><?php echo h_out($_row[0]['count'].$_row[0]['packing_name'], 'right'); ?></td>
                    <td><?php echo h_out($_row[0]['type_name'],'center'); ?></td>
                    <?php if($_row[0]['status']){ // 書き換え権限あり  ?>
                    <td><?php echo($this->form->text("TrnClaim.unit_price.{$_row[0]['id']}", array('class' => 'r txt num', 'maxlength' => '50', 'value'=>$_row[0]['unit_price']))); ?></td>
                    <?php } else { // 書き換え権限なし ?>
                    <td><?php echo h_out($this->Common->toCommaStr($_row[0]['unit_price']),'right'); ?></td>
                    <?php } ?>
                    <td><?php echo h_out($this->Common->toCommaStr($_row[0]['claim_price']),'right'); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($_row[0]['donor_name']); ?></td>
                    <td><?php echo h_out($_row[0]['work_no']); ?></td>
                    <td><?php echo h_out($_row[0]['standard']); ?></td>
                    <td><?php echo h_out($_row[0]['dealer_name']); ?></td>
                    <td><?php echo h_out($_row[0]['internal_code'],'center'); ?></td>
                    <td></td>
                    <td><?php echo h_out($this->Common->toCommaStr($_row[0]['transaction_price']),'right'); ?></td>
                    <td align="center">
                        <?php
                        if(strcmp($_row[0]['is_not_retroactive'],"true")==0){
                            echo $this->form->checkbox("TrnClaim.retroactive.{$_row[0]['id']}", array('class' => 'center','checked' => 'checked' , 'hiddenField'=>false));
                        }else{
                            echo $this->form->checkbox("TrnClaim.retroactive.{$_row[0]['id']}", array('class' => 'center' , 'hiddenField'=>false));
                        }
                        ?>
                    </td>
                </tr>
                <?php $i++; } ?>
            </table>
        </div>
        <div align="right" id="pageDow"></div>
        <div class="ButtonBox">
            <p class="center"><input type="button" class="btn btn2 [p2]" id="btn_dicision" /></p>
        </div>
    </div>
</form>
