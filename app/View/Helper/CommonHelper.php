<?php
/**
 * Common Helpers
 *
 * @version 1.0.0
 * @since 2010/07/29
 */
class CommonHelper extends AppHelper {

    public $helpers = array('Html', 'Number');
    
    /**
     * イメージファイルポップアップ用のアンカータグ文字列を返す
     * @param array $masterItem 'master_id'、及び'item_name'のパラメータを含む配列
     * @param string $actionStr アンカーテキストとして表示するHTML文字列。nullの場合は、$masterItemを基にイメージタグ文字列が生成される。
     * @param string $actionImgW イメージの幅(default: 50px)。アンカーテキストがイメージタグになる場合に指定する。
     * @param string $actionImgH イメージの高さ(default: 50px)。アンカーテキストがイメージタグになる場合に指定する。
     * @return string アンカータグ文字列
     */
    public function toImagePopupHtmlForMasterItem($masterItem, $actionStr = null, $actionImgW = '50px', $actionImgH = '50px') {
        $code = $masterItem['master_id'];
        $name = $masterItem['item_name'];
        $imageFile = $code . '.jpg';
        if (!is_readable(WWW_ROOT . 'images' . DS . 'item' . DS . $imageFile)) {
            if (isset($actionStr)) return '無';
            $imageFile = 'no-image.jpg';
        }
        $imagePath = $this->webroot . 'images' . DS . 'item' . DS . $imageFile;
        return $this->toImagePopupHtml($imagePath, $code, $name, $actionStr, $actionImgW, $actionImgH);
    }
    
    /**
     * イメージファイルポップアップ用のアンカータグ文字列を返す
     * @param unknown $imagePath ポップアップ表示対象のイメージファイルのパス。
     * @param string $groupName イメージのグループ名。イメージをポップアップ表示した際に同名のグループをスライド式に表示される。
     * @param string $imageTitle イメージのタイトル。イメージをポップアップ表示した際にタイトルとして表示される。
     * @param string $actionStr アンカーテキストとして表示するHTML文字列。nullの場合は、$masterItemを基にイメージタグ文字列が生成される。
     * @param string $actionImgW イメージの幅(default: 50px)。アンカーテキストがイメージタグになる場合に指定する。
     * @param string $actionImgH イメージの高さ(default: 50px)。アンカーテキストがイメージタグになる場合に指定する。
     * @return string アンカータグ文字列
     */
    public function toImagePopupHtml($imagePath, $groupName, $imageTitle, $actionStr, $actionImgW, $actionImgH) {
        if (is_null($actionStr)) {
            $actionStr = sprintf('<img class="img" src="%s" width="%s" height="%s" />', $imagePath, $actionImgW, $actionImgH);
        }
        $html = sprintf('<a href="%s" data-lightbox="%s" data-title="%s">%s</a>', $imagePath, $groupName, $imageTitle, $actionStr);
        return $html;
    }
    
    /**
     * 数値を3桁のカンマ区切り形式に変換する。数値ではない文字列が指定された場合は、その文字列をそのまま返す。
     * @param number $number 変換する数値文字列
     * @return 変換された文字列
     */
    public function toCommaStr($number) {
        $options = array(
                'negative' => '-',
                'wholeSymbol' => '',
        );
        return $this->toCurrencyStr($number, $options);
    }
    
    
    /**
     * 数値を通貨形式に変換する。数値ではない文字列が指定された場合は、その文字列をそのまま返す。
     * CakePHP NumberHelperに準拠。(http://book.cakephp.org/2.0/ja/core-libraries/helpers/number.html)
     * @param number $number 変換する数値文字列
     * @param array $options 変換オプション
     * @return 変換された文字列
     */
    private function toCurrencyStr($number, $options) {
        if (!is_numeric($number)) return $number;
        return $this->Number->currency($number, 'JPY', $options);
    }
    
}
