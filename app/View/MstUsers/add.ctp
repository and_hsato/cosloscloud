<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //チェックOKだったら、送信する
        validateDetachSubmit($('#AddForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result/' );
    }
}

$(document).ready(function(){
    <?php if(Configure::read('Password.Security') == 1 ) { ?>
    $('#temp_password').addClass('r validate[required,custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]');
    $('#temp_password2').addClass('r validate[required,custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]');
    <?php }else{ ?>
    $('#temp_password').addClass('r validate[required]');
    $('#temp_password2').addClass('r validate[required,equals[temp_password]]');
    <?php } ?>
    $("#AddForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
    $('table.TableStyle01 > tbody > tr:odd').addClass('odd');
    //チェックボックス一括変更
    $("#allCheck").click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    //新規なので仮パスワード発行フラグは強制ON
    $('#is_temp_issue').change(function(){
        $(this).attr('checked' , 'checked');
    });


});

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li class="pankuzu">運用設定</li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist">利用者設定</a></li>
        <li>利用者新規登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>利用者新規登録</span></h2>
<h2 class="HeaddingMid">利用者の新規登録が行えます。</h2>

<form class="search_form input_form"  action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckUserId" id="AddForm">
    <input type="hidden" name="mode" value="add" />
    <?php echo $this->form->input('MstUser.is_update' , array('type'=>'hidden' , 'value'=>'1'));?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstFacility.centerCode' , array('type' => 'hidden')); ?>

<h3 class="conth3">利用者の情報を入力</h3>
    <div class="SearchBox">
        <div style="float:left;">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>利用者ID</th>
                    <td><?php echo $this->form->input('MstUser.login_id' , array('type'=>'text' , 'value'=>'' ,'class'=>'validate[required,ajax[ajaxUserId]] search_canna r' , 'maxlength'=>20))?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>利用者名</th>
                    <td><?php echo $this->form->input('MstUser.user_name' , array('type'=>'text' , 'value'=>'' , 'class'=>'r validate[required] search_canna' , 'maxlength'=>100))?></td>
                    <td></td>
                </tr>
                 <tr>
                     <th>部署設定</th>
                     <td>
                         <?php echo $this->form->input('MstDepartment.departmentName' , array('id'=>'departmentName' , 'type'=>'hidden'))?>
                         <?php echo $this->form->input('MstDepartment.departmentText' , array('id'=>'departmentText' , 'type'=>'text' , 'class'=>'r validate[required] txt' , 'style'=>'width: 60px;'))?>
                         <?php echo $this->form->input('MstDepartment.departmentCode' , array('id'=>'departmentCode' , 'options'=>$department_list ,'class' => 'r validate[required] txt' , 'empty'=>'')); ?>
                    </td>
                    <td></td>
                </tr>
                <?php if(Configure::read('Password.Security') == 1 ){ ?>
                <tr>
                    <th>仮発行フラグ</th>
                    <td>
                        <?php echo $this->form->input('MstUser.is_temp_issue_dummy' , array('type'=>'checkbox' ,'hiddenField'=>false ,'checked'=>'checked' , 'disabled'=>'disabled'))?>
                        <?php echo $this->form->input('MstUser.is_temp_issue' , array('type'=>'hidden'))?>
                        <input type="button" value="仮パスワード再設定" id="password_btn" style="width:150px;" disabled="disabled">
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <th><?php if(Configure::read('Password.Security') == 1 ){ ?>仮<?php } ?>パスワード</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password' , array('type'=>'password' , 'class'=>'' , 'id'=>'temp_password'))?>
                    </td>
                </tr>
                <tr>
                    <th><?php if(Configure::read('Password.Security') == 1 ){ ?>仮<?php } ?>パスワード(確認用)</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password2' , array('type'=>'password' , 'class'=>'' , 'id'=>'temp_password2'))?>
                    </td>
                </tr>
            </table>
        </div>
        <?php if(Configure::read('Password.Security') == 1 ){ ?>
        <div style="border: 1px dotted black; width:400px;margin: 5px;float:left;margin-top:100px;">
            <p style="text-align:left;">【更新パスワード入力ルール】</p>
            <ol style="list-style-type: decimal !important;margin-left:25px;">
                <li style="list-style-type: decimal !important; text-align:left;">半角数字、半角英小文字、_（アンダーバー）、-（ハイフン）が使用出来ます。ただし<span class="ColumnRed">半角数字の0（ゼロ）、1（イチ）および  半角英小文字のo（オー）、l（エル）</span>は使用禁止です。</li>
                <li style="list-style-type: decimal !important; text-align:left;">パスワードは8文字とし、半角数字と半角英小文字が  それぞれ１文字以上含まれる値を設定してください。</li>
                <li style="list-style-type: decimal !important; text-align:left;">現在のパスワードとは異なる値を設定してください。</li>
            </ol>
        </div>
        <?php } ?>
    </div>
    
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_back" value="戻る"/>
        <input type="submit" class="common-button [p2]" value="登録" />
    </div>
</form>
</div><!--#content-wrapper-->