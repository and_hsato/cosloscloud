<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //チェックOKだったら、送信する
        validateDetachSubmit($('#userinfo') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result/' );
    }
}

$(document).ready(function(){
    //仮パスワード周り初期値
    if($('#is_temp_issue').attr('checked')==true){
        $('#is_temp_issue').attr('checked', false);
        $('#is_temp_issue').attr('disabled', 'disabled');
        $('#temp_password').val('dummy345');
        $('#temp_password2').val($('#temp_password').val());
    }else{
        $('#temp_password').val('');
        $('#temp_password2').val('');
    }
    <?php if($this->request->data['MstUser']['is_indefinite'] == true && Configure::read('Password.Security') == 0){ ?>
    $('#temp_password').addClass("r validate[required]");
    $('#temp_password2').addClass("r validate[required,equals[temp_password]]");
    $('#temp_password').val('<?php echo $this->request->data['MstUser']['password']; ?>');
    $('#temp_password2').val($('#temp_password').val());
    <?php }else{ ?>
    $('#temp_password2').addClass("r_lbl");
    $('#temp_password').addClass("r_lbl");
    $('#temp_password').attr('disabled' , 'disabled');
    $('#temp_password2').attr('disabled' , 'disabled');
    <?php } ?>
    $("#userinfo").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
    $('table.TableStyle01 > tbody > tr:odd').addClass('odd');
    //チェックボックス一括変更
    $("#allCheck").click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });


    //仮パスワード発行フラグが有効の場合、仮パスワード欄が有効になる。
    $('#is_temp_issue').change(function(){
        if($(this).attr('checked')==true){
            $('#temp_password').removeAttr("disabled");
            $('#temp_password').addClass("r validate[required,custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]");
            $('#temp_password').removeClass("r_lbl");

            $('#temp_password2').removeAttr("disabled");
            $('#temp_password2').addClass("r validate[required,equals[temp_password],custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]");
            $('#temp_password2').removeClass("r_lbl");
            $('#update').val('1');
        }else{
            $('#temp_password').attr('disabled' , 'disabled');
            $('#temp_password').addClass("r_lbl");
            $('#temp_password').removeClass("r validate[required,custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]");
            $('#temp_password').val('');

            $('#temp_password2').attr('disabled' , 'disabled');
            $('#temp_password2').addClass("r_lbl");
            $('#temp_password2').removeClass("r validate[required,equals[temp_password],custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]");
            $('#temp_password2').val('');

        }
    });
    //仮パスワード再設定ボタン押下
    $('#password_btn').click(function(){
        $('#is_temp_issue').attr('checked' , 'checked');
        $('#is_temp_issue').removeAttr("disabled");
        $('#temp_password').removeAttr("disabled");
        $('#temp_password').addClass("r validate[required,custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]");
        $('#temp_password').removeClass("r_lbl");
        $('#temp_password').val('');
        $('#temp_password2').removeAttr("disabled");
        $('#temp_password2').addClass("r validate[required,equals[temp_password],custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]");
        $('#temp_password2').removeClass("r_lbl");
        $('#temp_password2').val('');
        $('#update').val('1');
    });

});
</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li class="pankuzu">運用設定</li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist">利用者設定</a></li>
        <li>利用者編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>利用者編集</span></h2>
<h2 class="HeaddingMid">登録済み利用者の情報を編集できます。</h2>

<form class="search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckUserId" id="userinfo">
    <input type="hidden" name="mode" value="mod" />
    <?php echo $this->form->input('MstUser.id' , array('type'=>'hidden' , 'id'=>'user_id'))?>
    <?php echo $this->form->input('MstUser.parent_id' , array('type'=>'hidden' , 'id'=>'parent_id'))?>
    <?php echo $this->form->input('MstUser.is_update' , array('type'=>'hidden' , 'id'=>'update'))?>
    <?php echo $this->form->input('MstFacility.centerCode' , array('type' => 'hidden')); ?>
    
     <h3 class="conth3">利用者を編集</h3> 
     <div class="SearchBox">
         <div style="float:left;">
             <table class="FormStyleTable">
                 <colgroup>
                     <col />
                     <col />
                     <col width="20" />
                 </colgroup>
                 <tr>
                     <th>利用者ID</th>
                     <td>
                         <?php echo $this->form->input('MstUser.login_id' , array('type'=>'text' , 'class'=>'validate[required,ajax[ajaxUserId]] search_canna r' , 'maxlength'=>20))?>
                     </td>
                     <td></td>
                 </tr>
                 <tr>
                     <th>利用者名</th>
                     <td>
                         <?php echo $this->form->input('MstUser.user_name' , array('type'=>'text' , 'class'=>'validate[required] search_canna r' , 'maxlength'=>100))?>
                     </td>
                     <td></td>
                 </tr>
                 <tr>
                     <th>部署設定</th>
                     <td>
                         <?php echo $this->form->input('MstDepartment.departmentName' , array('id'=>'departmentName' , 'type'=>'hidden'))?>
                         <?php echo $this->form->input('MstDepartment.departmentText' , array('id'=>'departmentText' , 'type'=>'text' , 'class'=>'r validate[required] txt' , 'style'=>'width: 60px;'))?>
                         <?php echo $this->form->input('MstDepartment.departmentCode' , array('id'=>'departmentCode' , 'options'=>$department_list ,'class' => 'r validate[required] txt' , 'empty'=>'')); ?>
                    </td>
                    <td></td>
                </tr>
                <?php if($this->request->data['MstUser']['is_indefinite'] == false ){ ?>
                <tr>
                    <th>仮発行フラグ</th>
                    <td>
                        <?php echo $this->form->input('MstUser.is_temp_issue' , array('type'=>'checkbox' , 'hiddenField'=>false ,'id'=>'is_temp_issue'))?>
                        <input type="button" value="仮パスワード再設定" id="password_btn" style="width:150px;">
                    </td>
                </tr>
                <tr>
                    <th>仮パスワード</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password' , array('type'=>'password' , 'id'=>'temp_password'))?>
                    </td>
                </tr>
                <tr>
                    <th>仮パスワード(確認用)</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password2' , array('type'=>'password' , 'id'=>'temp_password2'))?>
                    </td>
                </tr>
                <?php } ?>
                <?php if($this->request->data['MstUser']['is_indefinite'] == true && Configure::read('Password.Security') == 0){ ?>
                <tr>
                    <th>パスワード</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password' , array('type'=>'password' , 'id'=>'temp_password' , 'maxlength'=>'20'))?>
                    </td>
                </tr>
                <tr>
                    <th>パスワード(確認用)</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password2' , array('type'=>'password' , 'id'=>'temp_password2' , 'maxlength'=>'20'))?>
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <th>削除フラグ</th>
                    <td>
                        <?php echo $this->form->input('MstUser.is_deleted' , array('type'=>'checkbox' , 'hiddenField'=>false))?>
                        削除する
                    </td>
                </tr>
            </table>
        </div>
        <?php if(Configure::read('Password.Security') == 1){ ?>
        <div style="border: 1px dotted black; width:400px;margin: 5px;float:left;margin-top:100px;">
            <p style="text-align:left;">【更新パスワード入力ルール】</p>
            <ol style="list-style-type: decimal !important;margin-left:25px;">
                <li style="list-style-type: decimal !important; text-align:left;">半角数字、半角英小文字、_（アンダーバー）、-（ハイフン）が使用出来ます。ただし<span class="ColumnRed">半角数字の0（ゼロ）、1（イチ）および  半角英小文字のo（オー）、l（エル）</span>は使用禁止です。</li>
                <li style="list-style-type: decimal !important; text-align:left;">パスワードは8文字とし、半角数字と半角英小文字が  それぞれ１文字以上含まれる値を設定してください。</li>
                <li style="list-style-type: decimal !important; text-align:left;">現在のパスワードとは異なる値を設定してください。</li>
            </ol>
        </div>
        <?php } ?>
    </div>
    
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_back" value="戻る"/>
        <input type="submit" value="登録" class="common-button [p2]" />
    </div>
</form>
</div><!--#content-wrapper-->
