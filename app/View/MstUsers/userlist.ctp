<script>
$(document).ready(function(){
    // 検索ボタン押下
    $("#btn_Search").click(function(){
        validateDetachSubmit($('#UserList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist#search_result' );
    });

    //新規ボタン押下
    $("#btn_Add").click(function(){
        validateDetachSubmit($('#UserList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add' );
    });

    //編集ボタン押下
    $("#btn_Mod").click(function(){
        $("#UserList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
    });

});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li class="pankuzu">運用設定</li>
        <li>利用者設定</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>利用者設定</span></h2>
<h2 class="HeaddingMid">利用者の新規登録、編集が行えます。</h2>

<form class="validate_form search_form" action='<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist/' method="post" id="UserList">
    <?php echo $this->form->input('MstUser.is_search' , array('type'=>'hidden'))?>
<h3 class="conth3">利用者を検索</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>利用者ID</th>
                <td><?php echo $this->form->input('MstUser.search_login_id' , array('type'=>'text' , 'class'=>'txt'))?></td>
                <td></td>
                <th></th>
                <td></td>
                <th></th>
                <td><?php echo $this->form->input('MstUser.search_is_deleted',array('type'=>'checkbox', 'hiddenField'=>false)); ?>削除も表示する</td>
            </tr>
            <tr>
                <th>利用者名</th>
                <td><?php echo $this->form->input('MstUser.search_user_name' , array('type'=>'text' , 'class'=>'txt search_canna'))?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Search" value="検索"/>
        <input type="button" class="common-button [p2]" id="btn_Add" value="新規"/>
    </div>
    <div class="results">
      <a name="search_result"></a>
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <?php echo $this->element('limit_combobox',array('result'=>count($User_List))); ?>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even" id="userlistTable">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col />
                    <col />
                    <col width="50"/>
                </colgroup>
                <thead>
                <tr>
                    <th></th>
                    <th>利用者ID</th>
                    <th>利用者名</th>
                    <th>部署名</th>
                    <th>削除</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($User_List as $user) { ?>
                <tr>
                    <td class="center"><input name="data[MstUser][id]" type="radio" class="validate[required]" id="user<?php echo $user['MstUser']['id']; ?>" value="<?php echo $user['MstUser']['id']; ?>" /></td>
                    <td><?php echo h_out($user['MstUser']['login_id']); ?></td>
                    <td><?php echo h_out($user['MstUser']['user_name']); ?></td>
                    <td><?php echo h_out($user['MstUser']['department_name']); ?></td>
                    <td><?php echo h_out($user['MstUser']['is_deleted'],'center');  ?></td>
                </tr>
                <?php } ?>
                </tbody>
                <?php if(isset($this->request->data['MstUser']['is_search']) && count($User_List) == 0){ ?>
                <tr>
                    <td colspan="5" class="center">該当するデータがありませんでした。</td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
    <?php if(count($User_List) > 0 ){ ?>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Mod" value="編集"/>
    </div>
    <?php } ?>
</form>
</div><!--#content-wrapper-->