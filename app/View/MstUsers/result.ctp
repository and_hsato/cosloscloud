<script>
$(document).ready(function(){
    $("#btn_new").click(function(){
        $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add').submit();
    });
    $("#btn_list").click(function(){
        $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist#search_result').submit();
    });
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li class="pankuzu">運用設定</li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist">利用者設定</a></li>
        <li class="pankuzu">利用者新規登録・編集</li>
        <li>利用者設定完了</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>利用者設定完了</span></h2>

<?php if(isset($this->request->data['MstUser']['is_deleted'])){ ?>
<h2 class="HeaddingMid">利用者を削除しました</h2>
<?php }else{ ?>
<h2 class="HeaddingMid">利用者を設定しました</h2>
<?php } ?>
<h3 class="conth3">利用者設定完了</h3>

<form method="post" id="result_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>利用者ID</th>
                <td class="border-none">
                    <?php echo $this->form->input('MstUser.login_id' , array('type'=>'text' , 'class'=>'lbl' ,'readonly'=>'readonly')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>利用者名</th>
                <td class="border-none">
                    <?php echo $this->form->input('MstUser.user_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>部署設定</th>
                <td class="border-none">
                    <?php echo $this->form->input('MstDepartment.departmentName' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    
    <div class="ButtonBox">
        <p class="center">
            <?php echo $this->form->input('MstUser.is_search', array('type'=>'hidden','value'=>1)); ?>
            <input type="button" class="common-button" id="btn_new" value="新規登録"/>
            <input type="button" class="common-button" id="btn_list" value="一覧表示"/>
        </p>
    </div>    
</form>
</div><!--.results -->
</div><!--#content-wrapper-->
