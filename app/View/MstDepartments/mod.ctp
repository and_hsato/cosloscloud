<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //チェックOKだったら、送信する
        validateDetachSubmit($('#ModForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result' );
    }
}

$(document).ready(function(){
    $("#ModForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li class="pankuzu">運用設定</li>
          <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/departments_list">部署設定</a></li>
          <li>部署編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>部署編集</span></h2>
<h2 class="HeaddingMid">登録済み部署の情報を編集できます。</h2>

<form class="input_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckDepartmentCode" id="ModForm">
    <input type="hidden" name="data[mode]" value="mod" />
    <?php echo $this->form->input('MstDepartment.id' , array('type'=>'hidden' , 'id'=>'department_id'))?>
    <?php echo $this->form->input('MstDepartment.mst_facility_id' , array('type'=>'hidden' , 'id'=>'facility_id')); ?>
 <h3 class="conth3">登録済み部署の情報を編集</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
           <tr>
               <th>部署名</th>
               <td>
                   <?php echo $this->form->input('MstDepartment.department_name' , array('type'=>'text' , 'class'=>'r validate[required] search_canna txt','maxlength'=>100 ))?>
               </td>
               <td></td>
           </tr>
           <tr>
               <th>部署コード</th>
               <td>
                   <?php echo $this->form->input('MstDepartment.department_code' , array('type'=>'text' , 'readonly'=>'readonly','class'=> 'validate[required,ajax[ajaxDepartmentCode]] txt ' , 'maxlength'=>20)) ?>
               </td>
               <td></td>
           </tr>
           <tr>
               <th>削除チェック</th>
               <td>
                   <?php echo $this->form->checkbox('MstDepartment.is_deleted' , array('checked'=>$this->request->data['MstDepartment']['is_deleted'] , 'hiddenField'=>false))?>
                   削除する
               </td>
               <td></td>
           </tr>
       </table>
    </div>
    <!--
    <div class="results">
    <h2 class="HeaddingSmall">登録済部署</h2>
    <div class="TableScroll">
        <table class="TableStyle01 table-even">
            <colgroup>
                <col />
                <col />
            </colgroup>
            <thead>
            <tr>
                <th>部署コード</th>
                <th>部署名</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($Department_List as $code => $name) { ?>
            <tr>
                <td><?php echo h_out($code); ?></td>
                <td><?php echo h_out($name); ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    </div>
    -->
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_back" value="戻る"/>
        <input type="submit" class="common-button [p2]" value="登録" />
    </div>
</form>
</div><!--#content-wrapper-->