<script>
$(document).ready(function(){
    $("#btn_list").click(function(){
        $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/departments_list#search_result').submit();
    });
    $("#btn_new").click(function(){
        $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add').submit();
    });
    $('td').addClass('border-none');
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li class="pankuzu">運用設定</li>
          <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/departments_list">部署設定</a></li>
          <li class="pankuzu">部署編集</li>
          <li>部署編集完了</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>部署編集完了</span></h2>
<?php if(isset($this->request->data['MstDepartment']['is_deleted'])){ ?>
<h2 class="HeaddingMid">部署のデータを削除しました。</h2>
<?php }else{ ?>
<h2 class="HeaddingMid">以下の内容で部署が設定されました。</h2>
<?php } ?>
<h3 class="conth3">設定完了部署</h3>
<div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
        </colgroup>
        <tr>
            <th>部署名</th>
            <td><?php echo $this->form->input('MstDepartment.department_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            <td></td>
        </tr>
        <tr>
            <th>部署コード</th>
            <td><?php echo $this->form->input('MstDepartment.department_code' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'));?></td>
            <td></td>
        </tr>
    </table>
</div>
<!--
<div class="results">
<h2 class="HeaddingSmall">登録済部署</h2>
<div class="TableScroll">
    <table class="TableStyle01 table-even">
        <thead>
        <tr>
            <th>部署コード</th>
            <th>部署名</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($Department_List as $code => $name) { ?>
        <tr>
            <td><?php echo h_out($code); ?></td>
            <td><?php echo h_out($name); ?></td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
</div>
  -->
<!--.results -->
<form id="result_form" method="post">
    <div class="ButtonBox">
        <p class="center">
            <?php echo $this->form->input('MstDepartment.is_search', array('type'=>'hidden','value'=>1)); ?>
            <input type="button" class="common-button" id="btn_new" value="続けて登録"/>
            <input type="button" class="common-button" id="btn_list" value="一覧表示"/>
        </p>
    </div>
</form>

</div><!--#content-wrapper-->
