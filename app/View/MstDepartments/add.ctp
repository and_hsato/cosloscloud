<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //チェックOKだったら、送信する
        validateDetachSubmit($('#AddForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result' );
    }
}

$(document).ready(function(){
    $("#AddForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li class="pankuzu">運用設定</li>
          <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/departments_list">部署設定</a></li>
          <li>新規部署登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>新規部署登録</span></h2>
<h2 class="HeaddingMid">カタログ利用者が請求を行う所属部署を新規登録できます。</h2>

<form class="input_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckDepartmentCode" id="AddForm">
    <input type="hidden" name="data[mode]" value="add" />
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <?php echo $this->form->input('MstDepartment.mst_facility_id' , array('type'=>'hidden' , 'id'=>'facility_id'))?>
    <h3 class="conth3">新規登録部署の情報を入力</h3> 
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>部署名</th>
                <td><?php echo $this->form->input('MstDepartment.department_name' , array('type'=>'text' , 'class'=>'r validate[required] search_canna txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>部署コード</th>
                <td>
                    <?php echo $this->form->input('MstDepartment.department_code' , array('type'=>'text' , 'readonly'=>'readonly' ,'class'=>'validate[required,ajax[ajaxDepartmentCode]] txt' , 'maxlength'=>'20'))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_back" value="戻る"/>
        <input type="submit" class="common-button [p2]" value="登録" />
    </div>
</form>
</div><!--#content-wrapper-->
