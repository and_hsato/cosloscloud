<script>
$(document).ready(function() {
    //新規追加
    $("#btn_Add").click(function(){
        //バリデート無効化 && ぐるぐる表示 && submit!!
        validateDetachSubmit($('#DepartmentsList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add' );
    });
    //施設に追加
    $("#btn_Mod1").click(function(){
        $("#DepartmentsList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/addmod').submit();
    });
    //編集
    $("#btn_Mod2").click(function(){
        $("#DepartmentsList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
    });
    //CSV
    $("#btn_Csv").click(function(){
        $('#DepartmentsList').validationEngine('detach');
        $("#DepartmentsList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
    });
  
    //検索
    $("#btn_Search").click(function(){
        //バリデート無効化 && ぐるぐる表示 && submit!!
        validateDetachSubmit($('#DepartmentsList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/departments_list#search_result' );
    });
});
</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
         <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
         <li class="pankuzu">運用設定</li>
         <li>部署設定</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>部署設定</span></h2>
<h2 class="HeaddingMid">カタログ利用者が請求を行う所属部署の新規登録と編集が行えます。</h2>

<form class="validate_form search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/departments_list" method="post"id="DepartmentsList">
    <?php echo $this->form->input('MstDepartment.is_search' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('MstDepartment.mst_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected') ))?>
    <?php echo $this->form->input('MstDepartment.mst_user_id' , array('type'=>'hidden' , 'value'=>  $this->Session->read('Auth.MstUser.id')))?> 
    
    <h3 class="conth3">編集する部署を検索</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>部署名</th>
                <td><?php echo $this->form->input('MstDepartment.search_department_name' , array('class'=>'txt search_canna')) ?></td>
                <th>&nbsp;</th>
                <td><?php echo $this->form->input('search_is_deleted',array('type'=>'checkbox' ,'hiddenFiled'=>false )); ?>削除も表示する</td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Search" value="検索"/>
        <input type="button" class="common-button [p2]" id="btn_Add" value="新規"/>
        <!--<input type="button" class="common-button" id="btn_Csv" value="CSV"/>-->
    </div>

    <div class="results">
      <a name="search_result"></a>
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
        <span class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($Departments_List))); ?>
        </span>
        <span class="BikouCopy"></span>
        </div>
    <div class="TableScroll">
        <table class="TableStyle01 table-even">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col width="50"/>
            </colgroup>
            <thead>
                <tr>
                    <th></th>
                    <th>部署コード</th>
                    <th>部署名</th>
                    <th>削除</th>
                </tr>
            </thead>

            <tbody>
                <?php $i=0 ; foreach ($Departments_List as $departments) { ?>
                <tr>
                    <td class="center"><input name="data[MstDepartment][id]" type="radio" class="validate[required]" id="radio<?php echo $i ?>" value="<?php echo $departments['MstDepartment']['id']; ?>" /></td>
                    <td><?php echo h_out($departments['MstDepartment']['department_code']); ?></td>
                    <td><?php echo h_out($departments['MstDepartment']['department_name']); ?></td>
                    <td><?php echo h_out($departments['MstDepartment']['is_deleted'],'center'); ?></td>
                </tr>
                <?php  $i++; } ?>
                <?php if(count($Departments_List) == 0  && isset($this->request->data['MstDepartment']['is_search'])){ ?>
                <tr><td colspan="4" class="center">該当データがありませんでした</td></tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
    </div>
    <?php if(count($Departments_List) > 0  ){ ?>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Mod2" value="編集"/>
    </div>
    <?php } ?>
</form>
</div><!--#content-wrapper-->