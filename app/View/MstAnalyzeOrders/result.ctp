<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/analyzes_list">分析並び順一覧</a></li>
        <li>分析並び順編集結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">分析並び順編集結果</h2>
<form method="post" id="analyze_form">
    <div class="SearchBox">
        <div class="results">
            <div class="TableScroll">
                <table class="TableStyle01 table-odd">
                    <colgroup>
                       <col width="30" />
                       <col />
                       <col />
                       <col width="75"/>
                       <col width="75"/>
                       <col width="150"/>
                    </colgroup>
                    <tr>
                        <th></th>
                        <th>タイトル</th>
                        <th>別名</th>
                        <th>並び順</th>
                        <th>非表示</th>
                        <th>センター</th>
                    </tr>
                    <?php  
                    $cnt=0;
                    foreach ($result as $r) {
                    ?>
                    <tr>
                        <td></td>
                        <td><?php echo h_out($r['MstAnalyze']['title']); ?></td>
                        <td><?php echo h_out($r['MstAnalyze']['other_title']); ?></td>
                        <td><?php echo h_out($r['MstAnalyze']['order_num']); ?></td>
                        <td><?php echo h_out($r['MstAnalyze']['is_hidden'],'center'); ?></td>
                        <td><?php echo h_out($r['MstAnalyze']['center_name'],'center'); ?></td>
                    </tr>
                    <?php $cnt++; } ?>
                </table>
            </div>
        </div>
    </div>
</form>
