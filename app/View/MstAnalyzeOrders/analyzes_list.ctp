<script>
    $(function(){
        //チェックボックス制御
        $('.checkAll').click(function(){
            $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
        });

        //更新ボタン押下
        $("#btn_Mod").click(function(){
            //必須チェック
            if($("input[type=checkbox].chk:checked").length > 0){
                $("#AnalyzeList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name ?>/result').submit();
            }else{
                alert('更新対象をチェックしてください。');
            }
        });
  
        //検索ボタン押下
        $("#btn_Search").click(function(){
            $("#AnalyzeList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name ?>/analyzes_list').submit();
        });
        $('#AnalyzeList').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name ?>/analyzes_list');
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>分析並び順一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">分析並び順一覧</h2>

<form class="validate_form search_form" method="post" action="" accept-charset="utf-8" id="AnalyzeList">
    <?php echo $this->form->input('MstAnalyze.is_search' , array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
            </colgroup>
            <tr>
                <th>タイトル</th>
                <td>
                    <?php echo $this->form->input('MstAnalyze.title' , array('type'=>'text' , 'class'=>'txt search_canna' , 'id'=>'SearchTitle' )); ?>
                </td>
                <td></td>
                <td>
                    <?php echo $this->form->input('MstAnalyze.hidden',array('type'=>'checkbox')); ?>
                    非表示も表示する
                </td>
            </tr>

        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>
    </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <?php echo $this->element('limit_combobox',array('result'=>count($Analyzes_List))); ?>
        </div>

        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <colgroup>
                    <col width="30" />
                    <col/>
                    <col/>
                    <col width="75"/>
                    <col width="75"/>
                    <col width="150"/>
                </colgroup>
                <tr>
                    <th><input type="checkbox" class="checkAll"/></th>
                    <th>タイトル</th>
                    <th>別名</th>
                    <th>並び順</th>
                    <th>非表示</th>
                    <th>センター</th>
                </tr>
                <?php 
                if(!empty($Analyzes_List)){
                    $cnt=0;
                    foreach ($Analyzes_List as $analyzes) {
                        $id = $analyzes['MstAnalyze']['id'];
                ?>
                <tr>
                    <td class="center">
                        <?php echo $this->form->input('MstAnalyze.id.'.$cnt , array('type' => 'checkbox' , 'class'=>'chk' , 'value' => $analyzes['MstAnalyze']['id'] ,  'hiddenField'=>false))?>
                        <?php echo $this->form->input('MstAnalyze.order_id.'.$id , array('type' => 'hidden' ,'value' => $analyzes['MstAnalyze']['order_id']))?>
                    </td>
                    <td><?php echo h_out($analyzes['MstAnalyze']['title']); ?></td>
                    <td>
                        <?php echo $this->form->input('MstAnalyze.other_title.'.$id , array('type'=>'text' , 'class'=>'txt' , 'value'=>$analyzes['MstAnalyze']['other_title']));?>
                    </td>
                    <td>
                        <?php echo $this->form->input('MstAnalyze.order_num.'.$id , array('type'=>'text' , 'class'=>'validate[custom[onlyNumber]] right txt' , 'value'=>$analyzes['MstAnalyze']['order_num']));?>
                    </td>
                    <td class="center">
                        <?php echo $this->form->input('MstAnalyze.is_hidden.'.$id , array('type'=>'checkbox' , 'class'=>'' , 'value'=>'' , 'hiddenField' => false ,'checked'=>$analyzes['MstAnalyze']['is_hidden']));?>
                    </td>
                    <td><?php echo h_out($analyzes['MstAnalyze']['center_name'],'center'); ?></td>
                </tr>
                <?php
                        $cnt++;
                    }
                } else {
                    if(isset($this->request->data['MstAnalyze']['is_search'])){
                ?>
                <tr>
                    <td colspan="6" class="center">該当するデータがありませんでした。</td>
                </tr>
                <?php } } ?>
            </table>
        </div>
    </div>
    <?php if(!empty($Analyzes_List)){ ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn24" id="btn_Mod"/>
    </div>
    <?php } ?>
</form>
