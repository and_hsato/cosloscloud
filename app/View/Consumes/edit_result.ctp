<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_search">消費履歴</a></li>
        <li class="pankuzu">消費履歴明細</li>
        <li>消費履歴取消完了</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>消費履歴取消完了</span></h2>
<h2 class="HeaddingMid">以下の消費履歴の取消を行いました。</h2>

<div class="results">
<table style="width: 100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
<div class="TableScroll2">
    <table class="TableStyle01 table-even2">
        <colgroup>
            <col width="110" />
            <col width="70" />
            <col width="90" />
            <col width="130" />
            <col width="120" />
            <col width="90" />
            <col width="80" />
        </colgroup>
        <thead>
        <tr>
            <th>消費番号</th>
            <th>消費日</th>
            <th>商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th>包装単位</th>
            <th>更新者</th>
        </tr>
        <tr>
            <th colspan="2">部署</th>
            <th>シール番号</th>
            <th>規格</th>
            <th>販売元</th>
            <th>売上価格</th>
            <th>備考</th>
        </tr>
        </thead>
        <tbody>
        <?php $_classes = Configure::read('Classes'); ?>
        <?php foreach ($result as $c){?>
        <tr>
            <td><?php echo h_out($c['TrnConsume']['work_no']); ?></td>
            <td><?php echo h_out($c['TrnConsume']['work_date'],'center'); ?></td>
            <td><?php echo h_out($c['TrnConsume']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($c['TrnConsume']['item_name']); ?></td>
            <td><?php echo h_out($c['TrnConsume']['item_code']); ?></td>
            <td><?php echo h_out($c['TrnConsume']['unit_name']); ?></td>
            <td><?php echo h_out($c['TrnConsume']['user_name']); ?></td>
        </tr>
        <tr>
            <td colspan="2"><?php echo h_out($c['TrnConsume']['department_name']); ?></td>
            <td><?php echo h_out($c['TrnConsume']['hospital_sticker_no'],'center'); ?> </td>
            <td><?php echo h_out($c['TrnConsume']['standard']); ?></td>
            <td><?php echo h_out($c['TrnConsume']['dealer_name']);?></td>
            <td class="num"><?php echo h_out($this->Common->toCommaStr($c['TrnConsume']['sales_price']),'right' ); ?></td>
            <td><?php echo h_out($c['TrnConsume']['recital']); ?></td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<table style="width: 100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
</div>
</div>