<script type="text/javascript">
$(function(){
    $('#bulk_input_btn').click(function(){
        $('.bulk_comment').val($('#comments_all_txt').val());
    });

    $('#comments_all_txt').change(function(){
        $('.bulk_comment').val($('#comments_all_txt').val());
    });

    $('#add_btn').click(function(){
        if( $('input[type=checkbox].chk:checked').length > 0 ){
            $(window).unbind('beforeunload');
            $('#confirm_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name?>/add_result').submit();
        }else{
            alert('登録を行いたい消費データを選択してください');
            return false;
        }
    });
    $('#remain_check_btn').click(function(){
        $('#confirm_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name?>/remain_check_csv').submit();
    });

    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
    });
    
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add">消費登録</a></li>
        <li>残数チェック確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>残数チェック確認</span></h2>
<form class="validate_form" id="confirm_form" method="post">
    <?php echo $this->form->input('TrnConsumeHeader.facilityId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnConsumeHeader.departmentId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnConsumeHeader.classId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnConsumes.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnConsumes.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>消費日</th>
                <td><?php echo $this->form->input('TrnConsumeHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnConsumeHeader.className',array('class' => 'lbl', 'readonly' => true)); ?>
                </td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('TrnConsumeHeader.facilityName', array('type'=>'text','label'=>false,'div'=>false,'readonly'=>'readonly', 'class'=>'lbl', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
                <th>備考日付</th>
                <td><?php echo $this->form->input('TrnConsumeHeader.comment_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('TrnConsumeHeader.departmentName', array('type'=>'text','label'=>false,'div'=>false,'readonly'=>'readonly', 'class'=>'lbl', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnConsumeHeader.recital', array('type'=>'text', 'label'=>false,'div'=>false,'readonly'=>'readonly', 'class'=>'lbl')); ?></td>
            </tr>
            <tr>
                <th></th>
                <td></td>
                <td></td>
                <th>備考２</th>
                <td><?php echo $this->form->input('TrnConsumeHeader.spare_recital', array('type'=>'text','label'=>false,'div'=>false,'readonly'=>'readonly', 'class'=>'lbl')); ?></td>
            </tr>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
        <tr>
            <td>表示件数：<?php echo count($result); ?>件</td>
            <td align="right"><input type="text" id="comments_all_txt" class="txt" style="width: 150px;" maxlength="200" ><input type="button" class="btn btn8 [p2]"  value="" id="bulk_input_btn"></td>
        </tr>
    </table>
    <div class="AlertLimitBox">※確定を押下した時点で消費登録されます。</div>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th rowspan="2" width="20" class="center"><input type="checkbox" class="checkAll" checked /></th>
                <th class="col10">商品ID</th>
                <th class="col20">商品名</th>
                <th class="col15">製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col15">部署シール</th>
                <th class="col20">作業区分</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>売上価格</th>
                <th>有効期限</th>
                <th colspan="2">備考</th>
            </tr>
        <?php $i = 0; foreach ($result as $i=>$r):?>
            <tr>
                <td width="20" rowspan="2" class="center">
                <?php echo $this->form->input('TrnConsume.report_id.'.$i, array('type'=>'hidden','value'=>$r[0]['id']));?>
                <?php if($r[0]['check'] != 0) { ?>
                <?php if($r[0]['check'] == 2 ) {$checked= true ; } else { $checked = false ; } ?>
                <?php echo $this->form->input('TrnConsume.id.'.$i, array('type'=>'checkbox','class' =>'chk','value'=>$r[0]['id'],'checked'=>$checked , 'hiddenField'=>false));?>
                <?php } ?>
                </td>
                <td class="col10"><?php echo h_out($r[0]['internal_code'],'center'); ?></td>
                <td class="col20"><?php echo h_out($r[0]['item_name']); ?></td>
                <td class="col15"><?php echo h_out($r[0]['item_code']); ?></td>
                <td class="col10"><?php echo h_out($r[0]['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($r[0]['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($r[0]['hospital_sticker_no']);?></td>
                <td class="col20">
                    <?php echo $this->form->input('TrnConsume.work_type.'.$r[0]['id'], array('options'=>$classes, 'class' => 'txt' , 'empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($r[0]['standard']); ?> </td>
                <td><?php echo h_out($r[0]['dealer_name']);?></td>
                <td class="num"><?php echo h_out($this->Common->toCommaStr($r[0]['sales_price']),'right'); ?></td>
                <td class="center"><?php echo h_out($r[0]['validated_date'],'center');?></td>
                <td colspan="2">
                    <?php echo $this->form->input('TrnConsume.recital.'.$r[0]['id'], array('type'=>'text', 'class' => 'txt bulk_comment' , 'value'=>$r[0]['message'])); ?>
                </td>
            </tr>
            <?php $i++; endforeach; ?>
        </table>
    </div>
    <div class="AlertLimitBox">※確定を押下した時点で消費登録されます。</div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="ButtonBox">
        <p class="center">
        <?php if (count($result) !== 0): ?>
            <input type="button" id="add_btn" class="btn btn2 submit [p2]" />
            <input type="button" id="remain_check_btn" class="btn btn5 submit [p2]" />
        <?php endif; ?>
        </p>
     </div>
</form>
