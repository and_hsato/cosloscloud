<script type="text/javascript">
$(function(){
    $("#recital_btn").click(function(){
        $(".recital").val($("#recitalAll").val()) ;
    });
    $("#edit_btn").click(function(){
        if($('input[:checkbox].chk:checked').length > 0) {
            if(window.confirm("取消を実行します。よろしいですか？")){
                $("#edit_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_result').submit();
            }
        } else {
           alert('消費データがチェックされていません');
           return false;
        }
    });
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
    });
});
</script>

<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_search">消費履歴</a></li>
        <li>消費履歴明細</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>消費履歴明細</span></h2>
<h2 class="HeaddingMid">消費を取り消す場合は、取消ボタンをクリックしてください。</h2>
<form class="validate_form" id="edit_form" method="post">
    <?php echo $this->form->input('TrnConsume.checktime' , array('type'=>'hidden' ,'value'=>date('Y/m/d H:i:s')))?>
    <?php echo $this->form->input('TrnConsumes.token' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('TrnConsume.mst_facility_id' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('TrnConsume.mst_user_id' , array('type'=>'hidden'))?>
    <div class="results">
  
    <table style="width: 100%;">
        <tr>
            <td>表示件数：<?php echo count($result); ?>件</td>
            <td align="right">
                <input type="text" maxlength="200" class="txt" id="recitalAll" style="width: 150px;">
                <input type="button" id="recital_btn" class="btn btn8 [p2]" value="">
            </td>
        </tr>
    </table>
    <div class="TableScroll2">
        <table class="TableStyle01 table-even2">
            <colgroup>
                <col width="20" />
                <col width="110" />
                <col width="70" />
                <col width="90" />
                <col width="130" />
                <col width="120" />
                <col width="90" />
                <col width="80" />
            </colgroup>
            <thead>
                <tr>
                    <th rowspan="2" class="center"><input type="checkbox" class="checkAll" /></th>
                    <th>消費番号</th>
                    <th>消費日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th colspan="2">部署</th>
                    <th>シール番号</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>売上単価</th>
                    <th>備考</th>
                </tr>
            </thead>
            <tbody>
            <?php $i = 0; foreach ($result as $c){?>
            <tr>
                <td rowspan="2" class="center">
                    <?php if($c['TrnConsume']['is_cancel'] == 0 ){?>
                    <?php echo $this->form->input('TrnConsume.id.'.$i ,array('type'=>'checkbox' , 'class'=>'chk center' , 'value'=>$c['TrnConsume']['id'] , 'hiddenField'=>false)); ?>
                    <?php } ?>
                </td> 
                <td><?php echo h_out($c['TrnConsume']['work_no']); ?></td>
                <td><?php echo h_out($c['TrnConsume']['work_date'],'center'); ?></td>
                <td><?php echo h_out($c['TrnConsume']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($c['TrnConsume']['item_name']); ?>
                </td>
                <td><?php echo h_out($c['TrnConsume']['item_code']); ?></td>
                <td>
                    <?php echo h_out( (($c['TrnConsume']['quantity'] > 1 )?$c['TrnConsume']['quantity']:'') . $c['TrnConsume']['unit_name'] ); ?>
                </td>
                <td><?php echo h_out($c['TrnConsume']['user_name']); ?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo h_out($c['TrnConsume']['department_name']); ?></td>
                <td><?php echo h_out($c['TrnConsume']['hospital_sticker_no'],'center'); ?></td>
                <td><?php echo h_out($c['TrnConsume']['standard']); ?></td>
                <td><?php echo h_out($c['TrnConsume']['dealer_name']);?></td>
                <td class="num"><?php echo h_out($this->Common->toCommaStr($c['TrnConsume']['sales_price']),'right'); ?></td>
                <td>
                    <?php echo $this->form->input('TrnConsume.recital.'.$i, array('class'=>'txt recital','value'=>$c['TrnConsume']['recital'], 'maxlength' => '200')); ?>
                </td>
            </tr>
            <?php $i++; } ?>
            </tbody>
        </table>
    </div>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="common-button submit [p2]" value="取消" id="edit_btn" /></p>
    </div>
</form>
</div>
