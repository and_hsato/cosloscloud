<script>
$(function($){
    //検索ボタン押下
    $("#search_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_search/').submit();
    });
    //帳票印刷ボタン押下
    $("#print_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/').submit();
    });
    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/').submit();
    });

    //明細表示ボタン押下
    $('#edit_btn').click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $('#search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit/').submit();
        }else{
            alert('変更を行いたい消費履歴を選択してください');
            return false;
        }
    });

    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
    });
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>消費履歴</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>消費履歴</span></h2>
<h2 class="HeaddingMid">消費履歴の検索と一覧表示、変更、削除が行えます。</h2>
<form class="validate_form search_form input_form" method="POST" id="search_form">
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden'));?>
    <?php echo $this->form->input('TrnConsume.mst_facility_id',array('type'=>'hidden'));?>
    <?php echo $this->form->input('TrnConsume.mst_user_id',array('type'=>'hidden'));?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <h3 class="conth3">消費履歴を検索</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="60" align="right" />
            </colgroup>
            <tr>
                <th>消費番号</th>
                <td>
                    <?php echo $this->form->input('TrnConsume.work_no', array('class'=>'txt','style'=>'width: 110px;',)); ?>
                </td>
                <td></td>
                <th>消費日</th>
                <td>
                    <span>
                        <?php echo $this->form->input('TrnConsume.work_date_from', array('label' => false, 'div' => 'false', 'id' => 'datepicker1', 'class' => 'txt validate[optional,custom[date]] date')); ?>
                        &nbsp;&nbsp;～&nbsp;&nbsp;
                        <?php echo $this->form->input('TrnConsume.work_date_to', array('label' => false, 'div' => 'false', 'id' => 'datepicker2', 'class' => 'txt validate[optional,custom[date]] date')); ?>
                    </span>
                </td>
                <td></td>
                <td colspan="2">
                    <?php echo $this->form->input('TrnConsume.is_deleted',array('type'=>'checkbox',)); ?>取消も表示する
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                </td>
                <td></td>
                <th>消費部署</th>
                <td>
                    <?php echo $this->form->input('TrnConsume.departmentText',array('class'=>'txt','style'=>'width:60px;','id'=>'departmentText')); ?>
                    <?php echo $this->form->input('TrnConsume.departmentCode',array('options'=>$department_list, 'class'=>'txt','style'=>'width:120px;','id'=>'departmentCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('TrnConsume.departmentName',array('type'=>'hidden','id'=>'departmentName')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.internal_code', array('class'=>'txt search_internal_code')); ?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_code', array('class'=>'txt search_upper')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_name', array('class'=>'txt search_canna')); ?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                    <?php echo $this->form->input('MstDealer.dealer_name', array('class'=>'txt search_canna')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.standard', array('class'=>'txt search_canna')); ?>
                </td>
                <td></td>
                <th>シール番号</th>
                <td>
                    <?php echo $this->form->input('TrnSticker.facility_sticker_no', array('class'=>'txt')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height: 10px;"></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="common-button" id="search_btn" value="検索"/>
            <!--<input type="button" class="btn btn10" id="print_btn" />-->
            <!--<input type="button" class="btn btn5" id="btn_Csv"/>-->
        </p>
    </div>
    <div class="results">
        <div class="DisplaySelect">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <?php echo $this->element('limit_combobox',array('result'=>count($consumes))) ?>
                    </td>
                </tr>
           </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <thead>
                    <tr>
                        <th style="width:25px;"><input type="checkbox" class="checkAll" /></th>
                        <th style="width: 140px;">消費番号</th>
                        <th style="width: 90px;">消費日</th>
                        <th>部署</th>
                        <th>登録者</th>
                        <th style="width:155px;">登録日時</th>
                        <th style="width:65px;">件数</th>
                    </tr>
                </thead>
                <tbody>
                   <?php $_j = 0; foreach($consumes as $c) {?>
                    <tr>
                        <td style="width:25px;" class="center">
                            <?php echo $this->form->input('TrnConsumeHeader.id.'.$_j, array('type'=>'checkbox','value'=>$c['TrnConsumeHeader']['id'],'class'=>'chk' , 'hiddenField'=>false)); ?>
                        </td>
                        <td style="width: 140px;"><?php echo h_out($c['TrnConsumeHeader']['work_no'] ,'center'); ?></td>
                        <td style="width: 90px;"><?php echo h_out($c['TrnConsumeHeader']['work_date'] , 'center'); ?></td>
                        <td><?php echo h_out($c['TrnConsumeHeader']['department_name'] ); ?></td>
                        <td><?php echo h_out($c['TrnConsumeHeader']['user_name']); ?></td>
                        <td style="width:155px;"><?php echo h_out($c['TrnConsumeHeader']['created'] , 'center'); ?></td>
                        <td style="width:65px;"><?php echo h_out($c['TrnConsumeHeader']['count'] . '/' . $c['TrnConsumeHeader']['detail_count'] , 'right'); ?></td>
                    </tr>
                    <?php $_j++; } ?>
                    <?php if(count($consumes) == 0 && isset($this->request->data['search']['is_search'])) { ?>
                    <tr><td colspan="7" class="center">該当するデータがありませんでした</td></tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
      </div>
        <?php if(count($consumes) > 0) { ?>
        <div class="ButtonBox">
            <p class="center"><input type="button" class="common-button" value="明細表示" id="edit_btn" /></p>
        </div>
        <?php } ?>
</form>
</div>