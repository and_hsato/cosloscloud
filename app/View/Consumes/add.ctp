<script type="text/javascript">
$(document).ready(function() {
    // シール読込ボタン押下
    $("#read_sticker_btn").click(function(){
        $("#consume_input_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/read_stickers/').submit();
    });
});

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>消費登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>消費登録</span></h2>
<form class="validate_form input_form" id="consume_input_form" method="post">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>消費日</th>
                <td><?php echo $this->form->input('TrnConsumeHeader.work_date', array('type'=>'text' , 'label' => false, 'div' => 'false', 'id' => 'datepicker1', 'class' => 'r validate[required,custom[date]] date','value'=>date('Y/m/d'))); ?></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('TrnConsumeHeader.departmentName',array('id' => 'departmentName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnConsumeHeader.departmentText',array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'r txt validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('TrnConsumeHeader.departmentCode', array('options'=>$department_list, 'id' =>'departmentCode', 'class' =>'r txt validate[required]','empty'=>true)); ?>
                </td>
                <td></td>
            </tr>
         </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="common-button" id="read_sticker_btn"  value="シール読込"/>
        </p>
    </div>
</form>
</div>