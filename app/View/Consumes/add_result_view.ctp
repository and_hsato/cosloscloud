<div id="content-wrapper">
<div id="TopicPath">
    <ul>
    <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
    <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add">消費登録</a></li>
    <li class="pankuzu">消費確認</li>
    <li>消費登録結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>消費登録結果</span></h2>
<div class="Mes01">消費登録が完了しました</div>
<div class="SearchBox">
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
        </colgroup>
        <tr>
            <th>消費日</th>
            <td><?php echo $this->form->input('TrnConsumeHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly' => true)); ?>
            <td width="20"></td>
        </tr>
        <tr>
            <th>部署</th>
            <td><?php echo $this->form->input('TrnConsumeHeader.departmentName', array('type'=>'text','label'=>false,'div'=>false,'readonly'=>true, 'class'=>'lbl', 'style' =>'width:150px;')); ?></td>
            <td></td>
        </tr>
    </table>
</div>
<hr class="Clear" />
<div class="results">
<div class="TableScroll2">
    <table class="TableStyle01 table-odd2">
        <tr>
            <th rowspan="2" width="20">&nbsp;</th>
            <th class="col15">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">製品番号</th>
            <th class="col20">包装単位</th>
            <th class="col15">シール番号</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>売上単価</th>
            <th>備考</th>
        </tr>
    <?php $i = 0; foreach ($result as $r) { ?>
        <tr>
            <td width="20" rowspan="2">&nbsp;</td>
            <td class="col15 center"><?php echo h_out($r['MstFacilityItem']['internal_code'],'center');?></td>
            <td class="col20"><?php echo h_out($r['MstFacilityItem']['item_name']);?></td>
            <td class="col20"><?php echo h_out($r['MstFacilityItem']['item_code']);?></td>
            <td class="col20"><?php echo h_out($r['TrnSticker']['unit_name']);?></td>
            <td class="col15"><?php echo h_out($r['TrnSticker']['hospital_sticker_no']);?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo h_out($r['MstFacilityItem']['standard']);?></td>
            <td><?php echo h_out($r['MstDealer']['dealer_name']);?></td>
            <td class="num"><?php echo h_out($this->Common->toCommaStr($r['TrnSticker']['sales_price']),'right');?></td>
            <td><?php echo h_out($recital[$r['TrnSticker']['id']]); ?></td>
        </tr>
        <?php $i++; } ?>
    </table>
</div>
</div>
</div>