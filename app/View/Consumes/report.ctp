<printdata>
    <setting>
        <layoutname>04_消費履歴.rpx</layoutname>
        <fixvalues>
            <value name="printer"><?php echo Configure::read('PrinterType.center'); ?></value>
            <value name="センター名"><?php echo $stickers_h['センター名']; ?></value>
            <value name="施設部署名"><?php echo $stickers_h['施設部署名']; ?></value> 
            <value name="施設部署コード"><?php echo $stickers_h['施設部署コード']; ?></value>
            <value name="検索条件"><?php echo $search_params; ?></value>
        </fixvalues>
    </setting>
    <datatable>
        <columns><?php echo join("\t", array_keys($stickers_h)); ?></columns>
        <rows>
            <?php foreach($stickers as $_row): ?>
            <row><?php echo h(join("\t", array_values($_row))); ?></row>
            <?php endforeach; ?>
        </rows>
    </datatable>
</printdata>