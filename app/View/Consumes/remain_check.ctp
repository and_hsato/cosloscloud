<script type="text/javascript">
$(function(){
    $("#remain_check_btn").click(function(){
        if ($('#consumes_csv_file').val()) {
            $("#consume_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/remain_confirm/').submit();
        } else {
            alert('CSVファイルが選択されていません');
            return false;
        }
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add">消費登録</a></li>
        <li>残数チェックデータ取込</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>残数チェックデータ取込</span></h2>

<form id="consume_form" class="validate_form" enctype="multipart/form-data" accept-charset="utf-8" method="POST">
    <?php echo $this->form->input('TrnConsumeHeader.classId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnConsumeHeader.facilityId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnConsumeHeader.departmentId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnConsumeHeader.facilityCode', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnConsumeHeader.departmentCode', array('type'=>'hidden')); ?>
    <?php echo $this->form->hidden('codez', array('id'=>'codez',)); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>消費日</th>
                <td><?php echo $this->form->input('TrnConsumeHeader.work_date' , array('type'=>'text' ,'class'=>'lbl' , 'readonly'=>true)); ?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('TrnConsumeHeader.className',array('class' => 'lbl', 'readonly' => true)); ?></td>
            </tr>
            <tr>
                <th>消費施設</th>
                <td>
                    <?php echo $this->form->input('TrnConsumeHeader.facilityName', array('type'=>'text','label'=>false,'div'=>false,'readonly'=>true, 'class'=>'lbl', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
                <th>備考日付</th>
                <td>
                    <?php echo $this->form->input('TrnConsumeHeader.comment_date', array('type'=>'text','label'=>false,'div'=>false,'readonly'=>true, 'class'=>'lbl')); ?>
                </td>
            </tr>
            <tr>
                <th>消費部署</th>
                <td>
                    <?php echo $this->form->input('TrnConsumeHeader.departmentName', array('type'=>'text','label'=>false,'div'=>false,'readonly'=>true, 'class'=>'lbl', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('TrnConsumeHeader.recital', array('type'=>'text','label'=>false,'div'=>false,'readonly'=>true, 'class'=>'lbl')); ?>
                </td>
            </tr>
            <tr>
                <th></th>
                <td></td>
                <td></td>
                <th>備考２</th>
                <td>
                    <?php echo $this->form->input('TrnConsumeHeader.spare_recital', array('type'=>'text','label'=>false,'div'=>false,'readonly'=>true, 'class'=>'lbl')); ?>
                </td>
            </tr>

            <tr>
                <th>CSVファイル</th>
                <td colspan="3">
                    <?php echo $this->form->input('upload_csv',array('type'=>'file','class'=>'txt r validate[required]','style'=>'width:250px;','id'=>'consumes_csv_file')); ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" id="remain_check_btn" class="common-button" value="確認" /></p>
    </div>
</form>
