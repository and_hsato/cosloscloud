<script type="text/javascript">
/*
 * バーコードリスト、削除処理
 * @param {event} event
 * @return {bool}
 */

String.prototype.trim = function() {return this.replace(/^\s+|\s+$/g,"");}

function editTextArea(event){
    var KeyChar   = event.keyCode;
    var codeInput = FHConvert.ftoh($('#codeinput').val().trim().toUpperCase());

    if (KeyChar == 13){ //Enterキー押下
        var _hasDup = false;
        if (codeInput === '') {
            $('#err_msg').text('空文字は入力できません');
            $('#codeinput').val('');
        } else {
            $('#barcodelist > li').each(function () {
                if( codeInput == this.innerText) {
                    $("#consume_codez_form").attr('action', '<?php echo $this->webroot;?><?php echo $this->name; ?>/beep').submit();
                    $('#err_msg').text("番号が重複しています");
                    _hasDup = true;
                    $('#codeinput').val('');
                }
            });
            if (_hasDup === false) {
                //要素追加
                $('#barcodelist').append('<li>'+codeInput+'</li>');
                $('#barcodelist > li').click(function(){
                    $('#barcodelist > li').removeClass('selected');
                    $(this).addClass('selected');
                });
                //スクロール
                $('#barcodelist').scrollTop($('#barcodelist > li').length * 14);
                $('#err_msg').text('');
                $('#codeinput').val('');
                if($('#codez').val() == ''){
                    $('#codez').val(codeInput);
                } else {
                    $('#codez').val($('#codez').val() + ',' + codeInput);
                }
                $('#barcode_count').text('読込件数：'+ $('#barcodelist > li').length +'件');
            }
        }
        $('#codeinput').focus();
        return false;
    }
}

$(document).ready(function(){
    // current codez.
    var _currentCodez = null;

    //初期フォーカスをバーコード入力欄にあわせる
    $('#codeinput').focus();

    //DELETEキー押下
    $(document).keyup(function(e){
        if(e.which === 46){ //DELETEキー
            obj = jQuery(document.activeElement);
            if(obj.attr('id') != 'codeinput'){ //バーコード入力上でDELが押された場合は無視
                $('#barcodelist li.selected').remove();
                $('#codez').val(''); //いったん全部消す
                $('#barcodelist li').each(function(){
                    if($('#codez').val() == '' ){
                        $('#codez').val(this.innerText);
                    } else {
                        $('#codez').val($('#codez').val() + ',' + this.innerText );
                    }
                });
                $('#barcode_count').text('読込件数：'+ $('#barcodelist > li').length +'件');
            }
        }
        $('#codeinput').focus();
    });
  
    //確認ボタン押下
    $("#confirmBtn").click(function(){
        if ($('#barcodelist li').length > 0 && $('#codez').val() != '') {
             $("#consume_codez_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_confirm').submit();
        } else {
             alert('バーコードが入力されていません');
             $('#codeinput').focus();
             return false;
        }
    });
    //クリアボタン押下
    $("#claerBtn").click(function(){
        $('#codeinput').val('');
        $('#err_msg').text('');
        $('#codeinput').focus();
    });

    //全クリアボタン押下
    $('#allClaerBtn').click(function(){
        $('#barcodelist > li').remove();
        $('#codeinput').val('');
        $('#codez').val('');
        $('#err_msg').text('');
        $('#barcode_count').text('読込件数：0件');
        $('#codeinput').focus();
    });
});
</script>
<style>
#barcodelist{
  width: 512px;
  height:280px;
  overflow: auto;
  margin:5px 0px 5px;
  padding:5px;
  display: block;
  border: groove 1px #111111;
}
#barcodelist > li{
  width: 500px;
  cursor: pointer;
  display: block;
}
  
.selected{
  background:#0A246A;
  color:#ffffff;
}
</style>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add">消費登録</a></li>
        <li>シール読込</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>シール読込</span></h2>

<form class="validate_form" id="consume_codez_form" onsubmit="return false;" method="post">
    <?php echo $this->form->input('TrnConsumeHeader.departmentId', array('type'=>'hidden')); ?>
    <?php echo $this->form->hidden('codez', array('id'=>'codez',)); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>消費日</th>
                <td><?php echo $this->form->input('TrnConsumeHeader.work_date' , array('type'=>'text' ,'class'=>'lbl' , 'readonly'=>true)); ?></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('TrnConsumeHeader.departmentName', array('type'=>'text','label'=>false,'div'=>false,'readonly'=>true, 'class'=>'lbl', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>バーコード</th>
                <td colspan="4">
                    <div>
                        <input type="text" class="txt15 r" id="codeinput" onkeyDown="return editTextArea(event)" />
                        <input type="button" id='claerBtn' value="クリア" class="common-button" />
                    </div>
                    <div id="err_msg" class="RedBold"></div>
                </td>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <td colspan="4" valign="bottom">
                    <div style="float:left;"><ul class="barcodez" id="barcodelist" name="barcodelist"></ul></div>
                    <div style="float:right;"><span class="barcodez_count" id="barcode_count">読込件数：0件</span></div>
                    <br clear="all" />
                    <input type="button" value="全クリア" class="common-button" id="allClaerBtn" />
                </td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" id="confirmBtn" class="common-button" value="確認" /></p>
    </div>
</form>
</div>