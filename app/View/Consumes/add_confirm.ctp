<script type="text/javascript">
$(function(){
    $('#bulk_input_btn').click(function(){
        $('.bulk_comment').val($('#comments_all_txt').val());
    });

    $('#comments_all_txt').change(function(){
        $('.bulk_comment').val($('#comments_all_txt').val());
    });

    $('#add_btn').click(function(){
        if( $('input[type=checkbox].chk:checked').length > 0 ){
            $(window).unbind('beforeunload');
            $('#confirm_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name?>/add_result').submit();
        }else{
            alert('登録を行いたい消費データを選択してください');
            return false;
        }
    });
  
    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
    });
  
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add">消費登録</a></li>
        <li>消費確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>消費確認</span></h2>
<form class="validate_form" id="confirm_form" method="post">
    <?php echo $this->form->input('TrnConsumeHeader.departmentId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnConsumes.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('TrnConsumes.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
    <?php echo $this->form->input('codez' , array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>消費日</th>
                <td><?php echo $this->form->input('TrnConsumeHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('TrnConsumeHeader.departmentName', array('type'=>'text','label'=>false,'div'=>false,'readonly'=>'readonly', 'class'=>'lbl', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
<div class="results">
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
        <tr>
            <td>表示件数：<?php echo count($item_units); ?>件</td>
            <td align="right"><input type="text" id="comments_all_txt" class="txt" style="width: 150px;" maxlength="200" ><input type="button" class="btn btn8 [p2]"  value="" id="bulk_input_btn"></td>
        </tr>
    </table>
    <div class="AlertLimitBox">※確定を押下した時点で消費登録されます。</div>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th rowspan="2" width="20" class="center"><input type="checkbox" class="checkAll" checked /></th>
                <th class="col15">商品ID</th>
                <th class="col20">商品名</th>
                <th class="col20">製品番号</th>
                <th class="col20">包装単位</th>
                <th class="col15">シール番号</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>売上価格</th>
                <th>備考</th>
            </tr>
        <?php $i = 0; foreach ($item_units as $iu):?>
        <?php if ((in_array(4 , $iu['TrnSticker']['consuming_status'])) || (in_array(5,$iu['TrnSticker']['consuming_status'])) ) { ?>
            <tr>
                <td width="20" rowspan="2">&nbsp;</td>
                <td class="col15">&nbsp;</td>
                <td class="col20">&nbsp;</td>
                <td class="col20">&nbsp;</td>
                <td class="col20">&nbsp;</td>
                <td class="col15"><?php echo $iu['TrnSticker']['hospital_sticker_no'] ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <span style="color:red;">
                    <?php if (in_array(4 ,$iu['TrnSticker']['consuming_status']) ):?>別施設
                    <?php elseif (in_array(5,$iu['TrnSticker']['consuming_status'] )):?>無効シール
                    <?php endif;?>
                    </span>
                </td>
            </tr>

        <?php } else { ?>
            <tr>
                <?php echo $this->form->input('consumes.'.$i.'.TrnConsume.TrnSticker.modified', array('type'=>'hidden','value'=>$iu['TrnSticker']['modified'])); ?>
                <td width="20" rowspan="2" class="center">
                <?php
                    if (in_array(1,$iu['TrnSticker']['consuming_status']) || in_array(6 ,$iu['TrnSticker']['consuming_status'] )|| in_array( 7 , $iu['TrnSticker']['consuming_status'] ) || in_array( 8 , $iu['TrnSticker']['consuming_status'] ) || in_array( 9 , $iu['TrnSticker']['consuming_status'] ) ) {
                    } else {
                        $_checking = 'cehcked' ;// (in_array( 2 , $iu['TrnSticker']['consuming_status'] ) || in_array( 3 , $iu['TrnSticker']['consuming_status'] ) )?'':'checked';
                        echo $this->form->input('TrnConsume.id.'.$i, array('type'=>'checkbox','class' =>'chk','value'=>$iu['TrnSticker']['id'],'checked'=>$_checking , 'hiddenField'=>false));
                    }
                ?>
                </td>
                <td class="col15"><?php echo h_out($iu['MstFacilityItem']['internal_code'],'center'); ?></td>
                <td class="col20"><?php echo h_out($iu['MstFacilityItem']['item_name']); ?></td>
                <td class="col20"><?php echo h_out($iu['MstFacilityItem']['item_code']); ?></td>
                <td class="col20"><?php echo h_out($iu['TrnSticker']['unit_name']); ?></td>
                <td class="col15"><?php echo h_out($iu['TrnSticker']['hospital_sticker_no'],'center');?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($iu['MstFacilityItem']['standard']); ?> </td>
                <td><?php echo h_out($iu['MstDealer']['dealer_name']);?></td>
                <td class="num"><?php echo h_out($this->Common->toCommaStr($iu['TrnSticker']['sales_price']),'right'); ?></td>
                <td>
            <?php
               $_comment = '';
               $style = '';
               
               if (in_array(8 , $iu['TrnSticker']['consuming_status'])) {
                 $_comment .= (($_comment=='')?'':' / ') . '重複シール';
                 $style = 'color:red;';
               }
               if (in_array(1 , $iu['TrnSticker']['consuming_status'])) {
                 $_comment .= (($_comment=='')?'':' / ') . '消費済み : ' . $iu['TrnConsume']['work_date'];
                 $style = 'color:red;';
               }
               if (in_array(9 , $iu['TrnSticker']['consuming_status'])) {
                 $_comment .= (($_comment=='')?'':' / ') . '売上設定無';
               }
               if (in_array(6 , $iu['TrnSticker']['consuming_status'])) {
                 $_comment .= (($_comment=='')?'':' / ') . '予約済み';
               }
               if (in_array(7 , $iu['TrnSticker']['consuming_status'])) {
                 $_comment .= (($_comment=='')?'':' / ') . '仕入先の締めが完了しています';
               }
               if (in_array(11 , $iu['TrnSticker']['consuming_status'])) {
                 $_comment .= (($_comment=='')?'':' / ') . '定数切替商品';
                 $style = 'color:red;';
               }
               if (in_array(3 , $iu['TrnSticker']['consuming_status'])) {
                 $_comment .= (($_comment=='')?'':' / ') . '有効期限切れ';
                 $style = 'color:red;';
               } elseif (in_array(2 , $iu['TrnSticker']['consuming_status'])) {
                 $_comment .= (($_comment=='')?'':' / ') . '有効期限警告';
                 $style = 'color:red;';
               }
               if (in_array(10 , $iu['TrnSticker']['consuming_status'])) {
                 $_comment .= (($_comment=='')?'':' / ') . '移動消費';
                 $style = 'color:red;';
               }
               echo $this->form->input('TrnConsume.recital.'.$iu['TrnSticker']['id'], array('label'=>false, 'style'=>$style, 'class'=>'txt bulk_comment', 'value'=>$_comment.$iu['TrnSticker']['recitail'], 'maxlength' => '200'));
            ?>
                </td>
            </tr>
            <?php } ?>
            <?php $i++; endforeach; ?>
        </table>
    </div>
    <div class="AlertLimitBox">※確定を押下した時点で消費登録されます。</div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
  </div>
    <div class="ButtonBox">
        <p class="center">
        <?php if (count($item_units) !== 0): ?>
            <input type="button" id="add_btn" class="common-button submit [p2]" value="確定" />
        <?php endif; ?>
        </p>
     </div>
</form>
</div>