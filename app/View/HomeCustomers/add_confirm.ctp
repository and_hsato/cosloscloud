<script type="text/javascript">
    $(document).ready(function(){
       $(document).keydown(function(e){
          var key_code;
          key_code = e.keyCode ? e.keyCode : e.which;
          // 9 = Tabキー
          if(key_code  == 9 ){
              className = document.activeElement.className;
              indexNum = 0;
              obj = jQuery(document.activeElement);
              //クラスにtabが含まれていたら
              m = className.search(/ tab[0-9]/i);
              if (m != -1 ) {
                  indexNum = parseInt(className.substring(m+4));
              }
              switch (indexNum){
                  case 1:
                      //2に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').next(':eq(0)').find('.tab2').focus();
                      break;
                  case 2:
                      //3に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').prev(':eq(0)').find('.tab3').focus();
                      break;
                  case 3:
                      //4に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').next(':eq(0)').find('.tab4').focus();
                      break;
                  case 4:
                      //5に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').prev(':eq(0)').find('.tab5').focus();
                      break;
                  case 5:
                      //6に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').next(':eq(0)').find('.tab6').focus();
                      break;
                  case 6:
                      //7に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').prev(':eq(0)').find('.tab7').focus();
                      break;
                  case 7:
                      //8に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').next(':eq(0)').find('.tab8').focus();
                      break;
                  case 8:
                      //9に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').prev(':eq(0)').find('.tab9').focus();
                      break;
                  case 9:
                      //コピーされている場合
                      tmp1 = obj.parent(':eq(0)').parent(':eq(0)').next(':eq(0)').next(':eq(0)').find('.tab1');
                      //商品が切り替わっている場合
                      tmp2 = obj.parent(':eq(0)').parent(':eq(0)').parent(':eq(0)').parent(':eq(0)').next(':eq(0)').find('.tab1');
                      if(tmp1.attr('class') != undefined){
                          //1に飛ぶ
                          tmp1.focus();
                      }else if(tmp2.attr('class') != undefined){
                          //1に飛ぶ
                          tmp2.focus();
                      }else{
                          //確定ボタンに飛ぶ
                          $("#btn_add_result").focus();
                      }
                     break;
                  default:
                     //初期は先頭行のチェックボックス
                     $(document).find(".tab1:first").focus();
                     break;
              }
              return false;
          }
        });
        $("#btn_add_result").click(function(){
            if($('input[type=checkbox].chk:checked').length === 0){
                alert('在宅品消費登録を行う商品を選択してください');
                return false;
            }else{
                var hasError = false;
                $('input[type=checkbox].chk').each(function(i){
                    if(this.checked){
                        var input = $('#containTables');
                        //数量
                        if(input.find('input[type=text].quantity:eq(' + i + ')').val() == ''){
                            alert('数量を入力してください');
                            hasError = true;
                            input.find('input[type=text].quantity:eq(' + i + ')').focus();
                            return false;
                        }else if(!input.find('input[type=text].quantity:eq(' + i + ')').val().match(/^[1-9][0-9]*/)){
                            alert('数量の入力が不正です');
                            hasError = true;
                            input.find('input[type=text].quantity:eq(' + i + ')').focus();
                            return false;
                        }else if(input.find('input[type=text].quantity:eq(' + i + ')').val() >= 100){
                            alert('100以上の数量は指定出来ません');
                            hasError = true;
                            input.find('input[type=text].quantity:eq(' + i + ')').focus();
                            return false;
                        }

                        //仕入単価
                        if(input.find('input[type=text].transaction_price:eq(' + i + ')').val() == ''){
                            alert('仕入単価を入力してください');
                            hasError = true;
                            input.find('input[type=text].transaction_price:eq(' + i + ')').focus();
                            return false;
                        }else if(!input.find('input[type=text].transaction_price:eq(' + i + ')').val().match(/^[0-9]{1,10}(\.[0-9]{1,2})?$/)){
                            alert('仕入単価を正しく入力してください');
                            hasError = true;
                            input.find('input[type=text].transaction_price:eq(' + i + ')').focus();
                            return false;
                        }

                        //売上単価
                        if(input.find('input[type=text].sales_price:eq(' + i + ')').val() == ''){
                            alert('売上単価を入力してください');
                            hasError = true;
                            input.find('input[type=text].sales_price:eq(' + i + ')').focus();
                            return false;
                        }else if(!input.find('input[type=text].sales_price:eq(' + i + ')').val().match(/^[0-9]{1,10}(\.[0-9]{1,2})?$/)){
                            alert('売上単価を正しく入力してください');
                            hasError = true;
                            input.find('input[type=text].sales_price:eq(' + i + ')').focus();
                            return false;
                        }

                        //有効期限
                        input.find('input[type=text].validated_date:eq(' + i + ')').val(convertDate(input.find('input[type=text].validated_date:eq(' + i + ')').val()));
                        var v = input.find('input[type=text].validated_date:eq(' + i + ')').val();
                        if(v != "" && v != null){
                            //YYYY/MM/DD形式かどうか
                            if(!v.match(/^\d{4}\/\d{2}\/\d{2}$/)){
                                alert("日付はYYYY/MM/DD形式で入力してください");
                                hasError = true;
                                input.find('input[type=text].validated_date:eq(' + i + ')').focus();
                                return false;
                            }

                            //入力された日付の妥当性チェック
                            var tempYear = v.substr(0, 4) - 0;
                            var tempMonth = v.substr(5, 2) - 1;
                            var tempDay = v.substr(8, 2) - 0;

                            if(tempYear >= 1970 && tempMonth >= 0 && tempMonth <= 11 && tempDay >= 1 && tempDay <= 31){
                                var checkDate = new Date(tempYear, tempMonth, tempDay);
                                if(isNaN(checkDate)){
                                    alert("入力された日付が不正です");
                                    hasError = true;
                                    input.find('input[type=text].validated_date:eq(' + i + ')').focus();
                                    return false;
                                }else if(checkDate.getFullYear() == tempYear && checkDate.getMonth() == tempMonth && checkDate.getDate() == tempDay){
                                    //
                                }else{
                                    alert("入力された日付が不正です");
                                    hasError = true;
                                    input.find('input[type=text].validated_date:eq(' + i + ')').focus();
                                    return false;
                                }
                            }else{
                                alert("入力された日付が不正です");
                                hasError = true;
                                input.find('input[type=text].validated_date:eq(' + i + ')').focus();
                                return false;
                            }
                        }

                        //ロット番号(クラス分類３以上のものはロット必須)
                        var class_separation = input.find('input[type=hidden].class_separation:eq(' + i + ')').val();
                        var lot_no = input.find('input[type=text].lot_no:eq(' + i + ')').val();
                        if(parseInt(class_separation) >= 3){
                            if(lot_no == "" || lot_no == null){
                                alert("クラス分類が3以上の施設採用品は、ロット番号の入力が必須です");
                                hasError = true;
                                input.find('input[type=text].lot_no:eq(' + i + ')').focus();
                                return false;
                            }
                        }
                    }
                });
                if(true === hasError){
                    return false;
                }else{
                    $(window).unbind('beforeunload');
                    $("#form_add_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_result').submit();
                }
            }
        });

        $('#cmdTransactionPriceCopy').click(function(){
             $('input[type=hidden].reference_transaction_price').each(function(i){
                 var i = $('input[type=hidden].reference_transaction_price').index(this);
                 var from = $('input[type=hidden].reference_transaction_price:eq(' + i + ')').val();
                 if (from.match(/^[0-9]{1,10}(\.[0-9]{1,2})?$/)){
                     var to = $('input[type=text].transaction_price:eq(' + i + ')');
                     to.val(from);
                 }
             })
        });

        $('#cmdSalesPriceCopy').click(function(){
            $('input[type=hidden].reference_sales_price').each(function(i){
                var i = $('input[type=hidden].reference_sales_price').index(this);
                var from = $('input[type=hidden].reference_sales_price:eq(' + i + ')').val();
                if (from.match(/^[0-9]{1,10}(\.[0-9]{1,2})?$/)){
                    var to = $('input[type=text].sales_price:eq(' + i + ')');
                    to.val(from);
                }
            })
        });

        //備考一括入力
        $('#recital_btn').click(function(){
            $('.recital').val($('#recital_txt').val()); 
        });

        //追加ボタン押下
        $('.add_btn').click(function(){
            var tr_count = Math.floor($('table > tbody > tr').size() / 2); //TR要素をカウント
            var add = $(this).attr('id');
            $('#inputTable'+add).append(
                $('<tr>').append(
                    $('<td rowspan="2" class="center">').append('<input type="hidden" name="data[d2customers]['+tr_count+'][is_lowlevel] value="'+$('input#is_lowlevel'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][id]" value="'+$('input#id'+add).val()+'" >')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][item_unit_id]" value="'+$('input#item_unit_id'+add).val()+'" >')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][no]" value="'+tr_count+'" >')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][reference_transaction_price]" class="reference_transaction_price" value="'+$('input#reference_transaction_price'+add).val()+'" >')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][reference_sales_price]" class="reference_sales_price" value="'+$('input#reference_sales_price'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][per_unit]" value="'+$('input#per_unit'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][internal_code]" value="'+$('input#internal_code'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][item_name]" value="'+$('input#item_name'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][item_code]" value="'+$('input#item_code'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][standard]" value="'+$('input#standard'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][dealer_name]" value="'+$('input#dealer_name'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2customers]['+tr_count+'][packing_name]" value="'+$('input#packing_name'+add).val()+'">')
                                                        .append('<input type="checkbox" class="center chk tab1" checked="checked" name="data[d2customers][d2customersNo][]" value="'+tr_count+'">')
                ).append(
                    $('<td align="center">').append(
                        $('<label>').append(
                            $('<p align="center">').text($('input#internal_code'+add).val())
                        ).attr('title',$('input#internal_code'+add).val())
                    )
                ).append(
                    $('<td align="center">').append(
                        $('<label>').text($('input#item_name'+add).val()).attr('title' ,$('input#item_name'+add).val() )
                    )
                ).append(
                    $('<td align="center">').append(
                        $('<label>').text($('input#item_code'+add).val()).attr('title' , $('input#item_code'+add).val())
                     )
                 ).append(
                     $('<td>').append(
                         $('<label>').text($('input#packing_name'+add).val()).attr('title' ,$('input#packing_name'+add).val() )
                     )
                 ).append(
                     $('<td>').append(
                         $('<label>').append(
                             $('<p align="right">').text($('input#reference_transaction_price'+add).val())
                         ).attr('title',$('input#reference_transaction_price'+add).val())
                     )
                 ).append(
                     $('<td>').append(
                         $('<input type="text" maxlength="13" style="border:solid 1px red; ime-mode:disabled;" class="tbl_txt right transaction_price tab3" name="data[d2customers]['+tr_count+'][transaction_price]" />')
                     )
                  ).append(
                     $('<td>').append('<input type="text" class="tbl_txt lot_no tab5"  maxlength="20" name="data[d2customers]['+tr_count+'][lot_no]" />')
                              .append('<input type="hidden" class="class_separation" id="class_separation_+tr_count+" value="">')
                 ).append(
                     $('<td>').append(
                        $('<select name="data[d2customers]['+tr_count+'][classId]" class="txt tab7">')
                            .append('<option value=""></option>')
                            <?php foreach($class_list as $k => $v){ ?>
                            .append('<option value="<?php echo $k ?>"><?php echo $v?></option>')
                            <?php } ?>
                     )
                 ).append(
                     $('<td align="center">').append(
                         $('<input type="checkbox" class="center tab9" name="data[d2customers]['+tr_count+'][sokyu]" value="1" />')
                     )
                 )
             ).append(
                 $('<tr>').append(
                     $('<td>')
                 ).append(
                     $('<td>').append(
                         $('<label>').text($('input#standard'+add).val()).attr('title' , $('input#standard'+add).val())
                     )
                 ).append(
                     $('<td>').append(
                         $('<label>').text($('input#dealer_name'+add).val()).attr('title' , $('input#dealer_name'+add).val())
                     )
                 ).append(
                     $('<td>').append(
                         $('<input type="text" maxlength="9" style="border:solid 1px red; ime-mode:disabled;" class="tbl_txt right quantity tab2" name="data[d2customers]['+tr_count+'][quantity]" >')
                     )
                 ).append(
                     $('<td class="right">').append(
                         $('<label>').append(
                             $('<p align="right">').text($('input#reference_sales_price'+add).val())
                         ).attr('title' , $('input#reference_sales_price'+add).val())
                     )
                 ).append(
                     $('<td>').append(
                         $('<input type="text" maxlength="13" style="border:solid 1px red; ime-mode:disabled;" class="tbl_txt right sales_price tab4" name="data[d2customers]['+tr_count+'][sales_price]">')
                     )
                 ).append(
                     $('<td>').append(
                         $('<input type="text" class="date validated_date tab6" name="data[d2customers]['+tr_count+'][validated_date]" id="validated_date'+tr_count+'" >')
                     )
                 ).append(
                     $('<td colspan="2">').append(
                         $('<input type="text" maxlength="200" class="tbl_txt recital tab8" name="data[d2customers]['+tr_count+'][recital]" id="bikou+tr_count" />')
                     )
                 )
             )

             //追加要素にカレンダーを追加
             $('#validated_date'+tr_count).datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
             //ストライプ塗りなおし
             r_count = 1;
             $('table > tbody > tr').each(function(){
                 cnt = Math.floor(r_count / 2 );
                 if((cnt % 2 )!= 0){
                     $(this).addClass('odd');
                 }else{
                     $(this).removeClass('odd');
                 }
                 r_count++;
             });

             // 件数表示を更新する。
             $("#count").text('表示件数：' + $('.TableScroll2:last input[type=checkbox].chk').length + '件');
         });
         $(window).bind('beforeunload' ,function(){
            return '登録が完了していません。';
         });
    });

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>login/home" tabindex=-1>TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index" tabindex=-1>在宅品消費登録</a></li>
        <li class="pankuzu">採用品選択</li>
        <li>在宅品消費登録確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>在宅品消費登録確認</span></h2>
<h2 class="HeaddingMid">以下の直納品登録を行います。</h2>

<form class="validate_form" id="form_add_result" action="#" method="post">
    <?php echo $this->form->input('d2customers.token' , array('type'=>'hidden','tabindex'=>'-1'));?>
    <?php echo $this->form->input('d2customers.departmentId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <?php echo $this->form->input('d2customers.supplierId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <?php echo $this->form->input('d2customers.hospitalId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>仕入先</th>
                <td><?php echo $this->form->input('d2customers.supplierName' , array('class'=>'lbl' ,'readonly'=>'readonly','tabindex'=>'-1')); ?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('d2customers.class_name' , array('class'=>'lbl', 'tabindex'=>'-1' ,'readonly'=>true)); ?>
                    <?php echo $this->form->input('d2customers.classId' , array('type' => 'hidden' )); ?>
                </td>
            </tr>
            <tr>
                <th>消費日</th>
                <td><?php echo $this->form->input('d2customers.work_date' , array('class'=>'lbl' , 'readonly'=>'readonly','tabindex'=>'-1')); ?></td>
                <td></td>
                <th>備考日付</th>
                <td><?php echo $this->form->input('d2customers.recital_date' , array('class'=>'lbl' , 'readonly'=>'readonly','tabindex'=>'-1')); ?></td>
            </tr>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('d2customers.hospitalName' , array('class'=>'lbl' , 'readonly'=>'readonly','tabindex'=>'-1')); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('d2customers.recital1' , array('class'=>'lbl' , 'readonly'=>'readonly','tabindex'=>'-1')); ?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td><?php echo $this->form->input('d2customers.departmentName' , array('class'=>'lbl' , 'readonly'=>'readonly','tabindex'=>'-1')); ?></td>
                <td></td>
                <th>備考２</th>
                <td><?php echo $this->form->input('d2customers.recital2' , array('class'=>'lbl' , 'readonly'=>'readonly','tabindex'=>'-1')); ?></td>
            </tr>
        </table>
    </div>

    <hr class="Clear" />
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
        <span class="DisplaySelect" id="count">表示件数：<?php echo count($FacilityItemList);  ?>件</span>
        <span class="BikouCopy">
            <input type="button" class="btn btn59 [p2]" id="cmdTransactionPriceCopy" value="" tabindex=-1 />
            <input type="button" class="btn btn60 [p2]" id="cmdSalesPriceCopy" value="" tabindex=-1 />
            <input type="text" id="recital_txt" maxlength="200" class="txt" tabindex=-1 />
            <input type="button" class="btn btn8 [p2]" id='recital_btn' value="" tabindex=-1 />
        </span>
    </div>
    <div class="TableScroll2" id="containTables">
        <table class="TableStyle01" id="inputTable" border=0 style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <thead>
                <tr>
                    <th rowspan="2" class="center"><input type="checkbox" checked onClick="listAllCheck(this);" tabindex=-1 /></th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>参考仕入単価</th>
                    <th>仕入単価</th>
                    <th>ロット番号</th>
                    <th>作業区分</th>
                    <th>遡及除外</th>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                    <th>参考売上単価</th>
                    <th>売上単価</th>
                    <th>有効期限</th>
                    <th colspan="2">備考</th>
                </tr>
            </thead>
        </table>
<?php if(!empty($FacilityItemList)){ 
         $cnt=0;
         for($i=0;$i<count($FacilityItemList);$i++) {
             $internal_code       = $FacilityItemList[$i]['MstFacilityItem']['internal_code'];//商品ID
             $item_name           = $FacilityItemList[$i]['MstFacilityItem']['item_name'];//商品名
             $item_code           = $FacilityItemList[$i]['MstFacilityItem']['item_code'];//製品番号
             $standard            = $FacilityItemList[$i]['MstFacilityItem']['standard'];//規格
             $dealer_name         = (isset($FacilityItemList[$i]['MstFacilityItem']['MstDealer']['dealer_name'])?$FacilityItemList[$i]['MstFacilityItem']['MstDealer']['dealer_name']:'');//販売元
             $transaction_price   = $FacilityItemList[$i]['MstItemUnit']['transaction_price'];//参考仕入単価
             $sales_price         = $FacilityItemList[$i]['MstItemUnit']['sales_price'];//参考売上単価
             $is_lowlevel         = $FacilityItemList[$i]['MstFacilityItem']['is_lowlevel'];//低レベル品フラグ
             $item_unit_id        = $FacilityItemList[$i]['MstItemUnit']['id'];//包装単位
             $showCheckbox        = $FacilityItemList[$i]['showCheckbox'];
             $classSeparation     = $FacilityItemList[$i]['MstFacilityItem']['class_separation'];//クラス分類
             $class_name          = $FacilityItemList[$i]['MstFacilityItem']['spare_key8'];
             $type_name           = $FacilityItemList[$i]['MstFacilityItem']['spare_key7'];
             //包装単位
             $item_unit = $FacilityItemList[$i]['MstUnitName']['unit_name'];
             //入数1で()ごと表示しない
             if($FacilityItemList[$i]['MstItemUnit']['per_unit'] != 1){
                 $item_unit .= "(".$FacilityItemList[$i]['MstItemUnit']['per_unit'].$FacilityItemList[$i]['MstParUnitName']['unit_name'].")";
             }
             //セル背景色調整
            if($cnt%2==0){$odd="";} else {$odd=" class=\"odd\"";}
            ?>
        <table id="inputTable<?php echo $cnt?>" class="TableStyle02" border=0  style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <tbody>
                <tr<?php echo $odd; ?>>
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][is_lowlevel]" id="is_lowlevel<?php echo $cnt?>" value="<?php echo $is_lowlevel; ?>">
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][id]" id="id<?php echo $cnt?>" value="<?php echo $FacilityItemList[$i]['MstFacilityItem']['id']; ?>">
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][item_unit_id]" id="item_unit_id<?php echo $cnt?>" value="<?php echo $item_unit_id; ?>">
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][no]" id="no<?php echo $cnt?>"value="<?php echo $cnt; ?>">
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][reference_transaction_price]" id="reference_transaction_price<?php echo $cnt?>" class="reference_transaction_price" value="<?php echo $transaction_price; ?>">
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][reference_sales_price]" id="reference_sales_price<?php echo $cnt ?>" class="reference_sales_price" value="<?php echo $sales_price; ?>">
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][per_unit]" id="per_unit<?php echo $cnt ?>" value="<?php echo $FacilityItemList[$i]['MstItemUnit']['per_unit']; ?>" />
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][internal_code]" id="internal_code<?php echo $cnt ?>" value="<?php echo h($internal_code); ?>" />
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][item_name]" id="item_name<?php echo $cnt ?>" value="<?php echo h($item_name); ?>" />
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][item_code]" id="item_code<?php echo $cnt ?>" value="<?php echo h($item_code); ?>" />
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][standard]" id="standard<?php echo $cnt ?>" value="<?php echo h($standard); ?>" />
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][dealer_name]" id="dealer_name<?php echo $cnt ?>" value="<?php echo h($dealer_name); ?>" />
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][packing_name]" id="packing_name<?php echo $cnt ?>" value="<?php echo h($item_unit); ?>" />
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][class_name]" id="class_name<?php echo $cnt ?>" value="<?php echo h($class_name); ?>" />
                    <input type="hidden" name="data[d2customers][<?php echo $cnt; ?>][type_name]" id="type_name<?php echo $cnt ?>" value="<?php echo h($type_name); ?>" />

                    <td rowspan="2" class="center">
                    <?php if($showCheckbox === true): ?>
                        <input type="checkbox" class="center chk tab1" checked="checked" name="data[d2customers][d2customersNo][]" value="<?php echo $cnt; ?>" tabindex="<?php echo $cnt * 10 + 1;?>" />
                    <?php else: ?>
                        <input type="checkbox" class="center chk" name="data[d2customers][d2customersNo][]" style="display: none;" value="<?php echo $cnt; ?>" tabindex=-1 />
                    <?php endif; ?>
                    </td>
                    <td><?php echo h_out($internal_code,'center'); ?></td>
                    <td><?php echo h_out($item_name); ?></td>
                    <td><?php echo h_out($item_code); ?></td>
                    <td><?php echo h_out($item_unit); ?></td>
                    <td><?php echo h_out($transaction_price === false ? '<span color="red">未設定</span>':$this->Common->toCommaStr($transaction_price),'right'); ?></td>
                    <td><input type="text" maxlength="13" style="border:solid 1px red; ime-mode:disabled;" class="tbl_txt right transaction_price tab3" name="data[d2customers][<?php echo $cnt; ?>][transaction_price]" tabindex="<?php echo $cnt * 10 + 3;?>"/></td>
                    <td>
                        <input type="text" class="tbl_txt lot_no tab5"  maxlength="20" name="data[d2customers][<?php echo $cnt; ?>][lot_no]" tabindex="<?php echo $cnt * 10 + 5;?>" />
                        <input type="hidden" class="class_separation" id="class_separation_<?php echo $cnt; ?>" value="<?php echo $classSeparation; ?>">
                    </td>
                    <td>
                        <?php echo $this->form->input('d2customers]['.$cnt.'][classId', array('options'=>$class_list, 'empty'=>true,'class' => 'txt tab7','empty'=>true,'tabindex'=>($cnt * 10 + 7))); ?>
                    </td>
                    <td align="center">
                        <input type="checkbox" class="center tab9" name="data[d2customers][<?php echo $cnt; ?>][sokyu]" value="1" tabindex="<?php echo $cnt * 10 + 9;?>" />
                    </td>
                </tr>
                <tr<?php echo $odd; ?>>
                    <td class="center"><?php if($showCheckbox === true){ ?><input type='button' class="btn btn55 add_btn" id="<?php echo $cnt ?>" tabindex=-1 ><?php } ?></td>
                    <td><?php echo h_out($standard); ?></td>
                    <td><?php echo h_out($dealer_name); ?></td>
                    <td><input type="text" maxlength="9" style="border:solid 1px red; ime-mode:disabled;" class="tbl_txt right quantity tab2" name="data[d2customers][<?php echo $cnt; ?>][quantity]" tabindex="<?php echo $cnt * 10 + 2;?>" /></td>
                    <td><?php echo h_out($sales_price === false ? '<span color="red">未設定</span>': $this->Common->toCommaStr($sales_price),'right'); ?></td>
                    <td><input type="text" maxlength="13" style="border:solid 1px red; ime-mode:disabled;" class="tbl_txt right sales_price tab4" name="data[d2customers][<?php echo $cnt; ?>][sales_price]" tabindex="<?php echo $cnt * 10 + 4;?>" /></td>
                    <td><input type="text" class="date validated_date tab6" name="data[d2customers][<?php echo $cnt; ?>][validated_date]" tabindex="<?php echo $cnt * 10 + 6;?>" /></td>
                    <td colspan="2"><input type="text" maxlength="200" class="tbl_txt recital tab8" name="data[d2customers][<?php echo $cnt; ?>][recital]" id="bikou<?php echo $cnt; ?>" tabindex="<?php echo $cnt * 10 + 8;?>" /></td>
                </tr>
            </tbody>
        </table>
        <?php $cnt++;//セルの色変更用
             } //endforeach
        ?>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
        <input type="button" class="btn btn2 [p2]" id="btn_add_result" name="btn_add_result"/>
    </div>
</form>

<?php }//endif ?>
