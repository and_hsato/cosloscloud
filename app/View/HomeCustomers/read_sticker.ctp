<script type="text/javascript">
/*
 * バーコードリスト、削除処理
 * @param {event} event
 * @return {bool}
 */

String.prototype.trim = function() {return this.replace(/^\s+|\s+$/g,"");}

function editTextArea(event){
    var KeyChar   = event.keyCode;
    var codeInput = $('#codeinput').val().trim().toUpperCase();

    if (KeyChar == 13){ //Enterキー押下
        var _hasDup = false;
        if (codeInput === '') {
            $('#err_msg').text('空文字は入力できません');
            $('#codeinput').val('');
        } else {
            $('#barcodelist > li').each(function () {
                if( codeInput == this.innerText) {
                    $('#err_msg').text("番号が重複しています");
                    _hasDup = true;
                    $('#codeinput').val('');
                }
            });
            if (_hasDup === false) {
                var check = codeInput.substr(0 ,2);
                if(check == '17' || check == '30' || check == '10'){
                    // 頭が 17 , 10 , 30 で始まる場合2段バーコードの2段目として処理
                    var code = $('#barcodelist > li:last');
                    code.text(code.text() + codeInput );
                    //スクロール
                    $('#err_msg').text('');
                    $('#codeinput').val('');
                    if($('#codez').val() == '' ){
                    }else{
                        $('#codez').val($('#codez').val() + codeInput);
                    }
                } else {
                    // JANかGS1の場合
                    $('#barcodelist').append('<li>'+codeInput+'</li>');
                    // 追加要素にイベントを付加
                    $('#barcodelist > li').click(function(){
                        $('#barcodelist > li').removeClass('selected');
                        $(this).addClass('selected');
                    });
                    //スクロール
                    $('#barcodelist').scrollTop($('#barcodelist > li').length * 14);
                    $('#err_msg').text('');
                    $('#codeinput').val('');
                    if($('#codez').val() == '' ){
                        $('#codez').val(codeInput);
                    }else{
                        $('#codez').val($('#codez').val() + ',' + codeInput);
                    }
                    $('#barcode_count').text('読込件数：'+ $('#barcodelist > li').length +'件');
                }
            }
        }
        $('#codeinput').focus();
        return false;
    }
}

$(document).ready(function(){
    // current codez.
    var _currentCodez = null;

    //初期フォーカスをバーコード入力欄にあわせる
    $('#codeinput').focus();

    //DELETEキー押下
    $(document).keyup(function(e){
        if(e.which === 46){ //DELETEキー
            obj = jQuery(document.activeElement);
            if(obj.attr('id') != 'codeinput'){ //バーコード入力上でDELが押された場合は無視
                $('#barcodelist li.selected').remove();
                $('#codez').val(''); //いったん全部消す
                $('#barcodelist li').each(function(){
                    if($('#codez').val() == ''){
                        $('#codez').val(this.innerText);
                    } else {
                        $('#codez').val($('#codez').val() + ',' + this.innerText);
                    }
                });
                $('#barcode_count').text('読込件数：'+ $('#barcodelist > li').length +'件');
            }
        }
        $('#codeinput').focus();
    });
  
    //確認ボタン押下
    $("#confirmBtn").click(function(){
        if ($('#barcodelist li').length > 0 && $('#codez').val() != '') {
             $("#read_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/read_confirm').submit();
        } else {
             alert('バーコードが入力されていません');
             $('#codeinput').focus();
             return false;
        }
    });
    //クリアボタン押下
    $("#claerBtn").click(function(){
        $('#codeinput').val('');
        $('#err_msg').text('');
        $('#codeinput').focus();
    });

    //全クリアボタン押下
    $('#allClaerBtn').click(function(){
        $('#barcodelist > li').remove();
        $('#codeinput').val('');
        $('#codez').val('');
        $('#err_msg').text('');
        $('#barcode_count').text('読込件数：0件');
        $('#codeinput').focus();
    });
});
</script>
<style>
#barcodelist{
  width: 512px;
  height:280px;
  overflow: auto;
  margin:5px 0px 5px;
  padding:5px;
  display: block;
  border: groove 1px #111111;
}
#barcodelist > li{
  width: 500px;
  cursor: pointer;
  display: block;
}
  
.selected{
  background:#0A246A;
  color:#ffffff;
}
</style>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add">在宅品消費登録</a></li>
        <li>バーコード読込</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>バーコード読込</span></h2>
<form class="validate_form" id="read_form" method="post" onsubmit="return false;">
    <?php echo $this->form->input('d2customers.classId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('d2customers.supplierId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('d2customers.hospitalId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('d2customers.departmentId', array('type'=>'hidden')); ?>
    <?php echo $this->form->hidden('codez', array('id'=>'codez',)); ?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>仕入先</th>
                <td>
                  <?php echo $this->form->input('d2customers.supplierName' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                  <?php echo $this->form->input('d2customers.class_name' , array('type' => 'text' , 'class'=>'lbl' ,'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                  <?php echo $this->form->input('d2customers.classId' , array('type' => 'hidden')); ?>
                </td>
            </tr>
            <tr>
                <th>消費日</th>
                <td>
                  <?php echo $this->form->input('d2customers.work_date' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
                <td></td>
                <th>備考日付</th>
                <td>
                  <?php echo $this->form->input('d2customers.recital_date' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                  <?php echo $this->form->input('d2customers.hospitalName' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                  <?php echo $this->form->input('d2customers.recital1' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                  <?php echo $this->form->input('d2customers.departmentName' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
                <td></td>
                <th>備考２</th>
                <td>
                  <?php echo $this->form->input('d2customers.recital2' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
            </tr>
            <tr>
                <th>バーコード</th>
                <td colspan="4">
                    <div>
                        <input type="text" class="txt15 r" id="codeinput" onkeyDown="return editTextArea(event)" />
                        <input type="button" id='claerBtn' value="" class="btn btn14" />
                    </div>
                    <div id="err_msg" class="RedBold"></div>
                </td>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <td colspan="4" valign="bottom">
                    <div style="float:left;"><ul class="barcodez" id="barcodelist" name="barcodelist"></ul></div>
                    <div style="float:right;"><span class="barcodez_count" id="barcode_count">読込件数：0件</span></div>
                    <br clear="all" />
                    <input type="button" value="" class="btn btn13" id="allClaerBtn" />
                </td>
            </tr>


        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" id="confirmBtn" class="btn btn3 submit [p2]" /></p>
    </div>
</form>
