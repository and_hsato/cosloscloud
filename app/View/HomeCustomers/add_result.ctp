<script type="text/javascript">
    $(document).ready(function(){
        $('#orderPrint').click(function(){
            $('#directForm').attr('action', '<?php echo $this->webroot ?>orders/report/order').submit();
        });
        $('#shippingPrint').click(function(){
            $('#directForm').attr('action', '<?php echo $this->webroot ?><?php echo $this->name; ?>/report').submit();
        });
        $('#hospitalStickerPrint').click(function(){
            $('#directForm').attr('action', '<?php echo $this->webroot ?><?php echo $this->name; ?>/seal/1').submit();
        });
        $('#costStickerPrint').click(function(){
            $('#directForm').attr('action', '<?php echo $this->webroot ?><?php echo $this->name; ?>/seal/2').submit();
        });

       $('input.lbl').attr('readonly', 'readonly');
    });


</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">在宅品消費登録</a></li>
        <li class="pankuzu">在宅品消費登録確認</li>
        <li>在宅品消費登録結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>在宅品消費登録確認</span></h2>
<div class="Mes01">在宅品の消費登録を行いました</div>
<div class="SearchBox">
    <table class="FormStyleTable">
        <tr>
            <th>消費番号</th>
            <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $result['direct_no']; ?>" readonly /></td>
            <td width="20"></td>
        </tr>
        <tr>
            <th>仕入先</th>
            <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $header['supplierName']; ?>" readonly/></td>
            <td width="20"></td>
            <th>作業区分</th>
            <td><?php echo $this->form->input('d2customers.class_name' , array('type'=>'text' , 'class'=>'lbl'))?></td>
        </tr>
        <tr>
            <th>消費日</th>
            <td><input type="text" class="lbl" value="<?php echo $header['work_date']; ?>" readonly/></td>
            <td></td>
            <th>備考日付</th>
            <td><input type="text" class="lbl" value="<?php echo $header['recital_date']; ?>" readonly/></td>
        </tr>
        <tr>
            <th>施設</th>
            <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $header['hospitalName']; ?>" readonly/></td>
            <td></td>
            <th>備考</th>
            <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $header['recital1']; ?>" readonly/></td>
        </tr>
        <tr>
            <th>部署</th>
            <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $header['departmentName']; ?>" readonly/></td>
            <td></td>
            <th>備考２</th>
            <td><input type="text" class="lbl" style="width:150px;" value="<?php echo $header['recital2']; ?>" /></td>
        </tr>
    </table>
</div>

<hr class="Clear" />
<form class="validate_form" id="directForm" action="#" method="POST">
    <input type="hidden" name="data[TrnOrder][selected_facility_id]" value="<?php echo $this->Session->read('Auth.facility_id_selected'); ?>" />
    <input type="hidden" name="data[TrnOrderHeader][id]" value="<?php echo $result['trn_order_header_id']; ?>" />
    <input type="hidden" name="data[TrnShippingHeader][id]" value="<?php echo $result['trn_shipping_header_id']; ?>" />
    <input type="hidden" name="data[facility_from_id]" value="<?php echo $this->Session->read('Auth.facility_id_selected'); ?>" />
    <input type="hidden" name="data[User][user_id]" value="<?php echo $this->Session->read('Auth.MstUser.id'); ?>" />
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
</form>
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td>表示件数：<?php echo count($result['all_data']) - 1; ?>件</td>
        <td align="right">
        </td>
    </tr>
</table>
<div class="TableHeaderAdjustment01">
    <table class="TableHeaderStyle01">
        <tr>
            <th class="col10">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th class="col10">包装単位</th>
            <th class="col10">参考仕入単価</th>
            <th class="col10">仕入単価</th>
            <th class="col15">ロット番号</th>
            <th class="col10">作業区分</th>
            <th class="col10">遡及除外</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>数量</th>
            <th>参考売上単価</th>
            <th>売上単価</th>
            <th>有効期限</th>
            <th colspan="2">備考</th>
        </tr>
    </table>
</div>
<div class="TableScroll">
    <table class="TableStyle01">
        <?php $i=0;foreach($result['all_data'] as $row): ?>
        <?php if(false === isset($row['internal_code'])) continue; ?>
        <tr class="<?php echo ($i%2===0?'':'odd'); ?>">
            <td class="col10"><?php echo h_out($row['internal_code'],'center'); ?></td>
            <td><?php echo h_out($row['item_name']); ?></td>
            <td><?php echo h_out($row['item_code']); ?></td>
            <td class="col10"><?php echo h_out($row['packing_name']); ?></td>
            <td class="col10"><?php echo h_out($this->Common->toCommaStr($row['reference_transaction_price']),'right'); ?></td>
            <td class="col10"><input type="text" class="num tbl_lbl right" value="<?php echo number_format($row['transaction_price'], 2); ?>" readonly="readonly" /></td>
            <td class="col15"><input type="text" class="tbl_lbl" value="<?php echo $row['lot_no']; ?>" readonly="readonly" /></td>
            <td class="col10"><input type="text" class="tbl_lbl" value="<?php echo $row['work_type_name']; ?>" readonly="readonly" /></td>
            <td class="col10"><?php echo h_out( (isset($row['sokyu']) && $row['sokyu'] == '1') ? '○' : '' ,'center'); ?></td>
        </tr>
        <tr class="<?php echo ($i%2===0?'':'odd'); ?>">
            <td></td>
            <td><?php echo h_out($row['standard']); ?></td>
            <td><?php echo h_out($row['dealer_name']); ?></td>
            <td><input type="text" class="tbl_lbl num right" value="<?php echo number_format($row['quantity']); ?>" readonly="readonly" /></td>
            <td><?php echo h_out($this->Common->toCommaStr($row['reference_sales_price']),'right'); ?></td>
            <td><input type="text" class="num tbl_lbl"  value="<?php echo number_format($row['sales_price'], 2); ?>" readonly="readonly" /></td>
            <td><input type="text" class="tbl_lbl" value="<?php echo $row['validated_date']; ?>" readonly="readonly" /></td>
            <td colspan="2"><input type="text" class="tbl_lbl" value="<?php echo $row['recital']; ?>" readonly="readonly" /></td>
        </tr>
        <?php $i++;endforeach; ?>
    </table>
</div>
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
<div class="ButtonBox">
    <input type="button" class="btn btn21 fix_btn" id="orderPrint" />
    <input type="button" class="btn btn19 fix_btn" id="shippingPrint" />
    <input type="button" class="btn btn16 fix_btn" id="hospitalStickerPrint" />
    <input type="button" class="btn btn12 fix_btn" id="costStickerPrint" />
</div>
