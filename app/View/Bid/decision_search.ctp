<script  type="text/javascript">
$(document).ready(function() {
    //検索ボタン押下
     $("#btn_Search").click(function(){
         validateDetachSubmit($('#BidInfo') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/decision_search' );
     });

    //明細表示ボタン押下
    $("#btn_Conf").click(function(){
        $("#BidInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/decision_confirm').submit();
    });
});

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>見積確定</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>見積確定</span></h2>
<h2 class="HeaddingMid">確定を行いたい見積を検索してください</h2>
    <form method="post" action="" accept-charset="utf-8" id="BidInfo" class="validate_form search_form">
        <?php echo $this->form->input('Bid.is_search' , array('type'=>'hidden'))?>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>見積番号</th>
                    <td><?php echo $this->form->input('Bid.work_no' , array('class'=>'txt'))?></td>
                    <td></td>
                    <th>締切日</th>
                    <td>
                        <?php echo $this->form->input('Bid.limit_date_from' , array('type'=>'text' , 'class'=>'txt validate[optional,custom[date]] date' , 'id'=>'datepicker1'))?>
                      &nbsp;～&nbsp;
                        <?php echo $this->form->input('Bid.limit_date_to' , array('type'=>'text' , 'class'=>'txt validate[optional,custom[date]] date' , 'id'=>'datepicker2'))?>
                    </td>
                    <td></td>
                    <th></th>
                    <td>
                        <?php echo $this->form->input('Bid.limit_flg' , array('type'=>'checkbox'))?>締切済みのみ表示
                    </td>
                    <td></td>
                </tr>

            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="common-button" value="検索" id="btn_Search"/>
        </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th class="col5"></th>
                    <th class="col15">見積番号</th>
                    <th class="col10">締切日</th>
                    <th>登録者</th>
                    <th class="col10">登録日時</th>
                    <th>備考</th>
                    <th>入札件数</th>
                    <th>商品件数</th>
                </tr>
                <?php $cnt=0; foreach($result as $r){ ?>
                <tr>
                    <td class="center"><?php echo $this->form->radio('Bid.id' , array($r['Bid']['id'] => '') , array('hiddenField'=>false , 'label'=>false , 'class'=>'validate[required] ')); ?></td>
                    <td><?php echo h_out($r['Bid']['work_no'],'center'); ?></td>
                    <td><?php echo h_out($r['Bid']['limit_date'],'center'); ?></td>
                    <td><?php echo h_out($r['Bid']['user_name']); ?></td>
                    <td><?php echo h_out($r['Bid']['created'],'center'); ?></td>
                    <td><?php echo h_out($r['Bid']['recital']); ?></td>
                    <td><?php echo h_out($r['Bid']['count'],'right'); ?></td>
                    <td><?php echo h_out($r['Bid']['detail_count'],'right'); ?></td>
                </tr>
                <?php $cnt++; } ?>
                <?php if( count($result)==0 && isset($this->request->data['Bid']['is_search']) ){ ?>
                <tr><td colspan="8" class="center">該当するデータがありませんでした</td></tr>
                <?php } ?>
            </table>
        </div>
        <?php if(count($result) > 0){ ?>
        <div class="ButtonBox">
            <input type="button" class="common-button" value="確認" id="btn_Conf"/>
        </div>
        <?php } ?>

    </div>
</form>
  </div>