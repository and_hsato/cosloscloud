<script type="text/javascript">
$(function(){
    $("#btn_Result").click(function(){
        if($("#result").val() === ''){
            alert('ファイルが選択されてません');
            return false;
        }else{
            if(confirm('登録を行います。\nよろしいですか？')){
                $("#Import").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/upload');
                $("#Import").attr('method', 'post');
                //送信
                $("#Import").submit();
            }
        }
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>見積登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>見積登録</span></h2>
<h2 class="HeaddingMid">登録する見積ファイルを選択してください</h2>
<div id="msg" class="err" style="font-size:20px"></div>

<form class="validate_form" method="post" action="" enctype="multipart/form-data" accept-charset="utf-8" id="Import">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>ファイル</th>
                <td colspan="2">
                    <?php echo $this->form->file('result',array('label'=>false,'div'=>false,'class'=>'r text validate[required]','style'=>'width:250px;','value'=>'')); ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn29 [p2]" id="btn_Result" />
    </div>
</form>
