<script type="text/javascript">
    $(document).ready(function(){
        // チェックボックス一括制御
        $(".all_check").click(function(){
            $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
        });

        // 確定ボタン押下
        $("#btn_add_result").click(function(){
            if($('input[type=checkbox].chk:checked').length === 0){
                alert('登録を行う商品を選択してください');
                return false;
            }else{
                $("#form_add_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/proposal_result').submit();
            }
        });
    });

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>login/home" >TOP</a></li>
        <li><a href="<?php echo $this->webroot ?><?php echo $this->name; ?>/proposal">見積登録</a></li>
        <li>見積登録確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>見積登録</span></h2>
<h2 class="HeaddingMid">以下の見積登録を行います。</h2>

<form class="validate_form" id="form_add_result" action="#" method="post">
    <hr class="Clear" />
    <div class="results">
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
        <span class="DisplaySelect">
            　表示件数：<?php echo count($result);  ?>件
        </span>
    </div>
    <div class="TableScroll" id="containTables">
        <table class="TableStyle01 table-even2" id="inputTable">
            <colgroup>
                <col class="col5" />
                <col class="col10" />
                <col />
                <col />
                <col class="col10" />
            </colgroup>
            <thead>
                <tr>
                    <th rowspan="2" class="center"><input type="checkbox" class="all_check"/></th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                </tr>
                <tr>
                    <th>JANコード</th>
                    <th>規格</th>
                    <th>金額</th>
                    <th>備考</th>
                </tr>
            </thead>
            <tbody>
                <?php $cnt=0; foreach($result as $r) { ?>
                <tr>
                    <td rowspan="2" class="center">
                        <input type="checkbox" class="center chk" name="data[rfp][id][<?php echo $r['MstFacilityItems']['id']?>]"  value="<?php echo $r['MstFacilityItems']['price']; ?>" />
                    </td>
                    <td><?php echo h_out($r['MstFacilityItems']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItems']['item_name']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItems']['item_code']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItems']['unit_name']); ?></td>

                </tr>
                <tr>
                    <td><?php echo h_out($r['MstFacilityItems']['jan_code'],'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItems']['standard']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($r['MstFacilityItems']['price']),'right'); ?></td>
                    <td ><input type="text" maxlength="200" class="tbl_txt recital" name="data[rfp][<?php echo $r['MstFacilityItems']['id']; ?>][recital]" id="bikou<?php echo $cnt; ?>" /></td>
                </tr>
                <?php $cnt++; } ?>
            </tbody>
        </table>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
        <input type="button" class="btn btn29 [p2]" id="btn_add_result" name="btn_add_result"/>
    </div>
</form>
