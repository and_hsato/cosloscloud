<script type="text/javascript">
    $(document).ready(function(){
        //チェックボックス一括変更
        $('.all_check').click(function(){
            $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
        });
  
        // 確定ボタン押下
        $("#btn_add_result").click(function(){
            var cnt = 0;
            var canCommit = true;
            $('input[type=checkbox].chk:checked').each(function(i){
                cnt++;
                if(!$(this).parent(':eq(0)').parent(':eq(0)').next(':eq(0)').find('input[type=radio]:checked').val()){
                    canCommit = false;
                }
            });
            if( cnt === 0 ){
                alert('登録を行う商品を選択してください');
                return false;
            }
            if(canCommit === true){
                $("#form_add_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/decision_result').submit();
            } else {
                alert('価格を選択してください');
                return false;
            }
        });

    });

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>login/home" >TOP</a></li>
        <li class="pankuzu">見積確定</li>
        <li>見積確定確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>見積確定</span></h2>
<h2 class="HeaddingMid">以下の見積確定を行います。</h2>

<form class="validate_form" id="form_add_result" action="#" method="post">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>適用開始日</th>
                <td><?php echo($this->form->text('Bid.start_date',array('class'=>'r date validate[required,custom[date]]','maxlength'=>'10','id'=>'start_date','label'=>''))); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo($this->form->text('Bid.recital',array('class'=>'txt','label'=>'',"maxlength"=>200))); ?></td>
            </tr>
        </table>
    </div>
    <hr class="Clear" />
    <div class="results">
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
        <span class="DisplaySelect">
            　表示件数：<?php echo count($result);  ?>件
        </span>
    </div>
    <div class="TableScroll" id="containTables">
        <table class="TableStyle01 table-even2" id="inputTable">
            <colgroup>
                <col width="25" />
                <col width="80"/>
                <col />
                <col />
                <col width="80"/>
                <col width="120"/>
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <thead>
                <tr>
                    <th rowspan="2" class="center"><input type="checkbox" class="all_check"/></th>
                    <th rowspan="2">商品ID</th>
                    <th rowspan="2">商品名</th>
                    <th rowspan="2">製品番号</th>
                    <th rowspan="2">包装単位</th>
                    <th rowspan="2">JANコード</th>
                    <th rowspan="2">規格</th>
                    <th>業者名1</th>
                    <th>業者名2</th>
                    <th>業者名3</th>
                </tr>
                <tr>
                    <th>価格1</th>
                    <th>価格2</th>
                    <th>価格3</th>
                </tr>
            </thead>
            <tbody>
<?php 
         $cnt=0;
         foreach($result as $r) {
            ?>
                <tr>
                    <td rowspan="2" class="center">
                        <input type="checkbox" class="center chk" name="data[Bid][id][<?php echo $r['Bid']['id']?>]"  value="<?php echo $r['Bid']['id']; ?>" />
                    </td>
                    <td rowspan="2"><?php echo h_out($r['Bid']['internal_code'],'center'); ?></td>
                    <td rowspan="2"><?php echo h_out($r['Bid']['item_name']); ?></td>
                    <td rowspan="2"><?php echo h_out($r['Bid']['item_code']); ?></td>
                    <td rowspan="2"><?php echo h_out($r['Bid']['unit_name']); ?></td>
                    <td rowspan="2"><?php echo h_out($r['Bid']['jan_code'],'center'); ?></td>
                    <td rowspan="2"><?php echo h_out($r['Bid']['standard']); ?></td>
                    <td><?php echo h_out($r['Bid']['proposal_supplier1']); ?></td>
                    <td><?php echo h_out($r['Bid']['proposal_supplier2']); ?></td>
                    <td><?php echo h_out($r['Bid']['proposal_supplier3']); ?></td>

                </tr>
                <tr>
                    <td><?php echo $this->form->radio('Bid.proposal_id.'.$r['Bid']['id'] , array($r['Bid']['proposal_id1'] => $this->Common->toCommaStr($r['Bid']['proposal_price1'])) , array('hiddenField'=>false , 'label'=>false , 'class'=>'')); ?></td>
                    <td><?php echo $this->form->radio('Bid.proposal_id.'.$r['Bid']['id'] , array($r['Bid']['proposal_id2'] => $this->Common->toCommaStr($r['Bid']['proposal_price2'])) , array('hiddenField'=>false , 'label'=>false , 'class'=>'')); ?></td>
                    <td><?php echo $this->form->radio('Bid.proposal_id.'.$r['Bid']['id'] , array($r['Bid']['proposal_id3'] => $this->Common->toCommaStr($r['Bid']['proposal_price3'])) , array('hiddenField'=>false , 'label'=>false , 'class'=>'')); ?></td>
                </tr>
        <?php $cnt++;//セルの色変更用
             } //endforeach
        ?>
            </tbody>
        </table>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
        <input type="button" value="登録" class="common-button [p2]" id="btn_add_result" />
    </div>
</form>
</div>