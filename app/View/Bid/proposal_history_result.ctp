<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/proppsal_history">見積履歴</a></li>
        <li>見積履歴結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>見積履歴結果</span></h2>
<div class="Mes01">見積依頼の取消を行いました</div>
<form class="validate_form" id="form_add_result" action="#" method="post">
    <hr class="Clear" />
    <div class="results">
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
        <span class="DisplaySelect">
            　表示件数：<?php echo count($result);  ?>件
        </span>
    </div>
    <div class="TableScroll" id="containTables">
        <table class="TableStyle01 table-even">
            <colgroup>
                <col width="80"/>
                <col />
                <col />
                <col width="80"/>
                <col width="120"/>
                <col />
                <col />
                <col />
            </colgroup>
            <thead>
                <tr>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>JANコード</th>
                    <th>規格</th>
                    <th>業者名</th>
                    <th>価格</th>
                </tr>
            </thead>
            <tbody>
<?php 
         $cnt=0;
         foreach($result as $r) {
            ?>
                <tr>
                    <td><?php echo h_out($r['Bid']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Bid']['item_name']); ?></td>
                    <td><?php echo h_out($r['Bid']['item_code']); ?></td>
                    <td><?php echo h_out($r['Bid']['unit_name']); ?></td>
                    <td><?php echo h_out($r['Bid']['jan_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Bid']['standard']); ?></td>
                    <td><?php echo h_out($r['Bid']['facility_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($r['Bid']['price'])); ?></td>
                </tr>
        <?php $cnt++;//セルの色変更用
             } //endforeach
        ?>
            </tbody>
        </table>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
</form>