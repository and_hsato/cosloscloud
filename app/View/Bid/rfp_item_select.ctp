<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#facilityItemForm #searchForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#facilityItemForm #cartForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });

    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );

    //検索ボタン押下
    $("#btn_Search").click(function(){
        var tmpid = '';
        $("#facilityItemForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);

        $("#facilityItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/rfp_item_select#search_result').submit();
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        $('#itemSelectedTable').empty();
        $("#facilityItemForm #searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
                cnt++;
            }
        });
        if(cnt === 0 ){
            alert('商品を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        //選択チェック
        var cnt = 0;
        var tmpid = '';
        $("#facilityItemForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);
        if(cnt === 0){
            alert('追加する商品を選択してください');
        }else{
            $("#facilityItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/rfp_item_select_confirm').submit();
        }
    });

});

function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>">TOP</a></li>
        <li class="pankuzu">見積依頼</li>
        <li>Myマスタ選択</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>見積依頼</span></h2>
<h2 class="HeaddingMid">見積依頼を行う商品を選択してください</h2>
<?php echo $this->form->create( 'FacilityItems',array('type' => 'post','action' => '' ,'id' =>'facilityItemForm','class' => 'validate_form search_form') );?>
    <?php echo $this->form->input('add_search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('cart.tmpid',array('type'=>'hidden','id' =>'tmpid'));?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFacilityItems.internal_code',array('class'=>'txt search_internal_code','style'=>'width:120px')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFacilityItems.item_code',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
                <th></th>
                <td><?php echo $this->form->input('MstFacilityItems.is_main' , array('type'=>'checkbox'))?>代表仕入れのみ表示</td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFacilityItems.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFacilityItems.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th></th>
                <td><?php echo $this->form->input('MstFacilityItems.is_bid_only' , array('type'=>'checkbox'))?>見積依頼可能商品のみ表示</td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFacilityItems.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('MstFacilityItems.jan_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>

        <div class="ButtonBox">
           <p class="center">
             <input type="button" value="検索" class="common-button" id="btn_Search"/>
           </p>
        </div>
    </div>
    <div align="right" id="pageTop" ></div>

    <div class="results">
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <colgroup>
            <col width="30" />
            <col width="60" />
            <col />
            <col />
            <col />
            <col width="100"/>
            <col width="100"/>
          </colgroup>
          <tr>
            <th class="center" ><input type="checkbox" class="checkAll_1"></th>
            <th>商品ID</th>
            <th>商品名</th>
            <th>規格</th>
            <th>製品番号</th>
            <th>販売元</th>
            <th>包装単位</th>
          </tr>
        </table>
      </div>
    
<?php
    if(isset($this->request->data['add_search']['is_search'])){
      if(count($SearchResult) === 0){
?>
      <span>該当するデータはありません。</span>
<?php
      }else{
?>


      <div class="TableScroll" id="searchForm">
        <?php $_cnt=0;foreach($SearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row["MstFacilityItems"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <colgroup>
            <col width="30" />
            <col width="60" />
            <col />
            <col />
            <col />
            <col width="100"/>
            <col width="100"/>
          </colgroup>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php echo "itemSelectedRow".$_row["MstFacilityItems"]['id'];?> >
            <td class="center">
            <?php if (!empty($_row["MstFacilityItems"]['mst_grandmaster_id'])) { ?>
              <?php echo $this->form->checkbox("SelectedID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row["MstFacilityItems"]["id"] , 'style'=>'width:20px;text-align:center;') ); ?>
            <?php } ?>
            </td>
            <td><?php echo h_out($_row['MstFacilityItems']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($_row['MstFacilityItems']['item_name']) ; ?></td>
            <td><?php echo h_out($_row['MstFacilityItems']['standard']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItems']['item_code']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItems']['dealer_name']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItems']['unit_name']); ?></td>
          </tr>

        </table>
        <?php $_cnt++;endforeach;?>
      </div>

      <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
          <input type="button" class="common-button" id="cmdSelect" value="選択"/>
        </p>
      </div>
<?php
      }
?>

    <?php if(isset($this->request->data['add_search']['is_search'])){?>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <colgroup>
            <col width="30" />
            <col width="60" />
            <col />
            <col />
            <col />
            <col width="100"/>
            <col width="100"/>
          </colgroup>
          <tr>
            <th class="center"><input type="checkbox" class="checkAll_2" checked></th>
            <th>商品ID</th>
            <th>商品名</th>
            <th>規格</th>
            <th>製品番号</th>
            <th>販売元</th>
            <th>包装単位</th>
          </tr>
        </table>
    </div>
    <div class="TableScroll" id="cartForm">
      <?php $_cnt=0;foreach($CartSearchResult as $_row){ ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row["MstFacilityItems"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <colgroup>
            <col width="30" />
            <col width="60" />
            <col />
            <col />
            <col />
            <col width="100"/>
            <col width="100"/>
          </colgroup>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php echo "itemSelectedRow".$_row['MstFacilityItems']['id'];?> >
            <td class="center">
              <?php echo $this->form->checkbox("SelectedID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row['MstFacilityItems']['id'] , 'style'=>'width:20px;text-align:center;' , 'checked'=>'checked') ); ?>
            </td>
            <td><?php echo h_out($_row['MstFacilityItems']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($_row['MstFacilityItems']['item_name']) ; ?></td>
            <td><?php echo h_out($_row['MstFacilityItems']['standard']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItems']['item_code']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItems']['dealer_name']); ?></td>
            <td><?php echo h_out($_row['MstFacilityItems']['unit_name']); ?></td>
          </tr>

        </table>
      <?php $_cnt++;}?>
      <div id="itemSelectedTable"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" class="common-button" value="確認"/>
      </p>
    </div>
    <?php }?>
<?php } ?>

<?php echo $this->form->end();?>
<div align="right" id="pageDow" ></div>
</div>