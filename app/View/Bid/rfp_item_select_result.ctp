<script type="text/javascript">
    $(document).ready(function(){
       $('input.lbl').attr('readonly', 'readonly');
    });

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>">TOP</a></li>
        <li class="pankuzu">見積依頼</li>
        <li>見積依頼結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>見積依頼登録確認</span></h2>
    <div class="SearchBox">
<div class="Mes01">見積依頼の登録を行いました</div>
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>締切日</th>
                <td><?php echo($this->form->text('limit_date',array('class'=>'lbl'))); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo($this->form->text('recital',array('class'=>'lbl'))); ?></td>
            </tr>
        </table>
    </div>
    
<hr class="Clear" />
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td>表示件数：<?php echo count($result) ; ?>件</td>
        <td align="right">
        </td>
    </tr>
</table>
<div class="TableHeaderAdjustment01">
    <table class="TableHeaderStyle01">
        <tr>
            <th class="col10">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th class="col10">包装単位</th>
        </tr>
        <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>備考</th>
        </tr>
    </table>
</div>
<div class="TableScroll">
    <table class="TableStyle01 table-even2">
        <?php $i=0;foreach($result as $r): ?>
        <tr>
            <td class="col10"><?php echo h_out($r['RfpItem']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($r['RfpItem']['item_name']); ?></td>
            <td><?php echo h_out($r['RfpItem']['item_code']); ?></td>
            <td class="col10"><?php echo h_out($r['RfpItem']['unit_name']); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo h_out($r['RfpItem']['standard']); ?></td>
            <td><?php echo h_out($r['RfpItem']['dealer_name']); ?></td>
            <td><?php echo h_out($r['RfpItem']['recital']); ?></td>
        </tr>
        <?php $i++;endforeach; ?>
    </table>
</div>
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
</div>