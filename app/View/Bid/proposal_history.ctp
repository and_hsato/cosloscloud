<script  type="text/javascript">
$(document).ready(function() {
    //検索ボタン押下
     $("#btn_Search").click(function(){
         validateDetachSubmit($('#BidInfo') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/proposal_history' );
     });

    //明細表示ボタン押下
    $("#btn_Conf").click(function(){
        $("#BidInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/proposal_history_confirm').submit();
    });

    //削除ボタン押下
    $("#btn_Del").click(function(){
        $("#BidInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/proposal_history_result').submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>見積履歴</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>見積履歴</span></h2>
<h2 class="HeaddingMid">変更を行いたい見積履歴を検索してください</h2>
    <form method="post" action="" accept-charset="utf-8" id="BidInfo" class="validate_form search_form">
        <?php echo $this->form->input('Bid.is_search' , array('type'=>'hidden'))?>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>見積番号</th>
                    <td><?php echo $this->form->input('Bid.work_no' , array('class'=>'txt'))?></td>
                    <td></td>
                    <th>締切日</th>
                    <td>
                        <?php echo $this->form->input('Bid.limit_date_from' , array('type'=>'text' , 'class'=>'date txt validate[optional,custom[date]]' , 'id'=>'datepicker1'))?>
                      &nbsp;～&nbsp;
                        <?php echo $this->form->input('Bid.limit_date_to' , array('type'=>'text' , 'class'=>'date txt validate[optional,custom[date]]' , 'id'=>'datepicker2'))?>
                    </td>
                    <td></td>
                    <th></th>
                    <td>
                      <?php echo $this->form->input('Bid.is_deleted' , array('type'=>'checkbox'))?>取消も表示する
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品ID</th>
                    <td><?php echo $this->form->input('Bid.internal_code' , array('class'=>'txt search_internal_code'))?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->input('Bid.item_code' , array('class'=>'txt search_upper'))?></td>
                    <td></td>
                    <th></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->input('Bid.item_name' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->input('BId.dealer_name' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                    <th>規格</th>
                    <td><?php echo $this->form->input('Bid.standard' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="btn btn1" id="btn_Search"/>
        </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th style="width:20px;"></th>
                    <th style="width:135px;">見積番号</th>
                    <th style="width:85px;">締切日</th>
                    <th>登録者</th>
                    <th style="width:140px;">登録日時</th>
                    <th>備考</th>
                    <th>入札件数</th>
                    <th>商品件数</th>
                </tr>
                <?php $cnt=0; foreach($result as $r){ ?>
                <tr>
                    <td class="center"><?php echo $this->form->radio('Bid.id' , array($r['Bid']['id'] => '') , array('hiddenField'=>false , 'label'=>false , 'class'=>'validate[required] ')); ?></td>
                    <td><?php echo h_out($r['Bid']['work_no']); ?></td>
                    <td><?php echo h_out($r['Bid']['limit_date']); ?></td>
                    <td><?php echo h_out($r['Bid']['user_name']); ?></td>
                    <td><?php echo h_out($r['Bid']['created'],'center'); ?></td>
                    <td><?php echo h_out($r['Bid']['recital']); ?></td>
                    <td><?php echo h_out($r['Bid']['count'],'right'); ?></td>
                    <td><?php echo h_out($r['Bid']['detail_count'],'right'); ?></td>
                </tr>
                <?php $cnt++; } ?>
                <?php if( count($result)==0 && isset($this->request->data['Bid']['is_search']) ){ ?>
                <tr><td colspan="8" class="center">該当するデータがありませんでした</td></tr>
                <?php } ?>
            </table>
        </div>
        <?php if(count($result) > 0){ ?>
        <div class="ButtonBox">
            <input type="button" class="btn btn7" id="btn_Conf"/>
            <input type="button" class="btn btn39" id="btn_Del"/>
        </div>
        <?php } ?>
    </div>
</form>
