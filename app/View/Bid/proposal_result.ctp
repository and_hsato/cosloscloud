<script type="text/javascript">
    $(document).ready(function(){
       $('input.lbl').attr('readonly', 'readonly');
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>">TOP</a></li>
        <li class="pankuzu">見積登録</li>
        <li>見積登録結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>見積登録</span></h2>
<div class="Mes01">見積登録を行いました</div>
<hr class="Clear" />
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td>表示件数：<?php echo count($result) ; ?>件</td>
        <td align="right">
        </td>
    </tr>
</table>
<div class="TableHeaderAdjustment01">
    <table class="TableHeaderStyle01">
        <tr>
            <th class="col10">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th class="col10">包装単位</th>
        </tr>
        <tr>
            <th>JANコード</th>
            <th>規格</th>
            <th>金額</th>
            <th>備考</th>
        </tr>
    </table>
</div>
<div class="TableScroll">
    <table class="TableStyle01 table-even2">
        <?php $i=0;foreach($result as $r): ?>
        <tr>
            <td class="col10"><?php echo h_out($r['MstFacilityItems']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($r['MstFacilityItems']['item_name']); ?></td>
            <td><?php echo h_out($r['MstFacilityItems']['item_code']); ?></td>
            <td class="col10"><?php echo h_out($r['MstFacilityItems']['unit_name']); ?></td>
        </tr>
        <tr>
            <td><?php echo h_out($r['MstFacilityItems']['jan_code']); ?></td>
            <td><?php echo h_out($r['MstFacilityItems']['standard']); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($r['MstFacilityItems']['price']),'right'); ?></td>
            <td><?php echo h_out($r['MstFacilityItems']['recital']); ?></td>
        </tr>
        <?php $i++;endforeach; ?>
    </table>
</div>
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
