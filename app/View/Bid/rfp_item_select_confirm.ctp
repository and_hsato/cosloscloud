<script type="text/javascript">
    $(document).ready(function(){
        // チェックボックス一括制御
        //$(".all_check").click(function(){
        //    $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
        //});

        //チェックボックス制御 (業者)
        $('.checkAllEdi').click(function(){
            $('input[type=checkbox].chkEdi').attr('checked', $(this).attr('checked'));
        });
        
        // 確定ボタン押下
        $("#btn_add_result").click(function(){
            if($('input[type=checkbox].chkEdi:checked').length === 0){
                alert('仕入先を選択してください');
                return false;
            }else{
                $("#form_add_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/rfp_item_select_result').submit();
            }
        });

    });

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>login/home" >TOP</a></li>
        <li class="pankuzu">見積依頼</li>
        <li>見積依頼確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>Myマスタ選択</span></h2>
<h2 class="HeaddingMid">以下商品の見積依頼を登録を行います。</h2>

<form class="validate_form" id="form_add_result" action="#" method="post">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>締切日</th>
                <td><?php echo($this->form->text('limit_date',array('class'=>'r date validate[required,custom[date]]','maxlength'=>'10','id'=>'limit_date','label'=>''))); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo($this->form->text('recital',array('class'=>'txt','label'=>'',"maxlength"=>200))); ?></td>
            </tr>
        </table>
    </div>
    <hr class="Clear" />
    <div class="results">
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
        <span class="DisplaySelect">
            　表示件数：<?php echo count($SearchResult);  ?>件
        </span>
    </div>
    <div class="TableScroll" id="containTables">
        <table class="TableStyle01 table-even2">
            <colgroup>
                <!--<col width="25" />-->
                <col />
                <col />
                <col />
            </colgroup>
            <thead>
                <tr>
                    <!--<th rowspan="2" class="center"><input type="checkbox" class="all_check"/></th>-->
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>備考</th>
                </tr>
            </thead>
            <tbody>
                <?php $cnt=0; foreach($SearchResult as $r) { ?>
                <input type="hidden" name="data[grandmaster_ids][]" value="<?php echo $r['MstFacilityItems']['mst_grandmaster_id']; ?>" />
                <tr>
                    <!--<td rowspan="2" class="center">
                        <input type="checkbox" class="center chk" name="data[grandmaster_ids][]"  value="<?php echo $r['MstFacilityItems']['mst_grandmaster_id']; ?>" />
                    </td>-->
                    <td><?php echo h_out($r['MstFacilityItems']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItems']['item_name']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItems']['item_code']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItems']['unit_name']); ?></td>

                </tr>
                <tr>
                    <td></td>
                    <td><?php echo h_out($r['MstFacilityItems']['standard']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItems']['dealer_name']); ?></td>
                    <td ><input type="text" maxlength="200" class="tbl_txt recital" name="data[recitals][<?php echo $r['MstFacilityItems']['mst_grandmaster_id']; ?>][recital]" id="bikou<?php echo $cnt; ?>" /></td>
                </tr>
                <?php $cnt++; } ?>
            </tbody>
        </table>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
        <table class="conciergetable table-even">
            <colgroup>
                <col width="20px"/>
                <col />
                <col />
            </colgroup>
            <tr style="background:#f7f7f7">
                <td colspan="3" style="font-size:14px">仕入先一覧 ※以下仕入先へ一括見積依頼を行います。</td>
            </tr>
            <tr>
                <td align="center"><input type="checkbox" class="checkAllEdi" ></td>
                <th>施設コード</th>
                <th>施設名</th>
            </tr>
         <?php foreach ($edis as $d) { $edi = $d['MstFacility']; ?>
            <tr>
                <td align="center"><input type="checkbox" class="chkEdi" name="data[edi_ids][]" value="<?php echo $edi['id'] ?>" ></td>
                <td><?php echo $edi['facility_code'] ?></td>
                <td><?php echo $edi['facility_name'] ?></td>
            </tr>
         <?php } ?>
        </table>
    <br />
    <div class="ButtonBox" id="page_footer">
        <input type="button" class="common-button [p2]" id="btn_add_result" value="確定"/>
    </div>
</form>
</div>