<div id="TopicPath">
<ul>
  <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
  <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_<?php echo ($retroact_type == '1') ? "stock" : "sales"; ?>price"><?php echo ($retroact_type == '1') ? "仕入" : "売上"; ?>価格変更</a></li>
  <li class="pankuzu">変更条件指定</li>
  <li class="pankuzu">変更条件確認</li>
  <li><?php echo ($retroact_type == '1') ? "仕入" : "売上"; ?>価格変更結果</li>
</ul>
</div>
<h2 class="HeaddingLarge"><span><?php echo ($retroact_type == '1') ? "仕入" : "売上"; ?>価格変更結果</span></h2>
<div class="Mes01"><?php echo $count; ?>件の商品で<?php echo ($retroact_type == '1') ? "仕入" : "売上"; ?>価格の変更を行いました</div>
<form>
<div class="SearchBox">
<?php echo $this->element('retroacts' . DS . 'header' , array('page_type' => 'confirm')); ?>
</div>

<!-- end Result --></form>
