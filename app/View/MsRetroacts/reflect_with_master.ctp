<script type="text/javascript">
$(function(){
  $("#retroacts_reflect_with_master_form_btn").click(function(){
    $("#retroacts_reflect_with_master_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/reflect_with_master_confirm');
    $("#retroacts_reflect_with_master_form").attr('method', 'post');
    $("#retroacts_reflect_with_master_form").submit();
  });
});

 // 日付妥当性チェック
 function validate2date(date_data){
  var date = date_data.val();
  var date_split;
  var di;
  if(date.match(/^[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}$/)){
    date_split = date.split("/");
    di = new Date(date_split[0],date_split[1]-1,date_split[2]);
    if(di.getFullYear() == date_split[0] && di.getMonth() == date_split[1]-1 && di.getDate() == date_split[2]){
      return true;
    } else {
      return false;
    }
  }
  return true;
}
</script>
<div id="TopicPath">
<ul>
  <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
  <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_<?php echo ($retroact_type == '1') ? "stock" : "sales"; ?>price"><?php echo ($retroact_type == '1') ? "仕入" : "売上"; ?>価格変更</a></li>
  <li>変更条件指定</li>
</ul>
</div>
<h2 class="HeaddingLarge"><span>変更条件指定</span></h2>
<h2 class="HeaddingMid">価格変更の対象となる条件を指定します。</h2>
<form id="retroacts_reflect_with_master_form" class="validate_form">
<div class="SearchBox">
<?php echo $this->element('retroacts' . DS . 'header' , array('page_type' => 'reflect_with_master')); ?>
</div>
<!--// ListTable DoubleHeader -->
<div class="ButtonBox"><input type="button" value="" class="btn btn3" id="retroacts_reflect_with_master_form_btn" /></div>
</div>
</div>
<!-- end Result -->
</form>