<script type="text/javascript">
//jquery calendar
$(function($){
  $("#datepicker1").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
  $("#datepicker2").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
});


$(function(){
  $("#retroacts_stockprice_with_master_btn").click(function(){
    $("#retroacts_stockprice_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/reflect_with_master/3');
    $("#retroacts_stockprice_form").attr('method', 'post');
    $("#retroacts_stockprice_form").submit();
  });
  $("#retroacts_stockprice_with_unitprice_btn").click(function(){
    $("#retroacts_stockprice_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/reflect_with_unitprice/3');
    $("#retroacts_stockprice_form").attr('method', 'post');
    $("#retroacts_stockprice_form").submit();
  });
  $("#retroacts_stockprice_with_csv_btn").click(function(){
    $("#retroacts_stockprice_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/reflect_with_csv/3');
    $("#retroacts_stockprice_form").attr('method', 'post');
    $("#retroacts_stockprice_form").submit();
  });
});

$(function(){
  $("#owner_code_select").change(function(){
    $("#owner_code_name").val($("#owner_code_select option:selected").text());
    $("#owner_code_txt").val($("#owner_code_select option:selected").val());
  });

  $("#supplier_code_select").change(function(){
    $("#supplier_code_name").val($("#supplier_code_select option:selected").text());
    $("#supplier_code_txt").val($("#supplier_code_select option:selected").val());
  });
});

 // 日付妥当性チェック
 function validate2date(date_data){
  var date = date_data.val();
  var date_split;
  var di;
  if(date.match(/^[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}$/)){
    date_split = date.split("/");
    di = new Date(date_split[0],date_split[1]-1,date_split[2]);
    if(di.getFullYear() == date_split[0] && di.getMonth() == date_split[1]-1 && di.getDate() == date_split[2]){
      return true;
    } else {
      return false;
    }
  }
  return true;
}
</script>
<div id="TopicPath">
<ul>
  <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
  <li>仕入価格変更</li>
</ul>
</div>
<h2 class="HeaddingLarge"><span>仕入価格変更</span></h2>
<h2 class="HeaddingMid">価格変更の条件を指定します。</h2>
<form id="retroacts_stockprice_form" class="validate_form">
<div class="SearchBox">
<table class="FormStyleTable">
  <colgroup>
    <col>
    <col>
    <col width="20">
    <col>
    <col>
  </colgroup>
  <tr>
    <th>施主</th>
    <td>
    <?php echo $this->form->input('TrnRetroactHeader.mst_facility', array('type'=>'text' , 'class'=>'r', 'style'=>'width: 60px;', 'id'=>'owner_code_txt')); ?> <?php echo $this->form->input('TrnRetroactHeader.mst_facility_code',array('options'=>$owners, 'empty'=>true, 'class'=>'r txt validate[required]', 'id'=>'owner_code_select')); ?>
    <?php echo $this->form->input('TrnRetroactHeader.mst_facility_name', array('type'=>'hidden', 'id'=>'owner_code_name')); ?>
    </td>
    <td></td>
    <th>作業区分</th>
    <td>
    <?php echo $this->form->input('TrnRetroactHeader.work_class',array('options'=>$work_types, 'empty'=>true, 'class'=>'txt')); ?>
   </td>
  </tr>
  <tr>
    <th>仕入先</th>
    <td>
    <?php echo $this->form->input('TrnRetroactHeader.facility_to', array('type'=>'text' , 'class'=>'r', 'style'=>'width: 60px;', 'id'=>'supplier_code_txt')); ?> <?php echo $this->form->input('TrnRetroactHeader.facility_to_code',array('options'=>$suppliers, 'empty'=>true, 'class'=>'r txt validate[required]', 'id'=>'supplier_code_select')); ?>
    <?php echo $this->form->input('TrnRetroactHeader.facility_to_name', array('type'=>'hidden', 'id'=>'supplier_code_name')); ?>
    </td>
    <td></td>
    <th>備考</th>
    <td><?php echo $this->form->input('TrnRetroactHeader.recital', array('type'=>'text', 'class'=>'txt', 'maxlength' => '200')); ?></td>
  </tr>
  <tr>
    <th>遡及期間</th>
    <td> <?php echo $this->form->input('TrnRetroactHeader.start_date',array('type'=>'text','class'=>'r txt date validate[required,custom[date],funcCall2[date2]]','id'=>'datepicker1')); ?> &nbsp;～&nbsp; <?php echo $this->form->input('TrnRetroactHeader.end_date',array('type'=>'text','class'=>'r txt date validate[required,custom[date],funcCall2[date2]]','id'=>'datepicker2')); ?></td>
    <td></td>
  </tr>
</table>
<div class="ButtonBox">
<input type="button" value="" class="btn btn23" id="retroacts_stockprice_with_master_btn" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" value="" class="btn btn40" id="retroacts_stockprice_with_unitprice_btn" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" value="" class="btn btn22" id="retroacts_stockprice_with_csv_btn" />
</div>
</div>
</form>
