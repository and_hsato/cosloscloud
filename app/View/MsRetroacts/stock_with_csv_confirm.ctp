<script type="text/javascript">
    $(function(){
        //日付け入力欄
        $(function($){
            $("#datepicker1").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
        });

        //確定ボタン押下時
        $("#confirm_btn").click(function(){
            if(!validate2date($("#datepicker1").val())){
                alert("計上日が不正です");
                return false;
            }

            $("#ConfirmForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/stock_with_csv_result/');
            $("#ConfirmForm").attr('method', 'post');
            $("#ConfirmForm").submit();
        });
    });

    //日付妥当性チェック
    function validate2date(date){
        var date_split;
        var di;
        if(date.match(/^[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}$/)){
            date_split = date.split("/");
            di = new Date(date_split[0],date_split[1]-1,date_split[2]);
            if(di.getFullYear() == date_split[0] && di.getMonth() == date_split[1]-1 && di.getDate() == date_split[2]){
                return true;
            }else{
                return false;
            }
        }else{
            //書式不正
            return false;
        }
        return true;
    }
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/stock">仕入赤黒作成</a></li>
        <li class="pankuzu">遡及条件指定</li>
        <li>遡及条件確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>遡及条件確認</span></h2>
<h2 class="HeaddingMid">以下の条件で遡及を行います</h2>
<form class="validate_form" method="post" action="" id="ConfirmForm">
    <div class="SearchBox" style="margin-bottom:20px;">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->hidden('Condition.owner_id',array('id'=>'owner_id',"value"=>$data["Condition"]["owner_id"])); ?>
                    <?php echo $this->form->input('Condition.owner_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'owner_name',"value"=>$data["Condition"]["owner_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->hidden('Condition.class_id',array('id'=>'class_id',"value"=>$data["Condition"]["class_id"])); ?>
                    <?php echo $this->form->input('Condition.class_name',array("type"=>"text",'class'=>'lbl',"style"=>"width:200px;","id"=>"class_name","value"=>$data["Condition"]["class_name"],"readonly"=>"readonly")); ?>
                </td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->hidden('Condition.supplier_id',array('id'=>'supplier_id',"value"=>$data["Condition"]["supplier_id"])); ?>
                    <?php echo $this->form->hidden('Condition.supplier_code',array('id'=>'supplier_code',"value"=>$data["Condition"]["supplier_code"])); ?>
                    <?php echo $this->form->hidden('Condition.supplier_department_id',array('id'=>'supplier_department_id',"value"=>$data["Condition"]["supplier_department_id"])); ?>
                    <?php echo $this->form->input('Condition.supplier_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'supplier_name',"value"=>$data["Condition"]["supplier_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Condition.recital',array('type'=>'text','class'=>'lbl','maxlength'=>'200',"readonly"=>"readonly","value"=>$data["Condition"]["recital"])); ?>
                </td>
            </tr>
            <tr>
                <th>遡及期間</th>
                <td>
                    <?php echo $this->form->input('Condition.start_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["start_date"],"readonly"=>"readonly")); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('Condition.end_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["end_date"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>計上日</th>
                <td>
                    <?php echo $this->form->input('Condition.posted_sun',array('type'=>'text','class'=>'r txt date',"id"=>"datepicker1")); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>


    <p style="font-size: 12pt;">CSV読込件数<?php echo $read_count; ?>件</p>
    <p style="font-size: 12pt;">対象明細件数<?php echo $regist_count; ?>件</p>
<?php
    if(empty($error_msg)){
?>
    <?php echo $this->form->hidden('Condition.trn_claims_id',array('id'=>'trn_claims_id',"value"=>$ids)); ?>
    <?php echo $this->form->hidden('Condition.read_count',array('id'=>'read_count',"value"=>$read_count)); ?>
    <?php echo $this->form->hidden('Condition.stocking_start_date',array('id'=>'stocking_start_date',"value"=>$stocking_start_date)); ?>
    <?php echo $this->form->hidden('Condition.chk_date',array('id'=>'chk_date',"value"=>date("Y-m-d H:i:s"))); ?>
    <div class="ButtonBox">
        <input type="button" value="" class="btn btn2 [p2]" id="confirm_btn" />
    </div>
<?php
    }else{
        foreach($error_msg as $value){
            echo $value."<br>";
        }
    }
?>