<script type="text/javascript">
    $(function(){
        $("#confirm_btn").click(function(){
            $("#CsvForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_with_csv_confirm');
            $("#CsvForm").attr('method', 'post');
            $("#CsvForm").submit();
        });
    });
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/sales">売上赤黒作成</a></li>
        <li>遡及条件指定</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>遡及条件指定</span></h2>
<h2 class="HeaddingMid">遡及の対象となる条件を指定します。(CSV)</h2>
<form id="CsvForm" class="validate_form" enctype="multipart/form-data" accept-charset="utf-8" >
    <div class="SearchBox" style="margin-bottom:20px;">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->hidden('Condition.owner_id',array('id'=>'owner_id',"value"=>$data["Condition"]["owner_id"])); ?>
                    <?php echo $this->form->input('Condition.owner_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'owner_name',"value"=>$data["Condition"]["owner_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->hidden('Condition.class_id',array('id'=>'class_id',"value"=>$data["Condition"]["class_id"])); ?>
                    <?php echo $this->form->input('Condition.class_name',array("type"=>"text",'class'=>'lbl',"style"=>"width:200px;","id"=>"class_name","value"=>$data["Condition"]["class_name"],"readonly"=>"readonly")); ?>
                </td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->hidden('Condition.hospital_id',array('id'=>'hospital_id',"value"=>$data["Condition"]["hospital_id"])); ?>
                    <?php echo $this->form->hidden('Condition.hospital_code',array('id'=>'hospital_code',"value"=>$data["Condition"]["hospital_code"])); ?>
                    <?php echo $this->form->input('Condition.hospital_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'hospital_name',"value"=>$data["Condition"]["hospital_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Condition.recital',array('type'=>'text','class'=>'lbl','maxlength'=>'200',"readonly"=>"readonly","value"=>$data["Condition"]["recital"])); ?>
                </td>
            </tr>
            <tr>
                <th>遡及期間</th>
                <td>
                    <?php echo $this->form->input('Condition.start_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["start_date"],"readonly"=>"readonly")); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('Condition.end_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["end_date"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>CSVファイル</th>
                <td>
                    <?php echo $this->form->input('Condition.csv_file',array('type'=>'file','class'=>'r txt validate[required]',"id"=>"csv_file","value"=>"","style"=>"width:250px;")); ?>
                </td>
            </tr>
        </table>
    </div>

    <div class="ButtonBox">
        <input type="button" value="" class="btn btn3" id="confirm_btn" />
    </div>
</form>
