<script type="text/javascript">
$(function(){
  $("#retroacts_reflect_with_master_confirm_form_btn").click(function(){
    $("#retroacts_reflect_with_master_confirm_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/reflect_with_master_result');
    $("#retroacts_reflect_with_master_confirm_form").attr('method', 'post');
    $("#retroacts_reflect_with_master_confirm_form").submit();
  });
});
</script>
<div id="TopicPath">
<ul>
  <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
  <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_<?php echo ($retroact_type == '1') ? "stock" : "sales"; ?>price"><?php echo ($retroact_type == '1') ? "仕入" : "売上"; ?>価格変更</a></li>
  <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/reflect_with_master">変更条件指定</a></li>
  <li>変更条件確認</li>
</ul>
</div>
<h2 class="HeaddingLarge"><span>変更条件確認</span></h2>
<h2 class="HeaddingMid">以下の条件で価格変更を行います</h2>
<form  class="validate_form" id="retroacts_reflect_with_master_confirm_form">
<div class="SearchBox">
<?php echo $this->element('retroacts' . DS . 'header' , array('page_type' => 'reflect_with_master_confirm')); ?>
</div>
<!--// ListTable DoubleHeader --> <br>
<p><font style="font-size: 12pt;">対象商品件数<?php echo count($item_units); ?>件</font></p>
<?php if(count($item_units) > 0){ ?>
<div class="ButtonBox"><input type="button" value="" class="btn btn2 [p2]" id="retroacts_reflect_with_master_confirm_form_btn" /></div>

<?php
  foreach($item_units as $item_key => $item_data){
    echo $this->form->input('TrnRetroactRecord.' . $item_key . '.mst_item_unit_id', array('type'=>'hidden', 'value'=>$item_data['mst_item_unit_id']));
    echo $this->form->input('TrnRetroactRecord.' . $item_key . '.price', array('type'=>'hidden', 'value'=>$item_data['price']));
  }
} ?>
</div>
</div>
<!-- end Result --></form>
