<script type="text/javascript">

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/sales">売上赤黒作成</a></li>
        <li class="pankuzu">遡及条件確認</li>
        <li>遡及完了</li>

    </ul>
</div>

<h2 class="HeaddingLarge"><span>遡及完了</span></h2>
<h2 class="HeaddingMid">遡及が完了しました</h2>

<form class="validate_form" id="ConfirmForm">
    <div class="SearchBox" style="margin-bottom:20px;">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->input('Condition.owner_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'owner_name',"value"=>$data["Condition"]["owner_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('Condition.class_name',array("type"=>"text",'class'=>'lbl',"style"=>"width:200px;","id"=>"class_name","value"=>$data["Condition"]["class_name"],"readonly"=>"readonly")); ?>
                </td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('Condition.hospital_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'supplier_name',"value"=>$data["Condition"]["hospital_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Condition.recital',array('type'=>'text','class'=>'lbl',"readonly"=>"readonly","value"=>$data["Condition"]["recital"])); ?>
                </td>
            </tr>
            <tr>
                <th>遡及期間</th>
                <td>
                    <?php echo $this->form->input('Condition.start_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["start_date"],"readonly"=>"readonly")); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('Condition.end_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["end_date"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>適用開始日</th>
                <td>
                    <?php echo $this->form->input('Condition.stocking_start_date',array('type'=>'text','class'=>'lbl',"value"=>$data["Condition"]["stocking_start_date"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>計上日</th>
                <td>
                    <?php echo $this->form->input('Condition.posted_sun',array('type'=>'text','class'=>'lbl',"value"=>$data["Condition"]["posted_sun"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <!--// ListTable DoubleHeader -->
    <p>
        <font style="font-size: 12pt;">対象期間内請求件数<?php echo $data["Condition"]["rec_count"]; ?>件中　</font>
        <font style="font-size: 12pt;"><?php echo "　".$execution_count; ?>件を遡及しました</font>
    </p>
    <!-- end Result -->
</form>