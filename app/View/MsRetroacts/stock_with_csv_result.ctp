<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/stock">仕入赤黒作成</a></li>
        <li class="pankuzu">遡及条件指定</li>
        <li class="pankuzu">遡及条件確認</li>
        <li>遡及条件完了</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>遡及条件完了</span></h2>
<h2 class="HeaddingMid">以下の条件で遡及を行いました</h2>
<form class="validate_form" method="post" action="" id="ResultForm">
    <div class="SearchBox" style="margin-bottom:20px;">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->input('Condition.owner_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'owner_name',"value"=>$data["Condition"]["owner_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('Condition.class_name',array("type"=>"text",'class'=>'lbl',"style"=>"width:200px;","id"=>"class_name","value"=>$data["Condition"]["class_name"],"readonly"=>"readonly")); ?>
                </td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('Condition.supplier_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'supplier_name',"value"=>$data["Condition"]["supplier_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Condition.recital',array('type'=>'text','class'=>'lbl','maxlength'=>'200',"readonly"=>"readonly","value"=>$data["Condition"]["recital"])); ?>
                </td>
            </tr>
            <tr>
                <th>遡及期間</th>
                <td>
                    <?php echo $this->form->input('Condition.start_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["start_date"],"readonly"=>"readonly")); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('Condition.end_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["end_date"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>計上日</th>
                <td>
                    <?php echo $this->form->input('Condition.posted_sun',array('type'=>'text','class'=>'lbl',"value"=>$data["Condition"]["posted_sun"])); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>


    <p style="font-size: 12pt;">CSV読込件数<?php echo $read_count; ?>件</p>
    <p style="font-size: 12pt;">更新明細件数<?php echo $regist_count; ?>件</p>
