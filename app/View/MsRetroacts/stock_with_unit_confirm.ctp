<script type="text/javascript">
    $(function(){
        //日付け入力欄
        $(function($){
            $("#datepicker1").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
        });

        //確定ボタン押下時
        $("#confirm_btn").click(function(){
            if(!validate2date($("#datepicker1").val())){
                alert("計上日が不正です");
                return false;
            }
            var isChecked = false;
            $("#ConfirmForm input[type=checkbox].all_check").each(function(){
                if($(this).attr("checked") == true){
                    //チェック有無確認
                    isChecked = true;
                }
            });
            if(isChecked){
                $("#ConfirmForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/stock_with_unit_result');
                $("#ConfirmForm").attr('method', 'post');
                $("#ConfirmForm").submit();
            }else{
                alert('明細を選択してください');
                return false;
            }
        });

        //チェックボックスオールチェック
        $('#all_check_1').click(function(){
            $('#ConfirmForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
        });

    });

    //日付妥当性チェック
    function validate2date(date){
        var date_split;
        var di;
        if(date.match(/^[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}$/)){
            date_split = date.split("/");
            di = new Date(date_split[0],date_split[1]-1,date_split[2]);
            if(di.getFullYear() == date_split[0] && di.getMonth() == date_split[1]-1 && di.getDate() == date_split[2]){
                return true;
            }else{
                return false;
            }
        }else{
            //書式不正
            return false;
        }
        return true;
    }

    //パンくずクリック
    function return1(){
        $("#ConfirmForm").attr('action',"<?php echo $this->webroot; ?><?php echo $this->name; ?>/stock_with_unit_search");
        $("#ConfirmForm").attr('method', 'post');
        $("#ConfirmForm").submit();
        return false;
    }
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/stock">仕入赤黒作成</a></li>
        <li><a href="#" id="return1" onclick="return1();">遡及対象選択</a></li>
        <li>遡及対象確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>遡及対象確認</span></h2>
<h2 class="HeaddingMid">以下の条件で遡及を行います</h2>

<form class="validate_form" id="ConfirmForm">
    <?php //画面の引継ぎ項目 ?>
    <?php echo $this->form->hidden("Condition.owner_id",array("value"=>$data["Condition"]["owner_id"])); ?>
    <?php echo $this->form->hidden("Condition.owner_code",array("value"=>$data["Condition"]["owner_code"])); ?>
    <?php echo $this->form->hidden("Condition.owner_name",array("value"=>$data["Condition"]["owner_name"])); ?>
    <?php echo $this->form->hidden("Condition.class_id",array("value"=>$data["Condition"]["class_id"])); ?>
    <?php echo $this->form->hidden("Condition.class_name",array("value"=>$data["Condition"]["class_name"])); ?>
    <?php echo $this->form->hidden("Condition.supplier_id",array("value"=>$data["Condition"]["supplier_id"])); ?>
    <?php echo $this->form->hidden("Condition.supplier_code",array("value"=>$data["Condition"]["supplier_code"])); ?>
    <?php echo $this->form->hidden("Condition.supplier_department_id",array("value"=>$data["Condition"]["supplier_department_id"])); ?>
    <?php echo $this->form->hidden("Condition.supplier_name",array("value"=>$data["Condition"]["supplier_name"])); ?>
    <?php echo $this->form->hidden("Condition.recital",array("value"=>$data["Condition"]["recital"])); ?>
    <?php echo $this->form->hidden("Condition.start_date",array("value"=>$data["Condition"]["start_date"])); ?>
    <?php echo $this->form->hidden("Condition.end_date",array("value"=>$data["Condition"]["end_date"])); ?>
    <div class="SearchBox" style="margin-bottom:20px;">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->hidden('Condition.owner_id',array('id'=>'owner_id',"value"=>$data["Condition"]["owner_id"])); ?>
                    <?php echo $this->form->input('Condition.owner_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'owner_name',"value"=>$data["Condition"]["owner_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->hidden('Condition.class_id',array('id'=>'class_id',"value"=>$data["Condition"]["class_id"])); ?>
                    <?php echo $this->form->input('Condition.class_name',array("type"=>"text",'class'=>'lbl',"style"=>"width:200px;","id"=>"class_name","value"=>$data["Condition"]["class_name"],"readonly"=>"readonly")); ?>
                </td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->hidden('Condition.supplier_id',array('id'=>'supplier_id',"value"=>$data["Condition"]["supplier_id"])); ?>
                    <?php echo $this->form->hidden('Condition.supplier_department_id',array('id'=>'supplier_code',"value"=>$data["Condition"]["supplier_department_id"])); ?>
                    <?php echo $this->form->input('Condition.supplier_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'supplier_name',"value"=>$data["Condition"]["supplier_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Condition.recital',array('type'=>'text','class'=>'lbl','maxlength'=>'200',"readonly"=>"readonly","value"=>$data["Condition"]["recital"])); ?>
                </td>
            </tr>
            <tr>
                <th>遡及期間</th>
                <td>
                    <?php echo $this->form->input('Condition.start_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["start_date"],"readonly"=>"readonly")); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('Condition.end_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["end_date"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>計上日</th>
                <td>
                    <?php echo $this->form->input('Condition.posted_sun',array('type'=>'text','class'=>'r txt date',"id"=>"datepicker1")); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="DisplaySelect">
        <h2 class="HeaddingSmall">選択商品</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th style="width: 20px;" rowspan="2"><input type="checkbox" id="all_check_1" checked></th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th class="col10">適用開始日</th>
                    <th class="col10">仕入単価</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th colspan="2">包装単位</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll" style="">
<?php
        if(isset($result)){
            $cnt=0;
            foreach($result as $row){
?>
            <table class="TableStyle02" border="0" id="SearchRow<?php echo $row["MstTransactionConfig"]["id"];?>" style="margin-top:-1px; margin-bottom:0px; padding:0px;">
                <tr class="<?php echo( $cnt % 2 == 0 ? '' : 'odd' ); ?>" >
                    <td style="width: 20px;" rowspan="2" class="center">
                        <?php echo $this->form->input("Confirm.id".$cnt,array("type"=>"checkbox","class"=>"center all_check","value"=>$row["MstTransactionConfig"]["id"],"hiddenField"=>false,"checked"=>"checked")); ?>
                    </td>
                    <td class="col10">
                        <?php echo h_out($row["MstFacilityItem"]["internal_code"],'center'); ?>
                    </td>
                    <td>
                        <?php echo h_out($row["MstFacilityItem"]["item_name"]); ?>
                    </td>
                    <td>
                        <?php echo h_out($row["MstFacilityItem"]["item_code"]); ?>
                    </td>
                    <td class="col10">
                        <?php echo $this->form->hidden('Condition.stocking_start_date',array("value"=>$row["MstTransactionConfig"]["start_date"])); ?>
                        <?php echo h_out($row["MstTransactionConfig"]["start_date"],'center'); ?>
                    </td>
                    <td class="col10" align="right">
                        <?php echo h_out($this->Common->toCommaStr($row["MstConfig"]["config_price"]),'right'); ?>
                    </td>
                </tr>
                <tr class="<?php echo( $cnt % 2 == 0 ? '' : 'odd' ); ?>">
                    <td></td>
                    <td>
                        <?php echo h_out($row["MstFacilityItem"]["standard"]); ?>
                    </td>
                    <td>
                        <?php echo h($row["MstDealer"]["dealer_name"]); ?>
                    </td>
                    <td colspan="2">
                        <?php echo h($row["MstUnitName"]["unit_name"]); ?>
                    </td>
                </tr>
            </table>
<?php
                $cnt++;
            }//end of foreach
        }//end of if
?>
        </div>

        <?php echo $this->form->hidden('Condition.chk_date',array('id'=>'chk_date',"value"=>date("Y-m-d H:i:s"))); ?>
        <div class="ButtonBox" style="margin-top:10px;">
            <input type="button" class="btn btn2 [p2]" id="confirm_btn" />
        </div>
    </div>
</form>