<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/stock">仕入赤黒作成</a></li>
        <li class="pankuzu">遡及対象選択</li>
        <li class="pankuzu">遡及対象確認</li>
        <li>遡及完了</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>遡及完了</span></h2>
<h2 class="HeaddingMid">以下の条件で遡及が完了しました</h2>

<form class="validate_form" id="ResultForm">
    <div class="SearchBox" style="margin-bottom:20px;">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->input('Condition.owner_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'owner_name',"value"=>$data["Condition"]["owner_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('Condition.class_name',array("type"=>"text",'class'=>'lbl',"style"=>"width:200px;","id"=>"class_name","value"=>$data["Condition"]["class_name"],"readonly"=>"readonly")); ?>
                </td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('Condition.supplier_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'supplier_name',"value"=>$data["Condition"]["supplier_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Condition.recital',array('type'=>'text','class'=>'lbl','maxlength'=>'200',"readonly"=>"readonly","value"=>$data["Condition"]["recital"])); ?>
                </td>
            </tr>
            <tr>
                <th>遡及期間</th>
                <td>
                    <?php echo $this->form->input('Condition.start_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["start_date"],"readonly"=>"readonly")); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('Condition.end_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["end_date"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>計上日</th>
                <td>
                    <?php echo $this->form->input('Condition.posted_sun',array('type'=>'text','class'=>'lbl',"value"=>$data["Condition"]["posted_sun"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="DisplaySelect">
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th style="width: 20px;" rowspan="2"></th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th class="col10">適用開始日</th>
                    <th class="col10">仕入単価</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th colspan="2">包装単位</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll" style="">
<?php
        if(isset($result)){
            $cnt=0;
            foreach($result as $row){
?>
            <table class="TableStyle02" border="0" id="SearchRow<?php echo $row["MstTransactionConfig"]["id"];?>" style="margin-top:-1px; margin-bottom:0px; padding:0px;">
                <tr class="<?php echo( $cnt % 2 == 0 ? '' : 'odd' ); ?>" >
                    <td style="width: 20px;" rowspan="2"></td>
                    <td class="col10">
                        <label title="<?php echo h($row["MstFacilityItem"]["internal_code"]); ?>">
                            <p align="center"><?php echo h($row["MstFacilityItem"]["internal_code"]); ?></p>
                        </label>
                    </td>
                    <td>
                        <label title="<?php echo h($row["MstFacilityItem"]["item_name"]); ?>">
                            <?php echo h($row["MstFacilityItem"]["item_name"]); ?>
                        </label>
                    </td>
                    <td>
                        <label title="<?php echo h($row["MstFacilityItem"]["item_code"]); ?>">
                            <?php echo h($row["MstFacilityItem"]["item_code"]); ?>
                        </label>
                    </td>
                    <td class="col10">
                        <label title="<?php echo h($row["MstTransactionConfig"]["start_date"]); ?>">
                            <p align="center"><?php echo h($row["MstTransactionConfig"]["start_date"]); ?></p>
                        </label>
                    </td>
                    <td class="col10" align="right">
                        <label title="<?php echo h($this->Common->toCommaStr($row["MstConfig"]["config_price"])); ?>">
                            <p align="right"><?php echo h($this->Common->toCommaStr($row["MstConfig"]["config_price"])); ?></p>
                        </label>
                    </td>
                </tr>
                <tr class="<?php echo( $cnt % 2 == 0 ? '' : 'odd' ); ?>">
                    <td></td>
                    <td>
                        <label title="<?php echo h($row["MstFacilityItem"]["standard"]); ?>">
                            <?php echo h($row["MstFacilityItem"]["standard"]); ?>
                        </label>
                    </td>
                    <td>
                        <label title="<?php echo h($row["MstDealer"]["dealer_name"]); ?>">
                            <?php echo h($row["MstDealer"]["dealer_name"]); ?>
                        </label>
                    </td>
                    <td colspan="2">
                        <label title="<?php echo h($row["MstUnitName"]["unit_name"]); ?>">
                            <?php echo h($row["MstUnitName"]["unit_name"]); ?>
                        </label>
                    </td>
                </tr>
            </table>
<?php
                $cnt++;
            }//end of foreach
        }//end of if
?>
        </div>
    </div>
</form>