<script type="text/javascript">

$(document).ready(function(){
   //カレンダー
   $("#datepicker1").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
   //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#facilityItemForm #searchForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#facilityItemForm #cartForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });

    //検索ボタン押下
    $("#btn_Search").click(function(){
        var tmpid = '';
        $("#facilityItemForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);

        $("#facilityItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/reflect_with_unitprice');
        $("#facilityItemForm").submit();
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        $('#itemSelectedTable').empty();
        $("#facilityItemForm #searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
            }
            cnt++;
        });
        if(cnt === 0 ){
            alert('商品を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        //選択チェック
        var cnt = 0;
        var tmpid = '';
        $("#facilityItemForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);
        if(cnt === 0){
            alert('追加する商品を選択してください');
        }else{
            $("#facilityItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/reflect_with_unitprice_confirm');
            $("#facilityItemForm").submit();
        }
    });

});

function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

</script>

<!-- breadcrums (start) -->
<div id="TopicPath">
<ul>
  <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
  <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_<?php echo ($retroact_type == '1') ? "stock" : "sales"; ?>price"><?php echo ($retroact_type == '1') ? "仕入" : "売上"; ?>価格変更</a></li>
  <li>変更条件指定</li>
</ul>
</div>
<h2 class="HeaddingLarge"><span>変更条件指定</span></h2>
<?php if((int)$retroact_type === 1){ ?>
<h2 class="HeaddingMid">価格変更の対象となる仕入条件を指定します。</h2>
<?php } else { ?>
<h2 class="HeaddingMid">価格変更の対象となる売上条件を指定します。</h2>
<?php } ?>
<?php
  echo $this->form->create( 'FacilityItems',array('type' => 'post','action' => '' ,'id' =>'facilityItemForm','class' => 'validate_form search_form') );
?>

    <?php echo $this->form->input('search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('cart.tmpid',array('type'=>'hidden','id' =>'tmpid'));?>

<?php
if( !isset($page_type))
  $page_type = 'default';
?>
    <div class="SearchBox">
       <table class="FormStyleTable">
         <colgroup>
           <col>
           <col>
           <col width="20">
           <col>
           <col>
         </colgroup>
         <tr>
           <th>施主</th>
           <td>
               <?php echo $this->form->input('TrnRetroactHeader.mst_facility_code',array('type' => 'hidden')); ?>
               <?php echo $this->form->input('TrnRetroactHeader.mst_facility',array('type'=>'hidden')); ?>
               <?php echo $this->form->input('TrnRetroactHeader.mst_facility_id',array('type'=>'hidden')); ?>
               <?php echo $this->form->input('TrnRetroactHeader.mst_facility_name',array('id' => 'user_facility_txt', 'label' => false, 'div' => false, 'class' => 'lbl', 'style'=>'width: 180px;' , 'readonly' =>true)); ?>
           </td>
           <td></td>
           <th>作業区分</th>
           <td>
               <?php echo $this->form->input('TrnRetroactHeader.work_class',array('type'=>'hidden')); ?>
               <?php echo $this->form->input('TrnRetroactHeader.work_class_txt',array('class'=>'lbl','readonly'=>'readonly')); ?>
           </td>
         </tr>
         <tr>
           <th><?php echo isset($retroact_type) && $retroact_type == 1 ? '仕入先' : '得意先'; ?></th>
           <td>
               <?php echo $this->form->input('TrnRetroactHeader.facility_to_code',array('type' => 'hidden')); ?>
               <?php echo $this->form->input('TrnRetroactHeader.facility_to',array('type'=>'hidden')); ?>
               <?php echo $this->form->input('TrnRetroactHeader.facility_to_id',array('type'=>'hidden')); ?>
               <?php echo $this->form->input('TrnRetroactHeader.facility_to_name',array('label' => false, 'div' => false, 'class' => 'lbl', 'style'=>'width: 180px;' , 'readonly' =>true)); ?>
           </td>
           <td></td>
           <th>備考</th>
           <td>
               <?php echo $this->form->input('TrnRetroactHeader.recital',array('class'=>'lbl','readonly'=>'readonly','maxlength' => '200')); ?>
           </td>
         </tr>
         <tr>
           <th>遡及期間</th>
           <td>
           <?php echo $this->form->input('TrnRetroactHeader.start_date',array('type' => 'text','class'=>'lbl','readonly'=>'readonly')); ?>
           ～
           <?php echo $this->form->input('TrnRetroactHeader.end_date',array('type' => 'text','class'=>'lbl','readonly'=>'readonly')); ?>
           <td></td>
         </tr>
       </table>
    </div>

    <h2 class="HeaddingSmall">検索条件</h2>

    <div class="SearchBox">
      <table class="FormStyleTable">
        <colgroup>
          <col />
          <col />
          <col width="20" />
          <col />
          <col />
          <col width="20" />
          <col />
          <col />
        </colgroup>
        <tr>
          <th>商品ID</th>
          <td><?php echo $this->form->input('MstFacilityItem.internal_code',array('class'=>'txt search_internal_code')); ?></td>
          <td></td>
          <th>製品番号</th>
          <td><?php echo $this->form->input('MstFacilityItem.item_code',array('class'=>'txt search_upper')); ?></td>
          <td></td>
          <th>適用開始日</th>
          <td><?php echo $this->form->input('MstFacilityItem.start_date',array('type'=>'text','class'=>'txt validate[optional,custom[date],funcCall2[date2]]','size'=>10,'id'=>'datepicker1')); ?></td>
        </tr>
        <tr>
          <th>商品名</th>
          <td><?php echo $this->form->input('MstFacilityItem.item_name',array('type'=>'text','class'=>'txt','size'=>10)); ?></td>
          <td></td>
          <th>販売元</th>
          <td><?php echo $this->form->input('MstDealer.dealer_name',array('type'=>'text','class'=>'txt')); ?></td>
          <td></td>
        </tr>
        <tr>
          <th>規格</th>
          <td><?php echo $this->form->input('MstFacilityItem.standard',array('type'=>'text','class'=>'txt','size'=>10)); ?></td>
          <td></td>
          <th>ＪＡＮコード</th>
          <td><?php echo $this->form->input('MstFacilityItem.jan_code',array('type'=>'text','class'=>'txt','size'=>10)); ?></td>
          <td></td>
        </tr>
      </table>
      
        <div class="ButtonBox">
           <p class="center">
             <input type="button" value="" class="btn btn1" id="btn_Search"/>
           </p>
        </div>
    </div><!-- searchBox -->
    <div align="right" id="pageTop" ></div>

<?php
    if(isset($this->request->data['search']['is_search'])){
      if(count($SearchResult) === 0){
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th style="width: 20px;" rowspan="2"><input type="checkbox" class="checkAll_1"></th>
            <th class="col10">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th class="col10">適用開始日</th>
            <?php if((int)$retroact_type === 1){ ?>
            <th class="col10">仕入単価</th>
            <?php } else { ?>
            <th class="col10">売上単価</th>
            <?php } ?>
          </tr>
          <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th colspan="2">包装単位</th>
          </tr>
        </table>
      </div>
      <span>該当するデータはありません。</span>
<?php
      }else{
?>
    <div id="results">
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th style="width: 20px;" rowspan="2"><input type="checkbox" class="checkAll_1"></th>
            <th class="col10">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th class="col10">適用開始日</th>
            <?php if((int)$retroact_type === 1){ ?>
            <th class="col10">仕入単価</th>
            <?php } else { ?>
            <th class="col10">売上単価</th>
            <?php } ?>
          </tr>
          <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th colspan="2">包装単位</th>
          </tr>
        </table>
      </div>

      <div class="TableScroll" style="" id="searchForm">
        <?php $_cnt=0;foreach($SearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row["TrnRetroact"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php echo "itemSelectedRow".$_row["TrnRetroact"]['id'];?> >
            <td style="width: 20px;" rowspan="2" class="center">
              <?php echo $this->form->checkbox("SelectedID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row["TrnRetroact"]["id"] , 'style'=>'width:20px;text-align:center;') ); ?>
            </td>
            <td class="col10 center">
              <label title="<?php echo $_row['TrnRetroact']['internal_code']; ?>">
                <div style="overflow:hidden;" align ="center"><?php echo $_row['TrnRetroact']['internal_code']; ?></div>
              </label>
            </td>
            <td>
              <label title="<?php echo $_row['TrnRetroact']['item_name'] ; ?>">
                <?php echo $_row['TrnRetroact']['item_name'] ; ?>
              </label>
            </td>
            <td>
              <label title="<?php echo $_row['TrnRetroact']['item_code'] ; ?>">
                <?php echo $_row['TrnRetroact']['item_code'] ; ?>
              </label>
            </td>
            <td class="col10">
              <label title="<?php echo $_row['TrnRetroact']['start_date']; ?>">
                <div style="overflow:hidden;"><?php echo $_row['TrnRetroact']['start_date']; ?></div>
              </label>
            </td>
            <td class="col10">
              <label title="<?php echo $this->Common->toCommaStr($_row['TrnRetroact']['price']); ?>">
                <?php echo $this->Common->toCommaStr($_row['TrnRetroact']['price']); ?>
              </label>
            </td>
          </tr>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" >
            <td>
            </td>
            <td>
              <label title="<?php echo $_row['TrnRetroact']['standard'] ; ?>">
                <?php echo $_row['TrnRetroact']['standard'] ; ?>
              </label>
            </td>
            <td >
              <label title="<?php echo $_row['TrnRetroact']['dealer_name'] ; ?>">
                <?php echo $_row['TrnRetroact']['dealer_name'] ; ?>
              </label>
            </td>
            <td colspan="2">
              <label title="<?php echo $_row['TrnRetroact']['unit_name'] ; ?>">
                <?php echo $_row['TrnRetroact']['unit_name'] ; ?>
              </label>
            </td>
          </tr>
        </table>
        <?php $_cnt++;endforeach;?>
      </div>

      <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
          <input type="button" value="" class="btn btn4" id="cmdSelect" />
        </p>
      </div>
    </div><!-- end results -->
<?php
      }
?>

    <?php if(isset($this->request->data['search']['is_search'])){?>
    <h2 class="HeaddingSmall">選択商品</h2>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th style="width: 20px;" rowspan="2"><input type="checkbox" class="checkAll_2" checked></th>
            <th class="col10">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th class="col10">適用開始日</th>
            <?php if((int)$retroact_type === 1){ ?>
            <th class="col10">仕入単価</th>
            <?php } else { ?>
            <th class="col10">売上単価</th>
            <?php } ?>
          </tr>
          <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th colspan="2">包装単位</th>
          </tr>
        </table>
    </div>
    <div class="TableScroll" style="" id="cartForm">
      <?php if(isset($CartSearchResult)){ ?>
      <?php $_cnt=0;foreach($CartSearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row["TrnRetroact"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php echo "itemSelectedRow".$_row["TrnRetroact"]['id'];?> >
            <td style="width: 20px;" rowspan="2" class="center">
              <?php echo $this->form->checkbox("SelectedID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row["TrnRetroact"]["id"] , 'style'=>'width:20px;text-align:center;' , 'checked'=>true) ); ?>
            </td>
            <td class="col10 center">
              <label title="<?php echo $_row['TrnRetroact']['internal_code']; ?>">
                <div style="overflow:hidden;" align ="center"><?php echo $_row['TrnRetroact']['internal_code']; ?></div>
              </label>
            </td>
            <td>
              <label title="<?php echo $_row['TrnRetroact']['item_name'] ; ?>">
                <?php echo $_row['TrnRetroact']['item_name'] ; ?>
              </label>
            </td>
            <td>
              <label title="<?php echo $_row['TrnRetroact']['item_code'] ; ?>">
                <?php echo $_row['TrnRetroact']['item_code'] ; ?>
              </label>
            </td>
            <td class="col10">
              <label title="<?php echo $_row['TrnRetroact']['start_date']; ?>">
                <div style="overflow:hidden;"><?php echo $_row['TrnRetroact']['start_date']; ?></div>
              </label>
            </td>
            <td class="col10">
              <label title="<?php echo $this->Common->toCommaStr($_row['TrnRetroact']['price']); ?>">
                <?php echo $this->Common->toCommaStr($_row['TrnRetroact']['price']); ?>
              </label>
            </td>
          </tr>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" >
            <td>
            </td>
            <td>
              <label title="<?php echo $_row['TrnRetroact']['standard'] ; ?>">
                <?php echo $_row['TrnRetroact']['standard'] ; ?>
              </label>
            </td>
            <td >
              <label title="<?php echo $_row['TrnRetroact']['dealer_name'] ; ?>">
                <?php echo $_row['TrnRetroact']['dealer_name'] ; ?>
              </label>
            </td>
            <td colspan="2">
              <label title="<?php echo $_row['TrnRetroact']['unit_name'] ; ?>">
                <?php echo $_row['TrnRetroact']['unit_name'] ; ?>
              </label>
            </td>
          </tr>
        </table>
      <?php $_cnt++;endforeach;?>
      <?php }?>
      <div id="itemSelectedTable"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" class="btn btn3" id="btn_Confirm" value=""/>
      </p>
    </div>
    <?php }?>
<?php
    }else{
      //検索実行フラグがfalse
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>0)); ?>
      </div>
      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <tr>
            <th style="width: 20px;" rowspan="2"><input type="checkbox" class="checkAll_1" checked></th>
            <th class="col10">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th class="col10">適用開始日</th>
            <?php if((int)$retroact_type === 1){ ?>
            <th class="col10">仕入単価</th>
            <?php } else { ?>
            <th class="col10">売上単価</th>
            <?php } ?>
          </tr>
          <tr>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th colspan="2">包装単位</th>
          </tr>
        </table>
      </div>
<?php
    }
    echo $this->form->end();
?>
<div align="right" id="pageDow" ></div>
