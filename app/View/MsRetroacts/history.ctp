<script type="text/javascript">
    //jquery calendar
    $(function($){
        $("#datepicker1").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
        $("#datepicker2").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
    });

    $(function(){
        $("#retroacts_history_search_form_btn").click(function(){
            if(headerCheck()){
                $("#retroacts_history_search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history');
                $("#retroacts_history_search_form").attr('method', 'post');
                $("#retroacts_history_search_form").submit();
            }
        });
        //CSVボタン押下
        $("#btn_Csv").click(function(){
            $("#retroacts_history_search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/');
            $("#retroacts_history_search_form").attr('method', 'post');
            $("#retroacts_history_search_form").submit();
        });

        // retroacts_select_history_form_btn
        $("#retroacts_select_history_form_btn").click(function(){
            var _count = 0;
            $('input[:radio].retroact_radio').each(function(){
                if (this.checked === true) {
                    _count++;
                }
            });
            if (_count > 0) {
                $("#retroacts_select_history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit');
                $("#retroacts_select_history_form").attr('method', 'post');
                $("#retroacts_select_history_form").submit();
            }else {
                alert('遡及データが選択されていません');
                return false;
            }
        });

        //取消ボタン押下時
        $("#del_btn").click(function(){
            var submit_flag = false;
            $('input[:radio].retroact_radio').each(function(){
                if (this.checked === true) {
                    submit_flag = true;
                }
            });
            if(submit_flag){
                if(window.confirm("取消を実行します。\rよろしいですか？")){
                    $("#retroacts_select_history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_result');
                    $("#retroacts_select_history_form").attr('method', 'post');
                    $("#retroacts_select_history_form").submit();
                }
            }else {
                alert('遡及データが選択されていません');
                return false;
            }
        });
    });

    $(function() {
        // change supplier code select, input supplier code to text field.
        $('#supplier_code_select').change(function() {
            $('#supplier_code_txt').val($('#supplier_code_select option:selected').val());
        });

        // change supplier code text, select supplier code selects.
        $('#supplier_code_txt').change(function(){
            $('#supplier_code_select').selectOptions($(this).val());
            $('#supplier_code_select').select();
        });

        // change hospital code select, input hospital code to text field.
        $('#hospital_code_select').change(function() {
            $('#hospital_code_txt').val($('#hospital_code_select option:selected').val());
        });

        // change hosptal code text, select hosptal code selects.
        $('#hospital_code_txt').change(function(){
            $('#hospital_code_select').selectOptions($(this).val());
            $('#hospital_code_select').select();
        });
    });

    //日付けチェック
    function headerCheck() {
        if( !dateCheck($("#datepicker1").val()) && ""!=$("#datepicker1").val() ) {
            alert('計上日（From）が正しくありません');
            return false;
        }
        if( !dateCheck($("#datepicker2").val()) && ""!=$("#datepicker2").val() ) {
            alert('計上日（To）が正しくありません');
            return false;
        }
        return true;
    }
</script>

<!-- breadcrums (start) -->
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>MS法人遡及履歴</li>
    </ul>
</div>

<!-- breadcrums (end) -->

<h2 class="HeaddingLarge"><span>MS法人遡及履歴</span></h2>

<!-- SearchBox Double Columns (start) -->
<form class="validate_form search_form" id="retroacts_history_search_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history">
    <?php echo $this->form->input('search_options',array('type'=>'hidden','value'=>'1')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="60" align="right" />
            </colgroup>
            <tr>
                <th>遡及番号</th>
                <td><?php echo $this->form->input('TrnRetroactHeader.work_no',array('class'=>'txt','style'=>'width:110px;')); ?></td>
                <td></td>
                <th>計上日</th>
                <td>
                    <span>
<?php
                    if(!isset($this->request->data['search_options']) || (isset($this->request->data['search_options']) && $this->request->data['search_options']!=='1')){
                        $this->request->data['TrnRetroactHeader']['start_date'] = date("Y/m/d",mktime(0, 0, 0, date("m")  , date("d")-7, date("Y")));
                        $this->request->data['TrnRetroactHeader']['end_date'] = date('Y/m/d');
                    }
?>
                    <?php echo $this->form->input('TrnRetroactHeader.start_date',array('type'=>'text','class'=>'txt date','id'=>'datepicker1','value'=>$this->request->data['TrnRetroactHeader']['start_date'] )); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('TrnRetroactHeader.end_date',array('type'=>'text','class'=>'txt date','id'=>'datepicker2','value'=>$this->request->data['TrnRetroactHeader']['end_date'] )); ?>
                    </span>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactRecord.department_code_from_txt',array('class'=>'txt require','style'=>'width:60px;','id'=>'supplier_code_txt')); ?>
                    <?php echo $this->form->input('TrnRetroactRecord.department_code_from',array('options'=>$suppliers, 'empty'=>true, 'class'=>'txt','style'=>'width:120px;','id'=>'supplier_code_select')); ?>
                </td>
                <td></td>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('TrnRetroactRecord.department_code_to_txt',array('class'=>'txt require','style'=>'width:60px;','id'=>'hospital_code_txt')); ?>
                    <?php echo $this->form->input('TrnRetroactRecord.department_code_to',array('options'=>$customers, 'empty'=>true, 'class'=>'txt','style'=>'width:120px;','id'=>'hospital_code_select')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code',array('class'=>'txt search_upper search_internal_code','style'=>'width:120px;')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_code',array('class'=>'txt search_upper','style'=>'width:120px;')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('TrnRetroactRecord.work_type',array('options'=>$work_types, 'empty'=>true, 'class'=>'txt','style'=>'width:120px;')); ?></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_name',array('class'=>'txt search_canna','style'=>'width:120px;')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstFacilityItem.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px;')); ?></td>
                <td></td>
                <th>遡及種別</th>
                <td><?php echo $this->form->input('TrnRetroactRecord.retroact_type',array('options'=>$retroact_types, 'empty'=>true, 'class'=>'txt','style'=>'width:120px;')); ?></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('MstFacilityItem.standard',array('class'=>'txt search_canna','style'=>'width:120px;')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('MstFacilityItem.jan_code',array('class'=>'txt','style'=>'width:120px;')); ?></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height: 10px;"></td>
            </tr>
        </table>

        <!-- ButtonBox (start) -->
        <div class="ButtonBox">
            <p class="center">
                <input type="button" value="" class="btn btn1" id="retroacts_history_search_form_btn" />
                &nbsp;&nbsp;
                <input type="button" value="" class="btn btn5" id="btn_Csv"/>
            </p>
        </div>
        <!-- ButtonBox (end) -->
    </div>

<!--// SearchBox Double Columns (end) -->

<div id="results">
    <!-- DisplaySelect (start) -->
    <div class="DisplaySelect">
<?php
    if(isset($RetroactHeaders)){
        echo $this->element('limit_combobox', array('param'=>'limit' , 'result' => count($RetroactHeaders)));
    }else{
        echo $this->element('limit_combobox', array('param'=>'limit' , 'result' => 0));
    }
        ?>
    </div>
</form>
    <!-- // DisplaySelect (start) -->
    <form id="retroacts_select_history_form">
<?php
    if(isset($this->request->data['search_options']) && $this->request->data['search_options']==='1'){
        echo $this->form->input('MstFacilityItem.internal_code',array('type'=>'hidden','value'=>$this->request->data['MstFacilityItem']['internal_code']));
        echo $this->form->input('MstFacilityItem.item_code',array('type'=>'hidden','value'=>$this->request->data['MstFacilityItem']['item_code']));
        echo $this->form->input('MstFacilityItem.item_name',array('type'=>'hidden','value'=>$this->request->data['MstFacilityItem']['item_name']));
        echo $this->form->input('MstFacilityItem.dealer_name',array('type'=>'hidden','value'=>$this->request->data['MstFacilityItem']['dealer_name']));
        echo $this->form->input('MstFacilityItem.standard',array('type'=>'hidden','value'=>$this->request->data['MstFacilityItem']['standard']));
        echo $this->form->input('MstFacilityItem.jan_code',array('type'=>'hidden','value'=>$this->request->data['MstFacilityItem']['jan_code']));
    }
?>

        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
                <colgroup>
                    <col width="20" />
                    <col width="150" />
                    <col width="90" />
                    <col width="90" />
                    <col />
                    <col width="120" />
                    <col width="100" />
                    <col width="120" />
                    <col width="80" />
                </colgroup>
                <tr>
                    <th></th>
                    <th>遡及番号</th>
                    <th>遡及種別</th>
                    <th>計上日</th>
                    <th>対象施設</th>
                    <th>登録者</th>
                    <th>登録日</th>
                    <th>備考</th>
                    <th>件数</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <?php if(isset($this->request->data['search_options']) && $this->request->data['search_options']==='1'){ ?>
            <table class="TableStyle01">
                <colgroup>
                    <col width="20" />
                    <col width="150" align="center" />
                    <col width="90" align="center" />
                    <col width="90" align="center" />
                    <col />
                    <col width="120" />
                    <col width="100" align="center" />
                    <col width="120" />
                    <col width="80" align="right" />
                </colgroup>
            <?php if (isset ($RetroactHeaders) && count($RetroactHeaders)){ ?>
                <?php $i = 0; foreach ($RetroactHeaders as $rs){ ?>
                <tr<?php if($i%2 !== 0){print " class=\"odd\"";} ?>>
                    <td>
                        <input type="radio" name="data[TrnRetroactHeader][id]" class="retroact_radio" value="<?php echo $rs['TrnRetroactHeader']['id']; ?>" />
                    </td>
                    <td>
                        <?php echo $this->form->input('TrnRetroactHeader.work_no',array('type'=>'hidden','value'=>$rs['TrnRetroactHeader']['work_no'])); ?>
                        <?php echo h_out($rs['TrnRetroactHeader']['work_no'],'center'); ?>
                    </td>
                    <td><?php echo h_out($rs['TrnRetroactHeader']['retroact_type'],'center'); ?></td>
                    <td>
                        <?php echo $this->form->input('TrnRetroactHeader.work_date',array('type'=>'hidden','value'=>$rs['TrnRetroactHeader']['work_date'])); ?>
                        <?php echo h_out($rs['TrnRetroactHeader']['work_date'],'center'); ?>
                    </td>
                    <td><?php echo h_out($rs['MstFacility']['facility_name']); ?></td>
                    <td><?php echo h_out($rs['MstUser']['user_name']); ?></td>
                    <td><?php echo h_out($rs['TrnRetroactHeader']['created'],'center'); ?></td>
                    <td><?php echo h_out($rs['TrnRetroactHeader']['recital']); ?></td>
                    <td><?php echo h_out($rs['TrnRetroactHeader']['count'],'right'); ?></td>
                </tr>
                <?php $i++; } ?>
            <?php }else{ ?>
                <tr>
                    <td colspan="9" style="text-align:center">該当のデータはありません</td>
                </tr>
            <?php } ?>
            </table>
            <?php }?>
        </div>

        <!-- ButtonBox (start) -->
        <?php if (isset ($RetroactHeaders) && count($RetroactHeaders)){ ?>
        <?php echo $this->form->input('TrnRetroactHeader.chk_date',array('type'=>'hidden','value'=>date("Y-m-d H:i:s") )); ?>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn7 submit" value="" id="retroacts_select_history_form_btn" />
                <input type="button" class="btn btn6 submit [p2]" value="" id="del_btn" style="margin-left:20px;">
            </p>
        </div>
        <?php } ?>
        <!-- ButtonBox (end) -->
    </form>
</div>