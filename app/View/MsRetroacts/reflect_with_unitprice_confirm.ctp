<script type="text/javascript">
$(function(){
  $("#retroacts_reflect_with_unitprice_confirm_form_btn").click(function(){
    if($('input.select_check:checked').length === 0){
      alert('明細は1つ以上選択してください。');
      return false;
    }
    $("#retroacts_reflect_with_unitprice_confirm_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/reflect_with_unitprice_result');
    $("#retroacts_reflect_with_unitprice_confirm_form").attr('method', 'post');
    $("#retroacts_reflect_with_unitprice_confirm_form").submit();
  });
});

/**
 * チェックボックス全選択・全解除
 */
$(function () {
  $('#select_all_check').click(function () {
    $('input.select_check').attr('checked',$(this).attr('checked'));
  });
});
$(function () {
  $('input.select_check').click(function () {
    if($('input.select_check').length == $('input.select_check:checked').length && $('input.select_check:checked').length != 0){
      $('#select_all_check').attr('checked','checked');
    } else {
      $('#select_all_check').attr('checked',false);
    }
  });
});
</script>
<div id="TopicPath">
<ul>
  <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
  <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_<?php echo ($retroact_type == '1') ? "stock" : "sales"; ?>price"><?php echo ($retroact_type == '1') ? "仕入" : "売上"; ?>価格変更</a></li>
  <li class="pankuzu">変更条件指定</li>
  <li >変更条件確認</li>
</ul>
</div>
<h2 class="HeaddingLarge"><span>変更条件確認</span></h2>
<h2 class="HeaddingMid">以下の条件で価格変更を行います</h2>
<form class="validate_form" id="retroacts_reflect_with_unitprice_confirm_form">
<div class="SearchBox">
<?php echo $this->element('retroacts' . DS . 'header' , array('page_type' => 'confirm')); ?>
</div>
<!--// ListTable DoubleHeader --> <br>

<div class="TableHeaderAdjustment01">
<table class="TableHeaderStyle02">
  <tr>
    <th style="width: 20px;" rowspan="2"><input type="checkbox" id="select_all_check" checked></th>
    <th class="col10">商品ID</th>
    <th>商品名</th>
    <th>製品番号</th>
    <th class="col10">適用開始日</th>
    <?php if((int)$retroact_type === 1){ ?>
    <th class="col10">仕入単価</th>
    <?php } else { ?>
    <th class="col10">売上単価</th>
    <?php } ?>
  </tr>
  <tr>
    <th></th>
    <th>規格</th>
    <th>販売元</th>
    <th colspan="2">包装単位</th>
  </tr>
</table>
</div>
<div class="TableScroll">
<?php if(isset($itemunits) && count($itemunits) > 0):?>
<table class="TableStyle02" border="0" style="margin:0;padding:0;">
<?php $_i = 0 ;foreach( $itemunits as $itemunit ): ?>
  <tr class="<?php echo( $_i % 2 == 0 ? '' : 'odd' ); ?>" >
    <td style="width: 20px;" rowspan="2" class="center">
      <?php echo $this->form->input('TrnRetroactRecord.'.$_i.'.'.$table_type['table_name'].'_id', array('type'=>'hidden','value'=>$itemunit['TrnRetroact']['id'])); ?>
      <?php echo $this->form->input('TrnRetroactRecord.'.$_i.'.mst_item_unit_id', array('type'=>'hidden','value'=>$itemunit['TrnRetroact']['item_unit_id'])); ?>
      <?php echo $this->form->input('TrnRetroactRecord.'.$_i.'.price', array('type'=>'hidden','value'=>$itemunit['TrnRetroact']['price'])); ?>
      <?php echo $this->form->input('TrnRetroactRecord.'.$_i.'.checked',array('type'=>'checkbox','class'=>'center select_check','value'=>'1', 'checked'=>'checked')); ?>
    </td>
    <td class="col10">
      <label title="<?php echo h( $itemunit['TrnRetroact']['internal_code']); ?>">
        <p align="center"><?php echo h( $itemunit['TrnRetroact']['internal_code']); ?></p>
      </label>
    </td>
    <td>
      <label title="<?php echo h( $itemunit['TrnRetroact']['item_name']); ?>">
        <?php echo h( $itemunit['TrnRetroact']['item_name']); ?>
      </label>
    </td>
    <td>
      <label title="<?php echo h( $itemunit['TrnRetroact']['item_code']); ?>">
        <?php echo h( $itemunit['TrnRetroact']['item_code']); ?>
      </label>
    </td>
    <td class="col10">
      <label title="<?php echo h($itemunit['TrnRetroact']['start_date']); ?>">
        <p align="center"><?php echo h($itemunit['TrnRetroact']['start_date']); ?></p>
      </label>
    </td>
    <td class="col10">
      <label title="<?php echo h( $this->Common->toCommaStr($itemunit['TrnRetroact']['price'])); ?>">
        <p align="right"><?php echo h( $this->Common->toCommaStr($itemunit['TrnRetroact']['price'])); ?></p>
      </label>
    </td>
  </tr>
  <tr class="<?php echo( $_i % 2 == 0 ? '' : 'odd' ); ?>">
    <td></td>
    <td>
      <label title="<?php echo h( $itemunit['TrnRetroact']['standard']); ?>">
        <?php echo h( $itemunit['TrnRetroact']['standard']); ?>
      </label>
    </td>
    <td>
      <label title="<?php echo $itemunit['TrnRetroact']['dealer_name']; ?>">
        <?php echo $itemunit['TrnRetroact']['dealer_name']; ?>
      </label>
    </td>
    <td colspan="2">
      <label title="<?php echo h( $itemunit['TrnRetroact']['unit_name']); ?>">
        <?php echo h( $itemunit['TrnRetroact']['unit_name']); ?>
      </label>
    </td>
  </tr>
<?php $_i++ ; endforeach; ?>
</table>
<?php else: ?>
  <table class="TableStyle02" border="0" id="itemSelectedRow" style="margin:0;padding:0;">
    <tr class="" >
      <td>
        該当するデータがありませんでした
      </td>
    </tr>
  </table>
<?php endif;?>
</div>

<div class="ButtonBox"><input type="button" value="" class="btn btn2 [p2]" id="retroacts_reflect_with_unitprice_confirm_form_btn" /></div>
</div>
</div>
<!-- end Result --></form>
