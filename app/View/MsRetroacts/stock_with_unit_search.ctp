<script type="text/javascript">
    $(function(){
        $("#datepicker1").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});

        //検索ボタン押下時
        $("#search_btn").click(function(){
            err_flg = true;
            tmpId = '';
            if($("#datepicker1").val() != ""){
                err_flg = validate2date($("#datepicker1").val());
            }
            if(!err_flg){
                alert("適用開始日が不正です");
                return false;
            }
            
            $("#CartForm input[type=checkbox].all_check").each(function(){
                if($(this).attr("checked") == true){
                tmpId = tmpId + $(this).val() + ',';
                }
            });
            $("#selectedItemId").val(tmpId);
            $("#SearchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/stock_with_unit_search/').submit();
        });

        //選択ボタン押下時
        $("#select_btn").click(function(){
            var isChecked = false;
            var selectedIemId = "";
            $("#SearchForm input[type=checkbox].all_check").each(function(){
                if($(this).attr("checked") == true){
                    //チェック有無確認
                    isChecked = true;
                    //追加確認
                    tempId = $("#selectedItemId").val();
                    if(tempId.match($(this).val())){
                        alert("すでに選択されています");
                    }else{
                        tempId = tempId + $(this).val() + ",";
                        $("#selectedItemId").val(tempId);
                        $("#SearchRow" + $(this).val()).clone().appendTo('#SelectedItem');
                    }
                }
            });
            if(!isChecked){
                alert('明細を選択してください');
            }
        });

        //チェックボックスオールチェック
        $('.all_check_1').click(function(){
            $('#SearchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
        });

        $('.all_check_2').click(function(){
            $('#CartForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
        });

        //確認ボタン押下
        $("#confirm_btn").click(function(){
            var isChecked = false;
            $("#CartForm input[type=checkbox].all_check").each(function(){
                if($(this).attr("checked") == true){
                    //チェック有無確認
                    isChecked = true;
                }
            });
            if(isChecked){
                //サブミット
                $("#CartForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/stock_with_unit_confirm').submit();
            }else{
                alert('明細を選択してください');
                return false;
            }
        });
    });

    //日付妥当性チェック
    function validate2date(check_date){
        var date_split;
        var di;
        if(check_date.match(/^[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}$/)){
            date_split = check_date.split("/");
            di = new Date(date_split[0],date_split[1]-1,date_split[2]);
            if(di.getFullYear() == date_split[0] && di.getMonth() == date_split[1]-1 && di.getDate() == date_split[2]){
                return true;
            }else{
                return false;
            }
        }else{
            //書式不正
            return false;
        }
        return true;
    }

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/stock">仕入赤黒作成</a></li>
        <li>遡及対象選択</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>遡及対象選択</span></h2>
<h2 class="HeaddingMid">遡及対象を検索してください</h2>

<form class="validate_form" id="SearchForm" method="post">
<?php
    echo $this->form->hidden("Search.flag",array("id"=>"search_flag","value"=>"1"));
    if(isset($data["Search"]["selectedItemId"])){
        echo $this->form->hidden("Search.selectedItemId",array("id"=>"selectedItemId","value"=>$data["Search"]["selectedItemId"]));
    }else{
        echo $this->form->hidden("Search.selectedItemId",array("id"=>"selectedItemId"));
    }
?>
    <div class="SearchBox" style="margin-bottom:20px;">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->hidden('Condition.owner_id',array('id'=>'owner_id',"value"=>$data["Condition"]["owner_id"])); ?>
                    <?php echo $this->form->hidden('Condition.owner_code',array('id'=>'owner_code',"value"=>$data["Condition"]["owner_code"])); ?>
                    <?php echo $this->form->input('Condition.owner_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'owner_name',"value"=>$data["Condition"]["owner_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->hidden('Condition.class_id',array('id'=>'class_id',"value"=>$data["Condition"]["class_id"])); ?>
                    <?php echo $this->form->input('Condition.class_name',array("type"=>"text",'class'=>'lbl',"style"=>"width:200px;","id"=>"class_name","value"=>$data["Condition"]["class_name"],"readonly"=>"readonly")); ?>
                </td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->hidden('Condition.supplier_id',array('id'=>'supplier_id',"value"=>$data["Condition"]["supplier_id"])); ?>
                    <?php echo $this->form->hidden('Condition.supplier_code',array('id'=>'supplier_code',"value"=>$data["Condition"]["supplier_code"])); ?>
                    <?php echo $this->form->hidden('Condition.supplier_department_id',array('id'=>'supplier_department_id',"value"=>$data["Condition"]["supplier_department_id"])); ?>
                    <?php echo $this->form->input('Condition.supplier_name',array('type'=>'text','class'=>'lbl','style'=>'width:200px;','id'=>'supplier_name',"value"=>$data["Condition"]["supplier_name"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Condition.recital',array('type'=>'text','class'=>'lbl','maxlength'=>'200',"readonly"=>"readonly","value"=>$data["Condition"]["recital"])); ?>
                </td>
            </tr>
            <tr>
                <th>遡及期間</th>
                <td>
                    <?php echo $this->form->input('Condition.start_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["start_date"],"readonly"=>"readonly")); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('Condition.end_date',array('type'=>'text','class'=>'lbl','value'=>$data["Condition"]["end_date"],"readonly"=>"readonly")); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <h2 class="HeaddingSmall">検索条件</h2>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('Search.internal_code',array('class'=>'txt')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('Search.item_code',array('class'=>'txt search_upper')); ?></td>
                <td></td>
                <th>適用開始日</th>
                <td><?php echo $this->form->input('Search.start_date',array('type'=>'text','class'=>'txt','size'=>10,'id'=>'datepicker1')); ?></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('Search.item_name',array('type'=>'text','class'=>'txt','size'=>10)); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('Search.dealer_name',array('class'=>'txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('Search.standard',array('type'=>'text','class'=>'txt','size'=>10)); ?></td>
                <td></td>
                <th>ＪＡＮコード</th>
                <td><?php echo $this->form->input('Search.jan_code',array('type'=>'text','class'=>'txt','size'=>10)); ?></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" value="" class="btn btn1" id="search_btn" />
        </p>
    </div>

<?php
    if(isset($data["Search"]["flag"]) && $data["Search"]["flag"] === "1"){
?>
    <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
    </div>

    <div class="DisplaySelect">
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th style="width: 20px;" rowspan="2"><input type="checkbox" id="all_check_1"></th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th class="col10">適用開始日</th>
                    <th class="col10">仕入単価</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th colspan="2">包装単位</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <div class="TableStyle02 SearchTable" id="SearchTable" border="0">
<?php
            $i = 0;
            if(isset($SearchResult) && count($SearchResult) > 0){
                foreach ($SearchResult as $row){
?>
                <table class="TableStyle02" border="0" id="SearchRow<?php echo $row["MstTransactionConfig"]["id"];?>" style="margin-top:-1px; margin-bottom:0px; padding:0px;">
                    <tr class="<?php echo( $i % 2 == 0 ? '' : 'odd' ); ?>" >
                        <td style="width: 20px;" rowspan="2" class="center">
                            <?php echo $this->form->input("Confirm.search_id".$i,array("type"=>"checkbox","class"=>"center all_check","value"=>$row["MstTransactionConfig"]["id"],"hiddenField"=>false)); ?>
                        </td>
                        <td class="col10">
                            <label title="<?php echo h($row["MstFacilityItem"]["internal_code"]); ?>">
                                <p align="center"><?php echo h($row["MstFacilityItem"]["internal_code"]); ?></p>
                            </label>
                        </td>
                        <td>
                            <label title="<?php echo h($row["MstFacilityItem"]["item_name"]); ?>">
                                <?php echo h($row["MstFacilityItem"]["item_name"]); ?>
                            </label>
                        </td>
                        <td>
                            <label title="<?php echo h($row["MstFacilityItem"]["item_code"]); ?>">
                                <?php echo h($row["MstFacilityItem"]["item_code"]); ?>
                            </label>
                        </td>
                        <td class="col10">
                            <label title="<?php echo h($row["MstTransactionConfig"]["start_date"]); ?>">
                                <p align="center"><?php echo h($row["MstTransactionConfig"]["start_date"]); ?></p>
                            </label>
                        </td>
                        <td class="col10" align="right">
                            <label title="<?php echo h($this->Common->toCommaStr($row["MstConfig"]["config_price"])); ?>">
                                <p align="right"><?php echo h($this->Common->toCommaStr($row["MstConfig"]["config_price"])); ?></p>
                            </label>
                        </td>
                    </tr>
                    <tr class="<?php echo( $i % 2 == 0 ? '' : 'odd' ); ?>">
                        <td></td>
                        <td>
                            <label title="<?php echo h($row["MstFacilityItem"]["standard"]); ?>">
                                <?php echo h($row["MstFacilityItem"]["standard"]); ?>
                            </label>
                        </td>
                        <td>
                            <label title="<?php echo h($row["MstDealer"]["dealer_name"]); ?>">
                                <?php echo h($row["MstDealer"]["dealer_name"]); ?>
                            </label>
                        </td>
                        <td colspan="2">
                            <label title="<?php echo h($row["MstUnitName"]["unit_name"]); ?>">
                                <?php echo h($row["MstUnitName"]["unit_name"]); ?>
                            </label>
                        </td>
                    </tr>
                </table>
<?php
                $i++ ;
                }//end of foreach
            }else{
?>
                <table class="TableStyle02" border="0" style="margin-top:-1px; margin-bottom:0px; padding:0px;">
                    <tr>
                        <td colspan="6">
                            <span>対象のデータが存在しません</span>
                        </td>
                    </tr>
                </table>
<?php
            }//end of if
?>
            </div>
        </div>

        <div class="ButtonBox" style="margin-top:10px;">
            <input type="button" value="" class="btn btn4" id="select_btn" />
        </div>
    </div>
</form>

<form class="validate_form" id="CartForm">
    <?php //次画面への引継ぎ項目 ?>
    <?php echo $this->form->hidden("Search.flag",array("id"=>"search_flag","value"=>"1")); ?>
    <?php echo $this->form->hidden("Condition.owner_id",array("value"=>$data["Condition"]["owner_id"])); ?>
    <?php echo $this->form->hidden("Condition.owner_code",array("value"=>$data["Condition"]["owner_code"])); ?>
    <?php echo $this->form->hidden("Condition.owner_name",array("value"=>$data["Condition"]["owner_name"])); ?>
    <?php echo $this->form->hidden("Condition.class_id",array("value"=>$data["Condition"]["class_id"])); ?>
    <?php echo $this->form->hidden("Condition.class_name",array("value"=>$data["Condition"]["class_name"])); ?>
    <?php echo $this->form->hidden("Condition.supplier_id",array("value"=>$data["Condition"]["supplier_id"])); ?>
    <?php echo $this->form->hidden("Condition.supplier_code",array("value"=>$data["Condition"]["supplier_code"])); ?>
    <?php echo $this->form->hidden("Condition.supplier_department_id",array("value"=>$data["Condition"]["supplier_department_id"])); ?>
    <?php echo $this->form->hidden("Condition.supplier_name",array("value"=>$data["Condition"]["supplier_name"])); ?>
    <?php echo $this->form->hidden("Condition.recital",array("value"=>$data["Condition"]["recital"])); ?>
    <?php echo $this->form->hidden("Condition.start_date",array("value"=>$data["Condition"]["start_date"])); ?>
    <?php echo $this->form->hidden("Condition.end_date",array("value"=>$data["Condition"]["end_date"])); ?>
    <div class="DisplaySelect">
        <h2 class="HeaddingSmall">選択商品</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th style="width: 20px;" rowspan="2"><input type="checkbox" id="all_check_2" checked></th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th class="col10">適用開始日</th>
                    <th class="col10">仕入単価</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th colspan="2">包装単位</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll" style="">
<?php
        if(isset($CartResult)){
            $cnt=0;
            foreach($CartResult as $row){
?>
            <table class="TableStyle02" border="0" id="SearchRow<?php echo $row["MstTransactionConfig"]["id"];?>" style="margin-top:-1px; margin-bottom:0px; padding:0px;">
                <tr class="<?php echo( $cnt % 2 == 0 ? '' : 'odd' ); ?>" >
                    <td style="width: 20px;" rowspan="2" class="center">
                        <?php echo $this->form->input("Confirm.select_id".$cnt,array("type"=>"checkbox","class"=>"center all_check","value"=>$row["MstTransactionConfig"]["id"],"hiddenField"=>false,"checked"=>"checked")); ?>
                    </td>
                    <td class="col10">
                        <label title="<?php echo h($row["MstFacilityItem"]["internal_code"]); ?>">
                            <p align="center"><?php echo h($row["MstFacilityItem"]["internal_code"]); ?></p>
                        </label>
                    </td>
                    <td>
                        <label title="<?php echo h($row["MstFacilityItem"]["item_name"]); ?>">
                            <?php echo h($row["MstFacilityItem"]["item_name"]); ?>
                        </label>
                    </td>
                    <td>
                        <label title="<?php echo h($row["MstFacilityItem"]["item_code"]); ?>">
                            <?php echo h($row["MstFacilityItem"]["item_code"]); ?>
                        </label>
                    </td>
                    <td class="col10">
                        <label title="<?php echo h($row["MstTransactionConfig"]["start_date"]); ?>">
                            <p align="center"><?php echo h($row["MstTransactionConfig"]["start_date"]); ?></p>
                        </label>
                    </td>
                    <td class="col10" align="right">
                        <label title="<?php echo h($this->Common->toCommaStr($row["MstConfig"]["config_price"])); ?>">
                            <p align="right"><?php echo h($this->Common->toCommaStr($row["MstConfig"]["config_price"])); ?></p>
                        </label>
                    </td>
                </tr>
                <tr class="<?php echo( $cnt % 2 == 0 ? '' : 'odd' ); ?>">
                    <td></td>
                    <td>
                        <label title="<?php echo h($row["MstFacilityItem"]["standard"]); ?>">
                            <?php echo h($row["MstFacilityItem"]["standard"]); ?>
                        </label>
                    </td>
                    <td>
                        <label title="<?php echo h($row["MstDealer"]["dealer_name"]); ?>">
                            <?php echo h($row["MstDealer"]["dealer_name"]); ?>
                        </label>
                    </td>
                    <td colspan="2">
                        <label title="<?php echo h($row["MstUnitName"]["unit_name"]); ?>">
                            <?php echo h($row["MstUnitName"]["unit_name"]); ?>
                        </label>
                    </td>
                </tr>
            </table>
<?php
                $cnt++;
            }//end of foreach
        }//end of if
?>
            <div id="SelectedItem"></div>
        </div>

        <div class="ButtonBox" style="margin-top:10px;">
            <p class="center">
                <input type="button" id="confirm_btn" value="" class="btn btn3" />
            </p>
        </div>
    </div>
</form>
<?php
    }//end of if
?>
