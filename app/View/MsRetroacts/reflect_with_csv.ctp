<script type="text/javascript">
$(function(){
  $("#retroacts_reflect_with_csv_form_btn").click(function(){
    $("#retroacts_reflect_with_csv_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/reflect_with_csv_confirm/<?php echo $mode; ?>');
    $("#retroacts_reflect_with_csv_form").attr('method', 'post');
    $("#retroacts_reflect_with_csv_form").submit();
  });
});
</script>
<div id="TopicPath">
<ul>
  <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
  <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_<?php echo ($retroact_type == '1') ? "stock" : "sales"; ?>price"><?php echo ($retroact_type == '1') ? "仕入" : "売上"; ?>価格変更</a></li>
  <li>&nbsp;&nbsp;変更条件指定</li>
</ul>
</div>
<h2 class="HeaddingLarge"><span>変更条件指定</span></h2>
<h2 class="HeaddingMid">価格変更の対象となる条件を指定します。(CSV)</h2>
<form id="retroacts_reflect_with_csv_form" class="validate_form" enctype="multipart/form-data" accept-charset="utf-8" >
<div class="SearchBox">
<?php echo $this->element('retroacts' . DS . 'header' , array('page_type' => 'confirm')); ?>
</div>
<div class="SearchBox">
  <table class="FormStyleTable" style="margin-top: 10px;">
    <tr>
      <th>CSVファイル</th>
      <td><input type="file" name="data[result]" class="text r validate[required]" style="width:250px;" value="" id="result" /></td>
    </tr>
  </table>
</div>
<!--// ListTable DoubleHeader -->
<div class="ButtonBox">
  <input type="button" value="" class="btn btn3" id="retroacts_reflect_with_csv_form_btn" />
</div>
<!-- end Result --></form>
