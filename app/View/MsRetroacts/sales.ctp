<script type="text/javascript">
    //日付け入力欄
    $(function($){
        $("#datepicker1").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
        $("#datepicker2").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
    });

    $(function(){
        //マスター反映ボタン押下時
        $("#master_btn").click(function(){
            if(!validate2date($("#datepicker1").val())){
                alert("期間開始日が不正です");
                return false;
            }
            if(!validate2date($("#datepicker2").val())){
                alert("期間終了日が不正です");
                return false;
            }
            $("#ConditionForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_with_master/');
            $("#ConditionForm").attr('method', 'post');
            $("#ConditionForm").submit();
        });

        //選択反映ボタン押下時
        $("#unitprice_btn").click(function(){
            if(!validate2date($("#datepicker1").val())){
                alert("期間開始日が不正です");
                return false;
            }
            if(!validate2date($("#datepicker2").val())){
                alert("期間終了日が不正です");
                return false;
            }
            $("#ConditionForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_with_unit_search/');
            $("#ConditionForm").attr('method', 'post');
            $("#ConditionForm").submit();
        });

        //CSV反映押下時
        $("#csv_btn").click(function(){
            if(!validate2date($("#datepicker1").val())){
                alert("期間開始日が不正です");
                return false;
            }
            if(!validate2date($("#datepicker2").val())){
                alert("期間終了日が不正です");
                return false;
            }
            $("#ConditionForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/sales_with_csv/');
            $("#ConditionForm").attr('method', 'post');
            $("#ConditionForm").submit();
        });
    });

    $(function(){
        //施主選択時
        $("#owner_select").change(function(){
            $("#owner_code").val($("#owner_select option:selected").val());
            $("#owner_name").val($("#owner_select option:selected").text());
        });

        //作業区分選択時
        $("#class_select").change(function(){
            $("#class_name").val($("#class_select option:selected").text());
        });

        //得意先選択時
        $("#hospital_select").change(function(){
            $("#hospital_code").val($("#hospital_select option:selected").val());
            $("#hospital_name").val($("#hospital_select option:selected").text());
        });
    });

    //日付妥当性チェック
    function validate2date(check_date){
        var date_split;
        var di;
        if(check_date.match(/^[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}$/)){
            date_split = check_date.split("/");
            di = new Date(date_split[0],date_split[1]-1,date_split[2]);
            if(di.getFullYear() == date_split[0] && di.getMonth() == date_split[1]-1 && di.getDate() == date_split[2]){
                return true;
            }else{
                return false;
            }
        }else{
            //書式不正
            return false;
        }
        return true;
    }
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>売上赤黒作成</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>売上遡及</span></h2>
<h2 class="HeaddingMid">遡及の条件を指定します。</h2>

<form id="ConditionForm" class="validate_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col>
                <col>
                <col width="20">
                <col>
                <col>
            </colgroup>
            <tr>
                <th>施主</th>
                <td>
                    <?php echo $this->form->hidden('Condition.owner_name', array('id'=>'owner_name')); ?>
                    <?php echo $this->form->input('Condition.owner_code', array('type'=>'text','class'=>'r','style'=>'width:60px;','id'=>'owner_code',"value"=>"")); ?>
                    <?php echo $this->form->input('Condition.owner_select',array('options'=>$owner_name_list,'empty'=>true,'class'=>'r txt validate[required]', 'id'=>'owner_select')); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->hidden('Condition.class_name', array('id'=>'class_name')); ?>
                    <?php echo $this->form->input('Condition.class_id',array('options'=>$class_list, 'empty'=>true, 'class'=>'txt',"id"=>"class_select")); ?>
                </td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->hidden('Condition.hospital_name', array('id'=>'hospital_name')); ?>
                    <?php echo $this->form->input('Condition.hospital_code', array('type'=>'text','class'=>'r','style'=>'width:60px;','id'=>'hospital_code',"value"=>"")); ?>
                    <?php echo $this->form->input('Condition.hospital_select',array('options'=>$hospital_name_list,'empty'=>true,'class'=>'r txt validate[required]','id'=>'hospital_select')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('Condition.recital', array('type'=>'text','class'=>'txt','maxlength'=>'200')); ?>
                </td>
            </tr>
            <tr>
                <th>遡及期間</th>
                <td>
                    <?php echo $this->form->input('Condition.start_date',array('type'=>'text','class'=>'r txt date','id'=>'datepicker1')); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('Condition.end_date',array('type'=>'text','class'=>'r txt date','id'=>'datepicker2')); ?></td>
                <td></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <input type="button" value="" class="btn btn23" id="master_btn" />
            <input type="button" value="" class="btn btn43" id="unitprice_btn" style="margin-left:10px;"/>
            <input type="button" value="" class="btn btn22" id="csv_btn" style="margin-left:10px;"/>
        </div>
    </div>
</form>