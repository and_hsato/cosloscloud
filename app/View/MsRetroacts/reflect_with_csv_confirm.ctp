<script type="text/javascript">
$(function(){
  $("#retroacts_reflect_with_csv_confirm_form_btn").click(function(){
    setTimeout(function(){$('#waitingPanel').click()},100);
    $("#retroacts_reflect_with_csv_confirm_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/reflect_with_csv_result/');
    $("#retroacts_reflect_with_csv_confirm_form").attr('method', 'post');
    $("#retroacts_reflect_with_csv_confirm_form").submit();
  });
});
</script>
<div id="TopicPath">
<ul>
  <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
  <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/set_<?php echo ($retroact_type == '1') ? "stock" : "sales"; ?>price">&nbsp;&nbsp;<?php echo ($retroact_type == '1') ? "仕入" : "売上"; ?>価格変更</a></li>
  <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/reflect_with_csv">変更条件指定</a></li>
  <li>変更条件確認</li>
</ul>
</div>
<h2 class="HeaddingLarge"><span>変更条件確認</span></h2>
<h2 class="HeaddingMid">以下の条件で価格変更を行います</h2>
<form class="validate_form" method="post" action="" id="retroacts_reflect_with_csv_confirm_form">
<div class="SearchBox">
<?php echo $this->element('retroacts' . DS . 'header' , array('page_type' => 'confirm')); ?>
</div>
<!--// ListTable DoubleHeader --> <br>
<p><font style="font-size: 12pt;">読込件数<?php echo $input_count; ?>件</font></p>
<?php if(empty($error_mes)){ ?>
<div class="ButtonBox">
  <input type="button" value="" class="btn btn2 [p2]" id="retroacts_reflect_with_csv_confirm_form_btn" />
</div>
<?php
  } else {
    for($i=0;$i<count($error_mes);$i++){
      if(isset($error_mes[$i]) and ($error_mes[$i]!="")){
        echo $error_mes[$i]."<br />";
      }
    }
  }
 ?>

<!-- end Result --></form>
