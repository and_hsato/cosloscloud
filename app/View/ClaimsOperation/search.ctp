<script type="text/javascript">
    var defaultZuraDate = {};
    $(document).ready(function(){
        $('#selectedHospital').change(function(){
            //入力実績　初期化
            $('#inputPrice').val("");
            //画面更新
            $('#form_hospital').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').attr('method', 'post').submit();
        });
        //延伸希望金額を入力
        $('#inputPrice').keypress( function ( e ) {
          if ((e.which && e.which === 13)){ //Enterキー押下
          return false; //Submit無効
          }
        });
        // 確定ボタン押下
        $('#btn_result').click(function() {
            inputNum = $('#inputPrice').val();
            if(inputNum === "" || isFinite(inputNum) === false){
              window.alert("入力実績を入力して下さい"); //入金実績　未入力
              return;
            }
            if(inputNum.match(/^[0-9]+$/) === null){
              window.alert("入力実績には数字のみ入力して下さい"); //入金実績　整数チェックエラー
              return;
            }
            present_month_payment_price = parseInt($('#default_present_month_payment_price').text());
            if (present_month_payment_price < inputNum){
              window.alert("入金予定額以上の金額が入金実績に入力されています"); //入金実績　論理チェックエラー
              return;
            }
            if(!confirm("入金実績を確定します。\nよろしいですか？")){
              return;
            }

            $('#form_hospital').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').attr('method', 'post').submit();

            window.alert("入力実績の入力が完了しました", 3);
        });
    });
</script>

<p id="default_present_month_payment_price" style="display:none"><?php echo $present_month_payment_price?></p>
<p id="default_" style="display:none"><?php echo $present_month_payment_price?></p>

<div id="content-wrapper">

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>請求情報管理</li>
    </ul>
</div>
<h2 class="HeaddingLarge">
    <span>管理画面</span>
</h2>
<div class="HeaddingMid">
</div>
<form id="form_hospital">
    <div class="box-float">
      <div>
        <span>病院名：</span>
        <?php echo $this->form->input('selectedHospital', array('options'=>$hospitalList, 'class' => 'txt', 'style'=> 'height:40px; width:360px; font-size:20px;', 'id' => 'selectedHospital', 'default' => $selectedHospital)); ?>
        <span >延伸上限金額：<?php echo substr($this->Common->toCommaStr($maximumAmount), 0 , strpos($this->Common->toCommaStr($maximumAmount), '.')) ?>円</span>
        <span>手数料利率：<?php echo $commissionRate; ?>％</span>
      </div>
    </div>
    <div id="claims-detail">
        <table>
          <thead>
            <tr>
              <th>支払予定日</th>
              <th>前月繰越</th>
              <th>前月繰越（税）</th>
              <th>請求合計</th>
              <th>請求合計（税）</th>
              <th>売掛金</th>
              <th>売掛金（税）</th>
              <th>入金予定額</th>
              <th>消費税</th>
              <th>入金予定額（税）</th>
              <th>入金実績</th>
              <th>入金実績（税）</th>
              <th>未入金</th>
              <th>未入金（税）</th>
              <th>延伸上限金額</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $_6month_before_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_before_carryOver), 0 , strpos($this->Common->toCommaStr($_6month_before_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_before_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_before_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_before_billing_price), 0 , strpos($this->Common->toCommaStr($_6month_before_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_before_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_before_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_before_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_6month_before_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_before_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_before_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_6month_before_payment_price), 0 , strpos($this->Common->toCommaStr($_6month_before_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_6month_before_payment_tax), 0 , strpos($this->Common->toCommaStr($_6month_before_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_6month_before_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_before_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_6month_before_payment_track_record), 0 , strpos($this->Common->toCommaStr($_6month_before_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_6month_before_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_before_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_before_not_payment), 0 , strpos($this->Common->toCommaStr($_6month_before_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_before_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_before_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_before_maximum_amount), 0 , strpos($this->Common->toCommaStr($_6month_before_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td><?php echo $_5month_before_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_before_carryOver), 0 , strpos($this->Common->toCommaStr($_5month_before_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_before_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_before_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_before_billing_price), 0 , strpos($this->Common->toCommaStr($_5month_before_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_before_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_before_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_before_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_5month_before_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_before_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_before_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_5month_before_payment_price), 0 , strpos($this->Common->toCommaStr($_5month_before_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_5month_before_payment_tax), 0 , strpos($this->Common->toCommaStr($_5month_before_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_5month_before_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_before_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_5month_before_payment_track_record), 0 , strpos($this->Common->toCommaStr($_5month_before_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_5month_before_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_before_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_before_not_payment), 0 , strpos($this->Common->toCommaStr($_5month_before_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_before_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_before_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_before_maximum_amount), 0 , strpos($this->Common->toCommaStr($_5month_before_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td><?php echo $_4month_before_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_before_carryOver), 0 , strpos($this->Common->toCommaStr($_4month_before_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_before_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_before_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_before_billing_price), 0 , strpos($this->Common->toCommaStr($_4month_before_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_before_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_before_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_before_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_4month_before_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_before_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_before_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_4month_before_payment_price), 0 , strpos($this->Common->toCommaStr($_4month_before_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_4month_before_payment_tax), 0 , strpos($this->Common->toCommaStr($_4month_before_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_4month_before_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_before_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_4month_before_payment_track_record), 0 , strpos($this->Common->toCommaStr($_4month_before_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_4month_before_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_before_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_before_not_payment), 0 , strpos($this->Common->toCommaStr($_4month_before_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_before_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_before_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_before_maximum_amount), 0 , strpos($this->Common->toCommaStr($_4month_before_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td><?php echo $_3month_before_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_before_carryOver), 0 , strpos($this->Common->toCommaStr($_3month_before_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_before_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_before_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_before_billing_price), 0 , strpos($this->Common->toCommaStr($_3month_before_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_before_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_before_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_before_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_3month_before_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_before_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_before_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_3month_before_payment_price), 0 , strpos($this->Common->toCommaStr($_3month_before_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_3month_before_payment_tax), 0 , strpos($this->Common->toCommaStr($_3month_before_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_3month_before_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_before_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_3month_before_payment_track_record), 0 , strpos($this->Common->toCommaStr($_3month_before_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_3month_before_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_before_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_before_not_payment), 0 , strpos($this->Common->toCommaStr($_3month_before_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_before_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_before_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_before_maximum_amount), 0 , strpos($this->Common->toCommaStr($_3month_before_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td><?php echo $_2month_before_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_before_carryOver), 0 , strpos($this->Common->toCommaStr($_2month_before_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_before_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_before_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_before_billing_price), 0 , strpos($this->Common->toCommaStr($_2month_before_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_before_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_before_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_before_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_2month_before_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_before_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_before_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_2month_before_payment_price), 0 , strpos($this->Common->toCommaStr($_2month_before_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_2month_before_payment_tax), 0 , strpos($this->Common->toCommaStr($_2month_before_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_2month_before_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_before_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_2month_before_payment_track_record), 0 , strpos($this->Common->toCommaStr($_2month_before_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_2month_before_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_before_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_before_not_payment), 0 , strpos($this->Common->toCommaStr($_2month_before_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_before_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_before_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_before_maximum_amount), 0 , strpos($this->Common->toCommaStr($_2month_before_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td><?php echo $_1month_before_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_before_carryOver), 0 , strpos($this->Common->toCommaStr($_1month_before_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_before_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_before_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_before_billing_price), 0 , strpos($this->Common->toCommaStr($_1month_before_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_before_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_before_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_before_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_1month_before_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_before_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_before_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_1month_before_payment_price), 0 , strpos($this->Common->toCommaStr($_1month_before_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_1month_before_payment_tax), 0 , strpos($this->Common->toCommaStr($_1month_before_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_1month_before_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_before_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_1month_before_payment_track_record), 0 , strpos($this->Common->toCommaStr($_1month_before_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_1month_before_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_before_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_before_not_payment), 0 , strpos($this->Common->toCommaStr($_1month_before_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_before_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_before_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_before_maximum_amount), 0 , strpos($this->Common->toCommaStr($_1month_before_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td class="OperationTarget"><?php echo $present_month_paymentDueDate ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_carryOver), 0 , strpos($this->Common->toCommaStr($present_month_carryOver), '.')) ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($present_month_carryOver_in_tax), '.')) ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_billing_price), 0 , strpos($this->Common->toCommaStr($present_month_billing_price), '.')) ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($present_month_billing_price_in_tax), '.')) ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_accounts_receivable), 0 , strpos($this->Common->toCommaStr($present_month_accounts_receivable), '.')) ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($present_month_accounts_receivable_in_tax), '.')) ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_payment_price), 0 , strpos($this->Common->toCommaStr($present_month_payment_price), '.')) ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_payment_tax), 0 , strpos($this->Common->toCommaStr($present_month_payment_tax), '.')) ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($present_month_payment_price_in_tax), '.')) ?></td>
              <td class="OperationTarget">
                <?php echo $this->form->input('inputPrice', array('class' => 'txt', 'id' => 'inputPrice', 'style' => 'text-align:right;', 'maxlength' => '8', 'value' => $present_month_payment_track_record)); ?>
              </td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($present_month_payment_track_record_in_tax), '.')) ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_not_payment), 0 , strpos($this->Common->toCommaStr($present_month_not_payment), '.')) ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($present_month_not_payment_in_tax), '.')) ?></td>
              <td class="OperationTarget"><?php echo substr($this->Common->toCommaStr($present_month_maximum_amount), 0 , strpos($this->Common->toCommaStr($present_month_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td><?php echo $_1month_later_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_later_carryOver), 0 , strpos($this->Common->toCommaStr($_1month_later_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_later_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_later_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_later_billing_price), 0 , strpos($this->Common->toCommaStr($_1month_later_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_later_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_later_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_later_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_1month_later_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_later_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_later_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_1month_later_payment_price), 0 , strpos($this->Common->toCommaStr($_1month_later_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_1month_later_payment_tax), 0 , strpos($this->Common->toCommaStr($_1month_later_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_1month_later_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_later_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_1month_later_payment_track_record), 0 , strpos($this->Common->toCommaStr($_1month_later_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_1month_later_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_later_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_later_not_payment), 0 , strpos($this->Common->toCommaStr($_1month_later_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_later_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_1month_later_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_1month_later_maximum_amount), 0 , strpos($this->Common->toCommaStr($_1month_later_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td><?php echo $_2month_later_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_later_carryOver), 0 , strpos($this->Common->toCommaStr($_2month_later_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_later_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_later_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_later_billing_price), 0 , strpos($this->Common->toCommaStr($_2month_later_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_later_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_later_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_later_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_2month_later_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_later_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_later_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_2month_later_payment_price), 0 , strpos($this->Common->toCommaStr($_2month_later_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_2month_later_payment_tax), 0 , strpos($this->Common->toCommaStr($_2month_later_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_2month_later_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_later_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_2month_later_payment_track_record), 0 , strpos($this->Common->toCommaStr($_2month_later_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_2month_later_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_later_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_later_not_payment), 0 , strpos($this->Common->toCommaStr($_2month_later_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_later_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_2month_later_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_2month_later_maximum_amount), 0 , strpos($this->Common->toCommaStr($_2month_later_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td><?php echo $_3month_later_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_later_carryOver), 0 , strpos($this->Common->toCommaStr($_3month_later_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_later_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_later_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_later_billing_price), 0 , strpos($this->Common->toCommaStr($_3month_later_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_later_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_later_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_later_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_3month_later_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_later_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_later_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_3month_later_payment_price), 0 , strpos($this->Common->toCommaStr($_3month_later_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_3month_later_payment_tax), 0 , strpos($this->Common->toCommaStr($_3month_later_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_3month_later_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_later_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_3month_later_payment_track_record), 0 , strpos($this->Common->toCommaStr($_3month_later_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_3month_later_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_later_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_later_not_payment), 0 , strpos($this->Common->toCommaStr($_3month_later_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_later_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_3month_later_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_3month_later_maximum_amount), 0 , strpos($this->Common->toCommaStr($_3month_later_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td><?php echo $_4month_later_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_later_carryOver), 0 , strpos($this->Common->toCommaStr($_4month_later_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_later_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_later_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_later_billing_price), 0 , strpos($this->Common->toCommaStr($_4month_later_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_later_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_later_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_later_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_4month_later_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_later_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_later_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_4month_later_payment_price), 0 , strpos($this->Common->toCommaStr($_4month_later_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_4month_later_payment_tax), 0 , strpos($this->Common->toCommaStr($_4month_later_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_4month_later_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_later_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_4month_later_payment_track_record), 0 , strpos($this->Common->toCommaStr($_4month_later_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_4month_later_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_later_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_later_not_payment), 0 , strpos($this->Common->toCommaStr($_4month_later_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_later_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_4month_later_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_4month_later_maximum_amount), 0 , strpos($this->Common->toCommaStr($_4month_later_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td><?php echo $_5month_later_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_later_carryOver), 0 , strpos($this->Common->toCommaStr($_5month_later_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_later_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_later_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_later_billing_price), 0 , strpos($this->Common->toCommaStr($_5month_later_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_later_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_later_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_later_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_5month_later_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_later_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_later_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_5month_later_payment_price), 0 , strpos($this->Common->toCommaStr($_5month_later_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_5month_later_payment_tax), 0 , strpos($this->Common->toCommaStr($_5month_later_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_5month_later_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_later_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_5month_later_payment_track_record), 0 , strpos($this->Common->toCommaStr($_5month_later_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_5month_later_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_later_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_later_not_payment), 0 , strpos($this->Common->toCommaStr($_5month_later_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_later_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_5month_later_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_5month_later_maximum_amount), 0 , strpos($this->Common->toCommaStr($_5month_later_maximum_amount), '.')) ?></td>
            </tr>

            <tr>
              <td><?php echo $_6month_later_paymentDueDate ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_later_carryOver), 0 , strpos($this->Common->toCommaStr($_6month_later_carryOver), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_later_carryOver_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_later_carryOver_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_later_billing_price), 0 , strpos($this->Common->toCommaStr($_6month_later_billing_price), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_later_billing_price_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_later_billing_price_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_later_accounts_receivable), 0 , strpos($this->Common->toCommaStr($_6month_later_accounts_receivable), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_later_accounts_receivable_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_later_accounts_receivable_in_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_6month_later_payment_price), 0 , strpos($this->Common->toCommaStr($_6month_later_payment_price), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_6month_later_payment_tax), 0 , strpos($this->Common->toCommaStr($_6month_later_payment_tax), '.')) ?></td>
              <td class="Scheduled"><?php echo substr($this->Common->toCommaStr($_6month_later_payment_price_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_later_payment_price_in_tax), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_6month_later_payment_track_record), 0 , strpos($this->Common->toCommaStr($_6month_later_payment_track_record), '.')) ?></td>
              <td class="Record"><?php echo substr($this->Common->toCommaStr($_6month_later_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_later_payment_track_record_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_later_not_payment), 0 , strpos($this->Common->toCommaStr($_6month_later_not_payment), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_later_not_payment_in_tax), 0 , strpos($this->Common->toCommaStr($_6month_later_not_payment_in_tax), '.')) ?></td>
              <td><?php echo substr($this->Common->toCommaStr($_6month_later_maximum_amount), 0 , strpos($this->Common->toCommaStr($_6month_later_maximum_amount), '.')) ?></td>
            </tr>

          </tbody>
          <?php echo $this->form->end(); ?>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="common-button" id="btn_result" value="確定" />
        </p>
    </div>
  </form>
