<script>
$(document).ready(function() {
    //チェックボックス一括変更
    $("#allCheck").click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    //選択ボタン押下
    $("#btn_Mod").click(function(){
        $("#selectDepartment").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/selectToItem').submit();
    });
    //検索ボタン押下
    $("#btn_Search").click(function(){
        //バリデート無効化 && ぐるぐる表示 && submit!!
        validateDetachSubmit($('#selectDepartment') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/selectDepartment' );
    });
});
</script>
<div id="TopicPath">
    <ul>
         <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
         <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
         <li>定数切替登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">定数切替部署選択</h2>

<?php echo $this->form->create('MstSwitch',array('class'=>'validate_form search_form','id' =>'selectDepartment','method'=>'post')); ?>
<?php echo $this->form->input('MstSwitch.fromItemId' , array('type'=>'hidden'))?>
<?php echo $this->form->input('MstSwitch.is_department_search' , array('type'=>'hidden'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('MstSwitch.facility_id' ,array('options'=>$Facility_List , 'class'=>'txt' , 'empty'=>'') ); ?></td>
                <th>&nbsp;</th>
                <td></td>
            </tr>

        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search" />
    </div>

    <?php if(isset($this->request->data['MstSwitch']['is_department_search'])){ ?>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
        </span>
        <span class="BikouCopy"></span>
    </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col />
                </colgroup>
                <thead>
                <tr>
                    <th><input type="checkbox" id="allCheck" /></th>
                    <th>施設名</th>
                    <th>部署名</th>
                </tr>
                </thead>

                <tbody>
                <?php $i=0 ; foreach ($SearchResult as $departments) { ?>
                <tr>
                    <td class="center"><input name="data[MstSwitch][department_id][]" type="checkbox" class="validate[required]" id="check<?php echo $departments['MstSwitch']['department_id']; ?>" value="<?php echo $departments['MstSwitch']['department_id']; ?>" /></td>
                    <td><?php echo h_out($departments['MstSwitch']['facility_name']); ?></td>
                    <td><?php echo h_out($departments['MstSwitch']['department_name']); ?></td>
                </tr>
                <?php  $i++; } ?>
            </tbody>
            </table>
        </div>
    </div>
    <?php if(count($SearchResult) > 0  ){ ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn4" id="btn_Mod"/>
    </div>
    <?php } ?>
</form>

    <?php if(count($SearchResult) == 0  ){ ?>

    <tr><td colspan="5">該当データがありませんでした</td></tr>
    </table>
    </div>
    </div>

<?php } } ?>
