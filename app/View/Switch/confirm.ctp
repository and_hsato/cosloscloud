<script type="text/javascript">
$(document).ready(function(){
    //開始日一括入力
    $('#startDateBtn').click(function(){
        //日付が空でない 、正しい日付の場合コピーする
        if( dateCheck($('#startDate').val()) ){
            $('.start_date').val($('#startDate').val());
        }
    });
  
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
  
    $('#confirm_btn').click(function(){
        submit = false;
        cnt = 0;
        //チェックボックスカウント
        $("#confirm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                submit = true;
                cnt++;
                //チェックの入っている行の日付チェック
                if(!dateCheck($(this).parent(':eq(0)').parent(':eq(0)').next().find('input[type=text]:eq(0)').val())){
                    //↑解説：チェックボックスの親要素<TD>の親要素<TR>の兄弟の要素2段目の<TR>の中のTEXTボックスの値を日付チェックする。
                    alert('開始日が不正です。\nYYYY/mm/dd の形式で入力してください。');
                    submit = false;
                    $(this).parent(':eq(0)').parent(':eq(0)').next().find('input[type=text]:eq(0)').focus();
                    return false;
                }
            }
        });
        if(cnt > 0){
            if(submit){
                $('#confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result').submit();
            }
        }else{
            alert('登録情報を選択してください。');
        }

    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>mst_switch/">定数切替登録</a></li>
        <li>定数切替登録確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>定数切替登録確認</span></h2>
<?php echo $this->form->create('MstSwitch',array('class'=>'validate_form search_form','id' =>'confirm','method'=>'post')); ?>
<?php echo $this->form->input('MstSwitch.fromItemId' , array('type'=>'hidden')) ?>
<?php echo $this->form->input('MstSwitch.toItemId' , array('type'=>'hidden')) ?>
<?php echo $this->form->input('MstSwitch.token' , array('type'=>'hidden')) ?>
<div class="SearchBox">
    <h2 class="HeaddingMid">切替元商品情報</h2>
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
        </colgroup>
        <tr>
            <th>商品ID</th>
            <td>
            <?php echo $this->form->input('MstSwitch.internal_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
            </td>
            <td width="20"></td>
            <th>規格</th>
            <td>
            <?php echo $this->form->input('MstSwitch.standard',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
            </td>
        </tr>
        <tr>
            <th>商品名</th>
            <td>
            <?php echo $this->form->input('MstSwitch.item_name',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
            </td>
            <td></td>
            <th>製品番号</th>
            <td>
                <?php echo $this->form->input('MstSwitch.item_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
            </td>
        </tr>
        <tr>
            <th>販売元</th>
            <td>
                <?php echo $this->form->input('MstSwitch.dealer_name' , array('class' => 'lbl' , 'readonly'=>'readonly') ) ;?>
            </td>
            <td></td>
            <th>包装単位</th>
            <td>
                <?php echo $this->form->input('MstSwitch.unit_name' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
            </td>
        </tr>
    </table>
</div>
<table style="width: 100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
<h2 class="HeaddingMid">切替先商品情報</h2>

<div class="SelectBikou_Area">
    <span class="DisplaySelect">
    </span>
    <span class="BikouCopy">
        <?php echo $this->form->input('input.startDate' , array('type'=>'text' , 'class'=> 'date' , 'id'=>'startDate')); ?>
        <input type="button" class="btn btn51 [p2]" id="startDateBtn"/>
    </span>
</div>

<div class="TableScroll2">
    <table class="TableStyle01 table-odd2">
    <tr>
        <th rowspan="2" class="col5">
            <input type="checkbox"  checked="checked" class='checkAll'/>
        </th>
        <th class="col15">施設名</th>
        <th class="col10">商品ID</th>
        <th class="col20">商品名</th>
        <th class="col20">製品番号</th>
        <th class="col10">センター在庫</th>
        <th class="col20">包装単位</th>
    </tr>
    <tr>
        <th>部署名</th>
        <th>開始日</th>
        <th>規格</th>
        <th>販売元</th>
        <th></th>
        <th>状態</th>
    </tr>
<?php
  $i = 0;
  foreach($result as $item){
  ?>
        <tr>
            <td rowspan="2" class="center">
                <?php if($item['MstSwitch']['err'] == ''){?>
                <?php echo $this->form->checkbox('MstSwitch.id.'.$i , array('value'=>$item['MstSwitch']['id'] , 'class' => 'checkAllTarget id' , 'checked'=>'checked' , 'hiddenField'=>false) ); ?>
                <?php } ?>
            </td>
            <td><?php echo h_out($item['MstSwitch']['facility_name']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['internal_code']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['item_name']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['item_code']) ?></td>
            <td>
                <?php echo $this->form->input('MstSwitch.center_stock.'.$i , array('options'=>array(1=>'計算に含める',2=>'計算に含めない') , 'class' => 'txt center_stock_flg') ); ?>
            </td>
            <td><?php echo h_out($item['MstSwitch']['unit_name']); ?></td>
        </tr>
        <tr>
            <td><?php echo h_out($item['MstSwitch']['department_name']); ?></td>
            <td>
                <?php if($item['MstSwitch']['err'] == ''){?>
                <?php echo $this->form->input('MstSwitch.start_date.'.$i , array('type'=>'text' , 'class'=>'start_date date' )); ?>
                <?php } ?>
            </td>
            <td><?php echo h_out($item['MstSwitch']['standard']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['dealer_name']); ?></td>
            <td></td>
            <td><p class="red" style='color:red;'><?php echo $item['MstSwitch']['err'] ?></p></td>
        </tr>
<?php $i++; } ?>
    </table>
</div>
<table style="width: 100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
<div class="ButtonBox">
    <p class="center">
        <input type="button" id="confirm_btn" class="btn btn2 submit [p2]"  />
    </p>
</div>
<?php echo $this->form->end(); ?>
