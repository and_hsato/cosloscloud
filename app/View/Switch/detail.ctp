<form id="detail_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history">
    <div id="TopicPath">
        <ul>
            <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
            <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history">定数切替履歴</a></li>
            <li>定数切替履歴明細</li>
        </ul>
    </div>
    <h2 class="HeaddingLarge"><span>定数切替履歴明細</span></h2>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th class="col5" rowspan="2"></th>
                <th class="col15 center">施設 / 部署</th>
                <th class="col10  center">区分</th>
                <th class="col10">切替元商品ID</th>
                <th class="col10">切替元商品名</th>
                <th class="col10">切替元規格</th>
                <th class="col10">切替元製品番号</th>
                <th class="col10">切替元包装単位</th>
                <th>切替元商品定数</th>
                <th class="col10">登録者</th>
            </tr>
            <tr>
                <th>消費番号</th>
                <th>センター在庫</th>
                <th>切替先商品ID</th>
                <th>切替先商品名</th>
                <th>切替先規格</th>
                <th>切替先製品番号</th>
                <th>切替先包装単位</th>
                <th>切替先商品定数</th>
                <th>登録日時</th>
            </tr>
            <?php $i=0;foreach($SearchResult as $_row): ?>
            <tr>
                <td rowspan="2" align="center"></td>
                <td><?php echo h_out($_row['MstSwitch']['facility_name'] . " / " . $_row['MstSwitch']['department_name']); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['type'],'center'); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['from_internal_code'],'center'); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['from_item_name']); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['from_standard']); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['from_item_code']); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['from_unit_name']); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['fixed_count_from']); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['user_name']); ?></td>
            </tr>
            <tr>
                <td><?php echo h_out($_row['MstSwitch']['work_no'],'center'); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['center_stock'],'center'); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['to_internal_code'],'center'); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['to_item_name']); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['to_standard']); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['to_item_code']); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['to_unit_name']); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['fixed_count_to']); ?></td>
                <td><?php echo h_out($_row['MstSwitch']['created'],'center'); ?></td>
            </tr>
            <?php $i++;endforeach; ?>
        </table>
    </div>
<?php echo $this->form->end(); ?>