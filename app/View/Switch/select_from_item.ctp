<script type="text/javascript">
$(function(){
    //検索ボタン押下
    $("#search_btn").click(function(){
        validateDetachSubmit($("#selectFromItem") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/selectFromItem');
    });
    //選択ボタン押下
    $("#select_btn").click(function(){
        $("#selectFromItem").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/selectDepartment').submit();
    });
});

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>定数切替登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>切替元商品選択</span></h2>
<h2 class="HeaddingMid">検索条件</h2>
<?php echo $this->form->create('MstSwitch',array('class'=>'validate_form search_form','id' =>'selectFromItem','method'=>'post')); ?>
    <?php echo $this->form->input('MstSwitch.is_search',array('type' => 'hidden')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstSwitch.internal_code',array('class' => 'txt search_internal_code','maxlength' => '50' ) ); ?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstSwitch.item_code',array('class' => 'txt search_upper','maxlength' => '50' ) ); ?></td>
                <td width="20"></td>
                <th></th>
                <td></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstSwitch.item_name',array('class' => 'search_canna txt','maxlength' => '50' ) ); ?></td>
                <td width="20"></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstSwitch.dealer_name',array('class' => 'search_canna txt','maxlength' => '50' ) ); ?></td>
                <td width="20"></td>
                <th></th>
                <td></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstSwitch.standard',array('class' => 'search_canna txt','maxlength' => '50' ) ); ?></td>
                <td width="20"></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('MstSwitch.jan_code',array('class' => 'txt','maxlength' => '50' ) ); ?></td>
                <td width="20"></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="search_btn" />
            </p>
        </div>
    </div>
    <hr class="Clear" />
    <div class="DisplaySelect">
        <div align="right" id="pageTop" style="padding-right: 10px;"></div>
            <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult) ) ); ?>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th width="20"></th>
                    <th class="col10">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col20">規格</th>
                    <th class="col20">製品番号</th>
                    <th class="col15">販売元</th>
                    <th class="col15">包装単位</th>
                </tr>
        <?php if(isset($this->request->data['MstSwitch']['is_search'])){ ?>
            <?php if(count($SearchResult) == 0 ){  ?>
                <tr>
                    <td colspan="7" class="center">該当するデータがありませんでした</td>
                </tr>
            <?php
                }else{
                    $i = 0;
                    foreach($SearchResult as $item){
            ?>
                <tr>
                    <td width="20" class="center">
                        <input type="radio" name="data[MstSwitch][fromItemId]" class="validate[required]" id="item<?php echo $item['MstSwitch']['id']; ?>" value="<?php echo $item['MstSwitch']['id']; ?>" />
                    </td>
                    <td class="col10"><?php echo h_out($item['MstSwitch']['internal_code'],'center'); ?></td>
                    <td class="col20"><?php echo h_out($item['MstSwitch']['item_name']); ?></td>
                    <td class="col20"><?php echo h_out($item['MstSwitch']['standard']); ?></td>
                    <td class="col20"><?php echo h_out($item['MstSwitch']['item_code']); ?></td>
                    <td class="col15"><?php echo h_out($item['MstSwitch']['dealer_name']); ?></td>
                    <td class="col15"><?php echo h_out($item['MstSwitch']['unit_name']); ?></td>
                </tr>
                <?php  $i++; } } ?>
            </table>
        </div>
    <?php if(count($SearchResult) !== 0 ){  ?>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn4" id="select_btn" />
        </p>
    </div>
    <div align="right" style="padding-right: 10px;" id="pageDow" ></div>
    <?php } } ?>
<?php echo $this->form->end(); ?>
