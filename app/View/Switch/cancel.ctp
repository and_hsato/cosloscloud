<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li class="pankuzu">定数切替履歴</li>
        <li>定数切替履歴結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>定数切替履歴結果</span></h2>
<div class="Mes01">定数切替を取り消しました。</div>
<?php echo $this->form->create('Switch',array('type'=>'post','action'=>'','id'=>'searchForm','class'=>'validate_form search_form')); ?>
    <div id="results">
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th class="col5" rowspan="2"></th>
                    <th class="col15" rowspan="2">施設 / 部署</th>
                    <th class="col10">切替元商品ID</th>
                    <th class="col10">切替元商品名</th>
                    <th class="col10">切替元規格</th>
                    <th class="col10">切替元製品番号</th>
                    <th class="col10">切替元包装単位</th>
                    <th class="col10">センター在庫</th>
                    <th class="col10">開始日</th>
                </tr>
                <tr>
                    <th class="col10">切替先商品ID</th>
                    <th class="col10">切替先商品</th>
                    <th class="col10">切替先規格</th>
                    <th class="col10">切替先製品番号</th>
                    <th class="col10">切替先包装単位</th>
                    <th class="col10">登録者</th>
                    <th class="col10">完了日</th>
                </tr>

                <?php $_cnt=0;foreach($SearchResult as $_row): ?>
                <tr>
                    <td rowspan="2"></td>
                    <td rowspan="2"><?php echo h_out($_row['MstSwitch']['facility_name']." / ".$_row['MstSwitch']['department_name']); ?></td>
                    <td><?php echo h_out($_row['MstSwitch']['from_internal_code']);?></td>
                    <td><?php echo h_out($_row['MstSwitch']['from_item_name']);?></td>
                    <td><?php echo h_out($_row['MstSwitch']['from_standard']);?></td>
                    <td><?php echo h_out($_row['MstSwitch']['from_item_code']);?></td>
                    <td><?php echo h_out($_row['MstSwitch']['from_unit_name']);?></td>
                    <td><?php echo h_out($_row['MstSwitch']['center_stock'],'center');?></td>
                    <td><?php echo h_out($_row['MstSwitch']['start_date'],'center');?></td>
                </tr>
                <tr>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['to_internal_code']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['to_item_name']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['to_standard']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['to_item_code']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['to_unit_name']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['user_name']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['end_date'],'center');?></td>
                </tr>
                <?php $_cnt++;endforeach;?>
            </table>
        </div>
    </div>
<?php echo $this->form->end(); ?>
