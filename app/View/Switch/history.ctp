<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
    });

    //検索ボタン押下
    $("#btn_Search").click(function(){
        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history').submit();
    });

    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/').submit();
    });

    //明細表示ボタン押下
    $("#btn_Confirm").click(function(){
        if( $("input.chk:checked").length > 0){
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/detail').submit();
        }else{
            alert("対象を選択して下さい。");
        }
    });

    //取消ボタン押下
    $("#btn_Cancel").click(function(){
        if( $("input.chk:checked").length > 0){
            err = false;
            $("#searchForm input[type=checkbox].chk").each(function(){
                if($(this).attr("checked") == true){
                    //チェックが入っていたら次のhidden要素（ステータス）を検索し値を確認
                    if($(this).parent(':eq(0)').find('input[type=hidden]:eq(0)').val() == 0 ){
                         //取消不可だった場合エラーフラグを立てておく
                         err = true;
                    }
                }
            });
            //取り消せないものを選んでいる場合エラーにする。
            if(err){
                alert('取り消せない定数切替履歴を含んでいます。');
            }else{
                if(window.confirm("取消を実行します。よろしいですか？")){
                    $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/cancel').submit();
                }
            }
        }else{
            alert("取消対象を選択してください。");
        }
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li>定数切替履歴</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>定数切替履歴</span></h2>

<?php echo $this->form->create('Switch',array('type'=>'post','action'=>'','id'=>'searchForm','class'=>'validate_form search_form input_form')); ?>
    <?php echo $this->form->input('MstSwitch.token' , array('type'=>'hidden')) ?>
    <?php echo $this->form->input('MstSwitch.is_search',array('type'=>'hidden','id'=>'is_search'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->text('search.facility_code', array('class'=>'txt','style'=>'width:60px','id'=>'facilityText'));?>
                    <?php echo $this->form->input('search.facilities',array('options'=> $Facility_List, 'class'=>'txt','style'=>'width:150px;' , 'empty'=>'' ,'id'=>'facilityCode')); ?>
                    <?php echo $this->form->input('search.facility_name', array( 'type'=>'hidden' ,'id'=>'facilityName'));?>
                </td>
                <td></td>
                <th>部署</th>
                <td>
                    <?php echo $this->form->text('search.department_code',array('class'=>'txt','style'=>'width:60px' ,'id'=>'departmentText'));?>
                    <?php echo $this->form->input('search.departments',array('options'=> $Department_List , 'class'=>'txt','style'=>'width:150px;' , 'empty'=>'' ,'id'=>'departmentCode')); ?>
                    <?php echo $this->form->input('search.department_name' ,array('type'=>'hidden' , 'id'=>'departmentName'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('search.internal_code',array('class'=>'txt search_upper search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('search.item_code',array('class'=>'txt search_upper')); ?></td>
                <td></td>
                <th></th>
                <td><?php echo $this->form->checkbox('search.is_deleted',array('hiddenField'=>false )); ?>取消は表示しない</td>
                <td>
                </td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('search.item_name',array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('search.dealer_name',array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th></th>
                <td><?php echo $this->form->checkbox('search.is_end',array('hiddenField'=>false )); ?>完了は表示しない</td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('search.standard',array('class'=>'txt')); ?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="btn_Search"/>
                &nbsp;&nbsp;
                <input type="button" class="btn btn5" id="btn_Csv"/>
            </p>
        </div>
    </div>

    <div id="results">
        <div class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
        </div>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <tr>
                    <th class="col5" rowspan="2"><?php echo $this->form->checkbox('MstSwitch.checkbox' , array('class'=>"checkAll")); ?></th>
                    <th class="col15" rowspan="2">施設 / 部署</th>
                    <th class="col10">切替元商品ID</th>
                    <th class="col10">切替元商品名</th>
                    <th class="col10">切替元規格</th>
                    <th class="col10">切替元製品番号</th>
                    <th class="col10">切替元包装単位</th>
                    <th class="col10">センター在庫</th>
                    <th class="col10">開始日</th>
                </tr>
                <tr>
                    <th class="col10">切替先商品ID</th>
                    <th class="col10">切替先商品名</th>
                    <th class="col10">切替先規格</th>
                    <th class="col10">切替先製品番号</th>
                    <th class="col10">切替先包装単位</th>
                    <th class="col10">登録者</th>
                    <th class="col10">完了日</th>
                </tr>

            <?php $_cnt=0;foreach($SearchResult as $_row): ?>
                <tr>
                    <td class="col5 center" rowspan="2">
                        <?php echo $this->form->checkbox("MstSwitch.id.{$_cnt}",array('class'=>'center chk','value'=>$_row['MstSwitch']['id'] , 'hiddenField'=>false) );?>
                        <?php echo $this->form->input("MstSwitch.status.{$_cnt}",array('type'=>'hidden','value'=>$_row['MstSwitch']['status']) );?>
                    </td>
                    <td class="col15" rowspan="2"><?php echo h_out($_row['MstSwitch']['facility_name']." / ".$_row['MstSwitch']['department_name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['from_internal_code'] , 'center');?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['from_item_name']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['from_standard']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['from_item_code']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['from_unit_name']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['center_stock'],'center');?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['start_date'],'center');?></td>
                </tr>
                <tr>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['to_internal_code'] , 'center');?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['to_item_name']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['to_standard']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['to_item_code']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['to_unit_name']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['user_name']);?></td>
                    <td class="col10"><?php echo h_out($_row['MstSwitch']['end_date'],'center');?></td>
                </tr>
                <?php $_cnt++;endforeach;?>
                <?php if(count($SearchResult) == 0 && isset($this->request->data['MstSwitch']['is_search'])){ ?>
                <tr>
                    <td colspan= '9' class="center">該当するデータがありませんでした
                    </td>
                </tr>
                <?php } ?>
            </table>
        </div>
        <?php if(count($SearchResult) > 0 ){ ?>
        <div class="ButtonBox" style="margin-top:5px;">
            <p class="center">
                <input type="button" class="btn btn6 [p2]" id="btn_Cancel" />
                <input type="button" class="btn btn7" id="btn_Confirm" />
            </p>
        </div>
        <?php } ?>
    </div>
<?php echo $this->form->end(); ?>
