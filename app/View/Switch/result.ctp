<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/">定数切替登録</a></li>
        <li>定数切替登録完了</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>定数切替登録確認</span></h2>
<?php echo $this->form->create('MstSwitch',array('class'=>'validate_form search_form','id' =>'confirm')); ?>
<div class="SearchBox">
<div class="Mes01">以下の内容で登録しました</div>
    <h2 class="HeaddingMid">切替元商品情報</h2>
    <table class="FormStyleTable">
        <colgroup>
            <col />
            <col />
            <col width="20" />
            <col />
            <col />
        </colgroup>
        <tr>
            <th>商品ID</th>
            <td>
            <?php echo $this->form->input('MstSwitch.internal_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
            </td>
            <td width="20"></td>
            <th>規格</th>
            <td>
            <?php echo $this->form->input('MstSwitch.standard',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
            </td>
        </tr>
        <tr>
            <th>商品名</th>
            <td>
            <?php echo $this->form->input('MstSwitch.item_name',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
            </td>
            <td></td>
            <th>製品番号</th>
            <td>
                <?php echo $this->form->input('MstSwitch.item_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?>
            </td>
        </tr>
        <tr>
            <th>販売元</th>
            <td>
                <?php echo $this->form->input('MstSwitch.dealer_name' , array('class' => 'lbl' , 'readonly'=>'readonly') ) ;?>
            </td>
            <td></td>
            <th>包装単位</th>
            <td>
                <?php echo $this->form->input('MstSwitch.unit_name' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
            </td>
        </tr>
    </table>
</div>
<table style="width: 100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
<h2 class="HeaddingMid">切替先商品情報</h2>
<div class="TableScroll2">
    <table class="TableStyle01 table-odd2">
        <tr>
            <th rowspan="2" class="col5"></th>
            <th class="col15">施設名</th>
            <th class="col10">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">製品番号</th>
            <th class="col10">センター在庫</th>
            <th class="col20">包装単位</th>
        </tr>
        <tr>
            <th>部署名</th>
            <th>開始日</th>
            <th>規格</th>
            <th>販売元</th>
            <th></th>
            <th></th>
        </tr>
<?php
  $i = 0;
  foreach($result as $item){
  ?>
        <tr>
            <td rowspan="2" class="center"></td>
            <td><?php echo h_out($item['MstSwitch']['facility_name']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['to_internal_code']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['to_item_name']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['to_item_code']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['center_stock']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['to_unit_name']); ?></td>
        </tr>
        <tr>
            <td><?php echo h_out($item['MstSwitch']['department_name']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['start_date']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['to_standard']); ?></td>
            <td><?php echo h_out($item['MstSwitch']['to_dealer_name']); ?></td>
            <td></td>
            <td></td>
        </tr>
<?php $i++; } ?>
    </table>
</div>
<table style="width: 100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
<?php echo $this->form->end(); ?>
