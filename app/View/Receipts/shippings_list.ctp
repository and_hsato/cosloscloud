<script>
$(document).ready(function() {
    $('#EdiShippingNo').val('').focus();
    // 確認ボタン押下
    $("#btn_Conf").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#ShippingsList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/conf');
            $("#ShippingsList").submit();
        } else {
            alert('必ず一つは選択してください');
        }
    });
  
    // 業者出荷伝票入力
    $('#EdiShippingNo').keyup(function(e){
        if(e.which === 13){
            if($('#EdiShippingNo').val().length > 0){
                $('#ShippingsList').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/conf/2').submit();
            }
        }
    });
  
    // 発注書入力
    $('#OrderNo').keyup(function(e){
        if(e.which === 13){
            if($('#OrderNo').val().length > 0){
                $('#ShippingsList').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/conf/3').submit();
            }
        }
    });
  
    // 検索ボタン押下
    $("#btn_Search").click(function(){
        $("#ShippingsList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ShippingsList/<?php echo $this->request->data['MstReceipts']['type'] ?>').submit();
    });
});
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a>入荷処理</a></li>
        <li>入荷予定一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入荷予定一覧</span></h2>
<form method="post" action="" accept-charset="utf-8" id="ShippingsList" class="validate_form search_form">
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden'))?>
    <?php echo $this->form->input('MstReceipts.type',array('type'=>'hidden'))?>
    
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>入荷日</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.work_date',array('type'=>'text' , 'class' => 'lbl' , 'readonly'=>'readonly'));?>
                </td>
                <td></td>
                <!--<th>作業区分</th>
                <td>
                    <?php //echo $this->form->input('MstReceipts.className',array('type'=>'text' , 'class' => 'lbl' , 'readonly'=>'readonly'));?>
                    <?php //echo $this->form->input('MstReceipts.classId',array('type'=>'hidden'));?>
                </td>
                <td></td> -->
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.facilityName' , array('type'=>'text' ,'class'=>'lbl' ,'readonly'=>'readonly'));?>
                    <?php echo $this->form->input('MstReceipts.facilityId' , array('type'=>'hidden'));?>
                    <?php echo $this->form->input('MstReceipts.facilityCode',array('type'=>'hidden',)); ?>
                </td>
                <td></td>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.departmentName' , array('class'=>'lbl' ,'readonly'=>'readonly') );?>
                    <?php echo $this->form->input('MstReceipts.departmentId' , array('type'=>'hidden') );?>
                    <?php echo $this->form->input('MstReceipts.departmentCode',array('type'=>'hidden')); ?>
                </td>
                <td></td>
            </tr>
           <!-- <tr>
                
                 <th>備考</th>
                <td>
                    <?php //echo $this->form->input('MstReceipts.recital.0' , array('class'=>'lbl' ,'readonly'=>'readonly') );?>
                </td>
                <td></td> 
                
            </tr>-->

        </table>
    </div>
    <h2 class="HeaddingMid">入荷処理を行いたい商品を検索してください。</h2>
        <h3 class="conth3">未入荷商品の検索</h3>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <?php if($this->request->data['MstReceipts']['type'] != 1) { ?>
            <tr>
                <!-- <th>業者出荷番号</th>
                <td>
                    <?php //echo $this->form->input('MstReceipts.search_edi_shippings_no' , array('class'=>'txt' ,'id'=>'EdiShippingNo'))?>
                </td>
                <td></td> -->
                <th>発注番号</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.search_order_no' , array('class'=>'txt' ,'id'=>'OrderNo'))?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <?php } ?>
            <tr>
               <!-- <th>出荷番号</th>
                <td>
                    <?php //echo $this->form->input('MstReceipts.search_shippings_no' , array('class'=>'txt'))?>
                </td>
                <td></td> -->
                <th>発注日</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.search_start_date' , array('class'=>'date validate[optional,custom[date]]'))?>
                     ～
                    <?php echo $this->form->input('MstReceipts.search_end_date' , array('class'=>'date validate[optional,custom[date]]'))?>
                </td>
                <td></td>
                <!-- <th>APコード</th>
                <td>
                    <?php //echo $this->form->input('MstReceipts.search_aptage_code' , array('class'=>'txt'))?>
                </td>
                <td></td> -->
            </tr>
            <tr>
                <th>商品ID</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.search_internal_code' , array('class'=>'txt search_internal_code'));?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.search_item_code' , array('class'=>'txt search_upper'));?>
                </td>
                <td></td>
            <!--    <th>ロット番号</th>
                <td>
                    <?php //echo $this->form->input('MstReceipts.search_lot_no' , array('class'=>'txt'));?>
                </td>
                <td></td> -->
            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.search_item_name' , array('class'=>'txt search_canna'));?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.search_dealer_name' , array('class'=>'txt search_canna'));?>
                </td>
                <td></td>
             <!--   <th>管理区分</th>
                <td>
                    <?php //echo $this->form->input('MstReceipts.search_type' , array('options'=>Configure::read('ReceiptClasses') , 'class'=>'txt' , 'empty'=>true ));?>
                </td>
                <td></td> -->
            </tr>
            <tr>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.search_standard' , array('class'=>'txt search_canna'));?>
                </td>
                <td></td>
             <!--   <th>シール番号</th>
                <td>
                    <?php //echo $this->form->input('MstReceipts.search_sticker_no' , array('class'=>'txt'));?>
                </td>
                <td></td> -->
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.supplierName',array('type'=>'hidden' ,'id' => 'supplierName')); ?>
                    <?php echo $this->form->input('MstReceipts.supplierText',array('id' => 'supplierText','style'=>'width:60px;', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('MstReceipts.supplierCode',array('options'=>$supplier_list ,'class'=>'txt','id' => 'supplierCode', 'empty'=>true)); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Search" value="検索"/>
    </div>
     <div class="results">

    <h2 class="HeaddingSmall">検索結果</h2>
     <div class="SelectBikou_Area">
         <span class="DisplaySelect">
             <?php echo $this->element('limit_combobox',array('result'=>count($ShippingList))); ?>
         </span>
     </div>
         <div class="TableScroll">
             <table class="TableStyle01 table-even">
                 <colgroup>
                     <col width="25" />
                     <col width="150"/>
                     <col width="120"/>
                     <col />
                     <col />
                 </colgroup>
                 <thead>
                 <tr>
                     <th><input type="checkbox" checked onClick="listAllCheck(this);"/></th>
                     <th>出荷番号</th>
                     <th>発注日</th>
                     <th>仕入先</th>
                     <th>施設 ／ 部署</th>
                     <th>シール枚数</th>
                 </tr>
                 </thead>
                 <tbody>
                 <?php foreach($ShippingList as $data){ ?>
                 <tr>
                     <td class="center"><input type="checkbox" checked class="center chk" name="data[MstReceipts][id][]" value="<?php echo $data['TrnShipingHeader']['id'] ?>"/></td>
                     <td><?php echo h_out($data['TrnShipingHeader']['work_no'],'center'); ?></td>
                     <td><?php echo h_out($data['TrnShipingHeader']['work_date'],'center'); ?></td>
                     <td><?php echo h_out($data['TrnShipingHeader']['supplier_name']); ?></td>
                     <td><?php echo h_out($data['TrnShipingHeader']['facility_name']." ／ ".$data['TrnShipingHeader']['department_name']); ?></td>
                     <td><?php echo h_out($data['TrnShipingHeader']['count'] . ' / ' . $data['TrnShipingHeader']['detail_count'],'right') ?></td>
                </tr>
                <?php } ?>
                <?php if(count($ShippingList)==0 && isset($this->request->data['search']['is_search'])){ ?>
                <tr><td colspan="6" class="center">該当するデータがありませんでした</td></tr> 
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php if(count($ShippingList)>0){ ?>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Conf" value="確認"/>
    </div>
    <?php } ?>
</form>
    </div><!--#content-wrapper-->
