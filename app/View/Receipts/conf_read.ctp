<script>
$(document).ready(function(){
    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    //備考一括制御
    $('#recitalbtn').click(function(){
        $('input[type=text].recital').val($('#recital').val());
    });

    //初期状態でチェックが入っていたら、バリデートクラスを追加する。
    $('input[type=checkbox].chk').each(function(){
       id = $(this).val();
       if($(this).attr('checked') == true ){
            //チェックが入ったら、日付形式チェックを追加
            $('#validate'+id).addClass("validate[custom[date]]");

            if($('#validate'+id).hasClass('hasCheck')){
                //クラス分類3以上だったら必須チェックを追加
                $('#validate'+id).addClass("validate[required]");
            }
            if($('#lot'+id).hasClass('hasCheck')){
                //クラス分類3以上だったら必須チェックを追加
                $('#lot'+id).addClass("validate[required]");
            }
       }
    });

    //チェックに変更があったらクラスをつけはずしする。
    $('.chk').change(function(){
        id = $(this).val();
        if($(this).attr('checked')==true){
            //チェックが入ったら、日付形式チェックを追加
            $('#validate'+id).addClass("validate[custom[date]]");

            if($('#validate'+id).hasClass('hasCheck')){
                //クラス分類3以上だったら必須チェックを追加
                $('#validate'+id).addClass("validate[required]");
            }
            if($('#lot'+id).hasClass('hasCheck')){
                //クラス分類3以上だったら必須チェックを追加
                $('#lot'+id).addClass("validate[required]");
            }
        }else{
            //チェックが外れたら、日付形式チェックを削除
            $('#validate'+id).removeClass("validate[custom[date]]");
            
            if($('#validate'+id).hasClass('hasCheck')){
                //クラス分類3以上だったら必須チェックを削除
                $('#validate'+id).removeClass("validate[required]");
            }
            if($('#lot'+id).hasClass('hasCheck')){
                //クラス分類3以上だったら必須チェックを削除
                $('#lot'+id).addClass("validate[required]");
            }
        }
    });
    $("#btn_Mod").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#ShippingsList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name ; ?>/result');
            $("#ShippingsList").submit();
         }else{
            alert('受領対象をチェックしてください。');
        }
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">受領</a></li>
        <li class="pankuzu">シール読み込み</a></li>
        <li>受領確認（預託品）</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>受領確認（預託品）</span></h2>
<h2 class="HeaddingMid">預託品の受領登録を行います。</h2>
<form class="validate_form" method="post" action="" accept-charset="utf-8" id="ShippingsList">
    <?php echo $this->form->input('Receipts.token', array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>受領日</th>
                <td><?php echo $this->form->input('MstReceipts.work_date' , array('class'=>'lbl' , 'readonly' => 'readonly'))?></td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.className' , array('class'=>'lbl' , 'readonly' => 'readonly'))?>
                    <?php echo $this->form->input('MstReceipts.classId' , array('type'=>'hidden' ))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.facilityName' , array('class'=>'lbl' , 'readonly' => 'readonly'))?>
                    <?php echo $this->form->input('MstReceipts.facilityId' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.recital.0' , array('class'=>'lbl' , 'readonly'=>'readonly')) ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.departmentName' , array('class'=>'lbl' , 'readonly' => 'readonly'))?>
                    <?php echo $this->form->input('MstReceipts.departmentId' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
            <table style="width:100%;">
                <tr>
                    <td>
                        <span class="DisplaySelect">
                        　表示件数：<?php echo count($ShippingList);  ?>件
                        </span>
                        <span class="BikouCopy">
                            <input type="text" class="txt" id="recital" value="" maxlength="200" />
                            <input type="button" class="btn btn8 [p2]" id="recitalbtn"/>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col width="85"/>
                    <col width="100"/>
                    <col />
                    <col />
                    <col width="85" />
                    <col />
                    <col />
                    <col width="75" />
                    <col width="75" />
                </colgroup>
                <tr>
                    <th rowspan="2"><input type="checkbox" class="checkAll" checked="checked"/></th>
                    <th>出荷番号</th>
                    <th>出荷日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>ロット番号</th>
                    <th>センターシール</th>
                    <th>作業区分</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th colspan="2">部署</th>
                    <th>管理区分</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th></th>
                    <th>有効期限</th>
                    <th>部署シール</th>
                    <th colspan="2">備考</th>
                </tr>
                <?php
                        $cnt=0;
                        foreach ($ShippingList as $shippMeisai) {
                            $err="";$err2="";$v_date_class="";$checked = " checked ";
                            //物流シール商品違い・預託品かどうかのチェック
                            if($shippMeisai['TrnShipping']['status'] != ""){$err="1";}
                            //物流シール有効期限チェック
                            if($shippMeisai['TrnShipping']['caution'] != ""){$err2="1";$v_date_class=" err ";$checked = "";}
                ?>
                <tr>
                    <td rowspan="2" class="center">
                        <?php
                            //施設採用品のクラス分類が3以上の場合は、有効期限・ロット番号の入力を必須とする
                            if($shippMeisai['MstFacilityItem']['class_separation'] >= 3){
                                $checkClass = "hasCheck";
                            } else {
                                $checkClass = "";
                            }

                            $validated_date = $shippMeisai['ean_data']['format_validated_date'];
                        ?>
                    <?php if($err != "1"){ ?>
                        <input type="checkbox" <?php echo $checked; ?>  class="center chk" name="data[MstReceipts][id][<?php echo $shippMeisai['TrnShipping']['id']; ?>]" value="<?php echo $shippMeisai['TrnShipping']['id']; ?>" />
                        <?php echo $this->form->input('MstReceipts.modified.'.$shippMeisai['TrnShipping']['id'] , array('type'=>'hidden' , 'value'=>$shippMeisai['TrnShipping']['modified']))?>
                    <?php } ?>
                    </td>
                    <td><?php echo h_out($shippMeisai['TrnShipping']['work_no']); ?></td>
                    <td><?php echo h_out($shippMeisai['TrnShipping']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($shippMeisai['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($shippMeisai['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($shippMeisai['MstFacilityItem']['item_code']); ?></td>
                    <td><?php echo h_out($shippMeisai['TrnShipping']['unit_name']); ?></td>
                    <td><input type="text" maxlength="20" class="tbl_txt <?php echo $checkClass?>" id="lot<?php echo $shippMeisai['TrnShipping']['id']; ?>" name="data[MstReceipts][lot_no][<?php echo $shippMeisai['TrnShipping']['id']; ?>]" value="<?php echo $shippMeisai['ean_data']['lot_no']; ?>" /></td>
                    <td></td>
                    <td><?php echo h_out($shippMeisai['TrnShipping']['class_name']); ?></td>
                    <td><?php echo h_out($shippMeisai['MstUser']['user_name']); ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($shippMeisai['TrnShipping']['facility_name']); ?></td>
                    <td><?php echo h_out(Configure::read('Classes.'.$shippMeisai['TrnShipping']['shipping_type']) , 'center'); ?></td>
                    <td><?php echo h_out($shippMeisai['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($shippMeisai['MstDealer']['dealer_name']); ?></td>
                    <td></td>
                    <td>
                        <input type="text" class="<?php echo $v_date_class .' ' . $checkClass . ' date'; ?>" id="validate<?php echo $shippMeisai['TrnShipping']['id']; ?>" name="data[MstReceipts][validated_date][<?php echo $shippMeisai['TrnShipping']['id']; ?>]" value="<?php echo $validated_date; ?>" />
                    </td>
                    <td><label title="<?php echo $shippMeisai['TrnSticker']['hospital_sticker_no']; ?>"><?php echo $shippMeisai['TrnSticker']['hospital_sticker_no']; ?></label></td>
                    <td colspan="2">
                    <?php if($err != "1"){ ?>
                        <input type="text" maxlength="200" name = "data[MstReceipts][recital][<?php echo $shippMeisai['TrnShipping']['id']; ?>]" class="tbl_txt recital" value="<?php echo $shippMeisai['TrnShipping']['caution']; ?>" />
                    <?php } else { ?>
                        <?php echo h_out($shippMeisai['TrnShipping']['status']); ?>
                    <?php } ?>
                    </td>
                </tr>
                <?php $cnt++; } ?>
            </table>
        </div>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
        <input type="button" class="btn btn2 [p2]" id="btn_Mod"/>
    </div>
</form>
