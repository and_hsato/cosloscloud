<script type="text/javascript">
/*
 * バーコードリスト、削除処理
 * @param {event} event
 * @return {bool}
 */

String.prototype.trim = function() {return this.replace(/^\s+|\s+$/g,"");}

function editTextArea(event){
    var KeyChar   = event.keyCode;
    var codeInput = $('#codeinput').val().trim().toUpperCase();

    if (KeyChar == 13){ //Enterキー押下
        var _hasDup = false;
        if (codeInput === '') {
            $('#err_msg').text('空文字は入力できません');
            $('#codeinput').val('');
        } else {
            //部署シールの場合は重複チェックを行う
            if (!codeInput.search(/^01[0-9][0-9]{13}/i) != -1) {
                $('#barcodelist_h > li').each(function () {
                    if( codeInput == this.innerText) {
                        $('#err_msg').text("番号が重複しています");
                        _hasDup = true;
                        $('#codeinput').val('');
                    }
                });
            }
            if (_hasDup === false) {
                if (codeInput.search(/^01[0-9][0-9]{13}/i) != -1) {
                    //物流シール
                    //要素追加
                    $('#barcodelist_b').append('<li>'+codeInput+'</li>');
                    $('#barcodelist_b > li').click(function(){
                        $('#barcodelist_b > li').removeClass('selected');
                        $(this).addClass('selected');
                    });
                    //スクロール
                    $('#barcodelist_b').scrollTop($('#barcodelist_b > li').length * 14);
                    $('#codez_b').attr('value', $('#codez_b').val() + codeInput + ',');
                    $('#barcode_count_b').text('読込件数：'+ $('#barcodelist_b > li').length +'件');

                }else{
                    //部署シール
                    //要素追加
                    $('#barcodelist_h').append('<li>'+codeInput+'</li>');
                    $('#barcodelist_h > li').click(function(){
                        $('#barcodelist_h > li').removeClass('selected');
                        $(this).addClass('selected');
                    });
                    $('#barcodelist_h').scrollTop($('#barcodelist_h > li').length * 14);
                    $('#codez_h').attr('value', $('#codez_h').val() + codeInput + ',');
                    $('#barcode_count_h').text('読込件数：'+ $('#barcodelist_h > li').length +'件');
                }
                $('#err_msg').text('');
                $('#codeinput').val('');
            }
        }
        $('#codeinput').focus();
        return false;
    }
}

$(document).ready(function(){
    // current codez.
    var _currentCodez = null;

    //初期フォーカスをバーコード入力欄にあわせる
    $('#codeinput').focus();

    //DELETEキー押下
    $(document).keyup(function(e){
        if(e.which === 46){ //DELETEキー
            obj = jQuery(document.activeElement);
            if(obj.attr('id') != 'codeinput'){ //バーコード入力上でDELが押された場合は無視
                $('#barcodelist_b li.selected').remove();
                $('#barcodelist_h li.selected').remove();
  
                $('#codez_b').val(''); //いったん全部消す
                $('#codez_h').val(''); //いったん全部消す
                $('#barcodelist_b li').each(function(){
                    $('#codez_b').val($('#codez_b').val() + this.innerText + ',');
                });
                $('#barcodelist_h li').each(function(){
                    $('#codez_h').val($('#codez_h').val() + this.innerText + ',');
                });
                $('#barcode_count_b').text('読込件数：'+ $('#barcodelist_b > li').length +'件');
                $('#barcode_count_h').text('読込件数：'+ $('#barcodelist_h > li').length +'件');
            }
        }
        $('#codeinput').focus();
    });
  
    //確認ボタン押下
    $("#confirmBtn").click(function(){
        if($('#barcodelist_b > li').length + $('#barcodelist_h > li').length == 0 ){
             alert('バーコードが入力されていません');
             $('#codeinput').focus();
             return false;
        }else if($('#barcodelist_b > li').length != $('#barcodelist_h > li').length){
             alert('物流シールの数と部署シールの数が合っていません');
             $('#codeinput').focus();
             return false;
        }else{
             $("#receipts_codez_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/conf_read').submit();
        }
    });
    //クリアボタン押下
    $("#claerBtn").click(function(){
        $('#codeinput').val('');
        $('#err_msg').text('');
        $('#codeinput').focus();
    });

    //全クリアボタン押下
    $('#allClaerBtn').click(function(){
        $('#barcodelist_b > li').remove();
        $('#barcodelist_h > li').remove();
        $('#codeinput').val('');
        $('#codez_h').val('');
        $('#codez_b').val('');
        $('#err_msg').text('');
        $('#barcode_count_h').text('読込件数：0件');
        $('#barcode_count_b').text('読込件数：0件');
        $('#codeinput').focus();
    });
});
</script>
<style>
#barcodelist_b,
#barcodelist_h {
  width: 362px;
  height:280px;
  overflow: auto;
  margin:5px 0px 5px;
  padding:5px;
  display: block;
  border: groove 1px #111111;
}
#barcodelist_b > li ,
#barcodelist_h > li {
  width: 350px;
  cursor: pointer;
  display: block;
}

.selected{
  background:#0A246A;
  color:#ffffff;
}
</style>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">受領登録</a></li>
        <li>シール読込</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>シール読込</span></h2>
<form class="validate_form" id="receipts_codez_form" method="POST">
    <?php echo $this->form->hidden('codez_b', array('id'=>'codez_b',)); ?>
    <?php echo $this->form->hidden('codez_h', array('id'=>'codez_h',)); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>受領日</th>
                <td><?php echo $this->form->input('MstReceipts.work_date' , array('class'=>'lbl' , 'readonly' => 'readonly'))?></td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.className' , array('class'=>'lbl' , 'readonly' => 'readonly'))?>
                    <?php echo $this->form->input('MstReceipts.classId' , array('type'=>'hidden' ))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.facilityName' , array('class'=>'lbl' , 'readonly' => 'readonly'))?>
                    <?php echo $this->form->input('MstReceipts.facilityId' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.recital.0' , array('class'=>'lbl' , 'readonly'=>'readonly')) ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.departmentName' , array('class'=>'lbl' , 'readonly' => 'readonly'))?>
                    <?php echo $this->form->input('MstReceipts.departmentId' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>バーコード</th>
                <td>
                    <input type="text" class="txt15 r" name="codeinput" id="codeinput" onkeyDown="return editTextArea(event);" />
                    <input type="button" value="" class="btn btn14" id="claerBtn" /><span id="err_msg" style="margin-left : 10px;" class="RedBold"></span>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <td colspan="2" valign="bottom">物流シール
                    <div style="float:left;"><ul class="barcodez" id="barcodelist_b" name="barcodelist_b"></ul></div>
                </td>
                <td colspan="2" valign="bottom">部署シール
                    <div style="float:left;"><ul class="barcodez" id="barcodelist_h" name="barcodelist_h"></ul></div>
                </td>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <td><span class="barcodez_count" id="barcode_count_b">読込件数：0件</span></td>
                <td></td>
                <td><span class="barcodez_count" id="barcode_count_h">読込件数：0件</span></td>
                <td></td>
            </tr>
            <tr>
                <th></th>
                <td colspan="4">
                    <input type="button" value="" class="btn btn13" id="allClaerBtn" />
                </td>
            </tr>
       </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" id="confirmBtn" class="btn btn3 submit" value="" /></p>
    </div>
</form>
