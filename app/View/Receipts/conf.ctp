<script>
$(document).ready(function(){
    $('#codeinput').focus();
    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    //備考一括制御
    $('#recitalbtn').click(function(){
        $('input[type=text].recital').val($('#recital').val());
    });

    $("#btn_Mod").click(function(){
        var cnt = 0;
        var err = 0;
        $("#ShippingsList input[type=checkbox].chk").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
            }
        });
        if(err == 0 ){
            if(cnt != 0){
                $(window).unbind('beforeunload');
                $("#ShippingsList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result/').submit();
            } else {
                alert('必ず一つは選択してください');
            }
        }
    });
    
    
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
});

</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a>入荷処理</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">入荷予定一覧</a></li>
        <li>入荷予定詳細</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入荷予定詳細</span></h2>
<h2 class="HeaddingMid">受け取り（入荷）をした商品に入荷日を指定し、入荷処理を行います。</h2>
<?php echo $this->form->create('MstReceipts',array('class'=>'validate_form','type' => 'post','action' => '' ,'id' =>'ShippingsList')); ?>
    <?php echo $this->form->input('MstReceipts.type' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('Receipts.token', array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>入荷日</th>
                <td>
                    <?php echo $this->form->input('MstReceipts.work_date' , array('class'=>'r date validate[required,custom[date]]'))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="results">
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">表示件数：<?php echo count($result) ?>件</span>

        </div>
        <div class="SelectBikou_Area" id="page_top">
            <span class="DisplaySelect">
            </span>
            <span style='text-align: right;display: inline-block;width:49%;'>
                <?php echo $this->form->input('recital.text' , array('type'=>'text' ,'class'=>'txt' , 'id'=>'recital' , 'maxlength'=>'200')); ?>
                <input type="button" class="common-button-2" id="recitalbtn" value="備考一括入力" />
            </span>
        </div>
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2" id="inputTable">
                <colgroup>
                    <col width="25" />
                    <col width="140" />
                    <col width="80" />
                    <col width="80" />
                    <col />
                    <col width="150" />
                    <col width="120" />
                    <col width="100" />
                </colgroup>
                <thead>
                <tr>
                    <th rowspan="2"><input type="checkbox" checked = 'checked' class='checkAll'/></th>
                    <th>発注番号</th>
                    <th>発注日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th colspan="2">部署</th>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>JANコード</th>
                    <th>備考</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $cnt=0;
                    foreach ($result as $data) {
                ?>
                <tr>
                    <td class="center" rowspan="2">
                    <?php
                            echo $this->form->input('MstReceipts.id.'.$cnt , array('type'=>'checkbox' , 'value'=>$data['TrnShipping']['id'] , 'class'=>'chk center' ,'hiddenField'=>false , 'checked'=>true));
                            echo $this->form->input('MstReceipts.modified.'.$data['TrnShipping']['id'] , array('type'=>'hidden' , 'value'=>$data['TrnShipping']['modified']));
                    ?>
                    </td>
                    <td><?php echo h_out($data['TrnShipping']['work_no'],'center'); ?></td>
                    <td><?php echo h_out($data['TrnShipping']['work_date'],'center') ; ?></td>
                    <td><?php echo h_out($data['TrnShipping']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($data['TrnShipping']['item_name']); ?></td>
                    <td><?php echo h_out($data['TrnShipping']['item_code']); ?></td>
                    <td><?php echo h_out($data['TrnShipping']['unit_name']); ?></td>
                    <td><?php echo h_out($data['TrnShipping']['user_name']); ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($data['TrnShipping']['department_name']); ?></td>
                    <td></td>
                    <td><?php echo h_out($data['TrnShipping']['standard']); ?></td>
                    <td><?php echo h_out($data['TrnShipping']['dealer_name']); ?></td>
                    <td><?php echo h_out($data['TrnShipping']['jan_code'])?></td>
                    <td>
                    <?php if($data['TrnShipping']['sales_price'] != ''){ ?>
                        <?php echo $this->form->input('MstReceipts.recital.'.$data['TrnShipping']['id'] , array('class'=>'txt recital' , 'style'=>'width:90%;','maxlength'=>'200'))?>
                    <?php } else { ?>
                        売上設定がありませんでした
                    <?php } ?>
                    </td>
                </tr>
                <?php $cnt++;} ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
    </div>
    <div class="ButtonBox" id="page_footer">
        <input type="button" class="common-button" id="btn_back" value="戻る"/>
        <input type="button" class="common-button [p2]" id="btn_Mod" value="入荷処理"/>
    </div>
</form>
</div><!--#content-wrapper-->
