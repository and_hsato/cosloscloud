<script type="text/javascript">
    $(document).ready(function(){
        $('#noStockBtn').click(function(){
            $('#ShippingsList').attr('action', '<?php echo $this->webroot ?><?php echo $this->name; ?>/index#search_result').submit();
        });
        $('input.lbl').attr('readonly', 'readonly');

        //部署シール印刷ボタン押下
        $("#btn_hospital_sticker_print").click(function(){
            $("#ShippingsList").attr('action', '<?php echo $this->webroot; ?>PdfReport/sticker').attr('target','_blank').submit();
            $("#ShippingsList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result').attr('target' , '_self');
        });
    });
    
</script>
<div id="content-wrapper">
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home" style="">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/">入荷処理</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">入荷予定一覧</a></li>
       <li class="pankuzu">入荷予定詳細</li>
        <li>入荷処理完了</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入荷処理完了</span></h2>
<h2 class="HeaddingMid">入荷処理が完了しました</h2>
<form class="validate_form" method="post" action="" accept-charset="utf-8" id="ShippingsList">
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <?php echo $this->form->input('User.user_id' , array('type'=>'hidden' , 'value' => $this->Session->read('Auth.MstUser.id')))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>入荷日</th>
                <td class="border-none">
                    <?php echo $this->form->input('MstReceipts.work_date' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="results">
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2">
                <colgroup>
                    <col width="25" />
                    <col width="140" />
                    <col width="80" />
                    <col width="80" />
                    <col />
                    <col width="150" />
                    <col width="120" />
                    <col width="100" />
                </colgroup>

                <thead>
                <tr>
                    <th rowspan="2"></th>
                    <th>発注番号</th>
                    <th>発注日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th colspan="2">部署</th>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>JANコード</th>
                    <th>備考</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($this->request->data['result'] as $i => $item) { ?>
                <tr>
                    <td rowspan="2"></td>
                    <td><?php echo h_out($item['MstReceipts']['work_no']); ?></td>
                    <td><?php echo h_out($item['MstReceipts']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($item['MstReceipts']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($item['MstReceipts']['item_name']); ?></td>
                    <td><?php echo h_out($item['MstReceipts']['item_code']); ?></td>
                    <td><?php echo h_out($item['MstReceipts']['unit_name']); ?></td>
                    <td><?php echo h_out($item['MstReceipts']['user_name']); ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($item['MstReceipts']['name']); ?></td>
                    <td></td>
                    <td><?php echo h_out($item['MstReceipts']['standard']); ?></td>
                    <td><?php echo h_out($item['MstReceipts']['dealer_name']); ?></td>
                    <td><?php echo h_out($item['MstReceipts']['jan_code']); ?></td>
                    <td>
                        <input type="text"  class="tbl_lbl" value="<?php echo h($this->request->data['MstReceipts']['recital'][$item['MstReceipts']['id']]); ?>" id="bikou" readonly="readonly" />
                        <?php echo $this->form->input('TrnOrderHeader.id.'.$i ,array('type'=>'hidden' , 'value'=>$item['MstReceipts']['trn_order_header_id'])); ?>
                        <?php echo $this->form->input('MstReceipts.sticker_id.'.$i ,array('type'=>'hidden' , 'value'=>$item['MstReceipts']['sticker_id'])); ?>
                    </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
        $mstUser = $this->Session->read('Auth.MstUser');
        $editionId = null;
        if (empty($mstUser['parent_id'])) {
            $mstRole = $this->Session->read('Auth.MstRole');
            $editionId = $mstRole['mst_edition_id'];
        } else {
            $parent = $this->Session->read('Auth.Parent');
            $editionId = $parent['MstRole']['mst_edition_id'];
        }
        $isStdEdition = Configure::read('edition.std') === $editionId;
    ?>
    <div class="ButtonBox">
        <?php echo $this->form->input('search.is_search',array('type'=>'hidden'))?>
        <input type="button" class="common-button fix_btn" id="noStockBtn" value="続けて入荷処理" >
        <?php if (!$isStdEdition) echo '<input type="button" class="common-button" id="btn_hospital_sticker_print" value="部署シール印刷"/>' ?>
    </div>
</form>
</div><!--#content-wrapper-->

