<script>
$(document).ready(function(){
    $('table.TableStyle01 > tbody > tr:odd').addClass('odd');
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist">EDIユーザ一覧</a></li>
        <li class="pankuzu">EDIユーザ編集</li>
        <li>EDIユーザ編集結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">EDIユーザ編集結果</h2>

<?php if(isset($this->request->data['MstUser']['is_deleted'])){ ?>
<div class="Mes01">データを削除しました</div>
<?php }else{ ?>
<div class="Mes01">以下の内容で設定しました</div>
<?php } ?>

<form method="post" id="">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>ユーザID</th>
                <td>
                    <?php echo $this->form->input('MstUser.login_id' , array('type'=>'text' , 'class'=>'lbl' ,'readonly'=>'readonly')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>ユーザ名</th>
                <td>
                    <?php echo $this->form->input('MstUser.user_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>所属施設デフォルト設定</th>
                <td>
                    <?php echo $this->form->input('MstFacility.facility_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>ロール</th>
                <td>
                    <?php echo $this->form->input('MstRole.role_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</form>
