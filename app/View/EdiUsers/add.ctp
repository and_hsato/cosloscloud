<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //チェックOKだったら、送信する
        validateDetachSubmit($('#AddForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result/' );
    }
}

$(document).ready(function(){
    <?php if(Configure::read('Password.Security') == 1 ) { ?>
    $('#temp_password').addClass('r validate[required,custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]');
    $('#temp_password2').addClass('r validate[required,custom[checkPasswordText],maxSize[8],minSize[8],custom[checkPasswordRule1],custom[checkPasswordRule2]]');
    <?php }else{ ?>
    $('#temp_password').addClass('r validate[required]');
    $('#temp_password2').addClass('r validate[required,equals[temp_password]]');
    <?php } ?>
    $("#AddForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
    $('table.TableStyle01 > tbody > tr:odd').addClass('odd');
    //チェックボックス一括変更
    $("#allCheck").click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    //プルダウンを変更したら施設コードを自動入力
    $("#facility_selected").change(function () {
        // set facility name selected.
        $('#user_facility_txt').val($('#facility_selected option:selected').val());
        $('#user_facility_name').val($('#facility_selected option:selected').text());
        $('#select_facility_code').val($('#facility_selected option:selected').val());
    });

    //施設コードを入力したら、施設のプルダウンを変更
    $('#user_facility_txt').change(function(){
        $('#facility_selected').selectOptions($(this).val());
        $('#user_facility_name').val($('#facility_selected option:selected').text());
        $('#facility_selected').select();
        $('#select_facility_code').val($('#facility_selected option:selected').val());
    });
    //新規なので仮パスワード発行フラグは強制ON
    $('#is_temp_issue').change(function(){
        $(this).attr('checked' , 'checked');
    });


});

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/userlist">EDIユーザ一覧</a></li>
        <li>EDIユーザ編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">EDIユーザ編集</h2>

<form class="search_form"  action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckUserId" id="AddForm">
    <input type="hidden" name="mode" value="add" />
    <?php echo $this->form->input('MstUser.is_update' , array('type'=>'hidden' , 'value'=>'1'));?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <div style="float:left;">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>ユーザID</th>
                    <td><?php echo $this->form->input('MstUser.login_id' , array('type'=>'text' , 'class'=>'validate[required,ajax[ajaxUserId]] search_canna r' , 'maxlength'=>20))?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>ユーザ名</th>
                    <td><?php echo $this->form->input('MstUser.user_name' , array('type'=>'text' , 'class'=>'r validate[required] search_canna' , 'maxlength'=>100))?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>所属施設デフォルト設定</th>
                    <td>
                         <?php echo $this->form->input('MstFacility.select_facility_code' , array('id'=>'select_facility_code' , 'type'=>'hidden'))?>
                         <?php echo $this->form->input('MstFacility.facility_name' , array('id'=>'user_facility_name' , 'type'=>'hidden'))?>
                         <?php echo $this->form->input('MstFacility.facility_code',array('id' => 'user_facility_txt', 'label' => false, 'div' => false, 'class' => 'r', 'style'=>'width: 60px;')); ?>
                         <?php echo $this->form->input('MstFacility.facility_code',array('options'=>$facilities_enabled,'id' => 'facility_selected', 'class' => 'r validate[required]' , 'empty'=>'')); ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>ロール</th>
                    <td><?php echo $this->form->input('MstUser.mst_role_id' , array('options'=>$Role_List , 'class'=>'r txt validate[required]' , 'empty'=>'選択してください'))?></td>
                    <td></td>
                </tr>
                <?php if(Configure::read('Password.Security') == 1 ){ ?>
                <tr>
                    <th>仮発行フラグ</th>
                    <td>
                        <?php echo $this->form->input('MstUser.is_temp_issue_dummy' , array('type'=>'checkbox' ,'hiddenField'=>false ,'checked'=>'checked' , 'disabled'=>'disabled'))?>
                        <?php echo $this->form->input('MstUser.is_temp_issue' , array('type'=>'hidden'))?>
                        <input type="button" value="仮パスワード再設定" id="password_btn" style="width:150px;" disabled="disabled">
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <th><?php if(Configure::read('Password.Security') == 1 ){ ?>仮<?php } ?>パスワード</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password' , array('type'=>'password' , 'class'=>'' , 'id'=>'temp_password'))?>
                    </td>
                </tr>
                <tr>
                    <th><?php if(Configure::read('Password.Security') == 1 ){ ?>仮<?php } ?>パスワード(確認用)</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password2' , array('type'=>'password' , 'class'=>'' , 'id'=>'temp_password2'))?>
                    </td>
                </tr>
            </table>
        </div>
        <?php if(Configure::read('Password.Security') == 1 ){ ?>
        <div style="border: 1px dotted black; width:400px;margin: 5px;float:left;margin-top:100px;">
            <p style="text-align:left;">【更新パスワード入力ルール】</p>
            <ol style="list-style-type: decimal !important;margin-left:25px;">
                <li style="list-style-type: decimal !important; text-align:left;">半角数字、半角英小文字、_（アンダーバー）、-（ハイフン）が使用出来ます。ただし<span class="ColumnRed">半角数字の0（ゼロ）、1（イチ）および  半角英小文字のo（オー）、l（エル）</span>は使用禁止です。</li>
                <li style="list-style-type: decimal !important; text-align:left;">パスワードは8文字とし、半角数字と半角英小文字が  それぞれ１文字以上含まれる値を設定してください。</li>
                <li style="list-style-type: decimal !important; text-align:left;">現在のパスワードとは異なる値を設定してください。</li>
            </ol>
        </div>
        <?php } ?>
    </div>
    <div class="ButtonBox">
        <input type="submit" class="btn btn2 [p2]" value="" />
    </div>
</form>
