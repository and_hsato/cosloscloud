<script>
$(document).ready(function() {

    //チェックボックス一括制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    //明細ボタン押下
    $("#btn_Conf").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#RepaymentList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/conf').submit();
        } else {
           alert('必ず一つは選択してください');
        }
    });

    //検索ボタン押下
    $("#btn_Search").click(function(){
        $("#RepaymentList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/index').submit();
    });
    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $("#RepaymentList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        $("#RepaymentList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/index');
    });

    //納品書印刷ボタン押下
    $("#btn_Print_Invoice").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            tmp = confirm('納品書印刷を行ってよろしいでしょうか？');
            if(tmp == true){
                $("#RepaymentList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/invoice').submit();
            }
        } else {
            alert('必ず一つは選択してください');
        }
    });

    //売上依頼表印刷ボタン押下
    $("#btn_Print_Requestsales").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            tmp = confirm('売上依頼票印刷を行ってよろしいでしょうか？');
            if(tmp == true){
                $("#RepaymentList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/requestsales').submit();
            }
        } else {
            alert('必ず一つは選択してください');
        }
   });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>返却履歴</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>返却履歴</span></h2>
<h2 class="HeaddingMid">返却履歴を検索してください。</h2>
<form class="validate_form search_form input_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index" id="RepaymentList">
    <?php echo $this->form->input('search.is_search', array('type'=>'hidden'))?>
    <?php echo $this->form->input('repay_user_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.MstUser.id')))?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>返却番号</th>
                <td><?php echo $this->form->input('MstRepayment.search_work_no' , array('class'=>'txt'))?></td>
                <td></td>
                <th>返却日</th>
                <td>
                    <?php echo $this->form->input('MstRepayment.search_start_date' , array('type'=>'text' , 'class'=>'validate[optional,custom[date],funcCall2[date2]] date' , 'id'=>'datepicker1'))?>
                     ～
                    <?php echo $this->form->input('MstRepayment.search_end_date' , array('type'=>'text' , 'class'=>'validate[optional,custom[date],funcCall2[date2]] date' , 'id'=>'datepicker2'))?>
                </td>
                <td></td>
                <th></th>
                <td>
                    <?php echo $this->form->checkbox('MstRepayment.search_display_deleted',array('hiddenField'=>false)); ?>取消は表示しない
                </td>
                <td></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('MstRepayment.facilityText',array('class'=>'txt require','style'=>'width:60px;','id'=>'facilityText')); ?>
                    <?php echo $this->form->input('MstRepayment.facilityCode',array('options'=>$facility_List,'class'=>'txt','style'=>'width:120px;','id'=>'facilityCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('MstRepayment.facilityName',array('type'=>'hidden','id'=>'facilityName')); ?>
                </td>
                <td></td>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('MstRepayment.departmentText',array('class'=>'txt','style'=>'width:60px;','id'=>'departmentText')); ?>
                    <?php echo $this->form->input('MstRepayment.departmentCode',array('options'=>$department_List, 'class'=>'txt','style'=>'width:120px;','id'=>'departmentCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('MstRepayment.departmentName',array('type'=>'hidden','id'=>'departmentName')); ?>
                </td>
                <td></td>
                <th>返却種別</th>
                <td>
                    <?php echo $this->form->input('MstRepayment.search_repaymentType' , array('options'=>array(1=>'通常品' , 2=>'預託品' ) , 'class' => 'txt' , 'empty'=>true))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstRepayment.search_internal_code' , array('class'=>'txt search_canna search_internal_code')) ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstRepayment.search_item_code' , array('class'=>'txt search_upper search_canna')) ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->input('MstRepayment.search_lot_no' , array('class'=>'txt search_canna') )?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstRepayment.search_item_name' , array('class'=>'txt search_canna'));?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstRepayment.search_dealer_name' , array('class'=>'txt search_canna'));?></td>
                <td></td>
                <th>管理区分</th>
                <td>
                    <?php echo $this->form->input('MstRepayment.search_type',array('options'=> Configure::read('Classes') ,'id' => 'MstRepaymentType', 'class' => 'txt' , 'empty'=>true)); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('MstRepayment.search_standard' , array('class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->input('MstRepayment.search_sticker_no' , array('class'=>'txt search_canna'))?></td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('MstRepayment.search_classId' , array('options'=>$class_list , 'class'=>'txt' , 'empty'=>true))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>&nbsp;&nbsp;
        <input type="button" class="btn btn5" id="btn_Csv"/>
    </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($ReturnList))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <colgroup>
                    <col width="25" />
                    <col width="150" />
                    <col width="70" />
                    <col width="100" />
                    <col />
                    <col />
                    <col width="140" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th><input type="checkbox" class="checkAll" /></th>
                    <th>返却番号</th>
                    <th>種別</th>
                    <th>返却日</th>
                    <th>施設</th>
                    <th>登録者</th>
                    <th>登録日時</th>
                    <th>備考</th>
                    <th>件数</th>
                </tr>
                <?php
                    $cnt=0;
                    foreach($ReturnList as $r) {
                ?>
                <tr>
                    <td class="center">
                        <input type="checkbox" class="chk" id="check<?php echo $cnt; ?>" class="center chk" name="data[TrnRepay][TrnRepayNo][]" value="<?php echo $r['TrnRepayHeader']['id']; ?>"/>
                    </td>
                    <td><?php echo h_out($r['TrnRepayHeader']['work_no']); ?></td>
                    <td><?php echo h_out(Configure::read('Repay.'.$r['TrnRepayHeader']['repay_type']),'center'); ?></td>
                    <td><?php echo h_out($r['TrnRepayHeader']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($r['MstFacility']['facility_name']); ?></td>
                    <td><?php echo h_out($r['MstUser']['user_name']); ?></td>
                    <td><?php echo h_out($r['TrnRepayHeader']['created'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnRepayHeader']['recital']); ?></td>
                    <td><?php echo h_out($r['TrnRepayHeader']['total_count']." ／ ".$r['TrnRepayHeader']['detail_count'] , 'right'); ?></td>
                </tr>
                <?php $cnt++; } ?>
                <?php if(isset($this->request->data['search']['is_search']) && count($ReturnList) == 0){ ?>
                <tr>
                    <td colspan="9" class="center">該当するデータがありませんでした</td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
<?php if($cnt != 0){ ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn7" id="btn_Conf"/>
        <input type="button" class="btn btn19" id="btn_Print_Invoice" name="btn_Print_Invoice"/>
        <input type="button" class="btn btn20" id="btn_Print_Requestsales" name="btn_Print_Requestsales" />
    </div>
<?php } ?>
</form>

