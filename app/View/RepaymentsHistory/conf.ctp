<script>
$(document).ready(function(){
    $('#chkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
    });
  
    $("#btn_Conf").click(function(){
        if($('input[type=checkbox].chk:checked').length === 0){
            alert('必ず一つは選択してください');
        }else{
            tmp = confirm('返却取消を行ってよろしいでしょうか？');
            if(tmp == true){
                $("#RepaymentList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result');
                $("#RepaymentList").submit();
            }
        }
    });

    $("#bikou_btn").click(function(){
        $(".bikou").val($("#bikou_txt").val());
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">返却履歴</a></li>
        <li>返却履歴編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>返却履歴編集</span></h2>
<h2 class="HeaddingMid">編集する返却履歴を選択してください。</h2>
<form class="validate_form" method="post" action="" accept-charset="utf-8" id="RepaymentList">
    <div class="results">
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
            <span class="DisplaySelect">
            　表示件数：<?php echo count($ReturnList); ?>件
            </span>
            <span class="BikouCopy">
                <input type="text" class="txt" name="" id="bikou_txt" value="" />
                <input type="button" id="bikou_btn" class="btn btn8 [p2]" value="" />
            </span>
        </div>

        <div class="TableScroll2" id="containTables">
            <table class="TableStyle01 table-odd2" style="margin:0;padding:0;">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col width="80"  />
                    <col width="80"  />
                    <col />
                    <col />
                    <col width="85" />
                    <col />
                    <col />
                    <col width="65" />
                    <col width="65" />
                </colgroup>
                <tr>
                    <th rowspan="2"><input type="checkbox" checked id="chkAll"/></th>
                    <th>返却番号</th>
                    <th>返却日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>ロット番号</th>
                    <th>センターシール</th>
                    <th>作業区分</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th colspan="2">仕入先</th>
                    <th>管理区分</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                    <th>有効期限</th>
                    <th>部署シール</th>
                    <th colspan="2">備考</th>
                </tr>
                <?php $cnt=0; foreach($ReturnList as $r ){ ?>
                <tr>
                    <td rowspan="2" class="center">
                    <!-- 締め済み・取消済みでチェックボックス非表示 -->
                    <?php if($r['TrnReceiving']['status'] == ""){ ?>
                        <input type="checkbox" checked class="center chk" name="data[MstRepayment][MstRepaymentNo][]" value="<?php echo $r['TrnReceiving']['id']; ?>" />
                    <?php  } ?>
                    </td>
                    <td><?php echo h_out($r['TrnRepayHeader']['work_no']); ?></td>
                    <td><?php echo h_out($r['TrnRepayHeader']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_code']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['unit_name']); ?></td>
                    <td><?php echo h_out($r['TrnSticker']['lot_no']); ?></td>
                    <td><?php echo h_out($r['TrnSticker']['facility_sticker_no']); ?></td>
                    <td><?php echo h_out($r['TrnReceiving']['class_name']); ?></td>
                    <td><?php echo h_out($r['MstUser']['user_name']); ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($r['OrderFacility']['facility_name']); ?></td>
                    <td><?php echo h_out(Configure::read('Classes.'.$r['TrnSticker']['trade_type']),'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($r['MstDealer']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['TrnReceiving']['quantity'],'right'); ?></td>
                    <td><?php echo h_out($r['TrnSticker']['validated_date'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnSticker']['hospital_sticker_no']); ?></td>
                    <td colspan="2">
                    <?php
                        if($r['TrnReceiving']['status'] == ""){ ?>
                        <input type="text" class="tbl_txt bikou" name="data[MstRepayment][<?php echo $r['TrnReceiving']['id']; ?>][recital]" value="<?php echo $r['TrnReceiving']['recital']; ?>" />
                    <?php } else{ ?>
                        <?php echo h_out($r['TrnReceiving']['status']); ?>
                    <?php } ?>
                        <input type="hidden" name="data[MstRepayment][<?php echo $r['TrnReceiving']['id']; ?>][is_lowlevel]" value="<?php echo $r['MstFacilityItem']['is_lowlevel']; ?>" />
                    </td>
                </tr>
                <?php $cnt++; ?>
                <?php } ?>
            </table>
            <input type="hidden" name="data[MstRepayment][repay_header]" id="" value="">
        </div>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
        <input type="button" class="btn btn6 [p2]" id="btn_Conf" name="btn_Conf" />
    </div>
</form>

