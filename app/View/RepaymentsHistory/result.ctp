<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">返却履歴</a></li>
        <li class="pankuzu">返却履歴編集</li>
        <li>返却履歴結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>返却履歴結果</span></h2>
<div class="Mes01">以下の内容で更新しました。</div>
<form method="post" action="" accept-charset="utf-8" id="RepaymentList">
    <div class="results">
        <div class="SelectBikou_Area" id="page_top">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="TableScroll2" id="containTables">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col width="85" />
                    <col />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>返却番号</th>
                    <th>返却日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>ロット番号</th>
                    <th>センターシール</th>
                    <th>作業区分</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th colspan="2">施設 ／ 部署</th>
                    <th>管理区分</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                    <th>有効期限</th>
                    <th>部署シール</th>
                    <th colspan="2">備考</th>
                </tr>
                <?php
                    $cnt=0;
                    foreach($ReturnList as $r){
                ?>

                <tr>
                    <td><?php echo h_out($r['TrnRepayHeader']['work_no']); ?></td>
                    <td><?php echo h_out($r['TrnRepayHeader']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_code']); ?></td>
                    <td><?php echo h_out($r['MstItemUnit']['unit_name']); ?></td>
                    <td><?php echo h_out($r['TrnSticker']['lot_no']); ?></td>
                    <td><?php echo h_out($r['TrnSticker']['facility_sticker_no']); ?></td>
                    <td><?php echo h_out($r['TrnReceiving']['class_name']); ?></td>
                    <td><?php echo h_out($r['MstUser']['user_name']); ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($r['OrderFacility']['facility_name']); ?></td>
                    <td><?php echo h_out(Configure::read('Classes.'.$r['TrnSticker']['trade_type']),'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($r['MstDealer']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['TrnReceiving']['quantity'],'right'); ?></td>
                    <td><?php echo h_out($r['TrnSticker']['validated_date'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnSticker']['hospital_sticker_no']); ?></td>
                    <td colspan="2">
                        <input readonly="readonly" type="" class="tbl_lbl" name="recital"  value="<?php echo $r['TrnReceiving']['recital']; ?>" />
                    </td>
                </tr>
                <?php $cnt++; } ?>
            </table>
        </div>
    </div>
    <div class="SelectBikou_Area" id="page_footer">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
</form>
