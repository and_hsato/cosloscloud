<script type="text/javascript">

$(function(){
    // 出荷リスト印字ボタン押下
    $("#shipping_print_btn").click(function(){
        if(confirm("出荷リスト印刷を実行します。よろしいですか？")){
            $("#result_form").attr('action', '<?php echo $this->webroot; ?>Shippings/report/makeShipping').submit();
        }
        return;
    });

    // 納品書印刷ボタン押下
    $("#invoice_print_btn").click(function(){
        if(confirm("納品書印刷を実行します。よろしいですか？")){
            $("#result_form").attr('action', '<?php echo $this->webroot; ?>Shippings/report/makeInvoice').submit();
        }
        return;
    });
  
    // 部署シール印字ボタン押下
    $("#hospital_sticker_print_btn").click(function(){
        if(confirm("部署シール印刷を実行します。よろしいですか？")){
            $("#result_form").attr('action', '<?php echo $this->webroot; ?>Shippings/seal/print_hospital_sticker').submit();
        }
        return;
    });
    
    // コストシール印字ボタン押下
    $("#cost_sticker_print_btn").click(function(){
        if(confirm("コストシール印刷を実行します。よろしいですか？")){
            $("#result_form").attr('action', '<?php echo $this->webroot; ?>Shippings/seal/print_cost_sticker').submit();
        }
        return;
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/ex_add">臨時消費登録</a></li>
        <li class="pankuzu"><?php
      if($input_type == 'INPUT_STICKER'){
        echo 'シール読込';
      } else {
        echo 'CSV読込';
      }
    ?>
        </li>
        <li class="pankuzu">臨時消費確認</li>
        <li>臨時消費結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>臨時消費結果</span></h2>
<div class="Mes01">臨時消費を行いました</div>

<form class="validate_form" id="result_form" method="post">
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>  
    <?php echo $this->form->input('TrnShipping.trn_shipping_header_id.0' , array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>消費日</th>
                <td><?php echo $this->form->input('TrnShippingHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.classId', array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.className', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
            <tr>
                <th>消費施設</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.facilityCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnShippingHeader.recital', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?></td>
            </tr>
            <tr>
                <th>消費部署</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.departmentCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
        <tr>
            <td>表示件数：<?php echo count($result); ?>件</td>
            <td align="right"></td>
        </tr>
    </table>
    <br>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th rowspan="2" width="20"></th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col15">部署シール</th>
                <th class="col15">作業区分</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>販売単価</th>
                <th>有効期限</th>
                <th>センターシール</th>
                <th>備考</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even2">
            <?php foreach($result as $i=>$r){ ?>
            <tr>
                <td width="20" rowspan="2" class="center">
                    <?php if($r['TrnShipping']['is_check']){ ?>
                    <?php echo $this->form->checkbox('TrnShipping.id.'.$i , array('value'=>$r['TrnShipping']['id'] , 'class' => 'chk id' , 'checked'=>'checked' , 'hiddenField'=>false) ); ?>
                    <?php } ?>
                </td>
                <td class="col10" align="center"><?php echo h_out($r['TrnShipping']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($r['TrnShipping']['item_name']); ?></td>
                <td><?php echo h_out($r['TrnShipping']['item_code']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnShipping']['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnShipping']['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnShipping']['hospital_sticker_no']); ?></td>
                <td class="col15">
                    <?php echo $this->form->input('TrnShipping.classId.'.$r['TrnShipping']['id'],array('options'=>$class_list,'id'=>'classId', 'class'=>'txt','empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($r['TrnShipping']['standard']); ?></td>
                <td><?php echo h_out($r['TrnShipping']['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($r['TrnShipping']['sales_price']),'right'); ?></td>
                <td><?php echo h_out($r['TrnShipping']['validated_date'],'center'); ?></td>
                <td><?php echo h_out($r['TrnShipping']['facility_sticker_no']); ?></td>
                <td>
                    <?php echo $this->form->input('TrnShipping.recital.'.$r['TrnShipping']['id'] , array('type'=>'text','class'=>'lbl','readonly'=>'readonly')); ?>
                </td>
            </tr>
         <?php } ?>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn27 submit print_btn" id="shipping_print_btn" />
            <input type="button" class="btn btn19 submit print_btn" id="invoice_print_btn" />
            <!--<input type="button" class="btn btn16 submit print_btn" id="hospital_sticker_print_btn" />-->
            <!--<input type="button" class="btn btn12 submit print_btn" id="cost_sticker_print_btn" />-->
        </p>
    </div>
</form>
