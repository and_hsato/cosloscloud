<script>
$(document).ready(function(){  
    $(function(){
        $("#update").click(function(){
            var tmpid = '';
            $(".fav_menu input[type=hidden]").each(function(){
                // 親要素が非表示だったら処理しない
                if($(this).parent().is(':visible')){
                    if(tmpid === ''){tmpid = $(this).val();
                    }else{
                        tmpid = tmpid + ','+$(this).val();
                    }
                }
            });
            $('#tmpid').val(tmpid);
            $("#MenuList").attr('action', '<?php echo $this->webroot; ?>mst_favorite_menu/menu_list/update');
            $("#MenuList").submit();
        });
    });

    $(".all_menu li").draggable({
        //特定要素にドラッグ&ドロップさせる系
        connectToSortable: ".fav_menu",
        containment: "d_d_sample",
        scope: "d_d",
        
        //特定要素にだけドラッグ&ドロップさせるときのオプション
        helper: "clone",
        revert: "invalid",
        revertDuration: 500,
        
        //グループ化・重ね順系
        addClasses: true,
        stack: "false",
        zIndex: 100,
        
        //ドラッグの見た目系
        cursor: "crosshair",
        opacity: 0.7,
        stop: function(evt, ui){
            $(".fav_menu li span").click(function(){
                $(this).parent().animate({
                    width: "hide"
                });
            });
        }
    });
  
    $(".fav_menu li span").click(function(){
        $(this).parent().animate({
            width: "hide"
        });
    });
  
    $(".fav_menu").sortable({
        placeholder: "drop_place"
    });
    $(".fav_menu").droppable({
        scope: "d_d",
        activeClass: "all_menu_active" 
    });
    $(".fav_menu p").draggable({
        scope: "d_d",
        activeClass: "all_menu_active_p" 
    });

    // 横スクロール処理
    //コンテンツの横サイズ
    var cont = $('#contents');
    var contW = $('.section').outerWidth(true) * $('div',cont ).length;
    cont.css('width', contW);
    //スクロールスピード
    var speed = 50;
    //マウスホイールで横移動
    $('html').mousewheel(function(event, mov) {
        //ie firefox
        $(this).scrollLeft($(this).scrollLeft() - mov * speed);
        //webkit
        $('body').scrollLeft($('body').scrollLeft() - mov * speed);
        return false;   //縦スクロール不可
    });


    $('#fav_table').draggable({
        opacity  : 0.5,    //ドラッグ時の不透明度
        cursor   : 'move'  //カーソル形状
    });
});
  
</script>
<style>
.all_menu {
   width: 240px;
    margin: 0;
    left: 10px;
    top: 50px;
}
  
.all_menu:hover , .fav_menu:hover{
      cursor: move;
}


.fav_menu {
    width: 240px;
    height: 300px;
    padding-top:10px;
    margin: 0;
    border: 1px solid #aaa;
    background: rgba(255,255,255,.1);
    border-radius: 3px;
}

.fav_menu p {
    width: 240px;
    height: 240px;
    margin: 0;
    padding: 0;
    font-size: 28px;
    font-weight: bold;
    color: #ccc;
    text-align: center;
    line-height: 240px;
    white-space: nowrap;
    position: absolute;
    left: 0;
    top: 0;
    z-index: -1;
}
  
.all_menu li, .fav_menu li {
    display: block;
    width: 180px;
    height: 30px;
    margin: 5px;
    padding: 0 15px;
    font-size: 16px;
    line-height: 30px;
    color: #fff;
    text-shadow: 1px 1px 2px rgba(0,0,0,.5);
    border-radius: 2px;
    position: relative;
    z-index: 10;
    
    background: #ff7072; /* Old browsers */
    background: -moz-linear-gradient(top,  #ff7072 0%, #a04041 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ff7072), color-stop(100%,#a04041)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  #ff7072 0%,#a04041 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  #ff7072 0%,#a04041 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  #ff7072 0%,#a04041 100%); /* IE10+ */
    background: linear-gradient(to bottom,  #ff7072 0%,#a04041 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff7072', endColorstr='#a04041',GradientType=0 ); /* IE6-9 */
}
    
.all_menu li:before,  .fav_menu li:before {
    content: " ";
}

.all_menu li span {
    display: none;
}

.all_menu li.disable {
    margin: 5px;
    background: #2c7e77; /* Old browsers */
    background: -moz-linear-gradient(top,  #2c7e77 0%, #007e6d 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#2c7e77), color-stop(100%,#007e6d)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  #2c7e77 0%,#007e6d 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  #2c7e77 0%,#007e6d 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  #2c7e77 0%,#007e6d 100%); /* IE10+ */
    background: linear-gradient(to bottom,  #2c7e77 0%,#007e6d 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2c7e77', endColorstr='#007e6d',GradientType=0 ); /* IE6-9 */
}
  

.fav_menu li {
    margin: 5px auto;
    background: #81d3cc; /* Old browsers */
    background: -moz-linear-gradient(top,  #81d3cc 0%, #00d3c2 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#81d3cc), color-stop(100%,#00d3c2)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  #81d3cc 0%,#00d3c2 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  #81d3cc 0%,#00d3c2 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  #81d3cc 0%,#00d3c2 100%); /* IE10+ */
    background: linear-gradient(to bottom,  #81d3cc 0%,#00d3c2 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#81d3cc', endColorstr='#00d3c2',GradientType=0 ); /* IE6-9 */
}

.fav_menu li span {
    display: block;
    width: 20px;
    height: 20px;
    margin: 5px;
    font-size: 12px;
    font-weight: bold;
    color: #fff;
    text-align: center;
    line-height: 20px;
    background: #633;
    border-radius: 100%;
    position: absolute;
    right: 0;
    top: 0;
    cursor: pointer;
}

.fav_menu li span:hover {
    color: #f00;
}

.fav_menu_active {
    background: #dcb6b6;
}
.fav_menu_active p {
    color: #fff;
}

</style>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>お気に入り</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>お気に入り</span></h2>

<form class="validate_form search_form" action='<?php echo $this->webroot; ?>mst_favorite_menu/menu_list/' method="post" id="MenuList">
    <?php echo $this->form->input('Menu.tmpid',array('type'=>'hidden','id' =>'tmpid'));?>
    <div class="results">
        <table>
            <tr>
                <td>
                    <ul class="all_menu">
                        <?php $i=1; foreach( $Menu as $m ){?>
                        <?php if($m[0]['disable']){ ?>
                        <li class="disable">
                        <?php }else{ ?>
                        <li>
                        <?php } ?>
                            <?php echo $m[0]['name']; ?><span>X</span>
                            <?php echo $this->form->input('Menu.id.' . $m[0]['id'] , array('type'=>'hidden' , 'value'=>$m[0]['id'])); ?>
                        </li>
                        <?php if($i%10==0){ ?>
                    </ul>
                </td>
                <td>
                    <ul class="all_menu">
                        <?php } ?>
                        <?php $i++;} ?>
                    </ul>
                </td>

            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" id="update" class="btn btn24 [p2]"/>
    </div>

    <div id="fav_table" class="search_form">
        <table>
            <tr>
                <td>
                    <ul class="fav_menu">
                        <p>
                            <?php foreach( $Favorite as $f ){ ?>
                            <li>
                            <?php echo $f[0]['name']; ?><span>X</span>
                            <?php echo $this->form->input('Fav.id.' . $f[0]['id'] , array('type'=>'hidden' , 'value'=>$f[0]['id'])); ?>
                            </li>
                            <?php } ?>
                        </p>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
    
</form>
