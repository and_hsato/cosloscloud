<script type="text/javascript">
$(document).ready(function(){
    $('#exportReport').click(function(){
        if($('#analyzeSelect').val() == ''){
            alert('出力を行う明細を選択してください');
            return false;
        }else if(!dateCheck($('#dateFrom').val()) && $('#dateFrom').val() != "" ){
            alert('日付開始が不正です。');
            return false;
        }else if(!dateCheck($('#dateTo').val()) && $('#dateTo').val() != ""){
            alert('日付終了が不正です。');
            return false;
        }else if(!Chk_DayNum()){
            return false;
        }else{
            $('#msg').text('出力中です。しばらくお待ちください。');
            $('#reportForm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report').submit();
            $('#exportReport').attr('disabled' , 'disabled');
        }
    });
    $('#dateFrom').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
    $('#dateTo').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});

    //帳票名
    $('#reportSelect').change(function(){
        $('#msg').text('');
        $('#AnalyzeDonorId').val('');
        $('#facility_select').val('');
        $('#department_select').val('');
        $('#department_select').children().remove();
        $('#donor_select').val('');
        $('#dateFrom').val('');
        $('#dateTo').val('');
        $('#dayNum').val('');
        $('#freeWord').val('');
        $('#exportReport').attr('disabled','true');
        $('.conditions').find('tr').hide();
        if($(this).val() != '' && $(this).val() != null){
            $.ajax({
                type: 'POST',
                cache: false,
                url: "<?php echo $this->webroot; ?><?php echo $this->name; ?>/getReportMethod/" + $(this).val(),
                success: function(data, dataType){
                    var obj = $.parseJSON(data);
                    var target = $('.conditions');
                    if(obj.MstReport.donor_selectable == true){
                        target.find('tr.donors').show();
                    }
                    if(obj.MstReport.facility_selectable == true){
                        target.find('tr.facilities').show();
                    }
                    if(obj.MstReport.department_selectable == true){
                        target.find('tr.departments').show();
                    }
                    if(obj.MstReport.dates_selectable == true){
                        target.find('tr.dates').show();
                    }
                    if(obj.MstReport.daynumber_selectable == true){
                        target.find('tr.day_number').show();
                    }
                    if(obj.MstReport.free_word == true){
                        target.find('tr.free_word').show();
                    }
                    $('#exportReport').removeAttr('disabled');
                    $('#msg').text('');
                },
                error: function(){
                    alert('帳票情報の取得に失敗しました');
                }
            });
        }
    });

    $('#facility_select').change(function(){
        var facility = $('#facility_select').val();
        $('#department_select').val('');
        if(facility !== '' && facility !== null){
            $.ajax({
                type: 'POST',
                cache: false,
                url: "<?php echo $this->webroot; ?><?php echo $this->name; ?>/getDepartments/" + $(this).val(),
                success: function(data, dataType){
                    $('#department_select').children().remove();
                    var obj = $.parseJSON(data);
                    $.each(obj, function(idx, o){
                        if(idx === 0 && obj.length > 1){
                            $('<option value=""></option>').appendTo('#department_select');
                        }
                        $('<option></option>').text(o.MstDepartment.department_name).attr('value', o.MstDepartment.id).appendTo('#department_select');
                    });
                    //施設が変わったらボタンの復活 & メッセージの削除
                    $('#exportReport').removeAttr('disabled');
                    $('#msg').text('');
                },
                error: function(){
                    alert('部署データの取得に失敗しました');
                }
            });
        }else{
            $('#department_select').children().remove();
        }
    });

    //日付が変わったらボタンの復活 & メッセージの削除
    $('#dateFrom').change(function(){
        $('#exportReport').removeAttr('disabled');
        $('#msg').text('');
    });
  
    //日付が変わったらボタンの復活 & メッセージの削除
    $('#dateTo').change(function(){
        $('#exportReport').removeAttr('disabled');
        $('#msg').text('');
    });

    //部署が変わったらボタンの復活 & メッセージの削除
    $('#department_select').change(function(){
        $('#exportReport').removeAttr('disabled');
        $('#msg').text('');
    });

    //施主が変わったらボタンの復活 & メッセージの削除
    $('#donor_select').change(function(){
        $('#exportReport').removeAttr('disabled');
        $('#msg').text('');
    });

    //日数が変わったらボタンの復活 & メッセージの削除
    $('#dayNum').change(function(){
        $('#exportReport').removeAttr('disabled');
        $('#msg').text('');
    });
   
    //初期状態
    $('.conditions').find('tr').hide();
    $('#exportReport').attr('disabled','true');
});
/**
 * 日数の入力値チェック
 */
function Chk_DayNum(){
    var numcheck = true;
    var num = $('#dayNum').val();
    if(num !=null && num!=''){
      if(num.match(/[^0-9]/g)){
           alert('日数が不正です');
           $('#dayNum').val('');
           numcheck=false;
       }
  }
  return numcheck;
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>帳票データ出力</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>帳票データ出力</span></h2>
<h2 class="HeaddingMid">出力を行う明細と条件を選択してください</h2>

<div id="msg" class="err" style="font-size:20px"></div>

<form id="reportForm" method="POST" action="<?php echo $this->webroot; ?>report/export">
<input type="hidden" name="data[Report][f]" value="<?php echo $this->Session->read('Auth.facility_id_selected'); ?>">
<input type="hidden" name="data[Report][u]" value="<?php echo $this->Session->read('Auth.MstUser.id'); ?>">
<hr class="Clear" />
<div class="SearchBox">
    <table class="FormStyleTable">
        <tr>
            <th>帳票名</th>
            <td><?php echo $this->form->input('MstReport.id', array('options'=>$items, 'empty'=>true, 'class'=>'txt','id'=>'reportSelect','style'=>'width:400px')); ?></td>
        </tr>
        <tr>
            <th>条件</th>
            <td>
                <div style="width: 400px;height: 180px;border:1px solid #ccc;">
                    <table class="conditions">
                        <tr class="donors">
                            <th>施主</th>
                            <td><?php echo $this->form->input('Report.donor_id', array('options'=>$donors, 'empty'=>true ,'class'=>'txt' , 'id'=>'donor_select')); ?></td>
                        </tr>
                        <tr class="facilities">
                            <th>施設</th>
                            <td><?php echo $this->form->input('Report.facility_id', array('options'=>$facilities, 'empty'=>true , 'class'=>'txt', 'id'=>'facility_select')); ?></td>
                        </tr>
                        <tr class="departments">
                            <th>部署</th>
                            <td><select id="department_select" name="data[Report][department_id]" class="txt"></select></td>
                        </tr>
                        <tr class="dates">
                            <th>日付開始</th>
                            <td><?php echo $this->form->input('Report.day_from', array('type'=>'text','class'=>'date','id'=>'dateFrom')); ?></td>
                        </tr>
                        <tr class="dates">
                            <th>日付終了</th>
                            <td><?php echo $this->form->input('Report.day_to', array('type'=>'text' , 'class'=>'date','id'=>'dateTo')); ?></td>
                        </tr>
                        <tr class="day_number">
                            <th>日数</th>
                            <td><?php echo $this->form->input('Report.day_num', array('class'=>'txt','id'=>'dayNum')); ?></td>
                        </tr>
                        <tr class="free_word">
                            <th>フリーワード</th>
                            <td><?php echo $this->form->input('Report.free_word', array('class'=>'txt','id'=>'freeWord')); ?></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
</form>
<br />
<div class="ButtonBox">
    <p class="center">
        <input type="button" class="btn btn10" id="exportReport" />
    </p>
</div>
