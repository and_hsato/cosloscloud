<script type="text/javascript">
    $(document).ready(function(){
        $('#showList').click(function(){
            if($('#sltTable').val() != '選択してください'){
                $('#tblName').val($('#sltTable').val());
                $('#main_form').submit();
            }
        });

        $('#exportTsv').click(function(){
            if($('#sltTable').val() != '選択してください'){
                $('#tblName').val($('#sltTable').val());
                $('#main_form').attr('action', '<?php echo $this->webroot; ?>AdminUseOnly/exportAsTsv').submit();
            }
        });
        $('#exportXML').click(function(){
            if($('#sltTable').val() != '選択してください'){
                $('#tblName').val($('#sltTable').val());
                $('#main_form').attr('action', '<?php echo $this->webroot; ?>AdminUseOnly/exportAsXML').submit();
            }
        });
        $('#execSQL').click(function(){
            if($('#sql').val() != ''){
                $('#tblName').val($('#sltTable').val());
                $('#main_form').attr('action', '<?php echo $this->webroot; ?>AdminUseOnly/executeSQL').submit();
            }
        });
        $('table.tblHeaderShow').find('th').mouseover(function(){
            $('#columnName').text($(this).text());
        });
        $('table.tblHeaderShow').find('th').mouseout(function(){
            $('#columnName').text('');
        });

    });
</script>


<h1 class="HeaddingLarge">データベース管理</h1>
<form class="validate_form" id="main_form" method="POST" class="" action="<?php echo $this->webroot; ?>AdminUseOnly/show">
<input type="hidden" id="tblName" name="data[tblName]" value="<?php echo $tblName; ?>" />
<hr />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>テーブル</td>
        <td>
            <select id="sltTable">
                <option>選択してください</option>
                <?php foreach ($tables as $t): ?>
                    <option value="<?php echo $t; ?>"><?php echo $t; ?></option>
                <?php endforeach; ?>
            </select>
            <input type="button" id="showList" value="表示" />
            <input type="button" id="exportTsv" value="TSV出力" />
            <input type="button" id="exportXML" value="XML出力" />
        </td>
    </tr>
    <tr>
        <td colspan="2">SQL発行(<span style="color:red;">※入力されたクエリをそのまま実行します！</span>)<br />
            <textarea id="sql" name="data[SQL]" rows="1" cols="1" style="width: 600px;height:200px;"><?php echo (isset($sqlResult) ? $sqlResult : ''); ?></textarea><br />
            <input type="button" id="execSQL" value="SQL発行" />
            <span id="columnName" style="font-size:x-large;"></span>
        </td>
    </tr>
</table>
<hr />
<div>
    <?php echo $this->element('limit_combobox',array('result'=>(isset($records) ? count($records) : 0))); ?>
</div>


<?php if(isset($records)) : ?>
    <?php if(count($records) == 0):?>
    <p>該当なし</p>
    <?php else: ?>

    <div class="TableHeaderAdjustment01">
    <table class="TableHeaderStyle02 tblHeaderShow">
      <tr>
        <?php foreach(array_keys($records[0]) as $_header): ?>
        <th class=""><?php echo $_header; ?></th>
        <?php endforeach; ?>
      </tr>
    </table>
    </div>

    <div class="TableScroll">
        <table class="TableStyle01">
        <?php $i=0;foreach($records as $_row): ?>
            <tr class="<?php echo($i%2===0?'':'odd'); ?>">
                <?php foreach(array_keys($records[0]) as $_header): ?>
                    <td><?php echo h_out($_row[$_header]); ?></td>
                <?php endforeach ?>
            </tr>
        <?php $i++;endforeach;?>
        </table>
    </div>
    <?php endif; ?>
<?php endif; ?>
</form>