<script type="text/javascript">

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#facilityItemForm #searchForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#facilityItemForm #cartForm input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
    });

    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );
  
    //検索ボタン押下
    $("#btn_Search").click(function(){
        var tmpid = '';
        $("#facilityItemForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);

        $("#facilityItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add').submit();
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        $('#itemSelectedTable').empty();
        $("#facilityItemForm #searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
                cnt++;
            }
        });
        if(cnt === 0 ){
            alert('商品を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        //選択チェック
        var cnt = 0;
        var tmpid = '';
        $("#facilityItemForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
                if(tmpid === ''){
                    tmpid = $(this).val();
                }else{
                    tmpid = tmpid + ','+$(this).val();
                }
            }
        });
        $('#tmpid').val(tmpid);
        if(cnt === 0){
            alert('追加する商品を選択してください');
        }else{
            $("#facilityItemForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_confirm').submit();
        }
    });

});

function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>login/home" style="">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">直納品要求</a></li>
        <li>施設採用品選択</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>施設採用品選択</span></h2>
<h2 class="HeaddingMid">直納品登録を行う商品を選択してください</h2>

<form class="validate_form search_form" id="facilityItemForm" method="post" action="add">
    <?php echo $this->form->input('add_search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('cart.tmpid',array('type'=>'hidden','id' =>'tmpid'));?>
    <?php echo $this->form->input('d2promises.centerId',array('type'=>'hidden'));?>
    <?php echo $this->form->input('d2promises.hospitalId',array('type'=>'hidden'));?>
    <?php echo $this->form->input('d2promises.supplierId',array('type'=>'hidden'));?>
    <?php echo $this->form->input('d2promises.departmentId',array('type'=>'hidden'));?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('d2promises.supplierName' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('d2promises.className' , array('type' => 'text' , 'class'=>'lbl' ,'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                    <?php echo $this->form->input('d2promises.classId' , array('type' => 'hidden')); ?>
                </td>
            </tr>
            <tr>
                <th>作業日付</th>
                <td>
                    <?php echo $this->form->input('d2promises.work_date' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('d2promises.hospitalName' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('d2promises.recital1' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('d2promises.departmentName' , array('type' => 'text' , 'class'=>'lbl', 'style'=>'width:150px;' , 'readonly'=> 'readonly')); ?>
                </td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
        </table>
    </div>

    <h2 class="HeaddingSmall">検索条件</h2>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFacilityItems.internal_code',array('class'=>'txt search_internal_code','style'=>'width:120px')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFacilityItems.item_code',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
                <th></th>
                <td><?php echo($this->form->checkbox('MstFacilityItems.canChecked', array('label' => false))); ?>要求可能な項目のみ</td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFacilityItems.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFacilityItems.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFacilityItems.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('MstFacilityItems.jan_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>

        </table>

        <div class="ButtonBox">
           <p class="center">
             <input type="button" class="btn btn1" id="btn_Search"/>
           </p>
        </div>
    </div><!-- searchBox -->
    <div align="right" id="pageTop" ></div>

<?php
    if(isset($this->request->data['add_search']['is_search'])){
      if(count($SearchResult) === 0){
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <colgroup>
            <col width="20" />
            <col width="30" />
            <col width="90" />
            <col width="100" />
            <col width="100" />
            <col />
            <col />
          </colgroup>
          <tr>
            <th class="center" ><input type="checkbox" class="checkAll_1"></th>
            <th class="col10">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">規格</th>
            <th class="col20">製品番号</th>
            <th class="col20">販売元</th>
            <th class="col10">包装単位</th>
          </tr>
        </table>
      </div>
      <span>該当するデータはありません。</span>
<?php
      }else{
?>
    <div id="results">
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <colgroup>
            <col width="20" />
            <col width="30" />
            <col width="90" />
            <col width="100" />
            <col width="100" />
            <col />
            <col />
          </colgroup>
          <tr>
            <th class="center" ><input type="checkbox" class="checkAll_1"></th>
            <th class="col10">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">規格</th>
            <th class="col20">製品番号</th>
            <th class="col20">販売元</th>
            <th class="col10">包装単位</th>
          </tr>
        </table>
      </div>

      <div class="TableScroll" style="" id="searchForm">
        <?php $_cnt=0;foreach($SearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row["MstFacilityItems"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <colgroup>
            <col width="20" />
            <col width="30" />
            <col width="90" />
            <col width="100" />
            <col width="100" />
            <col />
            <col />
          </colgroup>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php echo "itemSelectedRow".$_row["MstFacilityItems"]['id'];?> >
            <td class="center">
              <?php if($_row['MstFacilityItems']['check']){ ?>
              <?php echo $this->form->checkbox("SelectedID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row["MstFacilityItems"]["id"] , 'style'=>'width:20px;text-align:center;') ); ?>
              <?php } ?>
            </td>
            <td class="col10"><?php echo h_out($_row['MstFacilityItems']['internal_code'],'center'); ?></td>
            <td class="col20"><?php echo h_out($_row['MstFacilityItems']['item_name']); ?></td>
            <td class="col20"><?php echo h_out($_row['MstFacilityItems']['standard']); ?></td>
            <td class="col20"><?php echo h_out($_row['MstFacilityItems']['item_code']); ?></td>
            <td class="col20"><?php echo h_out($_row['MstFacilityItems']['dealer_name']); ?></td>
            <td class="col10"><?php echo h_out($_row['MstFacilityItems']['unit_name']); ?></td>
          </tr>

        </table>
        <?php $_cnt++;endforeach;?>
      </div>

      <div class="ButtonBox" style="margin-top:5px;">
        <p class="center">
          <input type="button" class="btn btn4" id="cmdSelect" />
        </p>
      </div>
    </div>
<?php
      }
?>

    <?php if(isset($this->request->data['add_search']['is_search'])){?>
    <h2 class="HeaddingSmall">選択商品</h2>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <colgroup>
            <col width="20" />
            <col width="30" />
            <col width="90" />
            <col width="100" />
            <col width="100" />
            <col />
            <col />
          </colgroup>
          <tr>
            <th class="center"><input type="checkbox" class="checkAll_2" checked></th>
            <th class="col10">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">規格</th>
            <th class="col20">製品番号</th>
            <th class="col20">販売元</th>
            <th class="col10">包装単位</th>
          </tr>
        </table>
    </div>
    <div class="TableScroll" style="" id="cartForm">
      <?php if(isset($CartSearchResult)){ ?>
      <?php $_cnt=0;foreach($CartSearchResult as $_row): ?>
        <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row["MstFacilityItems"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
          <colgroup>
            <col width="20" />
            <col width="30" />
            <col width="90" />
            <col width="100" />
            <col width="100" />
            <col />
            <col />
          </colgroup>
          <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php echo "itemSelectedRow".$_row["MstFacilityItems"]['id'];?> >
            <td class="center">
              <?php echo $this->form->checkbox("SelectedID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row["MstFacilityItems"]["id"] , 'style'=>'width:20px;text-align:center;' , 'checked'=>'checked') ); ?>
            </td>
            <td class="col10"><?php echo h_out($_row['MstFacilityItems']['internal_code'] , 'center'); ?></td>
            <td class="col20"><?php echo h_out($_row['MstFacilityItems']['item_name']) ; ?></td>
            <td class="col20"><?php echo h_out($_row['MstFacilityItems']['standard']); ?></td>
            <td class="col20"><?php echo h_out($_row['MstFacilityItems']['item_code']); ?></td>
            <td class="col20"><?php echo h_out($_row['MstFacilityItems']['dealer_name']); ?></td>
            <td class="col10"><?php echo h_out($_row['MstFacilityItems']['unit_name']); ?></td>
          </tr>
        </table>
      <?php $_cnt++;endforeach;?>
      <?php }?>
      <div id="itemSelectedTable"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" class="btn btn3" />
      </p>
    </div>
    <?php }?>
<?php
    }else{
      //検索実行フラグがfalse
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>0)); ?>
      </div>

      <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
          <colgroup>
            <col width="20" />
            <col width="100" />
            <col width="90" />
            <col width="100" />
            <col width="100" />
            <col />
            <col />
          </colgroup>
          <tr>
            <th ><input type="checkbox" class="checkAll_1"></th>
            <th class="col10">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">規格</th>
            <th class="col20">製品番号</th>
            <th class="col20">販売元</th>
            <th class="col10">包装単位</th>
          </tr>
        </table>

      </div>
<?php
    }
    echo $this->form->end();

?>
<div align="right" id="pageDow" ></div>
