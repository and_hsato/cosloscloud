<script type="text/javascript">
$(function(){
    //ボタン押下
    $("#btn_select").click(function(){
        $("#form_add").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add');
        $("#form_add").attr('method', 'post');
        $("#form_add").submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>直納品要求登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>直納品要求登録</span></h2>

<form class="validate_form input_form" id="form_add" method="" action="">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('d2promises.supplierName' , array('type'=>'hidden' , 'id'=>'supplierName')); ?>
                    <?php echo $this->form->input('d2promises.supplierText',array('id' => 'supplierText', 'label' => false, 'div' => false, 'class' => 'r validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('d2promises.supplierCode',array('options'=>$supplier_list,'id' => 'supplierCode', 'class' => 'r validate[required]','empty'=>true)); ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('d2promises.classId',array('options'=>$class_list,'id' => 'classId', 'class' => 'txt','empty'=>true)); ?>
                    <?php echo $this->form->input('d2promises.class_name',array('type'=>'hidden', 'id' => 'className')); ?>
                </td>
            </tr>
            <tr>
                <th>作業日時</th>
                <td><?php echo $this->form->input('d2promises.work_date' , array('type'=>'text' , 'class'=>'r validate[optional,custom[date],funcCall2[date2]] date' , 'id'=>'datepicker1') );?></td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('d2promises.hospitalName' , array('type'=>'hidden' , 'id'=>'facilityName')); ?>
                    <?php echo $this->form->input('d2promises.hospitalText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'r validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('d2promises.hospitalCode',array('options'=>$hospital_list,'id' => 'facilityCode', 'class' => 'txt r', 'empty' => true)); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('d2promises.recital1' , array('type'=>'text' ,'class'=>'txt' , 'maxlength'=>'200'))?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('d2promises.departmentName' , array('type'=>'hidden' , 'id'=>'departmentName')); ?>
                    <?php echo $this->form->input('d2promises.departmentText', array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'r validate[required]', 'style' => 'width: 60px;')); ?>
                    <?php echo $this->form->input('d2promises.departmentCode',array('options'=>array(),'id' => 'departmentCode', 'class' => 'txt validate[required] r', 'empty' => true)); ?>
                </td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn58 submit [p2]" id="btn_select" />
        </p>
    </div>
</form>
