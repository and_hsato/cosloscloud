<script  type="text/javascript">
$(document).ready(function(){
    //発注書印刷ボタン押下
    $("#btn_Order").click(function(){
        $("#form_add_result").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report');
        $("#form_add_result").attr('method', 'post');
        $("#form_add_result").submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/" >直納品要求</a></li>
        <li class="pankuzu">施設採用品選択</li>
        <li class="pankuzu">直納品要求登録確認</li>
        <li>直納品要求登録結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>直納品要求登録結果</span></h2>
<div class="Mes01">直納品の要求を行いました</div>

<form class="validate_form" id="form_add_result" action="#" method="post">
    <?php echo $this->form->input('d2promises.id.0' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('User.user_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.MstUser.id')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>仕入先</th>
                <td><?php echo $this->form->input('d2promises.supplierName' , array('class'=>'lbl' ,'readonly'=>'readonly')); ?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('d2promises.class_name' , array('class'=>'lbl','readonly'=>true)); ?>
                    <?php echo $this->form->input('d2promises.classId' , array('type' => 'hidden' )); ?>
                </td>
            </tr>
            <tr>
                <th>作業日付</th>
                <td><?php echo $this->form->input('d2promises.work_date' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('d2promises.hospitalName' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('d2promises.recital1' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td><?php echo $this->form->input('d2promises.departmentName' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
        </table>
    </div>

    <hr class="Clear" />
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
        <span class="DisplaySelect">
            　表示件数：<?php echo count($FacilityItemList);  ?>件
        </span>
    </div>
    <div class="TableScroll" id="containTables">
        <table class="TableStyle01 table-odd2">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th rowspan="2" class="center"></th>
                <th>商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th>包装単位</th>
                <th>作業区分</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>数量</th>
                <th>備考</th>
            </tr>

        <?php $cnt=0;
            foreach($FacilityItemList as $f){
            ?>

            <tr>
                <td rowspan="2" class="center">
                </td>
                <td><?php echo h_out($f['MstFacilityItems']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($f['MstFacilityItems']['item_name']); ?></td>
                <td><?php echo h_out($f['MstFacilityItems']['item_code']); ?></td>
                <td><?php echo h_out($f['MstFacilityItems']['unit_name']); ?></td>
                <td>
                    <?php echo $this->form->input('d2promises.work_type.'.$f['MstFacilityItems']['id'], array('options'=>$class_list,'class' => 'txt','empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($f['MstFacilityItems']['standard']); ?></td>
                <td><?php echo h_out($f['MstFacilityItems']['dealer_name']); ?></td>
                <td><?php echo $this->form->input('d2promises.quantity.'.$f['MstFacilityItems']['id'] , array('type'=>'text' , 'class'=>'lbl num' ,'readonly'=>'readonly', 'maxlength'=>'9' )); ?></td>
                <td><?php echo $this->form->input('d2promises.recital.'.$f['MstFacilityItems']['id'] , array('type'=>'text' , 'class'=>'lbl' ,'readonly'=>'readonly', 'maxlength'=>'200')); ?></td>
            </tr>
        <?php $cnt++;//セルの色変更用
             } //endforeach
        ?>
        </table>
    </div>    
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
       <p class="center">
            <input type="button" class="btn btn21" id="btn_Order"/>
        </p>
    </div>
</form>
