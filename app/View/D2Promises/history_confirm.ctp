<script type="text/javascript">
$(document).ready(function(){

    $("#btn_Result").click(function(){
        if($('input[type=checkbox].chk:checked').length === 0) { 
            alert('取消を実行する明細を選択してください');
            return false;
        }else{
            if(window.confirm("取消を実行します。よろしいですか？")){
                $("#ConfirmForm").attr('action' ,  '<?php echo $this->webroot ?><?php echo $this->name?>/history_result/');
                $("#ConfirmForm").submit();
            }
        }
    });
  
    //チェックボックス一括制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/history/">直納要求作成履歴</a></li>
        <li>直納要求作成履歴明細</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>直納要求作成履歴明細</span></h2>
<h2 class="HeaddingMid">直納要求作成履歴明細</h2>
<?php echo $this->form->create('D2Promise',array('class' => 'validate_form','type' => 'post','action' => '' ,'id' =>'ConfirmForm')); ?>
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right" id="pageTop" ></td>
    </tr>
    <tr>
        <td>表示件数：<?php echo count($result); ?>件</td>
    </tr>
</table>
<div class="TableHeaderAdjustment01">
    <table class="TableHeaderStyle02">
        <tr>
            <th style="width: 20px;" rowspan="2" class="center"><input type="checkbox" class="checkAll center" /></th>
            <th style="width:150px;">要求番号</th>
            <th style="width:80px">要求日</th>
            <th style="width:70px">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th style="width:80px;">包装単位</th>
            <th style="width:80px;">作業区分</th>
            <th style="width:70px;">更新者</th>
        </tr>
        <tr>
            <th colspan="2">施設　／　部署</th>
            <th></th>
            <th>規格</th>
            <th>販売元</th>
            <th>数量</th>
            <th colspan="2">備考</th>
        </tr>
    </table>
</div>
<div class="TableScroll">
    <table class="TableStyle02 table-even2">
        <colgroup>
            <col>
            <col>
            <col align="center">
            <col align="center">
            <col>
            <col>
            <col>
            <col>
            <col>
        </colgroup>
        <?php
          $i = 0;
          foreach($result as $r) {
        ?>
        <tr>     
            <td style="width:20px;" rowspan="2" class="center">
            <?php
              if($r['d2promises']['canCancel']){
                  echo $this->form->checkbox('d2promises.id.'.$i,array('value'=>$r['d2promises']['id'], 'class' =>'center chk' , 'hiddenField' => false) );
              } ?>
            </td>
            <td style="width:150px;"><?php echo h_out($r['d2promises']['work_no'],'center'); ?></td>
            <td style="width:80px"><?php echo h_out($r['d2promises']['work_date'],'center'); ?></td>
            <td style="width:70px"><?php echo h_out($r['d2promises']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($r['d2promises']['item_name']); ?></td>
            <td><?php echo h_out($r['d2promises']['item_code']); ?></td>
            <td style="width:80px;"><?php echo h_out($r['d2promises']['unit_name']); ?></td>
            <td style="width:80px"><?php echo h_out($r['d2promises']['class_name']); ?></td>
            <td style="width:70px"><?php echo h_out($r['d2promises']['user_name']); ?></td>
        </tr>
        <tr>
            <td colspan="2"><?php echo h_out($r['d2promises']['facility_name'] . ' / ' . $r['d2promises']['department_name']); ?></td>
            <td></td>
            <td><?php echo h_out($r['d2promises']['standard']); ?></td>
            <td><?php echo h_out($r['d2promises']['dealer_name']); ?></td>
            <td><?php echo h_out($r['d2promises']['quantity'],'right'); ?></td>
            <td colspan="2">
                <?php
                   echo $this->form->input('d2promises.recital.'.$i , array('type' => 'text' , 'class'=>'lbl' , 'readonly'=>'readonly' , 'value'=>$r['d2promises']['recital']));
                ?>
            </td>
        </tr>
        <?php $i++; }  ?>
    </table>
</div>
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>

<div class="ButtonBox"  id="pageDow">
    <p class="center">
        <input type="button" class="btn btn6 submit [p2]" id="btn_Result"/>
    </p>
</div>
<?php echo $this->form->end(); ?>
