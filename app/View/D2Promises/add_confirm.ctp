<script type="text/javascript">
    $(document).ready(function(){
        $("#btn_add_result").click(function(){
            if($('input[type=checkbox].chk:checked').length === 0){
                alert('直納品要求を行う商品を選択してください');
                return false;
            }else{
                var hasError = false;
                $('input[type=checkbox].chk').each(function(i){
                    if(this.checked){
                        var input = $('#containTables');
                        //数量
                        if(input.find('input[type=text].quantity:eq(' + i + ')').val() == ''){
                            alert('数量を入力してください');
                            hasError = true;
                            return;
                        }else if(!input.find('input[type=text].quantity:eq(' + i + ')').val().match(/^[1-9][0-9]*/)){
                            alert('数量の入力が不正です');
                            hasError = true;
                            return;
                        }else if(input.find('input[type=text].quantity:eq(' + i + ')').val() >= 100){
                            alert('100以上の数量は指定出来ません');
                            hasError = true;
                            return;
                        }

                    }
                });
                if(true === hasError){
                    return false;
                }else{
                    $("#form_add_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_result').submit();
                }
            }
        });

        //備考一括入力
        $('#recital_btn').click(function(){
            $('.recital').val($('#recital_txt').val()); 
        });
        
        //チェックボックス一括制御
        $('.checkAll').click(function(){
            $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
        });
    });

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/" >直納品要求</a></li>
        <li class="pankuzu">施設採用品選択</li>
        <li>直納品要求登録確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>直納品要求登録確認</span></h2>
<h2 class="HeaddingMid">以下の直納品要求を行います。</h2>

<form class="validate_form" id="form_add_result" action="#" method="post">
    <?php echo $this->form->input('d2promises.token' , array('type'=>'hidden','tabindex'=>'-1'));?>
    <?php echo $this->form->input('d2promises.departmentId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <?php echo $this->form->input('d2promises.supplierId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <?php echo $this->form->input('d2promises.hospitalId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <?php echo $this->form->input('d2promises.centerId',array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>仕入先</th>
                <td><?php echo $this->form->input('d2promises.supplierName' , array('class'=>'lbl' ,'readonly'=>'readonly')); ?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('d2promises.class_name' , array('class'=>'lbl' ,'readonly'=>true)); ?>
                    <?php echo $this->form->input('d2promises.classId' , array('type' => 'hidden' )); ?>
                </td>
            </tr>
            <tr>
                <th>作業日付</th>
                <td><?php echo $this->form->input('d2promises.work_date' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('d2promises.hospitalName' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('d2promises.recital1' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td><?php echo $this->form->input('d2promises.departmentName' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
        </table>
    </div>

    <hr class="Clear" />
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
        <span class="DisplaySelect">
            　表示件数：<?php echo count($FacilityItemList);  ?>件
        </span>
        <span class="BikouCopy">
            <input type="text" id="recital_txt" maxlength="200" class="txt" />
            <input type="button" class="btn btn8 [p2]" id='recital_btn' />
        </span>
    </div>
    <div class="TableScroll" id="containTables">
        <table class="TableStyle01 table-odd2">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th rowspan="2" class="center"><input type="checkbox" class="checkAll" /></th>
                <th>商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th>包装単位</th>
                <th>作業区分</th>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <th>規格</th>
                <th>販売元</th>
                <th>数量</th>
                <th>備考</th>
            </tr>

        <?php $cnt=0;
            foreach($FacilityItemList as $f){
            ?>

            <tr>
                <td rowspan="2" class="center">
                    <?php echo $this->form->checkbox("d2promises.id.{$cnt}", array('class'=>'center chk','value'=>$f["MstFacilityItems"]["id"] , 'style'=>'width:20px;text-align:center;' , 'hiddenField' => false) ); ?>
                </td>
                <td><?php echo h_out($f['MstFacilityItems']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($f['MstFacilityItems']['item_name']); ?></td>
                <td><?php echo h_out($f['MstFacilityItems']['item_code']); ?></td>
                <td><?php echo h_out($f['MstFacilityItems']['unit_name']); ?></td>
                <td>
                    <?php echo $this->form->input('d2promises.work_type.'.$f['MstFacilityItems']['id'], array('options'=>$class_list,'class' => 'txt','empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($f['MstFacilityItems']['standard']); ?></td>
                <td><?php echo h_out($f['MstFacilityItems']['dealer_name']); ?></td>
                <td><?php echo $this->form->input('d2promises.quantity.'.$f['MstFacilityItems']['id'] , array('type'=>'text' , 'class'=>'tbl_txt right quantity' , 'maxlength'=>'9' , 'style'=>'border:solid 1px red; ime-mode:disabled;')); ?></td>
                <td><?php echo $this->form->input('d2promises.recital.'.$f['MstFacilityItems']['id'] , array('type'=>'text' , 'class'=>'tbl_txt recital' , 'maxlength'=>'200')); ?></td>
            </tr>
        <?php $cnt++;//セルの色変更用
             } //endforeach
        ?>
        </table>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
        <input type="button" class="btn btn2 [p2]" id="btn_add_result" name="btn_add_result"/>
    </div>
</form>
