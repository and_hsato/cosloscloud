<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li class="pankuzu">申請確認</li>
        <li class="pankuzu">申請ステータス更新</li>
        <li>申請ステータス更新結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>申請ステータス更新</span></h2>
<div class="Mes01">申請ステータスの更新を行いました</div>
    <form method="post" action="" accept-charset="utf-8" id="ApplicationInfo" class="validate_form search_form">
    <div class="results">
        <div class="TableScroll">
            <table class="TableStyle01">
                <tr>
                    <th style="width:20px;"></th>
                    <th style="width:135px;">申請番号</th>
                    <th style="width:85px;">申請日</th>
                    <th>商品名</th>
                    <th>規格</th>
                    <th>製品番号</th>
                    <th>JANコード</th>
                    <th>販売元</th>
                    <th>備考</th>
                    <th>ステータス</th>
                </tr>
                <?php $cnt=0; foreach($result as $r){ ?>
                <tr class="<?php echo (($cnt%2==0)?'':'odd'); ?>">
                    <td></td>
                    <td><?php echo h_out($r['Application']['work_no']); ?></td>
                    <td><?php echo h_out($r['Application']['work_date']); ?></td>
                    <td><?php echo h_out($r['Application']['item_name']); ?></td>
                    <td><?php echo h_out($r['Application']['standard'],'center'); ?></td>
                    <td><?php echo h_out($r['Application']['item_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Application']['jan_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Application']['dealer_name'],'center'); ?></td>
                    <td><?php echo h_out($r['Application']['recital'],'center'); ?></td>
                    <td><?php echo h_out($r['Application']['status_name'],'center'); ?></td>
                </tr>
                <?php $cnt++; } ?>
            </table>
        </div>
    </div>
</form>
