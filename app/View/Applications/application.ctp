<script  type="text/javascript">
$(document).ready(function() {
    $(function(){
        $("#btn_conf").click(function(){
            // 販売元名をhidden パラメータに入れなおす
            $("#dealer_name").val($("#dealer_list option:selected").text());
            $("#ApplicationInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/application_confirm');
            $("#ApplicationInfo").submit();
        });
    });
});

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>マスタ申請登録</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>マスタ申請登録</span></h2>
<h2 class="HeaddingMid">商品情報を設定してください</h2>
    <form method="post" action="" accept-charset="utf-8" id="ApplicationInfo" class="validate_form">
        <div class="SearchBox">
            <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('Application.item_name' , array('class'=>'r validate[required] search_canna' , 'maxlength'=>100)); ?>
                </td>
                <td></td>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('Application.standard' , array('class'=>'r validate[required] search_canna' , 'maxlength'=>100))?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('Application.item_code' , array('class'=>'txt search_canna' , 'maxlength'=>100))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>JANコード</th>
                <td>
                    <?php echo $this->form->input('Application.jan_code' , array('class'=>'txt' , 'maxlength'=>14))?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                   <?php echo $this->form->input('Application.mst_dealer_id' , array('options'=>$dealer_list , 'class'=>'r validate[required]' , 'empty'=>true , 'id'=>'dealer_list')); ?>
                   <?php echo $this->form->input('Application.dealer_name' , array('type'=>'hidden' , 'id'=>'dealer_name')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                   <?php echo $this->form->input('Application.recital' , array('type'=>'text' , 'class'=>'txt' , 'maxlength'=>200)); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn3 [p2]" id="btn_conf"/>
    </div>
</form>
