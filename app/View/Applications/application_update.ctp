<script  type="text/javascript">
$(document).ready(function() {

    //検索ボタン押下
     $("#btn_Search").click(function(){
         validateDetachSubmit($('#ApplicationInfo') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/application_update' );
     });

  
    //ステータス更新ボタン押下
    $("#btn_Update").click(function(){
        $("#ApplicationInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/application_update_result').submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>申請紐づけ確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>申請紐づけ確認</span></h2>
    <form method="post" action="" accept-charset="utf-8" id="ApplicationInfo" class="validate_form search_form">
        <?php echo $this->form->input('Grand.is_search' , array('type'=>'hidden'))?>
        <?php echo $this->form->input('Application.id' , array('type'=>'hidden'))?>
        <h2 class="HeaddingSmall">申請情報</h2>
        <table class="FormStyleTable">
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('Application.item_name',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
                <th>規格</th>
                <td><?php echo $this->form->input('Application.standard',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('Application.item_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
            </tr>
            <tr>
                <th>JANコード</th>
                <td><?php echo $this->form->input('Application.jan_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('Application.dealer_name',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
                <th>備考</th>
                <td><?php echo $this->form->input('Application.recital',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
            </tr>
        </table>
        <h2 class="HeaddingMid">紐づけを行いたいマスタを検索してください</h2>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>マスタID</th>
                    <td><?php echo $this->form->input('Grand.master_id' , array('type'=>'text' , 'class'=>'txt'))?></td>
                    <td></td>
                    <th>商品名</th>
                    <td><?php echo $this->form->input('Grand.item_name' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                    <th>規格</th>
                    <td><?php echo $this->form->input('Grand.standard' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>製品番号</th>
                    <td><?php echo $this->form->input('Grand.item_code' , array('class'=>'txt search_upper'))?></td>
                    <td></td>
                    <th>JANコード</th>
                    <td><?php echo $this->form->input('Grand.jan_code' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                    <th></th>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="btn btn1" id="btn_Search"/>
        </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01">
                <tr>
                    <th style="width:20px;"></th>
                    <th style="width:135px;">マスタID</th>
                    <th style="width:85px;">商品名</th>
                    <th>規格</th>
                    <th>製品番号</th>
                    <th>JANコード</th>
                    <th>販売元</th>
                    <th>定価</th>
                    <th>定価単価</th>
                    <th>償還価格</th>
                </tr>
                <?php $cnt=0; foreach($result as $r){ ?>
                <tr class="<?php echo (($cnt%2==0)?'':'odd'); ?>">
                    <td><?php echo $this->form->radio('Application.master_id' , array($r['Grand']['master_id'] => '') , array('hiddenField'=>false , 'label'=>false , 'class'=>'')); ?></td>
                    <td><?php echo h_out($r['Grand']['master_id']); ?></td>
                    <td><?php echo h_out($r['Grand']['item_name']); ?></td>
                    <td><?php echo h_out($r['Grand']['standard'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['item_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['jan_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['dealer_name'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['unit_price'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['price'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['refund_price'],'center'); ?></td>
                </tr>
                <?php $cnt++; } ?>
                <?php if( count($result)==0 && isset($this->request->data['Grand']['is_search']) ){ ?>
                <tr><td colspan="10" class="center">該当するデータがありませんでした</td></tr>
                <?php } ?>
            </table>
        </div>
        <?php if(count($result) > 0){ ?>
        <div class="ButtonBox">
            <input type="button" class="btn btn24" id="btn_Update"/>
        </div>
        <?php } ?>
    </div>
</form>
