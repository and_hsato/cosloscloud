<script  type="text/javascript">
$(document).ready(function() {

    //検索ボタン押下
     $("#btn_Search").click(function(){
         validateDetachSubmit($('#ApplicationInfo') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/application_search' );
     });

    //新規ボタン押下
    $("#btn_New").click(function(){
        // 1件の場合だけ処理
        if($('input[type=checkbox].chk:checked').length > 0 ){
            if($('input[type=checkbox].chk:checked').length == 1 ){
                $("#ApplicationInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/application_edit_confirm');
                $("#ApplicationInfo").submit();
            }else{
                alert("1件のみ選択してください");
            }
        }else{
           alert("申請を選択してください");
        }
    });
  
    //紐づけ編集ボタン押下
    $("#btn_Edit").click(function(){
        // 1件の場合だけ処理
        if($('input[type=checkbox].chk:checked').length > 0 ){
            if($('input[type=checkbox].chk:checked').length == 1 ){
                $("#ApplicationInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/application_update');
                $("#ApplicationInfo").submit();
            }else{
                alert("1件のみ選択してください");
            }
        }else{
           alert("申請を選択してください");
        }
    });
  
    //ステータス更新ボタン押下
    $("#btn_Update").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#ApplicationInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/application_status_confirm');
            $("#ApplicationInfo").submit();
        } else {
            alert("申請を選択してください");
        }
    });

    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

  
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>申請確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>申請確認</span></h2>
<h2 class="HeaddingMid">変更を行いたいマスタ申請を検索してください</h2>
    <form method="post" action="" accept-charset="utf-8" id="ApplicationInfo" class="validate_form search_form">
        <?php echo $this->form->input('Application.is_search' , array('type'=>'hidden'))?>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>申請番号</th>
                    <td><?php echo $this->form->input('Application.work_no' , array('class'=>'txt'))?></td>
                    <td></td>
                    <th>申請日</th>
                    <td>
                        <?php echo $this->form->input('Application.work_date_from' , array('type'=>'text' , 'class'=>'date txt validate[optional,custom[date]]' , 'id'=>'datepicker1'))?>
                      &nbsp;～&nbsp;
                        <?php echo $this->form->input('Application.work_date_to' , array('type'=>'text' , 'class'=>'date txt validate[optional,custom[date]]' , 'id'=>'datepicker2'))?>
                    </td>
                    <td></td>
                    <th></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->input('Application.item_name' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                    <th>規格</th>
                    <td><?php echo $this->form->input('Application.standard' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->input('Application.item_code' , array('class'=>'txt search_upper'))?></td>
                    <td></td>
                </tr>
                <tr>
                    <th>JANコード</th>
                    <td><?php echo $this->form->input('Application.jan_code' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->input('Application.mst_dealer_id' , array('options'=>$dealer_list , 'class'=>'txt' , 'empty'=>true))?></td>
                    <td></td>
                    <th>ステータス</th>
                    <td><?php echo $this->form->input('Application.status' , array('options'=>$status_list , 'class'=>'txt' , 'empty'=>true))?></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" value="" class="btn btn1" id="btn_Search"/>
        </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01">
                <tr>
                    <th style="width:20px;"><input type="checkbox" class="checkAll"/></th>
                    <th style="width:135px;">申請番号</th>
                    <th style="width:85px;">申請日</th>
                    <th>商品名</th>
                    <th>規格</th>
                    <th>製品番号</th>
                    <th>JANコード</th>
                    <th>販売元</th>
                    <th>備考</th>
                    <th>ステータス</th>
                </tr>
                <?php $cnt=0; foreach($result as $r){ ?>
                <tr class="<?php echo (($cnt%2==0)?'':'odd'); ?>">
                    <td class="center"><?php echo $this->form->input('Application.id.'.$cnt , array('type'=>'checkbox' , 'value'=>$r['Application']['id'] , 'hiddenField'=>false , 'class'=>'chk'))?></td>
                    <td><?php echo h_out($r['Application']['work_no']); ?></td>
                    <td><?php echo h_out($r['Application']['work_date']); ?></td>
                    <td><?php echo h_out($r['Application']['item_name']); ?></td>
                    <td><?php echo h_out($r['Application']['standard'],'center'); ?></td>
                    <td><?php echo h_out($r['Application']['item_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Application']['jan_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Application']['dealer_name'],'center'); ?></td>
                    <td><?php echo h_out($r['Application']['recital'],'center'); ?></td>
                    <td><?php echo h_out($r['Application']['status_name'],'center'); ?></td>
                </tr>
                <?php $cnt++; } ?>
                <?php if( count($result)==0 && isset($this->request->data['Application']['is_search']) ){ ?>
                <tr><td colspan="10" class="center">該当するデータがありませんでした</td></tr>
                <?php } ?>
            </table>
        </div>
        <?php if(count($result) > 0){ ?>
        <div class="ButtonBox">
            <input type="button" class="btn btn11" id="btn_New"/>
            <input type="button" class="btn btn9" id="btn_Edit"/>
            <input type="button" class="btn btn24" id="btn_Update"/>
        </div>
        <?php } ?>
    </div>
</form>
