<script  type="text/javascript">
var webroot = "<?php echo $this->webroot; ?>";
$(document).ready(function() {
    $('select , input[type=text]').attr("readonly","readonly").removeClass('r txt date' ).addClass("lbl");
    $('input[type=button]').css("display","none");
    $('input[type=checkbox] , select').each(function(){
        $(this).after('<input type=hidden name=' + $(this).attr('name') + ' value=' + $(this).val() + ' >');
        $(this).attr('disabled' , 'disabled');
    });
});

</script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/facility_item.js"></script>
<style>
<!--
div#search_dealer_form {
  z-index: 9999;
  background-color: #808080;
  position: absolute;
  height: auto;
  width: auto;
  padding: 4px;
  color: #fff;
  left: 400px;
  top: 200px;
  width: 350px;
}

div#search_dealer_form ul li {
  margin-top: 2px;
  margin-left: 0px;
}

div#search_jmdn_form {
  z-index: 9999;
  background-color: black;
  position: absolute;
  height: auto;
  width: auto;
  padding: 4px;
  color: #fff;
  left: 400px;
  top: 200px;
  width: 350px;
}

div#search_jmdn_form ul li {
  margin-top: 2px;
  margin-left: 0px;
}

div#search_icc_form {
  z-index: 9999;
  background-color: red;
  position: absolute;
  height: auto;
  width: auto;
  padding: 4px;
  color: #fff;
  left: 400px;
  top: 200px;
  width: 350px;
}

div#search_icc_form ul li {
  margin-top: 2px;
  margin-left: 0px;
}

div#search_item_category_form {
  z-index: 9999;
  background-color: blue;
  position: absolute;
  height: auto;
  width: auto;
  padding: 4px;
  color: #fff;
  left: 400px;
  top: 200px;
  width: 350px;
}

div#search_item_category_form ul li {
  margin-top: 2px;
  margin-left: 0px;
}

/* root element for scrollable */
.scrollable { /* required settings */
  position: relative;
  overflow: auto;
  width: 350px;
}

/* root element for scrollable items */
.scrollable .items {
  display: none;
}

.floatClose {
  margin-left: 10px;
}
.item_scrollable {
  overflow: scroll;   /* スクロール表示 */ 
  width: auto;
  height: auto;
}
-->
</style>
<?php echo $this->element('masters/facility_items/item_hiddenAjaxForm'); ?>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/application_history">申請履歴</a></li>
        <li>申請履歴明細</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタ情報</span></h2>
<?php echo $this->element('masters/grandmaster/item_main'); ?>