<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li class="pankuzu">申請紐づけ確認</li>
        <li>申請紐づけ確認結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>申請紐づけ確認結果</span></h2>
<div class="Mes01">申請紐づけ更新を行いました</div>
<form method="post" action="" accept-charset="utf-8" id="ApplicationInfo" class="validate_form search_form">
    <?php echo $this->form->input('Application.id' , array('type'=>'hidden'))?>
    <h2 class="HeaddingSmall">申請情報</h2>
    <table class="FormStyleTable">
        <tr>
            <th>商品名</th>
            <td><?php echo $this->form->input('Application.item_name',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
            <th>規格</th>
            <td><?php echo $this->form->input('Application.standard',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
            <th>製品番号</th>
            <td><?php echo $this->form->input('Application.item_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
        </tr>
        <tr>
            <th>JANコード</th>
            <td><?php echo $this->form->input('Application.jan_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
            <th>販売元</th>
            <td><?php echo $this->form->input('Application.dealer_name',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
            <th>備考</th>
            <td><?php echo $this->form->input('Application.recital',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
        </tr>
    </table>

    <div class="results">
        <div class="TableScroll">
            <table class="TableStyle01">
                <tr>
                    <th style="width:20px;"></th>
                    <th style="width:135px;">マスタID</th>
                    <th style="width:85px;">商品名</th>
                    <th>規格</th>
                    <th>製品番号</th>
                    <th>JANコード</th>
                    <th>販売元</th>
                    <th>定価</th>
                    <th>定価単価</th>
                    <th>償還価格</th>
                </tr>
                <?php $cnt=0; foreach($result as $r){ ?>
                <tr class="<?php echo (($cnt%2==0)?'':'odd'); ?>">
                    <td></td>
                    <td><?php echo h_out($r['Grand']['master_id']); ?></td>
                    <td><?php echo h_out($r['Grand']['item_name']); ?></td>
                    <td><?php echo h_out($r['Grand']['standard'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['item_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['jan_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['dealer_name'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['unit_price'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['price'],'center'); ?></td>
                    <td><?php echo h_out($r['Grand']['refund_price'],'center'); ?></td>
                </tr>
                <?php $cnt++; } ?>
            </table>
        </div>
    </div>
</form>
