<script  type="text/javascript">
$(document).ready(function() {
    $(function(){
        $("#btn_conf").click(function(){
            $("#ApplicationInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/application_result');
            $("#ApplicationInfo").submit();
        });
    });
});

</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li class="pankuzu">マスタ申請登録</li>
        <li>マスタ申請確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge">マスタ申請登録</span></h2>
<h2 class="HeaddingMid">商品情報を確認してください</h2>
<form method="post" action="" accept-charset="utf-8" id="ApplicationInfo" class="validate_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('Application.item_name' , array('class'=>'lbl' , 'readonly'=>true )); ?>
                </td>
                <td></td>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('Application.standard' , array('class'=>'lbl' , 'readonly'=>true))?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('Application.item_code' , array('class'=>'lbl'  , 'readonly'=>true))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>JANコード</th>
                <td>
                    <?php echo $this->form->input('Application.jan_code' , array('class'=>'lbl'  , 'readonly'=>true))?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                   <?php echo $this->form->input('Application.dealer_name' , array('class'=>'lbl' , 'readonly'=>true)) ?>
                   <?php echo $this->form->input('Application.mst_dealer_id' , array('type'=>'hidden')) ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                   <?php echo $this->form->input('Application.recital' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true)); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn29 [p2]" id="btn_conf"/>
    </div>
</form>
