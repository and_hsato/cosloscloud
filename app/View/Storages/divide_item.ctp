<script type="text/javascript">
    $(document).ready(function(){
        $('#validated_date').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
        $('#barcode').keyup(function(e){
            if(e.which === 13){
                $('#readBarcode').click();
            }
        });
        $('#readBarcode').click(function(){
            $('#read_barcode_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/read_barcode').submit();
        });
        $('.checkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
        });

        $('#cmdCommit').click(function(){
            var cnt = 0;
            //入荷情報
            $('input[type=radio].item_select').each(function(){
                if(this.checked){
                    cnt++;
                }
            });
            if(cnt === 0){
                alert('入荷情報を選択してください');
                return false;
            }

            //在庫情報
            cnt = 0;
            var hasError = false;
            var sumStocks = 0;
            $('input[type=checkbox].checkAllTarget').each(function(i){
                if(this.checked){
                    var q = $('input[type=text].inputQuantity:eq(' + i + ')').val();
                    cnt++;
                    if(q == ''){
                        alert('数量を入力してください');
                        hasError = true;
                        return false;
                    }else if(q == "0"){
                        hasError = true;
                        alert('0以外の数字を入力してください');
                        return false;
                    }else if(q.match(/[\D]/g)){
                        hasError = true;
                        alert('数量に正しくない入力があります');
                        return false;
                    }else if(parseInt(q) > parseInt($('span[id].maxCreatableCount:eq(' + i + ')').text())){
                        hasError = true;
                        alert('最大作成可能数以上の入力はできません');
                        return false;
                    }else{
                        sumStocks += parseInt(q) * parseInt($('input[type=hidden][name=stock_per_unit]:eq(' + i + ')').val());
                    }
                }
            });
            if(hasError){
                return false;
            }else if(cnt === 0){
                alert('在庫情報を選択してください');
                return false;
            }

            //入荷数量と振り分け数があっているかどーか
            if(parseInt($('#totalQuantity').val()) != parseInt(sumStocks)){
                alert('入荷数量を適切に分配してください');
                return false;
            }
<?php if (isset($lotchk) && ($lotchk == true)){ ?>
                if( $('#lot_no').val() == ''){
                 if (!confirm('ロット番号が入力されていませんが間違いありませんか？')){
                    return false;
                }
            }

<?php  } ?>
    if(!dateCheck($('#validated_date').val()) && ""!=$("#validated_date").val() ){
      alert('有効期限の書式が不正です');
      return false;
    }
<?php if (isset($datechk) && ($datechk == true)){ ?>
    if ("" == $("#validated_date").val()){
        if (!confirm('有効期限が入力されていませんが間違いありませんか？')){
                    return false;
        }    
                }else{
        var term = <?php echo $expired_date; ?>;
        var cur = '<?php echo $current_date; ?>';
        if (!chkValidatedDate($('#validated_date').val() ,term ,cur)){
            if (!confirm('有効期限切れですが間違いありませんか？')){
                        return false;
                    }
                }
            }
<?php } ?>
            $('#trn_storage_commit_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/commit').submit();
        });
        //ajax method
        $('input[type=radio].item_select').change(function(){
            $('#classType').val($(this).parent('td:eq(0)').find('input[type=hidden]:eq(0)').val());
            $('#totalQuantity').val($(this).parent('td:eq(0)').find('input[type=hidden]:eq(1)').val());
            var u = '<?php echo $this->webroot; ?>storages/get_stocks_table/' + $(this).val();
            $('#stocksTable').find('tr').remove();
            $.ajax({
                type: 'GET',
                url: u,
                dataType: 'html',
                cache: false,
                success: function(data, dataType){
                    $('#stocksTable').append(data);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    alert('在庫情報の取得に失敗しました');
                }
            });
        });


    });
function dateCheck( checkStr ) {
  if( !checkStr.match(/^\d{4}\/\d{2}\/\d{2}$/)) {
  return false;
  }

  var year = checkStr.substr( 0 , 4 );
  var month = checkStr.substr( 5 , 2 );
  var day = checkStr.substr( 8 , 2 );

  if( month > 0 && month < 13 && day > 0 && day < 32 ) {
    var date = new Date( year ,month -1 , day );
    if( isNaN(date) ) {
      return false;
    } else if( date.getFullYear() == year && date.getMonth() == month - 1  ,date.getDate() == day ){
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function chkValidatedDate(target, term, cur){
    //期限切れ警告日数がなければチェックしない
    if( isNaN(term) ) {
        return true;
    }
    var targetdt = new Date(target);
    var todaydt  = new Date(cur);
    //本日日付(入庫日) + 有効期限日数
    var baseSec = todaydt.getTime();
    var addSec = term * 86400000;
    var targetSec = baseSec + addSec;
    todaydt.setTime(targetSec);

    //[本日日付(入庫日) + 有効期限日数] >= シールの有効期限 の場合 
    if(todaydt.getTime() >= targetdt.getTime()) {
        return false;
    }else{
        return true;
    }
}
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>読取入庫</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>読取入庫</span></h2>

<form id="read_barcode_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/read_barcode" method="post" onsubmit="return false;">
    <?php echo $this->form->input('TrnStorage.time' , array('type'=>'hidden' , 'value'=> date('Y/m/d H:i:s.u')));?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden')); ?>
    <input type="hidden" name="data[IsRead]" value="1" />
    <div class="SearchBox">
        <dl class="FormStyle01">
            <dt>バーコード</dt>
            <dd>
                <?php echo $this->form->input('barcode', array('class' => 'txt r')); ?>
                <input type="button" value="読込" id="readBarcode"/>
            </dd>
            <dt>読込登録種別</dt>
            <dd>
                <?php echo $this->form->radio('isAutoStorage', array('1' => ''), array('label' => '')); ?>自動入庫<br/>
                <font color="red"/>
                ※入荷済のうち、仕入先、包装単位、仕入単価が全て同じの場合は確認画面を表示しません<br/>
                ※バーコードにロット、有効期限の情報が無い場合は確認画面が表示されます<br>
                ※在庫を全て満たしている場合は、小分け単位での出力はできません<br>
                ※小分け指示は１つの単位でしか出力されません<br>
                </font>
                <?php echo $this->form->radio('isAutoStorage', array('0' => ''), array('label' => '')); ?>選択入庫<br/>
                <font color="red"/>
                ※どの入荷に対する入庫かを指定できます<br>
                ※確認画面を表示し、小分けする包装単位の配分を指定できます<br>
                ※入庫作業にコメントを設定できます
                </font>
            </dd>
        </dl>
    </div>
</form>
<form id="trn_storage_commit_form" class="validate_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/commit" method="post">
    <?php echo $this->form->input('TrnStorage.time' , array('type'=>'hidden' , 'value'=> date('Y/m/d H:i:s.u')));?>
    <input type="hidden" id="totalQuantity" value="" />
    <input type="hidden" id="classType" value="" />
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden')); ?>
    <div id="results">
        <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col/>
                    <col/>
                    <col width="20"/>
                    <col/>
                    <col/>
                    <col width="20"/>
                    <col/>
                    <col/>
                </colgroup>
                <tr>
                    <th>ロット番号</th>
                    <td><?php echo $this->form->input('TrnSticker.lot_no', array('class'=>'txt', 'id'=>'lot_no')); ?></td>
                    <td></td>
                    <th>有効期限</th>
                    <td><?php echo $this->form->input('TrnSticker.validated_date', array('class'=>'txt','type'=>'text','id'=>'validated_date',"maxlength"=>10)); ?></td>
                    <td></td>
                    <th>備考</th>
                    <td><?php echo $this->form->input('TrnSticker.recital', array('class'=>'txt',"maxlength"=>200)); ?></td>
                </tr>
            </table>
        </div>

        <h2 class="HeaddingSmall">入荷情報</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
                <tr>
                    <th style="width:20px;" rowspan="2"></th>
                    <th width="135">入荷番号</th>
                    <th width="95">入荷日</th>
                    <th class="col10">商品ID</th>
                    <th class="col15">商品名</th>
                    <th class="col15">製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col5">入荷数</th>
                    <th class="col10">作業区分</th>
                    <th class="col10">更新者</th>
                </tr>
                <tr>
                    <th class="col25" colspan="2">仕入先</th>
                    <th class="col10">区分</th>
                    <th class="col15">規格</th>
                    <th class="col15">販売元</th>
                    <th class="col10">仕入単価</th>
                    <th class="col5">残数</th>
                    <th class="col20" colspan="2">備考</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll" style="">
            <table class="TableStyle01">
                <?php $i=0;foreach($Receivings as $_row): ?>
                <tr class="<?php echo($i%2===0?'':'odd'); ?>">
                    <td style="width:20px;" rowspan="2" class="center">
                        <input type="radio" name="data[TrnReceiving][selected]" class="item_select" value="<?php echo($_row['id']); ?>" />
                        <input type="hidden" name="classType" value="<?php echo $_row['class_separation']; ?>" />
                        <input type="hidden" name="perUnit" value="<?php echo $_row['per_unit']; ?>" />
                    </td>
                    <td width="135"><?php echo h_out($_row['work_no']); ?></td>
                    <td width="95"><?php echo h_out(date('Y/m/d', strtotime($_row['work_date'])),'center'); ?></td>
                    <td class="col10"><?php echo h_out($_row['internal_code'] ,'center'); ?></td>
                    <td class="col15"><?php echo h_out($_row['item_name']); ?></td>
                    <td class="col15"><?php echo h_out($_row['item_code']); ?></td>
                    <td class="col10"><?php echo h_out($_row['packing_name']); ?></td>
                    <td class="col5"><?php echo h_out($_row['quantity'],'right'); ?></td>
                    <td class="col10"><?php echo h_out($_row['work_type_name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['user_name']); ?></td>
                </tr>
                <tr class="<?php echo($i%2===0?'':'odd'); ?>">
                    <td colspan="2"><?php echo h_out($_row['facility_name']); ?></td>
                    <td><?php echo h_out($_row['order_type_name'],'center'); ?></td>
                    <td><?php echo h_out($_row['standard']); ?></td>
                    <td><?php echo h_out($_row['dealer_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($_row['stocking_price']),'right'); ?></td>
                    <td><?php echo h_out($_row['remain_count'],'right'); ?></td>
                    <td colspan="2"><?php echo h_out($_row['recital']); ?></td>
                </tr>
                <?php $i++;endforeach; ?>
            </table>
        </div>

        <h2 class="HeaddingSmall">在庫情報</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
                <colgroup>
                    <col width="20"/>
                    <col/>
                    <col/>
                    <col/>
                    <col/>
                    <col/>
                    <col/>
                    <col/>
                </colgroup>
                <tr>
                    <th style="width:20px;"><input type="checkbox" checked class="checkAll" /></th>
                    <th>包装単位</th>
                    <th>定数</th>
                    <th>在庫数</th>
                    <th>要求数</th>
                    <th>引当可</th>
                    <th>最大作成可能数</th>
                    <th>数量</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01" id="stocksTable">
                <colgroup>
                    <col width="20"/>
                    <col/>
                    <col align="right"/>
                    <col align="right"/>
                    <col align="right"/>
                    <col align="right"/>
                    <col align="right"/>
                    <col/>
                </colgroup>
            </table>
        </div>
        <div class="ButtonBox">
            <p class="center"><input type="button" class="btn btn2 fix_btn" id="cmdCommit" /></p>
        </div>
    </div>
</form>
