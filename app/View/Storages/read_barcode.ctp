<script type="text/javascript">
    $(document).ready(function(){
        var submitFlg = true; // 2重サブミット対策
        $('#inputBarcode').attr('disabled' , 'disabled');
        <?php if (isset($isError)){ ?>
        $("#read_barcode_form").attr('action', '<?php echo $this->webroot;?><?php echo $this->name; ?>/beep').submit();
        <?php } ?>

        $('#inputBarcode').keyup(function(e){
            $('#read_barcode_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/read_barcode');
            if(e.which === 13){ //エンターキー押下
                $('#inputBarcode').val(FHConvert.ftoh($('#inputBarcode').val())); //半角化
                if(submitFlg == true ){ // 2重サブミット対策
                    $('#read_barcode_form').submit();
                    submitFlg = false;
                }
                $('#inputBarcode').attr('disabled' , 'disabled');
            }
        });

        $(".xxx").click(function(){
            $('#inputBarcode').focus();
        });
    });

    // 遅い回線だと開始時点でhidden要素が書き込めていないので処理をディレイかける
    $(window).load(function () {
        // Windowを読み込み終わったら処理をする。
        <?php if(isset($this->request->data['isPrintSticker']) && $this->request->data['isPrintSticker'] == true ){ ?>
        $('#read_barcode_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/dummy').submit();
        $('#read_barcode_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/read_barcode');
        <?php } ?>
        // 入力欄を有効にする
        $('#inputBarcode').removeAttr("disabled");
        $('#inputBarcode').val('').focus();
    });
  
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>読取入庫</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>読取入庫</span></h2>

<form class="validate_form" id="read_barcode_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/read_barcode" method="post" onsubmit="return false;">
    <?php echo $this->form->input('TrnStorage.time' , array('type'=>'hidden' , 'value'=> date('Y/m/d H:i:s.u')));?>
    <input type="hidden" name="data[IsRead]" value="1" />
    <input type="hidden" name="data[lastHeaderId]" value="<?php echo((isset($lastHeaderId) ? $lastHeaderId : '')); ?>" />
    <input type="hidden" name="data[facility_name]" value="<?php echo ((isset($facility_name) ? $facility_name : '')); ?>" />
    <?php echo $this->form->input('MstFacility.id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected'))); ?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden')); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>  
    <div class="SearchBox">
        <dl class="FormStyle01">
            <dt>バーコード</dt>
            <dd>
                <?php echo $this->form->input('barcode',array('class' => 'txt r', 'id'=>'inputBarcode')); ?>
            </dd>
            <dt>読込登録種別</dt>
            <dd>
                <?php echo $this->form->radio('isAutoStorage', array('1' => ''), array('label' => '','value' => '1' , 'class'=>'xxx')); ?>自動入庫<br/>
                <font color="red"/>
                ※入荷済のうち、仕入先、包装単位、仕入単価が全て同じの場合は確認画面を表示しません<br/>
                ※バーコードにロット、有効期限の情報が無い場合は確認画面が表示されます<br>
                ※在庫を全て満たしている場合は、小分け単位での出力はできません<br>
                ※小分け指示は１つの単位でしか出力されません<br>
                </font>
                <?php echo $this->form->radio('isAutoStorage', array('0' => ''), array('label' => '','value' => '1', 'class'=>'xxx')); ?>選択入庫<br/>
                <font color="red"/>
                ※どの入荷に対する入庫かを指定できます<br>
                ※確認画面を表示し、小分けする包装単位の配分を指定できます<br>
                ※入庫作業にコメントを設定できます
                </font>
            </dd>
        </dl>
    </div>
</form>
