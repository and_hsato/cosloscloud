<?php $i=0;foreach($Stocks as $_row): ?>
<tr class="<?php echo($i%2===0?'':'odd'); ?>">
    <td style="width:20px;" class="center">
    <?php
    if($_row['is_right_term_pack'] == TRUE){
      if (is_null($_row['fixed_count']) || $_row['fixed_count'] == 0){
        echo $this->form->checkbox("TrnStorage.Rows.{$_row['id']}.isSelected", array('class'=>'center checkAllTarget'));
      } else {
        echo $this->form->checkbox("TrnStorage.Rows.{$_row['id']}.isSelected", array('class'=>'center checkAllTarget', 'checked'=>'checked'));
      }
    }else{
      echo $this->form->text("TrnStock.Rows.noId.selected",array('type'=>'hidden'));
    }
    ?>
    </td>
    <td>
        <label title="<?php echo $_row['packing_name']; ?>">
        <?php echo $_row['packing_name']; ?>
        <?php if($_row['is_right_term_pack'] == TRUE){ ?>
          <input type="hidden" name="stock_per_unit" value="<?php echo $_row['per_unit']; ?>" />
        <?php }else{ ?>
          <input type="hidden" name="noId" value="<?php echo $_row['per_unit']; ?>" />
        <?php } ?>
        </label>
    </td>
    <td><?php echo h_out($_row['fixed_count'],'right'); ?></td>
    <td><?php echo h_out($_row['stock_count'],'right'); ?></td>
    <td><?php echo h_out($_row['promise_count'],'right'); ?></td>
    <td><?php echo h_out($_row['promisable_count'],'right'); ?></td>
    <td>
    <?php if($_row['is_right_term_pack'] == TRUE){ ?>
      <span id="maxCreatableCount_<?php echo $_row['id']; ?>" class="maxCreatableCount"><label title="<?php echo $_row['maxCreatableCount'];; ?>"><p align="right">
    <?php }else{ ?>
      <span><label title="<?php echo $_row['maxCreatableCount'];; ?>"><p align="right">
    <?php }
      echo $_row['maxCreatableCount'];
    ?>
      </p></label></span>
    </td>
    <td>
    <?php
    if($_row['is_right_term_pack'] == TRUE){
      echo $this->form->input("TrnStorage.Rows.{$_row['id']}.quantity", array('class'=>'r num inputQuantity', 'style'=>'width:95%', "maxlength"=>10));
    }else{
      echo $this->form->input("TrnStock.Rows.noId.quantity", array('class'=>'lbl', 'style'=>'text-align: right;','readOnly'=>'readOnly'));
    }
    ?>
    </td>
</tr>
<?php $i++;endforeach; ?>