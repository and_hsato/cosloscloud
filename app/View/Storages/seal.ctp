<stickers>
<?php if(isset($stickers[0][0])){?>
<?php foreach($stickers as $s ){ ?>
<printdata>
    <setting>
        <layoutname>01_センターシール.rpx</layoutname>
        <fixvalues>
            <value name="printer"><?php echo Configure::read('PrinterType.center'); ?></value>
        </fixvalues>
    </setting>
    <datatable>
        <columns><?php echo join("\t", array_keys($s[0])); ?></columns>
        <rows>
            <?php foreach($s as $_row): ?>
            <row><?php echo h(join("\t", array_values($_row))); ?></row>
            <?php endforeach; ?>
        </rows>
    </datatable>
</printdata>
<?php } ?>
<?php }else{ ?>
<printdata>
    <setting>
        <layoutname>01_センターシール.rpx</layoutname>
        <fixvalues>
            <value name="printer"><?php echo Configure::read('PrinterType.center'); ?></value>
        </fixvalues>
    </setting>
    <datatable>
        <columns><?php echo join("\t", array_keys($stickers[0])); ?></columns>
        <rows>
            <?php foreach($stickers as $_row): ?>
            <row><?php echo h(join("\t", array_values($_row))); ?></row>
            <?php endforeach; ?>
        </rows>
    </datatable>
</printdata>
<?php } ?>
<?php if(isset($costStickers) && count($costStickers) > 0) { ?>
<printdata>
    <setting>
        <layoutname>03_コストシール.rpx</layoutname>
        <fixvalues>
            <value name="printer"><?php echo Configure::read('PrinterType.cost'); ?></value>
        </fixvalues>
    </setting>
    <datatable>
        <columns><?php echo join("\t", array_keys($costStickers[0])); ?></columns>
        <rows>
            <?php foreach($costStickers as $_row): ?>
            <row><?php echo h(join("\t", array_values($_row))); ?></row>
            <?php endforeach; ?>
        </rows>
    </datatable>
</printdata>
<?php } ?>
</stickers>