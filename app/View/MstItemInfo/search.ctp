<script type="text/javascript">
    $(function(){
        //選択ボタン押下
        $("#btn_Mod").click(function(){
            $("#TransactionConfigList1").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
        });

        //検索ボタン押下
        $("#btn_Search").click(function(){
            validateDetachSubmit($("#TransactionConfigList1") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
        });
   });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>商品情報一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">商品情報一覧</h2>
<form class="validate_form search_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search" accept-charset="utf-8" id="TransactionConfigList1">
    <?php echo $this->form->input('MstItemInfo.is_search' , array('type'=>'hidden'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_internal_code' , array('type'=>'text' , 'class'=>'txt search_internal_code'))?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_item_code' , array('type'=>'text' , 'class'=>'txt search_upper'))?>
                </td>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_standard' , array('type'=>'text' , 'class'=>'txt search_canna'))?>
                </td>
                <td></td>

            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_item_name' , array('type'=>'text' , 'class'=>'txt search_canna'))?>
                </td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstFacilityItem.search_dealer_name' , array('type'=>'text' , 'class'=>'txt search_canna') ) ?></td>
                <th>JANコード</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_jan_code' , array('type'=>'text' , 'class'=>'txt' ));?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
    </div>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25" />
                <col width="90"/>
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th></th>
                <th>商品ID</th>
                <th>商品名</th>
                <th>規格</th>
                <th>製品番号</th>
                <th>販売元</th>
           </tr>
           <?php foreach ($result as $r) { ?>
           <tr>
               <td class="center"><input class="validate[required] rd"  name="data[MstFacilityItem][id]" id="id_<?php echo $r['MstFacilityItem']['id']; ?>" type="radio" value="<?php echo $r['MstFacilityItem']['id'] ?>"/></td>
               <td><?php echo h_out($r['MstFacilityItem']['internal_code'],'center'); ?></td>
               <td><?php echo h_out($r['MstFacilityItem']['item_name']); ?></td>
               <td><?php echo h_out($r['MstFacilityItem']['standard']); ?></td>
               <td><?php echo h_out($r['MstFacilityItem']['item_code']); ?></td>
               <td><?php echo h_out($r['MstFacilityItem']['dealer_name']); ?></td>
           </tr>
           <?php } ?>
           <?php if(count($result) == 0 && isset($this->request->data['MstItemInfo']['is_search'])){?>
           <tr><td colspan="6" class="center">該当するデータがありませんでした。</td></tr>
           <?php } ?>
       </table>
    </div>
    <?php if(count($result) > 0 ){?>
    <div class="ButtonBox">
        <input type="button" class="btn btn4" id="btn_Mod"/>
    </div>
    <?php } ?>
</form>
