<script>

</script>

<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
          <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">採用品選択</a></li>
          <li>商品情報</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">商品情報</h2>

<form class="validate_form" method="post" id="modForm">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.internal_code' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('MstFacilityItem.id' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_code' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;'  , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_name' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                   <?php echo $this->form->input('MstFacilityItem.dealer_name' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.standard' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>JANコード</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.jan_code' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>償還価格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.refund_price' , array('type'=>'text' , 'class'=>'right lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>保険請求区分</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.insurance_claim_department_name' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th></th>
                <td></td>
                <td></td>
                <th>保険請求名称</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.insurance_claim_name_s' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>

        </table>
        <div class="results">
            <div class="TableScroll">
                <table class="TableStyle01 table-even">
                    <thead>
                        <tr>
                            <th class="col15">適用開始日</th>
                            <th class="col15">適用終了日</th>
                            <th>仕入先</th>
                            <th>仕入先2</th>
                            <th class="col15">包装単位</th>
                            <th class="col5">適用</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i=0; foreach($TransactionConfig as $trn){ ?>
                        <tr>
                            <td><?php echo h_out($trn['MstTransactionConfig']['start_date'],'center'); ?></td>
                            <td><?php echo h_out($trn['MstTransactionConfig']['end_date'],'center'); ?></td>
                            <td><?php echo h_out($trn['MstTransactionConfig']['partner_facility_name']); ?></td>
                            <td><?php echo h_out($trn['MstTransactionConfig']['partner_facility_name2']); ?></td>
                            <td><?php echo h_out($trn['MstTransactionConfig']['unit_name']); ?></td>
                            <td><?php echo h_out($trn['MstTransactionConfig']['is_deleted'],'center');?></td>
                        </tr>
                    <?php $i++; } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <h2 class="HeaddingSmall">販売設定</h2>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <colgroup>
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>販売先</th>
                    <th>適用開始日</th>
                    <th>包装単位</th>
                    <th>売上単価</th>
                    <th>定価</th>
                    <th>値引き価格</th>
                    <th>値引き率</th>
                </tr>
                <?php foreach ($SalesConfig as $sales) { ?>
                <tr>
                    <td><?php echo h_out($sales['MstSalesConfig']['facility_name']); ?></td>
                    <td><?php echo h_out($sales['MstSalesConfig']['start_date'],'center'); ?></td>
                    <td><?php echo h_out($sales['MstSalesConfig']['unit_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($sales['MstSalesConfig']['sales_price']),'right'); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($sales['MstSalesConfig']['unit_price']),'right'); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($sales['MstSalesConfig']['cut_price']),'right'); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($sales['MstSalesConfig']['cut_rate']) . '%','right'); ?></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</form>
