<script  type="text/javascript">
$(document).ready(function(){
    //出荷伝票印刷ボタン押下
    $("#invoice").click(function(){
        $("#form_add_result").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/d2shipping').submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/d2promises_add" >直納品登録</a></li>
        <li class="pankuzu">採用品選択</li>
        <li class="pankuzu">直納品登録確認</li>
        <li>直納品登録結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>直納品登録結果</span></h2>
<div class="Mes01">直納品の登録を行いました</div>

<form class="validate_form" id="form_add_result" action="#" method="post">
    <?php echo $this->form->input('Edi.id' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$centerId));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>作業日付</th>
                <td><?php echo $this->form->input('d2promises.work_date' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('d2promises.recital1' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('d2promises.hospitalName' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>診療科</th>
                <td><?php echo $this->form->input('d2promises.recital2' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td><?php echo $this->form->input('d2promises.departmentName' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>手術名</th>
                <td><?php echo $this->form->input('d2promises.recital3' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
        </table>
    </div>

    <hr class="Clear" />
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
        <span class="DisplaySelect">
            表示件数：<?php echo count($FacilityItemList);  ?>件
        </span>
    </div>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">参考単価</th>
                <th class="col15">ロット番号</th>
                <th class="col10">遡及除外</th>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <th>規格</th>
                <th>販売元</th>
                <th>数量</th>
                <th>単価</th>
                <th>有効期限</th>
                <th>備考</th>
            </tr>
        <?php $cnt=0; foreach($FacilityItemList as $f) { ?>
            <?php if(false === isset($f['internal_code'])) continue; ?>
            <tr>
                <td><?php echo h_out($f['internal_code'],'center'); ?></td>
                <td><?php echo h_out($f['item_name']); ?></td>
                <td><?php echo h_out($f['item_code']); ?></td>
                <td><?php echo h_out($f['packing_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($f['reference_transaction_price']),'right'); ?></td>
                <td><input type="text" class="tbl_lbl" value="<?php echo $f['lot_no']; ?>" readonly="readonly" /></td>
                <td><?php echo h_out( (isset($f['sokyu']) && $f['sokyu'] == '1') ? '○' : '' ,'center'); ?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($f['standard']); ?></td>
                <td><?php echo h_out($f['dealer_name']); ?></td>
                <td><input type="text" class="tbl_lbl num right" value="<?php echo number_format($f['quantity']); ?>" readonly="readonly" /></td>
                <td><input type="text" class="num tbl_lbl right" value="<?php echo number_format($f['transaction_price'], 2); ?>" readonly="readonly" /></td>
                <td><input type="text" class="tbl_lbl" value="<?php echo $f['validated_date']; ?>" readonly="readonly" /></td>
                <td><input type="text" class="tbl_lbl" value="<?php echo $f['recital']; ?>" readonly="readonly" /></td>
            </tr>
            <?php $cnt++; } ?>
        </table>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
       <p class="center">
            <input type="button" class="btn btn27" id="invoice"/>
        </p>
    </div>
</form>
