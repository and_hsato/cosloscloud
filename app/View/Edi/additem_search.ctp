<script type="text/javascript">
$(function(){
    // 検索ボタン押下
    $("#search_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/additem_search');
        $("#search_form").attr('method', 'post');
        $("#search_form").submit();
    });
    // 独自採用品登録ボタン押下
    $("#add_own_items_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/additem_original');
        $("#search_form").attr('method', 'post');
        $("#search_form").submit();
    });
    // 選択ボタン押下
    $("#add_btn").click(function(){
        if( $('input.chk:checked').length > 0 ) {
            if($('input.chk:checked').next().val() != ''){
                if(window.confirm("すでに採用品登録されています。\n処理を続行してもよろしいですか？")){
                    $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/additem');
                    $("#search_form").attr('method', 'post');
                    $("#search_form").submit();
                }else{
                    return false;
                }
            }else{
                $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/additem');
                $("#search_form").attr('method', 'post');
                $("#search_form").submit();
            }
        }else{
            alert('採用品が選択されていません。');
        }
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>施設採用品一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>施設採用品一覧</span></h2>
<h2 class="HeaddingMid">採用する商品を選択してください</h2>
<form class="validate_form search_form input_form" id="search_form" method="post">
    <?php echo $this->form->input('search.is_search' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('centerId' , array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <div class="ColumnLeft">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>得意先</th>
                    <td>
                        <?php echo $this->form->input('hospitalName' , array('type'=>'hidden' , 'id'=>'facilityName')); ?>
                        <?php echo $this->form->input('hospitalText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'r validate[required]', 'style'=>'width: 60px;')); ?>
                        <?php echo $this->form->input('hospitalCode',array('options'=>$hospital_list,'id' => 'facilityCode', 'class' => 'txt r', 'empty' => true)); ?>
                    </td>
                </tr>
                <tr>
                    <th>マスターID</th>
                    <td><?php echo $this->form->text('MstGrandmaster.master_id' ,array('class' => 'txt','maxlength' => '50' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th>JANコード</th>
                    <td><?php echo $this->form->text('MstGrandmaster.jan_code' ,array('class' => 'txt','maxlength' => '50' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th>大分類</th>
                    <td><?php echo $this->form->input('MstGrandmaster.item_category1', array('options'=>$item_category1 , 'class'=>'txt' , 'id'=>'item_category1' , 'empty'=>true)); ?></td>
                    <th></th>
                    <td><?php echo $this->form->input('MstGrandmaster.isAccepted' , array('type'=>'checkbox' , 'hiddenField'=>false))?>採用済は表示しない</td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->text('MstGrandmaster.item_name',array('class' => 'txt search_canna','maxlength' => '50', 'label' =>'') );?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->text('MstGrandmaster.item_code',array('class' => 'txt search_upper','maxlength' => '50','label' =>'' ));?></td>
                    <td></td>
                    <th>中分類</th>
                    <td><?php echo $this->form->input('MstGrandmaster.item_category2', array('options'=>$item_category2 , 'class'=>'txt' , 'id'=>'item_category2' , 'empty'=>true)); ?></td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->text('MstGrandmaster.standard',array('class' => 'txt search_canna','maxlength' => '50' ,'label' =>'') ); ?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->text('MstGrandmaster.dealer_code',array('class' =>'txt','maxlength' => '50', 'label' =>'' )); ?></td>
                    <td></td>
                    <th>小分類</th>
                    <td><?php echo $this->form->input('MstGrandmaster.item_category3', array('options'=>$item_category3 , 'class'=>'txt' , 'id'=>'item_category3' , 'empty'=>true)); ?></td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="search_btn" />
        <input type="button" id="add_own_items_btn" class="btn btn84" />
    </div>

    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <?php echo $this->element('limit_combobox',array('result'=>count($gm_items))) ; ?>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th style="width: 20px;"></th>
                <th style="width: 135px;" >マスターID</th>
                <th style="width: 135px;" >ＪＡＮコード</th>
                <th>商品名</th>
                <th>規格</th>
                <th>製品番号</th>
                <th>販売元</th>
                <th>単位</th>
                <th style="width: 50px;" >採用済</th>
            </tr>
            <?php $i = 0; foreach ($gm_items as $gmi){ ?>
            <?php if($gmi['MstGrandmaster']['accepted'] == ''){$class='';}else{$class='tr-deleted';};?>
            <tr class="<?php echo $class; ?>">
                <td class="center">
                    <input type="radio" name="data[MstGrandmaster][opt]" class="chk" value="<?php echo $gmi['MstGrandmaster']['master_id']; ?>"/>
                    <?php echo $this->form->text('MstGrandmaster.Accepted' , array('type'=>'hidden' , 'class'=>'hasAccepted' , 'value'=>$gmi['MstGrandmaster']['accepted'] )); ?>
                </td>
                <td><?php echo h_out($gmi['MstGrandmaster']['master_id'],'center'); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['jan_code'],'center'); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['item_name']); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['standard']); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['item_code']); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['dealer_name']); ?></td>
                <td><?php echo h_out($gmi['MstGrandmaster']['unit_name']); ?></td>
                <td style="width: 50px;"><?php echo h_out($gmi['MstGrandmaster']['hasAccepted'],'center'); ?></td>
            </tr>
            <?php $i++; } ?>
            <?php if(isset($this->request->data['search']['is_search']) && count($gm_items) == 0){ ?>
            <tr><td colspan="8" class="center">該当するデータはありません。</td></tr>
            <?php } ?>
        </table>
    </div>

    <?php if (count($gm_items) != 0){ ?>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="ButtonBox"><input type="button" class="btn btn4" id="add_btn"/></div>
<?php } ?>
</form>