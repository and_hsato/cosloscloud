<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    
    //残数一括設定
    $('#cmdSetRemainCount').click(function(){
        $('input[type=text].quantity').each(function(){
            $(this).val($(this).parent(':eq(0)').find('input[type=hidden]:eq(0)').val());
        });
    });

    $('#confirm_btn').click(function(){
        if($('input[type=checkbox].checkAllTarget:checked').length === 0){
            alert('出荷登録を行う商品を選択してください');
            return false;
        }else{
            var hasError = false;
            var remainArray = {};
            $('input[type=checkbox].checkAllTarget').each(function(i){
                if(this.checked){
                    var input = $('#containTables');
                    //数量
                    if(input.find('input[type=text].quantity:eq(' + i + ')').val() == ''){
                        alert((i+1) + '行目の、数量を入力してください');
                        hasError = true;
                        return false;
                    }else if(!input.find('input[type=text].quantity:eq(' + i + ')').val().match(/^[1-9][0-9]*/)){
                        alert((i+1) + '行目の、数量の入力が不正です');
                        hasError = true;
                        return false ;
                    }

                    input.find('input[type=text].validated_date:eq(' + i + ')').val( convertDate( input.find('input[type=text].validated_date:eq(' + i + ')').val() ) );
  
                    var validated_date = input.find('input[type=text].validated_date:eq(' + i + ')').val();
                    if(validated_date != ''){
                        if(!dateCheck(validated_date)){
                            alert((i+1) +'行目の、有効期限の入力が不正です。' + validated_date);
                            hasError = true;
                            return false;
                        }
                    }
                    if(remainArray[this.value] == undefined){
                        //データなし
                        remainArray[this.value] = parseInt(input.find('input[type=text].quantity:eq(' + i + ')').val());
                    } else {
                        //データあり
                        remainArray[this.value] = parseInt(remainArray[this.value]) + parseInt(input.find('input[type=text].quantity:eq(' + i + ')').val());
                    }
                }
            });
            //残数チェック
            for (var id in remainArray) {
                if ( remainArray[id] > $('input#remain'+id).val()) {
                    alert('残数を超える数量が設定されています。');
                    hasError = true;
                    return;
                }
            }
  
            if(true === hasError){
                return false;
            }else{
                $('#confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/shipping_result');
                $('#confirm').attr('method', 'post');
                $('#confirm').submit();
            }
        }
    });

    //追加ボタン押下
    $('.add_btn').click(function(){
        var add = $(this).attr('id');
        var class_separation = $('input#facility_name'+add).val();
        var r_class = '';
        if(class_separation >= 3 ){
            r_class = "r";
        }

        var row_number = $('input[type=checkbox].checkAllTarget').length + 1;
  
        $('#inputTable'+add).append(
            $('<tr>').append(
                $('<td rowspan="2" class="col5 center">').append('<input type="checkbox" class="checkAllTarget id" checked="checked" name="data[Edi][id]['+row_number+']" value="'+ $('input#id'+add).val() +'">')
                ).append(
                    $('<td class="col25" colspan="2">').append(
                        $('<label>').text($('input#facility_name'+add).val()).attr('title' ,$('input#facility_name'+add).val() )
                    )
                ).append(
                    $('<td class="col10">').append(
                        $('<label>').append(
                            $('<p align="center">').text($('input#internal_code'+add).val())
                        ).attr('title',$('input#internal_code'+add).val())
                    )
                ).append(
                    $('<td class="col10">').append(
                        $('<label>').text($('input#item_name'+add).val()).attr('title' ,$('input#item_name'+add).val() )
                    )
                ).append(
                    $('<td class="col20">').append(
                        $('<label>').text($('input#item_code'+add).val()).attr('title' , $('input#item_code'+add).val())
                     )
                 ).append(
                     $('<td class="col10">').append(
                         $('<label>').text($('input#unit_name'+add).val()).attr('title' ,$('input#unit_name'+add).val() )
                     )
                 ).append(
                     $('<td class="col10">').append(
                         $('<input type="text" maxlength="13" class="txt ' + r_class + '" name="data[Edi][lot_no]['+row_number+']"/>')
                     )
                 ).append(
                     $('<td class="col10">').append(
                         $('<input type="text" maxlength="13" class="date validated_date ' + r_class + '" name="data[Edi][validated_date]['+row_number+']"/>')
                     )
                 )
             ).append(
                 $('<tr>').append(
                     $('<td>')
                 ).append(
                     $('<td>').append(
                         $('<label>').text($('input#work_no'+add).val()).attr('title' , $('input#work_no'+add).val())
                     )
                 ).append(
                    $('<td>').append(
                        $('<label>').append(
                            $('<p class="center">').text($('input#work_date'+add).val())
                        ).attr('title',$('input#work_date'+add).val())
                    )
                 ).append(
                     $('<td>').append(
                         $('<label>').text($('input#standard'+add).val()).attr('title' , $('input#standard'+add).val())
                     )
                 ).append(
                     $('<td>').append(
                         $('<label>').text($('input#dealer_name'+add).val()).attr('title' , $('input#dealer_name'+add).val())
                     )
                 ).append(
                    $('<td>').append(
                        $('<label>').append(
                            $('<p class="right">').text($('input#remain_count'+add).val())
                        ).attr('title',$('input#remain_count'+add).val())
                    )
                 ).append(
                     $('<td>').append(
                         $('<input type="text" class="r txt num quantity quantity' + add + '" name="data[Edi][quantity]['+row_number+']">')
                     )
                 ).append(
                     $('<td colspan="2">').append(
                         $('<input type="text" maxlength="200" class="txt" name="data[Edi][comment]['+row_number+']"  />')
                     )
                 )
             )

             //追加要素にカレンダーを追加
             $('.date').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});

             //ストライプ塗りなおし
             r_count = 0;
             $('table > tbody > tr').each(function(){
                 cnt = Math.floor(r_count / 2 );
                 if((cnt % 2 )!= 0){
                     $(this).addClass('odd');
                 }else{
                     $(this).removeClass('odd');
                 }
                 r_count++;
             });
    });

  
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>出荷登録</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>出荷登録</span></h2>
<?php echo $this->form->create('Edi',array('class'=>'validate_form search_form','id' =>'confirm')); ?>
    <?php echo $this->form->input('Edi.token' , array('type'=>'hidden'));?>
<table style="width: 100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
<h2 class="HeaddingMid">出荷情報</h2>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>出荷日</th>
                <td><?php echo($this->form->text('Edi.work_date',array('class'=>'r date validate[required,custom[date]]','maxlength'=>'10','id'=>'work_date','label'=>''))); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo($this->form->text('Edi.recital',array('class'=>'txt','label'=>'',"maxlength"=>200))); ?></td>
            </tr>
        </table>
    </div>

    <div class="SelectBikou_Area">
        <span class="DisplaySelect">
        </span>
        <span class="BikouCopy">
            <input type="button" class="btn btn61 [p2]" id="cmdSetRemainCount"/>
        </span>
    </div>

<div class="TableHeaderAdjustment01">
    <table class="TableHeaderStyle02">
        <thead>
            <tr>
                <th rowspan="2" class="col5">
                    <input type="checkbox"  checked="checked" class='checkAll_1'/>
                </th>
                <th class="col25" colspan="2">施設名</th>
                <th class="col10">商品ID</th>
                <th class="col20">商品名</th>
                <th class="col10">製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col10">有効期限</th>
            </tr>
            <tr>
                <th></th>
                <th>受注番号</th>
                <th>受注日</th>
                <th>規格</th>
                <th>販売元</th>
                <th>残数</th>
                <th>数量</th>
                <th>備考</th>
            </tr>
        </thead>
    </table>
</div>
<div class="TableScroll" id="containTables">
<?php
    $i = 0;
    foreach($SearchResult as $item){
       if($i%2==0){$odd="";} else {$odd="odd";}
       $class = '';
       // クラス分類3の以上の場合には、ロットUBDを必須にする。
       if($item['Edi']['class_separation'] >= 3 ){
           $class = 'r';
       }
?>
    <?php echo $this->form->input('hidden.id.'.$i , array('type'=>'hidden' ,'id'=>'id'.$i, 'value'=>$item['Edi']['id']) );?>
    <?php echo $this->form->input('hidden.facility_name.'.$i , array('type'=>'hidden' ,'id'=>'facility_name'.$i, 'value'=>$item['Edi']['facility_name'] . ' / ' . $item['Edi']['department_name']) );?>
    <?php echo $this->form->input('hidden.internal_code.'.$i , array('type'=>'hidden' ,'id'=>'internal_code'.$i, 'value'=>$item['Edi']['internal_code']) );?>
    <?php echo $this->form->input('hidden.item_name.'.$i , array('type'=>'hidden' ,'id'=>'item_name'.$i, 'value'=>$item['Edi']['item_name']) );?>
    <?php echo $this->form->input('hidden.item_code.'.$i , array('type'=>'hidden' ,'id'=>'item_code'.$i, 'value'=>$item['Edi']['item_code']) );?>
    <?php echo $this->form->input('hidden.unit_name.'.$i , array('type'=>'hidden' ,'id'=>'unit_name'.$i, 'value'=>$item['Edi']['unit_name']) );?>
    <?php echo $this->form->input('hidden.work_no.'.$i , array('type'=>'hidden' ,'id'=>'work_no'.$i, 'value'=>$item['Edi']['work_no']) );?>
    <?php echo $this->form->input('hidden.work_date.'.$i , array('type'=>'hidden' ,'id'=>'work_date'.$i, 'value'=>$item['Edi']['work_date']) );?>
    <?php echo $this->form->input('hidden.standard.'.$i , array('type'=>'hidden' ,'id'=>'standard'.$i, 'value'=>$item['Edi']['standard']) );?>
    <?php echo $this->form->input('hidden.dealer_name.'.$i , array('type'=>'hidden' ,'id'=>'dealer_name'.$i, 'value'=>$item['Edi']['dealer_name']) );?>
    <?php echo $this->form->input('hidden.remain_count.'.$i , array('type'=>'hidden' ,'id'=>'remain_count'.$i, 'value'=>$item['Edi']['remain_count']) );?>
    <?php echo $this->form->input('calc.remain_count.'.$i , array('type'=>'hidden' ,'id'=>'remain'.$item['Edi']['id'], 'value'=>$item['Edi']['remain_count']) );?>
  
    <table id="inputTable<?php echo $i ?>" class="TableStyle02" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
        <tbody>
            <tr class="<?php echo $odd; ?>">
                <td rowspan="2" class="col5 center">
                    <?php echo $this->form->checkbox('Edi.id.'.$i , array('value'=>$item['Edi']['id'] , 'class' => 'checkAllTarget id' , 'checked'=>'checked' , 'hiddenField'=>false) ); ?>
                </td>
                <td class="col25" colspan="2"><?php echo h_out($item['Edi']['facility_name'] . ' / ' . $item['Edi']['department_name']); ?></td>
                <td class="col10"><?php echo h_out($item['Edi']['internal_code'],'center'); ?></td>
                <td class="col20"><?php echo h_out($item['Edi']['item_name']); ?></td>
                <td class="col10"><?php echo h_out($item['Edi']['item_code']); ?></td>
                <td class="col10"><?php echo h_out($item['Edi']['unit_name']); ?></td>
                <td class="col10"><?php echo $this->form->input('Edi.lot_no.'.$i , array('type'=>'text' , 'class'=>'txt ' . $class ))?></td>
                <td class="col10"><?php echo $this->form->input('Edi.validated_date.'.$i , array('type'=>'text' , 'class'=>'validated_date date ' . $class ))?></td>
           </tr>
           <tr class="<?php echo $odd; ?>">
                <td class="center"><input type='button' class="btn btn55 add_btn" id="<?php echo $i; ?>"></td>
                <td><?php echo h_out($item['Edi']['work_no']); ?></td>
                <td><?php echo h_out($item['Edi']['work_date'],'center'); ?></td>
                <td><?php echo h_out($item['Edi']['standard']); ?></td>
                <td><?php echo h_out($item['Edi']['dealer_name']); ?></td>
                <td><?php echo h_out($item['Edi']['remain_count'],'right'); ?></td>
                <td><?php echo $this->form->input('Edi.quantity.'.$i , array('type'=>'text' , 'class'=>'r txt num quantity quantity' . $item['Edi']['id']  ))?>
                    <?php echo $this->form->input('Edi.remain_count.'.$i , array('type'=>'hidden' , 'id'=>'remain_count' . $item['Edi']['id'] ,  'class'=>'remain_count' , 'value'=>$item['Edi']['remain_count'] ))?>
                </td>
                <td><?php echo $this->form->input('Edi.comment.'.$i , array('type'=>'text' , 'class'=>'txt' ))?></td>
           </tr>
        </tbody>
    </table>
<?php $i++; } ?>
</div>
<table style="width: 100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
<div class="ButtonBox">
    <p class="center">
        <input type="button" id="confirm_btn" class="common-button" value="確定"/>
    </p>
</div>
<?php echo $this->form->end(); ?>
