<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/receiving_history">受注履歴</a></li>
        <li>受注履歴編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>受注履歴編集</span></h2>
<div class="Mes01">以下の受注を取り消しました。</div>
<form method="post" action="" accept-charset="utf-8" id="RecivingList" class="validate_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
        </table>
    </div>
    <div class="results">
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
            <span class="DisplaySelect">
            　表示件数：<?php echo count($result);  ?>件
            </span>
        </div>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="25" />
                    <col width="140" />
                    <col width="90" />
                    <col width="100" />
                    <col />
                    <col />
                    <col width="85"/>
                    <col width="140"/>
                    <col width="100"/>
                </colgroup>
                <tr>
                    <th rowspan="2"></th>
                    <th>受注番号</th>
                    <th>受注日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>作業区分</th>
                    <th>備考</th>
                </tr>
                <tr>
                    <th colspan="2">受注施設</th>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                    <th>発注番号</th>
                    <th>更新者</th>
                </tr>
                <?php $cnt=0; foreach ($result as $r) { ?>
                <tr>
                    <td rowspan="2" class="center"></td>
                    <td><?php echo h_out($r['Edi']['work_no']); ?></td>
                    <td><?php echo h_out($r['Edi']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($r['Edi']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Edi']['item_name']); ?></td>
                    <td><?php echo h_out($r['Edi']['item_code']); ?></td>
                    <td><?php echo h_out($r['Edi']['unit_name']); ?></td>
                    <td></td>
                    <td><?php echo h_out($r['Edi']['recital']); ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($r['Edi']['facility_name'] . ' / ' . $r['Edi']['department_name']); ?></td>
                    <td></td>
                    <td><?php echo h_out($r['Edi']['standard']); ?></td>
                    <td><?php echo h_out($r['Edi']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['Edi']['quantity'] , 'right'); ?></td>
                    <td><?php echo h_out($r['Edi']['order_work_no'] , 'center'); ?></td>
                    <td><?php echo h_out($r['Edi']['user_name']); ?></td>
                </tr>
                <?php $cnt++; } ?>
            </table>
        </div>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
</form>
