<script type="text/javascript">
$(function(){
    //選択ボタン押下
    $("#btn_select").click(function(){
        $("#form_add").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/d2promises_item_select').submit();
    });
    //シールボタン押下
    $("#btn_seal").click(function(){
        $("#form_add").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/d2promises_read_sticker').submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>直納品登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>直納品登録</span></h2>

<form class="validate_form input_form" id="form_add" method="post" action="">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>直納日付</th>
                <td><?php echo $this->form->input('d2promises.work_date' , array('type'=>'text' , 'class'=>'r validate[optional,custom[date],funcCall2[date2]] date' , 'id'=>'datepicker1') );?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('d2promises.recital1' , array('type'=>'text' ,'class'=>'txt' , 'maxlength'=>'200'))?></td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('d2promises.hospitalName' , array('type'=>'hidden' , 'id'=>'facilityName')); ?>
                    <?php echo $this->form->input('d2promises.hospitalText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'r validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('d2promises.hospitalCode',array('options'=>$hospital_list,'id' => 'facilityCode', 'class' => 'txt r', 'empty' => true)); ?>
                </td>
                <td></td>
                <th>診療科</th>
                <td><?php echo $this->form->input('d2promises.recital2' , array('type'=>'text' ,'class'=>'txt' , 'maxlength'=>'200'))?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('d2promises.departmentName' , array('type'=>'hidden' , 'id'=>'departmentName')); ?>
                    <?php echo $this->form->input('d2promises.departmentText', array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'r validate[required]', 'style' => 'width: 60px;')); ?>
                    <?php echo $this->form->input('d2promises.departmentCode',array('options'=>array(),'id' => 'departmentCode', 'class' => 'txt validate[required] r', 'empty' => true)); ?>
                </td>
                <td></td>
                <th>手術名</th>
                <td><?php echo $this->form->input('d2promises.recital3' , array('type'=>'text' ,'class'=>'txt' , 'maxlength'=>'200'))?></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn58 submit [p2]" id="btn_select" />
            <input type="button" class="btn btn46 submit [p2]" id="btn_seal"/>
        </p>
    </div>
</form>