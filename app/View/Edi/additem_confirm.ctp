<script type="text/javascript">
$(document).ready(function(){
    $('.date').datepicker('destroy');
    $('select , input[type=text]').attr("readonly","readonly").removeClass('r txt date' ).addClass("lbl");
    $('input[type=button]').css("display","none");
    $('#submit_btn').css("display","inline");
    $('select').each(function(){
        $(this).after('<input type=hidden name=' + $(this).attr('name') + ' value=' + $(this).val() + ' >');
        $(this).attr('disabled' , 'disabled');
    });
    $('input[type=checkbox]').each(function(){
        if($(this).attr('checked')){
            $(this).after('<input type=hidden name=' + $(this).attr('name') + ' value=' + $(this).val() + ' >');
        }
        $(this).attr('disabled' , 'disabled');
    });

    $("#submit_btn").click(function(){
        $(window).unbind('beforeunload');
        $("#confirm_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/additem_result');
        $("#confirm_form").attr('method', 'post');
        $("#confirm_form").submit();
    });
  
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
  
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/additem_search">施設採用品一覧</a></li>
        <li class="pankuzu">施設採用品登録</li>
        <li>施設採用品確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>施設採用品確認</span></h2>
<h2 class="HeaddingMid">登録する内容を確認してください</h2>
<h2 class="HeaddingSmall">得意先 [ <?php echo $this->request->data['hospitalName']; ?> ]</h2>
<h2 class="HeaddingSmall">施設採用品情報</h2>
<form class="validate_form" id="confirm_form">
    <div>
        <?php echo $this->form->input('MstFacilityItem.isOriginal', array('type'=>'hidden')); ?>
        <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
        <?php echo $this->form->input('centerId', array('type'=>'hidden')); ?>
        <?php echo $this->form->input('hospitalName', array('type'=>'hidden')); ?>
        <?php echo $this->element('masters/facility_items/item_Table');?>
    </div>
    <?php if ($this->request->data['MstFacilityItem']['isOriginal'] != 1): ?>
    <h2 class="HeaddingSmall">グランド基本単位</h2>
    <div class="SearchBox">
        <?php echo $this->form->input('MstItem.per_unit',array('type'=>'hidden','value'=>$this->request->data['MstItem']['per_unit'])); ?>
        <?php echo $this->form->input('MstItem.unit_name',array('type'=>'hidden','value'=>$this->request->data['MstItem']['unit_name'])); ?>
        <?php echo $this->form->input('MstFacilityItem.unit_name',array('type'=>'hidden')); ?>
        <table class="FormStyleTable">
            <tr>
                <th>グランドマスタ単位</th>
                <td>
                    <?php echo $this->form->input('MstGrandmaster.unit_name',array('type'=>'hidden','value'=>$this->request->data['MstGrandmaster']['unit_name'])); ?>
                    <?php echo $this->request->data['MstGrandmaster']['unit_name']; ?>
                </td>
                <td width="20"></td>
                <th>
                    参考包装単位（<?php echo $this->request->data['MstItem']['per_unit']; ?><?php echo ($this->request->data['MstGrandmaster']['unit_name'] != '') ? $this->request->data['MstGrandmaster']['unit_name']:''; ?>／箱）
                </th>
                <td></td>
            </tr>
        </table>
    </div>
    <?php endif;?>
    <h2 class="HeaddingSmall">採用基本単位</h2>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th></th>
                <th></th>
            </tr>
        </table>
        <table class="FormStyleTable">
            <tr>
                <th></th>
                <th>
                    <table>
                        <tr>
                            <td>基本単位</td>
                            <td>換算数</td>
                            <td><?php if ($this->request->data['MstFacilityItem']['isOriginal'] != 1): ?>グランドマスタ単位<?php endif; ?></td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo $this->form->input('MstFacilityItem.mst_unit_name_id', array('type'=>'hidden','value'=>$this->request->data['MstFacilityItem']['mst_unit_name_id'])); ?>
                                <?php echo $this->form->input('MstFacilityItem.mst_unit_name_id', array('options'=>$item_units, 'value'=>$this->request->data['MstFacilityItem']['mst_unit_name_id'], 'class' => 'lbl', 'style'=>'width: 120px;','disabled'=>'disabled')); ?>
                                &nbsp;&nbsp;＝
                            </td>
                            <td>
                            <?php
                            $_per_unit =(isset($this->request->data['MstFacilityItem']['per_unit']) && $this->request->data['MstFacilityItem']['per_unit'] != '') ? $this->request->data['MstFacilityItem']['per_unit']:1;
                            echo $this->form->input('MstFacilityItem.per_unit', array('type'=>'hidden','value'=>$_per_unit,'class'=>'lbl num','style'=>'width:60px;','value'=>1)); ?>
                            &nbsp;
                            <?php echo $this->form->input('MstFacilityItem.converted_num', array('id'=>'ReciprocalNumber','label'=>false,'div'=>false,'maxlength'=>50,'class'=>'lbl','style'=>'width:60px;','readonly'=>'readonly','value'=>$this->request->data['MstFacilityItem']['converted_num'])); ?>
                             &nbsp;&nbsp;
                            </td>
                            <td>
                                <?php 
                                if ($this->request->data['MstFacilityItem']['isOriginal'] != 1)
                                echo $this->request->data['MstGrandmaster']['unit_name']; 
                                ?>
                            </td>
                        </tr>
                    </table>
                </th>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn2 [p2]"  id="submit_btn" /></p>
    </div>
</form>
