<script  type="text/javascript">
$(document).ready(function() {
    //検索ボタン押下
     $("#btn_Search").click(function(){
         $("#EdiSearch").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/shipping_history').submit();
     });
    //CSVボタン押下
    $("#btn_Csv").click(function(){
         $("#EdiSearch").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/shipping').submit();
    });
    //部署シール印刷ボタン押下
    $("#hospital").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#EdiSearch").attr('action', '<?php echo $this->webroot; ?>PdfReport/sticker').attr('target','_blank').submit();
       } else {
           alert('必ず一つは選択してください');
       }
    });
    //出荷伝票ボタン押下
    $("#invoice").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#EdiSearch").attr('action', '<?php echo $this->webroot; ?>PdfReport/shipping').attr('target','_blank').submit();
        } else {
            alert('必ず一つは選択してください');
        }
    });

    //明細表示ボタン押下
    $("#btn_Conf").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            $("#EdiSearch").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/shipping_history_confirm').submit();
        } else {
            alert('必ず一つは選択してください');
        }
    });

    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>出荷履歴</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>出荷履歴</span></h2>
<h2 class="HeaddingMid">変更を行いたい出荷履歴を検索してください</h2>
    <form method="post" action="" accept-charset="utf-8" id="EdiSearch" class="validate_form search_form">
        <?php echo $this->form->input('Edi.is_search' , array('type'=>'hidden'))?>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>出荷番号</th>
                    <td><?php echo $this->form->input('Edi.search_receiving_no' , array('class'=>'txt'))?></td>
                    <td></td>
                    <th>出荷日</th>
                    <td>
                        <?php echo $this->form->input('Edi.search_start_date' , array('type'=>'text' , 'class'=>'txt validate[optional,custom[date]] date' , 'id'=>'datepicker1'))?>
                      &nbsp;～&nbsp;
                        <?php echo $this->form->input('Edi.search_end_date' , array('type'=>'text' , 'class'=>'txt validate[optional,custom[date]] date' , 'id'=>'datepicker2'))?>
                    </td>
                    <td></td>
                    <th></th>
                    <td>
                      <?php echo $this->form->input('Edi.search_display_deleted' , array('type'=>'checkbox'))?>取消も表示する
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>出荷施設</th>
                    <td>
                        <?php echo $this->form->input('Edi.mst_facility_code_txt',array('class'=>'txt require','style'=>'width:60px;','id'=>'facilityText')); ?>
                        <?php echo $this->form->input('Edi.mst_facility_code',array('options'=>$facility_list,'empty'=>true,'class'=>'txt','style'=>'width:120px;','id'=>'facilityCode')); ?>
                    </td>
                    <td></td>
                    <th>出荷部署</th>
                    <td>
                        <?php echo $this->form->input('Edi.mst_department_code_txt',array('class'=>'txt require','style'=>'width:60px;','id'=>'departmentText')); ?>
                        <?php echo $this->form->input('Edi.mst_department_code',array('options'=>$department_list,'empty'=>true,'class'=>'txt','style'=>'width:120px;','id'=>'departmentCode')); ?>
                    </td>
                    <td></td>
                    <th></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品ID</th>
                    <td><?php echo $this->form->input('Edi.search_internal_code' , array('class'=>'txt search_internal_code'))?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->input('Edi.search_item_code' , array('class'=>'txt search_upper'))?></td>
                    <td></td>
                    <th></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->input('Edi.search_item_name' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->input('Edi.search_dealer_name' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                    <th></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->input('Edi.search_standard' , array('class'=>'txt search_canna'))?></td>
                    <td></td>
                    <th>作業区分</th>
                    <td>
                        <?php echo $this->form->input('Edi.search_classId' , array('options'=>$class_list ,'class'=>'txt' , 'empty'=>true)); ?>
                    </td>
                    <td></td>
                    <th></th>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="common-button" id="btn_Search" value="検索"/>
            <input type="button" class="common-button" id="btn_Csv" value="CSV"/>
        </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <tr>
                    <th class="col5"><input type="checkbox" class="checkAll"/></th>
                    <th class="col15">出荷番号</th>
                    <th class="col10">出荷日</th>
                    <th>施設</th>
                    <th class="col15">登録者</th>
                    <th class="col15">登録日時</th>
                    <th>備考</th>
                    <th class="col5">件数</th>
                </tr>
                <?php $cnt=0; foreach($result as $r){ ?>
                <tr>
                    <td class="center"><?php echo $this->form->input('Edi.id.'.$cnt , array('type'=>'checkbox' , 'value'=>$r['Edi']['id'] , 'hiddenField'=>false , 'class'=>'chk'))?></td>
                    <td><?php echo h_out($r['Edi']['work_no'],'center'); ?></td>
                    <td><?php echo h_out($r['Edi']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($r['Edi']['facility_name'] . ' / ' . $r['Edi']['department_name']); ?></td>
                    <td><?php echo h_out($r['Edi']['user_name']); ?></td>
                    <td><?php echo h_out($r['Edi']['created'],'center'); ?></td>
                    <td><?php echo h_out($r['Edi']['recital']); ?></td>
                    <td><?php echo h_out($r['Edi']['count'].' / '.$r['Edi']['detail_count'],'right'); ?></td>
                </tr>
                <?php $cnt++; } ?>
                <?php if( count($result)==0 && isset($this->request->data['Edi']['is_search']) ){ ?>
                <tr><td colspan="8" class="center">該当するデータがありませんでした</td></tr>
                <?php } ?>
            </table>
        </div>
        <?php if(count($result) > 0){ ?>
        <div class="ButtonBox">
            <input type="button" class="common-button" id="btn_Conf" value="明細表示"/>
            <input type="button" class="common-button" id="hospital" value="部署シール印刷"/>
            <input type="button" class="common-button" id="invoice" value="出荷伝票印刷"/>
        </div>
        <?php } ?>
    </div>
</form>
