<script>
$(document).ready(function() {
    //チェックボックス一括変更
    $('#allCheck').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    //選択ボタン押下
    $('#btn_Mod').click(function(){
        $('#selectOrder').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/receiving_confirm').submit();
    });
    //発注書印刷ボタン押下
    $('#btn_Order').click(function(){
        $("#selectOrder").attr('action', '<?php echo $this->webroot; ?>PdfReport/order').attr('target','_blank').submit();
        $("#selectOrder").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/receiving').attr('target' , '_self');
    });
    //補充印刷ボタン押下
    $('#btn_Replenish').click(function(){
        $('#selectOrder').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/replenish').submit();
    });
    //検索ボタン押下
    $("#btn_Search").click(function(){
        //バリデート無効化 && ぐるぐる表示 && submit!!
        validateDetachSubmit($('#selectOrder') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/receiving' );
    });
});
</script>
<div id="TopicPath">
    <ul>
         <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
         <li>受注登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>受注登録</span></h2>
<h2 class="HeaddingMid">発注情報選択</h2>
<?php echo $this->form->create('Order',array('class'=>'validate_form search_form','id' =>'selectOrder')); ?>
    <?php echo $this->form->input('Order.is_search' , array('type'=>'hidden'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('Order.facility_id' ,array('options'=>$Facility_List , 'class'=>'txt' , 'empty'=>'') ); ?></td>
                <td></td>
                <th>発注日</th>
                <td>
                <?php echo $this->form->input('Order.order_date_from',array('class'=>'date validate[custom[date]]','div' => 'false','label' => 'false','maxlength' => '10')); ?>
                <span> ～ </span>
                <?php echo $this->form->input('Order.order_date_to',array('class'=>'date validate[custom[date]]','div' => 'false','label' => 'false','maxlength' => '10')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>発注番号</th>
                <td><?php echo $this->form->input('Order.work_no' ,array('type'=>'text' , 'class'=>'txt' , 'empty'=>'') ); ?></td>
                <td></td>
                <th>発注種別</th>
                <td><?php echo $this->form->input('Order.order_type' ,array('options'=>$order_type_List , 'class'=>'txt' , 'empty'=>'') ); ?></td>
                <td></td>
            </tr>

        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Search" value="検索" />
    </div>

    <?php if(isset($this->request->data['Order']['is_search'])){ ?>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
        </span>
        <span class="BikouCopy"></span>
    </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col class="col15" />
                    <col class="col10" />
                    <col class="col10" />
                    <col class="col15"/>
                    <col class="col10" />
                </colgroup>
                <thead>
                <tr>
                    <th><input type="checkbox" id="allCheck" /></th>
                    <th>施設名</th>
                    <th>発注番号</th>
                    <th>発注種別</th>
                    <th>発注日</th>
                    <th>登録者</th>
                    <th>件数</th>
                </tr>
                </thead>

                <tbody>
                <?php $i=0 ; foreach ($SearchResult as $r) { ?>
                <tr>
                    <td class="center"><input name="data[Order][id][]" type="checkbox" class="validate[required]" id="check<?php echo $r['Order']['id']; ?>" value="<?php echo $r['Order']['id']; ?>" /></td>
                    <td><?php echo h_out($r['Order']['facility_name']); ?></td>
                    <td><?php echo h_out($r['Order']['work_no'],'center'); ?></td>
                    <td><?php echo h_out($r['Order']['order_type_name'],'center'); ?></td>
                    <td><?php echo h_out($r['Order']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($r['Order']['user_name']); ?></td>
                    <td><?php echo h_out($r['Order']['count'],'right'); ?></td>
                </tr>
                <?php  $i++; } ?>
                <?php if( count($SearchResult)==0 && isset($this->request->data['Order']['is_search']) ){ ?>
                <tr><td colspan="7" class="center">該当データがありませんでした</td></tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
    </div>
    <?php if(count($SearchResult) > 0  ){ ?>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Mod" value="選択"/>
        <input type="button" class="common-button" id="btn_Order" value="発注書印刷"/>
        <input type="button" class="common-button" id="btn_Replenish" value="補充依頼票印刷"/>
    </div>
    <?php } ?>
</form>

<?php } ?>
