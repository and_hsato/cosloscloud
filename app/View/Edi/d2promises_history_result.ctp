<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/history/">直納品履歴</a></li>
        <li class="pankuzu">直納品履歴明細</li>
        <li>直納品履歴取消</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>直納品履歴取消</span></h2>
<div class="Mes01">以下の直納品の取消を行いました。</div>
<?php echo $this->form->create('D2Promise',array('class' => 'validate_form','type' => 'post','action' => '' ,'id' =>'ConfirmForm')); ?>
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right" id="pageTop" ></td>
    </tr>
    <tr>
        <td>表示件数：<?php echo count($result); ?>件</td>
    </tr>
</table>
<div class="TableScroll2">
    <table class="TableStyle01 table-odd2">
        <colgroup>
            <col width="25"/>
            <col width="135"/>
            <col width="95"/>
            <col />
            <col width="120"/>
            <col width="80"/>
            <col width="100"/>
            <col width="90"/>
            <col width="150"/>
            <col width="130"/>
        </colgroup>
        <tr>
            <th rowspan="2" class="center">&nbsp;</th>
            <th>直納番号</th>
            <th>直納日</th>
            <th class="center">商品名</th>
            <th class="center">製品番号</th>
            <th>包装単位</th>
            <th>単価</th>
            <th>ロット番号</th>
            <th rowspan="2">部署シール</th>
            <th>遡及除外</th>
        </tr>
        <tr>
            <th colspan="2">施設 ／ 部署</th>
            <th>規格</th>
            <th>販売元</th>
            <th>商品ID</th>
            <th></th>
            <th>有効期限</th>
            <th>備考</th>
        </tr>
        <?php
          $i = 0;
          foreach($result as $r) {
        ?>
        <tr>
            <td rowspan="2"></td>
            <td><?php echo h_out($r['d2promises']['work_no'],'center'); ?></td>
            <td><?php echo h_out($r['d2promises']['work_date'],'center'); ?></td>
            <td><?php echo h_out($r['d2promises']['item_name']); ?></td>
            <td><?php echo h_out($r['d2promises']['item_code']); ?></td>
            <td><?php echo h_out($r['d2promises']['unit_name']); ?></td>
            <td><?php echo h_out($this->Common->toCommaStr($r['d2promises']['transaction_price'])); ?></td>
            <td><?php echo h_out($r['d2promises']['lot_no']); ?></td>
            <td rowspan="2"><?php echo h_out($r['d2promises']['hospital_sticker_no']); ?></td>
            <td><?php echo h_out( (isset($r['d2promises']['is_retroactable']) && $r['d2promises']['is_retroactable'] == '1') ? '' : '○' ,'center'); ?></td>
        </tr>
        <tr>
            <td colspan="2"><?php echo h_out($r['d2promises']['facility_name'] . ' / ' . $r['d2promises']['department_name']); ?></td>
            <td><?php echo h_out($r['d2promises']['standard']); ?></td>
            <td><?php echo h_out($r['d2promises']['dealer_name']); ?></td>
            <td><?php echo h_out($r['d2promises']['internal_code'],'center'); ?></td>
            <td></td>
            <td><?php echo h_out($r['d2promises']['validated_date']); ?></td>
            <td>
                <?php
                   echo $this->form->input('d2promises.recital.'.$i , array('type' => 'text' , 'class'=>'lbl' , 'readonly'=>'readonly' , 'value'=>$r['d2promises']['recital']));
                ?>
            </td>
        </tr>
        <?php $i++; }  ?>
    </table>
</div>
<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
<?php echo $this->form->end(); ?>
