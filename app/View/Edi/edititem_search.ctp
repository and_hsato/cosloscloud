<script type="text/javascript">
$(function(){
    //検索ボタン押下
    $("#search_btn").click(function(){
        validateDetachSubmit($('#search_form') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edititem_search' );
    });
    //編集ボタン押下
    $("#edit_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edititem');
        $("#search_form").attr('method', 'post');
        $("#search_form").submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>施設採用品一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>施設採用品一覧</span></h2>
<h2 class="HeaddingMid">編集する施設採用商品を選択してください</h2>
<form class="validate_form search_form input_form" id="search_form">
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('centerId' , array('type'=>'hidden')); ?>
    <input type="hidden" name="data[hide_action]" id="hide_action" value="1" />
    <div class="SearchBox">
        <div class="ColumnLeft">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>得意先</th>
                    <td>
                        <?php echo $this->form->input('hospitalName' , array('type'=>'hidden' , 'id'=>'facilityName')); ?>
                        <?php echo $this->form->input('hospitalText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'r validate[required]', 'style'=>'width: 60px;')); ?>
                        <?php echo $this->form->input('hospitalCode',array('options'=>$hospital_list,'id' => 'facilityCode', 'class' => 'txt r', 'empty' => true)); ?>
                    </td>
                </tr>
                <tr>
                    <th>商品ID</th>
                    <td><?php echo $this->form->text('MstFacilityItem.internal_code' ,array('class' => 'txt','maxlength' => '50' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->text('MstFacilityItem.item_code',array('class' => 'txt search_upper','maxlength' => '50','label' =>'' ));?></td>
                    <td></td>
                    <th>大分類</th>
                    <td><?php echo $this->form->input('MstFacilityItem.item_category1', array('options'=>$item_category1 , 'class'=>'txt' , 'id'=>'item_category1' , 'empty'=>true)); ?></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->text('MstFacilityItem.item_name',array('class' => 'txt search_canna','maxlength' => '50', 'label' =>'') );?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->text('MstFacilityItem.dealer_code',array('class' =>'txt search_canna','maxlength' => '50', 'label' =>'' )); ?></td>
                    <td></td>
                    <th>中分類</th>
                    <td><?php echo $this->form->input('MstFacilityItem.item_category2', array('options'=>$item_category2 , 'class'=>'txt' , 'id'=>'item_category2' , 'empty'=>true)); ?></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->text('MstFacilityItem.standard',array('class' => 'txt search_canna','maxlength' => '50' ,'label' =>'') ); ?></td>
                    <td></td>
                    <th>JANコード</th>
                    <td><?php echo $this->form->text('MstFacilityItem.jan_code' ,array('class' => 'txt','maxlength' => '50' ,'label' => '')) ; ?></td>
                    <td></td>
                    <th>小分類</th>
                    <td><?php echo $this->form->input('MstFacilityItem.item_category3', array('options'=>$item_category3 , 'class'=>'txt' , 'id'=>'item_category3' , 'empty'=>true)); ?></td>
                </tr>
                <tr>
                    <th></th>
                    <td colspan="7">
                        <?php $check = (isset($data['search']['is_search']))?$data['MstFacilityItem']['is_deleted']:'checked'; ?>
                        <?php echo $this->form->input('MstFacilityItem.is_deleted',array('label'=>'削除は除外' , 'checked'=>$check)); ?>削除は除外
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="search_btn"/>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>(isset($fitems) && count($fitems) != 0) ? count($fitems):0)); ?>
        </span>
        <span class="BikouCopy">
        </span>
    </div>

    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <tr>
                <th style="width:25px;"></th>
                <th style="width:100px;">商品ID</th>
                <th>商品名</th>
                <th>規格</th>
                <th>製品番号</th>
                <th>販売元</th>
                <th style="width:130px;">ＪＡＮコード</th>
            </tr>
            <?php $i = 0; foreach ($fitems as $item){ ?>
            <tr>
                <td>
                    <input type="radio" name="data[MstFacilityItem][id]" style="width:25px;" class="validate[required]" id="item<?php echo $item['MstFacilityItem']['id']; ?>" value="<?php echo $item['MstFacilityItem']['id']; ?>" />
                </td>
                <td style="width:100px;"><?php echo h_out($item['MstFacilityItem']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($item['MstFacilityItem']['item_name']); ?></td>
                <td><?php echo h_out($item['MstFacilityItem']['standard']); ?></td>
                <td><?php echo h_out($item['MstFacilityItem']['item_code']); ?></td>
                <td><?php echo h_out($item['MstDealer']['dealer_name']); ?></td>
                <td style="width:130px;"><?php echo h_out($item['MstFacilityItem']['jan_code']); ?></td>
            </tr>
            <?php $i++; } ?>
            <?php if(isset($data['search']['is_search']) && count($fitems) == 0 ) { ?>
            <tr><td colspan="7" class="center">該当するデータはありません。</td></tr>
            <?php } ?>
        </table>
    </div>
    <div class="SelectBikou_Area" id="bottom">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox">
        <?php if(count($fitems) > 0){ ?>
        <input type="button" class="btn btn4" id="edit_btn"/>
        <?php } ?>
    </div>
</form>
