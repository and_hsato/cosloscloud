<script type="text/javascript">
$(document).ready(function(){
    //部署シール印刷ボタン押下
    $("#hospital").click(function(){
        $("#confirm").attr('action', '<?php echo $this->webroot; ?>PdfReport/sticker').attr('target','_blank').submit();
    });
    //コストシール印字ボタン押下
    $("#cost").click(function(){
        $('#confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/cost').submit();
    });
    //出荷伝票印刷ボタン押下
    $("#invoice").click(function(){
        $("#confirm").attr('action', '<?php echo $this->webroot; ?>PdfReport/shipping').attr('target','_blank').submit();
    });
  
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>出荷登録結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>出荷登録結果</span></h2>
<div class="Mes01">以下の商品を出荷しました。</div>
<?php echo $this->form->create('Edi',array('class'=>'validate_form search_form','id' =>'confirm')); ?>
    <?php echo $this->form->input('Edi.trn_shipping_header_id' , array('type'=>'hidden'))?>
    <?php foreach($trn_order_ids as $index => $id ){ ?>
        <?php echo $this->form->input('Edi.trn_order_id.'.$index , array('type'=>'hidden' , 'value'=>$id));?>
    <?php } ?>
    <?php foreach($header_ids as $index => $id ){ ?>
        <?php echo $this->form->input('Edi.id.'.$index , array('type'=>'hidden' , 'value'=>$id));?>
    <?php } ?>

<table style="width: 100%;">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
<h2 class="HeaddingMid">出荷情報</h2>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>出荷日</th>
                <td><?php echo($this->form->text('Edi.work_date',array('class'=>'lbl','maxlength'=>'10','id'=>'work_date','label'=>'' , 'readonly'=>true))); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo($this->form->text('Edi.recital',array('class'=>'lbl','label'=>'',"maxlength"=>200 , 'readonly'=>true))); ?></td>
            </tr>
        </table>
    </div>

    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th rowspan="2" class="col5"></th>
                <th class="col25">施設名</th>
                <th class="col10">商品ID</th>
                <th class="col20">商品名</th>
                <th class="col10">製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col10">有効期限</th>
            </tr>
            <tr>
                <th>受注番号</th>
                <th>受注日</th>
                <th>規格</th>
                <th>販売元</th>
                <th>残数</th>
                <th>数量</th>
                <th>備考</th>
            </tr>
<?php
  $i = 0;
  foreach($SearchResult as $item){
  ?>
            <tr>
                <td rowspan="2" class="col5 center"></td>
                <td class="col25"><?php echo h_out($item['Edi']['facility_name'] . ' / ' . $item['Edi']['department_name']); ?></td>
                <td class="col10"><?php echo h_out($item['Edi']['internal_code'],'center'); ?></td>
                <td class="col20"><?php echo h_out($item['Edi']['item_name']); ?></td>
                <td class="col10"><?php echo h_out($item['Edi']['item_code']); ?></td>
                <td class="col10"><?php echo h_out($item['Edi']['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($item['Edi']['lot_no']); ?></td>
                <td class="col10"><?php echo h_out($item['Edi']['validated_date']); ?></td>
            </tr>
            <tr>
                <td><?php echo h_out($item['Edi']['work_no']); ?></td>
                <td><?php echo h_out($item['Edi']['work_date'],'center'); ?></td>
                <td><?php echo h_out($item['Edi']['standard']); ?></td>
                <td><?php echo h_out($item['Edi']['dealer_name']); ?></td>
                <td><?php echo h_out($item['Edi']['remain_count'],'right'); ?></td>
                <td><?php echo h_out($item['Edi']['quantity'],'right'); ?></td>
                <td><?php echo h_out($item['Edi']['recital'])?></td>
            </tr>
<?php $i++; } ?>
        </table>
    </div>
    <div class="ButtonBox" id="page_footer">
        <?php if($hospital_flg){ ?>
        <input type="button" id="hospital" class="common-button" value="部署シール印刷"/>
        <?php } ?>
        <!-- <input type="button" id="cost" class="common-button" value="コストシール印字"/> -->
        <input type="button" id="invoice" class="common-button" value="出荷伝票印刷"/>
    </div>
<?php echo $this->form->end(); ?>
