<script type="text/javascript">
$(document).ready(function(){
 
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

        $("#confirm_btn").click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length === 0){
                alert('出荷登録を行う商品を選択してください');
                return false;
            }else{
                var hasError = false;
                var remainArray = {};
                $('input[type=checkbox].checkAllTarget').each(function(i){
                    if(this.checked){
                        var input = $('#containTables');
                        //数量
                        if(input.find('input[type=text].quantity:eq(' + i + ')').val() == ''){
                            alert((i+1) + '行目の、数量を入力してください');
                            hasError = true;
                            return false;
                        }else if(!input.find('input[type=text].quantity:eq(' + i + ')').val().match(/^[1-9][0-9]*/)){
                            alert((i+1) + '行目の、数量の入力が不正です');
                            hasError = true;
                            return false;
                        }

                        input.find('input[type=text].validated_date:eq(' + i + ')').val( convertDate( input.find('input[type=text].validated_date:eq(' + i + ')').val() ) );
  
                        var validated_date = input.find('input[type=text].validated_date:eq(' + i + ')').val();
                        if(validated_date != ''){
                            if(!dateCheck(validated_date)){
                                alert((i+1) +'行目の、有効期限の入力が不正です。');
                                hasError = true;
                                return false;
                            }
                        }
  
                        if(remainArray[this.value] == undefined){
                            //データなし
                            remainArray[this.value] = parseInt(input.find('input[type=text].quantity:eq(' + i + ')').val());
                        } else {
                            //データあり
                            remainArray[this.value] = parseInt(remainArray[this.value]) + parseInt(input.find('input[type=text].quantity:eq(' + i + ')').val());
                        }
                    }
                });

                //残数チェック
                for (var id in remainArray) {
                    if ( remainArray[id] > $('input#remain'+id).val()) {
                        alert('残数を超える数量が設定されています。');
                        hasError = true;
                        return false;
                    }
                }
                  
                if(true === hasError){
                    return false;
                }else{
                    $('#confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/shipping_result').submit();
                }
            }
        });
  
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>出荷登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>出荷登録</span></h2>
<?php echo $this->form->create('Edi',array('class'=>'validate_form search_form','id' =>'confirm','method'=>'post')); ?>
    <?php echo $this->form->input('Edi.token' , array('type'=>'hidden'));?>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <h2 class="HeaddingMid">出荷情報</h2>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>出荷日</th>
                <td><?php echo($this->form->text('Edi.work_date',array('class'=>'r date validate[required,custom[date]]','maxlength'=>'10','id'=>'work_date','label'=>''))); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo($this->form->text('Edi.recital',array('class'=>'txt','label'=>'',"maxlength"=>200))); ?></td>
            </tr>
        </table>
    </div>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th rowspan="2" class="col5">
                    <input type="checkbox"  checked="checked" class='checkAll_1'/>
                </th>
                <th class="col25">施設名</th>
                <th class="col10">商品ID</th>
                <th class="col20">商品名</th>
                <th class="col10">製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col10">有効期限</th>
            </tr>
            <tr>
                <th>受注番号</th>
                <th>受注日</th>
                <th>規格</th>
                <th>販売元</th>
                <th>残数</th>
                <th>数量</th>
                <th>備考</th>
            </tr>
        <?php $i = 0; foreach($Result as $item){ ?>
            <tr>
                <td rowspan="2" class="center">
                <?php if($item['MstFacilityItem']['check']){ ?>
                <?php echo $this->form->checkbox('Edi.id.'.$i , array('value'=>$item['Edi']['id'] , 'class' => 'checkAllTarget id' , 'checked'=>'checked' , 'hiddenField'=>false) ); ?>
                <?php } ?>
                </td>
                <td><?php echo h_out($item['Edi']['facility_name'] . ' / ' . $item['Edi']['department_name']) ?></td>
                <td><?php echo h_out($item['Edi']['internal_code'],'center') ?></td>
                <td><?php echo h_out($item['Edi']['item_name']) ?></td>
                <td><?php echo h_out($item['Edi']['item_code']) ?></td>
                <td><?php echo h_out($item['Edi']['unit_name'],'center'); ?></td>
                <?php if($item['MstFacilityItem']['check']){ ?>
                <td><?php echo $this->form->input('Edi.lot_no.'.$i , array('type'=>'text' , 'class'=>'txt','value'=>$item['MstFacilityItem']['lot_no'] ))?></td>
                <td><?php echo $this->form->input('Edi.validated_date.'.$i , array('type'=>'text' , 'class'=>'validated_date date' ,'value'=>$item['MstFacilityItem']['validated_date'] ))?></td>
                <?php } else { ?>
                <td">&nbsp;</td>
                <td">&nbsp;</td>
                <?php } ?>
            </tr>
            <tr>
                <td><?php echo h_out($item['Edi']['work_no']);?></td>
                <td><?php echo h_out($item['Edi']['work_date'],'center') ?></td>
                <td><?php echo h_out($item['Edi']['standard']); ?></td>
                <td><?php echo h_out($item['Edi']['dealer_name']) ?></td>
                <td><?php echo h_out($item['Edi']['remain_count'],'right') ?></td>
                <?php if($item['MstFacilityItem']['check']){ ?>
                <td>
                    <?php echo $this->form->input('Edi.quantity.'.$i , array('type'=>'text' , 'class'=>'r txt num quantity quantity'.$item['Edi']['id'] , 'value'=>$item['MstFacilityItem']['count'] ))?>
                    <?php echo $this->form->input('Edi.remain_count.'.$i, array('type'=>'hidden' ,'class'=>'remain_count' , 'value'=>$item['Edi']['remain_count'] ))?>
                    <?php echo $this->form->input('calc.remain_count.'.$i , array('type'=>'hidden' ,'id'=>'remain'.$item['Edi']['id'], 'value'=>$item['Edi']['remain_count']) );?>
                </td>
                <td><?php echo $this->form->input('Edi.comment.'.$i , array('type'=>'text' , 'class'=>'txt' ))?></td>
                <?php } else { ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <?php } ?>
            </tr>
        <?php $i++; } ?>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" id="confirm_btn" class="btn btn2 submit [p2]" />
        </p>
    </div>
<?php echo $this->form->end(); ?>
