<script type="text/javascript">
  var webroot  = "<?php echo $this->webroot; ?>";
  var prefix   = "<?php echo Configure::read('ItemType.originalItem');?>";
  var lowlevel = "<?php echo Configure::read('ItemType.lowlevelItem');?>";

$(function(){
    $('#spare_table').hide();
    $("#add_btn").click(function(){
        if(inputCheck()){
            if( $('#MstFacilityItemSealissueMedicalcode').val() == 0 &&
              ( $('#insurance_claim_type_sel').val() != ''  &&
                $('#insurance_claim_type_sel').val() != 6 ) ) {
                if(window.confirm("医事シールが印字されない設定ですがよろしいですか？")){
                    $("#add_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/additem_confirm').submit();
                }
            }else{
                $("#add_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/additem_confirm').submit();
            }
        }
    });
    $("#spare_header").click(function () {
        $('#spare_table').slideToggle('slow');
    });
});

function inputCheck() {
    if($('#RefundPrice').val() != ''){
        if(!numCheck($('#RefundPrice').val() , 12 , 2)){
            alert('償還価格が不正です');
            $('#RefundPrice').focus();
            return false;
        }
    }
    if($('#UnitPrice').val() != ''){
        if(!numCheck($('#UnitPrice').val() , 12 , 2)){
            alert('定価が不正です');
            $('#UnitPrice').focus();
            return false;
        }
    }
    if(!numCheck($('#ConvertedNum').val() , 8 , 3 , true )){
        alert('換算数が不正です');
        $('#ConvertedNum').focus();
        return false;
    }

    if(!dateCheck($('#datepicker1').val())){
        alert('適用開始日が不正です');
        $('#datepicker1').focus();
        return false;
    }
    if($('#datepicker2').val() != '' ){
        if(!dateCheck($('#datepicker2').val())){
            alert('適用終了日が不正です');
            $('#datepicker2').focus();
            return false;
        }
    }

    if($('#expired_date').val() != ''){
        if(!numCheck($('#expired_date').val() , 4 )){
            alert("期限切れ警告日数は数字で入力してください");
            $('#expired_date').focus();
            return false;
        }
    }

    // 予備キーバリデーション
    var sparekey_error = false;
    $('form.search_form').find('.sparekey').each(function(){
        if($(this).hasClass('r') && $(this).val() == ''){
            $('#spare_table').show();
            $(this).focus();
            alert($('.' + $(this).attr("id") + 'Name').html() + "は必須入力です");
            sparekey_error = true;
            return false;
        }
        if($(this).hasClass('numeric') && $(this).val() != ''){
            if(!numCheck($(this).val(), 50)){
                $('#spare_table').show();
                $(this).focus();
                alert($('.' + $(this).attr("id") + 'Name').html() + "は数字で入力してください");
                sparekey_error = true;
                return false;
            }
        }
    });
    if(sparekey_error){
      return false;
    }

    return true;
}

</script>

<script type="text/javascript" src="<?php echo $this->webroot; ?>js/facility_item.js"></script>

<style>
<!--
div#search_dealer_form {
  z-index: 9999;
  background-color: #808080;
  position: absolute;
  height: auto;
  width: auto;
  padding: 4px;
  color: #fff;
  left: 400px;
  top: 200px;
  width: 350px;
}

div#search_dealer_form ul li {
  margin-top: 2px;
  margin-left: 0px;
}

div#search_jmdn_form {
  z-index: 9999;
  background-color: black;
  position: absolute;
  height: auto;
  width: auto;
  padding: 4px;
  color: #fff;
  left: 400px;
  top: 200px;
  width: 350px;
}

div#search_jmdn_form ul li {
  margin-top: 2px;
  margin-left: 0px;
}

div#search_icc_form {
  z-index: 9999;
  background-color: red;
  position: absolute;
  height: auto;
  width: auto;
  padding: 4px;
  color: #fff;
  left: 400px;
  top: 200px;
  width: 350px;
}

div#search_icc_form ul li {
  margin-top: 2px;
  margin-left: 0px;
}

.floatClose {
  margin-left: 10px;
}
-->
</style>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/additem_search">施設採用品一覧</a></li>
        <li>施設採用品登録</li>
    </ul>
</div>
<?php echo $this->element('masters/facility_items/item_hiddenAjaxForm'); ?>

<h2 class="HeaddingLarge"><span>施設採用品登録</span></h2>
<h2 class="HeaddingSmall">得意先 [ <?php echo $this->request->data['hospitalName']; ?> ]</h2>
<h2 class="HeaddingSmall">施設採用品情報</h2>
<form class="validate_form search_form" id="add_form"  method="post">
    <?php echo $this->form->input('MstFacilityItem.isOriginal', array('type'=>'hidden', 'value'=>1)); ?>
    <?php echo $this->form->input('centerId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('hospitalName', array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <div>
            <?php echo $this->element('masters/facility_items/item_Table'); ?>
        </div>
        <h2 class="HeaddingSmall">採用基本単位</h2>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <tr>
                    <th>基本単位</th>
                    <th>
                        <?php echo $this->form->input('MstFacilityItem.mst_unit_name_id', array('options'=>$item_units, 'empty'=>true, 'class' => 'r validate[required]', 'style'=>'width: 120px;')); ?>
                        換算数<?php echo $this->form->input('MstFacilityItem.converted_num', array('id'=>'ConvertedNum','label'=>false,'div'=>false,'maxlength'=>50,'class'=>'lbl num','style'=>'width:60px;','value'=>'1', 'readonly'=>'readonly')); ?>
                    </th>
                </tr>
            </table>
        </div>
        <div class="ButtonBox">
            <p class="center"><input type="button" class="btn btn3 [p2]" id="add_btn" /></p>
        </div>
    </div>
</form>