<script>
$(function($) {
    $('#table2 tr:even').addClass('odd');
  
    //初期表示は適用中のもののみとする
    $('.display').attr('style' , 'display:none;');
    //カレンダー表示
    $(".date").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});

    //radioボタン変更時の処理
    $('#display0').change(function () {
        if(confirm("過去適用分も表示:表示を切り替えますと現在編集中の内容が破棄されますがよろしいでしょうか？")){
            //該当行を表示に
            $('.display').attr('style' , '');
        }
    });

    $('#display1').change(function () {
        if(confirm("適用中分のみ表示:表示を切り替えますと現在編集中の内容が破棄されますがよろしいでしょうか？")){
            //該当行を非表示に
            $('.display').attr('style' , 'display:none;');
        }
    });

    $('#add').click(function(){
        var tr_count = $('#table1 > tbody > tr').size(); //TR要素をカウント
        $('#table1').append(
            $('<tr>').append(
                $('<td>').append('<input type="text" name="data[MstTransactionConfig]['+tr_count+'][start_date]" class="r date validate[required] " id="start_date'+tr_count+'">')
                         .append('<input type="hidden" name="data[MstTransactionConfig]['+tr_count+'][end_date]" value="3000/01/01">')
            ).append(
                $('<td>').append(
                    $('<select name="data[MstTransactionConfig]['+tr_count+'][partner_facility_id]" id="partner_facility_id'+tr_count+'" class="r txt validate[required]" disabled="disabled">')
                     .append('<option value="">選択してください</option>')
                     <?php foreach($Suppliers_List as $k => $v){ ?>
                        <?php if($k == $me) { ?>
                        .append('<option value="<?php echo $k ?>" selected="selected"><?php echo $v?></option>')
                        <?php } else { ?>
                        .append('<option value="<?php echo $k ?>"><?php echo $v?></option>')
                        <?php } ?>
                     <?php } ?>
                )
            ).append(
                $('<td>').append(
                    $('<select name="data[MstTransactionConfig]['+tr_count+'][partner_facility_id2]" id="partner_facility_id2'+tr_count+'" class="r txt validate[required]">')
                     .append('<option value="">選択してください</option>')
                     <?php foreach($Suppliers_List as $k => $v){ ?>
                     .append('<option value="<?php echo $k ?>"><?php echo $v?></option>')
                     <?php } ?>
                )
            ).append(
                $('<td>').append(
                    $('<select name="data[MstTransactionConfig]['+tr_count+'][mst_item_unit_id]" id="mst_item_unit_id'+tr_count+'" class="r txt validate[required]">')
                     .append('<option value="">選択してください</option>')
                     <?php foreach($UnitName_List as $k => $v){ ?>
                     .append('<option value="<?php echo $k ?>"><?php echo $v?></option>')
                     <?php } ?>
                )
            ).append(
                $('<td>').append('<input type="text" name="data[MstTransactionConfig]['+tr_count+'][transaction_price]" id="transactionConfig'+tr_count+'" class="txt r right price validate[required]" maxlength="13">')
<?php if($this->Session->read('Auth.Config.MSCorporate') == '1' ) { ?>
            ).append(
                $('<td>').append('<input type="text" name="data[MstTransactionConfig]['+tr_count+'][ms_transaction_price]" id="mstransactionConfig'+tr_count+'" class="txt r right price validate[required]" maxlength="13">')
<?php } ?>
            ).append(
                $('<td class="center">').append('<input type="checkbox" name="data[MstTransactionConfig]['+tr_count+'][is_deleted]" checked>')
                                        .append('<input type="hidden" name="data[MstTransactionConfig]['+tr_count+'][id]">')
                                        .append('<input type="hidden" name="data[MstTransactionConfig]['+tr_count+'][status]">')
            )
        )
        //追加要素にカレンダーを追加
        $('#start_date'+tr_count).datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
    });

    $("#btn_Mod").click(function(){
        submitFlg = true;
        // 仕入れ設定が1行以上なければNG
        var tr_count = $('#table1 > tbody > tr').size(); //TR要素をカウント
        if(tr_count == 1){
            alert('仕入設定を1行以上設定してください。');
            submitFlg = false;
            return false;
        }

        //適用開始日チェック
        $("#modForm input[type=text].date").each(function(){
            if(!dateCheck($(this).val())){
                alert('入力された日付が不正です。');
                $(this).focus();
                submitFlg = false;
                return false;
            }
        });

        //小数点フォーマットチェック
        $("#modForm input[type=text].price").each(function(){
            if(!numCheck($(this).val() , 10 , 2 , false ) ){
                alert("仕入単価が不正です");
                $(this).focus();
                submitFlg = false;
                return false;
            }
        });

        if(submitFlg){
            $("#modForm").attr('action', '<?php echo $this->webroot; ?>Edi/transaction_result');
            $("#modForm").submit();
        }
    });
  });

</script>

<div id="TopicPath">
    <ul>
          <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
          <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/transaction_search">採用品選択</a></li>
          <li>仕入設定</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">仕入設定編集</h2>

<form class="validate_form" method="post" id="modForm">
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <?php echo $this->form->input('centerId', array('type'=>'hidden'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.internal_code' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('MstFacilityItem.id' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_code' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;'  , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_name' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                   <?php echo $this->form->input('MstFacilityItem.dealer_name' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.standard' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>JANコード</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.jan_code' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>償還価格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.refund_price' , array('type'=>'text' , 'class'=>'right lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>保険請求区分</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.insurance_claim_department_name' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th></th>
                <td></td>
                <td></td>
                <th>保険請求名称</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.insurance_claim_name_s' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>


            <tr>
                <td><input type="radio" id="display1" name="display" value="1" checked />適用中分のみ表示</td>
                <td><input type="radio" id="display0" name="display" value="0" />過去適用分も表示</td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
        <div class="results">
            <div class="SelectBikou_Area">
                <span class="DisplaySelect">
                    <input type="button" id="add" class="btn btn44"  />
                </span>
                <span class="BikouCopy"></span>
            </div>
            <div class="TableScroll">
                <table class="TableStyle01" id="table1">
                        <tr>
                            <th class="col15">適用開始日</th>
                            <th>仕入先</th>
                            <th>仕入先2</th>
                            <th class="col15">包装単位</th>
                            <th>仕入単価</th>
<?php if($this->Session->read('Auth.Config.MSCorporate') == '1' ) { ?>
                            <th>MS仕入単価</th>
<?php } ?>
                            <th class="col5">適用</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i=0; foreach($TransactionConfig as $trn){ ?>
                        <tr class="<?php echo (($trn['MstTransactionConfig']['status'])?'':'display')?>">
                            <td>
                                <?php echo $this->form->input('MstTransactionConfig.' . $i . '.start_date', array('type'=>'text' , 'id'=>'start_date'.$i ,'class'=>'r date validate[required]', 'value'=>$trn['MstTransactionConfig']['start_date'] ))?>
                                <?php echo $this->form->input('MstTransactionConfig.' . $i . '.end_date', array('type'=>'hidden' , 'value'=>'3000/01/01' ))?>
                            </td>
                            <td>
                                <?php echo $this->form->input('MstTransactionConfig.' . $i . '.partner_facility_id', array('options' => $Suppliers_List , 'value'=>$trn['MstTransactionConfig']['partner_facility_id'], 'class'=>'txt r validate[required]' , 'empty'=>'選択してください', 'disabled' => 'disabled'))?>
                            </td>
                            <td>
                                <?php echo $this->form->input('MstTransactionConfig.' . $i . '.partner_facility_id2', array('options' => $Suppliers_List , 'value'=>$trn['MstTransactionConfig']['partner_facility_id2'], 'class'=>'txt r validate[required]' , 'empty'=>'選択してください'))?>
                            </td>
                            <td>
                                <?php echo $this->form->input('MstTransactionConfig.' . $i . '.mst_item_unit_id', array('options' => $UnitName_List , 'value'=>$trn['MstTransactionConfig']['mst_item_unit_id'] , 'class'=>'txt r validate[required]' , 'empty'=>'選択してください'))?>
                            </td>
                            <td>
                                <?php echo $this->form->input('MstTransactionConfig.' . $i . '.transaction_price', array('type'=>'text' , 'class'=>'tbl_txt txt r right price validate[required]' , 'value'=>$trn['MstTransactionConfig']['transaction_price'] , 'maxlength'=>'13')); ?>
                            </td>
<?php if($this->Session->read('Auth.Config.MSCorporate') == '1' ) { ?>
                            <td>
                                <?php echo $this->form->input('MstTransactionConfig.' . $i . '.ms_transaction_price', array('type'=>'text' , 'class'=>'tbl_txt txt r right price validate[required]' , 'value'=>$trn['MstTransactionConfig']['ms_transaction_price'] , 'maxlength'=>'13')); ?>
                            </td>
<?php } ?>
                            <td class="center">
                                <?php echo $this->form->checkbox('MstTransactionConfig.' . $i . '.is_deleted', array('checked'=>(!$trn['MstTransactionConfig']['is_deleted']) , 'hiddenField'=>false));?>
                                <?php echo $this->form->input('MstTransactionConfig.' . $i . '.id', array('type'=>'hidden' , 'value'=>$trn['MstTransactionConfig']['id']));?>
                                <?php echo $this->form->input('MstTransactionConfig.' . $i . '.status', array('type'=>'hidden' , 'value'=>$trn['MstTransactionConfig']['status']));?>
                            </td>
                        </tr>
                    <?php $i++; } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <h2 class="HeaddingSmall">代表仕入</h2>
        <div class="TableScroll">
            <table class="TableStyle01" id="table3">
                <colgroup>
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>仕入先</th>
                    <th>包装単位</th>
                </tr>
                <?php foreach ($TrnMain as $m) { ?>
                    <?php echo $this->form->input('MstTransactionMain.id' , array('type' => 'hidden' , 'value' => $m['MstTransactionMain']['id']))?>
                    <?php echo $this->form->input('MstTransactionMain.mst_facility_id' , array('type' => 'hidden', 'value' => $me))?>
                    <tr>
                        <td>
                        <?php echo $this->form->input('MstTransactionMain.mst_facility_id' , array('options' => $Suppliers_List, 'class'=>'txt r validate[required]', 'empty'=>'選択してください', 'value' => $me, 'disabled' => 'disabled'))?>
                        </td>
                        <td>
                        <?php echo $this->form->input('MstTransactionMain.mst_item_unit_id' , array('options' => $UnitName_List, 'class'=>'txt r validate[required]', 'value' => $m['MstTransactionMain']['mst_item_unit_id'], 'empty'=>'選択してください'))?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <h2 class="HeaddingSmall">販売設定</h2>
        <div class="TableScroll">
            <table class="TableStyle01" id="table2">
                <colgroup>
                    <col />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>販売先</th>
                    <th>適用開始日</th>
                    <th>包装単位</th>
                    <th>売上単価</th>
                </tr>
                <?php foreach ($SalesConfig as $sales) { ?>
                <tr>
                    <td><?php echo h_out($sales['MstSalesConfig']['facility_name']); ?></td>
                    <td><?php echo h_out($sales['MstSalesConfig']['start_date'],'center'); ?></td>
                    <td><?php echo h_out($sales['MstSalesConfig']['unit_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($sales['MstSalesConfig']['sales_price']),'right'); ?></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn2 [p2]" id="btn_Mod" />
    </div>
</form>
