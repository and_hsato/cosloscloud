<script type="text/javascript">
    $(document).ready(function(){
        $("#btn_add_result").click(function(){
            if($('.TableScroll2:last > table > tbody > tr > td > input[type=checkbox].chk:checked').length === 0){
                alert('直納品登録を行う商品を選択してください');
                return false;
            }else{
                var hasError = false;
                $('.TableScroll2:last > table > tbody > tr > td > input[type=checkbox].chk').each(function(i){
                    if(this.checked){
                        var tr = $(this).parent(':eq(0)').parent(':eq(0)');
                        var tr2 = $(this).parent(':eq(0)').parent(':eq(0)').next();
                        //数量
                        if(tr2.find('input[type=text].quantity:eq(0)').val() == ''){
                            alert('数量を入力してください');
                            hasError = true;
                            tr2.find('input[type=text].quantity:eq(0)').focus();
                            return false;
                        }else if(!tr2.find('input[type=text].quantity:eq(0)').val().match(/^[1-9][0-9]*/)){
                            alert('数量の入力が不正です');
                            hasError = true;
                            tr2.find('input[type=text].quantity:eq(0)').focus();
                            return false;
                        }else if(tr2.find('input[type=text].quantity:eq(0)').val() >= 100){
                            alert('100以上の数量は指定出来ません');
                            hasError = true;
                            tr2.find('input[type=text].quantity:eq(0)').focus();
                            return false;
                        }

                        //仕入単価
                        if(tr2.find('input[type=text].transaction_price:eq(0)').val() == ''){
                            alert('単価を入力してください');
                            hasError = true;
                            tr2.find('input[type=text].transaction_price:eq(0)').focus();
                            return false;
                        }else if(!tr2.find('input[type=text].transaction_price:eq(0)').val().match(/^[0-9]{1,10}(\.[0-9]{1,2})?$/)){
                            alert('単価を正しく入力してください');
                            hasError = true;
                            tr2.find('input[type=text].transaction_price:eq(0)').focus();
                            return false;
                        }

                        //有効期限
                        tr2.find('input[type=text].validated_date:eq(0)').val(convertDate(tr2.find('input[type=text].validated_date:eq(0)').val()));
                        var v = tr2.find('input[type=text].validated_date:eq(0)').val();
                        if(v != "" && v != null){
                            //YYYY/MM/DD形式かどうか
                            if(!v.match(/^\d{4}\/\d{2}\/\d{2}$/)){
                                alert("日付はYYYY/MM/DD形式で入力してください");
                                hasError = true;
                                tr2.find('input[type=text].validated_date:eq(0)').focus();
                                return false;
                            }

                            //入力された日付の妥当性チェック
                            var tempYear = v.substr(0, 4) - 0;
                            var tempMonth = v.substr(5, 2) - 1;
                            var tempDay = v.substr(8, 2) - 0;

                            if(tempYear >= 1970 && tempMonth >= 0 && tempMonth <= 11 && tempDay >= 1 && tempDay <= 31){
                                var checkDate = new Date(tempYear, tempMonth, tempDay);
                                if(isNaN(checkDate)){
                                    alert("入力された日付が不正です");
                                    hasError = true;
                                    tr2.find('input[type=text].validated_date:eq(0)').focus();
                                    return false;
                                }else if(checkDate.getFullYear() == tempYear && checkDate.getMonth() == tempMonth && checkDate.getDate() == tempDay){
                                    //
                                }else{
                                    alert("入力された日付が不正です");
                                    hasError = true;
                                    tr2.find('input[type=text].validated_date:eq(0)').focus();
                                    return false;
                                }
                            }else{
                                alert("入力された日付が不正です");
                                hasError = true;
                                tr2.find('input[type=text].validated_date:eq(0)').focus();
                                return false;
                            }
                        }

                        //ロット番号(クラス分類３以上のものはロット必須)
                        var class_separation = tr.find('input[type=hidden].class_separation:eq(0)').val();
                        var lot_no = tr.find('input[type=text].lot_no:eq(0)').val();
                        if(parseInt(class_separation) >= 3){
                            if(lot_no == "" || lot_no == null){
                                alert("クラス分類が3以上の施設採用品は、ロット番号の入力が必須です");
                                hasError = true;
                                tr.find('input[type=text].lot_no:eq(0)').focus();
                                return false;
                            }
                        }
                    }
                });
                if(true === hasError){
                    return false;
                }else{
                    $(window).unbind('beforeunload');
                    $("#form_add_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/d2promises_add_result').submit();
                }
            }
        });

        // 参考仕入価格コピー
        $('#cmdTransactionPriceCopy').click(function(){
             $('.TableScroll2:last > table > tbody > tr > td > input[type=hidden].reference_transaction_price').each(function(i){
                 var transaction = $(this).parent(':eq(0)').parent(':eq(0)').next().find('input[type=text].transaction_price');
                 var reference_price = $(this).val();
                 if (reference_price.match(/^[0-9]{1,10}(\.[0-9]{1,2})?$/)){
                     transaction.val(reference_price);
                 }
             })
        });

        //備考一括入力
        $('#recital_btn').click(function(){
            $('.recital').val($('#recital_txt').val()); 
        });

        //チェックボックス一括制御
        $('.checkAll').click(function(){
            $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
        });  

        $(window).bind('beforeunload' ,function(){
            return '登録が完了していません。';
        });
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">直納品受領登録</a></li>
        <li class="pankuzu">バーコード読込</li>
        <li>直納品受領登録確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>直納品受領登録確認</span></h2>
<h2 class="HeaddingMid">以下の直納品受領登録を行います。</h2>

<form class="validate_form" id="form_add_result" action="#" method="post">
    <?php echo $this->form->input('d2promises.token' , array('type'=>'hidden','tabindex'=>'-1'));?>
    <?php echo $this->form->input('d2promises.departmentId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <?php echo $this->form->input('d2promises.supplierId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <?php echo $this->form->input('d2promises.hospitalId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <?php echo $this->form->input('d2promises.centerId',array('type'=>'hidden'));?>
    <?php echo $this->form->input('d2promises.centerDepartmentId',array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>直納日付</th>
                <td><?php echo $this->form->input('d2promises.work_date' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('d2promises.recital1' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('d2promises.hospitalName' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>診療科</th>
                <td><?php echo $this->form->input('d2promises.recital2' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td><?php echo $this->form->input('d2promises.departmentName' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>手術名</th>
                <td><?php echo $this->form->input('d2promises.recital3' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
        </table>
    </div>

    <hr class="Clear" />
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
        <span class="DisplaySelect">
            　表示件数：<?php echo count($result);  ?>件
        </span>
        <span class="BikouCopy">
            <input type="button" class="btn btn119 [p2]" id="cmdTransactionPriceCopy" value="" tabindex=-1 />
            <input type="text" id="recital_txt" maxlength="200" class="txt" tabindex=-1 />
            <input type="button" class="btn btn8 [p2]" id='recital_btn' value="" tabindex=-1 />
        </span>
    </div>
    <div class="TableScroll2">
        <table class="TableStyle01 table-even2" id="inputTable" border=0 style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <thead>
                <tr>
                    <th rowspan="2" class="center"><input type="checkbox" checked class="checkAll" tabindex=-1 /></th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>参考単価</th>
                    <th>ロット番号</th>
                    <th>遡及除外</th>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                    <th>単価</th>
                    <th>有効期限</th>
                    <th>備考</th>
                </tr>
            </thead>
            <tbody>
                <?php $cnt=1; foreach($result as $r){ ;?>
                <tr>
                    <td rowspan="2" class="center">
                    <?php if($r['MstFacilityItem']['check'] == true) { ?>
                        <?php echo $this->form->checkbox("d2promises.checked.{$cnt}" , array('class'=>'center chk' , 'value'=>$cnt , 'checked'=>true));?>
                    <?php } ?>
                    <?php echo $this->form->input("d2items.{$cnt}.is_lowlevel" , array('type'=>'hidden' , 'value'=>$r['MstFacilityItem']['is_lowlevel']));?>
                        <?php echo $this->form->input("d2items.{$cnt}.internal_code" , array('type'=>'hidden' , 'value'=>$r['MstFacilityItem']['internal_code']));?>
                        <?php echo $this->form->input("d2items.{$cnt}.item_name" , array('type'=>'hidden' , 'value'=>$r['MstFacilityItem']['item_name']));?>
                        <?php echo $this->form->input("d2items.{$cnt}.item_code" , array('type'=>'hidden' , 'value'=>$r['MstFacilityItem']['item_code']));?>
                        <?php echo $this->form->input("d2items.{$cnt}.standard" , array('type'=>'hidden' , 'value'=>$r['MstFacilityItem']['standard']));?>
                        <?php echo $this->form->input("d2items.{$cnt}.dealer_name" , array('type'=>'hidden' , 'value'=>$r['MstFacilityItem']['dealer_name']));?>
                        <?php echo $this->form->input("d2items.{$cnt}.sales_price" , array('type'=>'hidden' , 'value'=>$r['MstFacilityItem']['sales_price']));?>
                    </td>
                    <td><?php echo h_out($r['MstFacilityItem']['internal_code'] , 'center'); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['item_code']); ?></td>
                    <td>
                        <?php if($r['MstFacilityItem']['check'] == true ){?>
                        <?php echo $this->form->input("d2items.{$cnt}.mst_item_unit_id" , array('options'=>$r['MstFacilityItem']['item_unit'] , 'class'=>'txt item_unit')); ?>
                        <?php echo $this->form->input("d2items.{$cnt}.packing_name" , array('type'=>'hidden' , 'class'=>'packing_name' , 'value'=>''));?>
                        <?php } ?>
                    </td>
                    <td class="right"><?php echo h_out($this->Common->toCommaStr($r['MstFacilityItem']['transaction_price']),'right');?></td>
                    <td>
                        <?php if($r['MstFacilityItem']['check'] == true){ ?>
                        <?php echo $this->form->input("d2items.{$cnt}.lot_no" , array('type'=>'text' , 'class'=>'txt lot_no' , 'value'=>$r['MstFacilityItem']['lot_no'])); ?>
                        <?php echo $this->form->input("d2items.{$cnt}.class_separation" , array('type'=>'hidden' , 'class'=>'class_separation', 'id'=>'class_separation_' . $cnt , 'value'=>$r['MstFacilityItem']['class_separation']));?>
                        <?php } ?>
                    </td>
                    <td class="center">
                        <?php if($r['MstFacilityItem']['check'] == true ){?>
                        <?php echo $this->form->input("d2items.{$cnt}.sokyu" , array('type'=>'checkbox','class'=>'center','value'=>'1')); ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo h_out($r['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($r['MstFacilityItem']['dealer_name']);?></td>
                    <td>
                        <?php if($r['MstFacilityItem']['check']){ ?>
                        <?php echo $this->form->input("d2items.{$cnt}.quantity" , array('type'=>'text' , 'maxlength'=>9,'value'=>'1','class'=>'r tbl_txt right quantity'));?>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if($r['MstFacilityItem']['check'] == true ){ ?>
                        <?php echo $this->form->input("d2items.{$cnt}.transaction_price",array('type'=>'text' , 'class'=>'r tbl_txt right transaction_price')); ?>
                        <?php echo $this->form->input("d2items.{$cnt}.reference_transaction_price",array('type'=>'hidden' , 'class'=>'reference_transaction_price' , 'value'=>$r['MstFacilityItem']['transaction_price'])); ?>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if($r['MstFacilityItem']['check'] == true ){?>
                        <?php echo $this->form->input("d2items.{$cnt}.validated_date" ,array('type'=>'text','class'=>'date validated_date','value'=>$r['MstFacilityItem']['validated_date'])); ?>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if($r['MstFacilityItem']['check'] == true ){?>
                        <?php echo $this->form->input("d2items.{$cnt}.recital" , array('type'=>'text' , 'maxlength'=>'200' , 'class'=>'tbl_txt recital'));?>
                        <?php }else{ ?>
                        &nbsp;
                        <?php } ?>
                    </td>
                </tr>
                <?php $cnt++; } ?>
            </tbody>
        </table>

    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
        <input type="button" value="" class="btn btn2 [p2]" id="btn_add_result" name="btn_add_result"/>
    </div>
</form>

