<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li style=""><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/transaction_search">採用品選択</a></li>
        <li class="pankuzu">仕入設定</li>
        <li>仕入設定結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">仕入設定編集結果</h2>

<div class="Mes01">情報を設定しました</div>
<form method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/result" accept-charset="utf-8" id="ModForm">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.internal_code' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('MstFacilityItem.id' , array('type'=>'hidden'))?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_code' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;'  , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_name' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                   <?php echo $this->form->input('MstFacilityItem.dealer_name' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.standard' , array('type'=>'text' , 'class'=>'lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>JANコード</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.jan_code' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>償還価格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.refund_price' , array('type'=>'text' , 'class'=>'right lbl' ,'style'=>'width:250px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>保険請求区分</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.insurance_claim_department_name' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th></th>
                <td></td>
                <td></td>
                <th>保険請求名称</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.insurance_claim_name_s' , array('type'=>'text' , 'class'=>'lbl','style'=>'width:350px;' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
        </table>
        <div class="results">
            <div class="TableScroll">
                <table class="TableStyle01 table-odd" id="table1">
                    <tr>
                        <th class="col15">適用開始日</th>
                        <th class="col15">仕入先</th>
                        <th class="col15">仕入先2</th>
                        <th>包装単位</th>
                        <th>仕入単価</th>
<?php if($this->Session->read('Auth.Config.MSCorporate') == '1' ) { ?>
                        <th>MS仕入単価</th>
<?php } ?>
                        <th class="col5">適用</th>
                    </tr>
                    <?php foreach($result as $r){  ?>
                    <tr>
                        <td><?php echo h_out($r['MstTransactionConfig']['start_date'],'center'); ?></td>
                        <td><?php echo h_out($Suppliers_List[$r['MstTransactionConfig']['partner_facility_id']],'center'); ?></td>
                        <td><?php echo h_out($Suppliers_List[$r['MstTransactionConfig']['partner_facility_id2']],'center'); ?></td>
                        <td><?php echo h_out($UnitName_List[$r['MstTransactionConfig']['mst_item_unit_id']],'center'); ?></td>
                        <td><?php echo h_out($this->Common->toCommaStr($r['MstTransactionConfig']['transaction_price']),'right'); ?></td>
<?php if($this->Session->read('Auth.Config.MSCorporate') == '1' ) { ?>
                        <td><?php echo h_out($this->Common->toCommaStr($r['MstTransactionConfig']['ms_transaction_price']), 'right'); ?></td>
<?php } ?>
                        <td><?php echo h_out((($r['MstTransactionConfig']['is_deleted'])?'':'○'),'center'); ?></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        <h2 class="HeaddingSmall">代表仕入</h2>
        <div class="TableScroll">
            <table class="TableStyle01" id="table3">
                <colgroup>
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>代表仕入先</th>
                    <th>代表包装単位</th>
                </tr>
                <tr>
                    <td><?php echo h_out($Suppliers_List[$main_result['mst_facility_id']],'center'); ?></td>
                    <td><?php echo h_out($UnitName_List[$main_result['mst_item_unit_id']],'center'); ?></td>
                </tr>
            </table>
        </div>

        </div>
    </div>
</form>
