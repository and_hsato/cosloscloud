<script type="text/javascript">
    $(function(){
        //選択ボタン押下
        $("#btn_Mod").click(function(){
            $("#TransactionConfigList2").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/transaction_mod').submit();
        });

        //検索ボタン押下
        $("#btn_Search").click(function(){
            validateDetachSubmit($("#TransactionConfigList1") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/transaction_search');
        });
   });

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>仕入設定一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">仕入設定一覧</h2>
<form class="validate_form search_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/transaction_search" accept-charset="utf-8" id="TransactionConfigList1">
    <?php echo $this->form->input('MstTransactionConfig.is_search', array('type'=>'hidden'))?>
    <input type="hidden" id="printLimit" name="data[printLimit]" value="" />
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('hospitalName' , array('type'=>'hidden' , 'id'=>'facilityName')); ?>
                    <?php echo $this->form->input('hospitalText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'r validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('hospitalCode',array('options'=>$hospital_list,'id' => 'facilityCode', 'class' => 'txt r', 'empty' => true)); ?>
                </td>
                <td></td>
                <td></td>
                <td> <?php echo $this->form->input('MstTransactionConfig.is_setting' , array('type'=>'checkbox' , 'hiddenField'=>false , 'label'=>'raberl'))?>
                    仕入未設定のみ表示する
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_internal_code' , array('type'=>'text' , 'class'=>'txt'))?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_item_code' , array('type'=>'text' , 'class'=>'txt search_upper'))?>
                </td>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_standard' , array('type'=>'text' , 'class'=>'txt search_canna'))?>
                </td>
                <td></td>

            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_item_name' , array('type'=>'text' , 'class'=>'txt search_canna'))?>
                </td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstFacilityItem.search_dealer_name' , array('type'=>'text' , 'class'=>'txt search_canna') ) ?></td>
                <th>JANコード</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.search_jan_code' , array('type'=>'text' , 'class'=>'txt' ));?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($TransactionConfig_Info))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
    </div>
</form>
<form class="validate_form search_form" method="post" accept-charset="utf-8" id="TransactionConfigList2">
    <?php echo $this->form->input('centerId', array('type'=>'hidden'))?>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25" />
                <col width="90"/>
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <th></th>
                <th>商品ID</th>
                <th>商品名</th>
                <th>規格</th>
                <th>製品番号</th>
                <th>販売元</th>
           </tr>
           <?php foreach ($TransactionConfig_Info as $tran) { ?>
           <tr>
               <td class="center"><input class="validate[required] rd" id="MstFacilityItemId" name="data[MstFacilityItem][id]" type="radio" value="<?php echo $tran['MstFacilityItem']['id'] ?>"/></td>
               <td><?php echo h_out($tran['MstFacilityItem']['internal_code'],'center'); ?></td>
               <td><?php echo h_out($tran['MstFacilityItem']['item_name']); ?></td>
               <td><?php echo h_out($tran['MstFacilityItem']['standard']); ?></td>
               <td><?php echo h_out($tran['MstFacilityItem']['item_code']); ?></td>
               <td><?php echo h_out($tran['MstFacilityItem']['dealer_name']); ?></td>
           </tr>
           <?php } ?>
           <?php if(count($TransactionConfig_Info) == 0 && isset($this->request->data['MstTransactionConfig']['is_search'])){?>
           <tr><td colspan="7" class="center">該当するデータがありませんでした。</td></tr>
           <?php } ?>
       </table>
    </div>
    <?php if(count($TransactionConfig_Info) > 0 ){?>
    <div class="ButtonBox">
        <input type="button" value="" class="btn btn4" id="btn_Mod"/>
    </div>
    <?php } ?>
</form>
