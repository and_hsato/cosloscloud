<script type="text/javascript">
    $(document).ready(function(){

       $(document).keydown(function(e){
          var key_code;

          key_code = e.keyCode ? e.keyCode : e.which;
          // 9 = Tabキー
          if(key_code  == 9 ){
              className = document.activeElement.className;
              indexNum = 0;
              obj = jQuery(document.activeElement);
              //クラスにtabが含まれていたら
              m = className.search(/ tab[0-9]/i);
              if (m != -1 ) {
                  indexNum = parseInt(className.substring(m+4));
              }
              switch (indexNum){
                  case 1:
                      //2に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').next(':eq(0)').find('.tab2').focus();
                      break;
                  case 2:
                      //3に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').find('.tab3').focus();
                      break;
                  case 3:
                      //5に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').prev(':eq(0)').find('.tab5').focus();
                      break;
                  case 5:
                      //6に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').next(':eq(0)').find('.tab6').focus();
                      break;
                  case 6:
                      //8に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').find('.tab8').focus();
                      break;
                  case 8:
                      //9に飛ぶ
                      obj.parent(':eq(0)').parent(':eq(0)').prev(':eq(0)').find('.tab9').focus();
                      break;
                  case 9:
                      //コピーされている場合
                      tmp1 = obj.parent(':eq(0)').parent(':eq(0)').next(':eq(0)').next(':eq(0)').find('.tab1');
                      //商品が切り替わっている場合
                      tmp2 = obj.parent(':eq(0)').parent(':eq(0)').parent(':eq(0)').parent(':eq(0)').next(':eq(0)').find('.tab1');
                      if(tmp1.attr('class') != undefined){
                          //1に飛ぶ
                          tmp1.focus();
                      }else if(tmp2.attr('class') != undefined){
                          //1に飛ぶ
                          tmp2.focus();
                      }else{
                          //確定ボタンに飛ぶ
                          $("#btn_add_result").focus();
                      }
                     break;
                  default:
                     //初期は先頭行のチェックボックス
                     $(document).find(".tab1:first").focus();
                     break;
              }
              return false;
          }
        });

        $("#btn_add_result").click(function(){
            if($('.TableScroll2:last > table > tbody > tr > td > input[type=checkbox].chk:checked').length === 0){
                alert('直納品登録を行う商品を選択してください');
                return false;
            }else{
                var hasError = false;
                $('.TableScroll2:last > table > tbody > tr > td > input[type=checkbox].chk').each(function(i){
                    if(this.checked){
                        var tr = $(this).parent(':eq(0)').parent(':eq(0)');
                        var tr2 = $(this).parent(':eq(0)').parent(':eq(0)').next();
                        //数量
                        if(tr2.find('input[type=text].quantity:eq(0)').val() == ''){
                            alert('数量を入力してください');
                            hasError = true;
                            tr2.find('input[type=text].quantity:eq(0)').focus();
                            return false;
                        }else if(!tr2.find('input[type=text].quantity:eq(0)').val().match(/^[1-9][0-9]*/)){
                            alert('数量の入力が不正です');
                            hasError = true;
                            tr2.find('input[type=text].quantity:eq(0)').focus();
                            return false;
                        }else if(tr2.find('input[type=text].quantity:eq(0)').val() >= 100){
                            alert('100以上の数量は指定出来ません');
                            hasError = true;
                            tr2.find('input[type=text].quantity:eq(0)').focus();
                            return false;
                        }

                        //仕入単価
                        if(tr2.find('input[type=text].transaction_price:eq(0)').val() == ''){
                            alert('単価を入力してください');
                            hasError = true;
                            tr2.find('input[type=text].transaction_price:eq(0)').focus();
                            return false;
                        }else if(!tr2.find('input[type=text].transaction_price:eq(0)').val().match(/^[0-9]{1,10}(\.[0-9]{1,2})?$/)){
                            alert('単価を正しく入力してください');
                            hasError = true;
                            tr2.find('input[type=text].transaction_price:eq(0)').focus();
                            return false;
                        }

                        //有効期限
                        tr2.find('input[type=text].validated_date:eq(0)').val(convertDate(tr2.find('input[type=text].validated_date:eq(0)').val()));
                        var v = tr2.find('input[type=text].validated_date:eq(0)').val();
                        if(v != "" && v != null){
                            //YYYY/MM/DD形式かどうか
                            if(!v.match(/^\d{4}\/\d{2}\/\d{2}$/)){
                                alert("日付はYYYY/MM/DD形式で入力してください");
                                hasError = true;
                                tr2.find('input[type=text].validated_date:eq(0)').focus();
                                return false;
                            }

                            //入力された日付の妥当性チェック
                            var tempYear = v.substr(0, 4) - 0;
                            var tempMonth = v.substr(5, 2) - 1;
                            var tempDay = v.substr(8, 2) - 0;

                            if(tempYear >= 1970 && tempMonth >= 0 && tempMonth <= 11 && tempDay >= 1 && tempDay <= 31){
                                var checkDate = new Date(tempYear, tempMonth, tempDay);
                                if(isNaN(checkDate)){
                                    alert("入力された日付が不正です");
                                    hasError = true;
                                    tr2.find('input[type=text].validated_date:eq(0)').focus();
                                    return false;
                                }else if(checkDate.getFullYear() == tempYear && checkDate.getMonth() == tempMonth && checkDate.getDate() == tempDay){
                                    //
                                }else{
                                    alert("入力された日付が不正です");
                                    hasError = true;
                                    tr2.find('input[type=text].validated_date:eq(0)').focus();
                                    return false;
                                }
                            }else{
                                alert("入力された日付が不正です");
                                hasError = true;
                                tr2.find('input[type=text].validated_date:eq(0)').focus();
                                return false;
                            }
                        }

                        //ロット番号(クラス分類３以上のものはロット必須)
                        var class_separation = tr.find('input[type=hidden].class_separation:eq(0)').val();
                        var lot_no = tr.find('input[type=text].lot_no:eq(0)').val();
                        if(parseInt(class_separation) >= 3){
                            if(lot_no == "" || lot_no == null){
                                alert("クラス分類が3以上の施設採用品は、ロット番号の入力が必須です");
                                hasError = true;
                                tr.find('input[type=text].lot_no:eq(0)').focus();
                                return false;
                            }
                        }
                    }
                });
                if(true === hasError){
                    return false;
                }else{
                    $(window).unbind('beforeunload');
                    $("#form_add_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/d2promises_add_result').submit();
                }
            }
        });

        //追加ボタン押下
        $('.add_btn').click(function(){
            var tr_count = Math.floor($('.TableScroll2:last > table > tbody > tr').size() / 2); //TR要素をカウント
            var add = $(this).attr('id');
            var tr = $(this).parent(':eq(0)').parent(':eq(0)').parent(':eq(0)');
            tr.append(
                $('<tr>').append(
                    $('<td rowspan="2" class="center">').append('<input type="hidden" name="data[d2items]['+tr_count+'][is_lowlevel] value="'+$('input#is_lowlevel'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][id]" value="'+$('input#id'+add).val()+'" >')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][no]" value="'+tr_count+'" >')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][reference_transaction_price]" class="reference_transaction_price" value="'+$('input#reference_transaction_price'+add).val()+'" >')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][reference_sales_price]" class="reference_sales_price" value="'+$('input#reference_sales_price'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][sales_price]" class="sales_price" value="'+$('input#sales_price'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][internal_code]" value="'+$('input#internal_code'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][item_name]" value="'+$('input#item_name'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][item_code]" value="'+$('input#item_code'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][standard]" value="'+$('input#standard'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][dealer_name]" value="'+$('input#dealer_name'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][mst_item_unit_id]" value="'+$('input#mst_item_unit_id'+add).val()+'">')
                                                        .append('<input type="hidden" name="data[d2items]['+tr_count+'][packing_name]" value="'+$('input#packing_name'+add).val()+'">')
                                                        .append('<input type="checkbox" class="center chk tab1" checked="checked" name="data[d2promises][checked][]" value="'+tr_count+'">')
                ).append(
                    $('<td align="center">').append(
                        $('<label>').append(
                            $('<p align="center">').text($('input#internal_code'+add).val())
                        ).attr('title',$('input#internal_code'+add).val())
                    )
                ).append(
                    $('<td align="center">').append(
                        $('<label>').text($('input#item_name'+add).val()).attr('title' ,$('input#item_name'+add).val() )
                    )
                ).append(
                    $('<td align="center">').append(
                        $('<label>').text($('input#item_code'+add).val()).attr('title' , $('input#item_code'+add).val())
                     )
                 ).append(
                     $('<td>').append(
                         $('<label>').text($('input#packing_name'+add).val()).attr('title' ,$('input#packing_name'+add).val() )
                     )
                 ).append(
                     $('<td>').append(
                         $('<label>').append(
                             $('<p class="right">').text($('input#reference_transaction_price'+add).val())
                         ).attr('title',$('input#reference_transaction_price'+add).val())
                     )

                  ).append(
                     $('<td>').append('<input type="text" class="tbl_txt lot_no tab5"  maxlength="20" name="data[d2items]['+tr_count+'][lot_no]" />')
                              .append('<input type="hidden" class="class_separation" id="class_separation_+tr_count+" value="">')
                 ).append(
                     $('<td align="center">').append(
                         $('<input type="checkbox" class="center tab9" name="data[d2items]['+tr_count+'][sokyu]" value="1" />')
                     )
                 )
             ).append(
                 $('<tr>').append(
                     $('<td>')
                 ).append(
                     $('<td>').append(
                         $('<label>').text($('input#standard'+add).val()).attr('title' , $('input#standard'+add).val())
                     )
                 ).append(
                     $('<td>').append(
                         $('<label>').text($('input#dealer_name'+add).val()).attr('title' , $('input#dealer_name'+add).val())
                     )
                 ).append(
                     $('<td>').append(
                         $('<input type="text" maxlength="9" style="border:solid 1px red; ime-mode:disabled;" class="tbl_txt right quantity tab2" name="data[d2items]['+tr_count+'][quantity]" >')
                     )
                 ).append(
                     $('<td>').append(
                         $('<input type="text" maxlength="13" style="border:solid 1px red; ime-mode:disabled;" class="tbl_txt right transaction_price tab3" name="data[d2items]['+tr_count+'][transaction_price]" />')
                     )
                 ).append(
                     $('<td>').append(
                         $('<input type="text" class="date validated_date tab6" name="data[d2items]['+tr_count+'][validated_date]" id="validated_date'+tr_count+'" >')
                     )
                 ).append(
                     $('<td>').append(
                         $('<input type="text" maxlength="200" class="tbl_txt recital tab8" name="data[d2items]['+tr_count+'][recital]" id="bikou+tr_count" />')
                     )
                 )
             )

             //追加要素にカレンダーを追加
             $('#validated_date'+tr_count).datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});

             //ストライプ塗りなおし
             r_count = 0;
             $('.TableScroll2:last > table > tbody > tr').each(function(){
                 cnt = Math.floor(r_count / 2 );
                 if((cnt % 2 )!= 0){
                     $(this).addClass('odd');
                 }else{
                     $(this).removeClass('odd');
                 }
                 r_count++;
             });

             // 件数表示を更新する。
             $("#count").text('表示件数：' + $('.TableScroll2:last > table > tbody > tr > td > input[type=checkbox].chk').length + '件');
        });

        // 参考仕入価格コピー
        $('#cmdTransactionPriceCopy').click(function(){
             $('.TableScroll2:last > table > tbody > tr > td > input[type=hidden].reference_transaction_price').each(function(i){
                 var transaction = $(this).parent(':eq(0)').parent(':eq(0)').next().find('input[type=text].transaction_price');
                 var reference_price = $(this).val();
                 if (reference_price.match(/^[0-9]{1,10}(\.[0-9]{1,2})?$/)){
                     transaction.val(reference_price);
                 }
             })
        });

        //備考一括入力
        $('#recital_btn').click(function(){
            $('.TableScroll2:last > table > tbody > tr > td > .recital').val($('#recital_txt').val());
        });

        //チェックボックス一括制御
        $('.checkAll').click(function(){
            $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
        });
    });

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot ?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/d2promises_add" >直納品登録</a></li>
        <li class="pankuzu">採用品選択</li>
        <li>直納品登録確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>直納品登録確認</span></h2>
<h2 class="HeaddingMid">以下の直納品登録を行います。</h2>

<form class="validate_form" id="form_add_result" action="#" method="post">
    <?php echo $this->form->input('d2promises.token' , array('type'=>'hidden','tabindex'=>'-1'));?>
    <?php echo $this->form->input('d2promises.departmentId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <?php echo $this->form->input('d2promises.supplierId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <?php echo $this->form->input('d2promises.hospitalId' , array('type'=>'hidden','tabindex'=>'-1')); ?>
    <?php echo $this->form->input('d2promises.centerId',array('type'=>'hidden'));?>
    <?php echo $this->form->input('d2promises.centerDepartmentId',array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>作業日付</th>
                <td><?php echo $this->form->input('d2promises.work_date' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('d2promises.recital1' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('d2promises.hospitalName' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>診療科</th>
                <td><?php echo $this->form->input('d2promises.recital2' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td><?php echo $this->form->input('d2promises.departmentName' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
                <td></td>
                <th>手術名</th>
                <td><?php echo $this->form->input('d2promises.recital3' , array('class'=>'lbl' , 'readonly'=>'readonly')); ?></td>
            </tr>
        </table>
    </div>

    <hr class="Clear" />
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
        <span class="DisplaySelect" id="count">表示件数：<?php echo count($FacilityItemList);  ?>件</span>
        <span class="BikouCopy">
            <input type="button" class="btn btn119 [p2]" id="cmdTransactionPriceCopy" value="" tabindex=-1 />
            <input type="text" id="recital_txt" maxlength="200" class="txt" />
            <input type="button" class="btn btn8" id='recital_btn' />
        </span>
    </div>
    <div class="TableScroll2">
        <table class="TableStyle01 table-even2" id="inputTable" border=0 style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
            <colgroup>
                <col width="25" />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <thead>
                <tr>
                    <th rowspan="2" class="center"><input type="checkbox" class="checkAll" /></th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>参考単価</th>
                    <th>ロット番号</th>
                    <th>遡及除外</th>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                    <th>単価</th>
                    <th>有効期限</th>
                    <th>備考</th>
                </tr>
            </thead>
            <tbody>
<?php if(!empty($FacilityItemList)){
         $cnt=0;
         for($i=0;$i<count($FacilityItemList);$i++) {
             $mst_item_unit_id    = $FacilityItemList[$i]['MstFacilityItems']['id'];
             $internal_code       = $FacilityItemList[$i]['MstFacilityItems']['internal_code'];    // 商品ID
             $item_name           = $FacilityItemList[$i]['MstFacilityItems']['item_name'];        // 商品名
             $item_code           = $FacilityItemList[$i]['MstFacilityItems']['item_code'];        // 製品番号
             $standard            = $FacilityItemList[$i]['MstFacilityItems']['standard'];         // 規格
             $dealer_name         = $FacilityItemList[$i]['MstFacilityItems']['dealer_name'];      // 販売元
             $transaction_price   = $FacilityItemList[$i]['MstFacilityItems']['transaction_price'];// 参考仕入単価
             $sales_price         = $FacilityItemList[$i]['MstFacilityItems']['sales_price'];      // 参考売上単価
             $is_lowlevel         = $FacilityItemList[$i]['MstFacilityItems']['is_lowlevel'];      // 低レベル品フラグ
             $classSeparation     = $FacilityItemList[$i]['MstFacilityItems']['class_separation']; // クラス分類
             $item_unit           = $FacilityItemList[$i]['MstFacilityItems']['unit_name'];
            ?>
                <tr>
                    <td rowspan="2" class="center">
                        <input type="checkbox" class="center chk tab1" checked="checked" name="data[d2promises][checked][]" value="<?php echo $cnt; ?>" tabindex="<?php echo $cnt * 10 + 1;?>" />
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][is_lowlevel]" id="is_lowlevel<?php echo $cnt?>" value="<?php echo $is_lowlevel; ?>">
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][id]" id="id<?php echo $cnt?>" value="<?php echo $FacilityItemList[$i]['MstFacilityItems']['id']; ?>">
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][no]" id="no<?php echo $cnt?>"value="<?php echo $cnt; ?>">
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][reference_transaction_price]" id="reference_transaction_price<?php echo $cnt?>" class="reference_transaction_price" value="<?php echo $transaction_price; ?>">
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][reference_sales_price]" id="reference_sales_price<?php echo $cnt ?>" class="reference_sales_price" value="<?php echo $sales_price; ?>">
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][sales_price]" id="sales_price<?php echo $cnt ?>" class="sales_price" value="<?php echo $sales_price; ?>">
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][internal_code]" id="internal_code<?php echo $cnt ?>" value="<?php echo h($internal_code); ?>" />
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][item_name]" id="item_name<?php echo $cnt ?>" value="<?php echo h($item_name); ?>" />
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][item_code]" id="item_code<?php echo $cnt ?>" value="<?php echo h($item_code); ?>" />
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][standard]" id="standard<?php echo $cnt ?>" value="<?php echo h($standard); ?>" />
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][dealer_name]" id="dealer_name<?php echo $cnt ?>" value="<?php echo h($dealer_name); ?>" />
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][packing_name]" id="packing_name<?php echo $cnt ?>" value="<?php echo h($item_unit); ?>" />
                        <input type="hidden" name="data[d2items][<?php echo $cnt; ?>][mst_item_unit_id]" id="mst_item_unit_id<?php echo $cnt?>" value="<?php echo $mst_item_unit_id; ?>">
                    </td>
                    <td><?php echo h_out($internal_code,'center'); ?></td>
                    <td><?php echo h_out($item_name); ?></td>
                    <td><?php echo h_out($item_code); ?></td>
                    <td><?php echo h_out($item_unit); ?></td>
                    <td><?php echo h_out($transaction_price === false ? '<span color="red">未設定</span>':$this->Common->toCommaStr($transaction_price),'right'); ?></td>
                    <td>
                        <input type="text" class="tbl_txt lot_no tab5"  maxlength="20" name="data[d2items][<?php echo $cnt; ?>][lot_no]" tabindex="<?php echo $cnt * 10 + 5;?>" />
                        <input type="hidden" class="class_separation" id="class_separation_<?php echo $cnt; ?>" value="<?php echo $classSeparation; ?>">
                    </td>
                    <td align="center">
                        <input type="checkbox" class="center tab9" name="data[d2items][<?php echo $cnt; ?>][sokyu]" value="1" tabindex="<?php echo $cnt * 10 + 8;?>" />
                    </td>
                </tr>
                <tr>
                    <td class="center"><input type='button' class="btn btn55 add_btn" id="<?php echo $cnt ?>" tabindex=-1 ></td>
                    <td><?php echo h_out($standard); ?></td>
                    <td><?php echo h_out($dealer_name); ?></td>
                    <td><input type="text" maxlength="9" style="border:solid 1px red; ime-mode:disabled;" class="tbl_txt right quantity tab2" name="data[d2items][<?php echo $cnt; ?>][quantity]" tabindex="<?php echo $cnt * 10 + 2;?>" /></td>
                    <td><input type="text" maxlength="13" style="border:solid 1px red; ime-mode:disabled;" class="tbl_txt right transaction_price tab3" name="data[d2items][<?php echo $cnt; ?>][transaction_price]" tabindex="<?php echo $cnt * 10 + 3;?>"/></td>
                    <td><input type="text" class="date validated_date tab6" name="data[d2items][<?php echo $cnt; ?>][validated_date]" tabindex="<?php echo $cnt * 10 + 6;?>" /></td>
                    <td><input type="text" maxlength="200" class="tbl_txt recital tab8" name="data[d2items][<?php echo $cnt; ?>][recital]" id="bikou<?php echo $cnt; ?>" tabindex="<?php echo $cnt * 10 + 8;?>" value=""/></td>
                </tr>
        <?php $cnt++;//セルの色変更用
             } //endforeach
        ?>
            </tbody>
        </table>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>
    <div class="ButtonBox" id="page_footer">
        <input type="button" class="btn btn2 [p2]" id="btn_add_result" name="btn_add_result"/>
    </div>
</form>

<?php }//endif ?>