<script type="text/javascript">
$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
  
    $('#confirm_btn').click(function(){
        //チェックボックスカウント
        if($('input[type=checkbox].checkAllTarget:checked').length > 0){
             $('#confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/receiving_result').submit();
        }else{
            alert('登録情報を選択してください。');
        }

    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>受注登録</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>受注登録</span></h2>
<?php echo $this->form->create('Edi',array('class'=>'validate_form search_form','id' =>'confirm','method'=>'post')); ?>
    <?php echo $this->form->input('Edi.token' , array('type'=>'hidden'));?>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <h2 class="HeaddingMid">発注情報</h2>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>受注日</th>
                <td><?php echo($this->form->text('Order.edi_date',array('class'=>'r date validate[required,custom[date]] order_date','maxlength'=>'10','id'=>'work_date','label'=>''))); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo($this->form->text('Order.recital',array('class'=>'txt','label'=>'',"maxlength"=>200))); ?></td>
            </tr>
        </table>
    </div>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th rowspan="2" class="col5">
                    <input type="checkbox"  checked="checked" class='checkAll'/>
                </th>
                <th class="col15">施設名</th>
                <th class="col10">商品ID</th>
                <th class="col20">商品名</th>
                <th class="col20">製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col20">発注区分</th>
            </tr>
            <tr>
                <th>発注番号</th>
                <th>発注日</th>
                <th>規格</th>
                <th>販売元</th>
                <th>数量</th>
                <th>備考</th>
            </tr>
            <?php $i = 0;foreach($SearchResult as $item){ ?>
            <tr>
                <td rowspan="2" class="col5 center">
                    <?php echo $this->form->checkbox('Order.id.'.$i , array('value'=>$item['Order']['id'] , 'class' => 'checkAllTarget id' , 'checked'=>'checked' , 'hiddenField'=>false) ); ?>
                </td>
                <td class="col15"><?php echo h_out($item['Order']['facility_name']) ?></td>
                <td class="col10"><?php echo h_out($item['Order']['internal_code'],'center'); ?></td>
                <td class="col20"><?php echo h_out($item['Order']['item_name']) ?></td>
                <td class="col20"><?php echo h_out($item['Order']['item_code']) ?></td>
                <td class="col10"><?php echo h_out($item['Order']['unit_name'],'center'); ?></td>
                <td><?php echo h_out($item['Order']['order_type_name'],'center') ?></td>
            </tr>
            <tr>
                <td><?php echo h_out($item['Order']['work_no'])?></td>
                <td><?php echo h_out($item['Order']['work_date'],'center'); ?></td>
                <td><?php echo h_out($item['Order']['standard']) ?></td>
                <td><?php echo h_out($item['Order']['dealer_name']); ?></td>
                <td><?php echo h_out($item['Order']['quantity'],'right'); ?></td>
                <td><?php echo $this->form->input('Order.edi_comment.'.$item['Order']['id'] , array('type'=>'text' , 'class'=>'txt' ))?></td>
            </tr>
            <?php $i++; } ?>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" id="confirm_btn" class="common-button" value="確定"/>
        </p>
    </div>
<?php echo $this->form->end(); ?>
