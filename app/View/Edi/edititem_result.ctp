<script type="text/javascript">
$(document).ready(function(){
    $('.date').datepicker('destroy');
    $('select , input[type=text]').attr("readonly","readonly").removeClass('r txt date' ).addClass("lbl");
    $('input[type=button]').css("display","none");
    $('input[type=checkbox] , select').each(function(){
        $(this).after('<input type=hidden name=' + $(this).attr('name') + ' value=' + $(this).val() + ' >');
        $(this).attr('disabled' , 'disabled');
    });
});


</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/edititem_search">施設採用品一覧</a></li>
        <li class="pankuzu">施設採用品編集</li>
        <li>施設採用品編集結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>施設採用品登録結果</span></h2>
<div class="Mes01">施設採用品の編集を行いました</div>
<h2 class="HeaddingSmall">得意先 [ <?php echo $this->request->data['hospitalName']; ?> ]</h2>
<h2 class="HeaddingSmall">施設採用品情報</h2>
<div>
    <?php echo $this->element('masters/facility_items/item_Table'); ?>
</div>

<?php if (isset($this->request->data['Grandmaster']['per_unit']) && $this->request->data['Grandmaster']['per_unit'] != ''): ?>
<h2 class="HeaddingSmall">グランド基本単位</h2>
<div class="SearchBox">
    <table class="FormStyleTable">
        <tr>
            <th>基本単位</th>
            <td>
                <?php echo $this->form->input('Grandmaster.unit_name',array('class'=>'lbl','style'=>'width:120px;','readonly'=>'readonly')); ?>
            </td>
            <td width="20"></td>
            <th>参考包装単位（1／<?php echo $this->request->data['Grandmaster']['per_unit']; ?><?php echo $this->request->data['Grandmaster']['unit_name']; ?>）</th>
        </tr>
    </table>
</div>
<?php endif; ?>
<h2 class="HeaddingSmall">採用包装単位</h2>
<div class="SearchBox">
    <table class="FormStyleTable">
        <tr>
            <th>基本単位</th>
            <td>
                <?php echo $this->form->input('MstFacilityItem.basic_unit_name_id',array('options'=>$item_units, 'value'=>$this->request->data['MstFacilityItem']['basic_unit_name_id'], 'class'=>'lbl','disabled'=>'disabled')); ?>
            </td>
            <td width="20"></td>
            <th>換算数</th>
            <td>
                <?php echo $this->form->input('MstFacilityItem.converted_num',array('label'=>false,'class'=>'lbl num','maxlength'=>50,'style'=>'width:60px','readonly'=>'readonly')); ?>
            </td>
        </tr>
    </table>
</div>

<h2 class="HeaddingSmall">採用包装単位</h2>
<div class="TableHeaderAdjustment01">
    <table class="TableHeaderStyle01">
        <tr>
            <th class="col5"></th>
            <th>包装単位</th>
            <th>入数</th>
            <th>代表</th>
            <th>採用開始日</th>
            <th>採用中止日</th>
            <th width="35">適用</th>
        </tr>
    </table>
</div>
<div class="TableScroll">
    <table class="TableStyle01 itemUnitTable" id="itemUnitTable">
        <?php foreach($this->request->data['MstItemUnit']['id'] as $i => $id): ?>
        <tr id="itemUnitrow">
            <td class="col5"></td>
            <td>
                <?php echo $this->form->input('MstItemUnit.mst_unit_name_id',array('options'=>$item_units, 'value'=>$this->request->data['MstItemUnit']['mst_unit_name_id'][$i], 'class'=>'lbl','disabled'=>'disabled')); ?>
            </td>
            <td>
                <?php echo $this->form->input('MstItemUnit.per_unit',array('class'=>'tbl_lbl','disabled'=>'disabled','value'=>$this->request->data['MstItemUnit']['per_unit'][$i])); ?>
            </td>
            <td align="center">
                <?php if ($this->request->data['MstItemUnit']['is_standard'] == $i){ echo "○"; } ?>
            </td>
            <td>
                <?php echo $this->form->input('MstItemUnit.start_date',array('type'=>'text','label'=>false,'class'=>'tbl_lbl','disabled'=>'disabled','value'=>$this->request->data['MstItemUnit']['start_date'][$i])); ?>
            </td>
            <td>
                <?php echo $this->form->input('MstItemUnit.end_date',array('type'=>'text','label'=>false,'class'=>'tbl_lbl','disabled'=>'disabled','value'=>$this->request->data['MstItemUnit']['end_date'][$i])); ?>
            </td>
            <td class="center" width="35">
                <?php if(isset($this->request->data['MstItemUnit']['is_deleted'][$i])){echo "○";} ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>
