<script>
$(document).ready(function() {
    //チェックボックス一括変更
    $("#allCheck").click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    //選択ボタン押下
    $("#btn_Mod").click(function(){
        $("#selectReceiving").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/shipping_confirm').submit();
    });
    //読み込みボタン押下
    $("#btn_Read").click(function(){
        if($('input[type=checkbox].chk:checked').length === 0){
            alert("受注データを選択してください。");
        } else {
            var sendFlg = true;
            var department = '';
            $('input[type=checkbox].checkAllTarget').each(function(i){
                if(this.checked){
                    if(department == ''){
                        department = input.find('input[type=hidden].department_id:eq(' + i + ')').val();
                    } else {
                        if(department != input.find('input[type=hidden].department_id:eq(' + i + ')').val()){
                            sendFlg = false;
                        }
                    }
                }
            });
            if(sendFlg){
                $("#selectReceiving").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/shipping_read').submit();
            } else {
                alert("読み込みの場合、出荷先は複数選択できません。");
            }
        }
    });
    //検索ボタン押下
    $("#btn_Search").click(function(){
        //バリデート無効化 && ぐるぐる表示 && submit!!
        validateDetachSubmit($('#selectReceiving') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/shipping_select' );
    });
});
</script>
<div id="TopicPath">
    <ul>
         <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
         <li>出荷登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>出荷登録</span></h2>
<h2 class="HeaddingMid">受注情報選択</h2>
<?php echo $this->form->create('Receiving',array('class'=>'validate_form search_form','id' =>'selectReceiving')); ?>
<?php echo $this->form->input('Receiving.is_search' , array('type'=>'hidden'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('Receiving.facility_id' ,array('options'=>$Facility_List , 'class'=>'txt' , 'empty'=>'') ); ?></td>
                <td></td>
                <th>受注日</th>
                <td>
                <?php echo $this->form->input('Receiving.work_date_from',array('class'=>'date validate[custom[date]]','div' => 'false','label' => 'false','maxlength' => '10')); ?>
                <span> ～ </span>
                <?php echo $this->form->input('Receiving.work_date_to',array('class'=>'date validate[custom[date]]','div' => 'false','label' => 'false','maxlength' => '10')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>受注番号</th>
                <td><?php echo $this->form->input('Receiving.work_no' ,array('type'=>'text' , 'class'=>'txt' , 'empty'=>'') ); ?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>

        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Search" value="検索"/>
    </div>

    <?php if(isset($this->request->data['Receiving']['is_search'])){ ?>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
        </span>
        <span class="BikouCopy"></span>
    </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col class="col15" />
                    <col class="col10" />
                    <col />
                    <col class="col10" />
                </colgroup>
                <thead>
                <tr>
                    <th><input type="checkbox" id="allCheck" /></th>
                    <th>施設名</th>
                    <th>受注番号</th>
                    <th>受注日</th>
                    <th>登録者</th>
                    <th>件数</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=0 ; foreach ($SearchResult as $r) { ?>
                <tr>
                    <td class="center"><input name="data[Edi][id][]" type="checkbox" class="validate[required] chk" id="check<?php echo $r['Edi']['id']; ?>" value="<?php echo $r['Edi']['id']; ?>" /></td>
                    <td>
                        <?php echo h_out($r['Edi']['facility_name'] . ' / ' . $r['Edi']['department_name']); ?>
                        <?php echo $this->form->input('Edi.department_id.'.$i , array('type'=>'hidden' , 'class'=>'department_id','value'=>$r['Edi']['department_id']))?>
                    </td>
                    <td><?php echo h_out($r['Edi']['work_no'] , 'center'); ?></td>
                    <td><?php echo h_out($r['Edi']['work_date'] ,'center'); ?></td>
                    <td><?php echo h_out($r['Edi']['user_name']); ?></td>
                    <td><?php echo h_out($r['Edi']['count'] . ' / ' . $r['Edi']['detail_count'] , 'right'); ?></td>
                </tr>
                <?php  $i++; } ?>
                <?php if( count($SearchResult)==0 && isset($this->request->data['Receiving']['is_search']) ){ ?>
                <tr><td colspan="6" class="center">該当データがありませんでした</td></tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
    </div>
    <?php if(count($SearchResult) > 0  ){ ?>
    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_Mod" value="選択"/>
        <input type="button" class="common-button"  id="btn_Read" value="バーコード読込"/>
    </div>
    <?php } ?>
</form>

<?php } ?>
