<script type="text/javascript">
var webroot = "<?php echo $this->webroot; ?>";

    $(document).ready(function(){
        $('#spare_table').hide();
        $("#spare_header").click(function () {
            $('#spare_table').slideToggle('slow');
        });

        $('#change_masterId').show();

        // 付替ボタン押下
        $('#change_masterId').click(function () {
            if( $('#master_id').val() == ''){
                alert('マスタIDを入力してください');
                $("#master_id").focus();
                return false;
            }
            $("#item_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edititem/'+$('#master_id').val());
            $("#item_form").attr('method', 'post');
            $("#item_form").submit();
        });

        // 確定ボタン押下
        $("#edit_btn").click(function(){

            var SubmitFlg = true;
            if(!dateCheck($('#datepicker1').val())){
                alert('適用開始日が不正です');
                $('#datepicker1').focus();
                SubmitFlg = false;
            }
            if($('#datepicker2').val() != '' ){
                if(!dateCheck($('#datepicker2').val())){
                    alert('適用終了日が不正です');
                    $('#datepicker2').focus();
                    SubmitFlg = false;
                }
            }
            if($('#RefundPrice').val() != ''){
                if(!numCheck($('#RefundPrice').val() , 12 , 2 )){
                     alert('償還価格が不正です');
                     $('#RefundPrice').focus();
                     SubmitFlg = false;
                }
            }

            if($('#UnitPrice').val() != ''){
                if(!numCheck($('#UnitPrice').val() , 12 , 2)){
                    alert('定価が不正です');
                    $('#UnitPrice').focus();
                    SubmitFlg = false;
                }
            }

            if(!numCheck( $('#ConvertedNum').val(), 12 , 3 , true)){
                alert('換算数が不正です');
                $('#ConvertedNum').focus();
                SubmitFlg = false;
            }

            //基本単位入力チェック
            if($('#basic_item_unit_name').val() == '' ){
                 alert('基本単位を選択してください');
                 SubmitFlg = false;
            }

            // 有効包装単位数チェック
            if($('input[type=checkbox].chk:checked').length == 0 ){ 
                alert('適用する包装単位を1個以上選択してください');
                SubmitFlg = false;
            }

            if($('input[type=radio].standard_flags:checked').val() == undefined ){
                alert('代表包装単位を選択してください');
                SubmitFlg = false;
            }

            var per_unit_array = new Array();
            // いり数型チェック
            $('input[type=text].per_unit').each(function(){
                if(!numCheck($(this).val() , 12 )){
                    alert("包装単位入数は数字で入力してください");
                    SubmitFlg = false;
                }
                if(per_unit_array.in_array($(this).val())){
                    alert("包装単位入数が同じものは登録できません");
                    SubmitFlg = false;
                }
                per_unit_array.push($(this).val());
            });

            // 採用開始日 型チェック
            $('input[type=text].start_date').each(function(){
                if(!dateCheck($(this).val())){
                    alert('採用開始日が不正です');
                    $(this).focus();
                    SubmitFlg = false;
                }
            });

            // 採用中止日 型チェック
            $('input[type=text].end_date').each(function(){
                if($(this).val() != ''){
                    if(!dateCheck($(this).val())){
                        alert('採用中止日が不正です');
                        $(this).focus();
                        SubmitFlg = false;
                    }
                }
            });
            if($('#expired_date').val() != ''){
                if(!numCheck($('#expired_date').val() , 12 )){
                    alert("期限切れ警告日数は数字で入力してください");
                    $('#expired_date').focus();
                    SubmitFlg = false;
                }
            }

            // 予備キーバリデーション
            $('form.search_form').find('.sparekey').each(function(){
                if($(this).hasClass('r') && $(this).val() == ''){
                    $('#spare_table').show();
                    $(this).focus();
                    alert($('.' + $(this).attr("id") + 'Name').html() + "は必須入力です");
                    SubmitFlg = false;
                    return false;
                }
                if($(this).hasClass('numeric') && $(this).val() != ''){
                    if(!numCheck($(this).val(), 50)){
                        $('#spare_table').show();
                        $(this).focus();
                        alert($('.' + $(this).attr("id") + 'Name').html() + "は数字で入力してください");
                        SubmitFlg = false;
                        return false;
                    }
                }
            });

            if(SubmitFlg){
                if( $('#MstFacilityItemSealissueMedicalcode').val() == 0 &&
                  ( $('#insurance_claim_type_sel').val() != ''  &&
                    $('#insurance_claim_type_sel').val() != 6 ) ) {
                    if(window.confirm("医事シールが印字されない設定ですがよろしいですか？")){
                        $("#item_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edititem_result').submit();
                    }
                }else{
                    $("#item_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edititem_result').submit();
                }
            }
       });

       // 行追加ボタン押下
       $('#add_row_btn').click(function() {
        var date = new Date();
        var mm = date.getMonth() + 1;
        if (mm < 10) { mm = "0" + mm; }
        _today = date.getFullYear()  + "/" + mm + "/01"; //当月1日を入力

        //代表フラグをクリア
        $('.standard_flags').attr('checked','');
        var tr_count = $('#itemUnitTable tr').size(); //TR要素をカウント

        $('#itemUnitTable').append(
            $('<tr>').append(
                    $('<td>').append('<input type="hidden" name="data[MstItemUnit][id]['+tr_count+']" value="">')
                ).append(
                    $('<td>').append('<select class="txt" id="per_unit_id_'+tr_count+'" name="data[MstItemUnit][mst_unit_name_id]['+tr_count+']"></select>') //包装単位
                ).append(
                    $('<td class="num">').append('<input type="text" class="r txt num per_unit" name="data[MstItemUnit][per_unit]['+tr_count+']">') //いり数
                ).append(
                    $('<td class="center">').append('<input type="radio" class="standard_flags" name="data[MstItemUnit][is_standard]" value="'+tr_count+'">') //代表
                ).append(
                    $('<td>').append('<input type="text" class="r date start_date" name="data[MstItemUnit][start_date]['+tr_count+']" value="'+_today+'">') //採用開始日
                ).append(
                    $('<td>').append('<input type="text" class="date end_date" name="data[MstItemUnit][end_date]['+tr_count+']">') //採用中止日
                ).append(
                    $('<td width="35" class="center">').append('<input type="checkbox" name="data[MstItemUnit][is_deleted]['+tr_count+']"  class="chk" checked>') //適用フラグ
                )
        )
        $('#per_unit_id_'+tr_count).append( $('#basic_item_unit_name option').clone() );

        // 追加要素のカレンダー
        $('.date').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
    });
});

</script>

<script type="text/javascript" src="<?php echo $this->webroot; ?>js/facility_item.js"></script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/edititem_search">施設採用品一覧</a></li>
        <li>施設採用品編集</li>
    </ul>
</div>

<?php echo $this->element('masters/facility_items/item_hiddenAjaxForm'); ?>

<h2 class="HeaddingLarge"><span>施設採用品編集</span></h2>
<h2 class="HeaddingSmall">得意先 [ <?php echo $this->request->data['hospitalName']; ?> ]</h2>
<h2 class="HeaddingSmall">施設採用品情報</h2>
<form class="validate_form search_form" id="item_form"  method="post">
    <?php echo $this->form->input('MstFacilityItem.isOriginal', array('type'=>'hidden', 'value'=>0)); ?>
    <?php echo $this->form->input('MstFacilityItem.id',array('type'=>'hidden')); ?>
    <?php echo $this->form->input('MstFacilityItem.mst_facility_id',array('type'=>'hidden')); ?>
    <?php echo $this->form->input(sprintf("%s.%s",$this->name,"token"), array('type'=>'hidden'));  ?>
    <?php echo $this->form->input('MstFacilityItem.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u'))); ?>
    <?php echo $this->form->input('centerId', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('hospitalName', array('type'=>'hidden')); ?>
    <div class="SearchBox">
        <div>
            <?php echo $this->element('masters/facility_items/item_Table');?>
        </div>
        <?php if (isset($this->request->data['MstFacilityItem']['master_id']) && $this->request->data['MstFacilityItem']['master_id'] != ''): ?>
        <h2 class="HeaddingSmall">グランド基本単位</h2>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <tr>
                    <th>基本単位</th>
                    <td>
                        <?php echo $this->form->input('MstGrandmaster.unit_name',array('type'=>'text' , 'class'=>'lbl', 'style'=>'width: 120px;')); ?>
                    </td>
                    <td width="20"></td>
                    <th>参考包装単位（<?php echo $this->request->data['MstGrandmaster']['unit_name']; ?>／箱）</th>
                </tr>
            </table>
        </div>
        <?php endif; ?>
        <h2 class="HeaddingSmall">採用基本単位</h2>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <tr>
                    <th>基本単位</th>
                    <td>
                        <?php echo $this->form->input('MstFacilityItem.basic_unit_name_id',array('options'=>$item_units, 'class'=>'txt','id'=>'basic_item_unit_name')); ?>
                    </td>
                    <td width="20"></td>
                    <th>換算数</th>
                    <td>
                        <?php echo $this->form->input('MstFacilityItem.converted_num',array('type'=>'text','class'=>'r txt','maxlength'=>9,'style'=>'width:60px','id'=>'ConvertedNum')); ?>
                    </td>
                </tr>
            </table>
        </div>
        <input type="button" id="add_row_btn" value="行追加" />
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle01">
                <colgroup>
                    <col class="col5"; />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col width="40" />
                </colgroup>
                <tr>
                    <th></th>
                    <th>包装単位</th>
                    <th>入数</th>
                    <th>代表</th>
                    <th>採用開始日</th>
                    <th>採用中止日</th>
                    <th class="center">適用</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 itemUnitTable" id="itemUnitTable">
                <colgroup>
                    <col class="col5" />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col width="35" />
                </colgroup>
                <?php $i = 0; foreach($item_units_list as $u) { ?>
                <tr>
                    <td></td>
                    <td>
                        <?php echo $this->form->input('MstItemUnit.id.'.$i, array('type'=>'hidden','value'=>$u['MstItemUnit']['id'])) ?>
                        <?php echo $this->form->input('MstItemUnit.mst_unit_name_id.'.$i ,array('options'=>$item_units,'value'=>$u['MstItemUnit']['mst_unit_name_id'], 'class'=>'txt validate[required]')); ?>
                    </td>
                    <td class="num">
                        <?php echo $this->form->input('MstItemUnit.per_unit.'.$i,array('type'=>'hidden','class'=>'per_unit' , 'value'=>$u['MstItemUnit']['per_unit'] )); ?>
                        <?php echo $this->form->input('MstItemUnit.per_unit_dummy',array('class'=>'tbl_lbl num per_unit','disabled'=>true,'value'=>$u['MstItemUnit']['per_unit'])); ?>
                    </td>
                    <td class="center">
                    <?php echo $this->form->radio('MstItemUnit.is_standard' , array($i => '') , array('hiddenField'=>false , 'label'=>false , 'class'=>'standard_flags' , 'checked'=>$u['MstItemUnit']['is_standard'])); ?>
                    </td>
                    <td>
                        <?php echo $this->form->input('MstItemUnit.start_date.'.$i,array('type'=>'text','class'=>'r date start_date validate[required]','value'=>$u['MstItemUnit']['start_date'],'maxlength'=>10)); ?>
                    </td>
                    <td>
                        <?php echo $this->form->input('MstItemUnit.end_date.'.$i,array('type'=>'text','class'=>'date end_date','value'=>$u['MstItemUnit']['end_date'],'maxlength'=>10));?>
                    </td>
                    <td width="35" class="center">
                        <?php echo $this->form->input('MstItemUnit.is_deleted.'.$i , array('type'=>'checkbox' , 'class'=>'chk' , 'hiddenField'=>false , 'checked'=>$u['MstItemUnit']['checked'] )); ?>
                    </td>
                 </tr>
                 <?php $i++; } ?>
             </table>
         </div>
         <div class="ButtonBox">
             <p class="center"><input type="button" class="btn btn2" id="edit_btn" /></p>
         </div>
     </div>
</form>
