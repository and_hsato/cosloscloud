<script>
$(function(){
    var seal_print = true;
  
    //チェックボックス制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    //部署シール印刷ボタン押下
    $("#hospital").click(function(){
        var check_flg = false;
        if($('input[type=checkbox].chk:checked').length > 0 ){
                $('input[type=checkbox].chk').each(function(i){
                    if(this.checked){
                        var index = $('input[type=checkbox].chk').index(this);
                        var q = $('input[type=hidden].order_remain_count:eq(' + index + ')').val();
                        if(q != '0'){
                            check_flg = true;
                        }
                    }
                });
            if(check_flg){
                if(window.confirm("未納商品を含みますがよろしいですか？")){
                    if(seal_print){
                        $("#RecivingList").attr('action', '<?php echo $this->webroot; ?>PdfReport/sticker').attr('target','_blank').submit();
                        seal_print = false;
                    }else{
                        alert('シール印刷処理中です。しばらくお待ちください。');
                    }
                }
            }else{
                if(seal_print){
                    $("#RecivingList").attr('action', '<?php echo $this->webroot; ?>PdfReport/sticker').attr('target','_blank').submit();
                    seal_print = false;
                } else {
                     alert("シール印刷処理中です。しばらくお待ちください。");
                }
            }
        } else {
            alert('印字対象を選択してください。');
        }
    });
  
    //コストシール印字ボタン押下
    $("#cost").click(function(){
        var check_flg = false;
        if($('input[type=checkbox].chk:checked').length > 0 ){
                $('input[type=checkbox].chk').each(function(i){
                    if(this.checked){
                        var index = $('input[type=checkbox].chk').index(this);
                        var q = $('input[type=hidden].order_remain_count:eq(' + index + ')').val();
                        if(q != '0'){
                            check_flg = true;
                        }
                    }
                });
            if(check_flg){
                if(window.confirm("未納商品を含みますがよろしいですか？")){
                    $("#RecivingList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/cost_history').submit();
                }
            }else{
                $("#RecivingList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/cost_history').submit();
            }
        } else {
            alert('印字対象を選択してください。');
        }
    });
  
    //取消ボタン押下
    $("#btn_Mod").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0 ){
            if(window.confirm("取消を実行します。よろしいですか？")){
                $("#RecivingList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/shipping_history_result').submit();
            }
        } else {
            alert('必ず一つは選択してください');
        }
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/shipping_history">出荷履歴</a></li>
        <li>出荷履歴編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>出荷履歴編集</span></h2>
<h2 class="HeaddingMid">以下の商品の出荷履歴の編集を行います。</h2>
<form method="post" action="" accept-charset="utf-8" id="RecivingList" class="validate_form">
    <?php echo $this->form->input('Edi.token' , array('type'=>'hidden')); ?>
    <?php if(isset($center_id)){ ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$center_id));?>
    <?php } ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
        </table>
    </div>
    <div class="results">
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
            <span class="DisplaySelect">
            　表示件数：<?php echo count($result);  ?>件
            </span>
            <span class="BikouCopy">
                <input type="text" class="txt" id="recitalTxt" />
                <input type="button" class="btn btn8 [p2]" id="recitalBtn"/>
            </span>
        </div>
        <div class="TableScroll2">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="25" />
                    <col width="140" />
                    <col width="85" />
                    <col width="80" />
                    <col />
                    <col />
                    <col width="70" />
                    <col width="110"/>
                    <col width="140"/>
                    <col width="80"/>
                </colgroup>
                <tr>
                    <th rowspan='2'><input type='checkbox' checked class='checkAll'/></th>
                    <th>出荷番号</th>
                    <th>出荷日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>ロット番号</th>
                    <th>作業区分</th>
                    <th>備考</th>
                </tr>
                <tr>
                    <th colspan='2'>出荷施設</th>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>数量</th>
                    <th>有効期限</th>
                    <th>発注番号</th>
                    <th>更新者</th>
                </tr>
                <?php $hospital_flg = false;  ?>
                <?php $cost_flg = false;  ?>
                <?php $cnt=0; foreach ($result as $r) { ?>
                <?php if($r['Edi']['hospital_flg']){ $hospital_flg = true; } ; ?>
                <?php if($r['Edi']['cost_flg']){ $cost_flg = true; } ; ?>
                <tr>
                    <td rowspan='2' class='center'>
                    <?php if($r['Edi']['canCancel']){ ?>
                    <?php echo $this->form->input('Edi.id.'.$cnt , array('type' => 'checkbox' , 'class'=>'chk' , 'value' => $r['Edi']['id'] ,  'hiddenField'=>false , 'checked' => true))?>
                    <?php echo $this->form->input('Edi.order_remain_count.'.$cnt , array('type' => 'hidden' ,'class'=>'order_remain_count','value' => $r['Edi']['order_remain_count'] ))?>
                    <?php } ?>
                    </td>
                    <td><?php echo h_out($r['Edi']['work_no']); ?></td>
                    <td><?php echo h_out($r['Edi']['work_date'] , 'center'); ?></td>
                    <td><?php echo h_out($r['Edi']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['Edi']['item_name']); ?></td>
                    <td><?php echo h_out($r['Edi']['item_code']); ?></td>
                    <td><?php echo h_out($r['Edi']['unit_name']); ?></td>
                    <td><?php echo h_out($r['Edi']['lot_no']); ?></td>
                    <td></td>
                    <td><?php echo h_out($r['Edi']['recital']); ?></td>
                </tr>
                <tr>
                    <td colspan='2'><?php echo h_out($r['Edi']['facility_name'] . ' / ' . $r['Edi']['department_name']); ?></td>
                    <td></td>
                    <td><?php echo h_out($r['Edi']['standard']); ?></td>
                    <td><?php echo h_out($r['Edi']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['Edi']['quantity'],'right'); ?></td>
                    <td><?php echo h_out($r['Edi']['validated_date'],'center'); ?></td>
                    <td><?php echo h_out($r['Edi']['order_work_no']); ?></td>
                    <td><?php echo h_out($r['Edi']['user_name']); ?></td>
                </tr>
                <?php $cnt++; } ?>
            </table>
        </div>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect"></span>
        <span class="BikouCopy">
        </span>
    </div>

    <div class="ButtonBox" id="page_footer">
        <input type="button" class="common-button" id="btn_Mod" value="取消"/>
        <?php if($hospital_flg){ ?>
        <input type="button" class="common-button" id="hospital" value="部署シール印刷"/>
        <?php } ?>
        <?php if($cost_flg){ ?>
        <input type="button" class="common-button" id="cost" value="コストシール印字"/> 
        <?php } ?>
    </div>
</form>
