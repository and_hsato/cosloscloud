<script type="text/javascript">
$(document).ready(function(){
    $('#search').click(function(){
        if(!dateCheck($('#dateFrom').val()) && $('#dateFrom').val() != "" ){
            alert('日付開始が不正です。');
            return false;
        }else if(!dateCheck($('#dateTo').val()) && $('#dateTo').val() != ""){
            alert('日付終了が不正です。');
            return false;
        }else if(!Chk_DayNum()){
            return false;
        }else{
            $('#msg').text('CSV出力中です。しばらくお待ちください。');
            $('#downloadForm').attr('action', '<?php echo $this->webroot; ?>downloads/search').submit();
            //$('#exportCSV').attr('disabled' , 'disabled');
        }
    });
    $('#dateFrom').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
    $('#dateTo').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});

});
/**
 * 日数の入力値チェック
 */
function Chk_DayNum(){
    var numcheck = true;
    var num = $('#dayNum').val();
    if(num !=null && num!=''){
      if(num.match(/[^0-9]/g)){
           alert('日数が不正です');
           $('#dayNum').val('');
           numcheck=false;
       }
  }
  return numcheck;
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>データダウンロード</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>シール・仕入・売上データ出力</span></h2>
<h2 class="HeaddingMid">出力を行う明細と条件を選択してください</h2>

<div id="msg" class="err" style="font-size:20px"></div>

<form id="downloadForm" method="POST" action="<?php echo $this->webroot; ?>downloads/search">
<input type="hidden" name="data[IsSearch]" value="1" />
<hr class="Clear" />
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col width="220" />
                <col />
                <col width="500" />
            </colgroup>
            <tr>
                <th>ファイル種別</th>
                <td>
                    <?php echo $this->form->input('Download.file_type',array('options'=>$fileTypes, 'empty'=>true, 'class'=>'txt','id'=>'fileTypeSel')); ?>
                </td>
                <th>出力日</th>
                <td>
                    <?php echo $this->form->input('Download.day_from', array('type'=>'text','class'=>'date','id'=>'dateFrom')); ?>
                    &nbsp;～&nbsp;
                    <?php echo $this->form->input('Download.day_to', array('type'=>'text' , 'class'=>'date','id'=>'dateTo')); ?>
                </td>
            </tr>
         </table>
    </div>
    <br>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" value="" class="btn btn1" id="search" />
        </p>
    </div>

    <hr class="Clear" />
    <div id="results">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
            <div class="TableHeaderAdjustment01">
                <table class="TableHeaderStyle01">
                    <tr>
                        <th style="width: 20px;"><input type="checkbox" class="checkAll" checked="checked"/></th>
                        <th style="width: 60px;">ファイル種別</th>
                        <th style="width: 280px;" >ファイル名</th>
                        <th style="width: 100px;" >日付</th>
                    </tr>
                </table>
            </div>
<?php
            if(count($SearchResult) === 0 && isset($this->request->data['IsSearch'])){
?>
                <div class="TableScroll" style="">
                    <table class="TableStyle01">
                        <tr><td colspan="8" class="center"><span>該当データがありません。</span></td></tr>
                    </table>
                </div>
<?php
            }else{
?>
                <div class="TableScroll" style="">
                    <table class="TableStyle01">
                    <?php $_cnt=0;foreach($SearchResult as $_row): ?>
                        <tr class="<?php echo($_cnt%2===0?'':'odd'); ?> center">
                            <td style="width:20px;"><?php echo $this->form->checkbox("TrnOrderHeader.Rows.{$_row['trn_order_header_id']}", array('class'=>'center checkAllTarget','checked' => 'checked')); ?></td>
                            <td style="width: 140px;"><label title="<?php echo($_row['work_no']); ?>"><p align="center"><?php echo($_row['work_no']); ?></p></label></td>
                            <td style="width: 80px;" align="center"><label title="<?php echo(date('Y/m/d',strtotime($_row['work_date']))); ?>"><p align="left"><?php echo(date('Y/m/d',strtotime($_row['work_date']))); ?></p></label></td>
                        </tr>
                    <?php $_cnt++;endforeach;?>
                    </table>
                </div>
                <?php if(count($SearchResult) > 0): ?>
                <div class="ButtonBox">
                    <p class="center">
                        <input type="button" class="confirm_btn" value="　ダウンロード　" id="cmdConfirm" />
                    </p>
                </div>
                <?php endif; ?>
<?php
            }
?>
        </div>
    </div>

</form>
