<script type="text/javascript">
  $(function(){
        $("#cmdCenterSticker").click(function(){
            var _cnt = 0;
            var _i = 0;
            var _falsecnt = 0;
            $('input[type=checkbox].checkAllTarget').each(function(){
                if(this.checked){
                    var w = $('p.f_sticker:eq(' + _i + ')').text();
                    _cnt++;
                    if(w.length > 0 && w != ' '){
                    }else{
                        _falsecnt++;
                    }
                }
                _i++;
            });
            if(_cnt === 0){
                alert('センターシールを印刷する商品を選択してください');
                return false;
            }else if(_falsecnt != 0){
                alert('センターシール番号が設定されていないものが '+_falsecnt+' 件選択されています\n確認して下さい');
                return false;
            }else{
                if(confirm('センターシールの印字を行います。\nよろしいですか？')){
                    $("#form_edit").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/1').submit();
                    $("#form_edit").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
                }
            }
        });
        $("#cmdHospitalSticker").click(function(){
            var _cnt = 0;
            var _i = 0;
            var _falsecnt = 0;
            $('input[type=checkbox].checkAllTarget').each(function(){
                if(this.checked){
                    var v = $('p.h_sticker:eq(' + _i + ')').text();
                    _cnt++;
                    if(v.length > 0 && v != ' '){
                    }else{
                        _falsecnt++;
                    }
                    _cnt++;
                }
                _i++;
            });
            if(_cnt === 0){
                alert('部署シールを印刷する商品を選択してください');
                return false;
            }else if(_falsecnt != 0){
                alert('部署シール番号が設定されていないものが '+_falsecnt+' 件選択されています\n確認して下さい');
                return false;
            }else{
                if(confirm('部署シールの印字を行います。\nよろしいですか？')){
                    $("#form_edit").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/2').submit();
                    $("#form_edit").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
                }
            }
        });

        $('#btn_Print').click(function(){//金額差異一覧印刷
            $('#form_edit').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/dummy').submit();
            $("#form_edit").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
        });

        // CSVボタン押下
        $('#btn_Csv').click(function(){//CSV
            $('#form_edit').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
            $("#form_edit").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
        });

        // 編集ボタン押下
        $("#btn_edit").click(function(){
            var _cnt = 0;
            var _i = 0;
            $('input[type=checkbox].checkAllTarget').each(function(){

                if(this.checked){
                    _cnt++;
                    m = $('#trnSticker_id_'+ _i ).val();
                    $("#trnsticker_id_selected").val(m);
                }
                _i++;
            });
            if(_cnt === 0){
                alert('編集するシールを一つのみ選択してください');
                return false;
            }else if(_cnt != 1){
                alert('複数のシールが選択されています\n一つのみ選択して下さい');
                return false;
            }else{
                $("#form_edit").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit').submit();
            }
        });

        // 検索ボタン押下
        $("#btn_search").click(function(){
            $("#form_edit").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').submit();
        });

        $("#cmdCostSticker").click(function(){
            var _cnt = 0;
            var _i = 0;
            var _falsecnt = 0;
            _chkboxidx = new Array();

            $('input[type=checkbox].checkAllTarget').each(function(){
                if(this.checked){
                    var v = $('p.h_sticker:eq(' + _i + ')').text();
                    var w = $('p.f_sticker:eq(' + _i + ')').text();

                    _cnt++;
                    if(v.length > 0 || w.length > 0){
                    }else{
                        _falsecnt++;
                        _chkboxidx = _i;
                    }
                }
                _i++;
            });
            if(_cnt === 0){
                alert('コストシールを印刷する商品を選択してください');
                return false;
            }else if(_falsecnt != 0){
                alert('印刷不可シールが '+_falsecnt+' 件選択されてます\n確認して下さい');
                return false;
            }else{
                if(confirm('コストシールの印字を行います。\nよろしいですか？')){
                    $("#form_edit").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/3').submit();
                    $("#form_edit").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
                }
            }
        });

        $('.checkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked',$(this).attr('checked'));
        });

        $('#FreeKeyWord').focus();
    });


function anc2(){
   obj = $('#pageDow')[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

function anc(id){
   obj = $('#'+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>シール検索</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>シール検索</span></h2>
<h2 class="HeaddingMid">シールを検索してください。</h2>

<form class="validate_form search_form" id="form_edit" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search" method="post">
    <?php echo $this->form->input('isSearch', array('type' => 'hidden', 'value' => '1')); ?>
    <input type="hidden" name="data[sticker_no]" id="sticker_no" value="" />
    <input type="hidden" name="data[facility_name]" id="sticker_no" value="<?php echo $facility_name; ?>" />
    <input type="hidden" name="data[post_facility_name]" id="post_facility_no" value="" />
    <input type="hidden" name="data[facility_id]" id="facility_id" value="<?php echo $facility_id; ?>" />
    <?php echo $this->form->input('TrnSticker.mst_user_id' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>配置施設</th>
                <td>
                    <?php echo $this->form->input('Sticker.facilityName' , array('type'=>'hidden' , 'id'=>'facilityName')); ?>
                    <?php echo $this->form->input('Sticker.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('Sticker.facilityCode',array('options'=>$facilities,'id' => 'facilityCode', 'class' => 'txt', 'empty' => true)); ?>
                </td>
                <td width="20"></td>
                <th>配置部署</th>
                <td>
                    <?php echo $this->form->input('Sticker.departmentName' , array('type'=>'hidden' , 'id'=>'departmentName')); ?>
                    <?php echo $this->form->input('Sticker.departmentText', array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'txt', 'style' => 'width: 60px;')); ?>
                    <?php echo $this->form->input('Sticker.departmentCode',array('options'=>$department_list,'id' => 'departmentCode', 'class' => 'txt', 'empty' => true)); ?>
                </td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>商品ID</th><td><?php echo $this->form->input('MstFacilityItem.internal_code', array('class' => 'txt search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th><td><?php echo $this->form->input('MstFacilityItem.item_code', array('class' => 'txt search_upper')); ?></td>
                <td></td>
                <th>ロット番号</th><td><?php echo $this->form->input('TrnSticker.lot_no', array('class' => 'txt')); ?></td>
            </tr>
            <tr>
                <th>商品名</th><td><?php echo $this->form->input('MstFacilityItem.item_name', array('type' => 'text', 'class' => 'txt search_canna', 'size' => 10)); ?></td>
                <td></td>
                <th>販売元</th><td><?php echo $this->form->input('MstDealer.dealer_name', array('class' => 'txt search_canna')); ?></td>
                <td></td>
                <th>シール区分</th><td><?php echo $this->form->input('TrnSticker.trade_type', array('options'=> Configure::read('Classes') , 'class' => 'txt' , 'empty'=>true)); ?></td>
            </tr>
            <tr>
                <th>規格</th><td><?php echo $this->form->input('MstFacilityItem.standard', array('type' => 'text', 'class' => 'txt search_canna', 'size' => 10)); ?></td>
                <td></td>
                <th>シール番号</th><td><?php echo $this->form->input('TrnSticker.facility_sticker_no', array('type' => 'text', 'class' => 'txt search_upper', 'size' => 10)); ?></td>
                <td></td>
                <td colspan="2"><span><?php echo $this->form->input('TrnSticker.is_active', array('type' => 'checkbox')); ?>有効なシールのみ</span></td>
            </tr>
            <tr>
                <th><?php echo $this->Session->read('Auth.Config.SpareKey1Title'); ?></th><td><?php echo $this->form->input('MstFacilityItem.spare_key1', array('type' => 'text', 'class' => 'txt')); ?></td>
                <td></td>
                <th><?php echo $this->Session->read('Auth.Config.SpareKey2Title'); ?></th><td><?php echo $this->form->input('MstFacilityItem.spare_key2', array('type' => 'text', 'class' => 'txt')); ?></td>
                <td></td>
                <th><?php echo $this->Session->read('Auth.Config.SpareKey3Title'); ?></th><td><?php echo $this->form->input('MstFacilityItem.spare_key3', array('type' => 'text', 'class' => 'txt')); ?></td>
            </tr>
            <tr>
                <th>フリー検索</th>
                <td colspan="8"><?php echo $this->form->input('Free.text', array('type' => 'text', 'id'=>'FreeKeyWord' ,'class' => 'txt', 'style' => 'width:100%;')); ?></td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="btn_search" />
                <!-- <input type="button" class="btn btn53" id="btn_Print" /> -->
                <input type="button" class="btn btn5" id="btn_Csv" />
            </p>
        </div>
        <hr class="Clear" />
        <div id="hideer">
            <?php if (isset($stickers) && count($stickers) > 0): ?>
                <div align="right" id="pageTop" ></div>
            <?php endif; ?>
            <h2 class="HeaddingSmall">シールの印字を行いたい商品を選択し、ボタンをクリックしてください。</h2>
            <div class="DisplaySelect">
                <?php echo $this->element('limit_combobox', array('result' => count($stickers))); ?>
                </div>
                <div class="TableScroll2">
                    <table class="TableStyle01 table-odd2">
                        <tr>
                            <th style="width:20px;" rowspan="2" class="center"><input type="checkbox" class="center" class="checkAll" /></th>
                            <th class="col10">商品ID</th>
                            <th class="col20">商品名</th>
                            <th class="col16">製品番号</th>
                            <th class="col10">包装単位</th>
                            <th class="col10">シール区分</th>
                            <th class="col10">ロット番号</th>
                            <th class="col13">配置施設</th>
                            <th class="col11">センターシール</th>
                        </tr>
                        <tr>
                            <th>医事コード</th>
                            <th>規格</th>
                            <th>販売元</th>
                            <th>仕入単価</th>
                            <th></th>
                            <th>有効期限</th>
                            <th>配置部署</th>
                            <th>部署シール</th>
                        </tr>
                    <?php if (isset($stickers) && count($stickers) > 0): ?>
                    <?php $i = 0; ?>
                    <?php foreach ($stickers as $index => $sticker): ?>
                    <?php if($sticker['TrnSticker']['quantity'] == 0 || $sticker['TrnSticker']['is_deleted'] == true ) { $class= 'tr-deleted';} else { $class='' ;}; ?>
                            <tr class="<?php echo $class; ?>">
                                <td rowspan="2" class="center">
                                <?php echo $this->form->input('StickerNo.'.$i.'.checked',array('type'=>'checkbox','class' => 'center chk checkAllTarget' , 'hiddenField'=>false ,'value' => 1) ); ?>
                                <input type="hidden" name="trnSticker_id_<?php echo h($i); ?>" id="trnSticker_id_<?php echo h($index)?>" value = "<?php echo $sticker['TrnSticker']['id']; ?>" />
                                <input type="hidden" name="data[StickerNo][<?php echo h($i); ?>][sticker_no]" value = "<?php echo $sticker['TrnSticker']['facility_sticker_no']; ?>" />
                                <input type="hidden" name="data[StickerNo][<?php echo h($i); ?>][hospital_sticker_no]" value = "<?php echo $sticker['TrnSticker']['hospital_sticker_no']; ?>" />
                                <input type="hidden" name="data[StickerNo][<?php echo h($i); ?>][id]" value = "<?php echo $sticker['TrnSticker']['id']; ?>" />

                                </td>
                                <td><?php echo h_out($sticker['MstFacilityItem']['internal_code'],'center'); ?></td>
                                <td><?php echo h_out($sticker['MstFacilityItem']['item_name']); ?></td>
                                <td><?php echo h_out($sticker['MstFacilityItem']['item_code']); ?></td>
                                <td class="col10 right">
                                  <label title="<?php echo h($sticker['TrnSticker']['kawatetuzaiko_num']); ?><?php echo h($sticker['MstUnitName']['unit_name']); ?><?php if( isset($sticker['MstItemUnit']['per_unit']) && $sticker['MstItemUnit']['per_unit'] > 1 ): ?>（<?php echo $sticker['MstItemUnit']['per_unit']; ?><?php echo isset($sticker['MstPerUnitName']['unit_name']) ? h($sticker['MstPerUnitName']['unit_name'] ) : ''; ?>）<?php endif; ?>">
                                    <p align ="right"><?php echo h($sticker['TrnSticker']['kawatetuzaiko_num']); ?><?php echo h($sticker['MstUnitName']['unit_name']); ?><?php if( isset($sticker['MstItemUnit']['per_unit']) && $sticker['MstItemUnit']['per_unit'] > 1 ): ?>（<?php echo $sticker['MstItemUnit']['per_unit']; ?><?php echo isset($sticker['MstPerUnitName']['unit_name']) ? h($sticker['MstPerUnitName']['unit_name'] ) : ''; ?>）<?php endif; ?></p>
                                  </label>
                                </td>
                                <td><?php echo h_out($sticker['TrnSticker']['trade_type_name'],'center'); ?></td>
                                <td><?php echo h_out($sticker['TrnSticker']['lot_no']); ?></td>
                                <td><?php echo h_out($sticker['MstFacility']['facility_name']); ?></td>
                                <td><?php echo h_out($sticker['TrnSticker']['facility_sticker_no'],'f_sticker'); ?></td>
                            </tr>
                            <tr class="<?php echo $class; ?>">
                                <td><?php echo h_out($sticker['MstFacilityItem']['medical_code'],'h_medical_code'); ?></td>
                                <td><?php echo h_out($sticker['MstFacilityItem']['standard']); ?></td>
                                <td><?php echo h_out($sticker['MstDealer']['dealer_name']); ?></td>
                                <td><?php echo h_out($this->Common->toCommaStr($sticker['TrnSticker']['transaction_price']),'right'); ?></td>
                                <td></td>
                                <td><?php echo h_out($sticker['TrnSticker']['validated_date'],'center') ?></td>                                </td>
                                <td><?php echo h_out($sticker['MstDepartment']['department_name']); ?></td>
                                <td><?php echo h_out($sticker['TrnSticker']['hospital_sticker_no'],'h_sticker'); ?></td>
                            </tr>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                    <?php elseif(isset($stickers) && count($stickers) === 0): ?>
                        <?php if(isset($this->request->data['isSearch'])){ ?>
                        <tr><td colspan="9" class="center"><span>該当するデータが存在しません</span></td></tr>
                        <?php } ?>
                    <?php endif; ?>
                </table>
            </div>
            <?php if (isset($stickers) && count($stickers) > 0): ?>
            <div align="right" id ="pageDow"></div>
            <div class="ButtonBox">
                <input type="button" class="btn btn9" id="btn_edit" />
                <input type="button" class="btn btn28"id="cmdCenterSticker" />
                <input type="button" class="btn btn16" id="cmdHospitalSticker" />
                <input type="button" class="btn btn12" id="cmdCostSticker" />
            </div>
            <?php endif; ?>
        </div>
<?php unset($stickers); ?>
    </div>
    <input type="hidden" name="data[trnsticker_id_selected]" id="trnsticker_id_selected" value="" />
</form>
