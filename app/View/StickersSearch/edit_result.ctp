<script type="text/javascript">
$(function(){
   $('#inputBarcode').keyup(function(e){
        if(e.which === 13){
            $('#readBarcode').click();
        }
    });
    // シール区分変更
    $("#btn_change").click(function(){
        $("#read_barcode_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/change_trade_type').submit();
    });

    //有効期限、ロット番号を変更
    $("#btn_save").click(function(){
        var comment = 'ロット番号: '+$('#lot_no').val()+'\n有効期限: '+$('#validated_date').val()+'\n上記でロット情報を更新します。\nよろしいですか？';
        //有効期限チェック
        if($('#validated_date').val() != '' && !dateCheck($('#validated_date').val())){
            alert('有効期限の書式が不正です');
            return false;
        }

        //ロット番号チェック
        var lot = $("#lot_no").val();
        var class_separation = $("#class_separation").val();
        if(parseInt(class_separation) >= 3){
            if(lot == "" || lot == null){
                alert("クラス分類が3以上の施設採用品は、ロット番号の入力が必須です");
                return false;
            }
        }

        if(confirm(comment)){
            $("#read_barcode_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/edit_result').submit();
        }
     });

     $('#readBarcode').click(function(){
         $('#inputBarcode').val(FHConvert.ftoh($('#inputBarcode').val()));
         $('#read_barcode_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/read_barcode').submit();
     });

      $('#inputBarcode').val('').focus();
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">シール検索</a></li>
        <li>シール編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>シール編集</span></h2>
<?php if($view_type == 'result'): ?>
<div class="Mes01">以下の内容で更新しました</div><?php else: ?>
<h2 class="HeaddingMid">シール情報を編集してください。</h2>
<?php endif; ?>
<form class="validate_form" id="read_barcode_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/read_barcode" method="post" onsubmit="return false;">
    <input type="hidden" name="data[IsRead]" value="1" />
    <div class="SearchBox">
        <h2 class="HeaddingSmall">採用品情報</h2>
        <div style="width: 98%;">
            <div class="ColumnLeft">
                <table class="FormStyleTable">
                    <tr>
                        <th>商品ID</th><td><div class="lbl" style="width:250px;"><?php echo h_out($sticker[0][0]['internal_code']); ?><br></div></td><td width="20"></td>
                    </tr>
                    <tr>
                        <th>商品名</th><td><div class="lbl" style="width:250px;"><?php echo h_out($sticker[0][0]['item_name']); ?><br></div></td><td width="20"></td>
                    </tr>
                    <tr>
                        <th>規格</th><td><div class="lbl" style="width:250px;"><?php echo h_out($sticker[0][0]['standard']); ?><br></div></td><td width="20"></td>
                    </tr>
                    <tr>
                        <th>製品番号</th><td><div class="lbl" style="width:250px;"><?php echo h_out($sticker[0][0]['item_code']); ?><br></div></td><td width="20"></td>
                    </tr>
                    <tr>
                        <th>販売元</th><td><div class="lbl" style="width:250px;"><?php echo h_out($sticker[0][0]['dealer_name']); ?><br></div></td><td width="20"></td>
                    </tr>
                </table>
            </div>

            <div class="ColumnRight" style="width:50px;">
                <table class="FormStyleTable">
                    <tr>
                        <th>JANコード</th><td><div class="lbl" style="width:140px;"><?php echo h_out($sticker[0][0]['jan_code']); ?><br></div></td><td width="20"></td>
                    </tr>
                    <tr>
                        <th>入数</th><td><div class="lbl" style="width:140px;"><?php echo h_out($sticker[0][0]['per_unit']); ?><br></div></td><td width="20"></td>
                    </tr>
                    <tr>
                        <th>単位</th><td><div class="lbl" style="width:140px;"><?php echo h_out($sticker[0][0]['unit_name']); ?><br></div></td><td width="20"></td>
                    </tr>
                    <tr>
                        <th>換算数</th><td><div class="lbl" style="width:140px;"><?php echo h_out($sticker[0][0]['converted_num']); ?><br></div></td><td width="20"></td>
                    </tr>
                </table>
            </div>
            <input type="hidden" id="class_separation" value="<?php echo $sticker[0][0]['class_separation']?>">
        </div>

        <hr class="Clear" />

        <h2 class="HeaddingSmall">シール情報</h2>
        <div style="width: 98%;">
            <div class="ColumnLeft">
                <table class="FormStyleTable">
                    <tr>
                        <th>在庫施設</th><td><div class="lbl" style="width:250px;"><?php echo h_out($sticker[0][0]['facility_name']); ?><br></div></td><td width="20"></td>
                    </tr>
                    <tr>
                        <th>在庫部署</th><td><div class="lbl" style="width:250px;"><?php echo h_out($sticker[0][0]['department_name']); ?><br></div></td><td width="20"></td>
                    </tr>
                    <tr>
                        <th>資産施設</th><td><div class="lbl" style="width:250px;"><?php echo h_out($sticker[0][0]['asset_facility_name']); ?><br></div></td><td width="20"></td>
                    </tr>
                    <tr>
                        <th>資産部署</th><td><div class="lbl" style="width:250px;"><?php echo h_out($sticker[0][0]['asset_department_name']); ?><br></div></td><td width="20"></td>
                    </tr>
                    <tr>
                        <th>数量</th><td><div class="lbl" style="width:250px;"><?php echo h_out($sticker[0][0]['quantity']); ?><br></div></td><td width="20"></td>
                    </tr>

<?php if($view_type == 'result'): ?>
                    <tr>
                        <th>ロット番号</th><td><div class="lbl"><?php echo h_out($sticker[0][0]['lot_no']); ?><br></div></td>
                    </tr>
                    <tr>
                        <th>有効期限</th><td><div class="lbl"><?php echo h_out($sticker[0][0]['validated_date']); ?><br></div></td>
                    </tr>
<?php else: ?>
                    <tr>
                        <th>バーコード</th>
                        <td colspan="3">
                            <?php echo $this->form->input('barcode',array('class' => 'txt r','id' => 'inputBarcode', 'style'=>'width:250;')); ?>
                            <input type="button" class="btn btn30" id="readBarcode"/>
                        </td>
                    </tr>
                    <tr>
                        <th>ロット番号</th><td><?php echo $this->form->input('TrnSticker.lot_no', array('type'=>'text','id'=>'lot_no','class'=>'txt','maxlength' => '20')); ?></td>
                    </tr>
                    <tr>
                        <th>有効期限</th><td><?php echo $this->form->input('TrnSticker.validated_date', array('type'=>'text','id'=>'validated_date','class'=>'txt date','maxlength' => '10')); ?><br></div></td>
                    </tr>
<?php endif; ?>
                </table>
            </div>
            <div class="ColumnRight">
                <table class="FormStyleTable">
                    <tr>
                        <th>センターシール</th><td><div class="lbl" style="width:140px;"><?php echo h_out($sticker[0][0]['facility_sticker_no']); ?><br></div></td>
                    </tr>
                    <tr>
                        <th>部署シール</th><td><div class="lbl" style="width:140px;"><?php echo h_out($sticker[0][0]['hospital_sticker_no']); ?><br></div></td>
                    </tr>
                    <tr>
                        <th>仕入金額</th><td><div class="lbl" style="width:140px;"><?php echo $this->Common->toCommaStr($sticker[0][0]['original_price']); ?><br></div></td>
                    </tr>
                    <tr>
                        <th>仕入単価</th><td><div class="lbl" style="width:140px;"><?php echo $this->Common->toCommaStr($sticker[0][0]['transaction_price']); ?></div></td>
                    </tr>
                    <tr>
                        <th>状態</th><td><div class="lbl" style="width:140px;"><?php echo ($sticker[0][0]['is_deleted'] === FALSE) ? '通常' : '削除' ?></div></td>
                    </tr>
                </table>
                <?php echo $this->form->hidden("TrnSticker.modified",array("value"=>$sticker[0][0]['modified'])); ?>
            </div>
        </div>
        <hr class="Clear" />
        <h2 class="HeaddingSmall">移動履歴</h2>

        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th class="col20">日付</th>
                    <th class="col10">区分</th>
                    <th class="col40">施設／部署</th>
                    <th class="col15">センターシール</th>
                    <th class="col15">部署シール</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02 table-even">
            <?php foreach( $sticker_records as $r): ?>
                <tr>
                    <td class="col20"><?php echo h_out($r[0]['move_date'] , 'center'); ?></td>
                    <td class="col10"><?php echo h_out($r[0]['status'],'center'); ?></td>
                    <td class="col40"><?php echo h_out($r[0]['facility_name'].'／'.$r[0]['department_name']); ?></td>
                    <td class="col15"><?php echo h_out($r[0]['facility_sticker_no']); ?></td>
                    <td class="col15"><?php echo h_out($r[0]['hospital_sticker_no']); ?></td>
                </tr>
            <?php endforeach; ?>
            </table>
        </div>
    </div>
<div class="ButtonBox">
<?php if($view_type == 'edit'): ?>
  <input type="button" class="btn btn2 fix_btn [p2]" id="btn_save" />
<?php endif; ?>
<?php if($sticker[0][0]['trade_type'] == Configure::read('ClassesType.Temporary') 
      || $sticker[0][0]['trade_type'] == Configure::read('ClassesType.ExShipping') 
      || $sticker[0][0]['trade_type'] == Configure::read('ClassesType.ExNoStock') ){ ?>
  <input type="button" class="btn btn110 fix_btn [p2]" id="btn_change" />
<?php } ?>
</div>
<?php echo $this->form->input('trnsticker_id_selected', array('type'=>'hidden','value'=>$sticker[0][0]['id'])); ?>
</form>
