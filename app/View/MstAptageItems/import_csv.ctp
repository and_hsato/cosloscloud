<script type="text/javascript">
$(function(){
    //登録ボタン押下
    $("#btn_Result").click(function(){
        if($("#result").val() === ''){
            alert('ファイルが選択されてません');
            return false;
        }else{
            if(confirm('CSV登録を行います。\nよろしいですか？')){
                $("#CsvImport").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/import_csv_result');
                $("#CsvImport").attr('method', 'post');
                //登録中メッセージ追加
                $('#msg').text('登録処理中です。しばらくお待ちください。');
                //ボタン無効化
                $('#btn_Result').attr('disabled' , 'disabled');
                $('#download_btn').attr('disabled' , 'disabled');
                //日付項目のチェック削除
                $('#datepicker1').removeClass('validate[optional,custom[date]]');
                $('#datepicker2').removeClass('validate[optional,custom[date]]');
                //送信
                $("#CsvImport").submit();
            }
        }
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>アプテージデータ取込</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>アプテージデータ取込</span></h2>
<form class="validate_form" method="post" action="" enctype="multipart/form-data" accept-charset="utf-8" id="CsvImport">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>CSVファイル</th>
                <td colspan="2">
                    <?php echo $this->form->file('result',array('label'=>false,'div'=>false,'class'=>'r text validate[required]','style'=>'width:250px;','value'=>'')); ?>
                </td>
                <th></th><td></td>
                <td>
                    <?php echo $this->form->input('is_update' , array('type'=>'checkbox' , 'class'=>'chk' , 'hiddenField'=>false , 'checked'=>false )); ?>
                </td>
                <td>更新</td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn29 [p2]" id="btn_Result" />
    </div>
</form>
