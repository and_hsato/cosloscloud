<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //チェックOKだったら、送信する
        validateDetachSubmit($('#AddForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result' );
    }
}

$(document).ready(function(){
    $("#AddForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/unit_names_list">単位名一覧</a></li>
        <li>単位名編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">単位名編集</h2>

<form class="search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckUnitNameCode" id="AddForm">
    <input type="hidden" name="mode" value="add" />
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>単位コード</th>
                <td>
                    <?php echo $this->form->input('MstUnitName.internal_code' , array('type'=>'text' , 'class'=>'txt r validate[required,ajax[ajaxUnitNameCode]]' , 'maxlength'=>20))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>単位名</th>
                <td>
                    <?php echo $this->form->input('MstUnitName.unit_name' , array('type'=>'text' , 'class'=>'txt r validate[required] search_canna' , 'maxlength'=>80))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>表示順</th>
                <td>
                    <?php echo $this->form->input('MstUnitName.order_num' , array('type'=>'text' , 'class'=>'txt r validate[required]' , 'maxlength'=>80))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="submit" class="btn btn2 [p2]" value="" />
    </div>
</form>
