<script>
$(document).ready(function(){
    //検索ボタン押下
    $("#btn_Search").click(function(){
        validateDetachSubmit($("#UnitNameList") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/unit_names_list');
    });

    //新規ボタン押下
    $("#btn_Add").click(function(){
        validateDetachSubmit($('#UnitNameList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add' );
    });

    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $('#UnitNameList').validationEngine('detach');
        $("#UnitNameList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        $("#UnitNameList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/unit_names_list');
    });
  
    //編集ボタン押下
    $("#btn_Mod").click(function(){
        $("#UnitNameList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>単位名一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">単位名一覧</h2>

<form class="validate_form search_form" method="post" action="" accept-charset="utf-8" id="UnitNameList">
    <?php echo $this->form->input('search.is_search' , array('type'=>'hidden'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
            </colgroup>
            <tr>
                <th>単位コード</th>
                <td><?php echo $this->form->input('search.internal_code' , array('type'=>'text' , 'class'=>'txt'))?></td>
                <td></td>
                <td>
                    <?php echo $this->form->input('search.is_deleted',array('type'=>'checkbox', 'hiddenField'=>false)); ?>削除も表示する
                </td>
            </tr>
            <tr>
                <th>単位名</th>
                <td><?php echo $this->form->input('search.unit_name' , array('type'=>'text' , 'class'=>'txt search_canna'))?></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search" />
        <input type="button" class="btn btn11 [p2]" id="btn_Add" />
        <input type="button" class="btn btn5" id="btn_Csv" />
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($UnitName_List))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col />
                    <col />
                    <col width="50"/>
                </colgroup>
                <thead>
                <tr>
                    <th></th>
                    <th>単位コード</th>
                    <th>単位名</th>
                    <th>表示順</th>
                    <th>削除</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($UnitName_List as $UnitName) { ?>
                <tr>
                    <td class="center">
                        <input name="data[MstUnitName][id]" type="radio" class="validate[required]" id="unitname<?php echo $UnitName['MstUnitName']['id'];?>" value="<?php echo $UnitName['MstUnitName']['id']; ?>"/>
                    </td>
                    <td><?php echo h_out($UnitName['MstUnitName']['internal_code']); ?></td>
                    <td><?php echo h_out($UnitName['MstUnitName']['unit_name']); ?></td>
                    <td><?php echo h_out($UnitName['MstUnitName']['order_num']); ?></td>
                    <td><?php echo h_out($UnitName['MstUnitName']['is_deleted'],'center');  ?></td>
                </tr>
                <?php } ?>
                </tbody>
                <?php if(count($UnitName_List) == 0 && isset($this->request->data['search']['is_search'])){ ?>
                <tr><td colspan="4" class="center">該当するデータがありませんでした。</td></tr>
                <?php } ?>
            </table>
        </div>
    </div>
    <?php if(count($UnitName_List)>0){ ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn9" id="btn_Mod" />
    </div>
    <?php } ?>
</form>
