<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/unit_names_list">単位名一覧</a></li>
        <li class="pankuzu">単位名編集</li>
        <li>単位名編集結果</li>
   </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">単位名編集</h2>

<?php if(isset($this->request->data['MstUnitName']['is_deleted'])) {  ?>
<div class="Mes01">データを削除しました</div>
<?php }else{ ?>
<div class="Mes01">以下の内容で設定しました</div>
<?php } ?>
<form method="post" action="" accept-charset="utf-8" >
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>単位コード</th>
                <td>
                    <?php echo $this->form->input('MstUnitName.internal_code' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                 </td>
                <td></td>
            </tr>
            <tr>
                <th>単位名</th>
                <td>
                    <?php echo $this->form->input('MstUnitName.unit_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>表示順</th>
                <td>
                    <?php echo $this->form->input('MstUnitName.order_num' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</form>
