<table>
  <tr>
    <td class="title"><?php echo $title; ?></td>
  </tr>
  <tr>
    <td class="work_no">【出荷番号：<?php echo $data[0][0]['work_no']; ?>】</td>
  </tr>
  <tr>
    <td class="barcode"><barcode code="<?php echo $data[0][0]['work_no'] ?>" type="C39" size="0.55" height="0.9" /></td>
  </tr>
</table>
<br/>
<table>
  <tr>
    <td class="facility">
      <p class="to_name"><?php echo $data[0][0]['facility_formal_name'] . '&nbsp;' . $data[0][0]['department_formal_name']; ?>　御中</p>
      <p>〒<?php echo $data[0][0]['zip']; ?></p>
      <p><?php echo $data[0][0]['address']; ?></p>
      <p>TEL：<?php echo $data[0][0]['tel']; ?></p>
      <p>FAX：<?php echo $data[0][0]['fax']; ?></p>
    </td>
    <td class="supplier">
      <p><?php echo $data[0][0]['work_date']; ?></p>
      <p><?php echo $data[0][0]['supplier_name']; ?></p>
      <p>〒<?php echo $data[0][0]['supplier_zip']; ?></p>
      <p><?php echo $data[0][0]['supplier_address']; ?></p>
      <p>TEL：<?php echo $data[0][0]['supplier_tel']; ?></p>
      <p>FAX：<?php echo $data[0][0]['supplier_fax']; ?></p>
    </td>
  </tr>
</table>
<br/>
<table class="report">
  <tr>
    <th align="right" colspan="7">単位(円)</th>
  </tr>
  <tr>
    <th class="clm1 lineTB" rowspan="2"></th>
    <th class="clm2 lineT">管理区分</th>
    <th class="clm3 lineT">商品名</th>
    <th class="clm4 lineT">商品コード</th>
    <th class="clm5 lineT">数量</th>
    <th class="clm6 lineT">単価</th>
    <th class="clm7 lineT">金額</th>
  </tr>
  <tr>
    <th class="clm2 lineB">商品ID</th>
    <th class="clm3 lineB">規格</th>
    <th class="clm4 lineB">販売元</th>
    <th class="clm5 lineB">ロット番号</th>
    <th class="clm6 lineB">有効期限</th>
    <th class="clm7 lineB">明細備考</th>
  </tr>
</table>