<table class="report">
<?php for ($c = 0; $c < 1; $c++) { // テスト用に複数件のデータを出すためのループ?>
<?php foreach ($data as $i => $d) { ?>
  <tr>
    <td class="clm1 lineB no" rowspan="2"><?php echo ($i + 1); ?></td>
    <td class="clm2 item_name"><?php echo $d[0]['item_name']; ?></td>
    <td class="clm3 quantity_unit_name"><?php echo $d[0]['quantity'] . $d[0]['unit_name']; ?></td>
    <td class="clm4 stocking_price"><?php echo $this->Common->toCommaStr($d[0]['stocking_price']); ?>円</td>
    <td class="clm5 claim_price"><?php echo $this->Common->toCommaStr($d[0]['claim_price']); ?>円</td>
  </tr>
  <tr>
    <td class="clm2 lineB standard"><?php echo $d[0]['standard']; ?></td>
    <td class="clm3 lineB item_code"><?php echo $d[0]['item_code']; ?></td>
    <td class="clm4 lineB dealer_name"><?php echo $d[0]['dealer_name']; ?></td>
    <td class="clm5 lineB internal_code"><?php echo $d[0]['internal_code']; ?></td>
  </tr>
<?php } ?>
<?php } ?>
</table>
