<?php foreach ($data as $i => $d) { ?>
<?php if ($i > 0) echo '<pagebreak />'; ?>
<table class="sticker">
  <tr>
    <td class="facility_department_name" colspan="3"><?php echo $d[0]['facility_name'] . '　' . $d[0]['department_name'] ?></td>
    <td class="item_id" colspan="1"><?php echo $d[0]['internal_code'] ?></td>
  </tr>
  <tr>
    <td class="dealer_name" colspan="3"><?php echo $d[0]['dealer_name'] ?></td>
    <td class="insurance_claim" colspan="1"><?php echo $d[0]['insurance_claim'] ?></td>
  </tr>
  <tr>
    <td class="item_name" colspan="3"><?php echo $d[0]['item_name'] ?></td>
    <td class="biogenous" colspan="1"><?php echo $d[0]['biogenous'] ?></td>
  </tr>
  <tr>
    <td class="standard" colspan="4"><?php echo $d[0]['standard'] ?></td>
  </tr>
  <tr>
    <td class="item_code" colspan="4"><?php echo $d[0]['item_code'] ?></td>
  </tr>
  <tr>
    <td class="barcode" colspan="4"><barcode code="<?php echo $d[0]['hospital_sticker_no'] ?>" type="C39" size="0.72" height="0.8" /></td>
  </tr>
  <tr>
    <td class="hospital_sticker_no" colspan="2"><?php echo $d[0]['hospital_sticker_no'] ?></td>
    <td class="unit_name" colspan="2"><?php echo $d[0]['unit_name'] ?></td>
  </tr>
  <tr>
    <td class="center_shelf_no" colspan="2"><?php echo $d[0]['center_shelf_no'] ?></td>
    <td class="unit_price" colspan="2"><?php echo $this->Common->toCommaStr($d[0]['unit_price']); ?></td>
  </tr>
  <tr>
    <td class="pospital_shelf_no" colspan="2"><?php echo $d[0]['pospital_shelf_no'] ?></td>
    <td class="fixed_count" colspan="2"><?php echo $d[0]['fixed_count'] ?></td>
  </tr>
  <tr>
    <td class="validated_date" colspan="2"><?php echo 'UBD:' . $d[0]['validated_date'] ?>
    </td>
    <td class="lot_no" colspan="2">LOT:<?php echo $d[0]['lot_no'] ?></td>
  </tr>
</table>
<?php } ?>