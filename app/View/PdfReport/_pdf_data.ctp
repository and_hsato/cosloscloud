<?php
if (empty($data)) {
    echo '出力できるデータがないか、またはPDFの出力に失敗しました。';
} else {
    $this->response->type('pdf');
    echo $data;
}
?>
