<style>
.facility_department_name {
    text-align: left;
    width: 70%;
}
.item_id {
    text-align: right;
    width: 30%;
}

.dealer_name {
    text-align: left;
    width: 70%;
}
.insurance_claim {
    text-align: right;
    width: 30%;
}

.item_name {
    text-align: left;
    width: 70%;
}
.biogenous{
    text-align: right;
    width: 30%;
}

.standard {
    text-align: left;
    width: 100%;
}

.item_code {
    text-align: left;
    width: 100%;
}

.barcode {
    text-align: center;
    width: 100%;
}

.hospital_sticker_no {
    text-align: left;
    width: 50%;
}
.unit_name {
    text-align: right;
    width: 50%;
}

.center_shelf_no {
    text-align: left;
    width: 50%;
}
.unit_price {
    text-align: right;
    width: 50%;
}

.pospital_shelf_no {
    text-align: left;
    width: 50%;
}
.fixed_count {
    text-align: right;
    width: 50%;
}

.validated_date {
    text-align: left;
    width: 50%;
}
.lot_no {
    text-align: left;
    width: 50%;
}
</style>
