<style>
.title {
    text-align: center;
    font-weight: bold;
    font-size: xx-large;
}
.work_no {
    text-align: left;
}
.supplier {
    width: 50%;
    text-align: left;
}
.facility {
    width: 50%;
    text-align: right;
}
.to_name {
    font-size: large;
}

.clm1 {
    width: 5%;
}
.clm2 {
    width: 30%;
}
.clm3 {
    width: 25%;
}
.clm4 {
    width: 25%;
}
.clm5 {
    width: 15%;
}

.no {
    vertical-align: middle;
}
.item_name {
    text-align: left;
}
.standard {
    text-align: left;
}
.quantity_unit_name {
    text-align: center;
}
.item_code {
    text-align: center;
}
.stocking_price {
    text-align: center;
}
.dealer_name {
    text-align: center;
}
.claim_price {
    text-align: center;
}
.internal_code {
    text-align: center;
}
</style>