<style>
span.bold {font-weight: bold; }
span.lighter {font-weight: lighter; }
span.bolder {font-weight: bolder; }

/*ヘッダ*/
.title {
    text-align: left;
    font-size: xx-large;
    padding: 0 0 0 70%;
}

/*連絡先*/
.address{
    font-size: 10pt;
    margin-bottom: 32px;
}
.address .left {
    display: table-cell;
    float: left;
    vertical-align: top;
    text-align: left;
    width: 65%;
}
.address .right {
    display: table-cell;
    float: left;
    vertical-align: top;
    text-align: left;
    width: auto;
}

/*料金項目*/
.fee{
    margin-bottom: 32px;
}
.fee TH {
    font-size: 10pt;
    border: solid;
    border-width: 1px;
    width: 14%;
}
.fee TD {
    font-size: 10pt;
    border: solid;
    border-width: 1px;
    text-align: right;
    height: 48px;
}

/*請求内容*/
.summary{
}
.summary TH {
    font-size: 8pt;
    border: solid;
    border-width: 1px;
}
.summary TH.date{
    width: 10%;
}
.summary TH.no{
    width: 7%;
}
.summary TH.name{
    width: 47%;
}
.summary TH.quentity{
    width: 7%;
}
.summary TH.unit{
    width: 7%;
}
.summary TH.unit_price{
    width: 10%;
}
.summary TH.price{
    width: 10%;
}
.summary TD.item_data{
    font-size: 8pt;
    border: solid;
    border-width: 0 1px;
    text-align: right;
    padding:0 4px;
    height: 32px;
}
.summary TD.default{
    font-size: 8pt;
    border: solid;
    border-width: 0 1 1 1px;
    padding: 72 4 4 4px;
    height: 48px;
}
.summary TD.name{
    text-align: left;
}
.summary TD.unit{
    text-align: center;
}

</style>
