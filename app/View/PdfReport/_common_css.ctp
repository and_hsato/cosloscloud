<style>
TABLE {
    <?php /* フォントはmpdfのconfig_fonts.phpで定義されているフォントが利用可能 */ ?>
    <?php /* フォントを追加する場合はmpdfのttfontsフォルダにttfファイルを追加し、config_fonts.phpに定義を追加する。*/ ?>
    font-family: ipamincho;
    border-collapse: collapse;
    width: 100%;
}

TABLE.report TH {
    font-size: 11pt;
    border-top-width: 2px;
    border-bottom-width: 2px;
}
TABLE.report TD {
    font-size: 10pt;
    border-top-width: 1px;
    border-bottom-width: 1px;
}
TABLE.report .lineTB {
    border-top-style: solid;
    border-bottom-style: solid;
}
TABLE.report .lineT {
    border-top-style: solid;
}
TABLE.report .lineB {
    border-bottom-style: solid;
}
TABLE.report .total {
    font-weight: bold;
    font-size: x-large;
    text-decoration: underline;
    text-align: right;
}

TABLE.sticker {
    border: none;
    font-size: 9pt;
}
TABLE.sticker TH, TD {
    border: none;
}

</style>
