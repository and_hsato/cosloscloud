﻿<style>
body {
    font-family:メイリオ;
    font-size:12px;
}
table {
    border-collapse:collapse;
}
th {
    text-align:center;
}
.main {
    margin:0mm;
}
  
h1 {
    margin:0px;
    margin-bottom:1mm;
    font-weight:bold;
    text-align:center;
}

.r {text-align:right;}
.c {text-align:center;}
.l {text-align:left;}

tr.bbtb th {
    border-top:0.5mm solid #000000;
}

tr.btbb th {
    border-bottom:0.5mm solid #000000;
}

.atena {
    width:100%;
}
        
.atena .customer h2{
    margin: 0;
}
        
.atena .company {
    text-align:right;
}

.atena .company h2 {
}

.company .date {
    font-weight: bold;
}

.outline {
    border: 0.5mm solid #000000;
    margin: 1px;
    font-weight: bold;
}

.outline th {
    font-size:3mm;
    background:#eeeeee;
    border:1px solid #000000;
}

.outline td{
    border:1px solid #000000;
    text-align:center;
    font-size:3mm;
}

.meisai {
    width:100%;
    border-bottom: 1px solid #000;
}

.meisai th {
    font-size:3mm;
    padding:0.5mm;
    background:#eeeeee;
}

.meisai td{
    padding:0.5mm;
    font-size:12px;
    font-size:3mm;
}

p {
    margin:0px;
    padding:0px;
}

.hidariyose {
    text-align:left;
}

.colorLine0 td {
    background:#eeeeee;
}

.order-no{
    font-weight: bold;
}
.box{
    position: absolute;
    right: 0px;
    top: 4mm;
}

.box li{
    border: 1px solid #444;
    width: 20mm;
    height: 20mm;
    float: left;
    list-style: none;
}

hr {
    page-break-after:always;
    height:0;
    border:0 none #ffffff;
    color:#ffffff;
}
@media print {
    hr {
    }
}
@media screen {
    hr {
        border-top:2px dashed #999999;
        border-bottom:2px dashed #999999;
    }
}


</style>
<div class="page">
<h1>発 注 書</h1>
<p class="order-no">【発注番号：<?php echo $data[0][0]['work_no']; ?>】</p>
<table class="atena">
    <tr>
        <td class="customer">
            <p>
                〒<?php echo $data[0][0]['supplier_zip']; ?><br />
                <?php echo $data[0][0]['supplier_address']; ?></p>
                <h2><?php echo $data[0][0]['supplier_name']; ?> 御中</h2>
                <table>
                <tr><td>TEL</td><td><?php echo $data[0][0]['supplier_tel']; ?></td></tr>
                <tr><td>FAX</td><td><?php echo $data[0][0]['supplier_fax']; ?></td></tr>
                </table>
            </td>
            <td class="company">
            <p class="date"><?php echo $data[0][0]['work_date']; ?></p>
            <h2><?php echo $data[0][0]['facility_formal_name'] .' '. $data[0][0]['department_formal_name'] ?></h2>
            〒<?php echo $data[0][0]['zip']; ?> <?php echo $data[0][0]['address']; ?><br />
            
            TEL <?php echo $data[0][0]['tel']; ?><br />
            FAX <?php echo $data[0][0]['fax']; ?><br />
        </td>
    </tr>
</table>

<p class="r">単位：円</p>

<table class="meisai">
    <tr class="bbtb">
        <th width="5%"></th>
        <th width="20%">商品名</th>
        <th width="25%">数量</th>
        <th width="35%">単価</th>
        <th width="20%">金額</th>
    </tr>
    <tr class="btbb">
        <th></th>
        <th>規格</th>
        <th>商品コード</th>
        <th>販売元</th>
        <th>商品ID</th>
    </tr>
    <?php $total_price = 0; $i = 1 ; foreach($data as $d){ ?>
    <tr class="colorLine<?php echo ($i % 2); ?>">
        <td class="c" rowspan="2"><?php echo $i; ?></td>
        <td class="c"><?php echo $d[0]['item_name']; ?></td>
        <td class="c"><?php echo $d[0]['quantity'] . $d[0]['unit_name']; ?></td>
        <td class="c"><?php echo $d[0]['stocking_price']; ?>円</td>
        <td class="c"><?php echo $d[0]['claim_price']; ?>円</td>
     </tr>
    <tr class="colorLine<?php echo ($i % 2); ?>">
        <td class="c"><?php echo $d[0]['standard']?></td>
        <td class="c"><?php echo $d[0]['item_code']; ?></td>
        <td class="c"><?php echo $d[0]['dealer_name']; ?></td>
        <td class="c"><?php echo $d[0]['internal_code']; ?></td>
     </tr>
     <?php $total_price = $total_price + $d[0]['claim_price']; $i++; } ?>
</table>
<table width="100%">
    <tr>
        <td width='60%'></td>
        <td class="r" width='40%'>
            <table class="">
                <tr>
                    <td>合計</td>
                    <td><?php echo $total_price; ?>円</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</div>
