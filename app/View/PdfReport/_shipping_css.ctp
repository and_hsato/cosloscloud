<style>
.title {
    text-align: center;
    font-weight: bold;
    font-size: xx-large;
}
.work_no {
    text-align: left;
}
.supplier {
    width: 50%;
    text-align: right;
}
.facility {
    width: 50%;
    text-align: left;
}
.to_name {
    font-size: large;
}

.clm1 {
    width: 5%;
}
.clm2 {
    width: 15%;
}
.clm3 {
    width: 25%;
}
.clm4 {
    width: 20%;
}
.clm5 {
    width: 15%;
}
.clm6 {
    width: 10%;
}
.clm7 {
    width: 10%;
}

.no {
    vertical-align: middle;
}

.type_name {
    text-align: center;
}
.internal_code {
    text-align: center;
}

.item_name {
    text-align: left;
}
.standard {
    text-align: left;
}

.item_code {
    text-align: left;
}
.dealer_name {
    text-align: left;
}

.quantity_unit_name {
    text-align: left;
}
.lot_no {
    text-align: left;
}

.stocking_price {
    text-align: right;
}
.validated_date {
    text-align: right;
}

.claim_price {
    text-align: right;
}
.recital {
    text-align: right;
}
</style>