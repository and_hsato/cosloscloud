<table class="summary">
  <tr>
    <th class="date">伝票日付</th>
    <th class="no">伝票No.</th>
    <th class="name">品名</th>
    <th class="quentity">数量</th>
    <th class="unit">単位</th>
    <th class="unit_price">単価</th>
    <th class="price">金額</th>
  </tr>
  <tr>
    <td class="item_data date"><?php echo $slip_date ?></td>
    <td class="item_data no"><?php echo $slip_no ?></td>
    <td class="item_data name">診療材料費</td>
    <td class="item_data quentity">1</td>
    <td class="item_data unit">式</td>
    <td class="item_data unit_price"> <?php echo substr($this->Common->toCommaStr($sum_purchase_price), 0 , strpos($this->Common->toCommaStr($sum_purchase_price), '.')) ?> </td>
    <td class="item_data price"> <?php echo substr($this->Common->toCommaStr($sum_purchase_price), 0 , strpos($this->Common->toCommaStr($sum_purchase_price), '.')) ?> </td>
  </tr>
  <tr>
    <td class="item_data date"></td>
    <td class="item_data no"></td>
    <td class="item_data name">消費税等</td>
    <td class="item_data quentity"></td>
    <td class="item_data unit"></td>
    <td class="item_data unit_price"></td>
    <td class="item_data price"> <?php echo substr($this->Common->toCommaStr($data[0][0]['total_tax']), 0 , strpos($this->Common->toCommaStr($data[0][0]['total_tax']), '.')) ?> </td>
  </tr>
  <tr>
    <td class="item_data date"></td>
    <td class="item_data no"></td>
    <td class="item_data name">【合計】</td>
    <td class="item_data quentity"></td>
    <td class="item_data unit"></td>
    <td class="item_data unit_price"></td>
    <td class="item_data price"> <?php echo substr($this->Common->toCommaStr($total_price), 0 , strpos($this->Common->toCommaStr($total_price), '.')) ?> </td>
  </tr>
  <tr>
    <td class="default"></td>
    <td class="default"></td>
    <td class="default name">
      <div>お支払期日　<?php echo $deadline_date ?></div>
      <div><br></div>
      <div>（支払日が休日の場合は</div>
      <div>前日までにお振込みくださいます様</div>
      <div>宜しくお願い致します）</div>
    </td>
    <td class="default"></td>
    <td class="default"></td>
    <td class="default"></td>
    <td class="default"></td>
  </tr>
</table>
