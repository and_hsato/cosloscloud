<table>
  <tr>
    <td class="title"><?php echo $title; ?></td>
  </tr>
  <tr>
    <td class="work_no">【発注番号：<?php echo $data[0][0]['work_no']; ?>】</td>
  </tr>
  <tr>
    <td class="barcode"><barcode code="<?php echo $data[0][0]['work_no'] ?>" type="C39" size="0.55" height="0.9" /></td>
  </tr>
</table>
<br/>
<table>
  <tr>
    <td class="supplier">
      <p class="to_name"><?php echo $data[0][0]['supplier_name']; ?>　御中</p>
      <p>〒<?php echo $data[0][0]['supplier_zip']; ?></p>
      <p><?php echo $data[0][0]['supplier_address']; ?></p>
      <p>TEL：<?php echo $data[0][0]['supplier_tel']; ?></p>
      <p>FAX：<?php echo $data[0][0]['supplier_fax']; ?></p>
    </td>
    <td class="facility">
      <p><?php echo $data[0][0]['work_date']; ?></p>
      <p><?php echo $data[0][0]['facility_formal_name'] . '&nbsp;' . $data[0][0]['department_formal_name']; ?></p>
      <p>〒<?php echo $data[0][0]['zip']; ?></p>
      <p><?php echo $data[0][0]['address']; ?></p>
      <p>TEL：<?php echo $data[0][0]['tel']; ?></p>
      <p>FAX：<?php echo $data[0][0]['fax']; ?></p>
    </td>
  </tr>
</table>
<br/>
<table class="report">
  <tr>
    <th align="right" colspan="5">単位(円)</th>
  </tr>
  <tr>
    <th class="clm1 lineTB" rowspan="2"></th>
    <th class="clm2 lineT">商品名</th>
    <th class="clm3 lineT">数量</th>
    <th class="clm4 lineT">売上単価</th>
    <th class="clm5 lineT">金額</th>
  </tr>
  <tr>
    <th class="clm2 lineB">規格</th>
    <th class="clm3 lineB">商品コード</th>
    <th class="clm4 lineB">販売元</th>
    <th class="clm5 lineB">商品ID</th>
  </tr>
</table>