<table class="fee">
  <tr class="header">
    <th>前回御請求金額</th>
    <th>御入金額</th>
    <th>調整額</th>
    <th>繰越金額</th>
    <th>今回御買上額</th>
    <th>今回御請求額</th>
    <th>請求残金額</th>
  </tr>
  <tr class="data">
    <td> <?php echo substr($this->Common->toCommaStr($last_time_purchase_in_tax), 0 , strpos($this->Common->toCommaStr($last_time_purchase_in_tax), '.')) ?> </td>
    <td> <?php echo substr($this->Common->toCommaStr($last_time_payment_track_record_in_tax), 0 , strpos($this->Common->toCommaStr($last_time_payment_track_record_in_tax), '.')) ?> </td>
    <td>0</td>
    <td> <?php echo substr($this->Common->toCommaStr($data[0][0]['last_month_carry_over_in_tax']), 0 , strpos($this->Common->toCommaStr($data[0][0]['last_month_carry_over_in_tax']), '.')) ?> </td>
    <td> <?php echo substr($this->Common->toCommaStr($data[0][0]['purchase_in_tax']), 0              , strpos($this->Common->toCommaStr($data[0][0]['purchase_in_tax']), '.')) ?> </td>
    <td> <?php echo substr($this->Common->toCommaStr($data[0][0]['payment_track_record_in_tax']), 0  , strpos($this->Common->toCommaStr($data[0][0]['payment_track_record_in_tax']), '.')) ?> </td>
    <td> <?php echo substr($this->Common->toCommaStr($data[0][0]['carry_over_in_tax']), 0            , strpos($this->Common->toCommaStr($data[0][0]['carry_over_in_tax']), '.')) ?> </td>
  </tr>
</table>
