﻿<style>
body {
    font-size:3.5mm;
    font-family:sans-serif;
}
table {
    border-collapse:collapse;
}
th {
    text-align:center;
}
.main {
    margin:0mm;
}
/* 請求書 */
h1 {
    margin:0px;
    margin-bottom:1mm;
    font-size:5mm;
    font-weight:bold;
    text-align:center;
    text-decoration:underline;
}
/* 社名等 */
h2 {
    font-size:150%;
    margin-bottom:10px;
}

#uriage {
    margin:0mm;
    height:80mm;
}
#nohin {
    margin:5mm 0mm 5mm 0mm;
    height:99mm;
}
#juryo {
    margin:0mm;
}

.r {text-align:right;}
.c {text-align:center;}
.l {text-align:left;}

tr.bbtb td, tr.bbtb th {
    border-top:0.5mm solid #000000;
    border-bottom:0.5mm solid #000000;
}

.atena {
    width:100%;
    /*border:0mm none;*/
}
.atena .customer {
        }
        
.atena .customer h2{
    margin: 0;
        }
        
.atena .company {
    text-align:right;
}

.atena .company h2 {
        margin: 0 0 2mm;
}

.company .date {
        font-weight: bold;
}

.outline {
    border: 0.5mm solid #000000;
    margin: 0;
    width: 30%;
    float: right;
    font-weight: bold;
}
.outline th {
    font-size:3mm;
    background:#eeeeee;
    border:1px solid #000000;
}
.outline td{
    border:1px solid #000000;
    text-align:center;
}

.meisai {
    width:100%;
    margin:1mm 0mm 1mm auto;
    border-bottom: 2px solid #000;
    margin: 0 0 5mm 0;
}
.meisai th {
    font-size:3mm;
    padding:0.5mm;
    background:#eeeeee;
}
.meisai td{
    padding:0.5mm;
}
.fixtext {
    width:100%;
    border:0mm none;
}
.fixtext .hidari {

}
.fixtext .migi {
    text-align:right;
}

td.juryoin {
    text-align:center;
    vertical-align:top;
}
td.juryoin p{
    background:#eeeeee;
    border-bottom:1px solid #000000;
}

p {
    margin:0px;
    padding:0px;
}
.hidariyose {
    text-align:left;
}

.migiyose {
    text-align:right;
}


.colorLine0 td {
background:#eeeeee;
}
.colorLine1 td {
}

.order-no{
    font-weight: bold;
    margin: 0 0 3mm;
    }
.box{
    position: absolute;
    right: 0px;
    top: 4mm;
    margin: 0 0 5mm 0;
    }

.box li{
        border: 1px solid #444;
    width: 20mm;
    height: 20mm;
    float: left;
    margin: 0 2mm 0 0;
    list-style: none;
    }

hr {
    page-break-after:always;
    height:0;
    border:0 none #ffffff;
    color:#ffffff;
}
@media print {
    hr {
    /*    display:none;*/
    }
}
@media screen {
    hr {
        border-top:2px dashed #999999;
        border-bottom:2px dashed #999999;
        margin:30px;
    }
}


</style>
<div class="page">
<h1>発 注 書</h1>
<p class="order-no">【発注番号：<?php echo $data[0][0]['work_no']; ?>】</p>
<table class="atena">
    <tr>
        <td class="customer">
            <p>
                〒<?php echo $data[0][0]['supplier_zip']; ?><br />
                <?php echo $data[0][0]['supplier_address']; ?></p>
                <h2><?php echo $data[0][0]['supplier_name']; ?> 御中</h2>
                <p>TEL <?php echo $data[0][0]['supplier_tel']; ?><br />
                FAX <?php echo $data[0][0]['supplier_fax']; ?><br />
            </p>
            <br />
            </td>
            <td class="company">
            <p class="date"><?php echo $data[0][0]['work_date']; ?></p>
            <h2><?php echo $data[0][0]['facility_formal_name'] .' '. $data[0][0]['department_formal_name'] ?></h2>
            〒<?php echo $data[0][0]['zip']; ?> <?php echo $data[0][0]['address']; ?><br />
            TEL <?php echo $data[0][0]['tel']; ?><br />FAX <?php echo $data[0][0]['fax']; ?><br />
        </td>
    </tr>
</table>

<p class="migiyose">単位：円</p>

<table class="meisai">
    <colgroup>
        <col style="width: 5%; text-align:center;" />
        <col style="width: 20%; text-align:center;" />
        <col style="width: 25%; text-align:right;" />
        <col style="width: 35%; text-align:left;" />
        <col style="width: 20%; text-align:right;" />
    </colgroup>

    <tr class="bbtb">
        <th></th>
        <th>商品名</th>
        <th>数量</th>
        <th>単価</th>
        <th>金額</th>
    </tr>
       <tr class="bbtb">
        <th></th>
        <th>規格</th>
        <th>商品コード</th>
        <th>販売元</th>
        <th>商品ID</th>
    </tr>
