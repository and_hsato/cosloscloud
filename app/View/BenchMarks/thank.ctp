<div id="content-wrapper">

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">ＴＯＰ</a></li>
        <li>メッカル医療材料ベンチマークサービス</li>
    </ul>
</div>

<div id="meccul-header">
<h2><span>meccul</span>メッカル医療材料ベンチマークサービス</h2>
</div><!--#meccul-header-->

<div id="wrapper-in">
<p style="font-weight:bold;font-size:24px;">メッカル医療材料ベンチマークのお申し込みを受け付けました。</p>
<p>メッカル医療材料ベンチマークのサービスご利用にお申し込みくださいましてありがとうございます。<br>
お申し込み時にご入力頂いた情報を確認後、一両日中にマスタコンシェルジュよりご連絡させて頂きますのでお待ち頂ければ幸いです。</p>
</div><!--#wrapper-in-->
</div><!--#content-wrapper-->
