<script type="text/javascript">
$(function(){
    $("#btn_select").click(function(){
        $("#form_add").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add').submit();
    });
    $("#btn_seal").click(function(){
        $("#form_add").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/read_sticker').submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>直納品受領登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>直納品受領登録</span></h2>

<form class="validate_form input_form" id="form_add" method="post" action="">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo $this->form->input('d2customers.supplierName' , array('type'=>'hidden' , 'id'=>'supplierName')); ?>
                    <?php echo $this->form->input('d2customers.supplierText',array('id' => 'supplierText', 'label' => false, 'div' => false, 'class' => 'r validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('d2customers.supplierCode',array('options'=>$supplier_list,'id' => 'supplierCode', 'class' => 'r validate[required]','empty'=>true)); ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('d2customers.classId',array('options'=>$class_list,'id' => 'classId', 'class' => 'txt','empty'=>true)); ?>
                    <?php echo $this->form->input('d2customers.class_name',array('type'=>'hidden', 'id' => 'className')); ?>
                </td>
            </tr>
            <tr>
                <th>消費日時</th>
                <td><?php echo $this->form->input('d2customers.work_date' , array('type'=>'text' , 'class'=>'r validate[required,custom[date],funcCall2[date2]] date' , 'id'=>'datepicker1') );?></td>
                <td></td>
                <th>備考日付</th>
                <td><?php echo $this->form->input('d2customers.recital_date' , array('type'=>'text' , 'class'=>'validate[optional,custom[date],funcCall2[date2]] date' , 'id'=>'datepicker2') );?></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('d2customers.hospitalName' , array('type'=>'hidden' , 'id'=>'facilityName')); ?>
                    <?php echo $this->form->input('d2customers.hospitalText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'r validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('d2customers.hospitalCode',array('options'=>$hospital_list,'id' => 'facilityCode', 'class' => 'txt r', 'empty' => true)); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('d2customers.recital1' , array('type'=>'text' ,'class'=>'txt' , 'maxlength'=>'200'))?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('d2customers.departmentName' , array('type'=>'hidden' , 'id'=>'departmentName')); ?>
                    <?php echo $this->form->input('d2customers.departmentText', array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'r validate[required]', 'style' => 'width: 60px;')); ?>
                    <?php echo $this->form->input('d2customers.departmentCode',array('options'=>array(),'id' => 'departmentCode', 'class' => 'txt validate[required] r', 'empty' => true)); ?>
                </td>
                <td></td>
                <th>備考２</th>
                <td><?php echo $this->form->input('d2customers.recital2' , array('type'=>'text' ,'class'=>'txt' , 'maxlength'=>'200'))?></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn58 submit [p2]" id="btn_select"/>
            <input type="button" class="btn btn46 submit [p2]" id="btn_seal"/>
        </p>
    </div>
</form>
