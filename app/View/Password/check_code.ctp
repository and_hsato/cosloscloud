<script>
$(document).ready(function(){
    $('body').addClass('loginBody');
    $('#content').css({"background-color":"#F9F9F9"});
    $('#MstUserCheckCode').focus();
    
    $('#btn_next').click(function(e){
        $('#MstUserCheckCodeForm').submit();
    });
});
</script>
<header>
<h1 class="center login-logo"><img src="<?php echo $this->webroot?>img/login/login_logo.png" alt="コンパクトSPDシサービス「CoslosCloud」コスロス・クラウド" width="360px" height="100px" /></h1>
</header>

<div id="login-page" class="center" style="display:block;">
  <?php echo $this->Form->create(null); ?>
  <div class="center">
    <br/>
    <div style="font-size: 14px;">メールアドレス宛に確認コードが送信されました。</div>
    <div style="font-size: 14px;">メールに記載されている確認コードを入力後、</div>
    <div style="font-size: 14px;">次へボタンを押してください。</div>
  </div>
  <div class="center">
    <label>確認コード</label>
    <div><?php echo $this->Form->input('MstUser.check_code', array('type'=>'text', 'maxlength' => 6, 'class' => 'user r validate[required]')); ?></div>
  </div>
  <div class="center">
    <?php echo $this->Form->end(array('type' => 'button', 'label' => '次へ', 'class' => 'common-button', 'id' => 'btn_next')); ?>
  </div>
</div>
