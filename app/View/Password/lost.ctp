<script>
  $(document).ready(function(){
      $('body').addClass('loginBody');
      $('#content').css({"background-color":"#F9F9F9"});
   $('#MstUserLoginId').focus();
  
    $('#btn_next').click(function(e){
        $('#MstUserLostForm').submit();
    });
  
});
</script>
<header>
<h1 class="center login-logo"><img src="<?php echo $this->webroot?>img/login/login_logo.png" alt="コンパクトSPDシサービス「CoslosCloud」コスロス・クラウド" width="360px" height="100px" /></h1>
</header>

<div id="login-page" class="center" style="display:block;">
  <?php echo $this->Form->create(null); ?>
  <div class="center">
    <br/>
    <div style="font-size: 14px;">ご登録済みのメールアドレス(ログインID)を入力後、</div>
    <div style="font-size: 14px;">次へボタンを押してください。</div>
  </div>
  <div class="center">
    <label>メールアドレス</label>
    <div><?php echo $this->Form->input('MstUser.login_id', array('type'=>'text', 'maxlength' => 40, 'class' => 'user r validate[required]')); ?></div>
  </div>
  <div class="center">
    <?php echo $this->Form->end(array('type' => 'button', 'label' => '次へ', 'class' => 'common-button', 'id' => 'btn_next')); ?>
  </div>
</div>