<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //チェックOKだったら、送信する
        validateDetachSubmit($('#MstUserSubmitCheckUserIdForm'), '<?php echo $this->webroot; ?><?php echo $this->name; ?>/changePassword' );
    }
}

$(document).ready(function(){
    $('body').addClass('loginBody');
    $('#content').css({"background-color":"#F9F9F9"});
    $("#MstUserSubmitCheckUserIdForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
    
    $('#btn_next').click(function(e){
        $('#MstUserSubmitCheckUserIdForm').submit();
    });
});
</script>

<header>
<h1 class="center login-logo"><img src="<?php echo $this->webroot?>img/login/login_logo.png" alt="コンパクトSPDシサービス「CoslosCloud」コスロス・クラウド" width="360px" height="100px" /></h1>
</header>

<div id="login-page" class="center" style="display:block;">
  <?php echo $this->Form->create(null, array('action' => 'submitCheckUserId')); ?>
  <div class="center">
    <br/>
    <div style="font-size: 14px;">新しいパスワードを入力後、</div>
    <div style="font-size: 14px;">次へボタンを押してください。</div>
  </div>
  <div class="center">
    <label>新パスワード</label>
    <div><?php echo $this->form->input('MstUser.password', array('type'=>'password', 'maxlength'=>'20', 'class' => 'user r validate[required]')) ?></div>
  </div>
  <div class="center">
    <label>パスワード(確認用)</label>
    <div><?php echo $this->form->input('MstUser.password2', array('type'=>'password', 'maxlength'=>'20', 'class' => 'user r validate[required,equals[MstUserPassword]]')) ?></div>
  </div>
  <div class="center">
    <?php echo $this->Form->end(array('type' => 'button', 'label' => '次へ', 'class' => 'common-button', 'id' => 'btn_next')); ?>
  </div>
</div>
