<script>
$(document).ready(function(){
    $('body').addClass('loginBody');
    $('#content').css({"background-color":"#F9F9F9"});
});
</script>

<header>
<h1 class="center login-logo"><img src="<?php echo $this->webroot?>img/login/login_logo.png" alt="コンパクトSPDシサービス「CoslosCloud」コスロス・クラウド" width="360px" height="100px" /></h1>
</header>

<div id="login-page" class="center" style="display:block;">
  <div class="center">
    <br/>
    <div style="font-size: 14px;">パスワードを変更しました。</div>
  </div>
  <div class="center">
    <div><a href="<?php echo $this->webroot; ?>/login">ログイン画面へ</a></div>
  </div>
</div>
