<script type="text/javascript">
$(document).ready(function(){
    $('#exportCSV').click(function(){
        if($('#analyzeSelect').val() == ''){
            alert('出力を行う明細を選択してください');
            return false;
        }else if(!dateCheck($('#dateFrom').val()) && $('#dateFrom').val() != "" ){
            alert('日付開始が不正です。');
            return false;
        }else if(!dateCheck($('#dateTo').val()) && $('#dateTo').val() != ""){
            alert('日付終了が不正です。');
            return false;
        }else if(!Chk_DayNum()){
            return false;
        }else{
            $('#msg').text('CSV出力中です。しばらくお待ちください。');
            $('#analyzeForm').attr('action', '<?php echo $this->webroot; ?>analyze/output_csv').submit();
            $('#exportCSV').attr('disabled' , 'disabled');
        }
    });
    $('#dateFrom').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
    $('#dateTo').datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});

    //分析名
    $('#analyzeSelect').change(function(){
        $('#AnalyzeDonorId').val('');
        $('#facility_select').val('');
        $('#department_select').val('');
        $('#department_select').children().remove();
        $('#donor_select').val('');
        $('#dateFrom').val('');
        $('#dateTo').val('');
        $('#dayNum').val('');
        $('#exportCSV').attr('disabled','true');
        $('.conditions').find('tr').hide();
        if($(this).val() != '' && $(this).val() != null){
            $.ajax({
                type: 'POST',
                cache: false,
                url: "<?php echo $this->webroot; ?>analyze/getAnalyzeMethod/" + $(this).val(),
                success: function(data, dataType){
                    var obj = $.parseJSON(data);
                    var target = $('.conditions');
                    if(obj.MstAnalyze.donor_selectable == true){
                        target.find('tr.donors').show();
                    }
                    if(obj.MstAnalyze.facility_selectable == true){
                        target.find('tr.facilities').show();
                    }
                    if(obj.MstAnalyze.department_selectable == true){
                        target.find('tr.departments').show();
                    }
                    if(obj.MstAnalyze.dates_selectable == true){
                        target.find('tr.dates').show();
                    }
                    if(obj.MstAnalyze.daynumber_selectable == true){
                        target.find('tr.day_number').show();
                    }
                    $('#exportCSV').removeAttr('disabled');
                    $('#msg').text('');
                },
                error: function(){
                    alert('分析情報の取得に失敗しました');
                }
            });
        }
    });

    $('#facility_select').change(function(){
        var facility = $('#facility_select').val();
        $('#department_select').val('');
        if(facility !== '' && facility !== null){
            $.ajax({
                type: 'POST',
                cache: false,
                url: "<?php echo $this->webroot; ?>analyze/getDepartments/" + $(this).val(),
                success: function(data, dataType){
                    $('#department_select').children().remove();
                    var obj = $.parseJSON(data);
                    $.each(obj, function(idx, o){
                        if(idx === 0 && obj.length > 1){
                            $('<option value=""></option>').appendTo('#department_select');
                        }
                        $('<option></option>').text(o.MstDepartment.department_name).attr('value', o.MstDepartment.id).appendTo('#department_select');
                    });
                    //施設が変わったらボタンの復活 & メッセージの削除
                    $('#exportCSV').removeAttr('disabled');
                    $('#msg').text('');
                },
                error: function(){
                    alert('部署データの取得に失敗しました');
                }
            });
        }else{
            $('#department_select').children().remove();
        }
    });

    //日付が変わったらボタンの復活 & メッセージの削除
    $('#dateFrom').change(function(){
        $('#exportCSV').removeAttr('disabled');
        $('#msg').text('');
    });
  
    //日付が変わったらボタンの復活 & メッセージの削除
    $('#dateTo').change(function(){
        $('#exportCSV').removeAttr('disabled');
        $('#msg').text('');
    });

    //部署が変わったらボタンの復活 & メッセージの削除
    $('#department_select').change(function(){
        $('#exportCSV').removeAttr('disabled');
        $('#msg').text('');
    });

    //施主が変わったらボタンの復活 & メッセージの削除
    $('#donor_select').change(function(){
        $('#exportCSV').removeAttr('disabled');
        $('#msg').text('');
    });

    //日数が変わったらボタンの復活 & メッセージの削除
    $('#dayNum').change(function(){
        $('#exportCSV').removeAttr('disabled');
        $('#msg').text('');
    });

  

    //初期状態
    $('.conditions').find('tr').hide();
    $('#exportCSV').attr('disabled','true');
});
/**
 * 日数の入力値チェック
 */
function Chk_DayNum(){
    var numcheck = true;
    var num = $('#dayNum').val();
    if(num !=null && num!=''){
      if(num.match(/[^0-9]/g)){
           alert('日数が不正です');
           $('#dayNum').val('');
           numcheck=false;
       }
  }
  return numcheck;
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>分析データ出力</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>分析データ出力</span></h2>
<h2 class="HeaddingMid">出力を行う明細と条件を選択してください</h2>

<div id="msg" class="err" style="font-size:20px"></div>

<form id="analyzeForm" method="POST" action="<?php echo $this->webroot; ?>analyze/export">
<input type="hidden" name="data[Analyze][f]" value="<?php echo $this->Session->read('Auth.facility_id_selected'); ?>">
<input type="hidden" name="data[Analyze][u]" value="<?php echo $this->Session->read('Auth.MstUser.id'); ?>">
<hr class="Clear" />
<div class="SearchBox">
    <table class="FormStyleTable">
        <tr>
            <th>分析名</th>
            <td><?php echo $this->form->input('MstAnalyze.id', array('options'=>$items, 'empty'=>true, 'class'=>'txt','id'=>'analyzeSelect','style'=>'width:400px')); ?></td>
        </tr>
        <tr>
            <th>条件</th>
            <td>
                <div style="width: 400px;height: 180px;border:1px solid #ccc;">
                    <table class="conditions">
                        <tr class="donors">
                            <th>施主</th>
                            <td><?php echo $this->form->input('Analyze.donor_id', array('options'=>$donors, 'empty'=>true ,'class'=>'txt' , 'id'=>'donor_select')); ?></td>
                        </tr>
                        <tr class="facilities">
                            <th>施設</th>
                            <td><?php echo $this->form->input('Analyze.facility_id', array('options'=>$facilities, 'empty'=>true , 'class'=>'txt', 'id'=>'facility_select')); ?></td>
                        </tr>
                        <tr class="departments">
                            <th>部署</th>
                            <td><select id="department_select" name="data[Analyze][department_id]" class="txt"></select></td>
                        </tr>
                        <tr class="dates">
                            <th>日付開始</th>
                            <td><?php echo $this->form->input('Analyze.day_from', array('type'=>'text','class'=>'date','id'=>'dateFrom')); ?></td>
                        </tr>
                        <tr class="dates">
                            <th>日付終了</th>
                            <td><?php echo $this->form->input('Analyze.day_to', array('type'=>'text' , 'class'=>'date','id'=>'dateTo')); ?></td>
                        </tr>
                        <tr class="day_number">
                            <th>日数</th>
                            <td><?php echo $this->form->input('Analyze.day_num', array('class'=>'txt','id'=>'dayNum')); ?></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
</form>
<br />
<div class="ButtonBox">
    <p class="center">
        <input type="button" class="btn btn45" id="exportCSV" />
    </p>
</div>
