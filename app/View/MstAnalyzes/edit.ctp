<script type="text/javascript">

    $(document).ready(function(){
        
        $("#btn_Commit").click(function(){
            
            // タイトル必須チェック
            if ( $("#MstAnalyzeTitle").val() == "" ) {
                alert("タイトルは必須です");
                $("#MstAnalyzeTitle").focus();
                return false;
            }
            
            // SQL文必須チェック
            if ( $("#MstAnalyzeSqlData").val() == "" ) {
                alert("SQL文は必須です");
                $("#MstAnalyzeSqlData").focus();
                return false;
            }
            
            // 出力対象者必須チェック
            var cnt = 0;
            var objCheckRow = document.getElementsByName( "data[MstAnalyzeRole][mst_role_id][]" );
            for ( var i=0 ; i<objCheckRow.length ; i++ ) {
                var objCheck = objCheckRow[i];
                if ( objCheck.checked ) {
                    cnt++;
                    break;
                }
            }
            if ( cnt == 0 ) {
                alert("出力対象者は必ず1件以上選択してください");
                return false;
            }

            $("#analyze_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result');
            $("#analyze_form").submit();
            
        });
        
    });

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/analyzes_list">分析一覧</a></li>
        <li>分析編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">分析編集</h2>

<form method="post" id="analyze_form">
    <input type="hidden" name="mode" value="<?php echo $mode; ?>">
<?php
    $AnalyzeData = $AnalyzesData[0];
?>
    
    <div class="SearchBox">
        <input type="hidden" name="data[MstAnalyze][id]" value="<?php echo $AnalyzeData['MstAnalyze']['id']; ?>" />
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>タイトル</th>
                <td>
                    <input type="text"
                           SIZE="90"
                           name="data[MstAnalyze][title]"
                           class="r search_canna"
                           id="MstAnalyzeTitle"
                           value="<?php echo $AnalyzeData['MstAnalyze']['title']; ?>"
                          />
                </td>
                <td></td>
            </tr>
            <tr>
                <th>SQL文</th>
                <td>
                    <TEXTAREA NAME="data[MstAnalyze][sql_data]"
                              rows="13"
                              cols="70"
                              wrap="soft"
                              id="MstAnalyzeSqlData"
                             ><?php echo $AnalyzeData['MstAnalyze']['sql_data']; ?></TEXTAREA>
                </td>
                <td></td>
            </tr>
            </tr>
            <tr>
                <th>施主指定</th>
                <td>
<?php
                    echo $this->form->radio('MstAnalyze.donor_selectable'
                                     ,$ConditionSelectableRadio
                                     ,array('legend'=>''
                                           ,'label'=>false
                                           ,'value'=>($AnalyzeData['MstAnalyze']['donor_selectable']=="")?0:$AnalyzeData['MstAnalyze']['donor_selectable']
                                           ,'id'=>'donor_selectable')
                                     );
?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>施設指定</th>
                <td>
<?php 
                    echo $this->form->radio('MstAnalyze.facility_selectable'
                                     ,$ConditionSelectableRadio
                                     ,array('legend'=>''
                                           ,'label'=>false
                                           ,'value'=>($AnalyzeData['MstAnalyze']['facility_selectable']=="")?0:$AnalyzeData['MstAnalyze']['facility_selectable']
                                           ,'id'=>'facility_selectable')
                                     ); 
?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>部署指定</th>
                <td>
<?php 
                    echo $this->form->radio('MstAnalyze.department_selectable'
                                     ,$ConditionSelectableRadio
                                     ,array('legend'=>''
                                           ,'label'=>false
                                           ,'value'=>($AnalyzeData['MstAnalyze']['department_selectable']=="")?0:$AnalyzeData['MstAnalyze']['department_selectable']
                                           ,'id'=>'department_selectable')
                                     ); 
?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>日付指定</th>
                <td>
<?php 
                    echo $this->form->radio('MstAnalyze.dates_selectable'
                                     ,$ConditionSelectableRadio
                                     ,array('legend'=>''
                                           ,'label'=>false
                                           ,'value'=>($AnalyzeData['MstAnalyze']['dates_selectable']=="")?0:$AnalyzeData['MstAnalyze']['dates_selectable']
                                           ,'id'=>'dates_selectable')
                                     ); 
?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>日数指定</th>
                <td>
<?php 
                    echo $this->form->radio('MstAnalyze.daynumber_selectable'
                                     ,$ConditionSelectableRadio
                                     ,array('legend'=>''
                                           ,'label'=>false
                                           ,'value'=>($AnalyzeData['MstAnalyze']['daynumber_selectable']=="")?0:$AnalyzeData['MstAnalyze']['daynumber_selectable']
                                           ,'id'=>'daynumber_selectable')
                                     ); 
?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>タイトル行指定</th>
                <td>
<?php 
                    echo $this->form->radio('MstAnalyze.title_selectable'
                                     ,$ConditionSelectableRadio
                                     ,array('legend'=>''
                                           ,'label'=>false
                                           ,'value'=>($AnalyzeData['MstAnalyze']['title_selectable']=="")?0:$AnalyzeData['MstAnalyze']['title_selectable']
                                           ,'id'=>'title_selectable')
                                     ); 
?>
                </td>
                <td></td>
            </tr>
            <?php if(Configure::read('SplitTable.flag') == 1){ ?>
            <tr>
                <th>分割対応</th>
                <td>
<?php 
                    echo $this->form->radio('MstAnalyze.split_table_flg'
                                     ,$ConditionSelectableRadio
                                     ,array('legend'=>''
                                           ,'label'=>false
                                           ,'value'=>($AnalyzeData['MstAnalyze']['split_table_flg']=="")?0:$AnalyzeData['MstAnalyze']['split_table_flg']
                                           ,'id'=>'split_table_flg')
                                     ); 
?>
                </td>
                <td></td>
            </tr>
            <?php } ?>
            <?php if ($mode=="mod") { ?>
                <tr>
                    <th>削除フラグ</th>
                    <td>
                        <input type="checkbox"
                               name="data[MstAnalyze][is_deleted]"
                               class=""
                               id="MstAnalyzeDelete"
                               value="1"
                               <?php echo ($AnalyzeData['MstAnalyze']['is_deleted']=='1')?"CHECKED":""; ?>
                              />
                        削除する
                    </td>
                    <td></td>
                </tr>
            <?php } ?>

            <tr>
                <th>センター</th>
                <td>
                    <?php echo $this->form->input('MstAnalyze.mst_facility_id' , array('options'=>$facility_enabled , 'value'=>(isset($AnalyzeData['MstAnalyze']['mst_facility_id'])?$AnalyzeData['MstAnalyze']['mst_facility_id']:null) ,'class'=>'txt' , 'empty'=>'すべて')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>区切り文字</th>
                <td>
                    <?php echo $this->form->input('MstAnalyze.delimiter_type' , array('options'=>Configure::read('AnalyzeDelimiterType.typelist') , 'value'=>$AnalyzeData['MstAnalyze']['delimiter_type'] ,'class'=>'txt' , 'empty'=>false)); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>囲み文字</th>
                <td>
                    <?php echo $this->form->input('MstAnalyze.quote_type' , array('options'=>Configure::read('AnalyzeQuoteType.typelist') , 'value'=>$AnalyzeData['MstAnalyze']['quote_type'] ,'class'=>'txt' , 'empty'=>false)); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle01 table-odd">
            <colgroup>
                <col width="25" />
                <col />
            </colgroup>
            <tr>
                <th><input type="checkbox" onClick="listAllCheck(this);" /></th>
                <th>出力対象者</th>
            </tr>
<?php
            $cnt=0;
            foreach($AnalyzesRoleData As $AnalyzeRoleData) {
                $checked = "";
                if ( $AnalyzeRoleData['MstAnalyzeRole']['checked'] == '1' ) {
                    $checked = " CHECKED";
                }
?>
                <tr>
                    <td class="center">
                        <input type="checkbox"
                               class="chk"
                               name="data[MstAnalyzeRole][mst_role_id][]"
                               id="MstAnalyzeRoleable"
                               value="<?php echo $AnalyzeRoleData['MstRole']['id']; ?>"
                               <?php echo $checked; ?>
                              >
                    </td>
                    <td><?php echo h_out($AnalyzeRoleData['MstRole']['role_name']); ?></td>
                </tr>
<?php
                $cnt++;
            }
?>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn2 [p2]" id="btn_Commit" />
    </div>
</form>
