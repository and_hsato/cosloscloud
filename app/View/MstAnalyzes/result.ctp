<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/analyzes_list">分析一覧</a></li>
        <li class="pankuzu">分析編集</li>
        <li>分析編集結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">分析編集結果</h2>
<div class="Mes01"><?php echo $ResultMessage; ?></div>
<?php
if ( $ErrorFlag == false && $DeleteFlag == false ) {
?>
    <form method="post" id="analyze_form">
<?php
        $AnalyzeData = $AnalyzesData[0];
?>
        <div class="SearchBox">
            <input type="hidden" name="data[MstAnalyze][id]" value="<?php echo $AnalyzeData['MstAnalyze']['id']; ?>" />
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>タイトル</th>
                    <td>
                        <?php echo h_out($AnalyzeData['MstAnalyze']['title']); ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>SQL文</th>
                    <td>
                        (長いので非表示)
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>施主指定</th>
                    <td>
                        <?php echo ($AnalyzeData['MstAnalyze']['donor_selectable']=='')?"無し":"有り"; ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>施設指定</th>
                    <td>
                        <?php echo ($AnalyzeData['MstAnalyze']['facility_selectable']=='')?"無し":"有り"; ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>部署指定</th>
                    <td>
                        <?php echo ($AnalyzeData['MstAnalyze']['department_selectable']=='')?"無し":"有り"; ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>日付指定</th>
                    <td>
                        <?php echo ($AnalyzeData['MstAnalyze']['dates_selectable']=='')?"無し":"有り"; ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>日数指定</th>
                    <td>
                        <?php echo ($AnalyzeData['MstAnalyze']['daynumber_selectable']=='')?"無し":"有り"; ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>タイトル行</th>
                    <td>
                        <?php echo ($AnalyzeData['MstAnalyze']['title_selectable']=='')?"無し":"有り"; ?>
                    </td>
                    <td></td>
                </tr>
                <?php if(Configure::read('SplitTable.flag') == 1){ ?>
                <tr>
                    <th>分割対応</th>
                    <td>
                        <?php echo ($AnalyzeData['MstAnalyze']['split_table_flg']=='')?"無し":"有り"; ?>
                    </td>
                    <td></td>
                </tr>
                <?php } ?>
                <tr>
                    <th>センター</th>
                    <td>
                        <?php echo ($AnalyzeData['MstAnalyze']['facility_name']); ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>区切り文字</th>
                    <td>
                        <?php echo ($AnalyzeData['MstAnalyze']['delimiter_type_name']); ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>囲み文字</th>
                    <td>
                        <?php echo ($AnalyzeData['MstAnalyze']['quote_type_name']); ?>
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd">
                <colgroup>
                    <col />
                </colgroup>
                <tr>
                    <th>出力対象者</th>
                </tr>
<?php
                $cnt=0;
                foreach($AnalyzesRoleData As $AnalyzeRoleData) {
?>
                <tr>
                    <td><?php echo h_out($AnalyzeRoleData['MstRole']['role_name']); ?></td>
                </tr>
<?php
                    $cnt++;
                }
?>
            </table>
        </div>
    </form>
<?php
}
?>