<script>
    $(function(){
        $("#btn_Add").click(function(){
            $("#AnalyzeList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add');
            $("#AnalyzeList").submit();
        });

        $("#btn_Mod").click(function(){
            if($('input[type=radio].chk:checked').length === 0){
                alert('編集を行いたい分析を選択してください');
            } else {
                $("#AnalyzeList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod');
                $("#AnalyzeList").submit();
            }
        });

        $("#btn_Search").click(function(){
            $("#AnalyzeList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/analyzes_list');
            $("#AnalyzeList").submit();
        });
        $('#AnalyzeList').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/analyzes_list');
    });
</script>

    <div id="TopicPath">
        <ul>
              <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
              <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
              <li>分析一覧</li>
        </ul>
    </div>
    <h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
    <h2 class="HeaddingMid">分析一覧</h2>

    <form class="validate_form search_form" method="post" action="" accept-charset="utf-8" id="AnalyzeList">
        <?php echo $this->form->input('MstAnalyze.is_search' , array('type'=>'hidden')); ?>
        <div class="SearchBox">
            <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
            </colgroup>
            <tr>
                <th>分析ID</th>
                <td>
                    <?php echo $this->form->input('MstAnalyze.id' , array('type'=>'text' , 'class'=>'txt' , 'id'=>'SearchId' )); ?>
                </td>
                <td></td>
                <td>
                    <?php echo $this->form->input('MstAnalyze.is_deleted',array('type'=>'checkbox')); ?>
                    削除も表示する
                </td>
            </tr>

            <tr>
                <th>タイトル</th>
                <td>
                    <?php echo $this->form->input('MstAnalyze.title' , array('type'=>'text' , 'class'=>'txt search_canna' , 'id'=>'SearchTitle' )); ?>
                </td>
                <td></td>
                <td>
                </td>
            </tr>
            <tr>
                <th>センター</th>
                <td>
                    <?php echo $this->form->input('MstAnalyze.mst_facility_id' , array('options'=>$facility_enabled , 'class'=>'txt' , 'empty'=>true)); ?>
                </td>
                <td></td>
                <td>
                </td>
            </tr>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="btn btn1" id="btn_Search"/>
            <input type="button" class="btn btn11 [p2]" id="btn_Add"/>
        </div>

        <div class="results">
            <h2 class="HeaddingSmall">検索結果</h2>
            <div class="SelectBikou_Area">
                <?php echo $this->element('limit_combobox',array('result'=>count($Analyzes_List))); ?>
            </div>

            <div class="TableScroll">
                <table class="TableStyle01 table-odd">
                <colgroup>
                    <col width="25" />
                    <col width="100"/>
                    <col />
                    <col width="150"/>
                    <col width="50"/>
                </colgroup>
                <tr>
                    <th></th>
                    <th>分析ID</th>
                    <th>タイトル</th>
                    <th>センター</th>
                    <th>削除</th>
                </tr>
<?php 
                if(!empty($Analyzes_List)){
                    $cnt=0;
                    foreach ($Analyzes_List as $analyzes) {
?>
                        <tr>
                            <td class="center">
                                <input name="data[MstAnalyze][id]"
                                       id="MstAnalyzeId"
                                       class="chk"
                                       type="radio"
                                       value="<?php echo $analyzes["MstAnalyze"]["id"]; ?>"
                                      />
                            </td>
                            <td><?php echo h_out($analyzes['MstAnalyze']['id']); ?></td>
                            <td><?php echo h_out($analyzes['MstAnalyze']['title']); ?></td>
                            <td><?php echo h_out($analyzes['MstAnalyze']['center_name'],'center'); ?></td>
                            <td>
                                <label title="<?php echo ($analyzes['MstAnalyze']['is_deleted'] == TRUE) ? "○" : "";  ?>"><p align="center"><?php echo ($analyzes['MstAnalyze']['is_deleted'] == TRUE) ? "○" : "";  ?></p></label>
                            </td>
                        </tr>
<?php
                        $cnt++;
                    }
                } else {
                    if(isset($this->request->data['MstAnalyze']['is_search'])){
?>
                        <tr>
                            <td colspan="5" class="center">該当するデータがありませんでした。</td>
                        </tr>
<?php
                    }
                }
?>
                </table>
            </div>
        </div>

<?php
        if(!empty($Analyzes_List)){
?>
            <div class="ButtonBox">
                <input type="button" class="btn btn9" id="btn_Mod"/>
            </div>
<?php
        }
?>
    </form>
