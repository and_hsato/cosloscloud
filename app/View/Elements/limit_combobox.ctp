<?php
  $_is_display= true;
  if( isset($is_display)) {
      $_is_display= $is_display;
  }
  $_param = 'limit';
  if( isset($param)) {
      $_param = $param;
  }
  $_result = 0;
  if( isset($result)) {
      $_result = $result;
  }
?>

<?php if( $_is_display ): ?>
<div class="DisplaySelect">
<?php echo $this->form->input($_param, array('options'=>Configure::read('displaycounts_combobox'),'class'=>' txt','empty'=>false)); ?>
<?php if(isset($max)){?>
<?php if($max==$_result){?>
<?php echo number_format($max)?>件中 <?php echo number_format($_result) ?>件表示しています
<?php }else{ ?>
<span class="AlertLimitBox"><?php echo number_format($max)?>件中 <?php echo number_format($_result) ?>件表示しています</span>
<?php } ?>
<?php }else{ ?>
<?php echo number_format($_result) ?>件表示しています
<?php }?>
</div>
<?php endif; ?>