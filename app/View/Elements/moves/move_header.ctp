<script type="text/javascript">

function dateCheck( checkStr ) {
	if( !checkStr.match(/^\d{4}\/\d{2}\/\d{2}$/)) {
	 return false;
	}

	var year = checkStr.substr( 0 , 4 );
  var month = checkStr.substr( 5 , 2 );
  var day = checkStr.substr( 8 , 2 );

  if( month > 0 && month < 13 && day > 0 && day < 32 ) {
	  var date = new Date( year ,month -1 , day );
	  if( isNaN(date) ) {
		  return false;
	  } else if( date.getFullYear() == year && date.getMonth() == month - 1  ,date.getDate() == day ){
		  return true;
		} else {
			return false;
		}
	} else {
	  return false;
  }
}

function headerCheck() {
	  if( !dateCheck($("#datepicker1").val()) ) {
		  alert('日付が正しくありません');
		  return false;
	  }

	  return true;
}

//jquery calendar
$(function($){
  $("#datepicker1").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});
});

/**
 * page transfer.
 */
$(function(){
  $("#move2outer_form_btn").click(function(){

    $("#move2outer_form").attr('action', '<?php echo $this->webroot; ?>moves/select_item/<?php echo Configure::read('MoveType.move2outer.reserved'); ?>');
    $("#move2outer_form").attr('method', 'post');

    if( !headerCheck() )
        return false;

    if( $("#user_facility_txt").val() == $("#facility_from_txt").val() ) {
        alert( "移動元施設と移動先施設は別々にしてください。" );
        return false;
    }

    $("#move2outer_form").submit();
  });
});

$(function($) {
    $(".facility").change( function() {

      prefix = $(this).attr('id').substring(0 , $(this).attr('id').indexOf('_'));

      $('#' + prefix + '_facility_txt').val($('#' + $(this).attr('id') + '  option:selected').val());
      $('#' + prefix + '_facility_name').val($('#' + $(this).attr('id') + ' option:selected').text());

      if( prefix == "user" ) {
            fieldName = $(this).attr('id');
            fieldValue = $(this).val();
            $.post("<?php echo $this->webroot; ?>consumes/get_departments/",
                    {
                      field: fieldName,
                      value: fieldValue
                    },
                    function (data) {
                      var options = '<option value=""></option>';
                      var obj = jQuery.parseJSON(data);
                      $.each(obj, function(index, o) {
                        //options += '<option value="' + o.id + '">' + o.department_name + '</option>';
                        options += '<option value="' + o.department_code + '">' + o.department_name + '</option>';
                      });
                      $('#depts').html(options);
                      $('#depts').show();
                      $('#user_department_name').val('');
                      $('#user_department_txt').val('');
                    },
                    function (success) {
                      // What do I do?
                    },
                    function(error) {
                      if(error.length != 0) {
                        $('#' + prefix +'facility_selected').after('<div class="error-message" id="'+ fieldName +'-exists">' + error + '</div>');
                      }
                      else {
                        $('#' + fieldName + '-exists').remove();
                      }
                    });
      }
      return false;
    });
});


/**
 * Set department's display name, selecting department from selectbox.
 */
$(function () {
  $('#depts').change(function () {
  prefix = $(this).attr('id').substring(0 , $(this).attr('id').indexOf('_'));
  $('#user_department_name').val($('#depts option:selected').text());
    $('#user_department_txt').val($('#depts option:selected').val());
  });
});
/**
 * To input facility code to #user_facility_txt textfield, and
 * #facility_selected selectbox will change.
 */
$(function(){
  $('.facility_txt').change(function(){
    prefix = $(this).attr('id').substring(0 , $(this).attr('id').indexOf('_'));
    $('#' + prefix + '_facility_selected').selectOptions($(this).val());
    $('#' + prefix + '_facility_name').val($('#' + prefix + '_facility_selected option:selected').text());
    $('#' + prefix + '_facility_selected').select();
  });
});

/**
 * selectboxより部署を選択後、textfieldに部署コードを自動入力
 */
$(function(){
  $('#user_department_txt').change(function(){
    $('#depts').selectOptions($(this).val());
    //$('#user_facility_name').val($('#facility_selected option:selected').text());
    $('#depts').select();
  });
});

/**
 * textfieldに部署コードを入力後、selectboxより当該部署名を自動選択
 */
$(function () {
  $('#depts').change(function () {
    $('#user_department_txt').val($('#depts option:selected').val());
  });
});

$(function () {
  $('#work_class').change(function () {
    $('#work_class_txt').val($('#work_class option:selected').text());
  });
});

$(function () {
	  $('#facility_from_selected').change(function () {
	    $('#facility_from_name').val($('#facility_from_selected option:selected').text());
	    $('#facility_from_txt').val($('#facility_from_selected option:selected').val());
	  });
	});

$(function(){
	  $('#facility_from_txt').change(function(){
	    $('#facility_from_selected').selectOptions($(this).val());
	    $('#facility_from_name').val($('#facility_selected option:selected').text());
	    $('#facility_from_selected').select();
	  });
	});

<?php if(isset($to_facilities) && (count($to_facilities) === 1)):?>
//Reading webpage, selects a facility and fetches departments.
$(function () {
  $('#user_facility_selected').bind(
    'change',
    function(event, id){
      $("this option[value='"+id+"']").attr('selected', 'selected');
    });
  $("#user_facility_selected").trigger("change", 1);
});
<?php endif; ?>
</script>
<div class="SearchBox">
<table class="FormStyleTable">
  <colgroup>
    <col />
    <col />
    <col width="20" />
    <col />
    <col />
  </colgroup>
  <tr>
    <th>移動日<?php if($input): ?><?php endif; ?></th>
    <td><?php if($input): ?> <?php echo $this->form->input('TrnMoveHeader.work_date',array('maxlength'=>50,'type'=>'text','class'=>'r date validate[required,custom[date]] ','id'=>'datepicker1')); ?> <?php else: ?> <?php echo $this->form->input('TrnMoveHeader.work_date',array('type' => 'text','class'=>'lbl','value'=>$this->request->data['TrnMoveHeader']['work_date'],'readonly'=>'readonly')); ?>
    <?php endif; ?></td>
    <td></td>
    <th>作業区分</th>
    <td><?php if($input): ?> <?php echo $this->form->input('TrnMoveHeader.work_class_txt',array('type'=>'hidden','id'=>'work_class_txt')); ?> <?php echo $this->form->input('TrnMoveHeader.work_class',array('options'=>$work_classes,'empty'=>true,'class'=>'txt','id'=>'work_class')); ?> <?php else: ?>
    <?php echo $this->form->input('TrnMoveHeader.work_class',array('type'=>'hidden','value'=>$this->request->data['TrnMoveHeader']['work_class'])); ?> <?php echo $this->form->input('TrnMoveHeader.work_class_txt',array('class'=>'lbl','value'=>$this->request->data['TrnMoveHeader']['work_class_txt'],'readonly'=>'readonly')); ?>
    <?php endif; ?></td>
  </tr>
  <?php if( $page != Configure::read('MoveType.move2outside')):?>
  <tr>
    <th>移動先施設<?php if($input): ?><?php endif; ?></th>
    <td><?php if($input): ?> <input type="hidden" id="user_facility_name" name="data[TrnMoveHeader][user_facility_name]" value="" /> <?php echo $this->form->input('TrnMoveHeader.user_facility_txt',array('id' => 'user_facility_txt', 'label' => false, 'div' => false, 'class' => 'r facility_txt', 'style'=>'width: 60px;')); ?>
    <?php
    $_hasEmpty = (count($to_facilities) > 1) ? true:false;
    echo $this->form->input('TrnMoveHeader.user_facility',array('options'=>$to_facilities,'empty'=>true,'class'=>'txt r validate[required] facility','style'=>'width:150px;','id' => 'user_facility_selected','empty'=>$_hasEmpty));
    ?> <?php else:?> <?php echo $this->form->input('TrnMoveHeader.user_facility',array('type'=>'hidden','value'=>$this->request->data['TrnMoveHeader']['user_facility'])); ?> <?php echo $this->form->input('TrnMoveHeader.user_facility_name',array('class'=>'lbl','value'=>$this->request->data['TrnMoveHeader']['user_facility_name'],'readonly'=>'readonly')); ?>
    <?php endif; ?></td>
    <td></td>
    <th>備考</th>
    <td><?php if($input): ?> <?php echo $this->form->input('TrnMoveHeader.recital',array('maxlength'=>50,'class'=>'txt')); ?> <?php else:?> <?php echo $this->form->input('TrnMoveHeader.recital',array('class'=>'lbl','value'=>$this->request->data['TrnMoveHeader']['recital'],'readonly'=>'readonly')); ?>
    <?php endif; ?></td>
  </tr>
  <tr>
    <th>移動先部署<?php if($input): ?><?php endif; ?></th>
    <td><?php if($input): ?> <input type="hidden" id="user_department_name" name="data[TrnMoveHeader][user_department_name]" value="" /> <?php echo $this->form->input('TrnMoveHeader.user_department_txt', array('id' => 'user_department_txt', 'label' => false, 'div' => false, 'class' => 'r', 'style' => 'width: 60px;')); ?>
    <?php
    $_hasEmptyDep = (count($department_list) > 1) ? true:false;
    $_hasSelected = (count($department_list) > 1) ? null:1;
    echo $this->form->input('TrnMoveHeader.user_department',array('options'=>$department_list,'value'=>$_hasSelected,'class'=>'txt validate[required]','style'=>'width:150px;','id' => 'depts','empty'=>$_hasEmptyDep));
    ?> <?php else:?> <?php echo $this->form->input('TrnMoveHeader.user_department',array('type'=>'hidden','value'=>$this->request->data['TrnMoveHeader']['user_department'])); ?> <?php echo $this->form->input('TrnMoveHeader.user_department_name',array('class'=>'lbl','value'=>$this->request->data['TrnMoveHeader']['user_department_name'],'readonly'=>'readonly')); ?>
    <?php endif; ?></td>
  </tr>
  <?php if( $page == Configure::read('MoveType.move2outer.reserved')):?>
  <tr>
    <th>移動元施設<?php if($input): ?><?php endif; ?></th>
    <td><?php if($input): ?> <input type="hidden" id="facility_from_name" name="data[TrnMoveHeader][facility_from_name]" value="" /> <?php echo $this->form->input('TrnMoveHeader.facility_from_txt', array('id' => 'facility_from_txt', 'label' => false, 'div' => false, 'class' => 'r ', 'style' => 'width: 60px;')); ?>
    <?php echo $this->form->input('TrnMoveHeader.facility_from',array('options'=>$from_facilities,'empty'=>true,'class'=>'txt r validate[required]','style'=>'width:150px;','id' => 'facility_from_selected',)); ?> <?php else:?> <?php echo $this->form->input('TrnMoveHeader.facility_from',array('type'=>'hidden','value'=>$this->request->data['TrnMoveHeader']['facility_from'])); ?>
    <?php echo $this->form->input('TrnMoveHeader.facility_from_name',array('class'=>'lbl','value'=>$this->request->data['TrnMoveHeader']['facility_from_name'],'readonly'=>'readonly')); ?> <?php endif; ?></td>
  </tr>
  <?php endif;?>
  <?php else: ?>
  <?php echo $this->form->input('TrnMoveHeader.user_facility',array('type'=>'hidden','value'=>$this->request->data['TrnMoveHeader']['user_facility'])); ?>
  <?php echo $this->form->input('TrnMoveHeader.user_department',array('type'=>'hidden','value'=>$this->request->data['TrnMoveHeader']['user_department'])); ?>
  <?php if($input): ?>
  <tr>
    <th><?php echo Configure::read('facility_subcode'); ?></th>
    <td><?php echo $this->form->input('TrnMoveHeader.subcode',array('maxlength'=>50,'type'=>'text','class'=>'r validate[required]')); ?></td>
    <td></td>
    <th>備考</th>
    <td><?php echo $this->form->input('TrnMoveHeader.recital',array('maxlength'=>50,'class'=>'txt')); ?></td>
  </tr>
  <?php else: ?>
  <tr>
    <th><?php echo Configure::read('facility_subcode'); ?></th>
    <td><?php echo $this->form->input('TrnMoveHeader.subcode',array('class'=>'lbl','value'=>$this->request->data['TrnMoveHeader']['asist_department'],'readonly'=>'readonly')); ?></td>
    <td></td>
    <th>備考</th>
    <td><?php echo $this->form->input('TrnMoveHeader.recital',array('class'=>'lbl','value'=>$this->request->data['TrnMoveHeader']['recital'],'readonly'=>'readonly')); ?></td>
  </tr>
  <?php endif; ?>
  <?php endif; ?>
</table>
</div>
  <?php if( $page == Configure::read('MoveType.move2inner.reserved') && !$input ):?>
  <?php echo $this->form->input('TrnMoveHeader.facility_from',array('type'=>'hidden','value'=>$this->request->data['TrnMoveHeader']['facility_from'])); ?>
  <?php echo $this->form->input('TrnMoveHeader.facility_from_txt',array('type'=>'hidden','value'=>$this->request->data['TrnMoveHeader']['facility_from_txt'])); ?>
  <?php echo $this->form->input('TrnMoveHeader.facility_from_name',array('type'=>'hidden','value'=>$this->request->data['TrnMoveHeader']['facility_from_name'])); ?>
  <?php elseif( ( $page == Configure::read('MoveType.move2other.reserved') ||$page == Configure::read('MoveType.move2outside')) && isset($this->request->data['TrnMoveHeader']['facility_from'])):?>
  <?php echo $this->form->input('TrnMoveHeader.facility_from',array('type'=>'hidden','value'=>$this->request->data['TrnMoveHeader']['facility_from'])); ?>
  <?php endif; ?>
