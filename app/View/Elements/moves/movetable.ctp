<?php
if( !isset($view_type))
$view_type = 'default';
?>
<script type="text/javascript">


function listAllCheckAndCopy(target){
    $(".chk").each(function() {
        this.checked=target.checked;
        this.onclick();
      }
    );
}


/**
 * Setting id of table object for copying to another div.
 * @param id
 */
function setCopyTableId(id) {
	if( $('#itemSelectedTable') != undefined) {
	  if ($('#editable-checkbox'+id).attr('checked')) {
	    var cloneRow = $('#itemSelectedRow'+id).clone().attr('id' , 'cloneRow' + id);
	    cloneRow.appendTo('#itemSelectedTable');
	    $('#itemSelectedTable > #cloneRow'+id+' > tbody > tr > td > .chk').each(function(){
	    	//this.disabled = 'disabled';
	    	$('#itemSelectedTable > #cloneRow'+id+' > tbody > tr > td > .chk').attr('onclick','');
		  });
	    $('#containTables > #itemSelectedRow'+id).attr('style','margin:0;padding:0;');
	  }
	  else {
		  $('#itemSelectedTable > #cloneRow' + id).remove();
	    $('#containTables > #itemSelectedRow'+id).attr('style','margin:0;padding:0;');
	  }
	}
}
</script>
<div class="TableHeaderAdjustment01">
<table class="TableHeaderStyle01">
  <tr>
    <th class="col5" style="width: 20px;" rowspan="2"><?php if($view_type != 'result'): ?><input type="checkbox" onClick="listAllCheckAndCopy(this);" /><?php endif; ?></th>
    <th class="col10">商品ID</th>
    <th class="col20">商品名</th>
    <th class="<?php echo ($view_type != 'default' ? 'col10' : 'col15');  ?>">製品番号</th>
    <th class="col10">包装単位</th>
    <th class="col10">ロット番号</th>
    <th class="col15">センターシール</th>
    <th class="col15">施設</th>
    <?php if($view_type != 'default'): ?>
    <th class="col10">作業区分</th>
    <?php endif; ?>
  </tr>
  <tr>
    <th></th>
    <th>規格</th>
    <th>販売元</th>
    <th>数量</th>
    <th>有効期限</th>
    <th>部署シール</th>
    <th>部署</th>
    <?php if($view_type != 'default'): ?>
    <th>備考</th>
    <?php endif; ?>
  </tr>
</table>
</div>
<div class="TableScroll" id="containTables"><?php $i = 0; foreach ($results as $s): ?> <?php
$_selected = false;
if( isset($selected_stickers)) {
  foreach( $selected_stickers as $selected_sticker ) {
  		if( $selected_sticker['TrnSticker']['id'] == $s['TrnSticker']['id']) {
  		  $_selected = true;
  		  break;
  		}
  }
}
?>
<table class="TableStyle02" id="itemSelectedRow<?php echo $s['TrnSticker']['id']?>" style="margin-bottom:-1; padding: 0;">
  <tr class="<?php echo ($i%2 ===1)? 'odd':'';?>">
    <td class="col5" style="width: 20px; text-align: center;" rowspan="2"><?php if($view_type != 'result'): ?> <?php echo $this->form->input('Moves.'.$i.'.TrnSticker.id', array('type'=>'hidden','value'=>$s['TrnSticker']['id'])); ?> <?php echo $this->form->input('Moves.'.$i.'.checked',array('type'=>'checkbox','class'=>'center chk','id'=>'editable-checkbox'.$s['TrnSticker']['id'],'onClick'=>'setCopyTableId('.$s['TrnSticker']['id'].');','checked' => ($_selected || $view_type == 'confirm'),'value'=>$s['TrnSticker']['id'])); ?>
    <?php endif; ?></td>
    <td class="col10"><?php echo h( $s['MstFacilityItem']['internal_code']); ?></td>
    <td class="col20"><?php echo h( $s['MstFacilityItem']['item_name']); ?></td>
    <td class="<?php echo ($view_type != 'default' ? 'col10' : 'col15');  ?>"><?php echo h( $s['MstFacilityItem']['item_code']); ?></td>
    <td class="col10"><?php echo h( $s['MstUnitName']['unit_name']); ?><?php if( isset($s['MstItemUnit']['per_unit']) && $s['MstItemUnit']['per_unit'] > 1 ): ?>（<?php echo $s['MstItemUnit']['per_unit']; ?><?php echo isset($s['MstPerUnitName']['unit_name']) ? h( $s['MstPerUnitName']['unit_name'] ) : ''; ?>）<?php endif; ?>
    </td>
    <td class="col10"><?php echo $s['TrnSticker']['lot_no']; ?></td>
    <td class="col15"><?php echo $s['TrnSticker']['facility_sticker_no']; ?></td>
    <td class="col15"><?php echo h( $s['MstFacility']['facility_name']); ?></td>
    <?php if($view_type == 'confirm'): ?>
    <td class="col10"><?php echo $this->form->input('Moves.'.$i.'.work_class',array('options'=>$work_classes,'value'=>$this->request->data['TrnMoveHeader']['work_class'],'class'=>'txt')); ?></td>
    <?php elseif($view_type == 'result'): ?>
    <td class="col10"><?php echo h($this->request->data['Moves'][$i]['work_class'] != '' ? $work_classes[$this->request->data['Moves'][$i]['work_class']] : ''); ?> <?php endif; ?>

  </tr>
  <tr class="<?php echo ($i%2 ===1)? 'odd':'';?>">
    <td><?php echo ($s['TrnSticker']['has_reserved'] ? '移動予約済':'') ;?> <?php echo $this->form->input('Moves.'.$i.'.has_reserved', array('type'=>'hidden','value'=>$s['TrnSticker']['has_reserved'])); ?></td>
    <td><?php echo h( $s['MstFacilityItem']['standard']); ?></td>
    <td><?php echo isset($s['MstDealer']['dealer_name']) ? h( $s['MstDealer']['dealer_name']) : ''; ?></td>
    <td><?php if( $view_type == 'confirm' ):?> <?php if( $s['MstFacilityItem']['is_lowlevel'] === true): ?> <?php echo $this->form->input('Moves.'.$i.'.quantity', array('class'=>'validate[custom[onlyNumber]]','style'=>'width: 80px','value'=>$s['TrnSticker']['quantity'])); ?>
    <?php else: ?> <?php echo $this->form->input('Moves.'.$i.'.quantity', array('type'=>'hidden','value'=>1)); ?>1 <?php endif; ?> <?php elseif( $view_type == 'result'): ?> <?php if( $s['MstFacilityItem']['is_lowlevel'] === true): ?> <?php echo $this->form->input('Moves.'.$i.'.quantity', array('class'=>'txt','style'=>'width:85px;','readonly'=>'readonly')); ?>
    <?php else: ?> <?php echo h( $s['TrnSticker']['quantity']); ?> <?php endif; ?> <?php else: ?> <?php echo h( $s['TrnSticker']['quantity']); ?> <?php endif; ?></td>
    <td><?php echo $s['TrnSticker']['validated_date']; ?></td>
    <td><?php echo $s['TrnSticker']['hospital_sticker_no']; ?></td>
    <td><?php echo h( $s['MstDepartment']['department_name']); ?> <?php echo $this->form->input('Moves.'.$i.'.department_from', array('type'=>'hidden','value'=>$s['MstDepartment']['department_code'])); ?></td>
    <?php if($view_type == 'confirm'): ?>
    <td><?php echo $this->form->input('Moves.'.$i.'.recital', array('class'=>'txt','style'=>'width:85px;')); ?></td>
    <?php elseif($view_type == 'result'): ?>
    <td><?php echo h($this->request->data['Moves'][$i]['recital']); ?></td>
    <?php endif; ?>
  </tr>
</table>
    <?php $i++; endforeach; ?></div>
