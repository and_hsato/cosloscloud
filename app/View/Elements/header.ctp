<?php if ($this->session->check('Auth.MstUser') && $this->session->read('Auth.enableMenu')) {?>
<script type="text/javascript">
<!--
$(function(){
    $("#usersFacilities").change(function(){
        if(!confirm('作業対象とする施設を「' + $('#usersFacilities option:selected').text() + '」に変更しますが、よろしいですか？')) {
            return false;
        } else {
            $('#facility_id_selected').val($('#usersFacilities option:selected').val());
            $("#header_form").attr('action', '<?php echo $this->webroot; ?>login/change_facility/'+$('#usersFacilities option:selected').val()).submit();
        }
    });
    $('#logout').click(function(){
        $("#header_form").attr('action', '<?php echo $this->webroot; ?>login/logout').submit();
    });
    $('#myPage').click(function(){
        $("#header_form").attr('action', '<?php echo $this->webroot; ?>MyPage').submit();
    });
});
//-->
</script>
<div id="header">
    <div class="HeaderLogo"></div>
    <form id="header_form" method="post">
        <p class="facility">
        <?php echo $this->form->input('facility_id_selected',array('type'=>'hidden','tabindex'=>'-1')); ?>
        <?php if(count($this->session->read('Auth.facilities')) > 1){ ?>
        <?php echo $this->Form->input('usersFacilities',array('type'=>'select','options'=>$this->session->read('Auth.facilities'),'value'=>$this->session->read('Auth.facility_id_selected'),'empty'=>false,'tabindex'=>'-1')); ?>
        <?php } else { ?>
        <?php $tmp = $this->session->read('Auth.facilities');
            echo $tmp[$this->session->read('Auth.facility_id_selected')];
        ?>
        <?php } ?>
        </p>
        <?php $masterItemInfo = $this->Session->read('Auth.masterItemInfo'); ?>
        <p class="h-menu">
          <span style="display:block;">
            <a href="https://teachme.jp/r/Coslos" target="_blank"><img src="<?php echo $this->webroot ?>img/question.png" height="18px" width="18px">マニュアル</a>
          | <span class="edition-bg bg_platinum"><?php echo $masterItemInfo['edition_name']?></span>
          | <span class="master-bg"><?php echo $masterItemInfo['plan_name']?></span>
          | Myマスタ登録件数 : <?php echo number_format($masterItemInfo['use_count']); ?> / <?php echo number_format($masterItemInfo['max_count'])?>
          | <span id="myPage"><?php echo $this->session->read('Auth.MstUser.user_name'); ?>(編集)</span>
          </span>
          <span style="display:block;float:right;">
            <span>コスロスマスタ総件数 : <?php echo number_format($masterItemInfo['grand_count'])?>件</span>
            <span style="margin-left:50px;" id="logout">ログアウト</span>
          </span>
        </p>
    </form>
</div>
<?php echo $this->session->read('Auth.menus'); ?>
<?php } ?>
