<?php if ($this->session->check('Auth.MstUser') && $this->session->check('Auth.enableMenu')) {?>
<footer>
<div class="common-footer">
<ul>
<li><a href="http://cloud.coslos.jp" target="_blank">Coslos Cloud</a></li>
<li><a href="http://www.andinfo.co.jp/company/summary.html" target="_blank">運営会社</a></li>
<li><a href="http://cloud.coslos.jp/terms/" target="_blank">利用規約</a></li>
<li><a href="http://coslos.jp/guide/privacy.php" target="_blank">プライバシーポリシー</a></li>
<li><a href="http://cloud.coslos.jp/contact/" target="_blank">ご相談窓口</a></li>
</ul>
<p class="common-copyRight">Copyright © AND Inc.</p>
</div>
</footer>

<div id="floating-menu">
  <ul style="list-style-type: none">
    <li><input type="button" class="common-button" id="btnAutoScrollTop" value="▲"/></li>
    <li><input type="button" class="common-button" id="btnAutoScrollBottom" value="▼"/></li>
  </ul>
</div>

<?php } ?>