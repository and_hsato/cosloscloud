    <form method="post" action="" accept-charset="utf-8" id="ApplicationInfo" class="validate_form search_form">
        <?php echo $this->form->input('Application.id' , array('type'=>'hidden'))?>
        <h2 class="HeaddingSmall">申請情報</h2>
        <table class="FormStyleTable">
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('Application.item_name',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
                <th>規格</th>
                <td><?php echo $this->form->input('Application.standard',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('Application.item_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
            </tr>
            <tr>
                <th>JANコード</th>
                <td><?php echo $this->form->input('Application.jan_code',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('Application.dealer_name',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
                <th>備考</th>
                <td><?php echo $this->form->input('Application.recital',array('class' => 'lbl', 'readonly' => 'readonly')); ?></td>
            </tr>
        </table>
        <h2 class="HeaddingSmall">マスタ情報</h2>

        <table class="FormStyleTable">
            <tr>
                <th>マスタID</th>
                <td><?php echo $this->form->input('MstGrandmaster.master_id', array('type'=>'text','label'=>false,'div'=>false, 'maxlength'=>'20','class'=>'r validate[required] search_upper','id'=>'internal_code'));?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstGrandmaster.item_code', array('label'=>false,'div'=>false, 'maxlength'=>'100','class'=>'txt search_upper'));?></td>
                <td></td>
                <th>予備キー１</th>
                <td><?php echo $this->form->input('MstGrandmaster.spare_key1', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstGrandmaster.item_name', array('type'=>'text','label'=>false,'div'=>false, 'maxlength'=>'100','class'=>'r validate[required] search_canna','width'=>'150px' ));?></td>
                <td></td>
                <th>販売元</th>
                <td>
                <?php echo $this->form->input('MstDealer.id', array('type'=>'hidden','id'=>'mst_dealer_id_hidden_txt')); ?>
                <?php echo $this->form->input('MstDealer.dealer_code', array('type'=>'hidden','id'=>'mst_dealer_code_hidden_txt')); ?>
                <?php echo $this->form->input('MstDealer.dealer_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'dealer_name_txt','readonly' => true));?>
                <input type="button" value="選択" id="call_search_dealer_form_btn" /></td>
                <td></td>
                <th>予備キー２</th>
                <td><?php echo $this->form->input('MstGrandmaster.spare_key2', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('MstGrandmaster.standard', array('type'=>'text','label'=>false,'div'=>false, 'maxlength'=>'100','class'=>'txt' ));?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('MstGrandmaster.jan_code', array('label'=>false,'div'=>false, 'maxlength'=>'13','class'=>'txt validate[optional,custom[onlyNumber]]' ));?></td>
                <td></td>
                <th>予備キー３</th>
                <td><?php echo $this->form->input('MstGrandmaster.spare_key3', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
            </tr>
            <tr>
                <th>保険請求区分</th>
                <td>
                    <?php echo $this->form->hidden('MstGrandmaster.insurance_claim_type_hidden',array('id'=>'insurance_claim_type_txt')); ?>
                    <?php echo $this->form->input('MstGrandmaster.insurance_claim_type_txt' , array('id'=>'insurance_claim_type_txt_p' , 'style'=>'width: 40px;','class'=>'txt','maxlength'=>'50')) ?>
                    <?php echo $this->form->input('MstGrandmaster.insurance_claim_type', array('options'=>$insuranceClaimDepartments, 'class' => 'txt', 'style'=>'width: 120px; height: 23px;','id'=>'insurance_claim_type_sel','empty'=>true)); ?>
                </td>
                <td></td>
                <th>JMDNコード</th>
                <td><?php echo $this->form->input('MstGrandmaster.jmdn_code', array('type'=>'hidden','id'=>'mst_jmdn_name_hidden_txt')); ?> <?php echo $this->form->input('MstFacilityItem.jmdn_code', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'jmdn_code_txt','readonly' => true));?>
                <input type="button" id="call_search_jmdn_form_btn" value="選択" /></td>
                <td></td>
                <th>予備キー４</th>
                <td><?php echo $this->form->input('MstGrandmaster.spare_key4', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
            </tr>
            <tr>
                <th>保険請求コード</th>
                <td>
                  <?php echo $this->form->input('MstGrandmaster.insurance_claim_code_hidden', array('type'=>'hidden','id'=>'mst_icc_name_hidden_txt')); ?>
                  <?php echo $this->form->input('MstGrandmaster.insurance_claim_code', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'icc_code_txt'));?>
                  <input type="button" id="call_search_icc_form_btn" value="選択" />
                </td>
                <td></td>
                <th>一般名称</th>
                <td><?php echo $this->form->input('MstGrandmaster.jmdn_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'mst_jmdn_name_txt','readonly' => true));?></td>
                <td></td>
                <th>入り数</th>
                <td><?php echo $this->form->input('MstGrandmaster.per_unit', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt num'));?></td>
            </tr>
            <tr>
                <th>保険請求略称</th>
                <td><?php echo $this->form->input('MstGrandmaster.insurance_claim_short_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'mst_icc_name_s_txt'));?></td>
                <td></td>
                <th>クラス分類</th>
                <td>
                <?php echo $this->form->input('MstGrandmaster.class_separation_hidden', array('type'=>'hidden','id'=>'class_separation_txt')); ?>
                <?php echo $this->form->input('MstGrandmaster.class_separation', array('options'=>$classseparations, 'label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'class_separation_sel' ,'style'=>'height: 23px;', 'empty'=>false)); ?>
                </td>
                <td></td>
                <th>単位</th>
                <td><?php echo $this->form->input('MstGrandmaster.unit_name' , array('options'=>$item_units , 'class'=>'txt' , 'empty'=>true))?></td>
            </tr>
            <tr>
                <th>保険請求名</th>
                <td><?php echo $this->form->input('MstGrandmaster.insurance_claim_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'mst_icc_name_txt'));?></td>
                <td></td>
                <th>生物由来区分</th>
                <td><?php echo $this->form->input('MstGrandmaster.biogenous_type', array('options'=>$biogenous_types,'empty'=>true, 'class'=>'txt','style'=>'width: 120px; height: 23px;')); ?></td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>償還価格</th>
                <td><?php echo $this->form->input('MstGrandmaster.refund_price', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt num r refund_price'));?></td>
                <td></td>
                <th>定価単価</th>
                <td><?php echo $this->form->input('MstGrandmaster.unit_price', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt num'));?></td>
                <td></td>
                <th>定価</th>
                <td><?php echo $this->form->input('MstGrandmaster.price', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt num'));?></td>
            </tr>
            <tr>
                <th>課税区分</th>
                <td><?php echo $this->form->input('MstGrandmaster.tax_type', array('options'=>$taxes, 'empty'=>true, 'class' => 'txt','style'=>'width: 120px; height: 23px;','div'=>false,'label'=>false)); ?></td>
                <td></td>
                <th>商品カテゴリ</th>
                <td>
                    <?php echo $this->form->input('MstGrandmaster.item_category', array('type'=>'hidden','id'=>'item_category_code_hidden_txt')); ?>
                    <?php echo $this->form->input('MstGrandmaster.item_category_txt', array('label'=>false,'div'=>false,'maxlength'=>50,'class'=>'txt','id'=>'item_category_name_txt')); ?>
                    <input type="button" id="call_search_item_category_form_btn" value="選択" />
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('MstGrandmaster.recital', array('label'=>false,'div'=>false,'maxlength'=>200,'class'=>'txt')); ?></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <input type="button" value="" class="btn btn29" id="btn_regist"/>
        </div>
    </form>
