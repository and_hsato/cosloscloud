<?php if (!empty($items)) { ?>
    <div class="results">
        <div class="TableScroll2">
            <table class="mymaster table-even">
                <colgroup>
                    <col width="1%" />
                </colgroup>
                <?php if (empty($hiddenCheckBox) || !$hiddenCheckBox) { ?>
                <tr style="background:#f7f7f7">
                    <td colspan="1"><input type="checkbox" class="<?php echo $chkAllClass ?>" ></td>
                    <td colspan="5" style="font-size:14px">全件選択</td>
                </tr>
                <?php } ?>
         <?php foreach ($items as $d) { $item = $d['MstGrandmaster']; ?>
                <tr>
                <?php if (empty($hiddenCheckBox) || !$hiddenCheckBox) { ?>
                    <td>
                        <input type="checkbox" class="<?php echo $chkClass ?>" name="data[grandmaster_ids][]" value="<?php echo $item['id'] ?>" >
                    </td>
                <?php } else { ?>
                        <input type="hidden" name="data[grandmaster_ids][]" value="<?php echo $item['id'] ?>" >
                <?php } ?>
                    <td>
                        <?php echo $this->Common->toImagePopupHtmlForMasterItem($item, null) ?> 
                    </td>
                    <td>
                        <span>【コスロスCD】</span><?php echo $item['master_id'] ?><br />
                        <span>【製品番号】</span<?php echo $item['item_code'] ?>
                    </td>
                    <td>
                        <span>【JANコード】</span><?php echo $item['jan_code'] ?><br />
                        <span>【販売先】</span><?php echo $item['dealer_name'] ?>
                    </td>
                    <td>
                        <span>【商品名】</span><?php echo $item['item_name'] ?><br />
                        <span>【規格】</span><?php echo $item['standard'] ?>
                    </td>
                    <td>
                        <span>【採用】</span><?php echo empty($d['MstFacilityItem']['id']) ? '未' : '済' ?>
                    </td>
                </tr>
         <?php } ?>
            </table>
        </div>
    </div>  
<?php } ?>
