        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>コスロスCD</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.master_id' , array('type'=>'text' , 'maxlength'=>'14' ,'class'=>'txt' , 'id'=>'master_id')) ?>
                    <input type="button" style="display:none;" value="付替" id="change_masterId" />
                </td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('MstFacilityItem.jan_code', array('maxlength'=>'13','class'=>'txt validate[optional,custom[onlyNumber]]'));?></td>
                <td></td>
                <th>保険請求区分</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.insurance_claim_type', array('options'=>$insuranceClaimTypes, 'class' => 'txt', 'style'=>'width: 120px; height: 23px;','id'=>'insurance_claim_type_sel','empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code', array('maxlength'=>'20','class'=>'r validate[required] search_upper','id'=>'internal_code'));?></td>
                <td></td>
                <th>旧JANコード</th>
                <td><?php echo $this->form->input('MstFacilityItem.old_jan_code', array('maxlength'=>'13','class'=>'txt validate[optional,custom[onlyNumber]]'));?></td>
                <td></td>
                <th>保険請求コード</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.insurance_claim_code_hidden', array('type'=>'hidden','id'=>'mst_icc_name_hidden_txt')); ?>
                    <?php echo $this->form->input('MstFacilityItem.insurance_claim_code', array('maxlength'=>50,'class'=>'txt','id'=>'icc_code_txt'));?>
                    <input type="button" id="call_icc_btn" value="選択" />
                </td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_name', array('type'=>'text', 'maxlength'=>'100','class'=>'r validate[required] search_canna','width'=>'300px'));?></td>
                <td></td>
                <th>JMDNコード</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.jmdn_code', array('type'=>'hidden','id'=>'mst_jmdn_name_hidden_txt')); ?> <?php echo $this->form->input('MstFacilityItem.jmdn_code', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'jmdn_code_txt','readonly' => true));?>
                    <input type="button" id="call_jmdn_btn" value="選択" />
                </td>
                <td></td>
                <th>保険請求名</th>
                <td><?php echo $this->form->input('MstFacilityItem.insurance_claim_name', array('maxlength'=>50,'class'=>'txt','id'=>'mst_icc_name_txt'));?></td>
            </tr>
            <tr>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_code', array('maxlength'=>'100','class'=>'txt search_upper'));?></td>
                <td></td>
                <th>一般名称</th>
                <td><?php echo $this->form->input('MstFacilityItem.jmdn_name', array('maxlength'=>50,'class'=>'txt','id'=>'mst_jmdn_name_txt','readonly' => true));?></td>
                <td></td>
                <th>償還係数</th>
                <td>
                    <?php echo
                     $this->form->input('MstFacilityItem.refund_converted_num', array('type'=>'text','class'=>'txt num ','id'=>'refund_converted_num'));
                     ?>
                </td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('MstFacilityItem.standard', array('type'=>'text','maxlength'=>'100','class'=>'txt search_canna'));?></td>
                <td></td>
                <th>定価</th>
                <td><?php echo $this->form->input('MstFacilityItem.unit_price', array('type'=>'text', 'maxlength'=>50,'class'=>'txt num validate[optional,custom[onlyDecimal]]' , 'id'=>'UnitPrice'));?></td>
                <td></td>
                <th>償還価格</th>
                <td><?php echo $this->form->input('MstFacilityItem.refund_price', array('type'=>'text', 'maxlength'=>50,'class'=>'txt num validate[optional,custom[onlyDecimal]] refund_price' , 'id'=>'RefundPrice'));?></td>
            </tr>
            <tr>
                <th>販売元</th>
                <td>
                    <?php echo $this->form->input('MstDealer.id', array('type'=>'hidden','id'=>'mst_dealer_id_hidden_txt')); ?>
                    <?php echo $this->form->input('MstDealer.dealer_code', array('type'=>'hidden','id'=>'mst_dealer_code_hidden_txt')); ?>
                    <?php echo $this->form->input('MstDealer.dealer_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'dealer_name_txt','readonly' => true));?>
                    <input type="button" value="選択" id="call_dealer_btn" />
                </td>
                <td></td>
                <th>課税区分</th>
                <td><?php echo $this->form->input('MstFacilityItem.tax_type', array('options'=>$taxes, 'empty'=>true, 'class' => 'txt','style'=>'width: 120px; height: 23px;','div'=>false,'label'=>false)); ?></td>
                <td></td>
                <th>医事コード</th>
                <td><?php echo $this->form->input('MstFacilityItem.medical_code', array('label'=>false,'div'=>false,'maxlength'=>20,'class'=>'txt')); ?>
                <?php echo $this->form->input('MstFacilityItem.enable_medicalcode' , array('type'=>'hidden' , 'value'=>'0' )); ?>
                </td>
            </tr>
            <tr>
                <th>大分類</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_category1', array('options'=>$item_category1 , 'class'=>'txt' , 'id'=>'item_category1' , 'empty'=>true)); ?><br>                    
                </td>
                <td></td>
                <th>クラス分類</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.class_separation_hidden', array('type'=>'hidden','id'=>'class_separation_txt')); ?>
                    <?php echo $this->form->input('MstFacilityItem.class_separation', array('options'=>$classseparations, 'label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'class_separation_sel' ,'style'=>'height: 23px;', 'empty'=>true)); ?>
                </td>
                <td></td>
                <th>薬事承認番号</th>
                <td><?php echo $this->form->input('MstFacilityItem.pharmaceutical_number', array('type'=>'text','maxlength'=>'100','class'=>'txt search_canna'));?></td>
            </tr>
            <tr>
                <th>中分類</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_category2', array('options'=>$item_category2 , 'class'=>'txt' , 'id'=>'item_category2' , 'empty'=>true)); ?><br>
                </td>
                <td></td>
                <th>生物由来区分</th>
                <td><?php echo $this->form->input('MstFacilityItem.biogenous_type', array('options'=>$biogenous_types, 'empty'=>true, 'class'=>'txt','style'=>'width: 120px; height: 23px;')); ?></td>
                <td></td>
                <th>保管区分</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.keep_type', array('options'=>$keep_type , 'class'=>'txt' , 'id'=>'keep_type' , 'empty'=>true)); ?><br>
                </td>
            </tr>
            <tr>
                <th>小分類</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.item_category3', array('options'=>$item_category3 , 'class'=>'txt' , 'id'=>'item_category3' , 'empty'=>true)); ?>
                </td>
                <td></td>
                <th>適用開始日</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.start_date', array('type'=>'text','class'=>'r date validate[required]','id'=>'datepicker1','style'=>'width:135px')); ?>
                </td>
                <td></td>
                <th>適用終了日</th>
                <td>
                    <?php echo $this->form->input('MstFacilityItem.end_date', array('type'=>'text' ,'class'=>'date','id'=>'datepicker2','style'=>'width:135px')); ?>
                    <?php echo $this->form->input('MstFacilityItem.buy_flg', array('type'=>'hidden' , 'value'=> 0)); ?>
                    <?php echo $this->form->input('MstFacilityItem.is_lowlevel', array( 'type'=>'hidden' , 'value'=>0)); ?>
                </td>
            </tr>
        </table>
