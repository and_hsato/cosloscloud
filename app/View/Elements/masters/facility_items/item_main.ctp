    <tr>
        <th>商品ID</th>
        <td><?php echo $this->form->input('MstFacilityItem.internal_code', array('label'=>false,'div'=>false, 'maxlength'=>'20','class'=>'r validate[required] search_upper','id'=>'internal_code'));?></td>
        <td></td>
        <th>製品番号</th>
        <td><?php echo $this->form->input('MstFacilityItem.item_code', array('label'=>false,'div'=>false, 'maxlength'=>'100','class'=>'txt search_upper'));?></td>
        <td></td>
        <th>予備キー１</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key1', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー７</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key7', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー１９</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key19', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
    <tr>
        <th>商品名</th>
        <td><?php echo $this->form->input('MstFacilityItem.item_name', array('type'=>'text','label'=>false,'div'=>false, 'maxlength'=>'100','class'=>'r validate[required] search_canna','width'=>'150px'));?></td>
        <td></td>
        <th>販売元</th>
        <td>
            <?php echo $this->form->input('MstDealer.id', array('type'=>'hidden','id'=>'mst_dealer_id_hidden_txt')); ?>
            <?php echo $this->form->input('MstDealer.dealer_code', array('type'=>'hidden','id'=>'mst_dealer_code_hidden_txt')); ?>
            <?php echo $this->form->input('MstDealer.dealer_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'dealer_name_txt','readonly' => true));?>
            <input type="button" value="選択" id="call_dealer_btn" />
        </td>
        <td></td>
        <th>予備キー２</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key2', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー８</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key8', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー２０</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key20', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
    <tr>
        <th>規格</th>
        <td><?php echo $this->form->input('MstFacilityItem.standard', array('type'=>'text','label'=>false,'div'=>false, 'maxlength'=>'100','class'=>'txt search_canna'));?></td>
        <td></td>
        <th>JANコード</th>
        <td><?php echo $this->form->input('MstFacilityItem.jan_code', array('label'=>false,'div'=>false, 'maxlength'=>'13','class'=>'txt validate[optional,custom[onlyNumber]]'));?></td>
        <td></td>
        <th>予備キー３</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key3', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー９</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key9', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー２１</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key21', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
    <tr>
        <th>保険請求区分</th>
        <td>
            <?php echo $this->form->hidden('MstFacilityItem.insurance_claim_type_hidden',array('id'=>'insurance_claim_type_txt')); ?>
            <?php echo $this->form->input('MstFacilityItem.insurance_claim_type_txt' , array('id'=>'insurance_claim_type_txt_p' , 'style'=>'width: 40px;','class'=>'txt','maxlength'=>'50')) ?>
            <?php echo $this->form->input('MstFacilityItem.insurance_claim_type', array('options'=>$insuranceClaimDepartments, 'class' => 'txt', 'style'=>'width: 120px; height: 23px;','id'=>'insurance_claim_type_sel','empty'=>true)); ?>
        </td>
        <td></td>
        <th>JMDNコード</th>
        <td>
            <?php echo $this->form->input('MstFacilityItem.jmdn_code', array('type'=>'hidden','id'=>'mst_jmdn_name_hidden_txt')); ?> <?php echo $this->form->input('MstFacilityItem.jmdn_code', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'jmdn_code_txt','readonly' => true));?>
            <input type="button" id="call_jmdn_btn" value="選択" />
        </td>
        <td></td>
        <th>予備キー４</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key4', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー１０</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key10', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー２２</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key22', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
    <tr>
        <th>保険請求コード</th>
        <td>
            <?php echo $this->form->input('MstFacilityItem.insurance_claim_code_hidden', array('type'=>'hidden','id'=>'mst_icc_name_hidden_txt')); ?>
            <?php echo $this->form->input('MstFacilityItem.insurance_claim_code', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'icc_code_txt'));?>
            <input type="button" id="call_icc_btn" value="選択" />
        </td>
        <td></td>
        <th>一般名称</th>
        <td><?php echo $this->form->input('MstFacilityItem.jmdn_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'mst_jmdn_name_txt','readonly' => true));?></td>
        <td></td>
        <th>予備キー５</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key5', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー１１</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key11', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー２３</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key23', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
    <tr>
        <th>保険請求略称</th>
        <td><?php echo $this->form->input('MstFacilityItem.insurance_claim_short_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'mst_icc_name_s_txt'));?></td>
        <td></td>
        <th>クラス分類</th>
        <td>
            <?php echo $this->form->input('MstFacilityItem.class_separation_hidden', array('type'=>'hidden','id'=>'class_separation_txt')); ?>
            <?php echo $this->form->input('MstFacilityItem.class_separation', array('options'=>$classseparations, 'label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'class_separation_sel' ,'style'=>'height: 23px;', 'empty'=>false)); ?>
        </td>
        <td></td>
        <th>予備キー６</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key6', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー１２</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key12', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー２４</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key24', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
    <tr>
        <th>保険請求名</th>
        <td><?php echo $this->form->input('MstFacilityItem.insurance_claim_name', array('label'=>false,'div'=>false, 'maxlength'=>50,'class'=>'txt','id'=>'mst_icc_name_txt'));?></td>
        <td></td>
        <th>生物由来区分</th>
        <td><?php echo $this->form->input('MstFacilityItem.biogenous_type', array('options'=>$biogenous_types, 'empty'=>true, 'class'=>'txt','style'=>'width: 120px; height: 23px;')); ?></td>
        <td></td>
        <th>期限切れ警告日数</th>
        <td><?php echo $this->form->input('MstFacilityItem.expired_date', array('type'=>'text', 'maxlength'=>'32','class'=>'txt num' , 'id'=>'expired_date'));?></td>
        <th>予備キー１３</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key13', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー２５</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key25', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
    <tr>
        <th>償還価格</th>
        <td><?php echo $this->form->input('MstFacilityItem.refund_price', array('type'=>'text', 'maxlength'=>50,'class'=>'txt num r refund_price' , 'id'=>'RefundPrice'));?></td>
        <td></td>
        <th>定価単価</th>
        <td><?php echo $this->form->input('MstFacilityItem.unit_price', array('type'=>'text', 'maxlength'=>50,'class'=>'txt num' , 'id'=>'UnitPrice'));?></td>
        <td></td>
        <th>定価</th>
        <td><?php echo $this->form->input('MstFacilityItem.price', array('type'=>'text', 'maxlength'=>50,'class'=>'txt num' , 'id'=>'Price'));?></td>
        <th>予備キー１４</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key14', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー２６</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key26', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
    <tr>
        <th>課税区分</th>
        <td><?php echo $this->form->input('MstFacilityItem.tax_type', array('options'=>$taxes, 'empty'=>true, 'class' => 'txt','style'=>'width: 120px; height: 23px;','div'=>false,'label'=>false)); ?></td>
        <td></td>
        <th>適用開始日</th>
        <td>
            <?php echo $this->form->input('MstFacilityItem.start_date', array('type'=>'text','div'=>false,'label'=>false,'class'=>'r date validate[required]','id'=>'datepicker1')); ?>
        </td>
        <td></td>
        <th>適用終了日</th>
        <td>
            <?php echo $this->form->input('MstFacilityItem.end_date', array('type'=>'text' ,'div'=>false,'label'=>false,'class'=>'date','id'=>'datepicker2')); ?>
        </td>
        <th>予備キー１５</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key15', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー２７</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key27', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
    <tr>
        <th>施主</th>
        <td>
            <?php echo $this->form->input('MstFacilityItem.mst_owner_code_text' , array('class'=>'txt r' ,  'style'=>'width: 60px;' ,'id'=>'owner_code_txt')) ?>
            <?php echo $this->form->input('MstFacilityItem.mst_owner_code', array('options' => $owners,'class' => 'validate[required] r', 'style'=>'width: 120px; height: 23px;','id'=>'owner_code_select' , 'empty'=>true )); ?>
        </td>
        <td></td>
        <th>商品カテゴリ</th>
        <td>
            <?php echo $this->form->input('MstFacilityItem.item_category1', array('options'=>$item_category1 , 'class'=>'txt' , 'id'=>'item_category1' , 'empty'=>true)); ?><br>
            <?php echo $this->form->input('MstFacilityItem.item_category2', array('options'=>$item_category2 , 'class'=>'txt' , 'id'=>'item_category2' , 'empty'=>true)); ?><br>
            <?php echo $this->form->input('MstFacilityItem.item_category3', array('options'=>$item_category3 , 'class'=>'txt' , 'id'=>'item_category3' , 'empty'=>true)); ?>
        </td>
        <td></td>
        <th>備考</th>
        <td><?php echo $this->form->input('MstFacilityItem.recital', array('label'=>false,'div'=>false,'maxlength'=>200,'class'=>'txt')); ?></td>
        <th>予備キー１６</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key16', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー２８</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key28', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
    <tr>
        <th>集計科目</th>
        <td><?php echo $this->form->input('MstFacilityItem.mst_accountlist_id',array('options'=>$accountlists,'empty'=>true,'class'=>'txt','style'=>'width: 120px; height: 23px;')); ?></td>
        <td>&nbsp;</td>
        <th>医事コード</th>
        <td><?php echo $this->form->input('MstFacilityItem.medical_code', array('label'=>false,'div'=>false,'maxlength'=>20,'class'=>'txt')); ?></td>
        <td>&nbsp;</td>
        <th>医事シール発行</th>
        <td><?php echo $this->form->input('MstFacilityItem.sealissue_medicalcode' , array('hiddenField'=>false , 'checked' => (isset($this->request->data['MstFacilityItem']['sealissue_medicalcode'])?$this->request->data['MstFacilityItem']['sealissue_medicalcode']:false) )); ?>医事シール発行する</td>
        <th>予備キー１７</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key17', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー２９</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key29', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
    <tr>
        <th>入庫時医事シール発行</th>
        <td><?php echo $this->form->input('MstFacilityItem.enable_medicalcode' , array('hiddenField'=>false , 'checked' => (isset($this->request->data['MstFacilityItem']['enable_medicalcode'])?$this->request->data['MstFacilityItem']['enable_medicalcode']:false) )); ?>医事シール発行する</td>
        <td></td>
        <th>自動入庫</th>
        <td><?php echo $this->form->input('MstFacilityItem.is_auto_storage', array('hiddenField'=>false, 'checked' => (isset($this->request->data['MstFacilityItem']['is_auto_storage'])?$this->request->data['MstFacilityItem']['is_auto_storage']:false) )); ?>自動入庫する</td>
        <td></td>
        <th>LOT/UBD警告</th>
        <td><?php echo $this->form->input('MstFacilityItem.lot_ubd_alert', array('options'=>$lotubd, 'empty'=>true, 'class' => 'validate[required] r','style'=>'width: 120px; height: 23px;','div'=>false,'label'=>false)); ?></td>
        <th>予備キー１８</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key18', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
        <th>予備キー３０</th>
        <td><?php echo $this->form->input('MstFacilityItem.spare_key30', array('label'=>false,'div'=>false, 'maxlength'=>'50','class'=>'txt'));?></td>
    </tr>
