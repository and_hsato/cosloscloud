<script type="text/javascript">
$(document).ready(function() {
<?php if (isset($type) && $type == 'important') : ?> 
  jGrowlTheme('mono', '重要', '<?php echo $this->session->read('Message.flash.message');?>', '<?php echo $this->webroot; ?>images/important.jpg'); 
<?php elseif (isset($type) && $type == 'error') : ?>
  jGrowlTheme('mono', 'エラー', '<?php echo $this->session->read('Message.flash.message');?>', '<?php echo $this->webroot; ?>images/error.jpg');
<?php elseif (isset($type) && $type == 'help') : ?>
  jGrowlTheme('mono', 'ヘルプ', '<?php echo $this->session->read('Message.flash.message');?>', '<?php echo $this->webroot; ?>images/help.jpg');
<?php elseif (isset($type) && $type == 'search') : ?>
  jGrowlTheme('mono', '検索', '<?php echo $this->session->read('Message.flash.message');?>', '<?php echo $this->webroot; ?>images/search.jpg');
<?php elseif (isset($type) && $type == 'timer') : ?>
  jGrowlTheme('mono', 'しばらくお待ちください...', '<?php echo $this->session->read('Message.flash.message');?>', '<?php echo $this->webroot; ?>images/timer.jpg');
<?php elseif (isset($type) && $type == 'stop') : ?>
  jGrowlTheme('mono', '停止', '<?php echo $this->session->read('Message.flash.message');?>', '<?php echo $this->webroot; ?>images/stop.jpg');
<?php elseif (isset($type) && $type == 'checked') : ?>
  jGrowlTheme('mono', '正常終了', '<?php echo $this->session->read('Message.flash.message');?>', '<?php echo $this->webroot; ?>images/checked.jpg');
<?php elseif (isset($type) && $type == 'star') : ?>
  jGrowlTheme('mono', 'お知らせ', '<?php echo $this->session->read('Message.flash.message');?>', '<?php echo $this->webroot; ?>images/star.jpg');
<?php endif ?>
});
</script>