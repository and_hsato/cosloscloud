<?php
if( !isset($page_type))
  $page_type = 'default';
?>

<table class="FormStyleTable">
    <colgroup>
        <col>
        <col>
        <col width="20">
        <col>
        <col>
    </colgroup>
    <tr>
        <th>施主<?php if( $page_type == 'input'):?><?php endif; ?></th>
        <td>
            <?php if( $page_type == 'input'):?>
            <?php echo $this->form->input('TrnRetroactHeader.mst_facility_code',array('id' => 'facility_code', 'label' => false, 'div' => false, 'class' => 'r facility_code', 'style'=>'width: 60px;')); ?> <?php echo $this->form->input('TrnRetroactHeader.mst_facility',array('options'=>$facilities,'empty'=>true,'class'=>' txt r validate[required]','id'=>'facility_select')); ?>
            <?php else: ?>
            <?php echo $this->form->input('TrnRetroactHeader.mst_facility_code',array('type' => 'hidden')); ?>
            <?php echo $this->form->input('TrnRetroactHeader.mst_facility',array('type'=>'hidden')); ?>
            <?php echo $this->form->input('TrnRetroactHeader.mst_facility_name',array('id' => 'user_facility_txt', 'label' => false, 'div' => false, 'class' => 'lbl', 'style'=>'width: 180px;' , 'readonly' =>true)); ?>
            <?php endif; ?>
        </td>
        <td></td>
        <th>作業区分</th>
        <td>
            <?php if( $page_type == 'input'):?>
            <?php echo $this->form->input('TrnRetroactHeader.work_class',array('options'=>$work_classes,'empty'=>true,'class'=>' txt','id'=>'work_class')); ?>
            <?php else: ?>
            <?php echo $this->form->input('TrnRetroactHeader.work_class',array('type'=>'hidden')); ?>
            <?php echo $this->form->input('TrnRetroactHeader.work_class_txt',array('class'=>'lbl','readonly'=>'readonly')); ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <th>
            <?php echo isset($retroact_type) && $retroact_type == 1 ? '仕入先' : '得意先'; ?>
            <?php if( $page_type == 'input'):?>
            <?php endif; ?>
        </th>
        <td>
            <?php if( $page_type == 'input'):?>
            <?php echo $this->form->input('TrnRetroactHeader.facility_to_code',array('id' => 'customer_code', 'label' => false, 'div' => false, 'class' => 'r customer_code', 'style'=>'width: 60px;')); ?> <?php echo $this->form->input('TrnRetroactHeader.facility_to',array('options'=>$customers,'empty'=>true,'class'=>' txt r validate[required]','id'=>'customer_select')); ?>
            <?php else: ?>
            <?php echo $this->form->input('TrnRetroactHeader.facility_to_code',array('type' => 'hidden')); ?> <?php echo $this->form->input('TrnRetroactHeader.facility_to',array('type'=>'hidden')); ?> <?php echo $this->form->input('TrnRetroactHeader.facility_to_name',array('label' => false, 'div' => false, 'class' => 'lbl', 'style'=>'width: 180px;' , 'readonly' =>true)); ?>
            <?php endif; ?>
        </td>
        <td></td>
        <th>備考</th>
        <td>
            <?php if( $page_type == 'input'):?>
            <?php echo $this->form->input('TrnRetroactHeader.recital',array('maxlength'=>50,'class'=>'txt')); ?>
            <?php else: ?>
            <?php echo $this->form->input('TrnRetroactHeader.recital',array('class'=>'lbl','readonly'=>'readonly','maxlength' => '200')); ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <th>遡及期間 <?php if( $page_type == 'input'):?><?php endif; ?></th>
        <td>
            <?php if( $page_type == 'input'):?>
            <?php echo $this->form->input('TrnRetroactHeader.start_date',array('maxlength'=>50,'type'=>'text','class'=>'r date validate[required,custom[date],funcCall2[date2]]','id'=>'datepicker1')); ?>～ <?php echo $this->form->input('TrnRetroactHeader.end_date',array('maxlength'=>50,'type'=>'text','class'=>'r date validate[required,custom[date],funcCall2[date2]]','id'=>'datepicker2')); ?>
            <?php else: ?>
            <?php echo $this->form->input('TrnRetroactHeader.start_date',array('type' => 'text','class'=>'lbl','readonly'=>'readonly')); ?>
             ～
            <?php echo $this->form->input('TrnRetroactHeader.end_date',array('type' => 'text','class'=>'lbl','readonly'=>'readonly')); ?>
            <?php endif; ?>
        </td>
        <td></td>
    </tr>
    <?php if ($page_type == 'reflect_with_master'):?>
    <tr>
        <th>適用開始日</th>
        <td><?php echo $this->form->input($table_type['table_alias'].'.start_date',array('maxlength'=>'50', 'type'=>'text', 'class'=>'date r validate[required,custom[date],funcCall2[date2]]', 'id'=>'datepicker1')); ?></td>
        <td></td>
    </tr>
    <?php elseif ($page_type == 'reflect_with_master_confirm'):?>
    <tr>
        <th>適用開始日</th>
        <td><input name="data[<?php echo $table_type['table_alias'] ?>][start_date]" maxlength="50" type="text" class="lbl" value="<?php echo $this->request->data[$table_type['table_alias']]['start_date']; ?>" /></td>
        <td></td>
    </tr>
    <?php endif; ?>
</table>
