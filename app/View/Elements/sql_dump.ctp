<?php
/**
 * SQL Dump element. Dumps out SQL log information
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Elements
 * @since         CakePHP(tm) v 1.3
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<script type="text/javascript">
$(document).ready(function(){
    $('.cake-sql-log').find('tr.hideTr').hide();
    $('#cmdHide').hide();

    // システムSQL 表示クリック
    $('#cmdShow').click(function(){
        $('.cake-sql-log').find('tr.hideTr').show();
        $('#cmdShow').hide();
        $('#cmdHide').show();
    });

    // システムSQL 非表示クリック
    $('#cmdHide').click(function(){
        $('.cake-sql-log').find('tr.hideTr').hide();
        $('#cmdShow').show();
        $('#cmdHide').hide();
    });

});
</script>
<?php
if (!class_exists('ConnectionManager') || Configure::read('debug') < 2) {
    return false;
}
$noLogs = !isset($sqlLogs);
if ($noLogs):
    $sources = ConnectionManager::sourceList();

    $sqlLogs = array();
    foreach ($sources as $source):
        $db = ConnectionManager::getDataSource($source);
        if (!method_exists($db, 'getLog')):
            continue;
        endif;
        $sqlLogs[$source] = $db->getLog();
    endforeach;
endif;

if ($noLogs || isset($_forced_from_dbo_)):
    foreach ($sqlLogs as $source => $logInfo):
        $text = $logInfo['count'] > 1 ? 'queries' : 'query';
        printf(
            '<table class="cake-sql-log" id="cakeSqlLog_%s" summary="Cake SQL Log" cellspacing="0">',
            preg_replace('/[^A-Za-z0-9_]/', '_', uniqid(time(), true))
        );
        printf('<caption>(%s) %s %s took %s ms</caption>', $source, $logInfo['count'], $text, $logInfo['time']);
    ?>
    <thead>
        <tr><th>Nr</th><th>Query</th><th>Error</th><th>Affected</th><th>Num. rows</th><th>Took (ms)</th></tr>
    </thead>
    <tbody>
    <?php
                // システムSQLのため非表示対象
                //$patterns[] = "/FROM\sINFORMATION_SCHEMA\.tables\sWHERE/i";
                //$patterns[] = "/^SELECT\sDISTINCT(.*)FROM(\s*)information_schema\.columns(\s*)WHERE(.*)ORDER\sBY\sposition$/i";
                //$patterns[] = "/^SELECT\s\"session\"\.\"id\"(.+)FROM\s\"cake_sessions\"\sAs\s\"session\"(\s+)WHER(.+)(LIMIT\s1)?$/i";
                //$patterns[] = "/^SELECT\sCOUNT\(\*\)\sAs\s\"count\"\sFROM\s\"cake_sessions\"\sAs\s\"session\"(\s+)WHERE(\s+)\"session\"\.\"id\"(.*)$/i";
                //$patterns[] = "/^UPDATE\s\"cake_sessions\"\sSET(.*)/i";
                //$patterns[] = "/^DELETE(\s+)FROM(\s+)\"cake_sessions\"(\s+)WHERE(\s+)\"cake_sessions\"\.\"id\"(\s+)\=(.*)/i";
                //$patterns[] = "/^INSERT\s+INTO\s+\"cake_sessions\"(.+)/i";
                //$patterns[] = "/^SELECT currval\(\'.*\'\) as max$/i";
      $patterns[] = "/^SELECT.*cake_sessions.*$/i";
      $patterns[] = "/^SELECT.*information_schema.*$/i";
      $patterns[] = "/^SELECT.*currval.*$/i";
                
        foreach ($logInfo['log'] as $k => $i) :

            $hideName = '';
            $i += array('error' => '');
            if (!empty($i['params']) && is_array($i['params'])) {
                $bindParam = $bindType = null;
                if (preg_match('/.+ :.+/', $i['query'])) {
                    $bindType = true;
                }
                foreach ($i['params'] as $bindKey => $bindVal) {
                    if ($bindType === true) {
                        $bindParam .= h($bindKey) . " => " . h($bindVal) . ", ";
                    } else {
                        $bindParam .= h($bindVal) . ", ";
                    }
                }
                $i['query'] .= " , params[ " . rtrim($bindParam, ', ') . " ]";

            }
      
                    foreach($patterns As $pattern){
                        $query = $i['query'];
                        $query = preg_replace('/\n/' , ' ' , $query);
                        $query = preg_replace('/\t/' , ' ' , $query);

                        if ( preg_match($pattern,h($query)) ) {
                            $hideName = "hideTr";
                            break;
                        }
                    }      
            printf('<tr class="' . $hideName . '"><td>%d</td><td>%s</td><td>%s</td><td style="text-align: right">%d</td><td style="text-align: right">%d</td><td style="text-align: right">%d</td></tr>%s',
                $k + 1,
                h($i['query']),
                $i['error'],
                $i['affected'],
                $i['numRows'],
                $i['took'],
                "\n"
            );
        endforeach;
    ?>
    </tbody></table>
    <?php
    endforeach;
else:
    printf('<p>%s</p>', __d('cake_dev', 'Encountered unexpected %s. Cannot generate SQL log.', '$sqlLogs'));
endif;
  ?>
    <INPUT TYPE="button" VALUE="システムSQL 表示" id="cmdShow">
    <INPUT TYPE="button" VALUE="システムSQL 非表示" id="cmdHide">
    <style type="text/css">
        /*sql-dump DEBUG*/
        .cake-sql-log table {
            background: #f4f4f4;
        }
        .cake-sql-log td {
            padding: 4px 8px;
            text-align: left;
            font-family: Monaco, Consolas, "Courier New", monospaced;
        }
        .cake-sql-log caption {
            color:#fff;
        }
    </style>