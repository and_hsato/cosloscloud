<script>
$(document).ready(function(){
    //検索ボタン押下
    $("#btn_Search").click(function(){
        validateDetachSubmit($("#DealersList") , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/dealers_list');
    });

    //新規ボタン押下
    $("#btn_Add").click(function(){
        validateDetachSubmit($('#DealersList') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add' );
    });

    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $('#DealersList').validationEngine('detach');
        $("#DealersList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
    });
  
    //編集ボタン押下
    $("#btn_Mod").click(function(){
        $("#DealersList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/mod').submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li>販売元一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">販売元一覧</h2>

<form class="validate_form search_form" method="post" action="" accept-charset="utf-8" id="DealersList">
    <?php echo $this->form->input('MstDealer.is_search' , array('type'=>'hidden'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
            </colgroup>
            <tr>
                <th>販売元コード</th>
                <td><?php echo $this->form->input('MstDealer.search_dealer_code' , array('type'=>'text' , 'class'=>'txt'))?></td>
                <td></td>
                <td>
                    <?php echo $this->form->input('search_is_deleted',array('type'=>'checkbox', 'hiddenField'=>false)); ?>削除も表示する
                </td>
            </tr>
            <tr>
                <th>販売元名</th>
                <td><?php echo $this->form->input('MstDealer.search_dealer_name' , array('type'=>'text' , 'class'=>'txt search_canna'))?></td>
                <td></td>
            </tr>
            <tr>
                <th>登録区分</th>
                <td>
                    <?php echo $this->form->input('MstDealer.search_dealer_type' , array('options'=>Configure::read('DealerType') , 'class'=>'txt' , 'empty'=>'' )); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search" />
        <input type="button" class="btn btn11 [p2]" id="btn_Add" />
        <input type="button" class="btn btn5" id="btn_Csv" />
    </div>
    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($Dealers_List))); ?>
            </span>
            <span class="BikouCopy"></span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col />
                    <col />
                    <col width="50"/>
                </colgroup>
                <thead>
                <tr>
                    <th></th>
                    <th>販売元コード</th>
                    <th>販売元名</th>
                    <th>登録区分</th>
                    <th>削除</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($Dealers_List as $dealers) { ?>
                    <tr>
                        <td class="center">
                        <?php
                            //販売元区分が独自のときのみラジオボタン表示（=編集可能）
                            if($dealers['MstDealer']['dealer_type'] == Configure::read('DealerTypes.cooperation')){ ?>
                            <input name="data[MstDealer][id]" type="radio" class="validate[required]" id="dealer<?php echo $dealers['MstDealer']['id']; ?>" value="<?php echo $dealers['MstDealer']['id']; ?>"/>
                        <?php } ?>
                        </td>
                        <td><?php echo h_out($dealers['MstDealer']['dealer_code']); ?></td>
                        <td><?php echo h_out($dealers['MstDealer']['dealer_name']); ?></td>
                        <td><?php echo h_out(Configure::read('DealerType.'.$dealers['MstDealer']['dealer_type'])); ?></td>
                        <td><?php echo h_out(($dealers['MstDealer']['is_deleted'] == TRUE) ? "○" : "" , 'center');  ?></td>
                    </tr>
                <?php } ?>
                </tbody>
                <?php if(count($Dealers_List) == 0 && isset($this->request->data['MstDealer']['is_search'])){ ?>
                <tr><td colspan="5" class="center">該当するデータがありませんでした。</td></tr>
                <?php } ?>
            </table>
        </div>
    </div>
    <?php if(count($Dealers_List)>0){ ?>
    <div class="ButtonBox">
        <input type="button" class="btn btn9" id="btn_Mod" />
    </div>
    <?php } ?>
</form>
