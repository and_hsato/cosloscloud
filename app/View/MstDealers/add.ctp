<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //チェックOKだったら、送信する
        validateDetachSubmit($('#AddForm') , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/result' );
    }
}

$(document).ready(function(){
    $("#AddForm").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/dealers_list">販売元一覧</a></li>
        <li>販売元編集</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">販売元編集</h2>

<form class="input_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/submitCheckDealerCode" id="AddForm">
    <input type="hidden" name="mode" value="add" />
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>販売元コード</th>
                <td>
                    <?php echo $this->form->input('MstDealer.dealer_code' , array('type'=>'text' , 'class'=>'txt r validate[required,ajax[ajaxDealerCode]]' , 'maxlength'=>20))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>販売元名</th>
                <td>
                    <?php echo $this->form->input('MstDealer.dealer_name' , array('type'=>'text' , 'class'=>'txt r validate[required] search_canna' , 'maxlength'=>80))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>有効期間</th>
                <td>
                    <?php echo $this->form->input('MstDealer.start_date' , array('type'=>'text' , 'class'=>'date txt r validate[required,custom[date]]' , 'id'=>'datepicker1'))?>
                     ～
                    <?php echo $this->form->input('MstDealer.end_date' , array('type'=>'text' , 'class'=>'date txt validate[optional,custom[date]]' , 'id'=>'datepicker2'))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="submit" class="btn btn2 [p2]" value="" />
    </div>
</form>
