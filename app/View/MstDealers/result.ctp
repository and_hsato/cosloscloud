<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?>masters">マスタメンテ</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/dealers_list">販売元一覧</a></li>
        <li class="pankuzu">販売元編集</li>
        <li>販売元編集結果</li>
   </ul>
</div>
<h2 class="HeaddingLarge"><span>マスタメンテナンス</span></h2>
<h2 class="HeaddingMid">販売元編集</h2>

<?php if(isset($this->request->data['MstDealer']['is_deleted'])) {  ?>
<div class="Mes01">データを削除しました</div>
<?php }else{ ?>
<div class="Mes01">以下の内容で設定しました</div>
<?php } ?>

<form method="post" action="" accept-charset="utf-8" >
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>販売元コード</th>
                <td>
                    <?php echo $this->form->input('MstDealer.dealer_code' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                 </td>
                <td></td>
            </tr>
            <tr>
                <th>販売元名</th>
                <td>
                    <?php echo $this->form->input('MstDealer.dealer_name' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>有効期限</th>
                <td>
                    <?php echo $this->form->input('MstDealer.start_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                    ～
                    <?php echo $this->form->input('MstDealer.end_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</form>
