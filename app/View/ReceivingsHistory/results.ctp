<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">入荷履歴</a></li>
        <li class="pankuzu">入荷履歴確認</li>
        <li>入荷履歴結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>入荷履歴結果</span></h2>
<div class="Mes01">以下の商品の入荷取消を行いました。</div>

<div class="SearchBox">
    <div class="TableScroll2">
        <table class="TableStyle01 table-even2" border=0>
            <thead>
            <tr>
                <th width="135">入荷番号</th>
                <th width="95">入荷日</th>
                <th class="col10">商品ID</th>
                <th class="col10">商品名</th>
                <th class="col20">製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col5">入荷数</th>
                <th class="col10">作業区分</th>
                <th class="col10">更新者</th>
            </tr>
            <tr>
                <th>発注番号</th>
                <th>仕入先</th>
                <th>区分</th>
                <th>規格</th>
                <th>販売元</th>
                <th>仕入単価</th>
                <th>残数</th>
                <th colspan="2">備考</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=0;foreach($SearchResult as $_row): ?>
            <tr>
                <td><?php echo h_out($_row['work_no'],'center'); ?></td>
                <td><?php echo h_out($_row['work_date'],'center'); ?></td>
                <td><?php echo h_out($_row['internal_code'] ,'center'); ?></td>
                <td><?php echo h_out($_row['item_name']); ?></td>
                <td><?php echo h_out($_row['item_code']); ?></td>
                <td><?php echo h_out($_row['packing_name']); ?></td>
                <td><?php echo h_out($_row['quantity'],'right'); ?></td>
                <td><?php echo h_out($_row['work_type_name']); ?></td>
                <td><?php echo h_out($_row['modifier_name']); ?></td>
            </tr>
            <tr>
                <td><?php echo h_out($_row['trn_order_work_no'],'center'); ?></td>
                <td><?php echo h_out($_row['facility_name']); ?></td>
                <td><?php echo h_out($_row['order_type_name'],'center'); ?></td>
                <td><?php echo h_out($_row['standard']); ?></td>
                <td><?php echo h_out($_row['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($_row['stocking_price']),'right'); ?></td>
                <td><?php echo h_out($_row['remain_count'],'right'); ?></td>
                <td colspan="2"><?php echo h_out($_row['recital']); ?></td>
            </tr>
            <?php $i++;endforeach; ?>
            </tbody>
        </table>
    </div>
</div>