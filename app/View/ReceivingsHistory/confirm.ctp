<script type="text/javascript">
    $(document).ready(function(){
        $('.checkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
        });

        $('#cmdCancel').click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length === 0){
                alert('取消を行う明細を選択してください');
                return false;
            }else{
                if(window.confirm("取消を実行します。よろしいですか？")){
                $('#trn_receiving_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/results').submit();
                }
            }
        });
    });
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">入荷履歴</a></li>
        <li>入荷履歴編集</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>入荷履歴確認</span></h2>
<h2 class="HeaddingMid">内容に問題が無ければ、確定ボタンをクリックしてください。</h2>

<?php echo $this->form->create('TrnReceiving', array('action' => $this->webroot . 'receivings_history/results', 'type' => 'post', 'id' => 'trn_receiving_form', 'class' => 'validate_form')); ?>
    <?php echo $this->form->input('TrnReceiving.token' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('TrnReceiving.time' , array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s')))?>
    <div class="SearchBox">
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2">
                <thead>
                <tr>
                    <th style="width:20px;" rowspan="2"><input type="checkbox" class="checkAll"/></th>
                    <th>入荷番号</th>
                    <th width="95">入荷日</th>
                    <th class="col10">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col10">製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col5">入荷数</th>
                    <th class="col10">作業区分</th>
                    <th class="col10">更新者</th>
                </tr>
                <tr>
                    <th>発注番号</th>
                    <th>仕入先</th>
                    <th>区分</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>仕入単価</th>
                    <th>残数</th>
                    <th colspan="2">備考</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=0;foreach($SearchResult as $_row): ?>
                <tr>
                    <td rowspan="2" class="center">
                        <?php if(true == $_row['IsSelectable']): ?>
                            <?php echo $this->form->checkbox("TrnReceiving.Rows.{$_row['id']}.selected", array('class'=>'center checkAllTarget')); ?>
                        <?php endif; ?>
                    </td>
                    <td><?php echo h_out($_row['work_no'],'center'); ?></td>
                    <td><?php echo h_out($_row['work_date'],'center'); ?></td>
                    <td><?php echo h_out($_row['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($_row['item_name']); ?></td>
                    <td><?php echo h_out($_row['item_code']); ?></td>
                    <td><?php echo h_out($_row['packing_name']); ?></td>
                    <td><?php echo h_out($_row['quantity'],'right'); ?></td>
                    <td><?php echo h_out($_row['work_type_name']); ?></td>
                    <td><?php echo h_out($_row['modifier_name']); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($_row['trn_order_work_no'],'center'); ?></td>
                    <td><?php echo h_out($_row['facility_name']); ?></td>
                    <td><?php echo h_out($_row['order_type_name'],'center'); ?></td>
                    <td><?php echo h_out($_row['standard']); ?></td>
                    <td><?php echo h_out($_row['dealer_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($_row['stocking_price']),'right'); ?></td>
                    <td><?php echo h_out($_row['remain_count'],'right'); ?></td>
                    <td colspan="2"><?php echo h_out($_row['recital']); ?></td>
                </tr>
                <?php $i++;endforeach; ?>
                </tbody>
            </table>
        </div>

        <?php if($IsShowFinal != false): ?>
        <div class="ButtonBox">
            <input type="button" class="btn btn6 [p2]" id="cmdCancel" />
        </div>
        <?php endif; ?>
    </div>
<?php echo($this->form->end()); ?>