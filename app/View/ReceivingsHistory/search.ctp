<script type="text/javascript">
    $(document).ready(function(){

        $('#search').click(function(){
            $('#search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').submit();
        });

        //CSVボタン押下
        $("#btn_Csv").click(function(){
            $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/').submit();
            $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search');
        });

        $('.checkAll').click(function(){
            $('input[type=checkbox].checkAllTarget').attr('checked', $(this).attr('checked'));
        });
        $('#cmdConfirm').click(function(){
            if($('input[type=checkbox].checkAllTarget:checked').length == 0){
                alert('変更を行いたい入荷履歴を選択してください');
                return false;
            }else{
                $('#search_form').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm').submit();
            }
        });
    });

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>入荷履歴</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>入荷履歴</span></h2>
<form id="search_form" method="POST" class="validate_form search_form" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/search">
    <input type="hidden" name="data[IsSearch]" value="1" />
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="60" align="right"/>
            </colgroup>
            <tr>
                <th>発注番号</th>
                <td><?php echo($this->form->text('TrnOrder.work_no', array('class' => 'txt', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>発注日</th>
                <td>
                    <?php echo($this->form->text('TrnOrder.work_date_from', array('class' => 'date txt validate[optional,custom[date]]', 'id' => 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo($this->form->text('TrnOrder.work_date_to', array('class' => 'date txt validate[optional,custom[date]]', 'id' => 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                </td>
                <td></td>
                <td colspan="2"><?php echo($this->form->checkbox('showDeleted', array('label' => false))); ?>取消も表示する</td>
            </tr>
            <tr>
                <th>入荷番号</th>
                <td><?php echo($this->form->text('TrnReceivingHeader.work_no', array('class' => 'txt', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>入荷日</th>
                <td>
                    <?php echo($this->form->text('TrnReceivingHeader.work_date_from', array('class' => 'date txt validate[optional,custom[date]]', 'id' => 'datepicker3', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo($this->form->text('TrnReceivingHeader.work_date_to', array('class' => 'date txt validate[optional,custom[date]]', 'id' => 'datepicker4', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo($this->form->input('MstFacility.facility_name', array('type'=>'hidden','id'=>'supplierName'))); ?>
                    <?php echo($this->form->input('FacilityCodeInput', array('type'=>'text','class' =>'txt', 'id'=>'supplierText','style' => 'width:60px;text-align:right;'))); ?>
                    <?php echo($this->form->input('MstFacility.facility_code', array('type'=>'select' , 'id'=>'supplierCode','options'=>$facilities, 'empty'=>true, 'class' => 'txt'))); ?>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo($this->form->text('MstFacilityItem.internal_code', array('class' => 'txt search_canna search_internal_code', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo($this->form->text('MstFacilityItem.item_code', array('class' => 'txt search_upper', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>発注区分</th>
                <td><?php echo $this->form->input('TrnOrder.order_type', array('type'=>'select','options'=>$orderTypes, 'empty'=>true , 'class' => 'txt', 'id' => 'orderTypeSel')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo($this->form->text('MstFacilityItem.item_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo($this->form->text('MstDealer.dealer_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('TrnReceiving.work_type', array('type'=>'select' , 'options'=>$classes, 'empty' => true , 'class' => 'txt', 'id' => 'classTypeSel')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo($this->form->text('MstFacilityItem.standard', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo($this->form->text('MstFacilityItem.jan_code', array('class' => 'txt', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo($this->form->checkbox('hasDifference', array('label' => false))); ?>金額に差異があるもの</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>
    </div>

    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn1" id="search" />
            <input type="button" class="btn btn5" id="btn_Csv"/>
        </p>
    </div>
    <div id="results">
        <?php echo $this->element('limit_combobox', array('result' => count($SearchResult))); ?>
        <div class="TableScroll">
            <table class="TableStyle01 table-even">
                <thead>
                <tr>
                    <th style="width:20px;"><input type="checkbox" class="checkAll" /></th>
                    <th>入荷番号</th>
                    <th style="width:85px;">入荷日</th>
                    <th class="col20">仕入先</th>
                    <th class="col10">登録者</th>
                    <th>登録日時</th>
                    <th class="col20">備考</th>
                    <th class="col10">件数</th>
                </tr>
                </thead>
                <tbody>
                <?php if(count($SearchResult) === 0 && isset($data["IsSearch"]) && $data["IsSearch"] === "1"){ ?>
                <tr><td colspan="8" class="center"><span>該当するデータが存在しません。</span></td></tr>
                <?php } ?>
                <?php $i = 0;  foreach ($SearchResult as $_row): ?>
                <tr>
                    <td align="center"><?php echo $this->form->checkbox("TrnReceivingHeader.Rows.{$_row['trn_receiving_header_id']}", array('class' => 'center checkAllTarget',)); ?></td>
                    <td><?php echo h_out($_row['work_no'],'center'); ?></td>
                    <td><?php echo h_out(date('Y/m/d', strtotime($_row['work_date'])),'center'); ?></td>
                    <td><?php echo h_out($_row['facility_name']); ?></td>
                    <td><?php echo h_out($_row['user_name']); ?></td>
                    <td><?php echo h_out(date('Y/m/d H:i:s', strtotime($_row['created'])),'center'); ?></td>
                    <td><?php echo h_out($_row['recital']); ?></td>
                    <td><?php echo h_out($_row['hit'].' ／ '.$_row['detail_count'] ,'right'); ?></td>
                </tr>
                <?php $i++; endforeach; ?>
                </tbody>
            </table>
        </div>
        <?php if (count($SearchResult) > 0): ?>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" id="cmdConfirm" class="btn btn7" />
            </p>
        </div>
        <?php endif; ?>
    </div>
</form>