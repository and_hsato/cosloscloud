  <div id="TopicPath">
    <ul>
      <li><a href="<?php echo $this->webroot; ?>login/home">TOP</a></li>
      <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">直納要求受領</a></li>
      <li class="pankuzu">受領予定一覧</a></li>
      <li class="pankuzu">受領確認</li>
      <li>受領結果</li>
    </ul>
  </div>
<h2 class="HeaddingLarge"><span>受領結果</span></h2>
<div class="Mes01">受領登録が完了しました</div>
<form class="validate_form" method="post" accept-charset="utf-8" id="ShippingsList">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>受領日</th>
                <td>
                    <?php echo $this->form->input('d2receipts.work_date' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('d2receipts.className' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('d2receipts.classId' , array('type'=>'hidden' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('d2receipts.facilityName' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('d2receipts.facilityId' , array('type'=>'hidden' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('d2receipts.recital.0' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('d2receipts.departmentName' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                    <?php echo $this->form->input('d2receipts.departmentId' , array('type'=>'hidden' , 'class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="results">
        <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect"></span>
            <span class="BikouCopy">
            </span>
        </div>
        <div class="SelectBikou_Area" id="page_top">
            <span class="DisplaySelect"> 
            表示件数：<?php echo count($result) ?>件
            </span>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-odd2">
                <colgroup>
                    <col width="25" />
                    <col />
                    <col  width="80" />
                    <col  width="80" />
                    <col />
                    <col />
                    <col  width="85" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th rowspan="2"></th>
                    <th>出荷番号</th>
                    <th>出荷日</th>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>ロット番号</th>
                    <th>作業区分</th>
                    <th>更新者</th>
                </tr>
                <tr>
                    <th>施設</th>
                    <th>部署</th>
                    <th>包装単位</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>有効期限</th>
                    <th>数量</th>
                    <th>備考</th>
                </tr>
                <?php
                    $cnt=0;
                    foreach ($result as $r) {
                ?>
                <tr>
                    <td rowspan="2" class="center"></td>
                    <td><?php echo h_out($r['d2receipts']['work_no']); ?></td>
                    <td><?php echo h_out($r['d2receipts']['work_date'],'center') ; ?></td>
                    <td><?php echo h_out($r['d2receipts']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['d2receipts']['item_name']); ?></td>
                    <td><?php echo h_out($r['d2receipts']['item_code']); ?></td>
                    <td><?php echo h_out($r['d2receipts']['lot_no']);  ?></td>
                    <td></td>
                    <td><?php echo h_out($r['d2receipts']['user_name']); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($r['d2receipts']['facility_name']); ?></td>
                    <td><?php echo h_out($r['d2receipts']['department_name']); ?></td>
                    <td><?php echo h_out($r['d2receipts']['unit_name']); ?></td>
                    <td><?php echo h_out($r['d2receipts']['standard']); ?></td>
                    <td><?php echo h_out($r['d2receipts']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['d2receipts']['validated_date'],'center') ?></td>
                    <td><?php echo h_out($r['d2receipts']['quantity'],'right'); ?></td>
                    <td><?php echo $this->form->input('d2receipts.recital.'.$r['d2receipts']['id'] , array('class'=>'txt recital' , 'style'=>'width:90%;','maxlength'=>'200'))?></td>
                </tr>
            <?php $cnt++;} ?>
            </table>
        </div>
    </div>
</form>
