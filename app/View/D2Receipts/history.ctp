<script type="text/javascript">
$(document).ready(function(){

    //検索ボタン押下
    $("#btn_Search").click(function(){
        $("#historyForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history/');
        $("#historyForm").attr('method', 'post');
        $("#historyForm").submit();
    });

    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $("#historyForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/');
        $("#historyForm").attr('method', 'post');
        $("#historyForm").submit();
        $("#historyForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history/');
    });
    
    //明細ボタン押下
    $("#btn_Confirm").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0){
            $("#historyForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history_confirm/');
            $("#historyForm").attr('method', 'post');
            $("#historyForm").submit();
        }else{
            alert('明細を確認する項目をチェックしてください。');
        }
    });
    //チェックボックス一括制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li>直納要求作成履歴</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>直納要求作成履歴</span></h2>
<h2 class="HeaddingMid">直納要求作成履歴</h2>
<form class="validate_form search_form input_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history" id="historyForm">
<?php echo $this->form->input('d2receipts.is_search',array('type' => 'hidden','id' => 'is_search')); ?>
<?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="60" align="right"/>
            </colgroup>
            <tr>
                <th>要求番号</th>
                <td><?php echo $this->form->input('d2receipts.work_no',array('class' => 'txt','style' => 'width:110px'));?></td>
                <td></td>
                <th>要求日</th>
                <td>
                    <?php echo $this->form->input('d2receipts.work_date_from',array('class'=>'validate[optional,custom[date],funcCall2[date2]] date', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo $this->form->input('d2receipts.work_date_to',array('class'=>'validate[optional,custom[date],funcCall2[date2]] date', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                </td>
                <td></td>
                <td colspan="2">
                  <?php echo $this->form->checkbox('d2receipts.is_deleted',array('class' =>'center')); ?>取消も表示する
                </td>
            </tr>
            <tr>
                <th>要求施設</th>
                <td>
                    <?php echo $this->form->input('d2receipts.facilityName',array('id' => 'facilityName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('d2receipts.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('d2receipts.facilityCode',array('options'=>$facility_list ,'id' => 'facilityCode','class'=>'txt','empty'=>true) ); ?>
                </td>
                <td></td>
                <th>要求部署</th>
                <td>
                    <?php echo $this->form->input('d2receipts.departmentName',array('id' => 'departmentName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('d2receipts.departmentText',array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('d2receipts.departmentCode', array('options'=>$department_list, 'id' =>'departmentCode', 'class' =>'txt','empty'=>true)); ?>
                </td>
                <td></td>
                <td colspan="2">
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('d2receipts.internal_code',array('class' => 'txt search_internal_code','style' => 'width:120px',)); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('d2receipts.item_code',array('class' => 'txt search_upper','style' => 'width:120px',)); ?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('d2receipts.item_name',array('class' => 'search_canna txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('d2receipts.dealer_name',array('class' => 'search_canna txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td>
                  <?php echo $this->form->input('d2receipts.work_type',array('options'=>$class_list,'style' => 'width:120px','class' => 'txt' , 'empty'=>true)); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('d2receipts.standard',array('class' => 'search_canna txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('d2receipts.jan_code',array('class' => 'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="5">
                </td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center">
              <input type="button" class="btn btn1" id="btn_Search" />
              <input type="button" class="btn btn5" id="btn_Csv"/>
            </p>
        </div>
    </div>
    <hr class="Clear" />
    <div class="DisplaySelect">
        <div align="right" id="pageTop" ></div>
        <?php echo $this->element('limit_combobox',array('result'=>count($result) ) ); ?>
    </div>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle01">
            <tr>
                <th style="width:20px;" class="center" ><input type="checkbox" class="checkAll" /></th>
                <th style="width:135px;">要求番号</th>
                <th style="width:85px;" align="center">要求日</th>
                <th>施設／部署</th>
                <th class="col10">登録者</th>
                <th style="width:140px;">登録日時</th>
                <th class="col15">備考</th>
                <th class="col10">件数</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle01 table-even">
        <?php
          $i = 0;
          foreach($result as $r) {
        ?>
            <tr>
                <td style="width:20px;" class="center">
                    <?php echo $this->form->checkbox('d2receipts.id.'.$i,array('value'=>$r['d2receipts']['id'], 'class' =>'center chk' , 'hiddenField' => false) ); ?>
                </td>
                <td style="width:135px;"><?php echo h_out($r['d2receipts']['work_no'],'center'); ?></td>
                <td style="width:85px;"><?php echo h_out($r['d2receipts']['work_date'],'center'); ?></td>
                <td><?php echo h_out($r['d2receipts']['facility_name'] . ' / ' . $r['d2receipts']['department_name']); ?></td>
                <td class="col10"><?php echo h_out($r['d2receipts']['user_name']); ?></td>
                <td style="width:140px;"><?php echo h_out($r['d2receipts']['created'], 'center'); ?></td>
                <td class="col15"><?php echo h_out($r['d2receipts']['recital']); ?></td>
                <td class="col10"><?php echo h_out($r['d2receipts']['count'] . ' / ' . $r['d2receipts']['detail_count'], 'right'); ?></td>
            </tr>
        <?php
            $i++;
          }
        if(isset($this->request->data['d2receipts']['is_search']) && count($result) == 0){
           echo "<td colspan'8' class='center'>該当するデータがありませんでした</td>";
        }
        ?>
          </table>
    </div>
<?php if( count($result) > 0 ){ ?>
    <div class="ButtonBox">
       <p class="center">
            <input type="button" class="btn btn7 submit" id="btn_Confirm"/>
        </p>
    </div>
    <div align="right" id ="pageDow"></div>
<?php }
?>
</form>
