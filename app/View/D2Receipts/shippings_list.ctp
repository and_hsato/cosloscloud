<script>
$(document).ready(function() {
    // 確認ボタン押下
    $("#btn_Conf").click(function(){
        if($('input[type=checkbox].chk:checked').length > 0){
            $("#ShippingsList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/conf').submit();
        } else {
            alert('確認する出荷を選択してください。');
        }
    });
    // 検索ボタン押下
    $("#btn_Search").click(function(){
        $("#ShippingsList").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ShippingsList').submit();
    });
    //チェックボックス一括制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li style=""><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index">直納要求受領</a></li>
        <li>受領予定一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>受領予定一覧</span></h2>
<h2 class="HeaddingMid">受領を行いたい商品を検索してください</h2>
<form method="post" action="" accept-charset="utf-8" id="ShippingsList" class="validate_form search_form">
<?php echo $this->form->input('search.is_search',array('type'=>'hidden'))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>受領日</th>
                <td>
                    <?php echo $this->form->input('d2receipts.work_date',array('type'=>'text' , 'class' => 'lbl' , 'readonly'=>'readonly'));?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('d2receipts.className',array('type'=>'text' , 'class' => 'lbl' , 'readonly'=>'readonly'));?>
                    <?php echo $this->form->input('d2receipts.classId',array('type'=>'hidden'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php  echo $this->form->input('d2receipts.facilityName' , array('type'=>'text' ,'class'=>'lbl' ,'readonly'=>'readonly'));?>
                    <?php  echo $this->form->input('d2receipts.facilityId' , array('type'=>'hidden'));?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('d2receipts.recital.0' , array('class'=>'lbl' ,'readonly'=>'readonly') );?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('d2receipts.departmentName' , array('class'=>'lbl' ,'readonly'=>'readonly') );?>
                    <?php echo $this->form->input('d2receipts.departmentId' , array('type'=>'hidden') );?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <h2 class="HeaddingMid">検索条件</h2>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>出荷番号</th>
                <td>
                    <?php echo $this->form->input('d2receipts.search_shippings_no' , array('class'=>'txt'))?>
                </td>
                <td></td>
                <th>出荷日</th>
                <td>
                    <?php echo $this->form->input('d2receipts.search_start_date' , array('class'=>'date validate[optional,custom[date],funcCall2[date2]]' , 'id'=>'datepicker1'))?>
                     ～
                    <?php echo $this->form->input('d2receipts.search_end_date' , array('class'=>'date validate[optional,custom[date],funcCall2[date2]]' , 'id'=>'datepicker2'))?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td>
                    <?php echo $this->form->input('d2receipts.search_internal_code' , array('class'=>'txt search_internal_code'));?>
                </td>
                <td></td>
                <th>製品番号</th>
                <td>
                    <?php echo $this->form->input('d2receipts.search_item_code' , array('class'=>'txt search_upper'));?>
                </td>
                <td></td>
                <th>ロット番号</th>
                <td>
                    <?php echo $this->form->input('d2receipts.search_lot_no' , array('class'=>'txt'));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td>
                    <?php echo $this->form->input('d2receipts.search_item_name' , array('class'=>'txt search_canna'));?>
                </td>
                <td></td>
                <th>販売元</th>
                <td>
                    <?php echo $this->form->input('d2receipts.search_dealer_name' , array('class'=>'txt search_canna'));?>
                </td>
                <td></td>
                <th>規格</th>
                <td>
                    <?php echo $this->form->input('d2receipts.search_standard' , array('class'=>'txt search_canna'));?>
                </td>
            </tr>

        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn1" id="btn_Search"/>
    </div>
    <h2 class="HeaddingSmall">検索結果</h2>
        <div class="SelectBikou_Area">
            <span class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($ShippingList))); ?>
            </span>
        </div>
        <div class="results">
            <div class="TableHeaderAdjustment01">
                <table class="TableHeaderStyle01">
                    <colgroup>
                        <col width="25" />
                        <col width="150"/>
                        <col width="120"/>
                        <col />
                        <col />
                    </colgroup>
                    <tr>
                        <th><input type="checkbox" class="checkAll"/></th>
                        <th>出荷番号</th>
                        <th>出荷日</th>
                        <th>施設 ／ 部署</th>
                        <th>シール枚数</th>
                    </tr>
                </table>
            </div>
            <div class="TableScroll">
                <table class="TableStyle01 table-even">
                    <colgroup>
                        <col width="25" />
                        <col width="150"/>
                        <col width="120"/>
                        <col />
                        <col />
                    </colgroup>
                    <?php $cnt=0;foreach($ShippingList as $s){ ?>
                    <tr>
                        <td class="center">
                            <?php echo $this->form->checkbox('d2receipts.id.'.$cnt, array('class'=>'center chk','value'=>$s['d2receipts']['id'] , 'style'=>'width:20px;text-align:center;','hiddenField'=>false ) ); ?>
                        </td>
                        <td><?php echo h_out($s['d2receipts']['work_no']); ?></td>
                        <td><?php echo h_out($s['d2receipts']['work_date'],'center'); ?></td>
                        <td><?php echo h_out($s['d2receipts']['facility_name']." ／ ".$s['d2receipts']['department_name']); ?></td>
                        <td><?php echo h_out($s['d2receipts']['count'] . ' / ' . $s['d2receipts']['detail_count'],'right'); ?></td>
                    </tr>
                    <?php $cnt++; } ?>
                    <?php if( count($ShippingList)==0 && isset($this->request->data['search']['is_search']) ){ ?>
                    <tr><td colspan="5" class="center">該当するデータがありませんでした</td></tr>
                   <?php }?>
                </table>
            </div>
        </div>
    <?php if(count($ShippingList)>0){ ?>
        <div class="ButtonBox">
            <input type="button" class="btn btn3" id="btn_Conf"/>
        </div>
    <?php } ?>
</form>
