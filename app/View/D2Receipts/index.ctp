<script  type="text/javascript">
$(document).ready(function() {
    // 出荷選択ボタン押下
    $("#btn_Select").click(function(){
        $("#ReceiptsInfo").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ShippingsList');
        $("#ReceiptsInfo").submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>直納品要求受領登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>直納品要求受領登録</span></h2>
<h2 class="HeaddingMid">受領条件を設定してください</h2>
<form method="post" action="" accept-charset="utf-8" id="ReceiptsInfo" class="validate_form input_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
                <col width="20" />
            </colgroup>
            <tr>
                <th>受領日</th>
                <td>
                    <?php echo $this->form->input('d2receipts.work_date' , array('class'=>'r validate[required,custom[date],funcCall2[date2]] date')); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('d2receipts.className',array('id' => 'className','type'=>'hidden')); ?>
                    <?php echo $this->form->input('d2receipts.classId' , array('options'=>$class_list ,'id'=>'classId','class'=>'txt', 'empty'=>true ))?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('d2receipts.facilityName',array('id' => 'facilityName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('d2receipts.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'r validate[required] txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('d2receipts.facilityCode',array('options'=>$customers_enabled,'id' => 'facilityCode', 'class' => 'txt r validate[required]','empty' => true)); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('d2receipts.recital.0' , array('type'=>'text' , 'class'=>'txt' , 'maxlength'=>'200')); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('d2receipts.departmentName', array('id' => 'departmentName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('d2receipts.departmentText', array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'r validate[required] txt', 'style' => 'width: 60px;')); ?>
                    <?php echo $this->form->input('d2receipts.departmentCode',array('options'=>array(),'id' => 'departmentCode', 'class' => 'r validate[required] txt','empty' => true)); ?>
                </td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <input type="button" class="btn btn66 [p2]" id="btn_Select"/>
    </div>
</form>
