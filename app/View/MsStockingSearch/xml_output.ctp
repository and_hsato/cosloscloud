<printdata>
    <setting>
        <layoutname><?php e($layout_name); ?></layoutname>
        <fixvalues>
            <?php
            foreach ($fix_value as $name => $data) {
                e('<value name="' . $name . '">' . $data . '</value>');
            }
            ?>
        </fixvalues>
    </setting>
    <datatable>
        <columns><?php e($columns);?></columns>
        <rows>
        <?php foreach ($posted as $data): ?>
            <row><?php echo h(join("\t", $data)); ?></row>
        <?php endforeach; ?>
        </rows>
    </datatable>
</printdata>
