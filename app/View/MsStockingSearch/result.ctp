<script type="text/javascript">
    $(document).ready(function(){
        $(function (){
          $('img.scroll').click(function(){
           var to_id = $(this).attr('to_id');
           var obj = $('#'+to_id);
           var offset = obj.offset();

           scrollTo(0,offset.top);
           });
        });
    });
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>">MS仕入検索</a></li>
        <li class="pankuzu">MS仕入変更確認</li>
        <li>MS仕入変更結果</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>MS仕入変更結果</span></h2>

<div class="Mes01">以下の仕入変更を行いました。</div>

<br>
    <div id="results">
        <div class="DisplaySelect">
            <table style="width:100%;">
            <tr>
                <td align="right"><div id="pageTop"></div></td>
            </tr>
            </table>
        </div>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <colgroup>
                    <col width="170"/>
                    <col width="150"/>
                    <col>
                    <col>
                    <col width="100"/>
                    <col width="100"/>
                    <col width="100"/>
                    <col width="120"/>
                </colgroup>
                <tr>
                    <th>仕入先</th>
                    <th>仕入日</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>数量</th>
                    <th>区分</th>
                    <th>入荷単価</th>
                    <th>金額</th>
                </tr>
                <tr>
                    <th>施主</th>
                    <th>発注番号</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>商品ID</th>
                    <th></th>
                    <th>マスタ単価</th>
                    <th>遡及除外</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even2">
                <colgroup>
                    <col width="170"/>
                    <col width="150" align="center"/>
                    <col>
                    <col>
                    <col width="100" align="right"/>
                    <col width="100" align="right"/>
                    <col width="100" align="right"/>
                    <col width="120" align="right"/>
                </colgroup>
                <?php
                $i = 0;
                foreach ($SearchResult as $_row){ ?>
                <tr>
                    <td><?php echo h_out($_row[0]['supplier']); ?></td>
                    <td><?php echo h_out($_row[0]['claim_date'] , 'center'); ?></td>
                    <td><?php echo h_out($_row[0]['item_name']); ?></td>
                    <td><?php echo h_out($_row[0]['item_code']); ?></td>
                    <td><?php echo h_out($_row[0]['count'].$_row[0]['packing_name'] , 'right'); ?></td>
                    <td><?php echo h_out($_row[0]['type_name'],'center') ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($_row[0]['unit_price']), 'right'); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($_row[0]['claim_price']), 'right'); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($_row[0]['donor_name']); ?></td>
                    <td><?php echo h_out($_row[0]['work_no']); ?></td>
                    <td><?php echo h_out($_row[0]['standard']); ?></td>
                    <td><?php echo h_out($_row[0]['dealer_name']); ?></td>
                    <td><?php echo h_out($_row[0]['internal_code'],'center'); ?></td>
                    <td></td>
                    <td><?php echo h_out($this->Common->toCommaStr($_row[0]['ms_transaction_price']),'right'); ?></td>
                    <td><?php if(strcmp($_row[0]['is_not_retroactive'],"true")==0){ echo h_out("○", 'center') ;} ?></td>
                </tr>
                <?php $i++; }?>
            </table>
        </div>
        <div align="right" id="pageDow"></div>
    </div>
