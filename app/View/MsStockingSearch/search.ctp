<script type="text/javascript">
    $(document).ready(function(){
        //検索ボタン押下
        $('#btn_Search').click(function(){
            $('#change_confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/search').submit();
        });

        //帳票印刷ボタン押下
        $('#btn_Print').click(function(){
            //日付チェック
            var date1 =$("#datepicker1").val();
            $('#change_confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/dummy').submit();
        });

        //CSVボタン押下
        $('#btn_Export').click(function(){
            $('#change_confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
            $('#change_confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/index');
        });

        $('.checkAll').click(function(){
            $('input[type=checkbox].chk').attr('checked', $(this).attr('checked'));
        });

        $('#btn_confirm').click(function(){
            if($('input[type=checkbox].chk:checked').length > 0 ){  
                $('#change_confirm').attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/confirm').submit();
            }else{
                alert('変更を行う明細を選択してください');
                return false;
            }
        });
  
        //仕入先のコード表示
        $('#MstFacilitySupplierCodeSelect').change(function(){
            $('#MstFacilitySupplierCode').val($(this).val());
        });
        $('#MstFacilitySupplierCode').blur(function(){
            if($('#MstFacilitySupplierCodeSelect').find('option[value=' + $(this).val() + ']').length === 0){
                $('#MstFacilitySupplierCodeSelect').val('');
            }else{
                $('#MstFacilitySupplierCodeSelect').val($(this).val());
            }
        });

        //施主名のコード表示
        $('#MstFacilityDonorCodeSelect').change(function(){
            $('#MstFacilityDonorCode').val($(this).val());
        });
        $('#MstFacilityDonorCode').blur(function(){
            if($('#MstFacilityDonorCodeSelect').find('option[value=' + $(this).val() + ']').length === 0){
                $('#MstFacilityDonorCodeSelect').val('');
            }else{
                $('#MstFacilityDonorCodeSelect').val($(this).val());
            }
        });
    });

$(function (){
  $('img.scroll').click(function(){
   var to_id = $(this).attr('to_id');
   var obj = $('#'+to_id);
   var offset = obj.offset();
   scrollTo(0,offset.top);
   });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>MS法人仕入検索</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>MS仕入検索</span></h2>

<form class="validate_form search_form" id="change_confirm" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/index" method="post">
    <input type="hidden" name="data[IsSearch]" value="1" />
    <input type="hidden" name="data[session]" value="<?php print($this->Session->read('Auth.facility_id_selected'));?>">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>仕入先</th>
                <td>
                    <?php echo($this->form->text('MstFacility.supplier_code', array('class'=>'txt', 'maxlength'=>'50', 'style'=>'width:60px;'))); ?>
                    <?php echo($this->form->input('MstFacility.supplier_code_select',array('type'=>'select','options'=> $facilities, 'empty'=>true ,'class' => 'txt','width' => '120px'))); ?>
                </td>
                <td width="20"></td>
                <th>施主</th>
                <td>
                    <?php echo($this->form->text('MstFacility.donor_code', array('class'=>'txt', 'maxlength'=>'50','label'=>'', 'style'=>'width:60px;'))); ?>
                    <?php echo($this->form->input('MstFacility.donor_code_select',array('type'=>'select','options'=> $donor, 'empty'=>true ,'class' => 'txt'))); ?>
                </td>
                <td width="20"></td>
                <th>仕入日</th>
                <td>
                    <?php echo($this->form->text('TrnClaim.claim_date_from', array('class' => 'date txt validate[custom[date]]','id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo($this->form->text('TrnClaim.claim_date_to', array('class' => 'date txt validate[custom[date]]', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'))); ?>
                </td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo($this->form->text('MstFacilityItem.internal_code', array('class' => 'txt search_internal_code', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo($this->form->text('MstFacilityItem.item_code', array('class' => 'txt search_upper', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th colspan="2"><?php if(!(isset($check))){$check = true;} echo($this->form->checkbox('TrnClaim.is_deleted', array('checked' => $check))); ?>取消は表示しない</th>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo($this->form->text('MstFacilityItem.item_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo($this->form->text('MstDealer.dealer_name', array('class' => 'txt search_canna', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo($this->form->text('MstFacilityItem.standard', array('class' => 'txt', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo($this->form->text('MstFacilityItem.jan_code', array('class' => 'txt', 'maxlength' => '50', 'label' => ''))); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>
    </div>

    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn1" id="btn_Search" />
            <input type="button" class="btn btn10 print_btn" id="btn_Print" />
            <input type="button" class="btn btn5" id="btn_Export" />
        </p>
    </div>
    <br>
    <div id="results">
        <table style="width:100%;">
        <tr>
            <td><div class="DisplaySelect"><?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?></div></td>
            <td align="right"><div id="pageTop"></div></td>
        </tr>
        </table>

        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <colgroup>
                    <col width="20"/>
                    <col width="150"/>
                    <col width="150"/>
                    <col>
                    <col>
                    <col width="100"/>
                    <col width="100"/>
                    <col width="100"/>
                    <col width="120"/>
                </colgroup>
                <tr>
                    <th rowspan="2"><input type="checkbox" class="checkAll"/></th>
                    <th>仕入先</th>
                    <th>仕入日</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>数量</th>
                    <th>区分</th>
                    <th>販売単価</th>
                    <th>金額</th>
                </tr>
                <tr>
                    <th>施主</th>
                    <th>発注番号</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>商品ID</th>
                    <th></th>
                    <th>マスタ単価</th>
                    <th>遡及除外</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle01 table-even2">
                <colgroup>
                    <col width="20"/>
                    <col width="150"/>
                    <col width="150" align="center"/>
                    <col>
                    <col>
                    <col width="100" align="right"/>
                    <col width="100" align="right"/>
                    <col width="100" align="right"/>
                    <col width="120" align="right"/>
                </colgroup>
                <?php if(isset($SearchResult) && count($SearchResult) === 0 && isset($this->request->data["IsSearch"]) && $this->request->data["IsSearch"] === "1"): ?>
                <tr><td colspan="9" class="center"><span>該当するデータが存在しません</span></td></tr>
                <?php endif;?>
                <?php foreach ($SearchResult as $index => $_row): ?>
                <tr>
                    <td rowspan="2" align="center" style="width:20px;">
                        <?php echo $this->form->checkbox("TrnClaim.id.{$index}", array('hiddenField'=>false , 'class'=>'chk' , 'value'=>$_row[0]['id'])); ?>
                    </td>
                    <td><?php echo h_out($_row[0]['supplier']); ?></td>
                    <td><?php echo h_out($_row[0]['claim_date'],'center'); ?></td>
                    <td><?php echo h_out($_row[0]['item_name']); ?></td>
                    <td><?php echo h_out($_row[0]['item_code']); ?></td>
                    <td><?php echo h_out($_row[0]['count'].$_row[0]['packing_name'],'right'); ?></td>
                    <td><?php echo h_out($_row[0]['type_name'],'center'); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($_row[0]['unit_price']), 'right'); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($_row[0]['claim_price']),'right'); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($_row[0]['donor_name']); ?></td>
                    <td><?php echo h_out($_row[0]['work_no']); ?></td>
                    <td><?php echo h_out($_row[0]['standard']); ?></td>
                    <td><?php echo h_out($_row[0]['dealer_name']); ?></td>
                    <td><?php echo h_out($_row[0]['internal_code'],'center'); ?></td>
                    <td></td>
                    <td><?php echo h_out($this->Common->toCommaStr($_row[0]['ms_transaction_price']),'right'); ?></td>
                    <td><?php echo h_out($_row[0]['is_not_retroactive'],'center'); ?></td>
                </tr>
    <?php endforeach; ?>
            </table>
        </div>
        <!-- -->
<?php if (count($SearchResult) > 0): ?>
        <div align="right" id="pageDow"></div>
        <div class="ButtonBox">
            <p class="center"><input type="button" value="" class="btn btn3 [p2]" id="btn_confirm" /></p>
        </div>
<?php endif; ?>
    </div>
</form>
