<script type="text/javascript">
$(function(){
    //確認
    $("#btn_execution_confirm").click(function(){
        if( $('input[name="data[TrnInventoryHeader][id]"]:checked').length > 0){
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_detail_confirm').submit();
        }else{
            alert('棚卸対象を選択してください。');
        }
    });
    //CSV取り込み
    $("#btn_execution_csv").click(function(){
        if( $('input[name="data[TrnInventoryHeader][id]"]:checked').length > 0){
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_add_confirm').submit();
        }else{
            alert('棚卸対象を選択してください。');
        }
    });

    //棚卸表印刷
    $("#btn_execution_print").click(function(){
        if( $('input[name="data[TrnInventoryHeader][id]"]:checked').length > 0){
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/report').submit();
        }else{
            alert('棚卸対象を選択してください。');
        }
    });

    //差異リスト印刷
    $("#btn_execution_diff_print").click(function(){
        if( $('input[name="data[TrnInventoryHeader][id]"]:checked').length > 0){
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/diff').submit();
        }else{
            alert('棚卸対象を選択してください。');
        }
    });

    //差異リストCSV出力
    $("#btn_execution_diff_csv").click(function(){
        if( $('input[name="data[TrnInventoryHeader][id]"]:checked').length > 0){
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        }else{
            alert('棚卸対象を選択してください。');
        }
    });

    //クリア
    $('#btn_execution_clear').click(function(){
        if( $('input[name="data[TrnInventoryHeader][id]"]:checked').length > 0){
            if(window.confirm("クリアを実行します。よろしいですか？")){
                $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_clear').submit();
            }
        }else{
            alert('クリア対象を選択してください。');
        }
     });
  
    //削除
    $('#btn_execution_delete').click(function(){
        if( $('input[name="data[TrnInventoryHeader][id]"]:checked').length > 0){
            if(window.confirm("削除を実行します。よろしいですか？")){
                $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_delete').submit();
            }
        }else{
            alert('削除対象を選択してください。');
        }
    });
  
    //検索
    $("#btn_Search").click(function(){
        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_add').submit();
    });
});

function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}


</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>棚卸実施登録</li>
    </ul>
</div>

<?php if (isset($clearSuccess)) { /*クリア成功の場合*/ ?> 
<div class="Mes01">棚卸データのクリアを行いました</div>
<?php }elseif (isset($deleteSuccess)) {  /*削除成功の場合*/ ?>
<div class="Mes01">棚卸データの削除を行いました</div>
<?php } ?>
<h2 class="HeaddingLarge"><span>棚卸実施登録</span></h2>
<h2 class="HeaddingMid">棚卸の実施を登録する棚卸番号を検索してください。</h2>

<?php echo $this->form->create( 'Inventory',array('type' => 'post','action' => '' ,'id' =>'searchForm','class' => 'validate_form search_form') ); ?>
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('TrnInventory.mst_facility_id',array('type'=>'hidden'));?>
    <?php echo $this->form->input('TrnInventory.token',array('type'=>'hidden'));?>
    <?php echo $this->form->input('TrnInventory.time',array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u')));?>
    <?php echo $this->form->input('mode',array('type'=>'hidden','id' =>'mode'));?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>棚卸番号</th>
                <td><?php echo $this->form->text('search.work_no',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td width="20"></td>
                <th>作成日</th>
                <td>
                    <?php echo $this->form->input('search.work_date_from',array('type'=> 'text' , 'class'=>'date validate[custom[date]]', 'id'=>'work_date_from')); ?>～<?php echo $this->form->input('search.work_date_to',array('type'=>'text' , 'class'=>'date validate[custom[date]]' , 'id'=>'work_date_to')); ?>
                </td>
            </tr>
            <tr>
                <th>棚卸実施名</th>
                <td><?php echo $this->form->text('search.title',array('class'=>'txt','style'=>'width:120px')); ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <?php echo $this->form->checkbox("search.check", array('class'=>'center checkAllTarget' , 'style'=>'width:20px;text-align:center;') ); ?>
                    未実施のみ
                </td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center"><input type="button" class="btn btn1" id="btn_Search" /></p>
        </div>
        <hr class="Clear" />

        <div id="hideer">
            <h2 class="HeaddingSmall">実施登録を行いたい棚卸番号を選択し、確認ボタンをクリックしてください。</h2>
            <div align="right" id="pageTop" ></div>
            <div class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
            </div>
            <div class="TableHeaderAdjustment01">
                <table class="TableHeaderStyle01">
                    <tr>
                        <th style="width:20px;"></th>
                        <th class="col15">棚卸番号</th>
                        <th class="col15">作成日時</th>
                        <th class="col45">実施名</th>
                        <th class="col10">区分</th>
                        <th class="col15">実施日</th>
                        <th class="col10">差異件数</th>
                    </tr>
                </table>
            </div>
            <div class="TableScroll" >
                <table class="TableStyle01 table-even">
                    <?php $i = 0;?>
                    <?php foreach($SearchResult as $item){?>
                    <tr>
                        <td style="width:20px;" class="center"><input type="radio" name="data[TrnInventoryHeader][id]" value="<?php echo $item['TrnInventoryHeader']['id']; ?>" /></td>
                        <td class="col15"><?php echo h_out($item['TrnInventoryHeader']['work_no'],'center'); ?></td>
                        <td class="col15"><?php echo h_out($item['TrnInventoryHeader']['created'],'center'); ?></td>
                        <td class="col45"><?php echo h_out($item['TrnInventoryHeader']['title']); ?></td>
                        <td class="col10"><?php echo h_out($item['TrnInventoryHeader']['inventory_type'],'center'); ?></td>
                        <td class="col15"><?php echo h_out($item['TrnInventoryHeader']['work_date'],'center'); ?></td>
                        <td class="col10"><?php echo h_out($item['TrnInventoryHeader']['difference_count'],'right'); ?></td>
                    </tr>
                    <?php $i++ ; } ?>
                    <?php if(isset($this->request->data['search']['is_search']) && count($SearchResult)==0){?>
                    <tr>
                        <td colspan="7" class="center">該当するデータがありませんでした</td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
            <?php if(count($SearchResult) > 0){ ?>
            <div class="ButtonBox">
                <input type="button" class="btn btn3 confirm_btn [p2]" id="btn_execution_confirm" />
                <input type="button" class="btn btn5 csv_btn [p2]" id="btn_execution_csv" />
                <input type="button" class="btn btn77 confirm_btn" id="btn_execution_print" />
                <input type="button" class="btn btn31 confirm_btn" id="btn_execution_diff_print" />
                <input type="button" class="btn btn45 confirm_btn" id="btn_execution_diff_csv" />
                <input type="button" class="btn btn14 confirm_btn" id="btn_execution_clear" />
                <input type="button" class="btn btn39 confirm_btn" id="btn_execution_delete" />
            </div>
            <?php } ?>
        </div>
    </div>
<?php echo $this->form->end(); ?>
<div align="right" id="pageDow" ></div>
