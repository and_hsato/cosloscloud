<script type="text/javascript">
$(function() {
    $("#result_btn").click(function(){
        if($('.TableStyle02 input:checked').length > 0) {
            $('#work_name').val($('#work_type option:selected').text());
            $("#inputForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/each_shelf_add_result').submit();
        }else{
            alert('棚卸データがチェックされていません');
            return false;
        }
    });
  
    //チェックボックス一括制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
});

function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}  
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/each_shelf_add">棚別予定登録</a></li>
        <li>棚別予定登録確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>棚別予定登録確認</span></h2>
<h2 class="HeaddingMid">以下の棚を対象に在庫を取得します。</h2>

<form class="validate_form" id="inputForm" action="#" method="post">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>棚卸実施名</th>
                <td><?php echo $this->form->input('inventory.title',array('type'=>'text','class'=>'r txt validate[required]','value'=>'','maxLength'=>'200' )); ?></td>
            </tr>
            <tr>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('inventory.work_type',array('options'=>$classes ,'id' => 'work_type', 'class'=>'txt' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('inventory.work_name',array('type'=>'hidden','value'=>'' , 'id' => 'work_name' )); ?>
                </td>
            </tr>
            <tr>
                <th>備考</th>
                <td><?php echo $this->form->input('inventory.recital',array('type'=>'text','class'=>'txt','value'=>'' ,'maxLength'=>'200')); ?></td>
            </tr>
        </table>
        <hr class="Clear" />
        <div align="right" id="pageTop" style="padding-right: 10px;"></div>
        <h2 class="HeaddingSmall">問題ない場合は確定ボタンをクリックしてください。</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th width="20"><input type="checkbox" class="checkAll" checked="checked"/></th>
                    <th class="col80">施設　／　部署</th>
                    <th class="col20">棚番号</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02 table-even">
                <colgroup>
                    <col>
                    <col>
                    <col align="center">
                </colgroup>
                <?php $i = 0;?>
                <?php foreach( $SearchResult as $item ){ ?>
                <tr>
                    <td width="20" class="center"><?php echo $this->form->input('inventory.id.'.$i.'',array('type'=>'checkbox','class'=>'chk','value'=>$item['inventory']['id'],'checked'=>'checked' ,'hiddenField' => false )); ?></td>
                    <td class="col80"><?php echo h_out($item['inventory']['facility_name'] . '／' . $item['inventory']['department_name']); ?></td>
                    <td class="col20"><?php echo h_out($item['inventory']['name'],'center'); ?></td>
                </tr>
                <?php $i++ ?>
                <?php } ?>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="btn btn2 fix_btn [p2]" id="result_btn" />
        </div>
        <div align="right" id="pageDow" ></div>
    </div>
</form>
