<script type="text/javascript">
$(function(){
    $("#btn_difference_detail").click(function(){
        if ($("input:checked").length > 0) {
            $("#form_difference_detail").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/difference_detail');
            $("#form_difference_detail").attr('method', 'post');
            $("#form_difference_detail").submit();
        }else{
            alert('棚卸データがチェックされていません');
            return false;
        }
    });

    //印刷ボタン
    $("#btn_differnce_report").click(function(){
        if ($("input:checked").length > 0) {
            $("#form_difference_detail").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/diff');
            $("#form_difference_detail").attr('method', 'post');
            $("#form_difference_detail").submit();
        }else{
            alert('棚卸データがチェックされていません');
            return false;
        }
    });

  
    //検索ボタン押下
    $("#btn_Search").click(function(){
        $("#form_difference_detail").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/difference_list');
        $("#form_difference_detail").attr('method', 'post');
        $("#form_difference_detail").submit();
    });

    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#form_difference_detail input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
});

function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>棚卸差異一覧</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>棚卸差異一覧</span></h2>
<h2 class="HeaddingMid">登録されている棚卸を検索してください。</h2>
<form class="validate_form" id="form_difference_detail" action="#" method="post">
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>棚卸番号 </th>
                <td>
                    <?php echo $this->form->text('search.work_no',array('class'=>'txt','style'=>'width:120px')); ?>
                </td>
            </tr>
            <tr>
                <th>棚卸実施名 </th>
                <td>
                    <?php echo $this->form->text('search.title',array('class'=>'txt','style'=>'width:120px')); ?>
                </td>
            </tr>
            <tr>
                <th>実施日 </th>
                <td>
                    <?php echo $this->form->input('search.work_date_from',array('type'=>'text','class'=>'date validate[custom[date]]', 'id'=>'work_date_from')); ?>～<?php echo $this->form->input('search.work_date_to',array('type'=>'text','class'=>'date validate[custom[date]]', 'id'=>'work_date_to')); ?>
                </td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center"><input type="button"  class="btn btn1" id="btn_Search" /></p>
        </div>
        <hr class="Clear" />
        <div align="right" id="pageTop" ></div>
        <div id="hideer">
            <h2 class="HeaddingSmall">現在の棚卸の差異状況を印字します。</h2>
            <div class="DisplaySelect">
                <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
            </div>
            <div class="TableHeaderAdjustment01">
                <table class="TableHeaderStyle01">
                    <tr>
                        <th style="width:20px;" class="center"><input type="checkbox" class="checkAll_1" /></th>
                        <th class="col20">棚卸番号</th>
                        <th class="col20">作成日時</th>
                        <th class="col10">区分</th>
                        <th class="col20">実施名</th>
                        <th class="col20">実施日</th>
                        <th class="col10">差異件数</th>
                    </tr>
                </table>
            </div>
            <div class="TableScroll">
                <table class="TableStyle01">
                    <?php $i=0; foreach($SearchResult as $item){  ?>
                    <tr class = "<?php echo($i % 2 == 0)?"":"odd" ?>">
                        <td style="width:20px;" class="center">
                        <?php if($item['TrnInventoryHeader']['difference_count'] > 0){ ?>
                            <input type="checkbox" name="data[TrnInventoryHeader][id][<?php echo $i ?>]" class = "chk center checkAllTarget" value=<?php echo $item['TrnInventoryHeader']['id'] ?>>
                        <?php } ?>
                        </td>
                        <td class="col20"><?php echo h_out($item['TrnInventoryHeader']['work_no']); ?></td>
                        <td class="col20"><?php echo h_out($item['TrnInventoryHeader']['created']); ?></td>
                        <td class="col10"><?php echo h_out($InventoryType[$item['TrnInventoryHeader']['inventory_type']]); ?></td>
                        <td class="col20"><?php echo h_out($item['TrnInventoryHeader']['title']); ?></td>
                        <td class="col20"><?php echo h_out($item['TrnInventoryHeader']['work_date']); ?></td>
                        <td class="col10"><?php echo h_out($item['TrnInventoryHeader']['difference_count'],'right'); ?></td>
                    </tr>
                    <?php $i++; ?>
                    <?php } ?>
                    <?php if(isset($this->request->data['search']['is_search']) && count($SearchResult) == 0){ ?>
                    <tr>
                        <td colspan="6">
                            該当するデータがありません
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
            <div align="right" id="pageDow" ></div>
            <?php if(count($SearchResult) > 0){ ?>
            <div class="ButtonBox">
                <p class="center">
                    <input type="button" class="btn btn7" id="btn_difference_detail" />
                    <input type="button" class="btn btn31" id="btn_differnce_report" />
                </p>
            </div>
            <?php } ?>
        </div>
    </div>
</form>
