<script type="text/javascript">

$(function(){
    //確定ボタン
    $("#result_btn").click(function(){
        if($("#inventories_name").val() == ""){
            alert('棚卸実施名を入力してください。');
            $("#inventories_name").focus();
            return;
        }
  
        if($(".TableStyle02 input:checked").length > 0){
            $('#work_name').val($('#work_type option:selected').text());
            $('#facilityName').val($('#facilityCode option:selected').text());
            
            $("#result_form").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/each_commodity_add_result').submit();
        }else{
            alert("棚卸対象の棚を選択してください。");
        }
    });

    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

  
    //検索ボタン
    $("#search_btn").click(function(){
        $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/each_commodity_add_confirm').submit();
    });
  
    // 施設プルダウンを変更したら施設コードを入力
    $('#facilityCode').change(function(){
        $('#facilityText').val($('#facilityCode').val());
    });

    // 施設コードを入力したら施設プルダウンを変更
    $('#facilityText').blur(function(){
        $('#facilityCode').selectOptions($(this).val());
    });


});
function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/each_commodity_add">商品別予定登録</a></li>
        <li>商品別予定登録確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>商品別予定登録確認</span></h2>
<h2 class="HeaddingMid">以下の商品を対象に在庫を取得します。</h2>

<form class="validate_form" id="result_form" action="#" method="post">
    <table class="FormStyleTable">
        <tr>
            <th>棚卸実施名</th>
            <td><?php echo $this->form->input('inventory.title',array('type'=>'text','class'=>'r txt')); ?></td>
            <td width="20"></td>
            <th>作業区分</th>
            <td>
                <?php echo $this->form->input('inventory.work_type',array('options'=>$classes ,'id' => 'work_type', 'class'=>'txt' , 'empty'=>true)); ?>
                <?php echo $this->form->input('inventory.work_name',array('type'=>'hidden','id' => 'work_name' )); ?>
            </td>
        </tr>
        <tr>
            <th>施設</th>
            <td>
                <?php echo $this->form->input('inventory.facilityName',array('id' => 'facilityName','type'=>'hidden')); ?>
                <?php echo $this->form->input('inventory.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                <?php echo $this->form->input('inventory.facilityCode',array('options'=>$facility_list ,'id' => 'facilityCode','class'=>'txt','empty'=>true) ); ?>
            </td>
            <td></td>
            <th>備考</th>
            <td><?php echo $this->form->input('inventory.recital',array('type'=>'text','class'=>'txt' )); ?></td>
        </tr>
    </table>

    <div class="ButtonBox">
        <input type="button" class="btn btn1 fix_btn [p2]" id="search_btn" />
    </div>

    <hr class="Clear" />

    <h2 class="HeaddingSmall">棚一覧</h2>
    <div align="right" id="pageTop" style="padding-right: 10px;"></div>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th width="20"><input type="checkbox" class="checkAll"/></th>
                    <th class="col80">施設　／　部署</th>
                    <th class="col20">棚番号</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02 table-even">
                <colgroup>
                    <col>
                    <col>
                    <col align="center">
                </colgroup>
                <?php $i = 0;?>
                <?php foreach( $SearchResult as $item ){ ?>
                <tr>
                    <td width="20" class="center">
                        <?php echo $this->form->checkbox("inventory.id.{$i}", array('class'=>'center chk','value'=>$item['inventory']['id'] , 'style'=>'width:20px;text-align:center;' , 'hiddenField' => false) ); ?>
                    </td>
                    <td class="col80"><?php echo h_out($item['inventory']['facility_name'] . '／' .$item['inventory']['department_name']); ?></td>
                    <td class="col20"><?php echo h_out($item['inventory']['name'],'center'); ?></td>
                </tr>
                <?php $i++ ?>
                <?php } ?>
            </table>
        </div>
        <div align="right" id="pageDow" ></div>
        <div class="ButtonBox">
            <input type="button" class="btn btn2 fix_btn [p2]" id="result_btn" />
        </div>
    </div>
    <?php echo $this->form->input('search.tempId',array('type'=>'hidden','id' =>'tempId'));?>
    <?php echo $this->form->input('inventory.is_search',array('type'=>'hidden','id' =>'is_search'));?>
</form>
