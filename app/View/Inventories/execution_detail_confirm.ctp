<script type="text/javascript">
$(function(){
    //追加ボタンクリック
    $("#btn_add").click(function(){
        chkflg = false;
        var cb = $("#StickerTable input:checkbox[@class='chk']");
        $.each(cb,function(i) {
           if ($(this).attr('checked')==true){
               chkflg = true;
               return false;
           }
        });
        if (chkflg) {
            $("#form_execution_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_detail_confirm/add').submit();
        }else{
            alert("追加シールを選択してください。");
        }
    });

    //更新ボタンクリック
    $("#btn_regist").click(function(){

        chkflg = false;
        var erorr_flag = false;
        var cb = $("#inventoryTables input:checkbox[@class='checkInventory']");
        $.each(cb,function(i) {
           if ($(this).attr('checked')==true){
               chkflg = true;
               id = $(this).val();
               if(!$('#quantity'+id).val()){
                   alert('数値は必ず入力してください。');
                   erorr_flag = true;
                   return false;
               }
           }
        });
        if(erorr_flag){
            return false;
        }

        if(chkflg){
            $("#form_execution_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_detail_confirm/regist').submit();
        }else{
            alert('更新対象の項目にチェックを入れてください。');
        }
    });

    //チェックボックスで個数を入力する。
    $(".checkInventory").change(function(){
        if($(this).attr('checked')){
            id = $(this).val();
            if($("#quantity"+id).val() ==""){
                $("#quantity"+id).val($("#init_quantity"+id).val());
            }
        }
    });
  
  
    //追加シール検索ボタンクリック
    $("#btn_search").click(function(){
        $("#form_execution_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_detail_confirm/search').submit();
    });

    //棚卸検索ボタンクリック
    $("#btn_inventory_search").click(function(){
        $("#form_execution_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_detail_confirm/inventory_search').submit();
    });

  
    //チェックボックス オールチェック
    $('#checkAllSticker').click(function(){
        $('#form_execution_result input[type=checkbox][name*=Sticker]').attr('checked',$(this).attr('checked'));
    });

      //棚卸チェックボックス オールチェック
    $('#checkAllInventory').click(function() {
        var chkbox = $('#form_execution_result input[type=checkbox][name*=Inventory]');
        chked = $(this).attr('checked');
        $.each(chkbox,function(i) {
            if(chked){
                $(this).attr('checked',chked);
                id = $(this).val();
                if($("#quantity"+id).val() ==""){
                    $("#quantity"+id).val($("#init_quantity"+id).val());
                 }
             }else{
                $(this).attr('checked',chked);
             }
        });
    });
  
    $('#PageNo').change(function(){
        $("#form_execution_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_detail_confirm/inventory_search').submit();
    });
  
});


  function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}  
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_add">棚卸実施登録</a></li>
        <li>棚卸実施明細確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>棚卸実施明細確認</span></h2>
<h2 class="HeaddingMid">棚卸在庫の数量を変更してください</h2>
<?php echo $this->form->create( 'Inventory',array('type' => 'post','action' => '' ,'id' =>'form_execution_result','class' => 'validate_form search_form') ); ?>
    <input type="hidden" name="data[work_time]" id="work_time" value="<?php echo date('Y/m/d H:i:s')?>" >
    <?php echo $this->form->input('TrnInventoryHeader.id' ,array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>棚卸番号</th>
                <td><?php echo $this->form->input('TrnInventoryHeader.work_no' ,array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>棚卸実施名</th>
                <td><?php echo $this->form->input('TrnInventoryHeader.title' ,array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
            </tr>
            <tr>
                <th>棚卸種別</th>
                <td><?php echo $this->form->input('TrnInventoryHeader.inventory_type' ,array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
            </tr>
        </table>

            <h2 class="HeaddingLarge2">棚卸検索条件</h2>
            <table class="FormStyleTable">
                <tr>
                    <th>商品ID</th>
                    <td><?php echo $this->form->text('Inventory.internal_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                    <td width="20"></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->text('Inventory.item_code',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                    <td width="20"></td>
                    <th>ロット番号</th>
                    <td><?php echo $this->form->text('Inventory.lot_no',array('class'=>'txt','style'=>'width:120px')); ?></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->text('Inventory.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->text('Inventory.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                    <td width="20"></td>
                    <th><?php echo $this->form->checkbox('Inventory.flg' , array('hiddenField'=>false)); ?></th>
                    <td>差分有りのみ表示</td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->text('Inventory.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                    <td></td>
                    <th>シール番号</th>
                    <td><?php echo $this->form->text('Inventory.sticker_no',array('class'=>'txt','style'=>'width:120px')); ?></td>
                </tr>
            </table>
            <div class="ButtonBox">
                <input type="button" value="" class="btn btn1"  id="btn_inventory_search"/>
            </div>
        <hr class="Clear" />
        <div id="results" class="results">
            <div class="DisplaySelect">
                <div align="right" id="pageTop" ></div>
<?php if(isset($page_list)){?>
ページ番号:<?php echo $this->form->input('inventory.pgno' , array('options'=>$page_list , 'class'=>'txt' , 'empty'=>false , 'id'=>'PageNo'))?>
<?php } ?>
                </div>
                <div class="TableHeaderAdjustment01">
                    <table class="TableHeaderStyle02">
                        <tr>
                            <th style="width:20px;" rowspan="2"><input type="checkbox" id="checkAllInventory"></th>
                            <th class="col10">商品ID</th>
                            <th class="col15">商品名</th>
                            <th class="col15">製品番号</th>
                            <th class="col10">論理在庫</th>
                            <th class="col15">センターシール</th>
                            <th class="col15">ロット番号</th>
                            <th class="col5">定数</th>
                            <th class="col15">配置場所</th>
                        </tr>
                        <tr>
                            <th>状態</th>
                            <th>規格</th>
                            <th>販売元</th>
                            <th>数量</th>
                            <th>部署シール</th>
                            <th>有効期限</th>
                            <th>不動日数</th>
                            <th>備考</th>
                        </tr>
                    </table>
                </div>
                <div class="TableScroll">
                    <table class="TableStyle02 table-even2" id="inventoryTables">
                        <colgroup>
                            <col>
                            <col align="center">
                            <col>
                            <col>
                            <col align="center">
                            <col align="center">
                            <col>
                            <col align="right">
                            <col>
                        </colgroup>
                        <?php $i = 0 ; ?>
                        <?php foreach($SearchResult as $item){ ?>
                        <tr>
                            <td style="width:20px;" rowspan="2" class="center">
                                <input type="checkbox" name="data[chkInventory][<?php echo $i ?>]" class="checkInventory" value="<?php echo $i ?>">
                                <input type="hidden" id=init_quantity<?php echo $i?> name="data[init_quantity][<?php echo $i?>]" value="<?php echo ($item['Inventory']['quantity'] == 1)?$item['Inventory']['quantity']:""; ?>">
                                <input type="hidden" name="data[id][<?php echo $i?>]" value="<?php echo $item['Inventory']['id']?>">
                            </td>
                            <td class="col10"><?php echo h_out($item['Inventory']['internal_code'],'center'); ?></td>
                            <td class="col15"><?php echo h_out($item['Inventory']['item_name']); ?></td>
                            <td class="col15"><?php echo h_out($item['Inventory']['item_code']); ?></td>
                            <td class="col10"><?php echo h_out($item['Inventory']['quantity'],'center'); ?></td>
                            <td class="col15"><?php echo h_out($item['Inventory']['facility_sticker_no'],'center'); ?></td>
                            <td class="col15"><?php echo h_out($item['Inventory']['lot_no'],'center'); ?></td>
                            <td class="col5"><?php echo h_out($item['Inventory']['fixed_count'],'right'); ?></td>
                            <td class="col15"><?php echo h_out($item['Inventory']['facility_name'] . '／' . $item['Inventory']['department_name']); ?></td>
                        </tr>
                        <tr>
                            <td><?php echo h_out($item['Inventory']['status']); ?></td>
                            <td><?php echo h_out($item['Inventory']['standard']); ?></td>
                            <td><?php echo h_out($item['Inventory']['dealer_name']); ?></td>
                            <td>
                                <input type="text" class="r num validate[optional,custom[onlyNumber]]" name="data[quantity][<?php echo $i?>]" value="<?php echo $item['Inventory']['real_quantity']?>" id="quantity<?php echo $i?>" style="width:90px;" maxlength="32">
                            </td>
                            <td><?php echo h_out($item['Inventory']['hospital_sticker_no'],'center'); ?></td>
                            <td><?php echo h_out($item['Inventory']['validated_date'],'right'); ?></td>
                            <td><?php echo h_out($item['Inventory']['immovable_date'],'right'); ?></td>
                            <td>
                                <input type="text" class="txt" name="data[recital][<?php echo $i?>]" value="<?php echo $item['Inventory']['recital']?>" style="width:160px;" maxlength="200">
                            </td>
                        </tr>
                        <?php $i++; ?>
                        <?php } ?>
                    </table>
                </div>
                <div class="ButtonBox">
                    <input type="button" class="btn btn24" id="btn_regist" />
                </div>
            </div>
            <h2 class="HeaddingLarge2">追加シール検索条件</h2>
            <table class="FormStyleTable">
                <tr>
                    <th>商品ID</th>
                    <td><?php echo $this->form->text('search.internal_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                    <td width="20"></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->text('search.item_code',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                    <td width="20"></td>
                    <th>ロット番号</th>
                    <td><?php echo $this->form->text('search.lot_no',array('class'=>'txt','style'=>'width:120px')); ?></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->text('search.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->text('search.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->text('search.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                    <td></td>
                    <th>シール番号</th>
                    <td><?php echo $this->form->text('search.sticker_no',array('class'=>'txt','style'=>'width:120px')); ?></td>
                </tr>
            </table>
            <div class="ButtonBox">
                <input type="button" class="btn btn1"  id="btn_search"/>
            </div>
            <div id="results2" class="results">
                <div class="DisplaySelect">
                    <?php echo $this->element('limit_combobox',array('result'=>count($Stickers))); ?>
                </div>
                <div class="TableHeaderAdjustment01">
                    <table class="TableHeaderStyle02">
                        <tr>
                            <th width="20" rowspan="2"><input type="checkbox" id="checkAllSticker" /></th>
                            <th class="col10">商品ID</th>
                            <th>商品名</th>
                            <th>製品番号</th>
                            <th class="col25">包装単位</th>
                            <th class="col15">センターシール</th>
                        </tr>
                        <tr>
                            <th>状態</th>
                            <th>規格</th>
                            <th>販売元</th>
                            <th>施設　／　部署</th>
                            <th>部署シール</th>
                        </tr>
                    </table>
                </div>
                <div class="TableScroll">
                    <table class="TableStyle02 table-even2" id="StickerTable">
                    <?php if(count($Stickers) > 0){ ?>
                        <input type='hidden' name="data[search][is_search]" value="1" id="is_search" >
                        <?php $i = 0; ?>
                        <?php foreach($Stickers as $s ){?>
                        <tr>
                            <td width="20" rowspan="2" class="center">
                                <input type="checkbox" id="chk_sticker<?php echo $i ?>" name="data[TrnSticker][sticker_id][<?php $i ?>]" class="chk checkAllTarget" value="<?php echo $s['TrnSticker']['id'] ?>"/>
                            </td>
                            <td class="col10"><?php echo h_out($s['TrnSticker']['internal_code'],'center'); ?></td>
                            <td><?php echo h_out($s['TrnSticker']['item_name']); ?></td>
                            <td><?php echo h_out($s['TrnSticker']['item_code']); ?></td>
                            <td class="col25"><?php echo h_out($s['TrnSticker']['unit_name']); ?></td>
                            <td class="col15"><?php echo h_out($s['TrnSticker']['facility_sticker_no'],'center'); ?></td>
                        </tr>
                        <tr>
                            <td><?php echo h_out($s['TrnSticker']['status'],'center'); ?></td>
                            <td><?php echo h_out($s['TrnSticker']['standard']); ?></td>
                            <td><?php echo h_out($s['TrnSticker']['dealer_name']); ?></td>
                            <td><?php echo h_out($s['TrnSticker']['facility_name'] . '/' . $s['TrnSticker']['department_name']); ?></td>
                            <td><?php echo h_out($s['TrnSticker']['hospital_sticker_no'],'center'); ?></td>
                        </tr>
                        <?php $i++;?>
                        <?php } ?>
                        <?php }else if(isset($data['search']['is_search']) && $data['search']['is_search'] === 1){ ?>
                        <tr>
                            <td colspan="6">
                            該当するデータがありませんでした
                            <input type='hidden' name="data[search][is_search]" value="0" id="is_search" >
                            </td>
                        </tr>
                        <?php }else{ ?>
                        <input type='hidden' name="data[search][is_search]" value="0" id="is_search" >
                        <?php } ?>
                    </table>
                </div>
                <div align="right" id="pageDow" ></div>
                <div class="ButtonBox">
                    <input type="button" class="btn btn81 [p2]" id="btn_add" />
                </div>
            </div>
        </div>
<?php echo $this->form->end(); ?>
