<script type="text/javascript">
$(function(){
    $("#btn_execution_add_result").click(function(){
        if(!$('#file').val()){
            alert('ファイルが選択されていません');
            return;
        } else if(!$('#file').val().match(/\.csv$|\.tsv$|\.vsv$/)){
            alert('CSVファイルを選択してください');
            return;
        }
        $("#form_execution_add_result").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_add_result');
        $("#form_execution_add_result").attr('method', 'post');
        $("#form_execution_add_result").submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_add">棚卸実施登録</a></li>
        <li>棚卸実施登録確認</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>棚卸実施登録確認</span></h2>
<h2 class="HeaddingMid">棚卸実施ファイルを選択してください。</h2>
<form id="form_execution_add_result" action="#" method="post" class="validate_form" enctype="multipart/form-data" accept-charset="utf-8">
    <?php echo $this->form->input('TrnInventoryHeader.token' , array('type'=>'hidden' ))?>
    <?php echo $this->form->input('TrnInventoryHeader.time' , array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u') ))?>
    <?php echo $this->form->input('TrnInventoryHeader.id' ,array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>棚卸番号</th>
                <td><?php echo $this->form->input('TrnInventoryHeader.work_no' ,array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>棚卸実施名</th>
                <td><?php echo $this->form->input('TrnInventoryHeader.title' ,array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
            </tr>
            <tr>
                <th>棚卸種別</th>
                <td><?php echo $this->form->input('TrnInventoryHeader.inventory_type' ,array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
            </tr>
        </table>

        <hr class="Clear" />
        <h2 class="HeaddingSmall">実施日と実施ファイルを選択し、確定ボタンをクリックしてください。</h2>

        <table class="FormStyleTable">
            <tr>
                <th>実施日</th>
                <td><?php echo($this->form->text('Inventory.date',array('class'=>'r date validate[required,custom[date]]','maxlength'=>'10','id'=>'work_date','label'=>''))); ?></td>
            </tr>
            <tr>
                <th>実施ファイル</th>
                <td><?php echo $this->form->file('Inventory.csv_file',array('label'=>false,'div'=>false,'class'=>'r text validate[required]','style'=>'width:250px;','value'=>'','id'=>'file')); ?></td>
            </tr>
        </table>

        <div><?php echo (isset($data['errmsg']))?$data['errmsg']:""; ?></div>

        <div class="ButtonBox">
            <input type="button" class="btn btn2 fix_btn [p2]" id="btn_execution_add_result" />
        </div>
    </div>
</form>
