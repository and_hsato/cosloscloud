<script type="text/javascript">
$(function(){
    // 確認ボタン押下
    $("#btn_each_shelf_add_confirm").click(function(){
        if($(".TableStyle02 input:checked").length  > 0) {
            $("#add_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/each_shelf_add_confirm').submit();
        }else{
            alert('棚卸データがチェックされていません');
            return false;
        }
    });

    //チェックボックス一括制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
  
    // 検索ボタン押下
    $("#btn_search").click(function(){
        $("#add_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/each_shelf_add').submit();
    });
});

function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>棚別予定登録</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>棚別予定登録</span></h2>
<h2 class="HeaddingMid">棚卸を行いたい棚を検索してください。</h2>
<form class="validate_form input_form" id="add_form" action="#" method="post">
    <?php echo $this->form->input('inventory.isSearch',array('type'=>'hidden','value'=>'1','class'=>'search_form')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('inventory.facilityName',array('id' => 'facilityName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('inventory.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('inventory.facilityCode',array('options'=>$facility_list ,'id' => 'facilityCode','class'=>'txt','empty'=>true) ); ?>
                </td>
                <th width="20"></th>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('inventory.departmentName',array('id' => 'departmentName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('inventory.departmentText',array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('inventory.departmentCode', array('options'=>$department_list, 'id' =>'departmentCode', 'class' =>'txt','empty'=>true)); ?>
                </td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center"><input type="button" value="" class="btn btn1" id="btn_search" /></p>
        </div>
        <hr class="Clear" />
        <div align="right" id="pageTop" style="padding-right: 10px;"></div>
        <div id="hideer">
            <h2 class="HeaddingSmall">棚卸を行いたい棚を選択し、確認ボタンをクリックしてください。</h2>
            <div class="DisplaySelect"><?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?></div>
            <div class="TableHeaderAdjustment01">
                <table class="TableHeaderStyle02">
                    <tr>
                        <th width="20"><input type="checkbox" class="checkAll" /></th>
                        <th class="col80">施設　／　部署</th>
                        <th class="col20">棚番号</th>
                    </tr>
                </table>
            </div>
            <div class="TableScroll">
                <table class="TableStyle02 table-even">
                    <colgroup>
                        <col>
                        <col>
                        <col align="center">
                    </colgroup>
                    <?php $i = 0;?>
                    <?php foreach($SearchResult as $item ){ ?>
                    <tr>
                        <td width="20" class="center"><?php echo $this->form->input('inventory.id.'.$i.'',array('type'=>'checkbox','class'=>'chk center','value'=>$item['inventory']['id'],'checked'=>'' ,'hiddenField' => false)); ?></td>
                        <td class="col80">
                            <?php echo h_out($item['inventory']['facility_name'] . '／' . $item['inventory']['department_name']); ?>
                        </td>
                        <td class="col20"><?php echo h_out($item['inventory']['name'],'center'); ?></td>
                    </tr>
                    <?php $i++ ?>
                    <?php } ?>
                    <?php if(count($SearchResult) == 0 && isset($this->request->data['inventory']['isSearch'])){ ?>
                    <tr><td colspan="3">該当するデータはありません。</td></tr>
                    <?php } ?>
                </table>
            </div>
            <?php if(count($SearchResult) > 0) { ?>
            <div align="right" id="pageDow" ></div>
            <div class="ButtonBox">
                <input type="button" class="btn btn3 confirm_btn [p2]" id="btn_each_shelf_add_confirm" />
            </div>
            <?php } ?>
        </div>
    </div>
</form>
