<script type="text/javascript">
$(document).ready(function(){
    $('.checkAll_1').click(function(){
        $('#form_add_confirm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
  
    $("#btn_add_confirm").click(function(){
        if ($(".TableStyle02 input:checked").length > 0) {
            $("#form_add_confirm").attr('action', '<?php echo $this->webroot; ?>expirations/adjustment_confirm');
            $("#form_add_confirm").attr('method', 'post');
            $("#form_add_confirm").submit();
        }else{
            alert('明細データがチェックされていません');
            return false;
        }
    });

    $('.adjustment').click(function(){
        var id = $(this).val();
        if(id == 1){
            $("#adjustment_name").val('自家消費（在庫減）');
        }
        if(id == 2){
            $("#adjustment_name").val('雑損（在庫減）');
        }
        if(id == 3){
            $("#adjustment_name").val('自家消費（在庫増）');
        }
        if(id == 4){
            $("#adjustment_name").val('雑損（在庫増）');
        }
    });
});


  function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
  }
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/difference_list">差異一覧</a></li>
        <li>差異明細</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>差異明細</span></h2>
<h2 class="HeaddingMid">差異一覧の明細です。</h2>
<?php echo $this->form->create('Expiration',array('type'=>'post','action'=>'','id'=>'form_add_confirm','class'=>'validate_form input_form')); ?>
    <input type="hidden" name="data[Search][Url]" value="adjustment_search" id="SearchUrl" />
    <input type="hidden" name="data[search][item_id]" id="tempId" value="" />
    <input type="hidden" name="data[search][item_code]" id="tempId" value="" />
    <input type="hidden" name="data[search][lot_no]" id="tempId" value="" />
    <input type="hidden" name="data[search][item_name]" id="tempId" value="" />
    <input type="hidden" name="data[search][dealer_name]" id="tempId" value="" />
    <input type="hidden" name="data[search][standard]" id="tempId" value="" />
    <input type="hidden" name="data[search][stickers_id]" id="tempId" value="" />
    <input type="hidden" name="data[search][temp_id]" id="tempId" value="" />
    <?php echo $this->form->input('search.temp_id',array('type'=>'hidden' ,'id'=>'tempId'))?>
    <?php foreach($this->request->data['TrnInventoryHeader']['id'] as $i=>$id){ ?>
    <?php echo $this->form->input('TrnInventoryHeader.id.'.$i,array('type'=>'hidden' , 'value'=>$id)); ?>
    <?php } ?>
    <?php echo $this->form->hidden("Search.Url",array("value"=>"adjustment_search")); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>調整日</th>
                <td>
                    <?php echo $this->form->text('setting.date',array('class'=>'r validate[required,custom[date],funcCall2[date2]]','style'=>'width:80px','maxvalue'=>'10','id'=>'datepicker1'));?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('setting.classes',array('options'=>$classes,'class'=>'txt','style'=>'width:100px;','empty'=>true , 'id'=>'classId')); ?>
                    <?php echo $this->form->input('setting.classes_name',array('type'=>'hidden' , 'id'=>'className'));?>
                </td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('setting.facility_code',array('class'=>'r','style'=>'width:60px','id'=>'facilityText'));?>
                    <?php echo $this->form->input('setting.facilities',array('options'=>$facilities,'class'=>'r validate[required]','style'=>'width:150px;','empty'=>true,'id'=>'facilityCode')); ?>
                    <?php echo $this->form->input('setting.facility_name' , array('type'=>'hidden' , 'id'=>'facilityName'));?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->text('setting.remarks',array('class'=>'txt','style'=>'width:150px','maxlength'=>'200'));?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('setting.department_code',array('class'=>'r','style'=>'width:60px','id'=>'departmentText'));?>
                    <?php echo $this->form->input('setting.departments',array('options'=>array(),'class'=>'r validate[required]','style'=>'width:150px;','id'=>'departmentCode')); ?>
                    <?php echo $this->form->input('setting.department_name', array('type'=>'hidden','id'=>'departmentName'));?>
                </td>
               <td></td>
            </tr>
            <tr>
                <th>調整種別</th>
                <td colspan="4">
                    <input type="radio" id="adjustment1" name="data[setting][adjustment_type]" class="adjustment" value="1"  checked /><span>自家消費（在庫減）&nbsp;</span>
                    <input type="radio" id="adjustment2" name="data[setting][adjustment_type]" class="adjustment" value="2" /><span>雑損（在庫減）&nbsp;</span>
                    <input type="radio" id="adjustment3" name="data[setting][adjustment_type]" class="adjustment" value="3" /><span>自家消費（在庫増）&nbsp;</span>
                    <input type="radio" id="adjustment4" name="data[setting][adjustment_type]" class="adjustment" value="4" /><span>雑損（在庫増）</span>
                    <input type="hidden" name="data[setting][adjustment_name]" id="adjustment_name" value="自家消費（在庫減）">
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div align="right" id="pageTop" ></div>
    <div class="SearchBox">
        <hr class="Clear" />
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th style="width:20px;" rowspan="2"><input type="checkbox" class="checkAll_1" /></th>
                    <th class="col15">実施番号</th>
                    <th class="col10">商品ID</th>
                    <th class="col15">商品名</th>
                    <th class="col15">製品番号</th>
                    <th class="col5">包装単位</th>
                    <th class="col15">センターシール</th>
                    <th class="col15">論理配置場所</th>
                    <th class="col15">最終更新日</th>
                </tr>
                <tr>
                    <th>実施日</th>
                    <th>状態</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>差異数量</th>
                    <th>部署シール</th>
                    <th>実施配置場所</th>
                    <th>備考</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02 table-even2">
                <?php $i=0;?>
                <?php foreach($SearchResult as $item ){?>
                <tr>
                    <td style="width:20px;" rowspan="2" class="center" >
                        <?php echo $this->form->hidden("SelectedItem.Id.{$i}", array('value'=>$item['Inventory']['id']) ); ?>
                        <?php echo $this->form->checkbox("SelectedId.{$i}", array('class'=>'chk center checkAllTarget','value'=>$item['Inventory']['id'] , 'style'=>'width:20px;text-align:center;') ); ?>
                    </td>
                    <td class="col15"><?php echo h_out($item['Inventory']['work_no']); ?></td>
                    <td class="col10"><?php echo h_out($item['Inventory']['internal_code'],'center'); ?></td>
                    <td class="col15"><?php echo h_out($item['Inventory']['item_name']); ?></td>
                    <td class="col15"><?php echo h_out($item['Inventory']['item_code']); ?></td>
                    <td class="col5"><?php echo h_out($item['Inventory']['unit_name']); ?></td>
                    <td class="col15"><?php echo h_out($item['Inventory']['facility_sticker_no']); ?></td>
                    <td class="col15"><?php echo h_out($item['Inventory']['logical_place']); ?></td>
                    <td class="col15"><?php echo h_out($item['Inventory']['last_move_date']); ?></td>
                </tr>
                <tr>
                    <td><?php echo h_out($item['Inventory']['work_date']); ?></td>
                    <td><?php echo h_out($item['Inventory']['status']); ?></td>
                    <td><?php echo h_out($item['Inventory']['standard']); ?></td>
                    <td><?php echo h_out($item['Inventory']['dealer_name']); ?></td>
                    <td><?php echo h_out($item['Inventory']['diff_quantity'],'right'); ?></td>
                    <td><?php echo h_out($item['Inventory']['hospital_sticker_no']); ?></td>
                    <td><?php echo h_out($item['Inventory']['real_place']); ?></td>
                    <td>
                        <input type="text" class="txt" style="width:90%" >
                    </td>
                </tr>
                <?php $i++;?>
                <?php } ?>
            </table>
        </div>
        <div align="right" id="pageDow" ></div>
        <div class="ButtonBox">
            <input type="button" value="" class="btn btn52 [p2]" id="btn_add_confirm" />
        </div>
    </div>
</form>
