<script type="text/javascript">

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#confirmForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });


    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );
  
    //検索ボタン押下
    $("#btn_Search").click(function(){
        //カート欄にある全IDを取得
        var tempId = "";
        $("#confirmForm input[type=checkbox].checkAllTarget:checked").each(function(){
            if(tempId != ""){
                tempId += ",";
            }
            tempId += $(this).val();
        });
        $("#tempId").val(tempId);

        $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/each_commodity_add').submit();
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        var objArray = new Array();
        var isChecked = false;
        $('#itemSelectedTable').empty();
        $("#searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#SelectedTrnReceivingId' + cnt).val( $(this).val() );
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
                isChecked = true;
            }else{
                $('#SelectedTrnReceivingId' + cnt).val("0");
            }
            cnt++;
        });
        if(!isChecked){
            alert('商品を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        var tempId = "";
        $("#confirmForm input[type=checkbox].checkAllTarget:checked").each(function(){
            if(tempId != ""){
                tempId += ",";
            }
            tempId += $(this).val();
        });
        $("#tempId").val(tempId);

        //選択チェック
        if($('#confirmForm input[type=checkbox].checkAllTarget:checked').length > 0 ){
            $("#searchForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/each_commodity_add_confirm').submit();
        } else { 
            alert('棚卸する商品を選択してください');
        }
    });
});

function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}  
  
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li>商品別予定登録</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>商品別予定登録</span></h2>
<h2 class="HeaddingMid">棚卸を行いたい商品を検索してください。</h2>

<?php echo $this->form->create( 'Inventory',array('type' => 'post','action' => '' ,'id' =>'searchForm','class' => 'validate_form search_form') );?>
    <?php echo $this->form->input('search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('search.tempId',array('type'=>'hidden','id' =>'tempId'));?>

    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
            </colgroup>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('search.item_id',array('class'=>'txt search_internal_code','style'=>'width:120px')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('search.item_code',array('class'=>'txt search_upper','style'=>'width:120px')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('search.item_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('search.dealer_name',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('search.standard',array('class'=>'txt search_canna','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('search.jan_code',array('class'=>'txt','style'=>'width:120px')); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>

        <div class="ButtonBox">
           <p class="center">
             <input type="button" class="btn btn1" id="btn_Search"/>
           </p>
        </div>
    </div>
    <div align="right" id="pageTop" ></div>
        <div class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
        </div>

        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th class="center" ><input type="checkbox" class="checkAll_1"></th>
                    <th class="col10">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col20">規格</th>
                    <th class="col20">製品番号</th>
                    <th class="col20">販売元</th>
                    <th class="col10">包装単位</th>
                </tr>
            </table>
        </div>
  
        <div class="TableScroll" class="table-odd">
            <?php $_cnt=0;foreach($SearchResult as $_row): ?>
            <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row['inventory']['id'];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
                <tr id=<?php echo "itemSelectedRow".$_row['inventory']['id'];?> class="<?php echo($_cnt%2===0?'':'odd'); ?>">
                    <td class="center">
                        <?php echo $this->form->checkbox("SelectedTrnReceivingID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row['inventory']['id'] , 'style'=>'width:20px;text-align:center;') ); ?>
                    </td>
                    <td class="col10"><?php echo h_out($_row['inventory']['internal_code'],'center'); ?></td>
                    <td class="col20"><?php echo h_out($_row['inventory']['item_name']); ?></td>
                    <td class="col20"><?php echo h_out($_row['inventory']['standard']); ?></td>
                    <td class="col20"><?php echo h_out($_row['inventory']['item_code']); ?></td>
                    <td class="col20"><?php echo h_out($_row['inventory']['dealer_name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['inventory']['unit_name']); ?></td>
                </tr>
            </table>
            <?php $_cnt++;endforeach;?>
        </div>

        <div class="ButtonBox" style="margin-top:5px;">
            <p class="center">
                <input type="button" class="btn btn4" id="cmdSelect" />
            </p>
        </div>

        <?php echo $this->form->input('search.temp_id',array('type'=>'hidden','id' =>'temp_id'));?>
    
        <h2 class="HeaddingSmall">選択商品</h2>
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <colgroup>
                    <col width="20" />
                    <col width="30" />
                    <col width="90" />
                    <col width="100" />
                    <col width="100" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th class="center"><input type="checkbox" class="checkAll_2" checked="checked"></th>
                    <th class="col10">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col20">規格</th>
                    <th class="col20">製品番号</th>
                    <th class="col20">販売元</th>
                    <th class="col10">包装単位</th>
                </tr>
            </table>
        </div>
    
        <div class="TableScroll" style="" id="confirmForm">
            <?php $_cnt=0;foreach($CartSearchResult as $_row): ?>
            <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$_row['inventory']['id'];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
                <colgroup>
                    <col width="20" />
                    <col width="30" />
                    <col width="90" />
                    <col width="100" />
                    <col width="100" />
                    <col />
                    <col />
                </colgroup>
                <tr class="<?php echo($_cnt%2===0?'':'odd'); ?>" id=<?php "itemSelectedRow".$_row['inventory']['id'];?> >
                    <td class="center">
                        <?php echo $this->form->checkbox("SelectedTrnReceivingID.{$_cnt}", array('class'=>'center checkAllTarget','value'=>$_row['inventory']['id'] , 'style'=>'width:20px;') ); ?>
                    </td>
                    <td class="col10"><?php echo h_out($_row['inventory']['internal_code'],'center'); ?></td>
                    <td class="col20"><?php echo h_out($_row['inventory']['item_name']); ?></td>
                    <td class="col20"><?php echo h_out($_row['inventory']['standard']); ?></td>
                    <td class="col20"><?php echo h_out($_row['inventory']['item_code']); ?></td>
                    <td class="col20"><?php echo h_out($_row['inventory']['dealer_name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['inventory']['unit_name']); ?></td>
                </tr>
            </table>
            <?php $_cnt++;endforeach;?>
           <div id="itemSelectedTable"></div>
        </div>
    
        <div class="ButtonBox" style="margin-top:5px;">
            <p class="center">
                <input type="button" id="btn_Confirm" class="btn btn3" />
            </p>
        </div>
    </div>

<?php echo $this->form->end();?>
<div align="right" id="pageDow" ></div>
