<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/each_commodity_add/">商品別予定登録</a></li>
        <li class="pankuzu">商品別予定登録確認</li>
        <li>商品別予定登録結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>商品別予定登録結果</span></h2>
<div class="Mes01">以下の商品を対象に在庫を取得しました。</div>
<form>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>棚卸番号</th>
                <td><?php echo $this->form->input('inventory.work_no' , array('type'=>'text' , 'class'=>'lbl', 'readonly' => true));?></td>
            </tr>
            <tr>
                <th>棚卸実施名</th>
                <td><?php echo $this->form->input('inventory.title' , array('type'=>'text' , 'class'=>'lbl', 'readonly' => true));?></td>
            </tr>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('inventory.facilityName' , array('type'=>'text' , 'class'=>'lbl', 'readonly' => true));?></td>
            </tr>
            <tr>
                <th>作業区分</th>
                <td><?php echo $this->form->input('inventory.work_name' , array('type'=>'text' , 'class'=>'lbl', 'readonly' => true));?></td>
            </tr>
            <tr>
                <th>備考</th>
                <td><?php echo $this->form->input('inventory.recital' , array('type'=>'text' , 'class'=>'lbl', 'readonly' => true));?></td>
            </tr>
        </table>
        <hr class="Clear" />
        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th class="col20">施設　／　部署</th>
                    <th class="col10">棚名称</th>
                    <th class="col10">商品ID</th>
                    <th class="col15">商品名</th>
                    <th class="col15">規格</th>
                    <th class="col10">製品番号</th>
                    <th class="col15">販売元</th>
                    <th class="col10">基本単位</th>
                    <th class="col10">シール枚数</th>
                </tr>
            </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02 table-even">
                <?php $i=0; ?>
                <?php foreach($SearchResult as $_row){ ?>
                <tr>
                    <td class="col20"><?php echo h_out($_row['inventory']['facility_name']  .'/'. $_row['inventory']['department_name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['inventory']['name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['inventory']['internal_code'],'center'); ?></td>
                    <td class="col15"><?php echo h_out($_row['inventory']['item_name']); ?></td>
                    <td class="col15"><?php echo h_out($_row['inventory']['standard']); ?></td>
                    <td class="col10"><?php echo h_out($_row['inventory']['item_code']); ?></td>
                    <td class="col15"><?php echo h_out($_row['inventory']['dealer_name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['inventory']['unit_name']); ?></td>
                    <td class="col10"><?php echo h_out($_row['inventory']['count']); ?></td>
                </tr>
                <?php $i++;?>
                <?php } ?>
            </table>
        </div>
    </div>
</form>
