<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/each_shelf_add">棚別予定登録</a></li>
        <li class="pankuzu">棚別予定登録確認</li>
        <li>棚別予定登録結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>棚別予定登録結果</span></h2>
<div class="Mes01">以下の棚を対象に在庫を取得しました。</div>
    <table class="FormStyleTable">
        <tr>
            <th>棚卸番号</th>
            <td><?php echo $this->form->input('inventory.work_no' , array('type'=>'text' , 'class'=>'lbl', 'readonly' => true));?></td>
        </tr>
        <tr>
            <th>棚卸実施名</th>
            <td><?php echo $this->form->input('inventory.title' , array('type'=>'text' , 'class'=>'lbl', 'readonly' => true));?></td>
        </tr>
        <tr>
            <th>作業区分</th>
            <td><?php echo $this->form->input('inventory.work_name' , array('type'=>'text' , 'class'=>'lbl', 'readonly' => true));?></td>
        </tr>
        <tr>
            <th>備考</th>
            <td><?php echo $this->form->input('inventory.recital' , array('type'=>'text' , 'class'=>'lbl', 'readonly' => true));?></td>
        </tr>
    </table>
<br>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th width="20"></th>
                <th class="col80">施設　／　部署</th>
                <th class="col20">棚番号</th>
           </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even">
            <colgroup>
                <col>
                <col>
                <col align="center">
            </colgroup>
            <?php foreach( $SearchResult as $item ){ ?>
            <tr>
                <td width="20"></td>
                <td class="col80"><?php echo h_out($item['inventory']['facility_name'] . '／' . $item['inventory']['department_name']); ?></td>
                <td class="col20"><?php echo h_out($item['inventory']['name'],'center'); ?></td>
            </tr>
            <?php } ?>
      </table>
</div>
