<script type="text/javascript">
$(function(){
    // 差異リストボタン押下
    $("#btn_execution_diff_report").click(function(){
        $("#form_execution_diff_report").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/diff');
        $("#form_execution_diff_report").attr('method', 'post');
        $("#form_execution_diff_report").submit();
    });
    // CSVボタン押下
    $("#btn_execution_diff_csv").click(function(){
        $("#form_execution_diff_report").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv');
        $("#form_execution_diff_report").attr('method', 'post');
        $("#form_execution_diff_report").submit();
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/execution_add">棚卸実施登録</a></li>
        <li class="pankuzu">棚卸実施登録確認</a></li>
        <li>棚卸実施登録結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>棚卸実施登録結果</span></h2>
<div class="Mes01">実施情報の反映を行いました。</div>
<form id="form_execution_diff_report">
    <?php echo $this->form->input('TrnInventoryHeader.id' ,array('type'=>'hidden'));?>
    <input type="hidden" name="data[TrnInventory][mst_facility_id]" value="<?php echo $this->request->data['TrnInventory']['mst_facility_id']; ?>" />
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>棚卸番号</th>
                <td><?php echo $this->form->input('TrnInventoryHeader.work_no' ,array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>棚卸実施名</th>
                <td><?php echo $this->form->input('TrnInventoryHeader.title' ,array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
            </tr>
            <tr>
                <th>棚卸種別</th>
                <td><?php echo $this->form->input('TrnInventoryHeader.inventory_type' ,array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
            </tr>
        </table>
        <div class="Mes01"><?php echo $read_count ?>件取り込みました。</div>
        <div class="Mes01"><?php echo $err_count ?>件エラーが発生しました。</div>
        <?php
            if(count($errdata) > 0 && isset($errdata)){
                foreach ($errdata as $err):
        ?>
                <div class="Mes01"><?php echo($err['row_no']); ?>行目 <?php echo($err['sticker_value']); ?></div>
        <?php 
                endforeach;
            }
        ?>
        <hr class="Clear" />
        <div class="ButtonBox">
            <p class="center">
                <input type="button" value="　差異リスト 印字　" id="btn_execution_diff_report" />
                <input type="button" value="　差異リスト CSV　" id="btn_execution_diff_csv" />
            </p>
        </div>
    </div>
</form>
