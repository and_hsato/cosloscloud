<script type="text/javascript">
$(document).ready(function() {
    $("#read_btn").click(function(){
        <?php if($this->Session->read('Auth.Config.Shipping') == '0' ){ ?>
        $("#shippings_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_check');
        <?php } else { ?>
        $("#shippings_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_check2');
        <?php } ?>
        $("#shippings_form").attr('method','post');
        $("#shippings_form").submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>出荷照合</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>出荷照合検索</span></h2>
<h2 class="HeaddingMid">検索してください</h2>
<form id="shippings_form" class="validate_form input_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>出荷日</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.work_date',array('type'=>'text', 'class'=>'r validate[required,custom[date],funcCall2[date2]] date', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.className', array('type'=>'hidden' , 'id' => 'className')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.classId',array('options'=>$class_list,'id'=>'classId', 'class'=>'txt','empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <th>出荷先施設</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.facilityName',array('id' => 'facilityName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'txt r validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityCode',array('options'=>$facility_list,'id' => 'facilityCode', 'class' => 'txt r validate[required]','empty' => true)); ?>
                </td>
                <td width="20"></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnShippingHeader.recital',array('class'=>'validate[length[0,200]] txt')); ?></td>
            </tr>
            <tr>
                <th>出荷先部署</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.departmentName', array('id' => 'departmentName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentText', array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'txt r validate[required]', 'style' => 'width: 60px;')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentCode',array('options'=>array(),'id' => 'departmentCode', 'class' => 'txt r validate[required]','empty' => true)); ?>
                </td>
            </tr>
        </table>

        <div class="ButtonBox">
            <input type="button" class="btn btn17 submit [p2]" value="" id="read_btn"/>
        </div>
    </div>
</form>