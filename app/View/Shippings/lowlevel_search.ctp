<script type="text/javascript">
/**
 * validation engine.
 */
$(document).ready(function() { 
    //検索ボタン押下

    $("#search_btn").click(function(){
        $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/lowlevel_search').submit();
    });

    //選択ボタン押下
    $("#cmdSelect").click(function(){
        var tempId = $("#tempId").val();
        var isChecked = false;
        $("#search_form input[type=checkbox].chk:checked").each(function(){
            isChecked = true;

            //全選択用にチェックボックスのクラス変更
            $(this).removeClass('chk');
            $(this).addClass('allChk');

            //選択行を追加
            var id = $(this).parents('tr:eq(0)').attr("id");
            if($('#addTable tr').length%2 === 0){
                $(this).parents('tr:eq(0)').removeClass('odd').appendTo('#addTable');
                $("#D" + id).removeClass('odd').appendTo('#addTable');
            }else{
                $(this).parents('tr:eq(0)').removeClass('odd').addClass('odd').appendTo('#addTable');
                $("#D" + id).removeClass('odd').removeClass('odd').addClass('odd').appendTo('#addTable');
            }
  
            //hidden項目にID追加
            if(tempId == ''){
                tempId = $(this).val();
            }else{
                tempId += "," + $(this).val();
            }
        });
        $("#tempId").val(tempId);
    });

    //確認ボタン押下
    $("#confirm_btn").click(function(){
        var isChecked = false;
        var selectId = $("#selectId").val();
        $("#itemSelectedTable input[type=checkbox].allChk:checked").each(function(){
               if(selectId!=""){
                   selectId += ",";
               }
               selectId += $(this).val();
               isChecked = true;
        });

        $("#selectId").val(selectId);
        if(!isChecked){
            alert("商品が選択されていません");
            return false;
        }else{
            $("#search_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/lowlevel_confirm').submit();
        }
    });

    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('input[type=checkbox].allChk').attr('checked',$(this).attr('checked'));
    });
  
});

</script>


<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/ex_add">臨時出荷登録</a></li>
        <li>臨時出荷(低レベル品検索)</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>臨時出荷</span></h2>
<h2 class="HeaddingMid">臨時出荷を行いたい低レベル品を選択してください。</h2>
<form class="validate_form search_form" id="search_form" method="post" action="lowlevel_search">
    <?php echo $this->form->hidden('TrnShippingHeader.searchFlg',array("value" => "1")); ?>
    <?php echo $this->form->hidden('search.tempId',array('id'=>'tempId'));?>
    <?php echo $this->form->hidden('select.selectId',array('id'=>'selectId'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>出荷日</th>
                <td><?php echo $this->form->input('TrnShippingHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.classId', array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.className', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
            <tr>
                <th>出荷先施設</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.facilityCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnShippingHeader.recital', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?></td>
            </tr>
            <tr>
                <th>出荷先部署</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.departmentCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
        </table>

        <h2 class="HeaddingSmall">検索条件</h2>
        <table class="FormStyleTable">
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('search.internal_code',array('class'=>'txt search_upper search_internal_code'));?></td>
                <td width="20"></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('search.item_code',array('class'=>'txt search_upper'));?></td>
                <td width="20"></td>
                <th><?php echo $this->form->checkbox('search.fixed_promise',array('class' =>'center')); ?>引当済みのみ</th>
                <td></td>
                <td width="20"></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('search.item_name',array('class'=>'txt search_canna'));?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('search.dealer_name',array('class'=>'txt search_canna'));?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('search.standard',array('class'=>'txt search_canna'));?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('search.jan_code',array('class'=>'txt search_upper'));?></td>
                <td></td>
                <th></th>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn1" value="" id="search_btn"/></p>
    </div>

    <div id="results">
        <hr class="Clear" /> 
        <div class="DisplaySelect">
            <div align='right' style='margin-right:25px; float:right;'>
            </div>
            <?php echo $this->element('limit_combobox',array('param' =>'search.limit' , 'result'=>count($result)));?>
            <span class="BikouCopy"></span>
        </div>
        <div class="">
            <table class="TableStyle01" style="margin-bottom:0px">
                <tr>
                    <th style="width:20px;"rowspan="2" align="center">
                        <input type="checkbox" class="checkAll_1"/>
                    </th>
                    <th style="width:100px;">商品ID</th>
                    <th>商品名</th>
                    <th>製品番号</th>
                    <th>包装単位</th>
                    <th>在庫数</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>JANコード</th>
                    <th>売上単価</th>
                </tr>
            </table>
            <?php if(isset($this->request->data['TrnShippingHeader']['searchFlg']) &&  count($result) === 0){?>
            <table class="TableStyle01">
                <tr>
                    <td colspan=7 align='center'>商品データがありません</td>
                </tr>
            </table>
            <?php } ?>
            <table class="TableStyle01" id="searchTable" style="margin-top:0px">
            <?php $i = 0; foreach ($result as $r): ?>
                <tr class="<?php echo ($i%2 == 0)?'':'odd';?>" id="<?php echo $i; ?>">
                    <td style="width:20px; text-align:center" rowspan="2">
                    <?php if(!empty($r['TrnShipping']['sales_price']) && ($r['TrnShipping']['quantity'] > 0) ) {?>
                        <?php echo $this->form->checkbox('TrnSticker', array('class'=>'center chk', 'value'=>$r['TrnShipping']['id'])); ?>
                    <?php }?>
                    </td>
                    <td style="width:100px;"><?php echo h_out($r['TrnShipping']['internal_code'] , 'center'); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['item_name']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['item_code']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['unit_name']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['quantity']); ?></td>
                </tr>
                <tr class="<?php echo ($i%2 == 0)?'':'odd';?>" id="<?php echo "D".$i; ?>">
                    <td class="center"></td>
                    <td style="width:250px;">
                        <?php echo h_out($r['TrnShipping']['standard']); ?></td>
                    <td style="width:250px;"><?php echo h_out($r['TrnShipping']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['jan_code'],'center'); ?></td>
                    <td>
                        <?php if(is_null($r['TrnShipping']['sales_price'])){ ?>
                        売り上げ設定なし
                        <?php } else { ?>
                        <?php echo h_out($this->Common->toCommaStr($r['TrnShipping']['sales_price']),'right'); ?>
                        <?php } ?>
                    </td>
                </tr>
                <?php $i++; endforeach; ?>
             </table>
         </div>

         <div class="ButtonBox">
             <p class="center"><input type="button"  class="btn btn4" id="cmdSelect" /></p>
         </div>
             <h2 class="HeaddingSmall">選択商品</h2>
             <div class="TableScroll">
                 <table class="TableStyle01" style="margin-bottom:0px;">
                     <tr>
                         <th style="width:20px;"rowspan="2" align="center">
                             <input type="checkbox" class="checkAll_2" CHECKED/>
                         </th>
                         <th style="width:100px;">商品ID</th>
                         <th>商品名</th>
                         <th>製品番号</th>
                         <th>包装単位</th>
                         <th>在庫数</th>
                     </tr>
                     <tr>
                         <th></th>
                         <th>規格</th>
                         <th>販売元</th>
                         <th>JANコード</th>
                         <th>売上単価</th>
                     </tr>
                 </table>
                 <div class="TableStyle01 itemSelectedTable" id="itemSelectedTable">
                     <table class="TableStyle01" id="addTable" style="margin:0;padding:0;">
                         <?php $i = 0; foreach ($cart_result as $c): ?>
                         <tr class="<?php echo ($i%2 == 0)?'':'odd';?>" id="<?php echo $i; ?>">
                             <td style="width:20px; text-align:center" rowspan="2">
                                 <?php echo $this->form->checkbox('SelectItemid', array('class'=>'center allChk', 'value'=>$c['TrnShipping']['id'], "checked"=>"checked")); ?>
                             </td>
                             <td style="width:100px;"><?php echo h_out($c['TrnShipping']['internal_code'],'center'); ?></td>
                             <td><?php echo h_out($c['TrnShipping']['item_name']); ?></td>
                             <td><?php echo h_out($c['TrnShipping']['item_code']); ?></td>
                             <td><?php echo h_out($c['TrnShipping']['unit_name']);?></td>
                             <td><?php echo h_out($c['TrnShipping']['quantity']); ?></td>
                         </tr>
                         <tr class="<?php echo ($i%2 == 0)?'':'odd';?>" id="<?php echo $i; ?>">
                             <td></td>
                             <td style="width:250px;"><?php echo h_out($c['TrnShipping']['standard']); ?></td>
                             <td style="width:250px;"><?php echo h_out($c['TrnShipping']['dealer_name']); ?></td>
                             <td><?php echo h_out($c['TrnShipping']['jan_code'],'center'); ?></td>
                             <td><?php echo h_out($this->Common->toCommaStr($c['TrnShipping']['sales_price']),'right'); ?></td>
                         </tr>
                         <?php $i++; endforeach; ?>
                     </table>
                 </div>
             </div>
             <div class="ButtonBox" style="margin-top:10px;">
                 <input type="button" value="" id="confirm_btn" class="btn btn3 confirm_btn">
             </div>
             <div align='right' style='margin-right:25px; margin-top:10px;'>
             </div>
         </div>
         <h3 id='foot'></h3>
<?php echo $this->form->end(); ?>

