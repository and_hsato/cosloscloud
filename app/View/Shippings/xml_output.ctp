<printdata>
    <setting>
        <layoutname><?php echo($layout_name); ?></layoutname>
        <fixvalues>
<?php
  foreach($fix_value as $name => $data) {
    echo('<value name="'.$name.'">'.$data.'</value>');
  }
?>
        </fixvalues>
    </setting>
    <datatable>

<?php
  echo('<columns>'.$columns.'</columns>');
?>
<?php
  echo('<rows>');
  foreach($posted as $data) {
    $tabed = implode("\t", $data);
    echo('<row>'.h($tabed).'</row>');
  }
  echo('</rows>');
?>
    </datatable>
</printdata>
