<script type="text/javascript">
$(function(){
    //出荷伝票印刷ボタン押下
    $("#shipping_print_btn").click(function(){
        if(confirm("出荷伝票印刷を実行します。よろしいですか？")){
            $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/makeShipping').submit();
        }
        return;
    });

    //納品書印刷ボタン押下
    $("#invoice_print_btn").click(function(){
        if(confirm("納品書印刷を実行します。よろしいですか？")){
            $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/makeInvoice').submit();
        }
        return;
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/ex_add">臨時出荷登録</a></li>
        <li class="pankuzu">臨時出荷(低レベル品検索)</li>
        <li class="pankuzu">臨時出荷(低レベル品出荷)</li>
        <li>臨時出荷(低レベル品出荷確定)</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>臨時出荷</span></h2>
<h2 class="HeaddingMid">以下の商品を出荷しました</h2>

<?php echo $this->form->create(array('action'=>'print','type'=>'post','class'=>'validate_form','id' => 'result_form')); ?>
    <?php echo $this->form->hidden('TrnShipping.trn_shipping_header_id.0'); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <input type="hidden" name="TemporaryClaimType" value="<?php echo $this->Session->read('Auth.Config.TemporaryClaimType') ; ?>" >
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>出荷番号</th>
                <td><?php echo $this->form->input('TrnShippingHeader.work_no' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
            </tr>
            <tr>
                <th>出荷日</th>
                <td><?php echo $this->form->input('TrnShippingHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.classId', array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.className', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
            <tr>
                <th>出荷先施設</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.facilityCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnShippingHeader.recital', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?></td>
            </tr>
            <tr>
                <th>出荷先部署</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.departmentCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
        </table>
    </div>

    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th style="width:20px;" rowspan="2"></th>
                <th style="width:100px;">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th>包装単位</th>
                <th>在庫数</th>
                <th>備考</th>
            </tr>
            <tr>
                <th>管理区分</th>
                <th>規格</th>
                <th>販売元</th>
                <th>売上単価</th>
                <th>出荷数</th>
                <th></th>
            </tr>
        <?php $i = 0; foreach ($this->request->data['TrnShippingHeader']['ResultList'] as $row): ?>
            <tr>
                <td rowspan="2"></td>
                <td><?php echo h_out($row['TrnShipping']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($row['TrnShipping']['item_name']); ?></td>
                <td><?php echo h_out($row['TrnShipping']['item_code']); ?></td>
                <td><?php echo h_out($row['TrnShipping']['unit_name']);?></td>
                <td><?php echo h_out(number_format($row['TrnShipping']['quantity']),'right'); ?></td>
                <td rowspan="2">
                    <?php echo $this->form->text("input.remarks.{$row['TrnShipping']['id']}",array(  'type'=>'lbl', 'class'=>'lbl', "readonly"=>"readonly")); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($row['TrnShipping']['standard']); ?></td>
                <td><?php echo h_out($row['TrnShipping']['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($row['TrnShipping']['sales_price']),'right'); ?></td>
                <td>
                    <?php echo $this->form->text("input.quantity.{$row['TrnShipping']['id']}",array('type'=>'text','class'=>'lbl',"style"=>"text-align:right;","readonly" => "readonly")); ?>
                </td>
            </tr>
            <?php $i++; endforeach; ?>
        </table>
    </div>

    <div class="ButtonBox" style="margin-top:10px;">
        <input type="button" class="btn btn27 submit print_btn" id="shipping_print_btn" value="" />
        <input type="button" class="btn btn19 submit print_btn" id="invoice_print_btn" value="" />
    </div>
<?php echo $this->form->end();?>