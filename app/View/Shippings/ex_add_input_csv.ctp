<script type="text/javascript">
  $(document).ready(function() {
    $('#csv_file').focus();

    $("#shippings_read_csv_informations_btn").click(function(){
      if(!$('#csv_file').val()){
        alert('ファイルが選択されていません');
        return;
      } else if(!$('#csv_file').val().match(/\.csv$|\.tsv$/)){
        alert('CSVファイルを選択してください');
        return;
      }
      $("#shippings_read_csv_informations_form").attr('action', '<?php echo $this->webroot; ?>shippings/ex_add_confirm');
      $("#shippings_read_csv_informations_form").attr('method','post');
      $("#shippings_read_csv_informations_form").submit();
    });
  });

</script>
<style type="text/css">
  #csv_file{
    /*display:none;*/
  }
</style>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/ex_add">臨時出荷登録</a></li>
        <li>臨時出荷_ＣＳＶ読込</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>ＣＳＶ読込</span></h2>
<form class="validate_form" id="shippings_read_csv_informations_form" enctype="multipart/form-data" accept-charset="utf-8">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>出荷日</th>
                <td><?php echo $this->form->input('TrnShippingHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.classId', array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.className', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
            <tr>
                <th>出荷先施設</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.facilityCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
                <td width="20"></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnShippingHeader.recital', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?></td>
            </tr>
            <tr>
                <th>出荷先部署</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.departmentCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
            <tr>
                <th>CSVファイル</th>
                <td colspan="4">
                    <?php echo $this->form->input('csv_file',array('type'=>'file','class'=>'txt r validate[required]','style'=>'width:250px;','id'=>'csv_file')); ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn3 submit" value="" id="shippings_read_csv_informations_btn"></p>
    </div>
</form>