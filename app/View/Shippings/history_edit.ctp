<script type="text/javascript">
$(function(){
    $("#shippings_edit_history_btn").click(function(){
        if(!$('input.chk:checked').length){
            alert('明細情報は１つ以上選択してください。');
            return false;
        }
        if(window.confirm("取消を実行します。よろしいですか？")){
            $("#shippings_edit_history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history_edit_result').submit();
        }
    });
    $("#seal_btn").click(function(){
        if(!$('input.chk:checked').length){
            alert('明細情報は１つ以上選択してください。');
            return false;
        }
        if(window.confirm("部署シールを印刷します。よろしいですか？")){
            $("#shippings_edit_history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/print_hospital_sticker/2').submit();
        }
    });
  
    $('.all_check').click(function () {
        $('input.chk').attr('checked',$(this).attr('checked'));
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history">出荷履歴</a></li>
        <li>出荷履歴編集</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>出荷履歴編集</span></h2>
<h2 class="HeaddingMid">出荷履歴編集</h2>
<form class="validate_form" id="shippings_edit_history_form" method="post">
    <?php echo $this->form->input('Shipping.time' , array('type'=>'hidden' , 'value'=> date('Y/m/d H:i:s.u'))); ?>
    <?php echo $this->form->input('Shipping.token' , array('type'=>'hidden')); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2">
             <thead>
                <tr>
                    <th style="width:20px;" rowspan="2"><input type="checkbox" class="all_check" /></th>
                    <th class="col15">出荷番号</th>
                    <th class="col10">出荷日</th>
                    <th class="col5">商品ID</th>
                    <th class="col10">商品名</th>
                    <th class="col10">製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col10">ロット番号</th>
                    <th class="col15">センターシール</th>
                    <th class="col10">作業区分</th>
                    <th class="col10">更新者</th>
                </tr>
                <tr>
                    <th colspan="2">施設　／　部署</th>
                    <th>管理区分</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>販売単価</th>
                    <th>有効期限</th>
                    <th>部署シール</th>
                    <th colspan="2">備考</th>
                </tr>
                </thead>
                <tbody>
                <?php $cnt = 0;foreach($result as $r){ ?>
                <tr>
                    <td style="width:20px;" rowspan="2" class="center">
                        <?php if(is_null($r['TrnShipping']['alert_message'])){ ?>
                        <?php echo $this->form->input('TrnShipping.id.'.$cnt, array('type'=>'checkbox', 'class'=>'center chk', 'value'=> $r['TrnShipping']['id'] , 'hiddenField'=>false)); ?>
                        <?php } ?>
                    </td>
                    <td><?php echo h_out($r['TrnShipping']['work_no']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['work_date'],'center') ; ?></td>
                    <td><?php echo h_out($r['TrnShipping']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['item_name']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['item_code']); ?></td>
                    <td><?php echo h_out(number_format($r['TrnShipping']['quantity']) . $r['TrnShipping']['unit_name']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['lot_no']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['facility_sticker_no']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['work_type']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['user_name']); ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($r['TrnShipping']['facility_name'] . ' ／ ' . $r['TrnShipping']['department_name']) ; ?></td>
                    <td><?php echo h_out($shipping_type_list[$r['TrnShipping']['shipping_type']], 'center'); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['standard']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['dealer_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($r['TrnShipping']['price']),'right'); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['validate_date'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['hospital_sticker_no']); ?></td>
                    <td colspan="2">
                        <?php if(is_null($r['TrnShipping']['alert_message'])){ ?>
                        <?php echo $this->form->input('TrnShipping.recital.'.$r['TrnShipping']['id'] , array('type'=>'text' , 'class'=>'txt', 'value'=>$r['TrnShipping']['recital'])); ?>
                        <?php }else{ ?>
                        <label title="<?php echo $r['TrnShipping']['alert_message']; ?>" class="RedBold"><?php echo $r['TrnShipping']['alert_message']; ?></label>
                        <?php } ?>
                    </td>
                </tr>
                <?php $cnt++; } ?>
                </tbody>
            </table>
        </div>

        <div class="ButtonBox">
            <input type="button" class="btn btn6 [p2]" id="shippings_edit_history_btn" />
            <input type="button" class="btn btn16 [p2]" id="seal_btn" />
        </div>
    </div>
</form>