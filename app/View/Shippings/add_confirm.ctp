<script type="text/javascript">

$(document).ready(function() {
    //確定ボタン押下
    $("#shippings_accept_informations_btn").click(function(){
        if(!$('input[type=checkbox].chk:checked').length){
            alert('明細は1つ以上選択してください。');
           return;
        }
        $("#shippings_accept_informations_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_result').submit();
    });

    // 備考一括入力
    $('#recital_input_btn').click( function () {
        $('input.recital').val($('#recital_all_txt').val());
    });

    $('#all_check').click(function () {
        $('input.chk').attr('checked',$(this).attr('checked'));
    });
  
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add">出荷照合検索</a></li>
        <li class="pankuzu">出荷照合</li>
        <li>出荷照合確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>出荷照合確認</span></h2>
<form class="validate_form" id="shippings_accept_informations_form" method="post">
    <?php echo $this->form->input('Shipping.token' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('Shipping.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>出荷日</th>
                <td><?php echo $this->form->input('TrnShippingHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.classId', array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.className', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
            <tr>
                <th>出荷先施設</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.facilityCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnShippingHeader.recital', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?></td>
            </tr>
            <tr>
                <th>出荷先部署</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.departmentCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
        <tr>
            <td>表示件数：<?php echo count($result); ?>件</td>
            <td align="right"><input type="text" id="recital_all_txt" class="txt" style="width: 150px;" maxlength="200" ><input type="button" class="btn btn8 [p2]"  value="" id="recital_input_btn"></td>
        </tr>
    </table>

    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th rowspan="2" width="20"><?php echo $this->form->input('all_check',array('type'=>'checkbox','id'=>'all_check' , 'checked'=>'checked')) ?></th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col15">部署シール</th>
                <th class="col15">作業区分</th>
                <th class="col15">警告メッセージ</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>販売単価</th>
                <th>有効期限</th>
                <th>センターシール</th>
                <th colspan="2">備考</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even2">
        <?php $i=0; foreach($result as $r){ ?>
            <tr>
                <td width="20" rowspan="2" class="center">
                <?php if(!$r['err_flg']){ ?>
                <?php echo $this->form->input("TrnShipping.id.{$i}",array('type'=>'checkbox' ,'class'=>'chk','checked'=>(strlen($r['alert_message'])==0),'hiddenField'=>false, 'value'=>$r['trn_sticker_id'] . '_' . $r['trn_sticker_issue_id'] )); ?>
                <?php } ?>
                </td>
                <td class="col10" align="center"><?php echo h_out($r['item_id'],'center'); ?></td>
                <td><?php echo h_out($r['item_name']); ?></td>
                <td><?php echo h_out($r['item_code']); ?></td>
                <td class="col10"><?php echo h_out($r['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($r['lot_number']); ?></td>
                <td class="col15"><?php echo h_out($r['hospital_sticker_no']); ?></td>
                <td class="col15"><?php echo $this->form->input("TrnShipping.work_type.{$r['trn_sticker_id']}",array('options'=>$classes_enabled,'class'=>'txt' , 'empty'=>true)); ?></td>
                <td class="col15"><label title="<?php echo $r['alert_message']; ?>"><span class="RedBold"><?php echo $r['alert_message']; ?></span></label></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($r['standard']); ?></td>
                <td><?php echo h_out($r['dealer']); ?></td>
                <td class="right"><?php echo h_out($this->Common->toCommaStr($r['sales_price']),'right'); ?></td>
                <td align="center"><?php echo h_out($r['validated_date'],'center');?></td>
                <td><?php echo h_out($r['facility_sticker_no']); ?></td>
                <td  colspan="2">
                    <?php echo $this->form->input("TrnShipping.recital.{$r['trn_sticker_id']}",array('type'=>'text','class'=>'txt recital','maxlength'=>200)); ?>
                </td>
            </tr>
         <?php $i++; } ?>
      </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn2 submit" value="" id="shippings_accept_informations_btn" />
        </p>
    </div>
</form>
