<script type="text/javascript">
$(function(){
    $(document).ready(function() {
        //登録ボタン押下
        $("#regist_btn").click(function(){
            var isChecked = false;
            var submitFlg = true;
            $("#confirm_form input[type=checkbox].chk").each(function(){
                if($(this).attr("checked") == true){
                    isChecked = true;
                    //数値チェック
                    var quantity = $(this).parents('tr:eq(0)').next().find('input[type=text].quantity');
                    var sticker_quantity = $(this).parents('tr:eq(0)').find('input[type=hidden].sticker_quantity');
                    if(quantity.val() == ""){
                        alert("出荷数を入力してください");
                        quantity.focus();
                        exit;
                    }else if(quantity.val() == "0"){
                        alert("0以外の数字を入力してください");
                        quantity.focus();
                        exit; 
                    }else if(quantity.val().match( /[\D]/g ) ){
                        alert("正の整数を入力してください");
                        quantity.focus();
                        exit;
                    }else if(sticker_quantity.val() < parseInt(quantity.val())){
                        alert("出荷数が在庫数を超えています");
                        quantity.focus();
                        exit;
                    }
                }
            });
            if(!isChecked){
                alert("商品が選択されていません");
                return false;
            }else{
                if(submitFlg){
                    $(window).unbind('beforeunload');
                    $("#confirm_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/lowlevel_result').submit();
                }
            }
        });
        //残数一括設定
        $('#cmdSetRemainCount').click(function(){
            $('input[type=text].quantity').each(function(){
                $(this).val($(this).parent(':eq(0)').find('input[type=hidden]:eq(0)').val());
            });
        });

        $('.all_check').click(function () {
            $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
        });
    });
  
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });

});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/ex_add">臨時出荷登録</a></li>
        <li class="pankuzu">臨時出荷(低レベル品検索)</li>
        <li>臨時出荷(低レベル品出荷)</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>臨時出荷</span></h2>
<h2 class="HeaddingMid">出荷数量を入力してください。</h2>

<?php echo $this->form->create(array('action'=>'lowlevel_result','type'=>'post','class'=>'validate_form','id' => 'confirm_form')); ?>
    <?php echo $this->form->input('ExShipping.token',array('type' => 'hidden')); ?>
    <?php echo $this->form->input('ExShipping.time',array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s.u')))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>出荷日</th>
                <td><?php echo $this->form->input('TrnShippingHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.classId', array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.className', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
            <tr>
                <th>出荷先施設</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.facilityCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnShippingHeader.recital', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?></td>
            </tr>
            <tr>
                <th>出荷先部署</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.departmentCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="SelectBikou_Area">
        <span class="DisplaySelect">
        </span>
        <span class="BikouCopy">
            <input type="button" class="btn btn61 [p2]" id="cmdSetRemainCount"/>
        </span>
    </div>
    <div class="TableScroll2">
        <table class="TableStyle01 table-odd2">
            <tr>
                <th style="width:20px;"rowspan="2" align="center">
                    <input type="checkbox" class="all_check" checked="checked" />
                </th>
                <th style="width:100px;">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th>包装単位</th>
                <th>在庫数</th>
                <th>備考</th>
            </tr>
            <tr>
                <th>管理区分</th>
                <th>規格</th>
                <th>販売元</th>
                <th>売上単価</th>
                <th>出荷数</th>
                <th></th>
            </tr>
            <?php $i = 0; foreach ($result as $row): ?>
            <tr id="<?php echo $i; ?>">
                <td class="center" rowspan="2">
                <?php
                if($row['TrnShipping']['quantity'] !== "0"){
                    echo $this->form->checkbox("SelectItemid.{$i}", array('class'=>'center chk', 'value'=>$row['TrnShipping']['id'], 'checked'=>true ,'hiddenField'=>false));
                }
                ?>
                </td>
                <td><?php echo h_out($row['TrnShipping']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($row['TrnShipping']['item_name']); ?></td>
                <td><?php echo h_out($row['TrnShipping']['item_code']); ?></td>
                <td><?php echo h_out($row['TrnShipping']['unit_name']); ?></td>
                <td><?php echo h_out(number_format($row['TrnShipping']['quantity']),'right'); ?>
                    <?php echo $this->form->input('TrnShipping.quantity',array('type'=>'hidden' , 'value'=>$row['TrnShipping']['quantity'] , 'class'=>'sticker_quantity'))?>
               </td>
               <td rowspan="2">
                   <?php echo $this->form->text("input.remarks.{$row['TrnShipping']['id']}",array('type'=>'text','class'=>'txt', 'maxlength'=>'200')); ?>
               </td>
           </tr>
           <tr>
               <td class="center">
                   <?php if($row['TrnShipping']['quantity'] === "0"){?>
                   <span style="color:#FF0000;">在庫なし</span>
                   <?php }?>
               </td>
               <td><?php echo h_out($row['TrnShipping']['standard']); ?></td>
               <td><?php echo h_out($row['TrnShipping']['dealer_name']); ?></td>
               <td><?php echo h_out($this->Common->toCommaStr($row['TrnShipping']['sales_price']),'right'); ?></td>
               <td style="text-align:right;">
                   <?php echo $this->form->input("input.quantity.{$row['TrnShipping']['id']}",array('type'=>'text','class'=>'r txt quantity','maxlength'=>'10','style'=>'text-align:right; ime-mode:disabled; width:90%')); ?>
                   <?php echo $this->form->input('input.remain_count.'.$row['TrnShipping']['id'],array('type'=>'hidden','class'=>'remain_count','value'=>$row['TrnShipping']['remain_count']))?>
                   <?php echo $this->form->hidden("input.modified.{$row['TrnShipping']['id']}",array("value" => $row['TrnShipping']['modified'])); ?>
               </td>
            </tr>
            <?php $i++; endforeach; ?>
        </table>
    </div>

    <div class="ButtonBox" style="margin-top:10px;">
        <input type="button" class="btn btn29" id="regist_btn">
    </div>
<?php echo $this->form->end();?>
