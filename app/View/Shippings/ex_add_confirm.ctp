<script type="text/javascript">
$(function(){
    $(document).ready(function() {
        //確定ボタン押下
        $("#accept_btn").click(function(){
            if($('.chk:checked').length > 0){
                $(window).unbind('beforeunload');
                $("#accept_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ex_add_result').submit();
            }else{
                alert('明細は1つ以上選択してください。');
                return;
            }
        });

        //チェックボックス一括制御
        $('#all_check').click(function () {
            $('.chk').attr('checked',$(this).attr('checked'));
        });
  
        //備考一括入力
        $('#recital_input_btn').click( function () {
            $('input.recital').val($('#recital_all_txt').val());
        });
    });
    $(window).bind('beforeunload' ,function(){
        return '登録が完了していません。';
    });
  
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/ex_add">臨時出荷登録</a></li>
        <li class="pankuzu"><?php
      if($input_type === 'INPUT_STICKER'){
        ?>
            シール読込
      <?php
      } else {
      ?>
            CSV読込
      <?php } ?>
        </li>
        <li>臨時出荷確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>臨時出荷確認</span></h2>
<span class="RedBold"><?php echo isset($header_error) ? $header_error : ''; ?></span>

<form class="validate_form" id="accept_form" method="post">
    <?php echo $this->form->input('ExShipping.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('ExShipping.time', array('type'=>'hidden' , 'value'=>date('Y/m/d H:i:s'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>出荷日</th>
                <td><?php echo $this->form->input('TrnShippingHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.classId', array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.className', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                 </td>  
            </tr>
            <tr>
                <th>出荷先施設</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.facilityCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnShippingHeader.recital', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?></td>
            </tr>
            <tr>
                <th>出荷先部署</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.departmentCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
        </table>
    </div>
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
        <tr>
            <td>表示件数：<?php echo count($result); ?>件</td>
            <td align="right"><input type="text" id="recital_all_txt" class="txt" style="width: 150px;" maxlength="200" ><input type="button" class="btn btn8 [p2]"  value="" id="recital_input_btn"></td>
        </tr>
    </table>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th rowspan="2" width="20"><?php echo $this->form->input('all_check',array('type'=>'checkbox','id'=>'all_check')) ?></th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col10">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col15">部署シール</th>
                <th class="col15">作業区分</th>
            </tr>
            <tr>
                <th></th>
                <th>規格</th>
                <th>販売元</th>
                <th>販売単価</th>
                <th>有効期限</th>
                <th>センターシール</th>
                <th>備考</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even2">
            <?php foreach($result as $i=>$r){ ?>
            <tr>
                <td width="20" rowspan="2" class="center">
                    <?php if($r['TrnShipping']['is_check']){ ?>
                    <?php echo $this->form->checkbox('TrnShipping.id.'.$i , array('value'=>$r['TrnShipping']['id'] , 'class' => 'chk id' , 'checked'=>'checked' , 'hiddenField'=>false) ); ?>
                    <?php } ?>
                </td>
                <td class="col10" align="center"><?php echo h_out($r['TrnShipping']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($r['TrnShipping']['item_name']); ?></td>
                <td><?php echo h_out($r['TrnShipping']['item_code']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnShipping']['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnShipping']['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnShipping']['hospital_sticker_no']); ?></td>
                <td class="col15">
                    <?php echo $this->form->input('TrnShipping.classId.'.$r['TrnShipping']['id'],array('options'=>$class_list,'id'=>'classId', 'class'=>'txt','empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo h_out($r['TrnShipping']['standard']); ?></td>
                <td><?php echo h_out($r['TrnShipping']['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($r['TrnShipping']['sales_price']),'right'); ?></td>
                <td><?php echo h_out($r['TrnShipping']['validated_date'],'center'); ?></td>
                <td><?php echo h_out($r['TrnShipping']['facility_sticker_no']); ?></td>
                <td>
                    <?php if ($r['TrnShipping']['is_check']) { ?>
                    <?php echo $this->form->input('TrnShipping.recital.'.$r['TrnShipping']['id'] , array('type'=>'text','class'=>'txt','value'=>$r['TrnShipping']['alert'])); ?>
                    <?php } else {  ?>
                    <?php echo h_out($r['TrnShipping']['alert']); ?>
                    <?php }  ?>
                </td>
            </tr>
         <?php } ?>
        </table>
    </div>
    <?php if(!isset($header_error)){ ?>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn2 submit" id="accept_btn"/></p>
    </div>
    <?php } ?>
</form>
