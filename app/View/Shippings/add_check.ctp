<script type="text/javascript">

String.prototype.trim = function() {return this.replace(/^\s+|\s+$/g,"");}

/**
 * page default.
 */
$(document).ready(function() {
  $('#input_barcord').focus();
  if($('table.TableStyle02 .center:parent').length){
      $('#center_count').text($('table.TableStyle02 .center:parent').length);
  }
  if($('table.TableStyle02 .center:parent').length){
      $('#hospital_count').text($('table.TableStyle02 .hospital:parent').length);
  }
});

/**
 * page transfer.
 */
$(function(){
  $("#okayed_btn").click(function(){
    if($("table.TableStyle02 .error:parent").text()){
      alert('エラーが存在するため、登録できません');
      //フォーカスを戻す
      $('#input_barcord').focus();  
      return;
    }else if($("table.TableStyle02 .center:parent").length == 0 || $("table.TableStyle02 .center:parent").length != $("table.TableStyle02 .hospital:parent").length){
      alert('センターシールと部署シールは、対になるように1組以上入力して下さい。');
      //フォーカスを戻す
      $('#input_barcord').focus();  
      return;
    }

    $("table.TableStyle02 .center").each(function(){
      $("#okayed_form").append('<input type="hidden" name="data[facility_sticker_no][]" value="' + $(this).text() + '" />')
    });
    $("table.TableStyle02 .hospital").each(function(){
      $("#okayed_form").append('<input type="hidden" name="data[hospital_sticker_no][]" value="' + $(this).text() + '" />')
    });
    $("#okayed_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_confirm');
    $("#okayed_form").attr('method','post');
    $("#okayed_form").submit();
  });
});

/**
 * all clear.
 */
$(function(){
  $("#all_clear_btn").click(function(){
    $("#input_barcord").val('');
    $("table.TableStyle02 tr").remove();
    $("table.TableStyle02").append('<tr><td class="center code_list"></td><td class="hospital code_list"></td><td class="error error_list"></td></tr>');
    $("table.TableStyle02 tr").bind("click",function(){has = $(this).hasClass("selected");$(".selected:not(this)").removeClass("selected");if(!has){$(this).addClass("selected");}return;});
    $("#center_count").text('0');
    $("#hospital_count").text('0');
    $("input[type='hidden'][name='data[hospital_sticker_no][]']").remove();
    $("input[type='hidden'][name='data[facility_sticker_no][]']").remove();
    $('#input_barcord').focus();
  });
});

/**
 * クリックしたら行を選択状態にする
 */
$(function(){
  $("table.TableStyle02 tr").click(function(){
    has = $(this).hasClass("selected");
    $(".selected:not(this)").removeClass("selected");
    if(!has){
      $(this).addClass("selected");
    }
    return;
  });

  $(document).keydown(function(e){
    var key_code;

    key_code = e.keyCode ? e.keyCode : e.which;
    // 13 = Enterキー
    if(key_code  == 13 &&
       document.activeElement.id != 'input_barcord' &&
       document.activeElement.id != 'okayed_btn' &&
       document.activeElement.id != 'all_clear_btn'
    ){
        $("#okayed_form").attr('action', '<?php echo $this->webroot;?>shippings/beep');
        $("#okayed_form").attr('method','post');
        $("#okayed_form").submit();
        return false;
    }
  });


  
  $(document).keyup(function(e){
    var key_code;

    key_code = e.keyCode ? e.keyCode : e.which;
    // 46 = deleteキー
    if(key_code  == 46){
      $("table.TableStyle02 tr.selected").remove();
      $("#center_count").text($("table.TableStyle02 .center:parent").length);
      $("#hospital_count").text($("table.TableStyle02 .hospital:parent").length);
      if(!$("table.TableStyle02 tr").length){
        $("table.TableStyle02").append('<tr><td class="center code_list"></td><td class="hospital code_list"></td><td class="error error_list"></td></tr>');
        $("table.TableStyle02 tr").bind("click",function(){has = $(this).hasClass("selected");$(".selected:not(this)").removeClass("selected");if(!has){$(this).addClass("selected");}return;});      }
      e.stopPropagation();
      $('#input_barcord').focus();
      return false;
    }
  });
});


// input barcord
$(function(){
  var code_object = <?php echo $to_codes; ?>;
  $("#input_barcord").keydown(function(e){
    var barcode = '';
    var check_type = '';
    var type_name = '';
    var check_id = '';
    var object_id = '';
    var key_code;
    var add_error = '';
    var barcode_count = 0;
    var lot_no = '';
    var matches = '';
    var pure_jan;
    var check_degit;
    var pair_select;
    var code_table = '';
    var empty_td = '';
    var input_td = '';
    var item_code = '';
    var item_unit_id = '';
    var check_barcode = "";
    var quantity = 1;

    key_code = e.keyCode ? e.keyCode : e.which;
    if(key_code  == 13){
      barcode = $("#input_barcord").val().trim();
      $("#input_barcord").val('');
      // Janコードチェック
      if(barcode.match(/^01[0-9][0-9]{13}/)){
        check_type = 'jan_code';
        type_name = 'JANコード';
        check_id = "center";
        //JANコード取得
        check_barcode = barcode.substr(3,13);

        //********* 数量(入数)、ロット番号の取得処理 *********//
        if(barcode.substr(16,2) === "17"){
          //有効期限が設定されている場合(JANの後が「17」)、有効期限以降の取得
          if(barcode.substr(24,2) === "30"){
            //数量が設定されている場合(有効期限の後が「30」)、数量設定が終わっている場所(半角スペース)を調べる
            index = barcode.search(/\s/);
            //数量取得
            if(index === -1){
              //数量しか設定されていない場合、最後の半角スペースが取得できないのでindexに-1が入る
              quantity = parseInt(barcode.substring(26));
            }else{
              quantity = parseInt(barcode.substring(26,index));
            }
            //ロット番号取得
            if(index !== -1){
              //半角スペース以降がロット番号か確認
              if(barcode.substr(parseInt(index) + 1,2) === "10"){
                //ロット番号
                lot_no = barcode.substr(parseInt(index) + 3);
              }
            }
          }else if(barcode.substr(24,2) === "10"){
            //ロット番号が設定されている場合(有効期限の後が「10」)、ロット番号の設定が終わっている場所(半角スペース)を調べる
            index = barcode.search(/\s/);
            //ロット番号取得
            if(index === -1){
              //ロット番号しか設定されていない場合、最後の半角スペースが取得できないのでindexに-1が入る
              lot_no = barcode.substr(26);
            }else{
              lot_no = barcode.substring(26,index);
            }
            //数量取得
            if(index !== -1){
              //半角スペース以降が数量か確認
              if(barcode.substr(parseInt(index) + 1,2) === "30"){
                //数量
                quantity = parseInt(barcode.substr(parseInt(index) + 3));
              }
            }
          }
        }else if(barcode.substr(16,2) === "30"){
          //数量が設定されている場合(JANの後が「30」)
          index = barcode.search(/\s/);
          //数量
          if(index === -1){
            quantity = parseInt(barcode.substring(18));
          }else{
            quantity = parseInt(barcode.substring(18,index));
          }
          //数量設定以降の確認
          if(index !== -1){
            if(barcode.substr(parseInt(index) + 1,2) === "17"){
              //数量の後に効期限が設定されていた場合
              lot_no = barcode.substr(parseInt(index) + 11);
            }else if(barcode.substr(parseInt(index) + 1,2) === "10"){
              //数量の後にロット番号が設定されていた場合
              tempBarcode = barcode.substr(parseInt(index) + 3);
              index = tempBarcode.search(/\s/);
              //ロット番号取得
              if(index === -1){
                lot_no = tempBarcode;
              }else{
                lot_no = tempBarcode.substring(0,index);
              }
            }
          }
        }else if(barcode.substr(16,2) === "10"){
          //ロット番号が設定されている場合(JANの後が「10」)
          index = barcode.search(/\s/);
          if(index === -1){
            lot_no = barcode.substr(18);
          }else{
            lot_no = barcode.substring(18,index);
          }
          //ロット番号以降の確認
          if(index !== -1){
            if(barcode.substr(parseInt(index) + 1,2) === "17"){
              //ロット番号の後に有効期限が設定されていた場合
              tempBarcode = barcode.substr(parseInt(index) + 11);
              if(tempBarcode.length >= 0){
                quantity = parseInt(barcode.substr(parseInt(index) + 11));
              }
            }else if(barcode.substr(parseInt(index) + 1,2) === "30"){
              //ロット番号の後に数量が設定されていた場合
              tempBarcode = barcode.substr(parseInt(index) + 3);
              index = tempBarcode.search(/\s/);
              //数量取得
              if(index === -1){
                quantity = parseInt(tempBarcode);
              }else{
                quantity = parseInt(tempBarcode.substring(0,index));
              }
            }
          }
        }

        //チェックデジット計算＆JANの整合性確認
        check_degit = check_barcode.slice(-1);
//        if(!check_jan(check_barcode,check_degit)){
          // エラーメッセージ追加
//          add_error = add_error + 'このバーコードは不正です。';
//        }
      }else if(barcode === ''){
        //バーコード未入力
        alert("バーコードを入力してください。");
        //フォーカスを戻す
        $('#input_barcord').focus();
        return;
      }else{
        //シール番号の英字を大文字に
        check_barcode = barcode.toUpperCase();
      }

      //取得したJAN、Lot、数量(入数)で商品チェック
      //object_id = check_exit(code_object,check_type,barcode);
      object_id = check_exit(code_object,check_type,check_barcode,lot_no,quantity);
      if(object_id === false){
        //商品チェックNG
        check_id = 'center';
        add_error = add_error + '配送可能なデータが存在しません。';
      }else if(object_id === "QuantityError"){
        //包装単位に入数が違う場合
        check_id = 'center';
        add_error = add_error + '包装単位が違います。';
      }else{
        //商品チェックOKの場合、シールコード分け
        if(check_type !== 'jan_code'){
          if(code_object[object_id].facility_sticker_no.replace(/^[\s　]+|[\s　]+$/g, "") !== ''){
            check_type = 'facility_sticker_no';
            type_name = 'センターシール';
            check_id = "center";
          } else {
            check_type = 'hospital_sticker_no';
            type_name = '部署シール';
            check_id = "hospital";
          }

          //川鉄シールではない場合、重複チェック
          if(!check_unique(barcode,check_id)) {
            add_error = add_error + type_name + 'が重複しています。';
          }
        }

        // 予約済みチェック
        if(code_object[object_id].reserved_flag){
          add_error = add_error + type_name + 'の商品は予約済みです。';
        }
      }

      // 対になるセレクトID取得
      if(check_id === "center"){
        pair_select = "hospital";
      } else {
        pair_select = "center";
      }
      if(object_id !== false && object_id !== "QuantityError"){
        item_code = code_object[object_id].item_code;
        item_unit_id = code_object[object_id].item_unit_id;
      }
      // 読み込んだバーコード追加
      code_table = $("table.TableStyle02");
      if(!$("table.TableStyle02 ." + check_id+":last").text()){
        empty_td = $("table.TableStyle02 ." + check_id+":empty");
        empty_td.each(function(){
            input_td = $(this);
            return false;
        });
      }

      if(!input_td){
        code_table.append('<tr><td class="center code_list"></td><td class="hospital code_list"></td><td class="error error_list"></td></tr>');
        input_td = $("table.TableStyle02 ." + check_id+":last");
        input_td.parent('tr').bind("click",function(){has = $(this).hasClass("selected");$(".selected:not(this)").removeClass("selected");if(!has){$(this).addClass("selected");}return;});
      }

      input_td.text(barcode);
      input_td.attr('item_code',item_code);
      input_td.attr('item_unit_id',item_unit_id);

      if(input_td.siblings("."+pair_select).text() && add_error === ''){
        // 商品IDチェック
        if(input_td.attr('item_code') !== input_td.siblings("."+pair_select).attr('item_code')){
          add_error = add_error + '商品の照合が一致しません';
        // 包装単位IDチェック
        }else if(input_td.attr('item_unit_id') !== input_td.siblings("."+pair_select).attr('item_unit_id')){
          add_error = add_error + '包装単位の照合が一致しません';
        }
      }
      // エラー表示
      add_error = add_error.replace(/\n/g,"");
      input_td.siblings(".error").text(input_td.siblings(".error").text() + add_error);

      // 読み込みバーコード数をカウント
      barcode_count = $("table.TableStyle02 ."+ check_id+":parent").length;
      $("#" + check_id + "_count").text(barcode_count);

      // スクロール
      $('.TableScroll').scrollTop($('.TableScroll').innerHeight() * barcode_count);
  
      // エラー時ビープ音再生
      if(add_error !== ''){
        $("#okayed_form").attr('action', '<?php echo $this->webroot;?>shippings/beep');
        $("#okayed_form").attr('method','post');
        $("#okayed_form").submit();
        return false;
      }
    }
  });
});

// 商品存在チェック
function check_exit(check_object,check_type,check_barcode,lot_no,quantity){
  var object_id = false;
  var jan_item_list = [];
  var jan_item_count = 0;
  var standerd_item = [];
  var standerd_item_count = 0;
  for(var i=0; check_object.length > i; i++ ){
    if(check_type === 'jan_code'){
      //センターシール番号：空文字、かつJANコード・ロット番号が一致するものを取得する
      if( check_object[i].facility_sticker_no.replace(/^[\s　]+|[\s　]+$/g, "") === ''
       && check_object[i][check_type].replace(/^[\s　]+|[\s　]+$/g, "") === check_barcode.substr(0 , 12)
       && check_object[i].lot_no === lot_no){
        if(parseInt(quantity) === parseInt(check_object[i].per_unit)){
          object_id = i;
          break;
        }else{
          object_id = "QuantityError";
        }
        //break;
      }
    }else if(check_object[i].hospital_sticker_no.replace(/^[\s　]+|[\s　]+$/g, "")  !== '' && check_object[i].hospital_sticker_no === check_barcode ){
      object_id = i;
      break;
    }else if(check_object[i].facility_sticker_no.replace(/^[\s　]+|[\s　]+$/g, "") !== '' && check_object[i].facility_sticker_no === check_barcode ){
      object_id = i;
      break;
    }
  }
  return object_id;
}

// 重複チェック
function check_unique(check_barcode,check_id){
  var check_flag = true;
  $('table.TableStyle02 .' + check_id).each(function(){
    if($(this).text() === check_barcode){
      check_flag = false;
      return false;
    }
  })
  return check_flag;
}

// Janコードチェック
function check_jan(jan,check_degit){
  var evenSum = 0;
  var oddSum = 0;
  var sum;
  var check;
  jan = jan.split('').reverse().join('');
  for(var i = 1;i < jan.length; i++){
    if(i%2 === 0){
      evenSum += parseInt(jan.charAt(i));
    }else{
      oddSum += parseInt(jan.charAt(i));
    }
  }
  sum = String((oddSum * 3) + evenSum);
  check = 10 - parseInt(sum.charAt(sum.length - 1));
  if(check === 10){
    check = 0;
  }
  return check == check_degit;
}
</script>
<style type="text/css">
  .code_list{
    width:250px;
  }
  .error_list{
    width:400px;
  }
  table.TableHeaderStyle02 {
    font-size:14px;
    background-color:#fff;
    border:none;
  }
  table.TableHeaderStyle02 td{
    padding-top:20px;
    padding-left:5px;
  }
  .TableScroll{
    overflow-y:scroll;
    height:270px;
  }
  .error{
    color:red;
  }
  .selected{
    background-color: #bfd3ef;
  }
  table.TableStyle02 tr:hover{
    background-color: #bfd3ef;
  }
</style>
<div id="TopicPath">
  <ul>
    <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
    <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add">出荷照合検索</a></li>
    <li>出荷照合</li>
  </ul>
</div>

<h2 class="HeaddingLarge"><span>出荷照合</span></h2>
<h2 class="HeaddingMid">検索してください</h2>
<div class="Mes01"><?php echo $error; ?></div>

<form class="validate_form" id="okayed_form">
    <div id="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>出荷日</th>
                <td><?php echo $this->form->input('TrnShippingHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.classId', array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.className', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
            <tr>
                <th>出荷先施設</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.facilityCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnShippingHeader.recital', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?></td>
            </tr>
            <tr>
                <th>出荷先部署</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.departmentCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
                <td>&nbsp;</td>
                <th>&nbsp;</th>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <th>バーコード</th>
                <td colspan="4"><input type="text" class="r" id="input_barcord" style="width:200px;" value=""/></td>
            </tr>
        </table>
        <div class="TableHeaderAdjustment01">
           <table class="TableHeaderStyle02">
               <tr>
                   <td class="code_list">センターシール</td>
                   <td class="code_list">部署シール</td>
                   <td class="error_list">出荷照合エラーリスト</td>
               </tr>
           </table>
        </div>
        <div class="TableScroll">
            <table class="TableStyle02">
                <?php if(!isset($params['facility_sticker_no']) || !count($params['facility_sticker_no'])): ?>
                <tr>
                    <td class='center code_list'></td>
                    <td class='hospital code_list'></td>
                    <td class='error error_list'></td>
                </tr>
                <?php else:
                foreach($params['facility_sticker_no'] as $no_key => $no_code):
                ?>
                <tr>
                    <td class='center code_list'><?php echo $no_code; ?></td>
                    <td class='hospital code_list'><?php echo $params['hospital_sticker_no'][$no_key]; ?></td>
                    <td class='error error_list'></td>
                </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </table>
        </div>
        <table>
            <tr>
                <td class="code_list"><span id="center_count">0</span>件読込</td>
                <td class="code_list"><span  id="hospital_count">0</span>件読込</td>
                <td class="error_list"><input type="button" id="all_clear_btn" class="btn btn13"/></td>
           </tr>
        </table>
        <br/>
        <div class="ButtonBox">
            <input type="button" class="btn btn3 confirm_btn" id="okayed_btn" />
        </div>
     </div>
</form>