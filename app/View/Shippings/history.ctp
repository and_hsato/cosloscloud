<script type="text/javascript">
$(function(){
    //検索ボタン押下
    $("#shipping_history_search_btn").click(function(){
        $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history').submit();
    });
    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv').submit();
        $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history');
    });

    //明細ボタン押下
    $("#shippings_read_history_btn").click(function(){
        if($('input[type=checkbox].chk:checked').length === 0){
            alert('ヘッダ情報は１つ以上選択してください。');
            return false;
        }
        $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history_edit').submit();
    });

    //出荷伝票印字ボタン押下
    $("#shipping_print_btn").click(function(){
        if($('input[type=checkbox].chk:checked').length === 0){
            alert('ヘッダ情報は１つ以上選択してください。');
            return false;
        }
        if(confirm("出荷リスト印刷を実行します。よろしいですか？")){
            $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/makeShipping').submit();
        }
        return;
    });

    //納品書ボタン押下
    $("#invoice_print_btn").click(function(){
        if($('input[type=checkbox].chk:checked').length === 0){
            alert('ヘッダ情報は１つ以上選択してください。');
            return false;
        }
        // 印刷情報チェック
        if($('input[type=checkbox].invoice_chk:checked').length === 0){
            alert("印刷可能な出荷データではありません");
            return false;
        } else {
            if(confirm("納品書印刷を実行します。よろしいですか？")){
                $("#history_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/makeInvoice').submit();
                return false;
            }
        }
    });
    //チェックボックス一括制御
    $('.all_check').click(function () {
        $('input.chk').attr('checked',$(this).attr('checked'));
    });

    $('img.scroll').click(function(){
        var to_id = $(this).attr('to_id');
        var obj = $('#'+to_id);
        var offset = obj.offset();
        scrollTo(0,offset.top);
    });
});

</script>
<style type="text/css">
  .text{
   width:120px;
   }
   .view_code{
     width:60px;
   }
</style>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>出荷履歴</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>出荷履歴</span></h2>
<h2 class="HeaddingMid">出荷履歴</h2>
<form id="history_form" class="validate_form search_form input_form" action="<?php echo $this->webroot; ?>shippings/history" method="POST">
    <?php echo $this->form->input('facility_from_id' , array('type'=>'hidden','value'=>$this->Session->read('Auth.facility_id_selected'))) ?>
    <?php echo $this->form->input('User.user_id' , array('type'=>'hidden','value'=>$this->Session->read('Auth.MstUser.id'))) ?>
    <?php echo $this->form->input('search.is_search' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <?php echo $this->form->input('TemporaryClaimType', array('type'=>'hidden' ,'value'=>$this->Session->read('Auth.Config.TemporaryClaimType'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>出荷番号</th>
                <td>
                    <?php echo $this->form->input('search.work_no',array('type'=>'text', 'class'=>'txt')); ?>
                </td>
                <td width="20"></td>
                <th>出荷日</th>
                <td>
                    <span>
                        <?php echo $this->form->input('search.start_date',array('type'=>'text','class'=>'date validate[optional,custom[date]]','id'=>'datepicker1',"maxlength"=>10)); ?>
                        <span>&nbsp;～&nbsp;</span>
                        <?php echo $this->form->input('search.end_date',array('type'=>'text', 'class'=>'date validate[optional,custom[date]]', 'id'=>'datepicker2',"maxlength"=>10)); ?>
                   </span>
                </td>
                <td width="20"></td>
                <td colspan="2">
                    <?php echo $this->form->input('search.is_deleted',array('type'=>'checkbox','hiddenField'=>false)); ?>取消も表示する
                </td>
            </tr>
            <tr>
                <th>出荷先施設</th>
                <td>
                    <?php echo $this->form->input('search.facilityName',array('id' => 'facilityName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('search.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('search.facilityCode',array('options'=>$facilities_enabled,'id' => 'facilityCode', 'class' => 'txt','empty' => true)); ?>
                </td>
                <td></td>
                <th>出荷先部署</th>
                <td>
                    <?php echo $this->form->input('search.departmentName', array('id' => 'departmentName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('search.departmentText', array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'txt', 'style' => 'width: 60px;')); ?>
                    <?php echo $this->form->input('search.departmentCode',array('options'=>$departments,'id' => 'departmentCode', 'class' => 'txt','empty' => true)); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('search.internal_code',array('type'=>'text', 'class'=>'txt search_internal_code')); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('search.item_code',array('type'=>'text', 'class'=>'txt search_upper')); ?></td>
                <td></td>
                <th>ロット番号</th>
                <td><?php echo $this->form->input('search.lot_no',array('type'=>'text', 'class'=>'txt')); ?></td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('search.item_name',array('type'=>'text', 'class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('search.dealer_name',array('type'=>'text', 'class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>管理区分</th>
                <td>
                    <?php echo $this->form->input('search.shipping_type',array('options'=>$shipping_type_list,'class' => 'txt' , 'empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('search.standard',array('type'=>'text', 'class'=>'txt search_canna')); ?></td>
                <td></td>
                <th>シール番号</th>
                <td><?php echo $this->form->input('search.sticker_no',array('type'=>'text', 'class'=>'txt')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('search.work_type',array('options'=>$classes_enabled,'class'=>'txt' , 'empty'=>true)); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="shipping_history_search_btn" />&nbsp;&nbsp;
                <input type="button" class="btn btn5" id="btn_Csv"/>
            </p>
        </div>
        <div id="results">
            <div align="right" id="pageTop" ></div>
            <?php echo $this->element('limit_combobox',array('result'=>count($result))); ?>
            <div class="TableScroll" >
                <table class="TableStyle01 table-even">
                    <thead>
                    <tr>
                        <th style="width:20px;"><input type="checkbox" class="all_check" /></th>
                        <th>出荷番号</th>
                        <th style="width:85px;">出荷日</th>
                        <th class="col20">施設　／　部署</th>
                        <th class="col10">登録者</th>
                        <th>登録日時</th>
                        <th class="col20">備考</th>
                        <th class="col10">件数</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $cnt = 0;foreach($result as $r){ ?>
                    <tr>
                        <td align="center">
                            <?php if($r['TrnShipping']['type_sum'] > 0 ){ ?>
                            <?php echo $this->form->input('TrnShipping.trn_shipping_header_id.'.$cnt , array('type'=>'checkbox','class'=>'center chk invoice_chk' , 'value'=>$r['TrnShipping']['id'] , 'hiddenField' => false) ); ?>
                            <?php } else { ?>
                            <?php echo $this->form->input('TrnShipping.trn_shipping_header_id.'.$cnt , array('type'=>'checkbox','class'=>'center chk' , 'value'=>$r['TrnShipping']['id'] , 'hiddenField' => false ) ); ?>
                            <?php } ?>
                        </td>
                        <td><?php echo h_out($r['TrnShipping']['work_no'],'center'); ?></td>
                        <td><?php echo h_out($r['TrnShipping']['work_date'],'center'); ?></td>
                        <td><?php echo h_out($r['TrnShipping']['facility_name'] . ' ／  '. $r['TrnShipping']['department_name']); ?></td>
                        <td><?php echo h_out($r['TrnShipping']['user_name']); ?></td>
                        <td><?php echo h_out($r['TrnShipping']['created'],'center'); ?></td>
                        <td><?php echo h_out($r['TrnShipping']['recital']); ?></td>
                        <td><?php echo h_out($r['TrnShipping']['count'] .'/'.$r['TrnShipping']['detail_count'],'right');?></td>
                    </tr>
                    <?php $cnt++; } ?>
                    <?php if(count($result)==0 && isset($this->request->data['search']['is_search'])){?>
                    <tr>
                        <td colspan="8" class="center">該当するデータがありませんでした</td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div align="right" id="pageDow"></div>
            <?php if(count($result)>0){?>
            <div class="ButtonBox">
                <p class="center"><input type="button" class="btn btn7 submit" id="shippings_read_history_btn"/>
                    <input type="button" class="btn btn27 print_btn" id="shipping_print_btn" />
                    <input type="button" class="btn btn19" id="invoice_print_btn" />
                </p>
            </div>
            <?php } ?>
        </div>
    </div>
</form>
