<script type="text/javascript">
$(document).ready(function() {
    // シール読込ボタン押下
    $("#shippings_input_seal_btn").click(function(){
        $("#ex_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ex_add_input_sticker');
        $("#ex_form").attr('method','post');
        $("#ex_form").submit();
    });
    // CSV取込ボタン押下
    $("#shippings_input_csv_btn").click(function(){
        $("#ex_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ex_add_input_csv');
        $("#ex_form").attr('method','post');
        $("#ex_form").submit();
    });
    // 低レベル品一覧ボタン押下
    $("#shippings_lowlevel_search_btn").click(function(){
        $("#ex_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/lowlevel_search');
        $("#ex_form").attr('method','post');
        $("#ex_form").submit();
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li>臨時出荷</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>臨時出荷登録</span></h2>
<form id="ex_form" class="validate_form input_form">
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>出荷日</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.work_date',array('type'=>'text', 'class'=>'r date validate[required,custom[date],funcCall2[date2]]', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10'));  ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.className', array('type'=>'hidden' , 'id' => 'className')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.classId',array('options'=>$class_list,'id'=>'classId', 'class'=>'txt','empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <th>出荷先施設</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.facilityName',array('id' => 'facilityName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityText',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'txt r validate[required]', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityCode',array('options'=>$facility_list,'id' => 'facilityCode', 'class' => 'txt r validate[required]','empty' => true)); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnShippingHeader.recital',array('class'=>'validate[length[0,200]] txt')); ?></td>
            </tr>
            <tr>
                <th>出荷先部署</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.departmentName', array('id' => 'departmentName','type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentText', array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'txt r validate[required]', 'style' => 'width: 60px;')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentCode',array('options'=>array(),'id' => 'departmentCode', 'class' => 'txt r validate[required]','empty' => true)); ?>
                </td>
             </tr>
         </table>
     </div>
     <div class="ButtonBox">
         <p class="center">
             <input type="button" class="btn btn17 submit" id="shippings_input_seal_btn" />
             <input type="button" class="btn btn82 submit" id="shippings_lowlevel_search_btn" />
         </p>
     </div>
</form>