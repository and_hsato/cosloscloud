<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history">出荷履歴</a></li>
        <li class="pankuzu">出荷履歴編集</li>
        <li>出荷履歴編集結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>出荷履歴編集結果</span></h2>
<div class="Mes01">以下の出荷の取消を行いました。</div>
<form class="validate_form">
    <div class="SearchBox">
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2">
                <thead>
                <tr>
                    <th style="width:20px;" rowspan="2"></th>
                    <th class="col15">出荷番号</th>
                    <th class="col10">出荷日</th>
                    <th class="col5">商品ID</th>
                    <th class="col10">商品名</th>
                    <th class="col10">製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col10">ロット番号</th>
                    <th class="col15">センターシール</th>
                    <th class="col10">作業区分</th>
                    <th class="col10">更新者</th>
                </tr>
                <tr>
                    <th colspan="2">施設　／　部署</th>
                    <th>管理区分</th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>販売単価</th>
                    <th>有効期限</th>
                    <th>部署シール</th>
                    <th colspan="2">備考</th>
                </tr>
                </thead>
                <hbody>
                <?php $cnt = 0;foreach($result as $r){ ?>
                <tr>
                    <td style="width:20px;" rowspan="2" class="center">
                    </td>
                    <td><?php echo h_out($r['TrnShipping']['work_no']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['work_date'],'center') ; ?></td>
                    <td style="width:70px;"><?php echo h_out($r['TrnShipping']['internal_code'] , 'center'); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['item_name']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['item_code']); ?></td>
                    <td><?php echo h_out(number_format($r['TrnShipping']['quantity']) . $r['TrnShipping']['unit_name']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['lot_no']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['facility_sticker_no']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['work_type']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['user_name']); ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($r['TrnShipping']['facility_name'] . ' ／ ' . $r['TrnShipping']['department_name']) ; ?></td>
                    <td style="width:70px;"><?php echo h_out($shipping_type_list[$r['TrnShipping']['shipping_type']],'center'); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['standard']); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['dealer_name']); ?></td>
                    <td><?php echo h_out($this->Common->toCommaStr($r['TrnShipping']['price']),'right'); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['validate_date'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnShipping']['hospital_sticker_no']); ?></td>
                    <td colspan="2"><?php echo h_out($r['TrnShipping']['recital']); ?></td>
                </tr>
                <?php $cnt++;} ?>
                </tbody>
            </table>
        </div>
    </div>
</form>