<script type="text/javascript">
$(function(){
    // 出荷伝票印字ボタン押下
    $("#shipping_print_btn").click(function(){
        if(confirm("出荷リスト印刷を実行します。よろしいですか？")){
            $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/makeShipping');
            $("#result_form").attr('method','post');
            $("#result_form").submit();
        }
        return;
    });
    // 納品書印字ボタン押下
    $("#invoice_print_btn").click(function(){
        if(confirm("納品書印刷を実行します。よろしいですか？")){
            $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/makeInvoice');
            $("#result_form").attr('method','post');
            $("#result_form").submit();
        }
        return;
    });
    // 未出荷一覧ボタン押下
    $("#unshippinglist_print_btn").click(function(){
        $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_result2/#unShippingList');
        $("#result_form").attr('method','post');
        $("#result_form").submit();
        return;
    });
    // 部署シール印字ボタン押下
    $("#hospital_sticker_print_btn").click(function(){
        if(confirm("部署シール印刷を実行します。よろしいですか？")){
            $("#result_form").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/seal/print_hospital_sticker');
            $("#result_form").attr('method','post');
            $("#result_form").submit();
        }
        return;
    });
  
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add">出荷照合検索</a></li>
        <li class="pankuzu">出荷照合シール読み込み</li>
        <li class="pankuzu">出荷照合確認</li>
        <li>出荷照合結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>出荷照合結果</span></h2>

<div class="Mes01">出荷照合が完了しました</div>
<form class="validate_form" id="result_form">
    <?php echo $this->form->input('TrnShipping.trn_shipping_header_id.0' , array('type'=>'hidden'))?>
    <?php echo $this->form->input('Shipping.UnShippingList', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('Shipping.token', array('type'=>'hidden')); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>出荷日</th>
                <td><?php echo $this->form->input('TrnShippingHeader.work_date' , array('type'=>'text' , 'class'=>'lbl' , 'readonly'=>true));?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.classId', array('type'=>'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.className', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
            <tr>
                <th>出荷先施設</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.facilityCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.facilityName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnShippingHeader.recital', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?></td>
            </tr>
            <tr>
                <th>出荷先部署</th>
                <td>
                    <?php echo $this->form->input('TrnShippingHeader.departmentCode', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentId', array('type' => 'hidden')); ?>
                    <?php echo $this->form->input('TrnShippingHeader.departmentName', array('class'=>'lbl', 'readonly'=>'readonly', 'style' =>'width:150px;')); ?>
                </td>
            </tr>
        </table>
    </div>
    <hr class="Clear" />
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
        <tr>
            <td>表示件数：<?php echo count($result); ?>件</td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th rowspan="2" width="20"></th>
                <th class="col10">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th class="col15">包装単位</th>
                <th class="col10">ロット番号</th>
                <th class="col15">部署シール</th>
                <th class="col15">作業区分</th>
            </tr>
            <tr>
                <th>管理区分</th>
                <th>規格</th>
                <th>販売元</th>
                <th>販売単価</th>
                <th>有効期限</th>
                <th>センターシール</th>
                <th>備考</th>
            </tr>
        </table>
    </div>
    <div class="TableScroll">
        <table class="TableStyle02 table-even2">
        <?php foreach($result as $r){ ?>
            <tr>
                <td width="20" rowspan="2"></td>
                <td class="col10"><?php echo h_out($r['TrnShipping']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($r['TrnShipping']['item_name']); ?></td>
                <td><?php echo h_out($r['TrnShipping']['item_code']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnShipping']['unit_name']); ?></td>
                <td class="col10"><?php echo h_out($r['TrnShipping']['lot_no']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnShipping']['hospital_sticker_no']); ?></td>
                <td class="col15"><?php echo h_out($r['TrnShipping']['work_type_name']); ?></td>
            </tr>
            <tr>
                <td><?php echo h_out($r['TrnShipping']['shipping_type_name'] , 'center'); ?></td>
                <td><?php echo h_out($r['TrnShipping']['standard']); ?></td>
                <td><?php echo h_out($r['TrnShipping']['dealer_name']); ?></td>
                <td><?php echo h_out($this->Common->toCommaStr($r['TrnShipping']['sales_price']),'right'); ?></td>
                <td><?php echo h_out($r['TrnShipping']['validated_date'],'center'); ?></td>
                <td><?php echo h_out($r['TrnShipping']['facility_sticker_no']); ?></td>
                <td><?php echo h_out($r['TrnShipping']['recital']) ; ?></td>
            </tr>
         <?php } ?>
        </table>
    </div>

    <div class="ButtonBox">
        <p class="center">
            <input type="button" class="btn btn27 print_btn" id="shipping_print_btn"/>
            <?php if($invoice_print_flg){ ?>
            <input type="button" class="btn btn19 print_btn" id="invoice_print_btn" />
            <?php } ?>
            <input type="button" class="btn btn88 print_btn" id="unshippinglist_print_btn" />
            <input type="button" class="btn btn16 submit print_btn" id="hospital_sticker_print_btn" />
        </p>
    </div>
</form>


<?php if(isset($unShippingList)){?>
<a name='unShippingList'>
    <h2 class="HeaddingLarge"><span>未出荷一覧</span></h2>
</a>
<table style="width: 100%;">
    <tr>
        <td>表示件数：<?php echo count($unShippingList); ?>件</td>
        <td align="right"></td>
    </tr>
</table>
<div class="TableHeaderAdjustment01">
    <table class="TableHeaderStyle02">
        <tr>
            <th width="40">区分</th>
            <th class="col10">商品ID</th>
            <th>商品名</th>
            <th>製品番号</th>
            <th>規格</th>
            <th class="col10">包装単位</th>
            <th class="col15">販売元</th>
        </tr>
    </table>
</div>
<div class="TableScroll">
    <table class="TableStyle02 table-even">
    <?php if(count($unShippingList)>0){ ?>
        <?php $i=1 ; foreach($unShippingList as $item){?>
        <tr>
            <td width="40"><?php echo h_out($item['UnShipping']['promise_type'],'center'); ?></td>
            <td class="col10" align="center"><?php echo h_out($item['UnShipping']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($item['UnShipping']['item_name']); ?></td>
            <td><?php echo h_out($item['UnShipping']['item_code']); ?></td>
            <td><?php echo h_out($item['UnShipping']['standard']); ?></td>
            <td class="col10"><?php echo h_out($item['UnShipping']['unit_name']); ?></td>
            <td class="col15"><?php echo h_out($item['UnShipping']['dealer_name']); ?></td>
        </tr>
        <?php $i++; } ?>
    <?php }else{ ?>
        <tr>
            <td class="center"> 未出荷はありません。 </td>
        </tr>
    <?php } ?>
    </table>
  </div>
<?php } ?>
