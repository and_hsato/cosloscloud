<script>
function ajaxValidationCallback(status, form, json, options){
    if (status === true) {
        //チェックOKだったら、送信する
        validateDetachSubmit($('#userinfo'), '<?php echo $this->webroot; ?><?php echo $this->name; ?>/update/' );
    }
}

$(document).ready(function(){
    $('#password').addClass("r validate[required]");
    $('#password2').addClass("r validate[required,equals[password]]");
    $("#userinfo").validationEngine({
        ajaxFormValidation: true,
        onAjaxFormComplete: ajaxValidationCallback,
        validationEventTrigger: 'blur'
    });
});
</script>

<?php
$secret_q = $this->request->data['MstUser']['secret_q'];
$secret_a = $this->request->data['MstUser']['secret_a'];
$questions = Configure::read('secretQuestions');
if (is_null($secret_q)) $secret_q = $questions[0];
if (is_null($secret_a)) $secret_a = $this->request->data['MstUser']['user_name'];

$secret_questions = array();
foreach (Configure::read('secretQuestions') as $val) {
    $secret_questions[$val] = $val; 
}
?>

<div id="content-wrapper">
    <div id="TopicPath">
        <ul>
            <li><a href="<?php echo $this->webroot; ?>">TOP</a></li>
            <li>Myページ</li>
        </ul>
    </div>
    <h2 class="HeaddingLarge"><span>Myページ編集</span></h2>
    <h2 class="HeaddingMid">Myページ情報を編集できます。</h2>
    <h3 class="conth3">Myページ情報を編集</h3> 
    <?php echo $this->form->create('null', array('url' => array('controller' => 'MyPage', 'action' =>'submitCheckUserId'), 'id' => 'userinfo')) ?>
    <?php echo $this->form->input('MstUser.id', array('type'=>'hidden')) ?>
    <?php echo $this->form->input('MstUser.mst_facility_id', array('type'=>'hidden')) ?>
    <div class="SearchBox">
        <div style="float:left;">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                </colgroup>
                <tr>
                    <th>利用者ID</th>
                    <td>
                        <?php echo $this->form->input('MstUser.login_id' , array('type'=>'hidden')) ?>
                        <?php echo $this->request->data['MstUser']['login_id'] ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>利用者名</th>
                    <td>
                        <?php echo $this->form->input('MstUser.user_name', array('type'=>'text', 'class'=>'validate[required] search_canna r', 'maxlength'=>100)) ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>部署設定</th>
                    <td>
                        <?php echo $this->form->input('MstDepartment.department_code', array('id'=>'departmentText', 'type'=>'text', 'class'=>'r validate[required] txt', 'style'=>'width: 60px;', 'value' => $this->request->data['MstDepartment']['department_code'])) ?>
                        <?php echo $this->form->input('MstDepartment.department_name', array('id'=>'departmentCode', 'options'=>$department_list,'class' => 'r validate[required] txt', 'empty'=>'', 'value' => $this->request->data['MstDepartment']['department_code'])); ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>パスワード</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password', array('type'=>'password', 'id'=>'password', 'maxlength'=>'20')) ?>
                    </td>
                </tr>
                <tr>
                    <th>パスワード(確認用)</th>
                    <td>
                        <?php echo $this->form->input('MstUser.password2', array('type'=>'password', 'id'=>'password2', 'maxlength'=>'20', 'value' => $this->request->data['MstUser']['password'])) ?>
                    </td>
                </tr>
                <?php if ($is_lite_user) {?>
                <tr>
                    <th>秘密の質問</th>
                    <td>
                        <?php echo $this->form->input('MstUser.secret_q', array('options'=>$secret_questions, 'id'=>'departmentCode', 'class' => 'r validate[required] txt', 'empty'=>'', 'value' => $secret_q)); ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th>秘密の答え</th>
                    <td>
                        <?php echo $this->form->input('MstUser.secret_a', array('type'=>'text', 'class'=>'validate[required] search_canna r', 'maxlength'=>20, 'value' => $secret_a)) ?>
                    </td>
                    <td></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>

    <div class="ButtonBox">
        <input type="button" class="common-button" id="btn_back" value="戻る"/>
        <input type="submit" value="更新" class="common-button [p2]" />
    </div>
    <?php echo $this->form->end() ?>
</div>

