    <div class="MesBox1">
        <h2><span>ダウンロードサイト</span></h2>
        <dl style="font-size:large;">
            <dt>AQUA-Vブラウザー</dt>
            <dd>
            	<br/>
            	<p>
            	<a href="<?php echo $this->webroot; ?>internalsites/download/1">インストーラー(Ver 1.1.8)</a>
            	</p>
            	<p>
            	<a href="<?php echo $this->webroot; ?>internalsites/download/2">セットアップマニュアル</a>
            	</p>
            	<p>
            	<a href="<?php echo $this->webroot; ?>internalsites/download/9">操作マニュアル</a>
            	</p>
        	</dd>
        	<br/>
        	<br/>
        	<dt>ハンディーターミナル</dt>
        	<dd>
        		<br/>
        		<p>
        		<a href="<?php echo $this->webroot; ?>internalsites/download/3">データ転送ソフト</a>
        		</p>
            	<p>
            	<a href="<?php echo $this->webroot; ?>internalsites/download/4">データ転送ソフト_セットアップマニュアル</a>
            	</p>
            	<p>
            	<a href="<?php echo $this->webroot; ?>internalsites/download/5">BT-1000_ユーザーズマニュアル</a>
            	</p>
            	<p>
            	<a href="<?php echo $this->webroot; ?>internalsites/download/10">BT-1000_システムファイル</a>
            	</p>
            	<p>
            	<a href="<?php echo $this->webroot; ?>internalsites/download/8">PDAプログラム</a>
            	</p>
            	<p>
            	<a href="<?php echo $this->webroot; ?>internalsites/download/6">PDAプログラム_アップデートマニュアル</a>
            	</p>
            	<p>
            	<a href="<?php echo $this->webroot; ?>internalsites/download/7">AQUA-V_PDA操作マニュアル</a>
            	</p>
           </dd>
        </dl>
    </div>
