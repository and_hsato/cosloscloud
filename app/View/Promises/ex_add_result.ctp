<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/extraordinary/">臨時要求入力</a></li>
        <li class="pankuzu">臨時要求入力確認</li>
        <li>臨時要求登録結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>臨時要求登録結果</span></h2>
<div class="Mes01">臨時要求を登録しました</div>
<form>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <th>要求番号</th>
                <td><?php echo $this->form->input('TrnPromise.work_no',array('class' =>'lbl','style'=>'width:150px','readOnly'=>'readOnly')); ?></td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>要求日</th>
                <td><?php echo $this->form->text('TrnPromise.work_date',array('class'=>'lbl','readOnly'=>'readOnly')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('TrnPromise.work_type',array('options' => $this->request->data['TrnPromise']['workType'] ,'class'=>'lbl', 'disabled'=>'disabled' , 'empty'=>'')); ?></td>
            </tr>
            <tr>
                <th>施設</th>
                <td><?php echo $this->form->input('TrnPromise.facility_name',array('style'=>'width:150px;','class'=>'lbl','readOnly'=>'readOnly')); ?></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->input('TrnPromise.recital',array('style'=>'width:150px;','class'=>'lbl','readOnly'=>'readOnly')); ?></td>
            </tr>
            <tr>
                <th>部署</th>
                <td><?php echo $this->form->input('TrnPromise.department_name',array('style'=>'width:150px;','class'=>'lbl','readOnly'=>'readOnly')); ?></td>
            </tr>
        </table>

        <hr class="Clear" />
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2">
                <thead>
                <tr>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th class="col20">製品番号</th>
                    <th class="col20">包装単位</th>
                    <th class="col10">数量</th>
                    <th class="col10">作業区分</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>在庫数</th>
                    <th colspan="2">備考</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($result as $r) { ?>
                <tr>
                    <td align='center'><?php echo h_out($r['TrnPromise']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnPromise']['item_name']); ?></td>
                    <td><?php echo h_out($r['TrnPromise']['item_code']); ?></td>
                    <td><?php echo h_out($r['TrnPromise']['unit_name']); ?></td>
                    <td><?php echo $this->form->text('TrnPromise.quantity.'.$r['TrnPromise']['id'],array('class'=>'num lbl','style'=>'width:90%','readOnly'=>'readOnly'  )); ?></td>
                    <td><?php echo $this->form->input('TrnPromise.work_type_.'.$r['TrnPromise']['id'] ,array('options'=> $this->request->data['TrnPromise']['workType'],'class' => 'lbl' , 'disabled'=>'disabled' , 'empty'=>'')); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo h_out($r['TrnPromise']['standard']); ?></td>
                    <td><?php echo h_out($r['TrnPromise']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['TrnPromise']['stock_count']); ?></td>
                    <td colspan="2"><?php echo $this->form->text('TrnPromise.recital_.'.$r['TrnPromise']['id'],array('class'=>'lbl','style'=>'width:90%','readOnly'=>'readOnly'  )); ?></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</form>