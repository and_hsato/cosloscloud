<script type="text/javascript">
$(function(){
    $("#promises_read_csv_informations_btn").click(function(){
        if(!$('#csv_file').val()){
            alert('ファイルが選択されていません');
            return;
        } else if(!$('#csv_file').val().match(/\.csv$|\.tsv$/)){
            alert('CSVファイルを選択してください');
            return;
        }
        $("#promises_read_csv_informations_form").attr('action', "<?php echo $this->webroot; ?><?php echo $this->name; ?>/ex_add_confirm");
        $("#promises_read_csv_informations_form").attr('method','post');
        $("#promises_read_csv_informations_form").submit();
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/extraordinary/">臨時要求作成</a></li>
        <li>ＣＳＶ読込</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>ＣＳＶ読込</span></h2>
<form class="validate_form" id="promises_read_csv_informations_form" enctype="multipart/form-data" accept-charset="utf-8">
    <input type="hidden" name="data[selected][from_csv]" value="1" />
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.facility_name' , array('class'=>'lbl' , 'style'=>'width:150px;' , 'readonly'=>'readonly')) ?>
                    <?php echo $this->form->input('TrnPromise.facility_code' , array('type'=>'hidden')) ?>
                    <?php echo $this->form->input('TrnPromise.facility_id' , array('type'=>'hidden')) ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.work_name' , array('class'=>'lbl' , 'style'=>'width:150px;' , 'readonly'=>'readonly')) ?>
                    <?php echo $this->form->input('TrnPromise.work_type' , array('type'=>'hidden') )?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.department_name' , array('class'=>'lbl' , 'style'=>'width:150px;' , 'readonly'=>'readonly')) ?>
                    <?php echo $this->form->input('TrnPromise.department_code' , array('type'=>'hidden')) ?>
                    <?php echo $this->form->input('TrnPromise.department_id' , array('type'=>'hidden')) ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.recital' , array('class'=>'lbl' , 'style'=>'width:150px;'))?>
                </td>
            </tr>
            <tr>
                <th>要求日</th>
                <td>
                    <?php echo $this->form->text('TrnPromise.work_date' , array('class'=>'lbl' , 'readonly'=>'readonly'))?>
                </td>
            </tr>
            <tr>
                <th>CSVファイル</th>
                <td>
                    <?php echo $this->form->input('selected.csv_file',array('type'=>'file','class'=>'txt r validate[required]','style'=>'width:250px;','id'=>'csv_file')); ?>
                </td>
            </tr>
        </table>
    </div>
</form>
<div class="ButtonBox">
    <p class="center"><input type="button" class="btn btn3 submit"  id="promises_read_csv_informations_btn" ></p>
</div>
