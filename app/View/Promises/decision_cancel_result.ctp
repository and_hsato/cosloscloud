<script type="text/javascript">
function anc2(){
   obj = $('#pageDow')[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

function anc(id){
   obj = $('#'+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/decision_history/">引当作成履歴</a></li>
        <li class="pankuzu">引当作成履歴編集</li>
        <li>引当作成履歴編集結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>引当作成履歴編集結果</span></h2>
<div class="Mes01">引当の取消が完了しました</div>

<form>
    <table style="width:100%;">
        <tr>
            <td></td>
            <td align="right" id="pageTop" ></td>
        </tr>
    </table>
    <div class="TableScroll2">
        <table class="TableStyle01 table-even2">
            <thead>
            <tr>
                <th style="width: 150px;">引当番号</th>
                <th style="width:80px;">引当日</th>
                <th class="col10">商品ID</th>
                <th class="col15" style="width:280px;">商品名</th>
                <th class="col10" style="width:120px;">製品番号</th>
                <th class="col10">包装単位</th>
                <th style="width:70px;">数量</th>
                <th style="width:80px;">作業区分</th>
                <th style="width:70px;">更新者</th>
            </tr>
            <tr>
                <th colspan="2">施設　／　部署</th>
                <th>状態</th>
                <th>規格</th>
                <th>販売元</th>
                <th></th>
                <th>残数</th>
                <th colspan="2">備考</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($result as $r) { ?>
            <tr>
                <td><?php echo h_out($r['TrnFixedPromise']['work_no']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['work_date'],'center'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['item_name']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['item_code']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['unit_name']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['quantity'],'right'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['work_name']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['user_name']); ?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo h_out($r['TrnFixedPromise']['facility_name'] . '/' . $r['TrnFixedPromise']['department_name']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['fixed_promise_status'],'center'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['standard']);?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['dealer_name']); ?></td>
                <td></td>
                <td><?php echo h_out($r['TrnFixedPromise']['remain_count'],'right'); ?></td>
                <td colspan="2"><?php echo h_out($r['TrnFixedPromise']['recital'],'right'); ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <table style="width:100%;" id="pageDow">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
<?php echo $this->form->end(); ?>