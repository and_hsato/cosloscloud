<script type="text/javascript">
$(function($){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
       $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    //検索ボタン押下
    $('#search_btn').click(function(){
        $("#promiseForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/select/');
        $("#promiseForm").submit();
    });
    //作成ボタン押下
    $("#result_btn").click(function(){
       if( $("input.chk:checked").length > 0){
            $("#promiseForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/add_result/');
            $("#promiseForm").submit();
        } else {
            alert('作成対象を選択してください。');
        }
    });
});
function anc(id){
   obj = $("#"+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}
function anc2(){
   obj = $("#pageDow")[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li>要求作成部署選択</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>要求作成部署選択</span></h2>
<?php echo $this->form->create('promise',array('class'=>'validate_form','type'=>'post','id'=>'promiseForm')); ?>
    <?php echo $this->form->input('promise.time' , array('type' =>'hidden' ,'value'=> date('Y/m/d H:i:s')))?>
    <?php echo $this->form->input('TrnPromise.token' , array('type' =>'hidden' ))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>使用定数</th>
                <td>
                    <?php
                        echo $this->form->text('promise.fixed_num_name',array('class' =>'lbl', 'readOnly' => 'readonly'));
                        echo $this->form->text('promise.fixed_num',array('type'=>'hidden','class' =>'lbl'));
                    ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php
                        echo $this->form->text('promise.work_type_name',array('class' => 'lbl','readOnly' =>'readonly' ));
                        echo $this->form->text('promise.work_type',array('type'=>'hidden','class' => 'lbl'));
                    ?>
                </td>
            </tr>
            <tr>
                <th>要求日</th>
                <td><?php echo $this->form->text('promise.work_date',array('class' => 'lbl' ,'readonly' =>'readonly' ));  ?> </td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->text('promise.recital',array('class' => 'lbl' ,'readonly' =>'readonly')); ?></td>
            </tr>
            <tr>
                <th>得意先</th>
                <td><?php echo $this->form->input('promise.facilities',array('options' => $selectIds, 'class' => 'txt' , 'style' => 'width:150px;','empty'=>true)); ?></td>
            </tr>
        </table>
        <?php echo $this->form->text('promise.is_search',array('type' => 'hidden','id' => 'is_search')); ?>
        <?php echo $this->form->text('promise.from_select',array('type' => 'hidden','id' => 'from_select','value'=>'1' )); ?>
        <div class="ButtonBox">
            <input type="button" class="btn btn1" id="search_btn" />
        </div>
        <?php if(isset($this->request->data['promise']['is_search'])){ ?>
        <div align="right" id="pageTop" style="padding-right: 10px;"></div>
        <hr class="Clear" />
        <div id="results">
            <div class="TableScroll">
                <table class="TableStyle01 table-even">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                             <?php echo $this->form->input('TrnPromise.checkAll' , array('class' => 'checkAll' , 'type' => 'checkbox' , 'checked' => true));?>
                        </th>
                        <th>施設/部署</th>
                        <th class="col20" style="width:200px;">前回作成日時</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i = 0;
                        foreach($searchResult as $ret){
                    ?>
                    <tr>
                        <td style="width:20px;" class="center">
                            <?php echo $this->form->input("TrnPromise.id.{$i}",array('type'=>'checkbox','checked' =>true, 'class' =>'center chk' , 'value'=>$ret['TrnPromise']['id'] , 'hiddenField'=>false) ); ?>
                        </td>
                        <td><?php echo h_out($ret['TrnPromise']['department']); ?></td>
                        <td class="col20" style="width:200px;"><?php echo h_out($ret['TrnPromise']['last_date']) ; ?></td>
                    </tr>
                    <?php $i++; } ?>
                    <?php if(isset($this->request->data['promise']['is_search']) && count($searchResult) == 0){?>
                    <tr>
                        <td colspan="3" class="center">該当するデータがありませんでした</td>
                    </tr>
                    <?php  } ?>
                    </tbody>
                </table>
            </div>
            <?php if(count($searchResult) >= 1){ ?>
            <div class="ButtonBox" id="pageDow">
                <input type="button" value="" class="btn btn73" id="result_btn" />
            </div>
            <div align="right"></div>
            <?php } ?>
        </div>
    <?php } ?>
    </div>
<?php echo $this->form->end(); ?>