<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/add/">要求作成部署選択</a></li>
        <li>要求作成結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>要求作成結果</span></h2>
<div class="Mes01">引当要求を作成しました</div>
<form>
    <table class="FormStyleTable">
        <tr>
            <th>使用定数</th>
            <td><?php echo $this->form->text('promise.fixed_num_name',array('class' => 'lbl' ,'readonly' => 'readonly' ));  ?></td>
            <td width="20"></td>
            <th>作業区分</th>
            <td><?php echo $this->form->text('promise.work_type_name',array('class' =>'lbl','readonly' =>'readonly' )); ?></td>
        </tr>
        <tr>
            <th>要求日</th>
            <td><?php echo $this->form->text('promise.work_date',array('class' =>'lbl','readonly' =>'readonly' )); ?></td>
            <td></td>
            <th>備考</th>
            <td><?php echo $this->form->text('promise.recital',array('class' =>'lbl','readonly' =>'readonly' )); ?></td>
        </tr>
    </table>
    <hr class="Clear" />
    <div class="TableScroll">
        <table class="TableStyle01 table-even">
            <thead>
            <tr>
                <th class="col15">要求作成番号</th>
                <th class="col15">施設</th>
                <th class="col15">部署</th>
                <th class="col15">消費シール枚数</th>
                <th class="col15">要求シール枚数</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($result as $r){ ?>
            <tr>
                <td class="col15"><?php echo h_out($r[0]['work_no']); ?></td>
                <td class="col15"><?php echo h_out($r[0]['facility_name']); ?></td>
                <td class="col15"><?php echo h_out($r[0]['department_name']); ?></td>
                <td class="col15"><?php echo h_out($r[0]['consume_count'],'right'); ?></td>
                <td class="col15"><?php echo h_out($r[0]['promise_request_count'],'right'); ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</form>
