<script type="text/javascript">

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll_1').click(function(){
        $('#promiseForm #searchForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('.checkAll_2').click(function(){
        $('#promiseForm #cartForm input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    // ENTERで送信
    $( 'input[type=text]').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            if(form.hasClass('search_form')){ // 検索画面だったら
                $("#btn_Search").click();
            }
        }
    } );
  
    //検索ボタン押下
    $("#btn_Search").click(function(){
        //カート欄にある全IDを取得
        var tmpId = "";
        $("#cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                if(tmpId === '' ){
                    tmpId = $(this).val();
                }else{
                    tmpId +="," +  $(this).val();
                }
            }
        });
        $("#tmpId").val(tmpId);

        $("#promiseForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ex_select').submit();
    });

    //選択ボタン押下
    $('#cmdSelect').click(function(){
        var cnt = 0;
        $('#itemSelectedTable').empty();
        $("#promiseForm #searchForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                $('#containTables'+$(this).val()).clone().appendTo('#itemSelectedTable');
                cnt++;
            }
        });
        if(cnt === 0 ){
            alert('商品を選択してください');
        }
    });

    //確認ボタン押下
    $('#btn_Confirm').click(function(){
        //カート欄にある全IDを取得
        //選択チェック
        var cnt = 0;
        var tmpId = '';
        $("#promiseForm #cartForm input[type=checkbox].checkAllTarget").each(function(){
            if($(this).attr("checked") == true){
                cnt++;
                if(tmpId === ''){
                    tmpId = $(this).val();
                }else{
                    tmpId = tmpId + ','+$(this).val();
                }
            }
        });
        $('#tmpId').val(tmpId);
        if(cnt === 0){
            alert('商品を選択してください');
        }else{
            $("#promiseForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ex_add_confirm/').submit();
        }
    });
});
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/extraordinary/">臨時要求作成</a></li>
        <li>商品選択</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>商品選択</span></h2>
<form class="validate_form search_form" id="promiseForm" method="post" action="ex_select">
    <?php echo $this->form->input('add_search.is_search',array('type'=>'hidden','id' =>'is_search'));?>
    <?php echo $this->form->input('promise.id',array('type'=>'hidden'));?>
    <?php echo $this->form->input('promise.item_unit_id',array('type'=>'hidden'));?>
    <?php echo $this->form->input('cart.tmpid',array('type'=>'hidden','id'=>'tmpId'));?>
        <div class="SearchBox">
            <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <th>要求日</th>
                    <td>
                      <?php echo $this->form->text('TrnPromise.work_date',array('class' => 'lbl','readOnly' => 'readOnly')); ?>
                    </td>
                    <td></td>
                    <th>作業区分</th>
                    <td>
                    <?php
                      echo $this->form->text('TrnPromise.work_name',array('class' => 'lbl' ,'readOnly' => 'readOnly'));
                      echo $this->form->text('TrnPromise.work_type',array('type' => 'hidden'));
                    ?>
                    </td>
                </tr>
                <tr>
                    <th>得意先</th>
                    <td>
                    <?php
                      echo $this->form->text('TrnPromise.facility_name',array('class' => 'lbl' ,'readOnly' => 'readOnly'));
                      echo $this->form->text('TrnPromise.facility_id',array('type' => 'hidden'));
                    ?>
                    </td>
                    <td></td>
                    <th>備考</th>
                    <td>
                      <?php echo $this->form->text('TrnPromise.recital',array('class' => 'lbl' ,'readOnly' => 'readOnly')); ?>
                    </td>
                </tr>
                <tr>
                    <th>部署</th>
                    <td>
                    <?php
                      echo $this->form->text('TrnPromise.department_name',array('class' => 'lbl' ,'readOnly' => 'readOnly'));
                      echo $this->form->text('TrnPromise.department_code',array('type' =>'hidden'));
                      echo $this->form->text('TrnPromise.department_id',array('type' =>'hidden'));
                    ?>
                    </td>
                </tr>
            </table>

            <h2 class="HeaddingLarge2">検索条件</h2>
                <table class="FormStyleTable">
                <colgroup>
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="20" />
                    <col />
                    <col />
                    <col width="60" align="right" />
                </colgroup>
                <tr>
                    <th>商品ID</th>
                    <td><?php echo $this->form->text('MstFacilityItem.internal_code',array('maxlength' => '50','class'=>'txt search_internal_code')); ?></td>
                    <td></td>
                    <th>製品番号</th>
                    <td><?php echo $this->form->text('MstFacilityItem.item_code',array('maxlength' => '50','class'=>'txt search_upper')); ?></td>
                    <td></td>
                    <td></td>
                    <td><?php echo($this->form->checkbox('MstFacilityItem.canChecked', array('label' => false))); ?>登録可能な項目のみ</td>
                    <td></td>
                </tr>
                <tr>
                    <th>商品名</th>
                    <td><?php echo $this->form->text('MstFacilityItem.item_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                    <td></td>
                    <th>販売元</th>
                    <td><?php echo $this->form->text('MstFacilityItem.dealer_name',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>規格</th>
                    <td><?php echo $this->form->text('MstFacilityItem.standard',array('maxlength' => '50','class'=>'search_canna txt')); ?></td>
                    <td></td>
                    <th>ＪＡＮコード</th>
                    <td><?php echo $this->form->text('MstFacilityItem.jan_code',array('maxlength' => '50','class'=>'txt')); ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <div class="ButtonBox">
                <p class="center"><input type="button" class="btn btn1" id="btn_Search" /></p>
            </div>
        </div>


    <div align="right" id="pageTop" ></div>

<?php if(isset($this->request->data['add_search']['is_search'])){ // 検索済み ?>
    <?php if(count($SearchResult) === 0){ //検索結果が0件だったら ?>
    <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
    </div>
    <div class="TableHeaderAdjustment01">
        <table class="TableHeaderStyle02">
            <tr>
                <th width="20"><input type="checkbox" /></th>
                <th style="width:90px;">商品ID</th>
                <th class="col20">商品名</th>
                <th class="col20">規格</th>
                <th class="col20">製品番号</th>
                <th class="col20">販売元</th>
                <th class="col10">包装単位</th>
            </tr>
        </table>
    </div>
    <span>該当するデータはありません。</span>
    <?php }else{ //検索結果が1件以上あったら ?>
    <div id="results">
        <div class="DisplaySelect">
            <?php echo $this->element('limit_combobox',array('result'=>count($SearchResult))); ?>
        </div>

        <div class="TableHeaderAdjustment01">
            <table class="TableHeaderStyle02">
                <tr>
                    <th width="20"><input type="checkbox" class="checkAll_1" /></th>
                    <th style="width:90px;">商品ID</th>
                    <th class="col20">商品名</th>
                    <th class="col20">規格</th>
                    <th class="col20">製品番号</th>
                    <th class="col20">販売元</th>
                    <th class="col10">包装単位</th>
                </tr>
            </table>
        </div>
        <div id="searchForm">
<?php
  $i = 0;
  foreach($SearchResult as $item){
  $class = ($i % 2 == 0)?'':'odd';
  ?>
            <table class="TableStyle02" border=0 id="<?php echo 'containTables'.$item["TrnPromise"]["id"];?>" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
                <tr class="<?php echo $class ; ?>" id=<?php echo "itemSelectedRow".$item["TrnPromise"]['id'];?> >
                    <td class="center" style="width:20px;">
                    <?php if($item['TrnPromise']['status'] == true){?>
                        <?php echo $this->form->checkbox("SelectedID.{$i}", array('class'=>'center checkAllTarget','value'=>$item["TrnPromise"]["id"] , 'style'=>'width:20px;text-align:center;' , 'hiddenField'=>false) ); ?>
                    <?php } ?>
                    </td>
                    <td style="width:90px;"><?php echo h_out($item['TrnPromise']['internal_code'],'center'); ?></td>
                    <td class="col20"><?php echo h_out($item['TrnPromise']['item_name']); ?></td>
                    <td class="col20"><?php echo h_out($item['TrnPromise']['standard']); ?></td>
                    <td class="col20"><?php echo h_out($item['TrnPromise']['item_code']); ?></td>
                    <td class="col20"><?php echo h_out($item['TrnPromise']['dealer_name']); ?></td>
                    <td class="col10"><?php echo h_out($item['TrnPromise']['unit_name']); ?></td>
                </tr>
            </table>
<?php
  $i++;
  }
  ?>
        </div>
        <div class="ButtonBox" style="margin-top:5px;">
            <p class="center">
                <input type="button" value="" class="btn btn4" id="cmdSelect" />
            </p>
        </div>
    </div>
<?php } ?>

    <?php if(isset($this->request->data['add_search']['is_search'])){?>
    <h2 class="HeaddingSmall">選択商品</h2>
    <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
            <th width="20"><input type="checkbox" class="checkAll_2" checked /></th>
            <th style="width:90px;">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">規格</th>
            <th class="col20">製品番号</th>
            <th class="col20">販売元</th>
            <th class="col10">包装単位</th>
        </tr>
      </table>
    </div>

    <div class="" broder='0'  id="cartForm" >
      <?php if(isset($CartSearchResult)){ ?>
<?php
  $i = 0;
  foreach($CartSearchResult as $item){
  $class = ($i % 2 == 0)?'':'odd';
  ?>
      <table class="TableStyle02" style="margin-top:-1px; margin-bottom:-1px; padding:0px;">
            <tr class=<?php echo $class ?>>
                <td class="center" rowspan="2" style="width:20px;">
                <?php echo $this->form->checkbox('TrnPromise.selectid.'.$i , array('checked' => 1 , 'class'=>'center checkAllTarget' , 'value'=>$item['TrnPromise']['id'] , 'hiddenField'=>false) ); ?>
                </td>
                <td  style="width:90px;"><?php echo h_out($item['TrnPromise']['internal_code'],'center') ?></td>
                <td class="col20"><?php echo h_out($item['TrnPromise']['item_name']); ?></td>
                <td class="col20"><?php echo h_out($item['TrnPromise']['standard']); ?></td>
                <td class="col20"><?php echo h_out($item['TrnPromise']['item_code']); ?></td>
                <td class="col20"><?php echo h_out($item['TrnPromise']['dealer_name']); ?></td>
                <td class="col10"><?php echo h_out($item['TrnPromise']['unit_name']); ?></td>
            </tr>
      </table>
            <?php
              $i++;
              }
            ?>
      <?php }?>
      <div id="itemSelectedTable" border="0" style="margin-top:-1px; margin-bottom:-1px; padding:0px;"></div>
    </div>

    <div class="ButtonBox" style="margin-top:5px;">
      <p class="center">
        <input type="button" id="btn_Confirm" value="" class="btn btn3"/>
      </p>
    </div>
    <?php }?>
<?php
    }else{
      //検索実行フラグがfalse
?>
      <div class="DisplaySelect">
        <?php echo $this->element('limit_combobox',array('result'=>0)); ?>
      </div>

      <div class="TableHeaderAdjustment01">
      <table class="TableHeaderStyle02">
        <tr>
            <th width="20"><input type="checkbox" /></th>
            <th style="width:90px;">商品ID</th>
            <th class="col20">商品名</th>
            <th class="col20">規格</th>
            <th class="col20">製品番号</th>
            <th class="col20">販売元</th>
            <th class="col10">包装単位</th>
        </tr>
      </table>

      </div>
<?php
    }
    echo $this->form->end();
?>
<div align="right" id="pageDow" ></div>
