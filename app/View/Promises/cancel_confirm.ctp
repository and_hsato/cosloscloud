<script type="text/javascript">
function toResult(){
    if( $('input[type=checkbox].checkAllTarget:checked').length === 0){
        alert('取消を実行する明細を選択してください');
        return false;
    }else{
        if(window.confirm("取消を実行します。よろしいですか？")){
            $("#TrnPromiseCancelConfirmForm").attr('action' ,  '<?php echo $this->webroot ?><?php echo $this->name; ?>/cancel_result/').submit();
        }
    }
}

$(function(){
    //ステータステーブル初期は非表示
    $('#status_table').hide();
    $('#status_table').bgiframe();
    
    //Ajaxでステータスを取得する
    $(".promise_status").click(function () {
        //いったん消す
        $('#status_table').hide();
        
        $.post("<?php echo $this->webroot; ?><?php echo $this->name; ?>/get_status/",
        {
            //POSTデータ
            id: $(this).parent(':eq(0)').find('input[type=hidden]:eq(0)').val() 
        },
        function (data) {
            var status_table = '';
            var obj = jQuery.parseJSON(data);
            //レスポンスを元に、表示データを生成
            status_table += '<div style="float:left"><table class="TableStyle01"><tr><th>要求日</th><th>引当日</th><th>部署シール発行日</th><th>出荷日</th><th>受領日</th><th>部署シール番号</th><th>センターシール番号</th></tr>';
            $.each(obj, function(index, o) {
                if(index % 2 == 0){
                    status_table += '<tr>';
                }else{
                    status_table += '<tr class="odd">';
                }
                status_table += '<td>' + o[0].promise_date + '</td>';
                status_table += '<td>' + o[0].fix_promise_date + '</td>';
                status_table += '<td>' + o[0].sticker_issue_date + '</td>';
                status_table += '<td>' + o[0].shipping_date + '</td>';
                status_table += '<td>' + o[0].receiving_date + '</td>';
                status_table += '<td>' + o[0].hospital_sticker_no + '</td>';
                status_table += '<td>' + o[0].facility_sticker_no + '</td>';
                status_table += '</tr>';
            });
            status_table += '</table></div>';
            status_table += '<div style="float:left"><img src="<?php echo $this->webroot; ?>img/close2_blk.gif" alt="close" class="floatClose" /></div>';
            $('#status_table').html(status_table);
            $('#status_table').show();
            $('#status_table').draggable({
                                   opacity  : 0.5,    //ドラッグ時の不透明度
                                   cursor   : 'move'  //カーソル形状
            });
            
            //閉じるボタン色変え処理
            $('img.floatClose').hover(function(){
                $(this).attr('src', '<?php echo $this->webroot; ?>img/close2_red.gif');
            }, function(){
                $(this).attr('src', '<?php echo $this->webroot; ?>img/close2_blk.gif');
            });
            
            //閉じる処理
            $('.floatClose').click(function(){
                $('#status_table').hide();
            });
        },
        function (success) {
            //成功
        },
        function(error) {
            //Ajax通信エラー
        });
        return false;
    });
  
    $('#comments_all_txt').change(function(){
        $('.TrnPromiseRecital').val($('#comments_all_txt').val());
    });
});
function anc2(){
   obj = $('#pageDow')[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

function anc(id){
   obj = $('#'+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/history/">要求作成履歴</a></li>
        <li>要求作成履歴明細</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>要求作成履歴明細</span></h2>
<h2 class="HeaddingMid">要求作成履歴明細</h2>
<?php echo $this->form->create('TrnPromise',array('class' => 'validate_form','type' => 'post','action' => '' ,'id' =>'TrnPromiseCancelConfirmForm')); ?>
    <?php echo $this->form->input('promise.time',array('type' => 'hidden','value' => date('Y/m/d H:i:s.U'))); ?>
    <?php echo $this->form->input('Promise.token',array('type' => 'hidden')); ?>
    <table style="width:100%;">
        <tr>
            <td></td>
            <td align="right" id="pageTop" ></td>
        </tr>
        <tr>
            <td>表示件数：<?php echo count($displayData); ?>件</td>
            <td align="right">
                <input type="text" id="comments_all_txt" class="txt [p2]" style="width: 150px;"><input type="button" class="btn btn8 [p2]" value="">
            </td>
       </tr>
    </table>
    <div class="TableScroll2">
        <table class="TableStyle01 table-even2">
            <tbody>
            <tr>
                <th style="width: 20px;" rowspan="2" class="center"><input type="checkbox" onClick="listAllCheck(this);" class="center" /></th>
                <th style="width:150px;">要求番号</th>
                <th style="width:80px">要求日</th>
                <th style="width:70px">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th style="width:80px;">包装単位</th>
                <th style="width:60px;">在庫数</th>
                <th style="width:60px;">要求済</th>
                <th style="width:60px;">作成数</th>
                <th style="width:80px;">作業区分</th>
                <th style="width:70px;">更新者</th>
            </tr>
            <tr>
                <th colspan="2">施設　／　部署</th>
                <th>状態</th>
                <th>規格</th>
                <th>販売元</th>
                <th>管理区分</th>
                <th>定数</th>
                <th>消費数</th>
                <th>未受領</th>
                <th colspan="2">備考</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 0; foreach($displayData as $contents) { ?>
                <tr>
                    <td rowspan="2" class="center">
            <?php
              if($contents['TrnPromise']['statusJudgment'] == "1"){
                  echo $this->form->checkbox('TrnPromise.id.'.$i,array('value'=>$contents['TrnPromise']['id'], 'class' =>'checkAllTarget center chk' , 'hiddenField' => false) );
                  echo $this->form->input('TrnPromise.remain_count.'.$i , array('type'=>'hidden' , 'value'=>$contents['TrnPromise']['remain_count']));
                  echo $this->form->input('TrnPromise.before_quantity.'.$i , array('type'=>'hidden' , 'value'=>$contents['TrnPromise']['before_quantity']));
                  echo $this->form->input('TrnPromise.quantity.'.$i , array('type'=>'hidden' , 'value'=>$contents['TrnPromise']['quantity']));
              } ?>
                    </td>
                    <td><?php echo h_out($contents['TrnPromiseHeaders']['work_no']); ?></td>
                    <td><?php echo h_out($contents['TrnPromiseHeaders']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($contents['MstFacilityItem']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($contents['MstFacilityItem']['item_name']); ?></td>
                    <td><?php echo h_out($contents['MstFacilityItem']['item_code']); ?></td>
                    <td><?php echo h_out($contents['MstUnitNames']['unit_name']); ?></td>
                    <td><?php echo h_out($contents['TrnPromise']['stock_count'],'right'); ?></td>
                    <td><?php echo h_out($contents['TrnPromise']['requested_count'],'right'); ?></td>
                    <td><?php echo h_out($contents['TrnPromise']['quantity'],'right'); ?></td>
                    <td><?php echo h_out($contents['TrnPromise']['work_name']); ?></td>
                    <td><?php echo h_out($contents['MstUser']['user_name']); ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo h_out($contents['TrnPromiseHeaders']['facility_name']); ?></td>
                    <td>
                        <label title="<?php echo $contents['TrnPromise']['promise_status_name']; ?>">
                        <p align="center" class="<?php echo ($contents['TrnPromise']['quantity'] > 0 )?'promise_status dummy_link':''; ?>">
                        <?php echo $contents['TrnPromise']['promise_status_name']; ?>
                        <?php echo $this->form->input('TrnPromise.status_id.'.$i , array('type'=>'hidden' , 'value'=>$contents['TrnPromise']['id'])); ?>
                        </p>
                        </label>
                    </td>
                    <td><?php echo h_out($contents['MstFacilityItem']['standard']); ?></td>
                    <td><?php echo h_out($contents['MstDealer']['dealer_name']); ?></td>
                    <td><?php echo h_out($contents['TrnPromise']['promise_type_name'],'center'); ?></td>
                    <td><?php echo h_out($contents['TrnPromise']['fixed_count'],'right'); ?></td>
                    <td><?php echo h_out($contents['TrnPromise']['consume_count'],'right'); ?></td>
                    <td><?php echo h_out($contents['TrnPromise']['unreceipt_count'],'right'); ?></td>
                    <td colspan="2">
                <?php
                if($contents['TrnPromise']['statusJudgment'] == "1"){
                   echo $this->form->input('TrnPromise.recital.'.$i , array('type' => 'text' , 'class'=>'TrnPromiseRecital txt' , 'maxlength'=>'200' , 'value'=>$contents['TrnPromise']['recital']));
                }else{
                   echo $this->form->input('TrnPromise.recital.'.$i , array('type' => 'text' , 'class'=>'lbl' , 'readonly'=>'readonly' , 'value'=>$contents['TrnPromise']['recital']));
                }
                ?>
                    </td>
                </tr>
                <?php $i++; } ?>
            </tbody>
        </table>
    </div>
    <table style="width:100%;">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
<?php if($butDisFlg === TRUE ) { ?>
<div class="ButtonBox"  id="pageDow">
    <p class="center">
        <input type="button" class="btn btn6 submit [p2]" value="" onClick="return toResult();"/>
    </p>
</div>
<?php } ?>
<?php echo $this->form->end(); ?>
<div id="status_table" class="search_form"></div>
