<script type="text/javascript">
$(function($){
    //チェックボックス一括制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });
  
    //検索ボタン押下
    $("#revealResult").click(function(){
        $("#TrnPromiseHistoryForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history/');
        $("#TrnPromiseHistoryForm").attr('method', 'post');
        $("#TrnPromiseHistoryForm").submit();
    });

    //要求作成履歴印刷ボタン押下
    $("#printOut").click(function(){
        $("#TrnPromiseHistoryForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/history');
        $("#TrnPromiseHistoryForm").attr('method', 'post');
        $("#TrnPromiseHistoryForm").submit();
    });
    
    //欠品リスト印刷ボタン押下
    $("#printOut2").click(function(){
        $("#TrnPromiseHistoryForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/stockout');
        $("#TrnPromiseHistoryForm").attr('method', 'post');
        $("#TrnPromiseHistoryForm").submit();
    });
  
    //請求伝票印刷ボタン押下
    $("#printOut3").click(function(){
        $("#TrnPromiseHistoryForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/promise_consume');
        $("#TrnPromiseHistoryForm").attr('method', 'post');
        $("#TrnPromiseHistoryForm").submit();
    });
    
    //CSVボタン押下
    $("#btn_Csv").click(function(){
        $("#TrnPromiseHistoryForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_csv/');
        $("#TrnPromiseHistoryForm").attr('method', 'post');
        $("#TrnPromiseHistoryForm").submit();
        $("#TrnPromiseHistoryForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history/');
    });
  
    //EXCELボタン押下
    $("#btn_Excel").click(function(){
        $("#TrnPromiseHistoryForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/export_excel/');
        $("#TrnPromiseHistoryForm").attr('method', 'post');
        $("#TrnPromiseHistoryForm").submit();
        $("#TrnPromiseHistoryForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/history/');
    });
    
    //明細表示ボタン押下
    $("#toConfirm").click(function(){
        if($('.TableStyle01 input:checked').length  > 0) {
            $("#TrnPromiseHistoryForm").attr('action' , '<?php echo $this->webroot; ?><?php echo $this->name; ?>/cancel_confirm');
            $("#TrnPromiseHistoryForm").attr('method', 'post');
            $("#TrnPromiseHistoryForm").submit();
        }else{
            alert('閲覧する明細を選択してください');
            return false;
        }
    });

});

function anc2(){
   obj = $('#pageDow')[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

function anc(id){
   obj = $('#'+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li>要求作成履歴</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>要求作成履歴</span></h2>
<h2 class="HeaddingMid">要求作成履歴</h2>
<form class="validate_form search_form input_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/history" id="TrnPromiseHistoryForm">
    <?php echo $this->form->input('selected.is_search',array('type' => 'hidden','id' => 'is_search')); ?>
    <?php echo $this->form->input('selected.fasName',array('type' => 'hidden','value' => h($fasName))); ?>
    <?php echo $this->form->input('selected.login_fas_id',array('type' => 'hidden','value' => h($login_fas_id))); ?>
    <?php echo $this->form->input('selected.mst_user_id',array('type' => 'hidden')); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="20"/>
                <col/>
                <col/>
                <col width="60" align="right"/>
            </colgroup>
            <tr>
                <th>要求番号</th>
                <td><?php echo $this->form->input('TrnPromise.work_no',array('class' => 'txt','style' => 'width:110px'));?></td>
                <td></td>
                <th>要求日</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.work_date1',array('class'=>'validate[optional,custom[date],funcCall2[date2]] date', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo $this->form->input('TrnPromise.work_date2',array('class'=>'validate[optional,custom[date],funcCall2[date2]] date', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                </td>
                <td></td>
                <td colspan="2">
                  <?php echo $this->form->checkbox('TrnPromise.delete_opt',array('class' =>'center')); ?>取消も表示する
                </td>
            </tr>
            <tr>
                <th>要求施設</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.facilityText',array('class'=>'txt require','style'=>'width:60px;','id'=>'facilityText')); ?>
                    <?php echo $this->form->input('TrnPromise.facilityCode',array('options'=>$facility_List,'class'=>'txt','style'=>'width:120px;','id'=>'facilityCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('TrnPromise.facilityName',array('type'=>'hidden','id'=>'facilityName')); ?>
                </td>
                <td></td>
                <th>要求部署</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.departmentText',array('class'=>'txt','style'=>'width:60px;','id'=>'departmentText')); ?>
                    <?php echo $this->form->input('TrnPromise.departmentCode',array('options'=>$department_List, 'class'=>'txt','style'=>'width:120px;','id'=>'departmentCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('TrnPromise.departmentName',array('type'=>'hidden','id'=>'departmentName')); ?>
                </td>
                <td></td>
                <td colspan="2">
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->input('MstFacilityItem.internal_code',array('class' => 'txt search_internal_code','style' => 'width:120px',)); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_code',array('class' => 'txt search_upper','style' => 'width:120px',)); ?></td>
                <td></td>
                <th>管理区分</th>
                <td>
                  <?php echo $this->form->input('TrnPromise.promise_type',array('options'=>Configure::read('PromiseType.list'),'style' => 'width:120px','class' => 'txt' , 'empty'=>true));?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->input('MstFacilityItem.item_name',array('class' => 'search_canna txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->input('MstFacilityItem.dealer_name',array('class' => 'search_canna txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td>
                  <?php echo $this->form->input('TrnPromise.work_type',array('options'=>$workType,'style' => 'width:120px','class' => 'txt' , 'empty'=>true)); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->input('MstFacilityItem.standard',array('class' => 'search_canna txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->input('MstFacilityItem.jan_code',array('class' => 'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="5">
                  <?php echo $this->form->radio('TrnPromise.opt',array("1" => "通常表示","0" => "欠品のみ表示"),array('label' => '','legend'=>'' )); ?>
                </td>
            </tr>
            <tr>
                <td colspan="9" style="height:10px;"></td>
            </tr>
        </table>

        <div class="ButtonBox">
            <p class="center">
              <input type="button" class="btn btn1" id="revealResult" />
              <input type="button" class="btn btn91 print_btn" id="printOut" />
              <input type="button" class="btn btn54 print_btn" id="printOut2" />
              <input type="button" class="btn btn108 print_btn" id="printOut3" />
              <input type="button" class="btn btn5" id="btn_Csv"/>
              <input type="button" class="btn btn109" id="btn_Excel"/>
            </p>
        </div>
    </div>
    <hr class="Clear" />
    <div class="DisplaySelect">
        <div align="right" id="pageTop" ></div>
        <?php echo $this->element('limit_combobox',array('result'=>count($displayData) ) ); ?>
    </div>
    <div class="TableScroll">
        <table class="TableStyle01 table-even">
            <thead>
                <tr>
                    <th style="width:20px;" class="center" ><input type="checkbox" class="checkAll" /></th>
                    <th style="width: 135px;">要求番号</th>
                    <th style="width: 85px;">要求日</th>
                    <th>施設／部署</th>
                    <th class="col10">登録者</th>
                    <th style="width: 140px;" class="col15">登録日時</th>
                    <th class="col15">備考</th>
                    <th class="col10">シール枚数</th>
                    <th class="col10">件数</th>
                </tr>
            </thead>
            <tbody>
        <?php
          $i = 0;
          foreach($displayData as $contents) {
        ?>
                <tr>
                    <td style="width:20px;" class="center">
                        <input type="checkbox" name="data[TrnPromiseHeaders][id][<?php echo $i ?>]" class="chk" value="<?php echo $contents['TrnPromiseHeaders']['id'] ?>" id="TrnPromiseHeadersId<?php echo $i ?>" />                </td>
                    </td>
                    <td style="width:135px;"><?php echo h_out($contents['TrnPromiseHeaders']['work_no'],'center'); ?></td>
                    <td style="width:85px;"><?php echo h_out($contents['TrnPromiseHeaders']['work_date'],'center'); ?></td>
                    <td><?php echo h_out($contents['TrnPromiseHeaders']['facility_name']); ?></td>
                    <td class="col10"><?php echo h_out($contents['MstUser']['user_name']); ?></td>
                    <td style="width:140px;"><?php echo h_out($contents['TrnPromiseHeaders']['created']); ?></td>
                    <td class="col15"><?php echo h_out($contents['TrnPromiseHeaders']['recital']); ?></td>
                    <td class="col10"><?php echo h_out($contents['TrnPromiseHeaders']['quantity'],'right'); ?></td>
                    <td class="col10"><?php echo h_out($contents['TrnPromiseHeaders']['count'],'right'); ?></td>
                </tr>
            <?php  $i++; } ?>
            <?php if(isset($this->request->data['selected']['is_search']) && count($displayData)==0){ ?>
                <tr><td colspan='9' class="center">該当するデータがありませんでした</td></tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="ButtonBox">
       <p class="center">
            <input type="button" class="btn btn7 submit" id="toConfirm"/>
        </p>
    </div>
    <div align="right" id ="pageDow"></div>
</form>
