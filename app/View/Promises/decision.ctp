<script type="text/javascript">
$(function($){
    $("#decision").click(function(){
        if($('#facility_selected').val() == "" && $('#depts').val() == ""){
            if (!confirm('全ての施設・部署で引当登録を行いますがよろしいですか？')){
                return false;
            }
        }
        $("#mode").val("1");
        $("#TrnPromiseDecision").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/decision/').submit();
    });
    $("#stockList").click(function(){
        $("#mode").val("2");
        $("#TrnPromiseDecision").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/decision/').submit();
    });
    $("#stockPrint").click(function(){
        $("#TrnPromiseDecision").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/report/deci').submit();
    });
    $("#departmentList").click(function(){
        $("#mode").val("3");
        if($('#depts').val() != "" ){
            $("#TrnPromiseDecision").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/decision/').submit();
        }else{
            if($('#facility_selected').val() !="") {
                $("#mode").val("4");
                $("#TrnPromiseDecision").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/decision/').submit();
            }else{
                alert('施設を選択してください。');
            }
        }
    });
    $("#departmentPrint").click(function(){
        if($('#depts').val() != "" ){
            $("#TrnPromiseDecision").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/report/deci2').submit();
        }else{
            if($('#facility_selected').val() !="") {
                $("#TrnPromiseDecision").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/report/deci3').submit();
            }else{
                alert('施設を選択してください。');
            }
        }
    });
    $("#stockListCsv").click(function(){
        $("#mode").val("5");
        $("#TrnPromiseDecision").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/output_csv/').submit();
    });

    $("#departmentListCsv").click(function(){
        if($('#depts').val() != "" ){
            $("#mode").val("6");
            $("#TrnPromiseDecision").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/output_csv/').submit();
        }else{
            if($('#facility_selected').val() !="") {
                $("#mode").val("7");
                $("#TrnPromiseDecision").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/output_csv/').submit();
            }else{
                alert('施設を選択してください。');
            }
        }
    });
});


</script>
    <div id="TopicPath">
        <ul>
            <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
            <li>引当登録</li>
        </ul>
    </div>
    <h2 class="HeaddingLarge"><span>引当登録</span></h2>
    <h2 class="HeaddingMid">引当を行う条件を指定してください</h2>
<?php echo $this->form->create('TrnPromise',array('class' => 'validate_form input_form','type' => 'post','action' => '' ,'id' =>'TrnPromiseDecision')); ?>
    <?php echo $this->form->input('selected.mode',array('type' => 'hidden','id' => 'mode')); ?>
    <?php echo $this->form->input('selected.headFas',array('type' => 'hidden','value'=>h($headFas))); ?>
    <?php echo $this->form->input('promise.time',array('type' => 'hidden','value'=>date('Y/m/d H:i:s.u'))); ?>
    <?php echo $this->form->input('SplitTable.center_facility_id' , array('type'=>'hidden' , 'value'=>$this->Session->read('Auth.facility_id_selected')));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.facilityText',array('id' => 'facilityText','style'=>'width:60px;', 'class' => 'txt','maxlength'=>20)); ?>
                    <?php echo $this->form->input('TrnPromise.facilityCode',array('options'=>$facility_List ,'empty'=>true,'id' => 'facilityCode', 'class' => 'txt')); ?>
                    <?php echo $this->form->input('TrnPromise.facilityName',array('type'=>'hidden' ,'id' => 'facilityName')); ?>
                </td>
                <td width="20"></td>
                <th>要求日</th>
                <td>
                <?php echo $this->form->text('TrnPromise.work_date1',array('class'=>'date txt validate[custom[date]]', 'id'=> 'datepicker1','maxlength' => '10' )); ?>
                ～
                <?php echo $this->form->text('TrnPromise.work_date2',array('class'=>'date txt validate[custom[date]]', 'id'=> 'datepicker2','maxlength' => '10' )); ?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.departmentText',array('class'=>'txt','style'=>'width:60px;','id'=>'departmentText')); ?>
                    <?php echo $this->form->input('TrnPromise.departmentCode',array('options'=>$department_List, 'class'=>'txt','id'=>'departmentCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('TrnPromise.departmentName',array('type'=>'hidden','id'=>'departmentName')); ?>
                </td>
                <td width="20"></td>
                <th>要求区分</th>
                <td>
                 <?php echo $this->form->input('TrnPromise.promise_type', array('options'=>Configure::read('PromiseType.list'), 'class' =>'txt' , 'empty'=>true)); ?>
                </td>
            </tr>
            <tr>
                <td style="height:10px;"></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center"><input type="button" class="btn btn49" id="decision" /></p>
    </div>
<div id="results">
    <span>
      <?php echo $this->request->data['TrnPromise']['afterPromise']; ?>
      <?php echo $this->form->input('TrnPromise.afterPromise',array('type' =>'hidden')); ?>
    </span>
    <div class="AlertLimitBox">
      <?php echo $this->request->data['TrnPromise']['alertMsg']; ?>
      <?php echo $this->form->input('TrnPromise.alertMsg',array('type' =>'hidden')); ?>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="ButtonBox">
        <input type="button" class="btn btn75" id="stockList" />
        <input type="button" class="btn btn76" id="stockPrint" />
        <input type="button" class="btn btn74" id="stockListCsv" />
        <input type="button" class="btn btn71" id="departmentList" />
        <input type="button" class="btn btn72" id="departmentPrint" />
        <input type="button" class="btn btn70" id="departmentListCsv" />
    </div>
    <hr class="Clear" />
<?php
if( isset($this->request->data['selected']['mode']) ){ 

if( $this->request->data['selected']['mode'] == "2" ){
?>
    <div id="results2">
        <h2 class="HeaddingSmall">センター在庫一覧</h2>
<?php }
if( $this->request->data['selected']['mode'] == "3" ){
  ?>
    <div id="results2">
        <h2 class="HeaddingSmall">要求部署在庫一覧</h2>
<?php } 
if( $this->request->data['selected']['mode'] == "4" ){
  ?>
    <div id="results2">
        <h2 class="HeaddingSmall">要求部署在庫一覧</h2>
<?php } 
?>
        <div class="TableScroll">
            <table class="TableStyle01">
                <colgroup>
<?php
if( $this->request->data['selected']['mode'] == "4" ){
?>
                    <col />
<?php
} 
?>
                    <col width="100" />
                    <col />
                    <col />
                    <col />
                    <col />
                    <col width="120" />
                    <col width="80" />
                    <col width="80" />
                    <col width="80" />
                    <col width="85" />
                </colgroup>
                <tr>
<?php
if( $this->request->data['selected']['mode'] == "4" ){
?>
                    <th>部署</th>
<?php
} 
?>
                    <th>商品ID</th>
                    <th>商品名</th>
                    <th>規格</th>
                    <th>製品番号</th>
                    <th>販売元</th>
                    <th>単位</th>
                    <th>未入荷</th>
                    <th>要求</th>
                    <th>引当可</th>
                    <th><label title="包装単位の終了日" style="width: 100%;">終了日</label></th>
                </tr>

<?php
if( $this->request->data['selected']['mode'] == "4" ){
?>
<?php
} 
?>

                <?php $count = $i = 0;?>
                <?php foreach($result as $r){ ?>
                <tr>
                    <?php if($count == 0){ ?>
                        <?php $count = $r[0]['count']; ?>
<?php
if( $this->request->data['selected']['mode'] == "4" ){
?>
                        <td rowspan="<?php echo $count; ?>"><?php echo h_out($r[0]['department_name'],'center'); ?></td>
<?php
} 
?>
                        <td rowspan="<?php echo $count; ?>"><label title="<?php echo h($r[0]['internal_code']); ?>"><p align="center">
                        <?php
                            if($r[0]['center_stock_flag']=="1") echo "★ " ;
                            echo h($r[0]['internal_code']);
                        ?></p></label></td>
                        <td rowspan="<?php echo $count; ?>"><?php echo h_out($r[0]['item_name']); ?></td>
                        <td rowspan="<?php echo $count; ?>"><?php echo h_out($r[0]['standard']); ?></td>
                        <td rowspan="<?php echo $count; ?>"><?php echo h_out($r[0]['item_code']); ?></td>
                        <td rowspan="<?php echo $count; ?>"><?php echo h_out($r[0]['dealer_name']); ?></td>
                    <?php } ?>
                    <td><?php echo h_out($r[0]['unit_name']); ?></td>
                    <td><?php echo h_out($r[0]['requested_count'],'right'); ?></td>
                    <td><?php echo h_out($r[0]['remain_count'],'right'); ?></td>
                    <td><?php echo h_out($r[0]['stock'],'right'); ?></td>
                    <td><?php echo h_out($r[0]['end_date'],'right'); ?></label></td>
                    <?php $count=$count-1; ?>
                    <?php if($count == 0 ) { ?>
                        <?php $i++; ?>
                     <?php } ?>
                </tr>
                <?php } ?>
            </table>
        </div>
<?php } ?>
   </div>
</div>
<?php echo $this->form->end(); ?>
