<script type="text/javascript">
$(document).ready(function() {
    // 商品選択ボタン押下
    $("#selectItem").click(function(){
        $("#TrnPromiseExtraordinaryForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/ex_select/');
        $("#TrnPromiseExtraordinaryForm").attr('method', 'post');
        $("#TrnPromiseExtraordinaryForm").submit();
    });
  
    // CSV取込ボタン押下
    $("#toCsv").click(function(){
        $("#TrnPromiseExtraordinaryForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/csv_register/');
        $("#TrnPromiseExtraordinaryForm").attr('method', 'post');
        $("#TrnPromiseExtraordinaryForm").submit();
    });

});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li>臨時要求作成</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>臨時要求入力</span></h2>
<h2 class="HeaddingMid">臨時要求データを入力してください</h2>
<?php echo $this->form->create('TrnPromise',array('type' => 'post','action' => '' ,'id' =>'TrnPromiseExtraordinaryForm','class' => 'validate_form input_form')); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>要求日</th>
                <td>
                    <?php echo $this->form->text('TrnPromise.work_date',array('class'=>'validate[required,custom[date],funcCall2[date2]] date r', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                </td>
                <td width="20"></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.work_type' , array('options'=>$workType, 'style'=>'width:120px;','class' =>'txt' , 'id'=>'classId' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('TrnPromise.work_name' , array('type'=>'hidden' , 'id'=>'className'))?>
                </td>
            </tr>
            <tr>
                <th>得意先</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.facility_name' , array('type'=>'hidden' , 'id'=>'facilityName')); ?>
                    <?php echo $this->form->input('TrnPromise.facility_code',array('id' => 'facilityText', 'label' => false, 'div' => false, 'class' => 'r txt', 'style'=>'width: 60px;')); ?>
                    <?php echo $this->form->input('TrnPromise.faclility_id',array('options'=>$facility_List , 'id' => 'facilityCode','class'=>'r txt validate[required]' , 'empty'=>true) ); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->text('TrnPromise.recital',array('class' => 'txt','style'=>'width:150px')); ?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->input('TrnPromise.department_name' , array('type'=>'hidden' , 'id'=>'departmentName'))?>
                    <?php echo $this->form->input('TrnPromise.department_code', array('id' => 'departmentText', 'label' => false, 'div' => false, 'class' => 'r txt', 'style' => 'width: 60px;')); ?>
                    <?php echo $this->form->input('TrnPromise.department_id' , array('options'=>array() , 'id'=>'departmentCode' , 'class'=>'r txt validate[required]')) ?>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="ButtonBox">
        <p class="center">
            <input type="button" id="selectItem" class="btn btn67 submit" />
            <!--<input type="button" id="toCsv" class="btn btn26 submit" />-->
        </p>
    </div>
    <hr class="Clear" />
<?php echo $this->form->end(); ?>