<script type="text/javascript">

$(document).ready(function(){
    //チェックボックス オールチェック
    $('.checkAll').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });

    $('#submitBtn').click(function(){
        var cnt = 0;
        var err = false;
        $("#TrnPromiseExAddConfirmForm input[type=checkbox].chk").each(function(){
            if($(this).attr("checked") == true){
                //チェックが入っている行の必須項目チェック
                var id = $(this).val();
                var quantity = $('#quantity'+id).val();
                if(!numCheck(quantity , 9 , 0 , true)){
                    alert("数量を正しく入力してください。");
                    $('#quantity'+id).focus();
                    err = true;
                    return false;
                }

                //警告有り商品だった場合、アラートメッセージを出す。
                if($('#TrnPromiseAlertMsg'+id).size() > 0){
                    if(window.confirm($('#TrnPromiseAlertMsg'+id).val() + 'が含まれますが、よろしいですか？')){
                    }else{
                        err = true
                        return false;
                    }
                }
                cnt++;
            }
        });
        if(!err){
            if((cnt) > 0  ){
                $("#TrnPromiseExAddConfirmForm").attr('action' ,'<?php echo $this->webroot ?><?php echo $this->name; ?>/ex_add_result/').submit();
            }else{
                alert("商品を選択してください");
                return false;
            }
        }
    });
});
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/extraordinary/">臨時要求入力</a></li>
        <li>臨時要求入力確認</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>臨時要求入力確認</span></h2>
<h2 class="HeaddingMid">臨時要求を行いたい商品を選択し、確定ボタンをクリックしてください。</h2>
<?php echo $this->form->create('TrnPromise',array('class'=>'validate_form' , 'type' => 'post','action' => '' ,'id' =>'TrnPromiseExAddConfirmForm')); ?>
    <?php echo $this->form->input('TrnPromise.token',array('type'=>'hidden'));?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <colgroup>
                <col />
                <col />
                <col width="20" />
                <col />
                <col />
            </colgroup>
            <tr>
                <th>要求日</th>
                <td>
                    <?php echo $this->form->text('TrnPromise.work_date',array('class'=>'lbl','readOnly' =>'readOnly')); ?>
                </td>
                <td></td>
                <th>作業区分</th>
                <td>
                    <?php echo $this->form->text('TrnPromise.work_name',array('class'=>'lbl','readOnly' =>'readOnly')); ?>
                    <?php echo $this->form->text('TrnPromise.work_type',array('type'=>'hidden')); ?>
                </td>
            </tr>
            <tr>
                <th>施設</th>
                <td>
                    <?php echo $this->form->text('TrnPromise.facility_name',array('class'=>'lbl','readOnly' =>'readOnly')); ?>
                    <?php echo $this->form->text('TrnPromise.facility_id',array('type'=>'hidden')); ?>
                </td>
                <td></td>
                <th>備考</th>
                <td>
                    <?php echo $this->form->text('TrnPromise.recital',array('class'=>'lbl','readOnly' =>'readOnly')); ?>
                </td>
            </tr>
            <tr>
                <th>部署</th>
                <td>
                    <?php echo $this->form->text('TrnPromise.department_name',array('class'=>'lbl','readOnly' =>'readOnly')); ?>
                    <?php echo $this->form->text('TrnPromise.department_id',array('type'=>'hidden')); ?>
                </td>
            </tr>
        </table>
        <hr class="Clear" />
        <div class="TableScroll2">
            <table class="TableStyle01 table-even2">
                <thead>
                <tr>
                    <th rowspan="2" width="20"><input type="checkbox" checked  class="checkAll"></th>
                    <th class="col10">商品ID</th>
                    <th>商品名</th>
                    <th class="col20">製品番号</th>
                    <th class="col10">包装単位</th>
                    <th class="col15">数量</th>
                    <th class="col15">作業区分</th>
                </tr>
                <tr>
                    <th></th>
                    <th>規格</th>
                    <th>販売元</th>
                    <th>在庫数</th>
                    <th colspan="2">備考</th>
                </tr>
                </thead>
                <tbody>
            <?php
              $i = 0;
              foreach($result as $r) { ?>
                <tr>
                    <td rowspan="2" class="center">
                    <?php
                        if(!is_null($r['TrnPromise']['id'])){
                            echo $this->form->checkbox('TrnPromise.id.'.$i,array('checked' =>1, 'class' =>'center chk' , 'value'=>$r['TrnPromise']['id'] , 'hiddenField'=>false) );
                        }else{
                            $r['TrnPromise']['id'] = 0;
                        }
                    ?>
                    </td>
                    <td><?php echo h_out($r['TrnPromise']['internal_code'],'center'); ?></td>
                    <td><?php echo h_out($r['TrnPromise']['item_name']); ?></td>
                    <td><?php echo h_out($r['TrnPromise']['item_code']); ?></td>
                    <td><?php echo h_out($r['TrnPromise']['unit_name']); ?></td>
                    <td>
                    <?php
                        if(isset($r['TrnPromise']['quantity'])){
                            //CSV取り込みの場合
                            echo $this->form->text('TrnPromise.quantity.'.$r['TrnPromise']['id'] ,array('class' =>'quantity num lbl ' ,'maxlength'=>'9','style' => 'width:90%' , 'readonly'=>'readonly' , 'value'=>$r['TrnPromise']['quantity']));
                        }else{
                            //検索からの場合
                            echo $this->form->text('TrnPromise.quantity.'.$r['TrnPromise']['id'] ,array('id'=>'quantity'.$r['TrnPromise']['id'] , 'class' =>'quantity num txt r ' ,'maxlength'=>'9','style' => 'width:90%'));
                        }
                    ?>
                    </td>
                    <td class="col15">
                        <?php echo $this->form->input('TrnPromise.work_type_.'.$r['TrnPromise']['id'] ,array('options'=>$workType,'empty'=>true,'value'=>$this->request->data['TrnPromise']['work_type'],'class' => 'txt')); ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo h_out($r['TrnPromise']['standard']);?></td>
                    <td><?php echo h_out($r['TrnPromise']['dealer_name']); ?></td>
                    <td><?php echo h_out($r['TrnPromise']['stock_count'],'right'); ?></td>
                    <td colspan="2">
                    <?php
                        if($r['TrnPromise']['alert'] != ''){
                            echo $this->form->text('TrnPromise.recital_.'.$r['TrnPromise']['id'] ,array('class'=>'txt','value'=> $r['TrnPromise']['alert'], 'style' => 'width:90%;color:red;') );
                            echo $this->form->input('TrnPromise.alert_msg.'.$r['TrnPromise']['id'] , array('type'=>'hidden', 'value'=>$r['TrnPromise']['alert']));
                        }elseif(isset($r['TrnPromise']['recital'])){
                            echo $this->form->text('TrnPromise.recital_.'.$r['TrnPromise']['id'] ,array('class'=>'lbl','value'=> $r['TrnPromise']['recital'], 'style' => 'width:90%' ,'readonly'=>'readonly') );
                        }else{
                            echo $this->form->text('TrnPromise.recital_.'.$r['TrnPromise']['id'] ,array('class'=>'txt', 'style' => 'width:90%') );
                        }
                    ?>
                    </td>
                </tr>
          <?php
                $i++;
              }
          ?>
            </tbody>
            </table>
        </div>
        <div class="ButtonBox">
            <input type="button" class="btn btn2 fix_btn" id="submitBtn" />
        </div>
    </div>
</form>
