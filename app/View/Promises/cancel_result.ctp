<script type="text/javascript">
function anc2(){
   obj = $('#pageDow')[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}
</script>

<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot?>login/home/">TOP</a></li>
        <li><a href="<?php echo $this->webroot?><?php echo $this->name; ?>/history/">要求作成履歴</a></li>
        <li>要求作成履歴明細結果</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>要求作成履歴明細結果</span></h2>
<div class="Mes01">以下の要求作成を取り消しました</div>

<table style="width:100%;">
    <tr>
        <td></td>
        <td align="right" id="pageTop" ></td>
    </tr>
</table>
<div class="TableScroll2">
    <table class="TableStyle01 table-even2">
        <thead>
        <tr>
            <th style="width:150px;">要求番号</th>
            <th style="width:80px">要求日</th>
            <th style="width:70px">商品ID</th>
            <th class="col10" style="width:200px">商品名</th>
            <th class="col10">製品番号</th>
            <th class="col10">包装単位</th>
            <th style="width:60px;">在庫数</th>
            <th style="width:60px;">要求済</th>
            <th style="width:60px;">作成数</th>
            <th style="width:80px;">作業区分</th>
            <th style="width:70px;">更新者</th>
        </tr>
        <tr>
            <th colspan="2">施設　／　部署</th>
            <th>状態</th>
            <th>規格</th>
            <th>販売元</th>
            <th>管理区分</th>
            <th>定数</th>
            <th>消費数</th>
            <th>未受領</th>
            <th colspan="2">備考</th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 0; foreach($result as $r) { ?>
        <tr>
            <td><?php echo h_out($r['TrnPromiseHeaders']['work_no']); ?></td>
            <td><?php echo h_out($r['TrnPromiseHeaders']['work_date']); ?></td>
            <td><?php echo h_out($r['MstFacilityItem']['internal_code'],'center'); ?></td>
            <td><?php echo h_out($r['MstFacilityItem']['item_name']); ?></td>
            <td><?php echo h_out($r['MstFacilityItem']['item_code']); ?></td>
            <td><?php echo h_out($r['MstUnitNames']['unit_name']); ?></td>
            <td><?php echo h_out($r['TrnPromise']['stock_count'],'right'); ?></td>
            <td><?php echo h_out($r['TrnPromise']['requested_count'],'right'); ?></td>
            <td><?php echo h_out($r['TrnPromise']['quantity'],'right'); ?></td>
            <td><?php echo h_out($r['TrnPromise']['work_name'],'right'); ?></td>
            <td><?php echo h_out($r['MstUser']['user_name']); ?></td>
        </tr>
        <tr>
            <td colspan="2"><?php echo h_out($r['TrnPromiseHeaders']['facility_name']); ?></td>
            <td><?php echo h_out($r['TrnPromise']['promise_status_name'],'center'); ?></td>
            <td><?php echo h_out($r['MstFacilityItem']['standard']); ?></td>
            <td><?php echo h_out($r['MstDealer']['dealer_name']); ?></td>
            <td><?php echo h_out($r['TrnPromise']['promise_type_name'],'center'); ?></td>
            <td><?php echo h_out($r['TrnPromise']['fixed_count'],'right'); ?></td>
            <td><?php echo h_out($r['TrnPromise']['consume_count'],'right'); ?></td>
            <td><?php echo h_out($r['TrnPromise']['unreceipt_count'],'right'); ?></td>
            <td colspan="2"><input value="<?php echo h($r['TrnPromise']['recital']); ?>" type="text" class="lbl" readonly></td>
        </tr>
    <?php $i++; } ?>
    </table>
</div>
<table style="width:100%;" id="pageDow">
    <tr>
        <td></td>
        <td align="right"></td>
    </tr>
</table>
