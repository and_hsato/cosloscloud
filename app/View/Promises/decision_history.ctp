<script type="text/javascript">
$(function($){
    //チェックボックス一括制御
    $('.checkAll').click(function(){
        $('input[type=checkbox].chk').attr('checked',$(this).attr('checked'));
    });

    //検索ボタン押下
    $("#revealResult").click(function(){
        $("#HistoryForm").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/decision_history/');
        $("#HistoryForm").attr('method', 'post');
        $("#HistoryForm").submit();
    });
  
    //印刷ボタン押下
    $("#btn_Print").click(function(){
        $("#HistoryForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/picking_list');
        $("#HistoryForm").attr('method', 'post');
        $("#HistoryForm").submit();
        $("#HistoryForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/decision_history');
    });  

    //補充リスト印刷ボタン押下
    $("#btn_xxx").click(function(){
        $("#HistoryForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/report/xxx_list');
        $("#HistoryForm").attr('method', 'post');
        $("#HistoryForm").submit();
        $("#HistoryForm").attr('action', '<?php echo $this->webroot; ?><?php echo $this->name; ?>/decision_history');
    });  

  
    //CSVボタン押下
    $("#btn_Csv").click(function(){
         $("#HistoryForm").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/decision_export_csv/');
         $("#HistoryForm").attr('method', 'post');
         $("#HistoryForm").submit();
         $("#HistoryForm").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/decision_history/');
    });

    // 明細表示ボタン押下
    $("#toConfirm").click(function(){
        if($('input[type=checkbox].chk:checked').length === 0){
            alert('閲覧する明細を選択してください');
            return false;
        }else{
            $("#HistoryForm").attr('action' , '<?php echo $this->webroot ?><?php echo $this->name; ?>/decision_cancel_confirm/');
            $("#HistoryForm").attr('method', 'post');
            $("#HistoryForm").submit();
        }
    });
});

function anc(id){
   obj = $("#"+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}
function anc2(){
   obj = $("#results")[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}
</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li>引当作成履歴</li>
    </ul>
</div>

<h2 class="HeaddingLarge"><span>引当作成履歴</span></h2>
<h2 class="HeaddingMid">引当作成履歴</h2>
<form class="validate_form search_form input_form" method="post" action="<?php echo $this->webroot; ?><?php echo $this->name; ?>/decision_history" id="HistoryForm">
    <?php echo $this->form->input('TrnFixedPromise.is_search',array('type' => 'hidden','id' => 'is_search'));?>
    <?php echo $this->form->input('MstFacility.id',array('type' => 'hidden','value'=>$this->Session->read('Auth.facility_id_selected'))); ?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>引当番号</th>
                <td><?php echo $this->form->text('TrnFixedPromiseHeader.work_no',array('class' => 'txt','style' => 'width:110px'));?></td>
                <td></td>
                <th>引当日</th>
                <td>
                    <?php echo $this->form->text('TrnFixedPromiseHeader.work_date1',array('class'=>'validate[optional,custom[date],funcCall2[date2]] date', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                    <span>&nbsp;～&nbsp;</span>
                    <?php echo $this->form->text('TrnFixedPromiseHeader.work_date2',array('class'=>'validate[optional,custom[date],funcCall2[date2]] date', 'id'=> 'datepicker2', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?>
                </td>
                <td></td>
                <td colspan="2">
                  <?php echo $this->form->checkbox('TrnFixedPromiseHeader.delete_opt',array('class' =>'center' , 'hiddenField' => false )); ?>取消も表示する
                  <?php echo $this->form->checkbox('TrnFixedPromiseHeader.remain_count',array('class' =>'center' , 'hiddenField' => false )); ?>未処理のみ表示する
                </td>
            </tr>
            <tr>
                <th>引当施設</th>
                <td>
                    <?php echo $this->form->input('TrnFixedPromiseHeader.facilityText',array('class'=>'txt require','style'=>'width:60px;','id'=>'facilityText')); ?>
                    <?php echo $this->form->input('TrnFixedPromiseHeader.facilityCode',array('options'=>$facility_List,'class'=>'txt','style'=>'width:120px;','id'=>'facilityCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('TrnFixedPromiseHeader.facilityName',array('type'=>'hidden','id'=>'facilityName')); ?>
                </td>
                <td></td>
                <th>引当部署</th>
                <td>
                    <?php echo $this->form->input('TrnFixedPromiseHeader.departmentText',array('class'=>'txt','style'=>'width:60px;','id'=>'departmentText')); ?>
                    <?php echo $this->form->input('TrnFixedPromiseHeader.departmentCode',array('options'=>$department_List, 'class'=>'txt','style'=>'width:120px;','id'=>'departmentCode' , 'empty'=>true)); ?>
                    <?php echo $this->form->input('TrnFixedPromiseHeader.departmentName',array('type'=>'hidden','id'=>'departmentName')); ?>
                </td>

                <td></td>
                <td colspan="2">
            </tr>
            <tr>
                <th>商品ID</th>
                <td><?php echo $this->form->text('MstFacilityItem.internal_code',array('class' => 'txt search_internal_code','style' => 'width:120px',)); ?></td>
                <td></td>
                <th>製品番号</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_code',array('class' => 'txt search_upper','style' => 'width:120px',)); ?></td>
                <td></td>
                <th>管理区分</th>
                <td>
                <?php echo $this->form->input('TrnFixedPromiseHeader.promise_type' , array('options'=>Configure::read('PromiseType.list') , 'style' => 'width:120px','class' => 'txt' , 'empty'=>true)); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>商品名</th>
                <td><?php echo $this->form->text('MstFacilityItem.item_name',array('class' => 'search_canna txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>販売元</th>
                <td><?php echo $this->form->text('MstFacilityItem.dealer_name',array('class' => 'search_canna txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>作業区分</th>
                <td>
                <?php echo $this->form->input('TrnFixedPromiseHeader.work_type', array('options'=>$workType,'style' => 'width:120px','class' => 'txt' ,'empty'=>true)); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>規格</th>
                <td><?php echo $this->form->text('MstFacilityItem.standard',array('class' => 'search_canna txt','style'=>'width:120px')); ?></td>
                <td></td>
                <th>JANコード</th>
                <td><?php echo $this->form->text('MstFacilityItem.jan_code',array('class' => 'txt','style'=>'width:120px')); ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

        </table>
        <div class="ButtonBox">
            <p class="center">
                <input type="button" class="btn btn1" id="revealResult" />
                <input type="button" class="btn btn5" id="btn_Csv"/>
                <input type="button" class="btn btn10" id="btn_Print"/>
                <input type="button" class="btn btn111" id="btn_xxx"/>
            </p>
        </div>
    </div>
    <hr class="Clear" />
    <div class="DisplaySelect" id="pageDow">
        <div align="right" style="padding-right: 10px;"></div>
        <?php echo $this->element('limit_combobox',array('param'=>'TrnFixedPromise.limit' , 'result'=>count($result) ) ); ?>
    </div>
    <div class="TableScroll">
        <table class="TableStyle01 table-even">
            <thead>
            <tr>
                <th style="width:20px;" class="center"><input type="checkbox" class="chk checkAll" /></th>
                <th style="width:135px;">引当番号</th>
                <th style="width:85px;" class="col10">引当日</th>
                <th>施設／部署</th>
                <th class="col10">登録者</th>
                <th style="width: 140px;">登録日時</th>
                <th class="col15">備考</th>
                <th class="col10">件数</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 0; foreach($result as $r) { ?>
            <tr>
                <td class="center">
                    <?php echo $this->form->checkbox('TrnFixedPromiseHeader.id.'.$i,array('class' => 'checkAllTarget center chk' , 'hiddenField' => false , 'value'=>$r['TrnFixedPromiseHeader']['id']) );?>
                </td>
                <td><?php echo h_out($r['TrnFixedPromise']['work_no'],'center'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['work_date'],'center'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['facility_name'] . ' / ' . $r['TrnFixedPromise']['department_name']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromiseHeader']['user_name']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['created'],'center'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromiseHeader']['recital']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['count'] . ' / ' . $r['TrnFixedPromise']['detail_count'],'right'); ?></td>
            </tr>
        <?php $i++; } ?>
        <?php if(isset($this->request->data['TrnFixedPromise']['is_search']) && count($result) == 0){ ?>
            <tr><td colspan="8" class="center">該当するデータがありませんでした</td></tr>
        <?php } ?>
        </tbody>
        </table>
    </div>
    <div class="ButtonBox" id="results">
        <p class="center">
            <input type="button" class="btn btn7 submit" id="toConfirm" />
        </p>
    </div>
    <div align="right"></div>
</form>
