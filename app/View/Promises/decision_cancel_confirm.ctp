<script type="text/javascript">
$(function(){
    $('.checkAll').click(function(){
        $('input[type=checkbox]').attr('checked',$(this).attr('checked'));
    });
    $('#comments_all_txt').change(function(){
        $('.TrnPromiseRecital').val($('#comments_all_txt').val());
    });

    // 取消ボタン押下
    $("#toResult").click(function(){
        if($('input[type=checkbox].checkAllTarget:checked').length === 0){
            alert('取消する明細を選択してください');
            return false;
        }else{
            if(window.confirm("取消を実行します。よろしいですか？")){
                $("#TrnFixedPromiseDecisionCancelConfirmForm").attr('action' ,'<?php echo $this->webroot ?><?php echo $this->name; ?>/decision_cancel_result/');
                $("#TrnFixedPromiseDecisionCancelConfirmForm").attr('method', 'post');
                $("#TrnFixedPromiseDecisionCancelConfirmForm").submit();
            }
        }
     });
});
function anc2(){
   obj = $('#pageDow')[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

function anc(id){
   obj = $('#'+id);
   y = obj.offsetTop;
   scrollTo(0,y);
}
</script>
    <div id="TopicPath">
        <ul>
            <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
            <li><a href="<?php echo $this->webroot; ?><?php echo $this->name; ?>/decision_history/">引当作成履歴</a></li>
            <li>引当作成履歴編集</li>
        </ul>
    </div>
    <h2 class="HeaddingLarge"><span>引当作成履歴編集</span></h2>
    <h2 class="HeaddingMid">引当作成履歴編集</h2>
<?php echo $this->form->create('TrnFixedPromise',array('class' => 'validate_form','type' => 'post','action' => '' ,'id' =>'TrnFixedPromiseDecisionCancelConfirmForm')); ?>
    <?php echo $this->form->input('promise.time',array('type' => 'hidden' , 'value'=> date('Y/m/d H:i:s.U')));?>
    <?php echo $this->form->input('Promise.token',array('type' => 'hidden'));?>
    
    <table style="width:100%;">
        <tr>
            <td></td>
            <td align="right" id="pageTop" ></td>
        </tr>
        <tr>
            <td>
                <?php echo "表示件数：".count($result)."件"; ?>
            </td>
            <td align="right">
                <input type="text" class="txt" style="width:150px;" id="comments_all_txt">
                <input type="button" class="btn btn8 [p2]" value="">
            </td>
        </tr>
    </table>
    <div class="TableScroll2">
        <table class="TableStyle01 table-even2">
            <thead>
            <tr>
                <th style="width: 20px;" rowspan="2" class="chk"><input type="checkbox" class="checkAll" /></th>
                <th style="width:150px;">引当番号</th>
                <th style="width:80px;">引当日</th>
                <th style="width:80px;">商品ID</th>
                <th>商品名</th>
                <th>製品番号</th>
                <th style="width:80px;">包装単位</th>
                <th style="width:70px;">数量</th>
                <th style="width:80px;">作業区分</th>
                <th style="width:70px;">更新者</th>
            </tr>
            <tr>
                <th colspan="2">施設　／　部署</th>
                <th>状態</th>
                <th>規格</th>
                <th>販売元</th>
                <th></th>
                <th>残数</th>
                <th colspan="2">備考</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 0; foreach($result as $r) { ?>
            <tr>
                <td rowspan="2" class="center">
            <?php
              if($r['TrnFixedPromise']['check'] == true ){
                echo $this->form->checkbox('TrnFixedPromise.id.'.$i,array('value'=>$r['TrnFixedPromise']['id'] , 'hiddenField'=>false , 'class'=>'checkAllTarget'));
              }
            ?>
                </td>
                <td><?php echo h_out($r['TrnFixedPromise']['work_no'],'center'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['work_date'],'center'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['internal_code'],'center'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['item_name']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['item_code']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['unit_name']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['quantity'],'right'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['work_name']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['user_name']); ?></td>
            </tr>
            <tr>
                <td colspan="2">
                <label title="<?php echo h($r['TrnFixedPromise']['facility_name']) . ' / ' . h($r['TrnFixedPromise']['department_name']) ; ?>">
                <?php
                  echo h($r['TrnFixedPromise']['facility_name']) . ' / ' . h($r['TrnFixedPromise']['department_name']);
                ?>
                </label>
                </td>
                <td><?php echo h_out($r['TrnFixedPromise']['fixed_promise_status'],'center'); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['standard']); ?></td>
                <td><?php echo h_out($r['TrnFixedPromise']['dealer_name']); ?></td>
                <td></td>
                <td><?php echo h_out($r['TrnFixedPromise']['remain_count'],'right'); ?></td>
                <td colspan="2">
                <?php
                if($r['TrnFixedPromise']['check'] == true){
                  echo $this->form->text('TrnFixedPromise.recital.'.$i,array('value'=>h($r['TrnFixedPromise']['recital']),'class'=>'TrnPromiseRecital txt'));
                }else{
                  echo $this->form->text('TrnFixedPromise.recital.'.$i,array('value'=>h($r['TrnFixedPromise']['recital']),'class'=>'lbl','readOnly'=>'readOnly'));
                }
                ?>
                </td>
            </tr>
            <?php $i++; } ?>
            </tbody>
        </table>
    </div>
    <table style="width:100%;" id="pageDow">
        <tr>
            <td></td>
            <td align="right"></td>
        </tr>
    </table>
    <div class="ButtonBox">
         <input type="button" class="btn btn6 fix_btn [p2]" id="toResult" />
    </div>
<?php echo $this->form->end(); ?>