<script type="text/javascript">
$(function($){
    $("#datepicker1").datepicker({showOn: 'button', buttonImage: '<?php echo $this->webroot; ?>img/calendar.png', buttonImageOnly: true});

    $("#move1").click(function(){
        if(!dateCheck($('#datepicker1').val())){
            alert('要求日が不正です');
            return false;
        }else{
            if (!confirm('全ての施設・部署で要求を作成しますがよろしいですか？')){
                return false;
            }
            $("#promiseAddForm").attr('action' , '<?php echo $this->webroot; ?>promises/add_result/');
            $("#promiseAddForm").attr('method', 'post');
            $("#promiseAddForm").submit();
        }
    });

    $("#move2").click(function(){
        if(!dateCheck($('#datepicker1').val()) ){
            alert('要求日が不正です');
            return false;
        }else{
            $("#promiseAddForm").attr('action' , '<?php echo $this->webroot; ?>promises/select/');
            $("#promiseAddForm").attr('method', 'post');
            $("#promiseAddForm").submit();
        }
    });
});

</script>
<div id="TopicPath">
    <ul>
        <li><a href="<?php echo $this->webroot; ?>login/home/">TOP</a></li>
        <li>引当要求作成</li>
    </ul>
</div>
<h2 class="HeaddingLarge"><span>要求作成部署選択</span></h2>
<?php echo $this->form->create('promise',array('action' => '','type'=>'post' ,'id'=>'promiseAddForm','name' => 'form', 'class'=>'validate_form')); ?>
    <?php echo $this->form->input('promise.time' , array('type' =>'hidden' ,'value'=> date('Y/m/d H:i:s.U')))?>
    <?php echo $this->form->input('TrnPromise.token' , array('type' =>'hidden' ))?>
    <div class="SearchBox">
        <table class="FormStyleTable">
            <tr>
                <th>使用定数</th>
                <td colspan="4"><?php echo $this->form->radio('promise.fixed_num',$fixedNum,array('value' => Configure::read('FixedType,Constant'),'label' => '','legend'=>'' )); ?></td>
            </tr>
            <tr>
                <th>要求日</th>
                <td><?php echo $this->form->input('promise.work_date',array('class'=>'date r', 'id'=> 'datepicker1', 'div' => 'false', 'label' => 'false', 'maxlength' => '10' )); ?></td>
                <td width="20"></td>
                <th>作業区分</th>
                <td><?php echo $this->form->input('promise.work_type',array('options'=>$workType,'class' => 'txt' ,'empty'=>true)); ?></td>
            </tr>
            <tr>
                <th></th>
                <td></td>
                <td></td>
                <th>備考</th>
                <td><?php echo $this->form->text('promise.recital',array('class'=>'txt')); ?></td>
            </tr>
        </table>
    <div class="ButtonBox">
        <input type="button" value="" class="btn btn73" id="move1" />
    &nbsp;&nbsp;&nbsp;
        <input type="button" value="" class="btn btn4" id="move2" />
    </div>
<?php echo $this->form->end(); ?>