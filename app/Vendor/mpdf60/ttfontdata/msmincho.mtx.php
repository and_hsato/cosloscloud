<?php
$name='MS-Mincho';
$type='TTF';
$desc=array (
  'CapHeight' => 680,
  'XHeight' => 449,
  'FontBBox' => '[-82 -137 996 859]',
  'Flags' => 5,
  'Ascent' => 859,
  'Descent' => -137,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 1000,
);
$unitsPerEm=256;
$up=-94;
$ut=47;
$strp=258;
$strs=51;
$ttffile='C:/xampp/htdocs/cosloscloud/app/Vendor/mpdf60/ttfonts/msmincho.ttf';
$TTCfontID='0';
$originalsize=9935360;
$sip=true;
$smp=false;
$BMPselected=false;
$fontkey='msmincho';
$panose=' 1 5 2 2 6 9 4 2 5 8 3 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 859, -141, 0
// usWinAscent/usWinDescent = 859, -141
// hhea Ascent/Descent/LineGap = 859, -141, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>