<?php 
App::import('Vendor','tcpdf/tcpdf'); 
 
class XTCPDF  extends TCPDF 
{ 
 
    var $xheadertext  = 'PDF created using CakePHP and TCPDF'; 
    var $xheadercolor = array(0,0,200); 
    var $xfootertext  = 'Copyright Ac %d XXXXXXXXXXX. All rights reserved.'; 
    var $xfooterfont  = PDF_FONT_NAME_MAIN ; 
    var $xfooterfontsize = 8 ; 
 
 
    /** 
    * Overwrites the default header // デフォルトのheader()関数を上書き
    * set the text in the view using // ビューの文字列は以下のようにして設定します。
    *    $fpdf->xheadertext = 'YOUR ORGANIZATION'; 
    * set the fill color in the view using // ビューの塗りつぶしの色は以下のようにして設定します。
    *    $fpdf->xheadercolor = array(0,0,100); (r, g, b) 
    * set the font in the view using // ビューのフォントは以下のようにして設定します。
    *    $fpdf->setHeaderFont(array('YourFont','',fontsize)); 
    */ 
    function xxHeader() 
    { 
 
        //list($r, $b, $g) = $this->xheadercolor; 
        $this->setY(5); // shouldn't be needed due to page margin, but helas, otherwise it's at the page top
        //$this->SetFillColor($r, $b, $g); 
        $this->SetTextColor(0 , 0, 0); 
//        $this->Cell(0,20, '', 0,1,'C', 1); 
        $this->Text(15,26,$this->xheadertext ); 
    } 
 
    /** 
    * Overwrites the default footer // デフォルトのfooter()を上書き
    * set the text in the view using // ビューの文字列は以下のようにして設定します。
    * $fpdf->xfootertext = 'Copyright Ac %d YOUR ORGANIZATION. All rights reserved.'; 
    */ 
    function xxFooter() 
    { 
        $year = date('Y'); 
        $footertext = sprintf($this->xfootertext, $year); 
        $this->SetY(-20); 
        $this->SetTextColor(0, 0, 0); 
        $this->SetFont($this->xfooterfont,'',$this->xfooterfontsize); 
        $this->Cell(0,8, $footertext,'T',1,'C'); 
    } 
} 
?>