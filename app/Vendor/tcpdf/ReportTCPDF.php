<?php 
App::import('Vendor','tcpdf/tcpdf'); 
 
class ReportTCPDF extends TCPDF {
    
    var $htmlHeader;
    
    public function setHtmlHeader($htmlHeader) {
        $this->htmlHeader = $htmlHeader;
    }
    
    public function Header() {
        if (empty($this->htmlHeader)) {
           parent::Header(); 
        } else {
            $this->writeHTML($this->htmlHeader, true, false, false, false, '');
        }
    }
}

?>