<?php
class TrnStock extends AppModel {
    var $name = 'TrnStock';
    var $belongsTo = array(
        "MstItemUnit" => array(
            'className'  => 'MstItemUnit',
            'foreignKey' => 'mst_item_unit_id',
            ),
        'MstDepartment' => array(
            'className' => 'MstDepartment',
            'foreignKey' => 'mst_department_id',
            'dependent' => false,
            ),
        );
    var $hasOne = array(
        "MstFixedCount" => array(
            'className'  => 'MstFixedCount',
            'foreignKey' => 'trn_stock_id',
            )
        );
    
    
    
    var $validate = array(
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_department_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'stock_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'reserve_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'promise_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'requested_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'receipted_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
    
    /**
     * 在庫を検索
     * @param $search_conditions 検索条件
     * @param $params オプション
     */
    function search( $search_conditions , $params = array() ) {
        $_sql = $this->__getSearchSqlSelect() . $this->__getSearchSqlWhere( $search_conditions );
        if( isset($params['order'])) {
            if( is_array($params['order'])){
              $_sql = $_sql . 'order by ' . $params['order'][0];
            } else { 
              $_sql = $_sql . 'order by ' . $params['order'];
            }
        }
        
        if( isset($params['limit'])) {
            $_sql = $_sql . ' limit ' . $params['limit'];
        } else {
            $_sql = $_sql . ' limit 100 ';
        }
        
        return $this->query( $_sql );
    }
    
    private function __getSearchSqlSelect() {
        $_sql = ' select 
         TrnStock.id as "TrnStock__id",
       TrnStock.stock_count as "TrnStock__stock_count",
       TrnStock.reserve_count as "TrnStock__reserve_count",
       TrnStock.promise_count as "TrnStock__promise_count",
       TrnStock.requested_count as "TrnStock__requested_count",
       TrnStock.receipted_count as "TrnStock__receipted_count",
       ( TrnStock.stock_count - TrnStock.reserve_count - TrnStock.promise_count ) as "TrnStock__enable_promise_count",
      MstFacilityItem.internal_code as "MstFacilityItem__internal_code",     
      MstFacilityItem.item_name as "MstFacilityItem__item_name" ,  
      MstFacilityItem.item_code as "MstFacilityItem__item_code" , 
      MstFacilityItem.standard as "MstFacilityItem__standard" , 
      MstFacilityItem.is_lowlevel as "MstFacilityItem__is_lowlevel",         
      MstFixedCount.order_point as "MstFixedCount__order_point",
      MstFixedCount.fixed_count as "MstFixedCount__fixed_count",
      MstFixedCount.holiday_fixed_count as "MstFixedCount__holiday_fixed_count",
      MstFixedCount.spare_fixed_count as "MstFixedCount__spare_fixed_count",            
      MstShelfName.code as "MstShelfName__code",                  
      MstItemUnit.per_unit as "MstItemUnit__per_unit",      
      MstUnitName.unit_name as "MstUnitName__unit_name",  
      MstPerUnitName.unit_name as "MstPerUnitName__unit_name", 
      MstDealer.dealer_name as "MstDealer__dealer_name",           
      MstDepartment.department_name as "MstDepartment__department_name",                         
      MstFacility.facility_name as "MstFacility__facility_name"       
     from
      trn_stocks as TrnStock 
         inner join mst_item_units as MstItemUnit on TrnStock.mst_item_unit_id = MstItemUnit.id
          left join mst_unit_names as MstUnitName on MstItemUnit.mst_unit_name_id = MstUnitName.id
          left join mst_unit_names as MstPerUnitName on MstItemUnit.per_unit_name_id = MstPerUnitName.id 
          inner join mst_facility_items as MstFacilityItem on MstItemUnit.mst_facility_item_id = MstFacilityItem.id
          left join mst_dealers as MstDealer on MstFacilityItem.dealer_code = MstDealer.dealer_code
          inner join mst_departments as MstDepartment on TrnStock.mst_department_id = MstDepartment.id
          inner join mst_facilities as MstFacility on MstDepartment.mst_facility_id = MstFacility.id
          left join mst_fixed_counts as MstFixedCount on TrnStock.id = MstFixedCount.trn_stock_id
          left join mst_shelf_names as MstShelfName on MstFixedCount.mst_shelf_name_id = MstShelfName.id
          left join mst_facilities as Owner on MstFacilityItem.mst_owner_id = Owner.id 
          left join mst_items as MstItem on MstFacilityItem.mst_item_id = MstItem.id          
    ';
        return $_sql;
    }
    
    private function __getSearchSqlWhere( $search_conditions ) {
        // 条件文
        $_where = "1 = 1 ";
        
        // 商品ID
        if( isset($search_conditions['MstFacilityItem']['internal_code']) &&  $search_conditions['MstFacilityItem']['internal_code'] != '') {
            $_where .= "and MstFacilityItem.internal_code like '%" . pg_escape_string($search_conditions['MstFacilityItem']['internal_code']) . "%'";
        }
        
        // 製品番号    
        if( isset($search_conditions['MstFacilityItem']['item_code']) &&  $search_conditions['MstFacilityItem']['item_code'] != '') {
            $_where .= "and MstFacilityItem.item_code like '%" . pg_escape_string($search_conditions['MstFacilityItem']['item_code']) . "%'";
        }
        
        // 商品名
        if( isset($search_conditions['MstFacilityItem']['item_name']) &&  $search_conditions['MstFacilityItem']['item_name'] != '') {
            $_where .= "and MstFacilityItem.item_name like '%" . pg_escape_string($search_conditions['MstFacilityItem']['item_name']) . "%'";
        }
        
        // 規格
        if( isset($search_conditions['MstFacilityItem']['standard']) &&  $search_conditions['MstFacilityItem']['standard'] != '') {
            $_where .= "and MstFacilityItem.standard like '%" . pg_escape_string($search_conditions['MstFacilityItem']['standard']) . "%'";
        }
        
        // 販売元
        if( isset($search_conditions['MstDealer']['dealer_name']) &&  $search_conditions['MstDealer']['dealer_name'] != '') {
            $_where .= "and MstDealer.dealer_name like '%" . pg_escape_string($search_conditions['MstDealer']['dealer_name']) . "%'";
        }
        
        // 管理区分での検索
        if( isset($search_conditions['trade_type']) &&  $search_conditions['trade_type'] != '') {    
            $_where .= " and MstFixedCount.trade_type= '" . $search_conditions['trade_type'] . "'";      
        }
        
        // 施主
        if( isset($search_conditions['owner']) &&  $search_conditions['owner'] != '') {    
            $_where .= " and Owner.facility_code = '" . $search_conditions['owner'] . "'";      
        }
        
        // マスタID
        if( isset($search_conditions['MstFacilityItem']['master_id']) &&  $search_conditions['MstFacilityItem']['master_id'] != '') {
            $_where .= "and MstFacilityItem.master_id like '%" . pg_escape_string($search_conditions['MstFacilityItem']['master_id']) . "%'";
        }
        
        // AWID
        if( isset($search_conditions['MstItem']['internal_code']) &&  $search_conditions['MstItem']['internal_code'] != '') {
            $_where .= " and MstItem.internal_code like '%" . $search_conditions['MstItem']['internal_code'] . "%'";
        }
        
        // ロット番号・シール番号
        if( ( isset($search_conditions['TrnSticker']['lot_no']) &&  $search_conditions['TrnSticker']['lot_no'] != '') || 
            ( isset($search_conditions['TrnSticker']['sticker_no']) &&  $search_conditions['TrnSticker']['sticker_no'] != '')
            ) {    
            $_subwhere = " select trn_stock_id ";
        }
        // 期限切れ
        
        $_where = " where " . $_where . " ";
        
        return $_where;
    }
}
?>