<?php
class TrnRetroactRecord extends AppModel {
    
    /**
     * 
     * @var string $name
     */
    var $name = 'TrnRetroactRecord';
    
    /**
     * 
     * @var array $belongsTo
     */
    var $belongsTo = array('TrnRetroactHeader','MstItemUnit');
    
    /**
     * 
     * @var array $validate
     */
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'trn_close_header_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>