<?php
class TrnPromise extends AppModel {
    var $name = 'TrnPromise';
    var $belongsTo = array(
        "TrnPromiseHeader" => array(
            'className'  => 'TrnPromiseHeader',
            'conditions' => '',
            'foreignKey' => 'trn_promise_header_id',
            'dependent'    => true
            ),
        'TrnFixedPromise' => array(
            'className' => 'TrnFixedPromise',
            'conditions' => null,
            'foreignKey' => 'id',
            'dependent' => false
            ),
        );
    
    
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'work_seq' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'promise_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'promise_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'before_quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'remain_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>