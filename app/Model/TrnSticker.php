<?php
App::import('Sanitize');
class TrnSticker extends AppModel {

  /**
   * Class name.
   * @var string $name
   */
  var $name = 'TrnSticker';

  /**
   *
   * @var array $hasOne
   */

    var $belongsTo = array('TrnReceiving','TrnShipping','TrnStorage','MstDepartment',
                           'MstItemUnit' => array(
                               'className' => 'MstItemUnit',
                               'foreignKey' => 'mst_item_unit_id'),
                           );
    
    /**
     *
     * @var array $validate
     */
    var $validate = array(
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_department_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'trade_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );

        /**
         * シールを検索する
         * @param 検索条件の配列
         * @param その他オプション
         */
        function search( $search_conditions , $params = array() , $selected_facility = null , $seleted_user = null , $count = false ) {
          $_sql = $this->__getSearchSqlSelect($seleted_user) . $this->__getSearchSqlWhere( $search_conditions,$selected_facility );
            if( isset($params['order'])) {
                if( is_array($params['order'])){
                    $_sql = $_sql . 'order by ' . $params['order'][0];
                } else {
                    $_sql = $_sql . 'order by ' . $params['order'];
                }
            }
            if($count){
                $sql = ' select count(*) as count from ( ' . $_sql . ' ) as x '; 
                return $this->query($sql);
            }
            
            if( isset($params['limit'])) {
                $_sql = $_sql . ' limit ' . $params['limit'];
            } else {
                $_sql = $_sql . ' limit 100 ';
            }
            return $this->query( $_sql );
        }

    /**
     * 検索用のSQL文を取得
     */
    private function __getSearchSqlSelect($user) {
        /*川鉄在庫数を表示させる */
        $sql  = ' select ';
        $sql .= '       trnsticker.id                       as "TrnSticker__id" ';
        $sql .= '     , trnsticker.lot_no                   as "TrnSticker__lot_no" ';
        $sql .= '     , trnsticker.facility_sticker_no      as "TrnSticker__facility_sticker_no" ';
        $sql .= '     , trnsticker.quantity                 as "TrnSticker__quantity" ';
        $sql .= '     , trnsticker.is_deleted               as "TrnSticker__is_deleted"';
        $sql .= "     , to_char(trnsticker.validated_date,'YYYY/mm/dd')";
        $sql .= '                                           as "TrnSticker__validated_date" ';
        $sql .= '     , trnsticker.hospital_sticker_no      as "TrnSticker__hospital_sticker_no" ';
        $sql .= '     , trnsticker.has_reserved             as "TrnSticker__has_reserved" ';
        $sql .= '     , trnsticker.original_price           as "TrnSticker__original_price" ';
        $sql .= '     , trnsticker.transaction_price        as "TrnSticker__transaction_price" ';
        $sql .= '     , trnsticker.modified                 as "TrnSticker__modified" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when (  ';
        $sql .= '           (trnsticker.facility_sticker_no is null)  ';
        $sql .= "           or (trnsticker.facility_sticker_no = '') ";
        $sql .= '         )  ';
        $sql .= '         and (  ';
        $sql .= '           (trnsticker.hospital_sticker_no is null)  ';
        $sql .= "           or (trnsticker.hospital_sticker_no = '') ";
        $sql .= '         )  ';
        $sql .= "         and trnsticker.lot_no != ' '  ";
        $sql .= '         and trnsticker.has_reserved = false  ';
        $sql .= '         and trnsticker.is_deleted = false  ';
        $sql .= "         then to_char(trnsticker.quantity, 'FM999,999,999')  ";
        $sql .= "         else ''  ";
        $sql .= '         end ';
        $sql .= '     )                                     as "TrnSticker__kawatetuzaiko_num" ';
        $sql .= '     , mstfacilityitem.id                  as "MstFacilityItem__id" ';
        $sql .= '     , mstfacilityitem.internal_code       as "MstFacilityItem__internal_code" ';
        $sql .= '     , mstfacilityitem.item_name           as "MstFacilityItem__item_name" ';
        $sql .= '     , mstfacilityitem.item_code           as "MstFacilityItem__item_code" ';
        $sql .= '     , mstfacilityitem.standard            as "MstFacilityItem__standard" ';
        $sql .= '     , mstfacilityitem.is_lowlevel         as "MstFacilityItem__is_lowlevel" ';
        $sql .= '     , mstfacilityitem.enable_medicalcode  as "MstFacilityItem__enable_medicalcode" ';
        $sql .= '     , medicalmstfacilityitem.medical_code as "MedicalMstFacilityItem__medical_code" ';
        $sql .= '     , coalesce(  ';
        $sql .= '       medicalmstfacilityitem.medical_code ';
        $sql .= '       , mstfacilityitem.medical_code ';
        $sql .= '     )                                     as "MstFacilityItem__medical_code" ';
        $sql .= '     , mstitemunit.per_unit                as "MstItemUnit__per_unit" ';
        $sql .= '     , mstunitname.unit_name               as "MstUnitName__unit_name" ';
        $sql .= '     , mstperunitname.unit_name            as "MstPerUnitName__unit_name" ';
        $sql .= '     , mstdealer.dealer_name               as "MstDealer__dealer_name" ';
        $sql .= '     , mstdepartment.department_name       as "MstDepartment__department_name" ';
        $sql .= '     , mstfacility.facility_name           as "MstFacility__facility_name" ';
        $sql .= '     , mstfacilityitem.is_lowlevel         as "MstFacilityItem__is_lowlevel" ';
        $sql .= '     , mstfixedcount.order_point           as "MstFixedCount__order_point" ';
        $sql .= '     , mstfixedcount.fixed_count           as "MstFixedCount__fixed_count" ';
        $sql .= '     , mstfixedcount.holiday_fixed_count   as "MstFixedCount__holiday_fixed_count" ';
        $sql .= '     , mstfixedcount.spare_fixed_count     as "MstFixedCount__spare_fixed_count" ';
        $sql .= '     , mstshelfname.code                   as "MstShelfName__code" ';
        $sql .= '     , trnstock.stock_count                as "TrnStock__stock_count" ';
        $sql .= '     , trnstock.reserve_count              as "TrnStock__reserve_count" ';
        $sql .= '     , trnstock.promise_count              as "TrnStock__promise_count" ';
        $sql .= '     , trnstock.requested_count            as "TrnStock__requested_count" ';
        $sql .= '     , trnstock.receipted_count            as "TrnStock__receipted_count" ';
        $sql .= '     , (  ';
        $sql .= '       trnstock.stock_count - trnstock.reserve_count - trnstock.promise_count ';
        $sql .= '     )                                     as "TrnStock__enable_promise_count" ';
        $sql .= '     , ( case  ';
        $sql .= '       when trnsticker.last_move_date is not null  ';
        $sql .= '       then date_part(  ';
        $sql .= "         'day' ";
        $sql .= '         , current_timestamp - trnsticker.last_move_date ';
        $sql .= '       )  ';
        $sql .= '       else 0  ';
        $sql .= '       end )                               as "TrnSticker__immovable"  ';
        $sql .= '     , ( case ';
        foreach(Configure::read('Classes') as $k => $v){
            $sql .= "   when trnsticker.trade_type = '" . $k . "' then '" . $v . "'";
        }
        $sql .= '       end )                               as "TrnSticker__trade_type_name" ';
        $sql .= '   from ';
        $sql .= '     trn_stickers as trnsticker  ';
        $sql .= '     inner join mst_item_units as mstitemunit  ';
        $sql .= '       on trnsticker.mst_item_unit_id = mstitemunit.id  ';
        $sql .= '     left join mst_unit_names as mstunitname  ';
        $sql .= '       on mstitemunit.mst_unit_name_id = mstunitname.id  ';
        $sql .= '     left join mst_unit_names as mstperunitname  ';
        $sql .= '       on mstitemunit.per_unit_name_id = mstperunitname.id  ';
        $sql .= '     left join mst_fixed_counts as mstfixedcount  ';
        $sql .= '       on mstitemunit.id = mstfixedcount.mst_item_unit_id  ';
        $sql .= '       and trnsticker.mst_department_id = mstfixedcount.mst_department_id  ';
        $sql .= '     inner join mst_facility_items as mstfacilityitem  ';
        $sql .= '       on mstitemunit.mst_facility_item_id = mstfacilityitem.id  ';
        $sql .= '     inner join mst_facilities as mstitemsfacility  ';
        $sql .= '       on mstitemsfacility.id = mstfacilityitem.mst_facility_id  ';
        $sql .= '     left join mst_dealers as mstdealer  ';
        $sql .= '       on mstfacilityitem.mst_dealer_id = mstdealer.id  ';
        $sql .= '     inner join mst_departments as mstdepartment  ';
        $sql .= '       on trnsticker.mst_department_id = mstdepartment.id  ';
        $sql .= '     inner join mst_facilities as mstfacility  ';
        $sql .= '       on mstdepartment.mst_facility_id = mstfacility.id  ';
        $sql .= '     left join mst_shelf_names as mstshelfname  ';
        $sql .= '       on mstfixedcount.mst_shelf_name_id = mstshelfname.id  ';
        $sql .= '     left join mst_facilities as owner  ';
        $sql .= '       on mstfacilityitem.mst_owner_id = owner.id  ';
        $sql .= '     left join mst_facilities as medicalmstfacility  ';
        $sql .= '       on mstfacility.id = medicalmstfacility.id  ';
        $sql .= '       and medicalmstfacility.facility_type = ' . Configure::read('FacilityType.hospital');
        $sql .= '     left join trn_stocks as trnstock  ';
        $sql .= '       on trnsticker.mst_item_unit_id = trnstock.mst_item_unit_id  ';
        $sql .= '       and trnsticker.mst_department_id = trnstock.mst_department_id  ';
        $sql .= '     left join mst_medical_codes as medicalmstfacilityitem  ';
        $sql .= '       on mstfacilityitem.id = medicalmstfacilityitem.mst_facility_item_id  ';
        $sql .= '       and medicalmstfacility.id = medicalmstfacilityitem.mst_facility_id  ';
        $sql .= '       and medicalmstfacilityitem.is_deleted = false  ';
        $sql .= '     left join trn_receivings as trnreceivings  ';
        $sql .= '       on trnsticker.trn_receiving_id = trnreceivings.id  ';
        $sql .= '     inner join mst_user_belongings as mstuserbelonging ';
        $sql .= '       on mstuserbelonging.mst_facility_id = mstfacility.id';
        $sql .= '       and mstuserbelonging.mst_user_id = ' .$user;
        $sql .= '       and mstuserbelonging.is_deleted = false ';
        
        return $sql;
  }

    /**
     * 検索用のWhere句を取得
     */
    private function __getSearchSqlWhere( $search_conditions , $selected_facility ) {

        // 条件文
        $_where = "1 = 1 ";
        
        // ロットに半角空文字が入っているものは無条件で非表示
        $_where .= " and TrnSticker.lot_no <> ' ' ";
        
        // 商品ID
        if( isset($search_conditions['MstFacilityItem']['internal_code']) &&  $search_conditions['MstFacilityItem']['internal_code'] != '') {
            $_where .= "and MstFacilityItem.internal_code = '" . pg_escape_string($search_conditions['MstFacilityItem']['internal_code'])."'";
        }
        
        // 製品番号
        if( isset($search_conditions['MstFacilityItem']['item_code']) &&  $search_conditions['MstFacilityItem']['item_code'] != '') {
            $_where .= "and MstFacilityItem.item_code like '" . pg_escape_string($search_conditions['MstFacilityItem']['item_code']) . "%'";
        }
        
        // 商品名
        if( isset($search_conditions['MstFacilityItem']['item_name']) &&  $search_conditions['MstFacilityItem']['item_name'] != '') {
            $_where .= "and MstFacilityItem.item_name like '%" . pg_escape_string($search_conditions['MstFacilityItem']['item_name']) . "%'";
        }
        
        // 規格
        if( isset($search_conditions['MstFacilityItem']['standard']) &&  $search_conditions['MstFacilityItem']['standard'] != '') {
            $_where .= "and MstFacilityItem.standard like '%" . pg_escape_string($search_conditions['MstFacilityItem']['standard']) . "%'";
        }
        
        // 販売元
        if( isset($search_conditions['MstDealer']['dealer_name']) &&  $search_conditions['MstDealer']['dealer_name'] != '') {
            $_where .= "and MstDealer.dealer_name like '%" . pg_escape_string($search_conditions['MstDealer']['dealer_name']) . "%'";
        }
        
        // シール番号
        if( isset($search_conditions['TrnSticker']['facility_sticker_no']) &&  $search_conditions['TrnSticker']['facility_sticker_no'] != '') {
            $_where .= "and ( TrnSticker.facility_sticker_no ilike '%" . pg_escape_string($search_conditions['TrnSticker']['facility_sticker_no']) . "%'";
            $_where .= " or TrnSticker.hospital_sticker_no ilike '%" . pg_escape_string($search_conditions['TrnSticker']['facility_sticker_no']) . "%' ) ";
        }
        
        // ロット番号
        if( isset($search_conditions['TrnSticker']['lot_no']) &&  $search_conditions['TrnSticker']['lot_no'] != '') {
            $_where .= "and TrnSticker.lot_no like '%" . pg_escape_string($search_conditions['TrnSticker']['lot_no']) . "%' ";
        }
        
        // 有効なもののみ表示
        if( isset($search_conditions['TrnSticker']['is_active']) &&  $search_conditions['TrnSticker']['is_active'] == 1) {
            $_where .= " and TrnSticker.is_deleted = false ";
            $_where .= " and TrnSticker.quantity > 0 ";
        }
        
        // 施設
        if( isset($search_conditions['Sticker']['facilityCode']) &&  $search_conditions['Sticker']['facilityCode'] != '') {
            $_where .= " and MstFacility.facility_code = '" . $search_conditions['Sticker']['facilityCode'] . "'";
        }
        
        // 部署
        if( isset($search_conditions['Sticker']['departmentCode']) &&  $search_conditions['Sticker']['departmentCode'] != '') {
            $_where .= " and MstDepartment.department_code = '" . $search_conditions['Sticker']['departmentCode'] . "'";
        }
        
        
        // 管理区分での検索
        if( isset($search_conditions['trade_type']) &&  $search_conditions['trade_type'] != '') {
            $_where .= " and MstFixedCount.trade_type= '" . $search_conditions['trade_type'] . "'";
        }
        
        // 施主
        if( isset($search_conditions['owner']) &&  $search_conditions['owner'] != '') {
            $_where .= " and Owner.facility_code = '" . $search_conditions['owner'] . "'";
        }
        
        // シール区分
        if( isset($search_conditions['TrnSticker']['trade_type']) &&  $search_conditions['TrnSticker']['trade_type'] != '') {
            $_where .= " and TrnSticker.trade_type = " . $search_conditions['TrnSticker']['trade_type'];
        }
        
        // マスタID
        if( isset($search_conditions['MstFacilityItem']['master_id']) &&  $search_conditions['MstFacilityItem']['master_id'] != '') {
            $_where .= " and MstFacilityItem.master_id like '%" . $search_conditions['MstFacilityItem']['master_id'] . "%'";
        }
        
        // 予備キー1
        if( isset($search_conditions['MstFacilityItem']['spare_key1']) &&  $search_conditions['MstFacilityItem']['spare_key1'] != '') {
            $_where .= "and MstFacilityItem.spare_key1 like '%" . pg_escape_string($search_conditions['MstFacilityItem']['spare_key1'])."%'";
        }
        
        // 予備キー2
        if( isset($search_conditions['MstFacilityItem']['spare_key2']) &&  $search_conditions['MstFacilityItem']['spare_key2'] != '') {
            $_where .= "and MstFacilityItem.spare_key2 like '%" . pg_escape_string($search_conditions['MstFacilityItem']['spare_key2'])."%'";
        }
        
        // 予備キー3
        if( isset($search_conditions['MstFacilityItem']['spare_key3']) &&  $search_conditions['MstFacilityItem']['spare_key3'] != '') {
            $_where .= "and MstFacilityItem.spare_key3 like '%" . pg_escape_string($search_conditions['MstFacilityItem']['spare_key3'])."%'";
        }
        
        // フリーキーワード
        if( isset($search_conditions['Free']['text']) &&  $search_conditions['Free']['text'] != '') {
            $_where .= " and ";
            $_where .= "coalesce(MstFacilityItem.internal_code,'')||";
            $_where .= "coalesce(MstFacilityItem.item_code, '')||";
            $_where .= "coalesce(MstFacilityItem.item_name, '')||";
            $_where .= "coalesce(MstFacilityItem.standard, '')||";
            $_where .= "coalesce(MstFacilityItem.spare_key1, '')||";
            $_where .= "coalesce(MstFacilityItem.spare_key2, '')||";
            $_where .= "coalesce(MstFacilityItem.spare_key3, '')||";
            $_where .= "coalesce(MstDealer.dealer_name, '')||";
            $_where .= "coalesce(TrnSticker.facility_sticker_no, '')||";
            $_where .= "coalesce(TrnSticker.hospital_sticker_no, '')||";
            $_where .= "coalesce(TrnSticker.lot_no, '')";
            $_where .= " ilike '%" . $search_conditions['Free']['text'] . "%'";
        }
        
        if(($selected_facility != '') and ($selected_facility != NULL)){
            $_where .= " and MstItemsFacility.id = " . $selected_facility;
        }
        
        $_where = " where " . $_where . " ";
        
        return $_where;
    }
    
}
?>