<?php
class TrnStorage extends AppModel {
    var $name = 'TrnStorage';
    var $belongsTo = array(
        'TrnStorageHeader',
        'MstItemUnit' => array(
            'className' => 'MstItemUnit',
            'conditions' => null,
            'foreignKey' => 'mst_item_unit_id',
            'dependent' => false
            ),
        'MstUser' => array(
            'className' => 'MstUser',
            'conditions' => null,
            'foreignKey' => 'modifier',
            'dependent' => false
            ),
        'TrnSticker' => array(
            'className' => 'TrnSticker',
            'conditions' => null,
            'foreignKey' => 'trn_sticker_id',
            'dependent' => false
            ),
        'MstClass' => array(
            'className' => 'MstClass',
            'conditions' => null,
            'foreignKey' => 'work_type',
            'dependent' => false
            ),
        'MstDepartment' => array(
            'className' => 'MstDepartment',
            'conditions' => null,
            'foreignKey' => 'mst_department_id',
            'dependent' => false
            ),
        );
    var $validate = array(
        'storage_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'storage_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_department_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'trn_sticker_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'trn_receivings_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        );
}
?>