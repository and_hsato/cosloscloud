<?php
class MstInsuranceClaim extends AppModel {
    var $name = 'MstInsuranceClaim';
    var $primaryKey = 'insurance_claim_code';
    
    var $hasOne = array(
        'MstItem' => array(
            'className' => 'MstItem',
            'conditions' => '',
            'foreignKey' => 'insurance_claim_code',
            'dependent' => false
            ),
        );
    
    var $validate = array(
        'insurance_claim_code' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'insurance_claim_name_s' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        );
}
?>