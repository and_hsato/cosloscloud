<?php
class TrnInventoryHeader extends AppModel {
    var $name = 'TrnInventoryHeader';
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'inventory_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'difference_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>