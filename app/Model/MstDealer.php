<?php
class MstDealer extends AppModel {
    var $name = 'MstDealer';
    
    var $hasOne = array(
        );
    
    var $validate = array(
        'dealer_code' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '販売元コードを入力してください',
                ),
            ),
        'start_date' => array(
            array(
                'rule' => array('notempty'),
                'last' => true,
                'message' => '有効期限（開始日）を入力してください'
                ),
            array(
                'rule' => array('date','ymd'),
                'last' => true,
                'message' => '有効期限（開始日）は日付形式（YYYY/MM/DD）で入力してください'
                ),
            ),
        
        'dealer_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '販売元名を入力してください',
                ),
            ),
        );
}
?>