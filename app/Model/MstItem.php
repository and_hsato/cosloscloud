<?php
/**
 * 採用品マスタ
 */
class MstItem extends AppModel {
    
    /**
     * 
     * @var string $name
     */
    var $name = 'MstItem';
    
    /**
     * 
     * @var aray $hasOne
     */
    var $hasOne = array(
        'MstItemUnit' => array(
            'className' => 'MstItemUnit',
            'conditions' => '',
            'foreignKey' => 'mst_item_id',
            'dependent' => false
            ),
        );
    
    var $belongsTo = array(
        'MstDealer' => array(
            'className' => 'MstDealer',
            'conditions' => array('MstDealer.is_deleted' => false),
            'foreignKey' => 'mst_dealer_id',
            'dependent' => true
            ),
        );
    
    var $validate = array(
        'item_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'mst_unit_name_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>