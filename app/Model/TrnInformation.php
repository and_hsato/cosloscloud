<?php
class TrnInformation extends AppModel {
    var $name = 'TrnInformation';


    /**
     *
     * @var string $belongsTo
     */
    var $belongsTo   = array(
        'MstFacility'  => array(
            'className'  => 'MstFacility',
            'foreignKey' => 'mst_facility_id')
        );
    
    var $displayField = 'title';
    var $validate = array(
        'start_date' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '公開開始日を入力してください',
                ),
            ),
        'title' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'タイトルを入力してください',
                ),
            ),
        'message' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '内容を入力してください',
                ),
            ),
        );
}
?>