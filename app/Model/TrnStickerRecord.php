<?php
class TrnStickerRecord extends AppModel {
    
    var $name = 'TrnStickerRecord';
    
    var $validate = array(
        'move_seq' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'record_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'record_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'trn_sticker_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        );
}
?>