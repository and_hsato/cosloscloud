<?php
class TrnCloseHeader extends AppModel {
  
    /**
     * 
     * @var string $name
     */
    var $name = 'TrnCloseHeader';
    
    /**
     *
     * @var array $belongTo
     */
    var $belongsTo = array(
        'MstFacility' => array(
            'className'  => 'MstFacility',
            'conditions' => '',
            'foreignKey' => 'mst_facility_id',
            'dependent' => false
            )
        );
    
    /**
     *
     * @var array $hasMeny
     */
    var $hasMany = array('TrnCloseRecord');
    
    /**
     * 
     * @var array $validate
     */
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_facility_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
    
}
?>