<?php
class MstUnitName extends AppModel {
  
  /**
   * 包装単位名
   * class name.
   * @var string $name
   */
    var $name = 'MstUnitName';
    
    /**
     * 
     * @var array $hasOne
     */
    var $hasOne = array(
        'MstItemUnit' => array(
            'className' => 'MstItemUnit',
            'conditions' => '',
            'foreignKey' => 'mst_unit_name_id',
            'dependent' => false
            ),
        );
    
    var $validate = array(
        'internal_code' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'unit_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Your custom message here',
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>