<?php
class MstClassSeparation extends AppModel {
    var $name = 'MstClassSeparation';
    var $primaryKey = 'class_separation_code';
    var $displayField = 'class_separation_name';
    var $validate = array(
        'class_separation_code' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        );
}
?>