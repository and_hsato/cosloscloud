<?php
class MstEdition extends AppModel {
    
    /**
     *
     * @var string $name
     */
    var $name = 'MstEdition';
    
    var $validate = array(
            'name' => array(
                    'notempty' => array(
                            'rule' => array('notempty'),
                            'message' => 'エディション名を入力してください',
                    )
            ),
    );
}
?>