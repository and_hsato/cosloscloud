<?php
class TrnConsume extends AppModel {
    
    /**
     * 
     * @var string $name
     */
    var $name = 'TrnConsume';
    
    /**
     * 
     * @var array $hasOne
     */
    var $hasOne = array(
        "TrnConsumeHeader" => array(
            'className'  => 'TrnConsumeHeader',
            'conditions' => '',
            'foreignKey' => 'id',
            'dependent'    => true
            ),
        "TrnStock" => array(
            'className'  => 'TrnStock',
            'conditions' => '',
            'foreignKey' => 'mst_item_unit_id',
            'dependent'    => true
            ),  
        "MstFixedCount" => array(
            'className'  => 'MstFixedCount',
            'conditions' => '',
            'foreignKey' => 'mst_item_unit_id',
            'dependent'    => true
            )
        );
    
    /**
     * 
     * @var array $belongsTo
     */
    var $belongsTo = array(
        'TrnSticker' => array(
            'className'  => 'TrnSticker',
            'conditions' => array('TrnSticker.is_deleted'=>false),
            'foreignKey' => 'trn_sticker_id',
            'dependent'    => true
            ),
        'MstDepartment' => array(
            'className'  => 'MstDepartment',
            'conditions' => array('MstDepartment.is_deleted'=>false),
            'foreignKey' => 'mst_department_id',
            'dependent'    => true
            ),
        'MstItemUnit' => array(
            'className'  => 'MstItemUnit',
            'conditions' => array('MstItemUnit.is_deleted'=>false),
            'foreignKey' => 'mst_item_unit_id',
            'dependent'    => true
            ),
        'TrnSalesRequestHeader' => array(
            'className'  => 'TrnSalesRequestHeader',
            'conditions' => array('TrnSalesRequestHeader.is_deleted'=>false),
            'foreignKey' => 'trn_sales_request_header_id',
            'dependent'    => false
            ),
        );
    
    /**
     * 
     * @var array $validte
     */
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'work_seq' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'use_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_department_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'stocking_close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'sales_close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'facility_close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_retroactable' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
    
}
?>