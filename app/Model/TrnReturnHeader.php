<?php
class TrnReturnHeader extends AppModel {
    var $name = 'TrnReturnHeader';
    var $belongsTo = array(
        'MstDepartmentFrom' => array(
            'className' => 'MstDepartment'
            ,'foreignKey' => 'department_id_from'
            ,'conditions' => array('MstDepartmentFrom.is_deleted' =>false))
        ,'MstDepartmentTo' => array(
            'className' => 'MstDepartment'
            ,'foreignKey' => 'department_id_to'
            ,'conditions' => array('MstDepartmentTo.is_deleted' =>false))
        ,'MstUser' => array(
            'className' => 'MstUser'
            ,'foreignKey' => 'creater'
            ,'conditions' => array('MstUser.is_deleted' =>false))
        );
    
    
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'detail_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>