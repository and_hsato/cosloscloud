<?php
class TrnShippingHeader extends AppModel {
  
    var $name = 'TrnShippingHeader';
    
    /**
     *
     * @var string $belongsTo
     */
    var $belongsTo   = array(
        'MstUser'  => array(
            'className'  => 'MstUser',
            'foreignKey' => 'modifier')
        );
    
    var $hasMany = array(
        'TrnShipping' => array(
            'className' => 'TrnShipping'
            )
        );
    
    var $validate = array(
        'wrok_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'shipping_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'shipping_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'detail_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>