<?php
class TrnOrder extends AppModel {
    var $name = 'TrnOrder';
    var $belongsTo = array(
        'MstItemUnit' => array(
            'className' => 'MstItemUnit',
            'conditions' => null,
            'foreignKey' => 'mst_item_unit_id',
            'dependent' => false
            ),
        'MstClass' => array(
            'className' => 'MstClass',
            'conditions' => null,
            'foreignKey' => 'work_type',
            'dependent' => false
            ),
        );
    
    
    var $validate = array(
        'work_seq' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'order_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'order_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'before_quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Your custom message here',
                ),
            ),
        'quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => '●',
                ),
            ),
        'remain_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>