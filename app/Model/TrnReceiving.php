<?php
class TrnReceiving extends AppModel {
  
    var $name = 'TrnReceiving';
    var $hasOne = array('TrnClaim');
    var $belongsTo = array(
        'TrnOrder' => array(
            'className' => 'TrnOrder',
            'foreignKey' => 'trn_order_id',
            'dependent' => false
            ),
        'MstItemUnit' => array(
            'className' => 'MstItemUnit',
            'conditions' => null,
            'foreignKey' => 'mst_item_unit_id',
            'dependent' => false
            ),
        'MstClass' => array(
            'className' => 'MstClass',
            'conditions' => null,
            'foreignKey' => 'work_type',
            'dependent' => false
            ),
        'MstDepartment' => array(
            'className' => 'MstDepartment',
            'conditions' => null,
            'foreignKey' => 'department_id_from',
            'dependent' => false,
            ),
        'MstUser' => array(
            'className' => 'MstUser',
            'conditions' => null,
            'foreignKey' => 'modifier',
            'dependent' => false,
            ),
        );
    
    var $validate = array(
        'receiving_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'receiving_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'stocking_close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'sales_close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'facility_close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        );
}
?>