<?php
class TrnReceivingHeader extends AppModel {
  
    var $name = 'TrnReceivingHeader';
    var $belongsTo = array(
        'MstDepartment' => array(
            'className' => 'MstDepartment',
            'conditions' => null,
            'foreignKey' => 'department_id_from',
            'dependent' => false
            ),
        'MstUser' => array(
            'className' => 'MstUser',
            'conditions' => null,
            'foreignKey' => 'creater',
            'dependent' => false
            ),
        );
    
    var $hasMany = array(
        'TrnReceiving' => array(
            'className' => 'TrnReceiving',
            'dependent' => false
            ),
        );
    
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'receiving_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'receiving_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'detail_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        );
}
?>