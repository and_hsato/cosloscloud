<?php
class MstRoleType extends AppModel {
    
    /**
     *
     * @var string $name
     */
    var $name = 'MstRoleType';
    
    var $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'タイプ名を入力してください',
                )
            )
    );
}
?>