<?php
class MstDepartment extends AppModel {
    
    /**
     *
     * @var string $name
     */
    var $name = 'MstDepartment';
    
    /**
     *
   * @var string $belongsTo
     */
    
    /**
     *
     * @var array $validate
     */
    var $validate = array(
        'facility_id' => array(
            'notempty'  => array(
                'rule'    => array('notempty'),
                'message'   => '施設を選択してください',
                ),
            ),
        'department_code' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '部署コードを入力してください',
                ),
            ),
        'department_name' => array(
            'notempty'      => array(
                'rule'        => array('notempty'),
                'message'       => '部署名を入力して下さい',
                ),
            ),
        'department_type' => array(
            'notempty'      => array(
                'rule'        => array('notempty'),
                'message'       => '部署区分を選択してください',
                ),
            ),
        'priority'   => array(
            'notempty' => array(
                'rule'   => array('notempty'),
                'message'  => '引当優先度を入力してください',
                ),
            'numeric' => array(
                'rule'   => array('custom' , '/^[0-9]+$/i'),
                'message'  => '引当優先度は数値で入力してください',
                ),
            )
        );
    
}
?>