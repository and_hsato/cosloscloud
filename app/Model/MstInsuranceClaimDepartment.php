<?php
class MstInsuranceClaimDepartment extends AppModel {
    var $name = 'MstInsuranceClaimDepartment';
    var $primaryKey = 'insurance_claim_department_code';
    var $validate = array(
        'insurance_claim_department_code' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        );
}
?>