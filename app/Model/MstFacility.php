<?php
class MstFacility extends AppModel {
    
    /**
     * 
     * @var string $name
     */
    var $name = 'MstFacility';
    
    /**
     *
     * @var array $hasMany
     */
    var $hasMany = array(
        'MstFacilityRelation' => array(
            'className'     => 'MstFacilityRelation',
            'foreignKey'    => 'mst_facility_id',
            'conditions'    => null,
            'order'    => 'MstFacilityRelation.id DESC',
            'limit'        => null,
            'dependent'=> true
            )
        );
    
    /**
     * 
     * @var array $validate
     */
    var $validate = array(
        'facility_code' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '施設コードを入力してください',
                ),
            ),
        'facility_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '施設名を入力してください',
                ),
            ),
        'days_before_expiration' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '期限切れ警告日数を入力してください',
                ),
            'numeric' => array(
                'rule' => array('custom', '/^[0-9]+$/i'),
                'message' => '期限切れ警告日数は数値で入力してください<br />',
                ),
            ),
        'facilities_type' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '種別を選択してください',
                ),
            ),
        'round' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '丸め区分を選択してください',
                ),
            ),
        'gross' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'まとめ区分を選択してください',
                ),
            ),
        );
}
?>