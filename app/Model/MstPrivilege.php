<?php
class MstPrivilege extends AppModel {
    var $name = 'MstPrivilege';
    var $validate = array(
        'mst_role_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'view_no' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'viewable' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        'decidable' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>