<?php
class MstTransactionConfig extends AppModel {
    var $name = 'MstTransactionConfig';
    
    /**
     *
     * @var array $belongsTo
     */
    
    var $validate = array(
        'start_date' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '適用開始日を入力してください',
                ),
            ),
        'partner_facility_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '仕入先を入力してください',
                ),
            ),
        'mst_item_unit_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '包装単位を入力してください',
                ),
            ),
        'transaction_price' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '取引単価を入力してください',
                ),
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => '取引単価は数値で入力してください',
                ),
            ),
        'mst_facility_relation_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '施設との関係設定がありません',
                ),
            ),
        );
}
?>