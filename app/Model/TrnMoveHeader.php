<?php
class TrnMoveHeader extends AppModel {
    
    /**
     *
     * @var string $name
     */
    var $name = 'TrnMoveHeader';
    
    /**
     *
     * @var array $hasMany
     */
    var $hasMany = array('TrnReceiving'=>
                         array(
                             'foreignKey' => 'trn_move_header_id',
                             ),
                         'TrnShipping' => array(
                             'foreignKey' => 'trn_move_header_id',
                             )
                         );
    
    /**
     *
     * @var array $validate
     */
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'work_date' => array(
            'date' => array(
                'rule' => array('date'),
                ),
            ),
        'move_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'detail_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>