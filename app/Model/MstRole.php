<?php
class MstRole extends AppModel {
    
    /**
     *
     * @var string $name
     */
    var $name = 'MstRole';
    
    var $belongsTo = array(
            'MstRoleType' => array(
                    'className' => 'MstRoleType',
                    'foreignKey' => 'mst_role_type_id'
            ),
            'MstEdition' => array(
                    'className' => 'MstEdition',
                    'foreignKey' => 'mst_edition_id'
            ),
    );
    
    var $validate = array(
        'role_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'ロール名を入力してください',
                )
            )
    );
}
?>