<?php
class MstUserBelonging extends AppModel {
    /**
     * 
     * @var string $name
     */
    var $name = 'MstUserBelonging';
    /**
     * 
     * @var array $belongsTo
     */
    var $belongsTo = array(
        'MstUser' => array(
            'className' => 'MstUser',
            'foreignKey' => 'mst_user_id',
            'fields' => array('id'),
            'conditions' => array('MstUser.is_deleted'=>false)
            ),
        'MstFacility' => array(
            'className' => 'MstFacility',
            'foreignKey' => 'mst_facility_id',
            'fields' => array('MstFacility.id','facility_type','facility_name'),
            ),    
        );
    
    /**
     * 
     * @var array $hasOne
     */
    var $hasOne = array();
    
    /**
     * 
     * @var array $validate
     */
    var $validate = array(
        'mst_user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_facility_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>