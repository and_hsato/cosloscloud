<?php
class TrnInventory extends AppModel {
    var $name = 'TrnInventory';
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'trn_sticker_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'real_quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'allowEmpty' => true
                ),
            ),        
        'is_exists' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        'trn_inventory_header_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_department_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_facility_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>