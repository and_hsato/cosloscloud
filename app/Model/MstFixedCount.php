<?php
class MstFixedCount extends AppModel {
  var $name = 'MstFixedCount';

  var $belongsTo = array(
          'MstFacilityItem' => array(
                          'className' => 'MstFacilityItem',
                          'conditions' => array('MstFacilityItem.is_deleted' => false),
                          'foreignKey' => 'mst_facility_item_id',
                          'dependent' => false
          ) ,
          'MstFacility' => array(
                          'className' => 'MstFacility',
                          'conditions' => array('MstFacility.is_deleted' => false),
                          'foreignKey' => 'mst_facilities_id',
                          'dependent' => false
          ) ,
          'MstDepartment' => array(
                          'className' => 'MstDepartment',
                          'conditions' => array('MstDepartment.is_deleted' => false),
                          'foreignKey' => 'mst_department_id',
                          'dependent' => false
          ) ,
          'MstShelfName' => array(
                          'className' => 'MstShelfName',
                          'conditions' => array('MstShelfName.is_deleted' => false),
                          'foreignKey' => 'mst_shelf_name_id',
                          'dependent' => false
          ) ,
          'MstItemUnit' => array(
                          'className' => 'MstItemUnit',
                          'conditions' => array('MstItemUnit.is_deleted' => false),
                          'foreignKey' => 'mst_item_unit_id',
                          'dependent' => false
          ) ,
          'TrnStock' => array(
                          'className' => 'TrnStock',
                          'conditions' => null,
                          'foreignKey' => 'trn_stock_id',
                          'dependent' => false
          ),
  );

  var $validate = array(
          'trn_stock_id' => array(
                          'numeric' => array(
                                          'rule' => array('numeric'),
                          ),
          ),
          'order_point' => array(
                          'numeric' => array(
                                          'rule' => array('numeric'),
                          ),
          ),
          'fixed_count' => array(
                          'numeric' => array(
                                          'rule' => array('numeric'),
                          ),
          ),
          'holiday_fixed_count' => array(
                          'numeric' => array(
                                          'rule' => array('numeric'),
                          ),
          ),
          'spare_fixed_count' => array(
                          'numeric' => array(
                                          'rule' => array('numeric'),
                          ),
          ),
          'trade_type' => array(
                          'numeric' => array(
                                          'rule' => array('numeric'),
                          ),
          ),
          'is_deleted' => array(
                          'boolean' => array(
                                          'rule' => array('boolean'),
                          ),
          ),
          'start_date' => array(
                          'date' => array(
                                          'rule' => array('date'),
                          ),
          ),
  );
}
?>