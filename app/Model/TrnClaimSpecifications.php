<?php
class TrnClaimSpecifications extends AppModel {

    /**
     *
     * @var string $name
     */
    var $name = 'TrnClaimSpecifications';

    /**
     *
     * @var array $validate
     */
    var $validate = array(
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'stocking_close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'sales_close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'facility_close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>
