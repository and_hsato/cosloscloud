<?php
/**
 * シール発行履歴ヘッダー
 * @version 1.0.0
 * @since 2010/08/30
 */
class TrnStickerIssueHeader extends AppModel {
  /**
   * Class name.
   * @var string $name
   */
  var $name = 'TrnStickerIssueHeader';

  /**
   *
   * @var array $hasOne
   */
  var $hasOne = array();
  /**
   *
   * @var array $belongsTo
   */
  var $belongsTo = array('MstItemUnit');

}
?>
