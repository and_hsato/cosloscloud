<?php
class TrnOrderHeader extends AppModel {
    var $name = 'TrnOrderHeader';
    
    var $hasMany = array('TrnOrder');
    
    var $belongsTo = array(
        'MstDepartment' => array(
            'className' => 'MstDepartment',
            'conditions' => null,
            'foreignKey' => 'department_id_to',
            'dependent' => false
            ),
        'MstUser' => array(
            'className' => 'MstUser',
            'conditions' => null,
            'foreignKey' => 'creater',
            'dependent' => false
            ),
        );
    
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'order_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'order_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'detail_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>