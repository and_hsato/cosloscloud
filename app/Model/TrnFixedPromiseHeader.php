<?php
class TrnFixedPromiseHeader extends AppModel {
    var $name = 'TrnFixedPromiseHeader';
    var $belongsTo = array(
        'MstDepartment' => array(
            'className' => 'MstDepartment',
            'conditions' => null,
            'foreignKey' => 'department_id_from',
            'dependent' => false
            ),
        );
    
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'fixed_promise_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'detail_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>