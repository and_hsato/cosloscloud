<?php

class Vesta extends AppModel {
    var $name     = 'Vesta';
    var $useTable = false;    // 専用のテーブルは使用しない
    var $split    = true;     // 分割有無

    /**
     * 業者の場合、分割テーブルを参照すると不都合が起きるので事前準備
     */
    public function setSplit ($user_type) {
        if ($user_type == Configure::read('UserType.edi')) {
            // 業者の場合は、分割テーブルを無視する
            $this->split = false;
        }
    }

    /**
     * query オーバーライド
     * このモデルを通す場合は自動で分割有無を見る
     */
    public function query ($sql, $cache=true) {
        return parent::query($sql , $cache, $this->split);
    }

    /**
     * 件数カウント
     * @param  $from  取得したいSQLのFROM句
     * @param  $where 取得したいSQLのWHERE句
     * @return 件数
     */
    public function getRecordCount ($from, $option=array()) {
        $select = 'count(*) AS count';

        // ソート、件数指定は不要
        $option['order'] = null;
        $option['limit'] = null;

        $sql = $this->createSelectSQL($select, $from, $option);

        // GROUP BY がある場合、集計方法を一段増やす
        if (!empty($option['group'])) {
            $from = '( ' . $sql . ' ) AS GROUPINGS ';
            $sql  = $this->createSelectSQL($select, $from);
        }

        $ret = $this->query($sql);

        return $ret[0][0]['count'];
    }

    /**
     * 取得件数付きSQL実行
     * @param  $select 取得したいSQLのSELECT句
     * @param  $from   取得したいSQLのFROM句
     * @param  $option 取得したいSQLのWHERE、GROUP、ORDER、LIMITなど
     * @param  $count  取得件数有無
     * @return 取得結果
     */
    public function customQuery ($select, $from, $option=array(), $count=false) {
        if (empty($select) || empty($from)) {
            return false;
        }

        $ret = array();

        // 件数取得
        if ($count) {
            $ret['count'] = $this->getRecordCount($from, $option);
        }

        // SQL生成＆実行
        $sql = $this->createSelectSQL($select, $from, $option);
        $ret['query'] = $this->query($sql);

        return $ret;
    }

    /**
     * 検索SQL文生成
     * @param  $select 取得したいSQLのSELECT句
     * @param  $from   取得したいSQLのFROM句
     * @param  $option 取得したいSQLのWHERE、GROUP、ORDER、LIMITなど
     * @return 組み立てたSQL文
     */
    private function createSelectSQL ($select, $from, $option=array()) {
        $sql = ' SELECT ' . $select . ' FROM ' . $from;

        if (isset($option['where']) && !empty($option['where'])) {
            $sql .= ' WHERE ' . $option['where'];
        }
        if (isset($option['group']) && !empty($option['group'])) {
            $sql .= ' GROUP BY ' . $option['group'];
        }
        if (isset($option['having']) && !empty($option['having'])) {
            $sql .= ' HAVING ' . $option['having'];
        }
        if (isset($option['order']) && !empty($option['order'])) {
            $sql .= ' ORDER BY ' . $option['order'];
        }
        if (isset($option['limit']) && !empty($option['limit'])) {
            $sql .= ' LIMIT ' . $option['limit'];
        }
        if (isset($option['lock']) && $option['lock'] == true) {
            $sql .= ' FOR UPDATE';
        }

        return $sql;
    }

    /**
     * 拡張 save メソッド
     * @param $tableName テーブル名
     * @param $tableData データ (Model => Column => Data)
     * @param $validate  モデルのバリデーションを利用するか モデル定義は基本利用しないので初期値：false
     * @return 登録出来たID
     * ※分割テーブルがある場合、親のsaveメソッドでSELECT count(*) が入り、エラーになる為
     * ※chkSplitSaveを使ってください。
     */
    public function enhanceSave ($tableName, $tableData, $validate=false) {
        if(is_null($tableName) || is_null($tableData)){
            return false;
        }
        if(!isset($tableData[$tableName])){
            $tableData[$tableName] = $tableData;
        }

        // 利用するモデルを読み込む
        App::uses($tableName, 'Model');

        ${$tableName} = new $tableName;
        ${$tableName}->create();
        $ret = ${$tableName}->save($tableData, $validate);
        if(!$ret){
            return false;
        }

        return ${$tableName}->getLastInsertID();
    }

    /**
     * テーブル更新メソッド（分割有無をチェックします）
     * @param $tableName 更新したいテーブル名
     * @param $tableData 更新したいデータ (Column => Data)
     * @param $condition 更新するキー
     * @return 処理した行数
     */
    public function chkSplitSave ($tableName, $tableData, $condition=null) {
        if (is_null($tableName) || is_null($tableData) || is_null($condition)) {
            return false;
        }

        // 利用するモデルを読み込む
        App::uses($tableName, 'Model');

        ${$tableName} = new $tableName;

        $sql  = ' UPDATE ';
        $sql .= ${$tableName}->useTable;
        $sql .= ' SET ';

        $update_line = array();
        $column_list = ${$tableName}->getColumnTypes();
        foreach ($tableData as $key => $value) {
            if(isset($column_list[$key])){
                switch ($column_list[$key]) {
                    case 'integer':
                        $update_line[] = $key . " = cast(nullif('" . $value . "','') AS integer)";
                        break;
                    case 'float':
                        $update_line[] = $key . " = cast(nullif('" . $value . "','') AS numeric)";
                        break;
                    case 'date':
                        $update_line[] = $key . " = cast(nullif('".$value."','') AS date)";
                        break;
                    case 'datetime':
                        $update_line[] = $key . " = cast(nullif('".$value."','') AS timestamp)";
                        break;
                    case 'boolean':
                        // 入力値整形
                        switch ((string)$value) {
                            case 'true':
                            case 'false':
                                break;
                            default:
                                $value = ($value)? 'true' : 'false';
                                break;
                        }
                        $update_line[] = $key . " = " . $value;
                        break;
                    default:
                        $update_line[] = $key . " = '" . $value . "'";
                        break;
                }
            }
        }
        $sql .= join(", ", $update_line);

        if ($condition) {
            $where = array();
            foreach ($condition as $key => $value) {
                $where[] = $key . " = '" . $value . "'";
            }
            $sql .= " WHERE " . join(' AND ', $where);
        }

        ${$tableName}->query($sql, false, $this->split);

        return ${$tableName}->getAffectedRows();
    }

    /**
     * 更新をかけるキーを指定して、DB保存メソッドを切り分ける
     * @param  $tableName テーブル名
     * @param  $tableData データ (Model => Column => Data)
     * @param  $validate  モデルのバリデーションを利用するか モデル定義は基本利用しないので初期値：false
     * @return 処理結果
     */
    public function dispatchSave ($tableName, $tableData, $update_keys, $validate=false) {
        if (is_null($tableName) || is_null($tableData)) {
            return false;
        }

        $ret       = false;
        $condition = array();

        // 更新有無をチェック
        foreach ($update_keys as $key) {
            if (isset($tableData[$tableName][$key]) && !empty($tableData[$tableName][$key])) {
                // 更新キー発見
                $condition[$key] = $tableData[$tableName][$key];
            }
        }

        // 切り分ける
        if (empty($condition)) {
            $ret = $this->enhanceSave($tableName, $tableData);
        } else {
            $ret = $this->chkSplitSave($tableName, $tableData[$tableName], $condition);
        }

        return $ret;
    }

    /**
     * テーブルロック
     */
    public function lockTable ($tableName, $conditions, $return=null) {
        // 利用するモデルを読み込む
        App::uses($tableName, 'Model');
        ${$tableName} = new $tableName;

        // テーブルロック
        $select = ' * ';
        if (!is_null($return) && is_array($return)) {
            $select = implode(',', $return);
        }

        $from  = ${$tableName}->useTable;

        $where = array();
        foreach ($conditions as $key => $value) {
            $where[] = $key . ' = ' .$value;
        }
        $option = array('where' => implode(' AND ', $where), 'lock' => true);
        $sql    = $this->createSelectSQL($select, $from, $option);
        $res    = $this->query($sql);
        if ($res == false) {
            return false;
        }

        // 戻り値有無
        if (!is_null($return)) {
            return $res;
        } else {
            return true;
        }
    }

    /**
     * テーブルロックしてから更新実行
     * @param  $conditions 更新実行の条件
     * @param  $update     更新する値
     * @param  $return     戻り値が欲しい場合、カラム名を設定
     * @return $returnで設定されたカラム値 : true : false
     */
    public function lockAfterUpdate ($tableName, $conditions, $update, $return=null) {
        if (is_null($tableName) || empty($conditions) || count($conditions) < 1 ) {
            return false;
        }

        // 利用するモデルを読み込む
        App::uses($tableName, 'Model');
        ${$tableName} = new $tableName;

        // テーブルロック
        $res = $this->lockTable($tableName, $conditions, $return);
        if ($res == false) {
            return false;
        }

        // 更新
        if (!$this->chkSplitSave($tableName, $update, $conditions)) {
            return false;
        }

        // 戻り値有無
        if (!is_null($return)) {
            return $res;
        } else {
            return true;
        }
    }

    /**
     * テーブルロックしてからdel実行
     * @param  $conditions del実行の条件
     * @param  $return     戻り値が欲しい場合、カラム名を設定
     * @return $returnで設定されたカラム値 : true : false
     */
    public function lockAfterDel ($tableName, $conditions, $return=null) {
        if (is_null($tableName) || empty($conditions) || count($conditions) < 1 ) {
            return false;
        }

        // 利用するモデルを読み込む
        App::uses($tableName, 'Model');
        ${$tableName} = new $tableName;

        // テーブルロック
        $res = $this->lockTable($tableName, $conditions, $return);
        if ($res == false) {
            return false;
        }

        // 論理削除
        if (!${$tableName}->del($conditions)) {
            return false;
        }

        // 戻り値有無
        if (!is_null($return)) {
            return $res;
        } else {
            return true;
        }
    }

    /**
     * 定数テーブルから全データ一覧、または指定したグループの一覧を取得
     * @param  $group 定数テーブルのグループコード 指定無しの場合は、全データ一覧
     * @return $list  取得したデータの一覧
     */
    public function getConstList ($group=null) {
        $path1     = "{n}.MstConst.const_cd";
        $path2     = "{n}.MstConst.const_nm";
        $groupPath = "{n}.MstConst.const_group_cd";

        $sql  = ' SELECT ';
        $sql .= '   const_group_cd AS "MstConst__const_group_cd" ';
        $sql .= ' , const_cd       AS "MstConst__const_cd" ';
        $sql .= ' , const_nm       AS "MstConst__const_nm" ';
        $sql .= ' FROM mst_consts ';

        if (!is_null($group)) {
            $sql .= " WHERE const_group_cd = '" . $group . "'";
            $groupPath = null;
        }

        $sql .= ' ORDER BY const_group_cd, const_cd ';
        $list = Set::Combine($this->query($sql), $path1, $path2, $groupPath);
        return $list;
    }

    /**
     * 商品カテゴリ情報リスト取得
     * @param  $rank      分類ランク
     * @param  $parent_id 親分類のID
     * @return $list      取得したデータの一覧
     */
    public function getItemCategoryList ($rank=1, $parent_id=null) {
        // 分類ランクが空 または分類ランクが大分類以外で親IDが空の場合は、空配列を返す
        if (empty($rank) || ($rank != 1 && empty($parent_id))) {
            return array();
        }

        // 分類ランクが１の場合は、親がいない
        if (($rank == 1 && !is_null($parent_id))) {
            $parent_id = null;
        }

        $sql  = ' SELECT ';
        $sql .= '   id            AS "MstItemCategory__id" ';
        $sql .= ' , category_name AS "MstItemCategory__category_name" ';
        $sql .= ' FROM mst_item_categories ';
        $sql .= ' WHERE ';
        $sql .= '     category_rank = ' . $rank;
        $sql .= ' AND is_deleted = false ';

        if(!is_null($parent_id)){
            $sql .= ' AND parent_id = ' . $parent_id;
        }

        $sql .= ' ORDER BY category_code ';
        $list = Set::Combine(
            $this->query($sql),
            "{n}.MstItemCategory.id",
            "{n}.MstItemCategory.category_name"
        );
        return $list;
    }

    /**
     * 施設リスト取得
     */
    public function getFacilityList(
        $facility_id = null ,
        $type = array() ,
        $deleted = true ,
        $field = array('facility_code' , 'facility_name') ,
        $order = null
    ){
        list($a , $b) = $field;
        $sql  = '';
        $sql .= 'select ';
        $sql .= "      b.{$a} as \"MstFacility__{$a}\" ";
        $sql .= "    , b.{$b} as \"MstFacility__{$b}\" ";
        $sql .= '  from';
        $sql .= '    mst_facility_relations as a ';
        $sql .= '    left join mst_facilities as b ';
        $sql .= '      on b.id = a.mst_facility_id ';
        $sql .= '      or b.id = a.partner_facility_id ';
        $sql .= '  where';
        $sql .= ' 1 = 1 ';
        if($facility_id){
            $sql .= '    and a.mst_facility_id = '.$facility_id;
        }

        if($deleted){
            $sql .= '    and a.is_deleted = false ';
            $sql .= '    and b.is_deleted = false ';
        }
        if(!empty($type)){
            $sql .= '    and b.facility_type in ('.implode("," , $type).') ';
        }
        $sql .= '  group by';
        $sql .= "    b.{$a}";
        $sql .= "    , b.{$b}";
        $sql .= "    , b.facility_code";
        $sql .= "    , b.facility_type";
        if(is_null($order)){
            $sql .= ' order by b.facility_type desc ,  b.facility_code ';
        }else{
            $sql .= ' order by ' . $order;
        }
        $list = Set::Combine(
            $this->query($sql),
            "{n}.MstFacility.{$a}",
            "{n}.MstFacility.{$b}"
            );
        return $list;
    }

    //編集画面用包装単位プルダウン作成用データ
    public function setItemUnits ($mst_facility_item_id) {
        $sql = ' select ';
        $sql.= '      a.id                   as "MstItemUnit__id"';
        $sql.= '    , (  ';
        $sql.= '      case  ';
        $sql.= '        when a.per_unit = 1  ';
        $sql.= '        then b.unit_name  ';
        $sql.= "        else b.unit_name || '(' || a.per_unit || c.unit_name || ')'  ";
        $sql.= '        end ';
        $sql.= '    )                        as "MstItemUnit__unit_name"  ';
        $sql.= '  from ';
        $sql.= '    mst_item_units as a  ';
        $sql.= '    left join mst_unit_names as b  ';
        $sql.= '      on b.id = a.mst_unit_name_id  ';
        $sql.= '    left join mst_unit_names as c  ';
        $sql.= '      on c.id = a.per_unit_name_id ';
        $sql.= '      where ';
        $sql.= '      a.mst_facility_item_id = ' . $mst_facility_item_id;
        $sql.= ' order by a.per_unit ';

        $list = Set::Combine(
             $this->query($sql),
            "{n}.MstItemUnit.id",
            "{n}.MstItemUnit.unit_name"
            );
        return $list;
    }

    /**
     * 包装単位情報リスト
     * @return $list  取得したデータの一覧
     */
    public function getUnitNameList () {
        $sql  = ' SELECT ';
        $sql .= '   id         AS "MstUnitName__id" ';
        $sql .= ' , unit_name AS "MstUnitName__unit_name" ';
        $sql .= ' FROM mst_unit_names ';
        $sql .= " WHERE is_deleted = false ";
        $sql .= ' ORDER BY order_num ';
        $list = Set::Combine(
            $this->query($sql),
            "{n}.MstUnitName.id",
            "{n}.MstUnitName.unit_name"
            );

        return $list;
    }

    /**
     * 集計科目情報リスト
     * @param  $mst_facility_id  取得する施設ID
     * @param  $field            リストにするカラム
     * @return $list             取得したデータの一覧
     */
    public function getAccountList ($mst_facility_id=null, $field=array('id', 'name')) {
        if (is_null($mst_facility_id) || !is_array($field)) {
            return array();
        }
        list($a, $b) = $field;

        $sql  = ' SELECT ';
        $sql .= "   {$a} AS \"MstAccountlist__{$a}\" ";
        $sql .= " , {$b} AS \"MstAccountlist__{$b}\" ";
        $sql .= ' FROM mst_accountlists ';
        $sql .= " WHERE mst_facility_id = '" . $mst_facility_id . "' ";
        $sql .= ' ORDER BY id ';
        $list = Set::Combine(
            $this->query($sql),
            "{n}.MstAccountlist.{$a}",
            "{n}.MstAccountlist.{$b}"
            );

        return $list;
    }

    /**
     * お気に入り情報リスト
     * @param  $mst_facility_id  取得する施設ID
     * @param  $field            リストにするカラム
     * @return $list             取得したデータの一覧
     */
    public function getFavoriteGroupList ($mst_facility_id=null, $field=array('id', 'group_name')) {
        if (is_null($mst_facility_id) || !is_array($field)) {
            return array();
        }
        list($a, $b) = $field;

        $sql  = ' SELECT ';
        $sql .= "   {$a} AS \"MstFavoriteGroup__{$a}\" ";
        $sql .= " , {$b} AS \"MstFavoriteGroup__{$b}\" ";
        $sql .= ' FROM mst_favorite_groups ';
        $sql .= " WHERE mst_facility_id = '" . $mst_facility_id . "' ";
        $sql .= ' ORDER BY group_code ';
        $list = Set::Combine(
            $this->query($sql),
            "{n}.MstFavoriteGroup.{$a}",
            "{n}.MstFavoriteGroup.{$b}"
            );

        return $list;
    }

    /**
     * グランドマスタ情報取得
     * @param  $id グランドマスタID
     * @return 取得したデータ配列
     */
    public function getGrandMasterData ($master_id = null) {
        if (is_null($master_id)) {
            return array();
        }

        $sql  =' select ';
        $sql .='       a.item_name               as "MstFacilityItem__item_name" ';
        $sql .='     , a.standard                as "MstFacilityItem__standard" ';
        $sql .='     , a.item_code               as "MstFacilityItem__item_code" ';
        $sql .='     , a.jan_code                as "MstFacilityItem__jan_code" ';
        $sql .='     , a.refund_price            as "MstFacilityItem__refund_price" ';
        $sql .='     , a.converted_num           as "MstFacilityItem__refund_converted_num"';
        $sql .='     , a.unit_price              as "MstFacilityItem__unit_price" ';
        $sql .='     , a.biogenous_type          as "MstFacilityItem__biogenous_type" ';
        $sql .='     , a.class_separation        as "MstFacilityItem__class_separation" ';
        $sql .='     , a.tax_type                as "MstFacilityItem__tax_type" ';
        $sql .='     , a.keep_type               as "MstFacilityItem__keep_type"';
        $sql .='     , a.master_id               as "MstFacilityItem__master_id" ';
        $sql .='     , a.unit_name               as "MstFacilityItem__unit_name" ';
        $sql .='     , b.id                      as "MstFacilityItem__mst_unit_name_id" ';
        $sql .='     , c.id                      as "MstDealer__id" ';
        $sql .='     , c.dealer_name             as "MstDealer__dealer_name" ';
        $sql .='     , c.dealer_code             as "MstDealer__dealer_code" ';
        $sql .='     , d.jmdn_code               as "MstFacilityItem__jmdn_code" ';
        $sql .='     , d.jmdn_name               as "MstFacilityItem__jmdn_name" ';
        $sql .='     , a.insurance_claim_type    as "MstFacilityItem__insurance_claim_type" ';
        $sql .='     , f.insurance_claim_code    as "MstFacilityItem__insurance_claim_code" ';
        $sql .='     , f.insurance_claim_name_s  as "MstFacilityItem__insurance_claim_short_name" ';
        $sql .='     , f.insurance_claim_name    as "MstFacilityItem__insurance_claim_name" ';
        $sql .='     , a.unit_name               as "MstGrandmaster__unit_name"';
        $sql .='     , a.unit_name               as "MstItem__unit_name"';
        $sql .='     , a.item_category1          as "MstFacilityItem__item_category1"';
        $sql .='     , a.item_category2          as "MstFacilityItem__item_category2"';
        $sql .='     , a.item_category3          as "MstFacilityItem__item_category3"';
        $sql .='     , a.pharmaceutical_number   as "MstFacilityItem__pharmaceutical_number"';
        $sql .='   from ';
        $sql .='     mst_grandmasters as a  ';
        $sql .='     left join mst_dealers as c ';
        $sql .='         on a.dealer_code = c.dealer_code  ';
        $sql .='         and c.is_deleted = false ';
        $sql .='     left join mst_jmdn_names as d on d.jmdn_code = a.jmdn_code ';
        $sql .='     left join mst_insurance_claims as f on f.insurance_claim_code = a.insurance_claim_code ';
        $sql .='     left join mst_unit_names as b on b.unit_name = a.unit_name';
        $sql .='   where ';
        $sql .='     a.master_id = ' ."'".$master_id."'";

        $tmp = $this->query($sql);
        return $tmp[0];
    }

    /**
     * 採用品情報取得
     * @param  $id 採用品ID
     * @return 取得したデータ配列
     */
    public function getFacilityItemData ($id = null) {
        if (is_null($id)) {
            return array();
        }

        $sql  =' select ';
        $sql .='       a.id                      as "MstFacilityItem__id" ';
        $sql .='     , a.mst_facility_id         as "MstFacilityItem__mst_facility_id" ';
        $sql .='     , a.internal_code           as "MstFacilityItem__internal_code" ';
        $sql .='     , a.item_name               as "MstFacilityItem__item_name" ';
        $sql .='     , a.standard                as "MstFacilityItem__standard" ';
        $sql .='     , a.item_code               as "MstFacilityItem__item_code" ';
        $sql .='     , a.jan_code                as "MstFacilityItem__jan_code" ';
        $sql .='     , a.old_jan_code            as "MstFacilityItem__old_jan_code" ';
        $sql .='     , a.refund_price            as "MstFacilityItem__refund_price" ';
        $sql .='     , a.unit_price              as "MstFacilityItem__unit_price" ';
        $sql .='     , a.price                   as "MstFacilityItem__price" ';
        $sql .='     , to_char(a.start_date , \'YYYY/mm/dd\') as "MstFacilityItem__start_date" ';
        $sql .='     , to_char(a.end_date , \'YYYY/mm/dd\')   as "MstFacilityItem__end_date" ';
        $sql .='     , a.converted_num           as "MstFacilityItem__converted_num" ';
        $sql .='     , a.medical_code            as "MstFacilityItem__medical_code" ';
        $sql .='     , a.enable_medicalcode      as "MstFacilityItem__enable_medicalcode" ';
        $sql .='     , a.biogenous_type          as "MstFacilityItem__biogenous_type" ';
        $sql .='     , a.class_separation        as "MstFacilityItem__class_separation" ';
        $sql .='     , a.tax_type                as "MstFacilityItem__tax_type" ';
        $sql .='     , a.mst_accountlist_id      as "MstFacilityItem__mst_accountlist_id" ';
        $sql .='     , a.master_id               as "MstFacilityItem__master_id" ';
        $sql .='     , a.mst_unit_name_id        as "MstFacilityItem__basic_unit_name_id" ';
        $sql .='     , a.expired_date            as "MstFacilityItem__expired_date" ';
        $sql .='     , a.is_auto_storage         as "MstFacilityItem__is_auto_storage" ';
        $sql .='     , a.is_lowlevel             as "MstFacilityItem__is_lowlevel" ';
        $sql .='     , a.is_ms_corporate         as "MstFacilityItem__is_ms_corporate"';
        $sql .='     , a.buy_flg                 as "MstFacilityItem__buy_flg"';
        $sql .='     , a.mst_favorite_group_id   as "MstFacilityItem__mst_favorite_group_id"';
        $sql .='     , a.lot_ubd_alert           as "MstFacilityItem__lot_ubd_alert" ';
        $sql .='     , a.refund_converted_num    as "MstFacilityItem__refund_converted_num"';
        $sql .='     , a.pharmaceutical_number   as "MstFacilityItem__pharmaceutical_number"';
        $sql .='     , a.parts_use               as "MstFacilityItem__parts_use"';
        $sql .='     , a.keep_type               as "MstFacilityItem__keep_type"';
        $sql .='     , a.recital                 as "MstFacilityItem__recital" ';
        $sql .='     , a.spare_key1              as "MstFacilityItem__spare_key1" ';
        $sql .='     , a.spare_key2              as "MstFacilityItem__spare_key2" ';
        $sql .='     , a.spare_key3              as "MstFacilityItem__spare_key3" ';
        $sql .='     , a.spare_key4              as "MstFacilityItem__spare_key4" ';
        $sql .='     , a.spare_key5              as "MstFacilityItem__spare_key5"';
        $sql .='     , a.spare_key6              as "MstFacilityItem__spare_key6" ';
        $sql .='     , a.spare_key7              as "MstFacilityItem__spare_key7" ';
        $sql .='     , a.spare_key8              as "MstFacilityItem__spare_key8" ';
        $sql .='     , a.spare_key9              as "MstFacilityItem__spare_key9" ';
        $sql .='     , a.spare_key10             as "MstFacilityItem__spare_key10" ';
        $sql .='     , a.spare_key11             as "MstFacilityItem__spare_key11" ';
        $sql .='     , a.spare_key12             as "MstFacilityItem__spare_key12" ';
        $sql .='     , a.spare_key13             as "MstFacilityItem__spare_key13" ';
        $sql .='     , a.spare_key14             as "MstFacilityItem__spare_key14" ';
        $sql .='     , a.spare_key15             as "MstFacilityItem__spare_key15" ';
        $sql .='     , a.spare_key16             as "MstFacilityItem__spare_key16" ';
        $sql .='     , a.spare_key17             as "MstFacilityItem__spare_key17" ';
        $sql .='     , a.spare_key18             as "MstFacilityItem__spare_key18" ';
        $sql .='     , a.spare_key19             as "MstFacilityItem__spare_key19" ';
        $sql .='     , a.spare_key20             as "MstFacilityItem__spare_key20" ';
        $sql .='     , a.spare_key21             as "MstFacilityItem__spare_key21" ';
        $sql .='     , a.spare_key22             as "MstFacilityItem__spare_key22" ';
        $sql .='     , a.spare_key23             as "MstFacilityItem__spare_key23" ';
        $sql .='     , a.spare_key24             as "MstFacilityItem__spare_key24" ';
        $sql .='     , a.spare_key25             as "MstFacilityItem__spare_key25" ';
        $sql .='     , a.spare_key26             as "MstFacilityItem__spare_key26" ';
        $sql .='     , a.spare_key27             as "MstFacilityItem__spare_key27" ';
        $sql .='     , a.spare_key28             as "MstFacilityItem__spare_key28" ';
        $sql .='     , a.spare_key29             as "MstFacilityItem__spare_key29" ';
        $sql .='     , a.spare_key30             as "MstFacilityItem__spare_key30" ';
        $sql .='     , b.facility_code           as "MstFacilityItem__mst_owner_code"  ';
        $sql .='     , b.facility_code           as "MstFacilityItem__mst_owner_code_text"  ';
        $sql .='     , c.id                      as "MstDealer__id"  ';
        $sql .='     , c.dealer_name             as "MstDealer__dealer_name"  ';
        $sql .='     , c.dealer_code             as "MstDealer__dealer_code"  ';
        $sql .='     , d.jmdn_code               as "MstFacilityItem__jmdn_code"  ';
        $sql .='     , d.jmdn_name               as "MstFacilityItem__jmdn_name"  ';
        $sql .='     , a.insurance_claim_type    as "MstFacilityItem__insurance_claim_type"';
        $sql .='     , a.item_category1          as "MstFacilityItem__item_category1"';
        $sql .='     , a.item_category2          as "MstFacilityItem__item_category2"';
        $sql .='     , a.item_category3          as "MstFacilityItem__item_category3"';
        $sql .='     , e.insurance_claim_code    as "MstFacilityItem__insurance_claim_code"';
        $sql .='     , e.insurance_claim_name    as "MstFacilityItem__insurance_claim_name"';
        $sql .='   from ';
        $sql .='     mst_facility_items as a  ';
        $sql .='     left join mst_facilities as b on a.mst_owner_id = b.id ';
        $sql .='     left join mst_dealers as c on a.mst_dealer_id = c.id  ';
        $sql .='     left join mst_jmdn_names as d on d.jmdn_code = a.jmdn_code ';
        $sql .='     left join mst_insurance_claims as e on e.insurance_claim_code = a.insurance_claim_code';
        $sql .='   where ';
        $sql .='     a.id = ' . $id ;

        $tmp = $this->query($sql);
        return $tmp[0];
    }

    /**
     * 包装単位情報取得
     * @param  $mst_facility_item_id 採用品ID
     * @param  $mst_facility_id      施設ID
     * @return 取得した配列
     */
    public function getItemUnitData ($mst_facility_item_id , $mst_facility_id) {
        if (empty($mst_facility_item_id) || empty($mst_facility_id)) {
            return array();
        }

        $sql  = ' select ';
        $sql .= '     id                     as "MstItemUnit__id" ';
        $sql .= '   , mst_facility_item_id   as "MstItemUnit__mst_facility_item_id" ';
        $sql .= '   , mst_facility_id        as "MstItemUnit__mst_facility_id" ';
        $sql .= '   , per_unit               as "MstItemUnit__per_unit" ';
        $sql .= '   , mst_unit_name_id       as "MstItemUnit__mst_unit_name_id" ';
        $sql .= "   , to_char(start_date, 'YYYY/mm/dd') ";
        $sql .= '                            as "MstItemUnit__start_date" ';
        $sql .= "   , to_char(end_date, 'YYYY/mm/dd')  ";
        $sql .= '                            as "MstItemUnit__end_date" ';
        $sql .= '   , is_standard            as "MstItemUnit__is_standard" ';
        $sql .= '   , is_deleted             as "MstItemUnit__is_deleted" ';
        $sql .= '   , ( case when is_deleted = true then false else true end ) ';
        $sql .= '                            as "MstItemUnit__checked" ';
        $sql .= '   , creater                as "MstItemUnit__creater" ';
        $sql .= '   , created                as "MstItemUnit__created" ';
        $sql .= '   , modifier               as "MstItemUnit__modifier" ';
        $sql .= '   , modified               as "MstItemUnit__modified" ';
        $sql .= '   , per_unit_name_id       as "MstItemUnit__per_unit_name_id" ';
        $sql .= '   , center_facility_id     as "MstItemUnit__center_facility_id"  ';
        $sql .= ' from ';
        $sql .= '   mst_item_units  ';
        $sql .= ' where ';
        $sql .= '   mst_facility_item_id = ' . $mst_facility_item_id;
        $sql .= '   and mst_facility_id = '  . $mst_facility_id;
        $sql .= ' order by ';
        $sql .= '   per_unit asc ';

        $ret = $this->query($sql);
        return $ret;
    }

    /**
     * 施設採用品IDから売上設定を取得する
     * @param $id      採用品ID
     * @param $user_id ユーザID
     */
    public function getSalesConfig($id, $user_id){
        $sql  = ' select ';
        $sql .= '       e.facility_name                       as "MstSalesConfig__facility_name" ';
        $sql .= '     , to_char(a.start_date, \'YYYY/mm/dd\') as "MstSalesConfig__start_date" ';
        $sql .= '     , (  ';
        $sql .= '       case  ';
        $sql .= '         when b.per_unit = 1  ';
        $sql .= '         then c.unit_name  ';
        $sql .= "         else c.unit_name || '(' || b.per_unit || d.unit_name || ')'  ";
        $sql .= '         end ';
        $sql .= '     )                                       as "MstSalesConfig__unit_name" ';
        $sql .= '     , a.sales_price                         as "MstSalesConfig__sales_price"  ';
        $sql .= '   from ';
        $sql .= '     mst_sales_configs as a  ';
        $sql .= '     left join mst_item_units as b  ';
        $sql .= '       on a.mst_item_unit_id = b.id  ';
        $sql .= '     left join mst_unit_names as c  ';
        $sql .= '       on b.mst_unit_name_id = c.id  ';
        $sql .= '     left join mst_unit_names as d  ';
        $sql .= '       on b.per_unit_name_id = d.id  ';
        $sql .= '     left join mst_facilities as e  ';
        $sql .= '       on e.id = a.partner_facility_id  ';
        // $sql .= '     inner join mst_user_belongings as f ';
        // $sql .= '       on f.mst_user_id = ' . $user_id ;
        // $sql .= '      and f.mst_facility_id = e.id ' ;
        // $sql .= '      and f.is_deleted = false' ;
        $sql .= '   where ';
        $sql .= '     a.mst_facility_item_id = ' . $id ;
        $sql .= '     and a.is_deleted = false ';
        $sql .= '     and a.start_date < now()  ';
        $sql .= '     and a.end_date > now() ';

        return $this->query($sql);
    }

    /**
     * 仕入設定された金額を取得
     * @param  $mst_item_unit_id 商品ID
     * @param  $supplier_id      仕入先ID
     * @param  $supplier_id2     仕入先2ID
     * @param  $work_date        チェックする日時
     * @return 取得した金額
     */
    public function getStokingPrice($mst_item_unit_id, $supplier_id, $supplier_id2, $work_date){
        $sql  = ' select ';
        $sql .= '     a.transaction_price  ';
        $sql .= ' from ';
        $sql .= '   mst_transaction_configs as a  ';
        $sql .= ' where ';
        $sql .= '   a.is_deleted = false  ';
        $sql .= '   and a.mst_item_unit_id = ' . $mst_item_unit_id;
        $sql .= '   and a.partner_facility_id  = ' . $supplier_id;
        $sql .= "   and a.start_date <= '" . $work_date . "'";
        $sql .= "   and a.end_date > '" . $work_date . "'";
        $ret = $this->query($sql);

        return $ret[0][0]['transaction_price'];
    }

    /**
     * 得意先コードからセンターのIDを取得する
     * @param  string $facility_code 取得したい得意先コード
     * @return string $center_id     分割テーブル等で利用されるセンターID
     */
    public function getCenterIdFromFacilityCode ($facility_code=null) {
        if (is_null($facility_code) || $facility_code == "") {
            return null;
        }

        $sql  = ' SELECT ';
        $sql .= '  b.mst_facility_id  as "centerId" ';
        $sql .= ' FROM ';
        $sql .= '  mst_facilities as a ';
        $sql .= ' LEFT JOIN mst_facility_relations as b ';
        $sql .= '  ON b.partner_facility_id = a.id ';
        $sql .= ' LEFT JOIN mst_departments as c ';
        $sql .= '  ON c.mst_facility_id = b.mst_facility_id ';
        $sql .= " WHERE a.facility_code = '" . $facility_code . "'";
        $tmp  = $this->query($sql);

        return $tmp[0][0]['centerId'];
    }

    /**
     * 次の施設採用品の商品IDを返す
     * @param  $prefix
     * @param  $facility_id 施設ID
     * @return 採番した商品ID
     */
    public function getFacilityItemNextInternalCode ($prefix='', $facility_id=null) {
        if (is_null($facility_id)) {
            return false;
        }

        $sql="SELECT max(substr(internal_code , 2)) FROM mst_facility_items WHERE mst_facility_id = " . $facility_id;

        // 製品番号のMAXを取得
        $_max_internal = $this->query($sql);
        $_max = (int)(ltrim($_max_internal[0][0]['max'],'0'));
        $_max++;

        return  $prefix . str_pad($_max,5,'0',STR_PAD_LEFT);
    }

    /**
     * 指定した商品の在庫テーブルの一覧を取得 レコードが無い場合は作成する
     * @param  $data     商品情報の連想配列
     * @return $stock_id 在庫テーブルの主キー
     */
    public function getStockRecode ($data) {
        if(is_null($data)){
            return false;
        }

        $sql  = ' SELECT ';
        $sql .= '  id as "TrnStock__id" ';
        $sql .= ' FROM ';
        $sql .= '  trn_stocks ';
        $sql .= ' WHERE ';
        $sql .= '      is_deleted = false ';
        $sql .= " AND  mst_item_unit_id  = '" . $data['mst_item_unit_id'] . "'";
        $sql .= " AND  mst_department_id = '" . $data['mst_department_id'] . "'";
        $Stock = Set::Combine($this->query($sql, false, $this->split), '{n}.TrnStock.id');
        if(empty($Stock)) {
            // 在庫レコードが無い場合はレコードを作成する
            $stock_data = array(
                'TrnStock' => array(
                    'mst_item_unit_id'  => $data['mst_item_unit_id'],
                    'mst_department_id' => $data['mst_department_id'],
                    'stock_count'       => '0',
                    'reserve_count'     => '0',
                    'promise_count'     => '0',
                    'requested_count'   => '0',
                    'receipted_count'   => '0',
                    'is_deleted'        => false,
                    'creater'           => $data['creater'],
                    'modifier'          => $data['modifier'],
                    'center_facility_id'=> $data['center_facility_id']
                )
            );

            // 書込先は在庫テーブル
            $stock_id = $this->enhanceSave('TrnStock', $stock_data);
        }else{
            $stock_id = $Stock['TrnStock']['id'];
        }
        return $stock_id;
    }

    /**
     * 包装単位登録
     */
    public function saveOneItemUnit ($data=null, $facility_item_id=null) {
        if(is_null($data) || is_null($facility_item_id)){
            return false;
        }

        $item_unit_data = array(
            'MstItemUnit' => array(
                'mst_facility_item_id' => $facility_item_id,
                'mst_facility_id'      => $data['mst_facility_id'],
                'per_unit'             => 1,
                'mst_unit_name_id'     => $data['mst_unit_name_id'],
                'start_date'           => $data['start_date'],
                'end_date'             => '3000/01/01',
                'is_standard'          => true,
                'is_deleted'           => false,
                'creater'              => $data['creater'],
                'created'              => $data['now'],
                'modifier'             => $data['modifier'],
                'modified'             => $data['now'],
                'per_unit_name_id'     => $data['mst_unit_name_id'],
                'center_facility_id'   => $data['center_facility_id'],
            )
        );

        // 書込先は包装単位テーブル
        return $this->enhanceSave('MstItemUnit', $item_unit_data);
    }

    /**
     * 売上設定を作成
     */
    public function createSalesConfig($data){
        //裏で売上設定を作る。
        foreach($data as $item){
            $sql  = ' select ';
            $sql .= '       c.id                           as mst_item_unit_id ';
            $sql .= '     , a.per_unit                     as tran_pre_unit ';
            $sql .= '     , c.per_unit                     as sale_per_unit ';
            $sql .= '     , a.mst_facility_item_id ';
            $sql .= '     , x.partner_facility_id  ';
            $sql .= '   from ';
            $sql .= '     mst_item_units as a  ';
            $sql .= '     left join mst_facility_items as b  ';
            $sql .= '       on b.id = a.mst_facility_item_id  ';
            $sql .= '       and b.item_type = ' . Configure::read('Items.item_types.normalitem');
            $sql .= '     left join mst_facility_relations as x  ';
            $sql .= '       on x.mst_facility_id = b.mst_facility_id  ';
            $sql .= '     inner join mst_facilities as y  ';
            $sql .= '       on y.id = x.partner_facility_id  ';
            $sql .= '       and y.facility_type = ' . Configure::read('FacilityType.hospital');
            $sql .= '     left join mst_item_units as c  ';
            $sql .= '       on c.mst_facility_item_id = b.id  ';
            $sql .= '     left join mst_sales_configs as d  ';
            $sql .= '       on d.mst_item_unit_id = c.id  ';
            $sql .= '      and d.partner_facility_id = y.id';
            $sql .= '   where ';
            $sql .= '     a.id = ' . $item['MstTransactionConfig']['mst_item_unit_id'];
            $sql .= '     and d.id is null ';

            $ret = $this->query($sql, false, $this->split);

            //売上げ設定がない場合作る
            foreach ($ret as $r) {
                $sales = array(
                    'MstSalesConfig' => array(
                        'mst_item_unit_id'         => $r[0]['mst_item_unit_id'],
                        'start_date'               => '1900/01/01',
                        'end_date'                 => '3000/01/01',
                        'mst_facility_relation_id' => $item['MstTransactionConfig']['mst_facility_relation_id'],
                        'mst_facility_item_id'     => $r[0]['mst_facility_item_id'],
                        'mst_facility_id'          => $item['MstTransactionConfig']['mst_facility_id'],
                        'partner_facility_id'      => $r[0]['partner_facility_id'],
                        'sales_price'              => round((( $item['MstTransactionConfig']['transaction_price'] / $r[0]['tran_pre_unit'] ) * $r[0]['sale_per_unit']) , 2),
                        'is_deleted'               => false,
                        'creater'                  => $item['MstTransactionConfig']['creater'],
                        'created'                  => $item['MstTransactionConfig']['created'],
                        'modifier'                 => $item['MstTransactionConfig']['modifier'],
                        'modified'                 => $item['MstTransactionConfig']['modified'],
                        'center_facility_id'       => $item['MstTransactionConfig']['mst_facility_id'],
                        )
                    );

                if (!$this->enhanceSave('MstSalesConfig', $sales)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 代表仕入作成
     */
    public function saveTrnMain ($main, $facility_item) {
        $writeModel = 'MstTransactionMain';
        $now        = date('Y/m/d H:i:s');

        // データ整形
        $data[$writeModel]['id']                   = $main['id'];
        $data[$writeModel]['mst_item_unit_id']     = $main['mst_item_unit_id'];
        $data[$writeModel]['mst_facility_item_id'] = $facility_item['id'];
        $data[$writeModel]['mst_facility_id']      = $main['mst_facility_id'];
        $data[$writeModel]['modifier']             = $main['user_id'];
        $data[$writeModel]['modified']             = $now;
        $data[$writeModel]['creater']              = $main['user_id'];
        $data[$writeModel]['created']              = $now;

        return $this->dispatchSave($writeModel, $data, array('id'));
    }

}