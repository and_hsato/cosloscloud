<?php
class TrnPromiseHeader extends AppModel {
    var $name = 'TrnPromiseHeader';
    
    var $belongsTo = array(
        'MstDepartment' => array(
            'className' => 'MstDepartment',
            'conditions' => '',
            'foreignKey' => 'department_id_from',
            'dependent' => false
            )
        );
    
    
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'promise_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'fixed_count' => array(
            'notempty' => array(
                'rule' => array('numeric'),
                ),
            ),
        'requested_count' => array(
            'notempty' => array(
                'rule' => array('numeric'),
                ),
            ),
        'stock_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'reserver_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'fixed_num_used' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'detail_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>