<?php
class MstFacilityRelation extends AppModel {
    var $name = 'MstFacilityRelation';
    
    var $belongsTo = array(
        'MstFacility' => array(
            'className' => 'MstFacility',
            'conditions' => '',
            'foreignKey' => 'partner_facility_id',
            'dependent' => true
            ),
        );
    
    var $validate = array(
        'mst_facility_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'partner_facility_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        );
}
?>