<?php
class TrnStorageHeader extends AppModel {
    var $name = 'TrnStorageHeader';
    var $hasMany = array('TrnStorage');
    var $belongsTo = array(
        'MstUser' => array(
            'className' => 'MstUser',
            'foreignKey' => 'creater',
            'dependent' => false,
            'conditions' => null,
            ),
        );
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'storage_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'storage_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_department_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'detail_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        );
}
?>