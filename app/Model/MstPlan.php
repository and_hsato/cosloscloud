<?php
class MstPlan extends AppModel {
    
    /**
     *
     * @var string $name
     */
    var $name = 'MstPlan';
    
    var $validate = array(
            'name' => array(
                    'notempty' => array(
                            'rule' => array('notempty'),
                            'message' => 'プラン名を入力してください',
                    )
            ),
            'max_items' => array(
                    'notempty' => array(
                            'rule' => array('notempty'),
                            'message' => '登録可能な最大マスタ件数を入力してください',
                    )
            ),
    );
}
?>