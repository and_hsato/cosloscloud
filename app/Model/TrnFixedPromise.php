<?php
class TrnFixedPromise extends AppModel {
    var $name = 'TrnFixedPromise';
    var $belongsTo = array(
        'MstItemUnit' => array(
            'className' => 'MstItemUnit',
            'conditions' => null,
            'foreignKey' => 'mst_item_unit_id',
            'dependent' => false
            ),
        'MstDepartment' => array(
            'className' => 'MstDepartment',
            'conditions' => null,
            'foreignKey' => 'department_id_from',
            'dependent' => false
            ),
        'MstDepartment_to' => array(
            'className' => 'MstDepartment',
            'conditions' => null,
            'foreignKey' => 'department_id_to',
            'dependent' => false
            ),
        'TrnPromise' => array(
            'className' => 'TrnPromise',
            'conditions' => null,
            'foreignKey' => 'trn_promise_id',
            'dependent' => false
            ),
        "TrnFixedPromiseHeader" => array(
            'className'  => 'TrnFixedPromiseHeader',
            'conditions' => null,
            'foreignKey' => 'trn_fixed_promise_header_id',
            'dependent'    => true
            ),
        );
    
    
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'work_seq' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'fixed_promise_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'trn_promise_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>