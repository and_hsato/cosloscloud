<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
    var $actsAs = array(
        'SoftDeletable' => array(
            'field'      => 'is_deleted',
            'field_date' => 'modified',
            'delete'     => true,
            'find'       => false
            ),
        'transaction' => array('keyName' => 'value')
        );
    
    /**
     * save をUpdate目的で使ってデフォルト値で上書きされる処理がいっぱいあるので共通処理で対応
     */
    public function save($data=null, $validate=true, $fieldList=array()) {
        if ( count($fieldList) === 0 ) {
            if(isset($data[$this->name]) && is_array($data[$this->name])){
                $fieldList = array_keys($data[$this->name]);
            }else{
                $fieldList = array_keys($data);
            }
        }

        // テーブル分割対応
        if(Configure::read('SplitTable.flag') == 1){
            $table_names = Configure::read('split_tables');
            foreach($table_names as $t){
                $check = strpos($this->table, $t);
                if($check !== false){
                    if(isset($data[$this->name]) && is_array($data[$this->name])){
                        if(!isset($data[$this->name]['center_facility_id'])){
                            $data[$this->name]['center_facility_id'] = $_SESSION['Auth']['facility_id_selected'];
                        }
                        $fieldList = array_keys($data[$this->name]);
                    }else{
                        if(!isset($data['center_facility_id'])){
                            $data['center_facility_id'] = $_SESSION['Auth']['facility_id_selected'];
                        }
                        $fieldList = array_keys($data);
                    }
                    //find , updateAll で子テーブル指定になってるので、親テーブルをさしなおす。
                    $this->table = $t;
                }
            }
        }
        return parent::save($data, $validate, $fieldList);
    }
    
    /**
     * updateALLをオーバーライド。
     * なぜかJOINしたSELECT文を投げて変なSQLエラー出てたので、recursiveが効くように修正
     * @param array $fields
     * @param array $conditions
     * @param int $recursive
     * @return boolean
     */
    public function updateAll($fields, $conditions = true, $recursive = null) {
        // テーブル分割対応
        if(Configure::read('SplitTable.flag') == 1){
            if(isset($_SESSION['Auth']['facility_id_selected']) || isset($_POST['data']['SplitTable']['center_facility_id']) ){
                if(isset($_SESSION['Auth']['facility_id_selected'])){
                    $center_id = $_SESSION['Auth']['facility_id_selected'];
                }else{
                    $center_id = $_POST['data']['SplitTable']['center_facility_id'];
                }
                $table_names = Configure::read('split_tables');
                foreach($table_names as $t){
                    $check = strpos($this->table, $t);
                    if($check !== false){
                        $this->table = $t . '_' . $center_id;
                    }
                }
            }
        }
        
        if(false === isset($recursive)){
            $recursive = $this->recursive;
        }
        if(-1 === $recursive){
            $this->unbindModel(array(
                'belongsTo' => array_keys($this->belongsTo),
                'hasOne' => array_keys($this->hasOne),
                'hasMany' => array_keys($this->hasMany),
                ), true);
        }
        return parent::updateAll($fields, $conditions);
    }
    
    function del($confitions = array()){
        if($confitions === array()){
            return false;
        }
        $fields = array('is_deleted' => 'true',
                        'modifier'   => $_SESSION['Auth']['MstUser']['id'],
                        'modified'   => "'". date('Y/m/d H:i:s.u') . "'"
                        );
        return parent::updateAll($fields , $confitions);
    }
    
    function maxLengthJp($check, $max) {
        $check_str = array_shift($check);
        $length = mb_strlen($check_str, mb_detect_encoding($check_str));
        return ($length <= $max);
        
    }
    
    /**
     * 環境によって全角も通すらしいのでオーバーライドします
     */
    function alphaNumeric($data) {
        $check = is_array($data) ? array_shift($data) : $data;
        if (preg_match('/[\\dA-Z]/i',$check)) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Query : 分割テーブル対応
     */
    public function query($sql , $cache = false , $replace = true){
        if(Configure::read('SplitTable.flag') == 1){
            if($replace){
                if(isset($_SESSION['Auth']['facility_id_selected']) || isset($_POST['data']['SplitTable']['center_facility_id']) ){
                    if(isset($_SESSION['Auth']['facility_id_selected'])){
                        $center_id = $_SESSION['Auth']['facility_id_selected'];
                    }else{
                        $center_id = $_POST['data']['SplitTable']['center_facility_id'];
                    }
                    $table_names = Configure::read('split_tables');
                    foreach($table_names as $t){
                        $tbl_name = $t . '_' . $center_id;
                        $sql = str_replace ($t, $tbl_name, $sql);
                    }
                }
            }
        }
        return parent::query($sql , $cache);
    }
    
    /**
     * find : 分割テーブル対応
     */
    public function find($type ,$params , $replace = true){
        if(Configure::read('SplitTable.flag') == 1){
            if($replace) {
                if(isset($_SESSION['Auth']['facility_id_selected']) || isset($_POST['data']['SplitTable']['center_facility_id']) ){
                    if(isset($_SESSION['Auth']['facility_id_selected'])){
                        $center_id = $_SESSION['Auth']['facility_id_selected'];
                    }else{
                        $center_id = $_POST['data']['SplitTable']['center_facility_id'];
                    }
                    $table_names = Configure::read('split_tables');
                    if($type == 'all' || $type == 'count') {
                        if(isset($params['joins'])){
                            foreach($params['joins'] as &$j){
                                foreach($table_names as $t){
                                    if($j['table'] === $t){
                                        $j['table'] = $t . '_' . $center_id;
                                    }
                                }
                            }
                            unset($j);
                        }
                        foreach($table_names as $t){
                            $check = strpos($this->table, $t);
                            if($check !== false){
                                $this->table = $t . '_' . $center_id;
                            }
                        }
                    }
                    if($type == 'first' || $type == 'list'){
                        foreach($table_names as $t){
                            $check = strpos($this->table, $t);
                            if($check !== false){
                                $this->table = $t . '_' . $center_id;
                            }
                        }
                    }
                }
            }
        }
        return parent::find($type , $params);
    }
}
