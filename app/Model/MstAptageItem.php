<?php
class MstAptageItem extends AppModel {

    /**
     *
     * @var string $name
     */
    var $name = 'MstAptageItem';

    /**
     * 商品IDの一覧を取得
     */
    function getItemCodes($center_id){
        $sql  = "SELECT facility_code, item_code, unit_name FROM mst_aptage_items WHERE is_deleted = false and mst_facility_id = " . $center_id ;
        $data = $this->query($sql);

        //データ整形 得意先コードでまとめる
        $ret = array();
        foreach ($data as $value) {
            $value = $value[0];
            $ret[$value['facility_code']][$value['item_code']] = $value['unit_name'];
        }
        return $ret;
    }
}
?>