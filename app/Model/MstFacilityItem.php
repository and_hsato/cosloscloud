<?php
class MstFacilityItem extends AppModel {
    
    /**
     * 
     * @var string $name
     */
    var $name = 'MstFacilityItem';

    var $belongsTo = array(
        'MstDealer' => array(
            'className' => 'MstDealer',
            'conditions' => array('MstDealer.is_deleted' => false),
            'foreignKey' => 'mst_dealer_id',
            'dependent' => true
            )
        );
    
    /**
     * 商品IDに重複値があるかチェック
     * true:重複なし false:重複あり
     */
    function chkDuplicateInternalCode($internal_code,$id,$mst_facility_id){
        
        $where = array('MstFacilityItem.internal_code =' => $internal_code,
                       'MstFacilityItem.id !=' => $id,
                       'MstFacilityItem.mst_facility_id =' => $mst_facility_id);
        $n = $this->find('count',
                         array('conditions' => $where));
        
        return $n > 0 ? false : true;
    }
}
?>