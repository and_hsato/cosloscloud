<?php
class TrnShipping extends AppModel {
    var $name = 'TrnShipping';
    
    /**
     *
     * @var string $belongsTo
     */
    var $belongsTo   = array(
        'MstClass'  => array(
            'className'  => 'MstClass',
            'foreignKey' => 'work_type')
        ,'TrnShippingHeader'  => array(
            'className'  => 'TrnShippingHeader',
            'foreignKey' => 'trn_shipping_header_id')
        ,'MstItemUnit' => array(
            'className' => 'MstItemUnit',
            'foreignKey' => 'mst_item_unit_id'
            )
        );
    
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'work_seq' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'shipping_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'shipping_status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_from' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'department_id_to' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'trn_sticker_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>