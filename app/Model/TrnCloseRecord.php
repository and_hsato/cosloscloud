<?php
class TrnCloseRecord extends AppModel {
    var $name = 'TrnCloseRecord';
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'target_date' => array(
            'date' => array(
                'rule' => array('date'),
                ),
            ),
        'mst_department_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_item_unit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'quantity' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'trn_close_header_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>