<?php
class TrnConsumeHeader extends AppModel {
  
    /**
     * 
     * @var string $name
     */
    var $name = 'TrnConsumeHeader';
    
    /**
     * 
     * @var array $hasMany
     */
    var $hasMany = array();
    
    /**
     * 
     * @var array|mixed $validate
     */
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'use_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_department_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'detail_count' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>