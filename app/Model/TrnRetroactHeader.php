<?php
class TrnRetroactHeader extends AppModel {
    /**
     * 
     * @var string $name
     */
    var $name = 'TrnRetroactHeader';
    
    /**
     * 
     * @var array $hasMany
     */
    var $hasMany = array(
        'TrnRetroactRecord' => array(
            'className'     => 'TrnRetroactRecord',
            'foreignKey'    => 'trn_retroact_header_id',
            'conditions'    => array('TrnRetroactRecord.is_deleted' => false),
            'order'    => 'TrnRetroactRecord.created DESC',
            'limit'        => null,
            'dependent'=> true
            ),
        );
    
    /**
     * 
     * @var array $belongsTo
     */
    var $belongsTo = array('MstFacility');
    
    /**
     * 
     * @var array $validate
     */
    var $validate = array(
        'work_no' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'close_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_facility_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>