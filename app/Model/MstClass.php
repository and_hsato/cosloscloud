<?php
/**
 * MstClass
 *　作業区分
 * @version 1.0.0
 * @since 2010/05/18
 */
class MstClass extends AppModel {
    
    var $name = 'MstClass';
    var $primaryKey = 'id';
    
    /**
     *
     * @var string $belongsTo
     */
    var $belongsTo   = array(
        'MstMenu'  => array(
            'className'  => 'MstMenu',
            'foreignKey' => 'mst_menu_id')
        );
    
    /**
     *
     * @var array $validate
     */
    var $validate = array(
        'code' => array(
            'notempty' => array(
                'rule'   => array('notempty'),
                'message'=> 'コードを入力してください',
                ),
            
            'maxLengthJp' => array(
                'rule'   => array('maxLengthJp','20'),
                'message'=> 'コードは20文字以内で入力してください',
                ),
            ),
        'name' => array(
            'notempty'    => array(
                'rule'      => array('notempty'),
                'message'   => '名称を入力してください',
                ),
            ),
        'mst_menu_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '表示箇所を選択してください',
                ),
            ),
        'mst_facility_id' => array(
            'notempty'   => array(
                'rule'   => array('notempty'),
                'message'   => '施設を選択してください',
                ),
            ),
        );
}
?>