<?php
class MstUser extends AppModel {

  /**
   *
   * @var string $name
   */
  var $name = 'MstUser';

  /**
   *
   * @var array $belongsTo
   */
  var $belongsTo = array(
    'MstDepartment' => array(
      'className' => 'MstDepartment',
      'foreignKey' => 'mst_department_id'
    ),
    'MstFacility' => array(
      'className' => 'MstFacility',
      'foreignKey' => 'mst_facility_id'
    ),
    'MstRole' => array(
      'className' => 'MstRole',
      'foreignKey' => 'mst_role_id'
    ),
    'Parent' => array(
        'className' => 'MstUser',
        'foreignKey' => 'parent_id'
    ),
    'MstPlan' => array(
        'className' => 'MstPlan',
        'foreignKey' => 'mst_plan_id'
    ),
  );

  /**
   * 
   * @var array $hasMany
   * FIXME hasManyが効かないのでは？
   */  
  var $hasMany = array(
    'MstUserBelonging' => array(
        'className'  => 'MstUserBelonging',
        'foreignKey' => 'mst_user_id',
        'conditions' => array('MstUserBelonging.mst_user_id' => 'MstUser.id'),
        'order'      => 'MstUserBelonging.created DESC',
        'limit'      => null,
        'dependent'  => true
    ),
  );  
  
  /**
   * 
   * @var array $validate（仕様未定）
   */
  var $validate = array(
      'login_id' => array(
      'notEmpty' => array(
          'rule' => array('notEmpty'),
       'message' => 'ユーザIDを入力してください'
      ),

    ),
      'password' => array(
        'notempty' => array(
          'rule'   => array('notempty'),
         'message' => 'パスワードを入力してください',
      ),
    ),
       'user_name' => array(
        'notempty' => array(
            'rule' => array('notempty'),
         'message' => 'ログイン名を入力してください',
      ),
    ),
        'user_facility' => array(     //TODO　IDかCDか統一
             'notempty' => array(
                 'rule' => array('notempty'),
              'message' => '施設を選択してください',
      ),
    ),
      'mst_role_id' => array(
         'notempty' => array(
             'rule' => array('notempty'),
          'message' => 'ロールを選択してください',
      ),
    ),
  );
    
    /**
     * hashPasswords
     * 
     * @param mixed $data
     * @return string|mixed $data
     */
    function hashPasswords($data){
        if(Configure::read('Password.Security') == 1 ){
            return md5(Configure::read('Security.salt') . $data);
        }else{
            return $data;
        }
    }
    
    function getParentProfileOf($mstUser) {
        if (empty($mstUser['parent_id'])) return null;
        return $this->getUserProfileById($mstUser['parent_id']);
    }
    
    function getUserProfileById($mstUserId) {
        return $this->find('first', array(
                'conditions' => array(
                        'MstUser.id' => $mstUserId,
                        'MstUser.is_deleted' => false,
                ),
                'recursive' => 2,
                ));
    }
}
?>