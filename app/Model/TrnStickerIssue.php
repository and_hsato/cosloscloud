<?php
/**
 * シール発行クラス
 * @version 1.0.0
 * @since 2010/08/30
 */
App::import('Sanitize');
class TrnStickerIssue extends AppModel {
  /**
   * Class name.
   * @var string $name
   */
  var $name = 'TrnStickerIssue';

  /**
   *
   * @var array $hasOne
   */
  var $hasOne = array();
  /**
   *
   * @var array $belongsTo
   */
  var $belongsTo = array('MstItemUnit');
}
?>
