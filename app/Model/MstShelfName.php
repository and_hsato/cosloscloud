<?php
class MstShelfName extends AppModel {
    var $name = 'MstShelfName';
    var $displayField = 'name';
    
    var $belongsTo   = array(
        'MstFacility'  => array(
            'className'  => 'MstFacility',
            'foreignKey' => 'mst_facility_id'
            ),
        'MstDepartment' => array(
            'className' => 'MstDepartment',
            'conditions' => null,
            'foreignKey' => 'mst_department_id',
            'dependent' => false
            ),
        );
    
    
    var $validate = array(
        'mst_facility_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'mst_department_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>