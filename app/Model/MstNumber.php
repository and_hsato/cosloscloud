<?php
class MstNumber extends AppModel {
    var $name = 'MstNumber';
    var $validate = array(
        'number_type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                ),
            ),
        'last_date' => array(
            'date' => array(
                'rule' => array('date'),
                ),
            ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                ),
            ),
        );
}
?>