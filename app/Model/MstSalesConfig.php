<?php
class MstSalesConfig extends AppModel {
    
    /**
     * Class name.
     * @var string $name
     */
    var $name = 'MstSalesConfig';
    
    /**
     * MstSalesConfig class belongs to others.
     * @var array $belongsTo
     */
    
    /**
     * 
     * @var array $validate
     */
    
    var $validate = array(
        'start_date' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '適用開始日を入力してください',
                ),
            ),
        'partner_facility_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '仕入先を入力してください',
                ),
            ),
        'mst_item_unit_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '包装単位を入力してください',
                ),
            ),
        'sales_price' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '取引単価を入力してください',
                ),
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => '取引単価は数値で入力してください',
                ),
            ),
        'partner_facility_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '相手先施設が取得できません',
                ),
            ),
        'mst_facility_relation_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => '施設との関係設定がありません',
                ),
            ),
        );
}
?>