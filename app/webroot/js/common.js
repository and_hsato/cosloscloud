//置換実装
String.prototype.replaceAll = function (org, dest){
  return this.split(org).join(dest);
}

//indexOfの実装
if (!Array.indexOf) {
    Array.prototype.indexOf = function(o) {
        for (var i in this) {
            if (this[i] == o) {
                return i;
            }
        }
        return -1;
    }
}

//Trimの実装
String.prototype.trim = function() {return this.replace(/^\s+|\s+$/g,"");}

//in_Arrayの実装
Array.prototype.in_array = function(val) {
    for(var i = 0, l = this.length; i < l; i++) {
        if(this[i] == val) {
            return true;
        }
    }
    return false;
}

$(window).keydown(function(e){
    if(e.keyCode == 8) { //バックスペース禁止
        if(e.target.tagName != 'INPUT'){
            if(e.target.tagName != 'TEXTAREA') { //テキスト入力はバックスペース許可
                return false;
            }
        }
    }
    // ALT+← 禁止
    if(e.keyCode == 37){
        if(e.altKey){
            return false;
        }
    }
    
    // F5 禁止
    if(e.keyCode == 116){
        return false;
    }
    // Ctr+R 禁止
    if(e.keyCode == 82){
        if(e.ctrlKey){
            return false;
        }
    }
});

//日付チェック
function dateCheck(val){
    // YYYY/MM/DD形式チェック(年は数字4桁,月は01～12,日は01～31)
    if(!val.match(/^([0-9]{4})\/(0[1-9]|1[012])\/(0[1-9]|[12][0-9]|3[01])$/)){
        return false;
    }
    //年月日
    var sYear  = val.split("/")[0] - 0;
    var sMonth = val.split("/")[1] - 1; // Javascriptは、0-11で表現
    var sDay   = val.split("/")[2] - 0;

    //日付の妥当性チェック
    var oDate = new Date(Date.parse(val));
    if(oDate.getFullYear() == sYear && oDate.getMonth() == sMonth && oDate.getDate() == sDay){
        return true;
    }else{
        return false;
    }
}

//数値チェック
function numCheck(val, intcount, decimalcount , zerochk  ){
    var reg;
    var regstr;
    if(val == ''){
        return false;
    }
    if(intcount == 0 || intcount == '' ){
        return false;
    }

    if(zerochk){
        regstr = "^[1-9][0-9]{0,"+( intcount -1 ) +"}";
    }else{
        regstr = "^[0-9]{1,"+intcount +"}";
    }
    
    if(decimalcount > 0){
        regstr = regstr + "(\\.[0-9]{1,"+decimalcount+"})?$";
    }
    reg = new RegExp(regstr);
    if(val.match(reg)){
        return true;
    }else{
        return false;
    }
}

// ページ上へ、下へ
function anc(id){
   obj = $("#"+id)[0];
   y = obj.offsetTop;
   scrollTo(0,y);
}

//number_formatをかける
function numberFormat(str) {
    var num = new String(str).replace(/,/g, "");
    while(num != (num = num.replace(/^(-?\d+)(\d{3})/, "$1,$2")));
    return num;
}


// バリデートエンジンの無効化 && ぐるぐる表示 && submit!!
function validateDetachSubmit( form ,  url ){
    form.validationEngine('detach');
    setTimeout(function(){$('#waitingPanel').click()},100);
    $('input[type=button]').attr('disabled' , 'disabled');
    
    form.attr('method', 'post');
    form.attr('action', url);
    if(form.hasClass('search_form')){
        form.find('input[type=text]').each(function(){
            $(this).val($.trim($(this).val()));
        });
        //form.find('input.search_canna').each(function(){
        //    this.value = FHConvert.ftoh(FHConvert.fkktohkk(this.value),{jaCode:true});
        //});
        form.find('input.search_upper').each(function(){
            this.value = FHConvert.ftoh(FHConvert.fkktohkk(this.value),{jaCode:true}).toUpperCase();
        });
        form.find('input.search_internal_code').each(function(){
            if(this.value.length > 0){
            this.value = (internal_code_prefix+this.value).slice('-'+internal_code_length);
            }
        });
    }
    form.submit();
}

function convertDate(val){
    if(val.indexOf("/") == -1 ){
        // スラッシュがない場合
        if( val.length == 8 ){ //YYYYMMDD
            var sYear  = val.substr(0,4);
            var sMonth = val.substr(4,2);
            var sDay   = val.substr(6,2);
            if(sDay == "00" ){
                sDay = new Date(sYear, sMonth, 0).getDate();
            }
         } else if( val.length == 6 ) { // YYMMDD
            var sYear  = '20' + val.substr(0,2);
            var sMonth = val.substr(2,2);
            var sDay = val.substr(4,2);
            if(sDay == "00" ){
                sDay = new Date(sYear, sMonth, 0).getDate();
            }
        } else {
            return val;
        }
    }else{
       // スラッシュがある場合。
       //年月日
       var sYear  = val.split("/")[0];
       var sMonth = val.split("/")[1];
       var sDay   = val.split("/")[2];
       if(sDay == undefined || sDay == "0" || sDay == "00"){
           //日付が空の場合
           sDay = new Date(sYear, sMonth, 0).getDate();
       }
    }
    if(sMonth.length < 2){
        sMonth = "0" + sMonth;
    }
    if(sDay.length < 2){
        sDay = "0" + sDay;
    }
    return sYear + "/" + sMonth + "/" + sDay;
}

function getDepartment(facilityCode){
    var params = {};
    params['field'] = '';
    params['value'] = facilityCode;
    $.ajax({
        type: 'POST',
        url: basepath + "/get_departments/",
        data: params,
        success: function(data, dataType){
            $('#departmentCode').children().remove();
            $('#departmentText').val('');
            var obj = $.parseJSON(data);
            $.each(obj, function(idx, o){
                if(idx === 0){
                    $('<option value=""></option>').appendTo('#departmentCode');
                }
                if(o.department_code == $('#departmentText').val()){
                    $('<option></option>').text(o.department_name).attr('value', o.department_code).attr('selected','selected').appendTo('#departmentCode');
                }else{
                    $('<option></option>').text(o.department_name).attr('value', o.department_code).appendTo('#departmentCode');
                }

            });
        },
        error: function(){
            alert('部署データの取得に失敗しました');
        }
    });
    return false;
}

function getFacility(facilityType){
    var params = {};
    params['field'] = '';
    params['value'] = facilityType;
    $.ajax({
        type: 'POST',
        url: basepath + "/get_facilities/",
        data: params,
        success: function(data, dataType){
            $('#facilityCode').children().remove();
            $('#facilityText').val('');
            var obj = $.parseJSON(data);
            $.each(obj, function(idx, o){
                if(idx === 0){
                    $('<option value=""></option>').appendTo('#facilityCode');
                }
                if(o.facility_code == $('#facilityText').val()){
                    $('<option></option>').text(o.facility_name).attr('value', o.facility_code).attr('selected','selected').appendTo('#facilityCode');
                }else{
                    $('<option></option>').text(o.facility_name).attr('value', o.facility_code).appendTo('#facilityCode');
                }

            });
        },
        error: function(){
            alert('施設データの取得に失敗しました');
        }
    });
    return false;
}

function getCategoryList(rank , id){
    var params = {};
    params['rank'] = rank;
    params['id']   = id;
    target = '#item_category' + rank;
    $.ajax({
        type: 'POST',
        url: basepath + "/get_category/",
        data: params,
        success: function(data, dataType){
            $(target).children().remove();
            var obj = $.parseJSON(data);
            $.each(obj, function(idx, o){
                if(idx === 0){
                    $('<option value=""></option>').appendTo(target);
                }
                $('<option></option>').text(o.category_name).attr('value', o.id).appendTo(target);
            });
        },
        error: function(){
            alert('カテゴリデータの取得に失敗しました');
        }
    });
    return false; 
}

function simple_tooltip(target_items, name){
    $(target_items).each(function(i){
        if($(this).hasClass('btn1')){$(this).attr("title","検索");}
        if($(this).hasClass('btn2')){$(this).attr("title","確定");}
        if($(this).hasClass('btn3')){$(this).attr("title","確認");}
        if($(this).hasClass('btn4')){$(this).attr("title","選択");}
        if($(this).hasClass('btn5')){$(this).attr("title","CSV");}
        if($(this).hasClass('btn6')){$(this).attr("title","取消");}
        if($(this).hasClass('btn7')){$(this).attr("title","明細表示");}
        if($(this).hasClass('btn8')){$(this).attr("title","備考一括設定");}
        if($(this).hasClass('btn9')){$(this).attr("title","編集");}
        if($(this).hasClass('btn10')){$(this).attr("title","印刷");}
        if($(this).hasClass('btn11')){$(this).attr("title","新規");}
        if($(this).hasClass('btn12')){$(this).attr("title","コストシール印字");}
        if($(this).hasClass('btn13')){$(this).attr("title","全クリア");}
        if($(this).hasClass('btn14')){$(this).attr("title","クリア");}
        if($(this).hasClass('btn15')){$(this).attr("title","在庫選択");}
        if($(this).hasClass('btn16')){$(this).attr("title","部署シール印字");}
        if($(this).hasClass('btn17')){$(this).attr("title","シール読込");}
        if($(this).hasClass('btn18')){$(this).attr("title","シール印字");}
        if($(this).hasClass('btn19')){$(this).attr("title","納品書印刷");}
        if($(this).hasClass('btn20')){$(this).attr("title","売上依頼票印刷");}
        if($(this).hasClass('btn21')){$(this).attr("title","発注書印刷");}
        if($(this).hasClass('btn22')){$(this).attr("title","CSVで対象を指定");}
        if($(this).hasClass('btn23')){$(this).attr("title","マスタの設定で反映");}
        if($(this).hasClass('btn24')){$(this).attr("title","更新");}
        if($(this).hasClass('btn25')){$(this).attr("title","付替");}
        if($(this).hasClass('btn26')){$(this).attr("title","CSV取込");}
        if($(this).hasClass('btn27')){$(this).attr("title","出荷伝票印刷");}
        if($(this).hasClass('btn28')){$(this).attr("title","センターシール印字");}
        if($(this).hasClass('btn29')){$(this).attr("title","登録");}
        if($(this).hasClass('btn30')){$(this).attr("title","読込");}
        if($(this).hasClass('btn31')){$(this).attr("title","差異リスト印刷");}
        if($(this).hasClass('btn32')){$(this).attr("title","補充依頼票印刷");}
        if($(this).hasClass('btn33')){$(this).attr("title","セット品明細票印刷");}
        if($(this).hasClass('btn34')){$(this).attr("title","ダウンロード");}
        if($(this).hasClass('btn35')){$(this).attr("title","ログイン");}
        if($(this).hasClass('btn36')){$(this).attr("title","仮パスワード再設定");}
        if($(this).hasClass('btn37')){$(this).attr("title","在庫受払CSV");}
        if($(this).hasClass('btn38')){$(this).attr("title","在庫表印刷");}
        if($(this).hasClass('btn39')){$(this).attr("title","削除");}
        if($(this).hasClass('btn40')){$(this).attr("title","単価を選択して反映");}
        if($(this).hasClass('btn41')){$(this).attr("title","売上設定印刷");}
        if($(this).hasClass('btn42')){$(this).attr("title","返品明細書印刷");}
        if($(this).hasClass('btn43')){$(this).attr("title","包装単位を選択して反映");}
        if($(this).hasClass('btn44')){$(this).attr("title","明細追加");}
        if($(this).hasClass('btn45')){$(this).attr("title","CSV出力");}
        if($(this).hasClass('btn46')){$(this).attr("title","バーコード読込");}
        if($(this).hasClass('btn47')){$(this).attr("title","マスタ単価一括設定");}
        if($(this).hasClass('btn48')){$(this).attr("title","ユーザ一覧CSV出力");}
        if($(this).hasClass('btn49')){$(this).attr("title","引当可能は全て確定");}
        if($(this).hasClass('btn50')){$(this).attr("title","仮締め実行");}
        if($(this).hasClass('btn51')){$(this).attr("title","開始日一括設定");}
        if($(this).hasClass('btn52')){$(this).attr("title","期限切れ処理");}
        if($(this).hasClass('btn53')){$(this).attr("title","金額差異一覧印刷");}
        if($(this).hasClass('btn54')){$(this).attr("title","欠品リスト印刷");}
        if($(this).hasClass('btn55')){$(this).attr("title","行をコピー");}
        if($(this).hasClass('btn56')){$(this).attr("title","行追加");}
        if($(this).hasClass('btn57')){$(this).attr("title","差異リストCSV");}
        if($(this).hasClass('btn58')){$(this).attr("title","採用品選択");}
        if($(this).hasClass('btn59')){$(this).attr("title","参考仕入単価一括コピー");}
        if($(this).hasClass('btn60')){$(this).attr("title","参考売上単価一括コピー");}
        if($(this).hasClass('btn61')){$(this).attr("title","残数一括設定");}
        if($(this).hasClass('btn62')){$(this).attr("title","仕入設定印刷");}
        if($(this).hasClass('btn63')){$(this).attr("title","施設に追加");}
        if($(this).hasClass('btn64')){$(this).attr("title","自動発注の計算から除く");}
        if($(this).hasClass('btn65')){$(this).attr("title","自動発注の計算に含む");}
        if($(this).hasClass('btn66')){$(this).attr("title","出荷選択");}
        if($(this).hasClass('btn67')){$(this).attr("title","商品選択");}
        if($(this).hasClass('btn68')){$(this).attr("title","数量を計算して登録");}
        if($(this).hasClass('btn69')){$(this).attr("title","数量を指定して登録");}
        if($(this).hasClass('btn70')){$(this).attr("title","選択部署未引当一覧CSV");}
        if($(this).hasClass('btn71')){$(this).attr("title","選択部署未引当一覧を表示");}
        if($(this).hasClass('btn72')){$(this).attr("title","選択部署未引当一覧印刷");}
        if($(this).hasClass('btn73')){$(this).attr("title","全てを作成");}
        if($(this).hasClass('btn74')){$(this).attr("title","全未引当一覧CSV");}
        if($(this).hasClass('btn75')){$(this).attr("title","全未引当一覧を表示");}
        if($(this).hasClass('btn76')){$(this).attr("title","全未引当一覧印刷");}
        if($(this).hasClass('btn77')){$(this).attr("title","棚卸表印刷");}
        if($(this).hasClass('btn78')){$(this).attr("title","棚番CSVダウンロード");}
        if($(this).hasClass('btn79')){$(this).attr("title","棚番CSV取込");}
        if($(this).hasClass('btn80')){$(this).attr("title","棚番バーコード印字");}
        if($(this).hasClass('btn81')){$(this).attr("title","追加");}
        if($(this).hasClass('btn82')){$(this).attr("title","低レベル品一覧");}
        if($(this).hasClass('btn83')){$(this).attr("title","定数一覧印刷");}
        if($(this).hasClass('btn84')){$(this).attr("title","独自品新規登録");}
        if($(this).hasClass('btn85')){$(this).attr("title","発注数を自動計算の対象にして確定");}
        if($(this).hasClass('btn86')){$(this).attr("title","発注数を自動計算の対象にしないで確定");}
        if($(this).hasClass('btn87')){$(this).attr("title","本締め実行");}
        if($(this).hasClass('btn88')){$(this).attr("title","未出荷一覧");}
        if($(this).hasClass('btn89')){$(this).attr("title","預託品シール読込");}
        if($(this).hasClass('btn90')){$(this).attr("title","預託品出荷選択");}
        if($(this).hasClass('btn91')){$(this).attr("title","要求作成履歴印刷");}
        if($(this).hasClass('btn92')){$(this).attr("title","履歴選択");}
        if($(this).hasClass('btn93')){$(this).attr("title","ログアウト");}
        if($(this).hasClass('btn94')){$(this).attr("title","お気に入り");}
        if($(this).hasClass('btn95')){$(this).attr("title","病院作業");}
        if($(this).hasClass('btn96')){$(this).attr("title","発注関連");}
        if($(this).hasClass('btn97')){$(this).attr("title","入荷関連");}
        if($(this).hasClass('btn98')){$(this).attr("title","在庫関連");}
        if($(this).hasClass('btn99')){$(this).attr("title","マスタメンテナンス");}
        if($(this).hasClass('btn100')){$(this).attr("title","管理者メニュー");}
        if($(this).hasClass('btn101')){$(this).attr("title","病院（部署）作業");}
        if($(this).hasClass('btn102')){$(this).attr("title","月次処理");}
        if($(this).hasClass('btn103')){$(this).attr("title","分析");}
        if($(this).hasClass('btn104')){$(this).attr("title","受注");}
        if($(this).hasClass('btn105')){$(this).attr("title","出荷");}
        if($(this).hasClass('btn106')){$(this).attr("title","未受領一覧");}
        if($(this).hasClass('btn107')){$(this).attr("title","商品検索へ進む");}
        if($(this).hasClass('btn108')){$(this).attr("title","請求伝票印刷");}
        if($(this).hasClass('btn109')){$(this).attr("title","Excel");}
        if($(this).hasClass('btn110')){$(this).attr("title","シール区分変更");}
        if($(this).hasClass('btn111')){$(this).attr("title","補充リスト印刷");}
        if($(this).hasClass('btn112')){$(this).attr("title","残数チェック");}
        if($(this).hasClass('btn113')){$(this).attr("title","アプテージ用連携データ");}
        if($(this).hasClass('btn114')){$(this).attr("title","発注書サマリ印刷");}
        if($(this).hasClass('btn115')){$(this).attr("title","払出登録");}
        if($(this).hasClass('btn116')){$(this).attr("title","返却登録");}
        if($(this).hasClass('btn117')){$(this).attr("title","中止：全返却");}
        if($(this).hasClass('btn118')){$(this).attr("title","一時保存");}
        if($(this).hasClass('btn119')){$(this).attr("title","参考単価一括コピー");}
        if($(this).hasClass('btn120')){$(this).attr("title","納品リスト印刷");}
        if($(this).hasClass('btn121')){$(this).attr("title","未納リスト印刷");}

        $("body").append("<div class='"+name+"' id='"+name+i+"'><p>"+$(this).attr('title')+"</p></div>");
        var my_tooltip = $("#"+name+i);
        $(this).removeAttr("title").mouseover(function(){
             my_tooltip.css({opacity:0.8, display:"none"}).fadeIn(400);
        }).mousemove(function(kmouse){
            var border_top = $(window).scrollTop(); 
            var border_right = $(window).width();
            var left_pos;
            var top_pos;
            var offset = 20;
            if(border_right - (offset *2) >= my_tooltip.width() + kmouse.pageX){
                left_pos = kmouse.pageX+offset;
            } else{
                left_pos = border_right-my_tooltip.width()-offset;
            }
            if(border_top + (offset *2)>= kmouse.pageY - my_tooltip.height()){
                top_pos = border_top +offset;
            } else{
                top_pos = kmouse.pageY-my_tooltip.height()-offset;
            }
            my_tooltip.css({left:left_pos, top:top_pos});
        }).mouseout(function(){
            my_tooltip.css({left:"-9999px"});
        });
     });
}
