﻿$(document).ready(function() {

     //材料削除ボタン押下
     $('.item_del_btn').click(function(){
         $(this).parent().parent().remove();
     });

     //材料セット削除ボタン押下
     $('.item_set_del_btn').click(function(){
         var setCode  = $(this).attr('id');
         $("tr." + setCode).remove();
         $(this).parent().parent().remove();
     });
    
     // 加算ボタン押下
     $('.item_plus_btn').click(function(){
         var quantity = $(this).parent().find('.quantity');
         quantity.val(parseInt(quantity.val()) + 1);
     });
     // 減算ボタン押下
     $('.item_minus_btn').click(function(){
         var quantity = $(this).parent().find('.quantity');
         quantity.val(parseInt(quantity.val()) - 1);
         if(parseInt(quantity.val()) < 1 ){
             $(this).parent().parent().remove();
         }
     });

  
    $('#search_item_form').hide();
    $('#call_item_btn').click(function() {
        $('#search_item_form').show();
        //$('#search_item_form').draggable();
        $('#search_item_name').focus();
        $('#search_method_form').hide();
        $('#search_item_set_form').hide();
    });
    
    $('#search_item_form').bgiframe();

    $('#search_item_set_form').hide();
    $('#call_item_set_btn').click(function() {
        $('#search_item_set_form').show();
        //$('#search_item_set_form').draggable();
        $('#search_item_set_name').focus();
        $('#search_item_form').hide();
        $('#search_method_form').hide();
    });
    
    $('#search_item_set_form').bgiframe();

    $('#search_method_form').hide();
    $('#call_method_btn').click(function() {
        $('#search_method_form').show();
        //$('#search_method_form').draggable();
        $('#search_method_name').focus();
        $('#search_item_form').hide();
        $('#search_item_set_form').hide();
    });
    
    $('#search_method_form').bgiframe();
    $('img.floatClose').hover(function(){
        $(this).attr('src', webroot + 'img/close2_red.gif');
    }, function(){
        $(this).attr('src', webroot + 'img/close2_blk.gif');
    });
    $('img.floatClose').click(function(){
        $(this).parent().hide();
    });  

    $('#search_method_name').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            $('#search_method_btn').click();
        }
    });

    $('#search_item_name').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            $('#search_item_btn').click();
        }
    });

    $('#search_item_set_name').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            $('#search_item_set_btn').click();
        }
    });

    /**
     * AJAX商品検索
     */
    $('#search_item_btn').click(function(){
        fieldName = 'item_name';
        fieldValue = $('#search_item_name').val();
        fieldValue = FHConvert.ftoh(FHConvert.fkktohkk(fieldValue),{jaCode:true});
        $('#search_item_name').val(fieldValue);
        $.post(opepath + "/get_item/",
        {
            field: fieldName,
            value: fieldValue
        },
        function (data) {
            var lis = '';
            var obj = jQuery.parseJSON(data);
            var msg = null;
    
            $.each(obj, function(index, o) {
                if (index == 0) {
                    msg = o.msg;
                }
                lis += '<li class="item"><a id="search_item_links" onClick="javascript:insertItemData( '+ o.id +',\'\', \''+ o.internal_code + '\', \''+o.item_name+'\' ,\''+ o.standard + '\',\''+ o.item_code +'\', 1 ,\'\');$(\'#search_item_form\').hide();"> ' + o.internal_code + ' : ' + o.item_name + ' '+ o.standard + ' ' + o.item_code + '</a></li>';
            });
            if (msg != null) {
                lis = '<SPAN><B>' + msg + '</B></SPAN>' + lis;
            }
            $('#search_item_results').html(lis);
            $('#search_item_form').find('div.scrollable').css('height', 300);
            $('#search_item_results').show();
  
        },
        function (success) {
            // What do I do?
        },
        function(error) {
        });
        return false;
    });


    /**
     * AJAX材料セット検索
     */
    $('#search_item_set_btn').click(function(){
        fieldName = 'item_set_name';
        fieldValue = $('#search_item_set_name').val();
        //fieldValue = FHConvert.ftoh(FHConvert.fkktohkk(fieldValue),{jaCode:true});
        $('#search_item_set_name').val(fieldValue);
        $.post(opepath + "/get_item_set_list/",
        {
            field: fieldName,
            value: fieldValue
        },
        function (data) {
            var lis = '';
            var obj = jQuery.parseJSON(data);
            var msg = null;
    
            $.each(obj, function(index, o) {
                if (index == 0) {
                    msg = o.msg;
                }
                lis += '<li class="item"><a id="search_item_links" onClick="javascript:getItemSet( \''+ o.code +'\', false );$(\'#search_item_set_form\').hide();"> ' + o.code + ' : ' + o.name + '</a></li>';
            });
            if (msg != null) {
                lis = '<SPAN><B>' + msg + '</B></SPAN>' + lis;
            }
            $('#search_item_set_results').html(lis);
            $('#search_item_set_form').find('div.scrollable').css('height', 300);
            $('#search_item_set_results').show();
  
        },
        function (success) {
            // What do I do?
        },
        function(error) {
        });
        return false;
    });


     /**
      * Ajax: 術式検索
      */
    $('#search_method_btn').click(function(){
        fieldName = 'method_name';
        fieldValue = $('#search_method_name').val();
        //fieldValue = FHConvert.ftoh(FHConvert.fkktohkk(fieldValue),{jaCode:true});
        $('#search_method_name').val(fieldValue);
        $.post(opepath + "/search_method/",
        {
            field: fieldName,
            value: fieldValue
        },
        function (data) {
            var lis = '';
            var obj = jQuery.parseJSON(data);
            var msg = null;

            $.each(obj, function(index, o) {
                if (index == 0) {
                    msg = o.msg;
                }
                lis += '<li class="item"><a id="search_method_links" onClick="javascript:setMId(' + o.id + ',\'' + o.method_code + '\',\''+ o.method_name +'\');">' + o.method_name + '</a></li>';
            });

            if (msg != null) {
                lis = '<SPAN><B>' + msg + '</B></SPAN>' + lis;
            }

            $('#search_method_results').html(lis);
            $('#search_method_form').find('div.scrollable').css('height', 300);
            $('#search_method_results').show();
        },
        function (success) {
            // What do I do?
        },
        function(error) {
            if(error.length != 0) {
                $('#search_method_results').after('<li class="error-message" id="'+ fieldName +'-exists">' + error + '</li>');
            } else {
                $('#' + fieldName + '-exists').remove();
            }
        });
    return false;

  });

    //登録ボタン押下
    $('#submit_btn').click(function(){
        //登録前チェック
        // 予定日必須
        if($('#OpeDate').val() == "" ){
            alert('予定日が未入力です。');
            $('#OpeDate').focus();
            return false ;
        }

        // 手術室必須
        if($('#OpeRoom').val() == "" ){
            alert('手術室が未選択です。');
            $('#OpeRoom').focus();
            return false ;
        }
        // 担当科必須
        //if($('#opeDepartmentCode').val() == "" ){
        //    alert('担当科が未選択です。');
        //    $('#opeDepartmentCode').focus();
        //    return false ;
        //}
        // 病棟必須
        if($('#wardCode').val() == "" ){
            alert('病棟が未選択です。');
            $('#wardCode').focus();
            return false ;
        }

        $("#Ope_form").attr('action', opepath + '/ope_result').submit();
    });

    //削除ボタン押下
    $('#delete_btn').click(function(){
        if(window.confirm("取消を実行します。よろしいですか？")){
            var id = $('#ope_id').val();
            validateDetachSubmit($('#Ope_form') , opepath + '/del/'+id );
        }
    });

});
  
function setMId(id, code, name) {
    $('#OpeMethod').val(name);
    $('#OpeMethodSetCode').val(code);
    $('#OpeMethodId').val(id);
    $('#search_method_form').hide();
    getItemSet(code,true); //関連付くデータを取得する
}


function getItemSet(code,claer_flg){
    fieldName = 'ope_method_set_code';
    if(claer_flg){
        // 術式を選択
        url = opepath + "/get_item_set/";
    } else {
        // 材料セットを選択
        url = opepath +"/get_item_set_one/";
    }
    $.post(url,
    {
        field: fieldName,
        value: code
    },
    function (data) {
        var lis = '';
        var obj = jQuery.parseJSON(data);
        var item_set = '';
        var msg = null;
        if(claer_flg == true ){
            // いったんクリアする。
            clearItemData();
        }
        $.each(obj, function(index, o) {
            if (index == 0) {
                msg = o.msg;
            }
            
            // 取得データをさしなおす。
            insertItemData(o.id,o.code,o.internal_code,o.item_name,o.standard,o.item_code,o.quantity,o.recital);
            if(o.code != item_set){
                insertItemSetData(o.code, o.name);
                item_set = o.code;
            }
            
        });

        if (msg != null) {
            lis = '<SPAN><B>' + msg + '</B></SPAN>' + lis;
        }

    },
    function (success) {
        // What do I do?
    },
    function(error) {
        
    });
    return false;
}


function insertItemData(id,code,internal_code,item_name,standard,item_code,quantity,recital){
     $('#input_item_line').append(
          $('<tr class="' + code + '">').append(
              $('<td>').append('<input type="hidden" name="data[TrnOpe][item_mst_id]['+ item_count +'] " value="' + id + '">')
                       .append('<input type="hidden" name="data[TrnOpe][code]['+ item_count +'] " value="' + code+ '">')
                       .append($('<label>').attr('title' , code).append(
                                   $('<p class="code center">').text(code)
                        )
              )
          ).append(
              $('<td>').append(
                  $('<label>').attr('title' , internal_code).append(
                      $('<p class="internal_code">').text(internal_code)
                  )
              )
          ).append(
              $('<td>').append(
                  $('<label>').text(item_name +' / '+ standard + ' / ' + item_code ).attr('title' , item_name +' / '+ standard + ' / ' + item_code )
              )
          ).append(
              $('<td>').append(
                  $('<input type="button" value="+" class="item_plus_btn" style="width:25px;">')
              ).append(
                  $('<input type="button" value="-" class="item_minus_btn" style="width:25px;">')
              ).append(
                  $('<input type="text" name="data[TrnOpe][quantity]['+ item_count +']"  value="'+quantity+'" class="right quantity" style="width:30px;" readonly="readonly">')
              )
          ).append(
              $('<td>').append(
                  $('<label>').attr('title' , recital).append(
                      $('<p class="recital">').text(recital)
                  )
                 .append('<input type="hidden" name="data[TrnOpe][recital]['+ item_count +'] " value="' + recital + '">')
              )
          ).append(
              $('<td class="center">').append(
                  $('<input type="button" value="DEL" class="item_del_btn">')
              )
          )
     )
     //追加したボタンにイベントを振りなおす
     $('.item_del_btn').click(function(){
         $(this).parent().parent().remove();
     });
     // 加算処理が2重に定義されるのでいったんイベントを削除する
     $(".item_plus_btn").unbind();
     $('.item_plus_btn').click(function(){
         var quantity = $(this).parent().find('.quantity');
         quantity.val(parseInt(quantity.val()) + 1);
     });
     // 減算処理が2重に定義されるのでいったんイベントを削除する
     $(".item_minus_btn").unbind();
     $('.item_minus_btn').click(function(){
         var quantity = $(this).parent().find('.quantity');
         quantity.val(parseInt(quantity.val()) - 1);
         if(parseInt(quantity.val()) < 1 ){
             $(this).parent().parent().remove();
         }
     });
     item_count++;
}


function insertItemSetData(code,name){
     $('#input_item_set_line').append(
          $('<tr>').append(
              $('<td>').append($('<label>').attr('title' , code).append(
                                   $('<p class="code">').text(code)
                        )
              )
          ).append(
              $('<td>').append($('<label>').attr('title' , name).append(
                                   $('<p>').text(name)
                        )
              )

          ).append(
              $('<td class="center">').append(
                  $('<input type="button" value="DEL" id="' + code + '" class="item_set_del_btn">')
              )
          )
     )
     //追加したボタンにイベントを振りなおす
     $('.item_set_del_btn').click(function(){
         var setCode  = $(this).attr('id');
         $("tr." + setCode).remove();
         $(this).parent().parent().remove();
     });
}

function clearItemData(){
    $('.item_del_btn').each(function(){
         $(this).parent().parent().remove();
    });
    $('.item_set_del_btn').each(function(){
         $(this).parent().parent().remove();
    });
}