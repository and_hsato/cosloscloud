//いり数、換算数
$(function() {
    $('#PerUnitNumber').change(function () {
    var _prceiprocal = Math.round(1 / $('#PerUnitNumber').val()*100)/100;
    $('#ReciprocalNumber').val(_prceiprocal);
    });

    $('#low_level').change(function () {
        re = new RegExp("^([1|3|5|9])([0-9]+)$");
        if($('#low_level').val() == '1' && $('#internal_code').val().match(re)){
            var code = lowlevel + RegExp.$2;
            $('#internal_code').val(code);
        }
        if($('#low_level').val() == '0' && $('#internal_code').val().match(re)){
           var code = prefix + RegExp.$2;
           $('#internal_code').val(code);
        }
    });
});

/* 検索小窓用準備 */
$(document).ready(function(){
    $('#search_dealer_form, #search_jmdn_form, #search_icc_form, #search_item_category_form').bgiframe();
    $('img.floatClose').hover(function(){
        $(this).attr('src', webroot+'img/close2_red.gif');
    }, function(){
        $(this).attr('src', webroot+'img/close2_blk.gif');
    });
    $('img.floatClose').click(function(){
        $(this).parent().hide();
    });
});

/**
 * 販売元検索窓準備
 */
$(function($) {
    $('#search_dealer_form').hide();
    $('#call_dealer_btn').click(function() {
        $('#search_dealer_form').show();
        //$('#search_dealer_form').draggable();
        $('#search_dealer_name').focus();
    });

/**
 * JMDN検索窓準備
 */
    $('#search_jmdn_form').hide();
    $('#call_jmdn_btn').click(function() {
        $('#search_jmdn_form').show();
        //$('#search_jmdn_form').draggable();
        $('#search_jmdn_name').focus();
    });

/**
 * 保険償還検索準備
 */
    $('#search_icc_form').hide();
    $('#call_icc_btn').click(function() {
        $('#search_icc_form').show();
        //$('#search_icc_form').draggable();
        $('#search_icc_name').focus();
    });

/**
 * 商品カテゴリ検索準備
 */
    $('#search_item_category_form').hide();
    $('#call_category_btn').click(function() {
        $('#search_item_category_form').show();
        //$('#search_item_category_form').draggable();
        $('#search_item_category_name').focus();
    });

    $('#class_separation_sel').change(function() {
        $('#class_separation_txt').val($('#class_separation_sel option:selected').text());
    });

    $('#insurance_claim_type_sel').change(function() {
        $('#insurance_claim_type_txt').val($('#insurance_claim_type_sel option:selected').val());
        $('#insurance_claim_type_txt_p').val($('#insurance_claim_type_sel option:selected').val());
    });

    $('#item_type_sel').change(function() {
        $('#item_type_txt').val($('#item_type_sel option:selected').text());
    });

   $('#owner_code_select').change(function() {
       $('#owner_code_txt').val($('#owner_code_select option:selected').val());
   });
});


/**
 * 販売元選択時処理
 * @param id
 * @param name
 */
function setDId(id, code, name) {
    var _id = id;
    var _code = code;
    var _name = name;
    $('#dealer_name_txt').val(name);
    $('#mst_dealer_code_hidden_txt').val(code);
    $('#mst_dealer_id_hidden_txt').val(id);
    $('#search_dealer_form').hide();
}

/**
 * JMDN選択時処理
 * @param code
 * @param name
 */
function setJmdnCode(code, name) {
    var _code = code;
    var _name = name;
    $('#jmdn_code_txt').val(_code);
    $('#mst_jmdn_name_hidden_txt').val(_name);
    $('#mst_jmdn_name_txt').val(_name);
    $('#search_jmdn_form').hide();
}

/**
 * 保険請求選択時処理
 * @param code
 * @param name
 */
function setICCode(code, name, name_s) {
    var _code = code;
    var _name = name;
    var _name_s = name_s;
    $('#icc_code_txt').val(_code);
    $('#mst_icc_name_hidden_txt').val(_name);
    $('#mst_icc_name_txt').val(_name);
    $('#mst_icc_name_s_txt').val(_name_s);
    $('#search_icc_form').hide();
}

/**
 * 商品カテゴリ選択時処理
 * @param code
 * @param name
 */
function setItemCategoryCode(code, name) {
    var _code = code;
    var _name = name;
    $('#item_category_code_hidden_txt').val(_code);
    $('#item_category_name_txt').val(_name);
    $('#search_item_category_form').hide();
}

$(function($) {
    $('#search_dealer_name').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            $('#search_dealer_btn').click();
        }
    });
});

/**
* Ajax: 販売元検索
*/
$(function($) {
    $('#search_dealer_btn').click(function(){
        fieldName = 'dealer_name';
        fieldValue = $('#search_dealer_name').val();
        fieldValue = FHConvert.ftoh(FHConvert.fkktohkk(fieldValue),{jaCode:true});
        $('#search_dealer_name').val(fieldValue);
        $.post(webroot+"mst_facility_items/search_dealer/",
        {
            field: fieldName,
           value: fieldValue
        },
        function (data) {
            var lis = '';
            var obj = jQuery.parseJSON(data);
            var msg = null;

            $.each(obj, function(index, o) {
                if (index == 0) {
                    msg = o.msg;
                }
                lis += '<li class="item"><a id="search_dealer_links" onClick="javascript:setDId(' + o.id + ',\'' + o.dealer_code + '\',\''+ o.dealer_name +'\');">' + o.dealer_name + '</a></li>';
            });

            if (msg != null) {
                lis = '<SPAN><B>' + msg + '</B></SPAN>' + lis;
            }

            $('#search_dealer_results').html(lis);
            $('#search_dealer_form').find('div.scrollable').css('height', 300);
            $('#search_dealer_results').show();
        },
        function (success) {
            // What do I do?
        },
        function(error) {
            if(error.length != 0) {
                $('#search_dealer_results').after('<li class="error-message" id="'+ fieldName +'-exists">' + error + '</li>');
            } else {
                $('#' + fieldName + '-exists').remove();
            }
        });
    return false;
    });
});


$(function($) {
    $('#search_jmdn_name').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            $('#search_jmdn_btn').click();
        }
    });
});

/**
* Ajax: JMDN検索
*/
$(function($) {
    $('#search_jmdn_btn').click(function(){
        fieldName = 'jmdn_name';
        fieldValue = $('#search_jmdn_name').val();
        fieldValue = FHConvert.ftoh(FHConvert.fkktohkk(fieldValue),{jaCode:true});
        $('#search_jmdn_name').val(fieldValue);
        $.post(webroot + "mst_facility_items/search_jmdn_name/",
        {
            field: fieldName,
            value: fieldValue
        },
        function (data) {
            var lis = '';
            var obj = jQuery.parseJSON(data);
            var msg = null;
    
            $.each(obj, function(index, o) {
                if (index == 0) {
                    msg = o.msg;
                }
                lis += '<li class="item"><a id="search_jmdn_links" onClick="javasciprt:setJmdnCode(\'' + o.jmdn_code + '\',\''+ o.jmdn_name +'\');">' + o.jmdn_code + ':' + o.jmdn_name + '</a></li>';
            });

            if (msg != null) {
                lis = '<SPAN><B>' + msg + '</B></SPAN>' + lis;
            }
    
            $('#search_jmdn_results').html(lis);
            $('#search_jmdn_form').find('div.scrollable').css('height', 300);
            $('#search_jmdn_results').show();
        },
        function (success) {
            // What do I do?
        },
        function(error) {
            if(error.length != 0) {
                $('#search_jmdn_results').after('<li class="error-message" id="'+ fieldName +'-exists">' + error + '</li>');
            } else {
                $('#' + fieldName + '-exists').remove();
            }
        });
    return false;
    });
});


$(function($) {
    $('#search_icc_name').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            $('#search_icc_btn').click();
        }
    });
});

/**
* Ajax: 保険請求検索
*/
$(function($) {
    $('#search_icc_btn').click(function(){
        fieldName = 'icc_name';
        fieldValue = $('#search_icc_name').val();
        fieldValue = FHConvert.ftoh(FHConvert.fkktohkk(fieldValue),{jaCode:true});
        $('#search_icc_name').val(fieldValue);
        $.post(webroot + "mst_facility_items/search_icc_name/",
        {
            field: fieldName,
            value: fieldValue
        },
        function (data) {
            var lis = '';
            var obj = jQuery.parseJSON(data);
            var msg = null;

            $.each(obj, function(index, o) {
                if (index == 0) {
                    msg = o.msg;
                }
                lis += '<li class="item"><a id="search_icc_links" onClick="javasciprt:setICCode(\'' + o.insurance_claim_code + '\',\''+ o.insurance_claim_name + '\',\''+ o.insurance_claim_name_s +'\');">' + o.insurance_claim_code + ':' + o.insurance_claim_name_s + '</a></li>';
            });

            if (msg != null) {
                lis = '<SPAN><B>' + msg + '</B></SPAN>' + lis;
            }

            $('#search_icc_results').html(lis);
            $('#search_icc_form').find('div.scrollable').css('height', 300);
            $('#search_icc_results').show();
        },
        function (success) {
            // What do I do?
        },
        function(error) {
            if(error.length != 0) {
                $('#search_icc_results').after('<li class="error-message" id="'+ fieldName +'-exists">' + error + '</li>');
            } else {
                $('#' + fieldName + '-exists').remove();
            }
        });
    return false;
    });
});


$(function($) {
    $('#search_item_category_name').keypress( function ( e ) {
        form = $(this).parents('form');
        if ( e.which == 13 ) { // ENTER で送信
            $('#search_item_category_btn').click();
        }
    });
});

/**
* Ajax: 商品カテゴリ検索
*/
$(function($) {
    $('#search_item_category_btn').click(function(){
        fieldName = 'category_code';
        fieldValue = $('#search_item_category_name').val();
        fieldValue = FHConvert.ftoh(FHConvert.fkktohkk(fieldValue),{jaCode:true});
        $('#search_item_category_name').val(fieldValue);
        $.post(webroot + "mst_facility_items/search_item_category_code/",
        {
            field: fieldName,
            value: fieldValue
        },
        function (data) {
            var lis = '';
            var obj = jQuery.parseJSON(data);
            var msg = null;
            $.each(obj, function(index, o) {
                if (index == 0) {
                    msg = o.msg;
                }
                lis += '<li class="item"><a id="search_item_category_links" onClick="javasciprt:setItemCategoryCode(\'' + o.category_code + '\',\''+ o.category_name + '\');">' + o.category_code + ':' + o.category_name + '</a></li>';
            });

            if (msg != null) {
                lis = '<SPAN><B>' + msg + '</B></SPAN>' + lis;
            }

            $('#search_item_category_results').html(lis);
            $('#search_item_category_form').find('div.scrollable').css('height', 300);
            $('#search_item_category_results').show();
        },
        function (success) {
            // What do I do?
        },
        function(error) {
            if(error.length != 0) {
                $('#search_item_category_results').after('<li class="error-message" id="'+ fieldName +'-exists">' + error + '</li>');
            } else {
                $('#' + fieldName + '-exists').remove();
            }
        });
    return false;
    });
});
