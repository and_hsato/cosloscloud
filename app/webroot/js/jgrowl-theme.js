function jGrowlTheme(type, header, message, image) {
    switch(type) {
        case "mono": 
            var theText = '<img src="' + image + '" class="img-thumb" /><span class="separator">&nbsp;</span>' + message;
            $.jGrowl.defaults.position = 'top-left';
            $.jGrowl(theText, {
                header: header,
                theme: 'themed', 
                life: 10000
            });
        break;
    }
}