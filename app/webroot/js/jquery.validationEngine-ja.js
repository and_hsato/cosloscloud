(function($) {
    $.fn.validationEngineLanguage = function() {};
    $.validationEngineLanguage = {
        newLang : function() {
            $.validationEngineLanguage.allRules = {
                "CheckSelect" : {
                    "nname":"CheckSelect",
                    "alertText":"* 必須選択項目です"
                },
                "required" : {
                    "regex" : "none",
                    "alertText" : "* 必須入力項目です",
                    "alertTextCheckboxMultiple" : "* この選択は必須です",
                    "alertTextCheckboxe" : "* チェックは必須です",
                    "alertTextRadioe"    : "* どれかひとつを選択してください"
                },
                "length" : {
                    "regex" : "none",
                    "alertText" : "* ",
                    "alertText2" : " 文字以上 ",
                    "alertText3" : " 文字以下で入力してください"
                },
                "minSize": {
                    "regex": "none",
                    "alertText": "* ",
                    "alertText2": "文字以上にしてください"
                },
                "maxSize": {
                    "regex": "none",
                    "alertText": "* ",
                    "alertText2": "文字以下にしてください"
                },
                "equals": {
                    "regex": "none",
                    "alertText": "* 入力された値が一致しません"
                },
                "maxCheckbox" : {
                    "regex" : "none",
                    "alertText" : "* チェックできる最大数を超えています"
                },
                "minCheckbox" : {
                    "regex" : "none",
                    "alertText" : "* 最低 ",
                    "alertText2" : " つチェックしてください"
                },
                "confirm" : {
                    "regex" : "none",
                    "alertText" : "* 入力した値が間違っています"
                },
                "telephone" : {
                    "regex" : "/^[0-9\-\(\)\ ]+$/",
                    "alertText" : "* 正しい電話番号ではありません"
                },
                "email" : {
                    "regex" : "/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
                    "alertText" : "* 正しいメールアドレスではありません"
                },
                "date" : {
                    "regex": /^\d{4}[\/](0?[1-9]|1[012])[\/](0?[1-9]|[12][0-9]|3[01])$|^$/,
                    "alertText" : "* 日付が間違っています。 YYYY/MM/DDで入力してください"
                },
                'month' : {
                    "regex":/^[0-9]{4}[\/][0-9]{1,2}$/,
                    "alertText" : "* 年月が間違っています。 YYYY/MMで入力してください"
                },
                "onlyNumber" : {
                    "regex":/^[0-9]+$/,
                    "alertText" : "* 入力値は数値のみです"
                },
                "onlyDecimal" : {
                    "regex":/^[0-9]+(\.[0-9]{1,2})?$/,
                    "alertText" : "* 小数点第2位まで入力可能です"
                },
                "noSpecialCaracters" : {
                    "regex" : "/^[0-9a-zA-Z]+$/",
                    "alertText" : "* 半角英数字のみ入力可能です"
                },
                "ajaxDepartmentCode" : {
                    "url" : "duplicateCheckDepartmentCode",
                    "extraData": "name=eric",
                    "extraDataDynamic": ['#facility_id','#department_id'],
                    "alertText" : "* 部署コードは既に使用されています",
                    "alertTextOk": "* 部署コードは使用できます",
                    "alertTextLoad" : "* しばらくお待ちください"
                },
                "ajaxFacilityCode" : {
                    "url" : "duplicateCheckFacilityCode",
                    "extraData": "name=eric",
                    "extraDataDynamic": ['#facility_id','#facility_type'],
                    "alertText" : "* 仕入先コードは既に使用されています",
                    "alertTextOk": "* 仕入先コードは使用できます",
                    "alertTextLoad" : "* しばらくお待ちください"
                },
                "ajaxUserId" : {
                    "url" : "duplicateCheckUserId",
                    "extraData": "name=eric",
                    "extraDataDynamic": ['#user_id'],
                    "alertText" : "* 利用者IDは既に使用されています",
                    "alertTextOk": "* 利用者IDは使用できます",
                    "alertTextLoad" : "* しばらくお待ちください"
                },
                "ajaxDealerCode" : {
                    "url" : "duplicateCheckDealerCode",
                    "extraData": "name=eric",
                    "extraDataDynamic": ['#dealer_id'],
                    "alertText" : "* 販売元コードは既に使用されています",
                    "alertTextOk": "* 販売元コードは使用できます",
                    "alertTextLoad" : "* しばらくお待ちください"
                },
                "ajaxUnitNameCode" : {
                    "url" : "duplicateCheckUnitNameCode",
                    "extraData": "name=eric",
                    "extraDataDynamic": ['#unitname_id'],
                    "alertText" : "* 単位コードは既に使用されています",
                    "alertTextOk": "* 単位コードは使用できます",
                    "alertTextLoad" : "* しばらくお待ちください"
                },
                "ajaxMethodCode" : {
                    "url" : "duplicateCheckMethodCode",
                    "extraData": "name=eric",
                    "extraDataDynamic": ['#method_id' ,'#facility_id'],
                    "alertText" : "* 術式コードは既に使用されています",
                    "alertTextOk": "* 術式コードは使用できます",
                    "alertTextLoad" : "* しばらくお待ちください"
                },
                "ajaxItemSetCode" : {
                    "url" : "duplicateCheckItemSetCode",
                    "extraData": "name=eric",
                    "extraDataDynamic": ['#itemset_id' , '#facility_id'],
                    "alertText" : "* 材料セットコードは既に使用されています",
                    "alertTextOk": "* 材料セットコードは使用できます",
                    "alertTextLoad" : "* しばらくお待ちください"
                },

                "onlyLetter" : {
                    "regex" : "/^[a-zA-Z\ \']+$/",
                    "alertText" : "* 半角英文字のみ入力可能です"
                },
                "validate2fields" : {
                    "nname" : "validate2fields",
                    "alertText" : "* 姓名を入力してください"
                },
                'Zenkaku' : {
                    "regex" : "/^[\x7F-\xFF]+$/",
                    "alertText" : "* 全角文字を入力してください"
                },
                'ZenkakuKanna' : {
                    "regex" : "/^(?:\xE3\x82[\xA1-\xBF]|\xE3\x83[\x80-\xB6])+$/",
                    "alertText" : "* 全角カナ文字を入力してください"
                },
                'ZenkakuCanna' : {
                    "regex" : "/^(?:\xE3\x81[\x81-\xBF]|\xE3\x82[\x80-\x93])+$/",
                    "alertText" : "* 全角かな文字を入力してください"
                },
                'ZenkakuAlphaNumeric' : {
                    "regex" : "/^[０-９ａ-ｚＡ-Ｚ]+$/",
                    "alertText" : "*　全角英数字を入力してください"
                },
                'ZenkakuAlphabetic' : {
                    "regex" : "/^[ａ-ｚＡ-Ｚ]+$/",
                    "alertText" : "* 全角英字を入力してください"
                },
                'ZenkakuNumeric' : {
                    "regex" : "/^[０-９]+$/",
                    "alertText" : "* 全角数字を入力してください"
                },
                'date2':{
                    "nname":"validate2date",
                    "alertText" : "* 日付が正しくありません。"
                },
                'checkPasswordText':{
                    "regex" : /^[abcdefghijkmnpqrstuvwxyz23456789_-]+$|^$/,
                    "alertText" : "* パスワードの使用できない文字が含まれています。"
                },
                'checkPasswordRule1':{
                    "regex" : /[abcdefghijkmnpqrstuvwxyz]|^$/,
                    "alertText" : "* パスワードは半角英小文字を1文字以上含めてください。"
                },
                'checkPasswordRule2':{
                    "regex" : /[0-9]|^$/,
                    "alertText" : "* パスワードは半角数字を1文字以上含めてください。"
                },
                "checkNotEquals": {
                    "alertText": "* 現在のパスワードと同じ値は使用できません。"
                }
            }
        }
    }
})(jQuery);

$(document).ready(function() {
    // $.validationEngineLanguage.newLang()
    jQuery.validationEngineLanguage.newLang()
});

